EzBiz Web App
=================

EzBiz is an application to manage your business online! More info on http://ezbiz.lk.

This is the main repository of the application which is made on top of ZF2. The modules of the application are separated as independent git repos for the ease of maintenance. Current modules can be found below.

Setup
-----

Clone the project in to your server/local machine, do the following.

Tip: If your project file permissions are different from the ones in the repo, GIT will show it as a change. To avoid this, do
`git config --local core.filemode false`


### Composer

Install composer  

`curl -sS https://getcomposer.org/installer | php`

... and run an update command  

`php composer.phar update`

This will byinstall the necessary packages/dependencies specified in the `composer.json` file. Please refer the "Dependencies" section below for more details on these and other necessary dependencies which should be configured manually.  


### ZF2 modules

By running the previous `php composer.phar update` command, the necessary ezbiz modules will be cloned in to the `vendor/ezbiz/` folder.


### Confugurations

Copy the local config dist file to be your project's configuration file

`cp config/autoload/local.php.dist config/autoload/local.php`

And change it's values accordingly. Ideally, you might only have to change the `servername`, which should be the servername you specify in the virtualhost. Eg: ezbiz.local  


#### Creating log files

Create a `log` folder inside `data` folder, and place a log file inside it with write permissions.

`mkdir -p data/log`
`touch data/log/ezBiz.debug.log`
`chmod -R a+rwx data/log`

Create another log file for AppEngine inside `vendor/appengine/appengine`

`cd vendor/appengine/appengine`
`mkdir -p data/logs`
`touch data/logs/appengine.log`
`chmod -R a+rwx data/logs`  


#### VirtualHost

In the vhost file you create pointing to the project, make sure the following enviroment variables are available and replace their values as necessary. 

```
SetEnv DBHOST ""
SetEnv DBNAME ""
SetEnv DBUSER ""
SetEnv DBPASS ""
SetEnv SSOSERVICE "AppEngine"
SetEnv SSO_API_URL "https://sso.thinkcube.net"
SetEnv USERMGT_API_URL "http://usermgt.thinkcube.net"
```


Dependencies
------------

  
### Phinx - DB migrations

You will get this by getting a `composer update`. To start using it, get a copy of the phinx config dist file in to your project's root

`cp config/phinx/phinx.yml.dist phinx.yml`
 
Edit the new `phinx.yml` file with your database details (last 4 lines - host, name, user, pass) for the DB migrations to work. To access phinx from the terminal, run `php vendor/bin/phinx` from the root of your project.

To update your database to the latest change, simply run `php vendor/bin/phinx migrate`  

For more information on creating migration scripts - http://docs.phinx.org/en/latest/migrations.html#creating-a-new-migration


### WKHTMLTOPDF - PDF exporting

Get the compressed file of the application binary using
`wget http://wkhtmltopdf.googlecode.com/files/wkhtmltopdf-0.9.2-static-amd64.tar.bz2`

Untar it
`tar xvjf wkhtmltopdf-0.9.2-static-amd64.tar.bz2`

Move it to /usr/bin so it's accessible throughout the system
`mv wkhtmltopdf-amd64 /usr/bin/`

Create a shell script on the same path,
`vim /usr/bin/wkhtmltopdf.sh`

And put this content inside it.
`#!/bin/bash
xvfb-run -a -s "-screen 0 640x480x16" wkhtmltopdf-amd64 $*`

Give access for it to be executed
`sudo chmod a+x  /usr/bin/wkhtmltopdf.sh /usr/bin/wkhtmltopdf-amd64`

Install `xvfb`
`apt-get install xvfb`

After installing, make sure it runs by running the command `wkhtmltopdf.sh http://google.com sample.pdf`