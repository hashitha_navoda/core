<?php

use Zend\Session\Container as SessionContainer;

session_start();
if (!function_exists('to_user_date')) {

    function to_user_date($time = false, $format = 'Y-m-d H:i:s')
    {
        $session = new SessionContainer('ezBizUser');

        $timezone = (isset($session->timeZone) && $session->timeZone) ? $session->timeZone : 'UTC';

        try {

            $time = ($time) ? : gmdate($format);
            $from = new DateTimeZone('GMT');
            $to = new DateTimeZone($timezone);
            $orgTime = new DateTime($time, $from);
            $toTime = new DateTime($orgTime->format("c"));
            $toTime->setTimezone($to);

            return $toTime->format($format);
        } catch (Exception $e) {
            return '-';
        }
    }

}