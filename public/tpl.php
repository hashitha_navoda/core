<?php
header("Access-Control-Allow-Origin: *");

if (!isset($_GET['id'])) {
    die('"id" parameter needed');
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset=utf-8 />
        <title></title>
        <link rel="stylesheet" type="text/css" media="screen" href="css/master.css" />
        <script type="text/javascript" src="/js/jquery.1.9.min.js"></script>
        <script type="text/javascript" src="/js/jquery-ui.js"></script>
        <!--[if IE]>
        <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">

        <!-- Optional theme -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">

        <!-- Latest compiled and minified JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    </head>
    <body>
        <div class="loading" style="position: fixed; left: 0px; top: 0px; right: 0px; bottom: 0px; width: 100%; height: 100%; display: block; background: #fff; z-index: 999; opacity: 0.6;"></div>
        <div class="modal-body" id="edit-products-columns">
            <form class="form-horizontal">
                <div>
                    <input type="hidden" name="templateID" value="<?php echo $_GET['id']; ?>" />
                    <input type="text" name="templateName" class="form-control" value="Template" /><br />
                    <input type="hidden" name="documentSizeID" class="form-control" /><br />
                    <textarea name="templateContent" style="width: 100%; height: 500px; font-size: 12px;" class="form-control"></textarea>
                </div>
                <div class="form-group">
                    <label class="col-md-4 control-label input-sm">
                        <input type="checkbox" style="margin-left: 5px; margin-right: 10px;" checked value="1" name="hide_product_table_headers">
                        Hide product table headers
                    </label>
                    <label class="col-md-3 control-label input-sm">
                        <input type="checkbox" style="margin-left: 5px; margin-right: 10px;" checked value="1" name="hide_product_table_borders">
                        Hide product table borders
                    </label>
                    <label class="col-md-3 control-label input-sm">
                        <input type="checkbox" style="margin-left: 5px;" value="1" name="categorize_products" class="templateOptions">
                        Group Products by Category
                    </label>
                </div>
                <table class="table table-bordered table-condensed table-striped table-hover products-table-columns">
                    <thead>
                        <tr>
                            <th>Table Column Name</th>
                            <th width="15%" class="text-center">Column Width</th>
                            <th width="7%" class="text-center">Show</th>
                            <th width="7%" class="text-center">Sort</th>
                        </tr>
                    </thead>
                    <tbody class="ui-sortable" style="">
                        <tr data-item="product_index" class="added" style="">
                            <td class="small" style="text-decoration: none;">#</td>
                            <td class="text-center">
                                <div class="input-group input-group-sm">
                                    <input type="number" aria-label="Width Percentage" class="form-control" max="99" min="1" disabled>
                                    <span class="input-group-addon">%</span>
                                </div>
                            </td>
                            <td class="text-center check"><input type="checkbox"></td>
                            <td style="cursor: move;" class="text-center sorter-handle"><span class="fa fa-bars"></span></td>
                        </tr><tr data-item="product_code" class="added" style="">
                            <td class="small" style="text-decoration: none;">Product Code</td>
                            <td class="text-center">
                                <div class="input-group input-group-sm">
                                    <input type="number" aria-label="Width Percentage" class="form-control" value="10" max="99" min="1">
                                    <span class="input-group-addon">%</span>
                                </div>
                            </td>
                            <td class="text-center check"><input type="checkbox" checked></td>
                            <td style="cursor: move;" class="text-center sorter-handle"><span class="fa fa-bars"></span></td>
                        </tr><tr data-item="product_name" class="added" style="">
                            <td class="small" style="text-decoration: none;">Product Name</td>
                            <td class="text-center">
                                <div class="input-group input-group-sm">
                                    <input type="number" aria-label="Width Percentage" class="form-control" value="60" max="99" min="1">
                                    <span class="input-group-addon">%</span>
                                </div>
                            </td>
                            <td class="text-center check"><input type="checkbox" checked></td>
                            <td style="cursor: move;" class="text-center sorter-handle"><span class="fa fa-bars"></span></td>
                        </tr>
                        <tr data-item="product_description" class="added" style="">
                            <td class="small" style="text-decoration: none;">Product Description</td>
                            <td class="text-center">
                                <div class="input-group input-group-sm">
                                    <input type="number" aria-label="Width Percentage" class="form-control" value="60" max="99" min="1">
                                    <span class="input-group-addon">%</span>
                                </div>
                            </td>
                            <td class="text-center check"><input type="checkbox" checked></td>
                            <td style="cursor: move;" class="text-center sorter-handle"><span class="fa fa-bars"></span></td>
                        </tr>
                        <tr data-item="price" class="added" style="">
                            <td class="small" style="text-decoration: none;">Price</td>
                            <td class="text-center">
                                <div class="input-group input-group-sm">
                                    <input type="number" aria-label="Width Percentage" class="form-control" value="10" max="99" min="1">
                                    <span class="input-group-addon">%</span>
                                </div>
                            </td>
                            <td class="text-center check"><input type="checkbox" checked></td>
                            <td style="cursor: move;" class="text-center sorter-handle"><span class="fa fa-bars"></span></td>
                        </tr>
                        <tr data-item="mrp-price" class="added" style="">
                            <td class="small" style="text-decoration: none;">Mrp</td>
                            <td class="text-center">
                                <div class="input-group input-group-sm">
                                    <input type="number" aria-label="Width Percentage" class="form-control" value="10" max="99" min="1">
                                    <span class="input-group-addon">%</span>
                                </div>
                            </td>
                            <td class="text-center check"><input type="checkbox" checked></td>
                            <td style="cursor: move;" class="text-center sorter-handle"><span class="fa fa-bars"></span></td>
                        </tr>
                        <tr data-item="price_w_tax" class="added" style="">
                            <td class="small" style="text-decoration: none;">Price With Tax</td>
                            <td class="text-center">
                                <div class="input-group input-group-sm">
                                    <input type="number" aria-label="Width Percentage" class="form-control" value="10" max="99" min="1">
                                    <span class="input-group-addon">%</span>
                                </div>
                            </td>
                            <td class="text-center check"><input type="checkbox"></td>
                            <td style="cursor: move;" class="text-center sorter-handle"><span class="fa fa-bars"></span></td>
                        </tr>
                        <tr data-item="quantity" class="added" style="">
                            <td class="small" style="text-decoration: none;">Quantity</td>
                            <td class="text-center">
                                <div class="input-group input-group-sm">
                                    <input type="number" aria-label="Width Percentage" class="form-control" value="10" max="99" min="1">
                                    <span class="input-group-addon">%</span>
                                </div>
                            </td>
                            <td class="text-center check"><input type="checkbox" checked></td>
                            <td style="cursor: move;" class="text-center sorter-handle"><span class="fa fa-bars"></span></td>
                        </tr>
                        <tr data-item="discount" class="added" style="">
                            <td class="small" style="text-decoration: none;">Discount</td>
                            <td class="text-center">
                                <div class="input-group input-group-sm">
                                    <input type="number" aria-label="Width Percentage" class="form-control" max="99" min="1" disabled>
                                    <span class="input-group-addon">%</span>
                                </div>
                            </td>
                            <td class="text-center check"><input type="checkbox"></td>
                            <td style="cursor: move;" class="text-center sorter-handle"><span class="fa fa-bars"></span></td>
                        </tr>
                        <tr data-item="discount_per_or_val" class="added" style="">
                            <td class="small" style="text-decoration: none;">Discount (Percentage / Value)</td>
                            <td class="text-center">
                                <div class="input-group input-group-sm">
                                    <input type="number" aria-label="Width Percentage" class="form-control" max="99" min="1" disabled>
                                    <span class="input-group-addon">%</span>
                                </div>
                            </td>
                            <td class="text-center check"><input type="checkbox"></td>
                            <td style="cursor: move;" class="text-center sorter-handle"><span class="fa fa-bars"></span></td>
                        </tr>
                        <tr data-item="total" class="added" style="">
                            <td class="small" style="text-decoration: none;">Total</td>
                            <td class="text-center">
                                <div class="input-group input-group-sm">
                                    <input type="number" aria-label="Width Percentage" class="form-control" value="10" max="99" min="1">
                                    <span class="input-group-addon">%</span>
                                </div>
                            </td>
                            <td class="text-center check"><input type="checkbox" checked></td>
                            <td style="cursor: move;" class="text-center sorter-handle"><span class="fa fa-bars"></span></td>
                        </tr>
                        <tr data-item="total_wo_tx" class="added" style="">
                            <td class="small" style="text-decoration: none;">Total (Total without Tax)</td>
                            <td class="text-center">
                                <div class="input-group input-group-sm">
                                    <input type="number" aria-label="Width Percentage" class="form-control" value="10" max="99" min="1">
                                    <span class="input-group-addon">%</span>
                                </div>
                            </td>
                            <td class="text-center check"><input type="checkbox" checked></td>
                            <td style="cursor: move;" class="text-center sorter-handle"><span class="fa fa-bars"></span></td>
                        </tr>
                        <tr data-item="product_code_and_name" style="">
                            <td class="small">Product Code &amp; Name</td>
                            <td class="text-center">
                                <div class="input-group input-group-sm">
                                    <input type="number" aria-label="Width Percentage" class="form-control" max="99" min="1" disabled>
                                    <span class="input-group-addon">%</span>
                                </div>
                            </td>
                            <td class="text-center check"><input type="checkbox"></td>
                            <td style="cursor: move;" class="text-center sorter-handle text-muted"><span class="fa fa-bars"></span></td>
                        </tr>
                        <tr data-item="attribute_01" style="">
                            <td class="small">
                                <select disabled="true" class="attribute" id="attribute_01_select">
                                    <option value="0">Select an item attribute</option>
                                </select>
                            </td>
                            <td class="text-center">
                                <div class="input-group input-group-sm">
                                    <input type="number" aria-label="Width Percentage" class="form-control" max="99" min="1" disabled>
                                    <span class="input-group-addon">%</span>
                                </div>
                            </td>
                            <td class="text-center check"><input type="checkbox"></td>
                            <td style="cursor: move;" class="text-center sorter-handle text-muted"><span class="fa fa-bars"></span></td>
                        </tr>
                        <tr data-item="attribute_02" style="">
                            <td class="small">
                                <select disabled="true" class="attribute" id="attribute_02_select">
                                    <option value="0">Select an item attribute</option>
                                </select>
                            </td>
                            <td class="text-center">
                                <div class="input-group input-group-sm">
                                    <input type="number" aria-label="Width Percentage" class="form-control" max="99" min="1" disabled>
                                    <span class="input-group-addon">%</span>
                                </div>
                            </td>
                            <td class="text-center check"><input type="checkbox"></td>
                            <td style="cursor: move;" class="text-center sorter-handle text-muted"><span class="fa fa-bars"></span></td>
                        </tr>
                        <tr data-item="attribute_03" style="">
                            <td class="small">
                                <select disabled="true" class="attribute" id="attribute_03_select">
                                    <option value="0">Select an item attribute</option>
                                </select>
                            </td>
                            <td class="text-center">
                                <div class="input-group input-group-sm">
                                    <input type="number" aria-label="Width Percentage" class="form-control" max="99" min="1" disabled>
                                    <span class="input-group-addon">%</span>
                                </div>
                            </td>
                            <td class="text-center check"><input type="checkbox"></td>
                            <td style="cursor: move;" class="text-center sorter-handle text-muted"><span class="fa fa-bars"></span></td>
                        </tr>
                        
                    </tbody>
                </table>

                <table class="table table-bordered table-condensed table-striped table-hover products-table-footer-rows">
                    <thead>
                        <tr>
                            <th>Footer Row</th>
                            <th width="7%" class="text-center">Show</th>
                            <th width="7%" class="text-center">Sort</th>
                        </tr>
                    </thead>
                    <tbody class="ui-sortable" style="">
                        <tr data-item="sub_total" class="added" style="">
                            <td class="small" style="text-decoration: none;">Sub Total</td>
                            <td class="text-center check"><input type="checkbox" checked></td>
                            <td style="cursor: move;" class="text-center sorter-handle"><span class="fa fa-bars"></span></td>
                        </tr>
                        <tr data-item="sub_total_wo_tax" class="added" style="">
                            <td class="small" style="text-decoration: none;">Sub Total (without Tax)</td>
                            <td class="text-center check"><input type="checkbox" checked></td>
                            <td style="cursor: move;" class="text-center sorter-handle"><span class="fa fa-bars"></span></td>
                        </tr>
                        <tr data-item="discount" class="added" style="">
                            <td class="small" style="text-decoration: none;">Discount</td>
                            <td class="text-center check"><input type="checkbox" checked></td>
                            <td style="cursor: move;" class="text-center sorter-handle"><span class="fa fa-bars"></span></td>
                        </tr><tr data-item="vat" class="added" style="">
                            <td class="small" style="text-decoration: none;">VAT</td>
                            <td class="text-center check"><input type="checkbox" checked></td>
                            <td style="cursor: move;" class="text-center sorter-handle"><span class="fa fa-bars"></span></td>
                        </tr><tr data-item="total" class="added" style="">
                            <td class="small" style="text-decoration: none;">Total</td>
                            <td class="text-center check"><input type="checkbox" checked></td>
                            <td style="cursor: move;" class="text-center sorter-handle"><span class="fa fa-bars"></span></td>
                        </tr>



                    </tbody>
                </table>

            </form>
            <div class="text-right">
                <button class="btn btn-primary" type="submit" style="position: fixed; top: 10px; right: 10px; ">Submit</button>
            </div>
        </div>
        <script type="text/javascript">
            $(function() {
                var attributeData = {};
                var $edit_products_modal = $('#edit-products-columns');

                $("button[type='submit']").click(function(e) {

                    var $products_columns = $('table.products-table-columns');
                    var $products_footer_rows = $('table.products-table-footer-rows');

                    $edit_products_modal.modal('hide');

                    var product_table_data = {
                        hide_table_borders: $("input[name='hide_product_table_borders']", $edit_products_modal).prop('checked'),
                        hide_table_header: $("input[name='hide_product_table_headers']", $edit_products_modal).prop('checked'),
                    };

                    var columns = [];
                    $('tbody tr', $products_columns).each(function(index, $column) {
                        var $tr = $(this);

                        columns.push({
                            name: $tr.data('item'),
                            show: $("input[type='checkbox']", $tr).prop('checked'),
                            width: $("input[type='number']", $tr).val(),
                            order: index
                        });
                    });

                    var rows = [];
                    $('tbody tr', $products_footer_rows).each(function(index, $row) {
                        var $tr = $(this);

                        rows.push({
                            name: $tr.data('item'),
                            show: $("input[type='checkbox']", $tr).prop('checked'),
                            order: index
                        });
                    });

                    product_table_data.columns = columns;
                    product_table_data.rows = rows;

                    /**/


                    var data = {};
                    data.templateID = $("input[name='templateID']").val();
                    data.documentSizeID = $("input[name='documentSizeID']").val();
                    data.templateName = ($("input[name='templateName']").val()).trim();
                    data.templateContent = ($("textarea[name='templateContent']").val()).trim();

                    var templateOptions = {};
                    templateOptions.categorize_products = false;
                    templateOptions.product_table = product_table_data;
                    templateOptions.categorize_products = $("input[name='categorize_products']").prop('checked');

                    data.templateOptions = templateOptions;
                    data.attributeData = attributeData;
                    $('.loading').show();

                    $.ajax({
                        type: 'POST',
                        url: '/api/template/update',
                        data: data,
                        success: function(respond) {
                            $('.loading').hide();
                        }
                    });

                    e.preventDefault();
                    return false;
                });

                $.ajax({
                    type: 'POST',
                    url: '/api/template/get',
                    data: {templateID: <?php echo $_GET['id']; ?>},
                    success: function(respond) {
                        $('.loading').hide();
                        console.log(respond.data);

                        if (respond.data.documentTypeID == "4") {
                            $('.products-table-footer-rows').addClass('hidden');
                        }

                        $.each(respond.data.itemAttributes, function(key, value) {   
                            $('#attribute_01_select')
                                 .append($("<option></option>")
                                            .attr("value",key)
                                            .text(value));
                            $('#attribute_02_select')
                                 .append($("<option></option>")
                                            .attr("value",key)
                                            .text(value));
                            $('#attribute_03_select')
                                 .append($("<option></option>")
                                            .attr("value",key)
                                            .text(value));

                        });

                        $("textarea[name='templateContent']").val(respond.data.templateContent);
                        $("input[name='templateName']").val(respond.data.templateName);
                        $("input[name='documentSizeID']").val(respond.data.documentSizeID);

                        $("input[name='categorize_products']").prop('checked', (respond.data.templateOptions.categorize_products === true || respond.data.templateOptions.categorize_products == "true"));
                        var product_table_data = respond.data.templateOptions.product_table;
                        var templateItemAttrData = respond.data.templateItemAtrribute;
                        getInvoiceProductsTableEditor(product_table_data, templateItemAttrData);
//                        $("input[name='hide_product_table_headers']", $edit_products_modal).prop('checked', (respond.data.hide_table_header === true || product_table_data.hide_table_header == "true"));
//                        $("input[name='hide_product_table_borders']", $edit_products_modal).prop('checked', (respond.data.hide_table_borders === true || product_table_data.hide_table_borders == "true"));

                    }
                });

                function getInvoiceProductsTableEditor(product_table_data, templateItemAttrData) {

                    var $products_columns = $('table.products-table-columns');
                    var $products_footer_rows = $('table.products-table-footer-rows');

                    $("input[name='hide_product_table_headers']", $edit_products_modal).prop('checked', (product_table_data.hide_table_header === true || product_table_data.hide_table_header == "true"));
                    $("input[name='hide_product_table_borders']", $edit_products_modal).prop('checked', (product_table_data.hide_table_borders === true || product_table_data.hide_table_borders == "true"));

//                    $edit_products_modal.modal('show');

                    $('tbody tr', $products_columns).removeClass('added');
                    $('tbody tr', $products_footer_rows).removeClass('added');
                    $.each(product_table_data.columns, function(index, column) {
                        var $last_added_item = $('tbody tr.added:last', $products_columns);
                        var $target_item = $("tbody tr[data-item='" + column.name + "']", $products_columns);

                        if ($last_added_item.length) {
                            $last_added_item.after($target_item);
                        }

                        $("input[type='checkbox']", $target_item).prop('checked', (column.show === true || column.show == "true")).trigger('change');

                        if (column.show === true || column.show == "true") {
                            $target_item.addClass('added');
                            setTimeout(function() {
                                $("input[type='number']", $target_item).val(column.width);
                            }, 100)
                        }
                    });

                    $.each(product_table_data.rows, function(index, row) {
                        var $last_added_item = $('tbody tr.added:last', $products_footer_rows);
                        var $target_item = $("tbody tr[data-item='" + index + "']", $products_footer_rows);

                        if ($last_added_item.length) {
                            $last_added_item.after($target_item);
                        }

                        $("input[type='checkbox']", $target_item).prop('checked', (row.show === true || row.show == "true")).trigger('change');
                        if (row.show === true || row.show == "true") {
                            $target_item.addClass('added');
                        }
                    });

                    // to help maintain table row with when being sorted
                    var fixHelper = function(e, ui) {
                        ui.children().each(function() {
                            $(this).width($(this).width());
                        });
                        return ui;
                    };

                    // for both products columns editor table and footer row editor table, do
                    $.each([$products_columns, $products_footer_rows], function(index, $table) {

                        $("input[type='checkbox']", $table).unbind('click').click(function(e) {
                            e.stopPropagation();
                        }).unbind('change').change(function(e) {
                            var checked = $(this).is(':checked');
                            var $parentTr = $(this).parents('tr');

                            $("input[type='number']", $parentTr).val('').prop('disabled', !checked);

                            if (!checked) {
                                if ($parentTr[0]['dataset']['item'] == "attribute_01" || $parentTr[0]['dataset']['item'] == "attribute_02" || $parentTr[0]['dataset']['item'] == "attribute_03") {
                                    $('.attribute', $parentTr).attr('disabled', true);
                                    $('.attribute', $parentTr).val("0");
                                    delete attributeData[$parentTr[0]['dataset']['item']];
                                } else {
                                    $("td.small", $parentTr).addClass('text-muted').css('text-decoration', 'line-through');
                                }
                                $("span.fa-bars", $parentTr).parents('td').addClass('text-muted');
                            } else {
                                if ($parentTr[0]['dataset']['item'] == "attribute_01" || $parentTr[0]['dataset']['item'] == "attribute_02" || $parentTr[0]['dataset']['item'] == "attribute_03") {
                                    $('.attribute', $parentTr).attr('disabled', false);
                                    if (!$.isEmptyObject(templateItemAttrData)) {
                                        if ($parentTr[0]['dataset']['item'] == "attribute_01" && templateItemAttrData['attributeOneID'] != null) {
                                            $('.attribute', $parentTr).val(templateItemAttrData['attributeOneID']);
                                            attributeData[$parentTr[0]['dataset']['item']] = templateItemAttrData['attributeOneID'];
                                        } else if ($parentTr[0]['dataset']['item'] == "attribute_02" && templateItemAttrData['attributeTwoID'] != null) {
                                            $('.attribute', $parentTr).val(templateItemAttrData['attributeTwoID']);
                                            attributeData[$parentTr[0]['dataset']['item']] = templateItemAttrData['attributeTwoID'];
                                        } else if ($parentTr[0]['dataset']['item'] == "attribute_03" && templateItemAttrData['attributeThreeID'] != null) {
                                            $('.attribute', $parentTr).val(templateItemAttrData['attributeThreeID']);
                                            attributeData[$parentTr[0]['dataset']['item']] = templateItemAttrData['attributeThreeID'];
                                        }
                                    } 
                                } else {
                                    $("td.small", $parentTr).removeClass('text-muted').css('text-decoration', 'none');
                                }
                                $("span.fa-bars", $parentTr).parents('td').removeClass('text-muted');
                            }

                            // products table columns sort
                            $('tbody', $table).sortable({
                                handle: ".sorter-handle:not(.text-muted)",
                                helper: fixHelper,
                                axis: 'y',
                                forcePlaceholderSize: true,
                                placeholder: "tr"
                            }).disableSelection();

                            $('.attribute', $parentTr).change(function(event) {
                                if (checked) {
                                    attributeData[$parentTr[0]['dataset']['item']] = $(this).val();
                                }
                            });

                        }).trigger('change');

                        // if clicked outside of the input checkbox, still count as a click on the input
                        $("td.check", $table).unbind('click').click(function() {
                            var $check = $("input[type='checkbox']", $(this));
                            $check.prop('checked', !$check.prop('checked')).trigger('change');
                        });

                    });

                }
            });
        </script>
    </body>
</html>