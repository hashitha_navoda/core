<?php

/**
 * @author Malitta Nanayakkara <malitta@thinkcube.net>
 *
 * This script is used to run the phinx migration scripts through the URL.
 *
 * Since the Apache Server Env variables cannot be accessed through the PHP CLI, this was made as a
 * workaround to create a phinxconfig script with the DB config details stored as Env variables and
 * run the phinx migration tool with the created config file.
 *
 * @optional Pass `force` parameter to create the config file even if it exists
 * before running the migration tool. This can be run if DB config details were later changed.
 */
chdir('../'); // switch to root directory
// as a security measure, pass beyond this point only if header is sent
if (!isset($_SERVER['HTTP_MIGRATOR'])) {
    header("HTTP/1.0 404 Not Found");
    exit;
}

define('DOC_ROOT', getcwd());

$migrationFilesDir = 'data/migrations';
$phinxConfigFilesDir = 'config/phinx';

$migrationFilesFullDir = DOC_ROOT . '/' . $migrationFilesDir;
$phinxConfigFilesFullDir = DOC_ROOT . '/' . $phinxConfigFilesDir;

$server = getenv('HTTP_HOST');

$db_host = getenv('DBHOST');
$db_name = getenv('DBNAME');
$db_user = getenv('DBUSER');
$db_pass = getenv('DBPASS');

// check if phinx folders are available
if (is_dir($migrationFilesFullDir) && is_dir($phinxConfigFilesDir)) {

    $confFile = $server . '.phinxconf.php';
    $confFileFullPath = $phinxConfigFilesFullDir . '/' . $confFile;

    // create config file if it doesn't exist
    if (!file_exists($confFileFullPath) || isset($_GET['force'])) {

        $fileContent = <<<EOF

<?php

return array(
    "paths" => array(
        "migrations" => "application/migrations"
    ), "environments" => array(
        "default_migration_table" => "phinxlog",
        "default_database" => "dev",
        "dev" => array(
            "adapter" => "mysql",
            "host" => '{$db_host}',
            "name" => '{$db_name}',
            "user" => '{$db_user}',
            "pass" => '{$db_pass}'
        )
    )
);

EOF;

        file_put_contents($confFileFullPath, $fileContent);
    }

    // run migration
    exec("php vendor/bin/phinx migrate --configuration=\"{$phinxConfigFilesDir}/$confFile\"", $output);
    echo print_r($server, true), "\n";
    echo print_r($output, true), "\n\n";
} else {
    die('Phinx Conf/Migration folders are not set!');
}

