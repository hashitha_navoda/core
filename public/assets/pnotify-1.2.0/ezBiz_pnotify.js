/*********** Custom Stacks ***********
 * A stack is an object which PNotify uses to determine where
 * to position notices. A stack has two mandatory variables, dir1
 * and dir2. dir1 is the first direction in which the notices are
 * stacked. When the notices run out of room in the window, they
 * will move over in the direction specified by dir2. The directions
 * can be "up", "down", "right", or "left". Stacks are independent
 * of each other, so a stack doesn't know and doesn't care if it
 * overlaps (and blocks) another stack. The default stack, which can
 * be changed like any other default, goes down, then left. Stack
 * objects are used and manipulated by PNotify, and therefore,
 * should be a variable when passed. So, calling something like
 *
 * $.pnotify({stack: {"dir1": "down", "dir2": "left"}});
 *
 * will **NOT** work. It will create a notice, but that notice will
 * be in its own stack and may overlap other notices.
 */


/*********** Positioned Stack ***********
 * This stack is initially positioned through code instead of CSS.
 * This is done through two extra variables. firstpos1 and firstpos2
 * are pixel values, relative to a viewport edge. dir1 and dir2,
 * respectively, determine which edge. It is calculated as follows:
 *
 * - dir = "up" - firstpos is relative to the bottom of viewport.
 * - dir = "down" - firstpos is relative to the top of viewport.
 * - dir = "right" - firstpos is relative to the left of viewport.
 * - dir = "left" - firstpos is relative to the right of viewport.
 */

var positions = {
    stack_bottomleft: {"dir1": "up", "dir2": "right", "firstpos1": 25, "firstpos2": 25},
    stack_bottomright: {"dir1": "up", "dir2": "left", "firstpos1": 25, "firstpos2": 25}
};

function p_notification(status, msg, pos, time) {
	time =(time == '')? 3000 : time;
    pos = (pos == undefined||pos == '') ? positions.stack_bottomleft : positions[pos];

    var type, title;
    if (status === true) {
        title = "Success";
        type = 'success';
    } else if (status == 'info') {
        title = "Info";
        type = 'info';
    } else if(status == 'cust details'){
    	title = "Customer Details";
        type = 'info';
    } else{
        title = "Error";
        type = 'error';
    }
    
    $.pnotify({
        title: title,
        text: msg,
        type: type,
        animation: 'fade',
        delay: time,
        stack: pos
    });
}