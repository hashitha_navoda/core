/**
 * @author ASHAN <ashan@thinkcube.com>
 * This file contains form validation
 */

var locationIn;
var customerID;
var salesOrderID = '';
var batchProducts = {};
var batchSerialProducts = {};
var serialProducts = {};
var checkSubProduct = {};
var deliverProducts = {};
var methods = [];
var productsTotal = {};
var productsTax = {};
var deliverSubProducts = {};
var SubProductsQty = {};
var currentProducts = {};
var selectedPID;
var locationID;
var customCurrencyRate = 1;
var customercredit = 0;
var cusProfID;
var locationProducts;
var dimensionData = {};
var selectedAccount;
var existTaxArray = [];
var selectedAuthourType = 1;
var ignoreBudgetLimitFlag = false;
var inventoryItemFlag = false;
var tmpFinalTotal = 0;
var loyalty = {};

loyalty.isAvailable = function() {
    return loyalty.cust_loyal_id != undefined ? true : false;
};

loyalty.calculateLoyaltyPoints = function(paidAmount, totalBill) {
    if (loyalty.min <= totalBill && loyalty.max >= totalBill) {
        loyalty.earned_points = totalBill / loyalty.earning_ratio;
    }
    else if (loyalty.max <= totalBill) {
        loyalty.earned_points = loyalty.max / loyalty.earning_ratio;
    }
};

loyalty.isValid = function(paidAmount, totalBill) {

    loyalty.earned_points = 0;
    loyalty.redeemed_points = 0;

    if (!loyalty.isAvailable) {
        return false;
    }


    if (loyalty.available_points * loyalty.redeem_ratio >= paidAmount) {
        loyalty.redeemed_points = paidAmount / loyalty.redeem_ratio;
        return true;
    }
    return false;
};

loyalty.getLoyaltySaveData = function() {
    if (!loyalty.isAvailable()) {
        return {};
    }

    return {
        cust_loyalty_id: loyalty.cust_loyal_id,
        earned_points: loyalty.earned_points,
        redeemed_points: loyalty.redeemed_points,
        current_points: loyalty.available_points,
    };
};

$(document).ready(function() {
    var nonItemRowId = 2;
    var decimalPoints = 0;
    var currentItemTaxResults;
    var currentTaxAmount;
    var totalTaxList = {};
    var $productTable = $("#productTable");
    var $addRowSample = $('tr.add-row.sample.hidden', $productTable);
    var $scanSubmit = false;
    var priceListItems = [];
    var priceListId = '';
    var defaultSellingPriceData = [];
    var dimensionArray = {};
    var dimensionTypeID = null;

    loadDropDownFromDatabase('/accounts-api/search-active-accounts-for-dropdown', "", "", '#cashGlAccountID');
    loadDropDownFromDatabase('/accounts-api/search-active-accounts-for-dropdown', "", "", '#chequeGlAccountID');
    loadDropDownFromDatabase('/accounts-api/search-active-accounts-for-dropdown', "", "", '#creditGlAccountID');
    loadDropDownFromDatabase('/accounts-api/search-active-accounts-for-dropdown', "", "", '#bankTransferGlAccountID');
    loadDropDownFromDatabase('/accounts-api/search-active-accounts-for-dropdown', "", "", '#lcGlAccountID');
    loadDropDownFromDatabase('/accounts-api/search-active-accounts-for-dropdown', "", "", '#ttGlAccountID');

    invoiceLeftToPay = new Array();
    var flag = 0;
    var paymentTotal = 0.00;
    var restBalance = 0.00;
    var paymentMethodIncrement = 1;
    var oldDiscountAmount = 0.00;

    $("#paymentbill").hide();
    $("#creditGroup").hide();
    $("#checkGroup").hide();
    $("#bankTransferGroup").hide();
    $("#giftCardGroup").hide();
    $("#postdatedChequeGroup").hide();
    $("#lcGroup").hide();
    $("#ttGroup").hide();
    $(".postdated-cheque-date").hide();

    $(document).on('change', '.postdated-cheque', function() {
        if ($(this).is(":checked")) {
            $(this).parents('.payMethod').find(".postdated-cheque-date").show();
        } else {
            $(this).parents('.payMethod').find(".postdated-cheque-date").hide();
        }
    });

    $('#creditAmount').on('change', function() {
        var creditAmount = $(this).val();
        if (customercredit == 0.00) {
            p_notification(false, eb.getMessage('ERR_PAY_CUST_NOCRDT_BAL'));
            $(this).val(0.00);
        } else if (isNaN(creditAmount) || creditAmount < 0) {
            p_notification(false, eb.getMessage('ERR_PAY_CREDIT_AMOUNT_SHOUDBE_NUMBER'));
            $(this).val(0.00);
        } else {
            var totalPaidAmount = parseFloat(getTotalPaidAmount());
            var creditAmountCanPay = paymentTotal - totalPaidAmount;
            if (creditAmountCanPay < creditAmount) {
                p_notification(false, eb.getMessage('ERR_PAY_CAMOUNT_AM_CANT_BE_MORE_THAN_RESTPAID'));
                $(this).val(0.00);
            }
        }
        setCreditAmountAndRestToPaid();

    });
    
    $('#paidAmount').on('change', function() {
        var paidAmount = $(this).val();
        var creditAmount = $('#creditAmount').val();
        if (creditAmount > 0) {
            var totalPaidAmount = parseFloat(getTotalPaidAmount());
            var restToPaid = paymentTotal - totalPaidAmount - creditAmount;
            if (restToPaid < 0) {
                p_notification('info', eb.getMessage('INFO_PAY_RESTPAID_CANT_BE_MORE_THAN_CAMOUNT'));
                var restToPaidValue = Math.abs(restToPaid);
                var newCreditAmount = creditAmount - restToPaidValue;
                if (newCreditAmount < 0) {
                    newCreditAmount = 0;
                }
                $('#creditAmount').val(newCreditAmount)
            }
        }
        setCreditAmountAndRestToPaid();
    });

    $('#discountAmount').on('change', function() {
        var discount = $('#discountAmount').val();
        if (discount < 0 || isNaN(discount)) {
            p_notification(false, eb.getMessage('ERR_PAY_DISCOUNT_AMOUNT_SHOUDBE_NUMBER'));
            $(this).val(0.00);
        } else {
            if (paymentTotal <= discount) {
                p_notification(false, eb.getMessage('ERR_PAY_DISCOUNT_AMOUNT'));
                $(this).val(0.00);
            } else {
                var creditAmount = parseFloat(($('#creditAmount').val() != '') ? $('#creditAmount').val() : 0);
                var totalPaidAmount = parseFloat(getTotalPaidAmount());
                var discountCanGive = parseFloat(paymentTotal) - totalPaidAmount - creditAmount + oldDiscountAmount;
                if (discountCanGive < discount) {
                    p_notification(false, eb.getMessage('ERR_PAY_DISC_AM_CANT_BE_MORE_THAN_RESTPAID'));
                    $(this).val(0.00);
                }
            }
        }
        setPaymentTotal();
    });
    
    $(document).on('change', '.paymentMethod', function() {
        $(".paymentMethod option[value='1']").attr('disabled', false);
        var paymentMethods = $(this).val();
        var $payMethod = $(this).parents('.payMethod');
        $("#cashGroup", $payMethod).hide();
        $("#paidAmount", $payMethod).attr('disabled', false);
        $("#creditGroup", $payMethod).hide();
        $("#checkGroup", $payMethod).hide();
        $("#bankTransferGroup", $payMethod).hide();
        $("#giftCardGroup", $payMethod).hide();
        $("#postdatedChequeGroup", $payMethod).hide();
        $("#lcGroup", $payMethod).hide();
        $("#ttGroup", $payMethod).hide();

        if(paymentMethods == 1){
            $("#cashGroup", $payMethod).show();
              $(".paymentMethod option[value='1']").attr('disabled', 'disabled');
        }else if (paymentMethods == 3) {
            $("#creditGroup", $payMethod).show();
        } else if (paymentMethods == 2) {
            $(this).parents('.payMethod').find("#creditGroup").hide();
            $(this).parents('.payMethod').find("#bankTransferGroup").hide();
            //for postdated cheque
            $(this).parents('.payMethod').find("#postdatedChequeGroup").show();
            $(this).parents('.payMethod').find("#checkGroup").show();

        } else if (paymentMethods == 4) {

            if (loyalty.isAvailable()) {
                $(this).parents('.payMethod').find("#creditGroup").hide();
                $(this).parents('.payMethod').find("#checkGroup").hide();
                $(this).parents('.payMethod').find("#bankTransferGroup").hide();
//                $(this).parents('.payMethod').find("#loyalty_details").show();
            } else {
                p_notification(false, eb.getMessage('ERR_DOESNT_HAVE_LOYALTY_CARD'));
                $('.paymentMethod').val('1').change();
            }



        } else if (paymentMethods == 5) {
            $("#bankTransferGroup", $payMethod).show();
        } else if (paymentMethods == 6) {
            $("#giftCardGroup", $payMethod).show();
            $("#paidAmount", $payMethod).val('').attr('disabled', true);
            $("#giftCardID", $payMethod).val('');
        } else if (paymentMethods == 7) {
            $("#lcGroup", $payMethod).show();
        } else if (paymentMethods == 8) {
            $("#ttGroup", $payMethod).show();
        }
    });


    $(document).on('change', '.accountID', function() {
        var glAcc = $("option:selected", $(this)).attr('data-glaccount');
        var bankdiv = $(this).parents('#bankTransferGroup');

        if (glAcc) {
            $('.main_div').addClass('Ajaxloading');
            eb.ajax({
                type: 'post',
                url: BASE_URL + '/accounts-api/getFinanceAccountsByAccountsID',
                data: {
                    financeAccountsID: glAcc
                },
                dataType: "json",
                async: false,
                success: function(data) {
                    if (data['status'] == true) {
                        var fName= data.data.financeAccounts.financeAccountsCode+'_'+data.data.financeAccounts.financeAccountsName

                        bankdiv.find('#bankTransferGlAccountID').append("<option value='"+glAcc+"'>"+fName+"</option>")
                        bankdiv.find('#bankTransferGlAccountID').val(glAcc).selectpicker('refresh');
                        
                    }
                    $('.main_div').removeClass('Ajaxloading');
                },
            });
        } else {
            bankdiv.find('.accountID').attr('disabled', true);
        }
    });

    $(document).on('change', '.bankID', function() {
        var bankdiv = $(this).parents('#bankTransferGroup');
        var bankID = $(this).val();
        var bank = $(this);
        var bankName = $("option:selected", $(this)).text();
        bankdiv.find('.accountID').find('option').each(function() {
            if ($(this).val()) {
                $(this).remove();
            }
        });
        if (bankID) {
            $('.main_div').addClass('Ajaxloading');
            eb.ajax({
                type: 'post',
                url: BASE_URL + '/account-api/bankAccountListForDropdown',
                data: {
                    bankId: bankID,
                    withGLAccount: true
                },
                dataType: "json",
                async: false,
                success: function(data) {
                    if (data['status'] == true) {
                        if ($.isEmptyObject(data['data'].list)) {
                            p_notification(false, eb.getMessage('ERR_DOESNT_HAVE_ANY_ACC_FOR_THIS_BANK', bankName));
                            $(bank).val('');
                            bankdiv.find('.accountID').attr('disabled', true);
                        } else {
                            $.each(data['data'].list, function(key, value) {
                                var option = "<option data-glaccount="+value.glAccountID+" value=" + value.value + ">" + value.text + "</option>";
                                bankdiv.find('.accountID').attr('disabled', false);
                                bankdiv.find('.accountID').append(option);
                            });
                        }
                    }
                    $('.main_div').removeClass('Ajaxloading');
                },
            });
        } else {
            bankdiv.find('.accountID').attr('disabled', true);
        }
    });

    $(document).on('change', '.giftCardID', function() {
        var giftCardID = $(this).val();
        var expireDate = GIFTCARDLIST[giftCardID].giftCardExpireDate;
        var value = GIFTCARDLIST[giftCardID].giftCardValue;
        var flag = 0;
        $('.giftCardID').each(function() {
            if (giftCardID == $(this).val()) {
                flag++;
            }
        });
        if (flag > 1) {
            p_notification(false, eb.getMessage('ERR_PAY_METHOD_GIFR_CARD_ALREDY_SELECT'));
            $(this).val('');
            value = 0.00;
        } else if (expireDate) {
            var exdate = new Date(expireDate);
            var today = new Date();
            if (exdate < today) {
                p_notification(false, eb.getMessage('ERR_PAY_METHOD_GIFR_CARD_EXPIRED'));
                $(this).val('');
                value = 0.00;
            }
        }
        $(this).parents('.payMethod').find(".paidAmount").val(value).trigger('change');
    });

    $('#paymentMethodAdd').on('click', function() {
        paymentMethodIncrement++;
        var NewPaymentMethodDiv = $('#payMethod_1').clone();
        NewPaymentMethodDiv.attr('id', 'payMethod_' + paymentMethodIncrement);
        var newID = 'payMethod_' + paymentMethodIncrement;

        $('.pamentMethodDelete', NewPaymentMethodDiv).removeClass('hidden');
        $('#creditGroup', NewPaymentMethodDiv).hide();
        $('#checkGroup', NewPaymentMethodDiv).hide();
        $('#bankTransferGroup', NewPaymentMethodDiv).hide();
        $('#giftCardGroup', NewPaymentMethodDiv).hide();
        $('#lcGroup', NewPaymentMethodDiv).hide();
        $('#ttGroup', NewPaymentMethodDiv).hide();
        $('#paidAmount', NewPaymentMethodDiv).val('').attr('disabled', false);
        $('#cashGroup', NewPaymentMethodDiv).show();

        var flag = 0;
        $(".paymentMethod", ".addPaymentMethod").each(function() {
            if ($(this).val() == 1)
                flag = 1;
        });
        if (flag == 1) {
            $('#cashGroup', NewPaymentMethodDiv).hide();
            $(".paymentMethod option[value='1']", NewPaymentMethodDiv).attr('disabled', 'disabled');
            $(".paymentMethod", NewPaymentMethodDiv).val(2);
            $('#checkGroup', NewPaymentMethodDiv).show();
        } else {
            $(".paymentMethod option[value='1']", '.addPaymentMethod').attr('disabled', 'disabled');
        }
        $('.addPaymentMethod').append(NewPaymentMethodDiv);
        //set selectpickers to the clonedPayment Methods
        $('#'+newID).find('#cashGlAccountID').removeData('AjaxBootstrapSelect');
        $('#'+newID).find('#cashGlAccountID').removeData('selectpicker');
        $('#'+newID).find('#cashGlAccountID').siblings('.bootstrap-select').remove();
        $('#'+newID).find('#cashGlAccountID').siblings('.bootstrap-select').find('.dropdown-menu').remove();
        loadDropDownFromDatabase('/accounts-api/search-active-accounts-for-dropdown', "", "", $('#'+newID).find('#cashGlAccountID'));

        $('#'+newID).find('#chequeGlAccountID').removeData('AjaxBootstrapSelect');
        $('#'+newID).find('#chequeGlAccountID').removeData('selectpicker');
        $('#'+newID).find('#chequeGlAccountID').siblings('.bootstrap-select').remove();
        $('#'+newID).find('#chequeGlAccountID').siblings('.bootstrap-select').find('.dropdown-menu').remove();
        loadDropDownFromDatabase('/accounts-api/search-active-accounts-for-dropdown', "", "", $('#'+newID).find('#chequeGlAccountID'));

        $('#'+newID).find('#creditGlAccountID').removeData('AjaxBootstrapSelect');
        $('#'+newID).find('#creditGlAccountID').removeData('selectpicker');
        $('#'+newID).find('#creditGlAccountID').siblings('.bootstrap-select').remove();
        $('#'+newID).find('#creditGlAccountID').siblings('.bootstrap-select').find('.dropdown-menu').remove();
        loadDropDownFromDatabase('/accounts-api/search-active-accounts-for-dropdown', "", "", $('#'+newID).find('#creditGlAccountID'));

        $('#'+newID).find('#bankTransferGlAccountID').removeData('AjaxBootstrapSelect');
        $('#'+newID).find('#bankTransferGlAccountID').removeData('selectpicker');
        $('#'+newID).find('#bankTransferGlAccountID').siblings('.bootstrap-select').remove();
        $('#'+newID).find('#bankTransferGlAccountID').siblings('.bootstrap-select').find('.dropdown-menu').remove();
        loadDropDownFromDatabase('/accounts-api/search-active-accounts-for-dropdown', "", "", $('#'+newID).find('#bankTransferGlAccountID'));

        $('#'+newID).find('#lcGlAccountID').removeData('AjaxBootstrapSelect');
        $('#'+newID).find('#lcGlAccountID').removeData('selectpicker');
        $('#'+newID).find('#lcGlAccountID').siblings('.bootstrap-select').remove();
        $('#'+newID).find('#lcGlAccountID').siblings('.bootstrap-select').find('.dropdown-menu').remove();
        loadDropDownFromDatabase('/accounts-api/search-active-accounts-for-dropdown', "", "", $('#'+newID).find('#lcGlAccountID'));

        $('#'+newID).find('#ttGlAccountID').removeData('AjaxBootstrapSelect');
        $('#'+newID).find('#ttGlAccountID').removeData('selectpicker');
        $('#'+newID).find('#ttGlAccountID').siblings('.bootstrap-select').remove();
        $('#'+newID).find('#ttGlAccountID').siblings('.bootstrap-select').find('.dropdown-menu').remove();
        loadDropDownFromDatabase('/accounts-api/search-active-accounts-for-dropdown', "", "", $('#'+newID).find('#ttGlAccountID'));


        //for postdated cheque
        $("#postdatedChequeGroup", NewPaymentMethodDiv).hide();
        $(".postdated-cheque-date", NewPaymentMethodDiv).hide();
        NewPaymentMethodDiv.find('.postdated-cheque').attr('checked', false);
        postdatedDate = $('.postdated-cheque-date').datepicker({
            format: 'yyyy-mm-dd',
            onRender: function(date) {
                return date.valueOf() < now.valueOf() ? ((U_DATE_R_OVERRIDE)?'':'disabled') : '';
            }
        }).on('changeDate', function(e) {
            postdatedDate.hide();
        }).data('datepicker');

        //addjust alignment
        $('.payMethod')
                .not('#payMethod_1')
                .addClass('panel panel-default');

        $('.payMethod .form-horizontal:first-child')
                .not('#payMethod_1 .form-horizontal:first-child')
                .addClass('panel-body');

    });

    $(document).on('click', '.pamentMethodDelete', function() {
        $(this).parents('.payMethod').remove();
        setCreditAmountAndRestToPaid();
    });
    
    $(document).on('change', '.paidAmount', function() {
        var paidAmount = $(this).val();
        if (isNaN(paidAmount) || paidAmount < 0) {
            p_notification(false, eb.getMessage('ERR_PAY_PAID_AMOUNT_SHOUDBE_NUMBER'));
            $(this).val(0.00);
        } else {
            var creditAmount = $('#creditAmount').val();
            var restToPaidTotal = paymentTotal - creditAmount;
            var totalPaidAmount = parseFloat(getTotalPaidAmount());
            if (restToPaidTotal < totalPaidAmount) {
                restBalance = totalPaidAmount - restToPaidTotal;
            }
        }
        setCreditAmountAndRestToPaid();

    });



    var productID = $("table.deliveryNoteProductTable > tbody#add-new-item-row > tr.add-row").attr('id');
    loadDropDownFromDatabase('/accounts-api/search-active-accounts-for-dropdown', "", "", '#glAccount');

    var getAddRow = function(productID) {
        if (productID != undefined) {

            var $row = $('table.deliveryNoteProductTable > tbody > tr', $productTable).filter(function() {
                return $(this).data("id") == productID;
            });
            return $row;
        }

        return $('tr.add-row:not(.sample)', $productTable);
    };
    if (!getAddRow().length) {
        $($addRowSample.clone()).removeClass('sample hidden').appendTo($('#add-new-item-row'));
    }
    locationOut = $('#idOfLocation').val();
    clearProductScreen();

    loadDropDownFromDatabase('/customerAPI/search-customers-for-dropdown', "", 0, '#customer');
    $('#customer').selectpicker('refresh');
    $('#customer').on('change', function() {
        if ($(this).val() > 0 && customerID != $(this).val()) {
            customerID = $(this).val();
            getCustomerDetails(customerID);
        } else {
            $('#customerCurrentBalance').val('');
            $('#customerCurrentCredit').val('');
            $('#paymentTerm').val('');
        }
    });

    function getCustomerDetails(custID) {
        eb.ajax({
            type: 'POST', url: BASE_URL + '/invoice-api/getCustomerDetails', data: {customerID: custID}, success: function(respond) {
                if (respond.status == true) {
                    setCustomerDetails(respond.data);
                } else {
                    $('#customerCurrentBalance').val('');
                    $('#customerCurrentCredit').val('');
                    $('#paymentTerm').val('');
                    p_notification(false, respond.msg);
                }
            }
        });
    }

    var setCustomerDetails = function($customer) {
        if ($customer.customerID) {
            $('#deliveryAddress').val($customer.customerAddress);
            $('#customerCurrentBalance').val(parseFloat($customer.customerCurrentBalance).toFixed(2));
            customercredit = $customer.customerCurrentCredit;
            $('#customerCurrentCredit').val(parseFloat($customer.customerCurrentCredit).toFixed(2));
            $('#customer').empty();
            if($('#customer').val()!= $customer.customerID){
                $('#customer').
                        append($("<option></option>").
                                attr("value", $customer.customerID).
                                text($customer.customerName + '-' + $customer.customerCode));
                $('#customer').selectpicker('refresh');
            }

            if ($customer.customerPaymentTerm == 'null') {
                $('#paymentTerm').val(1);
            } else {
                $('#paymentTerm').val($customer.customerPaymentTerm);
            }

            if ($customer.customerPriceList == null) {
                $('#priceListId').val('');
            } else {
                $('#priceListId').val($customer.customerPriceList);
            }
            $('#paymentTerm').trigger('change');
            $('#priceListId').trigger('change');
        } else {
            $('#customerCurrentBalance').val('');
            $('#customerCurrentCredit').val('');
            $('#paymentTerm').val('');
        }

    };

    // changeAuthorType();
    $('#authorType').on('change', function(){
        changeAuthorType();
    });


    function changeAuthorType() {
        selectedAuthourType = $('#authorType').val();
        if (selectedAuthourType == 1) { // if supplier
            $('#customer_div').removeClass('hidden');
            $('#gl_account_div').addClass('hidden');
        } else { // gl account
            $('#gl_account_div').removeClass('hidden');
            $('#customer_div').addClass('hidden');
        }
    }

    $('#item-add-show').on('change', function() {
        if (this.checked) {
            inventoryItemFlag = true;
            $('.deliveryNoteProductTable').removeClass('hidden');
        }
        else {
            inventoryItemFlag = false;
            var incrValue = 0;
            $('#form_row tr').each(function() {
                incrValue++;
            });
            if (incrValue == 1) {
                $('.deliveryNoteProductTable').addClass('hidden');
            }
        }
    });

    $('#cusProfileDN').on('change', function(){
        var customerFID;
        if(customerFID != $(this).val())
            customerFID = $(this).val();
            eb.ajax({
                type: 'POST',
                url: BASE_URL + '/customerAPI/getCustomerProfileDataByCustomerProfileID',
                data: {customerProfileID: customerFID},
                    success: function(respond) {
                        if (respond.status == true) {
                            $('#deliveryNoteAddress').val(respond.data.addressLine);
                        } else {
                            $('#deliveryNoteAddress').val("");
                        }
                    }
            });
    });

    $('#dimensionView').on('click', function(e) {
        clearDimensionModal();
        var deliveryNoteNo = $('#deliveryNoteNo').val();

        if (!$.isEmptyObject(dimensionData)) {
            $.each(dimensionData[deliveryNoteNo], function(index, val) {
                dimensionArray[index] = val;                
            });
            $.each(dimensionArray, function(index, value) {
                var newTrID = 'tr_' + value.dimensionTypeId;
                var clonedRow = $($('#dimensionPreSample').clone()).attr('id', newTrID).addClass('addedDimensions');
                $("input[name='dimensionType']", clonedRow).val(value.dimensionTypeTxt);
                $("input[name='dimensionValue']", clonedRow).val(value.dimensionValueTxt);
                clonedRow.removeClass('hidden');
                clonedRow.insertBefore('#dimensionPreSample');
                clearAddedNewDimensionRow();
            });
        }
        $('#addDimensionModal').modal('show');
    });

    // dimension modal
    function clearDimensionModal()
    {
        $("#addDimensionModal #dimensionTable tbody .addedDimensions").remove();
        dimensionArray = {};
    }


    $('.dimenisonchange').on('change', function(e) {
        dimensionTypeID = this.value;
        getDimensionValueListByDimensionType(dimensionTypeID, function() {
        });
    });

    loadDropDownFromDatabase('/api/expense-type/get-expense-type-by-search', "", 'expenseType', '.non-item-ex-type-selectbox');

    function getDimensionValueListByDimensionType(dimensionTypeID, callback) {
        switch (dimensionTypeID) {
            case "job":
                $('.dimensionVal').addClass('hidden');
                $('.dimensionProjectNo').addClass('hidden');
                $('.dimensionJobNo').removeClass('hidden');
                break;
            case "project":
                $('.dimensionVal').addClass('hidden');
                $('.dimensionProjectNo').removeClass('hidden');
                $('.dimensionJobNo').addClass('hidden');
                break;
           default:
                $('.dimensionVal').removeClass('hidden');
                $('.dimensionProjectNo').addClass('hidden');
                $('.dimensionJobNo').addClass('hidden');
                $.ajax({
                    type: 'POST',
                    url: BASE_URL + '/dimension-api/getRelatedDimensionValues',
                    data: {dimensionId: dimensionTypeID},
                    success: function(respond) {
                        //empty options
                        $('#dimensionVal').empty();
                        $('#dimensionVal').append($("<option>", {value: '', html: 'Select dimension value'}));
                        $(respond.data).each(function(index, dimValue) {
                            $.each(dimValue, function(ind, val) {
                                $('#dimensionVal').append($("<option>", {value: ind, html: val.value}));
                            });
                        });
                        callback();
                    }
                });
                break;
        }
    }

     $('#non-item-table').on('click', '.non-item-desc',function() {
        var $element = $(this).closest('tr');
        var taxes = TAXES;
        var taxStatus = TAXSTATUS;

        if (taxStatus) {
            $element.find('.nonItemTaxApplied').removeClass('glyphicon glyphicon-check');
            $element.find('.nonItemTaxApplied').addClass('glyphicon glyphicon-unchecked');
            $element.find('.tempLi').remove();

            for (var i in taxes) {
                if (taxes[i].tS == 1) {
                    var clonedLi = $($('.samplennItemLi:first').clone()).removeClass('hidden samplennItemLi').attr('id', 'linon_' + i).addClass("tempLi");
                    clonedLi.children(".taxChecks").attr('id', taxes[i].tID + '_' + i).prop('checked', false).addClass('addNewTaxCheck');
                    clonedLi.children(".taxName").html('&nbsp&nbsp' + taxes[i].tN + '&nbsp&nbsp' + taxes[i].tP + '%').attr('for', taxes[i].tID + '_' + i);
                    clonedLi.insertBefore('.samplennItemLi');
                }
            }
        }
    });


    $('#non-item-tbody').on('click', '.add-non-item', function() {
        var descript = $(this).parents('tr').find('input[name=non-item-description]').val();
        var nonItemQty = $(this).parents('tr').find('input[name=non-item-qty]').val();
        var nonItemUnitPrice = $(this).parents('tr').find('input[name=non-item-unit-prc]').val();
        var nonItemTotalPrice = $(this).parents('tr').find('#addNewNonItemTotal').text();
        var nonItemExType = $(this).parents('tr').find('.non-item-ex-type-selectbox');
        var nonItemExTypeId = nonItemExType.val();
        var nonItemExTypeText = nonItemExType.find('optgroup option:selected').text();
        var data = {
            descript: descript,
            nonItemQty: nonItemQty,
            nonItemUnitPrice: nonItemUnitPrice,
            nonItemTotalPrice: nonItemTotalPrice,
            nonItemExType: nonItemExTypeId
        };
        if (checkNonItemValidation(data))
        {
            if(!($(this).parent('td').hasClass('editedRow'))){
                cloneNonItem(nonItemRowId);
                nonItemRowId++;
            }

            $(this).parents('tr').find('.pv-non-item-edit').removeClass('hidden');
            $(this).parents('tr').find('input[name=non-item-description]').attr('disabled', true);
            $(this).parents('tr').find('input[name=non-item-qty]').attr('disabled', true);
            $(this).parents('tr').find('input[name=non-item-unit-prc]').attr('disabled', true);
            $(this).parents('tr').find('.non-item-tax').attr('disabled', true);
            $(this).parents('tr').find('.add-non-item').parent('td').addClass('hidden');
            $(this).parents('tr').find('#removeNonItem').removeClass('hidden');
            $(this).parents('tr').addClass('non-item');
            var $selectPicker = $(this).parents('tr').find('.non-item-ex-type-selectbox');
            $selectPicker.prop('disabled', true).selectpicker('refresh');
            $selectPicker.removeClass('ex-type');

        }
        setGrnTotalCost();
    });

    function checkNonItemValidation(data)
    {
        if (data.descript == '' || data.descript == null) {
            p_notification(false, eb.getMessage('NON_ITEM_NOT_SELECT'));
            return false;
        } else if (data.nonItemUnitPrice == '' || data.nonItemUnitPrice == null) {
            p_notification(false, eb.getMessage('NON_ITEM_UNIT_PRICE_NOT_SET'));
            return false;
        } else if(isNaN(data.nonItemUnitPrice)) {
                p_notification(false, eb.getMessage('ERR_NUMBER_FORMET_PV'));
                return false;
        } else if(data.nonItemUnitPrice < 0) {
                p_notification(false, eb.getMessage('ERR_MINUS_UNIT_PRICE_PV'));
                return false;
        } else if (data.nonItemExType == '' || data.nonItemExType == null || data.nonItemExType == false) {
            p_notification(false, eb.getMessage('ERR_GL_AC_NO_SELECT'));
            return false;
        } else {
            return true;
        }
    }

    function cloneNonItem(nonItemRowId) {
        var row = document.getElementById('non-item-row-sample');
        var tableBody = document.getElementById('non-item-tbody');
        var clone = row.cloneNode(true);
        clone.id = 'non-item-row-' + nonItemRowId;
        var createdID = 'non-item-row-' + nonItemRowId;
        tableBody.appendChild(clone);
        $("#" + createdID).removeClass('hidden');
        clearNonItemProductRow('non-item-row-' + nonItemRowId);
    }
    function clearNonItemProductRow(id) {
        $('#' + id).find('input[name=non-item-description]').val('').attr('disabled', false);
        $('#' + id).find('input[name=non-item-qty]').val('').attr('disabled', false);
        $('#' + id).find('input[name=non-item-unit-prc]').val('').attr('disabled', false);
        $('#' + id).find('.non-item-total-price').html(accounting.formatMoney('0.00'));
        $('#' + id).find('.non-item-tax').attr('disabled', false);
        $('#' + id).find('.add-non-item').parent('td').removeClass('hidden');
        $('#' + id).find('#removeNonItem').addClass('hidden');

        var $exTypePicker = $('#' + id).find('select.non-item-ex-type-selectbox');
        $exTypePicker.siblings('.bootstrap-select').remove();
        $exTypePicker.removeData();
        loadDropDownFromDatabase('/api/expense-type/get-expense-type-by-search', "", 'expenseType', '#' + id + ' .non-item-ex-type-selectbox');

    }

    $('#non-item-tbody').on('click', '.remove-non-item', function() {
        deleteNonItem($(this).parents('tr').attr('id'));
    });
    function deleteNonItem(rowID) {
        $('#' + rowID).remove();
        setGrnTotalCost();
    }

    $('#non-item-tbody').on('focusout', '#addNewNonItemTax,#non-item-unit-prc', function() {
        var nonItemTax = Array();
        $(this).parents('tr').find('input:checkbox.addNewTaxCheck').each(function() {
            if (this.checked) {
                var cliTID = this.id;
                var taxID = cliTID.split('_')[1];
                nonItemTax.push(taxID);
            }
        });
        var unitPrice = $(this).parents('tr').find('input[name=non-item-unit-prc]').val();
//        var qty = $(this).parents('tr').find('input[name=non-item-qty]').val();
        var qty = 1;
        var tmpNonItemCost = toFloat(unitPrice) * toFloat(qty);
        currentItemTaxResults = calculateItemCustomTax(tmpNonItemCost, nonItemTax);
        currentTaxAmount = (currentItemTaxResults == null) ? null : currentItemTaxResults.tTA;
        var itemCost = toFloat(tmpNonItemCost) + toFloat(currentTaxAmount);
        $(this).parents('tr').find('.non-item-total-price').html(accounting.formatMoney(itemCost));
    });
    $('#non-item-tbody').on('click', '#addNonItem', function() {
        var nonItemTax = Array();
        $(this).parents('tr').find('input:checkbox.addNewTaxCheck').each(function() {
            if (this.checked) {
                var cliTID = this.id;
                var taxID = cliTID.split('_')[1];
                nonItemTax.push(taxID);
            }
        });
        var unitPrice = $(this).parents('tr').find('input[name=non-item-unit-prc]').val();

//        var qty = $(this).parents('tr').find('input[name=non-item-qty]').val();
        var qty = 1;
        var tmpNonItemCost = toFloat(unitPrice) * toFloat(qty);
        currentItemTaxResults = calculateItemCustomTax(tmpNonItemCost, nonItemTax);
        currentTaxAmount = (currentItemTaxResults == null) ? null : currentItemTaxResults.tTA;
        var itemCost = toFloat(tmpNonItemCost) + toFloat(currentTaxAmount);
        $(this).parents('tr').find('.non-item-total-price').html(accounting.formatMoney(itemCost));
    });

    $('#non-item-tbody').on('change', '.taxChecks.addNewTaxCheck', function() {
        var $element = $(this).closest('ul');
        var $checkBox = $(this).closest('td').find('.nonItemTaxApplied');

        var allchecked = false;
        $($element).find('.taxChecks.addNewTaxCheck').each(function() {
            if (this.checked) {
                allchecked = true;
            }
        });
        if (allchecked == false) {
            $checkBox.removeClass('glyphicon glyphicon-check');
            $checkBox.addClass('glyphicon glyphicon-unchecked');
        } else {
            $checkBox.removeClass('glyphicon glyphicon-unchecked');
            $checkBox.addClass('glyphicon glyphicon-check');
        }

        $("#non-item-tbody #non-item-unit-prc").trigger('focusout');
    });


    $('#add_dimension').on('click', function(e) {
        e.preventDefault();
        //if plus button is clicked then disabled the button
        //if button is clicked more than one, then it does not continue
        if ($(this).is(':disabled')) {
            return false;
        } else {
            $(this).attr('disabled', true);
            var $thisRow = $(this).parents('tr');
            if ($('.dimenisonchange').val() == 0 || $('.dimenisonchange').val() == '' || $('.dimenisonchange').val() == null) {
                p_notification(false, eb.getMessage('ERR_DIMENSION_SELCET'));
                $(this).attr('disabled', false);
            } else {

                if ($('.dimenisonchange').val() != 'job' && $('.dimenisonchange').val() != 'project') {
                    if ($('.dimensionVal').val() == 0 || $('.dimensionVal').val() == '' || $('.dimensionVal').val() == null) {
                        p_notification(false, eb.getMessage('ERR_DIMENSION_VALUE_SELCET'));
                        $(this).attr('disabled', false);
                        return;
                    }
                }

                if ($('.dimenisonchange').val() == 'project') {
                    if ($('.dimensionProjectNo').val() == 0 || $('.dimensionProjectNo').val() == '' || $('.dimensionProjectNo').val() == null) {
                        p_notification(false, eb.getMessage('ERR_DIMENSION_VALUE_SELCET'));
                        $(this).attr('disabled', false);
                        return;
                    }
                }

                if ($('.dimenisonchange').val() == 'job') {
                    if ($('.dimensionJobNo').val() == 0 || $('.dimensionJobNo').val() == '' || $('.dimensionJobNo').val() == null) {
                        p_notification(false, eb.getMessage('ERR_DIMENSION_VALUE_SELCET'));
                        $(this).attr('disabled', false);
                        return;
                    }
                }

                if (dimensionArray[dimensionTypeID]) {
                    p_notification(false, eb.getMessage('ERR_DIMENSION_ALREADY_INSERTED'));
                    $('.dimenisonchange').val('');
                    $('.dimenisonchange').selectpicker('render');
                    $('.dimenisonchange').trigger('change');
                } else {
                    addNewDimensionRow($(this).parents('tr'));
                }
                $(this).attr('disabled', false);
            }
        }
    });

    function addNewDimensionRow(thisRow) {

        var dimensionTypeId = $('.dimenisonchange', thisRow).val();
        var dimensionTypeTxt = $('.dimenisonchange', thisRow).find("option:selected").text();

        switch (dimensionTypeID) {
            case "job":
                var dimensionValueTxt = $('.dimensionJobNo', thisRow).find("option:selected").text();
                var dimensionValueId = $('.dimensionJobNo', thisRow).val();
                break;
            case "project":
                var dimensionValueTxt = $('.dimensionProjectNo', thisRow).find("option:selected").text();
                var dimensionValueId = $('.dimensionProjectNo', thisRow).val();
                break;
           default:
                var dimensionValueTxt = $('.dimensionVal', thisRow).find("option:selected").text();
                var dimensionValueId = $('.dimensionVal', thisRow).val();
                break;
        }
       
        var dimensionDuplicateCheck = false;
        if (!$.isEmptyObject(dimensionArray)) {
            $.each(dimensionArray, function(index, value) {
                if (value.dimensionTypeId == dimensionTypeId) {
                    dimensionDuplicateCheck = true;
                }
            });   
        }
       
        if (dimensionDuplicateCheck) {
            p_notification(false, eb.getMessage("ERR_DIMENSION_ALREADY_INSERTED"));
            return false;
        }

        var newTrID = 'tr_' + dimensionTypeId;
        var clonedRow = $($('#dimensionPreSample').clone()).attr('id', newTrID).addClass('addedDimensions');
        $("input[name='dimensionType']", clonedRow).val(dimensionTypeTxt);
        $("input[name='dimensionValue']", clonedRow).val(dimensionValueTxt);
        clonedRow.removeClass('hidden');
        clonedRow.insertBefore('#dimensionPreSample');
        dimensionArray[dimensionTypeId] = new dimension(dimensionTypeId, dimensionValueId,dimensionTypeTxt , dimensionValueTxt);
        clearAddedNewDimensionRow();
    }


    function dimension(dimensionTypeId, dimensionValueId, dimensionTypeTxt, dimensionValueTxt) {
        this.dimensionTypeId = dimensionTypeId;
        this.dimensionValueId = dimensionValueId;
        this.dimensionTypeTxt = dimensionTypeTxt;
        this.dimensionValueTxt = dimensionValueTxt;
    }

    function clearAddedNewDimensionRow() {
        $('.dimenisonchange').val('');
        $('.dimenisonchange').selectpicker('render');
        $('.dimenisonchange').trigger('change');
    }

    $('.addDimensions').on('click', function(e) {
        e.preventDefault();

        // validate products before closing modal
        if (!dimensionModalValidate(e)) {
            e.stopPropagation();
            return false;
        } else {
            $('#addDimensionModal').modal('hide');
        }
    });

    function dimensionModalValidate(e) {
        var deliveryNoteNo = $('#deliveryNoteNo').val();
        if ($.isEmptyObject(dimensionArray)) {
            dimensionData = {};
            return true;   
        } else {
            var dimensionSingleData = {};
            $.each(dimensionArray, function(index, val) {
                dimensionSingleData[index] = val;
            });
            dimensionData[deliveryNoteNo] = dimensionSingleData;
            return true;
        }
    }

    $("#addDimensionModal #dimensionTable").on('click', '.deleteDimensionType', function(e) {
        e.preventDefault();
        var deleteDTrID = $(this).closest('tr').attr('id');
        var deleteDID = deleteDTrID.split('tr_')[1].trim();
        bootbox.confirm('Are you sure you want to remove this dimension ?', function(result) {
            if (result == true) {
                delete dimensionArray[deleteDID];
                $('#' + deleteDTrID).remove();
            }
        });
    });

    loadDropDownFromDatabase('/job-api/searchJobsForDropdown', "", 0, '.dimensionJobNo');
    $('.dimensionJobNo').selectpicker('refresh');
    $('.dimensionJobNo').on('change', function() {
        var dimensionValue = $(this).val();
    });

    loadDropDownFromDatabase('/api/project/searchProjectsForDropdown', "", 0, '.dimensionProjectNo');
    $('.dimensionProjectNo').selectpicker('refresh');
    $('.dimensionProjectNo').on('change', function() {
        var dimensionValue = $(this).val();
    });

    setProductTypeahead();
    function setProductTypeahead() {

        var $currentRow = getAddRow();
        $('#itemCode', $currentRow).selectpicker();
        locationID = $('#idOfLocation').val();
        var documentType = 'deliveryNote';
        loadDropDownFromDatabase('/productAPI/search-location-products-for-dropdown', locationID, 0, '#itemCode', $currentRow, '', documentType);

        $('#itemCode', $currentRow).selectpicker('refresh');
        $('#itemCode', $currentRow).on('change', function() {
            if ($(this).val() > 0 && selectedPID != $(this).val()) {
                selectedPID = $(this).val();
                eb.ajax({
                    type: 'POST',
                    url: BASE_URL + '/productAPI/get-location-product-details',
                    data: {productID: selectedPID, locationID: locationID},
                    success: function(respond) {
                        selectProductForTransfer(respond.data);
                        var currentElem = new Array();
                        currentElem[selectedPID] = respond.data;
                        locationProducts = $.extend(locationProducts, currentElem);
                    }
                });
            }
            $('#availableQuantity', $currentRow).parent().addClass('input-group');
            $('#deliverQuanity', $currentRow).parent().addClass('input-group');
        });
    }

    function selectProductForTransfer(selectedlocationProduct) {
// check if product is already selected
        if (deliverProducts[selectedlocationProduct.pID] != undefined) {
            p_notification(false, eb.getMessage('ERR_DELI_ALREADY_SELECT_PROD'));
        }
        currentProducts[selectedlocationProduct.pID] = selectedlocationProduct;
        var productID = selectedlocationProduct.pID;
        var productCode = selectedlocationProduct.pC;
        var productType = selectedlocationProduct.pT;
        var productName = selectedlocationProduct.pN;
        var availableQuantity = (selectedlocationProduct.LPQ == null) ? 0 : selectedlocationProduct.LPQ;
        var defaultSellingPrice = parseFloat(selectedlocationProduct.dSP);
        if (selectedlocationProduct.dPR != null) {
            var productDiscountPrecentage = parseFloat(selectedlocationProduct.dPR).toFixed(2);
        } else {
            var productDiscountPrecentage = 0;
        }
        
        if (selectedlocationProduct.dV != null) {
            var productDiscountValue = parseFloat(selectedlocationProduct.dV).toFixed(2);
        } else {
            var productDiscountValue = 0;
    
        }

        if (priceListItems[productID] != undefined && parseFloat(priceListItems[productID].itemPrice) > 0) {
            defaultSellingPrice = priceListItems[productID].itemPrice;
            var itemDiscountType = priceListItems[productID].itemDiscountType;
            productDiscountValue = (itemDiscountType == 1) ? priceListItems[productID].itemDiscount : 0;
            productDiscountPrecentage = (itemDiscountType == 2) ? priceListItems[productID].itemDiscount : 0;
        }
        defaultSellingPriceData[productID] = defaultSellingPrice;

        var $currentRow = getAddRow();
        clearProductRow($currentRow);
        $("input[name='discount']", $currentRow).prop('readonly', false);
        $("#itemCode", $currentRow).data('PT', productType);
        $("#itemCode", $currentRow).data('PN', productName);
        $("#itemCode", $currentRow).data('PC', productCode);
        $("input[name='availableQuantity']", $currentRow).val(availableQuantity).prop('readonly', true);
        $("input[name='unitPrice']", $currentRow).val(defaultSellingPrice).addUomPrice(selectedlocationProduct.uom);
        if (selectedlocationProduct.dEL == 1 || (priceListItems[productID] != undefined && parseFloat(priceListItems[productID].itemPrice) > 0)) {
            if (selectedlocationProduct.dV != null) {
                productDiscountValue = parseFloat(productDiscountValue).toFixed(2);
                $("input[name='discount']", $currentRow).val(productDiscountValue).addUomPrice(selectedlocationProduct.uom);
                $("input[name='discount']", $currentRow).addClass('value');
                if ($("input[name='discount']", $currentRow).hasClass('precentage')) {
                    $("input[name='discount']", $currentRow).removeClass('precentage');
                }
                $(".sign", $currentRow).text(companyCurrencySymbol);
            } else if (selectedlocationProduct.dPR != null) {
                productDiscountPrecentage = parseFloat(productDiscountPrecentage).toFixed(2);
                $("input[name='discount']", $currentRow).val(productDiscountPrecentage);
                $("input[name='discount']", $currentRow).addClass('precentage');
                if ($("input[name='discount']", $currentRow).hasClass('value')) {
                    $("input[name='discount']", $currentRow).removeClass('value');
                }
                $(".sign", $currentRow).text('%');
            }
        } else {
            $("input[name='discount']", $currentRow).prop('readonly', true);
        }
        $("input[name='deliverQuanity']", $currentRow).prop('readonly', false);
        $currentRow.data('id', productID);
        $currentRow.data('copied', false);
        // add uom list
        $("input[name='availableQuantity'],input[name='deliverQuanity']", $currentRow).addUom(selectedlocationProduct.uom);
        $('.uomqty').attr("id", "itemQuantity");
        $('.uomPrice').attr("id", "itemUnitPrice");
        var deleted = 0;
        // clear old rows
        $("#batch_data tr:not(.hidden)").remove();
        addBatchProduct(productID, productCode, deleted);
        addSerialProduct(productID, productCode, deleted);
        addSerialBatchProduct(productID, productCode, deleted);        

        //check this product serail or batch
        if (!$.isEmptyObject(selectedlocationProduct.serial)) {
            $('#product-batch-modal .serial-auto-select').removeClass('hidden');
            $('#numberOfRow').val('');
        } else {
            $('#product-batch-modal .serial-auto-select').addClass('hidden');
        }

         // check if any batch / serial products exist
        if ($.isEmptyObject(selectedlocationProduct.batch) &&
                $.isEmptyObject(selectedlocationProduct.serial) &&
                $.isEmptyObject(selectedlocationProduct.batchSerial)) {

            $currentRow.removeClass('subproducts');
            $('.edit-modal', $currentRow).parent().addClass('hidden');
            $("td[colspan]", $currentRow).attr('colspan', 2);
            $("input[name='deliverQuantity']", $currentRow).prop('readonly', false).focus();
        } else {
            $currentRow.addClass('subproducts');
            $('.edit-modal', $currentRow).parent().removeClass('hidden');
            $("td[colspan]", $currentRow).attr('colspan', 1);
            $("input[name='deliverQuantity']", $currentRow).prop('readonly', true);
            $('#addDeliveryNoteProductsModal').modal('show');
        }
        setTaxListForProduct(productID, $currentRow);
        $currentRow.attr('id', 'product' + productID);
    }

    function clearProductRow($currentRow) {
        $("input[name='availableQuantity']", $currentRow).val('').siblings('.uomqty,.uom-select').remove().show();
        $("input[name='deliverQuanity']", $currentRow).val('').siblings('.uomqty,.uom-select').remove().show();
        $("input[name='unitPrice']", $currentRow).val('').siblings('.uomPrice,.uom-price-select').remove().show();
        $("input[name='discount']", $currentRow).val('');
        $(".tempLi", $currentRow).remove('');
        $("#taxApplied", $currentRow).removeClass('glyphicon-check');
        $("#taxApplied", $currentRow).addClass('glyphicon-unchecked');
        $(".uomList", $currentRow).remove('');
    }

    $('#batch-save').on('click', function(e) {
        e.preventDefault();
        // validate batch / serial products before closing modal
        if (!batchModalValidate(e)) {
            e.stopPropagation();
            return false;
        } else {
            $('#addDeliveryNoteProductsModal').modal('hide');
        }

    });

    $('#select_serials').on('click',function(e) {
        var numberOfRow = $('#numberOfRow').val();
        if (numberOfRow != '' || numberOfRow != 0) {
            $('#batch_data > tr').each(function(key, value){
                if (numberOfRow != 0 && !($("input[name='deliverQuantityCheck']", value).is('[disabled=disabled]'))) {
                    $("input[name='deliverQuantityCheck']", value).prop('checked', true);
                    numberOfRow--;
                } else {
                    $("input[name='deliverQuantityCheck']", value).prop('checked', false);
                }
            });
        }    
        
    });

    $('.close').on('click', function() {
        $('#addDeliveryNoteProductsModal').modal('hide');
    });
    function getCurrentProductData($thisRow) {
        $("input.uomqty", $thisRow).change();
        var discountType = '';
        if ($("input[name='discount']", $thisRow).hasClass('value')) {
            discountType = 'value';
        } else if ($("input[name='discount']", $thisRow).hasClass('precentage')) {
            discountType = 'precentage';
        }
        var availableQuantity = $("input[name='availableQuantity']", $thisRow).val();
        if (!availableQuantity) {
            availableQuantity = 0;
        }
        var thisVals = {
            productID: $thisRow.data('id'),
            productCode: $("#itemCode", $thisRow).data('PC'),
            productName: $("#itemCode", $thisRow).data('PN'),
            productPrice: $("input[name='unitPrice']", $thisRow).val(),
            productDiscount: $("input[name='discount']", $thisRow).val(),
            productDiscountType: discountType,
            productTotal: productsTotal[$thisRow.data('id')],
            pTax: productsTax[$thisRow.data('id')],
            productType: $("#itemCode", $thisRow).data('PT'),
            copied: $thisRow.data('copied'),
            availableQuantity: {
                qty: availableQuantity,
            },
            deliverQuantity: {
                qty: $("input[name='deliverQuanity']", $thisRow).val(),
            }
        };

        return thisVals;
    }


    $productTable.on('click', '#add_item, button.save', function(e) {
        e.preventDefault();

        var $thisRow = $(this).parents('tr');
        var thisVals = getCurrentProductData($thisRow);

        $('.itemDescText', $thisRow).prop('disabled', true);
        $("input[name='availableQuantity']", $thisRow).prop('readonly', true);

        if (!$(this).hasClass('save')) {
            if (deliverProducts[thisVals.productID] != undefined) {
                p_notification(false, eb.getMessage('ERR_DELI_ALREADY_SELECT_PROD'));
                $("#itemCode", $thisRow).focus().select();
                $thisRow.remove();
                var $newRow = $($addRowSample.clone()).removeClass('sample hidden').appendTo($('#add-new-item-row', $productTable));
                setProductTypeahead();
                $newRow.find("#itemCode").focus();
                return false;
            }
        }

        if (($('#itemCode', $thisRow).val()) == undefined || ($('#itemCode', $thisRow).val() == '')) {
            p_notification(false, eb.getMessage('ERR_DELI_SELECT_PROD'));
            $("#itemCode", $thisRow).focus().select();
            return false;
        }

        if (thisVals.productType != 2 && (isNaN(parseFloat(thisVals.deliverQuantity.qty)) || parseFloat(thisVals.deliverQuantity.qty) <= 0)) {
            p_notification(false, eb.getMessage('ERR_DELI_ENTER_QUAN'));
            $("input[name='deliverQuantity']", $thisRow).focus().select();
            return false;
        }

        if (thisVals.productType != 2 && (parseFloat(thisVals.deliverQuantity.qty) > parseFloat(thisVals.availableQuantity.qty))) {
            p_notification(false, eb.getMessage('ERR_DELI_QUAN'));
            $("input[name='deliverQuantity']", $thisRow).focus().select();
            return false;

        }

        if (thisVals.productDiscount) {
            if (isNaN(parseFloat(thisVals.productDiscount)) || parseFloat(thisVals.productDiscount) < 0) {
                p_notification(false, eb.getMessage('ERR_DELI_VALID_DISCOUNT'));
                $("input[name='discount']", $thisRow).focus().select();
                return false;
            }
        }

        if (thisVals.productTotal < 0) {
            if (isNaN(parseFloat(thisVals.productTotal)) || parseFloat(thisVals.productTotal) <= 0) {
                p_notification(false, eb.getMessage('ERR_INVO_PROD_TOTAL'));
                $("input[name='unitPrice']", $thisRow).focus().select();
                return false;
            }
        }

        if ($thisRow.hasClass('subproducts')) {
            if (parseFloat(SubProductsQty[thisVals.productID]) != parseFloat(thisVals.deliverQuantity.qty)) {
                p_notification(false, eb.getMessage('ERR_DELI_SUB_PRODUCT_CHECK', thisVals.productCode));
                $('.edit-modal', $thisRow).focus();
                return false;
            }
        }

// if add button is clicked
        if ($(this).hasClass('add')) {
            var $currentRow = $thisRow;
            var $flag = false;
            if ($currentRow.hasClass('add-row')) {
                $flag = true;
            }
            $currentRow
                    .removeClass('add-row')
                    .find("#itemCode, input[name='deliverQuanity'],input[name='availableQuantity'],input[name='unitPrice'],input[name='discount']").prop('readonly', true).end()
                    .find('td').find('#addNewTax').attr('disabled', true)
                    .find("button.delete").removeClass('disabled');
            $currentRow.find("input[name='discount']").change();
            $currentRow.find("input[name='deliverQuanity']").change();
            $currentRow.find("input[name='unitPrice']").change();
            $currentRow.find("#itemCode").prop('disabled', true).selectpicker('render');
            if ($flag) {
                var $newRow = $($addRowSample.clone()).removeClass('sample hidden').appendTo($('#add-new-item-row', $productTable));
                setProductTypeahead();
                $newRow.find("#itemCode").focus();
            }
        } else if ($(this).hasClass('save')) {
            var productId = $(this).parent().parent().data('id');
            var $currentRow = getAddRow(productId);
            $currentRow
                    .removeClass('edit-row')
                    .find("#itemCode, input[name='deliverQuanity'],input[name='availableQuantity'],input[name='unitPrice'],input[name='discount']").prop('readonly', true).end()
                    .find('td').find('#addNewTax').attr('disabled', true)
                    .find("button.delete").removeClass('disabled');
            $currentRow.find("input[name='discount']").change();
            $currentRow.find("input[name='deliverQuanity']").change();
            $currentRow.find("input[name='unitPrice']").change();
        } else { // if save button is clicked
            $thisRow.removeClass('edit-row');
            $thisRow.find("input[name='deliverQuanity']").prop('readonly', true);
        }
        // if batch product modal is available for this product
        $(this, $currentRow).parent().find('.edit').removeClass('hidden');
        $(this, $currentRow).parent().find('.add').addClass('hidden');
        $(this, $currentRow).parent().find('.save').addClass('hidden');
        $(this, $currentRow).parent().parent().find('.delete').removeClass('disabled');
        if ($thisRow.hasClass('subproducts')) {
            $('.edit-modal', $currentRow).parent().addClass('hidden');
            $("td[colspan]", $currentRow).attr('colspan', 2);
        }
        deliverProducts[thisVals.productID] = thisVals;
        setGrnTotalCost();
        setTotalTax();
        $('#customCurrencyId').attr('disabled', true);
        $('#priceListId').attr('disabled', true);
    });

    $productTable.on('click', 'button.edit', function(e) {
        var $thisRow = $(this).parents('tr');
        var produtID = $thisRow.data('id');
        $thisRow.addClass('edit-row')
                .find("input[name='deliverQuanity'],input[name='unitPrice'],input[name='discount']").prop('readonly', false).end()
                .find('td').find('#addNewTax').attr('disabled', false)
                .find("button.delete").addClass('disabled');
        if (currentProducts[produtID].dEL != 1) {
            $thisRow.find("input[name='discount']").prop('readonly', true);
        }
        $thisRow.find("input[name='discount']").change();
        $thisRow.find("input[name='deliverQuanity']").change();
        $thisRow.find("input[name='unitPrice']").change();
        $(this, $thisRow).parent().find('.edit').addClass('hidden');
        $(this, $thisRow).parent().find('.save').removeClass('hidden');
        if ($thisRow.hasClass('subproducts')) {
            $("input[name='deliverQuanity']", $thisRow).prop('readonly', true);
            $('.edit-modal', $thisRow).parent().removeClass('hidden');
            $("td[colspan]", $thisRow).attr('colspan', 1);
        } else {
            $("input[name='deliverQuanity']", $thisRow).prop('readonly', false).focus();
        }
    });
    $productTable.on('click', 'button.delete', function(e) {
        e.preventDefault();
        if (!$(this).hasClass('disabled')) {
            var $thisRow = $(this).parents('tr');
            var productID = $thisRow.data('id');
            delete deliverProducts[productID];
            // when all products deleted customcurrency symbal selection will be enable
            // if used any selection from 'start invoice by' then cann't enable
            if (_.size(deliverProducts) == 0 && !$('#salesOrderNo').val()) {
                $('#customCurrencyId').attr('disabled', false);
            }
            setGrnTotalCost();
            setTotalTax();
            selectedPID = '';
            $thisRow.remove();
        }
    });

    function batchModalValidate(e) {
        var productID = $('#addDeliveryNoteProductsModal').data('id');
        productID = (productID == 'undefined')? undefined :productID;
        var $thisParentRow = getAddRow(productID);
        var thisVals = getCurrentProductData($thisParentRow);
        var $batchTable = $("#addDeliveryNoteProductsModal .batch-table tbody");
        var qtyTotal = 0;
        var subProducts = [];
        var deliveryUnitPrice;
        $("input[name='deliverQuantity'], input[name='deliverQuantityCheck']:checked", $batchTable).each(function() {

            var $thisSubRow = $(this).parents('tr');
            var thisDeliverQuantity = $(this).val();
            if ((thisDeliverQuantity).trim() != "" && isNaN(parseFloat(thisDeliverQuantity))) {
                p_notification(false, eb.getMessage('ERR_DELI_ENTER_QUAN'));
                $(this).focus();
                return qtyTotal = false;
            }

            thisDeliverQuantity = (isNaN(parseFloat(thisDeliverQuantity))) ? 0 : parseFloat(thisDeliverQuantity);
            var thisAvailableQuantity = $("input[name='availableQuantity']", $thisSubRow).val();
            if(thisDeliverQuantity != 0){
                deliveryUnitPrice = $("input[name='btPrice']", $thisSubRow).val();
            }
            thisAvailableQuantity = (isNaN(parseFloat(thisAvailableQuantity))) ? 0 : parseFloat(thisAvailableQuantity);
            if (thisDeliverQuantity > thisAvailableQuantity) {
                p_notification(false, eb.getMessage('ERR_DELI_QUAN'));
                $(this).focus();
                return qtyTotal = false;
            }

            var warranty = $thisSubRow.find('#warranty').val();
            if (isNaN(warranty)) {
                p_notification(false, eb.getMessage('ERR_INVO_WARRANTY_PERIOD_SHBE_NUM'));
                $thisSubRow.find('#warranty').focus().select();
                return qtyTotal = false;
            }

            qtyTotal = qtyTotal + thisDeliverQuantity;
            // if a product transfer is present, prepare array to be sent to backend

            if (thisDeliverQuantity > 0) {

                var thisSubProduct = {};
                if ($(".batchCode", $thisSubRow).data('id')) {
                    thisSubProduct.batchID = $(".batchCode", $thisSubRow).data('id');
                }

                if ($(".serialID", $thisSubRow).data('id')) {
                    thisSubProduct.serialID = $(".serialID", $thisSubRow).data('id');
                }

                thisSubProduct.qtyByBase = thisDeliverQuantity;
                thisSubProduct.warranty = $thisSubRow.find('#warranty').val();
                subProducts.push(thisSubProduct);
            }


        });
        if (checkSubProduct[thisVals.productID] != undefined) {
            if (qtyTotal != $("input[name='deliverQuanity']", $thisParentRow).val()) {
                p_notification(false, eb.getMessage('ERR_DELI_TOTAL_SUBQUAN'));
                return qtyTotal = false;
            } else {
                checkSubProduct[thisVals.productID] = 1;
            }
        }

        // to break out form $.each and exit function
        if (qtyTotal === false)
            return false;
        // ideally, since the individual batch/serial quantity is checked to be below the available qty,
        // the below condition should never become true
        if (qtyTotal > thisVals.availableQuantity.qty) {
            p_notification(false, eb.getMessage('ERR_DELI_TOTAL_DELIQUAN'));
            return false;
        }
        deliverSubProducts[thisVals.productID] = subProducts;
        SubProductsQty[thisVals.productID] = qtyTotal;
        var $transferQ = $("input[name='deliverQuanity']", $thisParentRow).prop('readonly', true);
        if (checkSubProduct[thisVals.productID] == undefined) {
            $transferQ.val(qtyTotal).change();
        }


       // use to set unit price using batch price
        if(subProducts.length == 1 && subProducts[0].batchID != null && subProducts[0].serialID == null && deliveryUnitPrice != 0 ){
            $("input[name='unitPrice']", $thisParentRow).val(deliveryUnitPrice).change();
        } else if(subProducts[0].batchID != null && subProducts[0].serialID != null){
            var tempBatchIDs = [];
            var sameBatchFlag = true;
            $.each(subProducts, function(key, value){
                tempBatchIDs[key] = value.batchID;
            });
            var tmpBtID;
            $.each(tempBatchIDs, function(key, value){

                if(key == 0){
                    tmpBtID = value;
                }
                if(tmpBtID != value){
                    sameBatchFlag = false;
                }
            });
            if(sameBatchFlag){
                $("input[name='unitPrice']", $thisParentRow).val(deliveryUnitPrice).change();
            } else {
                $("input[name='unitPrice']", $thisParentRow).val(defaultSellingPriceData[thisVals.productID]).change();
            }

        } else {
            $("input[name='unitPrice']", $thisParentRow).val(defaultSellingPriceData[thisVals.productID]).change();
        }

        $("#unitPrice", $thisParentRow).trigger(jQuery.Event("focusout"));
        $('#addDeliveryNoteProductsModal').data('id',"undefined");
        return true;
    }

    $productTable.on('click', 'button.edit-modal', function(e) {
        var productID = $(this).parents('tr').data('id');
        var productCode = $(this).parents('tr').find('#itemCode').val();
        $("#batch_data tr:not(.hidden)").remove();
        addBatchProduct(productID, productCode, 0);
        addSerialProduct(productID, productCode, 0);
        addSerialBatchProduct(productID, productCode, 0);

        //check this product serail or batch
        if (!$.isEmptyObject(currentProducts[productID].serial)) {
            $('#product-batch-modal .serial-auto-select').removeClass('hidden');
            $('#numberOfRow').val('');
        } else {
            $('#product-batch-modal .serial-auto-select').addClass('hidden');
        }
        
        $("tr", '#batch_data').each(function() {
            var $thisSubRow = $(this);
            for (var i in deliverSubProducts[productID]) {
                if (deliverSubProducts[productID][i].serialID != undefined) {
                    if ($(".serialID", $thisSubRow).data('id') == deliverSubProducts[productID][i].serialID) {
                        $("input[name='deliverQuantityCheck']", $thisSubRow).prop('checked', true);
                        $("input[name='warranty']", $thisSubRow).val(deliverSubProducts[productID][i].warranty);
                    }
                    
                } else if (deliverSubProducts[productID][i].batchID != undefined) {
                    if ($(".batchCode", $thisSubRow).data('id') == deliverSubProducts[productID][i].batchID) {
                        $(this).find("input[name='deliverQuantity']").val(deliverSubProducts[productID][i].qtyByBase).change();
                    }
                }
            }
        });
        $('#addDeliveryNoteProductsModal').data('id', $(this).parents('tr').data('id')).modal('show');
        $('#addDeliveryNoteProductsModal').unbind('hide.bs.modal');
    });
    $('form#deliveryNoteForm').on('submit', function() {
        if ($("tr.edit-row", $productTable).length > 0) {
            p_notification(false, eb.getMessage('ERR_DELI_SAVE_ALLPROD'));
            return false;
        }

        saveDeliveryNote();
        return false;
    });

    $('#glAccount').on('change', function() {
        if ($(this).val() > 0 && $(this).val() != selectedAccount) {
            selectedAccount = $(this).val();
        }
    });

    function saveDeliveryNote() {
        for (var i in checkSubProduct) {
            var $currentRow = getAddRow(i);
            if ($($currentRow).find('.add').parent('td').hasClass('hidden') && !$($currentRow).find('.edit-modal').parent('td').hasClass('hidden')) {
                if (checkSubProduct[i] == 0) {
                    p_notification(false, eb.getMessage('ERR_DELI_ADD_PROD', deliverProducts[i].productCode));
                    return;
                }
            }
        }
        var grandTotal = 0;
        for (var i in deliverProducts) {
            grandTotal += toFloat(deliverProducts[i].productTotal);
        }

        var tPayamount = 0.00;
        var balance = 0.00;
        var creditAmount = parseFloat(($('#creditAmount').val() != '') ? $('#creditAmount').val() : 0);

        $('.addPaymentMethod').find('.payMethod').each(function() {
            tPayamount += parseFloat($(this).find('.paidAmount').val());
        });

        balance = tPayamount - parseFloat(($('#totalpayment').text().replace(/,/g, ''))) + creditAmount;

        var cusID = (selectedAuthourType == 1) ? customerID : null;
        var accountID = (selectedAuthourType == 2) ? selectedAccount : null;

        var incomeData = {
            inventoryItemFlag: inventoryItemFlag,
            incomeCode: $('#deliveryNoteNo').val(),
            locationID: locationOut,
            products: deliverProducts,
            subProducts: deliverSubProducts,
            date: $('#deliveryDate').val(),
            customerID: cusID,
            accountID: accountID,
            customerName: $('#customer option:selected').text(),
            incomeTotal: parseFloat(($('#finaltotal').text().replace(/,/g, ''))),
            customCurrencyRate: customCurrencyRate,
            dimensionData: dimensionData,
            comment: $('#comment').val(),
            nonItempr: getNonItemProductList(),
        };

        if (selectedAuthourType == 1 && (typeof(customerID) == 'undefined' || customerID == '' || customerID == 0)) {
            p_notification(false, eb.getMessage('ERR_PROJECT_CUST_SBE_SELECT'));
        } else if (selectedAuthourType == 2 && (typeof(selectedAccount) == 'undefined' || selectedAccount == '' || selectedAccount == 0)) {
            p_notification(false, eb.getMessage('ERR_SELECT_PI_ACCOUNT'));
        } else if (typeof (locationOut) == 'undefined' || locationOut == '') {
            p_notification(false, eb.getMessage('ERR_GRN_SELECT_LOCAT'));
        }
        else if ($('#deliveryDate').val() == '') {
            p_notification(false, eb.getMessage('ERR_PI_VALID_DUE'));
        }
        else if ($('#deliveryNoteNo').val() == '') {
            p_notification(false, eb.getMessage('ERR_PLEASE_SET_REFERENCE_FOR_THE_INCOME_VOUCHER'));
        } else if($.isEmptyObject(deliverProducts) && getNonItemProductList().length == 0){
            p_notification(false,eb.getMessage('ERR_NO_INCOME_DATA'));
        } else if (validate_payment()){

            var paymentPostData = {
                // customerCredit: customercredit,
                // customerBalance: customerbalance,
                amount: $('#amount').val().replace(/,/g, ''),
                discount: $('#discountAmount').val().replace(/,/g, ''),
                memo: $('#memo').val(),
                paymentMethods: methods,
                creditAmount: $('#creditAmount').val(),
                loyaltyData: loyalty.getLoyaltySaveData(),
                restBalance: restBalance,
                totalPaidAmount: tPayamount,
                balance: balance,
                // customCurrencyId: $('#customCurrencyId').val(),
                customCurrencyRate: customCurrencyRate,
                cardType: $('#cardType').val(),
            };   

            incomeData.paymentPostData = paymentPostData;

            eb.ajax({
                type: 'POST',
                url: BASE_URL + '/income-api/saveIncome',
                data: incomeData,
                success: function(respond) {
                    p_notification(respond.status, respond.msg);
                    if (respond.status) {
                        var incomeEmail = '/income-api/send-income-email';
                        documentPreview(BASE_URL + '/income/preview/' + respond.data, 'documentpreview', incomeEmail, function($preview) {
                            $preview.on('hidden.bs.modal', function() {
                                if (!$("#preview:visible").length) {
                                    window.location.reload();
                                }
                            });
                        });
                    } 
                }
            });
        }
    }

    function validate_payment() {

        var valid = {
            amount: $('#amount').val().replace(/,/g, ''),
            discountAmount: $('#discountAmount').val().replace(/,/g, ''),
        };

        if (isNaN(valid.amount)) {
            p_notification(false, eb.getMessage('ERR_ADVPAY_AMOUNT_NUMR'));
            return false;
        } else if (valid.amount < 0) {
            p_notification(false, eb.getMessage('ERR_ADVPAY_AMOUNT_NEG'));
            return false;
        } else if (isNaN(valid.discountAmount)) {
            p_notification(false, eb.getMessage('ERR_PAY_DISCOUNT_NUMR'));
            return false;
        } else if (valid.discountAmount < 0) {
            p_notification(false, eb.getMessage('ERR_PAY_DISCOUNT_NEG'));
            return false;
        } else if ($('#restToPaid').val().replace(/,/g, '') > 0) {
            p_notification(false, eb.getMessage('ERR_PAY_TOTAL_SHOULD_BE_PAY'));
            return false;
        } else {
            var checked = true;
            methods = [];
            var creditAmount = parseFloat($('#creditAmount').val().replace(/,/g, ''));
            $('.payMethod').each(function() {
                var methodID = $('.paymentMethod option:selected', this).val();
                var paidAmount = $('.paidAmount', this).val();
                var checquenumber = $('.checquenumber', this).val();
                var bank = $('.bank', this).val();
                var reciptnumber = $('.reciptnumber', this).val();
                var cardnumber = $('.cardnumber', this).val();
                var bankID = $('.bankID', this).val();
                var accountID = $('.accountID', this).val();
                var customerBank = $('.customerBank', this).val();
                var customerAccountNumber = $('.customerAccountNumber', this).val();
                var giftCardID = $('.giftCardID', this).val();
                var lcPaymentReference = $('.lcPaymentReference', this).text();
                var ttPaymentReference = $('.ttPaymentReference', this).text();
                var postdatedStatus = 0;
                var postdatedDate = null;
                var cashAccountID = $(this).find('#cashGlAccountID').val();
                var chequeAccountID = $(this).find('#chequeGlAccountID').val();
                var creditAccountID = $(this).find('#creditGlAccountID').val();
                var bankTransferAccountID = $(this).find('#bankTransferGlAccountID').val();
                var lcAccountID = $(this).find('#lcGlAccountID').val();
                var ttAccountID = $(this).find('#ttGlAccountID').val();
                var bankTransferRef = $(this).find('#bankTransferRef').val();
                //check whether postdated cheque
                if ($('.postdated-cheque', this).is(":checked")) {
                    postdatedStatus = 1;
                    postdatedDate = $('.postdated-cheque-date', this).val();
                }

                methods.push({
                    methodID: methodID,
                    paidAmount: paidAmount,
                    checkNumber: checquenumber,
                    bank: bank,
                    reciptnumber: reciptnumber,
                    cardnumber: cardnumber,
                    bankID: bankID,
                    accountID: accountID,
                    customerBank: customerBank,
                    customerAccountNumber: customerAccountNumber,
                    giftCardID: giftCardID,
                    postdatedStatus: postdatedStatus,
                    postdatedDate: postdatedDate,
                    lcPaymentReference: lcPaymentReference,
                    ttPaymentReference: ttPaymentReference,
                    cashAccountID : cashAccountID,
                    chequeAccountID : chequeAccountID,
                    creditAccountID : creditAccountID,
                    bankTransferAccountID : bankTransferAccountID,
                    lcAccountID : lcAccountID,
                    ttAccountID : ttAccountID,
                    bankTransferRef: bankTransferRef
                });

                if (!paidAmount && !creditAmount) {
                    p_notification(false, eb.getMessage('ERR_PAY_PAID_AM_CAN_BE_EMTY'));
                    $('.paidAmount', this).focus();
                    checked = false;
                    return false;
                } else if (methodID == '') {
                    p_notification(false, eb.getMessage('ERR_PAY_PAID_METHOD_SHBE_SELECT'));
                    $('.advancepaymentMethod', this).focus();
                    checked = false;
                    return false;
                } else if (methodID == 3) {
                    if (reciptnumber != "" && reciptnumber.length > 12) {
                        p_notification(false, eb.getMessage('ERR_PAY_RECIPTNO_VALIDITY'));
                        $('.reciptnumber', this).focus();
                        checked = false;
                        return false;
                    } else if (cardnumber != "" && isNaN(cardnumber)) {
                        p_notification(false, eb.getMessage('ERR_ADVPAY_CRDTCARD_NUMR'));
                        $('.cardnumber', this).focus();
                        checked = false;
                        return false;
                    }
                } else if (methodID == 5) {

                    if (bankID == null || bankID == "") {
                       p_notification(false, eb.getMessage('ERR_PAY_METHOD_BANK_ID'));
                       $('.bankID', this).focus();
                       checked = false;
                       return false;
                    } else if (accountID == null || accountID == "") {
                       p_notification(false, eb.getMessage('ERR_PAY_METHOD_ACCOUNT_ID'));
                       $('.accountID', this).focus();
                       checked = false;
                       return false;
                    }


                    if (customerAccountNumber != '' && isNaN(customerAccountNumber)) {
                        p_notification(false, eb.getMessage('ERR_PAY_METHOD_CUST_ACCUNT'));
                        $('.customerAccountNumber', this).focus();
                        checked = false;
                        return false;
                    }
                } else if (methodID == 6) {
                    if (giftCardID == null || giftCardID == "") {
                        p_notification(false, eb.getMessage('ERR_PAY_METHOD_GIFT_CARD_SH_SELECT'));
                        $('.giftCardID', this).focus();
                        checked = false;
                        return false;
                    }
                } else if (methodID == 4) {
                    var credit = parseFloat($('#creditAmount').val().replace(/,/g, ''));
                    credit = isNaN(credit) ? 0 : credit;
                    var amount = parseFloat($('#amount').val().replace(/,/g, ''));
                    var totalBill = amount - credit;
                    totalBill = totalBill * customCurrencyRate;
                    var pAmount = paidAmount * customCurrencyRate;

                    if (!loyalty.isValid(pAmount, totalBill)) {
                        p_notification(false, eb.getMessage('ERR_LOYALTY_REDEEM_EXCEED'));
                        checked = false;
                        return false;
                    }
                    loyalty.calculateLoyaltyPoints(0, totalBill);
                    return true;
                }

                if (methodID != 4) {
                    var credit = parseFloat($('#creditAmount').val().replace(/,/g, ''));
                    credit = isNaN(credit) ? 0 : credit;
                    var amount = parseFloat($('#amount').val().replace(/,/g, ''));

                    var totalBill = amount - credit;
                    totalBill = totalBill * customCurrencyRate;
                    loyalty.calculateLoyaltyPoints(0, totalBill);
                }
            });
            if (checked) {
                return true;
            } else {
                return false;
            }
        }
    }

    $('table#deliveryNoteProductTable>tbody').on('keypress', 'input#itemQuantity, input#itemUnitPrice, input#unitPrice, input#deliveryNoteDiscount, #deliverQuanity,#availableQuantity ', function(e) {

        if (e.which == 13) {
            $('#add_item', $(this).parents('tr.add-row')).trigger('click');
            e.preventDefault();
        }

    });

    if ($(this).parents('tr').find('#itemCode').val() != '') {
        if ($(this).val().indexOf('.') > 0) {
            decimalPoints = $(this).val().split('.')[1].length;
        }
        if (this.id == 'unitPrice' && (!$.isNumeric($(this).parents('tr').find('#unitPrice').val()) || $(this).parents('tr').find('#unitPrice').val() < 0 || decimalPoints > 2)) {
            decimalPoints = 0;
            $(this).parents('tr').find('#unitPrice').val('');
        }
        if ((this.id == 'deliverQuanity') && (!$.isNumeric($(this).parents('tr').find('#deliverQuanity').val()) || $(this).parents('tr').find('#deliverQuanity').val() < 0)) {
            decimalPoints = 0;
            $(this).parents('tr').find('#deliverQuanity').val('');
        }
        if ((this.id == 'deliveryNoteDiscount')) {
            if ($(this).hasClass('precentage') && (!$.isNumeric($(this).parents('tr').find('#deliveryNoteDiscount').val()) || $(this).parents('tr').find('#deliveryNoteDiscount').val() < 0 || $(this).parents('tr').find('#deliveryNoteDiscount').val() >= 100 || (decimalPoints > 2))) {
                decimalPoints = 0;
                $(this).parents('tr').find('#deliveryNoteDiscount').val('');
            } else {
                decimalPoints = 0;
            }
        }
    }

    $productTable.on('focusout', '#deliverQuanity,#unitPrice,#deliveryNoteDiscount,.uomqty,.uomPrice', function() {
        var $thisRow = $(this).parents('tr');
        var productID = $thisRow.data('id');
        var checkedTaxes = Array();
        $thisRow.find('input:checkbox.addNewTaxCheck').each(function() {
            if (this.checked) {
                var cliTID = this.id;
                var tTaxID = cliTID.split('_')[1];
                checkedTaxes.push(tTaxID);
            }
        });
        var tmpItemQuentity = $(this).parents('tr').find('#deliverQuanity').val();
        var productType = $(this).parents('tr').find("#itemCode").data('PT');
        if (productType == 2 && tmpItemQuentity == 0) {
            tmpItemQuentity = 1;
        }
        var tmpItemTotal = (toFloat($(this).parents('tr').find('#unitPrice').val()) * toFloat(tmpItemQuentity));
        var tmpItemCost = tmpItemTotal;
        if ($(this).parents('tr').find('#deliveryNoteDiscount').hasClass('value')) {
            var discountType = "Val";
            var discount = $(this).parents('tr').find('#deliveryNoteDiscount').val();
            var unitPrice = $(this).parents('tr').find('#unitPrice').val();
            var tmpPDiscountvalue = validateDiscount(productID,discountType,discount,unitPrice);
            if (tmpPDiscountvalue >= 0) {
                tmpItemCost -= tmpPDiscountvalue * tmpItemQuentity;
            }
        } else {
            var discountType = "Per";
            var discount = $(this).parents('tr').find('#deliveryNoteDiscount').val();
            var tmpPDiscount = validateDiscount(productID,discountType,discount);
            if (tmpPDiscount >= 0) {
                tmpItemCost -= (tmpItemTotal * toFloat(tmpPDiscount) / toFloat(100));
            }
        }
        if (checkedTaxes) {
            currentItemTaxResults = calculateItemCustomTax(tmpItemCost, checkedTaxes);
            currentTaxAmount = (currentItemTaxResults == null) ? null : currentItemTaxResults.tTA;
        } else {
            currentTaxAmount = 0;
            currentItemTaxResults = '';
        }
        var itemCost = toFloat(tmpItemCost) + toFloat(currentTaxAmount);
        productsTotal[productID] = itemCost;
        productsTax[productID] = currentItemTaxResults;
        //if unit price is zero or discount greater than unit price,Then total may be negative value
        //So Total should be zero
//        if (itemCost < 0) {
//            itemCost = 0;
//        }
        $(this).parents('tr').find('#addNewTotal').html(accounting.formatMoney(itemCost));
        if (productsTax[productID] != null) {
            if (jQuery.isEmptyObject(productsTax[productID].tL)) {
                $(this).parents('tr').find('#taxApplied').addClass('glyphicon glyphicon-unchecked');
            } else {
                $(this).parents('tr').find('#taxApplied').removeClass('glyphicon glyphicon-unchecked');
                $(this).parents('tr').find('#taxApplied').addClass('glyphicon glyphicon-check');
            }
        }
    });
    $productTable.on('change', '.taxChecks.addNewTaxCheck', function() {
        $(this).parents('tr').find("#unitPrice").trigger(jQuery.Event("focusout"));
    });
    $productTable.on('click', '#selectAll', function() {
        $(this).parents('#addTaxUl').find('input:checkbox.addNewTaxCheck').each(function() {
            if (!this.checked) {
                this.checked = true;
            }
        });
         $(this).parents('tr').find("#unitPrice").trigger(jQuery.Event("focusout"));
    });

    $productTable.on('click', '#deselectAll', function() {
        $(this).parents('#addTaxUl').find('input:checkbox.addNewTaxCheck').each(function() {
            if (this.checked) {
                this.checked = false;
            }
        });
         $(this).parents('tr').find("#unitPrice").trigger(jQuery.Event("focusout"));
    });

    $productTable.on('click', 'ul.dropdown-menu', function(e) {
        e.stopPropagation();
    });
    function validateTransferForm(formData) {
        var selectedid = "#" + document.activeElement.id;
        var tableId = $(selectedid).closest('table').attr("id");

        if (formData.location == null || formData.location == "") {
            p_notification(false, eb.getMessage('ERR_DELI_SELECT_LOCAT'));
            $('#currentLocation').focus();
            return false;
        } else if (formData.deliveryCode == null || formData.deliveryCode == "") {
            p_notification(false, eb.getMessage('ERR_DELI_NUM_NOTBLANK'));
            return false;
        } else if (formData.customer == null || formData.customer == "") {
            p_notification(false, eb.getMessage('ERR_DELI_SELECT_CUST'));
            $('#customer').focus();
            return false;
        } else if (formData.date == null || formData.date == "") {
            p_notification(false, eb.getMessage('ERR_DELI_DATE_NOTBLANK'));
            $('#deliveryDate').focus();
            return false;
        } else if (formData.customer == null || formData.customer == "") {
            p_notification(false, eb.getMessage('ERR_DELI_SELECT_CUST'));
            $('#customer').focus();
            return false;
        } else if (formData.customerID == null || formData.customerID == "") {
            p_notification(false, eb.getMessage('ERR_INVOICE_INVALID_CUST'));
            $('#customer').focus();
            return false;
        } else if (formData.deliveryAddress == null || formData.deliveryAddress == "") {
            p_notification(false, eb.getMessage('ERR_DELI_ADDR_NOTBLANK'));
            $('#deliveryNoteAddress').focus();
            return false;
        } else if (jQuery.isEmptyObject(deliverProducts)) {
            if (tableId != "deliveryNoteProductTable") {
                p_notification(false, eb.getMessage('ERR_DELI_ADD_ATLEAST_ONEPROD'));
            }
            return false;
        } else if ((tableId == "deliveryNoteProductTable") && ((selectedid != '#showTax') || (selectedid != '#deliveryChargeEnable') || (selectedid != '#deliveryCharge'))) {
            return  true;
        }
        return true;
    }

    function isDate(txtDate)
    {
        var currVal = txtDate;
        if (currVal == '')
            return false;
        //Declare Regex
        var rxDatePattern = /^(\d{4})(\/|-|.)(\d{1,2})(\/|-|.)(\d{1,2})$/;
        var dtArray = currVal.match(rxDatePattern); // is format OK?

        if (dtArray == null)
            return false;
        //Checks for mm/dd/yyyy format.
        dtMonth = dtArray[3];
        dtDay = dtArray[5];
        dtYear = dtArray[1];
        if (dtMonth < 1 || dtMonth > 12)
            return false;
        else if (dtDay < 1 || dtDay > 31)
            return false;
        else if ((dtMonth == 4 || dtMonth == 6 || dtMonth == 9 || dtMonth == 11) && dtDay == 31)
            return false;
        else if (dtMonth == 2) {
            var isleap = (dtYear % 4 == 0 && (dtYear % 100 != 0 || dtYear % 400 == 0));
            if (dtDay > 29 || (dtDay == 29 && !isleap))
                return false;
        }
        return true;
    }

    /**
     * Inside Batch / Serial Product modal
     */

    function addBatchProduct(productID, productCode, deleted) {
        if ((!$.isEmptyObject(currentProducts[productID].batch))) {
            batchProduct = currentProducts[productID].batch;
            for (var i in batchProduct) {
                if (deleted != 1 && batchProduct[i].PBQ != 0) {
                    if ((!$.isEmptyObject(currentProducts[productID].productIDs))) {
                        if (currentProducts[productID].productIDs[i]) {
                            continue;
                        }
                    }

                    var batchKey = productCode + "-" + batchProduct[i].PBID;
                    var $clonedRow = $($('.batch_row').clone());
                    $clonedRow
                            .data('id', i).removeAttr('id')
                            .removeClass('hidden batch_row')
                            .addClass('remove_row');
                    $(".batchCode", $clonedRow).html(batchProduct[i].PBC).data('id', batchProduct[i].PBID);
                    $("input[name='availableQuantity']", $clonedRow).val(batchProduct[i].PBQ);
                    if (batchProduct[i].PBQ == 0) {
                        $("input[name='deliverQuantity']", $clonedRow).prop('disabled', true);
                    }
                    $("input[name='deliverQuantityCheck']", $clonedRow).remove();
                    $("input[name='warranty']", $clonedRow).prop('readonly', true);
                    $("input[name='warranty']", $clonedRow).val(batchProduct[i].PBWoD);
                    $("input[name='expireDate']", $clonedRow).val(batchProduct[i].PBExpD);
                    $("input[name='btPrice']", $clonedRow).val(parseFloat(batchProduct[i].PBUP).toFixed(2));
                    if (priceListItems[productID] != undefined && parseFloat(priceListItems[productID].itemPrice) > 0) {
                        $("input[name='btPrice']", $clonedRow).val(parseFloat(priceListItems[productID].itemPrice).toFixed(2));
                    }
                    if (batchProducts[batchKey]) {
                        $("input[name='deliverQuantity']", $clonedRow).val(batchProducts[batchKey].Qty);
                    }
                    if(!(batchProduct[i].PBExpD == null || batchProduct[i].PBExpD == '0000-00-00')){
                        var expireCheck = checkEpireData(batchProduct[i].PBExpD);
                        if(expireCheck){
                            $("input[name='deliverQuantity']", $clonedRow).attr('disabled',true);
                        }
                    }
                    $clonedRow.insertBefore('.batch_row');
                    $("input[name='deliverQuantity'],input[name='availableQuantity']", $clonedRow).addUom(currentProducts[productID].uom);
                } else {
                    batchProducts[batchKey] = undefined;
                }
            }
        }
    }

    function addSerialProduct(productID, productCode, deleted) {
        if ((!$.isEmptyObject(currentProducts[productID].serial))) {
            productSerial = currentProducts[productID].serial;
            for (var i in productSerial) {
                if (deleted != 1) {
                    var serialKey = productCode + "-" + productSerial[i].PSID;
                    var serialQty = (productSerial[i].PSS == 1) ? 0 : 1;
                    var $clonedRow = $($('.batch_row').clone());
                    $clonedRow
                            .data('id', i).removeAttr('id')
                            .removeClass('hidden batch_row')
                            .addClass('remove_row');
                    $(".serialID", $clonedRow)
                            .html(productSerial[i].PSC)
                            .data('id', productSerial[i].PSID);
                    $("input[name='availableQuantity']", $clonedRow).val(serialQty);
                    $("input[name='warranty']", $clonedRow).val(productSerial[i].PSWoD);
                    $("input[name='expireDate']", $clonedRow).val(productSerial[i].PSExpD);
                    if(!(productSerial[i].PSExpD == null || productSerial[i].PSExpD == '0000-00-00')){
                        var expireCheck = checkEpireData(productSerial[i].PSExpD);
                        if(expireCheck){
                            $("input[name='deliverQuantityCheck']", $clonedRow).attr('disabled',true);
                        }
                    }
                    if (serialQty = 0) {
                        $("input[name='deliverQuantityCheck']", $clonedRow).prop('disabled', true);
                    }
                    $("input[name='deliverQuantity']", $clonedRow).parent().remove();
                    if (serialProducts[serialKey]) {
                        if (serialProducts[serialKey].Qty == 1) {
                            $("input[name='deliverQuantityCheck']", $clonedRow).attr('checked', true);
                        }
                    }

                    $clonedRow.insertBefore('.batch_row');
                    $("input[name='deliverQuantity'],input[name='availableQuantity']", $clonedRow).addUom(currentProducts[productID].uom);
                } else {
                    serialProducts[serialKey] = undefined;
                }
            }
        }
    }

    function addSerialBatchProduct(productID, productCode, deleted) {
        if ((!$.isEmptyObject(currentProducts[productID].batchSerial))) {
            productBatchSerial = currentProducts[productID].batchSerial;
            for (var i in productBatchSerial) {
                if (deleted != 1) {
                    var serialKey = productCode + "-" + productBatchSerial[i].PBID;
                    var serialQty = (productBatchSerial[i].PSS == 1) ? 0 : 1;
                    var $clonedRow = $($('.batch_row').clone());
                    $clonedRow
                            .data('id', i).removeAttr('id')
                            .removeClass('hidden batch_row')
                            .addClass('remove_row');
                    $(".serialID", $clonedRow)
                            .html(productBatchSerial[i].PSC)
                            .data('id', productBatchSerial[i].PSID);
                    $(".batchCode", $clonedRow)
                            .html(productBatchSerial[i].PBC)
                            .data('id', productBatchSerial[i].PBID);
                    $("input[name='availableQuantity']", $clonedRow).val(serialQty);
                    $("input[name='warranty']", $clonedRow).val(productBatchSerial[i].PBSWoD);
                    if (serialQty = 0) {
                        $("input[name='deliverQuantityCheck']", $clonedRow).prop('disabled', true);
                    }
                    $("input[name='deliverQuantity']", $clonedRow).parent().remove();
                    $("input[name='expireDate']", $clonedRow).val(productBatchSerial[i].PBSExpD);
                    $("input[name='btPrice']", $clonedRow).val(parseFloat(productBatchSerial[i].PBUP).toFixed(2));
                    if (priceListItems[productID] != undefined && parseFloat(priceListItems[productID].itemPrice) > 0) {
                        $("input[name='btPrice']", $clonedRow).val(parseFloat(priceListItems[productID].itemPrice).toFixed(2));
                    }
                    if(!(productBatchSerial[i].PBSExpD == null || productBatchSerial[i].PBSExpD == '0000-00-00')){
                        var expireCheck = checkEpireData(productBatchSerial[i].PBSExpD);
                        if(expireCheck){
                            $("input[name='deliverQuantityCheck']", $clonedRow).attr('disabled',true);
                        }
                    }
                    if (batchProducts[serialKey]) {
                        if (batchProducts[serialKey].Qty == 1) {
                            $("input[name='deliveryQuantityCheck']", $clonedRow).attr('checked', true);
                        }
                    }

                    $clonedRow.insertBefore('.batch_row');
                    $("input[name='deliverQuantity'],input[name='availableQuantity']", $clonedRow).addUom(currentProducts[productID].uom);
                } else {
                    batchProducts[serialKey] = undefined;
                }
            }
        }
    }

    function checkEpireData(ExpireDate){
        var today = new Date();
        var exDate = new Date(ExpireDate);

        var ctoday = Date.UTC(today.getFullYear(), today.getMonth()+1, today.getDate());
        var cexDate = Date.UTC(exDate.getFullYear(), exDate.getMonth()+1, exDate.getDate());

        var flag = true;
        if(cexDate >= ctoday){
            flag = false;
        }
        return flag;
    }

    function clearProductScreen() {
        products = {};
        setGrnTotalCost();
        setTotalTax();
        $('.addedProducts').remove();
        locProductCodes = '';
        locProductNames = '';
        selectedProduct = '';
        selectedProductQuantity = '';
        selectedProductUom = '';
        batchCount = '';
    }
    function setGrnTotalCost() {
        var grnTotal = 0;
        for (var i in deliverProducts) {
            grnTotal += toFloat(deliverProducts[i].productTotal);
        }
        if (grnTotal < 0) {
            grnTotal = 0;
        }

        var tempNonItemTotal = 0;
        var tempTotal = grnTotal;
        var tempSubTotal = grnTotal;
        var nonItemDetails = getNonItemProductList();

        for (j = 0; j < (nonItemDetails.length); j++) {
            tempNonItemTotal += accounting.unformat(nonItemDetails[j]['totalPrice']);
        }
        var tempAllSubTotal = parseFloat(tempSubTotal) + parseFloat(tempNonItemTotal);

        $('#subtotal').html(accounting.formatMoney(tempAllSubTotal));
        if ($('#deliveryChargeEnable').is(':checked')) {
            var deliveryAmount = $('#deliveryCharge').val();
            tempAllSubTotal += toFloat(deliveryAmount);
        }
        $('#finaltotal').html(accounting.formatMoney(tempAllSubTotal));
        // $('#amount').val(accounting.formatMoney(tempAllSubTotal));
        tmpFinalTotal = tempAllSubTotal;
        setPaymentTotal(tempAllSubTotal);
    }

    function setPaymentTotal(tempAllSubTotal) {
        tempAllSubTotal = (tempAllSubTotal == undefined) ? tmpFinalTotal : tempAllSubTotal;
        var totalOfPayment = tempAllSubTotal;

        paymentTotal = totalOfPayment;
        oldDiscountAmount = parseFloat(($('#discountAmount').val() != '') ? $('#discountAmount').val() : 0);
        if (oldDiscountAmount != 0 && paymentTotal != 0.00) {
            paymentTotal = paymentTotal - oldDiscountAmount;
        }
        $('#amount').val(numberWithCommas(totalOfPayment.toFixed(2)));
        $('#restToPaid').val(numberWithCommas(paymentTotal.toFixed(2)));
        $('#totalpayment').html(numberWithCommas(paymentTotal.toFixed(2)));

        // var customerCurrentBalance = customerbalance - totalOfPayment;
        // if ($('#customCurrencyId').val() == '') {
        //     $('#currentBalance').val(numberWithCommas(customerCurrentBalance.toFixed(2)));
        // }

        setCreditAmountAndRestToPaid();

    }

    function setCreditAmountAndRestToPaid() {
        var creditAmount = parseFloat(($('#creditAmount').val() != '') ? $('#creditAmount').val() : 0);
        var customerCreditAmount = 0.00;
        var paidAmount = 0.00;
        if (creditAmount > paymentTotal) {
            $('#creditAmount').val(paymentTotal);
            creditAmount = paymentTotal;
        }

        if ((creditAmount * customCurrencyRate) > parseFloat(customercredit)) {
            p_notification(false, eb.getMessage('ERR_PAY_CUST_CREDIT_CNT_MO_THAN_AMOUNT'));
            $('#creditAmount').val(0.00);
            creditAmount = 0.00;
            customerCreditAmount = customercredit;
        } else {
            customerCreditAmount = (customercredit - creditAmount).toFixed(2);
        }

//get the total of paid amount form many payment methods
        var totalPaidAmount = getTotalPaidAmount();

        paidAmount = paymentTotal - creditAmount - totalPaidAmount;
        if ($('#customCurrencyId').val() == '') {
            $('#currentCredit').val(numberWithCommas(customerCreditAmount));
        }
        $('#restToPaid').val(numberWithCommas(paidAmount.toFixed(2)));
    }

    function getTotalPaidAmount() {
        var totalPaidAmount = 0.00;
        $('.paidAmount').each(function() {
            totalPaidAmount += ($(this).val()) ? parseFloat($(this).val()) : 0.00;
        });
        return totalPaidAmount;
    }

    function getNonItemProductList() {
        var i = 0;
        var nonItemProduct = new Array();
        $('#non-item-tbody tr.non-item').each(function() {
            var $tr = $(this);
            var nonItemTax = Array();
            var rowID = $(this).attr('id');

            $($tr.find('.taxChecks.addNewTaxCheck')).each(function() {
                if (this.checked) {
                    var cliTID = this.id;
                    var taxID = cliTID.split('_')[1];
                    var rowID = cliTID.split('_')[0];
                    nonItemTax.push(taxID);
                }
            });

            nonItemProduct[i] = {
                description: $(this).find('input[name=non-item-description]').val(),
                qty: 1,
                unitPrice: $(this).find('input[name=non-item-unit-prc]').val(),
                totalPrice: accounting.unformat($(this).find('#addNewNonItemTotal').text()),
                tax: nonItemTax,
                exType: getNonItemExpenseType($(this).find('.non-item-ex-type-selectbox').val()),
                exFormat: getNonItemExpenseFormat($(this).find('.non-item-ex-type-selectbox').val())
            };
            i++;
        });
        return nonItemProduct;
    }

    // expense type dopdown option list has merge with GLAccounts
    // there vale has a prifix to identify whether it is a expense type or a GLAccount
    // eg: exp_14 - for expense format
    // eg: acc_14 - for GLAccount format
    function getNonItemExpenseType(str){
        return str.split('_')[1];
    }

    // expense type dopdown option list has merge with GLAccounts
    // there vale has a prifix to identify whether it is a expense type or a GLAccount
    // eg: exp_14 - for expense format
    // eg: acc_14 - for GLAccount format
    function getNonItemExpenseFormat(str){
        return str.split('_')[0];
    }

    function setTotalTax() {
        totalTaxList = {};
        if (TAXSTATUS) {
            for (var k in deliverProducts) {
                for (var l in deliverProducts[k].pTax.tL) {
                    if (totalTaxList.hasOwnProperty(l)) {
                        var curTA = totalTaxList[l].tA;
                        var newTA = (toFloat(curTA) + toFloat(deliverProducts[k].pTax.tL[l].tA));
                        totalTaxList[l].tA = newTA.toFixed(2);
                    } else {
                        totalTaxList[l] = {tN: deliverProducts[k].pTax.tL[l].tN, tP: deliverProducts[k].pTax.tL[l].tP, tA: deliverProducts[k].pTax.tL[l].tA};
                    }
                }
            }

            var totalTaxHtml = "";
            for (var t in totalTaxList) {
                totalTaxHtml += totalTaxList[t].tN + "(" + totalTaxList[t].tP + "%)&nbsp" + accounting.formatMoney(totalTaxList[t].tA) + "<br>";
            }
            $('#totalTaxShow').html(totalTaxHtml);
        }
    }

    $('#non-item-tbody').on('click', '#editNonItemLine', function(){
        var idValue = $(this).parents('tr').attr('id');
        delete(existTaxArray[idValue.split('-')[3]]);
        $(this).parents('tr').find('.pv-non-item-edit').addClass('hidden');
        $(this).parents('tr').find('.pv-non-item-add').removeClass('hidden').addClass('editedRow');
        $(this).parents('tr').find('input[name=non-item-description]').attr('disabled',false);
        $(this).parents('tr').find('input[name=non-item-qty]').attr('disabled',false);
        $(this).parents('tr').find('input[name=non-item-unit-prc]').attr('disabled',false);
        $(this).parents('tr').find('.non-item-tax').attr('disabled', false);
        var lineID = $(this).parents('tr').attr('data-lienid');
        var $selectPicker = $(this).parents('tr').find('.non-item-ex-type-selectbox');
        $selectPicker.prop('disabled', false).selectpicker('refresh');
        $selectPicker.addClass('ex-type');

    });

    function setTaxListForProduct(productID, $currentRow) {
        if ((!jQuery.isEmptyObject(currentProducts[productID].tax))) {
            productTax = currentProducts[productID].tax;
            $currentRow.find('#taxApplied').removeClass('glyphicon glyphicon-unchecked');
            $currentRow.find('#taxApplied').addClass('glyphicon glyphicon-check');
            $currentRow.find('.tempLi').remove();
            $currentRow.find('#addNewTax').attr('disabled', false);
            for (var i in productTax) {
                var clonedLi = $($currentRow.find('#sampleLi').clone()).removeClass('hidden').attr('id', 'li_' + i).addClass("tempLi");
                clonedLi.children(".taxChecks").attr('id', productID + '_' + i).prop('checked', true).addClass('addNewTaxCheck');
                if (productTax[i].tS == 0) {
                    clonedLi.children(".taxChecks").attr('disabled', 'disabled');
                    clonedLi.children(".taxName").addClass('crossText');
                }
                clonedLi.children(".taxName").html('&nbsp&nbsp' + productTax[i].tN + '&nbsp&nbsp' + productTax[i].tP + '%').attr('for', productID + '_' + i);
                clonedLi.insertBefore($currentRow.find('#sampleLi'));
            }
        } else {
            $('#taxApplied').addClass('glyphicon glyphicon-unchecked');
            $currentRow.find('#addNewTax').attr('disabled', true);
        }

    }

    $('#deliveryNoteCancel').on('click', function() {
        window.location.reload();
    });

    $('#showTax').on('click', function() {
        if (this.checked) {
            if (!jQuery.isEmptyObject(deliverProducts)) {
                $('#totalTaxShow').removeClass('hidden');
            } else {
                p_notification(false, eb.getMessage('ERR_GRN_PROD_TAX'));
            }

        } else {
            $('#totalTaxShow').addClass('hidden');
        }

    });
    $('#deliveryChargeEnable').on('click', function() {
        setGrnTotalCost();
        if (this.checked) {
            $('.deliCharges').removeClass('hidden');
            $('#deliveryCharge').focus();
        }
        else {
            $('#deliveryCharge').val(0);
            $('.deliCharges').addClass('hidden');
        }
    });
    $('#deliveryCharge').on('change keyup', function() {
        if (!$.isNumeric($('#deliveryCharge').val()) || $(this).val() < 0) {
            $(this).val('');
            setGrnTotalCost();
        } else {
            setGrnTotalCost();
        }
    });
    var nowTemp = new Date();
    var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);
    var checkin = $('#deliveryDate').datepicker({onRender: function(date) {
            return (date.valueOf() > now.valueOf() ? ((U_DATE_R_OVERRIDE) ? '' : 'disabled') : '');
        },
        //format: 'yyyy-mm-dd'
    }).on('changeDate', function(ev) {
        checkin.hide();
    }).data('datepicker');
    checkin.setValue(now);
    function resetDeliveryNotePage() {
        clearProductScreen();
        deliverProducts = {};
        deliverSubProducts = {};
        $('#salesPersonID').attr('disabled', false);
        $('#salesPersonID').selectpicker('render');
        $('#add-new-item-row').find('tr').each(function() {
            if (!$(this).hasClass('add-row')) {
                $(this).remove();
            }
        });
        $('#subtotal').val('');
        $('#finaltotal').val('');
        $('#totalTaxShow').html('');
        $('#priceListId').val('').attr('disabled', false).selectpicker('refresh');
        $('#priceListId').trigger('change');
    }


    $('#customCurrencyId').on('change', function() {
        eb.ajax({
            type: 'POST',
            url: BASE_URL + '/currencySymbol/getCustomCurrencyByCustomCurrencyId',
            data: {customCurrencyId: $(this).val()},
            success: function(respond) {
                if (respond.status == true) {
                    $('.cCurrency').text(respond.data.currencySymbol);
                }
            }
        });
    });

    $('#priceListId').on('change', function() {
        priceListId = $(this).val();
        eb.ajax({
            type: 'POST',
            url: BASE_URL + '/currencySymbol/getPriceListWithDiscount',
            data: {priceListId: priceListId},
            success: function(respond) {
                if (respond.status == true) {
                    priceListItems = respond.data;
                } else {
                    priceListItems = [];
                }
            }
        });
    });

});

function getCustomerProfilesDetails(customerID) {
    eb.ajax({
        type: 'POST',
        url: BASE_URL + '/customerAPI/getCustomerProfilesByCustomerID',
        data: {customerID: customerID},
        success: function(respond) {
            if (respond.status == true) {
                $('.cus_prof_div').removeClass('hidden');
                $('.cus-prof-select').removeClass('hidden');
                $('.default-cus-prof').addClass('hidden');
                $('.tooltip_for_default_cus').removeClass('hidden');
                setCustomerProfilePicker(respond.data['customerProfileData']);
            } else {
                $('.cus_prof_div').addClass('hidden');
                $('.cus-prof-select').addClass('hidden');
                $('.default-cus-prof').removeClass('hidden');
                $('.tooltip_for_default_cus').addClass('hidden');
            }
        }
    });
}

function setCustomerProfilePicker(data) {
    $('#cusProfileDN').html("<option value=''>" + "Select a Customer Profile" + "</option>");
    $.each(data, function(index, value) {
        $('#cusProfileDN').append("<option value='" + index + "'>" + value['profName'] + "</option>");
        if (value['isPrimary'] == 1) {
            $('#cusProfileDN').html("<option value='" + index + "'>" + value['profName'] + "</option>");
            cusProfID = index;
        }
    });
    $('#cusProfileDN').selectpicker('refresh');

    $('#cusProfileDN').on('change', function(e) {
        e.preventDefault();
        cusProfID = $(this).val();

    });
}

function validateDiscount(productID, discountType, currentDiscount, unitPrice) {
    var defaultProductData = locationProducts[productID];
    var newDiscount = 0;
    if (defaultProductData.dEL == 1) {
        if (discountType == 'Val') {
            if (toFloat(defaultProductData.dV) !=0 && toFloat(currentDiscount) > toFloat(defaultProductData.dV)) {
                    newDiscount = toFloat(defaultProductData.dV);
                    p_notification(false, eb.getMessage('ERR_INVO_DISCOUNT_VALUE'));
            } else if (toFloat(defaultProductData.dV) == 0) {
                if (toFloat(currentDiscount) > toFloat(unitPrice)) {
                    newDiscount = toFloat(unitPrice);
                } else {
                    newDiscount = toFloat(currentDiscount);    
                }
            } else {
                newDiscount = toFloat(currentDiscount);
            }

        } else {
            if (toFloat(defaultProductData.dPR) != 0 && toFloat(currentDiscount) > toFloat(defaultProductData.dPR)) {
                    newDiscount = toFloat(defaultProductData.dPR);
                    p_notification(false, eb.getMessage('ERR_INVO_DISC_PERCENTAGE'));
            } else if (toFloat(defaultProductData.dPR) == 0) {
                if (toFloat(currentDiscount) > 100 ) {
                    newDiscount = 100;
                    p_notification(false, eb.getMessage('ERR_PI_DISC_PERCENTAGE'));   
                } else {
                    newDiscount = toFloat(currentDiscount);    
                }
            } else {
                newDiscount = toFloat(currentDiscount);
            }
        }
    } 
    return newDiscount;
}
