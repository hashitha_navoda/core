if(!wizard){
	var wizard = false;
}
$(document).ready(function() {

	loadDropDownFromDatabase('/accounts-api/search-active-accounts-for-dropdown', "", "", '#glAccountSetupItemDefaultSalesAccountID');
	loadDropDownFromDatabase('/accounts-api/search-active-accounts-for-dropdown', "", "", '#glAccountSetupItemDefaultInventoryAccountID');
	loadDropDownFromDatabase('/accounts-api/search-active-accounts-for-dropdown', "", "", '#glAccountSetupItemDefaultCOGSAccountID');
	loadDropDownFromDatabase('/accounts-api/search-active-accounts-for-dropdown', "", "", '#glAccountSetupItemDefaultAdjusmentAccountID');
	loadDropDownFromDatabase('/accounts-api/search-active-accounts-for-dropdown', "", "", '#glAccountSetupSalesAndCustomerReceivableAccountID');
	loadDropDownFromDatabase('/accounts-api/search-active-accounts-for-dropdown', "", "", '#glAccountSetupSalesAndCustomerSalesAccountID');
	loadDropDownFromDatabase('/accounts-api/search-active-accounts-for-dropdown', "", "", '#glAccountSetupSalesAndCustomerSalesDiscountAccountID');
	loadDropDownFromDatabase('/accounts-api/search-active-accounts-for-dropdown', "", "", '#glAccountSetupSalesAndCustomerAdvancePaymentAccountID');
	loadDropDownFromDatabase('/accounts-api/search-active-accounts-for-dropdown', "", "", '#glAccountSetupPurchasingAndSupplierPayableAccountID');
	loadDropDownFromDatabase('/accounts-api/search-active-accounts-for-dropdown', "", "", '#glAccountSetupPurchasingAndSupplierPurchaseDiscountAccountID');
	loadDropDownFromDatabase('/accounts-api/search-active-accounts-for-dropdown', "", "", '#glAccountSetupPurchasingAndSupplierGrnClearingAccountID');
	loadDropDownFromDatabase('/accounts-api/search-active-accounts-for-dropdown', "", "", '#glAccountSetupPurchasingAndSupplierAdvancePaymentAccountID');
	loadDropDownFromDatabase('/accounts-api/search-active-accounts-for-dropdown', "", "", '#glAccountSetupGeneralExchangeVarianceAccountID');
	loadDropDownFromDatabase('/accounts-api/search-active-accounts-for-dropdown', "", "", '#glAccountSetupGeneralProfitAndLostYearAccountID');
	loadDropDownFromDatabase('/accounts-api/search-active-accounts-for-dropdown', "", "", '#glAccountSetupGeneralBankChargersAccountID');
	loadDropDownFromDatabase('/accounts-api/search-active-accounts-for-dropdown', "", "", '#glAccountSetupGeneralDeliveryChargersAccountID');
	loadDropDownFromDatabase('/accounts-api/search-active-accounts-for-dropdown', "", "", '#glAccountSetupGeneralLoyaltyExpenseAccountID');

	if($('#glAccountSetupID').val()!= ''){
		var glAccountSetupItemDefaultSalesAccountID = $('#glAccountSetupItemDefaultSalesAccountID').data('id');
		var glAccountSetupItemDefaultSalesAccountName = $('#glAccountSetupItemDefaultSalesAccountID').data('value');
		if(glAccountSetupItemDefaultSalesAccountID!=''){
			$('#glAccountSetupItemDefaultSalesAccountID').append("<option value='"+glAccountSetupItemDefaultSalesAccountID+"'>"+glAccountSetupItemDefaultSalesAccountName+"</option>")
			$('#glAccountSetupItemDefaultSalesAccountID').val(glAccountSetupItemDefaultSalesAccountID).selectpicker('refresh');
		}

		var glAccountSetupItemDefaultInventoryAccountID = $('#glAccountSetupItemDefaultInventoryAccountID').data('id');
		var glAccountSetupItemDefaultInventoryAccountName = $('#glAccountSetupItemDefaultInventoryAccountID').data('value');
		if(glAccountSetupItemDefaultInventoryAccountID!=''){
			$('#glAccountSetupItemDefaultInventoryAccountID').append("<option value='"+glAccountSetupItemDefaultInventoryAccountID+"'>"+glAccountSetupItemDefaultInventoryAccountName+"</option>")
			$('#glAccountSetupItemDefaultInventoryAccountID').val(glAccountSetupItemDefaultInventoryAccountID).selectpicker('refresh');
		}

		var glAccountSetupItemDefaultCOGSAccountID = $('#glAccountSetupItemDefaultCOGSAccountID').data('id');
		var glAccountSetupItemDefaultCOGSAccountName = $('#glAccountSetupItemDefaultCOGSAccountID').data('value');
		if(glAccountSetupItemDefaultCOGSAccountID!=''){
			$('#glAccountSetupItemDefaultCOGSAccountID').append("<option value='"+glAccountSetupItemDefaultCOGSAccountID+"'>"+glAccountSetupItemDefaultCOGSAccountName+"</option>")
			$('#glAccountSetupItemDefaultCOGSAccountID').val(glAccountSetupItemDefaultCOGSAccountID).selectpicker('refresh');
		}

		var glAccountSetupItemDefaultAdjusmentAccountID = $('#glAccountSetupItemDefaultAdjusmentAccountID').data('id');
		var glAccountSetupItemDefaultAdjusmentAccountName = $('#glAccountSetupItemDefaultAdjusmentAccountID').data('value');
		if(glAccountSetupItemDefaultAdjusmentAccountID!=''){
			$('#glAccountSetupItemDefaultAdjusmentAccountID').append("<option value='"+glAccountSetupItemDefaultAdjusmentAccountID+"'>"+glAccountSetupItemDefaultAdjusmentAccountName+"</option>")
			$('#glAccountSetupItemDefaultAdjusmentAccountID').val(glAccountSetupItemDefaultAdjusmentAccountID).selectpicker('refresh');
		}

		var glAccountSetupSalesAndCustomerReceivableAccountID = $('#glAccountSetupSalesAndCustomerReceivableAccountID').data('id');
		var glAccountSetupSalesAndCustomerReceivableAccountName = $('#glAccountSetupSalesAndCustomerReceivableAccountID').data('value');
		if(glAccountSetupSalesAndCustomerReceivableAccountID!=''){
			$('#glAccountSetupSalesAndCustomerReceivableAccountID').append("<option value='"+glAccountSetupSalesAndCustomerReceivableAccountID+"'>"+glAccountSetupSalesAndCustomerReceivableAccountName+"</option>")
			$('#glAccountSetupSalesAndCustomerReceivableAccountID').val(glAccountSetupSalesAndCustomerReceivableAccountID).selectpicker('refresh');
		}

		var glAccountSetupSalesAndCustomerSalesAccountID = $('#glAccountSetupSalesAndCustomerSalesAccountID').data('id');
		var glAccountSetupSalesAndCustomerSalesAccountName = $('#glAccountSetupSalesAndCustomerSalesAccountID').data('value');
		if(glAccountSetupSalesAndCustomerSalesAccountID!=''){
			$('#glAccountSetupSalesAndCustomerSalesAccountID').append("<option value='"+glAccountSetupSalesAndCustomerSalesAccountID+"'>"+glAccountSetupSalesAndCustomerSalesAccountName+"</option>")
			$('#glAccountSetupSalesAndCustomerSalesAccountID').val(glAccountSetupSalesAndCustomerSalesAccountID).selectpicker('refresh');
		}

		var glAccountSetupSalesAndCustomerSalesDiscountAccountID = $('#glAccountSetupSalesAndCustomerSalesDiscountAccountID').data('id');
		var glAccountSetupSalesAndCustomerSalesDiscountAccountName = $('#glAccountSetupSalesAndCustomerSalesDiscountAccountID').data('value');
		if(glAccountSetupSalesAndCustomerSalesDiscountAccountID!=''){
			$('#glAccountSetupSalesAndCustomerSalesDiscountAccountID').append("<option value='"+glAccountSetupSalesAndCustomerSalesDiscountAccountID+"'>"+glAccountSetupSalesAndCustomerSalesDiscountAccountName+"</option>")
			$('#glAccountSetupSalesAndCustomerSalesDiscountAccountID').val(glAccountSetupSalesAndCustomerSalesDiscountAccountID).selectpicker('refresh');
		}

		var glAccountSetupSalesAndCustomerAdvancePaymentAccountID = $('#glAccountSetupSalesAndCustomerAdvancePaymentAccountID').data('id');
		var glAccountSetupSalesAndCustomerAdvancePaymentAccountName = $('#glAccountSetupSalesAndCustomerAdvancePaymentAccountID').data('value');
		if(glAccountSetupSalesAndCustomerAdvancePaymentAccountID!=''){
			$('#glAccountSetupSalesAndCustomerAdvancePaymentAccountID').append("<option value='"+glAccountSetupSalesAndCustomerAdvancePaymentAccountID+"'>"+glAccountSetupSalesAndCustomerAdvancePaymentAccountName+"</option>")
			$('#glAccountSetupSalesAndCustomerAdvancePaymentAccountID').val(glAccountSetupSalesAndCustomerAdvancePaymentAccountID).selectpicker('refresh');
		}

		var glAccountSetupPurchasingAndSupplierPayableAccountID = $('#glAccountSetupPurchasingAndSupplierPayableAccountID').data('id');
		var glAccountSetupPurchasingAndSupplierPayableAccountName = $('#glAccountSetupPurchasingAndSupplierPayableAccountID').data('value');
		if(glAccountSetupPurchasingAndSupplierPayableAccountID!=''){
			$('#glAccountSetupPurchasingAndSupplierPayableAccountID').append("<option value='"+glAccountSetupPurchasingAndSupplierPayableAccountID+"'>"+glAccountSetupPurchasingAndSupplierPayableAccountName+"</option>")
			$('#glAccountSetupPurchasingAndSupplierPayableAccountID').val(glAccountSetupPurchasingAndSupplierPayableAccountID).selectpicker('refresh');
		}

		var glAccountSetupPurchasingAndSupplierPurchaseDiscountAccountID = $('#glAccountSetupPurchasingAndSupplierPurchaseDiscountAccountID').data('id');
		var glAccountSetupPurchasingAndSupplierPurchaseDiscountAccountName = $('#glAccountSetupPurchasingAndSupplierPurchaseDiscountAccountID').data('value');
		if(glAccountSetupPurchasingAndSupplierPurchaseDiscountAccountID!=''){
			$('#glAccountSetupPurchasingAndSupplierPurchaseDiscountAccountID').append("<option value='"+glAccountSetupPurchasingAndSupplierPurchaseDiscountAccountID+"'>"+glAccountSetupPurchasingAndSupplierPurchaseDiscountAccountName+"</option>")
			$('#glAccountSetupPurchasingAndSupplierPurchaseDiscountAccountID').val(glAccountSetupPurchasingAndSupplierPurchaseDiscountAccountID).selectpicker('refresh');
		}

		var glAccountSetupPurchasingAndSupplierGrnClearingAccountID = $('#glAccountSetupPurchasingAndSupplierGrnClearingAccountID').data('id');
		var glAccountSetupPurchasingAndSupplierGrnClearingAccountName = $('#glAccountSetupPurchasingAndSupplierGrnClearingAccountID').data('value');
		if(glAccountSetupPurchasingAndSupplierGrnClearingAccountID!=''){
			$('#glAccountSetupPurchasingAndSupplierGrnClearingAccountID').append("<option value='"+glAccountSetupPurchasingAndSupplierGrnClearingAccountID+"'>"+glAccountSetupPurchasingAndSupplierGrnClearingAccountName+"</option>")
			$('#glAccountSetupPurchasingAndSupplierGrnClearingAccountID').val(glAccountSetupPurchasingAndSupplierGrnClearingAccountID).selectpicker('refresh');
		}

		var glAccountSetupPurchasingAndSupplierAdvancePaymentAccountID = $('#glAccountSetupPurchasingAndSupplierAdvancePaymentAccountID').data('id');
		var glAccountSetupPurchasingAndSupplierAdvancePaymentAccountName = $('#glAccountSetupPurchasingAndSupplierAdvancePaymentAccountID').data('value');
		if(glAccountSetupPurchasingAndSupplierAdvancePaymentAccountID!=''){
			$('#glAccountSetupPurchasingAndSupplierAdvancePaymentAccountID').append("<option value='"+glAccountSetupPurchasingAndSupplierAdvancePaymentAccountID+"'>"+glAccountSetupPurchasingAndSupplierAdvancePaymentAccountName+"</option>")
			$('#glAccountSetupPurchasingAndSupplierAdvancePaymentAccountID').val(glAccountSetupPurchasingAndSupplierAdvancePaymentAccountID).selectpicker('refresh');
		}

		var glAccountSetupGeneralExchangeVarianceAccountID = $('#glAccountSetupGeneralExchangeVarianceAccountID').data('id');
		var glAccountSetupGeneralExchangeVarianceAccountName = $('#glAccountSetupGeneralExchangeVarianceAccountID').data('value');
		if(glAccountSetupGeneralExchangeVarianceAccountID!=''){
			$('#glAccountSetupGeneralExchangeVarianceAccountID').append("<option value='"+glAccountSetupGeneralExchangeVarianceAccountID+"'>"+glAccountSetupGeneralExchangeVarianceAccountName+"</option>")
			$('#glAccountSetupGeneralExchangeVarianceAccountID').val(glAccountSetupGeneralExchangeVarianceAccountID).selectpicker('refresh');
		}

		var glAccountSetupGeneralProfitAndLostYearAccountID = $('#glAccountSetupGeneralProfitAndLostYearAccountID').data('id');
		var glAccountSetupGeneralProfitAndLostYearAccountName = $('#glAccountSetupGeneralProfitAndLostYearAccountID').data('value');
		if(glAccountSetupGeneralProfitAndLostYearAccountID!=''){
			$('#glAccountSetupGeneralProfitAndLostYearAccountID').append("<option value='"+glAccountSetupGeneralProfitAndLostYearAccountID+"'>"+glAccountSetupGeneralProfitAndLostYearAccountName+"</option>")
			$('#glAccountSetupGeneralProfitAndLostYearAccountID').val(glAccountSetupGeneralProfitAndLostYearAccountID).selectpicker('refresh');
		}

		var glAccountSetupGeneralBankChargersAccountID = $('#glAccountSetupGeneralBankChargersAccountID').data('id');
		var glAccountSetupGeneralBankChargersAccountName = $('#glAccountSetupGeneralBankChargersAccountID').data('value');
		if(glAccountSetupGeneralBankChargersAccountID!=''){
			$('#glAccountSetupGeneralBankChargersAccountID').append("<option value='"+glAccountSetupGeneralBankChargersAccountID+"'>"+glAccountSetupGeneralBankChargersAccountName+"</option>")
			$('#glAccountSetupGeneralBankChargersAccountID').val(glAccountSetupGeneralBankChargersAccountID).selectpicker('refresh');
		}

		var glAccountSetupGeneralDeliveryChargersAccountID = $('#glAccountSetupGeneralDeliveryChargersAccountID').data('id');
		var glAccountSetupGeneralDeliveryChargersAccountName = $('#glAccountSetupGeneralDeliveryChargersAccountID').data('value');
		if(glAccountSetupGeneralDeliveryChargersAccountID!=''){
			$('#glAccountSetupGeneralDeliveryChargersAccountID').append("<option value='"+glAccountSetupGeneralDeliveryChargersAccountID+"'>"+glAccountSetupGeneralDeliveryChargersAccountName+"</option>")
			$('#glAccountSetupGeneralDeliveryChargersAccountID').val(glAccountSetupGeneralDeliveryChargersAccountID).selectpicker('refresh');
		}

		var glAccountSetupGeneralLoyaltyExpenseAccountID = $('#glAccountSetupGeneralLoyaltyExpenseAccountID').data('id');
		var glAccountSetupGeneralLoyaltyExpenseAccountName = $('#glAccountSetupGeneralLoyaltyExpenseAccountID').data('value');
		if(glAccountSetupGeneralLoyaltyExpenseAccountID!=''){
			$('#glAccountSetupGeneralLoyaltyExpenseAccountID').append("<option value='"+glAccountSetupGeneralLoyaltyExpenseAccountID+"'>"+glAccountSetupGeneralLoyaltyExpenseAccountName+"</option>")
			$('#glAccountSetupGeneralLoyaltyExpenseAccountID').val(glAccountSetupGeneralLoyaltyExpenseAccountID).selectpicker('refresh');
		}
	}

	$('#create-gl-account-setup-form').on('submit', function(e) {
		e.preventDefault();
		glAccountsCreate(function() {window.location.reload()});
	});
});

	function valid(formData){
		
		if(formData.glAccountSetupItemDefaultSalesAccountID == '' &&
			formData.glAccountSetupItemDefaultInventoryAccountID == '' &&
			formData.glAccountSetupItemDefaultCOGSAccountID == '' &&
			formData.glAccountSetupItemDefaultAdjusmentAccountID == '' &&
			formData.glAccountSetupSalesAndCustomerReceivableAccountID == '' &&
			formData.glAccountSetupSalesAndCustomerSalesAccountID == '' &&
			formData.glAccountSetupSalesAndCustomerSalesDiscountAccountID == '' &&
			formData.glAccountSetupSalesAndCustomerAdvancePaymentAccountID == '' &&
			formData.glAccountSetupPurchasingAndSupplierPayableAccountID == '' &&
			formData.glAccountSetupPurchasingAndSupplierPurchaseDiscountAccountID == '' &&
			formData.glAccountSetupPurchasingAndSupplierGrnClearingAccountID == '' &&
			formData.glAccountSetupPurchasingAndSupplierAdvancePaymentAccountID == '' &&
			formData.glAccountSetupGeneralExchangeVarianceAccountID == '' &&
			formData.glAccountSetupGeneralProfitAndLostYearAccountID == '' &&
			formData.glAccountSetupGeneralBankChargersAccountID == '' &&
			formData.glAccountSetupGeneralDeliveryChargersAccountID == '' &&
			formData.glAccountSetupGeneralLoyaltyExpenseAccountID == ''  ){
			p_notification(false, eb.getMessage('ERR_PLEASE_SELECT_AT_LEASE_ONE_ACCOUNT'));
			return false;
		}
		return true;
	}

	function updateGlAccFromWizard(callback){
		glAccountsCreate(callback);
	}

	function glAccountsCreate(callback){
		var formData ={
			glAccountSetupItemDefaultSalesAccountID :$('#glAccountSetupItemDefaultSalesAccountID').val(),
			glAccountSetupItemDefaultInventoryAccountID :$('#glAccountSetupItemDefaultInventoryAccountID').val(),
			glAccountSetupItemDefaultCOGSAccountID :$('#glAccountSetupItemDefaultCOGSAccountID').val(),
			glAccountSetupItemDefaultAdjusmentAccountID :$('#glAccountSetupItemDefaultAdjusmentAccountID').val(),
			glAccountSetupSalesAndCustomerReceivableAccountID :$('#glAccountSetupSalesAndCustomerReceivableAccountID').val(),
			glAccountSetupSalesAndCustomerSalesAccountID :$('#glAccountSetupSalesAndCustomerSalesAccountID').val(),
			glAccountSetupSalesAndCustomerSalesDiscountAccountID :$('#glAccountSetupSalesAndCustomerSalesDiscountAccountID').val(),
			glAccountSetupSalesAndCustomerAdvancePaymentAccountID :$('#glAccountSetupSalesAndCustomerAdvancePaymentAccountID').val(),
			glAccountSetupPurchasingAndSupplierPayableAccountID :$('#glAccountSetupPurchasingAndSupplierPayableAccountID').val(),
			glAccountSetupPurchasingAndSupplierPurchaseDiscountAccountID :$('#glAccountSetupPurchasingAndSupplierPurchaseDiscountAccountID').val(),
			glAccountSetupPurchasingAndSupplierGrnClearingAccountID :$('#glAccountSetupPurchasingAndSupplierGrnClearingAccountID').val(),
			glAccountSetupPurchasingAndSupplierAdvancePaymentAccountID :$('#glAccountSetupPurchasingAndSupplierAdvancePaymentAccountID').val(),
			glAccountSetupGeneralExchangeVarianceAccountID :$('#glAccountSetupGeneralExchangeVarianceAccountID').val(),
			glAccountSetupGeneralProfitAndLostYearAccountID :$('#glAccountSetupGeneralProfitAndLostYearAccountID').val(),
			glAccountSetupGeneralBankChargersAccountID :$('#glAccountSetupGeneralBankChargersAccountID').val(),
			glAccountSetupGeneralDeliveryChargersAccountID :$('#glAccountSetupGeneralDeliveryChargersAccountID').val(),
			glAccountSetupGeneralLoyaltyExpenseAccountID :$('#glAccountSetupGeneralLoyaltyExpenseAccountID').val(),
		}
		
		if(valid(formData)){
			eb.ajax({
				type: 'POST',
				url: BASE_URL + '/gl-account-setup-api/saveGlAccountSetup',
				data: formData,
				success: function(res) {
					p_notification(res.status, res.msg);
					if (res.status) {
						callback();
					}
				}
			});
		}
	}