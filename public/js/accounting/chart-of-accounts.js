addFromModal = true;

$(document).ready(function() {

	$('.financeAccountClass').on('click',function(){
		var classID = $(this).parents('tr').attr('id');

		$('tr','.chartOfAccountBody').each(function(){
			var cid = $(this).data('classid')
			
			if(cid == classID && !$(this).hasClass('class_'+classID)){
				$(this).addClass('hidden');		
			}
		});

		if($('.class_'+classID).hasClass('hidden')){
			$('.class_'+classID).removeClass('hidden');
		}else{
			$('.class_'+classID).addClass('hidden');
		}
		
	});

	$('.financeAccountChild').on('click',function(){
		var parentID = $(this).parents('tr').attr('id');
		
		if($(this).parents('tr').data('autotrigger') == true){
			$(this).parents('tr').data('autotrigger',false);
			if($('.'+parentID).hasClass('hidden')){
				$('.'+parentID).removeClass('hidden');	
			}
		}

		if($('.'+parentID).hasClass('hidden')){
			$('.'+parentID).removeClass('hidden');
		}else{
			$('.'+parentID).addClass('hidden').data('autotrigger',true);
			$('.'+parentID).find('.financeAccountChild').trigger('click');
		}
	});

	$('.addFinanceAccount').on('click',function(){
		var classID = $(this).parents('tr').attr('id');
		$('#addFinanceAccountsModal').modal('show');
		$('#financeAccountClassID').val(classID).attr('disabled',true);
		$('#financeAccountClassID').selectpicker('refresh');
		$('#financeAccountsParentID').attr('disabled',false).selectpicker('refresh');
		getParentAccounts(classID);
	});

	$('.collapseAllAccounts').on('click',function(){
		if($(this).is(':checked')){
			$('.collapsAll').removeClass('hidden');
		}else{
			$('.collapsAll').addClass('hidden');
		}
	});
});