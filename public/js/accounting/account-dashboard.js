var colorPalettex = ['#3023AE','#F76B1C','#1298FF', '#FF4560', '#775DD0','#6023ea','#ea2385'];
$(document).ready(function() {
if (!$('.main_body').hasClass('hide-side-bar')) {
    $('.sidbar-toggle-menu').trigger('click');
}
var cashFlow;
var currentPeriod;
var expenseGlobalData = [];
var incomeGlobalData = [];
var currencySymbol;
var chequeDetailsSummaryData;
    currentPeriod = 'thisWeek';
    // getWidgetData('thisYear');

    updateDashboardData('thisWeek');

    function updateDashboardData(period) {
        updateChartData(period, function() {
            getHeaderWidgetData(period, function () {
                updateFooterChartData(period, function () {

                });
            });
        });
    }


    function getHeaderWidgetData (period, callback) {

        eb.ajax({
            type: 'POST',
            url: BASE_URL + '/account-dashboard-api/getHeaderWidgetData',
            data: {period: period},
            success: function(data) {
                currencySymbol = data.companyCurrencySymbol;
                $('#total-sales').each(function () {
                    $(this).prop('Counter',0).animate({
                        Counter: data.totalBankBalance
                    }, {
                        duration: 1000,
                        easing: 'swing',
                        step: function (now) {
                            $(this).text(currencySymbol+' '+accounting.formatMoney(now));
                        }
                    });
                });
                $('#gross-profit').each(function () {
                    $(this).prop('Counter',0).animate({
                        Counter: data.recivedChequeTotal
                    }, {
                        duration: 1000,
                        easing: 'swing',
                        step: function (now) {
                            $(this).text(currencySymbol+' '+accounting.formatMoney(now));
                        }
                    });
                });
                $('#total-payments').each(function () {
                    $(this).prop('Counter',0).animate({
                        Counter: data.settledInvoiceChequesTotal
                    }, {
                        duration: 1000,
                        easing: 'swing',
                        step: function (now) {
                            $(this).text(currencySymbol+' '+accounting.formatMoney(now));
                        }
                    });
                });
                $('#totalInvoiceCount').each(function () {
                    $(this).prop('Counter',0).animate({
                        Counter: data.pettyCashFloatBalance
                    }, {
                        duration: 1000,
                        easing: 'swing',
                        step: function (now) {
                            $(this).text(currencySymbol+' '+accounting.formatMoney(now));
                        }
                    });
                });
            }
        });
        callback();
    }

    function updateChartData (period, callback) {

        eb.ajax({
            type: 'POST',
            url: BASE_URL + '/account-dashboard-api/updateChartData',
            data: {period: period},
            success: function(data) {
                $('#cash-chart').html('');
                currencySymbol = data.companyCurrencySymbol;
                
                cashFlow = data.cashFlow;
                chequeDetailsSummaryData = data.chequeDetailsSummaryData;
                cashCreditChart(data.cashFlow); 
            }
        });
        callback();
    }

    function updateFooterChartData (period, callback) {

        eb.ajax({
            type: 'POST',
            url: BASE_URL + '/account-dashboard-api/updateFooterChartData',
            data: {period: period},
            success: function(data) {
                currencySymbol = data.companyCurrencySymbol;

                var expenseTotal = 0;
                $.each(data.expenses.expenseData, function(index, val) {
                    expenseTotal += parseFloat(val.accountValue);
                });

                var incomeTotal = 0;
                $.each(data.income.incomeData, function(index, val) {
                    incomeTotal += parseFloat(val.accountValue);
                });

                salesSummaryChart(data.expenses, expenseTotal);
                salesAndGpSummaryChart(data.income,incomeTotal);
            }
        });
        callback();
    }

    function getWidgetData(period){
        eb.ajax({
            type: 'POST',
            url: BASE_URL + '/account-dashboard-api/getWidgetData',
            data: {period: period},
            success: function(data) {
                $('#target-chart').html('');
                $('#cash-chart').html('');
                $('#gp-chart').html('');
                $('#total-sales').each(function () {
                    $(this).prop('Counter',0).animate({
                        Counter: data.totalBankBalance
                    }, {
                        duration: 1000,
                        easing: 'swing',
                        step: function (now) {
                            $(this).text('LKR '+accounting.formatMoney(now));
                        }
                    });
                });
                $('#gross-profit').each(function () {
                    $(this).prop('Counter',0).animate({
                        Counter: data.recivedChequeTotal
                    }, {
                        duration: 1000,
                        easing: 'swing',
                        step: function (now) {
                            $(this).text('LKR '+accounting.formatMoney(now));
                        }
                    });
                });
                $('#total-payments').each(function () {
                    $(this).prop('Counter',0).animate({
                        Counter: data.settledInvoiceChequesTotal
                    }, {
                        duration: 1000,
                        easing: 'swing',
                        step: function (now) {
                            $(this).text('LKR '+accounting.formatMoney(now));
                        }
                    });
                });
                $('#totalInvoiceCount').each(function () {
                    $(this).prop('Counter',0).animate({
                        Counter: data.pettyCashFloatBalance
                    }, {
                        duration: 1000,
                        easing: 'swing',
                        step: function (now) {
                            $(this).text('LKR '+accounting.formatMoney(now));
                        }
                    });
                });

                if (data.recivedChequeProfitLoss > 0) {
                    $('#total-gross-profit').removeClass('hidden');
                    $('#total-gross-loss').addClass('hidden');
                    $('#total-gross-profit').html('+'+data.recivedChequeProfitLoss+'%<span><i class="glyphicon glyphicon-arrow-up"></i></span>');
                } else {
                    $('#total-gross-profit').addClass('hidden');
                    $('#total-gross-loss').removeClass('hidden');
                    var loss = data.recivedChequeProfitLoss * -1;
                    $('#total-gross-loss').html('+'+loss+'%<span><i class="glyphicon glyphicon-arrow-down"></i></span>');
                }

                if (data.settledChequeProfitLoss > 0) {
                    $('#total-payment-profit').removeClass('hidden');
                    $('#total-payment-loss').addClass('hidden');
                    $('#total-payment-profit').html('+'+data.settledChequeProfitLoss+'%<span><i class="glyphicon glyphicon-arrow-up"></i></span>');
                } else {
                    $('#total-payment-profit').addClass('hidden');
                    $('#total-payment-loss').removeClass('hidden');
                    var loss = data.settledChequeProfitLoss * -1;
                    $('#total-payment-loss').html('+'+loss+'%<span><i class="glyphicon glyphicon-arrow-down"></i></span>');
                }

                var expenseTotal = 0;
                $.each(data.expenses.expenseData, function(index, val) {
                    expenseTotal += parseFloat(val.accountValue);
                });

                var incomeTotal = 0;
                $.each(data.income.incomeData, function(index, val) {
                    incomeTotal += parseFloat(val.accountValue);
                });

                salesSummaryChart(data.expenses, expenseTotal);
                salesAndGpSummaryChart(data.income,incomeTotal);
             
                cashFlow = data.cashFlow;
                chequeDetailsSummaryData = data.chequeDetailsSummaryData;
                // $('#cash-chart').html('');
                cashCreditChart(data.cashFlow);
                switch (period) {
                    case 'thisYear':
                        $('#last-period-total-gross').text('Compared to (LKR '+accounting.formatMoney(data.lastPeriodRecivedChequeTotal)+' last year)');
                        $('#last-period-total-payment').text('Compared to (LKR '+accounting.formatMoney(data.lastPeriodSettledInvoiceChequesTotal)+' last year)');
                        break;
                    case 'thisMonth':
                        $('#last-period-total-gross').text('Compared to (LKR '+accounting.formatMoney(data.lastPeriodRecivedChequeTotal)+' last month)');
                        $('#last-period-total-payment').text('Compared to (LKR '+accounting.formatMoney(data.lastPeriodSettledInvoiceChequesTotal)+' last month)');
                        break;
                    case 'thisWeek':
                        $('#last-period-total-gross').text('Compared to (LKR '+accounting.formatMoney(data.lastPeriodRecivedChequeTotal)+' last week)');
                        $('#last-period-total-payment').text('Compared to (LKR '+accounting.formatMoney(data.lastPeriodSettledInvoiceChequesTotal)+' last week)');
                        break;
                    case 'thisDay':
                        $('#last-period-total-gross').text('Compared to (LKR '+accounting.formatMoney(data.lastPeriodRecivedChequeTotal)+' last day)');
                        $('#last-period-total-payment').text('Compared to (LKR '+accounting.formatMoney(data.lastPeriodSettledInvoiceChequesTotal)+' last day)');
                        break;
                    default:
                        // statements_def
                        break;
                }
                
            }
        });
    }

    $('.period').on('change', function(event) {
        event.preventDefault();
        currentPeriod = $(this).val();
        // getWidgetData($(this).val());
        updateDashboardData($(this).val());
    });


    function gpChart(endAngle)
    {
        var options1 = {
            chart: {
                height: 200,
                type: 'radialBar',
                toolbar: {
                    show: false
                }
            },
            plotOptions: {
                radialBar: {
                    startAngle: 0,
                    endAngle: parseFloat(endAngle),
                    hollow: {
                        margin: 0,
                        size: '70%',
                        background: '#fff',
                        image: undefined,
                        imageOffsetX: 0,
                        imageOffsetY: 0,
                        position: 'front',
                        dropShadow: {
                            enabled: true,
                            top: 3,
                            left: 0,
                            blur: 4,
                            opacity: 0.24
                        }
                    },
                    track: {
                        background: '#fff',
                        strokeWidth: '70%',
                        margin: 0, // margin is in pixels
                        dropShadow: {
                            enabled: true,
                            top: -3,
                            left: 0,
                            blur: 4,
                            opacity: 0.35
                        }
                    },

                    dataLabels: {
                        showOn: 'always',
                        name: {
                            offsetY: 0,
                            show: true,
                            color: '#888',
                            fontSize: '15px'
                        },
                        value: {
                            formatter: function(val) {
                                return parseInt(val);
                            },
                            color: '#111',
                            fontSize: '20px',
                            show: false,
                        }
                    }
                }
            },
            fill: {
                type: 'gradient',
                gradient: {
                    shade: 'dark',
                    type: 'horizontal',
                    shadeIntensity: 0.5,
                    gradientToColors: ['#ABE5A1'],
                    inverseColors: true,
                    opacityFrom: 1,
                    opacityTo: 1,
                    stops: [0, 100]
                }
            },
            series: [75],
            stroke: {
                lineCap: 'round'
            },
            labels: ['Gross Profit'],
        }
        var chart1 = new ApexCharts(
            document.querySelector("#target-chart"),
            options1
        );
        chart1.render();
    }

    $('#sales-gross-profit').on('click', function(event) {
        event.preventDefault();
        $('#gp-chart').removeClass('hidden');
        $('#cash-chart').addClass('hidden');
        if ($('#gp-chart')[0].children.length > 0) {
            $('#gp-chart')[0].children[0].remove();
        }
        salsVsGpChart(chequeDetailsSummaryData);
    });

    function salsVsGpChart(chequeDetailsSummaryData)
    {
        var recivedCheque = [];
        var issuedCheque = [];
        var period = [];
        $.each(chequeDetailsSummaryData, function(index, val) {
            recivedCheque.push(val.recivedCheque);
            issuedCheque.push(val.issuedCheque);
            period.push(index+' days');
        });

        var options = {
            chart: {
                height: 350,
                type: 'bar',
            },
            plotOptions: {
                bar: {
                    columnWidth: '50%',   
                }
            },
            dataLabels: {
                enabled: false
            },
            stroke: {
                width: 2
            },
            series: [{
                name: 'Recived Cheques',
                data: recivedCheque
            }, {
                name: 'Issued Cheques',
                data: issuedCheque
            }],
            grid: {
                row: {
                    colors: ['#fff', '#f2f2f2']
                }
            },
            xaxis: {
                labels: {
                    rotate: -45
                },
                type: 'category',
                categories: period,
            },
            yaxis: {
                labels: {
                    formatter: (value) => { return currencySymbol+" "+accounting.formatMoney(value) },
                }
            },
            tooltip: {
                x: {
                    format: 'dd/MM/yy HH:mm'
                },
                y: {
                    formatter: function(value, { series, seriesIndex, dataPointIndex, w }) {
                        return currencySymbol+" "+accounting.formatMoney(value)
                    }
                }
            },
            fill: {
                type: 'gradient',
                gradient: {
                    shade: 'light',
                    type: "horizontal",
                    shadeIntensity: 0.25,
                    gradientToColors: undefined,
                    inverseColors: true,
                    opacityFrom: 0.85,
                    opacityTo: 0.85,
                    stops: [50, 0, 100]
                },
            },
        }

        var chart = new ApexCharts(
            document.querySelector("#gp-chart"),
            options
        );

        chart.render();
    }



    $('#cash-credit').on('click', function(event) {
        event.preventDefault();
        $('#cash-chart').removeClass('hidden');
        $('#gp-chart').addClass('hidden');
        if ($('#cash-chart')[0].children.length > 0) {
            $('#cash-chart')[0].children[0].remove();
        }
        $('#gp-chart').html('');
        cashCreditChart(cashFlow);
    });    

    function cashCreditChart(cashFlow)
    {
        $('#cash-chart').html('');
        var cashInFlow = [];
        var cashOutFlow = [];
        var period = [];
        var options = {};
        var i = 0;
        
        $.each(cashFlow.cashInFlow, function(index, val) {
            period.push(index);
            cashInFlow.push(val);
        });

        $.each(cashFlow.cashOutFlow, function(index, val) {
            // period.push(index);
            cashOutFlow.push(val);
        });
        
        if (period.length > 1) {
            options = {
                chart: {
                    height: 350,
                    type: 'area',
                },
                dataLabels: {
                    enabled: false
                },
                stroke: {
                    curve: 'smooth'
                },
                series: [{
                    name: 'Cash Inflow',
                    data: cashInFlow
                }, {
                    name: 'Cash Outflow',
                    data: cashOutFlow
                }],

                xaxis: {
                    type: 'category',
                    categories: period,                
                },
                yaxis: {
                    labels: {
                        formatter: (value) => { return currencySymbol+" "+accounting.formatMoney(value) },
                    }
                },
                tooltip: {
                    y: {
                        formatter: function(value, { series, seriesIndex, dataPointIndex, w }) {
                            return currencySymbol+" "+accounting.formatMoney(value)
                        }
                    }
                }
            }
        } else {
            options = {
                chart: {
                    height: 350,
                    type: 'bar',
                },
                plotOptions: {
                    bar: {
                        columnWidth: '50%',   
                    }
                },
                dataLabels: {
                    enabled: false
                },
                stroke: {
                    width: 2
                },
                series: [{
                    name: 'Cash Inflow',
                    data: cashInFlow
                }, {
                    name: 'Cash Outflow',
                    data: cashOutFlow
                }],
                grid: {
                    row: {
                        colors: ['#fff', '#f2f2f2']
                    }
                },
                xaxis: {
                    labels: {
                        rotate: -45
                    },
                    categories: period,
                },
                yaxis: {
                    labels: {
                        formatter: (value) => { return currencySymbol+" "+accounting.formatMoney(value) },
                    }
                },
                tooltip: {
                    y: {
                        formatter: function(value, { series, seriesIndex, dataPointIndex, w }) {
                            return currencySymbol+" "+accounting.formatMoney(value)
                        }
                    }
                },
                fill: {
                    type: 'gradient',
                    gradient: {
                        shade: 'light',
                        type: "horizontal",
                        shadeIntensity: 0.25,
                        gradientToColors: undefined,
                        inverseColors: true,
                        opacityFrom: 0.85,
                        opacityTo: 0.85,
                        stops: [50, 0, 100]
                    },
                },
            }
        }

        

        



        renderChart(options, 'cash-chart');
    }


    function renderChart(options, chartID)
    {
        var chart = new ApexCharts(
            document.querySelector("#"+chartID),
            options
        );
        chart.render();
    }


    function salesAndGpSummaryChart(income, incomeTotal)
    {
        let chartConfig = {
            graphset : [

            ]
        };

        var gdata = {
            x : 0*200,
            y : 0*200,
            width : '100%',
            height : '100%',
            type : 'pie',
            "legend":{ 
                "max-items":3,
                "overflow":"page",
                'background-color': "white",
                borderColor : 'white',
                borderRadius : '5px',
                shadow : true,
                shadowAlpha : 1,
                shadowAngle : -45,
                borderAlpha : 0.5,
                'border-width': 1,
                "draggable":true,
                "drag-handler":"icon", //"header" (default) or "icon"
                "icon":{
                  "line-color":"red"
                },
            },
            noData:{
                text:"Currently there is no data in the chart",
                backgroundColor: "#5081e7",
                fontSize:18,
                textAlpha:.9,
                alpha:.6,
                bold:true
            },
            plotarea : {
                margin : '15 5 5 5'
            },
            "scale":{
                "size-factor":0.9
            },
            labels:[
                {
                    text:currencySymbol+" "+accounting.formatMoney(incomeTotal),
                    x:"50%", 
                    y:"47%",
                    anchor : 'c',
                    fontSize:"17px",
                    fontStyle : 'bold',
                    fontColor : 'black',
                    padding : "10px",
                },
                {
                    text:"Total <br> Income",
                    x:"50%", 
                    y:"58%",
                    fontSize:"13px",
                    anchor : 'c',
                    fontStyle : 'bold',
                    fontColor : 'grey',
                    padding : "10px",
                },
            ],
            plot : {
                borderWidth : 2,
                borderColor : 'white',
                shadow : true,
                shadowAlpha : 1,
                shadowAngle : -45,
                borderAlpha : 0.5,
                slice : '89%',
                pieTransform : 'flow=4',
                hoverState : {
                    visible : false    
                },
                tooltip : {
                    text : '%t <br> '+currencySymbol+' %node-value',
                    thousandsSeparator:','
                },
                'background-color': "black",
                'value-box': {
                    text: currencySymbol+" %v",
                    'font-color': "black",
                    'font-size': 10,
                    'background-color': "white",
                    borderColor : 'white',
                    shadow : true,
                    shadowAlpha : 1,
                    shadowAngle : -45,
                    borderAlpha : 0.5,
                    'border-width': 1,
                    padding: "5%",
                    borderRadius : '5px',
                    placement:'out',
                    thousandsSeparator:',',
                }
            },
            series:[
            ]
        };

        var i = 0;
        $.each(income.incomeData, function(index, val) {
            var series = {
                "values":[parseFloat(val.accountValue)],  
                "text":val.className,  
                "background-color":colorPalettex[i]
            }
            gdata.series.push(series);
            incomeGlobalData.push(series);
            i++;
        });



        chartConfig.graphset.push(gdata);

        setTimeout(function(){ 
            zingchart.render({
                id: 'sales-gp-summary',
                width: '100%',
                height: '100%',
                data: chartConfig
            });

            if (incomeTotal == 0) {
                $('#sales-gp-summary .zc-text').remove();
            }
        }, 500);

    }
    
    zingchart.node_click = function(e) {
        if (e.id == "sales-summary") {
            $("#sales-summary").hide(1000);
            getExpenseAccounts(expenseGlobalData[e.plotindex].text);
            $("#expense-home").removeClass('hidden');
            $("#expense-detail").show(1000);  
        } else if (e.id == "sales-gp-summary") {
            $("#sales-gp-summary").hide(1000);  
            getIncomeAccounts(incomeGlobalData[e.plotindex].text);
            $("#income-home").removeClass('hidden');
            $("#income-detail").show(1000);  
        }
    }

    function getIncomeAccounts(acClassName)
    {
        eb.ajax({
            type: 'POST',
            url: BASE_URL + '/account-dashboard-api/getDetailIncome',
            data: {acClassName: acClassName, period: currentPeriod},
            success: function(data) {
                var incomeAcTotal = 0;
                $.each(data.incomeData, function(index, val) {
                    incomeAcTotal += val.accountValue;
                });
                $("#income-detail").html('');
                incomeDetailChart(data.incomeData, incomeAcTotal, acClassName);
            }
        });
    }

    function salesSummaryChart(expenses, expenseTotal)
    {
        let chartConfig = {
            graphset : [

            ]
        };

        var gdata = {
            x : 0*200,
            y : 0*200,
            width : '100%',
            height : '100%',
            type : 'pie',
            "legend":{ 
                "max-items":3,
                "overflow":"page",
                'background-color': "white",
                borderColor : 'white',
                borderRadius : '5px',
                shadow : true,
                shadowAlpha : 1,
                shadowAngle : -45,
                borderAlpha : 0.5,
                'border-width': 1,
                "draggable":true,
                "drag-handler":"icon", //"header" (default) or "icon"
                "icon":{
                  "line-color":"red"
                },
            },
            noData:{
                text:"Currently there is no data in the chart",
                backgroundColor: "#5081e7",
                fontSize:18,
                textAlpha:.9,
                alpha:.6,
                bold:true
            },
            plotarea : {
                margin : '15 5 5 5'
            },
            "scale":{
                "size-factor":0.9
            },
            labels:[
                {
                    text:currencySymbol+" "+accounting.formatMoney(expenseTotal),
                    x:"50%", 
                    y:"47%",
                    anchor : 'c',
                    fontSize:"17px",
                    fontStyle : 'bold',
                    fontColor : 'black',
                    padding : "10px",
                },
                {
                    text:"Total <br> Expenses",
                    x:"50%", 
                    y:"58%",
                    fontSize:"13px",
                    anchor : 'c',
                    fontStyle : 'bold',
                    fontColor : 'grey',
                    padding : "10px",
                },
            ],
            plot : {
                borderWidth : 2,
                borderColor : 'white',
                shadow : true,
                shadowAlpha : 1,
                shadowAngle : -45,
                borderAlpha : 0.5,
                slice : '89%',
                pieTransform : 'flow=4',
                hoverState : {
                    visible : false    
                },
                tooltip : {
                    text : '%t <br> '+currencySymbol+' %node-value',
                    thousandsSeparator:','
                },
                'background-color': "black",
                'value-box': {
                    text: currencySymbol+" %v",
                    'font-color': "black",
                    'font-size': 10,
                    'background-color': "white",
                    borderColor : 'white',
                    borderRadius : '5px',
                    shadow : true,
                    shadowAlpha : 1,
                    shadowAngle : -45,
                    borderAlpha : 0.5,
                    'border-width': 1,
                    padding: "5%",
                    placement:'out',
                    thousandsSeparator:',',
                }
            },
            series:[  
            ]
        };

        var i = 0;
        $.each(expenses.expenseData, function(index, val) {
            var series = {
                "values":[parseFloat(val.accountValue)],  
                "text":val.className,  
                "background-color":colorPalettex[i]
            }
            gdata.series.push(series);
            expenseGlobalData.push(series);
            i++;
        });

        chartConfig.graphset.push(gdata);

        zingchart.render({
            id: 'sales-summary',
            width: '100%',
            height: '100%',
            data: chartConfig
        });



        if (expenseTotal == 0) {
            $('#sales-summary .zc-text').remove();
        }
    }

    $('#expense-home').on('click', function(event) {
        event.preventDefault();
        $(this).addClass('hidden');
        $("#expense-detail").hide(1000);

        setTimeout(function(){ 
            $("#sales-summary").show(1000);
        }, 500);
    });

     $('#income-home').on('click', function(event) {
        event.preventDefault();
        $(this).addClass('hidden');
        $("#income-detail").hide(1000);

        setTimeout(function(){ 
            $("#sales-gp-summary").show(1000);
        }, 500);
    });

    function getExpenseAccounts(acClassName)
    {
        eb.ajax({
            type: 'POST',
            url: BASE_URL + '/account-dashboard-api/getDetailExpense',
            data: {acClassName: acClassName, period: currentPeriod},
            success: function(data) {
                var expenseAcTotal = 0;
                $.each(data.expenseData, function(index, val) {
                    expenseAcTotal += val.accountValue;
                });
                $("#expense-detail").html('');
                expenseDetailChart(data.expenseData, expenseAcTotal, acClassName);
            }
        });
    }

    function expenseDetailChart(expenses, expenseTotal, acClassName)
    {
        let chartConfig = {
            graphset : [

            ]
        };

        var gdata = {
            x : 0*200,
            y : 0*200,
            width : '100%',
            height : '100%',
            type : 'pie',
            "legend":{ 
                "max-items":3,
                "overflow":"page",
                'background-color': "white",
                borderColor : 'white',
                borderRadius : '5px',
                shadow : true,
                shadowAlpha : 1,
                shadowAngle : -45,
                borderAlpha : 0.5,
                'border-width': 1,
                "draggable":true,
                "drag-handler":"icon", //"header" (default) or "icon"
                "icon":{
                  "line-color":"red"
                },
            },
            noData:{
                text:"Currently there is no data in the chart",
                backgroundColor: "#5081e7",
                fontSize:18,
                textAlpha:.9,
                alpha:.6,
                bold:true
            },
            plotarea : {
                margin : '15 5 5 5'
            },
            "scale":{
                "size-factor":0.9
            },
            // labels:[
            //     {
            //         text:"Rs "+accounting.formatMoney(expenseTotal),
            //         x:"50%", 
            //         y:"47%",
            //         anchor : 'c',
            //         fontSize:"17px",
            //         fontStyle : 'bold',
            //         fontColor : 'black',
            //         padding : "10px",
            //     },
            //     {
            //         text:"Total <br> Of "+acClassName,
            //         x:"50%", 
            //         y:"58%",
            //         fontSize:"10px",
            //         anchor : 'c',
            //         fontStyle : 'bold',
            //         fontColor : 'grey',
            //         padding : "10px",
            //     },
            // ],
            plot : {
                borderWidth : 2,
                borderColor : 'white',
                shadow : true,
                shadowAlpha : 1,
                shadowAngle : -45,
                borderAlpha : 0.5,
                slice : '89%',
                pieTransform : 'flow=4',
                hoverState : {
                    visible : false    
                },
                tooltip : {
                    text : '%t <br> Rs %node-value',
                    thousandsSeparator:','
                },
                'background-color': "black",
                'value-box': {
                    text: "Rs %v",
                    'font-color': "black",
                    'font-size': 10,
                    'background-color': "white",
                    borderColor : 'white',
                    borderRadius : '5px',
                    shadow : true,
                    shadowAlpha : 1,
                    shadowAngle : -45,
                    borderAlpha : 0.5,
                    'border-width': 1,
                    padding: "5%",
                    placement:'out',
                    thousandsSeparator:',',
                }
            },
            series:[  
            ]
        };

        var i = 0;
        $.each(expenses, function(index, val) {
            var series = {
                "values":[parseFloat(val.accountValue)],  
                "text":val.accountName,  
                "background-color":colorPalettex[i]
            }
            gdata.series.push(series);
            i++;
        });

        chartConfig.graphset.push(gdata);

        setTimeout(function(){ 
            zingchart.render({
                id: 'expense-detail',
                width: '100%',
                height: '100%',
                data: chartConfig
            });
            
            if (expenseTotal == 0) {
                $('#expense-detail .zc-text').remove();
            }
        }, 500);


    }


    function incomeDetailChart(income, incomeTotal, acClassName)
    {
        let chartConfig = {
            graphset : [

            ]
        };

        var gdata = {
            x : 0*200,
            y : 0*200,
            width : '100%',
            height : '100%',
            type : 'pie',
            "legend":{ 
                "max-items":3,
                "overflow":"page",
                'background-color': "white",
                borderColor : 'white',
                borderRadius : '5px',
                shadow : true,
                shadowAlpha : 1,
                shadowAngle : -45,
                borderAlpha : 0.5,
                'border-width': 1,
                "draggable":true,
                "drag-handler":"icon", //"header" (default) or "icon"
                "icon":{
                  "line-color":"red"
                },
            },
            noData:{
                text:"Currently there is no data in the chart",
                backgroundColor: "#5081e7",
                fontSize:18,
                textAlpha:.9,
                alpha:.6,
                bold:true
            },
            plotarea : {
                margin : '15 5 5 5'
            },
            "scale":{
                "size-factor":0.9
            },
            // labels:[
            //     {
            //         text:"Rs "+accounting.formatMoney(incomeTotal),
            //         x:"50%", 
            //         y:"47%",
            //         anchor : 'c',
            //         fontSize:"17px",
            //         fontStyle : 'bold',
            //         fontColor : 'black',
            //         padding : "10px",
            //     },
            //     {
            //         text:"Total <br> Of "+acClassName,
            //         x:"50%", 
            //         y:"58%",
            //         fontSize:"10px",
            //         anchor : 'c',
            //         fontStyle : 'bold',
            //         fontColor : 'grey',
            //         padding : "10px",
            //     },
            // ],
            plot : {
                borderWidth : 2,
                borderColor : 'white',
                shadow : true,
                shadowAlpha : 1,
                shadowAngle : -45,
                borderAlpha : 0.5,
                slice : '89%',
                pieTransform : 'flow=4',
                hoverState : {
                    visible : false    
                },
                tooltip : {
                    text : '%t <br> Rs %node-value',
                    thousandsSeparator:','
                },
                'background-color': "black",
                'value-box': {
                    text: "Rs %v",
                    'font-color': "black",
                    'font-size': 10,
                    'background-color': "white",
                    borderColor : 'white',
                    borderRadius : '5px',
                    shadow : true,
                    shadowAlpha : 1,
                    shadowAngle : -45,
                    borderAlpha : 0.5,
                    'border-width': 1,
                    padding: "5%",
                    placement:'out',
                    thousandsSeparator:',',
                }
            },
            series:[  
            ]
        };

        var i = 0;
        $.each(income, function(index, val) {
            var series = {
                "values":[parseFloat(val.accountValue)],  
                "text":val.accountName,  
                "background-color":colorPalettex[i]
            }
            gdata.series.push(series);
            i++;
        });

        chartConfig.graphset.push(gdata);

        setTimeout(function(){ 
            zingchart.render({
                id: 'income-detail',
                width: '100%',
                height: '100%',
                data: chartConfig
            });
            
            if (incomeTotal == 0) {
                $('#income-detail .zc-text').remove();
            }
        }, 500);


    }
});


