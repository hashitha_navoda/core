$(document).ready(function() {
	var accountsID = '';

	loadDropDownFromDatabase('/accounts-api/search-accounts-for-dropdown', "", false, '#accounts-search');
    $('#accounts-search').selectpicker('refresh');
    $('#accounts-search').on('change',function(){
    	if ($(this).val() > 0 && accountsID != $(this).val()) {
    		accountsID = $(this).val();
    		if ($('#accounts-search').val() == '') {
    			$('#dynamicAccounts').html('');
    			$('#dynamicAccounts').hide();
    			$('#accounts-list').show();
    		} else {
    			var searchurl = BASE_URL + '/accounts-api/getAccountsFromSearch';
    			var searchrequest = eb.post(searchurl, {accountsID: accountsID});
    			searchrequest.done(function(searchdata) {
    				$('#accounts-list').hide();
    				$('#dynamicAccounts').show();
    				$('#dynamicAccounts').html(searchdata);
    			});
    		}
    	}
    });
});

function deleteAccounts(financeAccountsID){
	bootbox.confirm("Are you sure you want to Delete this Account", function(result) {
        if (result == true) {
            eb.ajax({
                type: 'POST',
                url: BASE_URL + '/accounts-api/deleteAccount',
                data: {
                    financeAccountsID: financeAccountsID,
                },
                success: function(respond) {
                    p_notification(respond.status, respond.msg);
                    if(respond.status){
	                    window.setTimeout(function() {
	                        location.reload();
	                    }, 1000);
                    }
                },
                async: false
            });
        }
    });
}