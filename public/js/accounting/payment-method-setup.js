$(document).ready(function() {
	loadDropDownFromDatabase('/accounts-api/search-active-accounts-for-dropdown', "", "", '#salesCashFinanceAccountID');
	loadDropDownFromDatabase('/accounts-api/search-active-accounts-for-dropdown', "", "", '#salesLcFinanceAccountID');
	loadDropDownFromDatabase('/accounts-api/search-active-accounts-for-dropdown', "", "", '#salesTtFinanceAccountID');
	loadDropDownFromDatabase('/accounts-api/search-active-accounts-for-dropdown', "", "", '#salesCardFinanceAccountID');
	loadDropDownFromDatabase('/accounts-api/search-active-accounts-for-dropdown', "", "", '#salesGiftCardFinanceAccountID');
	loadDropDownFromDatabase('/accounts-api/search-active-accounts-for-dropdown', "", "", '#salesChequeFinanceAccountID');
	loadDropDownFromDatabase('/accounts-api/search-active-accounts-for-dropdown', "", "", '#salesLoyaltyCardFinanceAccountID');
	loadDropDownFromDatabase('/accounts-api/search-active-accounts-for-dropdown', "", "", '#salesBankTransferFinanceAccountID');
    loadDropDownFromDatabase('/accounts-api/search-active-accounts-for-dropdown', "", "", '#salesUniformVoucherFinanceAccountID');
	loadDropDownFromDatabase('/accounts-api/search-active-accounts-for-dropdown', "", "", '#purchaseCashFinanceAccountID');
	loadDropDownFromDatabase('/accounts-api/search-active-accounts-for-dropdown', "", "", '#purchaseLcFinanceAccountID');
	loadDropDownFromDatabase('/accounts-api/search-active-accounts-for-dropdown', "", "", '#purchaseTtFinanceAccountID');
	loadDropDownFromDatabase('/accounts-api/search-active-accounts-for-dropdown', "", "", '#purchaseCardFinanceAccountID');
	loadDropDownFromDatabase('/accounts-api/search-active-accounts-for-dropdown', "", "", '#purchaseGiftCardFinanceAccountID');
	loadDropDownFromDatabase('/accounts-api/search-active-accounts-for-dropdown', "", "", '#purchaseChequeFinanceAccountID');
	loadDropDownFromDatabase('/accounts-api/search-active-accounts-for-dropdown', "", "", '#purchaseLoyaltyCardFinanceAccountID');
	loadDropDownFromDatabase('/accounts-api/search-active-accounts-for-dropdown', "", "", '#purchaseBankTransferFinanceAccountID');
    loadDropDownFromDatabase('/accounts-api/search-active-accounts-for-dropdown', "", "", '#purchaseUniformVoucherFinanceAccountID');

	if($('#salesCashFinanceAccountID').data('checked') == 1){
		$('#salesCashMethod').attr('checked',true);
	}
	var salesCashFinanceAccountID = $('#salesCashFinanceAccountID').data('id');
    if(salesCashFinanceAccountID!=''){
    	var salesCashFinanceAccountName = $('#salesCashFinanceAccountID').data('value');
    	$('#salesCashFinanceAccountID').append("<option value='"+salesCashFinanceAccountID+"'>"+salesCashFinanceAccountName+"</option>")
    	$('#salesCashFinanceAccountID').val(salesCashFinanceAccountID).selectpicker('refresh');
    }

    if($('#salesLcFinanceAccountID').data('checked') == 1){
    	$('#salesLcMethod').attr('checked',true);
    }
    var salesLcFinanceAccountID = $('#salesLcFinanceAccountID').data('id');
    if(salesLcFinanceAccountID!=''){
    	var salesLcFinanceAccountName = $('#salesLcFinanceAccountID').data('value');
    	$('#salesLcFinanceAccountID').append("<option value='"+salesLcFinanceAccountID+"'>"+salesLcFinanceAccountName+"</option>")
    	$('#salesLcFinanceAccountID').val(salesLcFinanceAccountID).selectpicker('refresh');
    }

    if($('#salesUniformVoucherFinanceAccountID').data('checked') == 1){
        $('#salesUniformVoucherMethod').attr('checked',true);
    }
    var salesUniformVoucherFinanceAccountID = $('#salesUniformVoucherFinanceAccountID').data('id');
    if(salesUniformVoucherFinanceAccountID!=''){
        var salesUniformVoucherFinanceAccountName = $('#salesUniformVoucherFinanceAccountID').data('value');
        $('#salesUniformVoucherFinanceAccountID').append("<option value='"+salesUniformVoucherFinanceAccountID+"'>"+salesUniformVoucherFinanceAccountName+"</option>")
        $('#salesUniformVoucherFinanceAccountID').val(salesUniformVoucherFinanceAccountID).selectpicker('refresh');
    }


    if($('#salesTtFinanceAccountID').data('checked') == 1){
    	$('#salesTtMethod').attr('checked',true);
    }
    var salesTtFinanceAccountID = $('#salesTtFinanceAccountID').data('id');
    if(salesTtFinanceAccountID!=''){
    	var salesTtFinanceAccountName = $('#salesTtFinanceAccountID').data('value');
    	$('#salesTtFinanceAccountID').append("<option value='"+salesTtFinanceAccountID+"'>"+salesTtFinanceAccountName+"</option>")
    	$('#salesTtFinanceAccountID').val(salesTtFinanceAccountID).selectpicker('refresh');
    }

    if($('#salesCardFinanceAccountID').data('checked') == 1){
    	$('#salesCardMethod').attr('checked',true);
    }
    var salesCardFinanceAccountID = $('#salesCardFinanceAccountID').data('id');
    if(salesCardFinanceAccountID!=''){
    	var salesCardFinanceAccountName = $('#salesCardFinanceAccountID').data('value');
    	$('#salesCardFinanceAccountID').append("<option value='"+salesCardFinanceAccountID+"'>"+salesCardFinanceAccountName+"</option>")
    	$('#salesCardFinanceAccountID').val(salesCardFinanceAccountID).selectpicker('refresh');
    }

    if($('#salesGiftCardFinanceAccountID').data('checked') == 1){
    	$('#salesGiftCardMethod').attr('checked',true);
    }
    var salesGiftCardFinanceAccountID = $('#salesGiftCardFinanceAccountID').data('id');
    if(salesGiftCardFinanceAccountID!=''){
    	var salesGiftCardFinanceAccountName = $('#salesGiftCardFinanceAccountID').data('value');
    	$('#salesGiftCardFinanceAccountID').append("<option value='"+salesGiftCardFinanceAccountID+"'>"+salesGiftCardFinanceAccountName+"</option>")
    	$('#salesGiftCardFinanceAccountID').val(salesGiftCardFinanceAccountID).selectpicker('refresh');
    }

    if($('#salesChequeFinanceAccountID').data('checked') == 1){
    	$('#salesChequeMethod').attr('checked',true);
    }
    var salesChequeFinanceAccountID = $('#salesChequeFinanceAccountID').data('id');
    if(salesChequeFinanceAccountID!=''){
    	var salesChequeFinanceAccountName = $('#salesChequeFinanceAccountID').data('value');
    	$('#salesChequeFinanceAccountID').append("<option value='"+salesChequeFinanceAccountID+"'>"+salesChequeFinanceAccountName+"</option>")
    	$('#salesChequeFinanceAccountID').val(salesChequeFinanceAccountID).selectpicker('refresh');
    }

    if($('#salesLoyaltyCardFinanceAccountID').data('checked') == 1){
    	$('#salesLoyaltyCardMethod').attr('checked',true);
    }
    var salesLoyaltyCardFinanceAccountID = $('#salesLoyaltyCardFinanceAccountID').data('id');
    if(salesLoyaltyCardFinanceAccountID!=''){
    	var salesLoyaltyCardFinanceAccountName = $('#salesLoyaltyCardFinanceAccountID').data('value');
    	$('#salesLoyaltyCardFinanceAccountID').append("<option value='"+salesLoyaltyCardFinanceAccountID+"'>"+salesLoyaltyCardFinanceAccountName+"</option>")
    	$('#salesLoyaltyCardFinanceAccountID').val(salesLoyaltyCardFinanceAccountID).selectpicker('refresh');
    }

    if($('#salesBankTransferFinanceAccountID').data('checked') == 1){
    	$('#salesBankTransferMethod').attr('checked',true);
    }
    var salesBankTransferFinanceAccountID = $('#salesBankTransferFinanceAccountID').data('id');
    if(salesBankTransferFinanceAccountID!=''){
    	var salesBankTransferFinanceAccountName = $('#salesBankTransferFinanceAccountID').data('value');
    	$('#salesBankTransferFinanceAccountID').append("<option value='"+salesBankTransferFinanceAccountID+"'>"+salesBankTransferFinanceAccountName+"</option>")
    	$('#salesBankTransferFinanceAccountID').val(salesBankTransferFinanceAccountID).selectpicker('refresh');
    }

    if($('#purchaseCashFinanceAccountID').data('checked') == 1){
		$('#purchaseCashMethod').attr('checked',true);
	}
	var purchaseCashFinanceAccountID = $('#purchaseCashFinanceAccountID').data('id');
    if(purchaseCashFinanceAccountID!=''){
    	var purchaseCashFinanceAccountName = $('#purchaseCashFinanceAccountID').data('value');
    	$('#purchaseCashFinanceAccountID').append("<option value='"+purchaseCashFinanceAccountID+"'>"+purchaseCashFinanceAccountName+"</option>")
    	$('#purchaseCashFinanceAccountID').val(purchaseCashFinanceAccountID).selectpicker('refresh');
    }

    if($('#purchaseLcFinanceAccountID').data('checked') == 1){
    	$('#purchaseLcMethod').attr('checked',true);
    }
    var purchaseLcFinanceAccountID = $('#purchaseLcFinanceAccountID').data('id');
    if(purchaseLcFinanceAccountID!=''){
    	var purchaseLcFinanceAccountName = $('#purchaseLcFinanceAccountID').data('value');
    	$('#purchaseLcFinanceAccountID').append("<option value='"+purchaseLcFinanceAccountID+"'>"+purchaseLcFinanceAccountName+"</option>")
    	$('#purchaseLcFinanceAccountID').val(purchaseLcFinanceAccountID).selectpicker('refresh');
    }

    if($('#purchaseTtFinanceAccountID').data('checked') == 1){
    	$('#purchaseTtMethod').attr('checked',true);
    }
    var purchaseTtFinanceAccountID = $('#purchaseTtFinanceAccountID').data('id');
    if(purchaseTtFinanceAccountID!=''){
    	var purchaseTtFinanceAccountName = $('#purchaseTtFinanceAccountID').data('value');
    	$('#purchaseTtFinanceAccountID').append("<option value='"+purchaseTtFinanceAccountID+"'>"+purchaseTtFinanceAccountName+"</option>")
    	$('#purchaseTtFinanceAccountID').val(purchaseTtFinanceAccountID).selectpicker('refresh');
    }

    if($('#purchaseCardFinanceAccountID').data('checked') == 1){
    	$('#purchaseCardMethod').attr('checked',true);
    }
    var purchaseCardFinanceAccountID = $('#purchaseCardFinanceAccountID').data('id');
    if(purchaseCardFinanceAccountID!=''){
    	var purchaseCardFinanceAccountName = $('#purchaseCardFinanceAccountID').data('value');
    	$('#purchaseCardFinanceAccountID').append("<option value='"+purchaseCardFinanceAccountID+"'>"+purchaseCardFinanceAccountName+"</option>")
    	$('#purchaseCardFinanceAccountID').val(purchaseCardFinanceAccountID).selectpicker('refresh');
    }

    if($('#purchaseGiftCardFinanceAccountID').data('checked') == 1){
    	$('#purchaseGiftCardMethod').attr('checked',true);
    }
    var purchaseGiftCardFinanceAccountID = $('#purchaseGiftCardFinanceAccountID').data('id');
    if(purchaseGiftCardFinanceAccountID!=''){
    	var purchaseGiftCardFinanceAccountName = $('#purchaseGiftCardFinanceAccountID').data('value');
    	$('#purchaseGiftCardFinanceAccountID').append("<option value='"+purchaseGiftCardFinanceAccountID+"'>"+purchaseGiftCardFinanceAccountName+"</option>")
    	$('#purchaseGiftCardFinanceAccountID').val(purchaseGiftCardFinanceAccountID).selectpicker('refresh');
    }

    if($('#purchaseChequeFinanceAccountID').data('checked') == 1){
    	$('#purchaseChequeMethod').attr('checked',true);
    }
    var purchaseChequeFinanceAccountID = $('#purchaseChequeFinanceAccountID').data('id');
    if(purchaseChequeFinanceAccountID!=''){
    	var purchaseChequeFinanceAccountName = $('#purchaseChequeFinanceAccountID').data('value');
    	$('#purchaseChequeFinanceAccountID').append("<option value='"+purchaseChequeFinanceAccountID+"'>"+purchaseChequeFinanceAccountName+"</option>")
    	$('#purchaseChequeFinanceAccountID').val(purchaseChequeFinanceAccountID).selectpicker('refresh');
    }

    if($('#purchaseLoyaltyCardFinanceAccountID').data('checked') == 1){
    	$('#purchaseLoyaltyCardMethod').attr('checked',true);
    }
    var purchaseLoyaltyCardFinanceAccountID = $('#purchaseLoyaltyCardFinanceAccountID').data('id');
    if(purchaseLoyaltyCardFinanceAccountID!=''){
    	var purchaseLoyaltyCardFinanceAccountName = $('#purchaseLoyaltyCardFinanceAccountID').data('value');
    	$('#purchaseLoyaltyCardFinanceAccountID').append("<option value='"+purchaseLoyaltyCardFinanceAccountID+"'>"+purchaseLoyaltyCardFinanceAccountName+"</option>")
    	$('#purchaseLoyaltyCardFinanceAccountID').val(purchaseLoyaltyCardFinanceAccountID).selectpicker('refresh');
    }

    if($('#purchaseBankTransferFinanceAccountID').data('checked') == 1){
    	$('#purchaseBankTransferMethod').attr('checked',true);
    }
    var purchaseBankTransferFinanceAccountID = $('#purchaseBankTransferFinanceAccountID').data('id');
    if(purchaseBankTransferFinanceAccountID!=''){
    	var purchaseBankTransferFinanceAccountName = $('#purchaseBankTransferFinanceAccountID').data('value');
    	$('#purchaseBankTransferFinanceAccountID').append("<option value='"+purchaseBankTransferFinanceAccountID+"'>"+purchaseBankTransferFinanceAccountName+"</option>")
    	$('#purchaseBankTransferFinanceAccountID').val(purchaseBankTransferFinanceAccountID).selectpicker('refresh');
    }


    if($('#purchaseUniformVoucherFinanceAccountID').data('checked') == 1){
        $('#purchaseUniformVoucherMethod').attr('checked',true);
    }
    var purchaseUniformVoucherFinanceAccountID = $('#purchaseUniformVoucherFinanceAccountID').data('id');
    if(purchaseUniformVoucherFinanceAccountID!=''){
        var purchaseUniformVoucherFinanceAccountName = $('#purchaseUniformVoucherFinanceAccountID').data('value');
        $('#purchaseUniformVoucherFinanceAccountID').append("<option value='"+purchaseUniformVoucherFinanceAccountID+"'>"+purchaseUniformVoucherFinanceAccountName+"</option>")
        $('#purchaseUniformVoucherFinanceAccountID').val(purchaseUniformVoucherFinanceAccountID).selectpicker('refresh');
    }


    $('tr','.paymentMethodTable > tbody').each(function(){
    	if(!$(this).find('.salesCheck').is(':checked')){
    		$(this).find('.salesAccountID').attr('disabled',true).selectpicker('refresh');
    	}
    	if(!$(this).find('.purchaseCheck').is(':checked')){
    		$(this).find('.purchaseAccountID').attr('disabled',true).selectpicker('refresh');
    	}
    });

    $('.salesCheck').on('click',function(){
    	if($(this).is(':checked')){
    		$(this).parents('tr').find('.salesAccountID').attr('disabled',false).selectpicker('refresh');
    	}else{
    		$(this).parents('tr').find('.salesAccountID').attr('disabled',true).selectpicker('refresh');
    	}
    });

    $('.purchaseCheck').on('click',function(){
    	if($(this).is(':checked')){
    		$(this).parents('tr').find('.purchaseAccountID').attr('disabled',false).selectpicker('refresh');
    	}else{
    		$(this).parents('tr').find('.purchaseAccountID').attr('disabled',true).selectpicker('refresh');
    	}
    });

	$('#create-payment-method-setup-form').on('submit',function(e){
		e.preventDefault();
		var form_data = []
		$('tr','.paymentMethodTable > tbody').each(function(){
			var method_data ={
				salesCheck : $(this).find('.salesCheck').is(":checked"),
				salesAccountID : $(this).find('.salesAccountID').val(),
				purchaseCheck : $(this).find('.purchaseCheck').is(":checked"),
				purchaseAccountID : $(this).find('.purchaseAccountID').val(),
				paymentMethodID : $(this).find('.salesAccountID').data('methodid'),
			}
			form_data.push(method_data);
		});
        eb.ajax({
            url: BASE_URL + '/gl-account-setup-api/updatePaymentMethodSetup',
            method: 'post',
            data: {methodData: form_data},
            success: function(respond) {
                p_notification(respond.status, respond.msg);
            }
        });

	});
});