$(document).ready(function() {
	var accountClassID = '';

	 loadDropDownFromDatabase('/account-classes-api/search-account-classes-for-dropdown', "", 0, '#account-class-search');
    $('#account-class-search').selectpicker('refresh');
    $('#account-class-search').on('change',function(){
    	if ($(this).val() > 0 && accountClassID != $(this).val()) {
    		accountClassID = $(this).val();
    		if ($('#account-class-search').val() == '') {
    			$('#dynamicAccountClass').html('');
    			$('#dynamicAccountClass').hide('');
    			$('#account-class-list').show();
    		} else {
    			var searchurl = BASE_URL + '/account-classes-api/getAccountClassFromSearch';
    			var searchrequest = eb.post(searchurl, {accountClassID: accountClassID});
    			searchrequest.done(function(searchdata) {
    				$('#account-class-list').hide();
    				$('#dynamicAccountClass').show();
    				$('#dynamicAccountClass').html(searchdata);
    			});
    		}
    	}
    });
});

function deleteAccountClass(financeAccountClassID){
	bootbox.confirm("Are you sure you want to Delete this Account Class", function(result) {
        if (result == true) {
            eb.ajax({
                type: 'POST',
                url: BASE_URL + '/account-classes-api/deleteAccountClass',
                data: {
                    financeAccountClassID: financeAccountClassID,
                },
                success: function(respond) {
                    p_notification(respond.status, respond.msg);
                    if(respond.status){
	                    window.setTimeout(function() {
	                        location.reload();
	                    }, 1000);
                    }
                },
                async: false
            });
        }
    });
}