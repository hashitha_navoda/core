$(document).ready(function() {

	$('#create-finance-groups-form').on('submit', function(e) {
		e.preventDefault();
		saveGroups();
	});

	function saveGroups(){
		var financeGroupsID = $('#financeGroupsID').val();
		var entityID = $('#entityId').val();

		var url = BASE_URL + '/groups-api/saveGroups';
		if (financeGroupsID) {
			url = BASE_URL + '/groups-api/updateGroups';
		}
		var formData ={
			financeGroupsName : $('#financeGroupsName').val(),
			financeGroupsShortName : $('#financeGroupsShortName').val(),
			financeGroupTypesID : $('#financeGroupTypesID').val(),
			financeGroupsAddress : $('#financeGroupsAddress').val(),
			financeGroupsParentID : $('#financeGroupsParentID').val(),
			financeGroupsID : financeGroupsID,
			entityID : entityID,
		}
		if(validateGroupsForm(formData)){
			eb.ajax({
				type: 'POST',
				url: url,
				data: formData,
				success: function(res) {
					p_notification(res.status, res.msg);
					if (res.status) {
						if ( financeGroupsID ) {
							window.location.href = BASE_URL + '/groups/list';
						} else {
							window.location.href = BASE_URL + '/groups/create';
						}
					}
				}
			});	
		}

	};

	function validateGroupsForm(formData){
		if( formData.financeGroupsName == '' ){
			p_notification(false, eb.getMessage('ERR_GROUPS_NAME_CNTBE_NULL'));
			$('#financeGroupsName').focus();
			return false;
		} else if( formData.financeGroupsShortName == ''){
			p_notification(false, eb.getMessage('ERR_GROUPS_SHORT_NAME_CNTBE_NULL'));
			$('#financeGroupsShortName').focus();
			return false;
		} else if( formData.financeGroupTypesID == ''){
			p_notification(false, eb.getMessage('ERR_GROUPS_TYPE_CNTBE_NULL'));
			$('#financeGroupTypesID').focus();
			return false;
		}
		return true;
	};

	$('#cancelFinanceGroups').on('click',function(){
		clearGroupsForm();
	});

	function clearGroupsForm(){
		$('#financeGroupsName').val('');
		$('#financeGroupsShortName').val('');
		$('#financeGroupTypesID').val('');
		$('#financeGroupsAddress').val('');
		$('#financeGroupsParentID').val('');
		$('.selectpicker').selectpicker('refresh');
	}
	
});