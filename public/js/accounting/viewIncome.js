var tmptotal;
var incomeID;
var customerID;

var loadDeliveryNotePreview;

$(document).ready(function() {

    $('#inv-search-select').val('Income No');
    $('.deli_row').hide();
    $('#deli_form_group').hide();
    $('#tax_td').hide();

    $('#view-div').hide();
    $('#custadiv').hide();

    $('#inv-search-select').on('change', function() {
        $('#deliverynote-list').show();
        if ($(this).val() == 'Income No') {
            $('#inv-search').selectpicker('show');
            $('#inv-custa-search').selectpicker('hide');
            $('#inv-custa-search').val('');
            $('#inv-custa-search').selectpicker('render');
            $('#view-div').hide();
            $('#custadiv').hide();
            selectedsearch = 'Income No';
        } else if ($(this).val() == 'Customer Name') {
            $('#inv-search').selectpicker('hide');
//            $('#invoice-list').hide();
            $('#inv-custa-search').selectpicker('show');
            $('#inv-search').val('');
            $('#inv-search').selectpicker('render');
            $('#view-div').hide();
            $('#custadiv').hide();
            selectedsearch = 'Customer Name';
        }
    });

    loadDropDownFromDatabase('/customerAPI/search-customers-for-dropdown', "", 'withDeactivatedCustomers', '#inv-custa-search');
    $('#inv-custa-search').on('change', function() {
        if ($(this).val() > 0 && customerID != $(this).val()) {
            customerID = $(this).val();
            var getinvbycusturl = '/income-api/retriveCustomerIncome';
            var requestinvbycust = eb.post(getinvbycusturl, {
                customerID: customerID
            });
            requestinvbycust.done(function(retdata) {
                if (retdata == "noIncome") {
                    $('#deliverynote-list').html('')
                    p_notification(false, eb.getMessage('ERR_VIEWINCOME_NO_NOTE'));
                } else {
                    $('#deliverynote-list').html(retdata)
                }
            });
        }
    });
    $('#inv-custa-search').selectpicker('hide');

    loadDropDownFromDatabase('/income-api/search-income-for-dropdown', "", 0, '#inv-search');
    $('#inv-search').on('change', function() {
        if ($(this).val() > 0 && incomeID != $(this).val()) {
            incomeID = $(this).val();
            var searchurl = BASE_URL + '/income-api/getIncomeFromSearch';
            var searchrequest = eb.post(searchurl, {incomeID: incomeID});
            searchrequest.done(function(searchdata) {
                $('#deliverynote-list').html(searchdata);
            });
        } else {
            $('#custadiv').html('');
            $('#custadiv').hide('');
            $('#deliverynote-list').show();
        }
    });

    $('#filter-button').on('click', function() {
        if ($('#from-date').val() == '' || $('#to-date').val() == '') {
            p_notification(false, eb.getMessage('ERR_VIEWPAY_FILLDATA'));
        } else {
            var invoicefilter = BASE_URL + '/income-api/getIncomeByDatefilter';
            var filterrequest = eb.post(invoicefilter, {
                'fromdate': $('#from-date').val(),
                'todate': $('#to-date').val(),
                'customerID': customerID
            });
            filterrequest.done(function(retdata) {
                if (retdata.msg == 'noIncome') {
                    p_notification(false, eb.getMessage('ERR_VIEWINCOME_NO_NOTE'));
                } else {
                    $('#deliverynote-list').html(retdata);
                }
            });
        }
    });


    ///////DatePicker\\\\\\\
    var nowTemp = new Date();
    var frmdate;
    var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);

    var checkin = $('#from-date').datepicker().on('changeDate', function(ev) {
        checkin.hide();
        frmdate = ev.date;
        $('#to-date')[0].focus();
    }).data('datepicker');
    var checkout = $('#to-date').datepicker({
        onRender: function(date) {
            return date.valueOf() < frmdate ? 'disabled' : '';
        }
    }).on('changeDate', function(ev) {
        checkout.hide();
    }).data('datepicker');
/////EndOFDatePicker\\\\\\\\\

    //responsive issue
    $(window).bind('resize ready', function() {
        ($(window).width() <= 1200) ? $('#filter-button').addClass('margin_top_low') : $('#filter-button').removeClass('margin_top_low');
    });

    $(window).bind('resize ready', function() {
        ($(window).width() <= 480) ? $('#filter-button').addClass('col-xs-12') : $('#filter-button').removeClass('col-xs-12');
    });
});