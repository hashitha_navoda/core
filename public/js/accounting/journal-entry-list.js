var ignoreBudgetLimitFlag = false;
$(document).ready(function() {
    var journalEntryID = '';
	var documentTypeID = '';
    $('#delete-all-button').hide();
	loadDropDownFromDatabase('/journal-entries-api/search-journal-entry-for-dropdown', "", false, '#journal-entry-search');
    $('#journal-entry-search').selectpicker('refresh');
    $('#journal-entry-search').on('change',function(){
    	if ($(this).val() > 0 && journalEntryID != $(this).val()) {
    		journalEntryID = $(this).val();
    		if ($('#journal-entry-search').val() == '') {
    			$('#dynamicJournalEntry').html('');
    			$('#dynamicJournalEntry').hide();
    			$('#journal-entry-list').show();
    		} else {
    			var searchurl = BASE_URL + '/journal-entries-api/getJournalEntryFromSearch';
    			var searchrequest = eb.post(searchurl, {journalEntryID: journalEntryID, journalEntryTemplateStatus: false});
    			searchrequest.done(function(searchdata) {
                    $('#delete-all-button').hide();
    				$('#journal-entry-list').hide();
    				$('#dynamicJournalEntry').show();
    				$('#dynamicJournalEntry').html(searchdata);
    			});
    		}
    	}
    });

    loadDropDownFromDatabase('/journal-entries-api/search-journal-entry-for-dropdown', "", true, '#journal-entry-template-search');
    $('#journal-entry-template-search').selectpicker('refresh');
    $('#journal-entry-template-search').on('change',function(){
    	if ($(this).val() > 0 && journalEntryID != $(this).val()) {
    		journalEntryID = $(this).val();
    		if ($('#journal-entry-template-search').val() == '') {
    			$('#dynamicJournalEntry').html('');
    			$('#dynamicJournalEntry').hide();
    			$('#journal-entry-list').show();
    		} else {
    			var searchurl = BASE_URL + '/journal-entries-api/getJournalEntryFromSearch';
    			var searchrequest = eb.post(searchurl, {journalEntryID: journalEntryID, journalEntryTemplateStatus: true});
    			searchrequest.done(function(searchdata) {
    				$('#journal-entry-list').hide();
    				$('#dynamicJournalEntry').show();
    				$('#dynamicJournalEntry').html(searchdata);
    			});
    		}
    	}
    });

    $('#documentTypes').on('change',function(){
        if ($(this).val() > 0 && documentTypeID != $(this).val()) {
            documentTypeID = $(this).val();
            if ($('#documentTypes').val() == '') {
                $('#dynamicJournalEntry').html('');
                $('#dynamicJournalEntry').hide();
                $('#journal-entry-list').show();
            } else {
                var searchurl = BASE_URL + '/journal-entries-api/getJournalEntriesByDocumentTypeID';
                var searchrequest = eb.post(searchurl, {documentTypeID: documentTypeID,});
                searchrequest.done(function(searchdata) {
                    $('#journal-entry-list').hide();
                    $('#dynamicJournalEntry').show();
                    $('#dynamicJournalEntry').html(searchdata);
                    $('#delete-all-button').show();
                });
            }
        }
    });

    $('#filter-button').on('click',function(){
        if(validateDates()){
            documentTypeID = $('#documentTypes').val();
            
            var searchurl = BASE_URL + '/journal-entries-api/getJournalEntriesByDateFilters';
            var searchrequest = eb.post(
                    searchurl,
                    {
                        fromDate: $('#from-date').val(),
                        toDate: $('#to-date').val(),
                        documentTypeID: documentTypeID,
                    }
                );
            searchrequest.done(function(searchdata) {
                $('#journal-entry-list').hide();
                $('#dynamicJournalEntry').show();
                $('#dynamicJournalEntry').html(searchdata);
            });
        }
    });
    
    $('#delete-journal-entries-button').on('click',function(e){
        e.preventDefault();
        var valid = new Array(
                        $('#username').val(),
                        $('#password').val(),
                        $('#from-date').val(),
                        $('#to-date').val(),
                        $('#documentTypes').val(),
                    );
        if(validateParams(valid)){
            documentTypeID = $('#documentTypes').val();
            eb.ajax({
                type: 'POST',
                url: BASE_URL + '/journal-entries-api/deleteJournalEntryByDocumentType',
                data: {
                    fromDate: $('#from-date').val(),
                    toDate: $('#to-date').val(),
                    documentTypeID: documentTypeID,
                    username: $("#username").val(),
                    password: $('#password').val()
                },
                success: function(respond) {
                    p_notification(respond.status, respond.msg);
                    if(respond.status){
                        window.setTimeout(function() {
                            location.reload();
                        }, 1000);
                    }
                },
                async: false
            });
        }
    });

    loadJEPreview = function(url) {
        var preview = eb.post(url);
        var path = "/journal-entries-api/sendJEEmail";
        
        preview.done(function(view_phtml) {
            var iframe = 'documentpreview';
            var $preview = $("#preview");

            $(".modal-dialog", $preview).html(view_phtml);
            $preview.modal('show');
            setTimeout(function() {
                $preview.scrollTop(0);
            }, 200);

            $("#templateID").on("change", function() {
            var $iframe = $('#' + iframe, $preview);
            $iframe.attr('src', $iframe.data('origsrc') + '/' + $(this).val());
        });

        $("#document_print", $preview).on("click", function() {
            printIframe(iframe);
        });

        $("#document_email", $preview).on("click", function() {
            $("#email-modal").modal("show");
        });

       $("#send", $preview).off('click').on("click", function() {
            // prevent doubleclick
            if ($(this).hasClass('disabled')) {
                return false;
            }
            var state = validateEmail($("#email_to", $preview).val(), $("#email-body", $preview).html());
            if (state) {
                var param = {
                    documentID: $(this).data("docid"), //$(this).data("so_id"),
                    to_email: $("#email_to", $preview).val(),
                    subject: $("#email_sub", $preview).val(),
                    body: $(".email-body", $preview).html(),
                    templateID: $('#templateID').val()
                };

                var mail = eb.post(path, param); ///<-
                mail.done(function(rep) {
                    p_notification(rep.status, rep.msg);

                    $("#email-modal button.btn", $preview).removeClass('disabled')
                    $("#email-modal", $preview).modal("hide");
                });
                $("#email-modal button.btn", $preview).addClass('disabled')
            }
        });

            $("button.email-modal-close", $preview).on("click", function() {
                $("#email-modal", $preview).modal("hide");
            });

            $("#cancel", $preview).on("click", function() {
                $preview.modal('hide');
            });

            if ($("#grnSave").length) {
                $preview.on('hidden.bs.modal', function() {

                    // since this event is being called when any modal is closed, even
                    // when the email modal is closed, this even gets triggered
                    // so check whether the main modal is visible
                    if (!$("#grnPreview:visible").length) {
                        window.location.reload();
                    }
                });
            }
        });
    }


    $(document).on("click", ".journal-entry-preview", function(e) {
        e.preventDefault();
        loadJEPreview(BASE_URL + $(this).attr("data-href"));
    });



    function validateDates()
    {
        if($('#from-date').val() == ''){
            p_notification(false, eb.getMessage('ERR_JOURNAL_ENTRY_LIST_FROM_DATE_CANT_NULL'));
            $('#from-date').focus();
            return false;
        }else if($('#to-date').val() == ''){
            p_notification(false, eb.getMessage('ERR_JOURNAL_ENTRY_LIST_To_DATE_CANT_NULL'));
            $('#to-date').focus();
            return false;
        }else if(eb.convertDateFormat('#from-date') > eb.convertDateFormat('#to-date')){
            p_notification(false, eb.getMessage('ERR_JOURNAL_ENTRY_DATE_RANGE'));
            return false;
        }
        return true;
    }
    
    function validateParams(valid)
    {
        var user = valid[0];
        var pass = valid[1];
        var fromDate = valid[2];
        var toDate = valid[3];
        var documentType = valid[4];
        if (user == null || user == "") {
            p_notification(false, eb.getMessage('ERR_VIEWPAY_UNAME'));
        } else if (pass == null || pass == "") {
            p_notification(false, eb.getMessage('ERR_VIEWPAY_PWD'));
        } else if(fromDate != '' && toDate == ''){
            p_notification(false, eb.getMessage('ERR_JOURNAL_ENTRY_LIST_To_DATE_CANT_NULL'));
            $('#to-date').focus();
            return false;
        }else if(toDate != '' && fromDate == ''){
            p_notification(false, eb.getMessage('ERR_JOURNAL_ENTRY_LIST_FROM_DATE_CANT_NULL'));
            $('#from-date').focus();
            return false;
        } else if (documentType == "" || documentType == null){
            p_notification(false, eb.getMessage('ERR_JOURNAL_ENTRY_LIST_FROM_DATE_CANT_NULL'));
            $('#documentTypes').focus();
            return false;
        } else {
            return true;
        }
    }

    var checkin = $('#from-date').datepicker().on('changeDate', function(ev) {
        checkin.hide();
    }).data('datepicker');

    var toDate = $('#to-date').datepicker().on('changeDate', function(ev) {
        toDate.hide();
    }).data('datepicker');


    $('#postOpeningBalance').on('click',function(){
        bootbox.confirm("Are you sure you want to Post opening balance Journal Entries", function(result) {
            if (result == true) {
                postOpeningJEs();
            }
        });
    });
});


function postOpeningJEs()
{
    eb.ajax({
        type: 'POST',
        data: {
            ignoreBudgetLimit: ignoreBudgetLimitFlag
        },
        url: BASE_URL + '/journal-entries-api/postOpeningBalance',
        success: function(respond) {
            if(respond.status){
                p_notification(respond.status, respond.msg);
                window.setTimeout(function() {
                    location.reload();
                }, 1000);
            } else {
                if (respond.data == "NotifyBudgetLimit") {
                    bootbox.confirm(respond.msg+' ,Are you sure you want to save this opening Journal Entries ?', function(result) {
                        if (result == true) {
                            ignoreBudgetLimitFlag = true;
                            postOpeningJEs();
                        } else {
                            setTimeout(function(){ 
                                location.reload();
                            }, 3000);
                        }
                    });
                } else {
                    p_notification(respond.status, respond.msg);
                }
            }
        },
        async: false
    });
}


function deleteJournalEntry(journalEntryID){
	bootbox.confirm("Are you sure you want to Delete this Journal Entry", function(result) {
        if (result == true) {
            eb.ajax({
                type: 'POST',
                url: BASE_URL + '/journal-entries-api/deleteJournalEntry',
                data: {
                    journalEntryID: journalEntryID,
                },
                success: function(respond) {
                    p_notification(respond.status, respond.msg);
                    if(respond.status){
	                    window.setTimeout(function() {
	                        location.reload();
	                    }, 1000);
                    }
                },
                async: false
            });
        }
    });
}

function viewJournalEntry(journalEntryID, statusID){
	eb.ajax({
		type: 'POST',
		url: BASE_URL + '/journal-entries-api/getJournalEntry',
		data: {
			journalEntryID: journalEntryID,
		},
		success: function(respond) {
			if(respond.status){
				$('#viewModal').html(respond.html);
				$('#viewModal').modal('show');
				$('.revereseJournalEntry').attr('disabled',false);
				if(statusID != 12){
					$('.revereseJournalEntry').attr('disabled',true);
				}
			}else{
				p_notification(respond.status, respond.msg);
			}
		},
		async: false
	});
}

function reverseJournalEntry(journalEntryID){
	eb.ajax({
		type: 'POST',
		url: BASE_URL + '/journal-entries-api/reverseJournalEntry',
		data: {
			journalEntryID: journalEntryID,
		},
		success: function(respond) {
			p_notification(respond.status, respond.msg);
			if(respond.status){
				window.setTimeout(function() {
					location.reload();
				}, 1000);
			}
		},
		async: false
	});
}