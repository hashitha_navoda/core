$(document).ready(function() {

	$('#create-finance-account-class-form').on('submit', function(e) {
		e.preventDefault();
		saveAccountClass();
	});

	function saveAccountClass(){
		var financeAccountClassID = $('#financeAccountClassID').val();
		var entityID = $('#entityId').val();

		var url = BASE_URL + '/account-classes-api/saveAccountClass';
		if (financeAccountClassID) {
			url = BASE_URL + '/account-classes-api/updateAccountClass';
		}

		var formData ={
			financeAccountClassName : $('#financeAccountClassName').val(),
			financeAccountTypesID : $('#financeAccountTypesID').val(),
			financeAccountClassID : financeAccountClassID,
			entityID : entityID,
		}
		if(validateAccountClassForm(formData)){
			eb.ajax({
				type: 'POST',
				url: url,
				data: formData,
				success: function(res) {
					p_notification(res.status, res.msg);
					if (res.status) {
						if ( financeAccountClassID ) {
							window.location.href = BASE_URL + '/account-classes/list';
						} else {
							window.location.href = BASE_URL + '/account-classes/create';
						}
					}
				}
			});	
		}

	};

	function validateAccountClassForm(formData){
		if( formData.financeAccountClassName == '' ){
			p_notification(false, eb.getMessage('ERR_ACCONT_CLASS_NAME_CANTBE_NULL'));
			$('#financeAccountClassName').focus();
			return false;
		} else if( formData.financeAccountTypesID == ''){
			p_notification(false, eb.getMessage('ERR_ACCONT_TYPES_CANTBE_NULL'));
			$('#financeAccountTypesID').focus();
			return false;
		}
		return true;
	};

	$('#cancelFinanceAccountClass').on('click',function(){
		clearAccountClassForm();
	});

	function clearAccountClassForm(){
		$('#financeAccountClassName').val('');
		$('#financeAccountTypesID').val('');
		$('.selectpicker').selectpicker('refresh');
	}
	
});