var journalEntryAccounts = {};
var ignoreBudgetLimitFlag = false;
var dimensionData = {};
$(document).ready(function() {

    var dimensionArray = {};


	loadDropDownFromDatabase('/journal-entry-type-api/search-journal-entry-type-for-dropdown', "", true, $('#journalEntryTypeID'));

	var jeTypeID = $('#journalEntryTypeID').data('id');
	if(jeTypeID != '' && jeTypeID != undefined){
		var jeTypeName = $('#journalEntryTypeID').data('value');
		$('#journalEntryTypeID').append("<option value = '"+jeTypeID+"'>"+jeTypeName+"</option>");
		$('#journalEntryTypeID').val(jeTypeID).selectpicker('refresh');
	}
	
	var isReversed = $('#journalEntryIsReverse').data('checked');
	if(isReversed == 1){
		$('#journalEntryIsReverse').prop('checked', true);;
	}

	var rowIncID = 1;
	$("tr", '#journal-entry-body').each(function() {
		$parentRow = $(this);
		if($parentRow.hasClass('add-row')){
			$parentRow.data('accincid',rowIncID);
			rowIncID++;
			var accountID = $parentRow.find('#financeAccountsID').data('id');
			var accountName = $parentRow.find('#financeAccountsID').data('value');
			var financeGroupsID = ($parentRow.find('#financeGroupsID').data('id') != '')? $parentRow.find('#financeGroupsID').data('id') : '';
			var financeGroupsName = $parentRow.find('#financeGroupsID').data('value');
			$parentRow.find('#financeAccountsID').selectpicker();
			$parentRow.find('#financeAccountsID').append("<option value ='"+accountID+"'>"+accountName+"</option>").val(accountID).selectpicker('refresh');
			$parentRow.find('#financeAccountsID').attr('disabled',true);

			$parentRow.find('#financeGroupsID').selectpicker();
			loadDropDownFromDatabase('/groups-api/search-groups-for-dropdown', "", true, $parentRow.find('#financeGroupsID'));
    		$parentRow.find('#financeGroupsID').selectpicker('refresh');
			$parentRow.find('#financeGroupsID').append("<option value ='"+financeGroupsID+"'>"+financeGroupsName+"</option>").val(financeGroupsID).selectpicker('refresh');
			$parentRow.find('#financeGroupsID').attr('disabled',true);
		}
	});

 	var $journalEntryTable = $("#journalEntryTable");
    var $addRowSample = $('#preSetSample', $journalEntryTable);

    var getAddRow = function(AccIncID) {
    	if (!(AccIncID === undefined || AccIncID == '')) {
            var $row = $('table#journalEntryTable > tbody#journal-entry-body > tr').filter(function() {
                return $(this).data("accincid") == AccIncID;
            });
            return $row;
        }

        return $('tr.current-row', $journalEntryTable);

    };
	if (!getAddRow().length) {
    	$($addRowSample.clone()).removeClass('hidden').addClass('current-row').attr('id','').appendTo($('#journal-entry-body'));
    }
    $row = getAddRow();
    setAccountsRowDropDowns($row);

    function setAccountsRowDropDowns($row){	
    	$row.find('#financeAccountsID').selectpicker();
    	loadDropDownFromDatabase('/accounts-api/search-active-accounts-for-dropdown', "", false, $row.find('#financeAccountsID'));
    	$row.find('#financeAccountsID').selectpicker('refresh');
    	var financeAccountsID = 0;
    	$row.find('#financeGroupsID').selectpicker();
    	loadDropDownFromDatabase('/groups-api/search-groups-for-dropdown', "", true, $row.find('#financeGroupsID'));
    	$row.find('#financeGroupsID').selectpicker('refresh');
    }

    $('#dimensionView').on('click',function(){
        clearDimensionModal();
        var journalEntryCode = $('#journalEntryCode').val();

        if (!$.isEmptyObject(dimensionData)) {
            $.each(dimensionData[journalEntryCode], function(index, val) {
                dimensionArray[index] = val;                
            });
            $.each(dimensionArray, function(index, value) {
                var newTrID = 'tr_' + value.dimensionTypeId;
                var clonedRow = $($('#dimensionPreSample').clone()).attr('id', newTrID).addClass('addedDimensions');
                $("input[name='dimensionType']", clonedRow).val(value.dimensionTypeTxt);
                $("input[name='dimensionValue']", clonedRow).val(value.dimensionValueTxt);
                clonedRow.removeClass('hidden');
                clonedRow.insertBefore('#dimensionPreSample');
                clearAddedNewDimensionRow();
            });
        }
        $('#addDimensionModal').modal('show');

    });


    $('#add_dimension').on('click', function(e) {
        e.preventDefault();
        //if plus button is clicked then disabled the button
        //if button is clicked more than one, then it does not continue
        if ($(this).is(':disabled')) {
            return false;
        } else {
            $(this).attr('disabled', true);
            var $thisRow = $(this).parents('tr');
            if ($('.dimenisonchange').val() == 0 || $('.dimenisonchange').val() == '' || $('.dimenisonchange').val() == null) {
                p_notification(false, eb.getMessage('ERR_DIMENSION_SELCET'));
                $(this).attr('disabled', false);
            } else {

                if ($('.dimensionVal').val() == 0 || $('.dimensionVal').val() == '' || $('.dimensionVal').val() == null) {
                    p_notification(false, eb.getMessage('ERR_DIMENSION_VALUE_SELCET'));
                    $(this).attr('disabled', false);
                    return;
                }

                if (dimensionArray[dimensionTypeID]) {
                    p_notification(false, eb.getMessage('ERR_DIMENSION_ALREADY_INSERTED'));
                    $('.dimenisonchange').val('');
                    $('.dimenisonchange').selectpicker('render');
                    $('.dimenisonchange').trigger('change');
                } else {
                    addNewDimensionRow($(this).parents('tr'));
                }
                $(this).attr('disabled', false);
            }
        }
    });

    function addNewDimensionRow(thisRow) {

        var dimensionTypeId = $('.dimenisonchange', thisRow).val();
        var dimensionTypeTxt = $('.dimenisonchange', thisRow).find("option:selected").text();

        switch (dimensionTypeID) {
            case "job":
                var dimensionValueTxt = $('.dimensionJobNo', thisRow).find("option:selected").text();
                var dimensionValueId = $('.dimensionJobNo', thisRow).val();
                break;
            case "project":
                var dimensionValueTxt = $('.dimensionProjectNo', thisRow).find("option:selected").text();
                var dimensionValueId = $('.dimensionProjectNo', thisRow).val();
                break;
           default:
                var dimensionValueTxt = $('.dimensionVal', thisRow).find("option:selected").text();
                var dimensionValueId = $('.dimensionVal', thisRow).val();
                break;
        }
       
        var dimensionDuplicateCheck = false;
        if (!$.isEmptyObject(dimensionArray)) {
            $.each(dimensionArray, function(index, value) {
                if (value.dimensionTypeId == dimensionTypeId) {
                    dimensionDuplicateCheck = true;
                }
            });   
        }
       
        if (dimensionDuplicateCheck) {
            p_notification(false, eb.getMessage("ERR_DIMENSION_ALREADY_INSERTED"));
            return false;
        }

        var newTrID = 'tr_' + dimensionTypeId;
        var clonedRow = $($('#dimensionPreSample').clone()).attr('id', newTrID).addClass('addedDimensions');
        $("input[name='dimensionType']", clonedRow).val(dimensionTypeTxt);
        $("input[name='dimensionValue']", clonedRow).val(dimensionValueTxt);
        clonedRow.removeClass('hidden');
        clonedRow.insertBefore('#dimensionPreSample');
        dimensionArray[dimensionTypeId] = new dimension(dimensionTypeId, dimensionValueId,dimensionTypeTxt , dimensionValueTxt);
        clearAddedNewDimensionRow();
    }


    function dimension(dimensionTypeId, dimensionValueId, dimensionTypeTxt, dimensionValueTxt) {
        this.dimensionTypeId = dimensionTypeId;
        this.dimensionValueId = dimensionValueId;
        this.dimensionTypeTxt = dimensionTypeTxt;
        this.dimensionValueTxt = dimensionValueTxt;
    }

    function clearAddedNewDimensionRow() {
        $('.dimenisonchange').val('');
        $('.dimenisonchange').selectpicker('render');
        $('.dimenisonchange').trigger('change');
    }

    $('.addDimensions').on('click', function(e) {
        e.preventDefault();

        // validate products before closing modal
        if (!dimensionModalValidate(e)) {
            e.stopPropagation();
            return false;
        } else {
            $('#addDimensionModal').modal('hide');
        }
    });

    function dimensionModalValidate(e) {
        var journalEntryCode = $('#journalEntryCode').val();
        if ($.isEmptyObject(dimensionArray)) {
            dimensionData = {};
            return true;   
        } else {
            var dimensionSingleData = {};
            $.each(dimensionArray, function(index, val) {
                dimensionSingleData[index] = val;
            });
            dimensionData[journalEntryCode] = dimensionSingleData;
            return true;
        }
    }

    $("#addDimensionModal #dimensionTable").on('click', '.deleteDimensionType', function(e) {
        e.preventDefault();
        var deleteDTrID = $(this).closest('tr').attr('id');
        var deleteDID = deleteDTrID.split('tr_')[1].trim();
        bootbox.confirm('Are you sure you want to remove this dimension ?', function(result) {
            if (result == true) {
                delete dimensionArray[deleteDID];
                $('#' + deleteDTrID).remove();
            }
        });
    });

    loadDropDownFromDatabase('/job-api/searchJobsForDropdown', "", 0, '.dimensionJobNo');
    $('.dimensionJobNo').selectpicker('refresh');
    $('.dimensionJobNo').on('change', function() {
        var dimensionValue = $(this).val();
    });

    loadDropDownFromDatabase('/api/project/searchProjectsForDropdown', "", 0, '.dimensionProjectNo');
    $('.dimensionProjectNo').selectpicker('refresh');
    $('.dimensionProjectNo').on('change', function() {
        var dimensionValue = $(this).val();
    });

    // dimension modal
    function clearDimensionModal()
    {
        $("#addDimensionModal #dimensionTable tbody .addedDimensions").remove();
        dimensionArray = {};
    }

    $('.dimenisonchange').on('change', function(e) {
        dimensionTypeID = this.value;
        getDimensionValueListByDimensionType(dimensionTypeID, function() {
        });
    });

    function getDimensionValueListByDimensionType(dimensionTypeID, callback) {
        switch (dimensionTypeID) {
            case "job":
                $('.dimensionVal').addClass('hidden');
                $('.dimensionProjectNo').addClass('hidden');
                $('.dimensionJobNo').removeClass('hidden');
                break;
            case "project":
                $('.dimensionVal').addClass('hidden');
                $('.dimensionProjectNo').removeClass('hidden');
                $('.dimensionJobNo').addClass('hidden');
                break;
           default:
                $('.dimensionVal').removeClass('hidden');
                $('.dimensionProjectNo').addClass('hidden');
                $('.dimensionJobNo').addClass('hidden');
                $.ajax({
                    type: 'POST',
                    url: BASE_URL + '/dimension-api/getRelatedDimensionValues',
                    data: {dimensionId: dimensionTypeID},
                    success: function(respond) {
                        //empty options
                        $('#dimensionVal').empty();
                        $('#dimensionVal').append($("<option>", {value: '', html: 'Select dimension value'}));
                        $(respond.data).each(function(index, dimValue) {
                            $.each(dimValue, function(ind, val) {
                                $('#dimensionVal').append($("<option>", {value: ind, html: val.value}));
                            });
                        });
                        callback();
                    }
                });
                break;
        }
    }

    $('#journal-entry-body').on('click','.save',function(){
    	$thisRow = $(this).parents('tr');
    	if(validateRow($thisRow)){
    		$thisRow.find('.save').addClass('hidden');
    		$thisRow.find('.edit').removeClass('hidden');
    		$thisRow.find('.delete').removeClass('disabled');
    		$thisRow.data('accincid',rowIncID);
    		$thisRow.removeClass('current-row').addClass('add-row');
    		rowIncID++;
    		disableRow($thisRow);
    		if (!getAddRow().length) {
    			$($addRowSample.clone()).removeClass('hidden').addClass('current-row').attr('id','').appendTo($('#journal-entry-body'));
    		}
    		$row = getAddRow();
    		setAccountsRowDropDowns($row);
    	}
    });

    $('#journal-entry-body').on('click','.edit',function(){
    	$thisRow = $(this).parents('tr');
    	$thisRow.find('.edit').addClass('hidden');
    	$thisRow.find('.update').removeClass('hidden');
    	$thisRow.addClass('edit-row');
    	enableRow($thisRow);
    });

    $('#journal-entry-body').on('click','.update',function(){
    	$thisRow = $(this).parents('tr');
    	if(validateRow($thisRow)){
    		$thisRow.find('.update').addClass('hidden');
    		$thisRow.find('.edit').removeClass('hidden');
    		$thisRow.removeClass('edit-row');
    		disableRow($thisRow);
    	}
    });

    $('#journal-entry-body').on('click','.delete',function(){
    	$(this).parents('tr').remove();
    });

    $('#create-journal-entry-form').on('submit', function(e) {
		e.preventDefault();
		saveJournalEntry();
	});

	$('#journalEntryTemplateStatus').on('click',function(){
        if($(this).is(':checked')){
            eb.ajax({
                type: 'POST',
                url: BASE_URL + '/journal-entries-api/getJournalEntryTemplateReference',
                data: {
                    jeTemplate: $(this).is(':checked'),
                },
                success: function(respond) {
                    if(respond.status){
                        $('#journalEntryCode').val(respond.data.refNumber);
                    }else{
                        p_notification(respond.status, respond.msg);
                        $('#journalEntryTemplateStatus').trigger('click');
                    }
                }
            });     
        }else{
            $('#journalEntryCode').val($('#journalEntryCode').data('id'));
        }
    })

    $('#journalEntryForOpeningBalance').on('click',function(){
		if($(this).is(':checked')){
			eb.ajax({
				type: 'POST',
				url: BASE_URL + '/journal-entries-api/getJournalEntryTemplateReference',
				data: {
					jeTemplate: $(this).is(':checked'),
				},
				success: function(respond) {
					if(respond.status){
						$('#journalEntryCode').val(respond.data.refNumber);
                        $('#createJournalEntry').addClass('hidden');
                        $('#createOpeningBalanceJournalEntry').removeClass('hidden');
                        $('#journalEntryTemplateStatus').attr('disabled', true);
                        $('#journalEntryIsReverse').attr('disabled', true);
					}else{
						p_notification(respond.status, respond.msg);
						$('#journalEntryForOpeningBalance').trigger('click');
					}
				}
			});		
		}else{
			$('#journalEntryCode').val($('#journalEntryCode').data('id'));
            $('#journalEntryTemplateStatus').attr('disabled', false);
            $('#journalEntryIsReverse').attr('disabled', false);
		}
	})

    function saveJournalEntry(){
    	if(validateForm()){
            var creditAmount = 0;
            var debitAmount = 0;
    		$("tr", '#journal-entry-body').each(function() {
    			if($(this).hasClass('edit-row')){
    				p_notification(false, eb.getMessage('ERR_PLEASE_SAVE_ALL_LINE'));
    				return false;
    			} else if($(this).hasClass('add-row')){
    				var thisRowVals ={
    					fAccountsIncID :$(this).data('accincid'),
    					financeAccountsID:$(this).find('#financeAccountsID').val(),
    					financeGroupsID:$(this).find('#financeGroupsID').val(),
    					journalEntryAccountsDebitAmount: parseFloat(($(this).find('#journalEntryAccountsDebitAmount').val() != '')? $(this).find('#journalEntryAccountsDebitAmount').val() : 0),
    					journalEntryAccountsCreditAmount:parseFloat(($(this).find('#journalEntryAccountsCreditAmount').val() != '')? $(this).find('#journalEntryAccountsCreditAmount').val() : 0),
    					journalEntryAccountsMemo:$(this).find('#journalEntryAccountsMemo').val(),
    				}
    				debitAmount+=thisRowVals.journalEntryAccountsDebitAmount;
    				creditAmount+=thisRowVals.journalEntryAccountsCreditAmount;
    				journalEntryAccounts[thisRowVals.fAccountsIncID] = thisRowVals; 
    			}
    		});

            if(debitAmount <= 0 && creditAmount <= 0){
                p_notification(false, eb.getMessage('ERR_JOURNAL_ENTRY_AMOUNT'));
                return false;
            }

            var diff = debitAmount - creditAmount;

            if (diff < 0) {
                diff = diff * -1;
            }

            if (diff > 0.000001) {
                console.log('xxxxxx');
                p_notification(false, eb.getMessage('ERR_CREDIT_AND_DEBIT_NOT_EQUAL'));
                return false;
            }

            if($.isEmptyObject(journalEntryAccounts)){
    			p_notification(false, eb.getMessage('ERR_SET_ONE_GL_ACCOUNT_AT_LEAST'));
                return false;
            }

    		eb.ajax({
    			type: 'POST',
    			url: BASE_URL + '/journal-entries-api/saveJournalEntry',
    			data: {
    				journalEntryAccounts: journalEntryAccounts,
    				journalEntryDate: $('#journalEntryDate').val(),
    				journalEntryCode: $('#journalEntryCode').val(),
    				journalEntryTypeID: $('#journalEntryTypeID').val(),
    				journalEntryIsReverse: ($('#journalEntryIsReverse').is(":checked")) ? 1 : 0,
    				journalEntryComment: $('#journalEntryComment').val(),
    				journalEntryTemplateStatus: $('#journalEntryTemplateStatus').is(':checked'),
                    ignoreBudgetLimit: ignoreBudgetLimitFlag,
                    dimensionData: dimensionData,
    			},
    			success: function(respond) {
    				if(respond.status){
    				    p_notification(respond.status, respond.msg);
    					window.location.href = BASE_URL + '/journal-entries/create';
    				} else {
                        if (respond.data == "NotifyBudgetLimit") {
                            bootbox.confirm(respond.msg+' ,Are you sure you want to save this journal entry ?', function(result) {
                                if (result == true) {
                                    ignoreBudgetLimitFlag = true;
                                    saveJournalEntry();
                                } else {
                                    setTimeout(function(){ 
                                        location.reload();
                                    }, 3000);
                                }
                            });
                        } else {
                            p_notification(respond.status, respond.msg);
                        }
                    }
    			}
    		});
    	}
    }

    function validateForm(){

    	if($('#journalEntryDate').val() == ''){
    		p_notification(false, eb.getMessage('ERR_PLEASE_SET_THE_DATE'));
    		$thisRow.find('#journalEntryDate').focus();
    		return false;
    	} else if($('#journalEntryCode').val() == ''){
    		p_notification(false, eb.getMessage('ERR_PLEASE_SET_CODE'));
    		$thisRow.find('#journalEntryCode').focus();
    		return false;
    	}
    	return true;
    }

    function disableRow($thisRow){
    	$thisRow.find('#financeAccountsID').attr('disabled',true);
    	$thisRow.find('#financeGroupsID').attr('disabled',true);
    	$thisRow.find('#journalEntryAccountsDebitAmount').attr('disabled',true);
    	$thisRow.find('#journalEntryAccountsCreditAmount').attr('disabled',true);
    	$thisRow.find('#journalEntryAccountsMemo').attr('disabled',true);
    }

    function enableRow($thisRow){
    	$thisRow.find('#financeGroupsID').attr('disabled',false);
    	$thisRow.find('#journalEntryAccountsDebitAmount').attr('disabled',false);
    	$thisRow.find('#journalEntryAccountsCreditAmount').attr('disabled',false);
    	$thisRow.find('#journalEntryAccountsMemo').attr('disabled',false);
    }

    function validateRow($thisRow){
    	if($thisRow.find('#financeAccountsID').val() == '' || $thisRow.find('#financeAccountsID').val() == 0){
    		p_notification(false, eb.getMessage('ERR_PLEASE_SELECT_ACCOUNT'));
    		$thisRow.find('#financeAccountsID').focus();
    		return false;
    	} else if($thisRow.find('#journalEntryAccountsDebitAmount').val() == '' && $thisRow.find('#journalEntryAccountsCreditAmount').val() == ''){
    		p_notification(false, eb.getMessage('ERR_PLEASE_SET_DEBIT_OR_CRDIT'));
    		if($thisRow.find('#journalEntryAccountsDebitAmount').val()== ''){
    			$thisRow.find('#journalEntryAccountsDebitAmount').focus();
    		}else if($thisRow.find('#journalEntryAccountsCreditAmount').val()== ''){
    			$thisRow.find('#journalEntryAccountsCreditAmount').focus();
    		}
    		return false;
    	} else if($thisRow.find('#journalEntryAccountsDebitAmount').val()!= "" && isNaN($thisRow.find('#journalEntryAccountsDebitAmount').val())){
    		p_notification(false, eb.getMessage('ERR_NAN_DEBIT_OR_CRDIT'));
    		$thisRow.find('#journalEntryAccountsDebitAmount').focus();
    		return false;
    	} else if($thisRow.find('#journalEntryAccountsCreditAmount').val()!= "" && isNaN($thisRow.find('#journalEntryAccountsCreditAmount').val())){
    		p_notification(false, eb.getMessage('ERR_NAN_CREDIT_OR_CRDIT'));
    		$thisRow.find('#journalEntryAccountsCreditAmount').focus();
    		return false;
    	}
    	return true; 
    }

    ///////DatePicker\\\\\\\
    var nowTemp = new Date();
    var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);
    var checkin = $('#journalEntryDate').datepicker({onRender: function(date) {
            return date.valueOf() < now.valueOf() ? ((U_DATE_R_OVERRIDE)?'':'disabled') : '';
        }
    }).on('changeDate', function(ev) {
        checkin.hide();
    }).data('datepicker');
    checkin.setValue(now);


    $('#createOpeningBalanceJournalEntry').on('click',function(){
        if(validateForm()){
            var creditAmount = 0;
            var debitAmount = 0;
            var financeAcIncId = 0;
            $("tr", '#journal-entry-body').each(function() {
                if($(this).hasClass('edit-row')){
                    p_notification(false, eb.getMessage('ERR_PLEASE_SAVE_ALL_LINE'));
                    return false;
                } else if($(this).hasClass('add-row')){
                    var thisRowVals ={
                        fAccountsIncID :$(this).data('accincid'),
                        financeAccountsID:$(this).find('#financeAccountsID').val(),
                        financeGroupsID:$(this).find('#financeGroupsID').val(),
                        journalEntryAccountsDebitAmount: parseFloat(($(this).find('#journalEntryAccountsDebitAmount').val() != '')? $(this).find('#journalEntryAccountsDebitAmount').val() : 0),
                        journalEntryAccountsCreditAmount:parseFloat(($(this).find('#journalEntryAccountsCreditAmount').val() != '')? $(this).find('#journalEntryAccountsCreditAmount').val() : 0),
                        journalEntryAccountsMemo:$(this).find('#journalEntryAccountsMemo').val(),
                    }
                    fAccountsIncID = thisRowVals.fAccountsIncID;
                    debitAmount+=thisRowVals.journalEntryAccountsDebitAmount;
                    creditAmount+=thisRowVals.journalEntryAccountsCreditAmount;
                    journalEntryAccounts[thisRowVals.fAccountsIncID] = thisRowVals; 
                }

            });

            if(debitAmount <= 0 && creditAmount <= 0){
                p_notification(false, eb.getMessage('ERR_JOURNAL_ENTRY_AMOUNT'));
                return false;
            }

            fAccountsIncID = fAccountsIncID + 1;
            var drafAccountCreditAmount = 0;
            var drafAccountDebitAmount = 0;

            if (debitAmount > creditAmount) {
                var draftAcData ={
                    fAccountsIncID :fAccountsIncID,
                    financeAccountsID:3000,
                    financeGroupsID:"",
                    journalEntryAccountsDebitAmount: 0,
                    journalEntryAccountsCreditAmount:parseFloat(debitAmount - creditAmount),
                    journalEntryAccountsMemo:"Created to balance the opening balance journal entry template of "+$('#journalEntryCode').val(),
                }

                journalEntryAccounts[fAccountsIncID] = draftAcData;
                drafAccountDebitAmount = 0; 
                drafAccountCreditAmount = parseFloat(debitAmount - creditAmount); 
            } else if (creditAmount > debitAmount) {
                var draftAcData ={
                    fAccountsIncID :fAccountsIncID,
                    financeAccountsID:3000,
                    financeGroupsID:"",
                    journalEntryAccountsDebitAmount: parseFloat(creditAmount - debitAmount),
                    journalEntryAccountsCreditAmount:0,
                    journalEntryAccountsMemo:"Created to balance the opening balance journal entry template of "+$('#journalEntryCode').val(),
                }

                journalEntryAccounts[fAccountsIncID] = draftAcData;
                drafAccountDebitAmount = parseFloat(creditAmount - debitAmount); 
                drafAccountCreditAmount = 0; 
            } else {
                var draftAcData ={
                    fAccountsIncID :fAccountsIncID,
                    financeAccountsID:"3000",
                    financeGroupsID:"",
                    journalEntryAccountsDebitAmount: 0,
                    journalEntryAccountsCreditAmount:0,
                    journalEntryAccountsMemo:"Created to balance the opening balance journal entry template of "+$('#journalEntryCode').val(),
                }

                journalEntryAccounts[fAccountsIncID] = draftAcData;
                drafAccountDebitAmount = 0; 
                drafAccountCreditAmount = 0; 
            }

            $('#draftAc').modal('show');
            $('.tempAc').val("Dummy account");
            $('.debitAmount').val(drafAccountDebitAmount);
            $('.creditAmount').val(drafAccountCreditAmount);
            $('.memo').val(journalEntryAccounts[fAccountsIncID].journalEntryAccountsMemo);
        }
    });

    $('#save').on('click',function(){
        saveJE();
    });

    function saveJE()
    {
        eb.ajax({
            type: 'POST',
            url: BASE_URL + '/journal-entries-api/saveJournalEntry',
            data: {
                journalEntryAccounts: journalEntryAccounts,
                journalEntryDate: $('#journalEntryDate').val(),
                journalEntryCode: $('#journalEntryCode').val(),
                journalEntryTypeID: $('#journalEntryTypeID').val(),
                journalEntryIsReverse: ($('#journalEntryIsReverse').is(":checked")) ? 1 : 0,
                journalEntryComment: $('#journalEntryComment').val(),
                journalEntryTemplateStatus: $('#journalEntryTemplateStatus').is(':checked'),
                journalEntryForOpeningBalance: $('#journalEntryForOpeningBalance').is(':checked'),
                ignoreBudgetLimit: ignoreBudgetLimitFlag
            },
            success: function(respond) {
                if(respond.status){
                    p_notification(respond.status, respond.msg);
                    window.location.href = BASE_URL + '/journal-entries/create';
                } else {
                    if (respond.data == "NotifyBudgetLimit") {
                        bootbox.confirm(respond.msg+' ,Are you sure you want to save this journal entry ?', function(result) {
                            if (result == true) {
                                ignoreBudgetLimitFlag = true;
                                saveJE();
                            } else {
                                setTimeout(function(){ 
                                    location.reload();
                                }, 3000);
                            }
                        });
                    } else {
                        p_notification(respond.status, respond.msg);
                    }
                }
            }
        });
    }

});