
/**
 * @author ASHAN <ashan@thinkcube.com>
 * This file contains form validation
 */


$(document).ready(function() {
    var methods = [];
    var addpayment = BASE_URL + '/income-api/editIncome';

     var nowTemp = new Date();
    var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);
    var checkin = $('#deliveryDate').datepicker({onRender: function(date) {
            return (date.valueOf() > now.valueOf() ? ((U_DATE_R_OVERRIDE) ? '' : 'disabled') : '');
        },
        //format: 'yyyy-mm-dd'
    }).on('changeDate', function(ev) {
        checkin.hide();
    }).data('datepicker');
    // checkin.setValue(now);

    $('.payMethod').each(function() {
        if ($(this).data('method') == 5) {
            var bankID = $(this).find('.bankID').val();
            var accountID = $(this).find('.selectAccountID').val();
            var bankdiv = $(this).find('#bankTransferGroup');
            if (bankID) {
                $(this).find('.bankID').attr('disabled', true);
                $('.main_div').addClass('Ajaxloading');
                eb.ajax({
                    type: 'post',
                    url: BASE_URL + '/account-api/bankAccountListForDropdown',
                    data: {
                        bankId: bankID,
                    },
                    dataType: "json",
                    async: false,
                    success: function(data) {
                        if (data['status'] == true) {
                            if ($.isEmptyObject(data['data'].list)) {
                                p_notification(false, eb.getMessage('ERR_DOESNT_HAVE_ANY_ACC_FOR_THIS_BANK', bankName));
                                $(bank).val('');
                                bankdiv.find('.accountID').attr('disabled', true);
                            } else {
                                $.each(data['data'].list, function(key, value) {
                                    var option = "<option value=" + value.value + ">" + value.text + "</option>";
                                    bankdiv.find('.accountID').attr('disabled', false);
                                    bankdiv.find('.accountID').append(option);
                                });
                                if (accountID != '') {
                                    bankdiv.find('.accountID').val(accountID).attr('disabled', true);
                                }
                            }
                        }
                        $('.main_div').removeClass('Ajaxloading');
                    },
                });
            } else {
                bankdiv.find('.accountID').attr('disabled', true);
            }
        }
    });

    $('#editPayment').on('submit', function(e) {
        e.preventDefault();
        if (validate_form()) {
            eb.ajax({
                type: 'post',
                url: addpayment,
                data: {
                    paymentMethods: methods,
                    incomeID: $('#incomeID').val(),
                    incomingPaymentID: $('#incomingPaymentID').val(),
                    paymentComment: $('#paymentComment').val(),
                    incomeComment: $('#incomeComment').val(),
                    date: $('#deliveryDate').val(),
                },
                dataType: "json",
                success: function(data) {
                    p_notification(data.status, data.msg);
                    if (data.status == true) {
                        window.location.assign(BASE_URL + '/income/list');    
                    }
                },
            });
        }

    });

    function validate_form() {

        var checked = true;
        methods = [];
        $('.payMethod').each(function() {
            var methodID = $(this).data('method');
            var methodTypeID = $(this).data('methodid');
            var paidAmount = $('.paidAmount', this).val();
            var checquenumber = $('.checquenumber', this).val();
            var bank = $('.bank', this).val();
            var reciptnumber = $('.reciptnumber', this).val();
            var cardnumber = $('.cardnumber', this).val();
            var bankID = $('.bankID', this).val();
            var accountID = $('.accountID', this).val();
            var customerBank = $('.customerBank', this).val();
            var customerAccountNumber = $('.customerAccountNumber', this).val();
            var giftCardID = $('.giftCardID', this).val();
            var lcPaymentReference = $('.lcPaymentReference', this).val();
            var ttPaymentReference = $('.ttPaymentReference', this).val();

            methods.push({
                methodID: methodID,
                methodTypeID: methodTypeID,
                paidAmount: paidAmount,
                checkNumber: checquenumber,
                bank: bank,
                reciptnumber: reciptnumber,
                cardnumber: cardnumber,
                bankID: bankID,
                accountID: accountID,
                customerBank: customerBank,
                customerAccountNumber: customerAccountNumber,
                giftCardID: giftCardID,
                lcPaymentReference: lcPaymentReference,
                ttPaymentReference: ttPaymentReference
            });
             if (methodID == 2) {
                if (checquenumber = "") {
                    p_notification(false, eb.getMessage('ERR_ADVPAY_CHEQUE_NUMR'));
                    $('.checquenumber', this).focus();
                    checked = false;
                    return false;
                }
    
            } else if (methodID == 3) {
                if (reciptnumber != "" && reciptnumber.length > 12) {
                    p_notification(false, eb.getMessage('ERR_PAY_RECIPTNO_VALIDITY'));
                    $('.reciptnumber', this).focus();
                    checked = false;
                    return false;
                } else if (cardnumber != "" && isNaN(cardnumber)) {
                    p_notification(false, eb.getMessage('ERR_ADVPAY_CRDTCARD_NUMR'));
                    $('.cardnumber', this).focus();
                    checked = false;
                    return false;
                }
            } else if (methodID == 5) {
                if (customerAccountNumber != '' && isNaN(customerAccountNumber)) {
                    p_notification(false, eb.getMessage('ERR_PAY_METHOD_CUST_ACCUNT'));
                    $('.customerAccountNumber', this).focus();
                    checked = false;
                    return false;
                }
            } else if (methodID == 6) {
                if (giftCardID == null || giftCardID == "") {
                    p_notification(false, eb.getMessage('ERR_PAY_METHOD_GIFT_CARD_SH_SELECT'));
                    $('.giftCardID', this).focus();
                    checked = false;
                    return false;
                }
            }
        });
        if (checked) {
            return true;
        } else {
            return false;
        }
    }

    $(document).on('change', '.bankID', function() {
        var bankdiv = $(this).parents('#bankTransferGroup');
        var bankID = $(this).val();
        var bank = $(this);
        var bankName = $("option:selected", $(this)).text();
        bankdiv.find('.accountID').find('option').each(function() {
            if ($(this).val()) {
                $(this).remove();
            }
        });
        if (bankID) {
            $('.main_div').addClass('Ajaxloading');
            eb.ajax({
                type: 'post',
                url: BASE_URL + '/account-api/bankAccountListForDropdown',
                data: {
                    bankId: bankID,
                },
                dataType: "json",
                async: false,
                success: function(data) {
                    if (data['status'] == true) {
                        if ($.isEmptyObject(data['data'].list)) {
                            p_notification(false, eb.getMessage('ERR_DOESNT_HAVE_ANY_ACC_FOR_THIS_BANK', bankName));
                            $(bank).val('');
                            bankdiv.find('.accountID').attr('disabled', true);
                        } else {
                            $.each(data['data'].list, function(key, value) {
                                var option = "<option value=" + value.value + ">" + value.text + "</option>";
                                bankdiv.find('.accountID').attr('disabled', false);
                                bankdiv.find('.accountID').append(option);
                            });
                        }
                    }
                    $('.main_div').removeClass('Ajaxloading');
                },
            });
        } else {
            bankdiv.find('.accountID').attr('disabled', true);
        }
    });

});
