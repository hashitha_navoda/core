var journalEntryTypeApprovers = {};
$(document).ready(function() {

	var $fiscalPeriodTable = $("#fiscalPeriodTable");
    var $addRowSample = $('.preSetSample', $fiscalPeriodTable);

	$('#createNewPiscalPeriod').on('click',function(){
		resetModal();
		$('#viewModal').modal('show');
		
	});

	$('#create-fiscal-period-form').on('submit', function(e) {
		e.preventDefault();
		saveFiscalPeriod();
	});

	$('#fiscalPeriodStatusID').on('change',function(){
		var statusID = $(this).val();
		$("#fiscalPeriodTable").find('tbody#fiscalPeriodBody > tr').each(function(){
		if($(this).hasClass('current-row')){
			$(this).find('.status').val(statusID);
		}
	});
	});

	function setChildFiscalPeriod()
	{   
		resetFiscalPeriodTable();
		if($('#fiscalPeriodStartDate').val() != '' && $('#fiscalPeriodEndDate').val() != ''){
			var startDate = eb.convertDateFormat('#fiscalPeriodStartDate');
			var endDate = eb.convertDateFormat('#fiscalPeriodEndDate');
			var childStartDate = '';
			var childEndDate = '';
			var checkDateFlag = false;
			if(startDate .getFullYear() != endDate .getFullYear()){
				checkDateFlag = true;
			}else if(startDate .getMonth() != endDate .getMonth()){
				checkDateFlag = true
			}
			if(checkDateFlag){
				while(startDate <= endDate){
					if($fiscalPeriodTable.hasClass('hidden')){
						$fiscalPeriodTable.removeClass('hidden');
					}
					$row = $($addRowSample.clone()).removeClass('hidden preSetSample').addClass('current-row').appendTo($('#fiscalPeriodBody'));
					setChildDataDatePicker($row,eb.convertDateFormat('#fiscalPeriodStartDate'),eb.convertDateFormat('#fiscalPeriodEndDate'));
					
					childStartDate = getFormatedDate(startDate,$row.find('.startDate'));
					$row.find('.startDate').val(childStartDate);

					startDate = new Date(startDate .getFullYear(), startDate .getMonth()+1, 0)
					childEndDate = getFormatedDate(startDate,$row.find('.endDate'));
					$row.find('.endDate').val(childEndDate);
					
					if(startDate > endDate){
						childEndDate = getFormatedDate(endDate,$row.find('.endDate'));
						$row.find('.endDate').val(childEndDate);
					}

					startDate.setDate(startDate .getDate() +1);
					$row.find('.status').val(14);
				}
			}
		}
	} 

	function saveFiscalPeriod()
	{
		if(formValid()){
			var fiscalPeriodChildren = [];
			var fiscalPeriodParent = {
				fiscalPeriodID :	$('#fiscalPeriodID').val(),
				fiscalPeriodStartDate :	$('#fiscalPeriodStartDate').val(),
				fiscalPeriodEndDate :	$('#fiscalPeriodEndDate').val(),
				fiscalPeriodStatusID :	$('#fiscalPeriodStatusID').val(),
				fiscalPeriodHasjournalEntries : $('#fiscalPeriodID').data('hasjournalentries'),
			};
			
			$fiscalPeriodTable.find('tbody#fiscalPeriodBody > tr').each(function(){
				$thisRow = $(this);
				if($thisRow.hasClass('current-row')){
					var thisVals = {
						fiscalPeriodID :$thisRow.data('fiscalperiodid'),
						fiscalPeriodStartDate :$thisRow.find('.startDate').val(),
						fiscalPeriodEndDate :$thisRow.find('.endDate').val(),
						fiscalPeriodStatusID :$thisRow.find('.status').val(),		
					};
					fiscalPeriodChildren.push(thisVals);
				}
			});
			var url = BASE_URL + '/fiscal-period-api/saveFiscalPeriod';
			if($('#fiscalPeriodID').val() != ''){
				url = BASE_URL + '/fiscal-period-api/updateFiscalPeriod';
			}
			eb.ajax({
    			type: 'POST',
    			url: url,
    			data: {
    				fiscalPeriodParent: fiscalPeriodParent,
    				fiscalPeriodChildren: fiscalPeriodChildren,
    			},
    			success: function(respond) {
    				p_notification(respond.status, respond.msg);
    				if(respond.status){
    					window.location.href = BASE_URL + '/fiscal-period/list';
    				}
    			}
    		});
		}
	}

	function formValid()
	{
		if($('#fiscalPeriodStartDate').val() == ''){
			p_notification(false, eb.getMessage('ERR_PLEASE_SELECT_FISCAL_PERIOD_START_DATE'));
    		$('#fiscalPeriodStartDate').focus();
    		return false;
		} else if($('#fiscalPeriodEndDate').val() == ''){
			p_notification(false, eb.getMessage('ERR_PLEASE_SELECT_FISCAL_PERIOD_END_DATE'));
    		$('#fiscalPeriodEndDate').focus();
    		return false;
		} else if(eb.convertDateFormat('#fiscalPeriodStartDate') >= eb.convertDateFormat('#fiscalPeriodEndDate')){
			p_notification(false, eb.getMessage('ERR_FISCAL_PERIOD_DATE_RANGE'));
    		return false;
		} else if($('#fiscalPeriodStatusID').val() == ''){
			p_notification(false, eb.getMessage('ERR_PLEASE_SELECT_FISCAL_PERIOD_STATUS'));
    		$('#fiscalPeriodStatusID').focus();
    		return false;
		}

		return true;
	}


	///////DatePicker\\\\\\\
    var nowTemp = new Date();
    var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);
    var checkin = $('#fiscalPeriodStartDate').datepicker({onRender: function(date) {
            return date.valueOf() < now.valueOf() ? ((U_DATE_R_OVERRIDE)?'':'disabled') : '';
        }
    }).on('changeDate', function(ev) {
    	setChildFiscalPeriod();
        checkin.hide();
    }).data('datepicker');
    checkin.setValue(now);

    ///////DatePicker\\\\\\\
    var checkOut = $('#fiscalPeriodEndDate').datepicker({onRender: function(date) {
            return date.valueOf() < now.valueOf() ? ((U_DATE_R_OVERRIDE)?'':'disabled') : '';
        }
    }).on('changeDate', function(ev) {
    	setChildFiscalPeriod();
        checkOut.hide();
    }).data('datepicker');

});

function editChildFiscalPeriod($row)
{	
	var $fiscalPeriodTable = $("#fiscalPeriodTable");
    var $addRowSample = $('.preSetSample', $fiscalPeriodTable);
	var childStartDate = '';
	var endDate = eb.convertDateFormat($row.find('.endDate'));
	var stDate = eb.convertDateFormat($row.find('.startDate'));
	startDate = new Date(endDate .getFullYear(), endDate .getMonth(), endDate .getDate()+1);
	var parentEndDate = eb.convertDateFormat('#fiscalPeriodEndDate');
	var parentStartDate = eb.convertDateFormat('#fiscalPeriodStartDate');
	$nextRow = $row.next('tr');
	if(parentEndDate > endDate && endDate > parentStartDate ){
		if($nextRow.length < 1){
			if(!(endDate < stDate)){
				$nextRow = $($addRowSample.clone()).removeClass('hidden preSetSample').addClass('current-row').appendTo($('#fiscalPeriodBody'));
				setChildDataDatePicker($nextRow, parentStartDate, parentEndDate);
				$nextRow.find('.status').val(14);
				childStartDate = getFormatedDate(startDate,$nextRow.find('.startDate'));
				$nextRow.find('.startDate').val(childStartDate);

				childEndDate = getFormatedDate(parentEndDate,$nextRow.find('.endDate'));
				$nextRow.find('.endDate').val(childEndDate);
			}else{
				thisDate = getFormatedDate(parentEndDate,$row.find('.endDate'));
				$row.find('.endDate').val(thisDate);
			}
		}else{
			if(!(endDate < stDate)){
				var nextRowendDate = eb.convertDateFormat($nextRow.find('.endDate'));
				while(nextRowendDate <= endDate){
					$nextnextrow = $nextRow; 
					$nextRow = $nextRow.next('tr');
					nextRowendDate = ($nextRow.length < 1)? '' : eb.convertDateFormat($nextRow.find('.endDate'));
					$nextnextrow.remove();
				}
				childStartDate = getFormatedDate(startDate,$nextRow.find('.startDate'));
				$nextRow.find('.startDate').val(childStartDate);

			}else{
				var thisDate = eb.convertDateFormat($nextRow.find('.startDate'));
				thisDate = new Date(thisDate .getFullYear(), thisDate .getMonth(), thisDate .getDate()-1);
				thisDate = getFormatedDate(thisDate,$row.find('.endDate'));
				$row.find('.endDate').val(thisDate);
			}
		}
	}else{
		if(!($nextRow.length < 1)){
			while(!($nextRow.length < 1)){
				$nextnextrow = $nextRow; 
				$nextRow = $nextRow.next('tr');
				$nextnextrow.remove();
			}
		}
		childEndDate = getFormatedDate(parentEndDate,$row.find('.endDate'));
		$row.find('.endDate').val(childEndDate);
	}
}

function setChildDataDatePicker($row, startDate, endDate){
	///////DatePicker\\\\\\\
	var nowTemp = new Date();
    var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);
	var endTemp = new Date(endDate);
	var end = new Date(endTemp.getFullYear(), endTemp.getMonth(), endTemp.getDate(), 0, 0, 0, 0);
	var startTemp = new Date(startDate);
	var start = new Date(startTemp.getFullYear(), startTemp.getMonth(), startTemp.getDate(), 0, 0, 0, 0);
	var checkOut = $row.find('.endDate').datepicker({onRender: function(date) {
		return (end.valueOf() < date.valueOf() || date.valueOf() < start.valueOf()) ? 'disabled' : '';
	},
	}).on('changeDate', function(ev) {
		editChildFiscalPeriod($row);
		checkOut.hide();
	}).data('datepicker');
	checkOut.setValue(now);
}

function getFormatedDate(myDate,dateID)
{
	var day = myDate.getDate() < 10 ? '0' + myDate.getDate() : myDate.getDate();
	var month = (myDate.getMonth() + 1) < 10 ? '0' + (myDate.getMonth() + 1) : (myDate.getMonth() + 1);
	var year = myDate.getFullYear();
	var deliDate = eb.getDateForDocumentEdit(dateID, day, month, year);
	return deliDate;
}

function resetModal()
{
	resetFiscalPeriodTable();
	$('#fiscalPeriodEndDate').val('');
	$('#fiscalPeriodStatusID').val(14);
	$('#fiscalPeriodID').val('');
	$('#fiscalPeriodID').data('hasjournalentries', 0);
	$('#createFiscalPeriod').val('Save');
	$('#fiscalPeriodStartDate').attr('disabled',false);
	$('#fiscalPeriodEndDate').attr('disabled',false);
}

function resetFiscalPeriodTable()
{
	$("#fiscalPeriodTable").find('tbody#fiscalPeriodBody > tr').each(function(){
		if($(this).hasClass('current-row')){
			$(this).remove();
		}
	});
	$("#fiscalPeriodTable").addClass('hidden');
}

function editFiscalPeriod(fiscalPeriodID)
{		
	eb.ajax({
		type: 'POST',
		url: BASE_URL + '/fiscal-period-api/getFiscalPeriodData',
		data: {
			fiscalPeriodID: fiscalPeriodID,
		},
		success: function(respond) {
			if(respond.status){
				resetModal();
				$('#viewModal').modal('show');
				var fiscalPeriodHasJournalEntries = respond.data.fiscalPeriodParentData.fiscalPeriodHasJournalEntries;
				var fiscalPeriodHasTransactions = respond.data.fiscalPeriodHasTransactions;

				var fiscalPeriodHasTransactions = respond.data.fiscalPeriodHasTransactions;

				var startDate = new Date(respond.data.fiscalPeriodParentData.fiscalPeriodStartDate);
				startDate = getFormatedDate(startDate,'#fiscalPeriodStartDate');
				$('#fiscalPeriodStartDate').val(startDate);
				
				var endDate = new Date(respond.data.fiscalPeriodParentData.fiscalPeriodEndDate);
				endDate = getFormatedDate(endDate,'#fiscalPeriodStartDate');
				$('#fiscalPeriodEndDate').val(endDate);
				
				$('#fiscalPeriodStatusID').val(respond.data.fiscalPeriodParentData.fiscalPeriodStatusID);
				$('#createFiscalPeriod').val('Update');
				$('#fiscalPeriodID').val(respond.data.fiscalPeriodParentData.fiscalPeriodID);
				$('#fiscalPeriodID').data('hasjournalentries',fiscalPeriodHasJournalEntries)
				
				if(fiscalPeriodHasJournalEntries == 1){
					$('#fiscalPeriodStartDate').attr('disabled',true);
					$('#fiscalPeriodEndDate').attr('disabled',true);
				}
				if(fiscalPeriodHasTransactions == true){
					$('#fiscalPeriodStartDate').attr('disabled',true);
					$('#fiscalPeriodEndDate').attr('disabled',true);
	
				}
				setFiscalPeriodChildData(respond.data.fiscalPeriodChildData, fiscalPeriodHasJournalEntries);
			}else{
				p_notification(respond.status, respond.msg);
			}
		}
	});
}

function setFiscalPeriodChildData(fiscalPeriodChildData, setFiscalPeriodChildData)
{
	var $fiscalPeriodTable = $("#fiscalPeriodTable");
    var $addRowSample = $('.preSetSample', $fiscalPeriodTable);

	$("#fiscalPeriodTable").removeClass('hidden');
	$.each(fiscalPeriodChildData, function(index,value){
		$row = $($addRowSample.clone()).removeClass('hidden preSetSample').addClass('current-row').appendTo($('#fiscalPeriodBody'));
		setChildDataDatePicker($row, eb.convertDateFormat('#fiscalPeriodStartDate'), eb.convertDateFormat('#fiscalPeriodEndDate'));

		var childStartDate = new Date(value.fiscalPeriodStartDate);
		childStartDate = getFormatedDate(childStartDate,$row.find('.startDate'));
		$row.find('.startDate').val(childStartDate);

		var childEndDate = new Date(value.fiscalPeriodEndDate);
		childEndDate = getFormatedDate(childEndDate,$row.find('.endDate'));
		$row.find('.endDate').val(childEndDate);

		if(setFiscalPeriodChildData == 1){
			$row.find('.endDate').attr('disabled',true);

		}
		
		$row.find('.status').val(value.fiscalPeriodStatusID);
		$row.data('fiscalperiodid',value.fiscalPeriodID)
	});
}

function deleteFiscalPeriod(fiscalPeriodID)
{
	bootbox.confirm("Are you sure you want to Delete this Fiscal Period", function(result) {
		if (result == true) {
			eb.ajax({
				type: 'POST',
				url: BASE_URL + '/fiscal-period-api/deleteFiscalPeriod',
				data: {
					fiscalPeriodID: fiscalPeriodID,
				},
				success: function(respond) {
					p_notification(respond.status, respond.msg);
					if(respond.status){
						window.setTimeout(function() {
							location.reload();
						}, 1000);
					}
				},
				async: false
			});
		}
	});
}

