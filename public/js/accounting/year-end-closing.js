var ignoreBudgetLimitFlag = false;
$(document).ready(function() {

	$('#yearEndClosingAc').val('1500');
	$('#retainedEarningAc').val('29');

	$("#proceedYearEndClosing").click(function(e){
		e.preventDefault();
		if ($('#fiscalPeriod').val() == "") {
			p_notification(false, eb.getMessage('ERR_PLEASE_SELECT_FISCAL_PERIOD'));
			return;
		} else if ($('#yearEndClosingAc').val() == "") {
			p_notification(false, eb.getMessage('ERR_PLEASE_SELECT_YEAR_END_CLOSING_AC'));
			return;
		} else if ($('#retainedEarningAc').val() == "") {
			p_notification(false, eb.getMessage('ERR_PLEASE_SELECT_RETAINED_EARNING_AC'));
			return;
		} else {
        	$("#yearEndClosingModal").modal("show");
		}
    });

	$('#process').click(function(event) {
		event.preventDefault();
		proceedYearEndClosing();
	});



	$('#fiscalPeriod').on('change', function() {

		var url = BASE_URL + '/fiscal-period-api/getFiscalPeriodById';

        if(!($(this).val() == null || $(this).val() == 0)){
			eb.ajax({
				type: 'POST',
				url: url,
				data: {fiscalPeriodID: $(this).val()},
				success: function(res) {
					if (res.status) {
						if (res.data.fiscalPeriodParentData.isCompleteStepOne == 1) {
							$('#step-one-status').text('Status : Completed');
						} else if (res.data.fiscalPeriodParentData.isCompleteStepOne == 0) {
							$('#step-one-status').text('Status : Pending');

						}

						if (res.data.fiscalPeriodParentData.isCompleteStepTwo == 1) {
							$('#step-two-status').text('Status : Completed');
						} else if (res.data.fiscalPeriodParentData.isCompleteStepTwo == 0) {
							$('#step-two-status').text('Status : Pending');

						}

						if (res.data.fiscalPeriodParentData.isCompleteStepTwo == 0 && res.data.fiscalPeriodParentData.isCompleteStepOne == 0) {
							$('#processStepOne').attr('disabled', false);
							$('#processStepTwo').attr('disabled', true);
						} else if (res.data.fiscalPeriodParentData.isCompleteStepTwo == 0 && res.data.fiscalPeriodParentData.isCompleteStepOne == 1) {
							$('#processStepOne').attr('disabled', true);
							$('#processStepTwo').attr('disabled', false);	
						}

						$('#processSection').removeClass('hidden');
	
					} 
				}
			});	
        	
        }
        
    });



    $('#processStepTwo').on('click', function() {

		if ($('#fiscalPeriod').val() == "") {
			p_notification(false, eb.getMessage('ERR_PLEASE_SELECT_FISCAL_PERIOD'));
			return;
		} else if ($('#yearEndClosingAc').val() == "") {
			p_notification(false, eb.getMessage('ERR_PLEASE_SELECT_YEAR_END_CLOSING_AC'));
			return;
		} else if ($('#retainedEarningAc').val() == "") {
			p_notification(false, eb.getMessage('ERR_PLEASE_SELECT_RETAINED_EARNING_AC'));
			return;
		} else {
			$("#yearEndClosingModal #process-one-msg").addClass('hidden');
			$("#yearEndClosingModal #process-two-msg").removeClass('hidden');

			$("#yearEndClosingModal #start-process-one").addClass('hidden');
			$("#yearEndClosingModal #start-process-two").removeClass('hidden');

			$("#yearEndClosingModal").modal("show");
		}
        
    });

    $('#processStepOne').on('click', function() {

		if ($('#fiscalPeriod').val() == "") {
			p_notification(false, eb.getMessage('ERR_PLEASE_SELECT_FISCAL_PERIOD'));
			return;
		} else {
			$("#yearEndClosingModal #process-one-msg").removeClass('hidden');
			$("#yearEndClosingModal #process-two-msg").addClass('hidden');

			$("#yearEndClosingModal #start-process-one").removeClass('hidden');
			$("#yearEndClosingModal #start-process-two").addClass('hidden');
			
			$("#yearEndClosingModal").modal("show");
		}
        
    });


    $('#start-process-one').on('click', function () {
    	event.preventDefault();
		proceedYearEndClosingProcessOne();

    });


    $('#start-process-two').on('click', function () {
    	event.preventDefault();
		proceedYearEndClosingProcessTwo();

    });



    function proceedYearEndClosingProcessOne() 
    {
		var url = BASE_URL + '/year-end-closing-api/processStepOneOfYearEndClosing';
    	var formData ={
			fiscalPeriod : $('#fiscalPeriod').val(),
			ignoreBudgetLimit : ignoreBudgetLimitFlag,
			currentLocationID : $('#navBarSelectedLocation').attr('data-locationid'),
		}

		eb.ajax({
			type: 'POST',
			url: url,
			data: formData,
			success: function(res) {
				if (res.status) {
					p_notification(res.status, res.msg);
					$('#step-one-status').text('Status : Completed');

					$('#processStepOne').attr('disabled', true);
					$('#processStepTwo').attr('disabled', false);
					$("#yearEndClosingModal").modal("hide");

				} else {
					$('#step-one-status').text('Status : Pending');
					$('#processStepOne').attr('disabled', false);
					$('#processStepTwo').attr('disabled', true);
					if (res.data == "NotifyBudgetLimit") {
                        bootbox.confirm(res.msg+' ,Are you sure you want to proceed year end closing ?', function(result) {
                            if (result == true) {
                                ignoreBudgetLimitFlag = true;
                                proceedYearEndClosing();
                            } else {
                                setTimeout(function(){ 
                                    location.reload();
                                }, 3000);
                            }
                        });
                    } else {
						p_notification(res.status, res.msg);
                    }
				}
			}
		});	
    }

    function proceedYearEndClosingProcessTwo() 
    {
		var url = BASE_URL + '/year-end-closing-api/processStepTwoOfYearEndClosing';
    	var formData ={
			fiscalPeriod : $('#fiscalPeriod').val(),
			yearEndClosingAc : $('#yearEndClosingAc').val(),
			retainedEarningAc : $('#retainedEarningAc').val(),
			currentLocationID : $('#navBarSelectedLocation').attr('data-locationid'),
			ignoreBudgetLimit : ignoreBudgetLimitFlag
		}

		eb.ajax({
			type: 'POST',
			url: url,
			data: formData,
			success: function(res) {
				if (res.status) {
					p_notification(res.status, res.msg);
					$("#yearEndClosingModal").modal("hide");
					window.location.href = BASE_URL + '/year-end-closing/index';

				} else {
					$('#step-one-status').text('Status : Pending');
					$('#processStepOne').attr('disabled', false);
					$('#processStepTwo').attr('disabled', true);
					if (res.data == "NotifyBudgetLimit") {
                        bootbox.confirm(res.msg+' ,Are you sure you want to proceed year end closing ?', function(result) {
                            if (result == true) {
                                ignoreBudgetLimitFlag = true;
                                proceedYearEndClosing();
                            } else {
                                setTimeout(function(){ 
                                    location.reload();
                                }, 3000);
                            }
                        });
                    } else {
						p_notification(res.status, res.msg);
                    }
				}
			}
		});	
    }

	function proceedYearEndClosing()
	{
		var formData ={
			fiscalPeriod : $('#fiscalPeriod').val(),
			yearEndClosingAc : $('#yearEndClosingAc').val(),
			retainedEarningAc : $('#retainedEarningAc').val(),
			currentLocationID : $('#navBarSelectedLocation').attr('data-locationid'),
			ignoreBudgetLimit : ignoreBudgetLimitFlag
		}

		var url = BASE_URL + '/year-end-closing-api/proceedYearEndClosing';

		eb.ajax({
			type: 'POST',
			url: url,
			data: formData,
			success: function(res) {
				if (res.status) {
					p_notification(res.status, res.msg);
					$("#yearEndClosingModal").modal("hide");
					window.location.href = BASE_URL + '/year-end-closing/index';	
				} else {
					if (res.data == "NotifyBudgetLimit") {
                        bootbox.confirm(res.msg+' ,Are you sure you want to proceed year end closing ?', function(result) {
                            if (result == true) {
                                ignoreBudgetLimitFlag = true;
                                proceedYearEndClosing();
                            } else {
                                setTimeout(function(){ 
                                    location.reload();
                                }, 3000);
                            }
                        });
                    } else {
						p_notification(res.status, res.msg);
                    }
				}
			}
		});	
	}

});