var journalEntryTypeApprovers = {};
$(document).ready(function() {

	$('#createNewJournalEntryType').on('click',function(){
		resetModal();
		$('#viewModal').modal('show');
	});

	var $journalEntryTypeApproverTable = $("#journalEntryTypeApproverTable");
    var $addRowSample = $('#presetSample', $journalEntryTypeApproverTable);
    var getAddRow = function(ApproverID) {
    	if (!(ApproverID === undefined || ApproverID == '')) {
            var $row = $('table#journalEntryTypeApproverTable > tbody > tr').filter(function() {
                return $(this).data("approverid") == ApproverID;
            });
            return $row;
        }

        return $('tr.current-row', $journalEntryTypeApproverTable);

    };
	if (!getAddRow().length) {
    	$($addRowSample.clone()).removeClass('hidden').addClass('current-row').attr('id','').appendTo($('#approverBody'));
    }

    $row = getAddRow();
    setApproversRowsDropDowns($row);

	$('#journalEntryTypeApprovedStatus').on('click',function(){
		if($(this).is(':checked')){
			$('#journalEntryTypeApproverValues').removeClass('hidden');
		}else{
			$('#journalEntryTypeApproverValues').addClass('hidden');
		}
	});

	$('#approverBody').on('click','.save',function(){
		var $row = $(this).parents('tr');
		if(approverValidation($row)){

			var approverid = $row.find('#employeeID').val();
			$row.data('approverid', approverid);
			$row.find('.edit').parents('td').removeClass('hidden');
			$row.find('.save').parents('td').addClass('hidden');
			$row.find('.delete').parents('td').removeClass('hidden');
			$row.removeClass('current-row').addClass('add-row');
			
			if (!getAddRow().length) {
    			$($addRowSample.clone()).removeClass('hidden').addClass('current-row').attr('id','').appendTo($('#approverBody'));
    		}

			disableRow($row);
   			$newrow = getAddRow();
    		setApproversRowsDropDowns($newrow);
		}
	});

	$('#approverBody').on('click','.edit',function(){
		var $row = $(this).parents('tr');

		$row.find('.edit').parents('td').addClass('hidden');
		$row.find('.update').parents('td').removeClass('hidden');
		$row.removeClass('add-row').addClass('edit-row');
		enableRow($row);
		
	});

	$('#approverBody').on('click','.update',function(){
		var $row = $(this).parents('tr');
		if(approverValidation($row)){

			var approverid = $row.find('#employeeID').val();
			$row.data('approverid', approverid);
			$row.find('.edit').parents('td').removeClass('hidden');
			$row.find('.update').parents('td').addClass('hidden');
			$row.removeClass('edit-row').addClass('add-row');

			disableRow($row);
		}
	});

	$('#approverBody').on('click','.delete',function(){
		$(this).parents('tr').remove();
	});

	$('#create-journal-entry-type-form').on('submit', function(e) {
		e.preventDefault();
		saveJournalEntryType();
	});

	function saveJournalEntryType(){
		if(validateForm()){
			$("tr", '#approverBody').each(function() {
				if($(this).hasClass('edit-row')){
					p_notification(false, eb.getMessage('ERR_JOURNAL_ENTRY_TYPE_APPROVERS_SAVE_ALL_LINE'));
					return false;
				}else if($(this).hasClass('add-row')){
					var thisRowVals = {
						employeeID : $(this).find('#employeeID').val(),
						journalEntryTypeApproversMinAmount : $(this).find('#journalEntryTypeApproversMinAmount').val(),
						journalEntryTypeApproversMaxAmount : $(this).find('#journalEntryTypeApproversMaxAmount').val(),
					}
					journalEntryTypeApprovers[thisRowVals.employeeID] = thisRowVals;
				}
			});

			if($('#journalEntryTypeApprovedStatus').is(':checked') && $.isEmptyObject(journalEntryTypeApprovers)){
				p_notification(false, eb.getMessage('ERR_JOURNAL_ENTRY_TYPE_SET_AT_LEASE_ONE_APPROVER'));
				return false;
			}

			var $url = BASE_URL + '/journal-entry-type-api/saveJournalEntryType';
			if( $('#journalEntryTypeID').val() != ''){
				$url = BASE_URL + '/journal-entry-type-api/updateJournalEntryType';
			}

			eb.ajax({
    			type: 'POST',
    			url: $url,
    			data: {
    				journalEntryTypeID: $('#journalEntryTypeID').val(),
    				journalEntryTypeName: $('#journalEntryTypeName').val(),
    				journalEntryTypeApprovedStatus: ($('#journalEntryTypeApprovedStatus').is(':checked')) ? 1: 0,
    				journalEntryTypeApprovers : journalEntryTypeApprovers
    			},
    			success: function(respond) {
    				p_notification(respond.status, respond.msg);
    				if(respond.status){
    					window.location.href = BASE_URL + '/journal-entry-type/create';
    				}
    			}
    		});
		}
	}

	function validateForm(){
		if($('#journalEntryTypeName').val() == ''){
			p_notification(false, eb.getMessage('ERR_JOURNAL_ENTRY_TYPE_NAME_EMPTY'));
			$('#journalEntryTypeName').focus();
			return false;
		}
		return true;	
	}


	function approverValidation($row){
		var minValue = parseFloat($row.find('#journalEntryTypeApproversMinAmount').val());
		var maxValue = parseFloat($row.find('#journalEntryTypeApproversMaxAmount').val()); 
		var employeeID = $row.find('#employeeID').val(); 
		var checkEmployee = false;
		$("tr", '#approverBody').each(function() {
			if(!($(this).hasClass('current-row') || $(this).hasClass('edit-row'))){
				if($(this).find('#employeeID').val() ==  employeeID){
					checkEmployee = true;
				}
			}

		});

		if( minValue!= '' && maxValue != '' && minValue > maxValue ){
			p_notification(false, eb.getMessage('ERR_JOURNAL_ENTRY_TYPE_MIN_VAL_CNTBE_MORETHAN_MAX_VAL'));
			return false;
		} else if( employeeID == ''){
			p_notification(false, eb.getMessage('ERR_JOURNAL_ENTRY_TYPE_EMPLOYEE_ID_EMPTY'));
			$row.find('#employeeID').focus();
			return false;
		} else if(checkEmployee){
			p_notification(false, eb.getMessage('ERR_JOURNAL_ENTRY_TYPE_EMPLOYEE_ALREDY_EXSIT'));
			$row.find('#employeeID').focus();
			return false;
		} else if (isNaN(minValue)) {
			p_notification(false, eb.getMessage('ERR_JOURNAL_ENTRY_TYPE_MIN_VAL'));
			$row.find('#journalEntryTypeApproversMinAmount').focus();
			return false;
		} else if (isNaN(maxValue)) {
			p_notification(false, eb.getMessage('ERR_JOURNAL_ENTRY_TYPE_MAX_VAL'));
			$row.find('#journalEntryTypeApproversMaxAmount').focus();
			return false;
		}
		return true;
	}
});

function setApproversRowsDropDowns($rows){
	$rows.find('#employeeID').selectpicker();
	loadDropDownFromDatabase('/api/approver/searchApproversForDropdown', "", 1, $rows.find('#employeeID'));
	$rows.find('#employeeID').selectpicker('refresh');
}

function disableRow($thisRow){
	$thisRow.find('#journalEntryTypeApproversMinAmount').attr('disabled',true);
	$thisRow.find('#journalEntryTypeApproversMaxAmount').attr('disabled',true);
	$thisRow.find('#employeeID').attr('disabled',true);
}

function enableRow($thisRow){
	$thisRow.find('#journalEntryTypeApproversMinAmount').attr('disabled',false);
	$thisRow.find('#journalEntryTypeApproversMaxAmount').attr('disabled',false);
	$thisRow.find('#employeeID').attr('disabled',false);
}

function resetModal(){
	$("tr", '#approverBody').each(function() {
		if(!($(this).hasClass('current-row') || this.id == 'presetSample')){
			$(this).remove();
		}else{
			if(this.id != 'presetSample'){
				$(this).find('#employeeID').val(0).selectpicker('refresh');
				$(this).find('#journalEntryTypeApproversMinAmount').val('');
				$(this).find('#journalEntryTypeApproversMaxAmount').val('');
			}
		}
	});
	$('#journalEntryTypeApprovedStatus').prop('checked',false);
	$('#journalEntryTypeApproverValues').addClass('hidden');
	$('#journalEntryTypeName').val('');
	$('#createJournalEntryType').attr('value','Save');
	$('#journalEntryTypeID').val('');

}

