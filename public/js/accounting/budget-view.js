addFromModal = true;
var dimensionTypeID = null;
var dimensionValue = null;
var fiscalPeriodID = null;
var budgetAcData = {};
$(document).ready(function() {
	if ($('#budgetEditMode').val() == 1) {
		eb.ajax({
            type: 'POST',
            url: BASE_URL + '/budget-api/getBudgetDataForEdit',
            data: {budgetID : $('#budgetID').val()},
            success: function(respond) {
                if(respond.status){
                	$('#budgetName').val(respond.data.budgetName);
                	$('#fiscalPeriods').val(respond.data.fiscalPeriodID);
                	$('#interval').val(respond.data.interval);
                	fiscalPeriodID = respond.data.fiscalPeriodID;
                	getfiscalPeriodData(respond.data.fiscalPeriodID);
					$(".smsSetting_div").removeClass('hidden');
					$(".budgetSave_div").removeClass('hidden');

					if (respond.data.dimension != null && respond.data.dimensionValue != null) {
                		$('#dimension').val(respond.data.dimension);
                		getDimensionValueListByDimensionType(respond.data.dimension, function() {
				        });
			        	$('.dimension_val_div').removeClass('hidden');
			        	$('.dimension_div').removeClass('hidden');
			        	setTimeout(function(){ 
				        	switch (respond.data.dimension) {
					            case "job":
					                $('.dimensionJobNo').val(respond.data.dimensionTxt);
					                break;
					            case "project":
					                $('.dimensionProjectNo').val(respond.data.dimensionTxt);
					                break;
					           default:
					                $('.dimensionVal').val(respond.data.dimensionValue);
					                break;
					        }
                        }, 1000);
					}
					$('.selectpicker').selectpicker('refresh'); 
					budgetAcData = respond.data.budgetAccountData;
                }                
            }
        });
	}

	$('.financeAccountClass').on('click',function(){
		var classID = $(this).parents('tr').attr('id');

		$('tr','.chartOfAccountBody').each(function(){
			var cid = $(this).data('classid')
			
			if(cid == classID && !$(this).hasClass('class_'+classID)){
				$(this).addClass('hidden');		
			}
		});

		if($('.class_'+classID).hasClass('hidden')){
			$('.class_'+classID).removeClass('hidden');
		}else{
			$('.class_'+classID).addClass('hidden');
		}
		
	});

	$('.financeAccountChild').on('click',function(){
		var parentID = $(this).parents('tr').attr('id');
		
		if($(this).parents('tr').data('autotrigger') == true){
			$(this).parents('tr').data('autotrigger',false);
			if($('.'+parentID).hasClass('hidden')){
				$('.'+parentID).removeClass('hidden');	
			}
		}

		if($('.'+parentID).hasClass('hidden')){
			$('.'+parentID).removeClass('hidden');
		}else{
			$('.'+parentID).addClass('hidden').data('autotrigger',true);
			$('.'+parentID).find('.financeAccountChild').trigger('click');
		}
	});

	$('.addFinanceAccount').on('click',function(){
		var classID = $(this).parents('tr').attr('id');
		$('#addFinanceAccountsModal').modal('show');
		$('#financeAccountClassID').val(classID).attr('disabled',true);
		$('#financeAccountClassID').selectpicker('refresh');
		$('#financeAccountsParentID').attr('disabled',false).selectpicker('refresh');
		getParentAccounts(classID);
	});

	$('.collapseAllAccounts').on('click',function(){
		if($(this).is(':checked')){
			$('.collapsAll').removeClass('hidden');
		}else{
			$('.collapsAll').addClass('hidden');
		}
	});


	function getfiscalPeriodData(fiscalPeriodID)
	{
		eb.ajax({
            type: 'POST',
            url: BASE_URL + '/fiscal-period-api/getFiscalPeriodData',
            data: {
            	fiscalPeriodID: fiscalPeriodID
            },
            success: function(respond) {
                if(respond.status){
                	if (respond.data.budgetData == false || $('#budgetEditMode').val() == 1) {
						$("#accountsTable thead tr .addedTh").remove();
						$(".addedTd").remove();
	                	setAccountsTable(respond.data);
                	} else {
                		$(".smsSetting_div").addClass('hidden');
						$(".budgetSave_div").addClass('hidden');
                		p_notification(false, eb.getMessage('ERR_FISCAL_ALREADY_BUDGETED'));
						return false;
                	}
                }                
            }
        });
	}


	function setAccountsTable(accountData)
	{
		var intervel = $("#interval").val();
		if (intervel == "monthly") {
			if (accountData.fiscalPeriodChildData.length == 0) {
				console.log(accountData.fiscalPeriodParentData);
				var thh = "<th class='text-center addedTh'>"+accountData.fiscalPeriodParentData.fiscalPeriodStartDate+" - "+accountData.fiscalPeriodParentData.fiscalPeriodEndDate+"</th>";
				$('#accountsTable thead tr').append(thh);
				$.each($('.chartOfAccountBody')[0].children, function(index, val) {
					if ($(val).hasClass("collapsAll")) {
						var thh = "<td class='text-center addedTd' data-acbudgetid="+accountData.fiscalPeriodParentData.fiscalPeriodID+"_"+val.id+"><input class='text-right budgetInput' type='number' readonly='true'></td>";
						$(val).append(thh);
					} else {
						var thh = "<td class='addedTd'></td>";
						$(val).append(thh);
					}
				});
			} else {
				$.each(accountData.fiscalPeriodChildData, function(index, val) {

					var thh = "<th class='text-center addedTh'>"+val.fiscalPeriodStartDate+" - "+val.fiscalPeriodEndDate+"</th>";
					$('#accountsTable thead tr').append(thh);
				});
				$.each($('.chartOfAccountBody')[0].children, function(index, val) {
					if ($(val).hasClass("collapsAll")) {
						$.each(accountData.fiscalPeriodChildData, function(ind, vall) {
							var thh = "<td class='text-center addedTd' data-acbudgetid="+vall.fiscalPeriodID+"_"+val.id+"><input class='text-right budgetInput' type='number' readonly='true'></td>";
							$(val).append(thh);
						});
					} else {
						$.each(accountData.fiscalPeriodChildData, function(ind, vall) {
							var thh = "<td class='addedTd'></td>";
							$(val).append(thh);
						});
					}
				});
			}
		} else {
			var thh = "<th class='text-center addedTh'>"+accountData.fiscalPeriodParentData.fiscalPeriodStartDate+" - "+accountData.fiscalPeriodParentData.fiscalPeriodEndDate+"</th>";
			$('#accountsTable thead tr').append(thh);
			$.each($('.chartOfAccountBody')[0].children, function(index, val) {
				if ($(val).hasClass("collapsAll")) {
					var thh = "<td class='text-center addedTd' data-acbudgetid="+accountData.fiscalPeriodParentData.fiscalPeriodID+"_"+val.id+"><input class='text-right budgetInput' type='number' readonly='true'></td>";
					$(val).append(thh);
				} else {
					var thh = "<td class='addedTd'></td>";
					$(val).append(thh);
				}
			});
		}
		if ($('#budgetEditMode').val() == 1) {
			setBudgetDataForUpdate(budgetAcData);
		}
	}

    function getDimensionValueListByDimensionType(dimensionTypeID, callback) {
        switch (dimensionTypeID) {
            case "job":
                $('.dimensionVal').addClass('hidden');
                $('.dimensionProjectNo').addClass('hidden');
                $('.dimensionJobNo').removeClass('hidden');
                break;
            case "project":
                $('.dimensionVal').addClass('hidden');
                $('.dimensionProjectNo').removeClass('hidden');
                $('.dimensionJobNo').addClass('hidden');
                break;
           default:
                $('.dimensionVal').removeClass('hidden');
                $('.dimensionProjectNo').addClass('hidden');
                $('.dimensionJobNo').addClass('hidden');
                $.ajax({
                    type: 'POST',
                    url: BASE_URL + '/dimension-api/getRelatedDimensionValues',
                    data: {dimensionId: dimensionTypeID},
                    success: function(respond) {
                        //empty options
                        $('#dimensionVal').empty();
                        $('#dimensionVal').append($("<option>", {value: '', html: 'Select dimension value'}));
                        $(respond.data).each(function(index, dimValue) {
                            $.each(dimValue, function(ind, val) {
                                $('#dimensionVal').append($("<option>", {value: ind, html: val.value}));
                            });
                        });
                        callback();
                    }
                });
                break;
        }
    }

    function setBudgetDataForUpdate(budgetUpdateData)
    {
    	$.each($('.chartOfAccountBody')[0].children, function(index, val) {
			if ($(val).hasClass("collapsAll")) {
				$.each(val.children, function(ind, value) {
					if ($(value).hasClass("addedTd")) {
						var fiscAcID = value.dataset.acbudgetid;
						var res = fiscAcID.split("_");
						var fiscalPeriodID = res[0];
						var financeAccountID = res[1];

						$.each(budgetUpdateData, function(i, bData) {
							var updateFiscKey = bData.subFiscalPeriodID+"_"+bData.financeAccountID;
							if (updateFiscKey == fiscAcID) {
								value.firstChild.value = bData.budgetValue;
							}

						});
					}
				});
			}
		});
    }
				
});