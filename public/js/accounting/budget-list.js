$(document).ready(function() {
    $('#searchBudget').on('click', function(event) {
        event.preventDefault();

        if ($('#fiscalPeriods').val() == 0 && $('#budgetSearchKey').val() == "") {
            p_notification(false, eb.getMessage('ERR_BUDGET_SEARCH_KEY_EMPTY'));
            return false;
        }

        var searchurl = BASE_URL + '/budget-api/getBudgetFromSearch';
        var searchrequest = eb.post(searchurl, {fiscalPeriodID: $('#fiscalPeriods').val(), searchKey: $('#budgetSearchKey').val()});
        searchrequest.done(function(searchdata) {
            $('#account-class-list').hide();
            $('#dynamicAccountClass').show();
            $('#dynamicAccountClass').html(searchdata);
        });
    });
});

function deleteBudget(budgetID){
	bootbox.confirm("Are you sure you want to delete this budget", function(result) {
        if (result == true) {
            eb.ajax({
                type: 'POST',
                url: BASE_URL + '/budget-api/deleteBudget',
                data: {
                    budgetID: budgetID,
                },
                success: function(respond) {
                    p_notification(respond.status, respond.msg);
                    if(respond.status){
	                    window.setTimeout(function() {
	                        location.reload();
	                    }, 1000);
                    }
                },
                async: false
            });
        }
    });
}