var addFromModal = false;
$(document).ready(function() {

	if($('#financeAccountsID').val()!= '') {
		var accountClassID = $('#financeAccountClassID').val();
		getParentAccounts(accountClassID);
	} else {
	 	$('#financeAccountsParentID').attr('disabled',true).selectpicker('refresh');
	}

	$('#create-finance-accounts-form').on('submit', function(e) {
		e.preventDefault();
		saveAccounts();
	});

	$('#financeAccountClassID').on('change',function(){
		var accountClassID = $(this).val();
		getParentAccounts(accountClassID);			
	});

	function saveAccounts(){
		var financeAccountsID = $('#financeAccountsID').val();
		var entityID = $('#entityId').val();
		var url = BASE_URL + '/accounts-api/saveAccounts';
		if (financeAccountsID) {
			url = BASE_URL + '/accounts-api/updateAccounts';
		}

		var formData ={
			financeAccountsID : financeAccountsID,
			financeAccountsCode : $('#financeAccountsCode').val(),
			financeAccountsName : $('#financeAccountsName').val(),
			financeAccountClassID : $('#financeAccountClassID').val(),
			financeAccountsParentID : $('#financeAccountsParentID').val(),
			financeAccountStatusID : $('#financeAccountStatusID').val(),
			entityID : entityID,
		}
		if(validateAccountsForm(formData)){
			eb.ajax({
				type: 'POST',
				url: url,
				data: formData,
				success: function(res) {
					p_notification(res.status, res.msg);
					if (res.status) {
						if ( financeAccountsID ) {
							window.location.href = BASE_URL + '/accounts/list';
						} else if(addFromModal) {
							window.location.href = BASE_URL + '/accounts/chartOfAccounts';
						} else {
							window.location.href = BASE_URL + '/accounts/create';
						}
					}
				}
			});	
		}

	};

	function validateAccountsForm(formData){
		if( formData.financeAccountClassID == '' ){
			p_notification(false, eb.getMessage('ERR_ACCONTS_CLASS_ID_CNTBE_NULL'));
			$('#financeAccountClassID').focus();
			return false;
		} else if( formData.financeAccountsCode == ''){
			p_notification(false, eb.getMessage('ERR_ACCONTS_CODE_CNTBE_NULL'));
			$('#financeAccountsCode').focus();
			return false;
		} else if( formData.financeAccountsName == '' ){
			p_notification(false, eb.getMessage('ERR_ACCONTS_NAME_CNTBE_NULL'));
			$('#financeAccountsName').focus();
			return false;
		} else if(formData.financeAccountStatusID == ''){
			p_notification(false, eb.getMessage('ERR_ACCONTS_STATUS_CNTBE_NULL'));
			$('#financeAccountStatusID').focus();
			return false;
		}
		return true;
	};

	$('#cancelFinanceAccounts').on('click',function(){
		var notClearAccountClass = $(this).data('ncleaerclass');
		clearAccountForm(notClearAccountClass);
	});

	function clearAccountForm(notClearAccountClass){

		if(!notClearAccountClass){
			$('#financeAccountClassID').val('');
		}
		$('#financeAccountsCode').val('');
		$('#financeAccountsName').val('');
		$('#financeAccountsParentID').val('');
		$('#financeAccountStatusID').val(1);
		$('.selectpicker').selectpicker('refresh');
	}
	
});

function getParentAccounts(accountClassID){
	var financeAccountsID = $('#financeAccountsID').val();
	eb.ajax({
		type: 'POST',
		url: BASE_URL + '/accounts-api/getAccountsClassRelatedAccounts',
		data: {accountClassID:accountClassID},
		success: function(res) {
			var fAccountsParentID = $('#fAccountsParentID').val();
			$('#financeAccountsParentID').html("<option value=''>select a account</option>");
			if (res.status) {
				var $newOptions = ''
				$.each(res.data.financeAccounts, function(index, value) {
					if(financeAccountsID != index){
						$newOptions+="<option value='"+index+"'>"+value+"</option>"; 
					}
				});
				$('#financeAccountsParentID').append($newOptions).attr('disabled',false).val(fAccountsParentID).selectpicker('refresh');
			}
		}
	});
}