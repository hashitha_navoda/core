$(document).ready(function() {
	var journalEntryTypeID = '';

	 loadDropDownFromDatabase('/journal-entry-type-api/search-journal-entry-type-for-dropdown', "", 0, '#journal-entry-type-search');
    $('#journal-entry-type-search').selectpicker('refresh');
    $('#journal-entry-type-search').on('change',function(){
    	if ($(this).val() > 0 && journalEntryTypeID != $(this).val()) {
    		journalEntryTypeID = $(this).val();
    		if ($('#journal-entry-type-search').val() == '') {
    			$('#dynamicJournalEntryType').html('');
    			$('#dynamicJournalEntryType').hide();
    			$('#journal-entry-type-list').show();
    		} else {
    			var searchurl = BASE_URL + '/journal-entry-type-api/getJournalEntryTypeFromSearch';
    			var searchrequest = eb.post(searchurl, {journalEntryTypeID: journalEntryTypeID});
    			searchrequest.done(function(searchdata) {
    				$('#journal-entry-type-list').hide();
    				$('#dynamicJournalEntryType').show();
    				$('#dynamicJournalEntryType').html(searchdata);
    			});
    		}
    	}
    });

});

function deleteJournalEntryType(journalEntryTypeID){
	bootbox.confirm("Are you sure you want to Delete this Journal Entry Type", function(result) {
        if (result == true) {
            eb.ajax({
                type: 'POST',
                url: BASE_URL + '/journal-entry-type-api/deleteJournalEntryType',
                data: {
                    journalEntryTypeID: journalEntryTypeID,
                },
                success: function(respond) {
                    p_notification(respond.status, respond.msg);
                    if(respond.status){
	                    window.setTimeout(function() {
	                        location.reload();
	                    }, 1000);
                    }
                },
                async: false
            });
        }
    });
}

function editJournalEntryType(journalEntryTypeID){
	eb.ajax({
		type: 'POST',
		url: BASE_URL + '/journal-entry-type-api/getJournalEntryTypeForEdit',
		data: {
			journalEntryTypeID: journalEntryTypeID,
		},
		success: function(respond) {
			if(respond.status){
				$('#viewModal').modal('show');
				resetModal();
				setJournalEntryDataForModal(respond.data.journalEntryType , respond.data.journalEntryTypeApprovers)
			}else{
				p_notification(respond.status, respond.msg);
			}
		},
		async: false
	});
}

function setJournalEntryDataForModal(journalEntryType , journalEntryTypeApprovers){
	
	$('#journalEntryTypeName').val(journalEntryType.journalEntryTypeName);
	$('#journalEntryTypeID').val(journalEntryType.journalEntryTypeID);
	$('#createJournalEntryType').attr('value','Update');

	if(journalEntryType.journalEntryTypeApprovedStatus == 1){
		$('#journalEntryTypeApprovedStatus').prop('checked',true);
		$('#journalEntryTypeApproverValues').removeClass('hidden');

		$.each(journalEntryTypeApprovers, function(index, value) {
			 $row = $('tr.current-row', '#journalEntryTypeApproverTable');
			 $row.find('#journalEntryTypeApproversMinAmount').val(value.journalEntryTypeApproversMinAmount);
			 $row.find('#journalEntryTypeApproversMaxAmount').val(value.journalEntryTypeApproversMaxAmount);
			 $row.find('#employeeID').append('<option value='+value.employeeID+'>'+value.firstName+'</option>').val(value.employeeID).selectpicker('refresh');
			 $row.data('approverid', value.employeeID);

			$row.find('.edit').parents('td').removeClass('hidden');
			$row.find('.save').parents('td').addClass('hidden');
			$row.find('.delete').parents('td').removeClass('hidden');
			$row.removeClass('current-row').addClass('add-row');
			
    		var $addRowSample = $('#presetSample', '#journalEntryTypeApproverTable');

			if (!$('tr.current-row', '#journalEntryTypeApproverTable').length) {
    			$($addRowSample.clone()).removeClass('hidden').addClass('current-row').attr('id','').appendTo($('#approverBody'));
    		}

			disableRow($row);
   			$newrow = $('tr.current-row', '#journalEntryTypeApproverTable');
    		setApproversRowsDropDowns($newrow);
		});

	}

}