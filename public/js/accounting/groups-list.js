$(document).ready(function() {
	var groupsID = '';

	 loadDropDownFromDatabase('/groups-api/search-groups-for-dropdown', "", 0, '#groups-search');
    $('#groups-search').selectpicker('refresh');
    $('#groups-search').on('change',function(){
    	if ($(this).val() > 0 && groupsID != $(this).val()) {
    		groupsID = $(this).val();
    		if ($('#groups-search').val() == '') {
    			$('#dynamicGroups').html('');
    			$('#dynamicGroups').hide();
    			$('#groups-list').show();
    		} else {
    			var searchurl = BASE_URL + '/groups-api/getGroupsFromSearch';
    			var searchrequest = eb.post(searchurl, {groupsID: groupsID});
    			searchrequest.done(function(searchdata) {
    				$('#groups-list').hide();
    				$('#dynamicGroups').show();
    				$('#dynamicGroups').html(searchdata);
    			});
    		}
    	}
    });
});

function deleteGroups(financeGroupsID){
	bootbox.confirm("Are you sure you want to Delete this Group", function(result) {
        if (result == true) {
            eb.ajax({
                type: 'POST',
                url: BASE_URL + '/groups-api/deleteGroups',
                data: {
                    financeGroupsID: financeGroupsID,
                },
                success: function(respond) {
                    p_notification(respond.status, respond.msg);
                    if(respond.status){
	                    window.setTimeout(function() {
	                        location.reload();
	                    }, 1000);
                    }
                },
                async: false
            });
        }
    });
}