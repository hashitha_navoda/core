var ignoreBudgetLimitFlag = false;
$(document).ready(function () {

    loadPiPreview = function (url) {
        var preview = eb.post(url);
        preview.done(function (view_phtml) {

            var $preview = $("#preview");

            $(".modal-dialog", $preview).html(view_phtml);
            $preview.modal('show');
            setTimeout(function () {
                $preview.scrollTop(0);
            }, 200);

            $("#piPrint", $preview).on("click", function () {
                printIframe('piPreview');
            });

            $("#piEmail", $preview).on("click", function (e) {
                $("#email-modal", $preview).modal("show");
                setTimeout(function () {
                    if (($("#email_to", $preview).val()).trim() == '') {
                        $("#email_to", $preview).focus();
                    }
                }, 500);

                e.preventDefault();
                return false;
            });

            $("#send", $preview).on("click", function () {

                // prevent doubleclick
                if ($(this).hasClass('disabled')) {
                    return false;
                }

                var state = validateEmail($("#email_to", $preview).val(), $("#email-body", $preview).html());
                if (state) {
                    var param = {
                        documentID: $(this).attr("data-pi_id"),
                        to_email: $("#email_to", $preview).val(),
                        subject: $("#email_sub", $preview).val(),
                        body: $(".email-body", $preview).html()
                    };
                    var mail = eb.post("/api/pi/send-pi-email", param);
                    mail.done(function (rep) {
                        p_notification(rep.status, rep.msg);

                        $("#email-modal button.btn", $preview).removeClass('disabled')
                        $("#email-modal", $preview).modal("hide");
                    });
                    $("#email-modal button.btn", $preview).addClass('disabled')
                }
            });

            $("button.email-modal-close", $preview).on("click", function () {
                $("#email-modal", $preview).modal("hide");
            });

            $("#cancel", $preview).on("click", function () {
                $preview.modal('hide');
            });

            if ($("#piSave").length) {
                $preview.on('hidden.bs.modal', function () {

                    // since this event is being called when any modal is closed, even
                    // when the email modal is closed, this even gets triggered
                    // so check whether the main modal is visible
                    if (!$("#piPreview:visible").length) {
                        window.location.reload();
                    }
                });
            }
        });
    }


    $(document).on("click", ".printViewPi", function (e) {
        e.preventDefault();
        loadPiPreview(BASE_URL + $(this).attr("href"));
    });



    //responsive issues
    $(window).bind('resize ready', function () {
        ($(window).width() <= 1200) ? $('#filter-button').addClass('margin_top_low') : $('#filter-button').removeClass('margin_top_low');
    });

    $(window).bind('resize ready', function () {
        ($(window).width() <= 480) ? $('#filter-button').addClass('col-xs-12') : $('#filter-button').removeClass('col-xs-12');
    });

    $('#piList').on('click', '.deletePi', function(e){
        e.preventDefault();
        var pIID = $(this).attr('data-id');
        cancelPI(pIID);

    });

    function cancelPI(pIID)
    {
        bootbox.confirm('Are you sure you want to delete this purchase invoice?', function(result) {
            if (result === true) {
                eb.ajax({
                    url: BASE_URL + '/api/pi/pi-cancel',
                    method: 'post',
                    data: {pIID: pIID, ignoreBudgetLimit: ignoreBudgetLimitFlag},
                    dataType: 'json',
                    success: function(data) {
                        if (data.status == true) {
                            p_notification(data.status, data.msg);
                            window.location.reload();
                        } else {
                            if (data.data == "NotifyBudgetLimit") {
                                bootbox.confirm(data.msg+' ,Are you sure you want to cancel ?', function(result) {
                                    if (result == true) {
                                        ignoreBudgetLimitFlag = true;
                                        cancelPI(pIID);
                                    } else {
                                        setTimeout(function(){ 
                                            location.reload();
                                        }, 3000);
                                    }
                                });
                            } else {
                                p_notification(data.status, data.msg);
                            }
                        }
                    }
                });
            }
        });
    }


    $('#piList').on('click', '.pi_related_docs', function() {
        setDataToHistoryModal($(this).attr('data-pi-related-id'));
        $('#addDocHistoryModal').modal('show');
    });

    $('#piList').on('click', '.doc_attachments', function() {
        setDataToAttachmentViewModal($(this).attr('data-pi-related-id'));
        $('#viewAttachmentModal').modal('show');
    });


    $('#doc-history-table').on('click', '.document_view', function() {
        var documentId = $(this).parents('tr').attr('data-docid');
        var documentType = $(this).parents('tr').attr('data-doctype');
        $('#DocumentViewModal').modal('show');
        getPrintPreviewByDocumentTypeAndDocumentID(documentId, documentType);

    });


});

function setDataToAttachmentViewModal(documentID) {
    $('#doc-attach-table tbody tr').remove();
    $('#doc-attach-table tfoot div').remove();
    $('#doc-attach-table tbody div').remove();

    eb.ajax({
        type: 'POST',
        url: BASE_URL + '/get-document-related-attachement',
        data: {
            documentID: documentID,
            documentTypeID: 12
        },
        success: function(respond) {
            if (respond.status == true) {
                $('#doc-attach-table thead tr').removeClass('hidden');
                $.each(respond.data, function(index, value) {
                    tableBody = "<tr><td><a href="+value.link+">"+value.docName+"</a></td></tr>";
                    $('#doc-attach-table tbody').append(tableBody);
                });
            } else {
                $('#doc-attach-table thead tr').addClass('hidden');
                var noDataFooter = "<div class= ' col-lg-6 text-right'><label><b>No Data</b></label>";
                $('#doc-attach-table tbody').append(noDataFooter);
            }
        }
    });
}

function setDataToHistoryModal(purchaseInvoiceID) {
    $('#doc-history-table tbody tr').remove();
    $('#doc-history-table tfoot div').remove();
    $('#doc-history-table tbody div').remove();

    eb.ajax({
        type: 'POST',
        url: BASE_URL + '/api/pi/getAllRelatedDocumentDetailsByPiId',
        data: {purchaseInvoiceID: purchaseInvoiceID},
        success: function(respond) {
            if (respond.status == true) {
                $('#doc-history-table thead tr').removeClass('hidden');
                $.each(respond.data, function(index, value) {
                    if (value != null) {
                        $.each(value, function(index2, value2) {
                            tableBody = "<tr data-docId=" + value2['documentID'] + " data-doctype=" + value2['type'] + "><td class='' >" + value2['type'] + "</td><td>" + value2['code'] + "</td><td>" + value2['amount'] + "</td><td>" + value2['issuedDate'] + "</td><td class='text-center'><span class='glyphicon glyphicon-eye-open openlist link_color document_view ' style='cursor:pointer'></span></td></tr>";
                            $('#doc-history-table tbody').append(tableBody);
                        });
                    }
                });
                var footer = " <div class='col-lg-12 col-md-12 col-sm-12 text-right'><button type='button' class='btn btn-primary col-lg-12 col-md-12 col-sm-12' data-dismiss='modal' aril-hidden='true'>Back</button></div>";
                $('#doc-history-table tfoot').append(footer);
            } else {
                $('#doc-history-table thead tr').addClass('hidden');
                var noDataFooter = "<div class= ' col-lg-9 text-right'><label><b>No Data</b></label>";
                $('#doc-history-table tbody').append(noDataFooter);
            }
        }
    });
}

function getPrintPreviewByDocumentTypeAndDocumentID(documentID, documentType) {
    var $iframe = $('#related-document-view');
    $iframe.ready(function() {
        $iframe.contents().find("body div").remove();
    });
    var URL;
    if (documentType == 'PurchaseOrder') {
        URL = BASE_URL + '/po/document/' + documentID;
    } else if (documentType == 'Grn') {
        URL = BASE_URL + '/grn/document/' + documentID;
    }
    else if (documentType == 'PurchaseInvoice') {
        URL = BASE_URL + '/pi/document/' + documentID;
    }
    else if (documentType == 'PurchaseReturn') {
        URL = BASE_URL + '/pr/document/' + documentID;
    }
    else if (documentType == 'DebitNote') {
        URL = BASE_URL + '/debit-note/document/' + documentID;
    }
    else if (documentType == 'SupplierPayment') {
        URL = BASE_URL + '/supplierPayments/document/' + documentID;
    }
    else if (documentType == 'DebitNotePayment') {
        URL = BASE_URL + '/debit-note-payments/document/' + documentID;
    }

    eb.ajax({
        type: 'POST',
        url: URL,
        success: function(respond) {
            var division = "<div></div>";
            $iframe.ready(function() {
                $iframe.contents().find("body").append(division);
            });
            $iframe.ready(function() {
                $iframe.contents().find("body div").append(respond);
            });
        }
    });

}