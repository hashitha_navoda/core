/**
 * @author Damith Thamara <damith@thinkcube.com>
 * This javascript contains PI related functions
 */
var locationProducts;
var locProductCodes;
var locProductNames;
var selectedProduct;
var selectedSupplier;
var selectedProductQuantity;
var selectedProductUom;
var batchCount = 0;
var currentlySelectedProduct;
var productLineTotal;
var grnIDs = {};
var documentTypeArray = [];
var dimensionData = {};
var uploadedAttachments = {};
var deletedAttachmentIds = [];
var deletedAttachmentIdArray = [];
var ignoreBudgetLimitFlag = false;

$(document).ready(function() {
    var selectedLocation;
    var selectedGrn = {};
    var grnSelected = false;
    var poSelected = false;
    var copyFromChooser = 'GRN';
    var grn;
    var po;
    var selectedPo;
    var decimalPoints = 0;
    var currentItemTaxResults;
    var currentTaxAmount;
    var products = new Array();
    var batchProducts = {};
    var serialProducts = {};
    var totalTaxList = {};
    var locRefID;
    var grnProducts = {};
    var poProducts = {};
    var addingGrnPro = '';
    var thisIsGrnAddFlag = false;
    var thisIsPoAddFlag = false;
    var idNumber = 1;
    var quon = 0;
    var grnProID;
    var grnProductID;
    var grnDocID;
    var piPostData = {};
    var pIExistingDetails = {};
    var piEditedDataSet = {};
    var piEditflag = false;
    var efectedPiID;
    var efectedPiCode;
    var editedPiBatachSerialRow;
    var removedLineID;
    var normalProRealQty = [];
    var editedIssueDate;
    var itemEditOpenFlag = false;
    var multipleGrnCopyFlag =false;
    var multipleGrnEditFlag =false;
    var piProductOriginalTax = null;
    var dimensionArray = {};
    var dimensionTypeID = null;
    $('#disc_presentage').attr('checked', 'checked');
    var invoiceTotalDiscountTrigger = false;
    
    ///////DatePicker\\\\\\\

    var myDate = new Date();
    var day = myDate.getDate() < 10 ? '0' + myDate.getDate() : myDate.getDate();
    var month = (myDate.getMonth() + 1) < 10 ? '0' + (myDate.getMonth() + 1) : (myDate.getMonth() + 1);
    var prettyDate = myDate.getFullYear() + '-' + ((myDate.getMonth() < 10 ? '0' : '') + (myDate.getMonth() + 1)) + '-' + (myDate.getDate() < 10 ? '0' : '') + myDate.getDate();
    $("#issueDate").val(eb.getDateForDocumentEdit('#issueDate', day, month, myDate.getFullYear()));

    var nowTemp = new Date();
    var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);

    var checkin = $('#deliveryDate').datepicker().on('changeDate', function(ev) {
        checkin.hide();
    }).data('datepicker');
    checkin.setValue(now);

    var batchMD = $('#newBatchMDate').datepicker({
        onRender: function(date) {
            return date.valueOf() > now.valueOf() ? 'disabled' : '';
        }
    }).on('changeDate', function(ev) {
        batchMD.hide();
    }).data('datepicker');

    var batchED = $('#newBatchEDate').datepicker().on('changeDate', function(ev) {
        batchED.hide();
    }).data('datepicker');

    var sED = $('.serialEDate').datepicker().on('changeDate', function(ev) {
        sED.hide();
    }).data('datepicker');

    var issueD = $('#issueDate').datepicker().on('changeDate', function(ev) {
        issueD.hide();
    }).data('datepicker');


/////EndOFDatePicker\\\\\\\\\

    function product(lPID, pID, pCode, pN, pQ, pD, pUP, pUom, pTotal, pTax, bProducts, sProducts, productType, stockUpdate, productindexID, pisubProID = null, grnFlag ,poFlag, grnDocId, dType) {
        this.locationPID = lPID;
        this.pID = pID;
        this.pCode = pCode;
        this.pName = pN;
        this.pQuantity = pQ;
        this.pDiscount = pD;
        this.dType = dType;
        this.pUnitPrice = pUP;
        this.pUom = pUom;
        this.pTotal = pTotal;
        this.pTax = pTax;
        this.bProducts = bProducts;
        this.sProducts = sProducts;
        this.productType = productType;
        this.stockUpdate = stockUpdate;
        this.productindexID = productindexID;
        this.pisubProID = pisubProID;
        this.grnFlag = grnFlag;
        this.poFlag = poFlag;
        this.grnDocId = grnDocId;
    }

    function batchProduct(bCode, bQty, mDate, eDate, warnty, bID) {
        this.bCode = bCode;
        this.bQty = bQty;
        this.mDate = mDate;
        this.eDate = eDate;
        this.warnty = warnty;
        this.bID = bID;
    }

    function serialProduct(sCode, sWarranty, sBCode, sEdate, sID, sWarrantyType) {
        this.sCode = sCode;
        this.sWarranty = sWarranty;
        this.sBCode = sBCode;
        this.sEdate = sEdate;
        this.sID = sID;
        this.sWarrantyType=sWarrantyType;
    }

    $('#dimensionView').on('click', function(e) {
        clearDimensionModal();
        var piNo = $('#piNo').val();

        if (!$.isEmptyObject(dimensionData)) {
            $.each(dimensionData[piNo], function(index, val) {
                dimensionArray[index] = val;                
            });
            $.each(dimensionArray, function(index, value) {
                var newTrID = 'tr_' + value.dimensionTypeId;
                var clonedRow = $($('#dimensionPreSample').clone()).attr('id', newTrID).addClass('addedDimensions');
                $("input[name='dimensionType']", clonedRow).val(value.dimensionTypeTxt);
                $("input[name='dimensionValue']", clonedRow).val(value.dimensionValueTxt);
                clonedRow.removeClass('hidden');
                clonedRow.insertBefore('#dimensionPreSample');
                clearAddedNewDimensionRow();
            });
        }
        $('#addDimensionModal').modal('show');
    });

    // dimension modal
    function clearDimensionModal()
    {
        $("#addDimensionModal #dimensionTable tbody .addedDimensions").remove();
        dimensionArray = {};
    }


    $('.dimenisonchange').on('change', function(e) {
        dimensionTypeID = this.value;
        getDimensionValueListByDimensionType(dimensionTypeID, function() {
        });
    });



    function getDimensionValueListByDimensionType(dimensionTypeID, callback) {
        switch (dimensionTypeID) {
            case "job":
                $('.dimensionVal').addClass('hidden');
                $('.dimensionProjectNo').addClass('hidden');
                $('.dimensionJobNo').removeClass('hidden');
                break;
            case "project":
                $('.dimensionVal').addClass('hidden');
                $('.dimensionProjectNo').removeClass('hidden');
                $('.dimensionJobNo').addClass('hidden');
                break;
           default:
                $('.dimensionVal').removeClass('hidden');
                $('.dimensionProjectNo').addClass('hidden');
                $('.dimensionJobNo').addClass('hidden');
                $.ajax({
                    type: 'POST',
                    url: BASE_URL + '/dimension-api/getRelatedDimensionValues',
                    data: {dimensionId: dimensionTypeID},
                    success: function(respond) {
                        //empty options
                        $('#dimensionVal').empty();
                        $('#dimensionVal').append($("<option>", {value: '', html: 'Select dimension value'}));
                        $(respond.data).each(function(index, dimValue) {
                            $.each(dimValue, function(ind, val) {
                                $('#dimensionVal').append($("<option>", {value: ind, html: val.value}));
                            });
                        });
                        callback();
                    }
                });
                break;
        }
    }

    $('#add_dimension').on('click', function(e) {
        e.preventDefault();
        //if plus button is clicked then disabled the button
        //if button is clicked more than one, then it does not continue
        if ($(this).is(':disabled')) {
            return false;
        } else {
            $(this).attr('disabled', true);
            var $thisRow = $(this).parents('tr');
            if ($('.dimenisonchange').val() == 0 || $('.dimenisonchange').val() == '' || $('.dimenisonchange').val() == null) {
                p_notification(false, eb.getMessage('ERR_DIMENSION_SELCET'));
                $(this).attr('disabled', false);
            } else {
                if ($('.dimenisonchange').val() != 'job' && $('.dimenisonchange').val() != 'project') {
                    if ($('.dimensionVal').val() == 0 || $('.dimensionVal').val() == '' || $('.dimensionVal').val() == null) {
                        p_notification(false, eb.getMessage('ERR_DIMENSION_VALUE_SELCET'));
                        $(this).attr('disabled', false);
                        return;
                    }
                }

                if ($('.dimenisonchange').val() == 'project') {
                    if ($('.dimensionProjectNo').val() == 0 || $('.dimensionProjectNo').val() == '' || $('.dimensionProjectNo').val() == null) {
                        p_notification(false, eb.getMessage('ERR_DIMENSION_VALUE_SELCET'));
                        $(this).attr('disabled', false);
                        return;
                    }
                }

                if ($('.dimenisonchange').val() == 'job') {
                    if ($('.dimensionJobNo').val() == 0 || $('.dimensionJobNo').val() == '' || $('.dimensionJobNo').val() == null) {
                        p_notification(false, eb.getMessage('ERR_DIMENSION_VALUE_SELCET'));
                        $(this).attr('disabled', false);
                        return;
                    }
                }
                
                if (dimensionArray[dimensionTypeID]) {
                    p_notification(false, eb.getMessage('ERR_DIMENSION_ALREADY_INSERTED'));
                    $('.dimenisonchange').val('');
                    $('.dimenisonchange').selectpicker('render');
                    $('.dimenisonchange').trigger('change');
                } else {
                    addNewDimensionRow($(this).parents('tr'));
                }
                $(this).attr('disabled', false);
            }
        }
    });

    function addNewDimensionRow(thisRow) {

        var dimensionTypeId = $('.dimenisonchange', thisRow).val();
        var dimensionTypeTxt = $('.dimenisonchange', thisRow).find("option:selected").text();

        switch (dimensionTypeID) {
            case "job":
                var dimensionValueTxt = $('.dimensionJobNo', thisRow).find("option:selected").text();
                var dimensionValueId = $('.dimensionJobNo', thisRow).val();
                break;
            case "project":
                var dimensionValueTxt = $('.dimensionProjectNo', thisRow).find("option:selected").text();
                var dimensionValueId = $('.dimensionProjectNo', thisRow).val();
                break;
           default:
                var dimensionValueTxt = $('.dimensionVal', thisRow).find("option:selected").text();
                var dimensionValueId = $('.dimensionVal', thisRow).val();
                break;
        }
       
        var dimensionDuplicateCheck = false;
        if (!$.isEmptyObject(dimensionArray)) {
            $.each(dimensionArray, function(index, value) {
                if (value.dimensionTypeId == dimensionTypeId) {
                    dimensionDuplicateCheck = true;
                }
            });   
        }
       
        if (dimensionDuplicateCheck) {
            p_notification(false, eb.getMessage("ERR_DIMENSION_ALREADY_INSERTED"));
            return false;
        }

        var newTrID = 'tr_' + dimensionTypeId;
        var clonedRow = $($('#dimensionPreSample').clone()).attr('id', newTrID).addClass('addedDimensions');
        $("input[name='dimensionType']", clonedRow).val(dimensionTypeTxt);
        $("input[name='dimensionValue']", clonedRow).val(dimensionValueTxt);
        clonedRow.removeClass('hidden');
        clonedRow.insertBefore('#dimensionPreSample');
        dimensionArray[dimensionTypeId] = new dimension(dimensionTypeId, dimensionValueId,dimensionTypeTxt , dimensionValueTxt);
        clearAddedNewDimensionRow();
    }


    function dimension(dimensionTypeId, dimensionValueId, dimensionTypeTxt, dimensionValueTxt) {
        this.dimensionTypeId = dimensionTypeId;
        this.dimensionValueId = dimensionValueId;
        this.dimensionTypeTxt = dimensionTypeTxt;
        this.dimensionValueTxt = dimensionValueTxt;
    }

    function clearAddedNewDimensionRow() {
        $('.dimenisonchange').val('');
        $('.dimenisonchange').selectpicker('render');
        $('.dimenisonchange').trigger('change');
    }

    $('.addDimensions').on('click', function(e) {
        e.preventDefault();

        // validate products before closing modal
        if (!dimensionModalValidate(e)) {
            e.stopPropagation();
            return false;
        } else {
            $('#addDimensionModal').modal('hide');
        }
    });

    function dimensionModalValidate(e) {
        var piNo = $('#piNo').val();
        if ($.isEmptyObject(dimensionArray)) {
            dimensionData = {};
            return true;   
        } else {
            var dimensionSingleData = {};
            $.each(dimensionArray, function(index, val) {
                dimensionSingleData[index] = val;
            });
            dimensionData[piNo] = dimensionSingleData;
            return true;
        }
    }

    $("#addDimensionModal #dimensionTable").on('click', '.deleteDimensionType', function(e) {
        e.preventDefault();
        var deleteDTrID = $(this).closest('tr').attr('id');
        var deleteDID = deleteDTrID.split('tr_')[1].trim();
        bootbox.confirm('Are you sure you want to remove this dimension ?', function(result) {
            if (result == true) {
                delete dimensionArray[deleteDID];
                $('#' + deleteDTrID).remove();
            }
        });
    });

    loadDropDownFromDatabase('/job-api/searchJobsForDropdown', "", 0, '.dimensionJobNo');
    $('.dimensionJobNo').selectpicker('refresh');
    $('.dimensionJobNo').on('change', function() {
        var dimensionValue = $(this).val();
    });

    loadDropDownFromDatabase('/api/project/searchProjectsForDropdown', "", 0, '.dimensionProjectNo');
    $('.dimensionProjectNo').selectpicker('refresh');
    $('.dimensionProjectNo').on('change', function() {
        var dimensionValue = $(this).val();
    });

    $('#viewUploadedFiles').on('click', function(e) {
        $('#doc-attach-table tbody tr').remove();
        $('#doc-attach-table tfoot div').remove();
        $('#doc-attach-table tbody div').remove();

        if (!$.isEmptyObject(uploadedAttachments)) {
            $('#doc-attach-table thead tr').removeClass('hidden');
            $.each(uploadedAttachments, function(index, value) {
                if (!deletedAttachmentIds.includes(value['documentAttachemntMapID'])) {
                    tableBody = "<tr id="+value['documentAttachemntMapID']+" data-attachId=" + value['documentAttachemntMapID'] + "><td><a href="+value.docLink+">"+value.documentRealName+"</a></td><td class='text-center'><span class='glyphicon glyphicon-trash delete_attachment' style='cursor:pointer'></span></td></tr>";
                    $('#doc-attach-table tbody').append(tableBody);
                }
            });
        } else {
            $('#doc-attach-table thead tr').addClass('hidden');
            var noDataFooter = "<div class= ' col-lg-6 text-right'><label><b>No Data</b></label>";
            $('#doc-attach-table tbody').append(noDataFooter);
        }
        $('#attach_view_footer').removeClass('hidden');
        $('#viewAttachmentModal').modal('show');
    });

    $('#doc-attach-table').on('click', '.delete_attachment', function() {
        var attachementID = $(this).parents('tr').attr('data-attachid');
        bootbox.confirm('Are you sure you want to remove this attachemnet?', function(result) {
            if (result == true) {
                $('#' + attachementID).remove();
                deletedAttachmentIdArray.push(attachementID);
            }
        });
    });

    $('#viewAttachmentModal').on('click', '#saveAttachmentEdit', function(event) {
        event.preventDefault();

        $.each(deletedAttachmentIdArray, function(index, val) {
            deletedAttachmentIds.push(val);
        });

        $('#viewAttachmentModal').modal('hide');
    });

    $('#po').selectpicker('hide');
    $('#multipleGrn').selectpicker('hide');
    $('#copyFromChooser').on('change', function() {
        if ($(this).val() == "GRN") {
            $('#po').selectpicker('hide');
            $('#grn').selectpicker('show');
            copyFromChooser='GRN';
        } else {
            $('#po').selectpicker('show');
            $('#grn').selectpicker('hide');
            copyFromChooser = 'PO';
        }
    });

    function enableMultiDeliNoteDropdown(){
        $('#multipleGrn').attr('disabled', false);
        var suppID = $('#supplier').val();
        $('#multipleGrn').find('option').remove();
        $('#multipleGrn').removeData('AjaxBootstrapSelect');
        $('#multipleGrn').removeData('selectpicker');
        $('#multipleGrn').siblings('.bootstrap-select').remove();
        $('#multipleGrn').siblings('.bootstrap-select').find('.dropdown-menu').remove();
        $('#multipleGrn').selectpicker('refresh');
        loadDropDownFromDatabase('/api/grn/search-open-grn-for-dropdown?supplierID='+ selectedSupplier, "", 0, '#multipleGrn');
    }

    $('#multipleCopy').on('click',function(){
        if($(this).is(':checked')){
            $('#multipleGrn').selectpicker('show');
            $('#po').selectpicker('hide');
            $('#grn').selectpicker('hide');
            multipleGrnCopyFlag = true;
            $('#copyFromChooser').attr('disabled', true);
            if(!($('#multipleGrn option[value=""]').length > 0)){
                $('#multipleGrn').append('<option value="">Select GRN Numbers</option>');
                $('#multipleGrn').val('').selectpicker('refresh');
            }

            if(!($('#supplier').val())){
                $('#multipleGrn').attr('disabled',true);
            }
            p_notification('info', eb.getMessage('INFO_SELCT_SUPPLIER'));
        }else{
            $('#multipleGrn').selectpicker('hide');
            $('#copyFromChooser').attr('disabled', false);
            $('#copyFromChooser').trigger('change');
            multipleGrnCopyFlag = false;
            if(!($('#grn option[value=""]').length > 0)){
                $('#grn').append('<option value="">Select GRN No</option>');
                $('#grn').val('').selectpicker('refresh');
            }
        }
        clearInvoiceForm();
    });

    $('#multipleGrn').on('change', function() {
        resetInvoicePage();
        if(!($(this).val() == null || $(this).val() == 0) && grnIDs != $(this).val()){
            grnIDs = $(this).val();
            loadGRN(grnIDs);
        }
    });

    loadDropDownFromDatabase('/api/grn/search-open-grn-for-dropdown', "", 0, '#grn');
    $('#grn').selectpicker('refresh');
    $('#grn').on('change', function() {
        if ($(this).val() > 0 && $(this).val() != selectedGrn) {
            selectedGrn = $(this).val();
            grnSelected = true;
            grn = grnSelected;
            loadGRN(selectedGrn);
        }else if($(this).val() == 0 && selectedGrn != null){
            //$('#grn').val(selectedGrn).selectpicker('render');
            grn = '';
        }else if($(this).val() == selectedGrn){
            grn = selectedGrn;
        }

    });

    loadDropDownFromDatabase('/api/po/search-po-for-dropdown', "", 0, '#po');
    $('#po').selectpicker('refresh');
    $('#po').on('change', function() {
        if ($(this).val() > 0 && $(this).val() != selectedPo) {
            selectedPo = $(this).val();
            poSelected = true;
            po = selectedPo;
            loadPo(selectedPo);
        }else if($(this).val() == 0 && selectedPo != null){
            //$('#po').val(selectedPo).selectpicker('render');
            po = '';
        }else if($(this).val() == selectedPo){
            po = selectedPo;
        }
    });

    // for update operations
    if($('#editMode').val() == '1'){
        var reqPiID = $('#editMode').attr('data-piid');
        loadDataToPi(reqPiID);
        $('#piSave').val('Edit');
        $('#savePiWithJournalEntry').val('Update Invoice With This Journal Entry');
    }



    loadDropDownFromDatabase('/supplierAPI/search-suppliers-for-dropdown', "", 0, '#supplier');
    $('#supplier').selectpicker('refresh');
    $('#supplier').on('change', function() {
        if ($(this).val() > 0 && $(this).val() != selectedSupplier) {
            selectedSupplier = $(this).val();
            getSupplierDetails(selectedSupplier);
            enableMultiDeliNoteDropdown();
        }
        else if($(this).val() == 0){
            selectedSupplier = '';
        }
    });

    $.each(LOCATION, function(index, value) {
        $('#retrieveLocation').append($("<option value='" + index + "'>" + value + "</option>"));
    });
    $('#retrieveLocation').selectpicker('refresh');
    $('#retrieveLocation').on('change', function() {
        if ($(this).val() == '') {
            selectedLocation = ''
            clearProductScreen();
            $('#productAddOverlay').addClass('overlay');
            $('#piNo').val('');
            $('#locRefID').val('');
        } else {
            selectedLocation = $(this).val();
            setDataForLocation($(this).val());
        }
    });

    var currentLocation = $('#navBarSelectedLocation').attr('data-locationid');
    if (!$('#retrieveLocation').prop('disabled')) {
        $('#retrieveLocation').val(currentLocation).change();
    }

    $('#itemCode').on('change', function() {
    	$parentRow = $(this).parents('tr');
        clearAddNewRow($parentRow);
        clearModalWindow();
        $('#addNewTax',$parentRow).attr('disabled', false);
        if ($(this).val() > 0 && $(this).val() != currentlySelectedProduct) {
            currentlySelectedProduct = $(this).val();
            selectedProduct = $(this).val();
            eb.ajax({
                type: 'POST',
                url: BASE_URL + '/productAPI/get-location-product-details',
                data: {productID: selectedProduct, locationID: selectedLocation},
                success: function(respond) {
                    var currentElem = new Array();
                    currentElem[selectedProduct] = respond.data;
                    locationProducts = $.extend(locationProducts, currentElem);
                    $('#addNewTax',$parentRow).attr('disabled', false);
                    setTaxListForProduct(selectedProduct,$parentRow);
                    $('#unitPrice').parent().addClass('input-group');

                    var baseUom;
                    for (var j in locationProducts[selectedProduct].uom) {
                        if (locationProducts[selectedProduct].uom[j].pUBase == 1) {
                            baseUom = locationProducts[selectedProduct].uom[j].uomID;
                        }
                    }


                    $('#unitPrice',$parentRow).val(locationProducts[selectedProduct].dPP).addUomPrice(locationProducts[selectedProduct].uom);
                    if (locationProducts[selectedProduct].pPDV != null && locationProducts[selectedProduct].dPEL == 1) {
                        $('#piDiscount',$parentRow).val(locationProducts[selectedProduct].pPDV).addUomPrice(locationProducts[selectedProduct].uom, baseUom);
                        $('#piDiscount',$parentRow).siblings('.uom-price-select').find('.uom_price_drop_down').attr('disabled', true);
                        $(".sign", $parentRow).text('Rs');
                        $('#piDiscount',$parentRow).attr('disabled', false);
                        $('#piDiscount',$parentRow).data('discount', 'true');
                    } else if (locationProducts[selectedProduct].pPDP != null && locationProducts[selectedProduct].dPEL == 1) {
                        $('#piDiscount',$parentRow).val(locationProducts[selectedProduct].pPDP);
                        $('#piDiscount',$parentRow).attr('disabled', false);
                        $(".sign", $parentRow).text('%');
                        $('#piDiscount',$parentRow).data('discount', 'true');
                    } else if ((locationProducts[selectedProduct].pPDP == null && locationProducts[selectedProduct].pPDV == null) || locationProducts[selectedProduct].dPEL == 0) {
                        $('#piDiscount',$parentRow).attr('disabled', true);
                        $('#piDiscount',$parentRow).data('discount', 'false');
                    }
                    $('.uomPrice',$parentRow).attr("id", "itemUnitPrice");
                    $('#qty',$parentRow).parent().addClass('input-group');
                    $('#qty',$parentRow).addUom(locationProducts[selectedProduct].uom);
                    $('.uomqty',$parentRow).attr("id", "itemQuantity");
                    $('#itemCode',$parentRow).data('PN', locationProducts[selectedProduct].pN);
                    $("#itemCode",$parentRow).data('PT', locationProducts[selectedProduct].pT);
                    $("#itemCode",$parentRow).data('PC', locationProducts[selectedProduct].pC);
                    $("#itemCode",$parentRow).data('pId', selectedProduct);
                }
            });

        }
    });

    $('#documentReference').on('click', function(e) {
        e.preventDefault();
        if ($('#retrieveLocation').val()) {
            $('#documentReferenceModal').modal('show');
            $('#retrieveLocation').attr('disabled', 'disabled');
        } else {
            $('#documentReferenceModal').modal('hide');
            p_notification(false, eb.getMessage('ERR_PLEASE_SELECT_LOCTION_PI'));
        }
    });

    $('#documentTable').on('change', '.documentTypeId', function() {
        var $dropDownIDParent = $(this).parents('tr');
        var documentTypeID = $(this).val();
        loadDocumentId($dropDownIDParent, documentTypeID);
    });

    function loadDocumentId($dropDownIDParent, documentTypeID) {
        var dropDownID = '.documentId';
        var locationID = $('#retrieveLocation').val();
        var url = '';
        if (documentTypeID == 1) {
            url = '/invoice-api/searchSalesInvoicesForDocumentDropdown';
        } else if (documentTypeID == 2) {
            url = '/quotation-api/searchAllQuotationForDropdown';
        } else if (documentTypeID == 3) {
            url = '/api/salesOrders/search-sales-orders-for-dropdown';
        } else if (documentTypeID == 4) {
            url = '/delivery-note-api/searchDeliveryNoteForDocumentDropdown';
        } else if (documentTypeID == 7) {
            url = '/customerPaymentsAPI/searchPaymentsForDocumentDropdown';
        } else if (documentTypeID == 9) {
            url = '/api/po/search-all-open-po-for-dropdown';
        } else if (documentTypeID == 10) {
            url = '/api/grn/search-all-grn-for-dropdown';
        } else if (documentTypeID == 12) {
            url = '/api/pi/search-allPV-for-dropdown';
        } else if (documentTypeID == 15) {
            url = '/transferAPI/search-transfer-for-dropdown';
        } else if (documentTypeID == 19) {
            url = '/job-api/searchAllJobsForDropdown';
        } else if (documentTypeID == 20) {
            url = '/expense-purchase-invoice-api/getPaymentVoucherForDropDown';
        } else if (documentTypeID == 22) {
            url = '/project-api/searchProjectsForDropdown';
        } else if (documentTypeID == 23) {
            url = '/api/activity/get-all-activities-for-dropdown';
        }
        $('div.bootstrap-select.documentId', $dropDownIDParent).remove();
        $('select.selectpicker.documentId', $dropDownIDParent).show().removeData().empty();
        loadDropDownFromDatabase(url, locationID, 0, dropDownID, $dropDownIDParent);
    }

    function updateDocTypeDropDown() {
        $allRow = $('tr.documentRow.editable');
        $allRow.find('option').removeAttr('disabled')
        $.each(documentTypeArray, function(index, value) {
            $allRow.find(".documentTypeId option[value='" + value + "']").attr('disabled', 'disabled');
        });
        $allRow.find('.documentTypeId').selectpicker('render');
        $allRow.find('.documentTypeId').selectpicker('refresh');
    }

    $('#documentTable').on('click', '.saveDocument', function(e) {
        e.preventDefault();
        var $row = $(this).parents('tr');
        if (validateDocumentTypeRow($row)) {
            $row.removeClass('editable');
            documentTypeArray.push($row.find('.documentTypeId').val());
            $row.find('.documentTypeId').attr('disabled', true);
            $row.find('.documentId').attr('disabled', true);
            $row.find('.saveDocument').addClass('hidden');
            $row.find('.deleteDocument').removeClass('hidden');
            var $cloneRow = $($('.sample').clone()).removeClass('sample hidden').addClass('documentRow editable');

            $cloneRow.find('.documentTypeId').selectpicker().attr('data-liver-search', true);
            $cloneRow.insertBefore($row);

            updateDocTypeDropDown();
        }
    });

    $('#documentTable').on('click', '.deleteDocument', function(e) {
        e.preventDefault();
        var $row = $(this).parents('tr');
        var documentTypeID = $row.find('.documentTypeId').val();
        //this is for remove documentTypeID from the documentType array
        documentTypeArray = jQuery.grep(documentTypeArray, function(value) {
            return value != documentTypeID;
        });
        $row.remove();
        updateDocTypeDropDown();
    });

    $('#documentTable').on('click', '.editDocument', function(e) {
        e.preventDefault();
        var $row = $(this).parents('tr');
        $row.find('.documentId').attr('disabled', false);
        $row.find('.updateDocument').removeClass('hidden');
        $row.find('.editDocument').addClass('hidden');
    });

    $('#documentTable').on('click', '.updateDocument', function(e) {
        e.preventDefault();
        var $row = $(this).parents('tr');
        if (validateDocumentTypeRow($row)) {
            $row.find('.documentTypeId').attr('disabled', true);
            $row.find('.documentId').attr('disabled', true);
            $row.find('.updateDocument').addClass('hidden');
            $row.find('.editDocument').removeClass('hidden');
        }
    });

    function validateDocumentTypeRow($row) {
        var flag = true;
        var documentTypeID = $row.find('.documentTypeId').val();
        var documentID = $row.find('.documentId').val();
        if (documentTypeID == '' || documentTypeID == null) {
            p_notification(false, eb.getMessage('ERR_PO_DOC_TYPE_SELECT'));
            flag = false;
        } else if (documentID == '' || documentID == null) {
            p_notification(false, eb.getMessage('ERR_PO_DOC_ID_SELECT'));
            flag = false;
        }
        return flag;
    }

    $('#refreshStartBy').on('click', function() {
        poSelected = false;
        grnSelected = false;
        selectedGrn = '';
        $('#supplier').prop('disabled', false);
        $('#supplier').empty().selectpicker('refresh');
        $('#grn').empty().val('').selectpicker('refresh');
        $('#po').empty().val('').selectpicker('refresh');
        $('#retrieveLocation').prop('disabled', false);
        $('#retrieveLocation').val('').selectpicker('render');
        $('#startByCode').val('');
        $('#startByCode').prop('disabled', false);
        $('#startByCode').val('');
        $('#deliveryDate').prop('disabled', false);
        $('#deliveryDate').val('');
        $('#supplierReference').prop('disabled', false);
        $('#supplierReference').val('');
        $('#comment').prop('disabled', false);
        $('#comment').val('');
        $('#retrieveLocation').prop('disabled', false);
        $('#retrieveLocation').val('');
        $('#piNo').val('');
        $('.tempGrnPro').remove();
        $('.tempPoPro').remove();
        selectedSupplier = '';
        selectedLocation = '';
        $('#deliveryCharge').val('');
        $('.deliCharges').addClass('hidden');
        $('#deliveryChargeEnable').prop('checked', false);
        clearProductScreen();
        $('#productAddOverlay').addClass('overlay');
        $('#addSupplierBtn').prop('disabled', false);
        $('#addSupplierBtn').unbind('click');
        $('#addNewItemTr').removeClass('hidden');
    });

    $('#addItem').on('click', function() {
        var row = $(this).parents('tr');
        var uomqty = $("#qty").siblings('.uomqty').val();
        $("#qty").siblings('.uomqty').change();
        $('.tempy').remove();
        idNumber = 1;
        quon = 0;
        $('#prefix').val('');
        //validate same item usage
        var sameItemFlag = false;
        for (var i in products) {
            if(selectedProduct == products[i].pID && toFloat($('#unitPrice').val()) == toFloat(products[i].pUnitPrice) 
                && toFloat($('#piDiscount').val()) == toFloat(products[i].pDiscount)){
                sameItemFlag = true;

            }
        }


        if(sameItemFlag){
            p_notification(false, eb.getMessage('ERR_SAME_ITEM_IN_PI'));
            return false;
        }


        var issDate = eb.convertDateFormat('#issueDate');

        var expD = $('#expire_date_autoFill').val('').datepicker().on('changeDate', function(ev) {
            expD.hide();
        }).data('datepicker');
        $('#quontity').val('');
        $('#startNumber').val('');
        $('#batch_code').val('');
        $('#wrtyDays').val('');
        $('#wrtyType').val('');
        $('#mf_date_autoFill').val('');
        var mfD = $('#mf_date_autoFill').datepicker({
            onRender: function(date) {
                return date.valueOf() > issDate.valueOf() ? 'disabled' : '';
            }
        }).on('changeDate', function(ev) {
            mfD.hide();
        }).data('datepicker');
        $('#temp_1').find('input[name=submit]').attr('disabled', false);
        $('#temp_1').find('input[name=prefix]').attr('disabled', false);
        $('#temp_1').find('input[name=startNumber]').attr('disabled', false);
        $('#temp_1').find('input[name=quontity]').attr('disabled', false);
        $('#temp_1').find('input[name=expireDateAutoFill]').attr('disabled', false);
        $('#temp_1').find('input[name=batch_code]').attr('disabled', false);
        $('#temp_1').find('input[name=mfDateAutoFill]').attr('disabled', false);
        $('#temp_1').find('input[name=wrtyDays]').attr('disabled', false);

        selectedProductUom = $("#qty").siblings('.uom-select').children('button').find('.selected').data('uomID');
        if (typeof (selectedProduct) == 'undefined' || selectedProduct == '') {
            p_notification(false, eb.getMessage('ERR_GRN_SELECT_PROD'));
        }
        else if (typeof (selectedProductUom) == 'undefined' || selectedProductUom == '') {
            p_notification(false, eb.getMessage('ERR_GRN_SELECT_UOM'));
        }
        else {
            var checkingPrice = toFloat($('#unitPrice').val());
            if ($('#unitPrice').val() == '') {
                p_notification(false, eb.getMessage('ERR_GRN_ENTER_UNITPRI'));
                $('#unitPrice').focus();
            } else if ($('#itemCode').data('PT') != 2 && $('#qty').val() == '') {
                p_notification(false, eb.getMessage('ERR_GRN_ENTER_QUANTITY'));
            } else if (((uomqty == 0) || (uomqty == '')) && $('#itemCode').data('PT') != 2) {
                p_notification(false, eb.getMessage('ERR_GRN_ENTER_QUANTITY'));
            }
            /*else if (products.indexOf(_.findWhere(products, {'locationPID': locationProducts[selectedProduct].lPID, 'pUnitPrice': checkingPrice})) >= 0) {
                p_notification(false, eb.getMessage('ERR_GRN_ENTER_SAMEPROD_SAMEPRIC'));
                $('#unitPrice').focus();
            }*/
             else {
                clearModalWindow();
                $('.cloneSerials').remove();
                $('#serialSample').children().children('#newSerialNumber').removeClass('serialNumberList');
                $('#serialSample').children().children('#newSerialBatch').removeClass('serialBatchList');
                $('#serialSample').children().children('#newSerialBatch').prop('disabled', false);
                if ($('#itemCode').data('PT') == 2) {
                    locationProducts[selectedProduct].bP = 0;
                    locationProducts[selectedProduct].sP = 0;
                }
                if (locationProducts[selectedProduct].bP == 1) {
                    $('#batchAddScreen').removeClass('hidden');
                    $('#newBatchMDate').addClass('white_bg');
                    $('#newBatchEDate').addClass('white_bg');
                }
                if (locationProducts[selectedProduct].sP == 1) {
                    $('#serialAddScreen').removeClass('hidden');
                }
                if (locationProducts[selectedProduct].sP == 1 && locationProducts[selectedProduct].bP == 0) {
                    $('#temp_1').find('input[name=mfDateAutoFill]').attr('disabled', true).removeClass('white_bg');
                    $('#temp_1').find('input[name=batch_code]').closest('td').hide();
                    $('#btch_code').hide();
                    $('#bCodeT').hide();
                    $('#bCode').hide();
                    $('.price_field').hide();
                    $('#temp_1').find('input[name=expireDateAutoFill]').addClass('white_bg');
                } else {
                    $('#temp_1').find('input[name=batch_code]').closest('td').show();
                    $('#btch_code').show();
                    $('.price_field').show();
                    $('#bCodeT').show();
                    $('#bCode').show();
                }

                if (locationProducts[selectedProduct].sP == 1 && locationProducts[selectedProduct].bP == 1) {
                    $('#batchAddScreen').addClass('hidden');
                    $('#temp_1').find('input[name=batch_code]').attr('disabled', false);
                    $('#temp_1').find('input[name=mfDateAutoFill]').attr('disabled', false).addClass('white_bg');
                }

                if (locationProducts[selectedProduct].bP == 0 && locationProducts[selectedProduct].sP == 0) {
                    $('#unitPrice', row).trigger('focusout');
                    addNewProductRow({}, {});
                    $('#qty').parent().removeClass('input-group');
                    $('#itemCode').focus();
                } else {
                    selectedProductQuantity = uomqty;
                    if (locationProducts[selectedProduct].bP == 1) {
                        $('#piProductCodeBatch').html(locationProducts[selectedProduct].pC);
                        $('#piProductQuantityBatch').html(uomqty);
                    }
                    if (locationProducts[selectedProduct].sP == 1) {
                        $('#addNewSerial input[type=text]').val('');
                        $('#piProductCodeSerial').html(locationProducts[selectedProduct].pC);
                        $('#piProductQuantitySerial').html($('#qty').val());
                        $('#serialSample').children().children('#newSerialBatch').addClass('serialBatchList');
                        $('#serialSample').children().children('#newSerialNumber').addClass('serialNumberList');

                        if (locationProducts[selectedProduct].bP == 0) {
                            $('#serialSample').children().children('#newSerialBatch').prop('disabled', true);
                        }
                        for (i = 0; i < $('#qty').val() - 1; i++) {
                            var serialAddRowClone = $('#serialSample').clone().attr('id', i).addClass('cloneSerials');
                            $('#serialSample').children().children('#newSerialNumber').addClass('serialNumberList');
                            $('#serialSample').children().children('#newSerialBatch').addClass('serialBatchList');
                            serialAddRowClone.children().children('#newSerialNumber').addClass('serialNumberList');
                            serialAddRowClone.children().children('#newSerialBatch').addClass('serialBatchList');
                            serialAddRowClone.children().children('#newSerialEDate').attr('id', 'sEDate' + i).addClass('sEDate');
                            serialAddRowClone.children().children('#serialWarrenty').attr('id', 'sEW' + i).addClass('sEW');
                            if (locationProducts[selectedProduct].bP == 0) {
                                $('#serialSample').children().children('#newSerialBatch').prop('disabled', true);
                                serialAddRowClone.children().children('#newSerialBatch').prop('disabled', true);
                            }
                            serialAddRowClone.insertBefore('#serialSample');
                        }
                        if(selectedProductQuantity<=100){
                        	var dDate = new Date(Date.parse($('#issueDate').val()));
                        	$('.sEDate').datepicker();
                        }
                    }
                    $('#addPiProductsModal').modal('show');

                }
            }

        }
    });
   
    $('table#piProductTable>tbody').on('keyup', 'input#qty, input#unitPrice, input#piDiscount,.uomqty', function(e) {
        if (e.which == 13) {
            if ($('#itemCode').data('PT') !=2 && $('#qty').val() == 0) {
                 p_notification(false, eb.getMessage('ERR_GRN_ENTER_QUANTITY'));
            } else {
                $('#addItem', $(this).parents('tr')).trigger('click');
                e.preventDefault();
            }
        }
    });

    $('#piProductTable').on('click', 'ul.dropdown-menu, .dropdown-menu label', function(e) {
        e.stopPropagation();
    });

    $('#piProductTable').on('keyup', '#qty,#unitPrice,#piDiscount', function() {
        if ($('#itemCode').val() != '') {
            if ($(this).val().indexOf('.') > 0) {
                decimalPoints = $(this).val().split('.')[1].length;
            }
            if (this.id == 'unitPrice' && (!$.isNumeric($('#unitPrice').val()) || $('#unitPrice').val() < 0 || decimalPoints > 2)) {
                decimalPoints = 0;
                $('#unitPrice').val('');
            }
            if ((this.id == 'qty') && (!$.isNumeric($('#qty').val()) || $('#qty').val() < 0)) {
                decimalPoints = 0;
                $('#qty').val('');
            }
            if ((this.id == 'piDiscount') && (!$.isNumeric($('#piDiscount').val()) || $('#piDiscount').val() < 0 || (decimalPoints > 2))) {
                decimalPoints = 0;
                $('#piDiscount').val('');
            }
        }
    });

    $('#piProductTable').on('focusout', '#itemCode,#qty,#unitPrice,#piDiscount,.uomqty,.uomPrice,input[name=discount]', function() {
        var checkedTaxes = Array();
        var row = $(this).parents('tr');
        $('input:checkbox.addNewTaxCheck', $(this).parents('tr')).each(function() {
            if (this.checked) {
                var cliTID = this.id;
                var tTaxID = cliTID.split('_')[1];
                checkedTaxes.push(tTaxID);
            }
        });
        var productID = $('#itemCode', $(this).parents('tr')).data('pId');
        if (productID) {
            productID = productID;
        } else {
            productID = row.attr('id').split("_");
            productID = productID[1];            
        }
        
        var qty = $("input[name='qty']", $(this).parents('tr')).val();
        var unitPrice = $("input[name='unitPrice']", $(this).parents('tr')).val();
        var uP = $(this).parents('tr').find('.uomPrice').val();
        if (unitPrice != "" && uP != "") {
            $("input[name='unitPrice']", $(this).parents('tr')).val(parseFloat(unitPrice));
            // $(this).parents('tr').find('.uomPrice').val(parseFloat(unitPrice));
        }
        var discount = $("input[name='discount']", $(this).parents('tr')).val();
        if ($('#itemCode', row).data('PT') == 2 && qty == 0) {
            qty = 1;
        }
        var discountType;
        if (locationProducts[productID]) {
            if (locationProducts[productID].pPDP != null) {
                discountType = "Per";
            } else if (locationProducts[productID].pPDV != null) {
                discountType = "Val";
            } else {
               discountType = "Non";
            }
            if (discountType == "Val") {
                var newDiscount = validateDiscount(productID,discountType,discount, unitPrice);
                var tmpItemCost = calculateTaxFreeItemCostByValueDiscount(unitPrice, qty, newDiscount);
                if (tmpItemCost < 0) {
                    tmpItemCost = 0;
                }
            } else if (discountType == "Per") {
                var newDiscount = validateDiscount(productID,discountType,discount, unitPrice);
                var tmpItemCost = calculateTaxFreeItemCost(unitPrice, qty, newDiscount);
            } else {
                newDiscount = 0;
                var tmpItemCost = calculateTaxFreeItemCost(unitPrice, qty, newDiscount);
            }

            if ($('#total_discount_rate').val() != '' && $('input[name=discount_type]:checked').val() == 'presentage') {
                var invoiceDiscountValue = $('#total_discount_rate').val();
                tmpItemCost -= (tmpItemCost * (toFloat(invoiceDiscountValue) / toFloat(100)));
            }

            $("input[name='discount']", $(this).parents('tr')).val(newDiscount);
            currentItemTaxResults = calculateItemCustomTax(tmpItemCost, checkedTaxes);
            currentTaxAmount = (currentItemTaxResults == null) ? null : currentItemTaxResults.tTA;
            var itemCost = toFloat(tmpItemCost) + toFloat(currentTaxAmount);
            $('.itemLineTotal', $(this).parents('tr')).html(accounting.formatMoney(itemCost));
            if (productID != undefined) {
                var currentEl = new Array();
                currentEl[productID+'_'+unitPrice] = itemCost;
                productLineTotal = $.extend(productLineTotal, currentEl);
            }
        } else {
            $("input[name='discount']", $(this).parents('tr')).val(discount);
            var tmpItemCost = calculateTaxFreeItemCost(unitPrice, qty, discount);
            currentItemTaxResults = calculateItemCustomTax(tmpItemCost, checkedTaxes);
            currentTaxAmount = (currentItemTaxResults == null) ? null : currentItemTaxResults.tTA;
            var itemCost = toFloat(tmpItemCost) + toFloat(currentTaxAmount);
            $('.itemLineTotal', $(this).parents('tr')).html(accounting.formatMoney(itemCost));
            if (productID != undefined) {
                var currentEl = new Array();
                currentEl[productID+'_'+unitPrice] = itemCost;
                productLineTotal = $.extend(productLineTotal, currentEl);
            }
        }
    });

    $('#piProductTable').on('change', '.taxChecks.addNewTaxCheck', function() {
        var allchecked = false;
        var $parentTaxSet = $(this).parents("td.taxSet");
        $parentTaxSet.find('input:checkbox.addNewTaxCheck').each(function() {
            if (this.checked && $(this).prop('disabled') === false) {
                allchecked = true;
            }
        });

        if (allchecked == false) {
            if ($(this).parents('tr').hasClass('tempPoPro')) {
                $parentTaxSet.find('#poTaxApplied').removeClass('glyphicon glyphicon-check');
                $parentTaxSet.find('#poTaxApplied').addClass('glyphicon glyphicon-unchecked');
            } else if ($(this).parents('tr').hasClass('tempGrnPro')) {
                $parentTaxSet.find('#poTaxApplied').removeClass('glyphicon glyphicon-check');
                $parentTaxSet.find('#poTaxApplied').addClass('glyphicon glyphicon-unchecked');
            } else if ($(this).parents('tr').hasClass('tempPIPro')) {
                $parentTaxSet.find('#poTaxApplied').removeClass('glyphicon glyphicon-check');
                $parentTaxSet.find('#poTaxApplied').addClass('glyphicon glyphicon-unchecked');
            }else {
                $parentTaxSet.find('#taxApplied').removeClass('glyphicon glyphicon-check');
                $parentTaxSet.find('#taxApplied').addClass('glyphicon glyphicon-unchecked');
            }
        } else {
            if ($(this).parents('tr').hasClass('tempPoPro')) {
                $parentTaxSet.find('#poTaxApplied').removeClass('glyphicon glyphicon-unchecked');
                $parentTaxSet.find('#poTaxApplied').addClass('glyphicon glyphicon-check');
            } else if ($(this).parents('tr').hasClass('tempGrnPro')) {
                $parentTaxSet.find('#poTaxApplied').removeClass('glyphicon glyphicon-unchecked');
                $parentTaxSet.find('#poTaxApplied').addClass('glyphicon glyphicon-check');
            } else if ($(this).parents('tr').hasClass('tempPIPro')) {
                $parentTaxSet.find('#poTaxApplied').removeClass('glyphicon glyphicon-unchecked');
                $parentTaxSet.find('#poTaxApplied').addClass('glyphicon glyphicon-check');
            } else {
                $parentTaxSet.find('#taxApplied').removeClass('glyphicon glyphicon-unchecked');
                $parentTaxSet.find('#taxApplied').addClass('glyphicon glyphicon-check');
            }
        }

        $(this).parents('tr').find(".uomPrice").trigger(jQuery.Event("focusout"));
        $(this).parents('tr').find("input[name='qty']").trigger(jQuery.Event("focusout"));
    });

    $('#piProductTable').on('click', '#selectAll', function() {
        $(this).parents('.dropdown-menu').find('input:checkbox.addNewTaxCheck').each(function() {
            if (!this.checked) {
                this.checked = true;
            }
        });

        $(this).parents('.dropdown-menu').find('input:checkbox.addNewTaxCheck').trigger(jQuery.Event("change"));

    });

    $('#piProductTable').on('click', '#deselectAll', function() {
        $(this).parents('.dropdown-menu').find('input:checkbox.addNewTaxCheck').each(function() {
            if (this.checked) {
                this.checked = false;
            }
        });

        $(this).parents('.dropdown-menu').find('input:checkbox.addNewTaxCheck').trigger(jQuery.Event("change"));

    });

    $('#piProductTable').on('change', '.addNewUomR', function() {
        $('#uomAb').html(locationProducts[selectedProduct].uom[$(this).val()].uA);
        selectedProductUom = $(this).val();
    });

    $('#deliveryChargeEnable').on('click', function() {
        if (this.checked) {
            $('.deliCharges').removeClass('hidden');
            $('#deliveryCharge').focus();
        }
        else {
            $('#deliveryCharge').val('');
            $('.deliCharges').addClass('hidden');
        }
        setPiTotalCost();
    });

    $('#deliveryCharge').on('change keyup', function() {
        if (!$.isNumeric($('#deliveryCharge').val()) || $(this).val() < 0) {
            $(this).val('');
            setPiTotalCost();
        } else {
            setPiTotalCost();
        }
    });

    $('#piProductTable').on('click', '.piDeleteProduct', function() {
        var deletePTrID = $(this).closest('tr').attr('id');
        var uniqID = $(this).closest('tr').attr('data-grnproid');
        var uniqID2 = $(this).closest('tr').attr('data-pisubid');
        var deletePID = deletePTrID.split("_");
        deletePID = deletePID[1];
        var deleteItemPrice = accounting.unformat($(this).closest('tr').find('#unit').html());
        var deleteProductIndex = products.indexOf(_.findWhere(products, {'locationPID': deletePID, 'pUnitPrice': deleteItemPrice}));
        bootbox.confirm('Are you sure you want to remove this product ?', function(result) {
            if (result == true) {
                products.splice(deleteProductIndex, 1);
                $('#form_row tr').each(function(){
                    if (Object.keys(selectedGrn).length > 0) {
                        if($(this).attr('data-grnproid') == uniqID){
                            $(this).remove();
                        }
                    } else {
                        if($(this).attr('data-pisubid') == uniqID2){
                            $(this).remove();
                        }
                    }
                });
                setPiTotalCost();
                setTotalTax();
                setItemViseTotal();
            }
        });
    });

    $('#piProductTable').on('click', '.piViewProducts', function() {
        var viewPTrID = $(this).closest('tr').attr('id');
        if (piEditflag) {
            var viewPID = viewPTrID.split('tr0_')[1].trim();
        } else {
            var viewPID = viewPTrID.split('tr_')[1].trim();
        }
        var viewItemPrice = accounting.unformat($(this).closest('tr').find('#unit').html());
        var viewProductIndex = products.indexOf(_.findWhere(products, {'locationPID': viewPID, 'pUnitPrice': viewItemPrice}));
        var vPID = products[viewProductIndex].pID;

        if (locationProducts[vPID].bP == 0 && locationProducts[vPID].sP == 0) {
            p_notification(false, eb.getMessage('ERR_GRN_SUBPROD'));
        } else {
            setProductsView(viewProductIndex);
            $('#viewPiSubProductsModal').modal('show');
        }

    });

    $('#showTax').on('click', function() {
        if (this.checked) {
            if (!jQuery.isEmptyObject(products)) {
                $('#totalTaxShow').removeClass('hidden');
            } else {
                p_notification(false, eb.getMessage('ERR_GRN_PROD_TAX'));
            }

        } else {
            $('#totalTaxShow').addClass('hidden');
        }

    });

    $('#piCancel').on('click', function() {
        window.location.reload();
    });

    $('#grnForm').on('submit', function(e) {
        e.preventDefault();
        var activeElementId = document.activeElement.id;
        var selectedid = "#" + document.activeElement.id;
        var tableId = $(selectedid).closest('table').attr("id");

        var documentReference = {};
        var flagfalse = false;
        $('.documentTypeBody > tr').each(function() {
            if ($(this).hasClass('documentRow')) {
                var documentTypeID = $(this).   find('.documentTypeId').val();
                var documentID = $(this).find('.documentId').val();
                if (!(documentTypeID == null || documentTypeID == '')) {
                    if (documentID != null) {
                        documentReference[documentTypeID] = documentID;
                    } else {
                        flagfalse = true;
                    }
                }
            }
        });

        var productRowFlag = false;
        $('#add-new-item-row > tr').each(function(){
        	if(this.id != 'addNewItemTr'){
        		productRowFlag = true;
        	}
        });

        if (flagfalse) {
            p_notification(false, eb.getMessage('ERR_PLEASE_FILL_ALL_DOCUMENT_REFERENCE'));
        } else if (grnSelected && copyFromChooser == 'GRN' && grn == ''){
            p_notification(false, eb.getMessage('ERR_PR_SELECTGRNCODE'));
        } else if(poSelected && copyFromChooser == 'PO' && po == ''){
            p_notification(false, eb.getMessage('ERR_PO_SELECTPOCODE'));
        } else if (typeof (selectedSupplier) == 'undefined' || selectedSupplier == '' || selectedSupplier == 0) {
            p_notification(false, eb.getMessage('ERR_GRN_SELECT_SUPP'));
        } else if (typeof (selectedLocation) == 'undefined' || selectedLocation == '') {
            p_notification(false, eb.getMessage('ERR_GRN_SELECT_LOCAT'));
        }
        else if ($('#deliveryDate').val() == '') {
            p_notification(false, eb.getMessage('ERR_PI_VALID_DUE'));
        }
        else if (eb.convertDateFormat('#issueDate') > eb.convertDateFormat('#deliveryDate')) {
            p_notification(false, eb.getMessage('ERR_PI_INV_DUE'));
            $('#deliveryDate').focus();
        } else if ($('#piNo').val() == '') {
            p_notification(false, eb.getMessage('ERR_GRN_SELECT_LOCAT_REFNO'));
        } else if (jQuery.isEmptyObject(products)) {
            if (tableId != 'piProductTable') {
                p_notification(false, eb.getMessage('ERR_PI_PRPOADD'));
            }
        }else if(productRowFlag){
        	p_notification(false, eb.getMessage('ERR_PI_PRPOADD_ROW'));
        }else {
            var gFT = 0;
            for (var i in products) {
                gFT += toFloat(products[i].pTotal);
            }
            var sID = selectedSupplier;
            var showTax = 0;
            if ($('#showTax').is(':checked')) {
                showTax = 1;
            }
            if ($('#deliveryCharge').val() != '') {
                gFT +=toFloat($('#deliveryCharge').val());
            }

            if ($('#disc_value').is(':checked')) {
                gFT -= parseFloat($('#total_discount_rate').val());
            }

            piPostData = {
                sID: sID,
                piC: $('#piNo').val(),
                dD: $('#deliveryDate').val(),
                iD: $('#issueDate').val(),
                sR: $('#supplierReference').val(),
                rL: selectedLocation,
                cm: $('#comment').val(),
                pr: products,
                dC: $('#deliveryCharge').val(),
                fT: gFT,
                sT: showTax,
                lRID: $('#locRefID').val(),
                startByGrnID: selectedGrn,
                startByPoID: selectedPo,
                pT: $('#paymentTerm').val(),
                multipleGrnCopyFlag: multipleGrnCopyFlag,
                documentReference: documentReference,
                dimensionData: dimensionData,
                ignoreBudgetLimit : ignoreBudgetLimitFlag,
                purchaseInvoiceWiseTotalDiscountType: $('input[name=discount_type]:checked').val(),
                purchaseInvoiceWiseTotalDiscountRate: $('#total_discount_rate').val(),
            };
            if (activeElementId == 'piSave') {
                $('#piSave,#journalEntryView').prop('disabled', true);
                if(piEditflag){
                    piPostData.efectedPiID = efectedPiID;
                    piPostData.piC = efectedPiCode;
                    updatePI(piPostData);
                } else {
                    savePI(piPostData);
                }
            } else if (activeElementId == 'journalEntryView') {
                piPostData.efectedPiID = efectedPiID;
                getPurchaseInvoiceJEs(piPostData);
            }
        }
    });

    function updatePI(piPostData)
    {
        if(piEditflag) {
            var existingAttachemnts = {};
            var deletedAttachments = {};
            $.each(uploadedAttachments, function(index, val) {
                if (!deletedAttachmentIds.includes(val['documentAttachemntMapID'])) {
                    existingAttachemnts[index] = val;
                } else {
                    deletedAttachments[index] = val;
                }
            });
        }

        eb.ajax({
            type: 'POST',
            url: BASE_URL + '/api/pi/updatePi',
            data: piPostData,
            success: function(respond) {
                if (respond.status == true) {
                    p_notification(respond.status, respond.msg);
                    if(respond.data == null) {
                        var documentID = "same";
                    } else {
                        var documentID = respond.data.piID;
                    }

                    var fileInput = document.getElementById('editDocumentFiles');
                    var form_data = false;
                    if (window.FormData) {
                        form_data = new FormData();
                    }
                    form_data.append("documentID", documentID);
                    form_data.append("documentTypeID", 12);
                    form_data.append("updateFlag", true);
                    form_data.append("editedDocumentID", efectedPiID);
                    form_data.append("documentCode", efectedPiCode);
                    form_data.append("deletedAttachmentIds", deletedAttachmentIds);
                    
                    if(fileInput.files.length > 0) {
                        for (var i = 0; i < fileInput.files.length; i++) {
                            form_data.append("files[]", fileInput.files[i]);
                        }
                    }

                    eb.ajax({
                        url: BASE_URL + '/store-files',
                        type: 'POST',
                        processData: false,
                        contentType: false,
                        data: form_data,
                        success: function(res) {
                        }
                    });

                    window.location.assign(BASE_URL + "/pi/list")
                } else {
                    if (respond.data == "NotifyBudgetLimit") {
                        bootbox.confirm(respond.msg+' ,Are you sure you want to update ?', function(result) {
                            if (result == true) {
                                ignoreBudgetLimitFlag = true;
                                piPostData.ignoreBudgetLimit = true;
                                updatePI(piPostData);
                            } else {
                                setTimeout(function(){ 
                                    location.reload();
                                }, 3000);
                            }
                        });
                    } else {
                        p_notification(respond.status, respond.msg);
                        $('#piSave,#journalEntryView').prop('disabled', false);
                    }
                }
            },
            async: false
        });
    }

    function savePI(piPostData)
    {
        eb.ajax({
            type: 'POST',
            url: BASE_URL + '/api/pi/savePi',
            data: piPostData,
            success: function(respond) {
                if (respond.status == true) {
                    p_notification(respond.status, respond.msg);
                    var fileInput = document.getElementById('documentFiles');
                    if(fileInput.files.length > 0) {
                        var form_data = false;
                        if (window.FormData) {
                            form_data = new FormData();
                        }
                        form_data.append("documentID", respond.data.piID);
                        form_data.append("documentTypeID", 12);
                        
                        for (var i = 0; i < fileInput.files.length; i++) {
                            form_data.append("files[]", fileInput.files[i]);
                        }

                        eb.ajax({
                            url: BASE_URL + '/store-files',
                            type: 'POST',
                            processData: false,
                            contentType: false,
                            data: form_data,
                            success: function(res) {
                            }
                        });
                    }

                    documentPreview(BASE_URL + '/pi/preview/' + respond.data.piID, 'documentpreview', "/api/pi/send-pi-email", function($preview) {
                        $preview.on('hidden.bs.modal', function() {
                            if (!$("#preview:visible").length) {
                                window.location.reload();
                            }
                        });
                    });
                } else {
                    if (respond.data == "NotifyBudgetLimit") {
                        bootbox.confirm(respond.msg+' ,Are you sure you want to save ?', function(result) {
                            if (result == true) {
                                ignoreBudgetLimitFlag = true;
                                piPostData.ignoreBudgetLimit = true;
                                savePI(piPostData);
                            } else {
                                setTimeout(function(){ 
                                    location.reload();
                                }, 3000);
                            }
                        });
                    } else {
                        p_notification(respond.status, respond.msg);
                        $('#piSave,#journalEntryView').prop('disabled', false);
                    }
                }
            },
            async: false
        });
    }

    function getPurchaseInvoiceJEs(piPostData)
    {
        eb.ajax({
            type: 'POST',
            url: BASE_URL + '/api/pi/get-purchase-invoice-journal-entries',
            data: piPostData,
            success: function(respond) {
                if (respond.status == true) {
                    setJournlaEntryData(respond.data);
                    $('#editJournalEntry').modal('show');
                } else {
                    if (respond.data == "NotifyBudgetLimit") {
                        bootbox.confirm(respond.msg+' ,Are you sure you want to continue ?', function(result) {
                            if (result == true) {
                                ignoreBudgetLimitFlag = true;
                                piPostData.ignoreBudgetLimit = true;
                                getPurchaseInvoiceJEs(piPostData);
                            } else {
                                setTimeout(function(){ 
                                    location.reload();
                                }, 3000);
                            }
                        });
                    } else {
                        p_notification(false, respond.msg);
                    }
                }
            },
            async: false
        });
    }

    function setJournlaEntryData(JEData){
        $('#journalEntryDate').val(JEData.journalEntryDate).attr('disabled',true);
        $('#journalEntryCode').val(JEData.journalEntryCode).attr('disabled',true);
        $('#journalEntryComment').val(JEData.journalEntryComment);

        $("#journal-entry-body").find('.add_acc').remove();
        $.each(JEData.journalEntryAccounts, function(index, value){
            var $newRow = $($('.temp_acc').clone()).removeClass('temp_acc hidden').addClass('add_acc').appendTo('#journal-entry-body');
            loadDropDownFromDatabase('/accounts-api/search-active-accounts-for-dropdown', "", "", ".accountID", $newRow);
            $('#accountID',$newRow).append("<option value='"+value.financeAccountsID+"'>"+value.financeAccountsName+"</option>");
            $('#accountID',$newRow).val(value.financeAccountsID).selectpicker('refresh');
            $newRow.find('.creditAmount').val(value.journalEntryAccountsCreditAmount).attr('disabled',true);
            $newRow.find('.debitAmount').val(value.journalEntryAccountsDebitAmount).attr('disabled',true);
            $newRow.find('.memo').val(value.journalEntryAccountsMemo);
        });
    }

    $('#savePiWithJournalEntry').on('click', function() {
        savePiWithJournalEntry();
    });


    function savePiWithJournalEntry(){
        var journalEntryAccounts = {};
        if (validatePiJournalEntry()) {
            $i = 0;
            $('#journal-entry-body > tr').each(function(){
                if($(this).hasClass('add_acc')){
                    journalEntryAccounts[$i] = {
                        financeAccountsID: $(this).find('.accountID').val(),
                        financeAccountsName: $(this).find("option:selected").text(),
                        journalEntryAccountsCreditAmount: $(this).find('.creditAmount').val(),
                        journalEntryAccountsDebitAmount: $(this).find('.debitAmount').val(),
                        journalEntryAccountsMemo: $(this).find('.memo').val(),
                    };
                    $i++;
                }
            });

            var journalEntryData ={
                journalEntryDate: $('#journalEntryDate').val(),
                journalEntryCode: $('#journalEntryCode').val(),
                journalEntryComment: $('#journalEntryComment').val(),
                journalEntryAccounts: journalEntryAccounts,
            };
            piPostData.journalEntryData = journalEntryData;
            piPostData.ignoreBudgetLimit = ignoreBudgetLimitFlag;
            if(piEditflag){
                piPostData.efectedPiID = efectedPiID;
                piPostData.piC = efectedPiCode;
                eb.ajax({
                    type: 'POST',
                    url: BASE_URL + '/api/pi/updatePi',
                    data: piPostData,
                    success: function(respond) {
                        if (respond.status == true) {
                            p_notification(respond.status, respond.msg);
                            window.location.assign(BASE_URL + "/pi/list")
                        } else {
                            if (respond.data == "NotifyBudgetLimit") {
                                bootbox.confirm(respond.msg+' ,Are you sure you want to update ?', function(result) {
                                    if (result == true) {
                                        ignoreBudgetLimitFlag = true;
                                        savePiWithJournalEntry();
                                    } else {
                                        setTimeout(function(){ 
                                            location.reload();
                                        }, 3000);
                                    }
                                });
                            } else {
                                p_notification(respond.status, respond.msg);
                                $('#piSave,#journalEntryView').prop('disabled', false);
                            }
                        }
                    },
                    async: false
                });
            } else {                
                eb.ajax({
                    type: 'POST',
                    url: BASE_URL + '/api/pi/savePi',
                    data: piPostData,
                    success: function(respond) {
                        if (respond.status == true) {
                            $('#piSave,#journalEntryView').prop('disabled', true);
                            $('#editJournalEntry').modal('hide');
                            p_notification(respond.status, respond.msg);
                            documentPreview(BASE_URL + '/pi/preview/' + respond.data.piID, 'documentpreview', "/api/pi/send-pi-email", function($preview) {
                                $preview.on('hidden.bs.modal', function() {
                                    if (!$("#preview:visible").length) {
                                        window.location.reload();
                                    }
                                });
                            });
                        } else {
                            if (respond.data == "NotifyBudgetLimit") {
                                bootbox.confirm(respond.msg+' ,Are you sure you want to save ?', function(result) {
                                    if (result == true) {
                                        ignoreBudgetLimitFlag = true;
                                        savePiWithJournalEntry(piPostData);
                                    } else {
                                        setTimeout(function(){ 
                                            location.reload();
                                        }, 3000);
                                    }
                                });
                            } else {
                                p_notification(respond.status, respond.msg);
                                $('#piSave,#journalEntryView').prop('disabled', false);
                            }
                        }
                    },
                    async: false
                });
            }
        } else {
            p_notification(false, 'Please select GL Accounts for Journal Entry.');
        }
    }

    $('#piProductTable').on('click', '.grnRemoveItem', function() {
        var tr = $(this).parents("tr");
        var removeProductID = tr.attr('id');
        $('#' + removeProductID).remove();
    });

    $('#piProductTable').on('click', '.grnAddItem', function() {

    	var currentRow = $(this).parents('tr');
    	var newqty = parseFloat($("input[name='qty']",currentRow).val());
        var oldqty = parseFloat($("input[name='qty']",currentRow).data('qty'));
    	var productCode = $(".grnItemCode",currentRow).data('PC');
        thisIsGrnAddFlag = true;
        var tr = $(this).parents("tr");
        var addProductID = tr.attr('id').split('_')[2].trim();
        var currentPID = tr.attr('data-pid');
        grnProID = addProductID;
        addingGrnPro = addProductID;
        grnProductID = grnProducts[addingGrnPro].gProId;
        grnDocID = grnProducts[addingGrnPro].grnDocId;
        var grnBatchProducts = grnProducts[addingGrnPro].bP;
        var grnSerialProducts = grnProducts[addingGrnPro].sP;
        if (products[locationProducts[currentPID].lPID]) {
            p_notification(false, eb.getMessage('ERR_GRN_INSERT_PROD'));
        }else if(newqty > oldqty){
            thisIsGrnAddFlag = false;
        	p_notification(false, eb.getMessage('ERR_PI_GRN_PRODUCT_QTY_CNNT_BE_MORE_THAN_COPIED_QTY',[productCode]));
        }else if(!$.isEmptyObject(grnSerialProducts) || !$.isEmptyObject(grnBatchProducts)){
        	$('#addGrnProductsModal').data('id',currentRow.attr('id'));
        	$('#addGrnProductsModal').modal('show');
        	$('.remove_row', $('#batch_data')).remove();
        	if(!$.isEmptyObject(grnSerialProducts) && !$.isEmptyObject(grnBatchProducts)){
        		for(var i in grnSerialProducts){
        		var batchID = Object.keys(grnBatchProducts).filter(function(batchID) {return grnBatchProducts[batchID].bC === grnSerialProducts[i].sBC})[0];
        		var $clonedRow = $($('.batch_row').clone());
                    $clonedRow
                            .data('id', i)
                            .removeAttr('id')
                            .removeClass('hidden batch_row')
                            .addClass('remove_row');
                    $(".batchCode", $clonedRow)
                            .html(grnSerialProducts[i].sBC)
                            .data('id',batchID);
                    $(".serialID", $clonedRow)
                            .html(grnSerialProducts[i].sC)
                            .data('id',i);
                    $("input[name='availableQuantity']", $clonedRow).val(1);
                    $("input[name='selectedQuantity']", $clonedRow).parent().remove();
            		$clonedRow.insertBefore('.batch_row');
            		$("input[name='availableQuantity'],input[name='selectedQuantity']", $clonedRow).addUom(locationProducts[currentPID].uom);
        		}
        	}else if(!$.isEmptyObject(grnSerialProducts)){
        		for(var i in grnSerialProducts){
        		var $clonedRow = $($('.batch_row').clone());
                    $clonedRow
                            .data('id', i)
                            .removeAttr('id')
                            .removeClass('hidden batch_row')
                            .addClass('remove_row');
                    $(".serialID", $clonedRow)
                            .html(grnSerialProducts[i].sC)
                            .data('id',i);
                    $("input[name='availableQuantity']", $clonedRow).val(1);
                    $("input[name='selectedQuantity']", $clonedRow).parent().remove();
            		$clonedRow.insertBefore('.batch_row');
            		$("input[name='availableQuantity'],input[name='selectedQuantity']", $clonedRow).addUom(locationProducts[currentPID].uom);
        		}
        	}else{
        		for(var i in grnBatchProducts){
        			var $clonedRow = $($('.batch_row').clone());
        			$clonedRow
        				.data('id', i)
        				.removeAttr('id')
        				.removeClass('hidden batch_row')
        				.addClass('remove_row');
        			$(".batchCode", $clonedRow).html(grnBatchProducts[i].bC).data('id', i);
        			$("input[name='availableQuantity']", $clonedRow).val(grnBatchProducts[i].bQ);
        			$("input[name='selectedQuantityCheck']", $clonedRow).remove();
            		$clonedRow.insertBefore('.batch_row');
            		$("input[name='availableQuantity'],input[name='selectedQuantity']", $clonedRow).addUom(locationProducts[currentPID].uom);
        		}
            }
        }else{
            selectedProduct = currentPID;
        	var grnBatchProducts = grnProducts[addProductID].bP;
        	var grnSerialProducts = grnProducts[addProductID].sP;
        	for (var b in grnBatchProducts) {
        		batchProducts[grnBatchProducts[b].bC] = new batchProduct(grnBatchProducts[b].bC, grnBatchProducts[b].bQ, grnBatchProducts[b].bMD, grnBatchProducts[b].bED, grnBatchProducts[b].bW, b);
        	}
        	for (var b in grnSerialProducts) {
        		serialProducts[grnSerialProducts[b].sC] = new serialProduct(grnSerialProducts[b].sC, grnSerialProducts[b].sW, grnSerialProducts[b].sBC, grnSerialProducts[b].sED, b ,grnSerialProducts[b].sWT);
        	}
        	addNewProductRow(batchProducts, serialProducts, currentRow);
        }
    });

	$('#batch-save').on('click',function(e){
		e.preventDefault();
		serialProducts = {};
		var $batchTable = $("#addGrnProductsModal .batch-table tbody");
		var parentTrID = $('#addGrnProductsModal').data('id');
        $thisParentRow = $('#'+parentTrID, $('#add-new-item-row'));
        var ProductQty = $("input[name='qty']",$thisParentRow).val();
		var ProductName = $(".grnItemCode", $thisParentRow).data('PN');
		var totoalSubQty = 0;
		$("input[name='selectedQuantity'], input[name='selectedQuantityCheck']:checked", $batchTable).each(function() {
			var $thisSubRow = $(this).parents('tr');
            var thisSelectedQuantityByBase = $(this).val();
            if ((thisSelectedQuantityByBase).trim() != "" && isNaN(parseFloat(thisSelectedQuantityByBase))) {
                p_notification(false, eb.getMessage('ERR_PI_SUB_QUANTITY'));
                $(this).focus();
                return qtyTotal = false;
            }

            thisSelectedQuantityByBase = (isNaN(parseFloat(thisSelectedQuantityByBase))) ? 0 : parseFloat(thisSelectedQuantityByBase);

            // check if selected qty is greater than available qty
            var thisAvailableQuantityByBase = $("input[name='availableQuantity']", $thisSubRow).val();
            thisAvailableQuantityByBase = (isNaN(parseFloat(thisAvailableQuantityByBase))) ? 0 : parseFloat(thisAvailableQuantityByBase);
            if (thisSelectedQuantityByBase > thisAvailableQuantityByBase) {
                p_notification(false, eb.getMessage('ERR_PI_QUANT_CANT_BE_MORE_AVAQTY'));
                return qtyTotal = false;
                $(this).focus().select();
            }

            // if a product invoice is present, prepare array to be sent to backend
            if (thisSelectedQuantityByBase > 0) {
            	totoalSubQty += thisSelectedQuantityByBase;
                var batchCode = '';
                if ($(".batchCode", $thisSubRow).data('id')) {
                	var batchID = $(".batchCode", $thisSubRow).data('id');
                	var batchCode = $(".batchCode", $thisSubRow).html();

                	if(batchQty[batchID] != null){
                		batchQty[batchID] += thisSelectedQuantityByBase;
                	} else {
                		batchQty[batchID] = thisSelectedQuantityByBase;
                	}
                	batchProducts[batchCode] = new batchProduct(batchCode, batchQty[batchID], '', '', '', batchID);
                }
                if ($(".serialID", $thisSubRow).data('id')) {
                	var serialID = $(".serialID", $thisSubRow).data('id');
                	var serialCode = $(".serialID", $thisSubRow).html();

                	serialProducts[serialCode] = new serialProduct(serialCode, '', batchCode, '', serialID,'');
                }
            }

		});

		if(ProductQty == totoalSubQty){
			$('#addGrnProductsModal').data('id','');
			$('#addGrnProductsModal').modal('hide');
			addNewProductRow(batchProducts, serialProducts, $thisParentRow);
		}else{
            batchQty = [];
			p_notification(false, eb.getMessage('ERR_PI_TOTAL_SUB_QTY_AND_PRODUCT_QTY_SLD_EQUAL',[ProductName],[ProductName]));
		}
	});

    $('#piProductTable').on('click', '.poAddItem', function() {
        thisIsPoAddFlag = true;
        var tr = $(this).parents("tr");
        tr.find(".uomPrice").trigger('focusout');
        var addProductID = tr.attr('id').split('poPro_')[1].trim();
        addingPoPro = locationProducts[addProductID].pC;
        selectedProduct = addProductID;
        var addProductQty = tr.find("input[name='qty']").val();
        var uomqty = $(".uomqty").val();
        $('.tempy').remove();
        idNumber = 1;
        quon = 0;
        $('#prefix').val('');
        var dDate = new Date(Date.parse($('#deliveryDate').val()));
        var epd = $('#expire_date_autoFill').val('').datepicker().on('changeDate', function(ev) {
            epd.hide();
        }).data('datepicker');
        $('#quontity').val('');
        $('#startNumber').val('');
        $('#wrtyDays').val('');
        $('#batch_code').val('');
        var mfd = $('#mf_date_autoFill').val('').datepicker({
            onRender: function(date) {
                return date.valueOf() > dDate.valueOf() ? 'disabled' : '';
            }
        }).on('changeDate', function(ev) {
            mfd.hide();
        }).data('datepicker');

        $('#temp_1').find('input[name=submit]').attr('disabled', false);
        $('#temp_1').find('input[name=prefix]').attr('disabled', false);
        $('#temp_1').find('input[name=startNumber]').attr('disabled', false);
        $('#temp_1').find('input[name=quontity]').attr('disabled', false);
        $('#temp_1').find('input[name=expireDateAutoFill]').attr('disabled', false);
        $('#temp_1').find('input[name=batch_code]').attr('disabled', false);
        $('#temp_1').find('input[name=mfDateAutoFill]').attr('disabled', false);
        $('#temp_1').find('input[name=wrtyDays]').attr('disabled', false);


        clearModalWindow();
        $('.cloneSerials').remove();
        $('#serialSample').children().children('#newSerialNumber').removeClass('serialNumberList');
        $('#serialSample').children().children('#newSerialBatch').removeClass('serialBatchList');
        $('#serialSample').children().children('#newSerialBatch').prop('disabled', false);
        if (tr.find("input[name='itemCode']").data('PT') == 2) {
            locationProducts[addProductID].bP = 0;
            locationProducts[addProductID].sP = 0;
        }
        if (locationProducts[addProductID].bP == 1) {
            $('#batchAddScreen').removeClass('hidden');
        }
        if (locationProducts[addProductID].sP == 1) {
            $('#serialAddScreen').removeClass('hidden');
        }
        if (locationProducts[selectedProduct].sP == 1 && locationProducts[selectedProduct].bP == 0) {
            $('#temp_1').find('input[name=mfDateAutoFill]').attr('disabled', true).removeClass('white_bg');
            $('#temp_1').find('input[name=batch_code]').attr('disabled', true);
            $('#temp_1').find('input[name=expireDateAutoFill]').addClass('white_bg');
        }
        if (locationProducts[addProductID].bP == 1 && locationProducts[addProductID].sP == 1) {
            $('#batchAddScreen').addClass('hidden');
            $('#temp_1').find('input[name=batch_code]').attr('disabled', false);
            $('#temp_1').find('input[name=mfDateAutoFill]').attr('disabled', false).addClass('white_bg').datepicker();
            $('#temp_1').find('input[name=expireDateAutoFill]').addClass('white_bg');

        }

        if (locationProducts[addProductID].bP == 0 && locationProducts[addProductID].sP == 0) {
            addNewProductRow({}, {}, tr);
            $('#itemCode').focus();
        } else {
            selectedProductQuantity = uomqty;
            selectedProduct = addProductID;
            var addProductQty = uomqty;


            $('.cloneSerials').remove();
            $('#serialSample').children().children('#newSerialNumber').removeClass('serialNumberList');
            $('#serialSample').children().children('#newSerialBatch').removeClass('serialBatchList');
            $('#serialSample').children().children('#newSerialBatch').prop('disabled', false);
            if (tr.find("#itemCode").data('PT') == 2) {
                locationProducts[addProductID].bP = 0;
                locationProducts[addProductID].sP = 0;
            }
            if (locationProducts[addProductID].bP == 1) {
                $('#piProductCodeBatch').html(locationProducts[addProductID].pC);
                $('#piProductQuantityBatch').html(addProductQty);
            }
            if (locationProducts[addProductID].sP == 1) {
                $('#addNewSerial input[type=text]').val('');
                $('#piProductCodeSerial').html(locationProducts[addProductID].pC);
                $('#piProductQuantitySerial').html(addProductQty);
                $('#serialSample').children().children('#newSerialBatch').addClass('serialBatchList');
                $('#serialSample').children().children('#newSerialNumber').addClass('serialNumberList');
                for (i = 0; i < addProductQty - 1; i++) {
                    var serialAddRowClone = $('#serialSample').clone().attr('id', i).addClass('cloneSerials');
                    $('#serialSample').children().children('#newSerialNumber').addClass('serialNumberList');
                    $('#serialSample').children().children('#newSerialBatch').addClass('serialBatchList');
                    serialAddRowClone.children().children('#newSerialNumber').addClass('serialNumberList');
                    serialAddRowClone.children().children('#newSerialBatch').addClass('serialBatchList');
                    serialAddRowClone.children().children('#serialWarrenty').attr('id', 'sEW' + i).addClass('sEW');
                    serialAddRowClone.children().children('#newSerialEDate').attr('id', 'sEDate' + i).addClass('sEDate');
                    if (locationProducts[addProductID].bP == 0) {
                        $('#serialSample').children().children('#newSerialBatch').prop('disabled', true);
                        serialAddRowClone.children().children('#newSerialBatch').prop('disabled', true);
                    }
                    serialAddRowClone.insertBefore('#serialSample');
                }
                if(addProductQty<=100){
                	var dDate = new Date(Date.parse($('#deliveryDate').val()));
                	$('.sEDate').datepicker();
                }
            }
            $('#addPiProductsModal').modal('show');
        }
    });

///////////Add batch & serial products //////////////
    $(document).on('keyup', '#newBatchQty,#newBatchWarrenty', function() {
        if ((this.id == 'newBatchQty') && ((!$.isNumeric($('#newBatchQty').val()) || $('#newBatchQty').val() <= -1))) {
            $('#newBatchQty').val('');
        }
        if ((this.id == 'newBatchWarrenty') && ((!$.isNumeric($('#newBatchWarrenty').val()) || $('#newBatchWarrenty').val() < 0))) {
            $('#newBatchWarrenty').val('');
        }
    });
    $(document).on('keyup', '.serialWarrenty', function() {
        if (!$.isNumeric($(this).val()) || $(this).val() < 0) {
            $(this).val('');
        }
    });
    $('#addBatchItem').on('click', function() {

        if ($('#newBatchNumber').val() == null || $('#newBatchQty').val() == '' || $('#newBatchNumber').val().trim() === '') {
            p_notification(false, eb.getMessage('ERR_GRN_BATCH_INFO'));
        } else {
            var newBCode = $('#newBatchNumber').val();
            var newBQty = $('#newBatchQty').val();
            var newBMDate = $('#newBatchMDate').val();
            var newBEDate = $('#newBatchEDate').val();
            var newBWrnty = $('#newBatchWarrenty').val();
            if ((toFloat(batchCount) + toFloat(newBQty)) > selectedProductQuantity) {
                p_notification(false, eb.getMessage('ERR_GRN_BATCH_COUNT'));
            } else {
                if (batchProducts[newBCode] != null) {
                    p_notification(false, eb.getMessage('ERR_GRN_BATCH_CODE'));
                } else {
                    if ((toFloat(batchCount) + toFloat(newBQty)) == selectedProductQuantity) {
                        $('#proBatchAddNew').addClass('hidden');
                    }
                    batchCount += toFloat(newBQty);
                    batchProducts[newBCode] = new batchProduct(newBCode, newBQty, newBMDate, newBEDate, newBWrnty, null);
                    var cloneBatchAdd = $($('#proBatchSample').clone()).attr('id', newBCode).removeClass('hidden').addClass('tempProducts');
                    cloneBatchAdd.children('#batchNmbr').html(newBCode);
                    cloneBatchAdd.children('#batchQty').html(newBQty);
                    cloneBatchAdd.children('#batchMDate').html(newBMDate);
                    cloneBatchAdd.children('#batchEDate').html(newBEDate);
                    cloneBatchAdd.children('#batchW').html(newBWrnty);
                    cloneBatchAdd.insertBefore('#proBatchSample');
                    clearAddBatchProductRow();
                }
            }
        }
    });

    $('#newBatchNumber').on('click', function(){
        $(this).parents('tr').find('#newBatchMDate').val('');
        $(this).parents('tr').find('#newBatchEDate').val('');
    });

    $('#newBatchNumber').typeahead({
        ajax: {
            url: BASE_URL + '/api/grn/get-batch-numbers-for-typeahead',
            timeout: 250,
            triggerLength: 1,
            method: "POST",
            preProcess: function(respond) {
                if (!respond.status) {
                    return false;
                }
                return respond.data;
            }
        }
    });
    $('#newBatchQty, #newBatchMDate, #newBatchEDate, #newBatchWarrenty').on('click',function(){
        var addedBatchCode = $(this).parents('tr').find('#newBatchNumber').val();
        var selectedRow = $(this).parents('tr');

        if(!($(this).parents('tr').find('#newBatchMDate').val() && $(this).parents('tr').find('#newBatchEDate').val())){
            eb.ajax({
            type: 'POST',
            url: BASE_URL + '/api/grn/get-man-date-and-exp-date-for-batch-item',
            data: {batchCode : addedBatchCode },
                success: function(respond) {
                    if (respond.status == true) {
                        if(!$('#newBatchMDate', selectedRow).val()){
                            $('#newBatchMDate', selectedRow).val(respond.data.manDate);
                        }
                        if(!$('#newBatchEDate', selectedRow).val()){
                            $('#newBatchEDate', selectedRow).val(respond.data.expDate);
                        }
                    }
                },

            });
        }

    });




    $('#addPiProductsModal').on('change', 'input.serialNumberList', function() {
        var matchSerialNumberCount = 0;
        var typedVal = $(this).val();
        var serialCheckStatus = false;
        eb.ajax({
            type: 'POST',
            url: BASE_URL + '/productAPI/checkSerialProductCodeExists',
            data: {serialCode: typedVal, productCode: locationProducts[selectedProduct].pC, },
            success: function(respond) {
                if (respond.data == true) {
                    serialCheckStatus = true;
                }
            },
            async: false
        });

        if (serialCheckStatus == true) {
            p_notification(false, eb.getMessage('ERR_GRN_SERIAL_CODE'));
            $(this).val('');
        } else {
            $('input.serialNumberList').each(function() {
                if ($(this).val() == typedVal) {
                    matchSerialNumberCount++;
                }
            });
            if (matchSerialNumberCount > 1) {
                p_notification(false, eb.getMessage('ERR_GRN_SAME_SERIAL_CODE'));
                $(this).val('');
            }
        }
    });

    $('#addPiProductsModal').on('change', 'input.serialBatchList,#newSerialBatch', function() {
        if (!batchProducts.hasOwnProperty($(this).val())) {
            p_notification(false, eb.getMessage('ERR_GRN_ENTER_BATCHNUM'));
            $(this).val('');
        } else {
            var enteredBatchCode = $(this).val();
            var sameBatchCodeCount = 0;
            $('input.serialBatchList,#newSerialBatch').each(function() {
                if ($(this).val() == enteredBatchCode) {
                    sameBatchCodeCount++;
                }
            });
            if (sameBatchCodeCount > batchProducts[enteredBatchCode].bQty) {
                p_notification(false, eb.getMessage('ERR_GRN_ENTER_PRODLIMIT'));
                $(this).val('');
            }
        }
    });

    $('#savePiProducts').on('click', function(event) {
        var serialEmpty = false;
        $('input.serialNumberList').each(function() {
            if ($(this).val() == '') {
                serialEmpty = true;
                return false;
            }
        });
        if (serialEmpty == true) {
            p_notification(false, eb.getMessage('ERR_GRN_FILL_SERIALNUM'));
            return false;
        }

        var batchEmpty = false;
        if (locationProducts[selectedProduct].bP == 1 && locationProducts[selectedProduct].sP == 0) {
            if (batchCount < selectedProductQuantity) {
                batchEmpty = true;
            }
        }
        if (batchEmpty == true) {
            p_notification(false, eb.getMessage('ERR_GRN_ADD_BATCH_DETAILS'));
            return false;
        }
        var serialBatchEmpty = false;
        if ((locationProducts[selectedProduct].bP == 1) && (locationProducts[selectedProduct].sP == 1)) {
            $('input.serialBatchList').each(function() {
                if ($(this).val() == '') {
                    serialBatchEmpty = true;
                    return false;
                }
            });
        }
        if (serialBatchEmpty == true) {
            p_notification(false, eb.getMessage('ERR_INVENTADJUS_FILLBATCHNUM'));
            return false;
        }
        if (serialEmpty == false && batchEmpty == false) {
            $('#addNewSerial > tr').each(function() {
                var sNmbr = $(this).children().children('#newSerialNumber').val();
                var sWrnty = $(this).children().children('#newSerialWarranty').val();
                var sWrntyT = $(this).children().children('#newSerialWarrantyType').val();
                var sBt = $(this).children().children('#newSerialBatch').val();
                var sED = $(this).children().children('.serialEDate').val();

                if (sNmbr != '') {
                    serialProducts[sNmbr] = new serialProduct(sNmbr, sWrnty, sBt, sED, null,sWrntyT);
                }
            });
            $('#addPiProductsModal').modal('hide');
            addNewProductRow(batchProducts, serialProducts, '');
            clearModalWindow();
            $('#itemCode').focus();
        } else {
            p_notification(false, eb.getMessage('ERR_GRN_BATCH_PRODDETAILS'));
        }

    });

    $('#addPiProductsModal').on('click', '.batchDelete', function() {
        var deleteBID = $(this).closest('tr').attr('id');
        var deleteBQty = batchProducts[deleteBID].bQty;
        batchCount -= toFloat(deleteBQty);
        delete(batchProducts[deleteBID]);
        $('#' + deleteBID).remove();
        $('#proBatchAddNew').removeClass('hidden');
    });

    $('#piProductTable').on('click', '.poRemoveItem', function() {
        var tr = $(this).parents("tr");
        var removeProductID = tr.attr('id');
        $('#' + removeProductID).remove();
    });

    $('#paymentTerm').on('change', function(e) {
        e.preventDefault();
        var days = 0;
        var i = parseInt($(this).val());
        switch (i) {
            case 2:
                days = 7;
                break;
            case 3:
                days = 14;
                break;
            case 4:
                days = 30;
                break;
            case 5:
                days = 60;
                break;
            case 6:
                days = 90;
                break;
            case 7:
                days = 21;
                break;
            case 8:
                days = 28;
                break;
            case 9:
                days = 45;
                break;
            case 10:
                days = 35;
                break;
            default:
                days = 0;
                break;
        }
        setDueDate(days);
    });


    function setDueDate(days) {
        var day = parseInt(days);
        var joindate = eb.convertDateFormat('#issueDate');
        joindate.setDate(joindate.getDate() + day);
        var dd = joindate.getDate() < 10 ? '0' + joindate.getDate() : joindate.getDate();
        var mm = (joindate.getMonth() + 1) < 10 ? '0' + (joindate.getMonth() + 1) : (joindate.getMonth() + 1);
        var y = joindate.getFullYear();
        var joinFormattedDate = eb.getDateForDocumentEdit('#deliveryDate', dd, mm, y);
        $("#deliveryDate").val(joinFormattedDate);
    }

    function getSupplierDetails(supplierID) {
        eb.ajax({
            type: 'POST',
            url: BASE_URL + '/supplierAPI/getSupplierDetails',
            data: {sID: supplierID}, success: function(respond) {
                $('#supplierCurrentBalance').val(parseFloat(respond.data.sOB).toFixed(2));
                $('#supplierCurrentCredit').val(parseFloat(respond.data.sCB).toFixed(2));
                if (respond.data.sP == 'null') {
                    $('#paymentTerm').val(1);
                } else {
                    $('#paymentTerm').val(respond.data.sP);
                }
                $('#paymentTerm').trigger('change');
            }
        });
    }

////////////////////////////////////////////////////

    function clone() {
        var sendStartingValue = quon;
        var originalQuontity = $('#qty').val();
        var poQuontity = $('.poAddItem').parents("tr").find("input[name='qty']").val();
        if (originalQuontity == "") {
            originalQuontity = poQuontity;
        }
        id = '#temp_' + idNumber;
        var quonnn = $(id).find('input[name=quontity]').val();
        var number = $(id).find('input[name=startNumber]').val();
        var startNumberStr = $(id).find('input[name=startNumber]').val();
        var prefix = $(id).find('input[name=prefix]').val();
        var pCode = locationProducts[selectedProduct].pC;
        if (quonnn == '') {
            quonnn = 0;
        }
        if (number == '') {
            p_notification(false, eb.getMessage('NO_START_NO'));
            return false;
        }
        if (number == 0) {
            p_notification(false, eb.getMessage('START_NO_ZERO'));
            return false;
        }
        var validationValue = true;
        validationValue = validation(number, prefix, quonnn, startNumberStr, pCode, sendStartingValue, id);
        if (validationValue == true) {

            if (toFloat(quonnn) <= originalQuontity && toFloat(quon) + toFloat(quonnn) <= originalQuontity && quonnn !== 0) {
                quon = toFloat(quon) + toFloat(quonnn);
                if (toFloat(quonnn) == originalQuontity || toFloat(quon) == originalQuontity) {
                    $(id).find('input[name=submit]').attr('disabled', true);
                    $(id).find('input[name=prefix]').attr('disabled', true);
                    $(id).find('input[name=startNumber]').attr('disabled', true);
                    $(id).find('input[name=quontity]').attr('disabled', true);
                    $(id).find('input[name=expireDateAutoFill]').attr('disabled', true);
                    $(id).find('input[name=batch_code]').attr('disabled', true);
                    $(id).find('input[name=mfDateAutoFill]').attr('disabled', true);
                    $(id).find('input[name=wrtyDays]').attr('disabled', true);
                    setValue(sendStartingValue, quonnn, id);
                }
                else {
                    var row = document.getElementById('temp_' + idNumber);
                    var table = document.getElementById('tempTbody');
                    var clone = row.cloneNode(true);
                    idNumber += 1;
                    clone.id = 'temp_' + idNumber;
                    table.appendChild(clone);
                    nextId = '#temp_' + idNumber;
                    $(nextId).addClass('tempy');
                    $(nextId).find('input[name=prefix]').val('');
                    $(nextId).find('input[name=startNumber]').val('');
                    $(nextId).find('input[name=quontity]').val('');
                    $(nextId).find('input[name=wrtyDays]').val('');
                    var issuDate = new Date(Date.parse($('#issueDate').val()));
                    $(nextId).find('input[name=expireDateAutoFill]').val('');
                    $(nextId).find('input[name=batch_code]').val('');
                    $(nextId).find('input[name=mfDateAutoFill]').val('');
                    var cloneMfD = $(nextId).find('input[name=mfDateAutoFill]').datepicker({
                        onRender: function(date) {
                            return date.valueOf() > issuDate.valueOf() ? 'disabled' : '';
                        }
                    }).on('changeDate', function(ev) {
                        cloneMfD.hide();
                    }).data('datepicker');
                    $(id).find('input[name=submit]').attr('disabled', true);
                    $(id).find('input[name=prefix]').attr('disabled', true);
                    $(id).find('input[name=startNumber]').attr('disabled', true);
                    $(id).find('input[name=quontity]').attr('disabled', true);
                    $(id).find('input[name=expireDateAutoFill]').attr('disabled', true);
                    $(id).find('input[name=batch_code]').attr('disabled', true);
                    $(id).find('input[name=mfDateAutoFill]').attr('disabled', true);
                    $(id).find('input[name=wrtyDays]').attr('disabled', true);
                    if (locationProducts[selectedProduct].sP == 1 && locationProducts[selectedProduct].bP == 1) {
                        $(nextId).find('input[name=mfDateAutoFill]').attr('disabled', false);
                        $(nextId).find('input[name=batch_code]').attr('disabled', false);
                    }
                    var cloneExpD = $(nextId).find('input[name=expireDateAutoFill]').datepicker().on('changeDate', function(ev) {
                        cloneExpD.hide();
                    }).data('datepicker');

                    setValue(sendStartingValue, quonnn, id);
                }
            }
            else {
                p_notification(false, eb.getMessage('WRNG_QUON_PI'));
                return false;
            }
        }
    }

    $('#tempTbody').on('click', '#submit_serial_number', function()
    {
        if ((locationProducts[selectedProduct].bP == 1) && (locationProducts[selectedProduct].sP == 1)) {
            if (!$(this).parents("tr").find("input[name='batch_code']").val()) {
                p_notification(false, eb.getMessage('ERR_NO_GRN_ADD_SERIAL_BATCH_DETAILS'));
                return false;
            }
        }

        if(isNaN($(this).parents('tr').find("input[name='startNumber']").val())){
            p_notification(false, eb.getMessage('ERR_SERIAL_START_NO'));
            $(this).parents('tr').find("input[name='startNumber']").val("");
            return false;
        }


        clone();
        if ($('#submit_serial_number').attr('disabled')) {
            $(this).parent().parent().find('#expire_date_autoFill').removeClass('white_bg');
            $(this).parent().parent().find('#mf_date_autoFill').removeClass('white_bg');
        }
    });
    function validation(startNumber, prefix, bQuntity, startNumberStr, productCode, sendStartingValue, id) {
        var strNumberLength = startNumberStr.length;
        var matchSerialNumberCount = 0;
        var serialCheckStatus = false;
        eb.ajax({
            type: 'POST',
            url: BASE_URL + '/productAPI/checkSerialProductCode',
            data: {startingNumber: startNumber, prefix: prefix, serialQuntity: bQuntity, strNumberLength: strNumberLength, productCode: productCode, },
            success: function(respond) {
                if (respond.data == true) {
                    serialCheckStatus = true;
                }
            },
            async: false
        });
        if (serialCheckStatus == true) {
            p_notification(false, eb.getMessage('ERR_GRN_SERIAL_CODE'));
            return false;
        }
        else {
            $('input.serialNumberList').each(function() {
                for (i = startNumber; i < (parseInt(startNumber) + parseInt(bQuntity)); i++) {
                    if (prefix) {
                        var startingValue = prefix + pad(i, strNumberLength);

                        if ($(this).val() == startingValue) {
                            matchSerialNumberCount++;
                        }
                    } else {
                        var startingValue = pad(i, strNumberLength);
                        if ($(this).val() == startingValue) {
                            matchSerialNumberCount++;
                        }
                    }

                }

            });
            if (matchSerialNumberCount >= 1) {
                p_notification(false, eb.getMessage('ERR_GRN_SAME_SERIAL_CODE'));
                return false;
            }
            else {
                return true;
            }
        }
    }


    function setValue(quon, quonnn, id) {
        var abc = 0;
        var bQuntity = toFloat(quonnn);
        var max = toFloat(quon) + toFloat(quonnn);
        var prefix = $(id).find('input[name=prefix]').val();
        var startNumber = parseInt($(id).find('input[name=startNumber]').val());
        var startNumberString = $(id).find('input[name=startNumber]').val();
        var expire_date = $(id).find('input[name=expireDateAutoFill]').val();
        var mf_date = $(id).find('input[name=mfDateAutoFill]').val();
        var wrnty = $(id).find('input[name=wrtyDays]').val();
        var wrntyType = $(id).find('#wrtyType').val();
        if (locationProducts[selectedProduct].sP == 1 && locationProducts[selectedProduct].bP == 1) {
            var batchNumber = $(id).find('input[name=batch_code]').val();
            batchProducts[batchNumber] = new batchProduct(batchNumber, bQuntity, mf_date, expire_date, wrnty, null);

            if (startNumber) {
                $('#addNewSerial tr').each(function() {
                    if (abc >= toFloat(quon) && abc < toFloat(max)) {
                        var numberLength = startNumberString.length;
                        var startingValue = pad(startNumber, numberLength);
                        if (prefix == '') {
                            $(this).find('input[name=serialCode]:first').val(startingValue);
                            $(this).find('input[name=serialEDate]').val(expire_date);
                            $(this).find('input[name=batchCode]').val(batchNumber);
                            $(this).find('input[name=serialWarronty]').val(wrnty);
                            $(this).find("#newSerialWarrantyType").val(wrntyType).change();

                        } else if (prefix != '') {
                            var num = prefix + startingValue;
                            $(this).find('input[name=serialCode]:first').val(num);
                            $(this).find('input[name=serialEDate]').val(expire_date);
                            $(this).find('input[name=batchCode]').val(batchNumber);
                            $(this).find('input[name=serialWarronty]').val(wrnty);
                            $(this).find("#newSerialWarrantyType").val(wrntyType).change();
                        }
                        startNumber += 1;
                    }
                    abc++;
                });
            }
        }
        else {
            if (startNumber) {
                $('#addNewSerial tr').each(function() {
                    if (abc >= toFloat(quon) && abc < toFloat(max)) {
                        var numberLength = startNumberString.length;
                        var startingValue = pad(startNumber, numberLength);
                        if (prefix == '') {
                            $(this).find('input[name=serialCode]:first').val(startingValue);
                            $(this).find('input[name=serialEDate]').val(expire_date);
                            $(this).find('input[name=serialWarronty]').val(wrnty);
                            $(this).find("#newSerialWarrantyType").val(wrntyType).change();
                        } else if (prefix != '') {
                            var num = prefix + startingValue;
                            $(this).find('input[name=serialCode]:first').val(num);
                            $(this).find('input[name=serialEDate]').val(expire_date);
                            $(this).find('input[name=serialWarronty]').val(wrnty);
                            $(this).find("#newSerialWarrantyType").val(wrntyType).change();
                        }
                        startNumber += 1;
                    }
                    abc++;
                });
            }
        }
    }

    function pad(n, width, z) {
        z = z || '0';
        n = n + '';
        return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
    }




    function setDataForLocation(locationID) {
        clearProductScreen();
        selectedLocation = locationID;
        var documentType = 'purchaseInvoice';
        loadDropDownFromDatabase('/productAPI/search-location-products-for-dropdown', locationID, 0, '#itemCode', '', '', documentType);

//      get Pi No For Location
        eb.ajax({
            type: 'POST',
            url: BASE_URL + '/api/pi/getPiNoForLocation',
            data: {locationID: locationID},
            success: function(respond) {
                if (respond.status == false) {
                    p_notification(false, respond.msg);
                    $('#piNo').val('');
                    $('#refNotSet').modal('show');
                }
                else if (respond.status == true) {
                    $('#piNo').val(respond.data.refNo);
                    $('#locRefID').val(respond.data.locRefID);
                }
            },
            async: false
        });

        if (TAXSTATUS == false) {
            p_notification('info', eb.getMessage('ERR_GRN_ENABLE_TAX'));
        }
        $('#productAddOverlay').removeClass('overlay');
    }

    function setProductsView(viewProductID) {
        $('.tempView').remove();
        $('#batchProductsView').addClass('hidden');
        $('#serialViewScreen').addClass('hidden');
        if (!jQuery.isEmptyObject(products[viewProductID].sProducts)) {
            $('#serialViewScreen').removeClass('hidden');
            $('#piserialCodeView').html(products[viewProductID].pCode);
            $('#piSerialQuantityView').html(products[viewProductID].pQuantity);
            for (var h in products[viewProductID].sProducts) {
                var viewSPr = products[viewProductID].sProducts[h];
                var cloneSerialProductView = $($('#serialViewSample').clone()).attr('id', '').removeClass('hidden').addClass('tempView');
                cloneSerialProductView.children('#serialNumberView').html(viewSPr.sCode);
                cloneSerialProductView.children('#serialWarrentyView').html(viewSPr.sWarranty);
                cloneSerialProductView.children('#serialBIDView').html(viewSPr.sBCode);
                cloneSerialProductView.children('#serialExDView').html(viewSPr.sEdate);
                cloneSerialProductView.insertBefore('#serialViewSample');
            }
        } else if (!jQuery.isEmptyObject(products[viewProductID].bProducts)) {
            $('#batchProductsView').removeClass('hidden');
            $('#batchCodeView').html(products[viewProductID].pCode);
            $('#batchQuantityView').html(products[viewProductID].pQuantity);
            for (var b in products[viewProductID].bProducts) {
                var viewBPr = products[viewProductID].bProducts[b];
                var cloneBatchProductView = $($('#proBatchViewSample').clone()).attr('id', '').removeClass('hidden').addClass('tempView');
                cloneBatchProductView.children('#batchNmbrView').html(viewBPr.bCode);
                cloneBatchProductView.children('#batchQtyView').html(viewBPr.bQty);
                cloneBatchProductView.children('#batchMDateView').html(viewBPr.mDate);
                cloneBatchProductView.children('#batchEDateView').html(viewBPr.eDate);
                cloneBatchProductView.children('#batchWView').html(viewBPr.warnty);
                cloneBatchProductView.insertBefore('#proBatchViewSample');
            }
        }
    }

    function setTotalTax() {
        totalTaxList = {};
        for (var k in products) {
            if (products[k].pTax != null) {
                for (var l in products[k].pTax.tL) {
                    if (totalTaxList.hasOwnProperty(l)) {
                        var curTA = totalTaxList[l].tA;
                        var newTA = (toFloat(curTA) + toFloat(products[k].pTax.tL[l].tA));
                        totalTaxList[l].tA = newTA.toFixed(2);
                    } else {
                        totalTaxList[l] = {tN: products[k].pTax.tL[l].tN, tP: products[k].pTax.tL[l].tP, tA: products[k].pTax.tL[l].tA};
                    }
                }
            }
        }

        var totalTaxHtml = "";
        for (var t in totalTaxList) {
            totalTaxHtml += totalTaxList[t].tN + "(" + totalTaxList[t].tP + "%)&nbsp" + accounting.formatMoney((totalTaxList[t].tA)) + "<br>";
        }
        $('#totalTaxShow').html(totalTaxHtml);
    }
    function setPiTotalCost($editMode = false) {
        
        var piTotal = 0;
        for (var i in products) {
            piTotal += toFloat(products[i].pTotal);
        }

        if ($('#total_discount_rate').val() != '' && $('input[name=discount_type]:checked').val() == 'Value') {


            var dicountVal = $('#total_discount_rate').val();

            if ((parseFloat(dicountVal) > toFloat(piTotal)) && $editMode) {
                $('#total_discount_rate').val('');
                dicountVal = 0;
                decimalPoints = 0;
            }

            
            if (piTotal > 0) {
                piTotal = toFloat(piTotal) - toFloat($('#total_discount_rate').val());
            } else {
                piTotal = toFloat(piTotal);
            }
        }

        $('#subtotal').html(accounting.formatMoney(piTotal));
        var deliveryAmount = $('#deliveryCharge').val();
        piTotal += toFloat(deliveryAmount);
        $('#finaltotal').html(accounting.formatMoney(piTotal));
    }
    function addNewProductRow(batchProducts, serialProducts, grnTr) {
    	var $parentTr = '';
        if (thisIsGrnAddFlag == true) {
        	$parentTr = grnTr;
        	var productID = grnProducts[addingGrnPro].gPID;
            var iC = grnProducts[addingGrnPro].gPC;
            var iN = grnProducts[addingGrnPro].gPN;
            var pT = grnProducts[addingGrnPro].productType;
            var uPrice = accounting.formatMoney($('#unitPrice',grnTr).val(), null, getDecimalPlaces($('#unitPrice',grnTr).val()));
            var uFPrice = $("input[name='unitPrice']",grnTr).val();
            var uomPrice = $("input[name='unitPrice']",grnTr).siblings('.uomPrice').val();
            var qt = $("input[name='qty']",grnTr).val();
            var uomqty = $("input[name='qty']",grnTr).siblings('.uomqty').val();
            var uomAbr = $(".uom-select",grnTr).find("span.selected").html();
            var uomId = $(".uom-select",grnTr).find("span.selected").data('uomID');
            var nTotal = $('.itemLineTotal',grnTr).html();
            var calNTotal = accounting.unformat(nTotal);
            var gD = $("input[name='discount']", grnTr).val();
            if ($("input[name='discount']", grnTr).val() == '')
                gD = 0;
            var locationProductID = grnProducts[addingGrnPro].lPID;
            selectedProductQuantity = qt;
            var grnTaxList = {}
            var tmpGrnTotalTaxAmount = 0;
            var sTaxList = {};
            for (var t in grnProducts[addingGrnPro].pT) {
                var tmpGrnTax = grnProducts[addingGrnPro].pT;
                tmpGrnTotalTaxAmount = toFloat(tmpGrnTotalTaxAmount) + toFloat(tmpGrnTax[t].pTA);
                sTaxList[t] = {
                    'tA': tmpGrnTax[t].pTA,
                    'tN': tmpGrnTax[t].pTN,
                    'tP': tmpGrnTax[t].pTP
                };
            }
            grnTaxList.tL = sTaxList;
            grnTaxList.tTA = tmpGrnTotalTaxAmount;
            $("input[name=discount]",grnTr).trigger(jQuery.Event("focusout"));

        } else if (thisIsPoAddFlag == true) {
            var iC = poProducts[addingPoPro].poPC;
            var pT = poProducts[addingPoPro].productType;
            var iN = poProducts[addingPoPro].poPN;
            var $poItemTr = $('#poPro_' + poProducts[addingPoPro].poPID);
            $parentTr = $poItemTr;
            var productID = poProducts[addingPoPro].poPID;

            var qty = $poItemTr.find("input[name='qty']").val();
            var uPrice = $poItemTr.find(".uomPrice").val();
            var unitPrice = uPrice;
            var itemDiscount = $poItemTr.find("input[name='discount']").val();
            var itemLineTotal = $poItemTr.find('.itemLineTotal').html();
            var uomAbr = $poItemTr.find(".uom-select").find("span.selected").html();
            var uomId = $poItemTr.find(".uom-select").find("span.selected").data('uomID');
            var uFPrice = $("input[name='unitPrice']",grnTr).val();
            var qt = $("input[name='qty']",grnTr).val();
            var uomqty = $poItemTr.find("input[name='qty']").siblings('.uomqty').val();
            var nTotal = itemLineTotal;
            var calNTotal = accounting.unformat(nTotal);
            var gD = itemDiscount;

            var uomPrice = uPrice;
            if (itemDiscount == '')
                gD = 0;
            var locationProductID = poProducts[addingPoPro].lPID;
            selectedProductQuantity = poProducts[addingPoPro].poPQ;
            $("input[name=discount]",$poItemTr).trigger(jQuery.Event("focusout"));
        } else {
        	$parentTr = $('#itemCode').parents('tr');
        	var productID = selectedProduct;
            var iC = $('#itemCode').data('PC');
            var pT = $('#itemCode').data('PT');
            var iN = $('#itemCode').data('PN');
            var uPrice = $('#unitPrice').val();
            var uFPrice = $('#unitPrice').val();
            var qt = $('#qty').val();
            if (qt == 0) {
                qt = 0;
            }
            var uomqty = $("#qty").siblings('.uomqty').val();
            var uomPrice = $("#unitPrice").siblings('.uomPrice').val();
            var nTotal = productLineTotal[locationProducts[selectedProduct].pID+'_'+uPrice];
            var calNTotal = accounting.unformat(nTotal);
            var gD = $('#piDiscount').val();
            if ($('#piDiscount').val() == '')
                gD = 0;
            var locationProductID = locationProducts[productID].lPID;
            selectedProductQuantity = $('#qty').val();
           	$("input[name=discount]").trigger(jQuery.Event("focusout"));
        }

        var newTrID = 'tr_' + locationProductID;
        if (thisIsGrnAddFlag == true) {
            var clonedRow = $($('#preSetSample').clone()).attr('id', newTrID).addClass('addedProducts').attr('data-grnproid', grnProID);
        } else {
            var clonedRow = $($('#preSetSample').clone()).attr('id', newTrID).addClass('addedProducts').attr('data-pisubid', locationProductID+'_'+uPrice);;
        }
        clonedRow.children('#code').html(iC + ' - ' + iN);
        clonedRow.children('#quantity').html(uomqty);
        clonedRow.children('#unit').html(uomPrice);

        if (thisIsGrnAddFlag == true) {
            clonedRow.children().children('#uomName').html(uomAbr);
        } else if (thisIsPoAddFlag == true) {
            clonedRow.children().children('#uomName').html(uomAbr);
        } else {
            clonedRow.children().children('#uomName').html(locationProducts[productID].uom[selectedProductUom].uA);
        }

        clonedRow.children('#disc').html(gD);
        clonedRow.children('#ttl').html(accounting.formatMoney(nTotal));

        if (locationProducts[productID].bP == 0 && locationProducts[productID].sP == 0) {
            clonedRow.children().children('#viewSubProducts').addClass('hidden');
        }

        if (thisIsGrnAddFlag == true) {
            var grnLiTaxList = "";
            for (var t in grnProducts[addingGrnPro].pT) {
                var tmpViewTax = grnProducts[addingGrnPro].pT;
                grnLiTaxList += "<li>&nbsp;&nbsp;<label>&nbsp;&nbsp;" + tmpViewTax[t].pTN + "&nbsp;&nbsp;" + tmpViewTax[t].pTP + "%</label></li>";
            }
            var clonedTax = $($('#addNewTaxDiv').clone()).attr('id', '');

            if (grnLiTaxList == '') {
                clonedTax.children('#addNewTax').children('#taxApplied').removeClass('glyphicon glyphicon-check');
                clonedTax.children('#addNewTax').children('#taxApplied').addClass('glyphicon glyphicon-unchecked');
                clonedTax.children('#addNewTax').children('#taxApplied').attr('id', '');
            } else {
                clonedTax.children('#addNewTax').children('#taxApplied').removeClass('glyphicon glyphicon-unchecked');
                clonedTax.children('#addNewTax').children('#taxApplied').addClass('glyphicon glyphicon-check');
                clonedTax.children('#addNewTax').children('#taxApplied').attr('id', '');
            }

            clonedTax.children('#addNewTax').attr('id', '');
            clonedTax.children('#addTaxUl').children().remove();
            clonedTax.children('#addTaxUl').html(grnLiTaxList);
            clonedTax.children('#addTaxUl').attr('id', '');
        } else if (thisIsPoAddFlag == true) {
            var clonedTax = $poItemTr.find('#poTaxDiv').attr('id', '');
            clonedTax.children('#poAddTaxDiv').children('#poTaxApplied').attr('id', '');
            clonedTax.children('#poAddTaxDiv').attr('id', '');
            clonedTax.children('#poAddTaxUl').children().removeClass('poTempLi');
            clonedTax.children('#poAddTaxUl').children().children().removeClass('addNewTaxCheck');
            clonedTax.children('#poAddTaxUl').children().children().attr('disabled', true);
            clonedTax.children('#poAddTaxUl').children('#poSampleLi').remove();
            clonedTax.children('#poAddTaxUl').attr('id', '');
        } else {
            var clonedTax = $($('#addNewTaxDiv').clone()).attr('id', '');
            clonedTax.children('#addNewTax').children('#taxApplied').attr('id', '');
            clonedTax.children('#addNewTax').attr('id', '');
            clonedTax.children('#addTaxUl').children().removeClass('tempLi');
            clonedTax.children('#addTaxUl').children().children().removeClass('addNewTaxCheck');
            clonedTax.children('#addTaxUl').children().children().attr('disabled', true);
            clonedTax.children('#addTaxUl').children('#sampleLi').remove();
            clonedTax.children('#addTaxUl').attr('id', '');
        }
        clonedTax.find('#toggleSelection').addClass('hidden');
        clonedRow.children('#tax').html(clonedTax);
        clonedRow.removeClass('hidden');
        clonedRow.insertBefore('#preSetSample');
        var newProduct = locationProducts[productID];
        var dType = null;
        if (newProduct.pPDP != null && newProduct.pPDV == null) {
            dType = 'precentage';
        } else if (newProduct.pPDP == null && newProduct.pPDV != null) {
            dType = 'value';

        }

        if (thisIsGrnAddFlag == true) {
            products.push(new product(newProduct.lPID, productID, iC, iN, qt, gD, toFloat(uFPrice), uomId, calNTotal, currentItemTaxResults, batchProducts, serialProducts, pT, false, grnProductID, null ,true,false,grnDocID, dType));
            $('#grnPro_'+productID+'_'+ grnProID).remove();
            thisIsGrnAddFlag = false;
        } else if (thisIsPoAddFlag == true) {
            products.push(new product(newProduct.lPID, productID, iC, iN, qt, gD, toFloat(uFPrice), uomId, calNTotal, currentItemTaxResults, batchProducts, serialProducts, pT, false, null, null,false, true,null, dType));
            $('#poPro_' + productID).remove();
            thisIsPoAddFlag = false;
        } else {
            products.push(new product(newProduct.lPID, productID, iC, iN, qt, gD, toFloat(uFPrice), selectedProductUom, calNTotal, currentItemTaxResults, batchProducts, serialProducts, pT, true, null,null ,false, false, null, dType));
        }
        setPiTotalCost();
        setTotalTax();
        clearAddNewRow($parentTr);
        $('#itemCode').val(0).trigger('change').empty().selectpicker('refresh');
        clearModalWindow();
    }
    function clearModalWindow() {
        $('.tempProducts').remove();
        $('.cloneSerials').remove();
        $('#piProductCodeBatch').html('');
        $('#piProductQuantityBatch').html('');
        $('#piProductCodeSerial').html('');
        $('#piProductQuantitySerial').html('');
        $('#batchAddScreen').addClass('hidden');
        $('#serialAddScreen').addClass('hidden');
        $('#addNewSerial input[type=text]').val('');
        $('#proBatchAddNew').removeClass('hidden');
        batchCount = 0;
        batchProducts = {};
        serialProducts = {};

    }

    function clearProductScreen() {
        products = new Array();
        clearAddBatchProductRow();
        var $parentTr = $('#itemCode').parents('tr');
        clearAddNewRow($parentTr);
        clearModalWindow();
        setPiTotalCost();
        setTotalTax();
        $('.addedProducts').remove();
        locationProducts = '';
        locProductCodes = '';
        locProductNames = '';
        selectedProduct = '';
        selectedProductQuantity = '';
        selectedProductUom = '';
        batchCount = '';
    }

    function setGrnProducts(preProducts, grLocId) {
        if ($('#addNewItemTr').hasClass('hidden')) {
            $('#addNewItemTr').removeClass('hidden');
        }
        $('#add-new-item-row').find('tr').each(function() {
            if (this.id != 'addNewItemTr') {
                $(this).remove();
            }
        });


        for (var p in preProducts) {
            var preProduct = preProducts[p];
            var baseUom;
            for (var j in preProduct.pUom) {
                if (preProduct.pUom[j].gPUomCon == 1) {
                    baseUom = preProduct.pUom[j].gPUom;
                }
            }

            var clonedAddNew = $($('#addNewItemTr').clone()).attr('id', 'grnPro_' + preProduct.gPID+'_'+preProduct.grnIndexKey).addClass('tempGrnPro').attr('data-pid',preProduct.gPID);
            clonedAddNew.data('stockupdate',false);

            $("#itemCode", clonedAddNew).empty().
                    append($("<option></option>").
                            attr("value", preProduct.gPID).
                            text(preProduct.gPN + '-' + preProduct.gPC));
            $('#itemCode', clonedAddNew).val(preProduct.gPID);
            $('div.bootstrap-select', clonedAddNew).remove();
            $('#itemCode', clonedAddNew).selectpicker('render');
            $('#itemCode', clonedAddNew).data('PT', preProduct.productType);
            $('#itemCode', clonedAddNew).data('PN', preProduct.gPN);
            $('#itemCode', clonedAddNew).data('PC', preProduct.gPC);
            $('#itemCode', clonedAddNew).data('pId', preProduct.gPID);
            $('#itemCode', clonedAddNew).prop('disabled', true);
            $('#itemCode', clonedAddNew).addClass('grnItemCode');
            $('#itemCode', clonedAddNew).attr('id', '');
            $('#itemCode', clonedAddNew).selectpicker('render');
            if (preProduct.gPQ == null) {
                preProduct.gPQ = 0;
            }
            $('#qty', clonedAddNew).addUom(locationProducts[preProduct.gPID].uom, preProduct.gPUomID);
            $('#qty', clonedAddNew).val(preProduct.gPQ).change();
            $('#qty', clonedAddNew).data('qty',preProduct.gPQ);
            $('#qty', clonedAddNew).parent().addClass('input-group');
            $('#qty', clonedAddNew).attr('id', '');
            $('#addNewUomDiv', clonedAddNew).attr('id', '');
            $('#unitPrice', clonedAddNew).val(preProduct.gPP);
            $('#unitPrice', clonedAddNew).addUomPrice(locationProducts[preProduct.gPID].uom, preProduct.gPUomID);
            $('#unitPrice', clonedAddNew).attr('id', '');
            $('#unitPrice', clonedAddNew).data('price', preProduct.gPP);
            if (preProduct.gPD == null) {
                preProduct.gPD = 0;
            }

            var baseUom;
            for (var j in locationProducts[preProduct.gPID].uom) {
                if (locationProducts[preProduct.gPID].uom[j].pUBase == 1) {
                    baseUom = locationProducts[preProduct.gPID].uom[j].uomID;
                }
            }

            if (locationProducts[preProduct.gPID].pPDV) {
                $('#piDiscount',clonedAddNew).val(preProduct.gPD).addUomPrice(locationProducts[preProduct.gPID].uom, baseUom);
                $('#piDiscount',clonedAddNew).siblings('.uom-price-select').find('.uom_price_drop_down').attr('disabled', true);
                $(".sign", clonedAddNew).text('Rs');
                $('#piDiscount', clonedAddNew).attr('id', '');
            } else if (locationProducts[preProduct.gPID].pPDP) {
                $('#piDiscount',clonedAddNew).val(preProduct.gPD);
                $(".sign", clonedAddNew).text('%');
                $('#piDiscount', clonedAddNew).attr('id', '');
            } else if (locationProducts[preProduct.gPID].pPDP == null && locationProducts[preProduct.gPID].pPDV == null) {
                $('#piDiscount',clonedAddNew).attr('readonly', true);
                $('#piDiscount', clonedAddNew).attr('id', '');
            }

            $('#addNewTotal', clonedAddNew).html(accounting.formatMoney(preProduct.gPT));
            $('#addNewTotal', clonedAddNew).attr('id', '');
            $('#addNewUom', clonedAddNew).prop('disabled', true);
            $('#addNewUom', clonedAddNew).attr('id', '');
            $('#uomAb', clonedAddNew).html(baseUom);
            $('#uomAb', clonedAddNew).attr('id', '');
            $('#addUomUl', clonedAddNew).attr('id', '');
            // $('#sampleLi', clonedAddNew).attr('id', '');
            /////////////////////////
            if (!jQuery.isEmptyObject(preProduct.pT)) {
                var currentProductTax = preProduct.pT;
                var realProductTax = locationProducts[preProduct.gPID].tax;
                $('#addNewTaxDiv', clonedAddNew).attr('id', "poTaxDiv");
                $('#addNewTax', clonedAddNew).attr('id', "poAddTaxDiv");
                $('#taxApplied', clonedAddNew).removeClass('glyphicon glyphicon-unchecked');
                $('#taxApplied', clonedAddNew).addClass('glyphicon glyphicon-check');
                $('#taxApplied', clonedAddNew).attr('id', 'poTaxApplied')
                $('#addTaxUl', clonedAddNew).attr('id', 'poAddTaxUl');
                $('.tempLi', clonedAddNew).remove();
                var taxIDArray = [];
                for (var i in currentProductTax) {
                    taxIDArray[i] = currentProductTax[i];
                    var clonedLi = $($('#sampleLi', clonedAddNew).clone()).removeClass('hidden').attr('id', 'li_' + i).addClass("poTempLi");
                    clonedLi.children(".taxChecks").attr('id', preProduct.poPID + '_' + i).prop('checked', true).addClass('addNewTaxCheck');
                    clonedLi.children(".taxName").html('&nbsp&nbsp' + currentProductTax[i].pTN + '&nbsp&nbsp' + currentProductTax[i].pTP + '%').attr('for', preProduct.poPID + '_' + i);
                    clonedLi.insertBefore($('#sampleLi', clonedAddNew));
                }

                for (var i in realProductTax) {
                    if(jQuery.isEmptyObject(taxIDArray[i])){
                        var clonedLi = $($('#sampleLi', clonedAddNew).clone()).removeClass('hidden').attr('id', 'li_' + i).addClass("poTempLi");
                        clonedLi.children(".taxChecks").attr('id', preProduct.gPID + '_' + i).prop('checked', false).addClass('addNewTaxCheck');
                        clonedLi.children(".taxName").html('&nbsp&nbsp' + realProductTax[i].tN + '&nbsp&nbsp' + realProductTax[i].tP + '%').attr('for', preProduct.gPID + '_' + i);
                        clonedLi.insertBefore($('#sampleLi', clonedAddNew));
                    }
                }

                $('#sampleLi', clonedAddNew).attr('id', 'poSampleLi');
            } else if((!jQuery.isEmptyObject(locationProducts[preProduct.gPID].tax))){
                var currentProductTax = locationProducts[preProduct.gPID].tax;
                $('#addNewTaxDiv', clonedAddNew).attr('id', "poTaxDiv");
                $('#addNewTax', clonedAddNew).attr('id', "poAddTaxDiv");
                $('#taxApplied', clonedAddNew).addClass('glyphicon glyphicon-unchecked');
                $('#taxApplied', clonedAddNew).attr('id', 'poTaxApplied')
                $('#addTaxUl', clonedAddNew).attr('id', 'poAddTaxUl');
                $('.tempLi', clonedAddNew).remove();
                for (var i in currentProductTax) {
                    var clonedLi = $($('#sampleLi', clonedAddNew).clone()).removeClass('hidden').attr('id', 'li_' + i).addClass("poTempLi");
                    clonedLi.children(".taxChecks").attr('id', preProduct.gPID + '_' + i).prop('checked', false).addClass('addNewTaxCheck');
                    clonedLi.children(".taxName").html('&nbsp&nbsp' + currentProductTax[i].tN + '&nbsp&nbsp' + currentProductTax[i].tP + '%').attr('for', preProduct.gPID + '_' + i);
                    clonedLi.insertBefore($('#sampleLi', clonedAddNew));
                }
                $('#sampleLi', clonedAddNew).attr('id', 'poSampleLi');

            } else {
                $('#addNewTaxDiv', clonedAddNew).attr('id', "poTaxDiv");
                $('#addNewTax', clonedAddNew).attr('id', "poAddTaxDiv");
                $('#taxApplied', clonedAddNew).attr('id', 'poTaxApplied');
                $('#addTaxUl', clonedAddNew).attr('id', 'poAddTaxUl');
                $('#sampleLi', clonedAddNew).remove();
                $('.tempLi', clonedAddNew).remove();
            }
            ////////////////////////////////

            $('.taxName', clonedAddNew).attr('id', '');
            $('#addTaxUl', clonedAddNew).attr('id', '');
            $('#tax', clonedAddNew).attr('id', '');
            $('#addNewTaxDiv', clonedAddNew).addClass('hidden');
            $('#addNewTaxDiv', clonedAddNew).attr('id', 'grnAddNewTaxDiv');
            $('#addNewTax', clonedAddNew).attr('id', '');
            $('#addNewTax', clonedAddNew).attr('id', '');
            $('#taxApplied', clonedAddNew).attr('id', '');
            $('#addItem', clonedAddNew).attr('id', '').addClass('grnAddItem');
            $('#removeItem', clonedAddNew).attr('id', '').addClass('grnRemoveItem').removeClass('hidden');
            clonedAddNew.insertBefore('#addNewItemTr');

        	$('.tempLi',clonedAddNew).remove();
        	productTax = preProduct.pT;
            if(!jQuery.isEmptyObject(productTax)){
            	$('#taxApplied',clonedAddNew).removeClass('glyphicon glyphicon-unchecked');
        		$('#taxApplied',clonedAddNew).addClass('glyphicon glyphicon-check');
            }
        	for (var i in productTax) {
            	var clonedLi = $($('#sampleLi',clonedAddNew).clone()).removeClass('hidden').attr('id', 'li_' + i).addClass("tempLi");
            	clonedLi.children(".taxChecks").attr('id', preProduct.gPID + '_' + i).prop('checked', true).addClass('addNewTaxCheck');
            	if (productTax[i].tS == 0) {
                	clonedLi.children(".taxChecks").attr('disabled', 'disabled');
                	clonedLi.children(".taxName").addClass('crossText');
           	 	}
            	clonedLi.children(".taxName").html('&nbsp&nbsp' + productTax[i].pTN + '&nbsp&nbsp' + productTax[i].pTP + '%').attr('for', preProduct.gPID + '_' + i);
            	clonedLi.insertBefore($('#sampleLi',clonedAddNew));
        	}
        }
        $('tr.tempGrnPro', '#piProductTable').find(".uomPrice").trigger('focusout');
    }

    function setPoProducts(preProducts, poLocId) {
        if ($('#addNewItemTr').hasClass('hidden')) {
            $('#addNewItemTr').removeClass('hidden');
        }
        $('#add-new-item-row').find('tr').each(function() {
            if (this.id != 'addNewItemTr') {
                $(this).remove();
            }
        });
        for (var p in preProducts) {
            var preProduct = preProducts[p];
            var clonedAddNew = $($('#addNewItemTr').clone()).attr('id', 'poPro_' + preProduct.poPID).addClass('tempPoPro');
            $("#itemCode", clonedAddNew).empty().
                    append($("<option></option>").
                            attr("value", preProduct.poPID).
                            text(preProduct.poPN + '-' + preProduct.poPC));
            $('#itemCode', clonedAddNew).val(preProduct.poPID);
            $('div.bootstrap-select', clonedAddNew).remove();
            $('#itemCode', clonedAddNew).selectpicker('render');
            $('#itemCode', clonedAddNew).data('PT', preProduct.productType);
            $('#itemCode', clonedAddNew).data('PN', preProduct.poPN);
            $('#itemCode', clonedAddNew).data('PC', preProduct.poPC);
            $('#itemCode', clonedAddNew).data('poPID', preProduct.poPID);
            $('#itemCode', clonedAddNew).prop('disabled', true);
            $('#itemCode', clonedAddNew).attr('id', '');
            $('#itemCode', clonedAddNew).selectpicker('render');
            eb.ajax({
                type: 'POST',
                url: BASE_URL + '/productAPI/get-location-product-details',
                data: {productID: preProduct.poPID, locationID: poLocId},
                success: function(respond) {
                    var currentElem = new Array();
                    currentElem[preProduct.poPID] = respond.data;
                    locationProducts = $.extend(locationProducts, currentElem);
                },
                async: false
            });

            if (preProduct.poPQ == null) {
                preProduct.poPQ = 0;
            }
            var poRemainQty = toFloat(preProduct.poPQ) - toFloat(preProduct.copiedQty);
            $('#qty', clonedAddNew).val(poRemainQty);
            $('#qty', clonedAddNew).addUom(locationProducts[preProduct.poPID].uom, preProduct.poPUomID);
            $('#qty', clonedAddNew).parent().addClass('input-group');
            $('#qty', clonedAddNew).attr('id', '');
            $('#addNewUomDiv', clonedAddNew).attr('id', '');
            $('#unitPrice', clonedAddNew).val(preProduct.poPP);
            $('#unitPrice', clonedAddNew).addUomPrice(locationProducts[preProduct.poPID].uom, preProduct.poPUomID);
            $('#unitPrice', clonedAddNew).attr('id', '');

            var baseUom;
            for (var j in locationProducts[preProduct.poPID].uom) {
                if (locationProducts[preProduct.poPID].uom[j].pUBase == 1) {
                    baseUom = locationProducts[preProduct.poPID].uom[j].uomID;
                }
            }
            
            
            if (locationProducts[preProduct.poPID].pPDV) {
                $('#piDiscount',clonedAddNew).val(preProduct.poPD).addUomPrice(locationProducts[preProduct.poPID].uom, baseUom);
                $('#piDiscount',clonedAddNew).siblings('.uom-price-select').find('.uom_price_drop_down').attr('disabled', true);
                $(".sign", clonedAddNew).text('Rs');
                $('#piDiscount', clonedAddNew).attr('id', '');
            } else if (locationProducts[preProduct.poPID].pPDP) {
                $('#piDiscount',clonedAddNew).val(preProduct.poPD);
                $(".sign", clonedAddNew).text('%');
                $('#piDiscount', clonedAddNew).attr('id', '');
            } else if (locationProducts[preProduct.poPID].pPDP == null && locationProducts[preProduct.poPID].pPDV == null) {
                $('#piDiscount',clonedAddNew).attr('readonly', true);
                $('#piDiscount', clonedAddNew).attr('id', '');
            }

            $('#addNewTotal', clonedAddNew).html(accounting.formatMoney(preProduct.poPT));
            $('#addNewTotal', clonedAddNew).attr('id', '');
            $('#addNewUom', clonedAddNew).prop('disabled', true);
            $('#addNewUom', clonedAddNew).attr('id', '');
            $('#uomAb', clonedAddNew).html(preProduct.poPUom);
            $('#uomAb', clonedAddNew).attr('id', '');
            $('#addUomUl', clonedAddNew).attr('id', '');

            //setting tax for products
            if (!jQuery.isEmptyObject(preProduct.pT)) {
                var currentProductTax = preProduct.pT;
                $('#addNewTaxDiv', clonedAddNew).attr('id', "poTaxDiv");
                $('#addNewTax', clonedAddNew).attr('id', "poAddTaxDiv");
                $('#taxApplied', clonedAddNew).removeClass('glyphicon glyphicon-unchecked');
                $('#taxApplied', clonedAddNew).addClass('glyphicon glyphicon-check');
                $('#taxApplied', clonedAddNew).attr('id', 'poTaxApplied')
                $('#addTaxUl', clonedAddNew).attr('id', 'poAddTaxUl');
                $('.tempLi', clonedAddNew).remove();
                for (var i in currentProductTax) {
                    var clonedLi = $($('#sampleLi', clonedAddNew).clone()).removeClass('hidden').attr('id', 'li_' + i).addClass("poTempLi");
                    clonedLi.children(".taxChecks").attr('id', preProduct.poPID + '_' + i).prop('checked', true).addClass('addNewTaxCheck');
                    clonedLi.children(".taxName").html('&nbsp&nbsp' + currentProductTax[i].pTN + '&nbsp&nbsp' + currentProductTax[i].pTP + '%').attr('for', preProduct.poPID + '_' + i);
                    clonedLi.insertBefore($('#sampleLi', clonedAddNew));
                }
                $('#sampleLi', clonedAddNew).attr('id', 'poSampleLi');
            } else {
                $('#addNewTaxDiv', clonedAddNew).attr('id', "poTaxDiv");
                $('#addNewTax', clonedAddNew).attr('id', "poAddTaxDiv");
                $('#taxApplied', clonedAddNew).attr('id', 'poTaxApplied');
                $('#addTaxUl', clonedAddNew).attr('id', 'poAddTaxUl');
                $('#sampleLi', clonedAddNew).remove();
                $('.tempLi', clonedAddNew).remove();
            }

            $('#tax', clonedAddNew).attr('id', '');
            $('#addItem', clonedAddNew).attr('id', '').addClass('poAddItem');
            $('#removeItem', clonedAddNew).attr('id', '').addClass('poRemoveItem').removeClass('hidden');
            clonedAddNew.insertBefore('#addNewItemTr');
        }
        //trigger focustout to poProducts-avoid recalculating costs here
        $('tr.tempPoPro', '#piProductTable').find(".uomPrice").trigger('focusout');
    }


    function loadGRN(grnIDs) {
        $('#addSupplierBtn').prop('disabled', 1).click(function(e) {
            e.stopPropagation;
            return false;
        });

        eb.ajax({
            type: 'POST',
            url: BASE_URL + '/api/pi/getGrnDetails',
            data: {grnIDs: grnIDs},
            success: function(respond) {
                if (respond.data.inactiveItemFlag) {
                    p_notification(false, respond.data.errorMsg);
                    setTimeout(function(){ 
                        window.location.assign(BASE_URL + "/pi")
                    }, 3000);
                    return false;
                }
                var grnData = respond.data.grnData;
                if (!$.isEmptyObject(respond.data.dimensionData)) {
                    var invoiceNo = $('#piNo').val();
                    $.each(respond.data.dimensionData, function(index, val) {
                        dimensionData[invoiceNo] = val;
                    });
                }
                $("#supplier").empty().
                        append($("<option></option>").
                                attr("value", grnData.gSID).
                                text(grnData.gSN));
                $('#supplier').val(grnData.gSID);
                $('#supplier').prop('disabled', true);
                $('#supplier').selectpicker('render');
                $('#supplierCurrentBalance').val(accounting.formatMoney(grnData.gSOB));
                $('#supplierCurrentCredit').val(accounting.formatMoney(grnData.gSCB));
                selectedSupplier = grnData.gSID;
                $('#supplierReference').val(grnData.gSR);
                $('#comment').val(grnData.gC);
                $('#retrieveLocation').val(grnData.gRLID);
                $('#retrieveLocation').prop('disabled', true);
                $('#retrieveLocation').selectpicker('render');
                if (toFloat(grnData.gDC) > 0) {
                    $('#deliveryChargeEnable').prop('checked', true);
                    $('.deliCharges').removeClass('hidden');
                    $('#deliveryCharge').val((accounting.unformat(grnData.gDC)).toFixed(2));
                    setPiTotalCost();
                }
                setDataForLocation(grnData.gRLID);
                var currentElem = new Array();
                $.each(respond.data.locationProducts, function(key, valueObj) {
                    if (valueObj.purchase) {
                        currentElem[key] = valueObj;
                    }
                });
                locationProducts = $.extend({}, currentElem);
                setGrnProducts(grnData.gProducts, grnData.gRLID);
                grnProducts = grnData.gProducts;

                $.each(respond.data.docRefData,function(key, value){
                    addDocRef(key, value);
                    updateDocTypeDropDown();
                });
            },
            async: false
        });
    }
    function loadPo(poID) {
        $('#addSupplierBtn').prop('disabled', 1).click(function(e) {
            e.stopPropagation;
            return false;
        });
        eb.ajax({
            type: 'POST',
            url: BASE_URL + '/api/po/getPoDetails',
            data: {poID: poID, toGrn: true},
            success: function(respond) {
                if (respond.data.inactiveItemFlag) {
                    p_notification(false, respond.data.errorMsg);
                    setTimeout(function(){ 
                        window.location.assign(BASE_URL + "/pi")
                    }, 3000);
                    return false;
                }
                var poData = respond.data.poData;
                $("#supplier").empty().
                        append($("<option></option>").
                                attr("value", poData.poSID).
                                text(poData.poSN));
                $('#supplier').val(poData.poSID);
                $('#supplier').prop('disabled', true);
                $('#supplier').selectpicker('render');
                $('#supplierCurrentBalance').val(accounting.formatMoney(poData.poSOB));
                $('#supplierCurrentCredit').val(accounting.formatMoney(poData.poSCB));
                selectedSupplier = poData.poSID;
                $('#deliveryDate').val(poData.poD);
                $('#supplierReference').val(poData.poSR);
                $('#supplierReference').prop('disabled', true);
                $('#comment').val(poData.poDes);
                $('#retrieveLocation').val(poData.poRLID);
                $('#retrieveLocation').prop('disabled', true);
                $('#retrieveLocation').selectpicker('render');
                if (toFloat(poData.poDC) > 0) {
                    $('#deliveryChargeEnable').prop('checked', true);
                    $('.deliCharges').removeClass('hidden');
                    $('#deliveryCharge').val(poData.poDC);
                    setPiTotalCost();
                }
                setDataForLocation(poData.poRLID);
                var currentElem = new Array();
                $.each(respond.data.locationProducts, function(key, valueObj) {
                    if (valueObj.purchase) {
                        currentElem[key] = valueObj;
                    }
                });
                locationProducts = $.extend({}, currentElem);
                setPoProducts(poData.poProducts,poData.poRLID);
                poProducts = poData.poProducts;

                $.each(respond.data.docRefData,function(key, value){
                    addDocRef(key, value);
                    updateDocTypeDropDown();
                });
            },
            async: false
        });
    }

   function loadDataToPi(PIID) {
        efectedPiID = null;
        eb.ajax({
            type: 'POST',
            url: BASE_URL + '/api/pi/get-data-for-update',
            data: {pIID: PIID},
            success: function(respond) {
                var pIData = respond.data.piDetails;
                if (parseFloat(pIData.piPayedAmo) > 0) {
                    p_notification(false, "This Purchase Invoice is partially paid, therefore cannot edit.");
                    setTimeout(function(){ 
                        window.location.assign(BASE_URL + "/pi/list")
                    }, 3000);
                    return false;
                }
                efectedPiCode = pIData.piCd;
                efectedPiID = PIID;
                pIExistingDetails = respond.data.piDetails.piProducts;
                piProductOriginalTax = respond.data.originalTaxDetails;
                $('.createPoDiv').addClass('hidden');
                $('.updatePoDiv').removeClass('hidden');
                $('.updatePoDiv_view').removeClass('hidden');
                if (!$.isEmptyObject(respond.data.uploadedAttachments)) {
                    uploadedAttachments = respond.data.uploadedAttachments;
                }

                $("#supplier").empty().
                        append($("<option></option>").
                                attr("value", pIData.piSupID).
                                text(pIData.piSName));
                $('#supplier').val(pIData.piSupID);
                $('#supplier').prop('disabled', true);
                $('#supplier').selectpicker('render');
                // $('#supplierCurrentBalance').val(accounting.formatMoney(pIData.poSOB));
                // $('#supplierCurrentCredit').val(accounting.formatMoney(pIData.poSCB));
                selectedSupplier = pIData.piSupID;
                $('#issueDate').val(pIData.piIssueD);
                editedIssueDate = pIData.piIssueD;
                $('#deliveryDate').val(pIData.piDD);//.prop('disabled', true);
                $('#supplierReference').val(pIData.piSR);
                $('#comment').val(pIData.piC);
                $("#retrieveLocation").empty().
                        append($("<option></option>").
                                attr("value", pIData.locID).
                                text(pIData.piRL));
                $('#retrieveLocation').val(pIData.locID);
                $('#retrieveLocation').prop('disabled', true);
                $('#retrieveLocation').selectpicker('render');
                $('#productAddOverlay').removeClass('overlay');
                
                

                if (pIData.piWTDT === "presentage") {
                    $("#disc_presentage").prop("checked", true);
                } else if (pIData.piWTDT === 'Value') {
                    $("#disc_value").prop("checked", true);
                }

                if (pIData.piWTDR > 0) {
                    $("#total_discount_rate").val(accounting.formatMoney(pIData.piWTDR)).trigger('keyup');
                }

                // $("#piNo").empty().
                //         append($("<option></option>").
                //                 attr("value", pIData.locID).
                //                 text(pIData.piCd));
                if (toFloat(pIData.piDC) > 0) {
                    $('#deliveryChargeEnable').prop('checked', true);
                    $('.deliCharges').removeClass('hidden');
                    $('#deliveryCharge').val(pIData.piDC);
                    setPiTotalCost();
                }

                if (!$.isEmptyObject(respond.data.dimensionData)) {
                    dimensionData = respond.data.dimensionData;
                }

                if (respond.data.inactiveItemFlag) {
                    p_notification(false, respond.data.errorMsg);
                    setTimeout(function(){ 
                        window.location.assign(BASE_URL + "/pi/list")
                    }, 3000);
                    return false;
                }
                selectedLocation = pIData.locID;
                setDataForLocation(pIData.locID);
                $("#piNo").val(pIData.piCd);
                var currentElem = new Array();
                $.each(respond.data.locationProDetails, function(key, valueObj) {
                    if (valueObj.purchase) {
                        currentElem[key] = valueObj;
                    }
                });
                locationProducts = $.extend({}, currentElem);
                var updatedDataSet = pIData.piProducts;
                for (var i in updatedDataSet) {
                    if (updatedDataSet[i].copyDocType == "10" && pIData.grnID == null) {
                        multipleGrnEditFlag = true;
                    }
                    if (!jQuery.isEmptyObject(updatedDataSet[i].bP)) {
                        for (var k in updatedDataSet[i].bP) {
                            updatedDataSet[i].bP[k].bQ = updatedDataSet[i].bP[k].bQ / parseFloat(locationProducts[updatedDataSet[i].piPID].uom[updatedDataSet[i].piUomID].uC)

                        }
                    }

                    addNewProductRowByEdit({}, {}, updatedDataSet[i], true, true);
                }

                $.each(respond.data.docRefData,function(key, value){
                    addDocRef(key, value);
                    updateDocTypeDropDown();
                });

                if (multipleGrnEditFlag) {
                    disableProductEditing();
                }
                
            },
            async: false
        });
        piEditflag = true;
    }

    // Disable invoice product editing if multiple delivery notes are coppied
    function disableProductEditing() {
        $('#piProductTable').find('#add-new-item-row, .editPiLine, .piDeleteProduct').hide();
        p_notification(false, "This purchase invoice create with multiple Grn. So you can not edit products in this purchase invoice. !");
    }

    function addNewProductRowByEdit(batchProducts, serialProducts, dataSet = null, fromBegin = false, atInitial = false) {
        var productID = dataSet.piPID;
        var iC = dataSet.piPC;
        var pT = dataSet.piPTypeID;
        var iN = dataSet.piPN;
        var uPrice = accounting.formatMoney(dataSet.piPP);
        var uFPrice = dataSet.piPP;
        var qt = dataSet.piPQ;
        if (qt == 0) {
            qt = 0;
        }
        var conversion = locationProducts[dataSet.piPID].uom[dataSet.piUomID].uC;
        var uomqty = dataSet.piPQ / parseFloat(conversion);
        var uomPrice = dataSet.piPP * parseFloat(conversion);
        var nTotal = dataSet.piPT;
        var calNTotal = accounting.unformat(nTotal);

        var gD = dataSet.piPD;
        if (gD == '')
            gD = 0;
        var locationProductID = locationProducts[productID].lPID;
        selectedProductQuantity = uomqty;
        $("input[name=discount]").trigger(jQuery.Event("focusout"));
        var pisubID = dataSet.pIProductID;

        currentItemTaxResults = null;

        var newTrID = 'tr'+ dataSet.piPD +'_' + locationProductID;
        var taxList = [];
        if(fromBegin){
            if(Object.keys(dataSet.pT).length > 0){
                for(i in dataSet.pT){
                    taxList.push(dataSet.pT[i].oTID)
                }
                var tmpItemCost = calculateTaxFreeItemCost(dataSet.piPP, dataSet.piPQ, dataSet.piPD);
                currentItemTaxResults = calculateItemCustomTax(tmpItemCost, taxList);

            }
        } else {
            if(dataSet.currentItemTaxResult != null){
                if(Object.keys(dataSet.currentItemTaxResult).length > 0 ){
                    currentItemTaxResults = dataSet.currentItemTaxResult;
                }

            }
        }
        var clonedRow = $($('#preSetSample').clone()).attr('id', newTrID).addClass('addedProducts').attr('data-pisubid', pisubID);
        clonedRow.children('#code').html(iC + ' - ' + iN);
        clonedRow.children('#quantity').html(uomqty);
        clonedRow.children('#unit').html(uomPrice);
        clonedRow.children('.editPi').find('.editPiLine').removeClass('hidden');
        if(locationProducts[productID].bP != 0){
            if(fromBegin){
                batchProducts = {};
                $.each(dataSet.bP, function(key, value){
                    batchProducts[value.bC] = new batchProduct(value.bC, value.bQ, value.bMD, value.bED, value.bW, value.bID);
                });
            }
        } else if(locationProducts[productID].sP != 0) {
            if(fromBegin){
                serialProducts = {};
                $.each(dataSet.sP, function(key, value){
                    serialProducts[value.sC] = new serialProduct(value.sC, value.sW, value.sBC, value.sED, value.sID,value.sWT);
                });
            }
        }
        if (atInitial) {
            var btchQty = 0;
            var bCode;
            if (!jQuery.isEmptyObject(dataSet.bSP)) {
                $.each(dataSet.bSP, function(key, value){
                    if (bCode != value.bC) {
                        btchQty = 0;
                    }
                    bCode = value.bC;
                    btchQty++;
                    batchProducts[value.bC] = new batchProduct(value.bC, btchQty, locationProducts[productID].batch[value.bID].bMD, locationProducts[productID].batch[value.bID].bED, locationProducts[productID].batch[value.bID].bW, value.bID);
                    serialProducts[value.sC] = new serialProduct(value.sC, value.sW, value.sBC, value.sED, value.sID , value.sWT);
                });
            }
        }

        clonedRow.children().children('#uomName').html(dataSet.piPUom);
        selectedProductUom = dataSet.piUomID;

        clonedRow.children('#disc').html(gD);
        clonedRow.children('#ttl').html(accounting.formatMoney(nTotal));

        if (locationProducts[productID].bP == 0 && locationProducts[productID].sP == 0) {
            clonedRow.children().children('#viewSubProducts').addClass('hidden');
        }

        var clonedTax = $($('#addNewTaxDiv').clone()).attr('id', '');
        if (currentItemTaxResults != null) {
            clonedTax.children('#addNewTax').find('#taxApplied').removeClass('glyphicon glyphicon-unchecked');
            clonedTax.children('#addNewTax').find('#taxApplied').addClass('glyphicon glyphicon-check');
            // clonedTax.children('#addTaxUl').children('#sampleLi').remove();

            for (var i in currentItemTaxResults.tL) {
                var clonedLi = $(clonedTax.find('#sampleLi').clone()).removeClass('hidden').attr('id', 'li_' + i).addClass("tempLi");
                clonedLi.children(".taxChecks").attr('id', productID + '_' + i).prop('checked', true).addClass('addNewTaxCheck');

                clonedLi.children(".taxName").html('&nbsp&nbsp' + currentItemTaxResults.tL[i].tN + '&nbsp&nbsp' + currentItemTaxResults.tL[i].tP + '%').attr('for', productID + '_' + i);
                clonedLi.insertBefore(clonedTax.find('#sampleLi'));
            }
        } else {
            clonedTax.children('#addNewTax').children('#taxApplied').attr('id', '');
            clonedTax.children('#addNewTax').attr('id', '');
            clonedTax.children('#addTaxUl').children().removeClass('tempLi');
            clonedTax.children('#addTaxUl').children().children().removeClass('addNewTaxCheck');
            clonedTax.children('#addTaxUl').children().children().attr('disabled', true);
            clonedTax.children('#addTaxUl').children('#sampleLi').remove();
            clonedTax.children('#addTaxUl').attr('id', '');

        }
        clonedRow.children('#tax').html(clonedTax);
        clonedRow.removeClass('hidden');
        clonedRow.insertBefore('#preSetSample');
        var newProduct = locationProducts[productID];
        var updatedFlag = true;
        $.each(pIExistingDetails, function(key, value){
            if(value.pIProductID == pisubID && value.copyDocID != null && value.copyDocType != null){
                updatedFlag = false;
            }
        });

        var dType = null;
        if (newProduct.pPDP != null && newProduct.pPDV == null) {
            dType = 'precentage';
        } else if (newProduct.pPDP == null && newProduct.pPDV != null) {
            dType = 'value';

        }

        products.push(new product(newProduct.lPID, productID, iC, iN, qt, gD, toFloat(uFPrice), selectedProductUom, calNTotal, currentItemTaxResults, batchProducts, serialProducts, pT, updatedFlag, null, pisubID, null, null, null, dType));
        setPiTotalCost();
        setTotalTax();
        $('#itemCode').val(0).trigger('change').empty().selectpicker('refresh');
        clearModalWindow();
    }

    function addDocRef(key, data) {

        var $cloneRow = $($('.sample').clone()).removeClass('sample hidden').addClass('documentRow');

        //set document type
        $cloneRow.find('.documentTypeId').val(key);
        $cloneRow.find('.documentTypeId').selectpicker('render');
        $cloneRow.find('.documentTypeId').attr('disabled', true);
        documentTypeArray.push(key);

        loadDocumentId($cloneRow, key);

        selectedDocID = [];

        $cloneRow.find('select.documentId').append('<optgroup label="Currently Selected"></optgroup>');
        $.each(data,function(index, value){
            $cloneRow.find('optgroup').append('<option class="slc" value="'+value.id+'">'+value.code+'</option>');
            selectedDocID.push(value.id);
        });

        $cloneRow.find('select.documentId').selectpicker('val', selectedDocID);
        $cloneRow.find('select.documentId').selectpicker('render');
        $cloneRow.find('select.documentId').selectpicker('refresh');
        $cloneRow.find('select.documentId').attr('disabled', true);

        $cloneRow.find('.saveDocument').addClass('hidden');
        $cloneRow.find('.deleteDocument').removeClass('hidden');
        $( ".documentTypeBody" ).append($cloneRow);
    }

    $('#piProductTable tbody').on('click', '.editPiLine', function(){
        if(!itemEditOpenFlag){
            var id = $(this).parents('tr').attr('data-pisubid');
            var lineID = $(this).parents('tr').attr('id');
            var deletedIndex;
            $.each(products, function(key, value){
                if(value == null){
                    return true;
                } else if(value.pisubProID == id){
                    normalProRealQty[value.pisubProID] = null;
                    deletedIndex = key;
                    normalProRealQty[value.pisubProID] = value.pQuantity;
                    setPiProducts(value);
                    setPiTotalCost();
                    setTotalTax();
                }
            });

            $('#'+lineID).remove();
            delete(products[deletedIndex]);
            itemEditOpenFlag = true;

        } else {
            p_notification(false, eb.getMessage('ERR_OPEN_ANOTHER_LINE'));
        }

    });


    function setPiProducts(preProducts) {
        originalTaxes = piProductOriginalTax[preProducts.pID]; 
        if ($('#addNewItemTr').hasClass('hidden')) {
            $('#addNewItemTr').removeClass('hidden');
        }
        $('#add-new-item-row').find('tr').each(function() {
            if (this.id != 'addNewItemTr') {
                $(this).remove();
            }
        });
            var preProduct = preProducts;
            var clonedAddNew = $($('#addNewItemTr').clone()).attr('id', 'pIEditPro_' + preProduct.pID).attr('data-piproid', preProduct.pisubProID).addClass('tempPIPro');
            $("#itemCode", clonedAddNew).empty().
                    append($("<option></option>").
                            attr("value", preProduct.pID).
                            text(preProduct.pName + '-' + preProduct.pCode));
            $('#itemCode', clonedAddNew).val(preProduct.pID);
            $('div.bootstrap-select', clonedAddNew).remove();
            $('#itemCode', clonedAddNew).selectpicker('render');
            $('#itemCode', clonedAddNew).data('PT', preProduct.productType);
            $('#itemCode', clonedAddNew).data('PN', preProduct.pName);
            $('#itemCode', clonedAddNew).data('PC', preProduct.pCode);
            $('#itemCode', clonedAddNew).prop('disabled', true);
            $('#itemCode', clonedAddNew).attr('id', '');
            $('#itemCode', clonedAddNew).selectpicker('render');
            if (preProduct.pQuantity == null) {
                preProduct.pQuantity = 0;
            }
            var poRemainQty = toFloat(preProduct.pQuantity);


            var baseUom;
            for (var j in locationProducts[preProduct.pID].uom) {
                if (locationProducts[preProduct.pID].uom[j].pUBase == 1) {
                    baseUom = locationProducts[preProduct.pID].uom[j].uomID;
                }
            }
            
            $('#qty', clonedAddNew).val(poRemainQty);
            $('#qty', clonedAddNew).addUom(locationProducts[preProduct.pID].uom, preProduct.pUom);
            $('#qty', clonedAddNew).parent().addClass('input-group');
            $('#qty', clonedAddNew).attr('id', '');
            if (!jQuery.isEmptyObject(locationProducts[preProduct.pID].batch)) {
                $('.uomqty', clonedAddNew).prop('disabled', true);
            } else if (!jQuery.isEmptyObject(locationProducts[preProduct.pID].batchSerial)) {
                $('.uomqty', clonedAddNew).prop('disabled', true);
            } else if (!jQuery.isEmptyObject(locationProducts[preProduct.pID].serial)) {
                $('.uomqty', clonedAddNew).prop('disabled', true);
            }
            $('#addNewUomDiv', clonedAddNew).attr('id', '');
            $('#unitPrice', clonedAddNew).val(preProduct.pUnitPrice);
            $('#unitPrice', clonedAddNew).addUomPrice(locationProducts[preProduct.pID].uom, preProduct.pUom);
            $('#unitPrice', clonedAddNew).attr('id', '');

            if (locationProducts[preProduct.pID].pPDV) {
                $('#piDiscount',clonedAddNew).val(preProduct.pDiscount).addUomPrice(locationProducts[preProduct.pID].uom, baseUom);
                $('#piDiscount',clonedAddNew).siblings('.uom-price-select').find('.uom_price_drop_down').attr('disabled', true);
                $(".sign", clonedAddNew).text('Rs');
                $('#piDiscount', clonedAddNew).attr('id', '');
            }
            if (locationProducts[preProduct.pID].pPDP) {
                $('#piDiscount',clonedAddNew).val(preProduct.pDiscount);
                $(".sign", clonedAddNew).text('%');
                $('#piDiscount', clonedAddNew).attr('id', '');
            }

            if (locationProducts[preProduct.pID].pPDP == null && locationProducts[preProduct.pID].pPDV == null) {
                $('#piDiscount',clonedAddNew).attr('readonly', true);
                $('#piDiscount', clonedAddNew).attr('id', '');
            }
            $('#addNewTotal', clonedAddNew).html(accounting.formatMoney(preProduct.pTotal));
            $('#addNewTotal', clonedAddNew).attr('id', '');
            $('#addNewUom', clonedAddNew).prop('disabled', true);
            $('#addNewUom', clonedAddNew).attr('id', '');
            $('#uomAb', clonedAddNew).html(preProduct.pUom);
            $('#uomAb', clonedAddNew).attr('id', '');
            $('#addUomUl', clonedAddNew).attr('id', '');

            // setting tax for products
                $('#addNewTaxDiv', clonedAddNew).attr('id', "poTaxDiv");
                $('#addNewTax', clonedAddNew).attr('id', "poAddTaxDiv");
                $('#taxApplied', clonedAddNew).attr('id', 'poTaxApplied')
                $('#addTaxUl', clonedAddNew).attr('id', 'poAddTaxUl');
                $('.tempLi', clonedAddNew).remove();
            if (!jQuery.isEmptyObject(preProduct.pTax)) {
                var currentProductTax = preProduct.pTax;
                $('#taxApplied', clonedAddNew).removeClass('glyphicon glyphicon-unchecked');
                $('#taxApplied', clonedAddNew).addClass('glyphicon glyphicon-check');
                for (var i in currentProductTax.tL) {
                    var clonedLi = $($('#sampleLi', clonedAddNew).clone()).removeClass('hidden').attr('id', 'li_' + i).addClass("poTempLi");
                    clonedLi.children(".taxChecks").attr('id', preProduct.poPID + '_' + i).prop('checked', true).addClass('addNewTaxCheck');
                    clonedLi.children(".taxName").html('&nbsp&nbsp' + currentProductTax.tL[i].tN + '&nbsp&nbsp' + currentProductTax.tL[i].tP + '%').attr('for', preProduct.poPID + '_' + i);
                    clonedLi.insertBefore($('#sampleLi', clonedAddNew));
                }
                $('#sampleLi', clonedAddNew).attr('id', 'poSampleLi');
            } else {
                if (!jQuery.isEmptyObject(originalTaxes)) {
                    $('#taxApplied', clonedAddNew).removeClass('glyphicon glyphicon-unchecked');
                    $('#taxApplied', clonedAddNew).addClass('glyphicon glyphicon-check');
                    for (var i in originalTaxes) {
                        var clonedLi = $($('#sampleLi',clonedAddNew).clone()).removeClass('hidden').attr('id', 'li_' + i).addClass("tempLi");
                        clonedLi.children(".taxChecks").attr('id', preProducts.pID + '_' + i).prop('checked', false).addClass('addNewTaxCheck');
                        if (originalTaxes[i].state == 0) {
                            clonedLi.children(".taxChecks").attr('disabled', 'disabled');
                            clonedLi.children(".taxName").addClass('crossText');
                        }
                        clonedLi.children(".taxName").html('&nbsp&nbsp' + originalTaxes[i].taxName + '&nbsp&nbsp' + originalTaxes[i].taxPrecentage + '%').attr('for', preProducts.pID + '_' + i);
                        clonedLi.insertBefore($('#sampleLi',clonedAddNew));

                    }
                } else {
                    $('#sampleLi', clonedAddNew).remove();
                    
                }
            }

            $('#tax', clonedAddNew).attr('id', '');
            $('#addItem', clonedAddNew).attr('id', '').addClass('pIEditAddItem').attr('data-lineid', 'pIEditPro_' + preProduct.pID);
            $('#removeItem', clonedAddNew).attr('id', '').addClass('pIEditRemoveItem').removeClass('hidden');
            clonedAddNew.insertBefore('#addNewItemTr');
        // }
        //trigger focustout to poProducts-avoid recalculating costs here
        $('tr.tempPoPro', '#piProductTable').find(".uomPrice").trigger('focusout');
    }

    $('#piProductTable').on('click', '.pIEditRemoveItem', function(){
        var remID = $(this).parents("tr").attr('id');
        $('#'+remID).remove();
        setPiTotalCost();
        setTotalTax();
    });


    $('#piProductTable').on('click', '.pIEditAddItem', function() {
        removedLineID = null;
        editedPiBatachSerialRow = null;
        thisIsPIEditFlag = true;
        var tr = $(this).parents("tr");
        editedPiBatachSerialRow = tr;
        var lineID = $(this).attr('data-lineid');
        tr.find(".uomPrice").trigger('focusout');
        var addProductID = tr.attr('id').split('pIEditPro_')[1].trim();
        var piProID = tr.attr('data-piproid');
        addingPIPro = locationProducts[addProductID].pC;
        piEditedDataSet = {};
       
        piEditedDataSet.piPID = addProductID;
        $.each(locationProducts, function(key, value){
            if(value.pID == addProductID){
                piEditedDataSet.piPC = value.pC;
                piEditedDataSet.piPTypeID = value.pT;
                piEditedDataSet.piPN = value.pN;
            }
        });
        // piEditedDataSet.piPP = accounting.formatMoney(tr.find('.uomPrice').val());
        piEditedDataSet.piPP = tr.find("input[name='unitPrice']").val();
        piEditedDataSet.piPQ = tr.find("input[name='qty']").val();
        piEditedDataSet.piPT = productLineTotal[locationProducts[addProductID].pID+'_'+$("input[name='unitPrice']", $(this).parents('tr')).val()];
        piEditedDataSet.piPD = tr.find("input[name=discount]").val();
        piEditedDataSet.piUomID = tr.find(".uomqty").siblings('.uom-select').children('button').find('.selected').data('uomID');
        piEditedDataSet.piPUom = locationProducts[piEditedDataSet.piPID].uom[piEditedDataSet.piUomID].uA;
        piEditedDataSet.pIProductID = piProID;

        var taxResult = calculateTaxForUpdatePi(tr, piEditedDataSet.piPQ, piEditedDataSet.piPP, piEditedDataSet.piPD);
        piEditedDataSet.currentItemTaxResult = taxResult;
        $.each(pIExistingDetails, function(key, value){
            if(toFloat(piProID) == toFloat(value.pIProductID)){
                if(Object.keys(value.bSP).length > 0){
                    $('#addInvoiceProductsModal').modal('show');
                    $('#addInvoiceProductsModal').find('#batch_data').find('tr.remove_row').remove();
                    addBatchSerialProduct(value.piPID, value.piPC, value.bSP, lineID);
                    removedLineID = tr.attr('id');
                } else if(Object.keys(value.sP).length > 0){
                    $('#addInvoiceProductsModal').modal('show');
                    $('#addInvoiceProductsModal').find('#batch_data').find('tr.remove_row').remove();
                    addSerialProduct(value.piPID, value.piPC, value.sP, lineID);
                    removedLineID = tr.attr('id');

                } else if( Object.keys(value.bP).length > 0){
                    $('#addInvoiceProductsModal').modal('show');
                    $('#addInvoiceProductsModal').find('#batch_data').find('tr.remove_row').remove();
                    addBatchProduct(value.piPID, value.piPC, value.bP, lineID);
                    removedLineID = tr.attr('id');
                } else {

                    if(value.copyDocID != null && value.copyDocType != null &&
                            toFloat(piEditedDataSet.piPQ) > toFloat(normalProRealQty[value.pIProductID])){
                        p_notification(false, eb.getMessage('ERR_QTY_LEVEL'));
                        return false;
                    } else {
                        $("#"+tr.attr('id')).remove();
                        addNewProductRowByEdit({}, {}, piEditedDataSet, false);

                        itemEditOpenFlag = false;

                    }
                }

            }
        });
    });


    $('#addInvoiceProductsModal').on('click', '#batch-save', function(e){
        e.preventDefault();
        var curProID = $(this).attr('data-productid');
        var editedLineID = $(this).attr('data-lineid');
        var $batchTable = $("#addInvoiceProductsModal .batch-table tbody");
        if($(this).hasClass('batchAdd')){
            var totalQty = 0;
            var validFlag = true;
            $("input[name='deliverQuantity']", $batchTable).each(function() {
                var row = $(this).parents('tr');
                if(row.attr('id') != 'defaultRow'){
                    var exitQty = $("input[name='availableQuantity']", row).val();

                    if(toFloat($(this).val()) > toFloat(exitQty)){
                        p_notification(false, eb.getMessage('ERR_BATCH_QTY'));
                        validFlag = false;
                        return false;
                    } else {
                        var bCode = row.attr('data-btchcode');
                        var Qty = $(this).siblings('.uomqty').val();
                        var actQty = $("input[name='deliverQuantity']", row).val();
                        var wrnty = $("input[name='warranty']", row).val();
                        var expDate = $("input[name='expireDate']", row).val();
                        var uprice = $("input[name='btPrice']", row).val();
                       batchProducts[bCode] = new batchProduct(bCode,Qty, '', expDate, wrnty, row.attr('id'));
                    }
                }
                totalQty += toFloat(actQty);
            });
            if(toFloat(totalQty) == 0){
                p_notification(false, eb.getMessage('ERR_BATCH_QTY_CANNOT_BE_NULL'));
                validFlag = false;
                return false;
            }

            if(validFlag){
                var taxResult = calculateTaxForUpdatePi(editedPiBatachSerialRow, totalQty, piEditedDataSet.piPP, piEditedDataSet.piPD);
                piEditedDataSet.currentItemTaxResult = taxResult;
                currentTaxAmount = (taxResult == null) ? null : taxResult.tTA;
                var discountType = null;
                if (locationProducts[piEditedDataSet['piPID']].pPDP != null) {
                    discountType = "Per";
                } else if (locationProducts[piEditedDataSet['piPID']].pPDV != null) {
                    discountType = "Val";
                } else {
                   discountType = "Non";
                }

                if (discountType == "Val") {
                    var singleItemCost = calculateTaxFreeItemCostByValueDiscount(piEditedDataSet.piPP, totalQty, piEditedDataSet.piPD);
                } else if (discountType == "Per") {
                    var singleItemCost = calculateTaxFreeItemCost(piEditedDataSet.piPP, totalQty, piEditedDataSet.piPD);
                } else {
                    var singleItemCost = calculateTaxFreeItemCost(piEditedDataSet.piPP, totalQty, piEditedDataSet.piPD);
                }
            
                piEditedDataSet.piPT = toFloat(singleItemCost) + toFloat(currentTaxAmount);
                piEditedDataSet.piPQ = totalQty;
                addNewProductRowByEdit(batchProducts, {}, piEditedDataSet, false);
                $('#'+removedLineID).remove();
                $('#addInvoiceProductsModal').modal('hide');
                itemEditOpenFlag = false;

            }

        } else if($(this).hasClass('serialAdd')){
            var totalQty = 0;
            $("input[name='deliverQuantityCheck']:checked", $batchTable).each(function() {
                var row = $(this).parents('tr');
                if(row.attr('id') != 'defaultRow'){
                    totalQty++;
                    var sCode = row.attr('data-serialcode');
                    var bCode = row.attr('data-btchcode');
                    var Qty = 1;
                    var wrnty = $("input[name='warranty']", row).val();
                    var wrntyType = $("input[name='warrantyType']", row).val();
                    var expDate = $("input[name='expireDate']", row).val();
                    var uprice = $("input[name='btPrice']", row).val();
                    serialProducts[sCode] = new serialProduct(sCode, wrnty, bCode, expDate, row.attr('id'),wrntyType);
                }

            });
            if(toFloat(totalQty) == 0){
                p_notification(false, eb.getMessage('ERR_SERIAL_QTY_CANNOT_BE_NULL'));
                validFlag = false;
                return false;
            } else {
                var taxResult = calculateTaxForUpdatePi(editedPiBatachSerialRow, totalQty, piEditedDataSet.piPP, piEditedDataSet.piPD);
                piEditedDataSet.currentItemTaxResult = taxResult;
                currentTaxAmount = (taxResult == null) ? null : taxResult.tTA;
                var singleItemCost = calculateTaxFreeItemCost(piEditedDataSet.piPP, totalQty, piEditedDataSet.piPD);
                piEditedDataSet.piPT = toFloat(singleItemCost) + toFloat(currentTaxAmount);
                piEditedDataSet.piPQ = totalQty;
                addNewProductRowByEdit({}, serialProducts, piEditedDataSet, false);
                $('#'+removedLineID).remove();
                $('#addInvoiceProductsModal').modal('hide');
                itemEditOpenFlag = false;
            }

        }

        if($(this).hasClass('batchAddForBatchSerial')){
           var totalQty = 0;
           var bQty = 1;
           var bCode;
           var batchPro = [];
            $("input[name='deliverQuantityCheck']:checked", $batchTable).each(function() {
                var row = $(this).parents('tr');
                if(row.attr('id') != 'defaultRow'){
                    totalQty++;
                    bQty++;
                    var sCode = row.attr('data-serialcode');
                    if (bCode != row.attr('data-btchcode')) {
                        bQty = 1;
                    }
                    bCode = row.attr('data-btchcode');
                    var bID = row.attr('data-btchID');
                    var Qty = 1;
                    var wrnty = $("input[name='warranty']", row).val();
                    var wrntyType = $("input[name='warrantyType']", row).val();
                    var expDate = $("input[name='expireDate']", row).val();
                    var uprice = $("input[name='btPrice']", row).val();
                    serialProducts[sCode] = new serialProduct(sCode, wrnty, bCode, expDate, row.attr('id'),wrntyType);
                    batchProducts[bCode] = new batchProduct(bCode,bQty, '', expDate, wrnty, bID);
                }
            });

            if(toFloat(totalQty) == 0){
                p_notification(false, eb.getMessage('ERR_SERIAL_QTY_CANNOT_BE_NULL'));
                validFlag = false;
                return false;
            } else {
                var taxResult = calculateTaxForUpdatePi(editedPiBatachSerialRow, totalQty, piEditedDataSet.piPP, piEditedDataSet.piPD);
                piEditedDataSet.currentItemTaxResult = taxResult;
                currentTaxAmount = (taxResult == null) ? null : taxResult.tTA;
                var singleItemCost = calculateTaxFreeItemCost(piEditedDataSet.piPP, totalQty, piEditedDataSet.piPD);
                piEditedDataSet.piPT = toFloat(singleItemCost) + toFloat(currentTaxAmount);
                piEditedDataSet.piPQ = totalQty;
                addNewProductRowByEdit(batchProducts, serialProducts, piEditedDataSet, false);
                $('#'+removedLineID).remove();
                $('#addInvoiceProductsModal').modal('hide');
                itemEditOpenFlag = false;
            }
        }
    });
        ////////////////////////
        function addBatchProduct(productID, productCode, batchProduct, lineID) {

            $('.warranty').removeClass('hidden');
            $('.expDate').removeClass('hidden');
            for (var i in batchProduct) {
                    var batchKey = productCode + "-" + batchProduct[i].bID;
                    var $clonedRow = $('.pi_batch_serial_row').clone();
                    $clonedRow
                            .attr('id', batchProduct[i].bID)
                            .removeClass('hidden pi_batch_serial_row')
                            .addClass('remove_row')
                            .attr('data-btchcode', batchProduct[i].bC);
                    $(".batchCode", $clonedRow).html(batchProduct[i].bC).data('id', batchProduct[i].bID);
                    $(".delQty", $clonedRow).removeClass('hidden');
                    $("input[name='warranty']", $clonedRow).val(batchProduct[i].bW);
                    $("input[name='expireDate']", $clonedRow).val(batchProduct[i].bED).datepicker();
                    // $("input[name='btPrice']", $clonedRow).val(parseFloat(batchProduct[i].PBUP).toFixed(2));

                    if (batchProducts[batchKey]) {
                        $("input[name='deliverQuantity']", $clonedRow).val(batchProducts[batchKey].Qty);
                    }

                    var conversion = locationProducts[productID].uom[piEditedDataSet.piUomID].uC;
                    $("input[name='availableQuantity']", $clonedRow).val(batchProduct[i].bQ * conversion);
                    $("input[name='availableQuantity'],input[name='deliverQuantity']", $clonedRow).addUom(locationProducts[productID].uom);
                    $clonedRow.insertBefore('.pi_batch_serial_row');

            }
                    $('#addInvoiceProductsModal').find('.batch-serial-add').removeClass('serialAdd').addClass('batchAdd').attr('data-productid', productID).attr('data-lineid', lineID);

    }

    function addSerialProduct(productID, productCode, serialProductSet, lineID) {

        for (var i in serialProductSet) {
            var serialQty =  1;
            var $clonedRow = $('.pi_batch_serial_row').clone();
            $clonedRow
                    .attr('id', serialProductSet[i].sID)
                    .removeClass('hidden pi_batch_serial_row')
                    .addClass('remove_row')
                    .attr('data-serialcode', serialProductSet[i].sC);
            $(".serialID", $clonedRow)
                    .html(serialProductSet[i].sC)
                    .data('id', serialProductSet[i].sID);
            $("input[name='availableQuantity']", $clonedRow).val(serialQty);
            $("input[name='warranty']", $clonedRow).val(serialProductSet[i].sW);
            let wPeriod = "";
                        if(serialProductSet[i].sWT==='4'){
                            wPeriod = "Years"
                        }else if(serialProductSet[i].sWT==='3'){
                            wPeriod = "Months"
                        }else if(serialProductSet[i].sWT==='2'){
                            wPeriod = "Weeks"
                        }else{
                            wPeriod = "Days"
                        }
            $("input[name='warrantyType']",$clonedRow).val(wPeriod).attr('disabled',true);

            $("input[name='expireDate']", $clonedRow).val(serialProductSet[i].sED).attr('disabled',true);
            $('.delCheck', $clonedRow).removeClass('hidden');
            $("input[name='deliverQuantityCheck']", $clonedRow).attr('checked', true);

            $("input[name='availableQuantity'],input[name='deliverQuantity']", $clonedRow).addUom(locationProducts[productID].uom);
            $clonedRow.insertBefore('.pi_batch_serial_row');

        }

       $('#addInvoiceProductsModal').find('.batch-serial-add').removeClass('batchAdd').addClass('serialAdd').attr('data-productid', productID).attr('data-lineid', lineID);

    }
    
    function addBatchSerialProduct(productID, productCode, batchSerialProductSet, lineID) {

        for (var i in batchSerialProductSet) {
            var serialQty =  1;
            var $clonedRow = $('.pi_batch_serial_row').clone();
            var batchKey = productCode + "-" + batchSerialProductSet[i].bID;
            $clonedRow
                    .attr('id', batchSerialProductSet[i].sID)
                    .removeClass('hidden pi_batch_serial_row')
                    .addClass('remove_row')
                    .attr('data-serialcode', batchSerialProductSet[i].sC)
                    .attr('data-btchcode', batchSerialProductSet[i].bC)
                    .attr('data-btchID', batchSerialProductSet[i].bID);
            $(".serialID", $clonedRow)
                    .html(batchSerialProductSet[i].sC)
                    .data('id', batchSerialProductSet[i].sID);
            $(".batchCode", $clonedRow).html(batchSerialProductSet[i].bC).data('id', batchSerialProductSet[i].bID);
            $("input[name='availableQuantity']", $clonedRow).val(serialQty);
            $('.delCheck', $clonedRow).removeClass('hidden');
            $("input[name='deliverQuantityCheck']", $clonedRow).attr('checked', true);

            $("input[name='availableQuantity'],input[name='deliverQuantity']", $clonedRow).addUom(locationProducts[productID].uom);
            $clonedRow.insertBefore('.pi_batch_serial_row');

        }

        $('#addInvoiceProductsModal').find('.batch-serial-add').removeClass('batchAdd').addClass('batchAddForBatchSerial').attr('data-productid', productID).attr('data-lineid', lineID);
    }

    function calculateTaxForUpdatePi(row, qty, unitPrice, discount)
    {
        var checkedTaxes = Array();
        $('input:checkbox.addNewTaxCheck', row).each(function() {
            if (this.checked) {
                var cliTID = this.id;
                var tTaxID = cliTID.split('_')[1];
                checkedTaxes.push(tTaxID);
            }
        });
        var tmpItemCost = calculateTaxFreeItemCost(unitPrice, qty, discount);
        currentItemTaxResults = calculateItemCustomTax(tmpItemCost, checkedTaxes);
        currentTaxAmount = (currentItemTaxResults == null) ? null : currentItemTaxResults.tTA;
        return currentItemTaxResults;
    }

    function calculateTotalForUpdatedBatchSerials()
    {
        var tmpItemCost = calculateTaxFreeItemCost(unitPrice, qty, discount);
        currentItemTaxResults = calculateItemCustomTax(tmpItemCost, checkedTaxes);
        currentTaxAmount = (currentItemTaxResults == null) ? null : currentItemTaxResults.tTA;
        var itemCost = toFloat(tmpItemCost) + toFloat(currentTaxAmount);
        $('.itemLineTotal', $(this).parents('tr')).html(accounting.formatMoney(itemCost));
    }

    // function addSerialBatchProduct(productID, productCode, deleted) {

    //     if ((!$.isEmptyObject(currentProducts[productID].batchSerial))) {
    //         productBatchSerial = currentProducts[productID].batchSerial;
    //         for (var i in productBatchSerial) {
    //             if (deleted != 1) {
    //                 var serialKey = productCode + "-" + productBatchSerial[i].PBID;
    //                 var serialQty = (productBatchSerial[i].PSS == 1) ? 0 : 1;
    //                 var $clonedRow = $($('.batch_row').clone());
    //                 $clonedRow
    //                         .data('id', i)
    //                         .removeAttr('id')
    //                         .removeClass('hidden batch_row')
    //                         .addClass('remove_row');
    //                 $(".serialID", $clonedRow)
    //                         .html(productBatchSerial[i].PSC)
    //                         .data('id', productBatchSerial[i].PSID);
    //                 $(".batchCode", $clonedRow)
    //                         .html(productBatchSerial[i].PBC)
    //                         .data('id', productBatchSerial[i].PBID);
    //                 $("input[name='availableQuantity']", $clonedRow).val(serialQty);
    //                 $("input[name='deliverQuantity']", $clonedRow).parent().remove();
    //                 $("input[name='btPrice']", $clonedRow).val(parseFloat(productBatchSerial[i].PBUP).toFixed(2));
    //                 if (priceListItems[productID] != undefined && parseFloat(priceListItems[productID].itemPrice) > 0) {
    //                     $("input[name='btPrice']", $clonedRow).val(parseFloat(priceListItems[productID].itemPrice).toFixed(2));
    //                 }
    //                 if (currentProducts[productID].giftCard == 0) {
    //                     $(".warranty").removeClass('hidden');
    //                     $(".expDate").removeClass('hidden');
    //                     $("input[name='warranty']", $clonedRow).val(productBatchSerial[i].PBSWoD);
    //                     $("input[name='expireDate']", $clonedRow).val(productBatchSerial[i].PBSExpD);
    //                     if(!(productBatchSerial[i].PBSExpD == null || productBatchSerial[i].PBSExpD == '0000-00-00')){
    //                         var expireCheck = checkEpireData(productBatchSerial[i].PBSExpD);
    //                         if(expireCheck){
    //                             $("input[name='deliverQuantityCheck']", $clonedRow).attr('disabled',true);
    //                         }
    //                     }
    //                 }
    //                 if (batchProducts[serialKey]) {
    //                     if (batchProducts[serialKey].Qty == 1) {
    //                         $("input[name='deliveryQuantityCheck']", $clonedRow).attr('checked', true);
    //                     }
    //                 }

    //                 $clonedRow.insertBefore('.batch_row');
    //                 $("input[name='availableQuantity']", "input[name='deliverQuantity']", $clonedRow).addUom(currentProducts[productID].uom);
    //             } else {
    //                 batchProducts[serialKey] = undefined;
    //             }
    //         }
    //     }
    // }

    function calculateItemTotal(thisRow, productID) {
        var thisRow = thisRow;
        var checkedTaxes = Array();
        thisRow.find('input:checkbox.taxChecks').each(function() {
            if (this.checked) {
                var cliTID = this.id;
                var tTaxID = cliTID.split('_')[1];
                checkedTaxes.push(tTaxID);
            }
        });
        
        var qty = thisRow.find('#quantity').html();
        var unitPrice = thisRow.find('#unit').html();
        var uP = thisRow.find('.uomPrice').html();

        var discount = thisRow.find('#disc').html();
        var discountType;
        if (locationProducts[productID]) {
            if (locationProducts[productID].pPDP != null) {
                discountType = "Per";
            } else if (locationProducts[productID].pPDV != null) {
                discountType = "Val";
            } else {
               discountType = "Non";
            }
            if (discountType == "Val") {
                var newDiscount = validateDiscount(productID,discountType,discount, unitPrice);
                var tmpItemCost = calculateTaxFreeItemCostByValueDiscount(unitPrice, qty, newDiscount);
                if (tmpItemCost < 0) {
                    tmpItemCost = 0;
                }
            } else if (discountType == "Per") {
                var newDiscount = validateDiscount(productID,discountType,discount, unitPrice);
                var tmpItemCost = calculateTaxFreeItemCost(unitPrice, qty, newDiscount);
            } else {
                newDiscount = 0;
                var tmpItemCost = calculateTaxFreeItemCost(unitPrice, qty, newDiscount);
            }

            if ($('#total_discount_rate').val() != '' && $('input[name=discount_type]:checked').val() == 'presentage') {
                var invoiceDiscountValue = $('#total_discount_rate').val();
                tmpItemCost -= (tmpItemCost * (toFloat(invoiceDiscountValue) / toFloat(100)));
            }

            $("#disc", thisRow).html(newDiscount);
            currentItemTaxResults = calculateItemCustomTax(tmpItemCost, checkedTaxes);
            currentTaxAmount = (currentItemTaxResults == null) ? null : currentItemTaxResults.tTA;
            var itemCost = toFloat(tmpItemCost) + toFloat(currentTaxAmount);
            $('#ttl', thisRow).html(accounting.formatMoney(itemCost));

            objIndex = products.findIndex((obj => obj.pID == productID));
            products[objIndex].pTotal = itemCost;

            if (productID != undefined) {
                var currentEl = new Array();
                currentEl[productID+'_'+unitPrice] = itemCost;
                productLineTotal = $.extend(productLineTotal, currentEl);
            }
        } else {
            $("#disc", thisRow).html(discount);
            var tmpItemCost = calculateTaxFreeItemCost(unitPrice, qty, discount);
            currentItemTaxResults = calculateItemCustomTax(tmpItemCost, checkedTaxes);
            currentTaxAmount = (currentItemTaxResults == null) ? null : currentItemTaxResults.tTA;
            var itemCost = toFloat(tmpItemCost) + toFloat(currentTaxAmount);

            $('#ttl', thisRow).html(accounting.formatMoney(itemCost));
            objIndex = products.findIndex((obj => obj.pID == productID));

            products[objIndex].pTotal = itemCost;
            if (productID != undefined) {
                var currentEl = new Array();
                currentEl[productID+'_'+unitPrice] = itemCost;
                productLineTotal = $.extend(productLineTotal, currentEl);
            }
        }
    }

    function calculateItemTotalForAddNewItemRow(thisRow, productID) {
        var thisRow = thisRow;
        var checkedTaxes = Array();

        thisRow.find('input:checkbox.taxChecks').each(function() {
            if (this.checked) {
                var cliTID = this.id;
                var tTaxID = cliTID.split('_')[1];
                checkedTaxes.push(tTaxID);
            }
        });
        
        var qty = thisRow.find('#itemQuantity').val();
        var unitPrice = thisRow.find('#itemUnitPrice').val();
        var uP = thisRow.find('.uomPrice').html();

        var discount = thisRow.find('#piDiscount').val();
        var discountType;
        if (locationProducts[productID]) {
            if (locationProducts[productID].pPDP != null) {
                discountType = "Per";
            } else if (locationProducts[productID].pPDV != null) {
                discountType = "Val";
            } else {
               discountType = "Non";
            }
            if (discountType == "Val") {
                var newDiscount = validateDiscount(productID,discountType,discount, unitPrice);
                var tmpItemCost = calculateTaxFreeItemCostByValueDiscount(unitPrice, qty, newDiscount);
                if (tmpItemCost < 0) {
                    tmpItemCost = 0;
                }
            } else if (discountType == "Per") {
                var newDiscount = validateDiscount(productID,discountType,discount, unitPrice);
                var tmpItemCost = calculateTaxFreeItemCost(unitPrice, qty, newDiscount);
            } else {
                newDiscount = 0;
                var tmpItemCost = calculateTaxFreeItemCost(unitPrice, qty, newDiscount);
            }

            if ($('#total_discount_rate').val() != '' && $('input[name=discount_type]:checked').val() == 'presentage') {
                var invoiceDiscountValue = $('#total_discount_rate').val();
                tmpItemCost -= (tmpItemCost * (toFloat(invoiceDiscountValue) / toFloat(100)));
            }

            $("#piDiscount", thisRow).val(newDiscount);
            currentItemTaxResults = calculateItemCustomTax(tmpItemCost, checkedTaxes);
            currentTaxAmount = (currentItemTaxResults == null) ? null : currentItemTaxResults.tTA;
            var itemCost = toFloat(tmpItemCost) + toFloat(currentTaxAmount);
            $('#addNewTotal', thisRow).html(accounting.formatMoney(itemCost));

            // objIndex = products.findIndex((obj => obj.pID == productID));
            // products[objIndex].pTotal = itemCost;

            if (productID != undefined) {
                var currentEl = new Array();
                currentEl[productID+'_'+unitPrice] = itemCost;
                productLineTotal = $.extend(productLineTotal, currentEl);
            }
        } else {
            $("#piDiscount", thisRow).html(discount);
            var tmpItemCost = calculateTaxFreeItemCost(unitPrice, qty, discount);
            currentItemTaxResults = calculateItemCustomTax(tmpItemCost, checkedTaxes);
            currentTaxAmount = (currentItemTaxResults == null) ? null : currentItemTaxResults.tTA;
            var itemCost = toFloat(tmpItemCost) + toFloat(currentTaxAmount);
            $('#addNewTotal', thisRow).html(accounting.formatMoney(itemCost));
            // objIndex = products.findIndex((obj => obj.pID == productID));
            // products[objIndex].pTotal = itemCost;

            if (productID != undefined) {
                var currentEl = new Array();
                currentEl[productID+'_'+unitPrice] = itemCost;
                productLineTotal = $.extend(productLineTotal, currentEl);
            }
        }
    }


    function setItemViseTotal() {
        invoiceTotalDiscountTrigger = true;
        $('#add-new-item-row tr').each(function() {
            var thisRow = $(this);
            var productID = thisRow.find('#itemCode').val();
            if (typeof productID == "undefined" || productID == "") {
                //do nothing
            } else {
                calculateItemTotalForAddNewItemRow(thisRow, productID);
                setTotalTax();
                setPiTotalCost();
            }
            // } 
        });

        $('#form_row tr').each(function(){
            if($(this).attr('id') !== "preSetSample"){
                var thisRow = $(this);
                var rowId = $(this).attr('id');
                var piProductID = rowId.split("_")[1];
                objIndex = products.findIndex((obj => obj.locationPID == piProductID));
                if (objIndex != -1) {
                    var productID = products[objIndex].pID;
                    if (typeof productID == "undefined" || productID == "") {
                                //do nothing
                    } else {
                        calculateItemTotal(thisRow, productID);
                        setTotalTax();
                        setPiTotalCost();
                    }
                }
            }
        });

        deliveryCEnable = false;
        invoiceTotalDiscountTrigger = false;
    }

    $('#total_discount_rate').on('keyup', function() {
        if ($('#disc_value').is(":checked")) {
            if ((!$.isNumeric($(this).parents('tr').find('#total_discount_rate').val()) || $(this).parents('tr').find('#total_discount_rate').val() < 0)) {
                decimalPoints = 0;
                $(this).parents('tr').find('#total_discount_rate').val('');
            }
        } else {
            if ((!$.isNumeric($(this).parents('tr').find('#total_discount_rate').val()) || $(this).parents('tr').find('#total_discount_rate').val() < 0 || $(this).parents('tr').find('#total_discount_rate').val() > 100)) {
                decimalPoints = 0;
                $(this).parents('tr').find('#total_discount_rate').val('');
            }
        }
    
        setItemViseTotal();
    
    });

    $('#disc_presentage,#disc_value').on('change', function() {
        setItemViseTotal();
    });

});

function clearAddBatchProductRow() {
    $('#newBatchNumber').val('');
    $('#newBatchQty').val('');
    $('#newBatchMDate').val('');
    $('#newBatchEDate').val('');
    $('#newBatchWarrenty').val('');
}
function clearAddNewRow($parentRow) {
    $('#unitPrice',$parentRow).val('').siblings('.uomPrice,.uom-price-select').remove();
    $("#unitPrice",$parentRow).show();
    $("#qty",$parentRow).val('').siblings('.uomqty,.uom-select').remove();
    $("#qty",$parentRow).show();
    $('#piDiscount',$parentRow).val('').siblings('.uomPrice,.uom-price-select').remove();
    $('#piDiscount',$parentRow).show();
    $('#addNewTotal',$parentRow).html('0.00');
    $('#taxApplied',$parentRow).removeClass('glyphicon glyphicon-checked');
    $('#taxApplied',$parentRow).addClass('glyphicon glyphicon-unchecked');
    $('.tempLi',$parentRow).remove();
    $('.uomLi',$parentRow).remove();
    $('#uomAb',$parentRow).html('');
    selectedProduct = '';
    selectedProductQuantity = '';
    selectedProductUom = '';
}

function setAddedSupplierToTheList(addedSupplier) {
    if (!jQuery.isEmptyObject(addedSupplier)) {
        var newSupplier = addedSupplier.supplierCode + '-' + addedSupplier.supplierName;
        $('#supplier').append($("<option value='" + addedSupplier.supplierID + "'>" + newSupplier + "</option>"));
        $('#supplier').selectpicker('refresh');
        $('#supplier').val(addedSupplier.supplierID);
        $('#supplier').selectpicker('render');
        selectedSupplier = addedSupplier.supplierID;
        $('#addSupplierModal').modal('hide');
        $('#supplierCurrentBalance').val(0.00);
        $('#supplierCurrentCredit').val(0.00);
    }
}

function setTaxListForProduct(productID, $parentRow) {
    if ((!jQuery.isEmptyObject(locationProducts[productID].tax))) {
        productTax = locationProducts[productID].tax;
        $('#taxApplied',$parentRow).removeClass('glyphicon glyphicon-unchecked');
        $('#taxApplied',$parentRow).addClass('glyphicon glyphicon-check');
        $('.tempLi',$parentRow).remove();
        for (var i in productTax) {
            var clonedLi = $($('#sampleLi',$parentRow).clone()).removeClass('hidden').attr('id', 'li_' + i).addClass("tempLi");
            clonedLi.children(".taxChecks").attr('id', productID + '_' + i).prop('checked', true).addClass('addNewTaxCheck');
            if (productTax[i].tS == 0) {
                clonedLi.children(".taxChecks").attr('disabled', 'disabled');
                clonedLi.children(".taxName").addClass('crossText');
            }
            clonedLi.children(".taxName").html('&nbsp&nbsp' + productTax[i].tN + '&nbsp&nbsp' + productTax[i].tP + '%').attr('for', productID + '_' + i);
            clonedLi.insertBefore($('#sampleLi',$parentRow));

        }
    } else {
        $('#taxApplied',$parentRow).addClass('glyphicon glyphicon-unchecked');
        $('#addNewTax',$parentRow).attr('disabled', 'disabled');
    }

}

function validatePiJournalEntry()
{
    var status = true;

    $('#journal-entry-body > tr').each(function(){
        if($(this).hasClass('add_acc')){
            var accountId = $(this).find('.accountID').val();
            if (accountId == 0) {
                status = false;
            }
        }
    });
    return status;
}

function validateDiscount(productID, discountType, currentDiscount, unitPrice) {
    var defaultProductData = locationProducts[productID];
    var newDiscount = 0;
    if (defaultProductData.dPEL == 1) {
        if (discountType == 'Val') {
            if (toFloat(defaultProductData.pPDV) !=0 && toFloat(currentDiscount) > toFloat(defaultProductData.pPDV)) {
                    newDiscount = toFloat(defaultProductData.pPDV);
                    p_notification(false, eb.getMessage('ERR_INVO_DISCOUNT_VALUE'));
            } else if (toFloat(defaultProductData.pPDV) == 0) {
                if (toFloat(currentDiscount) > toFloat(unitPrice)) {
                    newDiscount = toFloat(unitPrice);
                } else {
                    newDiscount = toFloat(currentDiscount);    
                }
            } else {
                newDiscount = toFloat(currentDiscount);
            }

        } else {
            if (toFloat(defaultProductData.pPDP) != 0 && toFloat(currentDiscount) > toFloat(defaultProductData.pPDP)) {
                    newDiscount = toFloat(defaultProductData.pPDP);
                    p_notification(false, eb.getMessage('ERR_INVO_DISC_PERCENTAGE'));
            } else if (toFloat(defaultProductData.pPDP) == 0) {
                if (toFloat(currentDiscount) > 100 ) {
                    newDiscount = 100;
                    p_notification(false, eb.getMessage('ERR_PI_DISC_PERCENTAGE'));   
                } else {
                    newDiscount = toFloat(currentDiscount);    
                }
            } else {
                newDiscount = toFloat(currentDiscount);
            }
        }
    } 
    return newDiscount;
}

function clearInvoiceForm() {
    resetInvoicePage();
    $('#supplier').find('option').remove();
    $('#supplier').removeData('AjaxBootstrapSelect');
    $('#supplier').removeData('selectpicker');
    $('#supplier').siblings('.bootstrap-select').remove();
    $('#supplier').siblings('.bootstrap-select').find('.dropdown-menu').remove();
    $('#supplier').append('<option value="">Select Supplier</option>');
    $('#supplier').attr('disabled',false).val('');
    $('#supplier').selectpicker('refresh');
    loadDropDownFromDatabase('/supplierAPI/search-suppliers-for-dropdown', "", 0, '#supplier');
    $('#deliveryDate').val('');
    $('#supplierReference').val('');
    $('#paymentTerm').val(1);
    $('#supplierCurrentBalance').val('');
    $('#supplierCurrentCredit').val('');
}

function resetInvoicePage() {
    //    clearProductScreen();
    batchProducts = {};
    serialProducts = {};
    totalTaxList = {};
    grnProducts = {};
    products = {};
    // $('#add-new-item-row').find('tr').each(function() {
    //     if (!$(this).hasClass('add-row')) {
    //         $(this).remove();
    //     }
    // });
    // var $currentRow = getAddRow();
    // $currentRow.removeClass('hidden');
    $('#subtotal').val('');
    $('#finaltotal').val('');
    $('#totalTaxShow').html('');
}

