
var selectedAdjustmentID;
var ignoreBudgetLimitFlag = false;
$(document).ready(function() {
    loadDropDownFromDatabase('/api/inventory-adjustment/search-adjstment-for-dropdown', "", 0, '#searchAdjustment');
    $('#searchAdjustment').trigger('change');
    $('#searchAdjustment').on('change', function() {
        if ($(this).val() > 0 && selectedAdjustmentID != $(this).val()) {
            selectedAdjustmentID = $(this).val();
            var param = {adjustmentID: selectedAdjustmentID}
            getViewAndLoad('/api/inventory-adjustment/get-adjustment-by-adjustmentID', 'adjustmentList', param)
        }
    });
    $('#adjustmentSearchClear').on('click', function() {
        getViewAndLoad('/api/inventory-adjustment/get-adjustment-by-adjustmentID/' + getCurrPage(), 'adjustmentList', {});
        $('#searchAdjustment').val('').trigger('change');
        selectedAdjustmentID = null;
    });

    $('#adjustmentList').on('click', '.doc_attachments', function() {
        setDataToAttachmentViewModal($(this).attr('data-adjustment-related-id'));
        $('#viewAttachmentModal').modal('show');
    });

});

function setDataToAttachmentViewModal(documentID) {
    $('#doc-attach-table tbody tr').remove();
    $('#doc-attach-table tfoot div').remove();
    $('#doc-attach-table tbody div').remove();

    eb.ajax({
        type: 'POST',
        url: BASE_URL + '/get-document-related-attachement',
        data: {
            documentID: documentID,
            documentTypeID: 16
        },
        success: function(respond) {
            if (respond.status == true) {
                $('#doc-attach-table thead tr').removeClass('hidden');
                $.each(respond.data, function(index, value) {
                    tableBody = "<tr><td><a href="+value.link+">"+value.docName+"</a></td></tr>";
                    $('#doc-attach-table tbody').append(tableBody);
                });
            } else {
                $('#doc-attach-table thead tr').addClass('hidden');
                var noDataFooter = "<div class= ' col-lg-6 text-right'><label><b>No Data</b></label>";
                $('#doc-attach-table tbody').append(noDataFooter);
            }
        }
    });
}

function editAdjustment(goodIssueID) {
    eb.ajax({
        type: 'POST',
        url: BASE_URL + '/api/inventory-adjustment/get-goods-issue-details',
        data: {giID: goodIssueID},
        success: function(respond) {
            var editData = respond.data;
            if (editData != null) {
                $('#adjusmentID').val(goodIssueID);
                $('#ref_no').val(editData.goodsIssueCode);
                $('#reason').val(editData.goodsIssueReason);
                $('#load_edit_adjustment_div').modal('show');
            }
        }
    });

    $('#ok-adjus-edit-button').on('click', function() {
        var goodIssueID = $('#adjusmentID').val();
        var reason = $('#reason').val();
        eb.ajax({
            type: 'POST',
            url: BASE_URL + '/api/inventory-adjustment/update-goods-issue-reason',
            data: {goodIssueID: goodIssueID, reason: reason},
            success: function(respond) {
                p_notification(respond.status, respond.msg);
                $('#load_edit_adjustment_div').modal('hide');
                window.location.reload();
            },
            async: false
        });
    });
}

function deleteAdjustment(goodIssueID) {
    bootbox.confirm("Are you sure you want to delete this Adjustment ?", function(result) {
        if (result == true) {
            eb.ajax({
                type: 'POST',
                url: BASE_URL + '/api/inventory-adjustment/update-goods-issue-by-id',
                data: {goodIssueID: goodIssueID, ignoreBudgetLimit: ignoreBudgetLimitFlag},
                success: function(respond) {
                    if (respond.status == true) {
                        p_notification(respond.status, respond.msg);
                        $('#adjustmentList').html(respond.html);
                    } else {
                        if (respond.data == "NotifyBudgetLimit") {
                            bootbox.confirm(respond.msg+' ,Are you sure you want to cancel ?', function(result) {
                                if (result == true) {
                                    ignoreBudgetLimitFlag = true;
                                    deleteAdjustment(goodIssueID);
                                } else {
                                    setTimeout(function(){ 
                                        location.reload();
                                    }, 3000);
                                }
                            });
                        } if (respond.data == "AlReadyDeleted") {
                            p_notification(respond.status, respond.msg);
                            $("#adjustmentList").html(respond.html);
                        } else {
                            p_notification(respond.status, respond.msg);
                        }
                    }
                },
                async: false
            });
        }
    });
}
