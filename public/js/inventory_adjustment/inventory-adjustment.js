/**
 * @author Sandun<sandun@thinkcube.com>
 * This file contains Inventory Adjustment related js functiond
 */

var LOCATION = new Array();
var selectedProductUom;
var rowCount = 0;
var selectedProductUomAbbr;
var locationID;
var locationProducts = [];
var batchCount = 0;
var adjustmentProductQty;
var uomQty;
var stockQty;
var selectedProductID;
var adjustmentType;
var dimensionData = {};
var ignoreBudgetLimitFlag = false;
$(document).ready(function() {
    $('#adjustmentUpload').hide();
    $('#locationName').focus();
    var $productTable = $("#productTable");
    var proId;
    var oldCode;
    var locationProductID;
    var serialData;
    var batchData;
    var serialAndBatchData;
    var proOnlyProductData;
    var bSBatchData;
    var bSSerialData;
    var batchTextData;
    var locationProductID;
    var productNameList = {};
    var productCodeList = {};
    var uomList = {};
    var items = {};
    var uomMapArray = {};
    var batchProducts = {};
    var serialProducts = {};
    var bProducts = {};
    var sProducts = {};
    var productHeader = false;
    var dataType;
    var productDelimiter;
    var batchSerialType;
    var dimensionArray = {};
    var dimensionTypeID = null;

    var $loadProductTypeModal = $('#addNegativeAdjustmentProductsModal');
    var $addPositiveAdjustmentProductsModal = $('#addPositiveAdjustmentProductsModal');
    $("input[name='qty']", $(this).parents('td')).data('conversion', 1);

///////DatePicker\\\\\\\
    var nowTemp = new Date();
    var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);
    var checkin = $('#deliveryDate').datepicker().on('changeDate', function(ev) {
        checkin.hide();
        checkin.setValue(now);
    }).data('datepicker');
    var batchMD = $('#newBatchMDate').datepicker({
        onRender: function(date) {
            return date.valueOf() > now.valueOf() ? ((U_DATE_R_OVERRIDE) ? '' : 'disabled') : '';
        }
    }).on('changeDate', function(ev) {
        batchMD.hide();
    }).data('datepicker');

    var batchED = $('#newBatchEDate').datepicker({
        onRender: function(date) {
            return date.valueOf() < now.valueOf() ? ((U_DATE_R_OVERRIDE) ? '' : 'disabled') : '';
        }
    }).on('changeDate', function(ev) {
        batchED.hide();
    }).data('datepicker');

    var sED = $('.serialEDate').datepicker({
        onRender: function(date) {
            return date.valueOf() < now.valueOf() ? ((U_DATE_R_OVERRIDE) ? '' : 'disabled') : '';
        }
    }).on('changeDate', function(ev) {
        sED.hide();
    }).data('datepicker');

/////EndOFDatePicker\\\\\\\\\
    var checkin = $('#adjusmentDate').datepicker({onRender: function(date) {
            return date.valueOf() > now.valueOf() ? ((U_DATE_R_OVERRIDE) ? '' : 'disabled') : '';
        },
        //format: 'yyyy-mm-dd'
    }).on('changeDate', function(ev) {
        checkin.hide();
    }).data('datepicker');

    setDataInitialLoading();

    $('#locationName').on('change', function() {
        var key = $('#locationName').val();
        locationID = key;
        setDataForLocation(key);
        getReferenceNo(key);
    });

    $('#locationName').on('focusout', function() {
        if ($(this).val() == '') {
            locationID = '';
            clearProductScreen();
            $('#div_overlay').addClass('overlay');
        }
    });

    function getReferenceNo(locationID) {
        eb.ajax({
            type: 'POST',
            url: BASE_URL + '/api/inventory-adjustment/get-adjustment-reference-no',
            data: {locationID: locationID},
            success: function(respond) {
                if (respond.status == false) {
                    p_notification(false, respond.msg);
                    $('#refNo').val('');
                    $('#refNotSet').modal('show');

                }
                else if (respond.status == true) {
                    $('#refNo').val(respond.data.refNo);
                    $('#locRefID').val(respond.data.locRefID);
                }
            },
            async: false
        });
    }

    function setDataForLocation(locationID) {
        clearProductScreen();
        var documentType = 'inventoryAdjusment';
        loadDropDownFromDatabase('/productAPI/search-location-products-for-dropdown', locationID, 0, '#itemCode', '', '', documentType);
        $('#item_code').trigger('change');
        $('#itemCode').children().not('.itemCodeDefaultSelect').remove();
        $('#itemCode').selectpicker('refresh');
        $('#div_overlay').removeClass('overlay');
    }

    function getProductsFromLocation(locationID) {
        $.ajax({
            type: 'POST',
            url: BASE_URL + '/api/inventory-adjustment/get-product-code-by-location-code',
            data: {locationID: locationID},
            success: function(respond) {
                productCodeList = respond.pcl;
                productNameList = respond.pl;
                uomList = respond.uoml;
                $('#itemCode').children().not('.itemCodeDefaultSelect').remove();
                $.each(productCodeList, function(index, value) {
                    $('#itemCode').append($("<option value='" + value + "'>" + value + ' - ' + productNameList[index] + "</option>"));
                });

                $('#itemCode').selectpicker('refresh');
                $('#div_overlay').removeClass('overlay');
            },
            async: false
        });
    }

    $('#dimensionView').on('click', function(e) {
        clearDimensionModal();
        var refNo = $('#refNo').val();

        if (!$.isEmptyObject(dimensionData)) {
            $.each(dimensionData[refNo], function(index, val) {
                dimensionArray[index] = val;                
            });
            $.each(dimensionArray, function(index, value) {
                var newTrID = 'tr_' + value.dimensionTypeId;
                var clonedRow = $($('#dimensionPreSample').clone()).attr('id', newTrID).addClass('addedDimensions');
                $("input[name='dimensionType']", clonedRow).val(value.dimensionTypeTxt);
                $("input[name='dimensionValue']", clonedRow).val(value.dimensionValueTxt);
                clonedRow.removeClass('hidden');
                clonedRow.insertBefore('#dimensionPreSample');
                clearAddedNewDimensionRow();
            });
        }
        $('#addDimensionModal').modal('show');
    });

    // dimension modal
    function clearDimensionModal()
    {
        $("#addDimensionModal #dimensionTable tbody .addedDimensions").remove();
        dimensionArray = {};
    }


    $('.dimenisonchange').on('change', function(e) {
        dimensionTypeID = this.value;
        getDimensionValueListByDimensionType(dimensionTypeID, function() {
        });
    });



    function getDimensionValueListByDimensionType(dimensionTypeID, callback) {
        switch (dimensionTypeID) {
            case "job":
                $('.dimensionVal').addClass('hidden');
                $('.dimensionProjectNo').addClass('hidden');
                $('.dimensionJobNo').removeClass('hidden');
                break;
            case "project":
                $('.dimensionVal').addClass('hidden');
                $('.dimensionProjectNo').removeClass('hidden');
                $('.dimensionJobNo').addClass('hidden');
                break;
           default:
                $('.dimensionVal').removeClass('hidden');
                $('.dimensionProjectNo').addClass('hidden');
                $('.dimensionJobNo').addClass('hidden');
                $.ajax({
                    type: 'POST',
                    url: BASE_URL + '/dimension-api/getRelatedDimensionValues',
                    data: {dimensionId: dimensionTypeID},
                    success: function(respond) {
                        //empty options
                        $('#dimensionVal').empty();
                        $('#dimensionVal').append($("<option>", {value: '', html: 'Select dimension value'}));
                        $(respond.data).each(function(index, dimValue) {
                            $.each(dimValue, function(ind, val) {
                                $('#dimensionVal').append($("<option>", {value: ind, html: val.value}));
                            });
                        });
                        callback();
                    }
                });
                break;
        }
    }

    $('#add_dimension').on('click', function(e) {
        e.preventDefault();
        //if plus button is clicked then disabled the button
        //if button is clicked more than one, then it does not continue
        if ($(this).is(':disabled')) {
            return false;
        } else {
            $(this).attr('disabled', true);
            var $thisRow = $(this).parents('tr');
            if ($('.dimenisonchange').val() == 0 || $('.dimenisonchange').val() == '' || $('.dimenisonchange').val() == null) {
                p_notification(false, eb.getMessage('ERR_DIMENSION_SELCET'));
                $(this).attr('disabled', false);
            } else {

                if ($('.dimenisonchange').val() != 'job' && $('.dimenisonchange').val() != 'project') {
                    if ($('.dimensionVal').val() == 0 || $('.dimensionVal').val() == '' || $('.dimensionVal').val() == null) {
                        p_notification(false, eb.getMessage('ERR_DIMENSION_VALUE_SELCET'));
                        $(this).attr('disabled', false);
                        return;
                    }
                }

                if ($('.dimenisonchange').val() == 'project') {
                    if ($('.dimensionProjectNo').val() == 0 || $('.dimensionProjectNo').val() == '' || $('.dimensionProjectNo').val() == null) {
                        p_notification(false, eb.getMessage('ERR_DIMENSION_VALUE_SELCET'));
                        $(this).attr('disabled', false);
                        return;
                    }
                }

                if ($('.dimenisonchange').val() == 'job') {
                    if ($('.dimensionJobNo').val() == 0 || $('.dimensionJobNo').val() == '' || $('.dimensionJobNo').val() == null) {
                        p_notification(false, eb.getMessage('ERR_DIMENSION_VALUE_SELCET'));
                        $(this).attr('disabled', false);
                        return;
                    }
                }
                
                if (dimensionArray[dimensionTypeID]) {
                    p_notification(false, eb.getMessage('ERR_DIMENSION_ALREADY_INSERTED'));
                    $('.dimenisonchange').val('');
                    $('.dimenisonchange').selectpicker('render');
                    $('.dimenisonchange').trigger('change');
                } else {
                    addNewDimensionRow($(this).parents('tr'));
                }
                $(this).attr('disabled', false);
            }
        }
    });

    function addNewDimensionRow(thisRow) {

        var dimensionTypeId = $('.dimenisonchange', thisRow).val();
        var dimensionTypeTxt = $('.dimenisonchange', thisRow).find("option:selected").text();

        switch (dimensionTypeID) {
            case "job":
                var dimensionValueTxt = $('.dimensionJobNo', thisRow).find("option:selected").text();
                var dimensionValueId = $('.dimensionJobNo', thisRow).val();
                break;
            case "project":
                var dimensionValueTxt = $('.dimensionProjectNo', thisRow).find("option:selected").text();
                var dimensionValueId = $('.dimensionProjectNo', thisRow).val();
                break;
           default:
                var dimensionValueTxt = $('.dimensionVal', thisRow).find("option:selected").text();
                var dimensionValueId = $('.dimensionVal', thisRow).val();
                break;
        }
       
        var dimensionDuplicateCheck = false;
        if (!$.isEmptyObject(dimensionArray)) {
            $.each(dimensionArray, function(index, value) {
                if (value.dimensionTypeId == dimensionTypeId) {
                    dimensionDuplicateCheck = true;
                }
            });   
        }
       
        if (dimensionDuplicateCheck) {
            p_notification(false, eb.getMessage("ERR_DIMENSION_ALREADY_INSERTED"));
            return false;
        }

        var newTrID = 'tr_' + dimensionTypeId;
        var clonedRow = $($('#dimensionPreSample').clone()).attr('id', newTrID).addClass('addedDimensions');
        $("input[name='dimensionType']", clonedRow).val(dimensionTypeTxt);
        $("input[name='dimensionValue']", clonedRow).val(dimensionValueTxt);
        clonedRow.removeClass('hidden');
        clonedRow.insertBefore('#dimensionPreSample');
        dimensionArray[dimensionTypeId] = new dimension(dimensionTypeId, dimensionValueId,dimensionTypeTxt , dimensionValueTxt);
        clearAddedNewDimensionRow();
    }


    function dimension(dimensionTypeId, dimensionValueId, dimensionTypeTxt, dimensionValueTxt) {
        this.dimensionTypeId = dimensionTypeId;
        this.dimensionValueId = dimensionValueId;
        this.dimensionTypeTxt = dimensionTypeTxt;
        this.dimensionValueTxt = dimensionValueTxt;
    }

    function clearAddedNewDimensionRow() {
        $('.dimenisonchange').val('');
        $('.dimenisonchange').selectpicker('render');
        $('.dimenisonchange').trigger('change');
    }

    $('.addDimensions').on('click', function(e) {
        e.preventDefault();

        // validate products before closing modal
        if (!dimensionModalValidate(e)) {
            e.stopPropagation();
            return false;
        } else {
            $('#addDimensionModal').modal('hide');
        }
    });

    function dimensionModalValidate(e) {
        var refNo = $('#refNo').val();
        if ($.isEmptyObject(dimensionArray)) {
            dimensionData = {};
            return true;   
        } else {
            var dimensionSingleData = {};
            $.each(dimensionArray, function(index, val) {
                dimensionSingleData[index] = val;
            });
            dimensionData[refNo] = dimensionSingleData;
            return true;
        }
    }

    $("#addDimensionModal #dimensionTable").on('click', '.deleteDimensionType', function(e) {
        e.preventDefault();
        var deleteDTrID = $(this).closest('tr').attr('id');
        var deleteDID = deleteDTrID.split('tr_')[1].trim();
        bootbox.confirm('Are you sure you want to remove this dimension ?', function(result) {
            if (result == true) {
                delete dimensionArray[deleteDID];
                $('#' + deleteDTrID).remove();
            }
        });
    });

    loadDropDownFromDatabase('/job-api/searchJobsForDropdown', "", 0, '.dimensionJobNo');
    $('.dimensionJobNo').selectpicker('refresh');
    $('.dimensionJobNo').on('change', function() {
        var dimensionValue = $(this).val();
    });

    loadDropDownFromDatabase('/api/project/searchProjectsForDropdown', "", 0, '.dimensionProjectNo');
    $('.dimensionProjectNo').selectpicker('refresh');
    $('.dimensionProjectNo').on('change', function() {
        var dimensionValue = $(this).val();
    });

    $('#itemCode').on('change', function(e) {
        var $thisRow = $(this).parents('tr');

        if ((typeof items[$(this).val()] != 'undefined' && $(this).val() != '') && items[$(this).val()].p_id != '') {

            p_notification(false, eb.getMessage("ERR_QUOT_PR_ALREADY_ADD"));

            $('.uomLi').remove();
            $('#uomAb').html('');
            $('#itemCode').val('');
            clearItemCode();
            $('#qty').val('');
            $('#unitPrice').val('');
            selectedProductUom = '';
            $('#addNewItemRow .uomqty').val('');
            $('#addNewItemRow .uomPrice').val('');
            $('#addNewItemRow .uom-select').remove();
            $('#addNewItemRow .uom-price-select').remove();
            $("#addNewItemRow .available-qty .uomqty").remove();
            $("#addNewItemRow .unit-price .uomPrice").remove();
            $("#addNewItemRow .available-qty #availableQuantity").css('display', 'none');
            $("#addNewItemRow .available-qty #availableQuantity").val('');
            $("#addNewItemRow .available-qty #availableQuantity").show();
            $("#addNewItemRow .available-qty #availableQuantity").removeAttr("readonly", "readonly");
        } else if ($(this).val() > 0 && selectedProductID != $('#itemCode').val()) {

            $('.uomLi').remove();
            selectedProductID = $(this).val();
            eb.ajax({
                type: 'POST',
                url: BASE_URL + '/productAPI/get-location-product-details',
                data: {productID: selectedProductID, locationID: locationID},
                success: function(respond) {
                    var currentElem = new Array();
                    currentElem[selectedProductID] = respond.data;
                    locationProducts = $.extend(locationProducts, currentElem);

                    var itemCode = $('#itemCode').val();
                    var data = new Array();
                    var dataPostiveAdded = new Array();
                    $("#qty").val('').siblings('.uomqty,.uom-select').remove();
                    $("#qty").parent().addClass('input-group');
                    $("#qty").show();

                    $thisRow.find("#unitPrice").val('').siblings('.uomPrice,.uom-price-select').remove();
                    $thisRow.find("#unitPrice").parent().addClass('input-group');
                    $thisRow.find("#unitPrice").show();

                    $thisRow.find("input[name='availableQuantity']").val('').siblings('.uomqty,.uom-select').remove();
                    $thisRow.find("input[name='availableQuantity']").parent().addClass('input-group');
                    $thisRow.find("input[name='availableQuantity']").show();

                    $thisRow.find("input[name='unitPrice']").val('').siblings('.uomPrice,.uom-price-select').remove();
                    $thisRow.find("input[name='unitPrice']").parent().addClass('input-group');
                    $thisRow.find("input[name='unitPrice']").show();

                    data[0] = locationProducts[selectedProductID];
                    $('#qty').addUom(data[0].uom);
                    $("input[name='availableQuantity']").val(data[0].LPQ).prop('readonly', true).addUom(data[0].uom);
                    $('.uomqty').attr("id", "itemQuantity");
                    $('#qty').parent().addClass('input-group');
                    $("input[name='availableQuantity']").parent().addClass('input-group');
                    if (data[0] === 'error' && dataPostiveAdded[0] === 'error') {
                        $('.uomqty').val('');
                        $('#unitPrice').val('');
                    }
                    else {
                        proId = data[0].pID;
                        locationProductID = data[0].lPID;
                        $('#itemCode').data('PT', data[0].pT);
                        $('#itemCode').data('PN', data[0].pN);
                        var dPP = parseFloat(data[0].dPP);
                        $thisRow.find("input[name='unitPrice']").val(parseFloat(dPP).toFixed(2)).addUomPrice(data[0].uom);
                        $('#unitPrice').parent().addClass('input-group');
                        $("input[name='unitPrice']").parent().addClass('input-group');
                        if ($('#unitPrice').val() != '') {
                            if ($('.uomqty').val() === '') {
                                $('.uomqty').val('0');
                                $('.uomqty').focus();
                            }
                        }
                        setItemCost();
                    }
                    $('.uomqty').focus();
                }
            });
        }
    });

    $('#batch_serial_data_table,#batch_data_table').on('change', "input[type='text']", function() {
        var UomAbbr = $(this).parent().find('.selected').text();
        var uomAbbrData = Array();
        uomAbbrData[0] = UOMLISTBYABBR[UomAbbr] ? UOMLISTBYABBR[UomAbbr] : 1;
        var uomID = uomAbbrData[0].uomID;

        var proConersionList = Array();
        proConersionList[0] = PRODUCTUOMLIST[proId + uomID] ? PRODUCTUOMLIST[proId + uomID] : 1;

        var proConersion = proConersionList[0].productUomConversion ? proConersionList[0].productUomConversion : 1;

        var enteredValue = $("input[name='batch_qty']", $(this).parents('td')).val();
        $("input[name='batch_qty']", $(this).parents('td')).data('basevalue', enteredValue * proConersion);
        $("input[name='batch_qty']", $(this).parents('td')).data('conversion', proConersion);

    });

    $('#adjustmentType').on('change', function() {
        clearProductScreenForAdjustment();
        adjustmentType = $(this).val();
        if ($(this).val() == 2) {
            $("#adjustmentProductTable .available-qty").addClass("hidden");
            $("#addNewItemRow .available-qty").addClass("hidden");
            $('#adjustmentUpload').show();
        } else {
            $("#adjustmentProductTable .available-qty").removeClass("hidden");
            $("#addNewItemRow .available-qty").removeClass("hidden");
            $('#adjustmentUpload').show();
        }
    });

    $('#add_item').on('click', function(e) {
        e.preventDefault();
        //if plus button is clicked then disabled the button
        //if button is clicked more than one, then it does not continue
        if ($(this).is(':disabled')) {
            return false;
        } else {
            $(this).attr('disabled', true);
            var $thisRow = $(this).parents('tr');
            $("#qty").siblings('.uomqty').change();
            $("#unitPrice").siblings('.uomPrice').change();
            $('.tempy').remove();
            idNumber = 1;
            quon = 0;
            $('#prefix').val('');
            //        $('#serialAddTableAdj').addClass('hidden');
            var adjsDate = new Date(Date.parse($('#adjusmentDate').val()));
            var expD = $('#expire_date_autoFill').val('').on('changeDate', function(ev) {
                expD.hide();
            }).data('datepicker');
            $('#quontity').val('');
            $('#startNumber').val('');
            $('#batch_code').val('');
            var mfD = $('#mf_date_autoFill').val('').datepicker({
                onRender: function(date) {
                    return date.valueOf() > adjsDate.valueOf() ? ((U_DATE_R_OVERRIDE) ? '' : 'disabled') : '';
                }
            }).on('changeDate', function(ev) {
                mfD.hide();
            }).data('datepicker');
            $('#temp_1').find('input[name=submit]').attr('disabled', false);

            $('#temp_1').find('input[name=prefix]').attr('disabled', false);
            $('#temp_1').find('input[name=startNumber]').attr('disabled', false);
            $('#temp_1').find('input[name=quontity]').attr('disabled', false);
            $('#temp_1').find('input[name=expireDateAutoFill]').attr('disabled', false);
            $('#temp_1').find('input[name=batch_code]').attr('disabled', false);
            $('#temp_1').find('input[name=mfDateAutoFill]').attr('disabled', false);
            $('#temp_1').find('input[name=wrtyDays]').attr('disabled', false);
            if ($('#itemCode').val() == '' || $('#itemCode').val() == null) {
                $('#addNewItemRow #itemQuantity').val('');
                $('#addNewItemRow .uomPrice').val('');
                p_notification(false, eb.getMessage('ERR_INVENTADJUS_PRODCODE'));
                $(this).attr('disabled', false);
            } else {
                adjustmentProductQty = $('#qty').val();
                uomQty = $(".adding-qty .uomqty", $thisRow).val();
                var productType = $('#itemCode').data('PT');
                selectedProductUom = $("#qty").siblings('.uom-select').children('button').find('.selected').data('uomID');
                if (typeof (selectedProductUom) == 'undefined' || selectedProductUom == '') {
                    p_notification(false, eb.getMessage('ERR_GRN_SELECT_UOM'));
                    $(this).attr('disabled', true);
                } else {
                    if ($('#adjustmentType').val() == 1) {
                        if (accounting.unformat($('#unitPrice').val()) == '' || accounting.unformat($('#unitPrice').val()) <= 0) {
                            p_notification(false, eb.getMessage('ERR_INVENTADJUS_UNIPRI'));
                            $(this).attr('disabled', false);
                            $('#unitPrice').focus().select();
                        } else {
                            negativeAdjustment();
                        }
                    } else if ($('#adjustmentType').val() == 2) {
                        if ($('#adjusmentDate').val() == '') {
                            p_notification(false, eb.getMessage('ERR_INVENTADJUS_SELECDATE'));
                            $(this).attr('disabled', false);
                            $('#adjusmentDate').focus();
                        } else if (productType != 2 && ($('#qty').val() == '' || $('#qty').val() == 0)) {
                            p_notification(false, eb.getMessage('ERR_GRN_ENTER_QUANTITY'));
                            $(this).attr('disabled', false);
                            $('.uomqty').focus();
                        } else if (accounting.unformat($('#unitPrice').val()) == '' || parseFloat(accounting.unformat($('#unitPrice').val())) <= 0) {
                            p_notification(false, eb.getMessage('ERR_INVENTADJUS_UNIPRI'));
                            $(this).attr('disabled', false);
                            $('#unitPrice').focus().select();
                        } else {
                            var unitPrice = $('#unitPrice').val();
                            $('#unitPrice').val(parseFloat(unitPrice).toFixed(2));
                            positiveAdjustment(productType);
                        }
                        $(this).attr('disabled', false);
                    } else {
                        p_notification(false, eb.getMessage('ERR_INVENTADJUS_ADJTYPE'));
                        $(this).attr('disabled', false);
                    }
                }
            }
        }

        $('body').click(function (event) {
            if(!$(event.target).closest('#addNegativeAdjustmentProductsModal').length && !$(event.target).is('#addNegativeAdjustmentProductsModal')) {
                if ($('#adjustmentType').val() == 1) {
                    if (serialData != undefined || batchData != undefined) {
                        if (serialData.length > 0 && batchData.length === 0 && serialAndBatchData === 0) {
                            $('#serial_body').html('');
                        } else if (serialData.length === 0 && batchData.length > 0 && serialAndBatchData === 0) {
                            $('#batch_body').html('');
                        } else if (serialData.length === 0 && batchData.length === 0 && serialAndBatchData > 0) {
                            $('#batch_serial_body').html('');
                        }
                    }
                } 
            }     
            $("#add_item").attr('disabled', false);
        });
    });

    $('table#adjustmentProductTable>tbody').on('keypress', 'input#qty, input#unitPrice,.uomqty,.uomPrice', function(e) {

        if (e.which == 13) {
            $('#add_item', $(this).parents('tr')).trigger('click');
            e.preventDefault();
        }
    });

    $('#ok-product-button').on('click', function() {
        var flag;
        var emptyTextField = 0;
        var emptyCheckField = 0;
        var countTextField = 0;
        var countCheck = 0;
        var checkCount = 0;

        $("input[name='select_serial_code']").each(function() {
            countCheck++;
            if (this.checked) {
                checkCount++;
            } else {
                emptyCheckField++;
            }
            flag = true;
        });

        $("input[name='batch_qty']").each(function() {
            var $thisSubRow = $(this).parents('tr');
            var thisVals = getThisRowData($thisSubRow);

            var thisAdjustmentQuantity = $(this).val();
            countTextField++;
            if (thisAdjustmentQuantity.trim() == '' || thisAdjustmentQuantity == 0) {
                emptyTextField++;
            }
            if ((thisAdjustmentQuantity).trim() != "" && isNaN(parseFloat(thisAdjustmentQuantity))) {
                p_notification(false, eb.getMessage('ERR_INVENTADJUS_VALQUAN'));
                $(this).focus();
                return flag = false;
            }

            // check if entered qty is greater than available qty
            var thisAvailableQuantity = $("input[name='availableQuantity']", $thisSubRow).data('basevalue');
            thisAvailableQuantity = (isNaN((thisAvailableQuantity))) ? 0 : (thisAvailableQuantity);

            var thisAdjustmentQuantityByBase = thisAdjustmentQuantity;
            if (parseFloat(thisAdjustmentQuantityByBase) > parseFloat(thisAvailableQuantity)) {
                p_notification(false, eb.getMessage('ERR_INVENTADJUS_QUAN'));
                $(this).focus();
                return flag = false;
            }

            flag = true;
        });

        if (flag === true) {
            if (serialData.length > 0 && batchData.length === 0 && serialAndBatchData === 0) {
                if ((countCheck) === (emptyCheckField)) {
                    p_notification(false, eb.getMessage('ERR_INVENTADJUS_CHKBOX'));
                    return flag = false;
                } else if (countCheck > stockQty) {
                    p_notification(false, eb.getMessage('ERR_INVENTADJUS_QUANT_STOCK'));
                    return flag = false;
                } else {
                    addItemToRow();
                    $('#serial_body').html('');
                }
            } else if (serialData.length === 0 && batchData.length > 0 && serialAndBatchData === 0) {
                if ((countTextField) === (emptyTextField)) {
                    p_notification(false, eb.getMessage('ERR_INVENTADJUS_ENTERQUAN'));
                    return flag = false;
                } else {
                    addItemToRow();
                    $('#batch_body').html('');
                }
            } else if (serialData.length === 0 && batchData.length === 0 && serialAndBatchData > 0) {
                if ((countCheck + countTextField) === (emptyCheckField + emptyTextField)) {
                    p_notification(false, eb.getMessage('ERR_INVENTADJUS_DOADJUS'));
                    return flag = false;
                } else if (countCheck > stockQty) {
                    p_notification(false, eb.getMessage('ERR_INVENTADJUS_QUANT_STOCK'));
                    return flag = false;
                } else {
                    addItemToRow();
                    $('#batch_serial_body').html('');
                }
            }
            $loadProductTypeModal.modal('hide');
        }

    });

    $('#cancel-product-button,.close').on('click', function() {

        if ($('#adjustmentType').val() == 1) {
            if (serialData.length > 0 && batchData.length === 0 && serialAndBatchData === 0) {
                $('#serial_body').html('');
            } else if (serialData.length === 0 && batchData.length > 0 && serialAndBatchData === 0) {
                $('#batch_body').html('');
            } else if (serialData.length === 0 && batchData.length === 0 && serialAndBatchData > 0) {
                $('#batch_serial_body').html('');
            }
            $("#add_item").attr('disabled', false);
        }
    });
    
    $('form#inventoryAdjustForm').on('submit', function(e) {
        e.preventDefault();
        submitInventoryAdjustmentForm();
    });

    function submitInventoryAdjustmentForm()
    {
        var selectedid = "#" + document.activeElement.id;
        var tableId = $(selectedid).closest('table').attr("id");
        $("#saveInventoryAdjustment").attr('disabled', true);

        if ($('#locationName').val() == '') {
            p_notification(false, eb.getMessage('ERR_INVENTADJUS_LOCAT'));
            $('#locationName').focus();
            $("#saveInventoryAdjustment").attr('disabled', false);
        } else if (adjustmentType == 'empty') {
            p_notification(false, eb.getMessage('ERR_INVENTADJUS_SELECTTYPE'));
            $("#saveInventoryAdjustment").attr('disabled', false);
        } else if ($('#refNo').val() == '') {
            p_notification(false, eb.getMessage('ERR_INVENTADJUS_REFNO'));
            $('#refNo').focus();
            $("#saveInventoryAdjustment").attr('disabled', false);
        } else if ($('#reason').val() == '') {
            p_notification(false, eb.getMessage('ERR_INVENTADJUS_REASON'));
            $('#reason').focus();
            $("#saveInventoryAdjustment").attr('disabled', false);
        } else if ($("#adjusmentDate").val() == '') {
            p_notification(false, eb.getMessage('ERR_INVENTADJUS_ADJDATE'));
            $('#adjusmentDate').focus();
            $("#saveInventoryAdjustment").attr('disabled', false);
        } else if (_.size(items) === 0) {
            if (tableId != 'adjustmentProductTable') {
                p_notification(false, eb.getMessage('ERR_INVENTADJUS_ONEPROD'));
                $("#saveInventoryAdjustment").attr('disabled', false);
            }
        } else {
            var goods_issue_date = $('#adjusmentDate').val();
            var goods_issue_reason = $('#reason').val();
            var goods_issue_type_id = adjustmentType;
            var goods_issue_id = $('#refNo').val();
            var locRefID = $('#locRefID').val();
            var goods_issue_tbl = {
                goods_issue_date: goods_issue_date,
                goods_issue_reason: goods_issue_reason,
                goods_issue_type_id: goods_issue_type_id,
                goods_issue_id: goods_issue_id,
                locRefID: locRefID,
                goods_issue_comment: $('#cmnt').val(),
                ignoreBudgetLimit: ignoreBudgetLimitFlag
            };


            if (goods_issue_type_id == 1) {
                eb.ajax({
                    type: 'POST',
                    url: BASE_URL + '/api/inventory-adjustment/update-negative-adjustment',
                    data: {items: items, goods_issue_tbl_objt: goods_issue_tbl, locationID: locationID, dimensionData: dimensionData},
                    success: function(respond) {
                        if (respond.status == true) {
                            p_notification(respond.status, respond.msg);
                            var fileInput = document.getElementById('documentFiles');
                            if(fileInput.files.length > 0) {
                                var form_data = false;
                                if (window.FormData) {
                                    form_data = new FormData();
                                }
                                form_data.append("documentID", respond.data.adjusmentID);
                                form_data.append("documentTypeID", 16);
                                
                                for (var i = 0; i < fileInput.files.length; i++) {
                                    form_data.append("files[]", fileInput.files[i]);
                                }

                                eb.ajax({
                                    url: BASE_URL + '/store-files',
                                    type: 'POST',
                                    processData: false,
                                    contentType: false,
                                    data: form_data,
                                    success: function(res) {
                                        console.log(res);
                                    }
                                });
                            }
                            setTimeout(function(){ 
                                window.location.reload();
                            }, 3000);
                        } else {
                            if (respond.data == "NotifyBudgetLimit") {
                                bootbox.confirm(respond.msg+' ,Are you sure you want to save ?', function(result) {
                                    if (result == true) {
                                        ignoreBudgetLimitFlag = true;
                                        submitInventoryAdjustmentForm();
                                    } else {
                                        setTimeout(function(){ 
                                            location.reload();
                                        }, 3000);
                                    }
                                });
                            } else {
                                p_notification(respond.status, respond.msg);
                            }
                        }
                    }
                });
            } else if (goods_issue_type_id == 2) {
                eb.ajax({
                    type: 'POST',
                    url: BASE_URL + '/api/inventory-adjustment/update-positive-adjustment',
                    data: {items: items, goods_issue_tbl_data: goods_issue_tbl, locationID: locationID, dimensionData: dimensionData},
                    success: function(respond) {
                        if (respond.status == true) {
                            var fileInput = document.getElementById('documentFiles');
                            if(fileInput.files.length > 0) {
                                var form_data = false;
                                if (window.FormData) {
                                    form_data = new FormData();
                                }
                                form_data.append("documentID", respond.data.adjusmnetID);
                                form_data.append("documentTypeID", 16);
                                
                                for (var i = 0; i < fileInput.files.length; i++) {
                                    form_data.append("files[]", fileInput.files[i]);
                                }

                                eb.ajax({
                                    url: BASE_URL + '/store-files',
                                    type: 'POST',
                                    processData: false,
                                    contentType: false,
                                    data: form_data,
                                    success: function(res) {
                                        console.log(res);
                                    }
                                });
                            }

                            setTimeout(function(){ 
                                window.location.reload();
                            }, 3000);
                        } else {
                            if (respond.data == "NotifyBudgetLimit") {
                                bootbox.confirm(respond.msg+' ,Are you sure you want to save ?', function(result) {
                                    if (result == true) {
                                        ignoreBudgetLimitFlag = true;
                                        submitInventoryAdjustmentForm();
                                    } else {
                                        setTimeout(function(){ 
                                            location.reload();
                                        }, 3000);
                                    }
                                });
                            } else {
                                p_notification(respond.status, respond.msg);
                            }
                        }
                    }
                });
            } else {
                p_notification(false, eb.getMessage('ERR_INVENTADJUS_SELECTTYPE'));
            }
        }
    }


    $('#addPositiveAdjustmentProductsModal').on('click', '.batchDelete', function() {
        var deleteBID = $(this).closest('tr').attr('id');
        var deleteBQty = batchProducts[deleteBID].bQty;
        batchCount -= toFloat(deleteBQty);
        delete(batchProducts[deleteBID]);
        $('#' + deleteBID).remove();
        $('#proBatchAddNew').removeClass('hidden');
    });

    $('#addPositiveAdjustmentProductsModal').on('change', 'input.serialNumberList', function() {
        var matchSerialNumberCount = 0;
        var typedVal = $(this).val();
        var serialCheckStatus = false;
        eb.ajax({
            type: 'POST',
            url: BASE_URL + '/productAPI/checkSerialProductCodeExists',
            data: {serialCode: typedVal, productCode: locationProducts[proId].pC, },
            success: function(respond) {
                if (respond.data == true) {
                    serialCheckStatus = true;
                }
            },
            async: false
        });

        if (serialCheckStatus == true) {
            p_notification(false, eb.getMessage('ERR_GRN_SERIAL_CODE'));
            $(this).val('');
        } else {
            $('input.serialNumberList').each(function() {
                if ($(this).val() == typedVal) {
                    matchSerialNumberCount++;
                }
            });
            if (matchSerialNumberCount > 1) {
                p_notification(false, eb.getMessage('ERR_GRN_SAME_SERIAL_CODE'));
                $(this).val('');
            }
        }
    });

    $('#addPositiveAdjustmentProductsModal').on('change', 'input.serialBatchList,#newSerialBatch', function() {
        if (!batchProducts.hasOwnProperty($(this).val())) {
            p_notification(false, eb.getMessage('ERR_GRN_ENTER_BATCHNUM'));
            $(this).val('');
        } else {
            var enteredBatchCode = $(this).val();
            var sameBatchCodeCount = 0;
            $('input.serialBatchList,#newSerialBatch').each(function() {
                if ($(this).val() == enteredBatchCode) {
                    sameBatchCodeCount++;
                }
            });
            if (sameBatchCodeCount > batchProducts[enteredBatchCode].bQty) {
                p_notification(false, eb.getMessage('ERR_GRN_ENTER_PRODLIMIT'));
                $(this).val('');
            }
        }
    });

    $('#addPositiveAdjustmentProducts').on('click', function() {
        var serialEmpty = false;
        $('input.serialNumberList').each(function() {
            if ($(this).val() == '') {
                serialEmpty = true;
                return false;
            }
        });
        if (serialEmpty == true) {
            p_notification(false, eb.getMessage('ERR_GRN_FILL_SERIALNUM'));
            return false;
        }

        var batchEmpty = false;
        if (locationProducts[proId].bP == 1 && locationProducts[proId].sP == 0) {
            var selectedUomAbbr = $("#qty").siblings('.uom-select').children('button').find('.selected').data('uomID');
            var conversion = locationProducts[proId].uom[selectedUomAbbr].uC;
            var qty = parseFloat($('#qty').val()) / parseFloat(conversion);
            if (batchCount < qty) {
                batchEmpty = true;
            }
        }
        if (batchEmpty == true) {
            p_notification(false, eb.getMessage('ERR_GRN_ADD_BATCH_DETAILS'));
            return false;
        }
        var serialBatchEmpty = false;
        if ((locationProducts[proId].bP == 1) && (locationProducts[proId].sP == 1)) {
            $('input.serialBatchList').each(function() {
                if ($(this).val() == '') {
                    serialBatchEmpty = true;
                    return false;
                }
            });
        }
        if (serialBatchEmpty == true) {
            p_notification(false, eb.getMessage('ERR_INVENTADJUS_FILLBATCHNUM'));
            return false;
        }
        if (serialEmpty == false && batchEmpty == false) {
            $('#addNewSerial > tr').each(function() {
                var sNmbr = $(this).children().children('#newSerialNumber').val();
                var sWrnty = $(this).children().children('#newSerialWarranty').val();
                var sWrntyType = $(this).children().children('#newSerialWarrantyType').val();
                var sBt = $(this).children().children('#newSerialBatch').val();
                var sED = $(this).children().children('.serialEDate').val();
                if (sNmbr != '') {
                    serialProducts[sNmbr] = new serialProduct(sNmbr, sWrnty, sBt, sED,sWrntyType);
                }
            });
            $('#addPositiveAdjustmentProductsModal').modal('hide');
            addNewProductRow(batchProducts, serialProducts);
            clearPositiveAdjustmentModal();
        } else {
            p_notification(false, eb.getMessage('ERR_GRN_BATCH_PRODDETAILS'));
        }
    });

    $('#closePositiveAdjustmentProducts').on('click', function() {
        clearPositiveAdjustmentModal();
    });

    $('#addBatchItem').on('click', function() {
        if ($('#newBatchNumber').val() == null || $('#newBatchQty').val() == '' || $('#newBatchNumber').val().trim() === '') {
            p_notification(false, eb.getMessage('ERR_GRN_BATCH_INFO'));
        } else {
            if ($('#newBatchEDate').val() != '') {
                if ($('#newBatchMDate').val() != '') {
                    var newBatchEDate = eb.convertDateFormat('#newBatchEDate');
                    var newBatchMDate = eb.convertDateFormat('#newBatchMDate');
                    if (newBatchEDate < newBatchMDate) {
                        p_notification(false, eb.getMessage('ERR_GRN_EXPIRE_DATE'));
                        return false;
                    }
                }
            }
            var newBCode = $('#newBatchNumber').val();
            var newBQty = $('#newBatchQty').val();
            var newBMDate = $('#newBatchMDate').val();
            var newBEDate = $('#newBatchEDate').val();
            var newBWrnty = $('#newBatchWarrenty').val();
            if ((toFloat(batchCount) + toFloat(newBQty)) > adjustmentProductQty) {
                p_notification(false, eb.getMessage('ERR_GRN_BATCH_COUNT'));
            } else {
                if (batchProducts[newBCode] != null) {
                    p_notification(false, eb.getMessage('ERR_GRN_BATCH_CODE'));
                } else {
                    if ((toFloat(batchCount) + toFloat(newBQty)) == adjustmentProductQty) {
                        $('#proBatchAddNew').addClass('hidden');
                    }
                    batchCount += toFloat(newBQty);
                    batchProducts[newBCode] = new batchProduct(newBCode, newBQty, newBMDate, newBEDate, newBWrnty);
                    var cloneBatchAdd = $($('#proBatchSample').clone()).attr('id', newBCode).removeClass('hidden').addClass('tempProducts');
                    cloneBatchAdd.children('#batchNmbr').html(newBCode);
                    cloneBatchAdd.children('#batchQty').html(newBQty);
                    cloneBatchAdd.children('#batchMDate').html(newBMDate);
                    cloneBatchAdd.children('#batchEDate').html(newBEDate);
                    cloneBatchAdd.children('#batchW').html(newBWrnty);
                    cloneBatchAdd.insertBefore('#proBatchSample');
                    clearAddBatchProductRow();
                }
            }
        }

    });

    $('#reset').on('click', function() {
        clearProductScreen();
        $('#div_overlay').addClass('overlay');
    });

    function clone() {
        var sendStartingValue = quon;
        var originalQuontity = $('#qty').val();
        id = '#temp_' + idNumber;
        var quonnn = $(id).find('input[name=quontity]').val();
        var number = $(id).find('input[name=startNumber]').val();
        var startNumberStr = $(id).find('input[name=startNumber]').val();
        var prefix = $(id).find('input[name=prefix]').val();
        var pCode = locationProducts[proId].pC;
        if (quonnn == '') {
            quonnn = 0;
        }
        if (number == '') {
            p_notification(false, eb.getMessage('NO_START_NO'));
            return false;
        }
        if (number == 0) {
            p_notification(false, eb.getMessage('START_NO_ZERO'));
            return false;
        }
        var validationValue = true;
        validationValue = validation(number, prefix, quonnn, startNumberStr, pCode, sendStartingValue, id);
        if (validationValue == true) {
            if (toFloat(quonnn) <= originalQuontity && toFloat(quon) + toFloat(quonnn) <= originalQuontity && quonnn !== 0) {
                quon = toFloat(quon) + toFloat(quonnn);
                if (toFloat(quonnn) == originalQuontity || toFloat(quon) == originalQuontity) {
                    $(id).find('input[name=submit]').attr('disabled', true);
                    $(id).find('input[name=prefix]').attr('disabled', true);
                    $(id).find('input[name=startNumber]').attr('disabled', true);
                    $(id).find('input[name=quontity]').attr('disabled', true);
                    $(id).find('input[name=expireDateAutoFill]').attr('disabled', true);
                    $(id).find('input[name=batch_code]').attr('disabled', true);
                    $(id).find('input[name=mfDateAutoFill]').attr('disabled', true);
                    $(id).find('input[name=wrtyDays]').attr('disabled', true);
                    setValue(sendStartingValue, quonnn, id);
                }
                else {
                    var row = document.getElementById('temp_' + idNumber);
                    var table = document.getElementById('tempTbody');
                    var clone = row.cloneNode(true);
                    idNumber += 1;
                    clone.id = 'temp_' + idNumber;
                    table.appendChild(clone);
                    nextId = '#temp_' + idNumber;
                    $(nextId).addClass('tempy');
                    $(nextId).find('input[name=prefix]').val('');
                    $(nextId).find('input[name=startNumber]').val('');
                    $(nextId).find('input[name=quontity]').val('');
                    var adjsDate = new Date(Date.parse($('#adjusmentDate').val()));
                    $(nextId).find('input[name=expireDateAutoFill]').val('');
                    $(nextId).find('input[name=batch_code]').val('');
                    $(nextId).find('input[name=wrtyDays]').val('');
                    $(nextId).find('input[name=mfDateAutoFill]').val('');
                    var cloneMfD = $(nextId).find('input[name=mfDateAutoFill]').datepicker({
                        onRender: function(date) {
                            return date.valueOf() > adjsDate.valueOf() ? ((U_DATE_R_OVERRIDE) ? '' : 'disabled') : '';
                        }
                    }).on('changeDate', function(ev) {
                        cloneMfD.hide();
                    }).data('datepicker');
                    $(id).find('input[name=submit]').attr('disabled', true);
                    $(id).find('input[name=prefix]').attr('disabled', true);
                    $(id).find('input[name=startNumber]').attr('disabled', true);
                    $(id).find('input[name=quontity]').attr('disabled', true);
                    $(id).find('input[name=expireDateAutoFill]').attr('disabled', true);
                    $(id).find('input[name=batch_code]').attr('disabled', true);
                    $(id).find('input[name=mfDateAutoFill]').attr('disabled', true);
                    $(id).find('input[name=wrtyDays]').attr('disabled', true);
                    if (locationProducts[proId].sP == 1 && locationProducts[proId].bP == 1) {
                        $(nextId).find('input[name=mfDateAutoFill]').attr('disabled', false);
                        $(nextId).find('input[name=batch_code]').attr('disabled', false);
                    }
                    var cloneExpD = $(nextId).find('input[name=expireDateAutoFill]').datepicker({
                        onRender: function(date) {
                            return date.valueOf() < adjsDate.valueOf() ? ((U_DATE_R_OVERRIDE) ? '' : 'disabled') : '';
                        }
                    }).on('changeDate', function(ev) {
                        cloneExpD.hide();
                    }).data('datepicker');

                    setValue(sendStartingValue, quonnn, id);
                }
            }
            else {
                p_notification(false, eb.getMessage('WRNG_QUON_PI'));
                return false;
            }
        }
    }

    $('#tempTbody').on('click', '#submit_serial_number', function()
    {
        if ((locationProducts[proId].bP == 1) && (locationProducts[proId].sP == 1)) {
            if (!$(this).parents("tr").find("input[name='batch_code']").val()) {
                p_notification(false, eb.getMessage('ERR_NO_GRN_ADD_SERIAL_BATCH_DETAILS'));
                return false;
            }
        }
        clone();
        if ($('#submit_serial_number').attr('disabled')) {
            $(this).parent().parent().find('#expire_date_autoFill').removeClass('white_bg');
            $(this).parent().parent().find('#mf_date_autoFill').removeClass('white_bg');
        }
    });

    function setValue(quon, quonnn, id) {
        var abc = 0;
        var bQuntity = toFloat(quonnn);
        var max = toFloat(quon) + toFloat(quonnn);
        var prefix = $(id).find('input[name=prefix]').val();
        var startNumber = parseInt($(id).find('input[name=startNumber]').val());
        var startNumberString = $(id).find('input[name=startNumber]').val();
        var expire_date = $(id).find('input[name=expireDateAutoFill]').val();
        var mf_date = $(id).find('input[name=mfDateAutoFill]').val();
        var wrnty = $(id).find('input[name=wrtyDays]').val();
        var wrntyType = $(id).find('#wrtyType').val();
        if (locationProducts[proId].sP == 1 && locationProducts[proId].bP == 1) {
            var batchNumber = $(id).find('input[name=batch_code]').val();
            batchProducts[batchNumber] = new batchProduct(batchNumber, bQuntity, mf_date, expire_date, wrnty, null);

            if (startNumber) {
                $('#addNewSerial tr').each(function() {
                    if (abc >= toFloat(quon) && abc < toFloat(max)) {
                        var numberLength = startNumberString.length;
                        var startingValue = pad(startNumber, numberLength);
                        if (prefix == '') {
                            $(this).find('input[name=serialCode]:first').val(startingValue);
                            $(this).find('input[name=serialEDate]').val(expire_date);
                            $(this).find('input[name=batchCode]').val(batchNumber);
                            $(this).find('input[name=serialWarronty]').val(wrnty);
                            $(this).find("#newSerialWarrantyType").val(wrntyType).change();
                        } else if (prefix != '') {
                            var num = prefix + startingValue;
                            $(this).find('input[name=serialCode]:first').val(num);
                            $(this).find('input[name=serialEDate]').val(expire_date);
                            $(this).find('input[name=batchCode]').val(batchNumber);
                            $(this).find('input[name=serialWarronty]').val(wrnty);
                            $(this).find("#newSerialWarrantyType").val(wrntyType).change();
                        }
                        startNumber += 1;
                    }
                    abc++;
                });
            }
        }
        else {
            if (startNumber) {
                $('#addNewSerial tr').each(function() {
                    if (abc >= toFloat(quon) && abc < toFloat(max)) {
                        var numberLength = startNumberString.length;
                        var startingValue = pad(startNumber, numberLength);
                        if (prefix == '') {
                            $(this).find('input[name=serialCode]:first').val(startingValue);
                            $(this).find('input[name=serialEDate]').val(expire_date);
                            $(this).find('input[name=serialWarronty]').val(wrnty);
                            $(this).find("#newSerialWarrantyType").val(wrntyType).change();
                        } else if (prefix != '') {
                            var num = prefix + startingValue;
                            $(this).find('input[name=serialCode]:first').val(num);
                            $(this).find('input[name=serialEDate]').val(expire_date);
                            $(this).find('input[name=serialWarronty]').val(wrnty);
                            $(this).find("#newSerialWarrantyType").val(wrntyType).change();
                        }
                        startNumber += 1;
                    }
                    abc++;
                });
            }
        }
    }
    function pad(n, width, z) {
        z = z || '0';
        n = n + '';
        return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
    }

    function validation(startNumber, prefix, bQuntity, startNumberStr, productCode, sendStartingValue, id) {
        var strNumberLength = startNumberStr.length;
        var matchSerialNumberCount = 0;
        var serialCheckStatus = false;
        eb.ajax({
            type: 'POST', url: BASE_URL + '/productAPI/checkSerialProductCode',
            data: {startingNumber: startNumber, prefix: prefix, serialQuntity: bQuntity, strNumberLength: strNumberLength, productCode: productCode, },
            success: function(respond) {
                if (respond.data == true) {
                    serialCheckStatus = true;
                }
            },
            async: false
        });
        if (serialCheckStatus == true) {
            p_notification(false, eb.getMessage('ERR_GRN_SERIAL_CODE'));
            return false;
        }
        else {
            $('input.serialNumberList').each(function() {

                for (i = startNumber; i < (parseInt(startNumber) + parseInt(bQuntity)); i++) {
                    if (prefix) {
                        var startingValue = prefix + pad(i, strNumberLength);
                        if ($(this).val() == startingValue) {
                            matchSerialNumberCount++;
                        }
                    } else {
                        var startingValue = pad(i, strNumberLength);
                        if ($(this).val() == startingValue) {
                            matchSerialNumberCount++;
                        }
                    }

                }

            });
            if (matchSerialNumberCount >= 1) {
                p_notification(false, eb.getMessage('ERR_GRN_SAME_SERIAL_CODE'));
                return false;
            }
            else {
                return true;
            }

        }
    }

    function addItemToRow() {
        var proCodeAndName = $('#itemCode option:selected').html();
        var iCode = $('#itemCode').val();
        var productType = $('#itemCode').data('PT');
        var item_desc = $('#itemCode').data('PN');
        var availableQty = $('#addNewItemRow #availableQuantity').val();
        var prc = accounting.unformat($('#addNewItemRow #unitPrice').val());
        var tot = accounting.unformat($('#total').html());
        var ft = accounting.unformat($('#finaltotal').html());
        var uom = $("#qty").siblings('.uom-select').children('button').find('.selected').data('abbr');
        var inttot = parseFloat(tot);
        var qty;
        bSSerialData = [];
        var batchTextData = [];

        var sD = serialData;
        var bD = batchData;
        var sBD = serialAndBatchData;
        if (sD.length > 0 && bD.length === 0 && sBD === 0) {
            serialData = getCheckedSerialValues('serial_body');
        } else if (sD.length === 0 && bD.length > 0 && sBD === 0) {
            batchTextData = getTextBoxValues('batch_qty');
        } else if (sD.length === 0 && bD.length === 0 && sBD > 0) {
            bSBatchData = getTextBoxValues('batch_qty');
            bSSerialData = getCheckedBatchValues('batch_serial_data_table');
        }

        var bD = _.size(bSBatchData) ? _.size(bSBatchData) : 0;
        var sD = _.size(bSSerialData) ? _.size(bSSerialData) : 0;
        var bS = bD + sD;
        var b = _.size(batchTextData) ? _.size(batchTextData) : 0;
        var s = serialData.length;
        if (s > 0 && b === 0 && bS === 0) {
            qty = getTotalChecked('serial_body');
        } else if (s === 0 && b > 0 && bS === 0) {
            qty = getTotalTextBoxValues('batch_qty');
        } else if (s === 0 && b === 0 && bS > 0) {
            var sQty = getTotalChecked('batch_serial_data_table') ? getTotalChecked('batch_serial_data_table') : 0;
            var bQty = getTotalTextBoxValues('batch_qty') ? getTotalTextBoxValues('batch_qty') : 0;
            qty = sQty + bQty;
        } else {
            qty = $('#qty').val();
        }
        var priceQty = qty;
        if (productType == 2 && qty == 0) {
            priceQty = 1;
        }
        if (typeof locationProductID === "undefined") {
            locationProductID = 0;
        }
        var tbl_row = "<tr class = 'addedProducts' id='tr_" + proId + "'><td><input value='" + proCodeAndName + "' type='text' readonly='true' name='productCode' class='form-control'\></td>"
                + "<td>"
                + "<div class='input-group'><input value=" + availableQty + " type='text' readonly='true' name='availableQty' id='availableQty' class='form-control'>"
                + "</div>"
                + "</td>"
                + "<td>"
                + "<div class='input-group'><input data-basevalue=" + qty + " value=" + qty + " type='text' readonly='true' name='quantity' class='form-control'>"
                + "</div>"
                + "</td>"
                + "<td class='input-group'><input value=" + prc + " type='text' readonly='true' name='unitPrice' id='unitPrice' class='form-control' align='center'>"
                + "<strong class='input-group-addon'>" + companyCurrencySymbol + "</strong>"
                + "</td>" + "<td id='ttl' class='text-right'>" + accounting.formatMoney(parseFloat(priceQty * prc).toFixed(4)) + "</td>"
                + "<td class='text-center'><button class='deleteAdjustment btn btn-default'><i class='fa fa-trash-o delete' id='" + proId + "'></i></button></td>";
        tbl_row += "</tr>";
        $("#formRow").append(tbl_row);

        $("#formRow").find('tr').each(function() {
            if (this.id == "tr_" + proId) {
                $(this).find("input[name='quantity']").addUom(locationProducts[iCode].uom);
                $(this).find("input[name='availableQty']").addUom(locationProducts[iCode].uom);
                $(this).find("input[name='unitPrice']").addUomPrice(locationProducts[iCode].uom);
            }
        });

        var rowTotal = parseFloat(priceQty) * parseFloat(prc);
        var netTotal = parseFloat(ft) + parseFloat(rowTotal);
        $('#finaltotal').html(accounting.formatMoney(parseFloat(netTotal).toFixed(2)));
        var uomConversionRate = $("#qty").siblings('.uom-select').find('.selected').data('uc');
        items[proId] = new item(iCode, item_desc, qty, prc, uom, selectedProductUom, inttot, JSON.stringify(serialData), JSON.stringify(batchTextData), JSON.stringify(bSBatchData), JSON.stringify(bSSerialData), locationProductID, productType, uomConversionRate);
        $('#itemCode, #unitPrice, #qty').val('');
        clearItemCode();
        $(".available-qty input[id='itemQuantity']").prop('disabled', false);
        $('#total').html('0.00');
        $('#addNewItemRow .uomqty').val('');
        $('#addNewItemRow .uom-select').remove();
        $("#addNewItemRow .uomqty").val('');

        $("#addNewItemRow .uomPrice").val('');
        $("#addNewItemRow .uom-price-select").remove();

        $('.uomLi').remove();
        $('#uomAb').html('');
        selectedProductUom = '';
        $("#add_item").attr('disabled', false);
        $('[data-id=itemCode]').trigger('click');
    }

    function addNewProductRow(batchProducts, serialProducts) {
        var uomConversionRate = $("#qty").siblings('.uom-select').find('.selected').data('uc');
        var proCodeAndName = $('#itemCode option:selected').html();
        var iC = $('#itemCode').val();
        var productType = $('#itemCode').data('PT');
        var iN = $('#itemCode').data('PN');
        var uPrice = accounting.unformat($('#unitPrice').val());
        var selectedUomAbbr = $("#qty").siblings('.uom-select').children('button').find('.selected').data('uomID');
        adjustmentProductQty = $('#qty').val();
        var qty = adjustmentProductQty;
        if (productType == 2 && adjustmentProductQty == 0) {
            qty = 1;
        }
        var locationProductID = locationProducts[proId].lPID;
        var rowCostTotal = setItemRowCost(qty, uPrice);
        var newTrID = 'tr_' + proId;
        var clonedRow = $($('#preSetSample').clone()).attr('id', newTrID).addClass('addedProducts');
        $("input[name='productCode']", clonedRow).val(proCodeAndName);
        $("input[name='qty']", clonedRow).val(adjustmentProductQty).addUom(locationProducts[proId].uom);
        $("input[name='unitPrice']", clonedRow).val(uPrice).addUomPrice(locationProducts[proId].uom);
        $(".uomPrice", clonedRow).prop('disabled', true);
        $(".uomqty", clonedRow).prop('disabled', true);
        clonedRow.children('#ttl').html(accounting.formatMoney(rowCostTotal));
        $('#addNewUom', clonedRow).hide();
        clonedRow.removeClass('hidden');
        clonedRow.insertBefore('#preSetSample');
        $('#finaltotal').html(accounting.formatMoney(setItemTotalCost(adjustmentProductQty, uPrice)));
        items[proId] = new positiveAdjustmentProduct(locationProductID, proId, iC, iN, adjustmentProductQty, uPrice, selectedUomAbbr, selectedProductUom, rowCostTotal, batchProducts, serialProducts, productType, uomConversionRate);
        clearAddedNewRow();
        $('[data-id=itemCode]').trigger('click');
    }

    function clearProductRow() {
        $('#itemCode').val('');
        $('#itemCode').selectpicker('render');
        $("#qty").val('').siblings('.uomqty,.uom-select').remove();
        $("#qty").show();
        $("#unitPrice").val('').siblings('.uomPrice,.uom-price-select').remove();
        $("#unitPrice").show();
        $('#total').html('');
        $('#uom').html('');
        $('#uomAb').html('');
    }

    function positiveAdjustment(productType) {
        if (items[proId]) {
            p_notification(false, eb.getMessage('ERR_INVENTADJUS_ITEMINSERT'));
            $('#itemCode').val('');
            $('#itemCode').selectpicker('render');
            $("#unitPrice").val('').siblings('.uomPrice,.uom-price-select').remove();
            $("#qty").val('').siblings('.uomqty,.uom-select').remove();
            $("#qty").show();
            $('#uomAb').html('');
            $('#total').html('');
        } else {
            clearPositiveAdjustmentModal();
            $('.cloneSerials').remove();
            $('#serialSample').children().children('#newSerialNumber').removeClass('serialNumberList');
            $('#serialSample').children().children('#newSerialBatch').removeClass('serialBatchList');
            $('#serialSample').children().children('#newSerialBatch').prop('disabled', false);
            if (productType == 2) {
                locationProducts[proId].bP = 0;
                locationProducts[proId].sP = 0;
            }
            if (locationProducts[proId].bP == 1) {
                $('#batchAddScreen').removeClass('hidden');
                $('#newBatchMDate').addClass('white_bg');
                $('#newBatchEDate').addClass('white_bg');
            }

            if (locationProducts[proId].sP == 1) {
                $('#serialAddScreen').removeClass('hidden');
            }
            if (locationProducts[proId].sP == 1 && locationProducts[proId].bP == 0) {
                $('#temp_1').find('input[name=batch_code]').attr('disabled', true);
                $('#temp_1').find('input[name=mfDateAutoFill]').attr('disabled', true).removeClass('white_bg');
                $('#temp_1').find('input[name=expireDateAutoFill]').addClass('white_bg');
            }
            if (locationProducts[proId].sP == 1 && locationProducts[proId].bP == 1) {
                $('#batchAddScreen').addClass('hidden');
                $('#temp_1').find('input[name=batch_code]').attr('disabled', false);
                $('#temp_1').find('input[name=mfDateAutoFill]').attr('disabled', false).addClass('white_bg');

            }
            if (locationProducts[proId].bP == 0 && locationProducts[proId].sP == 0) {
                addNewProductRow({}, {});

            } else {
                if (locationProducts[proId].bP == 1) {
                    var selectedUomAbbr = $("#qty").siblings('.uom-select').children('button').find('.selected').data('uomID');
                    var conversion = locationProducts[proId].uom[selectedUomAbbr].uC;
                    var qty = parseFloat($('#qty').val()) / parseFloat(conversion);
                    $('#positiveAdjustmentProductCodeBatch').html(locationProducts[proId].pC);
                    $('#positiveAdjustmentProductQuantityBatch').html(qty);
                }
                if (locationProducts[proId].sP == 1) {
                    $('#addNewSerial input[type=text]').val('');
                    $('#positiveAdjustmentProductCodeSerial').html(locationProducts[proId].pC);
                    $('#positiveAdjustmentProductQuantitySerial').html($('#qty').val());
                    $('#serialSample').children().children('#newSerialBatch').addClass('serialBatchList');
                    if (locationProducts[proId].bP == 0) {
                        $('#serialSample').children().children('#newSerialBatch').prop('disabled', true);
                    }
                    $('#serialSample').children().children('#newSerialNumber').addClass('serialNumberList');
                    for (i = 0; i < adjustmentProductQty - 1; i++) {
                        var serialAddRowClone = $('#serialSample').clone().attr('id', i).addClass('cloneSerials');
                        $('#serialSample').children().children('#newSerialNumber').addClass('serialNumberList');
                        $('#serialSample').children().children('#newSerialBatch').addClass('serialBatchList');
                        serialAddRowClone.children().children('#newSerialNumber').addClass('serialNumberList');
                        serialAddRowClone.children().children('#newSerialBatch').addClass('serialBatchList');
                        serialAddRowClone.children().children('#serialWarrenty').attr('id', 'sEW' + i).addClass('sEW');
                        serialAddRowClone.children().children('#newSerialEDate').attr('id', 'sEDate' + i);
                        if (locationProducts[proId].bP == 0) {
                            $('#serialSample').children().children('#newSerialBatch').prop('disabled', true);
                            serialAddRowClone.children().children('#newSerialBatch').prop('disabled', true);
                        }
                        serialAddRowClone.insertBefore('#serialSample');
                    }

                    var dDate = new Date(Date.parse($('#adjusmentDate').val()));
                    for (j = 0; j <= i; j++) {

                        $('#sEDate' + j).datepicker({
                            onRender: function(date) {
                                return date.valueOf() < dDate.valueOf() ? ((U_DATE_R_OVERRIDE) ? '' : 'disabled') : '';
                            }
                        });
                    }
                }
                $addPositiveAdjustmentProductsModal.modal('show');
            }
        }
    }

    $("#adjustmentProductTable").on('click', '.deleteAdjustment', function(e) {
        e.preventDefault();
        var deletePTrID = $(this).closest('tr').attr('id');
        var deletePID = deletePTrID.split('tr_')[1].trim();

        bootbox.confirm('Are you sure you want to remove this product ?', function(result) {
            if (result == true) {
                delete items[deletePID];
                var rowTotal = accounting.unformat($('#' + deletePTrID).find('#ttl').text());
                $('#' + deletePTrID).remove();
                var netTotal = accounting.unformat($('#finaltotal').html());
                var newTotal = parseFloat(netTotal) - parseFloat(rowTotal);
                $('#finaltotal').html(accounting.formatMoney(parseFloat(newTotal).toFixed(2)));
            }
        });
    });

    function negativeAdjustment() {
        var iCode = $('#itemCode').val();
        var productType = $('#itemCode').data('PT');
        var prc = parseFloat(accounting.unformat($('#unitPrice').val()));
        $('#unitPrice').val(prc.toFixed(2));
        var qty = 0;
        var $thisRow = $(this).parents('tr');

        var $thisVals = getThisRowData($thisRow);
        var qtyMainRow = $thisVals.qtyMainRow ? $thisVals.qtyMainRow : 1;
        if ($thisVals.qtyMainRow) {
            qty = qtyMainRow;
        } else {
            qty = $('#qty').val();
        }

        if (oldCode == $('#itemCode').val()) {
            iCode = '';   
        }
        if (iCode == '') {
            p_notification(false, eb.getMessage('ERR_INVENTADJUS_PRODCODE'));
            $("#add_item").attr('disabled', false);
            $('#itemCode').focus();
        } else if (prc == '' || isNaN(prc)) {
            $("#add_item").attr('disabled', false);
            p_notification(false, eb.getMessage('ERR_INVENTADJUS_STOCK'));
        } else {
            oldCode = $('#itemCode').val();
            if (items[proId]) {
                p_notification(false, eb.getMessage('ERR_INVENTADJUS_ITEMINSERT'));
                $('#itemCode').empty().val('').selectpicker('refresh');
                $("#unitPrice").val('').siblings('.uomPrice,.uom-price-select').remove();
                $("#qty").val('').siblings('.uomqty,.uom-select').remove();
                $("#qty").show();
                $("#unitPrice").show();
                $('#uomAb').html('');
                $('#total').html('');
                //reset available qty text feild
                $('.available-qty #itemQuantity').val('');
                $('.unit-price #itemQuantity').val('');
                $('.unit-price #itemQuantity').prop('disabled', false);
                $('.available-qty #itemQuantity').prop('disabled', false);
                $('.available-qty .uom-select').remove();
                $(".unit-price .uom-price-select").remove();
            } else {
                $.ajax({
                    type: 'POST',
                    url: BASE_URL + '/api/inventory-adjustment/get-serial-or-batch-details',
                    data: {productID: $('#itemCode').val(), locationID: locationID},
                    success: function(data) {
                        serialData = data.serialProductData;
                        batchData = data.batchProductData;
                        serialAndBatchData = _.size(data.seriaAndBatchlSerialListData);
                        proOnlyProductData = _.size(data.proOnlyProductData);
                        var uomS = locationProducts[proId].uom;

                        if (data.status == false) {
                            clearProductRow();
                            p_notification(data.status, data.msg);
                        } else {
                            stockQty = locationProducts[proId].LPQ;
                            if (productType != 2 && parseFloat(stockQty) == 0) {
                                p_notification(false, eb.getMessage('ERR_DELI_ENTER_QUAN'));
                                $("#add_item").attr('disabled', false);
                            } else if (productType != 2 && qty <= 0) {
                                p_notification(false, eb.getMessage('ERR_INVENTADJUS_ENTQUANT'));
                                $('.qty').focus();
                                $("#add_item").attr('disabled', false);
                            } else if (productType != 2 && parseFloat(qty) > stockQty) {
                                p_notification(false, eb.getMessage('ERR_INVENTADJUS_QUANT_STOCK'));
                                $('.qty').focus();
                            } else {
                                if (productType != 2 && serialData.length > 0 && batchData.length === 0 && serialAndBatchData === 0) {
                                    $.each(data.serialProductData, function(index, value) {
                                        locationProductID = value.locationProductID;

                                        var tbl_row = "<tr data-id=" + value.productSerialID + ">"
                                                + "<td>" + value.productSerialCode + "</td>"
                                                + "<td class = 'text-center'>"
                                                + "<div class='input-group'><input id='bID_" + value.productSerialID + "' type='text' data-basevalue='1' value='1' readonly='true' name='availableQuantity' class='form-control'>"
                                                + "</div>"
                                                + "</td>"
                                                + "<td class ='text-center'><input data-baseCheckedSerial='1' data-serialcheckedvalue='1' id=" + value.productSerialCode + " value=" + value.productSerialCode + " class = 'select_serial_code' type = 'checkbox' name = 'select_serial_code' ></td>"
                                        tbl_row += "</tr>";
                                        document.getElementById("serial_body").innerHTML += tbl_row;
                                    });
                                    $('.modal-body').addClass('serial_modal');
                                    $("#serial_body").children('tr').each(function() {
                                        $(this).find('input[name="availableQuantity"]').addUom(uomS);
                                    });

                                    $('#serial_table_outer_wrapper').show();
                                    $('#batch_serial_table_outer_wrapper,#batch_table_outer_wrapper').hide();

                                    $loadProductTypeModal.modal('show');
                                    $('.modal-body').removeClass('serial_modal');
                                } else if (productType != 2 && serialData.length === 0 && batchData.length > 0 && serialAndBatchData === 0) {
                                    $.each(batchData, function(index, value) {
                                        locationProductID = value.locationProductID;
                                        var qty = value.productBatchQuantity ? value.productBatchQuantity : 0;
                                        if (qty > 0) {
                                            var tbl_row = "<tr>"
                                                    + "<td>" + value.productBatchCode + "</td>"
                                                    + "<td id='availableQuantity'>" + "<div class='input-group'><input data-basevalue=" + qty + " value=" + qty + " type='text' readonly='true' name='availableQuantity' class='form-control'>"
                                                    + "</div>"
                                                    + "</td>"
                                                    + "<td class = 'text-center'>"
                                                    + "<div class='input-group'><input id='bID_" + value.productBatchID + "' type='text' name='batch_qty' class='form-control'>"
                                                    + "</div></td>"
                                            tbl_row += "</tr>";
                                            document.getElementById("batch_body").innerHTML += tbl_row;
                                        }
                                    });
                                    $("#batch_body").children('tr').each(function() {
                                        $(this).find('input[name="availableQuantity"]').addUom(uomS);
                                        $(this).find('input[name="batch_qty"]').addUom(uomS);
                                    });

                                    $('#batch_serial_table_outer_wrapper,#serial_table_outer_wrapper').hide();
                                    $('#batch,#batch_table_outer_wrapper').show();
                                    $loadProductTypeModal.modal('show');
                                } else if (serialData.length === 0 && batchData.length === 0 && serialAndBatchData === 0 && proOnlyProductData > 0) {
                                    addItemToRow();
                                    $loadProductTypeModal.modal('hide');
                                } else if (productType != 2 && serialData.length === 0 && batchData.length === 0 && serialAndBatchData > 0) {
                                    $.each(data.seriaAndBatchlSerialListData, function(subindex, subvalue) {
                                        locationProductID = subvalue.lpID;

                                        var tbl_row = "<tr>"
                                                + "<td>" + subvalue.pbCD + "</td>"
                                                + "<td>" + subvalue.psCD + "</td>"
                                                + "<td>"
                                                + "<div class='input-group'><input data-basevalue='1' value='1' type='text' readonly='true' name='availableQuantitySerial' class='form-control'>"
                                                + "</div>" + "</td>"
                                                + "<td class = 'text-center'><input data-baseCheckedSerial='1' id=" + subvalue.pbCD + " value=" + subvalue.psCD + " class = 'select_serial_code' type = 'checkbox' name = 'select_serial_code' >"
                                        tbl_row += "</tr>";
                                        document.getElementById("batch_serial_body").innerHTML += tbl_row;
                                    });
                                    $("#batch_serial_body").children('tr').each(function() {
                                        $(this).find('input[name="availableQuantitySerial"]').addUom(uomS);
                                    });
                                    $('#serial_table_outer_wrapper,#batch_table_outer_wrapper').hide();
                                    $('#batch_serial_table_outer_wrapper').show();

                                    $loadProductTypeModal.modal('show');
                                }
                                else {
                                    clearProductRow();
                                    p_notification(false, eb.getMessage('ERR_INVENTADJUS_NO_PRODQUAN'));
                                }
                            }
                        }

                    }
                });
            }
        }
    }

    function clearAddedNewRow() {
        $('.uomLi').remove();
        $('#uomAb').html('');
        $('#itemCode').val('');
        clearItemCode();
        $('#qty').val('');
        $('#unitPrice').val('');
        batchProducts = {};
        serialProducts = {};
        selectedProductUom = '';
        $('#addNewItemRow .uomqty').val('');
        $('#addNewItemRow .uom-select').remove();
        $('#addNewItemRow .uomPrice').val('');
        $('#addNewItemRow .uom-price-select').remove();
    }

    function setDataInitialLoading() {
        var key = $('#locationName option:selected').val();
        locationID = key;
        setDataForLocation(key);
        getReferenceNo(key);
    }
    var getAddRow = function(pro_id) {
        if (pro_id != undefined) {
            var $row = $('table.adjustment-table > tbody > tr', $productTable).filter(function() {
                return $(this).data("id") == pro_id
            });
            return $row;
        }
        return $('tr.add-row:not(.sample)', $productTable);
    };

    function positiveAdjustmentProduct(lPID, pID, pCode, pN, pQ, pUP, pUom, uomID, pTotal, bProducts, sProducts, productType, uomConversionRate) {
        this.locationPID = lPID;
        this.pID = pID;
        this.pCode = pCode;
        this.pName = pN;
        this.pQuantity = pQ;
        this.pUnitPrice = pUP;
        this.pUom = pUom;
        this.uomID = uomID;
        this.pTotal = pTotal;
        this.bProducts = bProducts;
        this.sProducts = sProducts;
        this.productType = productType;
        this.uomConversionRate = uomConversionRate;
    }

    function batchProduct(bCode, bQty, mDate, eDate, warnty) {
        this.bCode = bCode;
        this.bQty = bQty;
        this.mDate = mDate;
        this.eDate = eDate;
        this.warnty = warnty;
    }

    function serialProduct(sCode, sWarranty, sBCode, sEdate,sWarrantyType) {
        this.sCode = sCode;
        this.sWarranty = sWarranty;
        this.sBCode = sBCode;
        this.sEdate = sEdate;
        this.sWarrantyType = sWarrantyType;
    }

    function getThisRowData($thisRow) {
        var thisVals = {
            adjustmentQuantity: {
                qty: $("input[name='batch_qty']", $thisRow).val(),
                qtyMainRow: $("input[name='qty']", $thisRow).val(), //                conversionMainRow: $("input[name='qty']", $thisRow).data('conversion'),
                availableQuantity: $("input[name='availableQuantity']", $thisRow).val(),
            }
        };

        return thisVals;
    }

    function removeSpaces(string) {
        return string.split(' ').join('');
    }

    function getCheckedValuesBySelectList(id) {
        var chkArray = [];
        $("#" + id + " input:checked").each(function() {
            chkArray.push($(this).val());
        });

        return chkArray;
    }

    function getCheckedBatchValues(id) {
        var chkArray = [];
        $("#" + id + " input[type='checkbox']:checked").each(function() {
            chkArray.push({b_code: $(this).attr("id"), s_code: $(this).val(), baseQty: $(this).data('basecheckedserial')});
        });

        return chkArray;
    }
    function getCheckedSerialValues(id) {
        var chkArray = [];
        $("#" + id + " input[type='checkbox']:checked").each(function() {
            chkArray.push({s_code: $(this).val(), baseQty: $(this).data('basecheckedserial')});
        });

        return chkArray;
    }

    function getUploadSerialValues(data) {
        var chkArray = [];
        $.each(data, function(key, value){
            chkArray.push({s_code: value.s_code, baseQty: value.baseQty});
        });

        return chkArray;
    }

    function getUploadBatchSerialValues(data) {
        var chkArray = [];
        $.each(data, function(key, value){
            chkArray.push({b_code: value.newBCode ,s_code: value.newSCode, baseQty: 1});
        });

        return chkArray;
    }

    function getUploadBatchValues(data) {
        var chkArray = [];

        $.each(data, function(key, val){
           chkArray.push({id: val.bID, value: val.qty});
        });
        return chkArray;
    }

    function getTotalChecked(id) {
        var total = 0;
        $("#" + id + " input[type=checkbox]:checked").each(function() {
            if ($(this).val()) {
                var i = 1;
            }
            total += i;
        });

        return total;
    }

    function getTotalUnChecked(id) {
        var total = 0;
        $("#" + id + " input[type=checkbox]:not(:checked)").each(function() {
            if ($(this).val()) {
                var i = 1;
            }
            total += i;
        });

        return total;
    }

    function getTextBoxValues(id) {

        var values = [];
        $('input[name="' + id + '"]').map(function() {
            if ($(this).attr("id") != "" && $(this).val() != "") {
                values.push({id: $(this).attr("id"), value: $(this).data('basevalue')});
            }
        });
        return values;
    }

    function getTotalTextBoxValues(id) {
        var total = 0;
        $('input[name="' + id + '"]').map(function() {
            if ($(this).attr("id") != "" && $(this).val() != "") {
                total += ($(this).data('basevalue'));
            }
        });
        return total;
    }

    function isInArray(value, array) {
        return array.indexOf(value) > -1 ? true : false;
    }

    function setValueToBatchSelectList() {
        var batch = document.getElementById("batch_product");
        var option = document.createElement("option");
        option.text = "";
        option.id = 'batch_empty';
        batch.add(option);
        $('#batch_product option:not(#batch_empty)').remove();
    }

    function selectBatch() {
        var batch_empty = document.getElementById("batch_empty");
        batch_empty.remove(batch_empty.selectedIndex);
    }

    function item(pID, item, qty, uPrice, uom, uomID, totalCost, serialData, batchTextData, bSBatchData, bSSerialata, locationProductID, productType, uomConversionRate) {
        this.p_id = pID;
        this.product = item;
        this.quantity = qty;
        this.unit_price = uPrice;
        this.uom = uom;
        this.uomID = uomID;
        this.total_cost = totalCost;
        this.serial_data = serialData;
        this.batch_text_data = batchTextData;
        this.batch_serial_batch_data = bSBatchData;
        this.batch_serial_serial_data = bSSerialata;
        this.locationProductID = locationProductID;
        this.productType = productType;
        this.uomConversionRate = uomConversionRate;
    }

    function setItemCost() {
        var unitPrice = accounting.unformat($('#unitPrice').val());
        var qty = $('#qty').val();
        var tot = unitPrice * qty;
        $('#total').html(accounting.formatMoney(parseFloat(tot).toFixed(2)));
    }

    function setItemTotalCost(qty, uPrice) {
        var ft = accounting.unformat($('#finaltotal').html());
        var rowTotal = parseFloat(qty) * parseFloat(uPrice);
        var netTotal = parseFloat(ft) + parseFloat(rowTotal);
        return parseFloat(netTotal).toFixed(2);
    }

    function setItemRowCost(qty, uPrice) {
        var tot = uPrice * qty;
        return parseFloat(tot).toFixed(2);
    }
    function clearItemCode() {
        $('#itemCode')
                .empty()
                .val('')
                .append($("<option></option>")
                        .attr("value", "")
                        .text('Select Item Code or Name'))
                .selectpicker('refresh')
                .trigger('change');
    }

    function clearProductScreen() {
        $('.addedProducts').remove();
        $('#adjusmentDate').val('');
        $('#reason').val('');
        $('#adjustmentType').prop('selectedIndex', 0);
        $('#itemCode, #unitPrice, #qty').val('');
        clearItemCode();
        $('#total').html('0.00');
        $('#finaltotal').html('0.00');
        $('.uomLi').remove();
        $('#uomAb').html('');
        $('#refNo').val('');
        items = {};
//        locationProducts = '';
        productCodeList = {};
        productNameList = {};
        uomList = {};
    }

    function clearProductScreenForAdjustment() {
        $('.addedProducts').remove();
        $('#adjusmentDate').val('');
        $('#reason').val('');
        selectedProductID = '';
        $('#itemCode, #unitPrice, #qty').val('');
        clearItemCode();
        $(".uomqty").val('');
        $(".uomPrice").val('');
        $(".unit-price .uom-price-select").remove();
        $(".unit-price .uomPrice").prop('disabled', false);
        $(".available-qty .uom-select").remove();
        $(".available-qty .uomqty").prop('disabled', false);
        $('.uom-select').remove();
        $('.uom-price-select').remove();
        $('#total').html('0.00');
        $('#finaltotal').html('0.00');
        $('.uomLi').remove();
        $('#uomAb').html('');
        items = {};
    }

    function clearPositiveAdjustmentModal() {
        $('.tempProducts').remove();
        $('.cloneSerials').remove();
        $('#positiveAdjustmentProductCodeBatch').html('');
        $('#positiveAdjustmentProductQuantityBatch').html('');
        $('#positiveAdjustmentProductCodeSerial').html('');
        $('#positiveAdjustmentProductQuantitySerial').html('');
        $('#batchAddScreen').addClass('hidden');
        $('#serialAddScreen').addClass('hidden');
        $('#addNewSerial input[type=text]').val('');
        $('#proBatchAddNew').removeClass('hidden');
        batchCount = 0;
        batchProducts = {};
        serialProducts = {};
    }

    function clearAddBatchProductRow() {
        $('#newBatchNumber').val('');
        $('#newBatchQty').val('');
        $('#newBatchMDate').val('');
        $('#newBatchEDate').val('');
        $('#newBatchWarrenty').val('');
    }
    function isDate(txtDate)
    {
        var currVal = txtDate;
        if (currVal == '')
            return false;

        //Declare Regex
        var rxDatePattern = /^(\d{4})(\/|-)(\d{1,2})(\/|-)(\d{1,2})$/;
        var dtArray = currVal.match(rxDatePattern); // is format OK?
        if (dtArray == null)
            return false;

        //Checks for mm/dd/yyyy format.
        dtMonth = dtArray[3];
        dtDay = dtArray[5];
        dtYear = dtArray[1];
        if (dtMonth < 1 || dtMonth > 12)
            return false;
        else if (dtDay < 1 || dtDay > 31)
            return false;
        else if ((dtMonth == 4 || dtMonth == 6 || dtMonth == 9 || dtMonth == 11) && dtDay == 31)
            return false;
        else if (dtMonth == 2)
        {
            var isleap = (dtYear % 4 == 0 && (dtYear % 100 != 0 || dtYear % 400 == 0));
            if (dtDay > 29 || (dtDay == 29 && !isleap))
                return false;
        }
        return true;
    }

    $('#adjustmentUploadView').on('click', function(e){
        e.preventDefault();
        if(!$('#locationName').val()){
            $('#adjustmentImportModal').modal('hide');
            p_notification(false, eb.getMessage('ERR_GRN_SELECT_LOCAT'));
        } else if(!$('#adjusmentDate').val()) {
            $('#adjustmentImportModal').modal('hide');
            p_notification(false, eb.getMessage('ERR_INVENTADJUS_SELECDATE'));
        } else if(!$('#reason').val()) {
            $('#adjustmentImportModal').modal('hide');
            p_notification(false, eb.getMessage('ERR_INVENTADJUS_REASON'));
        } else {
            $('#adjustmentImportModal').modal('show');
            $('#locationName').attr('disabled', 'disabled');
            locationIDForUpload = $('#locationName').val();
        }

    });

    $('#productUploadCancelBtn').on('click', function(){
        $('#adjustmentImportModal').modal('hide');
    });
    
    var uploadedDatafieldLength;
    //adjustment Item upload process
    $('#productUploadBtn').on('click', function(){

        var file = document.getElementById("productFile").files[0];
        if (file) {
            var reader = new FileReader();
            productHeader = false;
            dataType = $('#productCharacterEncoding').val();
            productDelimiter = $('#productDelimiter').val();
            batchSerialType = $('#productBatchSerialType').val();
            if ($('#productHeader').is(":checked")){
                productHeader = true;
            }
            if(batchSerialType == 'none'){
                p_notification(false, eb.getMessage('ERR_ADJS_UPLOAD_TYPE_SELECT'));
                return false;
            }
            reader.readAsText(file, dataType);
            reader.onload = function (evt) {
               var dataSet = evt.target.result;

                eb.ajax({type: 'POST',
                    url: BASE_URL + '/api/inventory-adjustment/adjustment-item-import',
                    data: {dataType: dataType, productDelimiter: productDelimiter, dataSet: dataSet, batchSerialType: batchSerialType, productHeader: productHeader},
                    success: function(respond) {
                        if(respond.status){
                            $('#adjustmentImportModal').modal('hide');
                            $('#adjustmentImportSecondModal').modal('show');
                            $('#adjustmentItemUploadTable #adjustmentItemUploadBody tr').remove();
                            if(respond.data['headerFlag'] == 'true'){
                                var incrementID = 0;
                                uploadedDatafieldLength = 0;
                                $.each(respond.data['dataSet'], function(key, value){
                                    var newRow = "<tr>\n\
                                                   <td class='col-lg-3'>"+ value[0] +"</td>\n\
                                                    <td class='col-lg-3'>"+ value[1] +"</td>\n\
                                                    <td class='col-lg-3'><select class='form-control adjusmentproductimportselector selectpicker grnItemUploadSelect' data-live-search='true' id='selectID_"+incrementID+"'  name='select'>\n\
                                                        <option value='none'> Select Type </option>\n\
                                                        <option value='itemCode'> Item Code </option>\n\
                                                        <option value='batchCode'> Batch Code </option>\n\
                                                        <option value='serialCode'> Serial Code </option>\n\
                                                        <option value='quantity'> Quantity </option>\n\
                                                        <option value='unitPrice'> Unit Price </option>\n\
                                                        <option value='exDate'> Expire Date </option>\n\
                                                        <option value='manDate'> Manufacturing Date </option>\n\
                                                        <option value='wDays'> Warranty Days </option>\n\
                                                        <option value='uomVal'> UOM </option>\n\
                                                    </select> </td>\n\
                                                  </tr>";
                                    $('#adjustmentItemUploadTable #adjustmentItemUploadBody').append(newRow);
                                    incrementID++;
                                    uploadedDatafieldLength = incrementID;
                                });
                                $('#uploadedDataSet').val(respond.data['hiddenDataSet']);
                                $('#adjustment-product-importdata-button').attr('data-id', incrementID);
                            } else {
                                var incrementID = 0;
                                uploadedDatafieldLength = 0;
                                $.each(respond.data['dataSet'], function(key, value){
                                    var newRow = "<tr>\n\
                                                    <td class='col-lg-3'> No Header</td>\n\
                                                    <td class='col-lg-3'>"+ value[0] +"</td>\n\
                                                    <td class='col-lg-3'><select class='form-control adjusmentproductimportselector selectpicker grnItemUploadSelect' data-live-search='true' id='selectID_"+incrementID+"'  name='select'>\n\
                                                        <option value='none'> Select Type </option>\n\
                                                        <option value='itemCode'> Item Code </option>\n\
                                                        <option value='batchCode'> Batch Code </option>\n\
                                                        <option value='serialCode'> Serial Code </option>\n\
                                                        <option value='quantity'> Quantity </option>\n\
                                                        <option value='unitPrice'> Unit Price </option>\n\
                                                        <option value='exDate'> Expire Date </option>\n\
                                                        <option value='manDate'> Manufacturing Date </option>\n\
                                                        <option value='wDays'> Warranty Days </option>\n\
                                                        <option value='uomVal'> UOM </option>\n\
                                                    </select> </td>\n\
                                                  </tr>";
                                    $('#adjustmentItemUploadTable #adjustmentItemUploadBody').append(newRow);
                                    incrementID++;
                                    uploadedDatafieldLength = incrementID;
                                });
                                $('#uploadedDataSet').val(respond.data['hiddenDataSet']);
                                $('#adjustment-product-importdata-button').attr('data-id', incrementID);
                            }
                        }else{
                            p_notification(respond.status, respond.msg);
                        }
                    }
                });
            }
            reader.onerror = function (evt) {
            document.getElementById("fileContents").innerHTML = "error reading file";
            }
        }
    });

    $('#adjustment-product-importcancel-button').on('click', function(){
        $('#adjustmentImportSecondModal').modal('hide');
        $('#adjustmentImportModal').modal('show');
    });

     $('#adjustmentItemUploadTable #adjustmentItemUploadBody').on('change','.adjusmentproductimportselector', function() {
        $('option').prop('disabled', false); //reset all the disabled options on every change event
        $('select').each(function() { //loop through all the select elements
            var val = this.value;
            if(val != 'none'){
                $('select').not(this).find('option').filter(function() { //filter option elements having value as selected option
                    return this.value === val;
                }).prop('disabled', true); //disable those option elements
            }
        });
        var keyVal = $(this).val();
        if(keyVal == 'uomVal'){
            eb.ajax({type: 'POST',
                url: BASE_URL + '/api/inventory-adjustment/uom-mapping',
                data: {
                    dataArray: $('#uploadedDataSet').val(),
                    id: $(this).attr('id'),
                },
                success: function(respond) {
                    $('#adjustmentImportUomModal').modal('show');
                    var optionsAsString;
                    $.each(respond.data.systemUomList, function(key, value){
                        optionsAsString += "<option value='" + value['uomID'] + "'>" + value['uomName'] + "</option>";
                    });
                    var uomRowCount = 0;
                    $('#adjustmentItemUomModal #adjustmentUploadUom tr').remove();
                    $.each(respond.data.uploadedUomList, function(key, value){
                        if(productHeader && key == 0){

                        } else {
                            var newRow = "<tr>\n\
                                        <td class='col-lg-3'>"+ value +"</td>\n\
                                        <td class='col-lg-3'><select class='form-control  selectpicker adjusmentUOMUploadSelect' data-live-search='true' id='uom_"+uomRowCount+"' data-id='"+value+"' name='select'>\n\
                                            "+optionsAsString+"\n\
                                            </select> </td>\n\
                                        </tr>";
                            $('#adjustmentItemUomModal #adjustmentUploadUom').append(newRow);
                            uomRowCount++;
                        }

                    });
                    $('#saveUploadUom').attr('data-uomcount', uomRowCount);
                }
            });

        }
    }).change();

    $('#adjustmentUomModal1').on('click','#saveUploadUom', function(){
        var uomRows = $(this).attr('data-uomcount');
        for (var i = 0; i < uomRows; i++) {
            var value = $('#uom_'+i).val();
            var uploadedUom = $('#uom_'+i).attr('data-id');
            uomMapArray[uploadedUom] = value;
        };

        $('#adjustmentImportUomModal').modal('hide');
    });

    $('#adjustment-product-importdata-button').on('click', function(){

        var rowCount = $(this).attr('data-id');
        var dataSet = $('#uploadedDataSet').val();
        var choices = {};
        for (var i = 0; i <= rowCount; i++) {
            var value = $('#selectID_'+i).val();
            choices[value] = i;
        };
        var validStepTwo = validateAdjustmentUploadStepTwo(choices, batchSerialType);
        if(validStepTwo){
            eb.ajax({type: 'POST',
            url: BASE_URL + '/api/inventory-adjustment/load-imported-data-to-adjustment-create',
            data: {
                dataArray: choices,
                batchSerialType: batchSerialType,
                productDelimiter: productDelimiter,
                dataType: dataType,
                dataSet: dataSet,
                locationID: locationIDForUpload,
                productHeader: productHeader,
                uomMapArray: uomMapArray,
                adjustmentType: adjustmentType
            },
            success: function(respond) {
                if(respond.status && batchSerialType != 'batchSerialItem' && batchSerialType != 'normalItem'){
                    $.each(respond.data, function(key, value){
                        if(adjustmentType == "2"){
                            addNewPositiveAdjustmentProductRowByUploadDetails(value);
                        } else if (adjustmentType == "1") {
                            if (batchSerialType == 'batchItem') {
                                if (value.availableQty >= value.qnty) {
                                    addNewNegativeAdjustmentProductRowByUpload(value);
                                }
                            } else {
                                addNewNegativeAdjustmentProductRowByUpload(value);
                            }

                        }
                    });

                } else if(respond.status && batchSerialType == 'normalItem'){
                    $.each(respond.data, function(key, value){
                        var sameItemFlag = false;
                        for (var i in items) {
                            if(value.locProduct.pID == items[i].pID){
                                sameItemFlag = true;
                            }
                        }

                        if(sameItemFlag == false && adjustmentType == "2"){
                            addNewPositiveAdjustmentProductRowByUploadDetails(value);
                        } else if (sameItemFlag == false && adjustmentType == "1") {
                            addNewNegativeAdjustmentProductRowByUpload(value);
                        }

                    });

                } else if(respond.status && batchSerialType == 'batchSerialItem'){
                    $.each(respond.data, function(key, value){
                        $.each(value, function(keys, val){
                            if (adjustmentType == "2") {
                                addNewPositiveAdjustmentProductRowByUploadDetails(val);
                            } else {
                                addNewNegativeAdjustmentProductRowByUpload(val);
                            }
                        });
                    });
                }
                $('#adjustmentImportSecondModal').modal('hide');
                if(respond.msg){
                    p_notification('info', respond.msg);
                }
                productHeader = false;
                uploadDocumentFlag = true;
                $('#adjustmentUploadView').attr('disabled',true);
                }
            });
        }
    });

    function validateAdjustmentUploadStepTwo(choices, batchSerialType)
    {
        if(choices.itemCode == undefined){
            p_notification(false, eb.getMessage('ERR_ADJUSTMENT_UPLOAD_ITEM_CODE_SELECT'));
            return false;
        } else if(choices.unitPrice == undefined && batchSerialType != 'serialItem') {
            p_notification(false, eb.getMessage('ERR_ADJUSTMENT_UPLOAD_UNIT_PRICE_SELECT'));
            return false;
        } else if(choices.uomVal == undefined) {
            p_notification(false, eb.getMessage('ERR_ADJUSTMENT_UPLOAD_UOM_SELECT'));
            return false;
        } else if (batchSerialType == 'batchItem' && choices.quantity == undefined){
            p_notification(false, eb.getMessage('ERR_ADJUSTMENT_UPLOAD_QTY_SELECT'));
            return false;
        } else if (batchSerialType == 'normalItem' && choices.quantity == undefined){
            p_notification(false, eb.getMessage('ERR_ADJUSTMENT_UPLOAD_QTY_SELECT'));
            return false;
        } else if(batchSerialType == 'batchItem' && choices.batchCode == undefined){
            p_notification(false, eb.getMessage('ERR_ADJUSTMENT_UPLOAD_BATCH_CODE_SELECT'));
            return false;
        } else if (batchSerialType == 'serialItem' && choices.serialCode == undefined){
            p_notification(false, eb.getMessage('ERR_ADJUSTMENT_UPLOAD_SERIAL_CODE_SELECT'));
            return false;
        } else if (batchSerialType == 'batchSerialItem' && choices.serialCode == undefined){
            p_notification(false, eb.getMessage('ERR_ADJUSTMENT_UPLOAD_SERIAL_CODE_SELECT'));
            return false;
        } else if(batchSerialType == 'batchSerialItem' && choices.batchCode == undefined){
            p_notification(false, eb.getMessage('ERR_ADJUSTMENT_UPLOAD_BATCH_CODE_SELECT'));
            return false;
        } else {
            return true;
        }
    }

    function addNewPositiveAdjustmentProductRowByUploadDetails(data) {
        //uomConversionRate alrady 1. beacuse when upload Grn data UOM already base UOM.
        var uomConversionRate = 1;
        var uplaodedDiscType = null;
        var gD = 0;

        var productUom;
        if(data.batchData != null){
            $.each(data.batchData, function(key, value){
                batchProducts[key] = new batchProduct(value.newBCode, value.newBQty, value.newBMDate, value.newBEDate, value.newBWrnty, value.newBPrice, null);

            });
        }

        if(data.serilaData != null){
            $.each(data.serilaData, function(key, value){
                serialProducts[key] = new serialProduct(value.newSCode, value.newBWrnty, value.newBCode, value.newBEDate, value.newBWrntyType);
            });
        }

        $.each(data.locProduct.uom, function(key, value){
            if(data.uomID != null && data.uomID == value.uomID){
                productUom = data.uomID;
                uomConversionRate = value.uC;
            } else {
                if(value.pUDisplay == '1'){
                    productUom = value.uomID;
                    uomConversionRate = value.uC;
                }
            }

        });
    
        var uomSet = data.locProduct.uom[productUom];
        var selectedUomAbbr = data.uomID;

        var productCode = data.locProduct.pC;
        var iN = data.locProduct.pN;
        var iC = data.locProduct.pID;
        var productType = data.locProduct.pT;
        var proCodeAndName = productCode + ' - ' + iN;
        var uPrice = data.uPrice;
        var adjustmentProductQty = data.qnty;
        var qty = adjustmentProductQty;
        var uomQty = data.qnty;
         if (uPrice < 0) {
            p_notification(false, eb.getMessage('ERR_UNIT_PRICE_NEGATIVE'));
            return false;
        }
        if (qty < 0) {
            p_notification(false, eb.getMessage('ERR_QNTY_NEGATIVE'));
            return false;
        }
        if (productType == 2 && adjustmentProductQty == 0) {
            qty = 1;
        }
        var locationProductID = data.locProduct.lPID;
        var rowCostTotal = setItemRowCost(qty, uPrice);
        var newTrID = 'tr_' + iC;
        var clonedRow = $($('#preSetSample').clone()).attr('id', newTrID).addClass('addedProducts');
        $("input[name='productCode']", clonedRow).val(proCodeAndName);
        $("input[name='qty']", clonedRow).val(adjustmentProductQty).addUom(data.locProduct.uom);
        $("input[name='unitPrice']", clonedRow).val(uPrice).addUomPrice(data.locProduct.uom);
        $(".uomPrice", clonedRow).prop('disabled', true);
        $(".uomqty", clonedRow).prop('disabled', true);
        clonedRow.children('#ttl').html(accounting.formatMoney(rowCostTotal));
        $('#addNewUom', clonedRow).hide();
        clonedRow.removeClass('hidden');
        clonedRow.insertBefore('#preSetSample');
        $('#finaltotal').html(accounting.formatMoney(setItemTotalCost(adjustmentProductQty, uPrice)));
        items[iC] = new positiveAdjustmentProduct(locationProductID, iC, iC, iN, uomQty, uPrice, selectedUomAbbr, selectedUomAbbr, rowCostTotal, batchProducts, serialProducts, productType, uomConversionRate);
        clearAddedNewRow();
        $('[data-id=itemCode]').trigger('click');
    }

    function addNewNegativeAdjustmentProductRowByUpload(data)
    {
         //uomConversionRate alrady 1. beacuse when upload Grn data UOM already base UOM.
        var uomConversionRate = 1;
        var uplaodedDiscType = null;
        var gD = 0;

        var productUom;
        if(data.batchData != null && data.serilaData == null){
            batchTextData = getUploadBatchValues(data.batchData);
        }

        if(data.serilaData != null && data.batchData == null){
            serialData = getUploadSerialValues(data.serilaData);
        }

        if (data.serilaData != null && data.batchData != null) {
            bSSerialData = getUploadBatchSerialValues(data.serilaData);
        }

        $.each(data.locProduct.uom, function(key, value){
            if(data.uomID != null && data.uomID == value.uomID){
                productUom = data.uomID;
                uomConversionRate = value.uC;
            } else {
                if(value.pUDisplay == '1'){
                    productUom = value.uomID;
                    uomConversionRate = value.uC;
                }
            }

        });
    
        var uomSet = data.locProduct.uom[productUom];
        var selectedUomAbbr = productUom;

        var productCode = data.locProduct.pC;
        var item_desc = data.locProduct.pN;
        var iCode = data.locProduct.pID;
        var productType = data.locProduct.pT;
        var proCodeAndName = productCode + ' - ' + item_desc;
        var prc = data.uPrice;
        var adjustmentProductQty = data.qnty;
        var qty = adjustmentProductQty;
        var uomQty = data.qnty;
         if (prc < 0) {
            p_notification(false, eb.getMessage('ERR_UNIT_PRICE_NEGATIVE'));
            return false;
        }
        if (qty < 0) {
            p_notification(false, eb.getMessage('ERR_QNTY_NEGATIVE'));
            return false;
        }
        var locationProductID = data.locProduct.lPID;
        var rowCostTotal = setItemRowCost(qty, prc);

        
        var availableQty = data.availableQty;
        var tot = rowCostTotal;
        
        var ft = accounting.unformat($('#finaltotal').html());
        var uom = selectedUomAbbr;
        var inttot = parseFloat(tot);

        var priceQty = qty;
        if (productType == 2 && qty == 0) {
            priceQty = 1;
        }
        if (typeof locationProductID === "undefined") {
            locationProductID = 0;
        }
        var tbl_row = "<tr class = 'addedProducts' id='tr_" + iCode + "'><td><input value='" + proCodeAndName + "' type='text' readonly='true' name='productCode' class='form-control'\></td>"
                + "<td>"
                + "<div class='input-group'><input value=" + availableQty + " type='text' readonly='true' name='availableQty' id='availableQty' class='form-control'>"
                + "</div>"
                + "</td>"
                + "<td>"
                + "<div class='input-group'><input data-basevalue=" + qty + " value=" + qty + " type='text' readonly='true' name='quantity' class='form-control'>"
                + "</div>"
                + "</td>"
                + "<td class='input-group'><input value=" + prc + " type='text' readonly='true' name='unitPrice' id='unitPrice' class='form-control' align='center'>"
                + "<strong class='input-group-addon'>" + companyCurrencySymbol + "</strong>"
                + "</td>" + "<td id='ttl' class='text-right'>" + accounting.formatMoney(parseFloat(priceQty * prc).toFixed(4)) + "</td>"
                + "<td class='text-center'><button class='deleteAdjustment btn btn-default'><i class='fa fa-trash-o delete' id='" + iCode + "'></i></button></td>";
        tbl_row += "</tr>";
        $("#formRow").append(tbl_row);

        $("#formRow").find('tr').each(function() {
            if (this.id == "tr_" + iCode) {
                $(this).find("input[name='quantity']").addUom(data.locProduct.uom);
                $(this).find("input[name='availableQty']").addUom(data.locProduct.uom);
                $(this).find("input[name='unitPrice']").addUomPrice(data.locProduct.uom);
            }
        });

        var rowTotal = parseFloat(priceQty) * parseFloat(prc);
        var netTotal = parseFloat(ft) + parseFloat(rowTotal);
        $('#finaltotal').html(accounting.formatMoney(parseFloat(netTotal).toFixed(2)));
        items[iCode] = new item(iCode, item_desc, qty, prc, uom, selectedUomAbbr, inttot, JSON.stringify(serialData), JSON.stringify(batchTextData), JSON.stringify(bSBatchData), JSON.stringify(bSSerialData), locationProductID, productType, uomConversionRate);
        $('#itemCode, #unitPrice, #qty').val('');
        clearItemCode();
        $(".available-qty input[id='itemQuantity']").prop('disabled', false);
        $('#total').html('0.00');
        $('#addNewItemRow .uomqty').val('');
        $('#addNewItemRow .uom-select').remove();
        $("#addNewItemRow .uomqty").val('');

        $("#addNewItemRow .uomPrice").val('');
        $("#addNewItemRow .uom-price-select").remove();

        $('.uomLi').remove();
        $('#uomAb').html('');
        selectedProductUom = '';
        $("#add_item").attr('disabled', false);
        $('[data-id=itemCode]').trigger('click');
    }
});
