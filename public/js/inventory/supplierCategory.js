
$(function() {

    $(".category-search").submit(function(e) {
        var searchValue = $("#supplier-category-search-keyword").val();
        if (!searchValue) {
            searchCategory('');
            //if need to show error message please uncommit below line
//            p_notification(false, eb.getMessage('ERR_SUPCAT_VALUE_SEARCH'));
            return false;
        }
        searchCategory(searchValue);
        e.stopPropagation();
        return false;

    });

    $(".category-search").on('click', '.reset', function() {
        searchCategory('');

    });


    var editForm = $("#create-supplier-category-form");
    /**
     * Add new or  update Supplier Category
     */
    $("#create-supplier-category-form").submit(function(e) {
        var form_data = {
            supplierCategoryName: $("[name='supplierCategoryName']", this).val(),
        };

        if (form_data.supplierCategoryName.trim() == "") {
            $("#supplierCategoryName").focus();
            p_notification(false, eb.getMessage('ERR_SUPCAT_CATEGORY_NAME'));
            return false;
        }

        var submitted_btn = $("[type='submit']:visible", $(this));
        var update = submitted_btn.hasClass('update');
        if (update) {
            form_data = $.extend(form_data, {supplierCategoryID: $("[name='supplierCategoryID']", this).val()});
        }
        var URL = (update) ? 'updateSupplierCategory' : 'saveSupplierCategory';

        eb.ajax({
            url: '/supplierAPI/' + URL + '/' + getCurrPage(),
            method: 'post',
            data: form_data,
            success: function(data) {
                if (data.status == true) {
                    $("#supplierCategoryName").val('');
                    $(".category-form-container").removeClass('update');
                    $("#category-list").html(data.html);
                    p_notification(true, data.msg);
                } else {
                    p_notification(false, data.msg);
                }
            }
        });
        e.stopPropagation();
        return false;
    });


    /**
     * edit Supplier category
     */
    $("#category-list").on('click', 'a.edit', function(e) {
        var supplierCategoryID = $(this).parents('tr').data('categoryrow');

        $(".category-form-container").addClass('update');
        $("html, body").animate({scrollTop: $(".category-form-container").offset().top - 80});
        eb.ajax({
            url: '/supplierAPI/getSupplierCategoryName',
            method: 'post',
            data: {supplierCategoryID: supplierCategoryID},
            dataType: 'json',
            success: function(data) {
                if (data.status == true) {
                    var categoryData = data.data;
                    $("[name='supplierCategoryID']", editForm).val(categoryData.supplierCategoryID);
                    $("[name='supplierCategoryName']", editForm).val(categoryData.supplierCategoryName).focus();
                } else {

                }
            }
        });

        e.stopPropagation();
        return false;
    });

    /**
     * Delete Supplier category
     */
    $("#category-list").on('click', 'a.delete', function(e) {
        var supplierCategoryID = $(this).parents('tr').data('categoryrow');

        bootbox.confirm('Are you sure you want to delete this Category?', function(result) {
            if (result === true) {
                eb.ajax({
                    url: '/supplierAPI/deleteSupplierCategory/' + getCurrPage(),
                    method: 'post',
                    data: {supplierCategoryID: supplierCategoryID, },
                    dataType: 'json',
                    success: function(data) {
                        if (data.status == true) {
                            $("#category-list").html(data.html);
                            p_notification(true, data.msg);
                        } else {
                            p_notification(false, data.msg);
                        }
                    }
                });
            }
        });

        e.stopPropagation();
        return false;
    });

    $(".button-set.update button.reset").click(function() {
        $(".category-form-container")
                .removeClass('update')
                .find("[name='supplierCategoryName']")
                .focus();
    });

});

function searchCategory(searchValue) {
    eb.ajax({
        url: '/supplierAPI/searchSupplierCategory',
        method: 'POST',
        data: {supplierCategoryName: searchValue},
        success: function(data) {
            if (data.status == true) {
                $("#category-list").html(data.html);
            } else {
                p_notification(false, data.msg);
            }
        }
    });
}
;
