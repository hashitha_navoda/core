/**
 * @author Damith Thamara <damith@thinkcube.com>
 * This js contains Supplier related functions
 */
var updateSupplierID;
var insertedSupplierID;
var cFields = new Array();
var subCategory = {};
var subTitle = {};
$(document).ready(function() {
    var fileData = $('#fileData').val();
    var header = $('#fileHeader').val();

	loadDropDownFromDatabase('/accounts-api/search-active-accounts-for-dropdown', "", "", '#supplierPayableAccountID');
	loadDropDownFromDatabase('/accounts-api/search-active-accounts-for-dropdown', "", "", '#supplierPurchaseDiscountAccountID');
	loadDropDownFromDatabase('/accounts-api/search-active-accounts-for-dropdown', "", "", '#supplierGrnClearingAccountID');
	loadDropDownFromDatabase('/accounts-api/search-active-accounts-for-dropdown', "", "", '#supplierAdvancePaymentAccountID');

	var supplierPayableAccountID = $('#supplierPayableAccountID').data('id');
    if(supplierPayableAccountID!=''){
    	var supplierPayableAccountName = $('#supplierPayableAccountID').data('value');
    	$('#supplierPayableAccountID').append("<option value='"+supplierPayableAccountID+"'>"+supplierPayableAccountName+"</option>")
    	$('#supplierPayableAccountID').val(supplierPayableAccountID).selectpicker('refresh');
    }

    var supplierPurchaseDiscountAccountID = $('#supplierPurchaseDiscountAccountID').data('id');
    if(supplierPurchaseDiscountAccountID!=''){
    	var supplierPurchaseDiscountAccountName = $('#supplierPurchaseDiscountAccountID').data('value');
    	$('#supplierPurchaseDiscountAccountID').append("<option value='"+supplierPurchaseDiscountAccountID+"'>"+supplierPurchaseDiscountAccountName+"</option>")
    	$('#supplierPurchaseDiscountAccountID').val(supplierPurchaseDiscountAccountID).selectpicker('refresh');
    }

    var supplierGrnClearingAccountID = $('#supplierGrnClearingAccountID').data('id');
    if(supplierGrnClearingAccountID!=''){
    	var supplierGrnClearingAccountName = $('#supplierGrnClearingAccountID').data('value');
    	$('#supplierGrnClearingAccountID').append("<option value='"+supplierGrnClearingAccountID+"'>"+supplierGrnClearingAccountName+"</option>")
    	$('#supplierGrnClearingAccountID').val(supplierGrnClearingAccountID).selectpicker('refresh');
    }

    var supplierAdvancePaymentAccountID = $('#supplierAdvancePaymentAccountID').data('id');
    if(supplierAdvancePaymentAccountID!=''){
    	var supplierAdvancePaymentAccountName = $('#supplierAdvancePaymentAccountID').data('value');
    	$('#supplierAdvancePaymentAccountID').append("<option value='"+supplierAdvancePaymentAccountID+"'>"+supplierAdvancePaymentAccountName+"</option>")
    	$('#supplierAdvancePaymentAccountID').val(supplierAdvancePaymentAccountID).selectpicker('refresh');
    }

    $('.selectpicker').selectpicker();
    $('#clearbutton').on('click', function() {
        setTimeout(function() {
            $('.selectpicker').selectpicker('render');
        }, 10);
    });

    $('#moredetails').on('click', function() {
        $(this).hide();
        $('#supplier_more').removeClass('hidden');
    });

    $('#supplierimport_more').hide();
    $('form.supplier-search-form').submit(function(e) {
        e.preventDefault();
        var param = {searchSupplierString: $('#supplierSearch').val()};
        getViewAndLoad('/supplierAPI/getSuppliersForSearch', 'supplierList', param);
    });

    $('form.supplier-search-form button.supplier-reset').on('click', function(e) {
        $("#supplierSearch").val('');
        $("form.supplier-search-form").submit();
    });

    $('#showSupplierList').on('click', function() {
        var param = {};
        getViewAndLoad('/supplierAPI/getSupplierListView', 'supplierList', param);
        $('#supplierList').removeClass('hidden');
    });

    $('#supplierSave,#updateSupplier').on('click', function(e) {
        e.preventDefault();
        v = new Array(
                $('#supplierName').val(),
                $('#supplierCode').val(),
                $('#supplierEmail').val(),
                $('#supplierVatNumber').val(),
                $('#supplierCreditLimit').val(),
                $('#supplierOutstandingBalance').val(),
                $('#supplierTelephoneNumber').val(),
                $('#supplierCreditBalance').val()
                );
        if (validateinput(v)) {
            if (this.id == 'supplierSave') {
                var supplierFormParam = $('#Supplier').serialize();
                eb.ajax({
                    type: 'POST',
                    url: BASE_URL + '/supplierAPI/saveSupplier',
                    data: supplierFormParam,
                    success: function(respond) {
                        p_notification(respond.status, respond.msg);
                        if (respond.status == true) {
                            $('#Supplier')[0].reset();
                            $('.selectpicker').selectpicker('render');
                            if ($('#addSupplierModalClose').length) {
                                setAddedSupplierToTheList(respond.data);
                            }else{
                            	window.location.reload();
                            }
                        }
                    },
                    async: false
                });
            } else {
                var supplierFormParam = $('#Supplier').serialize();
                supplierFormParam = supplierFormParam + '&supplierID=' + updateSupplierID;
                eb.ajax({
                    type: 'POST',
                    url: BASE_URL + '/supplierAPI/updateSupplier',
                    data: supplierFormParam,
                    success: function(respond) {
                        p_notification(respond.status, respond.msg);
                        if (respond.status == true) {
                            $('#supplierList').html(respond.html);
                        }
                    },
                    async: false
                });
            }
            if ($('#edit_supplier').length) {
                $('#edit_supplier').modal('hide');
            }
        }
    });

     //supplier_import show_more_details button
    var importmoreflag = 0;
    $("#moredetailsimport").click(function() {
        if (importmoreflag == 0) {
            $("#supplierimport_more").slideDown(function() {
                $("#moredetailsimport").html('Hide details ' + '<span class="glyphicon glyphicon-circle-arrow-up"></span>');
            });
            importmoreflag = 1;
        } else {
            $("#supplierimport_more").slideUp(function() {
                $('#moredetailsimport').html('Show more details ' + '<span class="glyphicon glyphicon-circle-arrow-down"></span>');
            });
            importmoreflag = 0;
        }
    });

    columns = $('#importcolumncount').html();
    for (var k = 0; k < columns; k++) {
        cFields[k] = 'Unmapped';
    }


    var $importmodal = $('#supplier-import-form-div');
    $importmodal.on('hide.bs.modal', function() {
        $importmodal.removeData();
        return true;
    });
    $('#importdatabutton').on('click', function() {
        var sNameFlag = 0;
        var sCodeFlag = 0;
        var ctitleFlag = 0;
        var sCategoryFlag = 0;
        for (var t = 0; t < columns; t++) {
            if (cFields[t] == 'Supplier Name') {
                sNameFlag = 1;
            } else if (cFields[t] == 'Supplier Code') {
                sCodeFlag = 1;
            } else if (cFields[t] == 'Supplier Category') {
                sCategoryFlag = 1;
            }
        }
        
        if (sNameFlag == 1 && sCodeFlag == 1) {
            if (sCategoryFlag == 1) {
                if (Object.keys(subCategory).length > 0) {
                    var importmodalurl = BASE_URL + '/supplierAPI/getImportConfirm';
                    $importmodal.load(importmodalurl, '', function() {
                        $importmodal.modal('show');
                        $('#import-button').on('click', function() {
                            var radiovalue = $('input:radio[name=importmethod]:checked').val();
                            if (radiovalue == 'discard') {
                                supplierImport('discard');
                            }
                            else if (radiovalue == 'replace') {
                                supplierImport('replace');
                            } else {
                                p_notification(false, eb.getMessage('ERR_CUST_IMPORT'));
                            }
                        });
                    });    
                } else {
                    p_notification(false, eb.getMessage('ERR_SUP_CAT_SUB'));
                }
            } else {
                var importmodalurl = BASE_URL + '/supplierAPI/getImportConfirm';
                $importmodal.load(importmodalurl, '', function() {
                    $importmodal.modal('show');
                    $('#import-button').on('click', function() {
                        var radiovalue = $('input:radio[name=importmethod]:checked').val();
                        if (radiovalue == 'discard') {
                            supplierImport('discard');
                        }
                        else if (radiovalue == 'replace') {
                            supplierImport('replace');
                        } else {
                            p_notification(false, eb.getMessage('ERR_CUST_IMPORT'));
                        }
                    });
                });
            }
        } else {
            p_notification(false, eb.getMessage('ERR_SUP_DETAILS'));
        }
    });
    function supplierImport(method) {
        var importurl = BASE_URL + '/supplierAPI/importSupplier';
        var importrequest = eb.post(importurl, {
            method: method,
            header: $('#importheader').html(),
            delim: $('#importdelim').html(),
            columncount: $('#importcolumncount').html(),
            col: cFields,
            subCategory: subCategory,
            subTitle: subTitle,
        });
        importrequest.done(function(retdata) {
            p_notification(retdata.status, retdata.msg);
            if (retdata.status) {
                window.setTimeout(function() {
                    window.location.assign(BASE_URL + '/supplier/import');
                }, 1500);
            }

        });
    }

    $('.supplierimportselector').on('change', function(event, modalEnable) {
        var selectid = this.id;
        var selectedoption = $(this).val();
        enableoptions(cFields[selectid]);
        cFields[selectid] = $(this).val();
        var $tr = $(this).closest('tr');
        var column = $tr.data('column');
        var enumModalStatus = (modalEnable === undefined) ? true : modalEnable;        
        var html = '';
        
        if (selectedoption != 'Unmapped') {
            disableoptions(selectedoption);
        }

        if (selectedoption == "Supplier Category") {
            generateEnumeratorModal( column, 'supplierCategory', fileData, header, false, enumModalStatus, {}, function (res){
                if(res) {
                    $tr.find('.enum-modal-btn').removeClass('hidden');
                    $tr.find('.enum-modal-btn').addClass('supCat');
                }
            });
        } else if (selectedoption == "Supplier Title"){
            generateEnumeratorModal( column, 'supplierTitle', fileData, header, false, enumModalStatus, {}, function (res){
                if(res) {
                    $tr.find('.enum-modal-btn').removeClass('hidden');
                    $tr.find('.enum-modal-btn').addClass('supTitle');
                }
            });
        } else {
            $tr.find('.enum-modal-btn').addClass('hidden');
        }

        selectedFieldArr = [];
        $('.supplierimportselector').each(function(index,element){
            var selectedValue = $(this).val();
            if(selectedValue && $.inArray(selectedValue,selectedFieldArr) == -1){
                selectedFieldArr.push(selectedValue);
            }
        });
    });

    //for save sub set values
    $('#enumeratorModal').on( 'click', '.enum-save-btn', function(){
        var subSet = {}; 
        var field = $(this).data('field');
        $('#enumeratorModal .enum-tr').each(function( index, element){
            var $tr = $(this);            
            var key = $tr.data('id');
            var value = $tr.find('.enumSelector').val();
            
            if(value == 0){//for new sub set
                value = 'new_category-'+key;
            }
            subSet[key] = value;
        });
        
        if (field == 'supplierCategory') {
            subCategory = subSet;
        } else if (field == 'supplierTitle') {
            subTitle = subSet;
        }
        $('#enumeratorModal').modal('hide');
    });

    //for toggle enum modal 
    $('.enum-modal-btn').on( 'click', function (){
        var $tr = $(this).closest('tr');
        var columnId = $tr.data('column');
        var field;
        var subSet;

        if ($(this).hasClass("supCat")) {
            field = 'supplierCategory'
            var subCategory = {};
            subSet = subCategory;
        } else if ($(this).hasClass("supTitle")) {
            field = 'supplierTitle'
            var subTitle = {};
            subSet = subTitle;
        }
        generateEnumeratorModal( columnId, field, fileData, header, true, true, subSet, function(){});
        
        $('#enumeratorModal').modal('show');
    });
});

function validateinput(inputs) {
    var sname = inputs[0];
    var scode = inputs[1];
    var semail = inputs[2];
    var svatNumber = inputs[3];
    var sCreditLimit = inputs[4];
    var sOutstandingBalance = inputs[5];
    var stelephoneNumber = inputs[6];
    var sCreditBalance = inputs[7];

    var atpos = semail.indexOf("@");
    var dotpos = semail.lastIndexOf(".");
    var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    var shortNameregex = /^[a-zA-Z]+[a-zA-Z0-9]+/;
    var nameregex = /\d/g;
    var isEmail = true;
    if (semail == '') {
        isEmail = false;
    }
    isPhone = false;
    if (stelephoneNumber.length != 0) {
        if (stelephoneNumber.length < 9 || stelephoneNumber.length > 15) {
            isPhone = true;
        }
    }
    if (sname == null || sname == "") {
        p_notification(false, eb.getMessage('ERR_SUP_NAME_BLANK'));
        $('#supplierName').focus();
    } else if (scode == null || scode == "") {
        p_notification(false, eb.getMessage('ERR_SUP_CODE_BLANK'));
        $('#supplierCode').focus();
    } else if (isNaN(sCreditLimit)) {
        p_notification(false, eb.getMessage('ERR_CUST_CRDTLIMIT_NUMR'));
    } else if (sCreditLimit < 0) {
        p_notification(false, eb.getMessage('ERR_CUST_CRDTLIMIT_NEG'));
    } else if (isNaN(sOutstandingBalance)) {
        p_notification(false, eb.getMessage('ERR_CUST_OUSTAND_BAL_NUMR'));
    } else if (sOutstandingBalance < 0) {
        p_notification(false, eb.getMessage('ERR_CUST_OUSTAND_BAL_NEG'));
    } else if (isNaN(sCreditBalance)) {
        p_notification(false, eb.getMessage('ERR_CUST_CRDTBAL_NUMR'));
    } else if (sCreditBalance < 0) {
        p_notification(false, eb.getMessage('ERR_CUST_CRDTBAL_NEG'));
    } else if (isNaN(stelephoneNumber) || isPhone) {
        p_notification(false, eb.getMessage('ERR_CUST_TPNUM_VALIDITY'));
    } else if (isEmail && !regex.test(semail)) {
        p_notification(false, eb.getMessage('ERR_CUST_VALID_EMAIL'));
    } else {
        return true;
    }
}

function editSupplier(editID) {
    updateSupplierID = editID;
    $('#Supplier')[0].reset();
    eb.ajax({
        type: 'POST',
        url: BASE_URL + '/supplierAPI/getSupplierDetails',
        data: {sID: editID},
        success: function(respond) {
            supplierData = respond.data;
            
            if (supplierData != null) {
                $('#supplierTitle').val(supplierData.sNT);
                $('#supplierName').val(supplierData.sN);
                $('#supplierCode').val(supplierData.sC);
                $('#supplierCategory').selectpicker('val', supplierData.sCA);
                $('.selectpicker').selectpicker('render');
                $('#supplierCurrency').val(supplierData.sCU);
                $('#supplierTelephoneNumber').val(supplierData.sT);
                $('#supplierEmail').val(supplierData.sE);
                $('#supplierAddress').val(supplierData.sA);
                $('#supplierVatNumber').val(supplierData.sV);
                $('#supplierCreditLimit').val((supplierData.sCL == null) ? '0.00' : parseFloat(supplierData.sCL).toFixed(2));
                $('#supplierPaymentTerm').val(supplierData.sP);
                $('#supplierOutstandingBalance').val(parseFloat(supplierData.sOB).toFixed(2));
                $('#supplierCreditBalance').val(parseFloat(supplierData.sCB).toFixed(2));
                $('#supplierOther').val(supplierData.sO);

                if(supplierData.sPAID != ''){
                	$('#supplierPayableAccountID').append("<option value='"+supplierData.sPAID+"'>"+supplierData.sPAN+"</option>");
                	$('#supplierPayableAccountID').val(supplierData.sPAID).selectpicker('refresh');
                }

                if(supplierData.sPDAID != ''){
                	$('#supplierPurchaseDiscountAccountID').append("<option value='"+supplierData.sPDAID+"'>"+supplierData.sPDAN+"</option>");
                	$('#supplierPurchaseDiscountAccountID').val(supplierData.sPDAID).selectpicker('refresh');
                }

                if(supplierData.sGCAID != ''){
                	$('#supplierGrnClearingAccountID').append("<option value='"+supplierData.sGCAID+"'>"+supplierData.sGCAN+"</option>")
                	$('#supplierGrnClearingAccountID').val(supplierData.sGCAID).selectpicker('refresh');
                }

                if(supplierData.sAPAID != ''){
                	$('#supplierAdvancePaymentAccountID').append("<option value='"+supplierData.sAPAID+"'>"+supplierData.sAPAN+"</option>")
                	$('#supplierAdvancePaymentAccountID').val(supplierData.sAPAID).selectpicker('refresh');
                }
            }
        },
        async: false
    });
    $('#edit_supplier').modal('show');
}

function deleteSupplier(deleteID, supplierName) {
    bootbox.confirm("Are you sure you want to delete this Supplier '" + supplierName + "' ?", function(result) {
        if (result == true) {
            eb.ajax({
                type: 'POST',
                url: BASE_URL + '/supplierAPI/deleteSupplier',
                data: {sID: deleteID},
                success: function(respond) {
                    p_notification(respond.status, respond.msg);
                    window.setTimeout(function() {
                        location.reload();
                    }, 2000);
                }
            });
        }
    });
}

function enableoptions(selectedenableoption) {
    for (var j = 0; j < columns; j++) {
        $("#" + j + ' option[value="' + selectedenableoption + '"]').removeAttr('disabled');
    }
}

function disableoptions(selectedoption) {
    for (var i = 0; i < columns; i++) {
        //        $("#" + i + " option:contains(" + selectedoption + ")").attr('disabled', 'disabled');
        $("#" + i + ' option[value="' + selectedoption + '"]').attr('disabled', 'disabled');
    }
}

function generateEnumeratorModal( columnId, field, fileData, header, isUpdate, enumModalStatus, obj, callback){
    $.ajax({
        type: 'POST',
        url: BASE_URL + '/supplier/enumerate-modal',
        data: {columnId: columnId, field: field, fileData: fileData, header: header},
        success: function(data) {
            if (data.status == true) {
                if(parseInt(data.data) === 0){
                    p_notification(false, eb.getMessage('ERR_UPLOADED_FILE_EMPTY'));
                    return;
                }
                $('#enumeratorModalBody').html(data.html);
                if(isUpdate){
                    $.each(obj, function( key, value){
                        if(field === 'supplierCategory' && $.inArray(value, categoryIdArr) === -1){
                            value = 0;
                        }
                        if(field === 'supplierTitle' && $.inArray(value, categoryIdArr) === -1){
                            value = 0;
                        }
                        $('#enumeratorModal tr[data-id="'+key+'"]').find('.enumSelector').val(value);
                    });                   
                }
                $('#enumeratorModal').find('.enum-save-btn').data('field',field);
                if(enumModalStatus){
                    $('#enumeratorModal').modal('show');
                }                
                callback(true);                
            } else {
                p_notification(false, data.msg);
                callback(false);
            }
        }
    });
}