var colorPalettex = ['#3023AE','#F76B1C','#1298FF', '#FF4560', '#775DD0','#6023ea','#ea2385'];
$(document).ready(function() {
if (!$('.main_body').hasClass('hide-side-bar')) {
    $('.sidbar-toggle-menu').trigger('click');
}
var stockAgedDetails;
var heigstSalesItemData;
var currentPeriod;
var currencySymbol;
var expenseGlobalData = [];
var incomeGlobalData = [];
var chequeDetailsSummaryData;
    currentPeriod = 'thisWeek';
    // getWidgetData('thisWeek');
    getStockValuesData();
    updateDashboardData('thisWeek')
    getSalesGrossProfitData('thisWeek');


    function getWidgetData(period){
        eb.ajax({
            type: 'POST',
            url: BASE_URL + '/inventory-dashboard-api/getWidgetData',
            data: {period: period},
            success: function(data) {
                $('#target-chart').html('');
                $('#cash-chart').html('');
                $('#gp-chart').html('');
                $('#total-sales').each(function () {
                    $(this).prop('Counter',0).animate({
                        Counter: data.totalStockValue
                    }, {
                        duration: 1000,
                        easing: 'swing',
                        step: function (now) {
                            $(this).text('LKR '+accounting.formatMoney(now));
                        }
                    });
                });
                $('#gross-profit').each(function () {
                    $(this).prop('Counter',0).animate({
                        Counter: data.totalMinInventoryItems
                    }, {
                        duration: 1000,
                        easing: 'swing',
                        step: function (now) {
                            $(this).text(Math.ceil(now));
                        }
                    });
                });
                $('#total-payments').each(function () {
                    $(this).prop('Counter',0).animate({
                        Counter: data.reOrderReachedItems
                    }, {
                        duration: 1000,
                        easing: 'swing',
                        step: function (now) {
                            $(this).text(Math.ceil(now));
                        }
                    });
                });
                $('#totalInvoiceCount').each(function () {
                    $(this).prop('Counter',0).animate({
                        Counter: data.totalActiveItems
                    }, {
                        duration: 1000,
                        easing: 'swing',
                        step: function (now) {
                            $(this).text(Math.ceil(now));
                        }
                    });
                });

                salesSummaryChart(data.totalStockValue, data.totalStockSellingValue);
                stockAgedDetails = data.stockAgedDetails;
                cashCreditChart(data.stockAgedDetails);

                if (data.bestMovingItems.length > 0) {
                    $.each(data.bestMovingItems, function(index, val) {
                        $(".bestMovingItems").append("<div class='row item-row moving-item'><div class='col-lg-1 table-text'>"+(parseFloat(index)+1)+"</div><div class='col-lg-9 table-text p-text' style='width: 100%;'>"+val.productName+"</div><div class='col-lg-2 table-text text-right'>"+val.movingCount+"</div></div>");
                    });
                } else {
                    $(".moving-item").remove();
                }


                
            }
        });
    }

    function getSalesGrossProfitData(period)
    {
        eb.ajax({
            type: 'POST',
            url: BASE_URL + '/inventory-dashboard-api/getSalesGrossProfitData',
            data: {period: period},
            success: function(data) {
                heigstSalesItemData = data.heigstSalesItemData;
            }
        });
    }

    function updateDashboardData(period) {
        updateChartData(period, function() {
            updateFooterChartData(period, function () {

            });
        });
    }

    function updateFooterChartData(period, callback) { 
        eb.ajax({
            type: 'POST',
            url: BASE_URL + '/inventory-dashboard-api/updateFooterChartData',
            data: {period: period},
            success: function(data) {
                
                if (data.bestMovingItems.length > 0) {
                    $.each(data.bestMovingItems, function(index, val) {
                        $(".bestMovingItems").append("<div class='row item-row moving-item'><div class='col-lg-1 table-text'>"+(parseFloat(index)+1)+"</div><div class='col-lg-9 table-text p-text' style='width: 100%;'>"+val.productName+"</div><div class='col-lg-2 table-text text-right'>"+val.movingCount+"</div></div>");
                    });
                } else {
                    $(".moving-item").remove();
                }
            }
        });
        callback();

    }
    
    function updateChartData(period, callback) { 
        eb.ajax({
            type: 'POST',
            url: BASE_URL + '/inventory-dashboard-api/updateChartData',
            data: {period: period},
            success: function(data) {
                currencySymbol = data.companyCurrencySymbol;
                $('#cash-chart').html('');
                stockAgedDetails = data.stockAgedDetails;
                cashCreditChart(data.stockAgedDetails);
            }
        });
        callback();

    }

    function getStockValuesData(period, callback) {
        eb.ajax({
            type: 'POST',
            url: BASE_URL + '/inventory-dashboard-api/getStockValuesData',
            data: {},
            success: function(data) {
                currencySymbol = data.companyCurrencySymbol;
                $('#total-sales').each(function () {
                    $(this).prop('Counter',0).animate({
                        Counter: data.totalStockValue
                    }, {
                        duration: 1000,
                        easing: 'swing',
                        step: function (now) {
                            $(this).text(currencySymbol+' '+accounting.formatMoney(now));
                        }
                    });
                });
               
                $('#totalInvoiceCount').each(function () {
                    $(this).prop('Counter',0).animate({
                        Counter: data.totalActiveItems
                    }, {
                        duration: 1000,
                        easing: 'swing',
                        step: function (now) {
                            $(this).text(Math.ceil(now));
                        }
                    });
                });

                salesSummaryChart(data.totalStockValue, data.totalStockSellingValue);
                            
            }
        }).then(function () {
            getReorderItemData();
        });
        // callback();
    }

    function getReorderItemData (period) {
        eb.ajax({
            type: 'POST',
            url: BASE_URL + '/inventory-dashboard-api/getReorderItemData',
            data: {},
            success: function(data) {
                currencySymbol = data.companyCurrencySymbol;
                $('#total-payments').each(function () {
                    $(this).prop('Counter',0).animate({
                        Counter: data.reOrderReachedItems
                    }, {
                        duration: 1000,
                        easing: 'swing',
                        step: function (now) {
                            $(this).text(Math.ceil(now));
                        }
                    });
                });
            }
        }).then(function () {
            getMinInventoryItemData(period);
        });
    }

    function getMinInventoryItemData (period) {
        eb.ajax({
            type: 'POST',
            url: BASE_URL + '/inventory-dashboard-api/getMinInventoryItemData',
            data: {},
            success: function(data) {
                currencySymbol = data.companyCurrencySymbol;
                $('#gross-profit').each(function () {
                    $(this).prop('Counter',0).animate({
                        Counter: data.totalMinInventoryItems
                    }, {
                        duration: 1000,
                        easing: 'swing',
                        step: function (now) {
                            $(this).text(Math.ceil(now));
                        }
                    });
                });
            }
        });
    }


    $('.period').on('change', function(event) {
        event.preventDefault();
        currentPeriod = $(this).val();
        // getWidgetData($(this).val());
        updateDashboardData($(this).val());
        getSalesGrossProfitData($(this).val());
    });


    function gpChart(endAngle)
    {
        var options1 = {
            chart: {
                height: 200,
                type: 'radialBar',
                toolbar: {
                    show: false
                }
            },
            plotOptions: {
                radialBar: {
                    startAngle: 0,
                    endAngle: parseFloat(endAngle),
                    hollow: {
                        margin: 0,
                        size: '70%',
                        background: '#fff',
                        image: undefined,
                        imageOffsetX: 0,
                        imageOffsetY: 0,
                        position: 'front',
                        dropShadow: {
                            enabled: true,
                            top: 3,
                            left: 0,
                            blur: 4,
                            opacity: 0.24
                        }
                    },
                    track: {
                        background: '#fff',
                        strokeWidth: '70%',
                        margin: 0, // margin is in pixels
                        dropShadow: {
                            enabled: true,
                            top: -3,
                            left: 0,
                            blur: 4,
                            opacity: 0.35
                        }
                    },

                    dataLabels: {
                        showOn: 'always',
                        name: {
                            offsetY: 0,
                            show: true,
                            color: '#888',
                            fontSize: '15px'
                        },
                        value: {
                            formatter: function(val) {
                                return parseInt(val);
                            },
                            color: '#111',
                            fontSize: '20px',
                            show: false,
                        }
                    }
                }
            },
            fill: {
                type: 'gradient',
                gradient: {
                    shade: 'dark',
                    type: 'horizontal',
                    shadeIntensity: 0.5,
                    gradientToColors: ['#ABE5A1'],
                    inverseColors: true,
                    opacityFrom: 1,
                    opacityTo: 1,
                    stops: [0, 100]
                }
            },
            series: [75],
            stroke: {
                lineCap: 'round'
            },
            labels: ['Gross Profit'],
        }
        var chart1 = new ApexCharts(
            document.querySelector("#target-chart"),
            options1
        );
        chart1.render();
    }

    $('#sales-gross-profit').on('click', function(event) {
        event.preventDefault();
        $('#gp-chart').removeClass('hidden');
        $('#cash-chart').addClass('hidden');
        $('#turnover-chart').addClass('hidden');
        if ($('#gp-chart')[0].children.length > 0) {
            $('#gp-chart')[0].children[0].remove();
        }
        salsVsGpChart(heigstSalesItemData);
    });

    function salsVsGpChart(heigstSalesItemData)
    {
        var totalSales = [];
        var totalProfit = [];
        var products = [];
        $.each(heigstSalesItemData, function(index, val) {
            if (index < 10) {
                totalSales.push(val.totalSales);
                var profit = (val.profit == undefined) ? 0 : val.profit;
                totalProfit.push(profit);
                products.push(val.productName);
            }
        });

        var options = {
            chart: {
                height: 350,
                type: 'area',
            },
            dataLabels: {
                enabled: false
            },
            stroke: {
                curve: 'smooth'
            },
            series: [{
                name: 'Total Sales',
                data: totalSales
            }, {
                name: 'Total Profit',
                data: totalProfit
            }],

            xaxis: {
                type: 'category',
                categories: products,                
            },
            yaxis: {
                labels: {
                    formatter: (value) => { return "Rs "+accounting.formatMoney(value) },
                }
            },
            tooltip: {
                x: {
                    format: 'dd/MM/yy HH:mm'
                },
                y: {
                    formatter: function(value, { series, seriesIndex, dataPointIndex, w }) {
                        return "Rs "+accounting.formatMoney(value)
                    }
                }
            }
        }

        var chart = new ApexCharts(
            document.querySelector("#gp-chart"),
            options
        );

        chart.render();
    }



    $('#cash-credit').on('click', function(event) {
        event.preventDefault();
        $('#cash-chart').removeClass('hidden');
        $('#turnover-chart').addClass('hidden');
        $('#gp-chart').addClass('hidden');
        if ($('#cash-chart')[0].children.length > 0) {
            $('#cash-chart')[0].children[0].remove();
        }
        $('#gp-chart').html('');
        cashCreditChart(stockAgedDetails);
    });    

     $('#average-stock-duration').on('click', function(event) {
        event.preventDefault();
        $('#turnover-chart').removeClass('hidden');
        $('#gp-chart').addClass('hidden');
        $('#cash-chart').addClass('hidden');
        if ($('#turnover-chart')[0].children.length > 0) {
            $('#turnover-chart')[0].children[0].remove();
        }
        cashCreditChart(stockAgedDetails);
    });    

    function cashCreditChart(stockAgedDetails)
    {
        $('#cash-chart').html('');

        var stockVAlue = [];
        var period = [];
        $.each(stockAgedDetails, function(index, val) {
            stockVAlue.push(val);
            period.push(index+' days');
        });;

        var options = {
            chart: {
                height: 350,
                type: 'bar',
            },
            plotOptions: {
                bar: {
                    columnWidth: '50%',   
                }
            },
            dataLabels: {
                enabled: false
            },
            stroke: {
                width: 2
            },
            series: [{
                name: 'Stock Value',
                data: stockVAlue
            }],
            grid: {
                row: {
                    colors: ['#fff', '#f2f2f2']
                }
            },
            xaxis: {
                title: {
                    text: 'Aging Period',
                },
                labels: {
                    rotate: -45
                },
                categories: period,
            },
            yaxis: {
                title: {
                    text: 'Stock Value',
                },
                labels: {
                    formatter: (value) => { return currencySymbol+" "+accounting.formatMoney(value) },
                }
            },
            tooltip: {
                x: {
                    format: 'dd/MM/yy HH:mm'
                },
                y: {
                    formatter: function(value, { series, seriesIndex, dataPointIndex, w }) {
                        return currencySymbol+" "+accounting.formatMoney(value)
                    }
                }
            },
            fill: {
                type: 'gradient',
                gradient: {
                    shade: 'light',
                    type: "horizontal",
                    shadeIntensity: 0.25,
                    gradientToColors: undefined,
                    inverseColors: true,
                    opacityFrom: 0.85,
                    opacityTo: 0.85,
                    stops: [50, 0, 100]
                },
            },
        }

        var chart = new ApexCharts(
            document.querySelector("#cash-chart"),
            options
        );

        chart.render();
    }


    function salesSummaryChart(totalStockValue, totalStockSellingValue)
    {
        var predictedRevenue = parseFloat(totalStockValue) + parseFloat(totalStockSellingValue);
        let chartConfig = {
            graphset : [

            ]
        };

        var gdata = {
            x : 0*200,
            y : 0*200,
            width : '100%',
            height : '100%',
            type : 'pie',
            noData:{
                text:"Currently there is no data in the chart",
                backgroundColor: "#5081e7",
                fontSize:18,
                textAlpha:.9,
                alpha:.6,
                bold:true
            },
            plotarea : {
                margin : '15 5 5 5'
            },
            "scale":{
                "size-factor":0.9
            },
            labels:[
                {
                    text:currencySymbol+" "+accounting.formatMoney(predictedRevenue),
                    x:"50%", 
                    y:"47%",
                    anchor : 'c',
                    fontSize:"17px",
                    fontStyle : 'bold',
                    fontColor : 'black',
                    padding : "10px",
                },
                {
                    text:"Sale <br> Value",
                    x:"50%", 
                    y:"58%",
                    fontSize:"13px",
                    anchor : 'c',
                    fontStyle : 'bold',
                    fontColor : 'grey',
                    padding : "10px",
                },
            ],
            plot : {
                borderWidth : 2,
                borderColor : 'white',
                shadow : true,
                shadowAlpha : 1,
                shadowAngle : -45,
                borderAlpha : 0.5,
                slice : '89%',
                pieTransform : 'flow=4',
                hoverState : {
                    visible : false    
                },
                tooltip : {
                    text : '%t <br> '+currencySymbol+' %node-value',
                    thousandsSeparator:','
                },
                'background-color': "black",
                'value-box': {
                    text: "%t <br> "+currencySymbol+" %v",
                    'font-color': "black",
                    'font-size': 10,
                    'background-color': "white",
                    borderColor : 'white',
                    borderRadius : '5px',
                    shadow : true,
                    shadowAlpha : 1,
                    shadowAngle : -45,
                    borderAlpha : 0.5,
                    'border-width': 1,
                    padding: "5%",
                    thousandsSeparator:',',
                }
            },
            series:[  
            ]
        };

        
        if (parseFloat(totalStockValue) > 0) {
            var series = {
                "values":[parseFloat(totalStockValue)],  
                "text":"Stock Value",  
                "background-color":'#3023AE'
            }
            gdata.series.push(series);
        }

        if (parseFloat(totalStockSellingValue) > 0) {
            var series = {
                "values":[parseFloat(totalStockSellingValue)],  
                "text":"GP",  
                "background-color":'#F76B1C'
            }
            gdata.series.push(series);
        }

        chartConfig.graphset.push(gdata);

        zingchart.render({
            id: 'sales-summary',
            width: '100%',
            height: '100%',
            data: chartConfig
        });



        if (predictedRevenue == 0) {
            $('#sales-summary .zc-text').remove();
        }
    }

});


