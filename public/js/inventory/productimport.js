var pp = new Array();
var allSaveProduct = new Array();
var replaceProduct = new Array();
var productcolumns;
$(document).ready(function() {
    $('#step2').hide();
    $('#product-import-more').hide();
    $("#product-moredetails-click").click(function() {
        $("#product-import-more").slideToggle(function() {
//$('#product-moredetails-click').slideDown();
            if ($('#product-import-more').is(':visible') == true) {
                $('#product-moredetails-click').html('Hide details' + ' <span class="glyphicon glyphicon-circle-arrow-up"></span>');
            } else {
                $('#product-moredetails-click').html('Show more details' + ' <span class="glyphicon glyphicon-circle-arrow-down"></span>');
            }

        });
    });
    productcolumns = $('#productimportcolumncount').html();
    for (var k = 0; k < productcolumns; k++) {
        pp[k] = 'Unmapped';
    }

    $('.productimportselector').on('change', function() {
        var selectid = this.id;
        var selectedoption = $(this).val();
        enableoptions(pp[selectid]);
        pp[selectid] = $(this).val();
        if (selectedoption != 'Unmapped') {
            disableoptions(selectedoption);
        }
    });
    var $productimportmodal = $('#Product-import-form-div');
    $productimportmodal.on('hide.bs.modal', function() {
        $productimportmodal.removeData();
        return true;
    });
    $('#product-importdata-button').on('click', function() {

        var pcodeFlag = 0;
        var pnameFlag = 0;
        for (var t = 0; t < productcolumns; t++) {

            if (pp[t] == 'Item Code') {
                pcodeFlag = 1;
            } else if (pp[t] == 'Item Name') {
                pnameFlag = 1;
            }
        }
        if (pcodeFlag == 1 && pnameFlag == 1) {
            productimport();
        } else {
            p_notification(false, eb.getMessage('ERR_PROIMPORT_SELECODENAME'));
        }
    });
    $('#product-importcancel-button').on("click", function() {
        window.history.back(-1);
    });

    $(document).on('click', '.location,.global', function() {
        var id = $(this).parents('tr').data('productid');
        if ($(this).hasClass('location')) {
            $('#locationHide' + id).removeClass('hide');
        } else {
            $('#locationHide' + id).addClass('hide');
        }

    });

    $(document).on('click', '#product-location-submit', function(e) {
        e.preventDefault();
        var all = $(document).find('#allLocations').val();
        var allLocations = all.split(",");
        var localArray = [];
        var check = 0;
        function local() {
            this.productID = '';
            this.locationArray = '';
            this.categoryID = '';
            this.productGlobal = '';
        }
        $(document).find('.product-location-table').find('tbody').find('tr').each(function() {
            var pname = $(this).find('.pname').html();
            var pid = $(this).data('productid');
            var categoryid = $(this).find('#category' + pid).val();
            if ($(this).find('.global').is(":checked")) {
                var loc = new local();
                loc.productID = pid;
                loc.locationArray = allLocations;
                loc.categoryID = categoryid;
                loc.productGlobal = 1;
                localArray.push(loc);
            } else if ($(this).find('.location').is(":checked")) {
                var locations = [];
                $(this).find('.locationTable').find('tbody').find('.locCheck').each(function() {
                    if ($(this).is(':checked')) {
                        locations.push($(this).parents('tr').data('lid'));
                    }
                });
                if (jQuery.isEmptyObject(locations)) {
                    p_notification(false, eb.getMessage('ERR_PROIMPORT_LOC', pname));
                    check = 1;
                    return false;

                }
                var loc = new local();
                loc.productID = pid;
                loc.locationArray = locations;
                loc.categoryID = categoryid;
                loc.productGlobal = 0;
                localArray.push(loc);
            }
        });
        if (check == 0) {
            showLoader();
            var updateImportLocations = BASE_URL + '/productAPI/importProductLocations';
            var importLocationRequest = eb.post(updateImportLocations, {
                local: localArray,
            });
            importLocationRequest.done(function(retdata) {
                p_notification(retdata.status, retdata.msg);
                hideLoader();
                if (retdata.status) {
                    window.setTimeout(function() {
                        window.location.assign(BASE_URL + '/product/import');
                    }, 1500);
                }
            });
        }
    });

    function productimport() {
        var importurl = BASE_URL + '/productAPI/importproduct';
        var importrequest = eb.post(importurl, {
            header: $('#productimportheader').html(),
            delim: $('#productimportdelim').html(),
            columncount: $('#productimportcolumncount').html(),
            col: pp,
        });
        importrequest.done(function(retdata) {
            p_notification(retdata.status, retdata.msg);
            hideLoader();
            if (retdata.status && !jQuery.isEmptyObject(retdata.data)) {
                allSaveProduct = retdata.data['save'];
                replaceProduct = retdata.data['replace'];
                $('#importdatatable').hide();
                $('#product-importcancel-button').hide();
                $('#product-importdata-button').hide();
                $('#step2').show();
                $('#step2').append(retdata.html);
                $('#step2 .selectpicker').selectpicker();
            } else {
                window.setTimeout(function() {
                    window.location.assign(BASE_URL + '/product/import');
                }, 3000);
            }
        });
    }

    //responsive issue fixes
    $(window).bind('ready resize', function() {
        if ($(window).width() < 480) {
            $('#productuploadfile').addClass('col-xs-12').css('margin-bottom', '8px');
            $(':input[type="reset"]').addClass('col-xs-12').css('margin-bottom', '8px');
            $('.info_box').css({
                'border-left': 'none',
                'padding-left': '0',
                'border-right': 'none'
            });
        } else {
            $('#productuploadfile').removeClass('col-xs-12').css('margin-bottom', '0px');
            $(':input[type="reset"]').removeClass('col-xs-12').css('margin-bottom', '0px');
            $('.info_box').css({
                'border': '1px dashed #ccc',
                'border-left': '4px solid #888',
                'padding-left': '20px',
            });
        }
    });
});
function disableoptions(selectedoption) {
    for (var i = 0; i < productcolumns; i++) {
        $("#" + i + " option:contains(" + selectedoption + ")").attr('disabled', 'disabled');
    }
    $('.productimportselector').selectpicker('refresh');
}
function enableoptions(selectedenableoption) {
    for (var j = 0; j < productcolumns; j++) {
        $("#" + j + " option:contains(" + selectedenableoption + ")").removeAttr('disabled');
    }
    $('.productimportselector').selectpicker('refresh');
}