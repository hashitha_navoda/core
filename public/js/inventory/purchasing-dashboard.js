var colorPalettex = ['#3023AE','#F76B1C','#1298FF', '#FF4560', '#775DD0','#6023ea','#ea2385'];
$(document).ready(function() {
if (!$('.main_body').hasClass('hide-side-bar')) {
    $('.sidbar-toggle-menu').trigger('click');
}
var mostPurchaseBySupplierData;
var heigstSalesItemData;
var currencySymbol;
    // getWidgetData('thisWeek');

    updateDashboardData('thisWeek');

    function updateDashboardData(period) {
        getTotalPurchasesData(period, function () {
            updateChartData(period, function() {
                updateFooterChartData(period, function () {

                });
            });
        });
    }

    function getTotalPurchasesData (period, callback) {
        eb.ajax({
            type: 'POST',
            url: BASE_URL + '/purchasing-dashboard-api/getTotalPurchasesData',
            data: {period: period},
            success: function(data) {
                currencySymbol = data.companyCurrencySymbol;
                $('#total-sales').each(function () {
                    $(this).prop('Counter',0).animate({
                        Counter: data.totalPurchase
                    }, {
                        duration: 1000,
                        easing: 'swing',
                        step: function (now) {
                            $(this).text(currencySymbol+' '+accounting.formatMoney(now));
                        }
                    });
                });
                $('#gross-profit').each(function () {
                    $(this).prop('Counter',0).animate({
                        Counter: data.activeSuppliers
                    }, {
                        duration: 1000,
                        easing: 'swing',
                        step: function (now) {
                            $(this).text(Math.ceil(now));
                        }
                    });
                });
               
                if (data.totalPurchaseProfitLoss > 0) {
                    $('#total-sales-profit').removeClass('hidden');
                    $('#total-sales-loss').addClass('hidden');
                    $('#total-sales-profit').html('+'+data.totalPurchaseProfitLoss+'%<span><i class="glyphicon glyphicon-arrow-up"></i></span>');
                } else {
                    $('#total-sales-profit').addClass('hidden');
                    $('#total-sales-loss').removeClass('hidden');
                    var loss = data.totalPurchaseProfitLoss * -1;
                    $('#total-sales-loss').html('+'+loss+'%<span><i class="glyphicon glyphicon-arrow-down"></i></span>');
                }

                switch (period) {
                    case 'thisYear':
                        $('#last-period-total-sales').text('Compared to ('+currencySymbol+' '+accounting.formatMoney(data.totalPurchaseLastYear)+' last year)');
                        break;
                    case 'thisMonth':
                        $('#last-period-total-sales').text('Compared to ('+currencySymbol+' '+accounting.formatMoney(data.totalPurchaseLastYear)+' last month)');
                        break;
                    case 'thisWeek':
                        $('#last-period-total-sales').text('Compared to ('+currencySymbol+' '+accounting.formatMoney(data.totalPurchaseLastYear)+' last week)');
                        break;
                    case 'thisDay':
                        $('#last-period-total-sales').text('Compared to ('+currencySymbol+' '+accounting.formatMoney(data.totalPurchaseLastYear)+' last day)');
                        break;
                    default:
                        // statements_def
                        break;
                }
                
            }
        }).then(function () {
            showLoader();
            getTotalReturnsData(period);
        });
        callback();
    }
    function getTotalReturnsData (period) { 
        eb.ajax({
            type: 'POST',
            url: BASE_URL + '/purchasing-dashboard-api/getTotalReturnsData',
            data: {period: period},
            success: function(data) {
                currencySymbol = data.companyCurrencySymbol;
                $('#total-payments').each(function () {
                    $(this).prop('Counter',0).animate({
                        Counter: data.totalPurchaseReturn
                    }, {
                        duration: 1000,
                        easing: 'swing',
                        step: function (now) {
                            $(this).text(currencySymbol+' '+accounting.formatMoney(now));
                        }
                    });
                });
               

                if (data.totalPurchaseReturnProfitLoss > 0) {
                    $('#total-payment-profit').removeClass('hidden');
                    $('#total-payment-loss').addClass('hidden');
                    $('#total-payment-profit').html('+'+data.totalPurchaseReturnProfitLoss+'%<span><i class="glyphicon glyphicon-arrow-up"></i></span>');
                } else {
                    $('#total-payment-profit').addClass('hidden');
                    $('#total-payment-loss').removeClass('hidden');
                    var loss = data.totalPurchaseReturnProfitLoss * -1;
                    $('#total-payment-loss').html('+'+loss+'%<span><i class="glyphicon glyphicon-arrow-down"></i></span>');
                }

                

                switch (period) {
                    case 'thisYear':
                        $('#last-period-total-payment').text('Compared to ('+currencySymbol+' '+accounting.formatMoney(data.totalPurchaseReturnLastPeriod)+' last year)');
                        break;
                    case 'thisMonth':
                        $('#last-period-total-payment').text('Compared to ('+currencySymbol+' '+accounting.formatMoney(data.totalPurchaseReturnLastPeriod)+' last month)');
                        break;
                    case 'thisWeek':
                        $('#last-period-total-payment').text('Compared to ('+currencySymbol+' '+accounting.formatMoney(data.totalPurchaseReturnLastPeriod)+' last week)');
                        break;
                    case 'thisDay':
                        $('#last-period-total-payment').text('Compared to ('+currencySymbol+' '+accounting.formatMoney(data.totalPurchaseReturnLastPeriod)+' last day)');
                        break;
                    default:
                        // statements_def
                        break;
                }
                
            }
        }).then(function () {
            getPaymentData(period);
        });

    }

    function getPaymentData (period) { 
        eb.ajax({
            type: 'POST',
            url: BASE_URL + '/purchasing-dashboard-api/getPaymentData',
            data: {period: period},
            success: function(data) {
                currencySymbol = data.companyCurrencySymbol;
                $('#totalInvoiceCount').each(function () {
                    $(this).prop('Counter',0).animate({
                        Counter: data.totalPayments
                    }, {
                        duration: 1000,
                        easing: 'swing',
                        step: function (now) {
                            $(this).text(currencySymbol+' '+accounting.formatMoney(now));
                        }
                    });
                });
                if (data.totalPaymentProfitLoss > 0) {
                    $('#total-invoice-count-profit').removeClass('hidden');
                    $('#total-invoice-count-loss').addClass('hidden');
                    $('#total-invoice-count-profit').html('+'+data.totalPaymentProfitLoss+'%<span><i class="glyphicon glyphicon-arrow-up"></i></span>');
                } else {
                    $('#total-invoice-count-profit').addClass('hidden');
                    $('#total-invoice-count-loss').removeClass('hidden');
                    var loss = data.totalPaymentProfitLoss * -1;
                    $('#total-invoice-count-loss').html('+'+loss+'%<span><i class="glyphicon glyphicon-arrow-down"></i></span>');
                }

                switch (period) {
                    case 'thisYear':
                        $('#last-period-total-invoices').text('Compared to ('+data.totalPaymentsLastPeriod+' last year)');
                        break;
                    case 'thisMonth':
                        $('#last-period-total-invoices').text('Compared to ('+data.totalPaymentsLastPeriod+' last month)');
                        break;
                    case 'thisWeek':
                        $('#last-period-total-invoices').text('Compared to ('+data.totalPaymentsLastPeriod+' last week)');
                        break;
                    case 'thisDay':
                        $('#last-period-total-invoices').text('Compared to ('+data.totalPaymentsLastPeriod+' last day)');
                        break;
                    default:
                        // statements_def
                        break;
                }
                
            }
        });
    }

    function updateChartData (period, callback) {
        eb.ajax({
            type: 'POST',
            url: BASE_URL + '/purchasing-dashboard-api/updateChartData',
            data: {period: period},
            success: function(data) {
                currencySymbol = data.companyCurrencySymbol;
                $('#cash-chart').html('');
                
                var poTotal = 0;
                $.each(data.poStatusData, function(index, val) {
                    poTotal += parseFloat(val.poTotal); 
                });

                poStatusSummaryChart(data.poStatusData, poTotal);
                cashCreditChart(data.mostPurchaseBySupplierData);                
            }
        });
        callback();
    }

    function updateFooterChartData (period, callback) {
        eb.ajax({
            type: 'POST',
            url: BASE_URL + '/purchasing-dashboard-api/updateFooterChartData',
            data: {period: period},
            success: function(data) {
                currencySymbol  = data.companyCurrencySymbol;
                var prTotal = 0;
                $.each(data.mostPurchaseReturnBySupplierData, function(index, val) {
                    if (index < 10) {
                        prTotal += parseFloat(val.return); 
                    }
                });

                salesAndGpSummaryChart(data.mostPurchaseReturnBySupplierData, prTotal);
                salesSummaryChart(data.piStatusData);

                var payTotal = 0;
                $.each(data.paymentMethodData, function(index, val) {
                    payTotal += parseFloat(val.paymentTotal); 
                });

                paymentSummaryChart(data.paymentMethodData, payTotal);
            }
        }); 
        callback();  
    }



    function getWidgetData(period){
        eb.ajax({
            type: 'POST',
            url: BASE_URL + '/purchasing-dashboard-api/getWidgetData',
            data: {period: period},
            success: function(data) {
                $('#target-chart').html('');
                $('#cash-chart').html('');
                $('#gp-chart').html('');
                $('#total-sales').each(function () {
                    $(this).prop('Counter',0).animate({
                        Counter: data.totalPurchase
                    }, {
                        duration: 1000,
                        easing: 'swing',
                        step: function (now) {
                            $(this).text('LKR '+accounting.formatMoney(now));
                        }
                    });
                });
                $('#gross-profit').each(function () {
                    $(this).prop('Counter',0).animate({
                        Counter: data.activeSuppliers
                    }, {
                        duration: 1000,
                        easing: 'swing',
                        step: function (now) {
                            $(this).text(Math.ceil(now));
                        }
                    });
                });
                $('#total-payments').each(function () {
                    $(this).prop('Counter',0).animate({
                        Counter: data.totalPurchaseReturn
                    }, {
                        duration: 1000,
                        easing: 'swing',
                        step: function (now) {
                            $(this).text('LKR '+accounting.formatMoney(now));
                        }
                    });
                });
                $('#totalInvoiceCount').each(function () {
                    $(this).prop('Counter',0).animate({
                        Counter: data.totalPayments
                    }, {
                        duration: 1000,
                        easing: 'swing',
                        step: function (now) {
                            $(this).text('LKR '+accounting.formatMoney(now));
                        }
                    });
                });

                if (data.totalPurchaseProfitLoss > 0) {
                    $('#total-sales-profit').removeClass('hidden');
                    $('#total-sales-loss').addClass('hidden');
                    $('#total-sales-profit').html('+'+data.totalPurchaseProfitLoss+'%<span><i class="glyphicon glyphicon-arrow-up"></i></span>');
                } else {
                    $('#total-sales-profit').addClass('hidden');
                    $('#total-sales-loss').removeClass('hidden');
                    var loss = data.totalPurchaseProfitLoss * -1;
                    $('#total-sales-loss').html('+'+loss+'%<span><i class="glyphicon glyphicon-arrow-down"></i></span>');
                }

                if (data.totalPurchaseReturnProfitLoss > 0) {
                    $('#total-payment-profit').removeClass('hidden');
                    $('#total-payment-loss').addClass('hidden');
                    $('#total-payment-profit').html('+'+data.totalPurchaseReturnProfitLoss+'%<span><i class="glyphicon glyphicon-arrow-up"></i></span>');
                } else {
                    $('#total-payment-profit').addClass('hidden');
                    $('#total-payment-loss').removeClass('hidden');
                    var loss = data.totalPurchaseReturnProfitLoss * -1;
                    $('#total-payment-loss').html('+'+loss+'%<span><i class="glyphicon glyphicon-arrow-down"></i></span>');
                }

                if (data.totalPaymentProfitLoss > 0) {
                    $('#total-invoice-count-profit').removeClass('hidden');
                    $('#total-invoice-count-loss').addClass('hidden');
                    $('#total-invoice-count-profit').html('+'+data.totalPaymentProfitLoss+'%<span><i class="glyphicon glyphicon-arrow-up"></i></span>');
                } else {
                    $('#total-invoice-count-profit').addClass('hidden');
                    $('#total-invoice-count-loss').removeClass('hidden');
                    var loss = data.totalPaymentProfitLoss * -1;
                    $('#total-invoice-count-loss').html('+'+loss+'%<span><i class="glyphicon glyphicon-arrow-down"></i></span>');
                }

                var prTotal = 0;
                $.each(data.mostPurchaseReturnBySupplierData, function(index, val) {
                    if (index < 10) {
                        prTotal += parseFloat(val.return); 
                    }
                });

                salesAndGpSummaryChart(data.mostPurchaseReturnBySupplierData, prTotal);
                salesSummaryChart(data.piStatusData);

                var payTotal = 0;
                $.each(data.paymentMethodData, function(index, val) {
                    payTotal += parseFloat(val.paymentTotal); 
                });

                paymentSummaryChart(data.paymentMethodData, payTotal);

                var poTotal = 0;
                $.each(data.poStatusData, function(index, val) {
                    poTotal += parseFloat(val.poTotal); 
                });

                poStatusSummaryChart(data.poStatusData, poTotal);
                mostPurchaseBySupplierData = data.mostPurchaseBySupplierData;
                cashCreditChart(data.mostPurchaseBySupplierData);

                switch (period) {
                    case 'thisYear':
                        $('#last-period-total-sales').text('Compared to (LKR '+accounting.formatMoney(data.totalPurchaseLastYear)+' last year)');
                        $('#last-period-total-payment').text('Compared to (LKR '+accounting.formatMoney(data.totalPurchaseReturnLastPeriod)+' last year)');
                        $('#last-period-total-invoices').text('Compared to ('+data.totalPaymentsLastPeriod+' last year)');
                        break;
                    case 'thisMonth':
                        $('#last-period-total-sales').text('Compared to (LKR '+accounting.formatMoney(data.totalPurchaseLastYear)+' last month)');
                        $('#last-period-total-payment').text('Compared to (LKR '+accounting.formatMoney(data.totalPurchaseReturnLastPeriod)+' last month)');
                        $('#last-period-total-invoices').text('Compared to ('+data.totalPaymentsLastPeriod+' last month)');
                        break;
                    case 'thisWeek':
                        $('#last-period-total-sales').text('Compared to (LKR '+accounting.formatMoney(data.totalPurchaseLastYear)+' last week)');
                        $('#last-period-total-payment').text('Compared to (LKR '+accounting.formatMoney(data.totalPurchaseReturnLastPeriod)+' last week)');
                        $('#last-period-total-invoices').text('Compared to ('+data.totalPaymentsLastPeriod+' last week)');
                        break;
                    case 'thisDay':
                        $('#last-period-total-sales').text('Compared to (LKR '+accounting.formatMoney(data.totalPurchaseLastYear)+' last day)');
                        $('#last-period-total-payment').text('Compared to (LKR '+accounting.formatMoney(data.totalPurchaseReturnLastPeriod)+' last day)');
                        $('#last-period-total-invoices').text('Compared to ('+data.totalPaymentsLastPeriod+' last day)');
                        break;
                    default:
                        // statements_def
                        break;
                }
                
            }
        });
    }

    $('.period').on('change', function(event) {
        event.preventDefault();
        // getWidgetData($(this).val());
        updateDashboardData($(this).val());
    });


    function gpChart(endAngle)
    {
        var options1 = {
            chart: {
                height: 200,
                type: 'radialBar',
                toolbar: {
                    show: false
                }
            },
            plotOptions: {
                radialBar: {
                    startAngle: 0,
                    endAngle: parseFloat(endAngle),
                    hollow: {
                        margin: 0,
                        size: '70%',
                        background: '#fff',
                        image: undefined,
                        imageOffsetX: 0,
                        imageOffsetY: 0,
                        position: 'front',
                        dropShadow: {
                            enabled: true,
                            top: 3,
                            left: 0,
                            blur: 4,
                            opacity: 0.24
                        }
                    },
                    track: {
                        background: '#fff',
                        strokeWidth: '70%',
                        margin: 0, // margin is in pixels
                        dropShadow: {
                            enabled: true,
                            top: -3,
                            left: 0,
                            blur: 4,
                            opacity: 0.35
                        }
                    },

                    dataLabels: {
                        showOn: 'always',
                        name: {
                            offsetY: 0,
                            show: true,
                            color: '#888',
                            fontSize: '15px'
                        },
                        value: {
                            formatter: function(val) {
                                return parseInt(val);
                            },
                            color: '#111',
                            fontSize: '20px',
                            show: false,
                        }
                    }
                }
            },
            fill: {
                type: 'gradient',
                gradient: {
                    shade: 'dark',
                    type: 'horizontal',
                    shadeIntensity: 0.5,
                    gradientToColors: ['#ABE5A1'],
                    inverseColors: true,
                    opacityFrom: 1,
                    opacityTo: 1,
                    stops: [0, 100]
                }
            },
            series: [75],
            stroke: {
                lineCap: 'round'
            },
            labels: ['Gross Profit'],
        }
        var chart1 = new ApexCharts(
            document.querySelector("#target-chart"),
            options1
        );
        chart1.render();
    }

    $('#sales-gross-profit').on('click', function(event) {
        event.preventDefault();
        $('#gp-chart').removeClass('hidden');
        $('#cash-chart').addClass('hidden');
        if ($('#gp-chart')[0].children.length > 0) {
            $('#gp-chart')[0].children[0].remove();
        }
        salsVsGpChart(heigstSalesItemData);
    });

    function salsVsGpChart(heigstSalesItemData)
    {
        var totalSales = [];
        var totalProfit = [];
        var products = [];
        $.each(heigstSalesItemData, function(index, val) {
            if (index < 10) {
                totalSales.push(val.totalSales);
                var profit = (val.profit == undefined) ? 0 : val.profit;
                totalProfit.push(profit);
                products.push(val.productName);
            }
        });

        var options = {
            chart: {
                height: 350,
                type: 'area',
            },
            dataLabels: {
                enabled: false
            },
            stroke: {
                curve: 'smooth'
            },
            series: [{
                name: 'Total Sales',
                data: totalSales
            }, {
                name: 'Total Profit',
                data: totalProfit
            }],

            xaxis: {
                type: 'category',
                categories: products,                
            },
            yaxis: {
                labels: {
                    formatter: (value) => { return "Rs "+accounting.formatMoney(value) },
                }
            },
            tooltip: {
                x: {
                    format: 'dd/MM/yy HH:mm'
                },
                y: {
                    formatter: function(value, { series, seriesIndex, dataPointIndex, w }) {
                        return "Rs "+accounting.formatMoney(value)
                    }
                }
            }
        }

        var chart = new ApexCharts(
            document.querySelector("#gp-chart"),
            options
        );

        chart.render();
    }



    $('#cash-credit').on('click', function(event) {
        event.preventDefault();
        $('#cash-chart').removeClass('hidden');
        $('#gp-chart').addClass('hidden');
        if ($('#cash-chart')[0].children.length > 0) {
            $('#cash-chart')[0].children[0].remove();
        }
        $('#gp-chart').html('');
        cashCreditChart(customerSales);
    });    

    function cashCreditChart(mostPurchaseBySupplierData)
    {
        $('#cash-chart').html('');
        var totalPurchase = [];
        var totalReturn = [];
        var suppliers = [];
        var options = {};
        var i = 0;        
        $.each(mostPurchaseBySupplierData, function(index, val) {
            if (i < 10) {
                totalPurchase.push(parseFloat(val.purchase));
                totalReturn.push(parseFloat(val.return));
                suppliers.push(val.supplier);
            }
            i++;
        });


        var colWidth = '50%';

        if (suppliers.length < 3) {
            colWidth = '10%';
        }

        options = {
            chart: {
                height: 350,
                type: 'bar',
            },
            plotOptions: {
                bar: {
                    columnWidth: colWidth,   
                }
            },
            dataLabels: {
                enabled: false
            },
            stroke: {
                width: 2
            },
            series: [{
                name: 'Total Purchase',
                data: totalPurchase
            }, {
                name: 'Total Returns',
                data: totalReturn
            }],
            grid: {
                row: {
                    colors: ['#fff', '#f2f2f2']
                }
            },
            xaxis: {
                type: 'category',
                categories: suppliers,
            },
            yaxis: {
                labels: {
                    formatter: (value) => { return currencySymbol+" "+accounting.formatMoney(value) },
                }
            },
            tooltip: {
                x: {
                    format: 'dd/MM/yy HH:mm'
                },
                y: {
                    formatter: function(value, { series, seriesIndex, dataPointIndex, w }) {
                        return currencySymbol+" "+accounting.formatMoney(value)
                    }
                }
            },
            fill: {
                type: 'gradient',
                gradient: {
                    shade: 'light',
                    type: "horizontal",
                    shadeIntensity: 0.25,
                    gradientToColors: undefined,
                    inverseColors: true,
                    opacityFrom: 0.85,
                    opacityTo: 0.85,
                    stops: [50, 0, 100]
                },
            },
        }


        renderChart(options, 'cash-chart');
    }


    function renderChart(options, chartID)
    {
        var chart = new ApexCharts(
            document.querySelector("#"+chartID),
            options
        );
        chart.render();
    }


    function salesAndGpSummaryChart(prData, prTotal)
    {

        let chartConfig = {
            graphset : [

            ]
        };

        var gdata = {
            x : 0*200,
            y : 0*200,
            width : '100%',
            height : '100%',
            type : 'pie',
            noData:{
                text:"Currently there is no data in the chart",
                backgroundColor: "#5081e7",
                fontSize:18,
                textAlpha:.9,
                alpha:.6,
                bold:true
            },
            plotarea : {
                margin : '15 5 5 5'
            },
            "scale":{
                "size-factor":0.9
            },
            labels:[
                {
                    text:"Rs "+accounting.formatMoney(prTotal),
                    x:"50%", 
                    y:"47%",
                    anchor : 'c',
                    fontSize:"17px",
                    fontStyle : 'bold',
                    fontColor : 'black',
                    padding : "10px",
                },
                {
                    text:"Total <br> Returns",
                    x:"50%", 
                    y:"58%",
                    fontSize:"13px",
                    anchor : 'c',
                    fontStyle : 'bold',
                    fontColor : 'grey',
                    padding : "10px",
                },
            ],
            plot : {
                borderWidth : 2,
                borderColor : 'white',
                shadow : true,
                shadowAlpha : 1,
                shadowAngle : -45,
                borderAlpha : 0.5,
                slice : '89%',
                pieTransform : 'flow=4',
                hoverState : {
                    visible : false    
                },
                tooltip : {
                    text : '%t <br> '+currencySymbol+' %node-value',
                    thousandsSeparator:','
                },
                'background-color': "black",
                'value-box': {
                    text: "%t <br> "+currencySymbol+" %v",
                    'font-color': "black",
                    'font-size': 10,
                    'background-color': "white",
                    borderColor : 'white',
                    shadow : true,
                    shadowAlpha : 1,
                    shadowAngle : -45,
                    borderAlpha : 0.5,
                    'border-width': 1,
                    padding: "5%",
                    borderRadius : '5px',
                    placement:'out',
                    thousandsSeparator:',',
                    // offsetX: 15,
                    // offsetY: -15,
                }
            },
            series:[
            ]
        };

        $.each(prData, function(index, val) {
            var series = {
                "values":[parseFloat(val.return)],  
                "text":val.supplier,  
                "background-color":colorPalettex[index],
            }
            gdata.series.push(series);
        });


        chartConfig.graphset.push(gdata);

        zingchart.render({
            id: 'sales-gp-summary',
            width: '100%',
            height: '100%',
            data: chartConfig
        });

        if (prTotal == 0) {
            $('#sales-gp-summary .zc-text').remove();
        }
    }


    function salesSummaryChart(piStatusData)
    {
        let chartConfig = {
            graphset : [

            ]
        };

        var gdata = {
            x : 0*200,
            y : 0*200,
            width : '100%',
            height : '100%',
            type : 'pie',
            noData:{
                text:"Currently there is no data in the chart",
                backgroundColor: "#5081e7",
                fontSize:18,
                textAlpha:.9,
                alpha:.6,
                bold:true
            },
            plotarea : {
                margin : '15 5 5 5',
            },
            "scale":{
                "size-factor":0.9
            },
            labels:[
                {
                    text:currencySymbol+" "+accounting.formatMoney(parseFloat(piStatusData.openInvoiceTotal) + parseFloat(piStatusData.overdueInvoiceTotal) + parseFloat(piStatusData.closedInvoiceTotal)),
                    x:"50%", 
                    y:"47%",
                    anchor : 'c',
                    fontSize:"17px",
                    fontStyle : 'bold',
                    fontColor : 'black',
                    padding : "10px",
                },
                {
                    text:"Total <br> Purchase Summary",
                    x:"50%", 
                    y:"58%",
                    fontSize:"13px",
                    anchor : 'c',
                    fontStyle : 'bold',
                    fontColor : 'grey',
                    padding : "10px",
                },
            ],
            plot : {
                borderWidth : 2,
                borderColor : 'white',
                shadow : true,
                shadowAlpha : 1,
                shadowAngle : -45,
                borderAlpha : 0.5,
                slice : '89%',
                pieTransform : 'flow=4',
                hoverState : {
                    visible : false    
                },
                tooltip : {
                    text : '%t <br> '+currencySymbol+' %node-value',
                    thousandsSeparator:','
                },
                'background-color': "black",
                'value-box': {
                    text: "%t <br> "+currencySymbol+" %v",
                    'font-color': "black",
                    'font-size': 10,
                    'background-color': "white",
                    borderColor : 'white',
                    shadow : true,
                    shadowAlpha : 1,
                    shadowAngle : -45,
                    borderAlpha : 0.5,
                    'border-width': 1,
                    padding: "5%",
                    borderRadius : '5px',
                    placement:'out',
                    thousandsSeparator:',',
                    offsetX: 15,
                    offsetY: -15,
                }
            },
            series:[  
            ]
        };

        if (parseFloat(piStatusData.openInvoiceTotal) > 0) {
            var series = {
                "values":[parseFloat(piStatusData.openInvoiceTotal)],  
                "text":"Unpaid",  
                "background-color":"#3023AE",
            }
            gdata.series.push(series);
        }

        if (parseFloat(piStatusData.closedInvoiceTotal) > 0) {
            var series = {
                "values":[parseFloat(piStatusData.closedInvoiceTotal)],  
                "text":"Paid",  
                "background-color":"#F76B1C",
            }
            gdata.series.push(series);
        }

        if (parseFloat(piStatusData.overdueInvoiceTotal) > 0) {
            var series = {
                "values":[parseFloat(piStatusData.overdueInvoiceTotal)],  
                "text":"Due",  
                "background-color":"#1298FF",
            }
            gdata.series.push(series);
        }

        chartConfig.graphset.push(gdata);

        zingchart.render({
            id: 'sales-summary',
            width: '100%',
            height: '100%',
            data: chartConfig
        });

        if (parseFloat(piStatusData.openInvoiceTotal) == 0 && parseFloat(piStatusData.closedInvoiceTotal) == 0 && parseFloat(piStatusData.overdueInvoiceTotal) == 0) {
            $('#sales-summary .zc-text').remove();
        }
    }

    function poStatusSummaryChart(poData, poTotal)
    {
        let chartConfig = {
            graphset : [

            ]
        };

        var gdata = {
            x : 0*200,
            y : 0*200,
            width : '100%',
            height : '100%',
            type : 'pie',
            noData:{
                text:"Currently there is no data in the chart",
                backgroundColor: "#5081e7",
                fontSize:18,
                textAlpha:.9,
                alpha:.6,
                bold:true
            },
            plotarea : {
                margin : '15 5 5 5'
            },
            "scale":{
                "size-factor":0.9
            },
            labels:[
                {
                    text:currencySymbol+" "+accounting.formatMoney(poTotal),
                    x:"50%", 
                    y:"47%",
                    anchor : 'c',
                    fontSize:"17px",
                    fontStyle : 'bold',
                    fontColor : 'black',
                    padding : "10px",
                },
                {
                    text:"Total <br> PO Summary",
                    x:"50%", 
                    y:"58%",
                    fontSize:"13px",
                    anchor : 'c',
                    fontStyle : 'bold',
                    fontColor : 'grey',
                    padding : "10px",
                },
            ],
            plot : {
                borderWidth : 2,
                borderColor : 'white',
                shadow : true,
                shadowAlpha : 1,
                shadowAngle : -45,
                borderAlpha : 0.5,
                slice : '89%',
                pieTransform : 'flow=4',
                hoverState : {
                    visible : false    
                },
                tooltip : {
                    text : '%t <br> '+currencySymbol+' %node-value',
                    thousandsSeparator:','
                },
                'background-color': "black",
                'value-box': {
                    text: "%t <br> "+currencySymbol+" %v",
                    'font-color': "black",
                    'font-size': 10,
                    'background-color': "white",
                    borderColor : 'white',
                    shadow : true,
                    shadowAlpha : 1,
                    shadowAngle : -45,
                    borderAlpha : 0.5,
                    'border-width': 1,
                    padding: "5%",
                    borderRadius : '5px',
                    placement:'out',
                    thousandsSeparator:',',
                    // offsetX: 15,
                    offsetY: -10,
                }
            },
            series:[  
            ]
        };

        $.each(poData, function(index, val) {
            if (parseFloat(val.poTotal) != 0) {
                var series = {
                    "values":[parseFloat(val.poTotal)],  
                    "text":val.statusName,  
                    "background-color":colorPalettex[index],
                }
                gdata.series.push(series);
            }
        });

        chartConfig.graphset.push(gdata);

        zingchart.render({
            id: 'po-summary',
            width: '100%',
            height: '100%',
            data: chartConfig
        });

        if (poTotal == 0) {
            $('#po-summary .zc-text').remove();
        }
    }

    function paymentSummaryChart(paymentData, payTotal)
    {

        let chartConfig = {
            graphset : [

            ]
        };

        var gdata = {
            x : 0*200,
            y : 0*200,
            width : '100%',
            height : '100%',
            type : 'pie',
            noData:{
                text:"Currently there is no data in the chart",
                backgroundColor: "#5081e7",
                fontSize:18,
                textAlpha:.9,
                alpha:.6,
                bold:true
            },
            plotarea : {
                margin : '15 5 5 5'
            },
            "scale":{
                "size-factor":0.9
            },
            labels:[
                {
                    text:currencySymbol+" "+accounting.formatMoney(payTotal),
                    x:"50%", 
                    y:"47%",
                    anchor : 'c',
                    fontSize:"17px",
                    fontStyle : 'bold',
                    fontColor : 'black',
                    padding : "10px",
                },
                {
                    text:"Total <br> Payments",
                    x:"50%", 
                    y:"58%",
                    fontSize:"13px",
                    anchor : 'c',
                    fontStyle : 'bold',
                    fontColor : 'grey',
                    padding : "10px",
                },
            ],
            plot : {
                borderWidth : 2,
                borderColor : 'white',
                shadow : true,
                shadowAlpha : 1,
                shadowAngle : -45,
                borderAlpha : 0.5,
                slice : '89%',
                pieTransform : 'flow=4',
                hoverState : {
                    visible : false    
                },
                tooltip : {
                    text : '%t <br> '+currencySymbol+' %node-value',
                    thousandsSeparator:','
                },
                'background-color': "black",
                'value-box': {
                    text: "%t <br> "+currencySymbol+" %v",
                    'font-color': "black",
                    'font-size': 10,
                    'background-color': "white",
                    borderColor : 'white',
                    shadow : true,
                    shadowAlpha : 1,
                    shadowAngle : 45,
                    borderAlpha : 0.5,
                    'border-width': 1,
                    padding: "5%",
                    borderRadius : '5px',
                    placement:'out',
                    thousandsSeparator:','
                }
            },
            series:[
            ]
        };

        $.each(paymentData, function(index, val) {
            var series = {
                "values":[parseFloat(val.paymentTotal)],  
                "text":val.paymentMethodName,  
                "background-color":colorPalettex[index],
            }
            gdata.series.push(series);
        });

        chartConfig.graphset.push(gdata);

        zingchart.render({
            id: 'sales-payment-summary',
            width: '100%',
            height: '100%',
            data: chartConfig
        });
        if (payTotal == 0) {
            $('#sales-payment-summary .zc-text').remove();
        }
    }
});


