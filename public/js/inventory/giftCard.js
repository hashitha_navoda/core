var ignoreBudgetLimitFlag = false;
$(document).ready(function() {

	loadDropDownFromDatabase('/accounts-api/search-active-accounts-for-dropdown', "", "", '#giftCardGlAccountID');

	var accountID = $('#giftCardGlAccountID').data('id');
	if(!(accountID == null || accountID == 0)){
		var accountName = $('#giftCardGlAccountID').data('value');
    	$('#giftCardGlAccountID').append("<option value='"+accountID+"'>"+accountName+"</option>")
    	$('#giftCardGlAccountID').val(accountID).selectpicker('refresh');
	}

    $('#create-gift-card-form').on('submit', function(e) {
        e.preventDefault();
        if (validationForm()) {
            saveGiftCard();
        }
    });

    function saveGiftCard()
    {
        eb.ajax({
            type: 'POST',
            url: BASE_URL + '/gift-card-api/save',
            data: {
                giftCardName: $('#giftCardName').val(),
                giftCardPrice: $('#giftCardPrice').val(),
                giftCardPrefix: $('#giftCardPrefix').val(),
                startValue: $('#startValue').val(),
                endValue: $('#endValue').val(),
                giftCardDiscountPrecentage: $('#giftCardDiscountPrecentage').val(),
                giftCardDurationDays: $('#giftCardDurationDays').val(),
                giftCardGlAccountID: $('#giftCardGlAccountID').val(),
                ignoreBudgetLimit: ignoreBudgetLimitFlag
            },
            success: function(respond) {
                if (respond.status == true) {
                    window.location.reload();
                } else {
                    if (respond.data == "NotifyBudgetLimit") {
                        bootbox.confirm(respond.msg+' ,Are you sure you want to delete ?', function(result) {
                            if (result == true) {
                                ignoreBudgetLimitFlag = true;
                                saveGiftCard();
                            } else {
                                setTimeout(function(){ 
                                    location.reload();
                                }, 3000);
                            }
                        });
                    } else {
                        p_notification(respond.status, respond.msg);
                    }
                }

            }
        });
    }


    function validationForm() {
        var giftCardName = $('#giftCardName').val();
        var giftCardPrice = $('#giftCardPrice').val();
        var giftCardDiscountPrecentage = $('#giftCardDiscountPrecentage').val();
        var giftCardDurationDays = $('#giftCardDurationDays').val();
        var giftCardPrefix = $('#giftCardPrefix').val();
        var startValue = $('#startValue').val();
        var endValue = $('#endValue').val();

        if (giftCardName == '' || giftCardName == null) {
            p_notification(false, eb.getMessage('ERR_GIFCARD_NAME_CNT_BE_NULL'));
            $('#giftCardName').focus();
            return false;
        } else if (giftCardPrice == '' || giftCardPrice == null) {
            p_notification(false, eb.getMessage('ERR_GIFCARD_PRICE_CNT_BE_NULL'));
            $('#giftCardPrice').focus();
            return false;
        } else if (giftCardPrice < 0 || isNaN(giftCardPrice)) {
            p_notification(false, eb.getMessage('ERR_GIFCARD_PRICE_SH_POSITIVE_AND_INTEGER'));
            $('#giftCardPrice').val('');
            $('#giftCardPrice').focus();
            return false;
        } else if (giftCardPrefix == '' || giftCardPrefix == null) {
            p_notification(false, eb.getMessage('ERR_GIFCARD_PREFIX_CNT_BE_NULL'));
            $('#giftCardPrefix').focus();
            return false;
        } else if (startValue == '' || startValue == null) {
            p_notification(false, eb.getMessage('ERR_GIFCARD_START_VAL_CNT_BE_NULL'));
            $('#startValue').focus();
            return false;
        } else if (startValue < 0 || isNaN(startValue)) {
            p_notification(false, eb.getMessage('ERR_GIFCARD_START_VAL_SH_POSITIVE_AND_INTEGER'));
            $('#startValue').val('');
            $('#startValue').focus();
            return false;
        } else if (endValue == '' || endValue == null) {
            p_notification(false, eb.getMessage('ERR_GIFCARD_END_VAL_CNT_BE_NULL'));
            $('#endValue').focus();
            return false;
        } else if (endValue < 0 || isNaN(endValue)) {
            p_notification(false, eb.getMessage('ERR_GIFCARD_END_VAL_SH_POSITIVE_AND_INTEGER'));
            $('#endValue').val('');
            $('#endValue').focus();
            return false;
        } else if (giftCardDiscountPrecentage < 0 || isNaN(giftCardDiscountPrecentage)) {
            p_notification(false, eb.getMessage('ERR_GIFCARD_DISCOUNT_PRECENTAGE_SH_POSITIVE_AND_INTEGER'));
            $('#giftCardDiscountPrecentage').val('');
            $('#giftCardDiscountPrecentage').focus();
            return false;
        } else if (giftCardDurationDays < 0 || isNaN(giftCardDurationDays)) {
            p_notification(false, eb.getMessage('ERR_GIFCARD_DURATION_DAYS_SH_POSITIVE_AND_INTEGER'));
            $('#giftCardDurationDays').val('');
            $('#giftCardDurationDays').focus();
            return false;
        } else {
            return true;
        }
    }

    $("form.gift-card-search").submit(function(e) {
        e.preventDefault();
        var keyword = $('#giftCardSearch').val();
        var param = {searchGiftCardString: keyword};
        getViewAndLoad('/gift-card-api/getGiftCardForSearch', 'giftCardList', param);
        e.stopPropagation();
        return false;
    });

    $('.delete').on('click', function(e) {
        e.preventDefault();
        var serialData = {};
        $('tbody tr', '#rmGiftCard').each(function() {
            if ($(this).find('.serialCheck').is(':checked')) {
                serialData[this.id] = {
                    serialId: this.id,
                    serialCode: $(this).find('.serilaCode').text(),
                };
            }

        });
        if (!$.isEmptyObject(serialData)) {
            var formData = {
                serialData: serialData,
                giftCardId: $('#giftCardId').val(),
                ignoreBudgetLimit: ignoreBudgetLimitFlag
            }
            deleteGiftCards(formData);
        } else {
            p_notification(false, eb.getMessage('ERR_DOESENT_HAVE_GIFT_CARD'));
        }
    });
});

function deleteGiftCards(formData)
{
    eb.ajax({
        type: 'POST',
        url: BASE_URL + '/gift-card-api/deleteGiftCards',
        data: formData,
        success: function(respond) {
            if (respond.status == true) {
                p_notification(respond.status, respond.msg);
                window.location.reload();
            } else {
                if (respond.data == "NotifyBudgetLimit") {
                    bootbox.confirm(respond.msg+' ,Are you sure you want to delete ?', function(result) {
                        if (result == true) {
                            ignoreBudgetLimitFlag = true;
                            formData.ignoreBudgetLimit = true;
                            deleteGiftCards(formData);
                        } else {
                            setTimeout(function(){ 
                                location.reload();
                            }, 3000);
                        }
                    });
                } else {
                    p_notification(respond.status, respond.msg);
                }
            }
        }
    });
}


function productStatusChange(thisElem, productID, productName) {
    var state = !$('.glyphicon', thisElem).hasClass('glyphicon-check');
    var state_msg = (state) ? 'activate' : 'deactivate';
    bootbox.confirm("Are you sure you want to " + state_msg + " this Gift Card '" + productName + "'?", function(result) {
        if (result === true) {
            eb.ajax({
                type: 'POST',
                url: BASE_URL + '/gift-card-api/change-state/' + getCurrPage(),
                data: {productID: productID, productState: +state},
                success: function(data) {
                    p_notification(data.status, data.msg);
                    if (data.status == true) {
                        window.location.reload();
                    }
                }
            });
        }
    });
}