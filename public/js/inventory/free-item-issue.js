var ignoreBudgetLimitFlag = false;

function selectedItem(productID, quantity, productName, uomID) {
    this.productID = productID;
    this.quantity = quantity;
    this.productName = productName;
    this.selectedUom = uomID;
}
$(document).ready(function() {

    var majorItemId = null;
    var $freeItemTable = $('#addFreeIseueItemModal #ruleAddBody');
    var selectedProductID = '';
    var freeItemIssueDetailsID = null;
    var selectedFreeItems = {};
    var $addNewFreeItemRow = $('tr.free-new-item-add-row', $freeItemTable);
	
    $(document).on('click','.add-free-item', function(e){
        var thisRow = $(this).parents('tr');
        var itemName = thisRow.attr('data-product');
        var productID = thisRow.attr('data-id');

        majorItemId = productID;

        $('#addFreeIseueItemModal #itemNameCode').val(itemName);
        $('#addFreeIseueItemModal #itemNameCode').attr('disabled', true);
        // console.log(itemName);
        selectedFreeItems = {};
        getUomDetails(productID);
        $("#addFreeIseueItemModal #free-item-table tbody .addedRule").remove();

        $('#update-free-issue-items').addClass('hidden');
        $('#save-free-issue-items').removeClass('hidden');

        $('#addFreeIseueItemModal').modal('show');
    });


    $(document).on('click','.edit-free-item', function(e){
        var thisRow = $(this).parents('tr');
        var itemName = thisRow.attr('data-product');
        var productID = thisRow.attr('data-id');

        majorItemId = productID;
        selectedFreeItems = {};

        $('#addFreeIseueItemModal #itemNameCode').val(itemName);
        $('#addFreeIseueItemModal #itemNameCode').attr('disabled', true);

        $("#addFreeIseueItemModal #free-item-table tbody .addedRule").remove();
        $('#update-free-issue-items').removeClass('hidden');
        $('#save-free-issue-items').addClass('hidden');

        getFreeItemDetails(productID);
    });


    $("#addFreeIseueItemModal #free-item-table").on('click', '.delete-free-item', function(e) {
        e.preventDefault();
        var deleteMTrID = $(this).closest('tr').attr('id');

        bootbox.confirm('Are you sure you want to remove this free item ?', function(result) {
            if (result == true) {
                delete selectedFreeItems[deleteMTrID];
                $('#' + deleteMTrID).remove();
            }
        });
    });


    var locationID = $('#location').val();
    var documentType = 'promotion';
    loadDropDownFromDatabase('/productAPI/search-location-products-for-dropdown', locationID, 0, '#addFreeIseueItemModal #freeItemCode', '', '', documentType);
    $('#addFreeIseueItemModal #freeItemCode').selectpicker('refresh');


    $('#addFreeIseueItemModal #freeItemCode').on('change', function() {
        var locationID = $('#location').val();
        if ($(this).val() > 0) {
            selectedProductID = $(this).val();
            var hasVal = false;
            $.each(selectedFreeItems, function(key, valueObj) {
                if (valueObj['productID'] ==  selectedProductID) {
                    hasVal = true;
                }
            });

            if (hasVal) {
                resetAddFreeItemRow()
                p_notification(false, eb.getMessage("ERR_ITEM_ALREADY_IN_LIST"));
                return false;
            } else {
                eb.ajax({
                    type: 'POST',
                    url: BASE_URL + '/productAPI/getProductUomList',
                    data: {productID: $(this).val(), locationID: locationID},
                    success: function(respond) {
                        $("#addFreeIseueItemModal input[name='free-quantity']").val('').show().siblings('.uomqty,.uom-select').remove();
                        $("#addFreeIseueItemModal input[name='free-quantity']").addUom(respond.data).parent().addClass('input-group');
                    }
                });
            }


        }
    });

    function getUomDetails(productID) {

        var locationID = $('#location').val();
        if (productID > 0) {
            eb.ajax({
                type: 'POST',
                url: BASE_URL + '/productAPI/getProductUomList',
                data: {productID: productID, locationID: locationID},
                success: function(respond) {
                    $("#addFreeIseueItemModal input[name='majorQuantity']").val('').show().siblings('.uomqty,.uom-select').remove();
                    $("#addFreeIseueItemModal input[name='majorQuantity']").addUom(respond.data).parent().addClass('input-group');
                }
            });
        }
    }

    function getFreeItemDetails(productID) {

        var locationID = $('#location').val();
        if (productID > 0) {
            eb.ajax({
                type: 'POST',
                url: BASE_URL + '/productAPI/getFreeItemDetails',
                data: {productID: productID, locationID: locationID},
                success: function(respond) {

                    if (respond.status) {
                        freeItemIssueDetailsID = respond.data.freeIssueDetailID;
                        $("#addFreeIseueItemModal input[name='majorQuantity']").val('').show().siblings('.uomqty,.uom-select').remove();
                        $("#addFreeIseueItemModal input[name='majorQuantity']").val(respond.data.majorQuantity).addUom(respond.data.uomList).parent().addClass('input-group');
                        $("#addFreeIseueItemModal #conditionType").val(respond.data.majorConditionType);
                        $("#addFreeIseueItemModal #conditionType").selectpicker('refresh');
                        $.each(respond.data.relatedFreeItems, function(key, valueObj) {
                            addNewRow(valueObj);
                        });
                        $('#addFreeIseueItemModal').modal('show');
                    }

                }
            });
        }
    }

    function addNewRow(valueObj) {
        var randNum = Math.floor((Math.random() * 10000) + 1);
        var rowId = 'rule'+'_'+randNum;

        var newTrID = rowId;
        var clonedRow = $($('#addFreeIseueItemModal #tempRuleAddBody #rulePreSample').clone()).attr('id', newTrID).addClass('addedRule');
        $("input[name='productName']", clonedRow).val(valueObj['productName']);
        $("input[name='quantity']", clonedRow).val(parseFloat(valueObj['displayQty']));
        clonedRow.find("input[name='quantity']").prop('disabled', true);
        clonedRow.find("input[name='productName']").prop('disabled', true);
        clonedRow.find(".sign").html(valueObj['uomAbbr']);
        clonedRow.removeClass('hidden');
        clonedRow.insertBefore('#rulePreSample');
        selectedFreeItems[newTrID] = new selectedItem(valueObj['productID'], valueObj['quantity'], valueObj['productName'],valueObj['selectedUom']);
        resetAddFreeItemRow();
    }


    $('#searchItem').on('click', function() {
        $searchKey = $('#productSearch').val();
        if ($searchKey != '') {
            var keyword = $('#productSearch').val();
            var fixedAsset = false;
            
            var param = {searchProductString: $searchKey, fixedAssetFlag : fixedAsset};
            getViewAndLoad('/productAPI/getProductsForSearchForFreeIssue', 'productListView', param);
            return false;
        }

    });

    $('#search-reset').on('click', function() {
        window.location.reload();
    });


    $freeItemTable.on('click', 'button.add', function(e) {
        e.preventDefault();
        var quantity = $("input[name='free-quantity']", $addNewFreeItemRow).val();
        var productName = $("select[name='freeItemCode']", $addNewFreeItemRow).find('option:selected').text();
        var uomAbr = $("#addFreeIseueItemModal input[name='free-quantity']").siblings('.uom-select').find('.selected').html();
        var uomID = $("#addFreeIseueItemModal input[name='free-quantity']").siblings('.uom-select').find('.selected').data('uomID');
        if (selectedProductID == '') {
            p_notification(false, eb.getMessage('ERR_PROMO_SELECT_ITM_CHK'));
        } else if (quantity == '') {
            p_notification(false, eb.getMessage('ERR_ITEM_RULE_QTY_CHK'));
        } else {

            var randNum = Math.floor((Math.random() * 10000) + 1);
            var rowId = 'rule'+'_'+randNum;

            var newTrID = rowId;
            var clonedRow = $($('#addFreeIseueItemModal #tempRuleAddBody #rulePreSample').clone()).attr('id', newTrID).addClass('addedRule');
            $("input[name='productName']", clonedRow).val(productName);
            $("input[name='quantity']", clonedRow).val($addNewFreeItemRow.find('.uomqty').val());
            clonedRow.find("input[name='quantity']").prop('disabled', true);
            clonedRow.find("input[name='productName']").prop('disabled', true);
            console.log(uomAbr);
            clonedRow.find(".sign").html(uomAbr);
            clonedRow.removeClass('hidden');
            clonedRow.insertBefore('#rulePreSample');
            selectedFreeItems[newTrID] = new selectedItem(selectedProductID, quantity, productName,uomID);
            resetAddFreeItemRow();
        }
    });


    function resetAddFreeItemRow() {
        $("#addFreeIseueItemModal input[name='free-quantity']").val('').show().siblings('.uomqty,.uom-select').remove();
        $('#freeItemCode', $addNewFreeItemRow).val('0').trigger('change');
        $('#freeItemCode', $addNewFreeItemRow).text("Select Item");
        $('#freeItemCode', $addNewFreeItemRow).selectpicker('refresh');
        selectedProductID = "";
        selectedProduct = "";
    }


    $('#save-free-issue-items').on('click', function(){


        var formData = {
            majorProductID : majorItemId,
            majorItemCondition: $("#addFreeIseueItemModal #conditionType").val(),
            majorQuantity: $("#majorQuantity").val(),
            selectedMajorUom : $("#addFreeIseueItemModal input[name='majorQuantity']").siblings('.uom-select').find('.selected').data('uomID'),
            selectedProducts: selectedFreeItems,
        };


        if (validateFreeIssueItemData(formData)) {
            eb.ajax({
              type: 'POST',
              url: BASE_URL + '/productAPI/saveFreeIssueItemDetails',
              data: formData,
              success: function (respond) {
                p_notification(respond.status, respond.msg);                
                if(respond.status){
                    $('#addFreeIseueItemModal').modal('hide');
                    window.setTimeout(function() {
                        location.reload();
                    }, 1500);                    
                }                
            }
        });
      } 

    });

    $('#update-free-issue-items').on('click', function(){


        var formData = {
            freeItemIssueDetailsID: freeItemIssueDetailsID,
            majorProductID : majorItemId,
            majorItemCondition: $("#addFreeIseueItemModal #conditionType").val(),
            majorQuantity: $("#majorQuantity").val(),
            selectedMajorUom : $("#addFreeIseueItemModal input[name='majorQuantity']").siblings('.uom-select').find('.selected').data('uomID'),
            selectedProducts: selectedFreeItems,
        };


        if (validateFreeIssueItemData(formData)) {
            eb.ajax({
              type: 'POST',
              url: BASE_URL + '/productAPI/editFreeIssueItemDetails',
              data: formData,
              success: function (respond) {
                p_notification(respond.status, respond.msg);                
                if(respond.status){
                    $('#addFreeIseueItemModal').modal('hide');
                    window.setTimeout(function() {
                        location.reload();
                    }, 1500);                    
                }                
            }
        });
      } 

    });

    function validateFreeIssueItemData(data){
        if (data.majorItemCondition == '' || data.majorItemCondition == 0) {
            p_notification(false, eb.getMessage("ERR_MAJOR_CONDITION_EMPTY"));
            $("#serviceProvider").focus();
            return false;
        } else if (data.majorQuantity == 0 || data.majorQuantity == '') {
            p_notification(
              false,
              eb.getMessage("ERR_ITEMS_EMPTY_QTY")
            );
            return false;
        } else if (jQuery.isEmptyObject(data.selectedProducts)) {
            p_notification(
              false,
              eb.getMessage("ERR_EMPTY_FREE_ITEM_ISSUE_LIST")
            );
            return false;
        }
      return true;
    }

});