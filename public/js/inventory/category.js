// initialize global functions
var initializeCategoryTypeahead;
var createCategoryFormSubmitCallback = function() {
};

$(function() {
    var isWizard = $('#wizard-content').length ? $('#wizard-content').length : 0;
    removeHiddenAttr(isWizard);
    var createCategoryForm = $("#create-category-form");

    /**
     * Method to initialize Category Typeahead
     * @param {DOM Object} parentForm
     * @param {Map} pathsList
     * @param {Map} categoriesList
     * @returns {Dom Object} Initialized Typeahead Element
     */
    initializeCategoryTypeahead = function initializeCategoryTypeahead(parentForm, pathsList, categoriesList) {

        $("[name='categoryParentID']", parentForm).find("option[value!='']").remove();
        $.each(CATEGORIES_LIST, function(index, value) {
            var categorypath = (CATEGORIES_PATH_LIST[index]) ? CATEGORIES_PATH_LIST[index] : '';
            $("[name='categoryParentID']", parentForm).append($('<option value="' + index + '" data-subtext="' + categorypath + '">' + value + '</option>'));
        });

        $("[name='categoryParentID']", parentForm).selectpicker('refresh');

    }


    initializeCategoryTypeahead(createCategoryForm);

    createCategoryForm.submit(function(e) {

        var form_data = {
            categoryName: $("[name='categoryName']", this).val(),
            categoryPrefix: $("[name='categoryPrefix']", this).val(),
            categoryParentID: $("[name='categoryParentID']", this).val(),
            categoryState: $("[name='categoryState']", this).val(),
            isWizard: isWizard
        };
        if (($("[name='categoryParentID']", this).val()) == undefined || ($("[name='categoryParentID']", this).val()) == '') {
            form_data.categoryParentID = 0;
        } else if (CATEGORIES_PATH_LIST[form_data.categoryParentID] == undefined) {
            p_notification(false, eb.getMessage('ERR_CATEGORY_VALIDCATG'));
            return false;
        }

        var submitted_btn = $("[type='submit']:visible", $(this));
        var update = submitted_btn.hasClass('update');

        if (form_data.categoryName.trim() == "") {
            p_notification(false, eb.getMessage('ERR_CATEGORY_REQUIRE'));
            return false;
        } else if (form_data.categoryPrefix.length > 10) {
            p_notification(false, eb.getMessage('ERR_CATEGORY_PREFIX_LENGTH'));
            return false;
        } else if(update && $("[name='categoryParentID']", this).val() == $("[name='categoryName']", this).attr('cateid')){
            p_notification(false, eb.getMessage('ERR_CATEGORY_PARENT_SAME'));
            return false;
        }
        var URL = (update) ? 'update' : 'add';
        var verb = (update) ? 'updated' : 'added';
        if (update) {
            form_data = $.extend(form_data, {categoryID: $("[name='categoryID']", this).val()});
        }

        eb.ajax({
            url: '/api/company/category/' + URL + '/' + getCurrPage(),
            method: 'post',
            data: form_data,
            dataType: 'json',
            success: function(data) {
                if (data.status == true) {
                    $("#category-list").html(data.html);
                    removeHiddenAttr(isWizard);
                    var categorypath = (data.data.paths[data.data.categoryID]) ? data.data.paths[data.data.categoryID] : '';
                    if (!update) {
                        $("[name='categoryParentID']", createCategoryForm).append($('<option value="' + data.data.categoryID + '" data-subtext="' + categorypath + '">' + form_data.categoryName + '</option>'));
                        $("[name='categoryParentID']", createCategoryForm).selectpicker('refresh');
                    }
                    if ($("#create-product-form").length) {
                        $("[name='categoryParentID']", $("#create-product-form")).append($('<option value="' + data.data.categoryID + '" data-subtext="' + categorypath + '">' + form_data.categoryName + '</option>'));
                        $("[name='categoryParentID']", $("#create-product-form")).selectpicker('refresh');
                        $("[name='categoryParentID']", $("#create-product-form")).val(data.data.categoryID);
                        $("[name='categoryParentID']", $("#create-product-form")).selectpicker('render');
                    }
                    CATEGORIES_PATH_LIST = data.data['paths'];
                    CATEGORIES_LIST = data.data['categories'];
                    replaceParentIDWithName();
                    p_notification(true, data.msg);
                    $("button.reset", createCategoryForm).click();
                    createCategoryFormSubmitCallback(data);
                } else {
                    p_notification(false, data.msg);
                }
            }
        });

        e.stopPropagation();
        return false;
    });

    $("form.category-search").submit(function(e) {
        var form_data = {
            keyword: $("#category-search-keyword", this).val()
        };

        eb.ajax({
            url: '/api/company/category/search',
            method: 'post',
            data: form_data,
            dataType: 'json',
            success: function(data) {
                if (data.status == true) {
                    $("#category-list").html(data.html);
                    removeHiddenAttr(isWizard);
                    replaceParentIDWithName();
                } else {

                }
            }
        });

        e.stopPropagation();
        return false;
    });

    $("form.category-search button.reset").click(function(e) {
        $("#category-search-keyword").val('');
        $("form.category-search").submit();
    });

    $("#category-list").on('click', 'a.edit', function(e) {
        var categoryID = $(this).parents('tr').data('categoryrow');

        $(".category-form-container").addClass('update');
        $("html, body").animate({scrollTop: $(".category-form-container").offset().top - 80});

        eb.ajax({
            url: '/api/company/category/get',
            method: 'post',
            data: {categoryID: categoryID},
            dataType: 'json',
            success: function(data) {
                if (data.status == true) {
                    var categoryData = data.data;
                    var $editForm = $("#create-category-form");
                    $("[name='categoryName']", $editForm).val(categoryData.categoryName).focus();
                    $("[name='categoryName']", $editForm).attr("cateID", categoryData.categoryID);
                    $("[name='categoryParentID']", $editForm).val(categoryData.categoryParentID);
                    $("[name='categoryParentID']", $editForm).selectpicker('render');
                    $("[name='categoryParentIDHelper']", $editForm).val(CATEGORIES_PATH_LIST[categoryData.categoryParentID]);
                    $("[name='categoryPrefix']", $editForm).val(categoryData.categoryPrefix);
                    $("[name='categoryState']", $editForm).val(categoryData.categoryState);
                    $("[name='categoryID']", $editForm).val(categoryData.categoryID);
                } else {

                }
            }
        });

        e.stopPropagation();
        return false;
    })

    $(".button-set.update button.reset").click(function() {
        $(".category-form-container")
                .removeClass('update')
                .find("[name='categoryName']")
                .focus()
                .end()
                .find("[name='categoryParentID']").val(0)
                .change();
    });

    $("#category-list").on('click', 'a.statechange', function(e) {
        var categoryID = $(this).parents('tr').data('categoryrow');
        var state = !$('.glyphicon', this).hasClass('glyphicon-check');
        var state_msg = (state) ? 'activate' : 'deactivate';

        bootbox.confirm('Are you sure you want to ' + state_msg + ' this Category?', function(result) {
            if (result === true) {
                eb.ajax({
                    url: '/api/company/category/change-state/' + getCurrPage(),
                    method: 'post',
                    data: {categoryID: categoryID, categoryState: +state},
                    dataType: 'json',
                    success: function(data) {
                        p_notification(data.status, data.msg);
                        if (data.status == true) {
                            $("#category-list").html(data.html);
                            removeHiddenAttr(isWizard);
                            replaceParentIDWithName();
                        }
                    }
                });
            }
        });

        e.stopPropagation();
        return false;
    });

    $("#category-list").on('click', 'a.delete', function(e) {
        var category = $(this).parents('tr').data('categoryrow');

        bootbox.confirm('Are you sure you want to delete this Category?', function(result) {
            if (result === true) {
                eb.ajax({
                    url: '/api/company/category/delete/' + getCurrPage(),
                    method: 'post',
                    data: {categoryID: category},
                    dataType: 'json',
                    success: function(data) {
                        p_notification(data.status, data.msg);
                        if (data.status == true) {
                            $("#category-list").html(data.html);
                            CATEGORIES_PATH_LIST = data.data.paths;
                            CATEGORIES_LIST = data.data.categories;
                            removeHiddenAttr(isWizard);
                            initializeCategoryTypeahead(createCategoryForm, data.data.paths, data.data.categories);
                            replaceParentIDWithName();
                        }
                    }
                });
            }
        });

        e.stopPropagation();
        return false;
    });

    function replaceParentIDWithName() {
        $("#category-list tbody tr").each(function() {
            var pID = $(this).attr('data-categoryrow');
            var pName = (pID != 0) ? CATEGORIES_LIST[pID] : '-';
            var pNamePath = (pID != 0) ? CATEGORIES_PATH_LIST[pID] : '-';
            pNamePath = (pNamePath == false) ? '-' : pNamePath;
            $('td.cp', $(this)).text(pNamePath);
        });
    }

    //mobile view large search
    $(window).bind('ready resize', function() {
        if ($(window).width() < 480) {
            $('#category-search-keyword').css('margin-bottom', '15px');
            $('#category-search-keyword').parent().removeClass('input-group');
            $('#category-search-keyword').next().removeClass('input-group-btn');
        } else {
            $('#category-search-keyword').parent().addClass('input-group');
            $('#category-search-keyword').next().addClass('input-group-btn');
            $('#category-search-keyword').css('margin-bottom', '0');
        }
    });

    function removeHiddenAttr(isWizard) {
        if (isWizard == 0) {
            $('th').removeAttr('hidden');
            $('td').removeAttr('hidden');
        }
    }

    replaceParentIDWithName();
});