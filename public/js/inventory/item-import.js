var ignoreBudgetLimitFlag = false;
$(document).ready(function () {
    
    var selectedFieldArr = [];
    var obj = {};
    var subCategory = {};
    var subType = {};
    var subUom = {};
    var subUom1 = {};
    var subUom2 = {};
    var selectedUom = [];
    var subHandelingType = {};
    var subSerial = {};
    var subBatch = {};
    var subSalesDiscount = {};
    var subPurchaseDiscount = {};
    var mp = {};
    var pp = {};
    var sp = {};
    var cn = {};
    var fa = {};    
    var fileData = $('#fileData').val();
    var header = $('#fileHeader').val();
    var fileHandlerType = $('#fileHandlerType').val();
    var keyWordArr = {};    
    var categoryIdArr = (typeof CATEGORYLIST === 'undefined') ? [] : $.map( CATEGORYLIST, function(obj, index) {
        return obj.categoryId;
    });
    
    $('#error-div').fadeOut(5000);//hide 
    
    $('#itemLocationSelectBox').selectpicker({
        size: 4
    });
    
    $('select.ezBizFields').on('change', function ( event, modalEnable) {

        var $tr = $(this).closest('tr');
        var column = $tr.data('column');
        var selectedField = $(this).val();
        var enumModalStatus = (modalEnable === undefined) ? true : modalEnable;        
        var html = '';
        
        switch (selectedField) {
            case 'itemCode':
                $tr.find('.enum-modal-btn').addClass('hidden');
                break;
            case 'itemName':
                $tr.find('.enum-modal-btn').addClass('hidden');
                break;
            case 'itemCategory':
                var element = $('.ezbiz_select_field.sample').clone().removeClass('sample');
                $.each(CATEGORYLIST,function(i, v){
                    element.append($("<option>", { value: v.categoryId, html: v.categoryName }));
                });
                html = element.attr({id:'itemCategory'})[0];
                generateEnumeratorModal( column, 'itemCategory', fileData, header, false, enumModalStatus, {}, function (res){
                    if(res) {
                        $tr.find('.enum-modal-btn').removeClass('hidden');
                    }
                });
                break;
            case 'itemType':
                var element = $('.ezbiz_select_field.sample').clone().removeClass('sample');
                $.each(TYPELIST, function(i, v){
                    element.append($("<option>", { value: v.typeId, html: v.typeName }));
                });
                html = element.attr({id:'itemType'})[0];
                generateEnumeratorModal( column, 'itemType', fileData, header, false, enumModalStatus, {}, function (res){
                    if(res) {
                        $tr.find('.enum-modal-btn').removeClass('hidden');
                    }
                });
                break;
            case 'itemBarcode':
                var element = $('.ezbiz_text_field.sample').clone().removeClass('sample');
                html = element.attr({id:'itemBarcode',placeholder:'Item Barcode'})[0];
                $tr.find('.enum-modal-btn').addClass('hidden');
                break;
            case 'productDescription':
                var element = $('.ezbiz_text_area_field').clone();
                html = element.attr({id:'productDescription',placeholder:'Product discription'})[0];
                $tr.find('.enum-modal-btn').addClass('hidden');
                break;
            case 'hsCode':
                var element = $('.ezbiz_text_field.sample').clone().removeClass('sample');
                html = element.attr({id:'hsCode',placeholder:'HS Code'})[0];
                $tr.find('.enum-modal-btn').addClass('hidden');
                break;
            case 'rackId':
                $tr.find('.enum-modal-btn').addClass('hidden');
                break;
            case 'minInventoryLevel':
                var element = $('.ezbiz_text_field.sample').clone().removeClass('sample');
                html = element.attr({id:'minInventoryLevel',placeholder:'Min.Inventory Level'})[0];
                $tr.find('.enum-modal-btn').addClass('hidden');
                break;
            case 'reoderLevel':
                var element = $('.ezbiz_text_field.sample').clone().removeClass('sample');
                html = element.attr({id:'reoderLevel',placeholder:'Re-order Level'})[0];
                $tr.find('.enum-modal-btn').addClass('hidden');
                break;
            case 'sellPrice':
                var element = $('.ezbiz_text_field.sample').clone().removeClass('sample');
                html = element.attr({id:'sellPrice',placeholder:'Selling Price'})[0];
                $tr.find('.enum-modal-btn').addClass('hidden');
                break;
            case 'purPrice':
                var element = $('.ezbiz_text_field.sample').clone().removeClass('sample');
                html = element.attr({id:'purPrice', placeholder:'Purchasing Price'})[0];
                $tr.find('.enum-modal-btn').addClass('hidden');
                break;
            case 'batchProduct':
                var element = $('.ezbiz_select_field.sample').clone().removeClass('sample');
                $.each(FIELDS.batchProduct.list,function(i, v){
                    element.append($("<option>", { value: i, html: v }));
                });
                html = element.attr({id:'batchProduct'})[0];
                generateEnumeratorModal( column, 'batchProduct', fileData, header, false, enumModalStatus, {}, function (res){
                    if(res) {
                        $tr.find('.enum-modal-btn').removeClass('hidden');
                    }
                });
                break;
            case 'salesDiscount':
                var element = $('.ezbiz_select_field.sample').clone().removeClass('sample');
                $.each(FIELDS.salesDiscount.list,function(i, v){
                    element.append($("<option>", { value: i, html: v }));
                });
                html = element.attr({id:'salesDiscount'})[0];
                break;
            case 'purchaseDiscount':
                var element = $('.ezbiz_select_field.sample').clone().removeClass('sample');
                $.each(FIELDS.purchaseDiscount.list,function(i, v){
                    element.append($("<option>", { value: i, html: v }));
                });
                html = element.attr({id:'purchaseDiscount'})[0];
                break;
            case 'serialProduct':
                var element = $('.ezbiz_select_field.sample').clone().removeClass('sample');
                $.each(FIELDS.serialProduct.list,function(i, v){
                    element.append($("<option>", { value: i, html: v }));
                });
                html = element.attr({id:'serialProduct'})[0];
                generateEnumeratorModal( column, 'serialProduct', fileData, header, false, enumModalStatus, {}, function (res){
                    if(res) {
                        $tr.find('.enum-modal-btn').removeClass('hidden');
                    }
                });
                break;            
            case 'uomId':
                var element = $('.ezbiz_select_field.sample').clone().removeClass('sample');
                $.each(UOMLIST,function(i, v){
                    element.append($("<option>", { value: v.uomId, html: v.uomName }));
                });
               html = element.attr({id:'uomId'})[0];
                generateEnumeratorModal( column, 'uomId', fileData, header, false, enumModalStatus, {}, function (res){
                    if(res) {
                        $tr.find('.enum-modal-btn').removeClass('hidden');
                    }
                });
                break; 
            case 'uom1Id':
                var element = $('.ezbiz_select_field.sample').clone().removeClass('sample');
                $.each(UOMLIST,function(i, v){
                    element.append($("<option>", { value: v.uomId, html: v.uomName }));
                });
                html = element.attr({id:'uom1Id'})[0];
                generateEnumeratorModal( column, 'uom1Id', fileData, header, false, enumModalStatus, {}, function (res){
                    if(res) {
                        $tr.find('.enum-modal-btn').removeClass('hidden');
                    }
                });
                break;
            case 'uom2Id':
                var element = $('.ezbiz_select_field.sample').clone().removeClass('sample');
                $.each(UOMLIST,function(i, v){
                    element.append($("<option>", { value: v.uomId, html: v.uomName }));
                });
                html = element.attr({id:'uom2d'})[0];
                generateEnumeratorModal( column, 'uom2Id', fileData, header, false, enumModalStatus, {}, function (res){
                    if(res) {
                        $tr.find('.enum-modal-btn').removeClass('hidden');
                    }
                });
                break; 
            case 'productHandelingManufactureProduct':
            case 'productHandelingPurchaseProduct':
            case 'productHandelingSalesProduct':
            case 'productHandelingConsumables':
            case 'productHandelingFixedAssets':
                var element = $('.ezbiz_select_field.sample').clone().removeClass('sample');
                $.each(FIELDS.productHandelingType.subList,function(i, v){
                    element.append($("<option>", { value: i, html: v }));
                });
                html = element.attr({id:selectedField})[0];
                generateEnumeratorModal( column, selectedField, fileData, header, false, enumModalStatus, {}, function (res){
                    if(res) {
                        $tr.find('.enum-modal-btn').removeClass('hidden');
                    }
                });
                break;
            case 'stockInHand':
                var element = $('.ezbiz_select_field.sample').clone().removeClass('sample');
                $.each(LOCATION,function(i, v){
                    element.append($("<option>", { value: i, html: v }));
                });
                html = element.attr({id:selectedField})[0];
                break;
            default :
                $tr.find('.enum-modal-btn').addClass('hidden');
        }
        
        
        selectedFieldArr = [];
        $('.ezBizFields').each(function(index,element){
            var selectedValue = $(this).val();
            if(selectedValue && $.inArray(selectedValue,selectedFieldArr) == -1){
                selectedFieldArr.push(selectedValue);
            }
        }).find('option').attr('disabled', false);
        
        $('.ezBizFields option[value="uom1Conversion"]:not(:selected)').attr("disabled", true);
        $('.ezBizFields option[value="uom2Conversion"]:not(:selected)').attr("disabled", true);

        $('.mandatory-field').removeClass('hidden');        
        $('#mandatory-fields-table tr').removeClass('hidden');
        $('#mandatory-fields-table tr').attr('data-status','1');
        
        $.each( selectedFieldArr, function( index, value){
            $('.ezBizFields option[value="'+value+'"]:not(:selected)').attr("disabled", true);

            if(value == "uom1Id"){
                $('.ezBizFields option[value="uom1Conversion"]:not(:selected)').attr("disabled", false);
            }

            if(value == "uom2Id"){
                $('.ezBizFields option[value="uom2Conversion"]:not(:selected)').attr("disabled", false);
            }
            //for hide mandatory fields
            if(Object.keys(FIELDS.productHandelingType.list).indexOf(value) !== -1){
                value = 'productHandelingType';
            }
            
            var $mandatoryTr = $('#mandatory-fields-table tr[data-field="'+value+'"]');
            $mandatoryTr.addClass('hidden');
            $mandatoryTr.attr('data-status','0');
        });

        if(selectedFieldArr.includes("uom1Id") || selectedFieldArr.includes("uom2Id")){
            var $mandatoryTr = $('#mandatory-fields-table tr[data-field="displayUom"]');
            $mandatoryTr.removeClass('hidden');
            $mandatoryTr.attr('data-status','1');
        }else{
            var $mandatoryTr = $('#mandatory-fields-table tr[data-field="displayUom"]');
            $mandatoryTr.addClass('hidden');
            $mandatoryTr.attr('data-status','0');
        }

        $tr.find('.defaultValue').html(html);
    });
    
    //for auto select
    FIELDS = (typeof FIELDS === 'undefined') ? [] : FIELDS;
    $.each( FIELDS, function( index, value){        
        keyWordArr[index] = value.key_words;
    });

    $('#product-mapping-table tbody>tr').each(function(){
        var $tr = $(this);
        var text = $tr.children('td:first').text().trim().toLowerCase();

        $.each( keyWordArr, function( field, keyWords){
            if(keyWords.indexOf(text) != -1){                
                if($.inArray(field,selectedFieldArr) == -1){//if not selected
                    $tr.find('.ezBizFields').val(field).trigger('change',[false]);
                }
            }
        });       
    });
    
    //for enable / disable locations
    $("input[type=radio][name=itemLocation]").on( 'change', function (){
        var value = $(this).val();
        var isLocalProduct = (value != 'local');
        $('#itemLocationSelectBox').attr('disabled', isLocalProduct);
        $('#itemLocationSelectBox').selectpicker('refresh');
    });
    
    //for item import btn
    $('#importBtn').click( function (){
        obj = {};
        var productHandelingTypeArr = $('input:checkbox:checked.productHandelingType').map(function () {
            return this.value;
        }).get();
        selectedUom = [];
        //get mapped fields
        $.each( selectedFieldArr, function( index, value){
            var $tr = $('.ezBizFields option:selected[value="'+value+'"]').closest('tr');            
            var column = $tr.data('column');
            var temp = {};
            temp.column = column;
            temp.defaultValue = $('#'+value).val();
            
            switch (value) {
                case 'itemType':
                    temp.subset = subType;
                    break;
                case 'itemCategory':
                    temp.subset = subCategory;
                    break;
                case 'uomId':
                    temp.subset = subUom;
                    selectedUom.push(Object.values(subUom)[0]);
                    break;
                case 'uom1Id':
                    temp.subset = subUom1;
                    selectedUom.push(Object.values(subUom1)[0]);
                    break;
                case 'uom2Id':
                    temp.subset = subUom2;
                    selectedUom.push(Object.values(subUom2)[0]);
                    break;
                case 'serialProduct':
                    temp.subset = subSerial;
                    break;
                case 'batchProduct':
                    temp.subset = subBatch;
                    break;
                case 'salesDiscount':
                    temp.subset = subSalesDiscount;
                    break;
                case 'purchaseDiscount':
                    temp.subset = subPurchaseDiscount;
                    break;
                case 'productHandelingManufactureProduct':
                    productHandelingTypeArr.push(value);
                    temp.subset = mp;
                    break;
                case 'productHandelingPurchaseProduct':
                    productHandelingTypeArr.push(value);
                    temp.subset = pp;
                    break;
                case 'productHandelingSalesProduct':
                    productHandelingTypeArr.push(value);
                    temp.subset = sp;
                    break;
                case 'productHandelingConsumables':
                    productHandelingTypeArr.push(value);
                    temp.subset = cn;
                    break;
                case 'productHandelingFixedAssets':
                    productHandelingTypeArr.push(value);
                    temp.subset = fa;
                    break;
            }            
            obj[value] = temp;
        });
        
        //mandatory fields
        $('#mandatory-fields-table tr[data-status="1"]').each(function( index, element){
            $tr = $(this);
            var field = $tr.data('field');
            switch (field){
                case 'itemName':
                    obj.itemName = {
                        column : null,
                        defaultValue : $('#mandatoryItemName').val()
                    };
                    break;
                case 'itemType':
                    obj.itemType = {
                        column : null,
                        defaultValue : $('#mandatoryItemType').val(),
                        subset : {}
                    };
                    break;
                case 'itemCategory':
                    obj.itemCategory = {
                        column : null,
                        defaultValue : $('#mandatoryItemCategory').val(),
                        subset : {}
                    };
                    break;
                case 'uomId':
                    obj.uomId = {
                        column : null,
                        defaultValue : $('#mandatoryUomId').val(),
                        subset : {}
                    };
                    break;
                case 'productHandelingType':
                    $.each( productHandelingTypeArr, function ( index, value) {
                        obj[value] = {
                            column : null,
                            defaultValue : 1,
                            subset : {}
                        };
                    });
                    break;
                case 'displayUom':
                    obj.displayUom = {
                        column : null,
                        defaultValue : $('#mandatoryDisplayUom').val()
                    };
                    break;
            }
        });        
        obj.productHandelingTypes = productHandelingTypeArr;
        var locationType = $("input:radio[name=itemLocation]:checked").val();
        
        var location = {};
        location.type = locationType;
        location.list = (locationType == 'local') ? $('#itemLocationSelectBox').val() : null;
        
        if(validateInput(obj,location)){
            eb.ajax({
                type: 'POST',
                url: BASE_URL + '/product/import-simulate',
                data: { 
                    mapData : obj,
                    location : location,
                    fileData : fileData,
                    header : header,
                    fileHandlerType : fileHandlerType
                },
                success: function(data) {
                    if (data.status == true) {
                        $('#simulateModalBody').html(data.html);
                        $('#simulateModal').modal('show');
                        if(data.data.error.length > 0){
                            $('#continueBtn').addClass('hidden');
                        } else {
                            $('#continueBtn').removeClass('hidden');
                        }
                    } else {
                        p_notification(false, data.msg);
                    }
                }
            });            
        }        
    });
    
    //for save sub set values
    $('#enumeratorModal').on( 'click', '.enum-save-btn', function(){
        var subSet = {}; 
        var field = $(this).data('field');
        var valuePair = "";
        $('#enumeratorModal .enum-tr').each(function( index, element){
            var $tr = $(this);            
            var key = $tr.data('id');
            var value = $tr.find('.enumSelector').val();
            valuePair = value;
            if(value == 0){//for new sub set
                value = 'new_category-'+key;
            }
            subSet[key] = value;
        });
        
        switch (field) {
            case 'itemType':
                subType = subSet;
                break;
            case 'itemCategory':
                subCategory = subSet;
                break;
            case 'uomId':
                subUom = subSet;
                break;
            case 'uom1Id':
                subUom1 = subSet;
                break;
            case 'uom2Id':
                subUom2 = subSet;
                break;
            case 'serialProduct':
                subSerial = subSet;
                break;
            case 'batchProduct':
                subBatch = subSet;
                break;
            case 'salesDiscount':
                subSalesDiscount = subSet;
                break;
            case 'purchaseDiscount':
                subPurchaseDiscount = subSet;
                break;
            case 'productHandelingManufactureProduct':
                mp = subSet;
                break;
            case 'productHandelingPurchaseProduct':
                pp = subSet;
                break;
            case 'productHandelingSalesProduct':
                sp = subSet;
                break;
            case 'productHandelingConsumables':
                cn = subSet;
                break;
            case 'productHandelingFixedAssets':
                fa = subSet;
                break;
        }
        $('#enumeratorModal').modal('hide');
    });
    
    //for toggle enum modal 
    $('.enum-modal-btn').on( 'click', function (){
        var $tr = $(this).closest('tr');
        var columnId = $tr.data('column');
        var field = $tr.find('.ezBizFields').val();
        var subSet;
        
        switch (field) {
            case 'itemType':
                subSet = subType;
                break;
            case 'itemCategory':
                subSet = subCategory;
                break;
            case 'uomId':
                subSet = subUom;
                break;
            case 'uom1Id':
                subSet = subUom1;
                break;
            case 'uom2Id':
                subSet = subUom2;
                break;
            case 'productHandelingType':
                subSet = subHandelingType;
                break;
            case 'serialProduct':
                subSet = subSerial;
                break;
            case 'batchProduct':
                subSet = subBatch;
                break;
            case 'salesDiscount':
                subSet = subSalesDiscount;
                break;
            case 'purchaseDiscount':
                subSet = subPurchaseDiscount;
                break;
            case 'productHandelingManufactureProduct':
                subSet = mp;
                break;
            case 'productHandelingPurchaseProduct':
                subSet = pp;
                break;
            case 'productHandelingSalesProduct':
                subSet = sp;
                break;
            case 'productHandelingConsumables':
                subSet = cn;
                break;
            case 'productHandelingFixedAssets':
                subSet = fa;
                break;
        }
        generateEnumeratorModal( columnId, field, fileData, header, true, true, subSet, function(){});
        $('#enumeratorModal').modal('show');
    });
    
    //for start item upload process
    $('#simulateModal').on('click','#continueBtn', function (){
        finalizeItemImport();        
    });
    
    function finalizeItemImport()
    {
        var items = $('#dataBucket').val();
        var stockInHandLocation = ($('.ezBizFields option:selected[value="stockInHand"]').length > 0) ? $('#stockInHand').val() : null;
        eb.ajax({
            type: 'POST',
            url: BASE_URL + '/product/finalize-item-import',
            data: { items : items, fileHandlerType : fileHandlerType, stockInHandLocation : stockInHandLocation, ignoreBudgetLimit: ignoreBudgetLimitFlag },
            success: function(data) {
                if (data.status == true) {  
                    $('#simulateModal').modal('hide');
                    $('#finalizeModalBody').html(data.html);
                    $('#finalizeModal').modal('show');
                } else {
                    if (data.data == "NotifyBudgetLimit") {
                        bootbox.confirm(data.msg+' ,Are you sure you want to save this invoice ?', function(result) {
                            if (result == true) {
                                ignoreBudgetLimitFlag = true;
                                finalizeItemImport();
                            } else {
                                setTimeout(function(){ 
                                    location.reload();
                                }, 3000);
                            }
                        });
                    } else {
                        p_notification(false, data.msg);
                    }
                }
            }
        });
    }

    $('#productUploadCancelBtn').on( 'click', function (){
        location.reload();
    });
    
    $('#cancelBtn,#finishBtn').on( 'click', function (){
        window.location.href = BASE_URL + '/product/item-import';
    });
    
    function generateEnumeratorModal( columnId, field, fileData, header, isUpdate, enumModalStatus, obj, callback){
        $.ajax({
            type: 'POST',
            url: BASE_URL + '/product/enumerate-modal',
            data: {columnId: columnId, field: field, fileData: fileData, header: header},
            success: function(data) {
                if (data.status == true) {
                    if(parseInt(data.data) === 0){
                        $('.ezBizFields option:selected[value="'+field+'"]').val("").trigger('change');
                        p_notification(false, eb.getMessage('ERR_UPLOADED_FILE_EMPTY'));
                        return;
                    }
                    $('#enumeratorModalBody').html(data.html);
                    if(isUpdate){
                        $.each(obj, function( key, value){
                            if(field === 'itemCategory' && $.inArray(value, categoryIdArr) === -1){
                                value = 0;
                            }
                            $('#enumeratorModal tr[data-id="'+key+'"]').find('.enumSelector').val(value);
                        });                   
                    }
                    $('#enumeratorModal').find('.enum-save-btn').data('field',field);
                    if(enumModalStatus){
                        $('#enumeratorModal').modal('show');
                    }                
                    callback(true);                
                } else {
                    p_notification(false, data.msg);
                    callback(false);
                }
            }
        });
    }

    //for validate input
    function validateInput(obj,location) {  
        if(!obj.hasOwnProperty('itemCode')){
            p_notification( false, eb.getMessage('ERR_ITEM_CODE_NOT_SELECTED'));
            return false;
        } else if(!obj.hasOwnProperty('itemName')) {
            p_notification( false, eb.getMessage('ERR_ITEM_NAME_NOT_SELECTED'));
            return false;
        } else if(!obj.itemType.column && !obj.itemType.defaultValue) {
            p_notification( false, eb.getMessage('ERR_DEFAULT_ITEM_TYPE_NOT_SET'));
            return false;
        } else if(obj.itemType.column && Object.keys(obj.itemType.subset).length === 0) {
            p_notification( false, eb.getMessage('ERR_SUB_TYPE_NOT_SET'));
            return false;
        } else if(!obj.itemCategory.column && !obj.itemCategory.defaultValue) {
            p_notification( false, eb.getMessage('ERR_DEFAULT_ITEM_CAT_NOT_SET'));
            return false;
        } else if(obj.itemCategory.column && Object.keys(obj.itemCategory.subset).length === 0) {
            p_notification( false, eb.getMessage('ERR_SUB_CAT_NOT_SET'));
            return false;
        } else if(!obj.uomId.column && !obj.uomId.defaultValue) {
            p_notification( false, eb.getMessage('ERR_DEFAULT_ITEM_UOM_NOT_SET'));
            return false;
        } else if(obj.uomId.column && Object.keys(obj.uomId.subset).length === 0) {
            p_notification( false, eb.getMessage('ERR_SUB_UOM_NOT_SET'));
            return false;
        } else if(obj.hasOwnProperty('serialProduct') && obj.serialProduct.column && Object.keys(obj.serialProduct.subset).length === 0) {
            p_notification( false, eb.getMessage('ERR_SUB_SERIAL_NOT_SET'));
            return false;
        } else if(obj.hasOwnProperty('batchProduct') && obj.batchProduct.column && Object.keys(obj.batchProduct.subset).length === 0) {
            p_notification( false, eb.getMessage('ERR_SUB_BATCH_NOT_SET'));
            return false;
        } else if(location.type === 'local' && location.list === null) {
            p_notification( false, eb.getMessage('ERR_ITEM_IMPORT_LOCATION_NOT_SELECTED'));
            return false;
        } else if(obj.hasOwnProperty('stockInHand') && location.type === 'local' && location.hasOwnProperty('list') && $.inArray( obj.stockInHand.defaultValue, location.list) === -1) {
            p_notification( false, eb.getMessage('ERR_ITEM_IMPORT_STOCK_IN_LOCATION_NOT_SELECTED'));
            return false;
        } else if(!isValidReorderLevel(obj)) {
            return false;
        } else if(obj.hasOwnProperty('uom1Id') && !obj.hasOwnProperty('uom1Conversion')) {
            p_notification( false, eb.getMessage('ERR_ITEM_IMPORT_UOM1_WITH_UOM1CONVERSION_NOT_SET'));
            return false;
        } else if(obj.hasOwnProperty('uom2Id') && !obj.hasOwnProperty('uom2Conversion')) {
            p_notification( false, eb.getMessage('ERR_ITEM_IMPORT_UOM2_WITH_UOM2CONVERSION_NOT_SET'));
            return false;
        }else if((obj.hasOwnProperty('uomId') || obj.hasOwnProperty('uom1Id') || obj.hasOwnProperty('uom2Id')) && checkDuplicate(selectedUom)){
            p_notification( false, eb.getMessage('ERR_ITEM_IMPORT_MULTIPLE_UOM'));
            return false;
        } else if((obj.hasOwnProperty('uom1Id') || obj.hasOwnProperty('uom2Id')) && !obj.displayUom.defaultValue) {
            p_notification( false, eb.getMessage('ERR_DISPLAY_UOM_TYPE_NOT_SET'));
            return false;
        } else {
            return isValidProductHandelingType(obj);
        }

    }

    function checkDuplicate(array) {
        let result = false;
        const s = new Set(array);

        // compare the size of array and Set
        if(array.length !== s.size){
           result = true;
        }
        return result;
     }

    function isValidReorderLevel(obj) {
        if (!obj.hasOwnProperty('reoderLevel') || !obj.reoderLevel.defaultValue) {
            return true;
        }

        if (obj.reoderLevel.defaultValue) {
            if (!obj.hasOwnProperty('minInventoryLevel') || !obj.minInventoryLevel.defaultValue) {
                p_notification( false, eb.getMessage('ERR_ITEM_MIN_INV_MANDATORY'));
                return false;
            }

            if (obj.minInventoryLevel.defaultValue > obj.reoderLevel.defaultValue) {
                p_notification(false, eb.getMessage('ERR_ITEM_MIN_INV_LOWER'));
                return false;
            }
        }
        return true;
    }

    function isValidProductHandelingType(obj) {
        if(!obj.hasOwnProperty('productHandelingTypes') || obj.productHandelingTypes.length === 0){
            p_notification( false, eb.getMessage('ERR_ITEM_HANDLING_TYPE_NOT_SELECTED'));
            return false;
        } else {        
            var statusArr = [];
            $.each( obj.productHandelingTypes, function ( index, field){
                var hType = obj[field];            
                if(hType.column && $.isEmptyObject(hType.subset)){
                    statusArr.push(field);
                }
            });
            if(statusArr.length > 0){
                p_notification( false, 'please map "'+statusArr[0]+'" handling type products.');
                return false;
            }
            return true;
        }
    }

});

