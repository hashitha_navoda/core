$(document).ready(function(){
	var allLocations = false;
	var allProducts = false;
	var soFlag = false;
	var poFlag = false;
	var reOdrFlag = false;
    var jobReqFlag = false;
	var thisValue;
    var reqPoDataSet = {};
    var filterBySup = false;
    var filterByCat = false;

	loadDropDownFromDatabase('/productAPI/search-user-active-location-products-for-dropdown',
		 "", 0, '#product', "", true);
    $('#product').on('change', function() {
        thisValue = $(this).val();
    });

    ///////////////////////
    //data range
    var nowTemp = new Date();
    var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);
    var checkin = $('#selected_from_date').datepicker({
        onRender: function(date) {
            return date.valueOf() < now.valueOf() ? ((U_DATE_R_OVERRIDE)?'':'disabled') : '';
        }}).on('changeDate', function(ev) {
        if (ev.date.valueOf() > checkout.date.valueOf()) {
            var newDate = new Date(ev.date)
            newDate.setDate(newDate.getDate() + 1);
            checkout.setValue(newDate);
        }
        checkin.hide();
    }).data('datepicker');
    checkin.setValue(now);
    var checkout = $('#selected_to_date').datepicker({
    }).on('changeDate', function(ev) {
        if (ev.date.valueOf() < checkin.date.valueOf()) {
            p_notification(false, eb.getMessage('ERR_MRP_AFT_ISSDATE'));
            $(this).val('');
        }
        checkout.hide();
    }).data('datepicker');
    ///////////////////////

    $('#allLocChecked').on('click', function(){
        if ($('#allLocChecked').is(":checked")){
            $('#location').val('').trigger('change');
            $('#location').attr('disabled', true);
            allLocations = true;
        } else {
            $('#location').attr('disabled', false);
            allLocations = false;
        }
    });
    $('#allProSelected').on('click', function(){
        if ($('#allProSelected').is(":checked")){
            $('#product').val('').trigger('change');
            $('#product').attr('disabled', true);
            allProducts = true;
        } else {
            allProducts = false;
            $('#product').attr('disabled', false);
        }
    });


    $('#mRFSubmit').on('click', function(e){
    	e.preventDefault();

        allLocations = ($('#allLocChecked').is(":checked")) ? true : false;
        allProducts = ($('#allProSelected').is(":checked")) ? true : false;
        soFlag = ($('#salesOrdWiseTick').is(":checked")) ? true : false;
        poFlag = ($('#purchaseOrdWiseTick').is(":checked")) ? true : false;
        reOdrFlag = ($('#reOrdLvlWiseTick').is(":checked")) ? true : false;
        jobReqFlag = ($('#jobReqQtyTick').is(":checked")) ? true : false;
        filterBySup = ($('#potentialSuppTick').is(":checked")) ? true : false;
        filterByCat = ($('#productCatTick').is(":checked")) ? true : false;

        var locations = $('#location').val();
        
        var dataSet = {
        	allLocFlag: allLocations,
        	allProFlag: allProducts,
        	location: locations,
        	product: thisValue,
        	fromDate: $('#selected_from_date').val(),
            toDate: $('#selected_to_date').val(),
        	soFlag: soFlag,
        	poFlag: poFlag,
        	reOdrFlag: reOdrFlag,
            jobReqFlag: jobReqFlag,
            filterBySup: filterBySup,
            filterByCat: filterByCat
        }
        var validateFlag = validate(dataSet);
        if(validateFlag){
            var url = '/inventory-material-requisition/load-data';
            eb.ajax({
                url: url,
                method: 'POST',
                data: dataSet,
                dataType: 'json',
                success: function(data) {
                    if(data.status){
                        addDataColumnToView(data.data, filterBySup, filterByCat);    
                    }
                }
            });
        }
    });

    $('#createAllPo').on('click', function(){

        if (Object.keys(reqPoDataSet).length == 0) {
            p_notification(false, eb.getMessage('ERR_NO_ANY_ITME_SELECT'));
            return false;
        }

        var url = '/inventory-material-requisition/create-po';
            eb.ajax({
                url: url,
                method: 'POST',
                data: {data: reqPoDataSet},
                dataType: 'json',
                success: function(data) {
                    if(data.status){
                        p_notification(true, 'Draft purchasing order has been successfully saved.');
                        setTimeout(function() {
                            var url = BASE_URL + '/po/list';
                            window.location.assign(url);
                        }, 2000)
                          
                    }
                }
            });
    });



    $('#dataRecordsTable tbody').on('click', '.add-po-btn', function(e){
        e.preventDefault();
        var proValue = ($(this).parents('tr').attr('id')).split('_');
        var productID = proValue[1];
        var qty = $(this).parents('tr').find('.reQty').html();
        var url = BASE_URL + '/po/'+productID+'/'+qty;
        window.location.assign(url);
       
    });

    $('#dataRecordsTable tbody').on('click', '.select-po-btn', function(e){

        e.preventDefault();
        if ($(this).hasClass('fa-square-o')) {
            $(this).removeClass('fa-square-o');
            $(this).addClass('fa-check-square-o');
            var proValue = ($(this).parents('tr').attr('id')).split('_');
            var productID = proValue[1];
            var reqQty = $(this).parents('tr').find('.reQty').html();
            var supID = $(this).attr('supid');
            reqPoDataSet[productID] = {
                proID: productID,
                reqQty: reqQty,
                supID: supID
            }
        } else {
            $(this).addClass('fa-square-o');
            $(this).removeClass('fa-check-square-o');
            var proValue = ($(this).parents('tr').attr('id')).split('_');
            var productID = proValue[1];
            var reqQty = $(this).parents('tr').find('.reQty').html();
            delete reqPoDataSet[productID];
        }  
    });







    function addDataColumnToView(data, filterByCat, filterByCat)
    {
        $('#data-genarate-section').removeClass('hidden');
        $('#dataRecordsTable tbody tr.cloned').remove();
        $('#allPoCreation').addClass('hidden');
        // reqPoDataSet = [];
        $.each(data, function(key, value){
            // console.log(filterByCat);
            // console.log(filterBySup);
            if ( (filterByCat && !filterBySup) ||(!filterByCat && filterBySup) ) {
                for (var i = 0; i < value.length; i++) {
                    var newRow = $('#dataRecordsTable tbody tr.sample').clone()
                        .removeClass('sample hidden dataRow').addClass('cloned').attr('id','clonedRow_'+value[i].proID).appendTo('#dataRecordsTable tbody');
                    $('#clonedRow_'+value[i].proID).find('.proCode').html(value[i].proCode +'-'+ value[i].proName);
                    $('#clonedRow_'+value[i].proID).find('.supplier').html(value[i].potentialSupplier);
                    $('#clonedRow_'+value[i].proID).find('.category').html(value[i].proCat);
                    $('#clonedRow_'+value[i].proID).find('.stkInHnd').html((parseFloat(value[i].availableQty)).toFixed(2));
                    $('#clonedRow_'+value[i].proID).find('.soQty').html((parseFloat(value[i].soQty)).toFixed(2));
                    $('#clonedRow_'+value[i].proID).find('.poQty').html((parseFloat(value[i].poQty)).toFixed(2));
                    $('#clonedRow_'+value[i].proID).find('.minInvLvl').html((parseFloat(value[i].minQty)).toFixed(2));
                    $('#clonedRow_'+value[i].proID).find('.jobQty').html((parseFloat(value[i].jobQty)).toFixed(2));
                    $('#clonedRow_'+value[i].proID).find('.reQty').html(value[i].neededQty);
                    $('#clonedRow_'+value[i].proID).find('.addPOqty').html((parseFloat(value[i].poAddQty)).toFixed(2));
                    if(value[i].neededQty > 0){
                        $('#clonedRow_'+value[i].proID).find('.select-po-btn').removeClass('hidden');
                        $('#clonedRow_'+value[i].proID).find('.select-po-btn').addClass('fa-square-o');
                        var sup = (value[i].potentialSupID != null) ?  value[i].potentialSupID : 0;
                        $('#clonedRow_'+value[i].proID).find('.select-po-btn').attr('supID', sup);
                        $('#allPoCreation').removeClass('hidden');
                        // reqPoDataSet[value[i].proID] = {
                        //     proID: value[i].proID,
                        //     reqQty: value[i].neededQty,
                        // }
                    }
                }

            } else if (filterByCat && filterBySup) {
                $.each(value, function(key1, val1){
                    for (var i = 0; i < val1.length; i++) {
                        var newRow = $('#dataRecordsTable tbody tr.sample').clone()
                            .removeClass('sample hidden dataRow').addClass('cloned').attr('id','clonedRow_'+val1[i].proID).appendTo('#dataRecordsTable tbody');
                        $('#clonedRow_'+val1[i].proID).find('.proCode').html(val1[i].proCode +'-'+ val1[i].proName);
                        $('#clonedRow_'+val1[i].proID).find('.supplier').html(val1[i].potentialSupplier);
                        $('#clonedRow_'+val1[i].proID).find('.category').html(val1[i].proCat);
                        $('#clonedRow_'+val1[i].proID).find('.stkInHnd').html((parseFloat(val1[i].availableQty)).toFixed(2));
                        $('#clonedRow_'+val1[i].proID).find('.soQty').html((parseFloat(val1[i].soQty)).toFixed(2));
                        $('#clonedRow_'+val1[i].proID).find('.poQty').html((parseFloat(val1[i].poQty)).toFixed(2));
                        $('#clonedRow_'+val1[i].proID).find('.minInvLvl').html((parseFloat(val1[i].minQty)).toFixed(2));
                        $('#clonedRow_'+val1[i].proID).find('.jobQty').html((parseFloat(val1[i].jobQty)).toFixed(2));
                        $('#clonedRow_'+val1[i].proID).find('.reQty').html(val1[i].neededQty);
                        $('#clonedRow_'+val1[i].proID).find('.addPOqty').html((parseFloat(val1[i].poAddQty)).toFixed(2));
                        if(val1[i].neededQty > 0){
                            $('#clonedRow_'+val1[i].proID).find('.select-po-btn').removeClass('hidden');
                            var sup = (val1[i].potentialSupID != null) ?  val1[i].potentialSupID : 0;
                            $('#clonedRow_'+val1[i].proID).find('.select-po-btn').addClass('fa-square-o');
                            $('#clonedRow_'+val1[i].proID).find('.select-po-btn').attr('supID', sup);
                            $('#allPoCreation').removeClass('hidden');
                            // reqPoDataSet[val1[i].proID] = {
                            //     proID: val1[i].proID,
                            //     reqQty: val1[i].neededQty,
                            // }
                        }
                    }
                });
            } else  {

                var newRow = $('#dataRecordsTable tbody tr.sample').clone()
                    .removeClass('sample hidden dataRow').addClass('cloned').attr('id','clonedRow_'+key).appendTo('#dataRecordsTable tbody');
                $('#clonedRow_'+key).find('.proCode').html(value.proCode +'-'+ value.proName);
                $('#clonedRow_'+key).find('.supplier').html(value.potentialSupplier);
                $('#clonedRow_'+key).find('.category').html(value.proCat);
                $('#clonedRow_'+key).find('.stkInHnd').html((parseFloat(value.availableQty)).toFixed(2));
                $('#clonedRow_'+key).find('.soQty').html((parseFloat(value.soQty)).toFixed(2));
                $('#clonedRow_'+key).find('.poQty').html((parseFloat(value.poQty)).toFixed(2));
                $('#clonedRow_'+key).find('.minInvLvl').html((parseFloat(value.minQty)).toFixed(2));
                $('#clonedRow_'+key).find('.jobQty').html((parseFloat(value.jobQty)).toFixed(2));
                $('#clonedRow_'+key).find('.reQty').html(value.neededQty);
                $('#clonedRow_'+key).find('.addPOqty').html((parseFloat(value.poAddQty)).toFixed(2));
                if(value.neededQty > 0){
                    var sup = (value.potentialSupID != null) ?  value.potentialSupID : 0;
                    $('#clonedRow_'+key).find('.select-po-btn').removeClass('hidden');
                    $('#clonedRow_'+key).find('.select-po-btn').addClass('fa-square-o');
                    $('#clonedRow_'+key).find('.select-po-btn').attr('supID', sup);
                    $('#allPoCreation').removeClass('hidden');
                    // reqPoDataSet[key] = {
                    //     proID: key,
                    //     reqQty: value.neededQty,
                    // }
                }
            }
            
        });
        if(soFlag == false){
            $('#dataRecordsTable .sales-order-column').addClass('hidden');
        } else{
            $('#dataRecordsTable .sales-order-column').removeClass('hidden');
        }
        if(poFlag == false){
            $('#dataRecordsTable .purchase-order-column').addClass('hidden');   
        }else {
            $('#dataRecordsTable .purchase-order-column').removeClass('hidden');
        }
        if(reOdrFlag == false){
            $('#dataRecordsTable .min-inventy-column').addClass('hidden');   
        }else {
            $('#dataRecordsTable .min-inventy-column').removeClass('hidden');
        }
        if (jobReqFlag) {
            $('#dataRecordsTable .job-qty-column').removeClass('hidden');   
        } else {
            $('#dataRecordsTable .job-qty-column').addClass('hidden');   
        }
        
        soFlag = false;
        poFlag = false;
        reOdrFlag = false;
        jobReqFlag = false;
        allLocations = false;
        allProducts = false;
    }
    
    function validate(dataSet)
    {
        if(dataSet.allLocFlag == false && dataSet.location == null){
            p_notification(false, eb.getMessage('ERR_LOCATION_NOT_SET_MRF'));
            return false;
        } else if(dataSet.allProFlag == false && dataSet.product == undefined){
            p_notification(false,eb.getMessage('ERR_NO_SELECT_PRODUCT_MRF'));
            return false;
        } else if(dataSet.toDate == ''){
            p_notification(false, eb.getMessage('ERR_NO_DATE_RANGE_MRF'));
            return false;
        } else if(dataSet.poFlag == false && dataSet.reOdrFlag == false && dataSet.soFlag == false && dataSet.jobReqFlag == false){
            p_notification(false, eb.getMessage('ERR_FILTER_TYPE_NOT_SET_MRF'));
            return false;
        } else {
            return true;
        }
    }        

});