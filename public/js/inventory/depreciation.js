var ignoreBudgetLimitFlag = false;
$(document).ready(function() {
	var nowTemp = new Date();
    var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);
    var nowTemp = new Date();
    var now1 = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);
    var myDate = new Date();
    var prettyDate = myDate.getFullYear() + '-' + (myDate.getMonth() + 1) + '-' + (myDate.getDate() < 10 ? '0' : '') + myDate.getDate();
    var day = myDate.getDate() < 10 ? '0' + myDate.getDate() : myDate.getDate();
    var month = (myDate.getMonth() + 1) < 10 ? '0' + (myDate.getMonth() + 1) : (myDate.getMonth() + 1);
    $("#depreciationDate").val(eb.getDateForDocumentEdit('#depreciationDate', day, month, myDate.getFullYear()));
    var checkin = $('#depreciationDate').datepicker({onRender: function(date) {
            return date.valueOf() < now.valueOf() ? ((U_DATE_R_OVERRIDE) ? '' : 'disabled') : '';
        },
        //  format: 'yyyy-mm-dd',
    }).on('changeDate', function(ev) {
        checkin.hide();
    }).data('datepicker');

    loadDropDownFromDatabase('/accounts-api/search-active-accounts-for-dropdown', "", "", '#depreciationAc');
	loadDropDownFromDatabase('/accounts-api/search-active-accounts-for-dropdown', "", "", '#accumulatedDepreciationAc');

	var productDeperciationAccountID = $('#depreciationAc').data('id');
    if(productDeperciationAccountID!=''){
        var productDeperciationAccountName = $('#depreciationAc').data('value');
        $('#depreciationAc').append("<option value='"+productDeperciationAccountID+"'>"+productDeperciationAccountName+"</option>")
        $('#depreciationAc').val(productDeperciationAccountID).selectpicker('refresh');
    }


    $('#depreciationValue').on('keyup', function(event) {
    	event.preventDefault();
        if ($(this).val() < 0) {
            p_notification(false, eb.getMessage('ERR_DEPRECIATION_VALUE_NAN'));
            $(this).val('0.00');
            $('#assetNewValue').val(currentAssetValue);
            return false;
        }

    	var currentAssetValue = $('#currentAssetValue').val();

    	var newAssetValue = currentAssetValue - $(this).val();

    	if (newAssetValue < 0) {
			p_notification(false, eb.getMessage('ERR_DEPRECIATION_VALUE'));
			$(this).val('0.00');
			$('#assetNewValue').val(currentAssetValue);
			return false;
    	}

		$('#assetNewValue').val(newAssetValue);
    });

    $('.depreciate').on('click', function(event) {
    	event.preventDefault();
        
        saveDepreciation();
    });

    function saveDepreciation()
    {
    	var formData = {
            productID: $('#productID').val(), 
            depreciationValue: $('#depreciationValue').val(), 
            depreciationDate: $('#depreciationDate').val(),
            depreciationAcID: $('#depreciationAc').val(),
            accumulatedDepreciationAcID: $('#accumulatedDepreciationAc').val(),
            ignoreBudgetLimit: ignoreBudgetLimitFlag,
        };

        if (validateFormData(formData)) {
        	eb.ajax({
                type: 'POST',
                url: BASE_URL + '/fixed-asset-management-api/saveDepreciation',
                data: formData,
                success: function(respond) {
                    if (respond.status) {
                        p_notification(respond.status, respond.msg);
                        setTimeout(function(){ 
                            window.location.assign(BASE_URL + "/fixed-asset-management/listFixedAsset")
                        }, 1500);
                    } else {
                        if (respond.data == "NotifyBudgetLimit") {
                            bootbox.confirm(respond.msg+' ,Are you sure you want to save this depreciation ?', function(result) {
                                if (result == true) {
                                    ignoreBudgetLimitFlag = true;
                                    saveDepreciation();
                                } else {
                                    setTimeout(function(){ 
                                        location.reload();
                                    }, 3000);
                                }
                            });
                        } else {
                            p_notification(respond.status, respond.msg);
                        }
                    }
                }
            });
        }
    }

    function validateFormData(formData)
    {
    	if (formData.depreciationValue == "" || formData.depreciationValue == 0) {
    		p_notification(false, eb.getMessage('ERR_DEPRECIATION_VALUE_CANNOT_BE_EMPTY'));
    		return false;
    	} else if (formData.depreciationDate == "") {
    		p_notification(false, eb.getMessage('ERR_DEPRECIATION_DATE_EMPTY'));
    		return false;
    	} else if (formData.depreciationAcID == "0") {
    		p_notification(false, eb.getMessage('ERR_DEPRECIATION_AC_EMPTY'));
    		return false;
    	} else if (formData.accumulatedDepreciationAcID == "0") {
    		p_notification(false, eb.getMessage('ERR_ACCUMULATED_DEPRECIATION_AC_EMPTY'));
    		return false;
    	}

    	return true;
    }
});