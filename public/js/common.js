function getDecimalPlaces(number) {
    var decimalPlaces = 2;//default decimal places
    number = parseFloat(number).toString();
    if (number % 1 != 0) {//check whether number has decimals 
        var elementArr = number.split('.');
        var decimals = elementArr[elementArr.length - 1];
        decimalPlaces = (decimals.length > decimalPlaces) ? decimals.length : decimalPlaces;
    }
    return parseInt(decimalPlaces);
}

function showLoader() {
    $("#ajax-loader").removeClass('hidden');
}

function hideLoader() {
    $("#ajax-loader").addClass('hidden');
}

function isValidDate(dateString) {

    var regEx = /^\d{4}[-/.]\d{4}[-/.]\d{4}$/; ///^\d{4}-\d{2}-\d{2}$/;
    return dateString.match(regEx) != null;

    /////date validation
    //var dateFormat = ;

}

var eb = {
    defaults: {
        complete: function(xhr, statusText)
        {
            switch (xhr.status) {
                case 403:
                    p_notification(false, eb.getMessage('ERR_COMMON_PERMISSION'));
            }
        }
    },
    request_list: {}
};

eb.ajax = function(options) {

    if (eb.isDuplicateRequest(options.url)) {
        return false;
    }

    //Loader is showing for all requests.
    /**if (options.hasOwnProperty('showLoader')) {
     if (options.showLoader) {
     showLoader();
     }
     }**/

    showLoader();

    $.extend(options, eb.defaults);
    var request = $.ajax(options)
            .always(function() {
                eb.removeRequestFromList(options.url);
                hideLoader();
            });
    return request;
}

eb.isDuplicateRequest = function(url) {
    if (eb.request_list[url] === undefined) {
        eb.request_list[url] = 1;
    } else {
        return true;
    }
}

eb.removeRequestFromList = function(url) {
    delete eb.request_list[url];
}

eb.post = function(url, param1, param2) {
    if (eb.isDuplicateRequest(url)) {
        return false;
    }

    showLoader();
    var postReturn;
    if (param2 !== undefined) {
        postReturn = $.post(url, param1, param2);
    } else {
        postReturn = $.post(url, param1);
    }

    return postReturn
            .always(eb.defaults.complete)
            .always(function() {
                eb.removeRequestFromList(url);
                hideLoader();
            });
}

function getViewAndLoad(url, divID, param, done) {
    showLoader();
    var res;
    eb.ajax({
        type: 'POST',
        url: url,
        data: param,
        success: function(result)
        {
            if (result.status != false) {
                $('#' + divID).html(result.html);
            }

            res = result;
            hideLoader();
            if (done !== undefined) {
                done(result);
            }
        },
        async: false
    }).always(function() {
        hideLoader();
    });
    return res;
}


function ezBiz_post(url, param) {
    var res;
    $.ajax({
        type: 'POST',
        url: url,
        data: param,
        success: function(result)
        {
            res = result;
        },
        async: false
    });
    return res;
}

function getCurrPage() {
    var page = parseInt($("[name='pagination_current']").val());
    return !isNaN(page) ? page : 0;
}

function toFloat(value) {
    var floatVal = parseFloat(value);
    return (isNaN(floatVal)) ? 0 : floatVal;
}

$(document).ready(function() {
    $("#show_img_btn").on("click", function () {
      html2canvas(document.getElementById("capture")).then(canvas => {
        document.body.appendChild(canvas);
        var link = document.createElement('a');
        link.download = 'filename.bmp';
        link.href = canvas.toDataURL("image/bmp")
        link.click();
      });
    
    });
  
  
  });

function printIframe(id, posPrintoutImageConversionStatus = false)
{
    var iframe = document.frames ? document.frames[id] : document.getElementById(id);
    var ifWin = iframe.contentWindow || iframe;
    
    if(!posPrintoutImageConversionStatus){
        iframe.focus();
        ifWin.print();
        return false;
    }

    $('#'+id).removeClass("hidden");
    var $body = $('#'+id).contents().find("html");
    
    html2canvas($body[0]).then(function (canvas) { 
        $('#'+id).addClass("hidden");
        canvas.toDataURL('image/bmp');
        const iframe_fake = document.createElement('iframe');
        

        iframe_fake.style.height = 0;
        iframe_fake.style.visibility = 'hidden';
        iframe_fake.style.width = 0;

        // Set the iframe_fake's source
        iframe_fake.setAttribute('srcdoc', '<html moznomarginboxes mozdisallowselectionprint><body></body></html>');
        document.body.appendChild(iframe_fake);
        
        const new_style_element = document.createElement("style");
        new_style_element.textContent = `@media print
        {
            @page {
                size: auto;
                margin: 0mm;
                margin-bottom: 0mm;
            }
            body{
                background-color:#FFFFFF;
                margin: 0px;  /* the margin on the content before printing */
                padding: 0mm;
            }
        }`;
        iframe_fake.contentDocument.head.appendChild(new_style_element);
    
        // Append the image to the iframe's body
        const body = iframe_fake.contentDocument.body;
        body.style.textAlign = 'left';
        body.appendChild(canvas);

        iframe_fake.focus();
        iframe_fake.contentWindow.print();
        iframe_fake.parentNode.removeChild(iframe_fake);
        
        return false;
    });
}

function numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

function validate_location_form() {
    var locationCode = $('#locationCode').val();
    var locationName = $('#locationName').val();
    var locationEmail = $('#locationEmail').val();
    var locationTelephone = $('#locationTelephone').val();
    var locationFax = $('#locationFax').val();
    var atpos = locationEmail.indexOf("@");
    var dotpos = locationEmail.lastIndexOf(".");
    var phoneNO = /^[0-9\-\(\) \+]+$/;
    if (locationCode === null || locationCode === "") {
        p_notification(false, eb.getMessage('ERR_COMMON_LOCACODE'));
    } else if (locationName === null || locationName === "") {
        p_notification(false, eb.getMessage('ERR_COMMON_LOCANAME'));
    } else if ((atpos < 1 || dotpos < atpos + 2 || dotpos + 2 >= locationEmail.length) && (locationEmail !== "")) {
        p_notification(false, eb.getMessage('ERR_SALEP_EMAIL_VALID'));
    } else if (locationTelephone !== "" && !phoneNO.test(locationTelephone)) {
        p_notification(false, eb.getMessage('ERR_COMMON_TPNUMBER'));
    } else if (locationFax !== "" && !phoneNO.test(locationFax)) {
        p_notification(false, eb.getMessage('ERR_COMMON_FAXNUMBER'));
    } else {
        return true;
    }
}

function check_location(locationID, locationCode) {
    var res = ezBiz_post('/settings/location/checkLocation', {locationID: locationID, locationCode: locationCode});
    if (res.status) {
        p_notification('error', res.msg);
        return true;
    } else {
        return false;
    }

}
/**
 * Use to remove unwonted decimal places digits
 * @param {string} currentValue
 * @param {string} decimalDigits
 * @returns {String}
 */
function cutDecimalPoint(currentValue, decimalDigits) {
    var value = parseFloat(currentValue);
    var digits = parseFloat(decimalDigits);
    var tempValue = value * Math.pow(10, digits);
    tempValue = parseInt(tempValue);
    return (tempValue / Math.pow(10, digits)).toFixed(digits).toString();
}



/**
 * Get messages
 * @param {String} Message
 * @returns {String}
 */
eb.getMessage = function(msg) {
    var msg = MESSAGES[msg];
    // if more than one argument is passed, consider them as text to replace placeholders in the message
    if (arguments.length > 1) {
        for (var i = 1; i < arguments.length; i++) {
            msg = msg.replace(/%s/, arguments[i]);
        }

    }
    return msg;
}
$(function() {
    // vertically center loader image
    $(window).resize(function() {
        var total_height = $(window).height();
        $('#ajax-loader .loader').css('margin-top', total_height / 2);
    }).trigger('resize');

    if ($('div.notice_panel').length > 1) {
        $('div.notice_panel').not(':last').addClass('hidden');
    }

    $('.panelMinimize').on('click', function() {
        $(this).parents('.notice_panel').remove();
        $('div.notice_panel:last').removeClass('hidden');
    });
    $('.panelClose').on('click', function() {
        var noticeID = $(this).parents('.notice_panel').attr('id').split('_')[1];
        $(this).parents('.notice_panel').remove();
        $('div.notice_panel:last').removeClass('hidden');
        eb.ajax({
            type: 'POST',
            url: BASE_URL + '/api/user/removeNoticeForUser',
            data: {noticeID: noticeID},
            success: function(respond) {
            },
            async: false
        });
    });

    //initilize the popover
    $('.help_icon_js i.help_icon').addClass('fa fa-question-circle')
    $('.help_icon_js').popover({
        trigger: "hover"
    });
});
/**
 *
 * @param String name
 * @param json value
 * @param {type} formData
 * @param String dataName
 * @param {type} data
 * @param int w  //top left coordinate in x direction of source image
 * @param int h  //top left coordinate in y direction of source image
 * @param int x1
 * @param int y1
 * @param int x2 //width of original image
 * @param int y2 //height of original image
 */
eb.getImageDimension = function(name, value, formData, dataName, data, w, h, x1, y1, x2, y2) {
    var width = $('#w'),
            height = $('#h'),
            src_x = $('#x1'),
            src_y = $('#y1'),
            value_x = $('#x2'),
            value_y = $('#y2');
    if (width.val() == '' || width.val() == null) {
        width.val(w);
    }
    if (height.val() == '' || height.val() == null) {
        height.val(h);
    }
    if (src_x.val() == '' || src_x.val() == null) {
        src_x.val(x1);
    }
    if (src_y.val() == '' || src_y.val() == null) {
        src_y.val(y1);
    }
    if (value_x.val() == '' || value_x.val() == null) {
        value_x.val(x2);
    }
    if (value_y.val() == '' || value_y.val() == null) {
        value_y.val(y2);
    }

    formData.append(name, value);
    formData.append("w", width.val());
    formData.append("h", height.val());
    formData.append("x1", src_x.val());
    formData.append("y1", src_y.val());
    formData.append("x2", value_x.val());
    formData.append("y2", value_y.val());

//    formData.append(dataName, data.data);
//    $(width, height, value_x, value_y).val('');


//   $(width, height, value_x, value_y).val('');

    return formData;

}

/**
 * email address validation
 */
eb.validateEmailAddr = function(to) {
    var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    to = to.replace(/ /g, '');


    if (to != "") {
        to = to.split(',');

        var valid_email = true;

        $(to).each(function(index, email) {
            if (!regex.test(email)) {
                p_notification(false, eb.getMessage('ERR_SENDMAIL_RECIPADDR'));
                return valid_email = false;
            }
        });

        return valid_email;
    } else {
        p_notification(false, eb.getMessage('ERR_SENDMAIL_RECIPADDR_BLANK'));
        return false;
    }


    return true;
}

//return date according to the user date format, used in invoice, salesorder, quotation : to decide due date
eb.getDateForDocumentEdit = function(dateid, NowDate, NowMonth, NowYear) {

    switch ($(dateid).data("date-format")) {
        case 'yyyy/mm/dd':
            return (NowYear + "/" + NowMonth + "/" + NowDate);
            break;
        case 'yyyy-mm-dd':
            return (NowYear + "-" + NowMonth + "-" + NowDate);
            break;
        case 'yyyy.mm.dd':
            return (NowYear + "." + NowMonth + "." + NowDate);
            break;
        case 'dd/mm/yyyy':
            return (NowDate + "/" + NowMonth + "/" + NowYear);
            break;
        case 'dd-mm-yyyy':
            return (NowDate + "-" + NowMonth + "-" + NowYear);
            break;
        case 'dd.mm.yyyy':
            return (NowDate + "." + NowMonth + "." + NowYear);
            break;
        case 'mm/dd/yyyy':

            return (NowMonth + "/" + NowDate + "/" + NowYear);
            break;
        case 'mm-dd-yyyy':
            return (NowMonth + "-" + NowDate + "-" + NowYear);
            break;
        case 'mm.dd.yyyy':
            return (NowMonth + "." + NowDate + "." + NowYear);
            break;
    }
}
//return date as standard format for the validation part
eb.convertDateFormat = function(dateId) {
    var userDate = $(dateId).val()

    var params;
    var formats;
    var dateformat = $(dateId).data("date-format");
    var slash = userDate.indexOf("/");
    var dot = userDate.indexOf(".");
    var dash = userDate.indexOf("-");

    if (dot > 0) {
        params = userDate.split(".");
        formats = dateformat.split(".");
    }
    if (slash > 0) {
        params = userDate.split("/");
        formats = dateformat.split("/");
    }
    if (dash > 0) {
        params = userDate.split("-");
        formats = dateformat.split("-");
    }

    var joindate;
    if (formats[0] === 'yyyy') {
        joindate = new Date(params[0], params[1] - 1, params[2]);
    }
    else if (formats[0] === 'dd') {
        joindate = new Date(params[2], params[1] - 1, params[0]);
    }
    else if (formats[0] === 'mm') {
        joindate = new Date(params[2], params[0] - 1, params[1]);
    }
    return joindate;
}

/*  Dropdown will search from data base and load.
 *  return data must be an json array like
 *  [
 *      {'value' => '' ,'text' => '',},
 *  ]
 *  addFlag boolean if true, add another option from controller
 *  sharmilan
 */

var loadDropDownFromDatabase = function(url, locationID, addFlag, dropDownID, dropDownIDParent, isEmptyOption, documentType, checkDataLenght) {
    var params;
    var initialLoadLength = 25;

    $(dropDownID, dropDownIDParent)
            .data('location-id', locationID)
            .data('add-flag', addFlag);

    // set ajax dropdown search
    var setAjaxDropdown  = function() {
        $(dropDownID, dropDownIDParent)
            .selectpicker({
                liveSearch: true,
            })
            .ajaxSelectPicker({
                ajax: {
                    url: BASE_URL + url,
                    data: function() {
                        params = {
                            searchKey: '{{{q}}}',
                            locationID: $(dropDownID, dropDownIDParent).data('location-id'),
                            addFlag: $(dropDownID, dropDownIDParent).data('add-flag'),
                            documentType: documentType
                        };
                        return params;
                    },
                    cache: true,
                },
                locale: {
                    emptyTitle: '',
                    statusInitialized: '',
                    searchPlaceholder: 'Type to search..'},
                preprocessData: function(response) {
                    var otherOptions;
                    var data = (response.data.list) ? response.data.list : [];

                    if (response.data.extra) {
                        $(dropDownID).data('extra', response.data.extra);
                    }

                    if (!isEmptyOption) {
                        var subtext = (data.length === 0) ? 'No Results' : '';
                        var emptyData = [{"value": "0", "text": " ", "data": {"subtext": subtext}}];
                        otherOptions = response.data.otherOptions;
                        if (otherOptions) {
                            data = otherOptions.concat(data);
                        }
                        return emptyData.concat(data);
                    } else {
                        return data;
                    }
                },
                preserveSelected: true,
                preserveSelectedPosition: 'before',
            });
    }

    if(checkDataLenght){
        // initial ajax call
        // if results less than 'initialLoadLength' will load data to dropdown and skip ajax dropdown
        eb.ajax({
            url: BASE_URL + url,
            method: 'POST',
            data: {
                    locationID: $(dropDownID, dropDownIDParent).data('location-id'),
                    addFlag: $(dropDownID, dropDownIDParent).data('add-flag'),
                    documentType: documentType
            },
            success: function(response){
                var res = (response.data.list) ? response.data.list : [];

                if (res.length > 25) {
                    setAjaxDropdown();
                    return false;            
                }
                $.each(res, function(i, item){
                    $(dropDownID, dropDownIDParent).append($('<option>', { 
                        value: item.value,
                        text : item.text 
                        }));
                });

                $(dropDownID, dropDownIDParent).selectpicker('refresh');    
            },
        });
    }else {
        setAjaxDropdown();
    }
}
