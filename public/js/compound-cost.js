var ignoreBudgetLimitFlag = false;
var locationProducts;
$(document).ready(function() {

	function product(lPID, pID, pCode, pN, pQ, pUP,uom,supID, uomID, proType, productTotal) {
        this.locationPID = lPID;
        this.pID = pID;
        this.pCode = pCode;
        this.pName = pN;
        this.pQuantity = pQ;
        this.pUnitPrice = pUP;
        this.uom = uom;
        this.supID = supID;
        this.uomID = uomID;
        this.proType = proType;
        this.productTotal = productTotal
        
    }
    function supplierCost(supID,CostID,Cost) {
        this.supID = supID;
        this.CostID = CostID;
        this.Cost = Cost;
    }
	// add product row
	var clonedRow = $($('.product-line-sample').clone()).removeClass('hidden product-line-sample').addClass('added-line');
	$('.supplier', clonedRow).attr('id','supplier');
	$('.itemCode', clonedRow).attr('id','itemCode');
	$('.purchase-qty', clonedRow).attr('id','purchase-qty');
	$('.unitPrice', clonedRow).attr('id','unitPrice');
	clonedRow.insertBefore('.product-line-sample');

	// add cost row
	var clonedCostRow = $($('.cost-line-sample').clone()).removeClass('hidden cost-line-sample').addClass('added-cost-line');
	$('.cost-supplier', clonedCostRow).attr('id','supplierID');
	$('.costCode', clonedCostRow).attr('id','costCode');
	$('.cost-price', clonedCostRow).attr('id','cost-price');
	$('.cost-tick', clonedCostRow).attr('id','cost-tick');
	clonedCostRow.insertBefore('.cost-line-sample');
	// get current location ID
	var locationID = $('.compound-cost-add').attr('data-id');
	var currentlySelectedProduct = null;
	var selectedProduct;
	var locationProductID = null;
	var selectedItemDetails;
	var productSet = [];
	var costSet = [];
	var currentlySelectedCost = null;
	var currentlySelectedCostText = null;
	var selectedItemSupplier = null;
	var currentSelectedItemSupplierText = null;
	var costSuppliers = [];
	var selectedCostSupplier = null;
	var currentSelectedCostSupplierText = null;
	var $compoundTable = $("#compound-cost-table");
	var itemSuppliers = [];
	var documentType = 'GRN';
	var totalQty = 0;
	var totalCost = 0;
	var totalItemPrice = 0;
	var selectedItems = [];
	var supplierCostSet = [];
	var costType = 1;
	var selectedPO = null;
	var poProducts = {};
	
	// get product detials
	loadDropDownFromDatabase('/productAPI/search-location-products-for-dropdown', locationID, 0, '#itemCode', '', '', documentType);
	loadDropDownFromDatabase('/api/grn/search-compound-costing-types-for-dropdown', locationID, 0, '#costCode', '', '', documentType);
	loadDropDownFromDatabase('/supplierAPI/search-suppliers-for-dropdown', "", 0, '#supplier');
	loadDropDownFromDatabase('/supplierAPI/search-suppliers-for-dropdown', "", 0, '#supplierID');
    
    $('#supplier').on('change', function() {
        if ($(this).val() > 0) {
            selectedItemSupplier = $(this).val();
            currentSelectedItemSupplierText = $("#supplier option:selected").text();
        }
    });

	$('#supplierID').on('change', function() {
        if ($(this).val() > 0) {
            selectedCostSupplier = $(this).val();
            currentSelectedCostSupplierText = $("#supplierID option:selected").text();
        }
    });  


    loadDropDownFromDatabase('/api/po/search-po-for-dropdown', "", 0, '#startByCode');
    $('#startByCode').trigger('change');
    $('#startByCode').on('change', function() {
        if ($(this).val() > 0 && selectedPO != $(this).val()) {
            selectedPO = $(this).val();
            $('#startByCode').prop('disabled', true);
            loadPO(selectedPO);
        }
    });


    function loadPO(poID) {
        $('#addSupplierBtn').prop('disabled', 1).click(function(e) {
            e.stopPropagation;
            return false;
        });
        var myDate = new Date();
        var day = myDate.getDate() < 10 ? '0' + myDate.getDate() : myDate.getDate();
        var month = (myDate.getMonth() + 1) < 10 ? '0' + (myDate.getMonth() + 1) : (myDate.getMonth() + 1);
        var year = myDate.getFullYear();
        var deliDate = eb.getDateForDocumentEdit('#deliveryDate', day, month, year);
        eb.ajax({type: 'POST',
            url: BASE_URL + '/api/po/getPoDetails',
            data: {poID: poID, toGrn: true},
            success: function(respond) {
                if (respond.data.inactiveItemFlag) {
                    p_notification(false, respond.data.errorMsg);
                    setTimeout(function(){ 
                        window.location.assign(BASE_URL + "/grn/create")
                    }, 3000);
                    return false;
                }
                var poData = respond.data.poData;
                poProducts = poData.poProducts;


                var currentElem = new Array();
                $.each(respond.data.locationProducts, function(key, valueObj) {
                    if (valueObj.purchase) {
                        currentElem[key] = valueObj;
                    }
                });
                locationProducts = $.extend(locationProducts, currentElem);
                $('.added-line').addClass('hidden');
                setPoProducts(poData);
            },
            async: false
        });
    }  

    function setPoProducts(poData) {
    	var preProducts = poData.poProducts
        for (var p in preProducts) {
            var preProduct = preProducts[p];
            var poRemainQty = toFloat(preProduct.poPQ) - toFloat(preProduct.copiedQty);
            // $('#qty', clonedAddNew).val(poRemainQty);
            locationProductID = preProduct.lPID;

            var itemQty = poRemainQty;
		 	var unitPrice = preProduct.poPP;
		 	var totalPrice = toFloat(poRemainQty) * toFloat(preProduct.poPP);
		 	var umoID = $(this).parents('tr').find('.purchase-qty').siblings('.uom-select').find('.selected').data('uomID');
		 
	 		selectedProduct = preProduct.poPID;
	 		var id = locationProductID+'_'+itemQty;
	 		// calculated Item Cost
	 		var costPerUnit = unitPrice;
	 		
			selectedItemSupplier = poData.poSID;
	 		var clonedNewRow = $($('.product-line-sample').clone()).removeClass('hidden product-line-sample').addClass('added-line').attr('data-id',id);
			var itemCodeID = id+'_itemCode';
			$('div.bootstrap-select', clonedNewRow).remove();


	        $(".supplier", clonedNewRow).empty().
	            append($("<option></option>").
	                attr("value", poData.poSID).
	                    text(poData.poSN));
	        $('.supplier', clonedNewRow).val(selectedItemSupplier);
	        $('.supplier', clonedNewRow).prop('disabled', true);
	        $('.supplier', clonedNewRow).selectpicker('render');
	        $('.supplier', clonedNewRow).attr('id', '');
	        $(".itemCode", clonedNewRow).empty().
	            append($("<option></option>").
	                attr("value", preProduct.poPID).
	                    text(preProduct.poPC + ' - ' + preProduct.poPN));
	        $('.itemCode', clonedNewRow).val(preProduct.poPID);
	        $('.itemCode', clonedNewRow).data('productID', preProduct.poPID);
	        $('.itemCode', clonedNewRow).data('PT', preProduct.productType);
	        $('.itemCode', clonedNewRow).data('PC', preProduct.poPC);
	        $('.itemCode', clonedNewRow).data('PN', preProduct.poPN);
	        $('.itemCode', clonedNewRow).prop('disabled', true);
	        $('.itemCode', clonedNewRow).selectpicker('render');
	        $('.itemCode', clonedNewRow).attr('id', itemCodeID);
	        $('.unitPrice', clonedNewRow).val(parseFloat(unitPrice).toFixed(2)).addUomPrice(locationProducts[preProduct.poPID].uom, preProduct.poPUomID);
	        $('.unitPrice', clonedNewRow).parent().addClass('input-group').attr('disabled',false);
	        $('.totalPrice', clonedNewRow).val(totalPrice);
	        $('.totalPrice', clonedNewRow).attr('disabled',true);
	        $('.totalPrice', clonedNewRow).data('totPrice',totalPrice);
	        $('.purchase-qty', clonedNewRow).val(parseFloat(itemQty).toFixed(2)).addUom(locationProducts[preProduct.poPID].uom, preProduct.poPUomID);
	        $('.purchase-qty', clonedNewRow).parent().addClass('input-group').attr('disabled',false);
	        $('.uomqty', clonedNewRow).attr('disabled',false);
	      
	        clonedNewRow.data('selectedItemSupplier', selectedItemSupplier);
	        clonedNewRow.data('locationProductID', locationProductID);
	        clonedNewRow.data('selectedProduct', preProduct.poPID);

			clonedNewRow.insertBefore('.product-line-sample');
	 		selectedItemSupplier = null;

			$('#supplier').val(0).trigger('change').empty().selectpicker('refresh');
			$('#itemCode').val(0).trigger('change').empty().selectpicker('refresh');
	        $('#itemCode').focus();
	        $('#unitPrice').val('').siblings('.uomPrice,.uom-price-select').remove();
	    	$("#purchase-qty").val('').siblings('.uomqty,.uom-select').remove();
	    	$("#totalPrice").val('');
	    	$("#purchase-qty").show();
	    	$("#purchase-qty").parent('div').removeClass('input-group');
	    	$("#unitPrice").parent('div').removeClass('input-group');
	    	$('#unitPrice').show();
	    	calculate();
        }
        $('tr.tempPoPro', '#grnProductTable').find(".uomPrice").trigger('focusout');

    }


    $compoundTable.on('focusout', '#purchase-qty,#unitPrice,.uomqty,.uomPrice', function() {
        var $thisRow = $(this).parents('tr');
        var productID = $thisRow.data('productID');
            
        calculateItemTotal($thisRow, productID);
        
    });


    function calculateItemTotal (thisRow, productID) {

    	var tmpItemQuentity = thisRow.find('.purchase-qty').val();
        var tmpItemTotal = (toFloat(thisRow.find('.unitPrice').val()) * toFloat(tmpItemQuentity));
        thisRow.find('.totalPrice').val(accounting.formatMoney(tmpItemTotal));
        thisRow.find('.totalPrice').data('totPrice', tmpItemTotal);
        thisRow.find('.totalPrice').prop('readonly', true);

    }
 
	$('#itemCode').on('change', function() {
	 	if ($(this).val() > 0 && $(this).val() != currentlySelectedProduct) {
	 			locationProductID = null;
	 			var abc = $(this).parents('tr');
	            currentlySelectedProduct = $(this).val();
	            selectedProduct = $(this).val();
	            eb.ajax({
	                type: 'POST',
	                url: BASE_URL + '/productAPI/get-location-product-details',
	                data: {productID: selectedProduct, locationID: locationID},
	                success: function(respond) {
	                	$('#unitPrice').val('').siblings('.uomPrice,.uom-price-select').remove();
	    				$("#purchase-qty").val('').siblings('.uomqty,.uom-select').remove();
	    				$("#purchase-qty").show();
	    				$('#unitPrice').show();
	                	locationProductID = respond.data.lPID
	                	selectedItemDetails = respond.data;
	                	$('#unitPrice').val(parseFloat(respond.data.dPP).toFixed(2)).addUomPrice(respond.data.uom);
	                    $('#unitPrice').parent().addClass('input-group');
	                    $('#purchase-qty').parent().addClass('input-group');
	                    $('#purchase-qty').addUom(respond.data.uom);
	                	$('#purchase-qty').focus();
	                }
	            });
	    }

	});

	$('#costCode').on('change', function() {
	 	if ($(this).val() > 0 && $(this).val() != currentlySelectedCost) {
	 		currentlySelectedCost = $(this).val();
	 		currentlySelectedCostText = $("#costCode option:selected").text(); 
	 		$('#cost-price').focus();
	 		$(this).parents('tr').find('.cost-tick').prop('checked', true);
	 	}
	});


	$('#refreshStartBy').on('click', function() {
	 	window.location.reload();
	});
	$('#addCost').on('click', function() {
	 	var costAmo = $('#cost-price').val();
	 	// var commonCostFlag = true;
	 	// if ($(this).parents('tr').find('.cost-tick').attr('checked') == true) {
	 	// 	commonCostFlag = true;
	 	// } else {
	 	// 	commonCostFlag = false;
	 	// }
	 	if (selectedCostSupplier == null) {
	 		p_notification(false, eb.getMessage('ERR_COST_SUPPLIER_NOT_SELECT'));
	 	} else if (currentlySelectedCost == null) {
	 		p_notification(false, eb.getMessage('ERR_COST_TYPE_NOT_SELECT'));
	 	} else if (costAmo == '' || costAmo == 0) {
	 		p_notification(false, eb.getMessage('ERR_COST_AMOUNT_NOT_SELECT'));
	 	} else if (isNaN(costAmo)) {
	 		p_notification(false, eb.getMessage('ERR_ITEM_QTY_NOT_NUMBER'));
	 	} else if (costSet[currentlySelectedCost]) {
	 		p_notification(false, eb.getMessage('ERR_SAME_COST_TYPE'));

	 	} else {
	 		costSet[currentlySelectedCost] = costAmo;
	 		costSuppliers[selectedCostSupplier] = selectedCostSupplier;
	 		var cosSupID = currentlySelectedCost+'_'+selectedCostSupplier;
	 		var supData = new supplierCost(selectedCostSupplier,currentlySelectedCost, costAmo);
	 		supplierCostSet.push(supData);
	 		var clonedNewCostRow = $($('.cost-line-sample').clone()).removeClass('hidden cost-line-sample').addClass('added-line').attr('data-id',currentlySelectedCost);
			$('div.bootstrap-select', clonedNewCostRow).remove();
	        $(".cost-supplier", clonedNewCostRow).empty().
	            append($("<option></option>").
	                attr("value", selectedCostSupplier).
	                    text(currentSelectedCostSupplierText));
	        $('.cost-supplier', clonedNewCostRow).val(selectedCostSupplier);
	        $('.cost-supplier', clonedNewCostRow).prop('disabled', true);
	        $('.cost-supplier', clonedNewCostRow).selectpicker('render');
	        $('.cost-supplier', clonedNewCostRow).attr('id', '');
	        $(".costCode", clonedNewCostRow).empty().
	            append($("<option></option>").
	                attr("value", currentlySelectedCost).
	                    text(currentlySelectedCostText));
	        $('.costCode', clonedNewCostRow).val(currentlySelectedCost);
	        $('.costCode', clonedNewCostRow).prop('disabled', true);
	        $('.costCode', clonedNewCostRow).selectpicker('render');
	        $('.costCode', clonedNewCostRow).attr('id', '');
	        $('.cost-price', clonedNewCostRow).val(parseFloat(costAmo).toFixed(2)).attr('disabled', true);

	        // if (!commonCostFlag) {
	        // 	$('.cost-tick', clonedNewCostRow).prop('checked', true).prop('disabled', true);
	        // } else {
	        // 	$('.cost-tick', clonedNewCostRow).parent().addClass('hidden');
	        // 	$('.cost-map', clonedNewCostRow).removeClass('hidden').prop('disabled', true);

	        // }
	        $(this).parents('tr').find('.cost-tick').attr('checked', false);
	        $('.add', clonedNewCostRow).addClass('hidden');
	        $('.delete', clonedNewCostRow).removeClass('hidden');

			clonedNewCostRow.insertBefore('.cost-line-sample');
			$('#supplierID').val(0).trigger('change').empty().selectpicker('refresh');
			$('#costCode').val(0).trigger('change').empty().selectpicker('refresh');
	        $('#costCode').focus();
	        $('#cost-price').val('');
	        selectedCostSupplier = null;
	        calculate();

	 	}
	});

	$(document).on('click', '.addItem', function() {
		if (selectedPO == null) {
		 	var itemQty = $(this).parents('tr').find('.purchase-qty').val();
		 	var unitPrice = $(this).parents('tr').find('.unitPrice').val();
		 	var totalPrice = $(this).parents('tr').find('.totalPrice').data('totPrice');
		 	var umoID = $(this).parents('tr').find('.purchase-qty').siblings('.uom-select').find('.selected').data('uomID');
		 	if (selectedItemSupplier == null) {
		 		p_notification(false, eb.getMessage('ERR_SUPPLIER_NOT_SELECT'));
		 	} else if (locationProductID == null) {
		 		p_notification(false, eb.getMessage('ERR_ITEM_NOT_SELECT'));
		 	} else if (itemQty == '' || itemQty == 0) {
		 		p_notification(false, eb.getMessage('ERR_ITEM_QTY_NOT_SELECT'));
		 	} else if (selectedItems[locationProductID]) {
		 		p_notification(false, eb.getMessage('ERR_SAME_ITEM_SELECT'));
		 	} else {
		 		var id = locationProductID+'_'+itemQty;
		 		// calculated Item Cost
		 		var costPerUnit = unitPrice;
		 		selectedItems[locationProductID] = true;
		 		productSet[id] =  new product(locationProductID,selectedProduct,selectedItemDetails.pC,selectedItemDetails.pN,itemQty,costPerUnit,selectedItemDetails.uom,selectedItemSupplier, umoID, selectedItemDetails.pT, totalPrice);
		 		locationProductID = null;
		 		itemSuppliers[selectedItemSupplier] = selectedItemSupplier;
		 		var clonedNewRow = $($('.product-line-sample').clone()).removeClass('hidden product-line-sample').addClass('added-line').attr('data-id',id);
				var itemCodeID = id+'_itemCode';
				$('div.bootstrap-select', clonedNewRow).remove();
		        $(".supplier", clonedNewRow).empty().
		            append($("<option></option>").
		                attr("value", selectedItemSupplier).
		                    text(currentSelectedItemSupplierText));
		        $('.supplier', clonedNewRow).val(selectedItemSupplier);
		        $('.supplier', clonedNewRow).prop('disabled', true);
		        $('.supplier', clonedNewRow).selectpicker('render');
		        $('.supplier', clonedNewRow).attr('id', '');
		        $(".itemCode", clonedNewRow).empty().
		            append($("<option></option>").
		                attr("value", selectedItemDetails.pID).
		                    text(selectedItemDetails.pC + ' - ' + selectedItemDetails.pN));
		        $('.itemCode', clonedNewRow).val(selectedItemDetails.pID);
		        $('.itemCode', clonedNewRow).data('productID', selectedItemDetails.pID);
		        $('.itemCode', clonedNewRow).data('PT', selectedItemDetails.pT);
		        $('.itemCode', clonedNewRow).data('PC', selectedItemDetails.pC);
		        $('.itemCode', clonedNewRow).data('PN', selectedItemDetails.pN);
		        $('.itemCode', clonedNewRow).prop('disabled', true);
		        $('.itemCode', clonedNewRow).selectpicker('render');
		        $('.itemCode', clonedNewRow).attr('id', itemCodeID);
		        $('.unitPrice', clonedNewRow).val(parseFloat(unitPrice).toFixed(2)).addUomPrice(selectedItemDetails.uom);
		        $('.unitPrice', clonedNewRow).parent().addClass('input-group').attr('disabled',true);
		        $('.totalPrice', clonedNewRow).val(totalPrice);
		        $('.totalPrice', clonedNewRow).attr('disabled',true);
		        $('.purchase-qty', clonedNewRow).val(parseFloat(itemQty).toFixed(2)).addUom(selectedItemDetails.uom);
		        $('.purchase-qty', clonedNewRow).parent().addClass('input-group').attr('disabled',true);
		        $('.uomqty', clonedNewRow).attr('disabled',true);
		        $('.add', clonedNewRow).addClass('hidden');
		        $('.delete', clonedNewRow).removeClass('hidden');

				clonedNewRow.insertBefore('.product-line-sample');
		 		selectedItemSupplier = null;

				$('#supplier').val(0).trigger('change').empty().selectpicker('refresh');
				$('#itemCode').val(0).trigger('change').empty().selectpicker('refresh');
		        $('#itemCode').focus();
		        $('#unitPrice').val('').siblings('.uomPrice,.uom-price-select').remove();
		    	$("#purchase-qty").val('').siblings('.uomqty,.uom-select').remove();
		    	$("#totalPrice").val('');
		    	$("#purchase-qty").show();
		    	$("#purchase-qty").parent('div').removeClass('input-group');
		    	$("#unitPrice").parent('div').removeClass('input-group');
		    	$('#unitPrice').show();
		    	calculate();
		 	}
		} else {
		 	var itemQty = $(this).parents('tr').find('.purchase-qty').val();
		 	var unitPrice = $(this).parents('tr').find('.uomPrice').val();
		 	var totalPrice = $(this).parents('tr').find('.totalPrice').data('totPrice');
		 	var umoID = $(this).parents('tr').find('.purchase-qty').siblings('.uom-select').find('.selected').data('uomID');
		 	selectedItemSupplier = $(this).parents('tr').data('selectedItemSupplier');
		 	locationProductID = $(this).parents('tr').data('locationProductID');
		 	selectedProduct = $(this).parents('tr').data('selectedProduct');
		 	var itemCode = $(this).parents('tr').find('.itemCode').data('PC')
		 	var itemName = $(this).parents('tr').find('.itemCode').data('PN')
		 	var itemType = $(this).parents('tr').find('.itemCode').data('PT')

		 	var id = locationProductID+'_'+parseFloat(itemQty);

		 	var costPerUnit = unitPrice;
	 		selectedItems[locationProductID] = true;
	 		productSet[id] =  new product(locationProductID,selectedProduct,itemCode,itemName,itemQty,costPerUnit,locationProducts[selectedProduct].uom,selectedItemSupplier, umoID, itemType, totalPrice);
	 		locationProductID = null;
	 		itemSuppliers[selectedItemSupplier] = selectedItemSupplier;

	 		$(this).parents('tr').find('.add').addClass('hidden');
	 		$(this).parents('tr').find('.delete').removeClass('hidden');
		}
	 	
	});

	$('#item-table').on('click', '.remove-items',function() {
		var lineID = $(this).parents('tr').attr('data-id');
		delete(productSet[lineID]);
		var locID = lineID.split('_')[0];
		delete(selectedItems[locID]);
		$(this).parents('tr').remove();
		calculate();
	});

	$('#cost-table').on('click','#removeCost', function() {
		var costLineID = $(this).parents('tr').attr('data-id');
		delete(costSet[costLineID]);
		$(this).parents('tr').remove();
		calculate();
	});

	$('#cost-type-set').on('change', function() {
		costType = $(this).val();
		calculate();
	});

	function calculate() {
		// get all qty
		getTotalQty(productSet);
		getTotalCost(costSet);
		if (totalQty == 0) {
			$('.added-maped-line').remove();
		} else if (totalCost == 0) {
          	$('.added-maped-line').remove();
		} else {
			calculateItemCost(totalQty, totalCost);
		}
	}

	function calculateItemCost(totalQty, totalCost){
		// calculate unit price
		if (costType == 1) {
			var unitCost = parseFloat(totalCost)/parseFloat(totalQty);
		} else {
			var unitCost = parseFloat(totalCost)/parseFloat(totalItemPrice); 
		}
		$('.maped-item').removeClass('hidden');
		$('.added-maped-line').remove();
		for(var i in productSet) {
			productSet[i].itemCost = parseFloat(unitCost);
			if (costType == 2) {
				productSet[i].itemCost = (parseFloat(unitCost)* parseFloat(productSet[i].productTotal))/ parseFloat(productSet[i].pQuantity);
			} 
			productSet[i].itemTotalCost = parseFloat(productSet[i].itemCost) + parseFloat(productSet[i].pUnitPrice);
			addFinalItemLine(productSet[i]);
		}
					
	}
	function addFinalItemLine(itemSet) {
		var clonedMapedRow = $($('.maped-item-sample').clone()).removeClass('hidden maped-item-sample').addClass('added-maped-line');
	    $('.item-name-code', clonedMapedRow).val(itemSet.pCode + ' - ' + itemSet.pName);
	    $('.maped-unit-price', clonedMapedRow).val(parseFloat(itemSet.pUnitPrice).toFixed(2)).addUomPrice(itemSet.uom);
	    $('.maped-unit-price', clonedMapedRow).parent().addClass('input-group').attr('disabled',true);
        
        $('.maped-qty', clonedMapedRow).val(parseFloat(itemSet.pQuantity).toFixed(2)).addUom(itemSet.uom);
        $('.maped-qty', clonedMapedRow).parent().addClass('input-group').attr('disabled',true);
        $('.maped-cost', clonedMapedRow).val(parseFloat(itemSet.itemCost).toFixed(2)).addUomPrice(itemSet.uom);
	    $('.maped-cost', clonedMapedRow).parent().addClass('input-group').attr('disabled',true);
	    $('.maped-total-cost', clonedMapedRow).val(parseFloat(itemSet.itemTotalCost).toFixed(2)).addUomPrice(itemSet.uom);
	    $('.maped-total-cost', clonedMapedRow).parent().addClass('input-group').attr('disabled',true);
        $('.uomqty', clonedMapedRow).attr('disabled',true);
	    clonedMapedRow.insertBefore('.maped-item-sample');
	}

	function getTotalQty(itemDetails) {
		totalQty = 0;
		totalItemPrice = 0;
		for(var i in itemDetails) {
			totalQty += parseFloat(itemDetails[i].pQuantity);
			totalItemPrice += parseFloat(itemDetails[i].productTotal);
		}

	}

	function getTotalCost(costSet) {
		totalCost = 0;
		for(var i in costSet) {
			totalCost += parseFloat(costSet[i]);
		}
	}

	$('#convert-to-grn').on('click', function() {
		var templateName = $('.temp-name').val();
		var templateComment = $('.temp-comment').val();
		costType = $('#cost-type-set').val(); 
		productList = [];
		for(var i in productSet) {
			productList.push(productSet[i]);
		}
		
		if(templateName == '') {
			p_notification(false, eb.getMessage('ERR_TEMPLATE_NAME_NULL'));
		} else if (templateComment == '') {
			p_notification(false, eb.getMessage('ERR_TEMPLATE_COMMENT_NULL'));

		} else if (productList.length == 0) {
			p_notification(false, eb.getMessage('ERR_NO_PRODUCTS'));

		} else if (costSet.length == 0) {
			p_notification(false, eb.getMessage('ERR_NO_COSTS'));

		} else {
			var postData = {
					name: templateName,
	                location: locationID,
	                comment: templateComment,
	                cost: costSet,
	                products: productList,
	                itemSuppliers: itemSuppliers,
	                supplierCostSet: supplierCostSet,
	                costSuppliers: costSuppliers,
	                totalCost: totalCost,
	                totalQty: totalQty,
	                totalItemPrice: totalItemPrice,
	                costType: costType,
	                ignoreBudgetLimit: ignoreBudgetLimitFlag,
	                selectedPO: selectedPO
	            };
			
			saveCompoundCosting(postData);
		}
		
	});

	function saveCompoundCosting(postData)
	{
		eb.ajax({
            type: 'POST',
            url: BASE_URL + '/api/grn/save-compound-costing',
            data: postData,
            success: function(respond) {
            	
            	if(respond.status) {
            		var url = BASE_URL + '/pi/list/';
            		// var url = BASE_URL + '/grn/create/compound/'+respond.data;
                    window.location.assign(url);
            	} else {
            		if (respond.data == "NotifyBudgetLimit") {
                        bootbox.confirm(respond.msg+' ,Are you sure you want to save ?', function(result) {
                            if (result == true) {
                                ignoreBudgetLimitFlag = true;
                                postData.ignoreBudgetLimit = true;
                                saveCompoundCosting(postData);
                            } else {
                                setTimeout(function(){ 
                                    location.reload();
                                }, 3000);
                            }
                        });
                    } else {
            			p_notification(false, eb.getMessage(respond.msg));
                    }
            	}
            }
        });
	}
});