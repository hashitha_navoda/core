$(function() {
    var employeeID;
    var employeeFlag = false;
    $('#employee-list').on('click', 'a.edit', function(e) {
        e.preventDefault();
        $('.add').addClass('hidden');
        $('.update').removeClass('hidden');
        employeeID = $(this).parents('tr').data('employeesid');
        $('#employeeFirstName').val($(this).parents('tr').find('.fName').text());
        $('#employeeSecondName').val($(this).parents('tr').find('.sName').text());
        $('#telephoneNumber').val($(this).parents('tr').find('.emptp').text());
        $('#employeeEmail').val($(this).parents('tr').find('.empEmail').text());
        $('#userID').val($(this).parents('tr').find('.userID').attr('data-userID')).selectpicker('render');
        var hourlyValue = parseFloat($(this).parents('tr').find('.empHoCst').text());
        $('#empHourlyCost').val(hourlyValue.toFixed(2));
        $('#designation').val($(this).parents('tr').find('.desName').data('desid'));
        $('#designation').selectpicker('render');
        $('#division').val($(this).parents('tr').find('.divName').data('diviid'));
        $('#division').selectpicker('render');
        employeeFlag = true;
        $("html, body").animate({scrollTop: $(".category-form-container").offset().top - 80});
    });

    $('#create-employee-form').on('submit', function(e) {
        e.preventDefault();
        if (employeeFlag) {
            update();
        } else {
            add();
        }
        return false;
    });
    $('#create-employee-form').on('reset', function(e) {
        setTimeout(function() {
            $('#create-employee-form').find("select").trigger('change');
        }, 1);
    });

    $('#employee-list').on('click', 'a.status', function() {
        var msg;
        var status;
        var employeeID = $(this).parents('tr').data('employeesid');
        var empFlag = true;
        var currentDiv = $(this).contents();
        if ($(this).children().hasClass('fa-check-square-o')) {
            msg = 'Are you sure you want to Deactivate this employee';
            status = '0';
        }
        else if ($(this).children().hasClass('fa-square-o')) {
            msg = 'Are you sure you want to Activate this employee';
            status = '1';
        }
        activeInactiveType(employeeID, status, currentDiv, msg, empFlag);
    });

    $('#employee-list').on('click', 'a.delete', function(e) {
        e.preventDefault();
        employeeID = $(this).parents('tr').data('employeesid');
        var currentDiv = $(this).parents('tr').find('a.status').contents();
        var empFlag = false;
        var msg = 'This employee cannot be deleted. Do you want to de-activate this employee';
        var formData = {
            employeesID: employeeID,
        };
        bootbox.confirm('Are you sure you want to delete this Employee?', function(result) {
            if (result === true) {
                eb.ajax({
                    url: BASE_URL + '/employee_api/delete',
                    method: 'post',
                    data: formData,
                    dataType: 'json',
                    success: function(data) {
                        if (data.status == true) {
                            p_notification(data.status, data.msg);
                            window.location.reload();
                        }
                        else if (data.status == false && data.data == 'deactive') {
                            activeInactiveType(employeeID, '0', currentDiv, msg, empFlag);
                        } else {
                            p_notification(data.status, data.msg);

                        }
                    }
                });
            }
        });
    });
    $('#employee-search').on('click', '#emp-search', function(e) {
        e.preventDefault();
        searchKey = $('#employee-search-keyword').val();
        var formData = {
            searchKey: searchKey,
        }
        if (formData.searchKey) {
            searchValue(formData);
        } else {
            p_notification(false, eb.getMessage('ERR_NO_KEY_WORD'));
        }

    });
    $('#employee-search').on('click', '.reset', function(e) {
        e.preventDefault();
        searchValue('');
        $('#employee-search-keyword').val('');
    });
    $('#create-employee-form').on('.reset', function() {
        $('#designation').val('').datapicker('update');
    });

//This function use to validate All input data
    function validateFormData(formData) {
        var emailRegex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;

        if (formData.empFName == '') {
            p_notification(false, eb.getMessage('ERR_EMP_FNAME_CNT_BE_NULL'));
            $('#employeeFirstName').focus();
            return false;
        }
        if (formData.empSName == '') {
            p_notification(false, eb.getMessage('ERR_EMP_SNAME_CNT_BE_NULL'));
            $('#employeeSecondName').focus();
            return false;
        }
        if (formData.designation == '') {
            p_notification(false, eb.getMessage('ERR_DESIGNATION_CNT_BE_NULL'));
            $('#designation').focus();
            return false;
        }
        if (formData.employeeEmail == '') {
            p_notification(false, eb.getMessage('ERR_EMAIL_CNT_BE_NULL'));
            $('#employeeEmail').focus();
            return false;
        }
        if (!emailRegex.test(formData.employeeEmail)) {
            p_notification(false, eb.getMessage('ERR_EMAIL_NOT_VALID'));
            $('#employeeEmail').focus();
            return false;
        }
        if (formData.division == '') {
            p_notification(false, eb.getMessage('ERR_DIV_CNT_BE_NULL'));
            $('#division').focus();
            return false;
        }
//        if (formData.empHorlyCost == '') {
//            p_notification(false, eb.getMessage('ERR_HOURLY_COST_CNT_BE_NULL'));
//            $('#empHourlyCost').focus();
//            return false;
//        }
        if (formData.empHorlyCost) {
            if (isNaN(formData.empHorlyCost)) {
                p_notification(false, eb.getMessage('ERR_HOURLY_COST_TYPE'));
                $('#empHourlyCost').focus();
                return false;
            }
            if (parseInt(formData.empHorlyCost) < 0) {
                p_notification(false, eb.getMessage('ERR_HOURLY_COST_TYPE_MINUS'));
                $('#empHourlyCost').focus();
                return false;
            }
        }
        if (!isPhoneValid(formData.telephoneNumber)) {
            p_notification(false, eb.getMessage('ERR_TP_NUM'));
            $('#telephoneNumber').focus();
            return false;
        }

        return true;
    }


    var isPhoneValid = function(phoneNumber) {
        isValid = true;
        if (phoneNumber.length != 0) {
            if (isNaN(phoneNumber) || phoneNumber.length < 9 || phoneNumber.length > 15) {
                isValid = false;
            }
        }
        return isValid;
    }

    function searchValue(formData) {
        eb.ajax({
            url: BASE_URL + '/employee_api/getEmployeeBySearchKey',
            method: 'post',
            data: formData,
            dataType: 'json',
            success: function(data) {
                if (data.status == true) {
                    if (data.status) {
                        $("#employee-list").html(data.html);
                    } else {
                        p_notification(data.status, data.msg);
                    }
                }
            }
        });
    }
    //this function use to Send data to the update action in to the employee api controller
    function update() {
        var formData = {
            employeesID: employeeID,
            empFName: $('#employeeFirstName').val(),
            empSName: $('#employeeSecondName').val(),
            designation: $('#designation').val(),
            division: $('#division').val(),
            telephoneNumber: $('#telephoneNumber').val(),
            employeeEmail: $('#employeeEmail').val(),
            empHorlyCost: $('#empHourlyCost').val(),
            userID: $('#userID').val(),
        };
        if (validateFormData(formData)) {
            eb.ajax({
                url: BASE_URL + '/employee_api/update',
                method: 'post',
                data: formData,
                dataType: 'json',
                success: function(data) {
                    p_notification(data.status, data.msg);
                    window.setTimeout(function() {
                        location.reload();
                    }, 1000);
                }
            });
        }
    }
    //this function use to Send data to the add action in to the employee api controller
    function add() {
        var formData = {
            empFName: $('#employeeFirstName').val(),
            empSName: $('#employeeSecondName').val(),
            designation: $('#designation').val(),
            division: $('#division').val(),
            employeeEmail: $('#employeeEmail').val(),
            telephoneNumber: $('#telephoneNumber').val(),
            empHorlyCost: $('#empHourlyCost').val(),
            userID: $('#userID').val(),
        };

        if (validateFormData(formData)) {
            eb.ajax({
                url: BASE_URL + '/employee_api/add',
                method: 'post',
                data: formData,
                dataType: 'json',
                success: function(data) {
                    p_notification(data.status, data.msg);
                    if (data.status) {
                        window.setTimeout(function() {
                            location.reload();
                        }, 1000);
                    } else {
                        if (data.data == "EMP_EXIST") {
                            $('#employeeFirstName').focus();
                        }
                    }
                }
            });
        }
    }
    /**
     * use to change active state
     */
    function activeInactiveType(type, status, currentDiv, msg, empFlag) {
        bootbox.confirm(msg, function($result) {
            if ($result == true) {
                eb.ajax({
                    url: BASE_URL + '/employee_api/changeStatusID',
                    method: 'post',
                    dataType: 'json',
                    data: {
                        'employeeID': type,
                        'status': status,
                    },
                    success: function(data) {
                        if (data.status) {
                            p_notification(data.status, data.msg);
                            if (currentDiv.hasClass('fa-square-o') && empFlag) {
                                currentDiv.addClass('fa-check-square-o');
                                currentDiv.removeClass('fa-square-o');

                            }
                            else {
                                currentDiv.addClass('fa-square-o');
                                currentDiv.removeClass('fa-check-square-o');
                            }
                        } else {
                            p_notification(data.status, data.msg);
                        }
                    }
                });
            }
        });
    }

});


