$(function() {
    var jobStationID;
    var jobStationFlag = false;
    $('#job-station-list').on('click', 'a.edit', function(e) {
        e.preventDefault();
        $('.add').addClass('hidden');
        $('.update').removeClass('hidden');
        jobStationID = $(this).parents('tr').data('jobstationid');
        $('#jobStationName').val($(this).parents('tr').find('.joStName').text());
        $('#maxValue').val($(this).parents('tr').find('.joStMx').text());
        $('#minValue').val($(this).parents('tr').find('.joStMn').text());
        jobStationFlag = true
    });

    $('#create-job-station-form').on('submit', function(e) {
        e.preventDefault();
        if (jobStationFlag) {
            update();
        } else {
            add();
        }
    });

    $('#job-station-list').on('click', 'a.status', function() {
        var msg;
        var status;
        var jobTypeFlag = true;
        var jobStationID = $(this).parents('tr').data('jobstationid');
        var currentDiv = $(this).contents();
        if ($(this).children().hasClass('fa-check-square-o')) {
            msg = 'Are you sure you want to Deactivate this Job Station';
            status = '0';
        }
        else if ($(this).children().hasClass('fa-square-o')) {
            msg = 'Are you sure you want to Activate this Job Station';
            status = '1';
        }
        activeInactiveType(jobStationID, status, currentDiv, msg, jobTypeFlag);
    });

    $('#job-station-list').on('click', 'a.delete', function(e) {
        e.preventDefault();
        jobStationID = $(this).parents('tr').data('jobstationid');
        var currentDiv = $(this).parents('tr').find('a.status').contents();
        var jobTypeFlag = false;
        var msg = eb.getMessage('JOB_STATION_DELETE_AND_STATE');
        var formData = {
            jobStationID: jobStationID,
        };
        bootbox.confirm('Are you sure you want to delete this Job Station?', function(result) {
            if (result === true) {
                eb.ajax({
                    url: BASE_URL + '/job-station-api/delete',
                    method: 'post',
                    data: formData,
                    dataType: 'json',
                    success: function(data) {
                        if (data.status == true) {
                            p_notification(data.status, data.msg);
                            window.location.reload();
                        } else if (data.status == false && data.data == 'deactive') {
                            activeInactiveType(jobStationID, '0', currentDiv, msg, jobTypeFlag);
                        } else {
                            p_notification(data.status, data.msg);

                        }
                    }
                });
            }
        });
    });
    $('#job-station-search').on('click', '#jo-station-search', function(e) {
        e.preventDefault();
        searchKey = $('#jobStation-search-keyword').val();
        var formData = {
            searchKey: searchKey,
        };
        if (formData.searchKey) {
            searchValue(formData);
        } else {
            p_notification(false, eb.getMessage('ERR_NO_KEY_WORD'));
        }

    });

    $('#job-station-search').on('click', '.reset', function(e) {
        e.preventDefault();
        searchValue('');
        $('#jobStation-search-keyword').val('');
    });


    function validateFormData(formData) {

        if (formData.jobStationName == '') {
            p_notification(false, eb.getMessage('ERR_JOB_ST_NAME_CNT_BE_NULL'));
            $('#jobStationName').focus();
            return false;
        }
        if (isNaN(formData.maxValue)) {
            p_notification(false, eb.getMessage('ERR_MAX_VALUE_TYPE'));
            $('#maxValue').focus();
            return false;
        }
        if (isNaN(formData.minValue)) {
            p_notification(false, eb.getMessage('ERR_MIN_VALUE_TYPE'));
            $('#minValue').focus();
            return false;
        }
        if (formData.maxValue && formData.minValue) {
            if (parseInt(formData.minValue) > parseInt(formData.maxValue)) {
                p_notification(false, eb.getMessage('ERR_MIN_MAX_VALUE_LEVEL'));
                $('#minValue').focus();
                return false;
            }
        }
        if (formData.maxValue) {
            if (parseInt(formData.maxValue) <= 0) {
                p_notification(false, eb.getMessage('ERR_MAX_VALUE_MINUS'));
                $('#maxValue').focus();
                return false;
            }
        }
        if (formData.minValue) {
            if (parseInt(formData.minValue) <= 0) {
                p_notification(false, eb.getMessage('ERR_MIN_VALUE_MINUS'));
                $('#minValue').focus();
                return false;
            }
        }
        if (formData.maxValue == '' && formData.minValue != '') {
            p_notification(false, eb.getMessage('ERR_MAX_ZERO_VALUE_LEVEL'));
            $('#maxValue').focus();
            return false;
        }
        return true;
    }
    function searchValue(formData) {
        eb.ajax({
            url: BASE_URL + '/job-station-api/getJobStationBySearchKey',
            method: 'post',
            data: formData,
            dataType: 'json',
            success: function(data) {
                if (data.status == true) {
                    if (data.status) {
                        $("#job-station-list").html(data.html);
                    } else {
                        p_notification(data.status, data.msg);
                    }
                }
            }
        });
    }
    //This function use to send data to the update action into the job station api controller
    function update() {
        var formData = {
            jobStationName: $('#jobStationName').val(),
            maxValue: $('#maxValue').val(),
            minValue: $('#minValue').val(),
            jobStationID: jobStationID,
        };
        if (validateFormData(formData)) {
            eb.ajax({
                url: BASE_URL + '/job-station-api/update',
                method: 'post',
                data: formData,
                dataType: 'json',
                success: function(data) {
                    p_notification(data.status, data.msg);
                    if (data.status) {
                        window.setTimeout(function() {
                            location.reload();
                        }, 1000);
                    } else {
                        if (data.data == "JOB_STATION_EXIST") {
                            $('#jobStationName').focus();
                        }
                    }
                }
            });
        }
    }
    //This function use to send data to the add action into the job station api controller
    function add() {
        var formData = {
            jobStationName: $('#jobStationName').val(),
            jobStationID: $('#jobStationID').val(),
            maxValue: $('#maxValue').val(),
            minValue: $('#minValue').val(),
        };
        if (validateFormData(formData)) {
            eb.ajax({
                url: BASE_URL + '/job-station-api/add',
                method: 'post',
                data: formData,
                dataType: 'json',
                success: function(data) {
                    p_notification(data.status, data.msg);
                    if (data.status) {
                        window.setTimeout(function() {
                            location.reload();
                        }, 1000);
                    } else {
                        if (data.data == "JOB_STATION_EXIST") {
                            $('#jobStationName').focus();
                        }
                    }
                }
            });
        }
    }
    /**
     * use to change active state
     */
    function activeInactiveType(type, status, currentDiv, msg, jobTypeFlag) {
        bootbox.confirm(msg, function($result) {
            if ($result == true) {
                eb.ajax({
                    url: BASE_URL + '/job-station-api/changeStatusID',
                    method: 'post',
                    dataType: 'json',
                    data: {
                        'jobStationID': type,
                        'status': status,
                    },
                    success: function(data) {
                        if (data.status) {
                            p_notification(data.status, data.msg);
                            if (currentDiv.hasClass('fa-square-o') && jobTypeFlag) {
                                currentDiv.addClass('fa-check-square-o');
                                currentDiv.removeClass('fa-square-o');
                            }
                            else {
                                currentDiv.addClass('fa-square-o');
                                currentDiv.removeClass('fa-check-square-o');
                            }
                        } else {
                            p_notification(data.status, data.msg);
                        }
                    }
                });
            }
        });
    }

});


