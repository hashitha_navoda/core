
$(document).ready(function() {
    var customerID;
    var jobTypeID;
    var employeeID;
    var jobId;
    var empID;
    var tableID;
    var projectID;
    var jobStationID;
    var currentWeight;
    var totalWeight;
    var supervisorFlag = false;
    var ownerFlag = false;
    var goActivity = false;
    var suVisor = new Array();
    var multyOwn = new Array();
    var jobWeightId = new Array();
    var jobWeightValue = new Array();
    var formData;
    $('#jobEstimetedTime').addDuration();

    $('#customer_more').hide();
    $('#addCustomerModal').on('click', '#moredetails', function() {
        $('#customer_more').slideDown();
        $('#addCustomerModal #moredetails').hide();
    });
    $('#job_more').hide();
    $('#moredetails').on('click', function() {
        $('#job_more').slideDown();
        $('#moredetails').hide();
    });
    if ($('#projectReference').data('pid') != '') {
        projectID = $('#projectReference').data('pid');
        $('#projectReference').
                append($("<option></option>").
                        attr("value", projectID).
                        text(PROJECT_LIST[projectID]));
        $('#projectReference').val(projectID).prop('disabled', true);
        $('#projectReference').selectpicker('refresh');
        $('.cus-prof-create').removeClass('hidden');
        setSupervisorsAndOwnersByProjectID(projectID);
    }

    if ($('#customerName').data('cusid') != '') {
        customerID = $('#customerName').data('cusid');
        $('#customerName').
                append($("<option></option>").
                        attr("value", customerID).
                        text(CUSTOMERS_NAMES[customerID]));
        $('#customerName').val(customerID).prop('disabled', true);
        $('#addCustomerBtn').addClass('disabled');
        $('.cus-prof-create').removeClass('hidden');
        $('#customerName').selectpicker('refresh');
    }
    $('#addCustomerBtn').on('click', function(e) {
        e.preventDefault();
        if ($(this).hasClass('disabled')) {
            return false;
        }
    });
    if ($('#jobType').data('jbid') != '') {
        jobTypeID = $('#jobType').data('jbid');
        $('#jobType').
                append($("<option></option>").
                        attr("value", jobTypeID).
                        text(JOB_TYPES[jobTypeID]));
        $('#jobType').val(jobTypeID);
        $('#jobType').selectpicker('refresh');
    }

    if ($('#jobStation').data('jbstationid') != '') {
        jobStationID = $('#jobStation').data('jbstationid');
        $('#jobStation').
                append($("<option></option>").
                        attr("value", jobStationID).
                        text(JOB_STATION[jobStationID]));
        $('#jobStation').val(jobStationID);
        $('#jobStation').selectpicker('refresh');
    }

    loadDropDownFromDatabase('/project-api/searchProjectsForDropdown', "", 1, '#projectReference');
    $('#projectReference').selectpicker('refresh');
    $('#projectReference').on('change', function() {
        if (!ownerFlag) {
            $('#multipleJOwner').children('tbody').children('tr').remove();
        }
        if (!supervisorFlag) {
            $('#multipleSupervisor').children('tbody').children('tr').remove();
        }
        if ($(this).val() > 0 && ($(this).val() != projectID)) {
            projectID = $(this).val();
            customerID = setCustomerProfileForProjectReference(projectID);
            if(customerID){
                setSupervisorsAndOwnersByProjectID(projectID);
            }
        }
    });
    loadDropDownFromDatabase('/job-station-api/searchJobStationForDropdown', "", 1, '#jobStation', false, false, false, true);
    $('#jobStation').selectpicker('refresh');
    $('#jobStation').on('change', function() {
        jobStationID = $(this).val();
    });
    loadDropDownFromDatabase('/customerAPI/get-customer-details-by-customer-name-code-or-telephone', "", 0, '#customerName');
    $('#customerName').selectpicker('refresh');
    $('#customerName').on('change', function() {
        if ($(this).val() > 0 && ($(this).val() != customerID)) {
            customerID = $(this).val();
            $('#customerAddress').val('');
            setCustomerProfiles(customerID);
        }
    });
    loadDropDownFromDatabase('/job-api/searchAllJobsForDropdown', "", 0, '#jobSelection');
    $('#jobSelection').selectpicker('refresh');
    $('#jobSelection').on('change', function() {
        if ($(this).val() > 0) {
            var jobID = $(this).val();
            eb.ajax({
                url: BASE_URL + '/job-api/get-job-list-view-by-search',
                method: 'POST',
                data: {jobID: jobID},
                success: function(respond) {
                    if (respond.status) {
                        $('#from-date').val('');
                        $('#to-date').val('');
                        $('#search-job-list').html(respond.html);
                    }
                }
            });
        }
    });
    $('#date-filter').on('click', function(e) {
        e.preventDefault();
        if ($('#from-date').val() == '' || $('#to-date').val() == '') {
            p_notification(false, eb.getMessage('ERR_INQ_LOG_VIEW_DATE_RANGE'));
            return false;
        }
        eb.ajax({
            url: BASE_URL + '/job-api/getJobListViewByDateRange',
            method: 'POST',
            data: {fromDate: $('#from-date').val(), toDate: $('#to-date').val()},
            success: function(respond) {
                if (respond.status) {
                    $('#jobSelection').val('').selectpicker('render');
                    $('#search-job-list').html(respond.html);
                }
            }
        });
    });
    $('#clear-filter').on('click', function(e) {
        e.preventDefault();
        eb.ajax({
            url: BASE_URL + '/job-api/get-job-list-view-by-search',
            method: 'POST',
            success: function(respond) {
                if (respond.status) {
                    $('#from-date').val('');
                    $('#to-date').val('');
                    $('#jobSelection').val('').selectpicker('render');
                    $('#search-job-list').html(respond.html);
                }
            }
        });
    });
    ///////DatePicker\\\\\\\
    var nowTemp = new Date();
    var frmdate;
    var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);
    var checkin = $('#from-date').datepicker().on('changeDate', function(ev) {
        checkin.hide();
        frmdate = ev.date;
        $('#to-date')[0].focus();
    }).data('datepicker');
    var checkout = $('#to-date').datepicker({
        onRender: function(date) {
            return date.valueOf() < frmdate ? 'disabled' : '';
        }
    }).on('changeDate', function(ev) {
        checkout.hide();
    }).data('datepicker');
/////EndOFDatePicker\\\\\\\\\

    loadDropDownFromDatabase('/job-type-api/searchJobTypesForDropdown', "", 1, '#jobType', false, false, false, true);
    $('#jobType').selectpicker('refresh');
    $('#jobType').on('change', function() {
        jobTypeID = $(this).val();
    });
    loadDropDownFromDatabase('/employee_api/searchEmployeeForDropdown', "", 1, '#jobOwner');
    $('#jobOwner').selectpicker('refresh');
    $('#jobOwner').on('change', function() {
        if ($(this).val() > 0) {
            employeeID = $(this).val();
            empID = '#multipleJOwner';
            multipleEmployee(employeeID, empID);
            ownerFlag = true;
        }
        $('#jobOwner').
                append($("<option></option>").
                        attr("value", 'select').
                        text("Select Job Owners"));
        $('#jobOwner').val('select');
        $('#jobOwner').selectpicker('refresh');
    });
    loadDropDownFromDatabase('/employee_api/searchEmployeeForDropdown', "", 1, '#jobSupervisor');
    $('#jobSupervisor').selectpicker('refresh');
    $('#jobSupervisor').on('change', function() {
        if ($(this).val() > 0) {
            employeeID = $(this).val();
            empID = '#multipleSupervisor';
            multipleEmployee(employeeID, empID);
            supervisorFlag = true;
        }
        $('#jobSupervisor').
                append($("<option></option>").
                        attr("value", 'select').
                        text("Select Job Supervisors"));
        $('#jobSupervisor').val('select');
        $('#jobSupervisor').selectpicker('refresh');
    });
    $('#jobName').typeahead({
        ajax: {
            url: BASE_URL + '/job-api/get-jobs-for-typeahead',
            timeout: 250,
            triggerLength: 1,
            method: "POST",
            preProcess: function(respond) {
                if (!respond.status) {
                    return false;
                }
                return respond.data;
            }
        }
    });
    $('#multipleSupervisor').on('click', '.delete', function(e) {
        e.preventDefault();
        $(this).parents('tr').remove();
    });
    $('#multipleJOwner').on('click', '.delete', function(e) {
        e.preventDefault();
        $(this).parents('tr').remove();
    });
    function checkEmployeeExists(empID) {
        var empFlag = false;
        $(empID).children('tbody').find('tr').each(function() {
            if ($(this).data('empid')) {
                empFlag = true;
            }

        });
        if (!empFlag) {
            return true;
        }
        else {
            return false;
        }
    }
    function multipleEmployee(key, empID) {
        var flag = true;
        $(empID).children('tbody').find('tr').each(function() {
            if ($(this).data('empid') == key) {
                p_notification(false, eb.getMessage('ERR_JOB_OWNER_ALREDY_ADDED', EMPLOYE_LIST[key]));
                flag = false;
                return;
            }
        });
        if (flag) {
//Job Owner
            tableID = empID + ' tbody';
            $(empID).removeClass('hidden');
            var $newTableRow = "<tr class='emplist' data-empid='" + key + "'><td>" + EMPLOYE_LIST[key] + "</td>\n\
                <td class='text-right'>\n\
                    <a class='delete btn btn-default'>\n\
                        <i class='pointer_cursor fa fa-trash-o'></i>\n\
                    </a>\n\
                </td>\n\
            </tr>";
            $($(tableID)).append($newTableRow);
            $('#jobOwner').val('');
            $('#jobSupervisor').val('');
        }
    }
    function setSupervisorsAndOwnersByProjectID(projectID) {
        eb.ajax({
            type: 'POST',
            url: BASE_URL + '/project-api/getProjectSupervisorAndProjectOwnerByProjectID',
            data: {projectID: projectID},
            success: function(respond) {
                if (respond.data[2] > 0) {
                    $('#multipleJOwner').removeClass('hidden');
                }
                if (respond.data[3] > 0) {
                    $('#multipleSupervisor').removeClass('hidden');
                }
                for (i = 0; i < respond.data[2]; i++) {
                    empID = '#multipleJOwner';
                    var flag = checkEmployeeExists(empID);
                    if (flag) {
                        multipleEmployee(respond.data[1][i], empID);
                    }
                }
                for (i = 0; i < respond.data[3]; i++) {
                    empID = '#multipleSupervisor';
                    var flag = checkEmployeeExists(empID);
                    if (flag) {
                        multipleEmployee(respond.data[0][i], empID);
                    }
                }
            }
        });
    }
    function setCustomerProfileForProjectReference(projectID) {
        eb.ajax({
            type: 'POST',
            url: BASE_URL + '/project-api/getProjectDetails',
            data: {projectID: projectID},
            success: function(respond) {
                if (respond.status) {
                    var profileId = respond.data['customerProfileID'];
                    var setProfile = function(profileId) {
                        var prof = $('.customer-profiles .customer-profile-list');
                        for (var i = 0; i < prof.length; i++) {
                            if ($(prof[i]).attr('data-cpid') == profileId) {
                                $(prof[i]).find('input').attr('checked', 'true');
                                break;
                            }
                        }
                        getCustomerProfileDetails(profileId);
                    };
                    customerID = respond.data['customerID'];
                    setCustomerProfiles(customerID, setProfile, profileId);
                    $('#customerName').
                            append($("<option></option>").
                                    attr("value", customerID).
                                    text(CUSTOMERS_NAMES[customerID]));
                    $('#customerName').val(customerID).prop('disabled', true);
                    $('#customerName').selectpicker('refresh');
                    return true;
                } else {
                    p_notification(respond.status, respond.msg);
                    resetSelectPicker("projectReference","Project Reference");
                    projectID = null;
                    return false;
                }
            },
        });
    }

    $('#setJobWeightModal .refresh').on('click', function(e) {
        e.preventDefault();
        $('.job-weight-list').remove();
        $('.total-weight').remove();
        $('#cur-job-weight').val(' ');
    });
    $('#update').on('click', function(e) {
        e.preventDefault();
        jobId = $(this).val();
        if (!customerID) {
            customerID = $('#customerName').data('cusid');
        }
        if (!jobTypeID) {
            jobTypeID = $('#jobType').data('jbid');
        }
        if (!jobStationID) {
            jobStationID = $('#jobStation').data('jbstationid');
        }

    });

    $('.reset').on('click', function(e) {
        resetSelectPicker("projectReference","Project Reference");
        resetSelectPicker("customerName","Customer");
        resetSelectPicker("jobType","Job Type");
        resetSelectPicker("jobOwner","Job Owners");
        resetSelectPicker("jobSupervisor","Job Supervisors");
        resetSelectPicker("jobStation","Job Station");

        customerID = $('#customerName').data('cusid');
        $('#customerName').val(customerID).prop('disabled', false);
        $('#addCustomerBtn').removeClass('disabled');
        $('.cus-prof-create').addClass('hidden');
        $('#customerName').selectpicker('refresh');
        customerID = '';
    });

    $('#addAndGoActivity').on('click', function(e) {
        e.preventDefault();
        goActivity = true;
    });
    $('#add,#addAndGoActivity').on('click', function(e) {
        e.preventDefault();
        jobId = null;
        if (!customerID) {
            customerID = $('#customerName').attr('data-cusid');
        }
    });
    $('#update').on('click', function(e) {
        e.preventDefault();
        saveAndUpdate();
    });
    $('#add,#addAndGoActivity').on('click', function(e) {
        e.preventDefault();
        formData = {
            jobId: jobId,
            customerName: $('#customerName').val(),
            cusName: customerID,
            customerProfileID: $("input[name='cProfile']:checked").parents('tr').data('cpid'),
            jobName: $('#jobName').val(),
            jobTypeName: $('#jobType').val(),
            jobEstCost: $('#estimatedCost').val()
        };
        if (validation(formData)) {
            if (!projectID) {
                saveAndUpdate();
            }
            else {
                getDataForJobWeight(projectID);
                $('#setJobWeightModal').modal('show');
//                $('#job-weight-update').addClass('hidden');
                $('#job-weight-update').removeClass('hidden');
                $('#all-job-weight-update').addClass('hidden');
                $('#save_weight').removeClass('hidden');
            }
        }
    });
    $('#save_weight').on('click', function(e) {
        e.preventDefault();
        currentWeight = $('#cur-job-weight').val();
        if (parseFloat(currentWeight) < 0) {
            p_notification(false, eb.getMessage('ERROR_JOB_WEIGHT_CANT_NEGATIVE'));
            $('#cur-job-weight').focus();
            return false;
        }
        if (isNaN(parseFloat(currentWeight))) {
            p_notification(false, eb.getMessage('ERR_JOB_WEIGHT_DATA_TYPE'));
            $('#cur-job-weight').focus();
            return false;
        }
        totalWeight = $('#total-weight').data('id');
        if (parseFloat(currentWeight) + parseFloat(totalWeight) > 100) {
            p_notification(false, eb.getMessage('ERROR_JOB_WEIGHT_OVER_LIMIT'));
            $('#cur-job-weight').focus();
            $('#job-weight-update').removeClass('hidden');
            return false;
        }
        saveAndUpdate();
    });
    $('#job-weight-update').on('click', function(e) {
        $('#add_jw').addClass('hidden');
        $('#update_jw').removeClass('hidden');
        $('#save_weight').addClass('hidden');
        $('#job-weight-update').addClass('hidden');
        $('#all-job-weight-update').removeClass('hidden');
        getDataForJobWeight(projectID, true);
        $('#setJobWeightModal').modal('show');
    });
    $('#all-job-weight-update').on('click', function(e) {
        var checkUpdatedValue = 0;
        var tr_update = 1;
        var counter = 0;
        $('#addJobWeight tbody tr').each(function() {
            var id = '#tr-job-weight_' + tr_update;
            var idValue = '#jweightUp_' + tr_update;
            var updatedJobID = $(id).data('jobyid');
            var updatedValue = $(idValue).val();
            if (parseFloat(updatedValue) <= 0) {
                p_notification(false, eb.getMessage('ERROR_JOB_WEIGHT_CANT_NEGATIVE'));
                $(idValue).focus();
                counter++;
                return false;
            }
            if (isNaN(parseFloat(updatedValue))) {
                p_notification(false, eb.getMessage('ERR_JOB_WEIGHT_DATA_TYPE'));
                $(idValue).focus();
                counter++;
                return false;
            }
            jobWeightId[tr_update] = updatedJobID;
            jobWeightValue[tr_update] = updatedValue;
            checkUpdatedValue += parseFloat(updatedValue);
            tr_update++;
        });
        if (counter > 0) {
            return false;
        }
        checkUpdatedValue += parseFloat($('#cur-job-weight').val());
        currentWeight = parseFloat($('#cur-job-weight').val());
        if (parseFloat(currentWeight) < 0) {
            p_notification(false, eb.getMessage('ERROR_JOB_WEIGHT_CANT_NEGATIVE'));
            $('#cur-job-weight').focus();
            return false;
        }
        if (isNaN(parseFloat(currentWeight))) {
            p_notification(false, eb.getMessage('ERR_JOB_WEIGHT_DATA_TYPE'));
            $('#cur-job-weight').focus();
            return false;
        }
        if (parseFloat(checkUpdatedValue) > 100) {
            p_notification(false, eb.getMessage('ERROR_JOB_WEIGHT_OVER_LIMIT'));
            return false;
        }
        saveAndUpdate();
        updateWeight(jobWeightId, jobWeightValue, projectID);
    });
    function updateWeight(array, array2, projectID) {
        eb.ajax({
            url: BASE_URL + '/job-api/updateWeight',
            method: 'post',
            data: {array: array, array2: array2, projectID: projectID},
            dataType: 'json',
            success: function(data) {
                p_notification(data.status, data.msg);
                if (data.status) {
                    p_notification(true, eb.getMessage('WEIGHT_UPDATE'));
                } else {

                }
            }
        });
    }

    function saveAndUpdate() {
        var supervisorAutoInc = 0;
        var multyOwnAutoInc = 0;
        $('#multipleSupervisor tbody tr').each(function() {
            var su = $(this).data('empid');
            suVisor[supervisorAutoInc] = su;
            supervisorAutoInc++;
        });
        $('#multipleJOwner tbody tr').each(function() {
            var su = $(this).data('empid');
            multyOwn[multyOwnAutoInc] = su;
            multyOwnAutoInc++;
        });
        formData = {
            jobId: jobId,
            customerName: $('#customerName').val(),
            cusName: customerID,
            customerProfileID: $("input[name='cProfile']:checked").parents('tr').data('cpid'),
            jobName: $('#jobName').val(),
            jobTypeName: $('#jobType').val(),
            jobType: jobTypeID,
            jobOwner: multyOwn,
            jobOwnerLenght: multyOwn.length,
            jobSupervisor: suVisor,
            jobSupervisorLength: suVisor.length,
            jobDescrip: $('#jobDescription').val(),
            jobEstTime: $('#jobEstimetedTime').val(),
            jobSTime: $('#jobStartingDate').val(),
            jobETime: $('#jobEndingDate').val(),
            jobEstCost: $('#estimatedCost').val(),
            jobStation: jobStationID,
            jobReference: $('#jobReference').val(),
            projectID: projectID,
            currentWeight: currentWeight,
            days: parseInt($('.days').val()),
            minutes: parseInt($('.minutes').val()),
            hours: parseInt($('.hours').val()),
            inqID: $('#jobReference').data('inquiryid'),
            address: $('#customerAddress').val(),
            jobRepeatEnabled: ($('#jobRepeatEnabled').is(':checked')) ? '1' : '0',
            jobRepeatComment: $('#jobRepeatComment').val(),
        };
        if (validation(formData)) {
            if (jobId == null) {
                eb.ajax({
                    url: BASE_URL + '/job-api/add',
                    method: 'post',
                    data: formData,
                    dataType: 'json',
                    success: function(data) {
                        p_notification(data.status, data.msg);
                        if (data.status) {
                            if (goActivity == true) {
                                var url = BASE_URL + '/activity/create/' + data.data;
                                window.location.assign(url);
                            } else {
                                window.setTimeout(function() {
                                    location.reload();
                                }, 1000);
                            }
                        } else {
                            if (data.data == "JOB_EXIST") {
                                $('#customerName').focus();
                            }
                        }
                    }
                });
            } else {
                eb.ajax({
                    url: BASE_URL + '/job-api/update',
                    method: 'post',
                    data: formData,
                    dataType: 'json',
                    success: function(data) {
                        p_notification(data.status, data.msg);
                        if (data.status) {
                            window.setTimeout(function() {
                                window.history.back();
                            }, 1000);
                        } else {
                            if (data.data == "JOB_EXIST") {
                                $('#customerName').focus();
                            }
                        }
                    }
                });
            }
        }
    }

    $('#jobEstimetedTime').on('click', function(e) {
        e.preventDefault();
        setDates(false);
    });
    var startdateFormat = $('#jobStartingDate').data('date-format');
    $('#jobStartingDate').datetimepicker({
        format: startdateFormat + ' hh:ii'
    }).on('change', function(ev) {
        $('#jobStartingDate').datetimepicker('hide');
        setDates(true);
    });
    var enddateFormat = $('#jobEndingDate').data('date-format');
    $('#jobEndingDate').datetimepicker({
        format: enddateFormat + ' hh:ii',
    }).on('change', function(ev) {
        $('#jobEndingDate').datetimepicker('hide');
        setDates(true);
    });
    $(document).on('click', '.delete-job', function(e) {
        e.preventDefault();
        var jobID = $(this).parents('tr').data('jobid');
        bootbox.confirm('Are you sure you want to delete this Job?', function(result) {
            if (result === true) {
                eb.ajax({
                    url: BASE_URL + '/job-api/delete',
                    method: 'post',
                    data: {jobID: jobID},
                    dataType: 'json',
                    success: function(data) {
                        p_notification(data.status, data.msg);
                        if (data.status == true) {
                            window.location.reload();
                        }
                    }
                });
            }
        });
    });
    $('#job-search').on('click', '#j-search', function(e) {
        e.preventDefault();
        var searchKey = $('#job-search-keyword').val();
        var formData = {
            searchKey: searchKey,
        }
        if (formData.searchKey) {
            searchValue(formData);
        } else {
            p_notification(false, eb.getMessage('ERR_NO_KEY_WORD'));
        }

    });
    $('#job-list-view').on('click', '#j-reset', function(e) {
        e.preventDefault();
        searchValue('');
        $('#job-search-keyword').val('');
    });
    function validation(formData) {
        if (formData.customerName == '') {
            p_notification(false, eb.getMessage('ERR_CUST_NAME_CANT_BE_NULL'));
            $('#customerName').focus();
            return false;
        }
        else if (!$("input[name='cProfile']").is(':checked')) {
            p_notification(false, eb.getMessage('ERR_CUST_PROF_NOT_SELECT'));
            $('#customerName').focus();
            return false;
        }
        else if (formData.jobTypeName == '') {
            p_notification(false, eb.getMessage('ERR_JOB_TYPE_CANT_BE_NULL'));
            $('#jobType').focus();
            return false;
        }
        else if ($('#jobStartingDate').val() && $('#jobEndingDate').val() && $('#jobEndingDate').val() < $('#jobStartingDate').val()) {
            p_notification(false, eb.getMessage('ERR_JOB_ST_DATE_CANT_BE_MORE_THAN_ED_DATE'));
            $('#jobStartingDate').focus();
            return false;
        }
        else if (isNaN(formData.jobEstCost)) {
            p_notification(false, eb.getMessage('ERR_COST_DATA_TYPE'));
            $('#estimatedCost').focus();
            return false;
        }
        else if (parseFloat(formData.jobEstCost) <= 0) {
            p_notification(false, eb.getMessage('ERR_COST_DATA_CANT_BE_NEGATIVE'));
            $('#estimatedCost').focus();
            return false;
        }
        else {
            return true;
        }

    }

    function searchValue(formData) {
        eb.ajax({
            url: BASE_URL + '/job-api/getJobBySearchKey',
            method: 'post',
            data: formData,
            dataType: 'json',
            success: function(data) {
                if (data.status == true) {
                    if (data.status) {
                        $("#job-list-view").html(data.html);
                    } else {
                        p_notification(data.status, data.msg);
                    }
                }
            }
        });
    }
    function getDataForJobWeight(projectID, update) {
        eb.ajax({
            url: BASE_URL + '/job-api/getAllJobWeight',
            method: 'post',
            data: {projectID: projectID},
            dataType: 'json',
            success: function(data) {
                if (!update) {
                    if (data.status == true) {
                        var jWeight = data.data['jobDataList'];
                        if (jWeight != null) {
                            $.each(jWeight, function(index, value) {
                                var $newDiv = "<tr class='job-weight-list' data-jobyid='" + index + "'><td>" + value['jobRef'] + "</td><td>" + value['jobWeight'] + "</td></tr>";
                                $($('#addJobWeight tbody')).append($newDiv);
                            });
                        }
                        $($('#addJobWeight tbody')).append("<tr id='total-weight' class ='total-weight' data-id=" + data.data['jobWeightSum'] + "><td>Total Weight</td><td>" + data.data['jobWeightSum'] + "</td></tr>");
                    }
                } else {
                    if (data.status == true) {
                        var jWeight = data.data['jobDataList'];
                        if (jWeight != null) {
                            var jobweiUpdateAuto = 1;
                            $.each(jWeight, function(index, value) {
                                var $newDiv = "<tr id ='tr-job-weight_" + jobweiUpdateAuto + "' class='job-weight-list' data-jobyid='" + index + "'><td>" + value['jobRef'] + "</td><td><input type='text' class='new-value' id='jweightUp_" + jobweiUpdateAuto + "'></td></tr>";
                                var id = '#jweightUp_' + jobweiUpdateAuto;
                                jobweiUpdateAuto++;
                                $($('#addJobWeight tbody')).append($newDiv);
                                $(id).val(value['jobWeight']);
                            });
                        }
                    }
                }
            }
        });
    }

    function resetSelectPicker(referenceId, string) {
        $('#' + referenceId).val('').empty().selectpicker('refresh');
        $('#' + referenceId).append("<option value=''>" + "Select " + string + "</option>");
        $('#' + referenceId).val('').trigger('change').selectpicker('refresh');
    }

    function setDates(falg) {
        var durationTime = $('#jobEstimetedTime').val();
        var startingTime = $('#jobStartingDate').datetimepicker('getDate');
        var endingTime = $('#jobEndingDate').datetimepicker('getDate');
        if ($('#jobStartingDate').val() && $('#jobEndingDate').val()) {
            if (startingTime.getTime() > endingTime.getTime()) {
                p_notification(false, eb.getMessage('ERR_JOB_ST_DATE_CANT_BE_MORE_THAN_ED_DATE'));
                $('#jobStartingDate').focus();
                return false;
            }
        }

        if ($('#jobStartingDate').val() && $('#jobEndingDate').val() && falg) {
            var timeDiff = Math.abs(endingTime.getTime() - startingTime.getTime());
            var minutes = Math.round(timeDiff / (1000 * 60));
            $('#jobEstimetedTime').val(minutes);
            $('#jobEstimetedTime').trigger('change');
        } else if (durationTime) {
            var less = 0;
            var days = parseInt(durationTime / (60 * 24));
            less = durationTime % (60 * 24);
            var hours = parseInt(less / 60);
            less = less % 60;
            var minutes = less;
            if ($('#jobStartingDate').val()) {
                startingTime.setDate(startingTime.getDate() + days);
                startingTime.setHours(startingTime.getHours() + hours);
                startingTime.setMinutes(startingTime.getMinutes() + minutes);
                $('#jobEndingDate').datetimepicker('setDate', startingTime);
            } else if ($('#jobEndingDate').val()) {
                endingTime.setDate(endingTime.getDate() - days);
                endingTime.setHours(endingTime.getHours() - hours);
                endingTime.setMinutes(endingTime.getMinutes() - minutes);
                $('#jobStartingDate').datetimepicker('setDate', endingTime);
            }
        }
    }


    $('.customer-profiles tbody').on('click', '.cProfile', function() {
        profileID = $("input[name='cProfile']:checked").parents('tr').data('cpid');
        getCustomerProfileDetails(profileID);
    });
    $('.jobEditBack').on('click', function() {
        window.history.back();
    });
//redirect to activity list related to this job
    $(document).on('click', '.activityList', function() {
        var url = BASE_URL + '/activity/list/1/' + this.id;
        window.location.assign(url);
    });
//redirect to activity create page with this job reference
    $(document).on('click', '.createActivity', function() {
        var url = BASE_URL + '/activity/create/' + this.id;
        window.location.assign(url);
    });

    $(document).on('click', "a.editsInactive", function(e) {
        p_notification(false, eb.getMessage('ERR_CUST_INACTIVE'));
    });

//    if Repeat job checked allow to add comment for that
    $('#jobRepeatEnabled').on('change', function() {
        var isChecked = $('#jobRepeatEnabled').is(':checked');
        if (isChecked) {
            $('#jobRepeatComment').show();
        } else {
            $('#jobRepeatComment').val('');
            $('#jobRepeatComment').hide();
        }
    });
    $('#jobRepeatEnabled').trigger('change');

    $('#relatedDocs').on('click', function() {
        var currentJobID = $('#update').val();
        if (projectID && currentJobID == null) {
            setDataTorelatedDocsModal(projectID);
            $('#relatedDocumentModal').modal('show');
        }
        else if (currentJobID) {
            setDataTorelatedDocsModal(projectID, currentJobID);
            $('#relatedDocumentModal').modal('show');
        } else
        {
            p_notification(false, eb.getMessage('ERR_RELETED_DOC'));
            return false;
        }
    });

    $('#related-document-view').on('click', '.document_view', function() {
        var documentId = $(this).parents('tr').attr('data-id');
        var documentType = $(this).parents('tr').attr('data-typeid');
        getPrintPreviewByDocumentTypeAndDocumentID(documentId, documentType);
        $('#documentJobViewModal').modal('show');

    });

    $('#search-job-list').on('click', '.edit', function(e) {
        e.preventDefault();
        var url = BASE_URL + '/job/edit/' + this.id;
        window.location.assign(url);
    });

    $('#search-job-list').on('click', '.link_disabled', function(e) {
        e.preventDefault();
    });

    //for load change status modal
    $('#search-job-list').on('click', '.jobChangeStatus', function() {
        jobId = $(this).closest('tr').data('jobid');
        var jobStatus = $(this).data('status');
        $('#jobStatueChangeModal').modal('show');
        $('#statusValue').val(jobStatus);
    });

    //change project status
    $(document).on('click', '#changeStatusBtn', function() {

        var jobStatus = $('#statusValue').val();
        var obj = {};
        obj.jobId = jobId;
        obj.jobStatus = jobStatus;

        eb.ajax({
            url: BASE_URL + '/job-api/change-job-status',
            method: 'post',
            data: obj,
            dataType: 'json',
            success: function(data) {
                if (data.status == true) {
                    var label = null;
                    var $selectedTr = $('tr[data-jobid="' + jobId + '"]');
                    switch (jobStatus) {
                        case '3':
                            label = '<span class="label label-success">Open</span>';
                            break;
                        case '8':
                            label = '<span class="label label-primary">In Progress</span>';
                            break;
                        case '9':
                            label = '<span class="label label-info">Completed</span>';
                            break;
                        case '4':
                            //disable btns
                            $selectedTr.find('.edit').removeClass('edit').addClass('disabled');
                            $selectedTr.find('.delete-job').removeClass('delete-job').addClass('disabled');
                            $selectedTr.find('.jobChangeStatus').removeClass('jobChangeStatus').addClass('disabled');
                            label = '<span class="label label-danger">Closed</span>';
                            break;
                        default :
                            console.log('invalid option');
                    }
                    $('#jobStatueChangeModal').modal('hide');
                    //change label
                    $selectedTr.find('.jState').html(label);
                    //change data attribute
                    $selectedTr.find('.jobChangeStatus').data('status', jobStatus);
                    window.setTimeout(function() {
                        location.reload();
                    }, 1000);
                }
            }
        });
    });

});

function getCustomerDetails(customerID) {
    setCustomerProfiles(customerID);
    if (!CUSTOMERS_NAMES[customerID]) {
        eb.ajax({
            type: 'POST',
            url: BASE_URL + '/customerAPI/getcustomerByID',
            data: {customerID: customerID},
            success: function(respond) {
                $('#customerName').
                        append($("<option></option>").
                                attr("value", customerID).
                                text(respond.data['customerData']['customerName']));
                $('#customerName').val(customerID);
                $('#customerName').attr('data-cusid', customerID);
                $('#customerName').selectpicker('refresh');
            }
        });
    }


}

function setCustomerProfiles(customerID, callback, profileId) {
    eb.ajax({
        type: 'POST',
        url: BASE_URL + '/customerAPI/getCustomerProfilesByCustomerID',
        data: {customerID: customerID},
        success: function(respond) {
            if (respond.status) {
                $('.customer-profiles tbody tr').remove();

                var profID;
                $.each(respond.data['customerProfiles'], function(index, value) {
                    profID = index;
                    $('.customer-profiles').removeClass('hidden');
                    var $newDiv = "<tr class='customer-profile-list' data-cpid='" + index + "'><td>" + value + "</td><td class='text-center'><input type='radio' name='cProfile' class='cProfile'/></td></tr>";
                    $($('.customer-profiles tbody')).append($newDiv);
                });
                var rows = document.getElementById("cus-prof-table").getElementsByTagName("tbody")[0].getElementsByTagName("tr").length;
                if (rows == '1') {
                    $('.cProfile').attr('checked', true);
                    getCustomerProfileDetails(profID);
                }
            }

            if (callback) {
                callback(profileId);
            }

        }
    });
}

function getCustomerProfileDetails(ProfileID) {
    eb.ajax({
        type: 'POST',
        url: BASE_URL + '/customerAPI/getCustomerProfileDataByCustomerProfileID',
        data: {customerProfileID: ProfileID},
        success: function(respond) {
            if (respond.status) {
                $('#customerAddress').val(respond.data['addressLine']);
            } else {
                $('#customerAddress').val('');
            }
        }
    });
}
function setDataTorelatedDocsModal(projectID, jobID) {
    $('#docDeta tbody tr').remove();

    eb.ajax({
        type: 'POST',
        url: BASE_URL + '/job-api/getAllRelatedDocuments',
        data: {projectID: projectID, jobID: jobID},
        success: function(respond) {
            if (respond.status == true) {
                $.each(respond.data, function(index, value) {
                    if (value != null) {
                        $.each(value, function(index2, value) {
                            if (index == 'job') {
                                var dataBbody = "<tr data-id=" + value['jobID'] + "  data-typeID='job' ><td>" + index + "</td><td >" + value['code'] + "</td><td >" + value['name'] + "</td><td>" + value['createTime'] + "</td><td class='col-lg-3'><span class='glyphicon glyphicon-eye-open openlist link_color document_view '></span></td></tr>";
                                $('#docDeta tbody').append(dataBbody);
                            }
                            else if (index == 'activity') {
                                var dataBbody = "<tr data-id=" + value['activityID'] + " data-typeID='activity' ><td>" + index + "</td><td >" + value['code'] + "</td><td >" + value['name'] + "</td><td>" + value['createTime'] + "</td><td class='col-lg-3'><span class='glyphicon glyphicon-eye-open openlist link_color document_view '></span></td></tr>";
                                $('#docDeta tbody').append(dataBbody);
                            }
                            else if (index == 'inquiryLog') {
                                var dataBbody = "<tr data-id=" + value['inquiryLogID'] + "  data-typeID='inquiryLog' ><td>" + index + "</td><td >" + value['code'] + "</td><td >" + value['name'] + "</td><td>" + value['createTime'] + "</td><td class='col-lg-3'><span class='glyphicon glyphicon-eye-open openlist link_color document_view '></span></td></tr>";
                                $('#docDeta tbody').append(dataBbody);
                            }
                            else {
                                var dataBbody = "<tr><td>" + index + "</td><td >" + value['code'] + "</td><td >" + value['name'] + "</td><td>" + value['createTime'] + "</td><td class='col-lg-3'>-</td></tr>";
                                $('#docDeta tbody').append(dataBbody);
                            }
                        });
                    } else {
                        var dataBbody = "<tr><td></td><td ></td><td >No Related Details</td></tr>";
                        $('#docDeta tbody').append(dataBbody);
                    }
                });
            }
        }
    });
}

function getPrintPreviewByDocumentTypeAndDocumentID(documentID, documentType) {
    $('#job-document-view div').remove();
    if (documentType == 'job') {
        URL = BASE_URL + '/job/document/' + documentID;
    }
    else if (documentType == 'activity') {
        URL = BASE_URL + '/activity/view/' + documentID;
    }
    else if (documentType == 'inquiryLog') {
        URL = BASE_URL + '/inquiry-log/view/' + documentID;
    }

    eb.ajax({
        type: 'POST',
        url: URL,
        success: function(respond) {
            var division = "<div></div>";
            $('#job-document-view').append(division);
            if (documentType == 'job') {
                $('#job-document-view div').append(respond);
            } else {
                $('#job-document-view div').append(respond.html);

            }
        }
    });

}

