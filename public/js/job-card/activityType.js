$(function() {
    var isModalOnActivityType = false;
    if ($("#addActivityTypeModal").length) {
        isModalOnActivityType = true;
    } else {
        isModalOnActivityType = false;
    }

    var activityTypeID;
    var activityTypeFlag = false;
    $('#activityType-list').on('click', 'a.edit', function(e) {
        e.preventDefault();
        $('.add').addClass('hidden');
        $('.update').removeClass('hidden');
        activityTypeID = $(this).parents('tr').data('activitytypeid');
        $('#activityTypeName').val($(this).parents('tr').find('.actTypName').text());
        $('#maxValue').val($(this).parents('tr').find('.actTypMx').text());
        $('#minValue').val($(this).parents('tr').find('.actTypMn').text());
        activityTypeFlag = true;
    });

    $('#create-activityType-form').on('submit', function(e) {
        e.preventDefault();
        if (activityTypeFlag) {
            update();
        } else {
            add();
        }
    });

    $('#activityType-list').on('click', 'a.status', function() {
        var msg;
        var status;
        var flag = true;
        var activityTypeID = $(this).parents('tr').data('activitytypeid');
        var currentDiv = $(this).contents();
        if ($(this).children().hasClass('fa-check-square-o')) {
            msg = 'Are you sure you want to Deactivate this activiity Type';
            status = '0';
        }
        else if ($(this).children().hasClass('fa-square-o')) {
            msg = 'Are you sure you want to Activate this activity type';
            status = '1';
        }
        activeInactiveType(activityTypeID, status, currentDiv, msg, flag);
    });


    $('#activityType-list').on('click', 'a.delete', function(e) {
        e.preventDefault();
        activityTypeID = $(this).parents('tr').data('activitytypeid');
        var currentDiv = $(this).parents('tr').find('a.status').contents();
        var flag = false;
        var formData = {
            activityTypeID: activityTypeID,
        };
        var msg = 'This activity type cannot be deleted. Do you want to de-activate this activity type';
        bootbox.confirm('Are you sure you want to delete this Activity Type?', function(result) {
            if (result === true) {
                eb.ajax({
                    url: BASE_URL + '/activity_type_api/delete',
                    method: 'post',
                    data: formData,
                    dataType: 'json',
                    success: function(data) {
                        if (data.status == true) {
                            p_notification(data.status, data.msg);
                            window.location.reload();
                        }
                        else if (data.status == false && data.data == 'deactive') {
                            activeInactiveType(activityTypeID, '0', currentDiv, msg, flag);
                        } else {
                            p_notification(data.status, data.msg);

                        }
                    }
                });
            }
        });
    });
    $('#activity-type-search').on('click', '#act-search', function(e) {
        e.preventDefault();
        searchKey = $('#activityType-search-keyword').val();
        var formData = {
            searchKey: searchKey,
        };
        if (formData.searchKey) {
            searchValue(formData);
        } else {
            p_notification(false, eb.getMessage('ERR_NO_KEY_WORD'));
        }

    });

    $('#activity-type-search').on('click', '.reset', function(e) {
        e.preventDefault();
        searchValue('');
        $('#activityType-search-keyword').val('');
    });


    function validateFormData(formData) {

        if (formData.activityTypeName == '') {
            p_notification(false, eb.getMessage('ERR_ACT_TYPE_NAME_CNT_BE_NULL'));
            $('#activityTypeName').focus();
            return false;
        }

//        if (formData.maxValue == '') {
//            p_notification(false, eb.getMessage('ERR_ACT_TYPE_MX_VALUE_CNT_BE_NULL'));
//            $('#maxValue').focus();
//            return false;
//        }
//        if (formData.minValue == '') {
//            p_notification(false, eb.getMessage('ERR_ACT_TYPE_MN_VALUE_CNT_BE_NULL'));
//            $('#minValue').focus();
//            return false;
//        }
        if (isNaN(formData.maxValue)) {
            p_notification(false, eb.getMessage('ERR_MAX_VALUE_TYPE'));
            $('#maxValue').focus();
            return false;
        }

        if (isNaN(formData.minValue)) {
            p_notification(false, eb.getMessage('ERR_MIN_VALUE_TYPE'));
            $('#minValue').focus();
            return false;
        }

        if (formData.maxValue && formData.minValue) {
            if (parseInt(formData.minValue) > parseInt(formData.maxValue)) {
                p_notification(false, eb.getMessage('ERR_MIN_MAX_VALUE_LEVEL'));
                $('#minValue').focus();
                return false;
            }
        }
        if (formData.maxValue) {
            if (parseInt(formData.maxValue) <= 0) {
                p_notification(false, eb.getMessage('ERR_MAX_VALUE_MINUS'));
                $('#maxValue').focus();
                return false;
            }
        }
        if (formData.minValue) {
            if (parseInt(formData.minValue) <= 0) {
                p_notification(false, eb.getMessage('ERR_MIN_VALUE_MINUS'));
                $('#minValue').focus();
                return false;
            }
        }
        if (formData.maxValue == '' && formData.minValue != '') {
            p_notification(false, eb.getMessage('ERR_MAX_ZERO_VALUE_LEVEL'));
            $('#maxValue').focus();
            return false;
        }


        return true;
    }
    function searchValue(formData) {
        eb.ajax({
            url: BASE_URL + '/activity_type_api/getActivityTypeBySearchKey',
            method: 'post',
            data: formData,
            dataType: 'json',
            success: function(data) {
                if (data.status == true) {
                    if (data.status) {
                        $("#activityType-list").html(data.html);
                    } else {
                        p_notification(data.status, data.msg);
                    }
                }
            }
        });
    }
    function update() {
        var formData = {
            activityTypeName: $('#activityTypeName').val(),
            maxValue: $('#maxValue').val(),
            minValue: $('#minValue').val(),
            activityTypeID: activityTypeID,
        };
        if (validateFormData(formData)) {
            eb.ajax({
                url: BASE_URL + '/activity_type_api/update',
                method: 'post',
                data: formData,
                dataType: 'json',
                success: function(data) {
                    p_notification(data.status, data.msg);
                    if (data.status) {
                        window.setTimeout(function() {
                            location.reload();
                        }, 1000);
                    } else {
                        if (data.data == "ACTIVITY_TYPE_EXIST") {
                            $('#activityTypeName').focus();
                        }
                    }
                }
            });
        }
    }
    function add() {
        var formData = {
            activityTypeName: $('#activityTypeName').val(),
            activityTypeID: $('#activityTypeID').val(),
            maxValue: $('#maxValue').val(),
            minValue: $('#minValue').val(),
        };
        if (validateFormData(formData)) {
            eb.ajax({
                url: BASE_URL + '/activity_type_api/add',
                method: 'post',
                data: formData,
                dataType: 'json',
                success: function(data) {
                    p_notification(data.status, data.msg);
                    if (data.status && isModalOnActivityType == false) {
                        window.setTimeout(function() {
                            location.reload();
                        }, 1000);
                    } else if (isModalOnActivityType) {
                        $('#activityTypeName').val('');
                        $('#maxValue').val('');
                        $('#minValue').val('');
                        $("#addActivityTypeModal").modal('hide');
                        $('#activityTypeID').attr('data-frmactmdal', data.data);
                        $('#activityTypeID').empty();
                        $('#activityTypeID').
                                append($("<option></option>").
                                        attr("value", data.data).
                                        text(formData.activityTypeName));
                        $('#activityTypeID').selectpicker('refresh');
                    } else {
                        if (data.data == "ACTIVITY_TYPE_EXIST") {
                            $('#activityTypeName').focus();
                        }
                    }
                }
            });
        }
    }
    /**
     *
     * use to change active state
     */
    function activeInactiveType(type, status, currentDiv, msg, flag) {
        bootbox.confirm(msg, function($result) {
            if ($result == true) {
                eb.ajax({
                    url: BASE_URL + '/activity_type_api/changeStatusID',
                    method: 'post',
                    dataType: 'json',
                    data: {
                        'activityTypeID': type,
                        'status': status,
                    },
                    success: function(data) {
                        if (data.status) {
                            p_notification(data.status, data.msg);
                            if (currentDiv.hasClass('fa-square-o') && flag) {
                                currentDiv.addClass('fa-check-square-o');
                                currentDiv.removeClass('fa-square-o');
                            }
                            else {
                                currentDiv.addClass('fa-square-o');
                                currentDiv.removeClass('fa-check-square-o');
                            }
                        } else {
                            p_notification(data.status, data.msg);
                        }
                    }
                });
            }
        });
    }

});


