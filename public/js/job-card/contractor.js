/**
 * @author Sandun<sandun@thinkcube.com>
 * This file contains Contractor related js functiond
 */
var contractorID = null;
var designationID = null;
var divisionID = null;
var editMode = false;
$(document).ready(function() {
    $('#designation').on('change', function() {
        designationID = $(this).val();
    });
    $('#division').on('change', function() {
        divisionID = $(this).val();
    });
    $('#contractorSave,#contractorUpdate').on('click', function(e) {
        e.preventDefault();
        var inputData = {
            contractorFirstName: $('#contractorFirstName').val(),
            contractorSecondName: $('#contractorSecondName').val(),
            telNumber: $('#telNumber').val(),
            designationID: $('#designationID').val(),
            divisionID: $('#divisionID').val(),
            editMode: editMode,
            contractorID: contractorID
        };
        if (validateContractorForm(inputData)) {
            eb.ajax({
                type: 'POST',
                url: BASE_URL + '/api/contractor/saveContractor',
                data: inputData,
                success: function(respond) {
                    p_notification(respond.status, respond.msg);
                    if (respond.status == true) {
                        $('#contractor')[0].reset();
                        if ($('#addContractorsModal').length) {
                            setAddedContractorToList(respond.data);
                            $('#addContractorsModal').modal('hide');
                        } else {
                            window.setTimeout(function() {
                                location.reload();
                            }, 1000);
                        }
                    }
                },
                async: false
            });
        }
    });

    $('#contractorList').on('click', 'a.edit', function(e) {
        e.preventDefault();
        contractorID = $(this).parents('tr').data('contractorid');
        $('#contractorFirstName').val($(this).parents('tr').find('.fName').text()).attr('disabled', true);
        $('#contractorSecondName').val($(this).parents('tr').find('.sName').text()).attr('disabled', true);
        $('#designationID').val($(this).parents('tr').find('.desName').data('desid'));
        $('#designationID').selectpicker('render');
        $('#divisionID').val($(this).parents('tr').find('.divName').data('diviid'));
        $('#divisionID').selectpicker('render');
        $('#telNumber').val($(this).parents('tr').find('.contractp').text());
        $('.add-contractor-h5').addClass('hidden');
        $('.update-contractor-h5').removeClass('hidden');
        $('.add-button-set').addClass('hidden');
        $('.update-button-set').removeClass('hidden');
        editMode = true;
    });

    $('#contractor').on('click', '.reset, .cancel', function(e) {
        e.preventDefault();
        contractorID = null;
        $('#contractorFirstName').val('').attr('disabled', false);
        $('#contractorSecondName').val('').attr('disabled', false);
        $('#designationID').val('');
        $('#designationID').selectpicker('refresh');
        $('#divisionID').val('');
        $('#divisionID').selectpicker('refresh');
        $('#telNumber').val('');
        $('.add-contractor-h5').removeClass('hidden');
        $('.update-contractor-h5').addClass('hidden');
        $('.add-button-set').removeClass('hidden');
        $('.update-button-set').addClass('hidden');
        editMode = false;
    });


    $('#contractorList').on('click', 'a.status', function() {
        var msg;
        var status;
        var contractorID = $(this).parents('tr').data('contractorid');
        var currentDiv = $(this).contents();
        var contractorFlag = true;
        if ($(this).children().hasClass('fa-check-square-o')) {
            msg = 'Are you sure you want to Deactivate this Contractor';
            status = '0';
        }
        else if ($(this).children().hasClass('fa-square-o')) {
            msg = 'Are you sure you want to Activate this Contractor';
            status = '1';
        }
        activeInactiveType(contractorID, status, currentDiv, msg, contractorFlag);
//        bootbox.confirm(msg, function ($result) {
//            if ($result == true) {
//                eb.ajax({
//                    url: BASE_URL + '/api/contractor/changeStatusID',
//                    method: 'post',
//                    dataType: 'json',
//                    data: {
//                        'contractorID': contractorID,
//                        'status': status,
//                    },
//                    success: function (data) {
//                        if (data.status) {
//                            p_notification(data.status, data.msg);
//                            if (currentDiv.hasClass('fa-check-square-o')) {
//                                currentDiv.addClass('fa-square-o');
//                                currentDiv.removeClass('fa-check-square-o');
//                            }
//                            else {
//                                currentDiv.addClass('fa-check-square-o');
//                                currentDiv.removeClass('fa-square-o');
//                            }
//                        } else {
//                            p_notification(data.status, data.msg);
//                        }
//                    }
//                });
//            }
//        });
    });


    $('#contractorList').on('click', 'a.delete', function(e) {
        e.preventDefault();
        var contractorID = $(this).parents('tr').data('contractorid');
        var currentDiv = $(this).parents('tr').find('a.status').contents();
        var contractorFlag = false;
        var msg = eb.getMessage('CONTRACTOR_DELETE_AND_STATE');
        var formData = {
            contractorID: contractorID,
        };
        bootbox.confirm('Are you sure you want to delete this Contractor?', function(result) {
            if (result === true) {
                eb.ajax({
                    url: BASE_URL + '/api/contractor/deleteContractor',
                    method: 'post',
                    data: formData,
                    dataType: 'json',
                    success: function(data) {
                        if (data.status == true) {
                            p_notification(data.status, data.msg);
                            window.location.reload();
                        } else if (data.status == false && data.data == 'deactive') {
                            activeInactiveType(contractorID, '0', currentDiv, msg, contractorFlag);
                        } else {
                            p_notification(data.status, data.msg);

                        }
                    }
                });
            }
        });
    });

    $('#contractorSearch').on('click', function(e) {
        e.preventDefault();
        var formData = {
            contractorSearchKey: $('#contractorSearchKeyword').val(),
        };
        if (formData.contractorSearchKey != '') {
            searchValue(formData);
        } else {
            p_notification(false, eb.getMessage('ERR_CONTRACT_SEARCH_KEY'));
        }
    });

    $('#contractorSearchForm').on('click', '.reset', function(e) {
        e.preventDefault();
        searchValue('');
        $('#contractorSearchKeyword').val('');
    });

    $('#temporaryContractorSave').on('click', function(e) {
        e.preventDefault();
        var formInputs = {
            temporaryContractorFirstName: $('#temporaryContractorFirstName', '#temporaryContractor').val(),
            temporaryContractorSecondName: $('#temporaryContractorSecondName', '#temporaryContractor').val(),
            designationID: $('#designationID', '#temporaryContractor').val(),
            divisionID: $('#divisionID', '#temporaryContractor').val(),
            temporaryContractorTP: $('#temporaryContractorTP', '#temporaryContractor').val(),
        };
        if (validateTempContractorForm(formInputs)) {
            var tempContractorFormParam = formInputs;
            tempContractorFormParam.isModalRequest = true;
            eb.ajax({
                type: 'POST',
                url: BASE_URL + '/api/temporary-contractor/save-temporary-contractor',
                data: tempContractorFormParam,
                success: function(respond) {
                    p_notification(respond.status, respond.msg);
                    if (respond.status == true) {
                        $('#temporaryContractor')[0].reset();
                        if ($('#addTempContractorsModal').length) {
                            setAddedTempContractorToList(respond.data);
                            $('#addTempContractorsModal').modal('hide');
                        }
                    }
                },
                async: false
            });
        }
    }
    );

    function searchValue(formData) {
        eb.ajax({
            url: BASE_URL + '/api/contractor/getContractorBySearchKey',
            method: 'post',
            data: formData,
            dataType: 'json',
            success: function(data) {
                if (data.status) {
                    $("#contractorList").html(data.html);
                } else {
                    p_notification(data.status, data.msg);
                }
            }
        });
    }
    function activeInactiveType(type, status, currentDiv, msg, contractorFlag) {
        bootbox.confirm(msg, function($result) {
            if ($result == true) {
                eb.ajax({
                    url: BASE_URL + '/api/contractor/changeStatusID',
                    method: 'post',
                    dataType: 'json',
                    data: {
                        'contractorID': type,
                        'status': status,
                    },
                    success: function(data) {
                        if (data.status) {
                            p_notification(data.status, data.msg);
                            if (currentDiv.hasClass('fa-square-o') && contractorFlag) {
                                currentDiv.addClass('fa-check-square-o');
                                currentDiv.removeClass('fa-square-o');
                            }
                            else {
                                currentDiv.addClass('fa-square-o');
                                currentDiv.removeClass('fa-check-square-o');
                            }
                        } else {
                            p_notification(data.status, data.msg);
                        }
                    }
                });
            }
        });
    }
});
function validateContractorForm(inputs) {
    var fName = inputs.contractorFirstName;
    var sName = inputs.contractorSecondName;
    var telNumber = inputs.telNumber;
    if (fName == null || fName == "") {
        p_notification(false, eb.getMessage('ERR_CONTRAC_FNAME_BLANK'));
        $('#contractorFirstName').focus();
    } else if (sName == null || sName == "") {
        p_notification(false, eb.getMessage('ERR_CONTRAC_SNAME_BLANK'));
        $('#contractorSecondName').focus();
    } else if (isNaN(telNumber)) {
        p_notification(false, eb.getMessage('ERR_WIZARD_TELENUM'));
    } else if (telNumber != "" && (telNumber.length < 10 || telNumber.length > 13)) {
        p_notification(false, eb.getMessage('ERR_WIZARD_PHONENO_LIMIT'));
    } else {
        return true;
    }
}

function validateTempContractorForm(inputs) {
    var fName = inputs.temporaryContractorFirstName;
    var sName = inputs.temporaryContractorSecondName;
    var telNumber = inputs.temporaryContractorTP;
    var isPhone = false;
    if (telNumber.length != 0) {
        if (telNumber.length < 9 || telNumber.length > 15) {
            isPhone = true;
        }
    }
    if (fName == null || fName == "") {
        p_notification(false, eb.getMessage('ERR_TEMP_CONTRAC_FNAME_BLANK'));
        $('#temporaryContractorFirstName').focus();
    } else if (sName == null || sName == "") {
        p_notification(false, eb.getMessage('ERR_TEMP_CONTRAC_SNAME_BLANK'));
        $('#temporaryContractorSecondName').focus();
    } else if (isNaN(telNumber) || isPhone) {
        p_notification(false, eb.getMessage('ERR_CUST_TPNUM_VALIDITY'));
        $('#telNumber').focus();
    } else {
        return true;
    }
}

