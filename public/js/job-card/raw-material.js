/**
 * @author Sandun <sandun@thinkcube.com>
 * this file has the function of adding raw material
 */
var selectedProductID;
var locationProducts = {};
var adjustmentProductQty;
var uomQty;
var selectedProductUom;
var activeProductData;
$(function() {
    var proId;
    var items = {};
    var locationID = $('#locationID').val();

    loadDropDownFromDatabase('/api/activity/searchRawMaterialProductForDropdown', "", 1, '#itemCode');

    $('#itemCode').on('change', function(e) {
        var $thisRow = $(this).parents('tr');

        if ((typeof items[$(this).val()] != 'undefined' && $(this).val() != '') && items[$(this).val()].p_id != '') {

            p_notification(false, eb.getMessage("ERR_QUOT_PR_ALREADY_ADD"));

            $('.uomLi').remove();
            $('#uomAb').html('');
            $('#itemCode').val('');
            clearItemCode();
            $('#qty').val('');
            $('#unitPrice').val('');
            selectedProductUom = '';
            $('#addNewItemRow .uomqty').val('');
            $('#addNewItemRow .uomPrice').val('');
            $('#addNewItemRow .uom-select').remove();
            $('#addNewItemRow .uom-price-select').remove();
            $("#addNewItemRow .available-qty .uomqty").remove();
            $("#addNewItemRow .unit-price .uomPrice").remove();
            $("#addNewItemRow .available-qty #availableQuantity").css('display', 'none');
            $("#addNewItemRow .available-qty #availableQuantity").val('');
            $("#addNewItemRow .available-qty #availableQuantity").show();
            $("#addNewItemRow .available-qty #availableQuantity").removeAttr("readonly", "readonly");
        } else if ($(this).val() > 0 && selectedProductID != $('#itemCode').val()) {

            $('.uomLi').remove();
            selectedProductID = $(this).val();
            eb.ajax({
                type: 'POST',
                url: BASE_URL + '/productAPI/get-location-product-details',
                data: {productID: selectedProductID, locationID: locationID},
                success: function(respond) {
                    var currentElem = new Array();
                    currentElem[selectedProductID] = respond.data;
                    locationProducts = $.extend(locationProducts, currentElem);

                    var data = new Array();
                    var dataPostiveAdded = new Array();
                    $("#qty").val('').siblings('.uomqty,.uom-select').remove();
                    $("#qty").parent().addClass('input-group');
                    $("#qty").show();

                    $thisRow.find("#unitPrice").val('').siblings('.uomPrice,.uom-price-select').remove();
                    $thisRow.find("#unitPrice").parent().addClass('input-group');
                    $thisRow.find("#unitPrice").show();

                    $thisRow.find("input[name='availableQuantity']").val('').siblings('.uomqty,.uom-select').remove();
                    $thisRow.find("input[name='availableQuantity']").parent().addClass('input-group');
                    $thisRow.find("input[name='availableQuantity']").show();

                    $thisRow.find("input[name='unitPrice']").val('').siblings('.uomPrice,.uom-price-select').remove();
                    $thisRow.find("input[name='unitPrice']").parent().addClass('input-group');
                    $thisRow.find("input[name='unitPrice']").show();

                    data[0] = locationProducts[selectedProductID];
                    $('#qty').addUom(data[0].uom);
                    $("#addNewItemRow input[name='availableQuantity']").val(data[0].LPQ).prop('readonly', true).addUom(data[0].uom);
                    $('.uomqty').attr("id", "itemQuantity");
                    $('#qty').parent().addClass('input-group');
                    $("input[name='availableQuantity']").parent().addClass('input-group');
                    if (data[0] === 'error' && dataPostiveAdded[0] === 'error') {
                        $('.uomqty').val('');
                        $('#unitPrice').val('');
                    }
                    else {
                        proId = data[0].pID;
                        locationProductID = data[0].lPID;
                        $('#itemCode').data('PT', data[0].pT);
                        $('#itemCode').data('PN', data[0].pN);
                        if (data[0].grnProductPrice != null) {
                            $thisRow.find("input[name='unitPrice']").val(accounting.formatMoney(data[0].grnProductPrice)).addUomPrice(data[0].uom);
                        } else {
                            $thisRow.find("input[name='unitPrice']").val(accounting.formatMoney(data[0].unitOfPrice)).addUomPrice(data[0].uom);
                        }
                        $('#unitPrice').parent().addClass('input-group');
                        $("input[name='unitPrice']").parent().addClass('input-group');
                        if ($('#unitPrice').val() != '') {
                            if ($('.uomqty').val() === '') {
                                $('.uomqty').val('0');
                                $('.uomqty').focus();
                            }
                        }
                        setItemCost();
                    }
                    $('.uomqty').focus();
                }
            });
        }
    });

    $('#addRawMaterialModal').on('focusout', '#itemQuantity,.uomPrice', function() {
        var uPrice = accounting.unformat($('#addNewItemRow #unitPrice').val());
        var proQty = $('#qty').val();
        var qty = proQty;
        var itemTotal = setItemRowCost(qty, uPrice);
        $(this).parents('tr').find('#total').html(accounting.formatMoney(itemTotal));
    });

    $('#addItem').on('click', function() {
        var $thisRow = $(this).parents('tr');
        $("#qty").siblings('.uomqty').change();
        $("#unitPrice").siblings('.uomPrice').change();
        $('.tempy').remove();

        var expD = $('#expire_date_autoFill').val('').on('changeDate', function(ev) {
            expD.hide();
        }).data('datepicker');
        $('#quontity').val('');
        if ($('#itemCode').val() == '') {
            p_notification(false, eb.getMessage('ERR_INVENTADJUS_PRODCODE'));
        } else {
            adjustmentProductQty = $('#qty').val();
            uomQty = $(".adding-qty .uomqty", $thisRow).val();
            var productType = $('#itemCode').data('PT');
            selectedProductUom = $("#qty").siblings('.uom-select').children('button').find('.selected').data('uomID');
            if (typeof (selectedProductUom) == 'undefined' || selectedProductUom == '') {
                p_notification(false, eb.getMessage('ERR_GRN_SELECT_UOM'));
            } else {
                if (productType != 2 && ($('#qty').val() == '' || $('#qty').val() == 0)) {
                    p_notification(false, eb.getMessage('ERR_GRN_ENTER_QUANTITY'));
                    $('.uomqty').focus();
                } else if (accounting.unformat($('#addNewItemRow .uomPrice').val()) == '' || parseFloat(accounting.unformat($('#addNewItemRow .uomPrice').val())) <= 0) {
                    p_notification(false, eb.getMessage('ERR_INVENTADJUS_UNIPRI'));
                    $('.uomPrice').focus().select();
                } else {
                    positiveAdjustment(productType);
                }

            }
        }
    });

    $(document).on('click', '.delete', function() {
        var deletePTrID = $(this).closest('tr').attr('id');
        var deletePID = deletePTrID.split('tr_')[1].trim();

        bootbox.confirm('Are you sure you want to remove this product ?', function(result) {
            if (result == true) {
                delete items[deletePID];
                var rowTotal = accounting.unformat($('#' + deletePTrID).find('#ttl').text());
                $('#' + deletePTrID).remove();
                var netTotal = accounting.unformat($('#finaltotal').html());
                var newTotal = parseFloat(netTotal) - parseFloat(rowTotal);
                $('#finaltotal').html(accounting.formatMoney(parseFloat(newTotal).toFixed(2)));
            }
        });
    });

    $("#saveInventoryAdjustment").on('click', function(e) {
        e.preventDefault();
        var goods_issue_tbl = {
            goods_issue_date: "",
            goods_issue_reason: "Adding Quantity By Job Card - Raw Materials",
            goods_issue_type_id: "2",
            goods_issue_id: "",
            locRefID: "",
            goods_issue_comment: $('#cmnt').val(),
            type: 'Activity'
        };

        eb.ajax({
            type: 'POST',
            url: BASE_URL + '/api/inventory-adjustment/update-positive-adjustment',
            data: {items: items, goods_issue_tbl_data: goods_issue_tbl, locationID: locationID},
            success: function(respond) {
                if (respond.status == true) {
                    $("#addRawMaterialModal").modal('hide');
                    clearProductScreenForAdjustment();
                    //get active product list
                    eb.ajax({
                        type: 'POST',
                        url: BASE_URL + '/productAPI/getActiveProductsFromLocation',
                        data: {locationID: locationID},
                        success: function(respond) {
                            activeProductData = respond.data;
                        }
                    });
                    loadDropDownFromDatabase('/api/activity/searchRawMaterialProductForDropdown', "", 0, '#rawMaterialID');
                    p_notification(respond.status, respond.msg);
                }
            }
        });

    });

    function positiveAdjustment(productType) {
        if (items[proId]) {
            p_notification(false, eb.getMessage('ERR_INVENTADJUS_ITEMINSERT'));
            $('#itemCode').val('');
            $('#itemCode').selectpicker('render');
            $("#unitPrice").val('').siblings('.uomPrice,.uom-price-select').remove();
            $("#qty").val('').siblings('.uomqty,.uom-select').remove();
            $("#qty").show();
            $('#uomAb').html('');
            $('#total').html('');
        } else {
            $('.cloneSerials').remove();

            if (productType == 2) {
                locationProducts[proId].bP = 0;
                locationProducts[proId].sP = 0;
            }

            if (locationProducts[proId].bP == 0 && locationProducts[proId].sP == 0) {
                addNewProductRow({}, {});

            }
        }
    }

    function addNewProductRow() {
        var uomConversionRate = $("#qty").siblings('.uom-select').find('.selected').data('uc');
        var proCodeAndName = $('#itemCode option:selected').html();
        var iC = $('#itemCode').val();
        var productType = $('#itemCode').data('PT');
        var iN = $('#itemCode').data('PN');
        var uPrice = accounting.unformat($('#addNewItemRow #unitPrice').val());
        var selectedUomAbbr = $("#qty").siblings('.uom-select').children('button').find('.selected').data('uomID');
        adjustmentProductQty = $('#qty').val();
        var qty = adjustmentProductQty;
        if (productType == 2 && adjustmentProductQty == 0) {
            qty = 1;
        }
        var locationProductID = locationProducts[proId].lPID;
        var rowCostTotal = setItemRowCost(qty, uPrice);
        var newTrID = 'tr_' + proId;
        var clonedRow = $($('#preSetSample').clone()).attr('id', newTrID).addClass('addedProducts');
        $("input[name='productCode']", clonedRow).val(proCodeAndName);
        $("input[name='qty']", clonedRow).val(adjustmentProductQty).addUom(locationProducts[proId].uom);
        $("input[name='unitPrice']", clonedRow).val(uPrice).addUomPrice(locationProducts[proId].uom);
        $(".uomPrice", clonedRow).prop('disabled', true);
        $(".uomqty", clonedRow).prop('disabled', true);
        clonedRow.children('#ttl').html(accounting.formatMoney(rowCostTotal));
        $('#addNewUom', clonedRow).hide();
        clonedRow.removeClass('hidden');
        clonedRow.insertBefore('#preSetSample');
        $('#finaltotal').html(accounting.formatMoney(setItemTotalCost(adjustmentProductQty, uPrice)));
        items[proId] = new positiveAdjustmentProduct(locationProductID, proId, iC, iN, uomQty, uPrice, selectedUomAbbr, selectedProductUom, rowCostTotal, productType, uomConversionRate);
        clearAddedNewRow();
        $('[data-id=itemCode]').trigger('click');
    }

    function positiveAdjustmentProduct(lPID, pID, pCode, pN, pQ, pUP, pUom, uomID, pTotal, productType, uomConversionRate) {
        this.locationPID = lPID;
        this.pID = pID;
        this.pCode = pCode;
        this.pName = pN;
        this.pQuantity = pQ;
        this.pUnitPrice = pUP;
        this.pUom = pUom;
        this.uomID = uomID;
        this.pTotal = pTotal;
        this.productType = productType;
        this.uomConversionRate = uomConversionRate;
    }

    function setItemCost() {
        var unitPrice = accounting.unformat($('#unitPrice').val());
        var qty = $('#qty').val();
        var tot = unitPrice * qty;
        $('#total').html(accounting.formatMoney(parseFloat(tot).toFixed(2)));
    }

    function setItemRowCost(qty, uPrice) {
        var tot = uPrice * qty;
        return parseFloat(tot).toFixed(2);
    }

    function clearAddedNewRow() {
        $('.uomLi').remove();
        $('#uomAb').html('');
        $('#itemCode').val('');
        $('#total').html('0.00');
        clearItemCode();
        $('#qty').val('');
        $('#unitPrice').val('');
        selectedProductUom = '';
        $('#addNewItemRow .uomqty').val('');
        $('#addNewItemRow .uom-select').remove();
        $('#addNewItemRow .uomPrice').val('');
        $('#addNewItemRow .uom-price-select').remove();
    }

    function clearItemCode() {
        $('#itemCode')
                .empty()
                .val('')
                .append($("<option></option>")
                        .attr("value", "")
                        .text('Select Item Code or Name'))
                .selectpicker('refresh')
                .trigger('change');
    }

    function setItemTotalCost(qty, uPrice) {
        var ft = accounting.unformat($('#finaltotal').html());
        var rowTotal = parseFloat(qty) * parseFloat(uPrice);
        var netTotal = parseFloat(ft) + parseFloat(rowTotal);
        return parseFloat(netTotal).toFixed(2);
    }

    function clearProductScreenForAdjustment() {
        $('#addRawMaterialModal .addedProducts').remove();
        selectedProductID = '';
        $('#itemCode, #unitPrice, #qty').val('');
        clearItemCode();
        $("#addRawMaterialModal .uomqty").val('');
        $("#addRawMaterialModal .uomPrice").val('');
        $("#addRawMaterialModal .unit-price .uom-price-select").remove();
        $("#addRawMaterialModal .unit-price .uomPrice").prop('disabled', false);
        $("#addRawMaterialModal .available-qty .uom-select").remove();
        $("#addRawMaterialModal .available-qty .uomqty").prop('disabled', false);
        $('#addRawMaterialModal .uom-select').remove();
        $('#addRawMaterialModal .uom-price-select').remove();
        $('#total').html('0.00');
        $('#finaltotal').html('0.00');
        $('#addRawMaterialModal .uomLi').remove();
        $('#addRawMaterialModal #uomAb').html('');
        items = {};
    }
});


