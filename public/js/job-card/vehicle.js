$(document).ready(function() {

    var vehicleId = null;
    var vehicleMake = null;
    var vehicleModel = null;
    var vehicleRegNum = null;
    var vehicleCost = null;
    var entityId = null;

    $(document).on('click', '.vehicle-action', function(e) {
        e.preventDefault();
        var action = $(this).data('action');

        vehicleId = $(this).closest('tr').data('vehicle-id');
        vehicleMake = $(this).closest('tr').data('vehicle-make');
        vehicleModel = $(this).closest('tr').data('vehicle-model');
        vehicleRegNum = $(this).closest('tr').data('vehicle-reg-num');
        vehicleCost = $(this).closest('tr').data('vehicle-cost');
        entityId = $(this).closest('tr').data('entity-id');

        switch (action) {
            case 'edit':
                $('#vehicleMake').val(vehicleMake);
                $('#vehicleModel').val(vehicleModel);
                $('#vehicleRegistrationNumber').val(vehicleRegNum);
                $('#vehicleCost').val(vehicleCost);

                $('.updateDiv').removeClass('hidden');
                $('.addDiv').addClass('hidden');
                break;
            case 'delete':
                $('.vehicleDeleteModalBody').html('<p>Are you sure you want to delete <b>"' + vehicleRegNum + '"</b>  vehicle ?</p>');
                $('#vehicleDeleteModal').show();
                break;
            default :
                console.log('invalid option');
        }
    });

    //for save btn
    $('#saveBtn').on('click', function() {
        var vehicleMake = $('#vehicleMake').val();
        var vehicleModel = $('#vehicleModel').val();
        var vehicleRegNumber = $('#vehicleRegistrationNumber').val();
        var vehicleCost = $('#vehicleCost').val();

        var data = {
            vehicleMake: vehicleMake,
            vehicleModel: vehicleModel,
            vehicleRegistrationNumber: vehicleRegNumber,
            vehicleCost: vehicleCost
        }

        if (validateInput(data)) {
            eb.ajax({
                type: 'POST',
                url: BASE_URL + '/vehicle-api/create',
                data: data,
                success: function(respond) {
                    p_notification(respond.status, respond.msg);
                    if (respond.status) {
                        getVehicles('', 1);
                        $('#vehicleMake,#vehicleModel,#vehicleRegistrationNumber,#vehicleCost').val('');
                    }
                }
            });
        }
    });

    //for update btn
    $('#updateBtn').on('click', function() {
        var vehicleMake = $('#vehicleMake').val();
        var vehicleModel = $('#vehicleModel').val();
        var vehicleRegNumber = $('#vehicleRegistrationNumber').val();
        var vehicleCost = $('#vehicleCost').val();

        var data = {
            vehicleId: vehicleId,
            vehicleMake: vehicleMake,
            vehicleModel: vehicleModel,
            vehicleRegistrationNumber: vehicleRegNumber,
            vehicleCost: vehicleCost,
            entityId: entityId
        }

        if (validateInput(data)) {
            eb.ajax({
                type: 'POST',
                url: BASE_URL + '/vehicle-api/update',
                data: data,
                success: function(respond) {
                    p_notification(respond.status, respond.msg);
                    if (respond.status) {
                        $('.updateDiv').addClass('hidden');
                        $('.addDiv').removeClass('hidden');
                        getVehicles('', 1);
                        $('#vehicleMake,#vehicleModel,#vehicleRegistrationNumber,#vehicleCost').val('');
                    }
                }
            });
        }
    });

    //for delete btn
    $(document).on('click', '#btnDelete', function(e) {
        eb.ajax({
            type: 'POST',
            url: BASE_URL + '/vehicle-api/delete',
            data: {vehicleId: vehicleId},
            success: function(respond) {
                p_notification(respond.status, respond.msg);
                if (respond.status) {
                    $('#vehicleDeleteModal').modal('hide');
                    $('#vehicleMake,#vehicleModel,#vehicleRegistrationNumber,#vehicleCost').val('');
                    getVehicles('', 1);
                }
            }
        });
    });

    //for search btn
    $(document).on('click', '#searchBtn', function(e) {
        var key = $('#vehicle-search-keyword').val();
        getVehicles(key, 0);
    });

    //for search cancel btn
    $(document).on('click', '#cancelBtn', function(e) {
        $('#vehicle-search-keyword').val('');
        getVehicles('', 1);
    });

    $(document).on('click', '#updateCancelBtn', function(e) {
        $('#vehicleMake,#vehicleModel,#vehicleRegistrationNumber,#vehicleCost').val('');
        $('.updateDiv').addClass('hidden');
        $('.addDiv').removeClass('hidden');
    });

    //for reset btn
    $(document).on('click', '#resetBtn', function(e) {
        $('#vehicleMake,#vehicleModel,#vehicleRegistrationNumber,#vehicleCost').val('');
    });
});

function validateInput(data) {

    var regExCost = /^$|^[\d.]+$/;

    if (data.vehicleMake == null || data.vehicleMake == "") {
        p_notification(false, eb.getMessage('ERR_VHCL_MAKE_EMPTY'));
    }
    else if (data.vehicleModel == null || data.vehicleModel == "") {
        p_notification(false, eb.getMessage('ERR_VHCL_MODEL_EMPTY'));
    }
    else if (data.vehicleRegistrationNumber == null || data.vehicleRegistrationNumber == "") {
        p_notification(false, eb.getMessage('ERR_VHCL_REG_NUM_EMPTY'));
    }
    else if (!regExCost.test(data.vehicleCost)) {
        p_notification(false, eb.getMessage('ERR_VHCL_COST_TYPE'));
    }
    else {
        return true;
    }
}

function getVehicles(key, paginated) {
    eb.ajax({
        type: 'POST',
        url: BASE_URL + '/vehicle-api/search',
        data: {key: key, paginated: paginated},
        success: function(respond) {
            $('#vehicle-list').html(respond);
        }
    });
}
