var costTypeID = '';
var editMode = false;
var productID = '';
$(function() {

    loadDropDownFromDatabase('/productAPI/searchNonInventoryActiveProductsForDropdown', "", false, '#costItem');
    $('#costItem').selectpicker('refresh');
    $('#costItem').on('change', function() {
        productID = $(this).val();
    });

    $('#create-cost-type-form').on('submit', function(e) {
        e.preventDefault();
        var formData = {
            costTypeName: $('#costTypeName').val(),
            costTypeDescription: $('#costTypeDescription').val(),
            productID: productID,
            minimumValue: $('#minimumValue').val(),
            maximumValue: $('#maximumValue').val(),
            editMode: editMode,
            costTypeID: costTypeID,
        };
        if (validateFormData(formData)) {
            eb.ajax({
                url: BASE_URL + '/cost-type-api/add',
                method: 'post',
                data: formData,
                dataType: 'json',
                success: function(data) {
                    p_notification(data.status, data.msg);
                    if (data.status) {
                        window.setTimeout(function() {
                            location.reload();
                        }, 1000);
                    } else {
                        if (data.data == "CTCERR") {
                            $('#costTypeName').focus();
                        }
                    }
                }
            });
        }
    });
    $('#cost-type-search').on('submit', function(e) {
        e.preventDefault();
        var formData = {
            costTypeSearchKey: $('#cost-type-search-keyword').val(),
        };
        if (formData.costTypeSearchKey != '') {
            searchValue(formData);
        } else {
            p_notification(false, eb.getMessage('ERR_COSTYPE_SEARCH_KEY'));
        }
    });
    $('#cost-type-search').on('click', '.reset', function(e) {
        e.preventDefault();
        searchValue('');
        $('#cost-type-search-keyword').val('');
    });
    $('#cost-type-list').on('click', 'a.edit', function(e) {
        e.preventDefault();
        costTypeID = $(this).parents('tr').data('costtypeid');
        $('#costTypeName').val($(this).parents('tr').find('.CTN').text()).attr('disabled', true);
        $('#costTypeDescription').val($(this).parents('tr').find('.CTD').text());
        productID = $(this).parents('tr').find('.CTP').data('pid');
        $('#costItem').
                append($("<option></option>").
                        attr("value", productID).
                        text(NIPRODUCTLIST[productID]));
        $('#costItem').val(productID);
        $('#costItem').selectpicker('refresh');
        $('#minimumValue').val(accounting.unformat(($(this).parents('tr').find('.CTMINVAL').text())).toFixed(2));
        $('#maximumValue').val(accounting.unformat(($(this).parents('tr').find('.CTMAXVAL').text())).toFixed(2));
        $('.updateTitle').removeClass('hidden');
        $('.addTitle').addClass('hidden');
        $('.updateDiv').removeClass('hidden');
        $('.addDiv').addClass('hidden');
        editMode = true;
    });
    $('#create-cost-type-form').on('click', '.reset, .cancel', function(e) {
        e.preventDefault();
        costTypeID = '';
        $('#costTypeName').val('').attr('disabled', false);
        $('#costTypeDescription').val('');
        $('#minimumValue').val('');
        $('#maximumValue').val('');
        $('#costItem').val('').trigger('change');
        $('.updateTitle').addClass('hidden');
        $('.addTitle').removeClass('hidden');
        $('.updateDiv').addClass('hidden');
        $('.addDiv').removeClass('hidden');
        editMode = false;
    });

    $('#cost-type-list').on('click', 'a.status', function() {
        var msg;
        var status;
        var costFlag = true;
        var costTypeID = $(this).parents('tr').data('costtypeid');
        var currentDiv = $(this).contents();
        if ($(this).children().hasClass('fa-check-square-o')) {
            msg = 'Are you sure you want to Deactivate this cost type';
            status = '0';
        }
        else if ($(this).children().hasClass('fa-square-o')) {
            msg = 'Are you sure you want to Activate this cost type';
            status = '1';
        }
        activeInactive(costTypeID, status, currentDiv, msg, costFlag);
    });

    $('#cost-type-list').on('click', 'a.delete', function(e) {
        e.preventDefault();
        $('#costTypeName').val('').attr('disabled', false);
        $('#costTypeDescription').val('');
        $('.updateTitle').addClass('hidden');
        $('.addTitle').removeClass('hidden');
        $('.updateDiv').addClass('hidden');
        $('.addDiv').removeClass('hidden');
        editMode = false;
        var costTypeID = $(this).parents('tr').data('costtypeid');
        var costTypeName = $(this).parents('tr').find('.CTN').text();
        var currentDiv = $(this).parents('tr').find('a.status').contents();
        var costFlag = false;
        var msg = eb.getMessage('COST_TYPE_DELETE_DEACTIVE');
        bootbox.confirm('Are you sure you want to delete this Cost Type?', function(result) {
            if (result === true) {
                eb.ajax({
                    url: BASE_URL + '/cost-type-api/deleteCostTypeByCostTypeID',
                    method: 'post',
                    data: {
                        costTypeID: costTypeID,
                        costTypeName: costTypeName,
                    },
                    dataType: 'json',
                    success: function(data) {
                        if (data.status == true) {
                            p_notification(data.status, data.msg);
                            window.location.reload();
                        }
                        else if (data.status == false && data.data == 'deactive') {
                            activeInactive(costTypeID, '0', currentDiv, msg, costFlag);
                        } else {
                            p_notification(data.status, data.msg);
                        }
                    }
                });
            }
        });
    });
    function validateFormData(formData) {
        if (formData.costTypeName == "") {
            p_notification(false, eb.getMessage('ERR_COSTYPE_NAME_CNT_BE_NULL'));
            $('#costTypeName').focus();
            return false;
        } else if (formData.costTypeDescription == '') {
            p_notification(false, eb.getMessage('ERR_COSTYPE_DESCRIPTION_CNT_BE_NULL'));
            $('#costTypeDescription').focus();
            return false;
        } else if (formData.productID == '') {
            p_notification(false, eb.getMessage('ERR_COSTYPE_ITEM_CNT_BE_NULL'));
            $('#costItem').focus();
            return false;
        } else if (formData.minimumValue != '' && (isNaN(formData.minimumValue) || formData.minimumValue < 0)) {
            p_notification(false, eb.getMessage('ERR_COSTYPE_MINVAL_CANT_BE_NAN'));
            $('#minimumValue').focus();
            return false;
        } else if (formData.maximumValue != '' && (isNaN(formData.maximumValue) || formData.maximumValue < 0)) {
            p_notification(false, eb.getMessage('ERR_COSTYPE_MAXVAL_CANT_BE_NAN'));
            $('#maximumValue').focus();
            return false;
        } else if (formData.minimumValue != '' && formData.maximumValue != '') {
            if (formData.minimumValue > formData.maximumValue) {
                p_notification(false, eb.getMessage('ERR_COSTYPE_MINVAL_CANT_BE_MORE_THAN_MAXVAL'));
                return false;
            } else {
                return true;
            }
        }
        return true;
    }

    function searchValue(formData) {
        eb.ajax({
            url: BASE_URL + '/cost-type-api/getCostTypesBySearchKey',
            method: 'post',
            data: formData,
            dataType: 'json',
            success: function(data) {
                if (data.status) {
                    $("#cost-type-list").html(data.html);
                } else {
                    p_notification(data.status, data.msg);
                }
            }
        });
    }
    /**
     *
     * use to change active state
     */
    function activeInactive(type, status, currentDiv, msg, costFlag) {
        bootbox.confirm(msg, function($result) {
            if ($result == true) {
                eb.ajax({
                    url: BASE_URL + '/cost-type-api/changeStatusID',
                    method: 'post',
                    dataType: 'json',
                    data: {
                        'costTypeID': type,
                        'status': status,
                    },
                    success: function(data) {
                        if (data.status) {
                            p_notification(data.status, data.msg);
                            if (currentDiv.hasClass('fa-square-o')) {
                                currentDiv.addClass('fa-check-square-o');
                                currentDiv.removeClass('fa-square-o');
                            }
                            else {
                                currentDiv.addClass('fa-square-o');
                                currentDiv.removeClass('fa-check-square-o');
                            }
                        } else {
                            p_notification(data.status, data.msg);
                        }
                    }
                });
            }
        });
    }
}
);