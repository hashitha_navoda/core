/**
 * @author Sandun <sandun@thinkcube.com>
 * This file contains Activity related js function
 */
var activityTypeID = null;
var projectID = null;
var jobReferenceID = null;
var activeProductData = {};
var contractors = {};//contractor details object array
var tempContractors = {};//temporary contractor details object array
var activityOwners = {};//activity owners details object array
var activitySupervisors = {};//activity supervisors details object array
var rawMaterialProducts = {};
var rawMaterialSubProducts = {};
var selectedRMProductID = null;
var fixedAssetsProducts = {};
var selectedFAProductID = null;
var tempProducts = {};
var temporaryProductList = {};
var batchProducts = {};
var serialProducts = {};
var jobReference = {};
var costTypes = {};
var currentWeight = null;
var activityWeightId = new Array();
var activityWeightValue = {};
var temporaryProductImages = {};
var vehicles = {};

$(document).ready(function() {
    $('#activityRepeatComment').hide();
    var activityTypeID;
    //when click modal load btn focus first field on that modal
    $('#addContractorsBtn').on('click', function() {
        $('.modal').on('shown.bs.modal', function() {
            $(this).find('input:text:visible:first').focus();
        });
    });

    //set project code and job code if activity creats by job or project
    if ($('#projectReferenceID').data('projectid')) {
        projectID = $('#projectReferenceID').data('projectid');
        var projectReferenceID = $('#projectReferenceID').data('projectid');
        $('#projectReferenceID').
                append($("<option></option>").
                        attr("value", projectReferenceID).
                        text(PROJECT_LIST[projectReferenceID]));
        $('#projectReferenceID').val(projectReferenceID).prop('disabled', true);
        $('#projectReferenceID').selectpicker('refresh');
    }
    if ($('#jobReferenceID').data('jobid')) {
        jobReferenceID = $('#jobReferenceID').data('jobid');
        $('#jobReferenceID').
                append($("<option></option>").
                        attr("value", jobReferenceID).
                        text(JOBREFERENCELIST[jobReferenceID]));
        $('#jobReferenceID').val(jobReferenceID).prop('disabled', true);
        $('#jobReferenceID').selectpicker('refresh');
        setSupervisorAndOwnerByJobID(jobReferenceID);

    }
    $('#timeCost').hide();
    $('#showTimeAndCost').on('click', function() {
        $("#showTimeAndCost").toggleClass('expanded');
        $("#timeCost").slideToggle();
        $('#materialItemAndAssets').hide();
        $('#activeUsers').hide();
        $('#activityVehicle').hide();
    });

    $('#materialItemAndAssets').hide();
    $('#showMeterialAndItems').on('click', function() {
        $("#showMeterialAndItems").toggleClass('expanded');
        $("#materialItemAndAssets").slideToggle();
        $('#timeCost').hide();
        $('#activeUsers').hide();
        $('#activityVehicle').hide();
    });

    $('#activeUsers').hide();
    $('#showActivePerson').on('click', function() {
        $("#showActivePerson").toggleClass('expanded');
        $("#activeUsers").slideToggle();
        $('#timeCost').hide();
        $('#materialItemAndAssets').hide();
        $('#activityVehicle').hide();
    });

    $('#activityVehicle').hide();
    $('#activityVehiclePanel').on('click', function() {
        $("#activityVehiclePanel").toggleClass('expanded');
        $("#activityVehicle").slideToggle();
        $('#materialItemAndAssets').hide();
        $('#activeUsers').hide();
        $('#timeCost').hide();
    });

    var locationID = $('#locationID').val();
    var activityTypeName = 'activity';
    //get active product list
    eb.ajax({
        type: 'POST',
        url: BASE_URL + '/productAPI/getActiveProductsFromLocation',
        data: {locationID: locationID,
            dataType: activityTypeName},
        success: function(respond) {
            activeProductData = respond.data;
        }
    });

    //get active temporary product list
    eb.ajax({
        type: 'POST',
        url: BASE_URL + '/api/temporary-product/getTemporaryProducts',
        data: {},
        success: function(respond) {
            temporaryProductList = respond.data;
        }
    });

    $('#activityName').typeahead({
        ajax: {
            url: BASE_URL + '/api/activity/get-activities-for-typeahead',
            timeout: 250,
            triggerLength: 1,
            method: "POST",
            preProcess: function(respond) {
                if (!respond.status) {
                    return false;
                }
                return respond.data;
            }
        }
    });

    var $rawProductTable = $("#rawProductTable");
    var $addRowSample = $('tr.add-row.sample', $rawProductTable);
    var getAddRow = function(productID) {
        if (productID != undefined) {
            var $row = $('table.transfer-table > tbody > tr', $rawProductTable).filter(function() {
                return $(this).data("id") == productID
            });
            return $row;
        }
        return $('tr.add-row:not(.sample)', $rawProductTable);
    };

    $('#estimateTime').addDuration();
    var startdateFormat = $('#startTime').data('date-format');
    $('#startTime').datetimepicker({
        format: startdateFormat + ' hh:ii',
    }).on('change', function(ev) {
        $('#startTime').datetimepicker('hide');
        setDates(true);
    });
    $('#estimateTime').on('click', function(e) {
        e.preventDefault();
        setDates(false);
    });

    var enddateFormat = $('#endTime').data('date-format');
    $('#endTime').datetimepicker({
        format: enddateFormat + ' hh:ii',
    }).on('change', function(ev) {
        $('#endTime').datetimepicker('hide');
        setDates(true);
    });

    loadDropDownFromDatabase('/api/activity/searchActivityTypesForDropdown', "", 1, '#activityTypeID', false, false, false, true);
    $('#activityTypeID').selectpicker('refresh');
    $('#activityTypeID').on('change', function() {
        if ($(this).val() > 0)
            activityTypeID = $(this).val();

    });

    $('#childjob').selectpicker('hide');

    loadDropDownFromDatabase('/project-api/searchProjectsForDropdown', "", 1, '#projectReferenceID');
    $('#projectReferenceID').selectpicker('refresh');
    $('#projectReferenceID').on('change', function() {
        if ($(this).val() > 0 && ($(this).val() != projectID)) {
            projectID = $(this).val();
            $('#jobReferenceID').selectpicker('hide');
            $('#childjob').selectpicker('show');
            getJobList(projectID);
            $('#jobReferenceID').val('');
            $('#childjob').val('');
            jobReferenceID = '';
            activityID = '';
        }
        if ($(this).val() == 0) {
            $('#jobReferenceID').selectpicker('show');
            $('#childjob').selectpicker('hide');
            resetSelectPicker('jobReferenceID', 'Job');
        }
    });

    loadDropDownFromDatabase('/job-api/searchJobsForDropdown', "", '', '#jobReferenceID');
    $('#jobReferenceID').selectpicker('refresh');
    $('#jobReferenceID').on('change', function() {
        $('.addedActivitySupervisor').remove();
        $('.addedActivityOwner').remove();
        if ($(this).val() > 0 && ($(this).val() != jobReferenceID)) {
            jobReferenceID = $(this).val();
            setSupervisorAndOwnerByJobID(jobReferenceID);
            if (jobReferenceID) {

                getActivityList(jobReferenceID, JOBREFERENCELIST);
            }
        } else {

            jobReferenceID = '';
        }
        loadDropDownFromDatabase('/api/temporary-product/getTemporaryProductsBySearchKey', "", jobReferenceID, '#temporaryProductID');
    });

    function getActivityList(jobID, Jobs) {
        eb.ajax({
            url: BASE_URL + '/api/activity/getActivitiesByJobID',
            method: 'post',
            data: {
                jobID: jobID,
                jobCode: Jobs[jobID]
            },
            dataType: 'json',
            success: function(data) {
                if(data.status){
                    if (data.data.projectID) {
                        $('#projectReferenceID').html("<option value=''>" + "Select a Project" + "</option>\n\
                        <option value='" + data.data.projectID + "'>" + data.data.projectCode + "</option>");
                        $('#projectReferenceID').selectpicker('refresh');
                        projectID = data.data.projectID;
                    } else {
                        projectID = "";
                    }
                    $('#projectReferenceID').val(projectID).selectpicker('refresh');
                } else {
                     p_notification(data.status, data.msg);
                     resetSelectPicker("jobReferenceID", "Job");
                }
            }
        });
    }

    loadDropDownFromDatabase('/api/temporary-product/getTemporaryProductsBySearchKey', "", jobReferenceID, '#temporaryProductID');
    $('#temporaryProductID').selectpicker('refresh');
    $('#temporaryProductID').on('change', function() {
        if ($(this).val() > 0 && ($(this).val() != tempProductID)) {
            $('.temp-product-table').removeClass('hidden');
            tempProductID = $(this).val();
            if ($.isEmptyObject(tempProducts[tempProductID])) {
                var dataArray = TEMPORARYPRODUCTLIST[tempProductID]
                tempProducts[tempProductID] = new tempProductDetails(tempProductID, dataArray['temporaryProductCode'], dataArray['temporaryProductName'], dataArray['temporaryProductDescription'], dataArray['temporaryProductQuantity'], dataArray['temporaryProductPrice'], dataArray['uomID'], true, dataArray['temporaryProductBatch'], dataArray['temporaryProductSerial'], dataArray['temporaryProductImageURL']);
                setTempProductList(tempProductID, dataArray['temporaryProductCode'], dataArray['temporaryProductName'], dataArray['temporaryProductQuantity'], UOMLISTBYID[dataArray['uomID']].uomAbbr, dataArray['temporaryProductBatch'], dataArray['temporaryProductSerial']);
            } else {
                p_notification(false, eb.getMessage('ERR_TEMP_PRODUCT_ALREDY_ADDED'));
            }
        } else {
            tempProductID = '';

        }
    });

    function getJobList(projectIDparam) {
        eb.ajax({
            url: BASE_URL + '/job-api/getJobsByProjectID',
            method: 'post',
            data: {
                projectID: projectIDparam,
                projectCode: PROJECT_LIST[projectIDparam]
            },
            dataType: 'json',
            success: function(data) {
                if (data.status) {
                    setJobSelectPicker(data.data);
                } else {
                    p_notification(data.status, data.msg);
                    resetSelectPicker("projectReferenceID", "Project");
                    jobReferenceID = '';
                    activityID = '';
                    projectID = '';
                    $('#jobReferenceID').val('');
                    $('#progress').val('');
                }
            }
        });
    }

    function setJobSelectPicker(Jobs) {
        $('#childjob').html("<option value=''>" + "Select a Job" + "</option>");
        $.each(Jobs, function(index, value) {
            $('#childjob').append("<option value='" + index + "'>" + value + "</option>");
        });
        $('#childjob').selectpicker('refresh');

        $('#childjob').on('change', function(e) {
            e.preventDefault();
            if ($(this).val() > 0 && ($(this).val() != jobReferenceID)) {
                jobReferenceID = $(this).val();
                if (jobReferenceID) {
                    getActivityList(jobReferenceID, JOBREFERENCELIST);
                }

            } else {
                jobReferenceID = '';
            }
        });
    }

    loadDropDownFromDatabase('/api/activity/searchContractorForDropdown', "", 1, '#contractorID');
    $('#contractorID').selectpicker('refresh');
    var contractorKey;
    $('#contractorID').on('change', function() {
        if ($(this).val() > 0 && $(this).val() != contractorKey) {
            contractorKey = $(this).val();
            if (!contractors[contractorKey]) {
                $('#contractorID').
                        append($("<option></option>").
                                attr("value", 'select').
                                text("Select Contractor"));
                $('#contractorID').val('select');
                $('#contractorID').selectpicker('refresh');
                $('.contractor-table').removeClass('hidden');
                setContractorsList(contractorKey);
            } else {
                p_notification(false, eb.getMessage('ERR_CONTRACTR_NAME_ALREDY_EXIST'));
                $('#contractorID').focus();
            }
        }
    });

    $(document).on('click', '.contractor-delete', function() {
        var deleteCTrID = $(this).closest('tr').attr('id');
        var deleteCID = deleteCTrID.split('tr_')[1].trim();
        delete contractors[deleteCID];
        $('#' + deleteCTrID).remove();

        if (_.values(contractors).length == 0) {
            $('.contractor-table').addClass('hidden');
        }
    });

    $("#addContractorsModal .close").on("click", function(e) {
        e.preventDefault();
        resetContractorModal();
    });

    loadDropDownFromDatabase('/api/activity/searchTempContractorForDropdown', "", 1, '#temporaryContractorID');
    $('#temporaryContractorID').trigger('change');
    var tempContractorKey;
    $('#temporaryContractorID').on('change', function() {
        if ($(this).val() > 0 && $(this).val() != tempContractorKey) {
            tempContractorKey = $(this).val();
            if (!tempContractors[tempContractorKey]) {
                $('#temporaryContractorID').
                        append($("<option></option>").
                                attr("value", 'select').
                                text("Select Temporary Contractor"));
                $('#temporaryContractorID').val('select');
                $('#temporaryContractorID').selectpicker('refresh');
                $('.temp-contractor-table').removeClass('hidden');
                setTempContractorsList(tempContractorKey);
            } else {
                p_notification(false, eb.getMessage('ERR_TEMP_CONTRACTR_NAME_ALREDY_EXIST'));
                $('#temporaryContractorID').focus();
            }
        }
    });

    $(document).on('click', '.temp-contractor-delete', function() {
        var deleteTempCTrID = $(this).closest('tr').attr('id');
        var deleteTempCID = deleteTempCTrID.split('tr_')[1].trim();
        delete tempContractors[deleteTempCID];
        $('#' + deleteTempCTrID).remove();

        if (_.values(tempContractors).length == 0) {
            $('.temp-contractor-table').addClass('hidden');
        }
    });

    $("#addTempContractorsModal .close").on("click", function(e) {
        e.preventDefault();
        resetTempContractorModal();
    });

    loadDropDownFromDatabase('/employee_api/searchEmployeeForDropdown', "", 1, '#activityOwnerID');
    $('#activityOwnerID').trigger('change');
    var activityOwnerKey;
    $('#activityOwnerID').on('change', function() {
        if ($(this).val() > 0 && $(this).val() != activityOwnerKey) {
            activityOwnerKey = $(this).val();
            if (!activityOwners[activityOwnerKey]) {
                $('#activityOwnerID').
                        append($("<option></option>").
                                attr("value", 'select').
                                text("Select Activity Owner"));
                $('#activityOwnerID').val('select');
                $('#activityOwnerID').selectpicker('refresh');
                $('.activity-owner-table').removeClass('hidden');
                setActivityOwnerList(activityOwnerKey);
            } else {
                p_notification(false, eb.getMessage('ERR_ACT_OWNR_NAME_ALREDY_EXIST'));
                $('#activityOwnerID').focus();
            }
        }
    });

    $(document).on('click', '.activity-owner-delete', function() {
        var deleteActOwnerTrID = $(this).closest('tr').attr('id');
        var deleteActOwnerID = deleteActOwnerTrID.split('tr_')[1].trim();
        delete activityOwners[deleteActOwnerID];
        $('#' + deleteActOwnerTrID).remove();

        if (_.values(activityOwners).length == 0) {
            $('.activity-owner-table').addClass('hidden');
        }
    });

    loadDropDownFromDatabase('/employee_api/searchEmployeeForDropdown', "", 1, '#activitySupervisorID');
    $('#activitySupervisorID').trigger('change');
    var activitySupervisorKey;
    $('#activitySupervisorID').on('change', function() {
        if ($(this).val() > 0 && $(this).val() != activitySupervisorKey) {
            activitySupervisorKey = $(this).val();
            if (!activitySupervisors[activitySupervisorKey]) {
                $('#activitySupervisorID').
                        append($("<option></option>").
                                attr("value", 'select').
                                text("Select Supervisor"));
                $('#activitySupervisorID').val('select');
                $('#activitySupervisorID').selectpicker('refresh');
                $('.activity-supervisor-table').removeClass('hidden');
                setActivitySupervisorList(activitySupervisorKey);
            } else {
                p_notification(false, eb.getMessage('ERR_ACT_SUPWISR_NAME_ALREDY_EXIST'));
                $('#activitySupervisorID').focus();
            }
        }
    });

    $(document).on('click', '.activity-supervisor-delete', function() {
        var deleteActSupTrID = $(this).closest('tr').attr('id');
        var deleteActSupID = deleteActSupTrID.split('tr_')[1].trim();
        delete activitySupervisors[deleteActSupID];
        $('#' + deleteActSupTrID).remove();

        if (_.values(activitySupervisors).length == 0) {
            $('.activity-supervisor-table').addClass('hidden');
        }
    });

    loadDropDownFromDatabase('/api/activity/searchRawMaterialProductForDropdown', "", 0, '#rawMaterialID');
    $('#rawMaterialID').trigger('change');
    var rawMaterialKey;
    $('#rawMaterialID').on('change', function() {
        if ($(this).val() > 0 && $(this).val() != rawMaterialKey) {
            rawMaterialKey = $(this).val();
            selectedRMProductID = $(this).val();
            $('#rawMaterialID').
                    append($("<option></option>").
                            attr("value", 'select').
                            text("Select Raw Material"));
            $('#rawMaterialID').val('select');
            $('#rawMaterialID').selectpicker('refresh');
            if (!rawMaterialProducts[rawMaterialKey]) {
                $('form#product-batch-modal .product-type').attr("id", "rawProductModal");
                selectProducts(rawMaterialKey);
            } else {
                p_notification(false, eb.getMessage('ERR_GRN_INSERT_PROD'));
                $('#rawMaterialID').focus();
            }
        }
    });

    $('#batchSave').on('click', function(e) {
        e.preventDefault();
        var $batchTable = $("#add-product-batch .batch-table tbody");
        var qtyTotal = 0;
        var subProducts = {};
        $("input[name='addQuantity'], input[name='addQuantityCheck']:checked", $batchTable).each(function() {

            var $thisSubRow = $(this).parents('tr');
            var thisAddQuantity = $(this).val();
            if ((thisAddQuantity).trim() != "" && isNaN(parseFloat(thisAddQuantity))) {
                p_notification(false, eb.getMessage('ERR_TRANSFER_VALQUAN'));
                $(this).focus();
                return qtyTotal = false;
            }

            thisAddQuantity = (isNaN(parseFloat(thisAddQuantity))) ? 0 : parseFloat(thisAddQuantity);
            // check if trasnfer qty is greater than available qty
            var thisAvailableQuantity = $("input[name='availableQuantity']", $thisSubRow).val();
            thisAvailableQuantity = (isNaN(parseFloat(thisAvailableQuantity))) ? 0 : parseFloat(thisAvailableQuantity);
            var thisAvailableQuantityByBase = thisAvailableQuantity;
            var thisTransferQuantityByBase = thisAddQuantity;
            if (thisTransferQuantityByBase > thisAvailableQuantityByBase) {
                p_notification(false, eb.getMessage('ERR_TRANSFER_QUAN'));
                $(this).focus();
                return qtyTotal = false;
            }

            qtyTotal = qtyTotal + (thisAddQuantity);
            // if a product transfer is present, prepare array to be sent to backend

            if ((thisAddQuantity) > 0) {

                var thisSubProduct = {};
                var mapKey = '';
                if ($(".batchCode", $thisSubRow).data('id')) {
                    thisSubProduct.batchID = $(".batchCode", $thisSubRow).data('id');
                    mapKey = mapKey + 'B' + thisSubProduct.batchID;
                }

                if ($(".serialID", $thisSubRow).data('id')) {
                    thisSubProduct.serialID = $(".serialID", $thisSubRow).data('id');
                    mapKey = mapKey + 'S' + thisSubProduct.serialID;
                }

                if (mapKey != '') {
                    thisSubProduct.qtyByBase = thisAddQuantity;
                    subProducts[mapKey] = thisSubProduct;
                }
            }

        });

        if (qtyTotal > 0) {
            var modalType = $(".product-type").attr('id');
            if (modalType === "rawProductModal") {
                $('.raw-product-table').removeClass('hidden');
                setRawMaterialProductList(selectedRMProductID, qtyTotal, subProducts);
                $("input[name='unitPrice']").focus();
            } else if (modalType === "fixedAsstes") {
                $('.fixed-assests-product-table').removeClass('hidden');
                setFixedAssetsProductList(selectedFAProductID, qtyTotal, subProducts);
            }
        }
        if (_.values(rawMaterialProducts).length == 0) {
            $('.raw-product-table').addClass('hidden');
        }
        if (_.values(fixedAssetsProducts).length == 0) {
            $('.fixed-assests-product-table').addClass('hidden');
        }
        $('#add-product-batch').modal('hide');
    });

    $(document).on('click', '.raw-product-plus', function() {
        var unitPriceValue = $(this).closest('tr').find("input[name='unitPrice']").val();
        if (!isNaN(parseFloat(unitPriceValue)) && parseFloat(unitPriceValue) > 0) {
            var rawMProTrID = $(this).closest('tr').attr('id');
            var rawMProID = rawMProTrID.split('tr_')[1].trim();
            if (rawMaterialProducts[rawMProID]) {
                rawMaterialProducts[rawMProID].rawMaterialProUnitPrice = unitPriceValue;
                $("#rawMaterialID").attr('disabled', false);
                $(this).closest('tr').find("input[name='unitPrice']").val(parseFloat(unitPriceValue).toFixed(2));
                $(this).closest('tr').find("input.uomPrice").attr('disabled', true);
                $(this).addClass('hidden');
            }
        } else {
            p_notification(false, eb.getMessage('ERR_VLID_UPRICE'));
            $(this).closest('tr').find("input[name='unitPrice']").select();
        }
    });

    $(document).on('click', '.raw-product-delete', function() {
        var deleteRawMProTrID = $(this).closest('tr').attr('id');
        var deleteRawMProID = deleteRawMProTrID.split('tr_')[1].trim();
        delete rawMaterialProducts[deleteRawMProID];
        $('#' + deleteRawMProTrID).remove();
        $("#rawMaterialID").attr('disabled', false);

        if (_.values(rawMaterialProducts).length == 0) {
            $('.raw-product-table').addClass('hidden');
        }
    });

    loadDropDownFromDatabase('/api/activity/searchFixedAssetsProductForDropdown', "", 1, '#fixedAssetsID');
    $('#fixedAssetsID').trigger('change');
    var fixedAssetsKey;
    $('#fixedAssetsID').on('change', function() {
        if ($(this).val() > 0 && $(this).val() != fixedAssetsKey) {
            fixedAssetsKey = $(this).val();
            selectedFAProductID = $(this).val();
            $('#fixedAssetsID').
                    append($("<option></option>").
                            attr("value", 'select').
                            text("Select Fixed Assets"));
            $('#fixedAssetsID').val('select');
            $('#fixedAssetsID').selectpicker('refresh');

            if (!fixedAssetsProducts[fixedAssetsKey]) {
                $('form#product-batch-modal .product-type').attr("id", "fixedAsstes");
                $("#add-product-batch").modal('show');
                selectProducts(fixedAssetsKey);
            } else {
                p_notification(false, eb.getMessage('ERR_GRN_INSERT_PROD'));
                $('#fixedAssetsID').focus();
            }
        }
    });

    $(document).on('click', '.fixed-assests-product-plus', function() {
        var unitPriceValue = $(this).closest('tr').find("input[name='unitPrice']").val();
        if (!isNaN(parseFloat(unitPriceValue)) && parseFloat(unitPriceValue) > 0) {
            var fixedAProTrID = $(this).closest('tr').attr('id');
            var fixedAProID = fixedAProTrID.split('tr_')[1].trim();
            if (fixedAssetsProducts[fixedAProID]) {
                fixedAssetsProducts[fixedAProID].fixedAssetsProUnitPrice = unitPriceValue;
                $("#fixedAssetsID").attr('disabled', false);
                $(this).closest('tr').find("input[name='unitPrice']").val(parseFloat(unitPriceValue).toFixed(2));
                $(this).closest('tr').find("input.uomPrice").attr('disabled', true);
                $(this).addClass('hidden');
            }
        } else {
            p_notification(false, eb.getMessage('ERR_VLID_UPRICE'));
            $(this).closest('tr').find("input[name='unitPrice']").select();
        }
    });

    $(document).on('click', '.fixed-assests-product-delete', function() {

        var deleteFixedAProTrID = $(this).closest('tr').attr('id');
        var deleteFixedAProID = deleteFixedAProTrID.split('tr_')[1].trim();
        delete fixedAssetsProducts[deleteFixedAProID];
        $('#' + deleteFixedAProTrID).remove();
        $("#fixedAssetsID").attr('disabled', false);

        if (_.values(fixedAssetsProducts).length == 0) {
            $('.fixed-assests-product-table').addClass('hidden');
        }
    });

    $('#addBatchItem').on('click', function(e) {
        e.preventDefault();
        var batchCode = $("input[name='batchCode']").val();
        var temporaryProductQuantity = $('#temporaryProductQuantity').val();
        if ($("input[name='batchCode']").val() == '') {
            p_notification(false, eb.getMessage('ERR_ENTER_BCD'));
            return false;
        }
        $('#batchSample').addClass('hidden');
        var cloneBatchAdd = $($('#addNewBatchSample').clone()).attr('id', batchCode).removeClass('hidden').addClass('newBatchProduct');
        cloneBatchAdd.children('#batchCD').html(batchCode);
        cloneBatchAdd.children('#batchQty').html(temporaryProductQuantity);
        cloneBatchAdd.insertBefore('#addNewBatchSample');
        batchProducts[batchCode] = new batchProductDetails(batchCode, temporaryProductQuantity);
    });

    $('#tempProductModal').on('click', '.batchDelete', function() {
        var deleteBID = $(this).closest('tr').attr('id');
        delete(batchProducts[deleteBID]);
        $('#' + deleteBID).remove();
        $('#batchSample').removeClass('hidden');
    });

    $('#tempProductModal').on('change', "#addNewSerial input[name='batchCode']", function() {
        if (!batchProducts.hasOwnProperty($(this).val())) {
            p_notification(false, eb.getMessage('ERR_GRN_ENTER_BATCHNUM'));
            $(this).val('');
        }
    });

    $('#tempProductModal').on('change', "#addNewSerial input[name='serialCode']", function() {
        var matchSerialCodeCount = 0;
        var typedValue = $(this).val();
        $("#addNewSerial > tr input[name='serialCode']").each(function() {
            if ($(this).val() == typedValue) {
                matchSerialCodeCount++;
            }
        });
        if (matchSerialCodeCount > 1) {
            p_notification(false, eb.getMessage('ERR_ENTER_SAME_SCD'));
            $(this).val('');
        }
    });

    $('#tempProductSave').on('click', function(e) {
        e.preventDefault();

        var tempProductCode = $('#temporaryProductCode').val();
        var tempProductName = $('#temporaryProductName').val();
        var tempProductDescription = $('#temporaryProductDescription').val();
        var tempQuantity = $('#temporaryProductQuantity').val();
        var tempProductPrice = $('#temporaryProductPrice').val();
        var tempUomID = $('#uomID').val();
        var batchProduct = $('#batchProduct').val();
        var serialProduct = $('#serialProduct').val();
        var tempProductFormData = {
            tempProductCode: tempProductCode,
            tempProductName: tempProductName,
            tempQuantity: tempQuantity,
            tempUomID: tempUomID
        }

        if (validateTemporaryProductForm(tempProductFormData)) {
            $('.temp-product-table').removeClass('hidden');
            eb.ajax({
                type: 'POST',
                url: BASE_URL + '/api/temporary-product/save',
                data: {
                    temporaryProductCode: tempProductCode,
                    temporaryProductName: tempProductName,
                    temporaryProductDescription: tempProductDescription,
                    temporaryProductQuantity: tempQuantity,
                    temporaryProductPrice: tempProductPrice,
                    uomID: tempUomID,
                    batchProduct: batchProduct,
                    serialProduct: serialProduct,
                    jobID: jobReferenceID,
                    projectID: projectID,
                    temporaryProductImages: temporaryProductImages

                },
                success: function(respond) {
                    if (respond.status) {
                        var tempProductID = respond.data;
                        tempProducts[tempProductID] = new tempProductDetails(tempProductID, tempProductCode, tempProductName, tempProductDescription, tempQuantity, tempProductPrice, tempUomID, true, batchProduct, serialProduct, temporaryProductImages);
                        setTempProductList(tempProductID, tempProductCode, tempProductName, tempQuantity, UOMLISTBYID[tempUomID].uomAbbr, batchProduct, serialProduct);
                        resetTempProductModal();
                        $('#addTempProductModal').modal('hide');
                    } else {
                        p_notification(respond.status, respond.msg);
                    }
                }
            });
        }
    });

    $(document).on('click', '.temp-product-delete', function() {

        var deleteTempProTrCD = $(this).closest('tr').attr('id');
        var deleteTempProCD = deleteTempProTrCD.split('tr_')[1].trim();
        delete tempProducts[deleteTempProCD];
        $('#' + deleteTempProTrCD).remove();

        if (_.values(tempProducts).length == 0) {
            $('.temp-product-table').addClass('hidden');
        }
    });

    $("#showImageAndController").on('click', function() {
        $("#showImageAndController").toggleClass('expanded');
        $("#imageAndController").slideToggle();
    });

    $('#image-submit').on('click', function(e) {

        input = document.getElementById('image_file');
        var file;
        file = input.files[0];
        if (file) {

            imagedata = false;
            if (window.FormData) {
                imagedata = new FormData();
            }
            e.preventDefault();
            imagedata = eb.getImageDimension("images", file, imagedata, '200', '200', '0', '0', '200', '200');

            eb.ajax({
                url: BASE_URL + '/image-preview', type: 'POST',
                processData: false,
                contentType: false,
                data: imagedata,
                success: function(data) {
                    $('#thumbs').attr('src', data);
                    $('#imageUploaderModal').modal('hide');
                    $('#image_file').data('flag', true);
                    return data;
                }
            });

        }
    });

    $('#tempProductModal .close').on('click', function() {
        resetTempProductModal();
    });

    loadDropDownFromDatabase('/vehicle-api/searchVehicleForDropdown', "", 1, '#vehicleBox');
    $('#vehicleBox').trigger('change');
    var vehicleId = null;
    var vehicleCost = 0;
    var vehicalRegNum = null;
    var vehicleCostFlag = false;

    $('#vehicleBox').on('change', function() {
        vehicleCostFlag = false;
        if ($(this).val() > 0 && $(this).val() != vehicleId) {
            vehicleId = $(this).val();
            vehicalRegNum = $(this).find("option:selected").text();
            $('#vehicleBox').
                    append($("<option></option>").
                            attr("value", 'select').
                            text("Select Vehicle"));
            $('#vehicleBox').val('select');
            $('#vehicleBox').selectpicker('refresh');

            if (!vehicles[vehicleId]) {
                $('.vehicle-table').removeClass('hidden');
                $('#vehicleSampleRow').removeClass('hidden');
                $("#vehicleBox").prop('disabled', true);
                $("#vehicleSampleRow input[name='vehicleRegNum']").val(vehicalRegNum);
                $("#vehicleSampleRow input[name='vehicleRegNum']").data('vehicleId', vehicleId);
                $("#vehicleSampleRow input[name='startMileage']").focus();
            } else {
                p_notification(false, eb.getMessage('ERR_VHCL_ALREDY_EXIST'));
                $('#vehicleBox').focus();
            }
        }
    });

    $(document).on('focusin', '.vehicle-mileage', function() {
        if (vehicleCostFlag == false) {
            $.ajax({
                type: 'POST',
                url: BASE_URL + '/vehicle-api/get-vehicle',
                data: {vehicleId: vehicleId},
                success: function(respond) {
                    vehicleCost = respond.data.vehicleCost;
                    vehicleCostFlag = true;
                }
            });
        }
    });

    $(document).on('focusout', '.vehicle-mileage', function() {
        var startMileage = $(this).closest('#vehicleSampleRow').find("input[name='startMileage']").val();
        var endMileage = $(this).closest('#vehicleSampleRow').find("input[name='endMileage']").val();
        var distance;
        var amount;
        if (startMileage && endMileage) {
            if (validateVehicleInput(startMileage, endMileage)) {
                distance = endMileage - startMileage;
                amount = parseFloat(distance) * parseFloat(vehicleCost);
                $(this).closest('#vehicleSampleRow').find("input[name='vehicleDistance']").val(distance);
                $(this).closest('#vehicleSampleRow').find("input[name='vehicleCost']").val(amount.toFixed(2));
            } else {
                $(this).closest('#vehicleSampleRow').find("input[name='vehicleDistance']").val('');
                $(this).closest('#vehicleSampleRow').find("input[name='vehicleCost']").val('');
            }
        }
    });

    $(document).on('click', '.vehicle-plus', function() {
        var vehicleId = $(this).closest('#vehicleSampleRow').find("input[name='vehicleRegNum']").data('vehicleId');
        var startMileage = $(this).closest('#vehicleSampleRow').find("input[name='startMileage']").val();
        var endMileage = $(this).closest('#vehicleSampleRow').find("input[name='endMileage']").val();
        var distance = $(this).closest('#vehicleSampleRow').find("input[name='vehicleDistance']").val();
        var vehicleCost = $(this).closest('#vehicleSampleRow').find("input[name='vehicleCost']").val();

        if (validateVehicleInput(startMileage, endMileage)) {
            setVehicleList(vehicleId, vehicalRegNum, startMileage, endMileage, distance, vehicleCost);
            $(this).closest('#vehicleSampleRow').find("input[name='vehicleRegNum'],input[name='startMileage'],input[name='endMileage'],input[name='vehicleDistance'],input[name='vehicleCost']").val('');
        }

    });

    $(document).on('click', '.vehicle-delete', function() {
        var deleteVehicleTrID = $(this).closest('tr').attr('id');
        var deleteVehicleId = deleteVehicleTrID.split('tr_')[1].trim();
        delete vehicles[deleteVehicleId];
        $('#' + deleteVehicleTrID).remove();

        if (_.values(vehicles).length == 0) {
            $('.vehicle-table').addClass('hidden');
        }
    });

    loadDropDownFromDatabase('/api/activity/searchCostTypeForDropdown', "", 1, '#costTypeID', false, false, false, true);
    $('#costTypeID').trigger('change');
    var costTypeKey;
    $('#costTypeID').on('change', function() {
        if ($(this).val() > 0 && $(this).val() != costTypeKey) {
            costTypeKey = $(this).val();
            $('#costTypeID').
                    append($("<option></option>").
                            attr("value", 'select').
                            text("Select Cost Type"));
            $('#costTypeID').val('select');
            $('#costTypeID').selectpicker('refresh');

            if (!costTypes[costTypeKey]) {
                $('.cost-type-table').removeClass('hidden');
                $('#costSampleRow').removeClass('hidden');
                $("#costTypeID").prop('disabled', true);
                $("#costSampleRow input[name='costTypeName']").val(COSTTYPELIST[costTypeKey]);
                $("#costSampleRow input[name='costTypeName']").data('costid', costTypeKey);
                $("#costSampleRow input[name='estimateCost']").focus();
            } else {
                p_notification(false, eb.getMessage('ERR_ALREDY_COST_TYPE'));
                $('#costTypeID').focus();
            }
        }
    });

    $(document).on('click', '.cost-type-plus', function() {
        var flag = false;
        var costTypeID = $(this).closest('#costSampleRow').find("input[name='costTypeName']").data('costid');
        var estimateCost = $(this).closest('#costSampleRow').find("input[name='estimateCost']").val();
        var actualCost = $(this).closest('#costSampleRow').find("input[name='actualCost']").val();

        if (!isNaN(estimateCost) || estimateCost < 0) {
            flag = true;
        }

        if (!isNaN(actualCost) || actualCost < 0) {
            flag == true;
        }

        if (flag == true) {
            estimateCost = (estimateCost == "") ? 0 : estimateCost;
            actualCost = (actualCost == "") ? 0 : actualCost;
            setCostTypeList(costTypeID, estimateCost, actualCost);
            $("#costTypeName,#estimateCost,#actualCost").val('');
            $(this).closest('#costSampleRow').find("input[name='costTypeName']").data('costid', '');
            costTypeKey = '';
        } else {
            p_notification(false, eb.getMessage('ERR_COST'));
        }

    });

    $(document).on('click', '.cost-type-delete', function() {

        var deleteCostTypeTrID = $(this).closest('tr').attr('id');
        var deleteCostTypeID = deleteCostTypeTrID.split('tr_')[1].trim();
        delete costTypes[deleteCostTypeID];
        $('#' + deleteCostTypeTrID).remove();

        if (_.values(costTypes).length == 0) {
            $('.cost-type-table').addClass('hidden');
        }
    });

    $('#activityForm').on('submit', function(e) {
        e.preventDefault();

        if (validateActivityForm()) {
            getDataForActivityWeight(jobReferenceID);
            $('#setActivityWeightModal').modal('show');
//            $('#activity-weight-update').addClass('hidden');
            $('#activity-weight-update').removeClass('hidden');
            $('#all-activity-weight-update').addClass('hidden');
            $('#save_weight').removeClass('hidden');
        }
    });

    $('#save_weight').on('click', function(e) {
        e.preventDefault();
        currentWeight = $('#cur-activity-weight').val();
        if (parseFloat(currentWeight) < 0) {
            p_notification(false, eb.getMessage('ERROR_JOB_WEIGHT_CANT_NEGATIVE'));
            $('#cur-activity-weight').focus();
            return false;
        }
        if (isNaN(parseFloat(currentWeight))) {
            p_notification(false, eb.getMessage('ERR_JOB_WEIGHT_DATA_TYPE'));
            $('#cur-activity-weight').focus();
            return false;
        }
        totalWeight = $('#total-weight').data('id');
        if (parseFloat(currentWeight) + parseFloat(totalWeight) > 100) {
            p_notification(false, eb.getMessage('ERROR_JOB_WEIGHT_OVER_LIMIT'));
            $('#cur-activity-weight').focus();
            $('#activity-weight-update').removeClass('hidden');
            return false;
        }

        saveActivity();
    });

    $('#activity-weight-update').on('click', function(e) {
        $('#add_aw').addClass('hidden');
        $('#update_aw').removeClass('hidden');
        $('#save_weight').addClass('hidden');
        $('#activity-weight-update').addClass('hidden');
        $('#all-activity-weight-update').removeClass('hidden');
        getDataForActivityWeight(jobReferenceID, true);
        $('#setActivityWeightModal').modal('show');
    });

    $('#all-activity-weight-update').on('click', function(e) {
        e.preventDefault();
        var checkUpdatedValue = 0;
        var tr_update = 1;
        var counter = 0;
        $('#addActivityWeight tbody tr').each(function() {
            var id = '#tr-activity-weight_' + tr_update;
            var idValue = '#activityweightUp_' + tr_update;
            var updatedActivityID = $(id).data('activityid');
            var updatedValue = $(idValue).val();
            if (parseFloat(updatedValue) <= 0) {
                p_notification(false, eb.getMessage('ERROR_JOB_WEIGHT_CANT_NEGATIVE'));
                $(idValue).focus();
                counter++;
                return false;
            }
            if (isNaN(parseFloat(updatedValue))) {
                p_notification(false, eb.getMessage('ERR_JOB_WEIGHT_DATA_TYPE'));
                $(idValue).focus();
                counter++;
                return false;
            }
            activityWeightId[tr_update] = updatedActivityID;
            activityWeightValue[tr_update] = updatedValue;
            checkUpdatedValue += parseFloat(updatedValue);
            tr_update++;
        });
        if (counter > 0) {
            return false;
        }
        checkUpdatedValue += parseFloat($('#cur-activity-weight').val());
        currentWeight = parseFloat($('#cur-activity-weight').val());
        if (parseFloat(currentWeight) <= 0) {
            p_notification(false, eb.getMessage('ERROR_JOB_WEIGHT_CANT_NEGATIVE'));
            $('#cur-activity-weight').focus();
            return false;
        }
        if (isNaN(parseFloat(currentWeight))) {
            p_notification(false, eb.getMessage('ERR_JOB_WEIGHT_DATA_TYPE'));
            $('#cur-activity-weight').focus();
            return false;
        }
        if (parseFloat(checkUpdatedValue) > 100) {
            p_notification(false, eb.getMessage('ERROR_JOB_WEIGHT_OVER_LIMIT'));
            return false;
        }
        saveActivity();
        updateWeight(activityWeightId, activityWeightValue, jobReferenceID);

    });

    $('#setActivityWeightModal .refresh').on('click', function(e) {
        e.preventDefault();
        $('.activity-weight-list').remove();
        $('.total-weight').remove();
        $('#cur-activity-weight').val('');
    });

    $(".reset").on('click', function(e) {
        resetSelectPicker('jobReferenceID', 'Job');
        resetSelectPicker('childjob', 'Job');
        resetSelectPicker('projectReferenceID', 'Project');
        resetSelectPicker('activityTypeID', 'Activity');
        resetSelectPicker('contractorID', 'Contractor');
        $(".addedContractor").remove();
        $(".contractor-table").addClass('hidden');
        contractors = {};
        contractorKey = '';
        resetSelectPicker('temporaryContractorID', 'Temporary Contractor');
        $(".addedTempContractor").remove();
        $(".temp-contractor-table").addClass('hidden');
        tempContractors = {};
        tempContractorKey = '';
        resetSelectPicker('activityOwnerID', 'Activity Owner');
        $(".addedActivityOwner").remove();
        $(".activity-owner-table").addClass('hidden');
        activityOwners = {};
        activityOwnerKey = '';
        resetSelectPicker('activitySupervisorID', 'Activity Supervisor');
        $(".addedActivitySupervisor").remove();
        $(".activity-supervisor-table").addClass('hidden');
        activitySupervisors = {};
        activitySupervisorKey = '';
        resetSelectPicker('rawMaterialID', 'Raw Material');
        $(".addedRawMaterialProduct").remove();
        $(".raw-product-table").addClass('hidden');
        rawMaterialProducts = {};
        rawMaterialKey = '';
        resetSelectPicker('fixedAssetsID', 'Fixed Assets');
        $(".addedFixedAssetsProduct").remove();
        $(".fixed-assests-product-table").addClass('hidden');
        fixedAssetsProducts = {};
        fixedAssetsKey = '';
        resetSelectPicker('temporaryProductID', 'Temporary Product');
        $(".addedTempProduct").remove();
        $(".temp-product-table").addClass('hidden');
        tempProducts = {};
        tempProductID = '';
        resetSelectPicker('costTypeID', 'Cost Type');
        $(".addedCostType").remove();
        $(".cost-type-table").addClass('hidden');
        costTypes = {};
        costTypeID = '';
        $(".addedVehicleCls").remove();
        $(".vehicle-table").addClass('hidden');
        $('#activityVehicle').hide();
        $('#vehicleBox').selectpicker('refresh');
        vehicles = {};
        vehicleId = '';
    });

    function updateWeight(weightIds, weightValues, jobID) {
        eb.ajax({
            url: BASE_URL + '/api/activity/updateWeight',
            method: 'post',
            data: {weightIds: weightIds, weightValues: weightValues, jobID: jobID},
            dataType: 'json',
            success: function(data) {
                p_notification(data.status, data.msg);
                if (data.status) {
                    p_notification(true, eb.getMessage('WEIGHT_UPDATE'));
                } else {

                }
            }
        });
    }

    function saveActivity() {
        var path = window.location.pathname.split('/');
        var activityFormData = {
            activityCode: $('#activityID').val(),
            activityName: $('#activityName').val(),
            jobReferenceID: jobReferenceID,
            projectReferenceID: projectID,
            activityTypeID: activityTypeID,
            contractorData: contractors,
            tempContractorData: tempContractors,
            activityDescription: $('#activityDescription').val(),
            activityOwnerData: activityOwners,
            activitySupervisorData: activitySupervisors,
            rawMaterialData: rawMaterialProducts,
            fixedAssetsData: fixedAssetsProducts,
            activityEstimateTime: $('#estimateTime').val(),
            startTime: $('#startTime').val(),
            endTime: $('#endTime').val(),
            estimateCost: $('#activityEstimateCost').val(),
            tempProductData: tempProducts,
            costTypeData: costTypes,
            currentWeight: currentWeight,
            isNewActivity: true,
            isUpdateActivity: false,
            inqID: $('#activityID').data('inquiryid'),
            vehicleData: vehicles,
            activityRepeatEnabled: ($('#activityRepeatEnabled').is(':checked')) ? '1' : '0',
            activityRepeatComment: $('#activityRepeatComment').val(),
        };

        eb.ajax({
            type: 'POST',
            url: BASE_URL + '/api/activity/save',
            data: activityFormData,
            success: function(respond) {
                p_notification(respond.status, respond.msg);
                if (respond.status == true) {
                    if (path.length == 4) {
                        var url = BASE_URL + '/job/list';
                        window.location.assign(url);
                    } else {
                        window.setTimeout(function() {
                            location.reload();
                        }, 1000);
                    }
                }
            },
            async: false
        });
    }

    function getDataForActivityWeight(jobID, update) {
        eb.ajax({url: BASE_URL + '/api/activity/getAllActivityWeight',
            method: 'post',
            data: {jobID: jobID},
            dataType: 'json',
            success: function(data) {
                if (!update) {
                    if (data.status == true) {
                        var aWeight = data.data['activityDataList'];
                        if (aWeight != null) {
                            $.each(aWeight, function(index, value) {
                                var activityName = ((value['activityName']) ? "(" + value['activityName'] + ")" : "");
                                var $newDiv = "<tr class='activity-weight-list' data-activityid='" + index + "'><td width='40%'>" + value['activityCode'] + " " + activityName + " " + "</td><td width='40%'>" + value['activityWeight'] + "</td></tr>";
                                $($('#addActivityWeight tbody')).append($newDiv);
                            });
                        }
                        $($('#addActivityWeight tbody')).append("<tr id='total-weight' class ='total-weight' data-id=" + data.data['activityWeightSum'] + "><td width='40%'><b>Total Weight</b></td><td width='40%'><b>" + data.data['activityWeightSum'] + "</b></td></tr>");
                    }
                } else {
                    if (data.status == true) {
                        var activityWeight = data.data['activityDataList'];
                        if (activityWeight != null) {
                            var activityweiUpdateAuto = 1;
                            $.each(activityWeight, function(index, value) {
                                var activityName = ((value['activityName']) ? "(" + value['activityName'] + ")" : "");
                                var $newDiv = "<tr id ='tr-activity-weight_" + activityweiUpdateAuto + "' class='activity-weight-list added-weight' data-activityid='" + index + "'><td width='30%'>" + value['activityCode'] + " " + activityName + " " + "</td><td width='30%'> <input type='text' class='new-value' id='activityweightUp_" + activityweiUpdateAuto + "'></td></tr>";
                                var id = '#activityweightUp_' + activityweiUpdateAuto;
                                activityweiUpdateAuto++;
                                $($('#addActivityWeight tbody')).append($newDiv);
                                $(id).val(value['activityWeight']);
                            });
                        }
                    }
                }
            }
        });
    }

    function validateActivityForm() {
        if (jobReferenceID == 0 || jobReferenceID == null) {
            p_notification(false, eb.getMessage('ERR_ENTER_JOB_REF'));
            $('#jobReferenceID').focus();
            return false;
        } else if ($('#activityTypeID').val() == 0 || $('#activityTypeID').val() == null) {
            p_notification(false, eb.getMessage('ERR_ENTER_ACTIVITY_TYPE'));
            $('#activityTypeID').focus();
        } else if ($('#activityName').val() == "" || $('#activityName').val() == null) {
            p_notification(false, eb.getMessage('ERR_ENTER_ACTIVITY_NAME'));
            $('#activityName').focus();
            return false;
        } else {
            return true;
        }

    }

    function validateTemporaryProductForm(formData) {
        var success = false;
        if (formData.tempProductCode == "" || formData.tempProductCode == null) {
            p_notification(false, eb.getMessage('ERR_QUOT_PRODCODE_BLANK'));
            $('#temporaryProductCode').focus();
            success = false;
        } else if (formData.tempProductName == "" || formData.tempProductName == null) {
            p_notification(false, eb.getMessage('ERR_QUOT_PRODNAME_BLANK'));
            $('#temporaryProductName').focus();
            success = false;
        } else if (isNaN(parseFloat(formData.tempQuantity)) || formData.tempQuantity <= 0) {
            p_notification(false, eb.getMessage('ERR_TEMP_VALQUAN'));
            $("#temporaryProductQuantity").focus();
            success = false;
        } else if (formData.tempUomID == 0) {
            p_notification(false, eb.getMessage('ERR_GRN_SELECT_UOM'));
            success = false;
        } else {
            success = true;
        }
        return success;
    }

    function activityOwnerDetails(activityOwnerID, activityOwnerFullName) {
        this.activityOwnerID = activityOwnerID;
        this.activityOwnerFullName = activityOwnerFullName;
    }

    function setActivityOwnerList(activityOwnerID) {
        var newTrID = 'tr_' + activityOwnerID;
        var clonedRow = $($('#activityOwnerPreSetSample').clone()).attr('id', newTrID).addClass('addedActivityOwner');
        $("#activityOwnerName", clonedRow).text(EMPLOYE_LIST[activityOwnerID]);
        clonedRow.removeClass('hidden');
        clonedRow.insertBefore('#activityOwnerPreSetSample');
        $('#activityOwnerID').focus();
        activityOwners[activityOwnerID] = new activityOwnerDetails(activityOwnerID, EMPLOYE_LIST[activityOwnerID]);
        $('#activityOwnerID').selectpicker('refresh');
        $('#activityOwnerID').focus();
    }

    function activitySupervisorDetails(activitySupervisorID, activitySupervisorFullName) {
        this.activitySupervisorID = activitySupervisorID;
        this.activitySupervisorFullName = activitySupervisorFullName;
    }

    function setActivitySupervisorList(activitySupervisorID) {
        var newTrID = 'tr_' + activitySupervisorID;
        var clonedRow = $($('#actSupPreSetSample').clone()).attr('id', newTrID).addClass('addedActivitySupervisor');
        $("#activitySupervisorName", clonedRow).text(EMPLOYE_LIST[activitySupervisorID]);
        clonedRow.removeClass('hidden');
        clonedRow.insertBefore('#actSupPreSetSample');
        $('#activitySupervisorID').focus();
        activitySupervisors[activitySupervisorID] = new activitySupervisorDetails(activitySupervisorID, EMPLOYE_LIST[activitySupervisorID]);
    }

    function rawMaterialProductDetails(rawMaterialProID, rawMaterialProNameCode, rawMaterialProTotalQty, rawMaterialProUnitPrice, subProducts) {
        this.rawMaterialProID = rawMaterialProID;
        this.rawMaterialProNameCode = rawMaterialProNameCode;
        this.rawMaterialProTotalQty = rawMaterialProTotalQty;
        this.rawMaterialProUnitPrice = rawMaterialProUnitPrice;
        this.rawMaterialSubProducts = subProducts;
    }

    function setRawMaterialProductList(rawMaterialProID, qtyTotal, rawMaterialSubProducts) {

        var newTrID = 'tr_' + rawMaterialProID;
        var clonedRow = $($('#rawProductPreSetSample').clone()).attr('id', newTrID).addClass('addedRawMaterialProduct');
        $("#rawProdctNameAndCode", clonedRow).text(RAWMATERIALPRODUCTLIST[rawMaterialProID]);
        $("input[name='addedQuantity']", clonedRow).val(qtyTotal).addUom(activeProductData[rawMaterialProID].uom);
        $("input.uomqty", clonedRow).attr('disabled', true);
        $("input[name='unitPrice']", clonedRow).val(parseFloat(activeProductData[rawMaterialProID].dSP).toFixed(2)).addUomPrice(activeProductData[rawMaterialProID].uom);
        clonedRow.removeClass('hidden');
        clonedRow.insertBefore('#rawProductPreSetSample');
        $("#rawMaterialID").attr('disabled', true);
        rawMaterialProducts[rawMaterialProID] = new rawMaterialProductDetails(rawMaterialProID, RAWMATERIALPRODUCTLIST[rawMaterialProID], qtyTotal, activeProductData[rawMaterialProID].dSP, rawMaterialSubProducts);
    }

    function selectProducts(selectedProductID) {
        // check if product is already selected
        var $currentRow = getAddRow();
        var availableQuantity = (activeProductData[selectedProductID].LPQ == null) ? 0 : activeProductData[selectedProductID].LPQ;
        ClearRowFields($currentRow);
        // remove existing subproducts
        $("#batch_data tr:not(.hidden.batch_row)").remove();
        $(".bacth-head,.serial-head,.batchCode,.serialID").removeClass('hidden');
        $('.batch-table').removeClass('hidden');
        if (availableQuantity > 0) {

            // check if any batch / serial products exist
            if ($.isEmptyObject(activeProductData[selectedProductID].batch) &&
                    $.isEmptyObject(activeProductData[selectedProductID].serial) &&
                    $.isEmptyObject(activeProductData[selectedProductID].batchSerial)) {

                var $clonedRow = $($('.batch_row').clone());
                $clonedRow
                        .data('id', 1)
                        .removeAttr('id')
                        .removeClass('hidden batch_row')
                        .addClass('remove_row');
                $("input[name='availableQuantity']", $clonedRow).val(availableQuantity).addUom(activeProductData[selectedProductID].uom);
                $("input[name='addQuantity']", $clonedRow).addUom(activeProductData[selectedProductID].uom);
                $("input[name='addQuantityCheck']", $clonedRow).remove();
                $clonedRow.insertBefore('.batch_row');
                $(".bacth-head,.serial-head,.batchCode,.serialID").addClass('hidden');
                $('#add-product-batch').modal('show');
                $('#add-product-batch').find('h4').text('Select Product Quantity');
            } else {
                addSerialProduct(selectedProductID);
                addBatchProduct(selectedProductID);
                addSerilaBatchProduct(selectedProductID);
            }
        } else {
            p_notification(false, eb.getMessage('ERR_AVA_QTY'));
        }

    }

    function ClearRowFields($currentRow) {

        $("input[name='addQuantity']", $currentRow).prop('readonly', false);
        $("input[name='unitPrice']", $currentRow).prop('disabled', false);
    }

    function addSerialProduct(selectedProduct) {

        if ((!$.isEmptyObject(activeProductData[selectedProduct].serial))) {

            productSerial = activeProductData[selectedProduct].serial;
            for (var i in productSerial) {
                var serialID = productSerial[i].PSID;
                var serialQty = (productSerial[i].PSS == 1) ? 0 : 1;
                var $clonedRow = $($('.batch_row').clone());
                $clonedRow
                        .data('id', i)
                        .removeAttr('id')
                        .removeClass('hidden batch_row')
                        .addClass('remove_row');
                $(".serialID", $clonedRow)
                        .html(productSerial[i].PSC)
                        .data('id', productSerial[i].PSID);
                $("input[name='availableQuantity']", $clonedRow).val(serialQty).addUom(activeProductData[selectedProduct].uom);
                $("input[name='addQuantity']", $clonedRow).parent().remove();
                //                if (transferSubProducts[selectedProduct] != undefined && transferSubProducts[selectedProduct]['S' + serialID] != undefined) {
//                    $("input[name='transferQuantityCheck']", $clonedRow).attr('checked', (transferSubProducts[selectedProduct]['S' + serialID].qtyByBase == 1));
//                }

                $clonedRow.insertBefore('.batch_row');
            }
            $('#add-product-batch').modal('show');
            $('#add-product-batch').find('h4').text('Select Batch / Serial Products');

        }
    }

    function addBatchProduct(selectedProductID) {

        if ((!$.isEmptyObject(activeProductData[selectedProductID].batch))) {

            batchProduct = activeProductData[selectedProductID].batch;
            for (var i in batchProduct) {
                if ((!$.isEmptyObject(activeProductData[selectedProductID].productIDs))) {
                    if (activeProductData[selectedProductID].productIDs[i]) {
                        continue;
                    }
                }

                if (batchProduct[i].PBQ <= 0) {
                    continue;
                }

                var batchID = batchProduct[i].PBID;
                var $clonedRow = $($('.batch_row').clone());
                $clonedRow
                        .data('id', i)
                        .removeAttr('id')
                        .removeClass('hidden batch_row')
                        .addClass('remove_row');
                $(".batchCode", $clonedRow).html(batchProduct[i].PBC).data('id', batchProduct[i].PBID);
                $("input[name='availableQuantity']", $clonedRow).val(batchProduct[i].PBQ).addUom(activeProductData[selectedProductID].uom);
                $("input[name='addQuantity']", $clonedRow).addUom(activeProductData[selectedProductID].uom);
                $("input[name='addQuantityCheck']", $clonedRow).remove();
                // if value was entered previously, populate field
                //                if (transferSubProducts[selectedProductID] != undefined && transferSubProducts[selectedProductID]['B' + batchID] != undefined) {
//                    $("input[name='transferQuantity']", $clonedRow).val(transferSubProducts[selectedProductID]['B' + batchID].qtyByBase);
//                }
                $clonedRow.insertBefore('.batch_row');
            }

            $('#add-product-batch').modal('show');
            $('#add-product-batch').find('h4').text('Select Batch / Serial Products');
        }
    }

    function addSerilaBatchProduct(selectedRawMaterialProductID) {
        if ((!$.isEmptyObject(activeProductData[selectedRawMaterialProductID].batchSerial))) {

            productBatchSerial = activeProductData[selectedRawMaterialProductID].batchSerial;
            for (var i in productBatchSerial) {
                var serialID = productBatchSerial[i].PSID;
                var batchID = productBatchSerial[i].PBID;
                var serialQty = (productBatchSerial[i].PSS == 1) ? 0 : 1;
                var $clonedRow = $($('.batch_row').clone());
                $clonedRow
                        .data('id', i)
                        .removeAttr('id')
                        .removeClass('hidden batch_row')
                        .addClass('remove_row');
                $(".serialID", $clonedRow)
                        .html(productBatchSerial[i].PSC)
                        .data('id', productBatchSerial[i].PSID);
                $(".batchCode", $clonedRow)
                        .html(productBatchSerial[i].PBC)
                        .data('id', productBatchSerial[i].PBID);
                $("input[name='availableQuantity']", $clonedRow).val(serialQty).addUom(activeProductData[selectedRawMaterialProductID].uom);
                $("input[name='addQuantity']", $clonedRow).parent().remove();
                //                if (transferSubProducts[selectedProduct] != undefined && transferSubProducts[selectedRawMaterialProductID]['B' + batchID + 'S' + serialID] != undefined) {
                //                    $("input[name='transferQuantityCheck']", $clonedRow).attr('checked', (transferSubProducts[selectedRawMaterialProductID]['B' + batchID + 'S' + serialID].qtyByBase == 1));
                //                }
                $clonedRow.insertBefore('.batch_row');
            }

            $('#add-product-batch').modal('show');
            $('#add-product-batch').find('h4').text('Select Batch / Serial Products');
        }
    }

    function resetTempProductModal() {
        $('#temporaryProduct')[0].reset();
        $('#uomID').val('');
        $('#uomID').selectpicker('refresh');
        batchProducts = {};
        serialProducts = {};
    }


    function fixedAssetsProductDetails(fixedAssetsProID, fixedAssetsProNameCode, fixedAssetsProTotalQty, fixedAssetsProUnitPrice, subProducts) {
        this.fixedAssetsProID = fixedAssetsProID;
        this.fixedAssetsProNameCode = fixedAssetsProNameCode;
        this.fixedAssetsProTotalQty = fixedAssetsProTotalQty;
        this.fixedAssetsProUnitPrice = fixedAssetsProUnitPrice;
        this.fixedAssetsSubProducts = subProducts;
    }

    function setFixedAssetsProductList(fixedAssetsProID, qtyTotal, fixedAssetsSubProducts) {

        var newTrID = 'tr_' + fixedAssetsProID;
        var clonedRow = $($('#fixedProductPreSetSample').clone()).attr('id', newTrID).addClass('addedFixedAssetsProduct');
        $("#fixedProdctNameAndCode", clonedRow).text(FIXEDASSETSPRODUCTLIST[fixedAssetsProID]);
        $("input[name='addedQuantity']", clonedRow).val(qtyTotal).addUom(activeProductData[fixedAssetsProID].uom);
        $("input.uomqty", clonedRow).attr('disabled', true);
        $("input[name='unitPrice']", clonedRow).val(activeProductData[fixedAssetsProID].dSP).addUomPrice(activeProductData[fixedAssetsProID].uom);
        clonedRow.removeClass('hidden');
        clonedRow.insertBefore('#fixedProductPreSetSample');
        $('#fixedAssetsID').attr('disabled', true);
        fixedAssetsProducts[fixedAssetsProID] = new fixedAssetsProductDetails(fixedAssetsProID, FIXEDASSETSPRODUCTLIST[fixedAssetsProID], qtyTotal, activeProductData[fixedAssetsProID].dSP, fixedAssetsSubProducts);
    }

    function batchProductDetails(batchCD, qty) {
        this.batchCD = batchCD;
        this.qty = qty;
    }

    function serialProductDetails(serialCD, batchCD, qty) {
        this.serialCD = serialCD;
        this.batchCD = batchCD;
        this.qty = qty;
    }

    function tempProductDetails(proID, proCD, proName, description, qty, price, uomID, isNewItem, batchData, serialData, temporaryProductImages) {
        this.proID = proID;
        this.proCD = proCD;
        this.proName = proName;
        this.description = description;
        this.qty = qty;
        this.price = price;
        this.uomID = uomID;
        this.isNewItem = isNewItem;
        this.batchData = batchData;
        this.serialData = serialData;
        this.temporaryProductImages = temporaryProductImages;
    }

    function setTempProductList(tempProductID, tempProductCode, tempProductName, tempQuantity, uomAbbr, tempProductBatch, tempProductSerial) {

        var newTrID = 'tr_' + tempProductID;
        var clonedRow = $($('#tempProductPreSetSample').clone()).attr('id', newTrID).addClass('addedTempProduct');
        $("#tempProdctNameAndCode", clonedRow).text(tempProductCode + '-' + tempProductName);
        if (!tempProductBatch) {
            tempProductBatch = "-";
        }
        if (!tempProductSerial) {
            tempProductSerial = "-";
        }
        $("#tempProdctBatch", clonedRow).text(tempProductBatch);
        $("#tempProdctSerial", clonedRow).text(tempProductSerial);
        $("#tempProdctQty", clonedRow).text(tempQuantity + ' ' + uomAbbr);
        clonedRow.removeClass('hidden');
        clonedRow.insertBefore('#tempProductPreSetSample');
        $('#temporaryProductID').focus();
    }
    function setCostTypeList(costTypeID, estimateCost, actualCost) {
        var newTrID = 'tr_' + costTypeID;
        var clonedRow = $($('#costTypePreSetSample').clone()).attr('id', newTrID).addClass('addedCostType');
        $("#costTypeName", clonedRow).text(COSTTYPELIST[costTypeID]);
        $("#estimateCost", clonedRow).html(parseFloat(estimateCost).toFixed(2));
        $("#actualCost", clonedRow).html(parseFloat(actualCost).toFixed(2));
        clonedRow.removeClass('hidden');
        clonedRow.insertBefore('#costTypePreSetSample');
        $("#costTypeID").attr('disabled', false);
        $('#costSampleRow').addClass('hidden');
        costTypes[costTypeID] = new costTypeDetails(costTypeID, estimateCost, actualCost);
    }

    function costTypeDetails(costTypeID, estimateCost, actualCost) {
        this.costTypeID = costTypeID;
        this.estimateCost = estimateCost;
        this.actualCost = actualCost;
    }

    function setDates(flag) {
        var durationTime = $('#estimateTime').val();
        var startingTime = $('#startTime').datetimepicker('getDate');
        var endingTime = $('#endTime').datetimepicker('getDate');
        if (startingTime.getTime() > endingTime.getTime()) {
            p_notification(false, eb.getMessage('ERR_PROJECT_STARTDAY_CNT_BE_MORETHAN_ENDINGDAY'));
            $('#endTime').datetimepicker('setDate', startingTime);
            endingTime = startingTime;
        }
        if ($('#startTime').val() && $('#endTime').val() && flag) {
            var timeDiff = Math.abs(endingTime.getTime() - startingTime.getTime());
            var minutes = Math.round(timeDiff / (1000 * 60));
            $('#estimateTime').val(minutes);
            $('#estimateTime').trigger('change');
        } else if (durationTime) {
            var less = 0;
            var days = parseInt(durationTime / (60 * 24));
            less = durationTime % (60 * 24);
            var hours = parseInt(less / 60);
            less = less % 60;
            var minutes = less;
            if ($('#startTime').val()) {
                startingTime.setDate(startingTime.getDate() + days);
                startingTime.setHours(startingTime.getHours() + hours);
                startingTime.setMinutes(startingTime.getMinutes() + minutes);
                $('#endTime').datetimepicker('setDate', startingTime);
            } else if ($('#endTime').val()) {
                endingTime.setDate(endingTime.getDate() - days);
                endingTime.setHours(endingTime.getHours() - hours);
                endingTime.setMinutes(endingTime.getMinutes() - minutes);
                $('#startTime').datetimepicker('setDate', endingTime);
            }
        }
    }

    function setSupervisorAndOwnerByJobID(jobReferenceID) {
        eb.ajax({
            type: 'POST',
            url: BASE_URL + '/job-api/getJobSupervisorAndJobOwnerByJobID',
            data: {jobID: jobReferenceID},
            success: function(respond) {
                if (respond.data[0]) {
                    $('.activity-supervisor-table').removeClass('hidden');
                }
                if (respond.data[1]) {
                    $('.activity-owner-table').removeClass('hidden');
                }
                for (i = 0; i < respond.data[3]; i++) {
                    setActivityOwnerList(respond.data[1][i]['id']);
                }
                for (i = 0; i < respond.data[2]; i++) {
                    setActivitySupervisorList(respond.data[0][i]['id']);
                }
            }
        });
    }

    function resetContractorModal() {
        $("#contractorFirstName").val('');
        $("#contractorSecondName").val('');
        $("#contractor #designationID").val('');
        $("#contractor #designationID").selectpicker('refresh');
        $("#contractor #divisionID").val('');
        $("#contractor #divisionID").selectpicker('refresh');
        $("#contractor #telNumber").val('');
    }

    function resetTempContractorModal() {
        $("#temporaryContractorFirstName").val('');
        $("#temporaryContractorSecondName").val('');
        $("#designation").val('');
        $("#division").val('');
        $("#temporaryContractor #telNumber").val('');
    }

    // when Add temporary product modal load
    $('#activityForm').on('click', '#addTempItemBtn', function() {
        temporaryProductImages = {};
        // clear images which already added
        $('#tempProductModal #files').empty();
    });

    $(document).on('click', '.temp-product-duplicate', function() {
        var deleteTempProTrCD = $(this).closest('tr').attr('id');
        var deleteTempProCD = deleteTempProTrCD.split('tr_')[1].trim();
        var dataArray = tempProducts[deleteTempProCD];
        // clear images which already added
        temporaryProductImages = {};
        $('#tempProductModal #files').empty();

        $('#addTempProductModal').modal('show');
        $('#temporaryProductName').val(dataArray['proName']);
        $('#temporaryProductDescription').val(dataArray['description']);
        $('#temporaryProductPrice').val(dataArray['price']);
        $('#uomID').val(dataArray['uomID']);
        $('#uomID').selectpicker('refresh');
        $('#batchProduct').val(dataArray['batchData']);
        $('#serialProduct').val(dataArray['serialData']);
    });

    function resetSelectPicker(referenceId, string) {
        $('#' + referenceId).val('').empty().selectpicker('refresh');
        $('#' + referenceId).append("<option value=''>" + "Select a " + string + "</option>");
        $('#' + referenceId).val('').trigger('change').selectpicker('refresh');
    }

    function setVehicleList(vehicleId, vehicalRegNum, startMileage, endMileage, distance, vehicleCost) {
        var newTrID = 'tr_' + vehicleId;
        var clonedRow = $($('#vehiclePreSetSample').clone()).attr('id', newTrID).addClass('addedVehicleCls');
        $("#vehicleRegNum", clonedRow).text(vehicalRegNum);
        $("#startMileage", clonedRow).html(parseFloat(startMileage).toFixed(2));
        $("#endMileage", clonedRow).html(parseFloat(endMileage).toFixed(2));
        $("#vehicleDistance", clonedRow).html(parseFloat(distance).toFixed(2));
        $("#vehicleCost", clonedRow).html(parseFloat(vehicleCost).toFixed(2));
        clonedRow.removeClass('hidden');
        clonedRow.insertBefore('#vehiclePreSetSample');
        $("#vehicleBox").attr('disabled', false);
        $('#vehicleSampleRow').addClass('hidden');
        vehicles[vehicleId] = new vehicleDetails(vehicleId, startMileage, endMileage, vehicleCost);
    }

    function vehicleDetails(vehicleId, startMileage, endMileage, vehicleCost) {
        this.vehicleId = vehicleId;
        this.activityVehicleStartMileage = startMileage;
        this.activityVehicleEndMileage = endMileage;
        this.activityVehicleCost = vehicleCost;
    }

//    $('.act-type-add').on('focusout', function() {
//        var typeID = $('#activityTypeID').data('frmactmdal');
//        eb.ajax({
//            type: 'POST',
//            url: BASE_URL + '/api/activity/get-activity-types',
//            data: {activityTypeID: typeID},
//            success: function(respond) {
//                if (respond.status) {
//                    $('#activityTypeID').
//                            append($("<option></option>").
//                                    attr("value", typeID).
//                                    text(respond.data));
//                    $('#activityTypeID').val(typeID);
//                    $('#activityTypeID').selectpicker('refresh');
//                    activityTypeID = typeID;
//
//                }
//            }
//        });
//    });

    //    if Repeat Activity checked allow to add comment for that
    $('#activityRepeatEnabled').on('change', function() {
        var isChecked = $('#activityRepeatEnabled').is(':checked');
        if (isChecked) {
            $('#activityRepeatComment').show();
        } else {
            $('#activityRepeatComment').val('');
            $('#activityRepeatComment').hide();
        }
    });

    $('#relatedDocs').on('click', function() {
        if (jobReferenceID) {
            setDataTorelatedDocsModal(jobReferenceID);
            $('#relatedDocumentModal').modal('show');
        } else
        {
            p_notification(false, eb.getMessage('ERR_RELETED_DOC'));
            return false;
        }
    });
    $('#related-document-view').on('click', '.document_view', function() {
        var documentId = $(this).parents('tr').attr('data-id');
        var documentType = $(this).parents('tr').attr('data-typeid');
        getPrintPreviewByDocumentTypeAndDocumentID(documentId, documentType);
        $('#documentJobViewModal').modal('show');

    });

});

function contractorDetails(contractorID, contractorFullName) {
    this.contractorID = contractorID;
    this.contractorFullName = contractorFullName;
}

function setAddedContractorToList(data) {
    //inserted contractor details put into CONTRACTORSLIST object
    CONTRACTORSLIST[data.contractorID] = data.contractorFirstName + '-' + data.contractorSecondName;
//    var contractorNewList = _.values(CONTRACTORSLIST);
    //reset typeahead
    //    var autocomplete = $('#contractorID').typeahead();
//    autocomplete.data('typeahead').source = contractorNewList;
    //    $('#contractorID').val('');
    setContractorsList(data.contractorID);
}

function setContractorsList(contractorID) {
    $('.contractor-table').removeClass('hidden');
    var newTrID = 'tr_' + contractorID;
    var clonedRow = $($('#contractorPreSetSample').clone()).attr('id', newTrID).addClass('addedContractor');
    $("#contractorName", clonedRow).text(CONTRACTORSLIST[contractorID]);
    clonedRow.removeClass('hidden');
    clonedRow.insertBefore('#contractorPreSetSample');
    contractors[contractorID] = new contractorDetails(contractorID, CONTRACTORSLIST[contractorID]);
    $('#contractorID').selectpicker('refresh');
    $('#contractorID').focus();
}

function tempContractorDetails(tempContractorID, tempContractorFullName) {
    this.tempContractorID = tempContractorID;
    this.tempContractorFullName = tempContractorFullName;
}
function setTempContractorsList(tempContractorID) {
    $('.temp-contractor-table').removeClass('hidden');
    var newTrID = 'tr_' + tempContractorID;
    var clonedRow = $($('#tempContractorPreSetSample').clone()).attr('id', newTrID).addClass('addedTempContractor');
    $("#tempContractorName", clonedRow).text(TEMPORARYCONTRACTORSLIST[tempContractorID]);
    clonedRow.removeClass('hidden');
    clonedRow.insertBefore('#tempContractorPreSetSample');
    $('#temporaryContractorID').focus();
    tempContractors[tempContractorID] = new tempContractorDetails(tempContractorID, TEMPORARYCONTRACTORSLIST[tempContractorID]);
    $('#temporaryContractorID').selectpicker('refresh');
    $('#temporaryContractorID').focus();
}

function setAddedTempContractorToList(data) {
    //inserted contractor details put into CONTRACTORSLIST object
    TEMPORARYCONTRACTORSLIST[data.temporaryContractorID] = data.temporaryContractorFirstName + '-' + data.temporaryContractorSecondName;
    //    var tempContractorsList = _.values(TEMPORARYCONTRACTORSLIST);
//    //reset typeahead
//    var autocomplete = $('#temporaryContractorID').typeahead();
//    autocomplete.data('typeahead').source = tempContractorsList;
//    $('#temporaryContractorID').val('');
    setTempContractorsList(data.temporaryContractorID);
}

function validateVehicleInput(startMileage, endMileage) {

    var regExCost = /^$|^[\d.]+$/;

    if (startMileage == null || startMileage == "") {
        p_notification(false, eb.getMessage('ERR_VHCL_START_MILEAGE_EMPTY'));
    }
    else if (!regExCost.test(startMileage)) {
        p_notification(false, eb.getMessage('ERR_VHCL_START_MILEAGE_TYPE'));
    }
    else if (startMileage < 0) {
        p_notification(false, eb.getMessage('ERR_VHCL_START_MILEAGE_NEGATIVE'));
    }
    else if (endMileage == null || endMileage == "") {
        p_notification(false, eb.getMessage('ERR_VHCL_END_MILEAGE_EMPTY'));
    }
    else if (!regExCost.test(endMileage)) {
        p_notification(false, eb.getMessage('ERR_VHCL_END_MILEAGE_TYPE'));
    }
    else if (endMileage < 0) {
        p_notification(false, eb.getMessage('ERR_VHCL_END_MILEAGE_NEGATIVE'));
    }
    else if (parseFloat(startMileage) > parseFloat(endMileage)) {
        p_notification(false, eb.getMessage('ERR_VHCL_MILEAGE_RANGE'));
    }
    else {
        return true;
    }
}

function setDataTorelatedDocsModal(jobID) {
    $('#docDeta tbody tr').remove();

    eb.ajax({
        type: 'POST',
        url: BASE_URL + '/api/activity/getAllRelatedDocumentsforActivity',
        data: {jobID: jobID},
        success: function(respond) {
            if (respond.status == true) {
                $.each(respond.data, function(index, value) {
                    if (value != null) {
                        $.each(value, function(index2, value) {
                            if (index == 'job') {
                                var dataBbody = "<tr data-id=" + value['jobID'] + "  data-typeID='job' ><td>" + index + "</td><td >" + value['code'] + "</td><td >" + value['name'] + "</td><td>" + value['createTime'] + "</td><td class='col-lg-3'><span class='glyphicon glyphicon-eye-open openlist link_color document_view '></span></td></tr>";
                                $('#docDeta tbody').append(dataBbody);
                            }
                            else if (index == 'activity') {
                                var dataBbody = "<tr data-id=" + value['activityID'] + " data-typeID='activity' ><td>" + index + "</td><td >" + value['code'] + "</td><td >" + value['name'] + "</td><td>" + value['createTime'] + "</td><td class='col-lg-3'><span class='glyphicon glyphicon-eye-open openlist link_color document_view '></span></td></tr>";
                                $('#docDeta tbody').append(dataBbody);
                            }
                            else if (index == 'inquiryLog') {
                                var dataBbody = "<tr data-id=" + value['inquiryLogID'] + "  data-typeID='inquiryLog' ><td>" + index + "</td><td >" + value['code'] + "</td><td >" + value['name'] + "</td><td>" + value['createTime'] + "</td><td class='col-lg-3'><span class='glyphicon glyphicon-eye-open openlist link_color document_view '></span></td></tr>";
                                $('#docDeta tbody').append(dataBbody);
                            }
                            else {
                                var dataBbody = "<tr><td>" + index + "</td><td >" + value['code'] + "</td><td >" + value['name'] + "</td><td>" + value['createTime'] + "</td><td class='col-lg-3'>-</td></tr>";
                                $('#docDeta tbody').append(dataBbody);
                            }
                        });
                    } else {
                        var dataBbody = "<tr><td></td><td ></td><td >No Related Details</td></tr>";
                        $('#docDeta tbody').append(dataBbody);
                    }
                });
            }
        }
    });
}

function getPrintPreviewByDocumentTypeAndDocumentID(documentID, documentType) {
    $('#job-document-view div').remove();
    if (documentType == 'job') {
        URL = BASE_URL + '/job/document/' + documentID;
    }
    else if (documentType == 'activity') {
        URL = BASE_URL + '/activity/view/' + documentID;
    }
    else if (documentType == 'inquiryLog') {
        URL = BASE_URL + '/inquiry-log/view/' + documentID;
    }

    eb.ajax({
        type: 'POST',
        url: URL,
        success: function(respond) {
            var division = "<div></div>";
            $('#job-document-view').append(division);
            if (documentType == 'job') {
                $('#job-document-view div').append(respond);
            } else {
                $('#job-document-view div').append(respond.html);

            }
        }
    });

}
