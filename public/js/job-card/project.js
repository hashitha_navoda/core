var projectTypeID = '';
var customerID = '';
var ownerID = '';
var supervisorID = '';
var divisionID = '';

$(function() {

    if ($('#projectCustomer').data('cid') && !$('#editMode').val()) {
        customerID = $('#projectCustomer').data('cid');
        $('.customer-profiles').removeClass('hidden');
        $('#projectCustomer').
                append($("<option></option>").
                        attr("value", customerID).
                        text(CUSTOMERS_NAMES[customerID]));
        $('#projectCustomer').val(customerID).prop('disabled', true);
        $('#projectCustomer').selectpicker('refresh');
    }

    $('#projectExtimatedTimeDuration').addDuration();
    //disabled estimate cost text fields
    $("input.days,input.hours,input.minutes").attr("disabled", "disabled");

    if ($('#editMode').val()) {
        $('.owners').removeClass('hidden');
        $('.supervisors').removeClass('hidden');
        $('.divisions').removeClass('hidden');
        $('.customer-profiles').removeClass('hidden');
        $('.add').text('Update Project');
        projectTypeID = $('#projectType').data('ptypeid');
        $('#projectType').
                append($("<option></option>").
                        attr("value", projectTypeID).
                        text(PROJECT_TYPE_LIST[projectTypeID]));
        $('#projectType').val(projectTypeID);
        $('#projectType').selectpicker('refresh');
        customerID = $('#projectCustomer').data('cid');
        $('#projectCustomer').
                append($("<option></option>").
                        attr("value", customerID).
                        text(CUSTOMERS_NAMES[customerID]));
        $('#projectCustomer').val(customerID);
        $('#projectCustomer').selectpicker('refresh');
    }

    loadDropDownFromDatabase('/project-type-api/searchProjectTypesForDropdown', "", 1, '#projectType', false, false, false, true);
    $('#projectType').selectpicker('refresh');
    $('#projectType').on('change', function() {
        projectTypeID = $(this).val();
        if (!projectTypeID) {
            $('#projectType').
                    append($("<option></option>").
                            attr("value", 'select').
                            text("Select Project Type"));
            $('#projectType').val('select');
            $('#projectType').selectpicker('refresh');
        }
    });

    loadDropDownFromDatabase('/employee_api/searchEmployeeForDropdown', "", 1, '#projectOwners');
    $('#projectOwners').selectpicker('refresh');
    $('#projectOwners').on('change', function() {
        if ($(this).val() > 0 && ($(this).val() != ownerID)) {
            ownerID = $(this).val();
            setMultipleOwnersDiv(ownerID);
            $('#projectOwners').empty().val('').trigger('change');
            return;
        }
        $('#projectOwners').
                append($("<option></option>").
                        attr("value", 'select').
                        text("Select Project Owners"));
        $('#projectOwners').val('select');
        $('#projectOwners').selectpicker('refresh');
    });

    loadDropDownFromDatabase('/employee_api/searchEmployeeForDropdown', "", 1, '#projectSupervisors');
    $('#projectSupervisors').selectpicker('refresh');
    $('#projectSupervisors').on('change', function() {
        if ($(this).val() > 0 && ($(this).val() != supervisorID)) {
            supervisorID = $(this).val();
            setMultipleSupervisorsDiv(supervisorID);
            $('#projectSupervisors').empty().val('').trigger('change');
            return;
        }
        $('#projectSupervisors').
                append($("<option></option>").
                        attr("value", 'select').
                        text("Select Project Supervisors"));
        $('#projectSupervisors').val('select');
        $('#projectSupervisors').selectpicker('refresh');

    });

    loadDropDownFromDatabase('/customerAPI/search-customers-for-dropdown', "", 0, '#projectCustomer');
    $('#projectCustomer').selectpicker('refresh');
    $('#projectCustomer').on('change', function() {
        if ($(this).val() > 0 && ($(this).val() != customerID)) {
            customerID = $(this).val();
            setCustomerProfiles(customerID);
        } else {
            $('#create-project-form .customer-profiles').addClass('hidden');
        }
    });

    loadDropDownFromDatabase('/division_api/searchDivisionsForDropdown', "", 0, '#projectDivisions', false, false, false, true);
    $('#projectDivisions').selectpicker('refresh');
    $('#projectDivisions').on('change', function() {
        if ($(this).val() > 0 && ($(this).val() != divisionID)) {
            divisionID = $(this).val();
            setMultipleDivisions(divisionID);
            $('#projectDivisions').empty().val('').trigger('change');
            return;
        }
        $('#projectDivisions').
                append($("<option></option>").
                        attr("value", 'select').
                        text("Select Project Divisions"));
        $('#projectDivisions').val('select');
        $('#projectDivisions').selectpicker('refresh');
    });

    function setCustomerProfiles(customerID) {
        eb.ajax({
            type: 'POST',
            url: BASE_URL + '/customerAPI/getCustomerProfilesByCustomerID',
            data: {customerID: customerID},
            success: function(respond) {
                if (respond.status) {
                    $('#create-project-form .customer-profiles tbody tr').remove();
                    $.each(respond.data['customerProfiles'], function(index, value) {
                        $('#create-project-form .customer-profiles').removeClass('hidden');
                        var $newDiv = "<tr class='customer-profile-list' data-cpid='" + index + "'><td>" + value + "</td><td class='text-center'><input type='radio' name='cProfile' class='cProfile'/></td></tr>";
                        $($('#create-project-form .customer-profiles tbody')).append($newDiv);
                    });
                }
            }
        });
    }

    function setMultipleOwnersDiv(key) {
        var flag = true;
        $('#create-project-form .owners').children('tbody').find('tr').each(function() {
            if ($(this).data('oid') == key) {
                p_notification(false, eb.getMessage('ERR_PROJECT_OWNER_ALREDY_ADDED', EMPLOYE_LIST[key]));
                flag = false;
                ownerID = '';
                return;
            }
        });
        if (flag) {
            //create project owner
            $('#create-project-form .owners').removeClass('hidden');
            var $newDiv = "<tr class='ownerlist' data-oid='" + key + "'><td>" + EMPLOYE_LIST[key] + "</td>\n\
                <td class='text-right'>\n\
                    <a class='btn btn-default delete-owners'>\n\
                        <i class=' pointer_cursor fa fa-trash-o'></i>\n\
                    </a>\n\
                </td>\n\
            </tr>";
            $($('#create-project-form .owners tbody')).append($newDiv);
            ownerID = '';
        }
    }

    function setMultipleSupervisorsDiv(key) {
        var flag = true;
        $('#create-project-form .supervisors').children('tbody').find('tr').each(function() {
            if ($(this).data('supid') == key) {
                p_notification(false, eb.getMessage('ERR_PROJECT_SUPERVISORS_ALREDY_ADDED', EMPLOYE_LIST[key]));
                flag = false;
                supervisorID = '';
                return;
            }
        });
        if (flag) {
            //project_supliers
            $('#create-project-form .supervisors').removeClass('hidden');
            var $newDiv = "<tr class='supervisorslist' data-supid='" + key + "'><td>" + EMPLOYE_LIST[key] + "</td>\n\
                <td class='text-right'>\n\
                    <a class='btn btn-default delete-supervisors'>\n\
                        <i class='pointer_cursor fa fa-trash-o'></i>\n\
                    </a>\n\
                </td>\n\
            </tr>";
            $($('#create-project-form .supervisors tbody')).append($newDiv);
            supervisorID = '';
        }
    }

    function setMultipleDivisions(key) {
        var flag = true;
        $('#create-project-form .divisions').children('tbody').find('tr').each(function() {
            if ($(this).data('divid') == key) {
                p_notification(false, eb.getMessage('ERR_PROJECT_DIVISIONS_ALREDY_ADDED', DIVISIONS[key]));
                divisionID = '';
                flag = false;
                return;
            }
        });
        if (flag) {
            //divition
            $('#create-project-form .divisions').removeClass('hidden');
            var $newDiv = "<tr class='divisions-list' data-divid='" + key + "'><td>" + DIVISIONS[key] + "</td>\n\
                <td class='text-right'>\n\
                    <a class='btn btn-default'>\n\
                        <i class='delete-division custom_font_size pointer_cursor fa fa-trash-o'></i>\n\
                    </a>\n\
                </td>\n\
            </tr>";
            $($('#create-project-form .divisions tbody')).append($newDiv);
            divisionID = '';
        }
    }

    $('#create-project-form .owners').on('click', '.delete-owners', function(e) {
        e.preventDefault();
        $(this).parents('.ownerlist').remove();
        if ($('#create-project-form .owners tbody tr').length == 0) {
            $('#create-project-form .owners').addClass('hidden');
        }
    });

    $('#create-project-form .supervisors').on('click', '.delete-supervisors', function(e) {
        e.preventDefault();
        $(this).parents('.supervisorslist').remove();
        if ($('#create-project-form .supervisors tbody tr').length == 0) {
            $('#create-project-form .supervisors').addClass('hidden');
        }
    });

    $('#create-project-form .divisions').on('click', '.delete-division', function(e) {
        e.preventDefault();
        $(this).parents('.divisions-list').remove();
        if ($('#create-project-form .divisions tbody tr').length == 0) {
            $('#create-project-form .divisions').addClass('hidden');
        }
    });

    $('#projectExtimatedTimeDuration').on('click', function(e) {
        e.preventDefault();
        setDates(false);
    });

    var startdateFormat = $('#projectStartingDate').data('date-format');
    $('#projectStartingDate').datetimepicker({
        format: startdateFormat + ' hh:ii',
    }).on('change', function(ev) {
        $('#projectStartingDate').datetimepicker('hide');
        setDates(true);
    });

    var enddateFormat = $('#projectEndingDate').data('date-format');
    $('#projectEndingDate').datetimepicker({
        format: enddateFormat + ' hh:ii',
    }).on('change', function(ev) {
        $('#projectEndingDate').datetimepicker('hide');
        setDates(true);
    });

    function setDates(falg) {
        var durationTime = $('#projectExtimatedTimeDuration').val();
        var startingTime = $('#projectStartingDate').datetimepicker('getDate');
        var endingTime = $('#projectEndingDate').datetimepicker('getDate');
        if (startingTime.getTime() > endingTime.getTime()) {
            p_notification(false, eb.getMessage('ERR_PROJECT_STARTDAY_CNT_BE_MORETHAN_ENDINGDAY'));
            $('#projectEndingDate').datetimepicker('setDate', startingTime);
            endingTime = startingTime;
        }
        if ($('#projectStartingDate').val() && $('#projectEndingDate').val() && falg) {
            var timeDiff = Math.abs(endingTime.getTime() - startingTime.getTime());
            var minutes = Math.round(timeDiff / (1000 * 60));
            $('#projectExtimatedTimeDuration').val(minutes);
            $('#projectExtimatedTimeDuration').trigger('change');
        } else if (durationTime) {
            var less = 0;
            var days = parseInt(durationTime / (60 * 24));
            less = durationTime % (60 * 24);
            var hours = parseInt(less / 60);
            less = less % 60;
            var minutes = less;
            if ($('#projectStartingDate').val()) {
                startingTime.setDate(startingTime.getDate() + days);
                startingTime.setHours(startingTime.getHours() + hours);
                startingTime.setMinutes(startingTime.getMinutes() + minutes);
                $('#projectEndingDate').datetimepicker('setDate', startingTime);
            } else if ($('#projectEndingDate').val()) {
                endingTime.setDate(endingTime.getDate() - days);
                endingTime.setHours(endingTime.getHours() - hours);
                endingTime.setMinutes(endingTime.getMinutes() - minutes);
                $('#projectStartingDate').datetimepicker('setDate', endingTime);
            }
        }
    }
    $('#create-project-form').on('submit', function(e) {
        e.preventDefault();
        var continueMode = false;
        addProject(continueMode);
    });

    $('.addAndContinue').on('click', function(e) {
        e.preventDefault();
        var continueMode = true;
        addProject(continueMode);
    });

    function addProject(continueMode) {
        var owners = new Array();
        var supervisors = new Array();
        var divisions = new Array();
        var customerProfileID = '';
        if ($('#create-project-form .owners tbody tr').length != 0) {
            $('#create-project-form .owners tbody tr').each(function() {
                var ownerid = $(this).data('oid');
                owners.push(ownerid);
            });
        }
        if ($('#create-project-form .supervisors tbody tr').length != 0) {
            $('#create-project-form .supervisors tbody tr').each(function() {
                var supervisorsid = $(this).data('supid');
                supervisors.push(supervisorsid);
            });
        }
        if ($('#create-project-form .customer-profiles tbody tr').length != 0) {
            if ($("input[name='cProfile']").is(':checked')) {
                customerProfileID = $("input[name='cProfile']:checked").parents('tr').data('cpid');
            }
        }
        if ($('#create-project-form .divisions tbody tr').length != 0) {
            $('#create-project-form .divisions tbody tr').each(function() {
                var divisionID = $(this).data('divid');
                divisions.push(divisionID);
            });
        }
        var formData = {
            projectCode: $('#projectCode').val(),
            projectName: $('#projectName').val(),
            projectTypeID: projectTypeID,
            projectDescription: $('#projectDescription').val(),
            owners: owners,
            supervisors: supervisors,
            customerID: customerID,
            customerProfileID: customerProfileID,
            durationMinutes: $('#projectExtimatedTimeDuration').val(),
            startingTime: $('#projectStartingDate').val(),
            endingTime: $('#projectEndingDate').val(),
            extimatedCost: $('#projectExtimatedCost').val(),
            divisions: divisions,
            inqID: $('#projectCode').data('inquiryid')
        }
        var url = '';
        if ($('#editMode').val()) {
            formData.projectID = $('#editMode').data('pid');
            url = BASE_URL + '/project-api/updateProject';
        } else {
            url = BASE_URL + '/project-api/saveProject';
        }

        if (formIsValid(formData)) {
            eb.ajax({
                type: 'POST',
                url: url,
                data: formData,
                success: function(respond) {
                    p_notification(respond.status, respond.msg);
                    if (respond.status) {
                        if (continueMode) {
                            window.location.assign(BASE_URL + '/job/create/' + respond.data);
                        } else if ($('#editMode').val()) {
                            window.location.assign(BASE_URL + '/project/create');
                        } else {
                            window.location.reload();
                        }
                    }
                },
            });
        }
    }
    ;

    function formIsValid(formData) {
        if (formData.projectCode == '') {
            p_notification(false, eb.getMessage('ERR_PROJECT_P_CODE_CNT_BE_BLANK'));
            $('#projectCode').focus();
            return false;
        } else if (formData.projectName == '') {
            p_notification(false, eb.getMessage('ERR_PROJECT_P_NAME_CNT_BE_BLANK'));
            $('#projectName').focus();
            return false;
        } else if (formData.projectTypeID == '') {
            p_notification(false, eb.getMessage('ERR_PROJECT_P_TYPE_CNT_BE_BLANK'));
            $('#projectType').focus();
            return false;
        } else if (formData.customerID == '') {
            p_notification(false, eb.getMessage('ERR_PROJECT_CUST_SBE_SELECT'));
            $('#projectCustomer').focus();
            return false;
        } else if (formData.customerProfileID == '') {
            p_notification(false, eb.getMessage('ERR_PROJECT_CUST_PRO_SBE_SELECT'));
            $('#projectCustomer').focus();
            return false;
        } else if (formData.extimatedCost < 0 || isNaN(formData.extimatedCost)) {
            p_notification(false, eb.getMessage('ERR_PROJECT_COST_SHOULDBE_POSI_AND_NUMERIC'));
            $('#projectExtimatedCost').focus();
            return false;
        }
        return true;
    }

    $('.reset').on('click', function(e) {
        e.preventDefault();
        $('#projectName').val('');
        $('#projectType').val('');
        projectTypeID = '';
        $('#projectDescription').val('');
        $('.owners tr').remove();
        $('.owners').addClass('hidden');
        $('.supervisors tr').remove();
        $('.supervisors').addClass('hidden');
        $('.customer-profiles tbody tr').remove();
        $('.customer-profiles').addClass('hidden');
        customerID = '';
        $('#projectCustomer').val('');
        $('#projectStartingDate').val('');
        $('#projectEndingDate').val('');
        $('#projectExtimatedTimeDuration').val('').trigger('change');
        $('#projectExtimatedCost').val('');
        $('.divisions tr').remove();
        $('.divisions').addClass('hidden');

    });

    $('.back').on('click', function() {
        window.history.back();
    });
});