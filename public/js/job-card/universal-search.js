$(function() {

    $('#universalSeaech').on('submit', function(e) {
        e.preventDefault();
        eb.ajax({
            type: 'POST',
            url: BASE_URL + '/universal-search/searchProjectsJobsAndActivitiesBySearchKey',
            data: {searchKey: $('#searchKey').val()},
            success: function(respond) {
                if (respond.status) {
                    var data = respond.data.list;
                    var id = '';
                    var typePrefix = '';
                    $('#unTable').parent('div').removeClass('hidden');
                    var $tbody = $('#unTable').find('tbody');
                    $tbody.find('tr').remove();
                    var $newRow = '';
                    if (!$.isEmptyObject(data)) {
                        $.each(data, function(index, value) {
                            var typearray = index.split('_');
                            typePrefix = typearray[0];
                            id = typearray[1];
                            if (typePrefix == 'pro') {
                                $newRow = "<tr data-id=" + index + ">\n\
<td>" + value.projectCode + "</td>\n\
<td>" + value.projectName + "</td>\n\
<td>Project</td>\n\
<td>" + value.projectProgress + " %</td>\n\
<td class='text-center'>\n\
<span class='link_color' style='cursor:pointer'>\n\
<span id='23' class='glyphicon glyphicon-eye-open view'></span>\n\
</span>\n\
</td>\n\
</tr>";
                            } else if (typePrefix == 'job') {
                                $newRow = "<tr data-id=" + index + ">\n\
<td>" + value.jobReferenceNumber + "</td>\n\
<td>" + value.jobName + "</td>\n\
<td>Job</td>\n\
<td>" + value.jobProgress + " %</td>\n\
<td class='text-center'>\n\
<span class='link_color' style='cursor:pointer'>\n\
<span id='23' class='glyphicon glyphicon-eye-open view'></span>\n\
</span>\n\
</td>\n\
</tr>";

                            } else if (typePrefix == 'act') {
                                $newRow = "<tr data-id=" + index + ">\n\
<td>" + value.activityCode + "</td>\n\
<td>" + value.activityName + "</td>\n\
<td>Activity</td>\n\
<td>" + value.activityProgress + " %</td>\n\
<td class='text-center'>\n\
<span class='link_color' style='cursor:pointer'>\n\
<span id='23' class='glyphicon glyphicon-eye-open view'></span>\n\
</span>\n\
</td>\n\
</tr>";
                            } else if (typePrefix == 'inq') {
                                $newRow = "<tr data-id=" + index + ">\n\
<td>" + value.inquiryLogReference + "</td>\n\
<td>" + value.inquiryLogDescription + "</td>\n\
<td>Inquiery Log</td>\n\
<td> - </td>\n\
<td class='text-center'>\n\
<span class='link_color' style='cursor:pointer'>\n\
<span id='23' class='glyphicon glyphicon-eye-open view'></span>\n\
</span>\n\
</td>\n\
</tr>";
                            }
                            $tbody.append($newRow);
                        });
                    } else {
                        var $newRow = "<tr class='not_found'><td colspan='10'>No Results were found</td></tr>";
                        $tbody.append($newRow);
                    }
                }
            },
        });
    });

    $('tbody').on('click', '.view', function(e) {
        e.preventDefault();
        var prefix = $(this).parents('tr').data('id');
        var url = BASE_URL + '/universal-search/viewSpaceTree/' + prefix;
        window.location.assign(url);
    });
});