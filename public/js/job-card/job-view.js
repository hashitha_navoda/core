$(document).ready(function() {

    $('#jobViewBack').on('click', function() {
        window.history.back();
    });

    $('#createActivity').on('click', function() {
        var url = BASE_URL + '/activity/create/' + $('#jobid').val();
        window.location.assign(url);
    });

    $('#listActivity').on('click', function() {
        var url = BASE_URL + '/activity/list/1/' + $('#jobid').val();
        window.location.assign(url);
    });

    $('#editJob').on('click', function() {
        var url = BASE_URL + '/job/edit/' + $('#jobid').val();
        window.location.assign(url);
    });

    $(document).on('click', "a.editsInactive", function(e) {
        p_notification(false, eb.getMessage('ERR_CUST_INACTIVE'));
    });


});


