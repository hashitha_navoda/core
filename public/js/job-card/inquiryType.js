/**
 * @author Sandun <sandun@thinkcube.com>
 * There have inquiry type related functions
 */
var inquiryTypeID;
var editMode = false;

$(document).ready(function() {
    $('#create-inquiry-type-form').on('submit', function(e) {
        e.preventDefault();
        var formData = {
            inquiryTypeCode: $('#inquiryTypeCode').val(),
            inquiryTypeName: $('#inquiryTypeName').val(),
            editMode: editMode,
            inquiryTypeID: inquiryTypeID,
        };
        if (validateFormData(formData)) {
            eb.ajax({
                url: BASE_URL + '/api/inquiry-type/add',
                method: 'post',
                data: formData,
                dataType: 'json',
                success: function(data) {
                    p_notification(data.status, data.msg);
                    if (data.status) {
                        window.setTimeout(function() {
                            location.reload();
                        }, 1000);
                    } else {
                        if (data.data == "PTCERR") {
                            $('#inquiryTypeCode').focus();
                        }
                    }
                }
            });
        }
    });

    $('#inquiry-type-search').on('submit', function(e) {
        e.preventDefault();
        var formData = {
            inquiryTypeSearchKey: $('#inquiry-type-search-keyword').val(),
        };
        if (formData.inquiryTypeSearchKey != '') {
            searchValue(formData);
        } else {
            p_notification(false, eb.getMessage('ERR_INQUTYPE_SEARCH_KEY'));
        }
    });

    $('#inquiry-type-search').on('click', '.reset', function(e) {
        e.preventDefault();
        searchValue('');
        $('#inquiry-type-search-keyword').val('');
    });

    $('#inquiry-type-list').on('click', 'a.edit', function(e) {
        e.preventDefault();
        inquiryTypeID = $(this).parents('tr').data('inquirytypeid');
        $('#inquiryTypeCode').val($(this).parents('tr').find('.inquiry-type-code').text()).attr('disabled', true);
        $('#inquiryTypeName').val($(this).parents('tr').find('.inquiry-type-name').text());
        $('.updateTitle').removeClass('hidden');
        $('.addTitle').addClass('hidden');
        $('.updateDiv').removeClass('hidden');
        $('.addDiv').addClass('hidden');
        editMode = true;
    });

    $('#inquiry-type-list').on('click', 'a.status', function() {
        var msg;
        var status;
        var inquiryTypeID = $(this).parents('tr').data('inquirytypeid');
        var inquiryTypeFlag = true;
        var currentDiv = $(this).contents();
        if ($(this).children().hasClass('fa-check-square-o')) {
            msg = 'Are you sure you want to Deactivate this inquiry type';
            status = '0';
        }
        else if ($(this).children().hasClass('fa-square-o')) {
            msg = 'Are you sure you want to Activate this inquiry type';
            status = '1';
        }
        activeInactive(inquiryTypeID, status, currentDiv, msg, inquiryTypeFlag);
    });

    $('.cancel').on('click', function(e) {
        e.preventDefault();
        projectTypeID = '';
        $('#inquiryTypeCode').val('').attr('disabled', false);
        $('#inquiryTypeName').val('');
        $('.updateTitle').addClass('hidden');
        $('.addTitle').removeClass('hidden');
        $('.updateDiv').addClass('hidden');
        $('.addDiv').removeClass('hidden');
        editMode = false;
    });

    $('#inquiry-type-list').on('click', 'a.delete', function(e) {
        e.preventDefault();
        $('#inquiryTypeCode').val('').attr('disabled', false);
        $('#inquiryTypeName').val('');
        $('.updateTitle').addClass('hidden');
        $('.addTitle').removeClass('hidden');
        $('.updateDiv').addClass('hidden');
        $('.addDiv').removeClass('hidden');
        editMode = false;

        var inquiryTypeID = $(this).parents('tr').data('inquirytypeid');
        var inquiryTypeCode = $(this).parents('tr').find('.inquiry-type-code').text();
        var currentDiv = $(this).parents('tr').find('a.status').contents();
        var inquiryTypeFlag = false;
        var msg = eb.getMessage('INQ_TYPE_DELETE_AND_STATUS');
        bootbox.confirm('Are you sure you want to delete this Inquiry Type?', function(result) {
            if (result === true) {
                eb.ajax({
                    url: BASE_URL + '/api/inquiry-type/deleteInquiryTypeByInquiryTypeID',
                    method: 'post',
                    data: {
                        inquiryTypeID: inquiryTypeID,
                        inquiryTypeCode: inquiryTypeCode,
                    },
                    dataType: 'json',
                    success: function(data) {
                        if (data.status == true) {
                            p_notification(data.status, data.msg);
                            window.location.reload();
                        } else if (data.status == false && data.data == 'deactive') {
                            activeInactive(inquiryTypeID, '0', currentDiv, msg, inquiryTypeFlag);
                        } else {
                            p_notification(data.status, data.msg);
                        }
                    }
                });
            }
        });

    });

    function validateFormData(formData) {
        if (formData.inquiryTypeCode == "") {
            p_notification(false, eb.getMessage('ERR_INQUTYPE_CODE_CNT_BE_NULL'));
            $('#inquiryTypeCode').focus();
            return false;
        } else if (formData.inquiryTypeName == '') {
            p_notification(false, eb.getMessage('ERR_INQUTYPE_NAME_CNT_BE_NULL'));
            $('#inquiryTypeName').focus();
            return false;
        }
        return true;
    }

    function searchValue(formData) {
        eb.ajax({
            url: BASE_URL + '/api/inquiry-type/getInquiryTypesBySearchKey',
            method: 'post',
            data: formData,
            dataType: 'json',
            success: function(data) {
                if (data.status) {
                    $("#inquiry-type-list").html(data.html);
                } else {
                    p_notification(data.status, data.msg);
                }
            }
        });
    }
    /**
     * use to change active status
     *
     */
    function activeInactive(type, status, currentDiv, msg, inquiryTypeFlag) {
        bootbox.confirm(msg, function($result) {
            if ($result == true) {
                eb.ajax({
                    url: BASE_URL + '/api/inquiry-type/changeStatusID',
                    method: 'post',
                    dataType: 'json',
                    data: {
                        'inquiryTypeID': type,
                        'status': status,
                    },
                    success: function(data) {
                        if (data.status) {
                            p_notification(data.status, data.msg);
                            if (currentDiv.hasClass('fa-square-o') && inquiryTypeFlag) {
                                currentDiv.addClass('fa-check-square-o');
                                currentDiv.removeClass('fa-square-o');
                            }
                            else {
                                currentDiv.addClass('fa-square-o');
                                currentDiv.removeClass('fa-check-square-o');
                            }
                        } else {
                            p_notification(data.status, data.msg);
                        }
                    }
                });
            }
        });
    }
});