$(document).ready(function() {
    var inquirySetupFromData = {};
    $('#inquirySetupForm').on('submit', function(e) {
        e.preventDefault();
        bootbox.confirm("Are you sure you want to Update Inquiry Setup?", function(result) {
            if (result === true) {
                inquirySetupFromData[$('#createCheckBox').data('id')] = (createCheckBox.checked) ? '1' : '0';
                inquirySetupFromData[$('#updateCheckBox').data('id')] = (updateCheckBox.checked) ? '1' : '0';
                inquirySetupFromData[$('#deleteCheckBox').data('id')] = (deleteCheckBox.checked) ? '1' : '0';

                eb.ajax({
                    url: BASE_URL + '/inquiry-setup/update',
                    method: 'post',
                    data: inquirySetupFromData,
                    dataType: 'json',
                    success: function(data) {
                        p_notification(data.status, data.msg);
                        if (!data.status) {
                            location.reload();
                        }
                    }
                });
            }
        });
    });
});