$(function() {
    var designationFlag = false;
    var designationID;
    $('#designation-list').on('click', 'a.edit', function(e) {
        e.preventDefault();
        $('.add').addClass('hidden');
        $('.update').removeClass('hidden');
        designationID = $(this).parents('tr').data('designationid');
        $('#designationName').val($(this).parents('tr').find('.desigName').text());
        designationFlag = true;
    });

    $('#create-designation-form').on('submit', function(e) {
        e.preventDefault();
        if (designationFlag) {
            update();
        }
        else {
            add();
        }
    });
    $('#designation-list').on('click', 'a.status', function() {
        var msg;
        var status;
        var designationID = $(this).parents('tr').data('designationid');
        var currentDiv = $(this).contents();
        var flag = true;
        if ($(this).children().hasClass('fa-check-square-o')) {
            msg = 'Are you sure you want to Deactivate this designation';
            status = '0';
        }
        else if ($(this).children().hasClass('fa-square-o')) {
            msg = 'Are you sure you want to Activate this designation';
            status = '1';
        }
        activeInactive(designationID, status, currentDiv, msg, flag);
    });

    $('#designation-list').on('click', 'a.delete', function(e) {
        e.preventDefault();
        designationID = $(this).parents('tr').data('designationid');
        var currentDiv = $(this).parents('tr').find('a.status').contents();
        var flag = false;
        var msg = 'This designation cannot be deleted. Do you want to de-activate this designation';
        var formData = {
            designationID: designationID,
        };
        bootbox.confirm('Are you sure you want to delete this Destination?', function(result) {
            if (result === true) {
                eb.ajax({
                    url: BASE_URL + '/designation_api/delete',
                    method: 'post',
                    data: formData,
                    dataType: 'json',
                    success: function(data) {
                        if (data.status == true) {
                            p_notification(data.status, data.msg);
                            window.location.reload();
                        }
                        else if (data.status == false && data.data == 'deactive') {
                            activeInactive(designationID, '0', currentDiv, msg, flag);
                        }
                        else {
                            p_notification(data.status, data.msg);
                        }
                    }
                });
            }
        });
    });
    $('#designation-search').on('click', '#des-search', function(e) {
        e.preventDefault();
        searchKey = $('#designation-search-keyword').val();
        var formData = {
            searchKey: searchKey,
        };
        if (formData.searchKey) {
            searchValue(formData);
        } else {
            p_notification(false, eb.getMessage('ERR_NO_KEY_WORD'));
        }

    });

    $('#designation-search').on('click', '.reset', function(e) {
        e.preventDefault()
        searchValue('');
        $('#designation-search-keyword').val('');
    });


    function validateFormData(formData) {

        if (formData.designation == '') {
            p_notification(false, eb.getMessage('ERR_DESIGNATION_NAME_CNT_BE_NULL'));
            $('#designationName').focus();
            return false;
        }
        return true;
    }

    function searchValue(formData) {
        eb.ajax({
            url: BASE_URL + '/designation_api/getDesignationBySearchKey',
            method: 'post',
            data: formData,
            dataType: 'json',
            success: function(data) {
                if (data.status == true) {
                    if (data.status) {
                        $("#designation-list").html(data.html);
                    } else {
                        p_notification(data.status, data.msg);
                    }
                }
            }
        });
    }
//this function use to Send data to the add action in to the designation api controller
    function add() {
        var formData = {
            designation: $('#designationName').val(),
            designationID: $('#designationID').val(),
        };
        if (validateFormData(formData)) {
            eb.ajax({
                url: BASE_URL + '/designation_api/add',
                method: 'post',
                data: formData,
                dataType: 'json',
                success: function(data) {
                    p_notification(data.status, data.msg);
                    if (data.status) {
                        window.setTimeout(function() {
                            location.reload();
                        }, 1000);
                    } else {
                        if (data.data == "DESIGNATION_EXIST") {
                            $('#designationName').focus();
                        }
                    }
                }
            });
        }
    }
    //this function use to Send data to the update action in to the designation api controller
    function update() {
        var formData = {
            designation: $('#designationName').val(),
            designationID: designationID,
        };
        if (validateFormData(formData)) {
            eb.ajax({
                url: BASE_URL + '/designation_api/update',
                method: 'post',
                data: formData,
                dataType: 'json',
                success: function(data) {
                    p_notification(data.status, data.msg);
                    window.setTimeout(function() {
                        location.reload();
                    }, 1000);
                }
            });
        }

    }
    /**
     *
     *use to change active state
     */
    function activeInactive(type, status, currentDiv, msg, flag) {
        bootbox.confirm(msg, function($result) {
            if ($result == true) {
                eb.ajax({
                    url: BASE_URL + '/designation_api/changeStatusID',
                    method: 'post',
                    dataType: 'json',
                    data: {
                        'designationID': type,
                        'status': status,
                    },
                    success: function(data) {
                        if (data.status) {
                            p_notification(data.status, data.msg);
                            if (currentDiv.hasClass('fa-check-square-o') && flag) {
                                currentDiv.removeClass('fa-check-square-o');
                                currentDiv.addClass('fa-square-o');
                            }
                            else {
                                currentDiv.removeClass('fa-square-o');
                                currentDiv.addClass('fa-check-square-o');
                            }
                        } else {
                            p_notification(data.status, data.msg);
                        }
                    }
                });
            }
        });
    }

});


