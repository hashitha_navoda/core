$(document).ready(function () {

    var selectedItemId;
    var selectedItemType;

    $('.employee-item').draggable({revert: true});
    $('#statusBox').selectpicker('hide');
    $('.status_msg').addClass('hide');

    //get project / job details
    $('.dispatcher-item').click(function () {

        //reset employees
        $('.employee-item').animate({top: "0px", left: "0px"}).removeClass('hide');

        //get dropped element information
        selectedItemId = $(this).data('item-id');
        selectedItemType = $(this).data('item-type');

        eb.ajax({
            type: 'POST',
            url: BASE_URL + '/api/job-dispatch/get-dispatcher',
            data: {
                selectedItemId: selectedItemId,
                selectedItemType: selectedItemType
            },
            success: function (respond) {
                if (respond.status) {

                    $('#dispatcher-body').html(respond.html);
                    $('#statusBox').selectpicker('show');
                    $('.status_msg').removeClass('hide');

                    var status;
                    if (selectedItemType == 'project') {
                        $("#dispatcher-head span").text('Project');
                        status = respond.data.projectStatus;
                    } else {
                        $("#dispatcher-head span").text('Job');
                        status = respond.data.jobStatus;
                    }
                    $('#statusBox').selectpicker('val', status);
                    assignEmployee();


                    $('[data-toggle="tooltip"]').tooltip({
                        animation: true,
                        container: 'body'
                    });
                }
            }
        });

    });

    //for delete activity owner
    $('#dispatcher-body').on('click', '.activityOwnerDeleteBtn', function () {
        var $selectedEmployeeBtn = $(this);
        var selectedEmpId = $selectedEmployeeBtn.data('owner-id');
        var taskId = $selectedEmployeeBtn.closest('.employee-div').data('activity-id');

        eb.ajax({
            type: 'POST',
            url: BASE_URL + '/api/job-dispatch/update-employees',
            data: {
                employeeId: selectedEmpId,
                taskId: taskId,
                taskType: 'activity',
                action: 'remove'
            },
            success: function (respond) {
                if (respond.status) {
                    var $empContainer = $selectedEmployeeBtn.parents('.assigned-employee-div');
                    $selectedEmployeeBtn.closest('.activity-owner').remove();

                    var $noEmpsMsg = $empContainer.find('.no-emps');
                    if ($empContainer.find('.activity-owner').length == 0) {
                        $noEmpsMsg.removeClass('hidden');
                    } else {
                        $noEmpsMsg.addClass('hidden');
                    }
                }
                p_notification(respond.status, respond.msg);
            }
        });
    });

    $(document).on('click', "a.editsInactive", function(e) {
        p_notification(false, eb.getMessage('ERR_CUST_INACTIVE'));
    });
    //show popover
    $('#dispatcher-body').on('mouseenter', '.tag', function () {
        $('[data-toggle="popover"]').popover({
            trigger: 'hover',
            placement: 'right',
            html: true,
            container: 'body'
        });
    });

    //change status
    $('#statusBox').on('change', function () {
        var obj = {};
        var url = null;

        if (selectedItemType == 'project') {
            obj.projectId = selectedItemId;
            obj.projectStatus = $(this).val();
            url = BASE_URL + '/project-api/change-project-status';
        } else {
            obj.jobId = selectedItemId;
            obj.jobStatus = $(this).val();
            url = BASE_URL + '/job-api/change-job-status';
        }

        eb.ajax({
            url: url,
            method: 'post',
            data: obj,
            dataType: 'json',
            success: function (response) {
                p_notification(response.status, response.msg);
            }
        });
    });

    //assign employee
    function assignEmployee() {
        $('.assign-employee-div').droppable({
            activeClass: "ui-state-default",
            hoverClass: "ui-state-hover",
            drop: function (e, ui) {
                var selectedEmpId = ui.draggable.data('employee-id');
                var taskId = $(this).data('activity-id');
                var taskType = $(this).data('type');
                var dropEmployee = $('a[data-employee-id="' + selectedEmpId + '"]');

                eb.ajax({
                    type: 'POST',
                    url: BASE_URL + '/api/job-dispatch/update-employees',
                    data: {
                        employeeId: selectedEmpId,
                        taskId: taskId,
                        taskType: taskType,
                        action: 'add'
                    },
                    success: function (respond) {
                        if (respond.status) {
                            var html = generateAssignedEmployeeHTML(respond.data);
                            $('.employee-div[data-activity-id="' + taskId + '"]').find('.assigned-employee-div').append(html);
                            $('.employee-div[data-activity-id="' + taskId + '"]').find('.no-emps').addClass('hidden');
                        }
                        p_notification(respond.status, respond.msg);
                    }
                });
            }
        });
    }

    //generate assign employee HTML
    function generateAssignedEmployeeHTML(employee) {
        var html = '';
        html += '<div class="activity-owner">\n\
                <span>\n\
                ' + employee.employeeName
                + '<label class="activityOwnerDeleteBtn fa fa-times" data-owner-id="' + employee.employeeId + '"></label>\n\
                </span>\n\
                </div>';
        return html;
    }

});