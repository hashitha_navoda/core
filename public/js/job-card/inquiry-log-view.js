$(document).ready(function() {
    $('#viewBack').on('click', function() {
        window.history.back();
    });

    $('#btn_link_docs').on('click', function() {
        var inqID = $(this).data('innqid');
        setDataToRelatedDocumentViewModal(inqID);
        $('#cus_histry_view').addClass('hidden');
        $('#relat_doc_view').removeClass('hidden');
        $('#addUserHistoryModal').modal('show');

    });

    function setDataToRelatedDocumentViewModal(documentID) {
        $('#cus-history-table tbody tr').remove();
        $('#cus-history-table tfoot div').remove();
        $('#cus-history-table tbody div').remove();

        eb.ajax({
            type: 'POST',
            url: BASE_URL + '/inquiry-log-api/getAllDocumentDetailsByInqID',
            data: {documentID: documentID},
            success: function(respond) {
                if (respond.status == true) {
                    $('#cus-history-table thead tr').removeClass('hidden');
                    $.each(respond.data, function(index, value) {
                        if (value != null) {
                            $.each(value, function(index2, value2) {
                                var tableBody;
                                if (value2['type'] == 'project') {
                                    tableBody = "<tr data-docId=" + value2['documentID'] + " data-doctype=" + value2['type'] + "><td class='' >" + value2['type'] + "</td><td>" + value2['code'] + "</td><td>" + value2['amount'] + "</td><td>" + value2['issuedDate'] + "</td><td></td></tr>";
                                }
                                else {
                                    tableBody = "<tr data-docId=" + value2['documentID'] + " data-doctype=" + value2['type'] + "><td class='' >" + value2['type'] + "</td><td>" + value2['code'] + "</td><td>" + value2['amount'] + "</td><td>" + value2['issuedDate'] + "</td><td class='text-center'><span class='glyphicon glyphicon-eye-open openlist link_color document_view ' style='cursor:pointer'></span></td></tr>";
                                }
                                $('#cus-history-table tbody').append(tableBody);
                            });
                        }
                    });
                    var footer = " <div class='col-lg-12 col-md-12 col-sm-12 text-right'><button type='button' class='btn btn-primary col-lg-12 col-md-12 col-sm-12' data-dismiss='modal' aril-hidden='true'>Back</button></div>";
                    $('#cus-history-table tfoot').append(footer);
                } else {
                    $('#cus-history-table thead tr').addClass('hidden');
                    var noDataFooter = "<div class= ' col-lg-9 text-right'><label><b>No Data</b></label>";
                    $('#cus-history-table tbody').append(noDataFooter);
                }
            }
        });
    }
});