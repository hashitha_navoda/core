$(document).ready(function() {

    $('#activityViewBack').on('click', function() {
        window.history.back();
        window.setTimeout(function() {
            location.reload();
        }, 1000);
    });

    $('#editActivity').on('click', function() {
        var url = BASE_URL + '/activity/edit/' + $('#actID').val();
        window.location.assign(url);
    });

    $('.viewTemporaryProduct').on('click', function(e) {
        e.preventDefault();
        var temporaryProductID = $(this).attr('data-id');
        eb.ajax({
            async: false,
            type: 'POST',
            url: BASE_URL + '/api/activity/getTemporaryProductDetails',
            data: {temporaryProductID: temporaryProductID},
            success: function(respond) {
                var tProduct = respond.data.tProductData;
                $('#temporaryProductItemCode', '#TemporaryProductModal').html(tProduct.tPC);
                $('#temporaryProductItemName', '#TemporaryProductModal').html(tProduct.tPN);
                $('#temporaryProductDescription', '#TemporaryProductModal').html(tProduct.tPD);
                $('#temporaryProductPrice', '#TemporaryProductModal').html(tProduct.tPP);
                $('#temporaryProductQuantity', '#TemporaryProductModal').html(tProduct.tPQ + ' ' + tProduct.aPUom);
                $('#temporaryProductBatch', '#TemporaryProductModal').html(tProduct.bCode);
                $('#temporaryProductSerial', '#TemporaryProductModal').html(tProduct.sCode);

//              appending images to modal
                var images = tProduct.images;
                $('#images', '#TemporaryProductModal').empty();
                $.each(images, function(index, element) {

                    var imageContainer = $('<div/>')
                            .addClass('image-display-alignment')
                            .appendTo('#images');

                    var imageObj = new Image();
                    imageObj.src = decodeURIComponent(element);
                    imageObj.style.height = '100%';
                    imageObj.style.width = '100%';
                    var imageDiv = $('<div/>').addClass('view');

                    imageObj.onload = function() {
                        imageDiv.html(imageObj);
                        var zoomIcon = $('<a>').addClass('image-zoom')
                                .prop('href', decodeURIComponent(element))
                                .prop('target', '_blank');
                        imageDiv.wrapInner(zoomIcon);
                        var imageName = element.split("/").pop();
                        imageDiv.appendTo(imageContainer).append($('<span/>').addClass('fileName').text(imageName));
                    };
                });
                $('#TemporaryProductModal').modal('show');
            }
        });
        return false;
    });
});
