var projectID = '';
$(function() {
    $('#project-search').on('submit', function(e) {
        e.preventDefault();
        eb.ajax({
            type: 'POST',
            url: BASE_URL + '/project-api/getProjectSearchList',
            data: {searchKey: $('#project-search-keyword').val()},
            success: function(respond) {
                if (respond.status) {
                    $('#projectList').html(respond.html);
                }
            },
        });
    });

    $(document).on('click', '.projectDelete', function() {
        $('#delete_project').modal('show');
        projectID = this.id;
    });

    $('#delete-project-button').on('click', function(e) {
        e.preventDefault();
        eb.ajax({
            type: 'POST',
            url: BASE_URL + '/project-api/deleteProjectByProjectID',
            data: {projectID: projectID},
            success: function(respond) {
                p_notification(respond.status, respond.msg)
                if (respond.status) {
                    window.location.reload();
                }
            },
        });
        $('#delete_project').modal('hide');
    });

    $(document).on('click', '.projectEdit', function(e) {
        e.preventDefault();
        var url = BASE_URL + '/project/edit/' + this.id;
        window.location.assign(url);
    });

    $(document).on('click', '.projectView', function() {
        var url = BASE_URL + '/project/view/' + this.id;
        window.location.assign(url);
    });

    $('#viewBack').on('click', function() {
        window.history.back();
    });

    $(document).on('click', '.jobList', function() {
        var url = BASE_URL + '/job/list/1/' + this.id;
        window.location.assign(url);
    });

    $(document).on('click', '.createJob', function() {
        var url = BASE_URL + '/job/create/' + this.id;
        window.location.assign(url);
    });

    $('#createJob').on('click', function() {
        var url = BASE_URL + '/job/create/' + $('#projectID').val();
        window.location.assign(url);
    });

    $('#listJob').on('click', function() {
        var url = BASE_URL + '/job/list/1/' + $('#projectID').val();
        window.location.assign(url);
    });

    $('#editProject').on('click', function() {
        var url = BASE_URL + '/project/edit/' + $('#projectID').val();
        window.location.assign(url);
    });

    var durationTime = parseInt($('#viewTimeDuration').val());
    var less = 0;
    var days = parseInt(durationTime / (60 * 24));
    less = durationTime % (60 * 24);
    var hours = parseInt(less / 60);
    less = less % 60;
    var minutes = less;
    $('#viewTimeDuration').val(days + ':Days ' + hours + ':Hours  ' + minutes + ':Minutes');

    // project selction in view page
    loadDropDownFromDatabase('/project-api/searchProjectsForDropdown', "", "", '#projectSelection');
    $('#projectSelection').selectpicker('refresh');
    $('#projectSelection').on('change', function() {
        if ($(this).val() > 0) {
            var projectID = $(this).val();
            eb.ajax({
                url: BASE_URL + '/project-api/get-project-list-view-byID',
                method: 'POST',
                data: {projectID: projectID},
                success: function(respond) {
                    if (respond.status) {
                        $('#from-date').val('');
                        $('#to-date').val('');
                        $('#projectList').html(respond.html);
                    }
                }
            });
        }
    });

    $(document).on('click', "a.editsInactive", function(e) {
        p_notification(false, eb.getMessage('ERR_CUST_INACTIVE'));
    });

    $('#date-filter').on('click', function(e) {
        e.preventDefault();
        if ($('#from-date').val() == '' || $('#to-date').val() == '') {
            p_notification(false, eb.getMessage('ERR_INQ_LOG_VIEW_DATE_RANGE'));
            return false;
        }
        eb.ajax({
            url: BASE_URL + '/project-api/get-project-list-view-by-date-range',
            method: 'POST',
            data: {fromDate: $('#from-date').val(), toDate: $('#to-date').val()},
            success: function(respond) {
                if (respond.status) {
                    $('#projectSelection').val('').selectpicker('render');
                    $('#projectList').html(respond.html);
                }
            }
        });
    });

    $('#clear-filter').on('click', function(e) {
        e.preventDefault();

        eb.ajax({
            url: BASE_URL + '/project-api/get-project-list-view-byID',
            method: 'POST',
            success: function(respond) {
                if (respond.status) {
                    $('#from-date').val('');
                    $('#to-date').val('');
                    $('#projectSelection').val('').selectpicker('render');
                    $('#projectList').html(respond.html);
                }
            }
        });
    });

    //for load change status modal
    $('#projectList').on( 'click', '.projectChangeStatus', function(){
        projectID = $(this).closest('tr').data('project-id');
        var projectStatus = $(this).data('status');
        $('#projectStatueChangeModal').modal('show');
        $('#statusValue').val(projectStatus);
    });

    //change project status
    $(document).on('click','#changeStatusBtn',function (){

        var projectStatus = $('#statusValue').val();
        var obj = {};
        obj.projectId = projectID;
        obj.projectStatus = projectStatus;

        eb.ajax({
            url: BASE_URL + '/project-api/change-project-status',
            method: 'post',
            data: obj,
            dataType: 'json',
            success: function(data) {
                if (data.status == true) {
                    var label = null;
                    var $selectedTr = $('tr[data-project-id="'+projectID+'"]');
                    switch (projectStatus){
                        case '3':
                            label = '<span class="label label-success">Open</span>';
                            break;
                        case '8':
                            label = '<span class="label label-primary">In Progress</span>';
                            break;
                        case '9':
                            label = '<span class="label label-info">Completed</span>';
                            break;
                        case '4':
                            label = '<span class="label label-danger">Closed</span>';
                            //disable btns
                            $selectedTr.find('.projectEdit').removeClass('projectEdit').addClass('disabled');
                            $selectedTr.find('.projectDelete').removeClass('projectDelete').addClass('disabled');
                            $selectedTr.find('.projectChangeStatus').removeClass('projectChangeStatus').addClass('disabled');
                            break;
                        default :
                            console.log('invalid option');
                    }
                    $('#projectStatueChangeModal').modal('hide');
                    //change label
                    $selectedTr.find('.proStatus').html(label);
                    //change data attribute
                    $selectedTr.find('.projectChangeStatus').data('status',projectStatus);
                }
            }
        });
    });

    ///////DatePicker\\\\\\\
    var nowTemp = new Date();
    var frmdate;
    var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);

    var checkin = $('#from-date').datepicker().on('changeDate', function(ev) {
        checkin.hide();
        frmdate = ev.date;
        $('#to-date')[0].focus();
    }).data('datepicker');
    var checkout = $('#to-date').datepicker({
        onRender: function(date) {
            return date.valueOf() < frmdate ? 'disabled' : '';
        }
    }).on('changeDate', function(ev) {
        checkout.hide();
    }).data('datepicker');
/////EndOFDatePicker\\\\\\\\\
});