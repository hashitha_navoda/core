
var contractorID = null;
var editMode = false;
$(document).ready(function() {

    $('#temporaryContractor').on('submit', function(e) {
        e.preventDefault();
        var formInputs = {
            temporaryContractorFirstName: $('#temporaryContractorFirstName').val(),
            temporaryContractorSecondName: $('#temporaryContractorSecondName').val(),
            designationID: $('#designationID').val(),
            divisionID: $('#divisionID').val(),
            temporaryContractorTP: $('#temporaryContractorTP').val(),
            contractorID: contractorID,
        };
        if (validateContractorForm(formInputs)) {
            formInputs.editMode = editMode;
            eb.ajax({
                type: 'POST',
                url: BASE_URL + '/api/temporary-contractor/save-temporary-contractor',
                data: formInputs,
                success: function(response) {
                    if (response.status) {
                        clearTemporaryContractorForm();
                        $('#temporary-product-list').html(response.html);
                        editMode = false;
                    }
                    p_notification(response.status, response.msg);
                },
            });
        }

        return false;
    });
    $('#temporaryContractor').on('reset', function() {
        clearTemporaryContractorForm();
        $('.add-contractor-h5').removeClass('hidden');
        $('.update-contractor-h5').addClass('hidden');
        $('.add-button-set').removeClass('hidden');
        $('.update-button-set').addClass('hidden');
        editMode = false;
        return false;
    });

    $('#temporaryContractorSearchForm').on('submit', function(e) {
        e.preventDefault();
        eb.ajax({
            type: 'POST',
            url: BASE_URL + '/api/temporary-contractor/search-temporary-contractor',
            data: {contractorSearchKey: $('#temporaryContractorSearchKeyword').val()},
            success: function(response) {
                if (response.status) {
                    $('#temporary-product-list').html(response.html);
                } else {
                    p_notification(response.data.status, response.msg);
                }
            }
        });
        return false;
    });
    $('#temporaryContractorSearchForm').on('reset', function(e) {
        e.preventDefault();
        $('#temporaryContractorSearchKeyword').val('');
        $('#temporaryContractorSearchForm').submit();
        return false;
    });

    $('#temporary-product-list').on('click', '.edit', function(e) {
        e.preventDefault();
        contractorID = $(this).closest('tr').attr('data-contractorid');
        eb.ajax({
            type: 'POST',
            url: BASE_URL + '/api/temporary-contractor/get-temporary-contractor',
            data: {contractorID: contractorID},
            success: function(response) {
                if (response.status) {
                    $('#temporaryContractorFirstName').val(response.data.temporaryContractorFirstName).prop('disabled', true);
                    $('#temporaryContractorSecondName').val(response.data.temporaryContractorSecondName).prop('disabled', true);
                    $('#designationID').val(response.data.designationID).selectpicker('render');
                    $('#divisionID').val(response.data.divisionID).selectpicker('render');
                    $('#temporaryContractorTP').val(response.data.temporaryContractorTP);
                    $('.add-contractor-h5').addClass('hidden');
                    $('.update-contractor-h5').removeClass('hidden');
                    $('.add-button-set').addClass('hidden');
                    $('.update-button-set').removeClass('hidden');
                    editMode = true;
                } else {
                    p_notification(response.status, response.msg);
                    contractorID = null;
                }
            }
        });
        return false;
    });

    $('#temporary-product-list').on('click', 'a.delete', function(e) {
        e.preventDefault();
        contractorID = $(this).closest('tr').attr('data-contractorid');

        bootbox.confirm("Are you sure you want to delete this Temporary Contractor?", function($result) {
            if ($result == true) {
                eb.ajax({
                    type: 'POST',
                    url: BASE_URL + '/api/temporary-contractor/delete-temporary-contractor',
                    data: {contractorID: contractorID},
                    success: function(response) {
                        if (response.status) {
                            $('#temporary-product-list').html(response.html);
                        }
                        p_notification(response.status, response.msg);
                        contractorID = null;
                        editMode = null;
                    }
                });
            }
        });

        return false;
    });

    $('#temporary-product-list').on('click', 'a.status', function(e) {
        e.preventDefault();
        var status;
        var currentDiv = $(this).find('span');
        contractorID = $(this).closest('tr').attr('data-contractorid');
        if ($(this).children().hasClass('fa-check-square-o')) {
            msg = 'Are you sure you want to Deactivate this Temporary Contractor';
            status = '0';
        }
        else if ($(this).children().hasClass('fa-square-o')) {
            msg = 'Are you sure you want to Activate this Temporary Contractor';
            status = '1';
        }
        bootbox.confirm(msg, function($result) {
            if ($result == true) {
                eb.ajax({
                    url: BASE_URL + '/api/temporary-contractor/update-temporary-contractor-status',
                    method: 'post',
                    dataType: 'json',
                    data: {
                        'contractorID': contractorID,
                        'status': status,
                    },
                    success: function(data) {
                        p_notification(data.status, data.msg);
                        if (data.status) {
                            if (currentDiv.hasClass('fa-square-o')) {
                                currentDiv.addClass('fa-check-square-o');
                                currentDiv.removeClass('fa-square-o');
                            }
                            else {
                                currentDiv.addClass('fa-square-o');
                                currentDiv.removeClass('fa-check-square-o');
                            }
                        }
                    }
                });
            }
        });
        return false;
    });
});

var clearTemporaryContractorForm = function() {
    $('#temporaryContractorFirstName').val('').prop('disabled', false);
    $('#temporaryContractorSecondName').val('').prop('disabled', false);
    $('#designationID').val('');
    $('#divisionID').val('');
    $('#temporaryContractorTP').val('');
    $('#designationID').selectpicker('render');
    $('#divisionID').selectpicker('render');
    contractorID = null;
    editMode = false;
};

var validateContractorForm = function(inputs) {
    var fName = inputs.temporaryContractorFirstName;
    var sName = inputs.temporaryContractorSecondName;
    var telNumber = inputs.temporaryContractorTP;
    if (fName == null || fName == "") {
        p_notification(false, eb.getMessage('ERR_CONTRAC_FNAME_BLANK'));
        $('#temporaryContractorFirstName').focus();
    } else if (sName == null || sName == "") {
        p_notification(false, eb.getMessage('ERR_CONTRAC_SNAME_BLANK'));
        $('#temporaryContractorSecondName').focus();
    } else if (isNaN(telNumber)) {
        p_notification(false, eb.getMessage('ERR_WIZARD_TELENUM'));
    } else if (telNumber != "" && (telNumber.length < 10 || telNumber.length > 13)) {
        p_notification(false, eb.getMessage('ERR_WIZARD_PHONENO_LIMIT'));
    } else {
        return true;
    }
};