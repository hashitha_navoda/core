var projectID = '';
var jobID = '';
var activityID = '';
var activityCode = '';
$(function() {
    $('#childjob').selectpicker('hide');
    $('#activity').prop('disabled', true).selectpicker('refresh');

    loadDropDownFromDatabase('/project-api/searchProjectsForDropdown', "", 1, '#project');
    $('#project').selectpicker('refresh');
    $('#project').on('change', function() {
        if ($(this).val() > 0 && ($(this).val() != projectID)) {
            projectID = $(this).val();
            $('#job').selectpicker('hide');
            $('#childjob').selectpicker('show');
            getJobList(projectID);
            $('#job').val('');
            $('#childjob').val('');
            $('#activity').prop('disabled', true).selectpicker('refresh');
            jobID = '';
            activityID = '';
        }
        if ($(this).val() == 0) {
            $('#job').selectpicker('show');
            $('#childjob').selectpicker('hide');
            $('#job').empty().trigger('change');
            //$('#job').append("<option value=''>" + "Select a Job" + "</option>");
            $('#job').selectpicker('refresh');
            $('#job').val('').selectpicker('refresh');
            $('#activity').prop('disabled', true).selectpicker('refresh');
        }
    });

    loadDropDownFromDatabase('/job-api/searchJobsForDropdown', "", '', '#job');
    $('#job').selectpicker('refresh');
    $('#job').on('change', function() {
        if ($(this).val() > 0 && ($(this).val() != jobID)) {
            jobID = $(this).val();
            $('#activity').val('');
            activityID = '';
            if (jobID) {
                getActivityList(jobID, JOBREFERENCELIST);
            }
        } else {
            $('#activity').prop('disabled', true).selectpicker('refresh');
            jobID = '';
        }
    });

    function getJobList(projectIDparam) {
        eb.ajax({
            url: BASE_URL + '/job-api/getJobsByProjectID',
            method: 'post',
            data: {
                projectID: projectIDparam,
                projectCode: PROJECT_LIST[projectIDparam]
            },
            dataType: 'json',
            success: function(data) {
                if (data.status) {
                    setJobSelectPicker(data.data);
                } else {
                    p_notification(data.status, data.msg);
                    resetSelectPicker('project', 'Project');
                    jobID = '';
                    activityID = '';
                    projectID = '';
                    $('#job').val('');
                    $('#activity').val('');
                    $('#progress').val('');
                }
            }
        });
    }

    function setJobSelectPicker(Jobs) {
        $('#childjob').html("<option value=''>" + "Select a Job" + "</option>");
        $.each(Jobs, function(index, value) {
            $('#childjob').append("<option value='" + index + "'>" + value + "</option>");
        });
        $('#childjob').selectpicker('refresh');

        $('#childjob').on('change', function(e) {
            e.preventDefault();
            if ($(this).val() > 0 && ($(this).val() != jobID)) {
                jobID = $(this).val();
                $('#activity').val('');
                activityID = '';
                if (jobID) {
                    getActivityList(jobID, Jobs);
                }
            } else {
                $('#activity').prop('disabled', true).selectpicker('refresh');
                jobID = '';
            }
        });
    }

    function getActivityList(jobID, Jobs) {
        eb.ajax({
            url: BASE_URL + '/api/activity/getActivitiesByJobID',
            method: 'post',
            data: {
                jobID: jobID,
                jobCode: Jobs[jobID]
            },
            dataType: 'json',
            success: function(data) {
                if (data.status) {
                    setActivitySelectpicker(data.data.activityData);
                } else {
                    p_notification(data.status, data.msg);
                    activityID = '';
                    jobID = '';
                    resetSelectPicker('job', 'Job');
                    $('#activity').val('');
                    $('#progress').val('');
                }
                if (data.data.projectID) {
                    $('#project').html("<option value=''>" + "Select a Project" + "</option>\n\
<option value='" + data.data.projectID + "'>" + data.data.projectCode + "</option>");
                    $('#project').selectpicker('refresh');
                    projectID = data.data.projectID;
                } else {
                    projectID = "";
                }
                $('#project').val(projectID).selectpicker('refresh');



            }
        });
    }


    function setActivitySelectpicker(Activities) {
        $('#activity').prop('disabled', false).selectpicker('refresh');

        $('#activity').html("<option value=''>" + "Select a Activity" + "</option>");
        $.each(Activities, function(index, value) {
            $('#activity').append("<option value='" + index + "'>" + value + "</option>");
        });
        $('#activity').selectpicker('refresh');

        $('#activity').on('change', function(e) {
            e.preventDefault();
            activityID = $(this).val();
            getActivityProgress(activityID, Activities);
        });
    }

    function getActivityProgress(activityID, Activities) {
        activityCode = Activities[activityID];
        eb.ajax({
            url: BASE_URL + '/api/activity/getActivityByActivityId',
            method: 'post',
            data: {
                activityID: activityID,
                activityCode: activityCode,
            },
            dataType: 'json',
            success: function(data) {
                if (data.status) {
                    if (data.data == null) {
                        $('#progress').val('0.00');
                    } else {
                        $('#progress').val(data.data);
                    }
                } else {
                    p_notification(data.status, data.msg);
                    $('#progress').val('');
                }
            }
        });
    }

    function resetSelectPicker(referenceId, string) {
        $('#' + referenceId).val('').empty().selectpicker('refresh');
        $('#' + referenceId).append("<option value=''>" + "Select a " + string + "</option>");
        $('#' + referenceId).val('').trigger('change').selectpicker('refresh');
    }

    $('#progressForm').on('submit', function(e) {
        e.preventDefault();
        var progress = $('#progress').val();
        if (jobID == '' || activityID == '') {
            p_notification(false, eb.getMessage('ERR_SELECT_A_PROJECT_JOB_AND_ACTIVITY'));
        } else if (progress > 100 || progress < 0 || isNaN(progress)) {
            p_notification(false, eb.getMessage('ERR_PROGRESS_VAL_CANBE_NAN_NEGATIVE'));
        } else {
            eb.ajax({
                url: BASE_URL + '/progress-api/updateActivity',
                method: 'post',
                data: {
                    activityID: activityID,
                    activityCode: activityCode,
                    jobID: jobID,
                    projectID: projectID,
                    progress: progress
                },
                dataType: 'json',
                success: function(data) {
                    p_notification(data.status, data.msg);
                    if (data.status) {
                        window.location.reload();
                    }
                }
            });
        }
    });
});