/**
 * @author Sandun <sandun@thinkcube.com>
 * There have inquiry type related functions
 */
var inquiryStatusID = null;
var editMode = false;

$(document).ready(function() {
    //set selectpicker to inquiry status select option
    $('#inquiryStatusTypeID').selectpicker();

    $('#create-inquiry-status-form').on('submit', function(e) {
        e.preventDefault();
        var formData = {
            inquiryStatusName: $('#inquiryStatusName').val(),
            inquiryStatusTypeID: $('#inquiryStatusTypeID').val(),
            editMode: editMode,
            inquiryStatusID: inquiryStatusID,
        };
        if (validateFormData(formData)) {
            eb.ajax({
                url: BASE_URL + '/api/inquiry-status/add',
                method: 'post',
                data: formData,
                dataType: 'json',
                success: function(data) {
                    p_notification(data.status, data.msg);
                    if (data.status) {
                        window.setTimeout(function() {
                            location.reload();
                        }, 1000);
                    } else {
                        if (!data.status) {
                            $('#inquiryStatusName').focus();
                        }
                    }
                }
            });
        }
    });
    $('#create-inquiry-status-form').on('reset', function(e) {
        setTimeout(function() {
            $('#create-inquiry-status-form').find("select").trigger('change');
        }, 1);
    });

    $('#inquiryStatusSearch').on('submit', function(e) {
        e.preventDefault();
        var formData = {
            inquiryStatusSearchKey: $('#inquiry-status-search-keyword').val(),
        };
        eb.ajax({
            url: BASE_URL + '/api/inquiry-status/getInquiryStatusBySearchKey',
            method: 'post',
            data: formData,
            dataType: 'json',
            success: function(data) {
                if (data.status) {
                    $("#inquiry-status-list").html(data.html);
                }
            }
        });
    });

    $("form#inquiryStatusSearch button.reset").on('click', function() {
        $('#inquiry-status-search-keyword').val('');
        $('#inquiryStatusSearch').submit();
    });

    $('#inquiry-status-list').on('click', 'a.edit', function(e) {
        e.preventDefault();
        inquiryStatusID = $(this).parents('tr').data('inquirystatusid');
        $('#inquiryStatusName').val($(this).parents('tr').find('.inquiry-status-name').text()).attr('disabled', true);
        $('#inquiryStatusTypeID').val($(this).parents('tr').find('.inquiry-status-type-id').text());
        $('#inquiryStatusTypeID').selectpicker('render');
        $('.updateTitle').removeClass('hidden');
        $('.addTitle').addClass('hidden');
        $('.updateDiv').removeClass('hidden');
        $('.addDiv').addClass('hidden');
        editMode = true;
    });

    $('.cancel').on('click', function(e) {
        e.preventDefault();
        inquiryStatusID = '';
        $('#inquiryStatusName').val('').attr('disabled', false);
        $('#inquiryStatusTypeID').val(0);
        $('#inquiryStatusTypeID').selectpicker('render');
        $('.updateTitle').addClass('hidden');
        $('.addTitle').removeClass('hidden');
        $('.updateDiv').addClass('hidden');
        $('.addDiv').removeClass('hidden');
        editMode = false;
    });

    $('#inquiry-status-list').on('click', 'a.status', function() {
        var msg;
        var status;
        var inquiryStatusID = $(this).parents('tr').data('inquirystatusid');
        var currentDiv = $(this).contents();
        var inquiryStatusFlag = true;
        if ($(this).children().hasClass('fa-check-square-o')) {
            msg = 'Are you sure you want to Deactivate this inquiry Status';
            status = '0';
        }
        else if ($(this).children().hasClass('fa-square-o')) {
            msg = 'Are you sure you want to Activate this inquiry Status';
            status = '1';
        }
        activeInactive(inquiryStatusID, status, currentDiv, msg, inquiryStatusFlag);
    });

    $('#inquiry-status-list').on('click', 'a.delete', function(e) {
        e.preventDefault();
        $('#inquiryStatusName').val('').attr('disabled', false);
        $('#inquiryStatusTypeID').val(0);
        $('#inquiryStatusTypeID').selectpicker('render');
        $('.updateTitle').addClass('hidden');
        $('.addTitle').removeClass('hidden');
        $('.updateDiv').addClass('hidden');
        $('.addDiv').removeClass('hidden');
        editMode = false;

        var inquiryStatusID = $(this).parents('tr').data('inquirystatusid');
        var currentDiv = $(this).parents('tr').find('a.status').contents();
        var inquiryStatusFlag = false;
        var msg = eb.getMessage('INQ_STATUS_DELETE_AND_STATE');
        var inquiryStatusName = $(this).parents('tr').find('.inquiry-status-name').text();
        bootbox.confirm('Are you sure you want to delete this Inquiry Status?', function(result) {
            if (result === true) {
                eb.ajax({
                    url: BASE_URL + '/api/inquiry-status/deleteInquiryStatusByInquiryStatusID',
                    method: 'post',
                    data: {
                        inquiryStatusID: inquiryStatusID,
                        inquiryStatusName: inquiryStatusName,
                    },
                    dataType: 'json',
                    success: function(data) {
                        if (data.status == true) {
                            window.location.reload();
                            p_notification(data.status, data.msg);
                        }
                        else if (data.status == false && data.data == 'deactive') {
                            activeInactive(inquiryStatusID, '0', currentDiv, msg, inquiryStatusFlag);
                        } else {
                            p_notification(data.status, data.msg);
                        }
                    }
                });
            }
        });

    });

    function validateFormData(formData) {
        if (formData.inquiryStatusName == "") {
            p_notification(false, eb.getMessage('ERR_INQUSTATUS_NAME_CNT_BE_NULL'));
            $('#inquiryStatusName').focus();
            return false;
        } else if (formData.inquiryStatusTypeID == "") {
            p_notification(false, eb.getMessage('ERR_INQUSTATUS_TYPE_CNT_BE_NULL'));
            $('#inquiryStatusTypeID').focus();
            return false;
        }
        return true;
    }
    /**
     *
     *use to change active state
     */
    function activeInactive(type, status, currentDiv, msg, inquiryStatusFlag) {
        bootbox.confirm(msg, function($result) {
            if ($result == true) {
                eb.ajax({
                    url: BASE_URL + '/api/inquiry-status/changeStatusID',
                    method: 'post',
                    dataType: 'json',
                    data: {
                        'inquiryStatusID': type,
                        'status': status,
                    },
                    success: function(data) {
                        if (data.status) {
                            p_notification(data.status, data.msg);
                            if (currentDiv.hasClass('fa-square-o') && inquiryStatusFlag) {
                                currentDiv.addClass('fa-check-square-o');
                                currentDiv.removeClass('fa-square-o');
                            }
                            else {
                                currentDiv.addClass('fa-square-o');
                                currentDiv.removeClass('fa-check-square-o');
                            }
                        } else {
                            p_notification(data.status, data.msg);
                        }
                    }
                });
            }
        });
    }
});