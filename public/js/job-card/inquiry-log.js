var addCustomer = false;
var CUSTOMERS_NAMES;
$(document).ready(function() {
    var customerID;
    var employeeID;
    var inqLogId;
    var projectID;
    var jobID;
    var activityID;
    var formData;
    var inquiryTypeID;
    var inquiryStatusNameID;
    var linkProject = false;
    var linkJob = false;
    var linkActivity = false;
    var duManager = new Array();
    var documentTypeID;
    var inqQuotDocumentTypeID;
    var inqSalesDocumentTypeID;
    var inqInvDocumentTypeID;
    var inqProDocumentTypeID;
    var inqJobDocumentTypeID;
    var inqActDocumentTypeID;
    var inqInquiryDocumentTypeID;
    var datatypeListWithRefIds = new Array();
    var inquiryLodId = null;


    $('#inqProReference').hide();
    $('#inqJobReference').hide();
    $('#inqActReference').hide();
    $('#inqCompSODocuTypeID').hide();
    $('#inqCompInvDocuTypeID').hide();
    $('#inqCompProDocuTypeID').hide();
    $('#inqCompJobDocuTypeID').hide();
    $('#inqCompActDocuTypeID').hide();
    $('#inqCompInqDocuTypeID').hide();
    $('#create-inquiry-log-form #userHistory').hide();
    $('#customer_more').hide();
    $('#addCustomerModal').on('click', '#moredetails', function() {
        $('#customer_more').slideDown();
        $('#addCustomerModal #moredetails').hide();
    });
    $('#inquiry_log_more').hide();
    $('#inquiry_log_edit_more').hide();
    $('#moredetails').on('click', function() {
        $('#inquiry_log_more').slideDown();
        $('#inquiry_log_edit_more').slideDown();
        $('#moredetails').hide();
    });
    $('#addAndJob').hide();


    $('#jobRef').selectpicker('hide');
    $('#actRef').selectpicker('hide');

//    $('#addActivityBtn').addClass('hidden');
    $('#addActivityBtn').addClass('disabled');


    // inquiry log selction in view page
    loadDropDownFromDatabase('/inquiry-log-api/searchInquiryLogForDropdown', "", "", '#inquiryLogSelection');
    $('#inquiryLogSelection').selectpicker('refresh');
    $('#inquiryLogSelection').on('change', function() {
        if ($(this).val() > 0) {
            var inquiryLogID = $(this).val();
            eb.ajax({
                url: BASE_URL + '/inquiry-log-api/get-inquiry-log-by-search',
                method: 'POST',
                data: {inquiryLogID: inquiryLogID},
                success: function(respond) {
                    if (respond.status) {
                        $('#from-date').val('');
                        $('#to-date').val('');
                        $('#search-inquiry-log-data-list').html(respond.html);
                    }
                }
            });
        }
    });

    $('#date-filter').on('click', function(e) {
        e.preventDefault();
        if ($('#from-date').val() == '' || $('#to-date').val() == '') {
            p_notification(false, eb.getMessage('ERR_INQ_LOG_VIEW_DATE_RANGE'));
            return false;
        }
        eb.ajax({
            url: BASE_URL + '/inquiry-log-api/get-inquiry-log-by-date-range',
            method: 'POST',
            data: {fromDate: $('#from-date').val(), toDate: $('#to-date').val()},
            success: function(respond) {
                if (respond.status) {
                    $('#inquiryLogSelection').val('').selectpicker('render');
                    $('#search-inquiry-log-data-list').html(respond.html);
                }
            }
        });
    });

    $('#clear-filter').on('click', function(e) {
        e.preventDefault();
        eb.ajax({
            url: BASE_URL + '/inquiry-log-api/get-inquiry-log-by-search',
            method: 'POST',
            success: function(respond) {
                if (respond.status) {
                    $('#from-date').val('');
                    $('#to-date').val('');
                    $('#inquiryLogSelection').val('').selectpicker('render');
                    $('#search-inquiry-log-data-list').html(respond.html);
                }
            }
        });
    });

    ///////DatePicker\\\\\\\
    var nowTemp = new Date();
    var frmdate;
    var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);

    var checkin = $('#from-date').datepicker().on('changeDate', function(ev) {
        checkin.hide();
        frmdate = ev.date;
        $('#to-date')[0].focus();
    }).data('datepicker');
    var checkout = $('#to-date').datepicker({
        onRender: function(date) {
            return date.valueOf() < frmdate ? 'disabled' : '';
        }
    }).on('changeDate', function(ev) {
        checkout.hide();
    }).data('datepicker');
/////EndOFDatePicker\\\\\\\\\

    $('#addActivityBtnUpdate').addClass('hidden');
    loadDropDownFromDatabase('/job-api/searchJobsForDropdown', "", "", '#jobRef2');
    $('#jobRef2').selectpicker('refresh');
    $('#jobRef2').on('change', function() {
        if ($(this).val() > 0 && jobID != $(this).val())
            jobID = $(this).val();
    });
    loadDropDownFromDatabase('/api/activity/searchActivitiesForDropdown', "", "", '#actRef2');
    $('#actRef2').selectpicker('refresh');
    $('#actRef2').on('change', function() {
        if ($(this).val() > 0 && activityID != $(this).val())
            activityID = $(this).val();
    });
//    if ($('#proRef').data('proid')) {
//        projectID = $('#proRef').data('proid');
//        $('#proRef').
//                append($("<option></option>").
//                        attr("value", projectID).
//                        text(PROJECT_LIST[projectID]));
//        $('#proRef').selectpicker('refresh');
//    }
    if ($('#inquiryLogCustomerID').data('cusid')) {



        customerID = $('#inquiryLogCustomerID').data('cusid');
        customerName = $('#inquiryLogCustomerID').data('cusname');
        $('#inquiryLogCustomerID').
                append($("<option></option>").
                        attr("value", customerID).
                        text(customerName));
        $('#inquiryLogCustomerID').val(customerID);
        $('#inquiryLogCustomerID').val(customerID).prop('disabled', true);
        $('#inquiryLogCustomerID').selectpicker('refresh');
    }
    if ($('#inquiryTypeID').data('inqtypeid')) {
        inquiryTypeID = $('#inquiryTypeID').data('inqtypeid');
        $('#inquiryTypeID').
                append($("<option></option>").
                        attr("value", inquiryTypeID).
                        text(ALLINQUIRYTYPES[inquiryTypeID]));
        $('#inquiryTypeID').val(inquiryTypeID).prop('disabled', false);
        $('#inquiryTypeID').selectpicker('refresh');
    }
    if ($('#inquiryLogStatusID').data('inqstatid')) {
        inquiryStatusNameID = $('#inquiryLogStatusID').data('inqstatid');
        $('#inquiryLogStatusID').
                append($("<option></option>").
                        attr("value", inquiryStatusNameID).
                        text(ALLINQUIRYSTATUSNAMES[inquiryStatusNameID]));
        $('#inquiryLogStatusID').val(inquiryStatusNameID).prop('disabled', false);
        $('#inquiryLogStatusID').selectpicker('refresh');
    }
    if ($('#proRef').data('proid')) {
        projectID = $('#proRef').data('proid');
        $('#proRef').
                append($("<option></option>").
                        attr("value", projectID).
                        text(PROJECT_LIST[projectID]));
        $('#proRef').val(projectID).prop('disabled', false);
        $('#proRef').selectpicker('refresh');
    }
    if ($('#jobRef2').data('jbid')) {
        jobID = $('#jobRef2').data('jbid');
        $('#jobRef2').
                append($("<option></option>").
                        attr("value", jobID).
                        text(JOBREFERENCELIST[jobID]));
        $('#jobRef2').val(jobID).prop('disabled', false);
        $('#jobRef2').selectpicker('refresh');
    }
    if ($('#actRef2').data('actid')) {
        activityID = $('#actRef2').data('actid');
        $('#actRef2').
                append($("<option></option>").
                        attr("value", activityID).
                        text(ALLACTIVITYREFERENCE[activityID]));
        $('#actRef2').val(activityID).prop('disabled', false);
        $('#actRef2').selectpicker('refresh');
    }
    loadDropDownFromDatabase('/project-api/searchProjectsForDropdown', "", 1, '#proRef');
    $('#proRef').selectpicker('refresh');
    $('#proRef').on('change', function() {
        if ($(this).val() > 0 && $(this).val() != projectID) {
            projectID = $(this).val();
            $('#jobRef2').selectpicker('hide');
            $('#jobRef').selectpicker('show');
            $('#jobRef').empty().val('').trigger('change');
            $('#actRef').empty().val('').trigger('change');
            $('#jobRef').html("<option value=''>Select Job</option>");
            $('#actRef').html("<option value=''>Select Activity</option>");
            jobID = '';
            setJobReference(projectID)
            $('#jobRef').selectpicker('refresh');
            $('#actRef').selectpicker('refresh');
            $('#jobRef').on('change', function() {
                if ($(this).val() > 0 && jobID != $(this).val()) {
                    jobID = $(this).val();
                    $('#actRef2').selectpicker('hide');
                    $('#actRef').selectpicker('show');
//                    $('#addActivityBtn').removeClass('hidden');
                    $('#addActivityBtn').removeClass('disabled');
                    $('#addActivityBtnUpdate').removeClass('hidden');
                    $('#actRef').empty().val('').trigger('change');
                    $('#actRef').html("<option value=''>Select activity</option>");
                    activityID = '';
                    setActivityReference(jobID);
                    $('#actRef').selectpicker('refresh');
                    $('#actRef').on('change', function() {
                        if ($(this).val() > 0 && activityID != $(this).val())
                            activityID = $(this).val();
                    });
                }
            });
        }
    });
    $('#jobRef2').on('change', function() {
        if (!projectID) {
            loadDropDownFromDatabase('/job-api/searchJobsForDropdown', "", "", '#jobRef2');
            $('#jobRef2').selectpicker('refresh');
            $('#jobRef2').on('change', function() {
                $('#actRef').selectpicker('refresh');
                if ($(this).val() > 0) {
                    jobID = $(this).val();
                    $('#actRef2').selectpicker('hide');
                    $('#actRef').selectpicker('show');
//                    $('#addActivityBtn').removeClass('hidden');
                    $('#addActivityBtn').removeClass('disabled');
                    $('#addActivityBtnUpdate').removeClass('hidden');
                    $('#actRef').empty().val('').trigger('change');
                    $('#actRef').html("<option value=''>Select activity</option>");
                    setActivityReference(jobID);
                    $('#actRef').on('change', function() {
                        if ($(this).val() > 0 && activityID != $(this).val())
                            activityID = $(this).val();
                    });
                }
            });
        }
    });
    $('#actRef').on('click', function(e) {
        e.preventDefault();
        if (jobID) {
//            $('#addActivityBtn').removeClass('hidden');
            $('#addActivityBtn').removeClass('disabled');

            $('#addActivityBtnUpdate').removeClass('hidden');
        }

    });
    if ($('#jobRef').data('jbid')) {
        $('#addActivityBtnUpdate').removeClass('hidden');
    }
    loadDropDownFromDatabase('/customerAPI/getCustomerDetailsByCustomerNameCodeOrTelephone', "", 1, '#inquiryLogCustomerSearchID');
    $('#inquiryLogCustomerSearchID').selectpicker('refresh');
    $('#inquiryLogCustomerSearchID').on('change', function() {
        getCustomerData();
        $('#create-inquiry-log-form #userHistory').show();
        if ($(this).val() > 0 && customerID != $(this).val()) {
            $('.ui-pnotify').remove();
            customerID = $(this).val();
            setCustomerProfiles(customerID);

            $('#inquiryLogCustomerSearchID').selectpicker('refresh');
            $('#inquiryLogCustomerID').
                    append($("<option></option>").
                            attr("value", customerID).
                            text(CUSTOMERS_NAMES[customerID]));
            $('#inquiryLogCustomerID').val(customerID);
            $('#inquiryLogCustomerID').selectpicker('refresh');
        }
    });

    loadDropDownFromDatabase('/api/inquiry-type/searchInquiryTypesForDropdown', "", 1, '#inquiryTypeID', false, false, false, true);
    $('#inquiryTypeID').selectpicker('refresh');
    $('#inquiryTypeID').on('change', function() {
        inquiryTypeID = $(this).val();
        $('#inquiryTypeID').selectpicker('refresh');
    });
    loadDropDownFromDatabase('/api/inquiry-status/searchInquiryStatusForDropdown', "", 1, '#inquiryLogStatusID', false, false, false, true);
    $('#inquiryLogStatusID').selectpicker('refresh');
    $('#inquiryLogStatusID').on('change', function() {
        inquiryStatusNameID = $(this).val();
        $('#inquiryLogStatusID').selectpicker('refresh');
    });
    loadDropDownFromDatabase('/employee_api/searchEmployeeForDropdown', "", 1, '#inquiryLogDutyManager');
    $('#inquiryLogDutyManager').selectpicker('refresh');
    $('#inquiryLogDutyManager').on('change', function() {


        if ($(this).val() > 0 && $(this).val() != employeeID) {
            employeeID = $(this).val();
            multipleEmployee(employeeID);
            $('#inquiryLogDutyManager').empty().val('').trigger('change');

        }
        $('#inquiryLogDutyManager').val('select');
        $('#inquiryLogDutyManager').selectpicker('refresh');
    });
    loadDropDownFromDatabase('/customerAPI/searchCustomersForDropdown', "", 1, '#inquiryLogCustomerID');
    $('#inquiryLogCustomerID').selectpicker('refresh');
    $('#inquiryLogCustomerID').on('change', function(e) {
        e.preventDefault();
        getCustomerData();
        $('#create-inquiry-log-form #userHistory').show();
        if ($(this).val() > 0 && customerID != $(this).val()) {
            $('.ui-pnotify').remove();
            customerID = $(this).val();
            setCustomerProfiles(customerID);

            $('#inquiryLogCustomerID').selectpicker('refresh');
            $('#inquiryLogCustomerSearchID').
                    append($("<option></option>").
                            attr("value", customerID).
                            text(CUSTOMERS_NAMES[customerID]));
            $('#inquiryLogCustomerSearchID').val(customerID);
            $('#inquiryLogCustomerSearchID').selectpicker('refresh');
        }
    });

    var startdateFormat = $('#inquiryLogDateAndTime').data('date-format');
    $('#inquiryLogDateAndTime').datetimepicker({
        format: startdateFormat + ' hh:ii',
    });
    if ($('#iqComType').hasClass('inquiry')) {
        $('#inquiryComplainType').val('Inquiry');
        $('#inqProReference').show();
        $('#inqJobReference').show();
        $('#inqActReference').show();
        $('#inqCompDocuType').hide();
        $('#inqCompQotDocuTypeID').hide();
        $('#inqCompSODocuTypeID').hide();
        $('#inqCompInvDocuTypeID').hide();
        $('#inqCompProDocuTypeID').hide();
        $('#inqCompJobDocuTypeID').hide();
        $('#inqCompActDocuTypeID').hide();
        $('#inqCompInqDocuTypeID').hide();
        $('#addAndJob').show();
    }
    $('#inquiryComplainType').on('change', function() {
        if ($(this).val() == 'Complain') {
            $('#inqProReference').hide();
            $('#inqJobReference').hide();
            $('#inqActReference').hide();
            $('#inqCompDocuType').show();
            $('#inqCompDocuTypeID').show();
            $('#inqCompSODocuTypeID').hide();
            $('#inqCompInvDocuTypeID').hide();
            $('#inqCompProDocuTypeID').hide();
            $('#inqCompJobDocuTypeID').hide();
            $('#inqCompActDocuTypeID').hide();
            $('#inqCompInqDocuTypeID').hide();
            if ($('#iqComType').hasClass('inquiry')) {
                $('#addAndJob').show();
            } else {
                $('#addAndJob').hide();
            }
        }
        else {
            $('#inqProReference').show();
            $('#inqJobReference').show();
            $('#inqActReference').show();
            $('#inqCompDocuType').hide();
            $('#inqCompQotDocuTypeID').hide();
            $('#inqCompSODocuTypeID').hide();
            $('#inqCompInvDocuTypeID').hide();
            $('#inqCompProDocuTypeID').hide();
            $('#inqCompJobDocuTypeID').hide();
            $('#inqCompActDocuTypeID').hide();
            $('#inqCompInqDocuTypeID').hide();
            $('#addAndJob').show();
        }
    });
    $('#documentTypeName').on('change', function() {
        var TypeID = $(this).val();
        if (TypeID == '1') {
            $('#inqCompQotDocuTypeID').show();
            $('#inqCompSODocuTypeID').hide();
            $('#inqCompInvDocuTypeID').hide();
            $('#inqCompProDocuTypeID').hide();
            $('#inqCompJobDocuTypeID').hide();
            $('#inqCompActDocuTypeID').hide();
            $('#inqCompInqDocuTypeID').hide();
            loadDropDownFromDatabase('/inquiry-log-api/searchDocumentsForDropdown', "", TypeID, '#qotDcumentTypeID');
            $('#qotDcumentTypeID').on('change', function() {
                inqQuotDocumentTypeID = $(this).val();
                $('#qotDcumentTypeID').selectpicker('refresh');
            });
        }
        else if (TypeID == '2') {
            $('#inqCompQotDocuTypeID').hide();
            $('#inqCompSODocuTypeID').show();
            $('#inqCompInvDocuTypeID').hide();
            $('#inqCompProDocuTypeID').hide();
            $('#inqCompJobDocuTypeID').hide();
            $('#inqCompActDocuTypeID').hide();
            $('#inqCompInqDocuTypeID').hide();
            loadDropDownFromDatabase('/inquiry-log-api/searchDocumentsForDropdown', "", TypeID, '#sODocumentTypeID');
            $('#sODocumentTypeID').on('change', function() {
                inqSalesDocumentTypeID = $(this).val();
                $('#sODocumentTypeID').selectpicker('refresh');
            });
        }
        else if (TypeID == '3') {
            $('#inqCompQotDocuTypeID').hide();
            $('#inqCompSODocuTypeID').hide();
            $('#inqCompInvDocuTypeID').show();
            $('#inqCompProDocuTypeID').hide();
            $('#inqCompJobDocuTypeID').hide();
            $('#inqCompActDocuTypeID').hide();
            $('#inqCompInqDocuTypeID').hide();
            loadDropDownFromDatabase('/inquiry-log-api/searchDocumentsForDropdown', "", TypeID, '#invDocumentTypeID');
            $('#invDocumentTypeID').on('change', function() {
                inqInvDocumentTypeID = $(this).val();
                $('#invDocumentTypeID').selectpicker('refresh');
            });
        }
        else if (TypeID == '21') {
            $('#inqCompQotDocuTypeID').hide();
            $('#inqCompSODocuTypeID').hide();
            $('#inqCompInvDocuTypeID').hide();
            $('#inqCompProDocuTypeID').show();
            $('#inqCompJobDocuTypeID').hide();
            $('#inqCompActDocuTypeID').hide();
            $('#inqCompInqDocuTypeID').hide();
            loadDropDownFromDatabase('/inquiry-log-api/searchDocumentsForDropdown', "", TypeID, '#proDocumentTypeID');
            $('#proDocumentTypeID').on('change', function() {
                inqProDocumentTypeID = $(this).val();
                $('#proDocumentTypeID').selectpicker('refresh');
            });
        }
        else if (TypeID == '22') {
            $('#inqCompQotDocuTypeID').hide();
            $('#inqCompSODocuTypeID').hide();
            $('#inqCompInvDocuTypeID').hide();
            $('#inqCompProDocuTypeID').hide();
            $('#inqCompJobDocuTypeID').show();
            $('#inqCompActDocuTypeID').hide();
            $('#inqCompInqDocuTypeID').hide();
            loadDropDownFromDatabase('/inquiry-log-api/searchDocumentsForDropdown', "", TypeID, '#jobDocumentTypeID');
            $('#jobDocumentTypeID').on('change', function() {
                inqJobDocumentTypeID = $(this).val();
                $('#jobDocumentTypeID').selectpicker('refresh');
            });
        }
        else if (TypeID == '23') {
            $('#inqCompQotDocuTypeID').hide();
            $('#inqCompSODocuTypeID').hide();
            $('#inqCompInvDocuTypeID').hide();
            $('#inqCompProDocuTypeID').hide();
            $('#inqCompJobDocuTypeID').hide();
            $('#inqCompActDocuTypeID').show();
            $('#inqCompInqDocuTypeID').hide();
            loadDropDownFromDatabase('/inquiry-log-api/searchDocumentsForDropdown', "", TypeID, '#actDocumentTypeID');
            $('#actDocumentTypeID').on('change', function() {
                inqActDocumentTypeID = $(this).val();
                $('#actDocumentTypeID').selectpicker('refresh');
            });
        }
        else if (TypeID == '24') {
            $('#inqCompQotDocuTypeID').hide();
            $('#inqCompSODocuTypeID').hide();
            $('#inqCompInvDocuTypeID').hide();
            $('#inqCompProDocuTypeID').hide();
            $('#inqCompJobDocuTypeID').hide();
            $('#inqCompActDocuTypeID').hide();
            $('#inqCompInqDocuTypeID').show();
            loadDropDownFromDatabase('/inquiry-log-api/searchDocumentsForDropdown', "", TypeID, '#inqDocumentTypeID');
            $('#inqDocumentTypeID').on('change', function() {
                inqInquiryDocumentTypeID = $(this).val();
                $('#inqDocumentTypeID').selectpicker('refresh');
            });
        }
    });

    $('#userHistory').on('click', function() {
        setDataToHistoryModal(customerID);
        $('#addUserHistoryModal').modal('show');

    });

    $('#search-inquiry-log-data-list').on('click', '.cus_related_docs', function() {
        setDataToHistoryModal($(this).attr('data-cus-related-id'));
        $('#addUserHistoryModal').modal('show');
    });

    $('#cus-history-table').on('click', '.document_view', function() {
        var documentId = $(this).parents('tr').attr('data-docid');
        var documentType = $(this).parents('tr').attr('data-doctype');
        $('#DocumentViewModal').modal('show');
        getPrintPreviewByDocumentTypeAndDocumentID(documentId, documentType);

    });
    $('#search-inquiry-log-data-list').on('click', '.document_view', function() {
        var documentId = $(this).parents('tr').attr('data-docid');
        var documentType = $(this).parents('tr').attr('data-doctype');
        $('#DocumentViewModal').modal('show');
        getPrintPreviewByDocumentTypeAndDocumentID(documentId, documentType);

    });

    $('#addProjectBtn').on('click', function(e) {
        e.preventDefault();
        linkProject = true;
        saveAndUpdate();
    });
    $('#addjobBtn').on('click', function(e) {
        e.preventDefault();
        linkJob = true;
        saveAndUpdate();
    });
    $('#addActivityBtn').on('click', function(e) {
        e.preventDefault();
        if ($(this).hasClass('disabled')) {
            return false;
        }
        linkActivity = true;
        saveAndUpdate();
    });
    $('#create-inquiry-log-form').on('submit', function(e) {
        e.preventDefault();
        saveAndUpdate();
    });
    function setValueforUpdate() {
        inqLogId = $('#inquiryLogReference').data('inqlogid');
        if (!customerID) {
            customerID = $('#inquiryLogCustomerID').data('cusid');
        }
        if (!inquiryTypeID) {
            inquiryTypeID = $('#inquiryTypeID').data('inqtypeid');
        }
        if (!inquiryStatusNameID) {
            inquiryStatusNameID = $('#inquiryLogStatusID').data('inqstatid');
        }
        if (!projectID) {
            projectID = $('#proRef').data('proid');
        }
        if (!jobID) {
            jobID = $('#jobRef').data('jbid');
        }
        if (!activityID) {
            activityID = $('#actRef').data('actid');
        }
    }
    $('#addProjectBtnUpdate').on('click', function(e) {
        e.preventDefault();
        linkProject = true;
        setValueforUpdate();
        saveAndUpdate();
    });
    $('#addjobBtnUpdate').on('click', function(e) {
        e.preventDefault();
        linkJob = true;
        setValueforUpdate();
        saveAndUpdate();
    });
    $('#addAndJob').on('click', function(e) {
        e.preventDefault();
        linkJob = true;
        setValueforUpdate();
        saveAndUpdate();
    });
    $('#addActivityBtnUpdate').on('click', function(e) {
        e.preventDefault();
        linkActivity = true;
        setValueforUpdate();
        saveAndUpdate();
    });
    $('#edit-inquiry-log-form').on('submit', function(e) {
        e.preventDefault();
        setValueforUpdate();
        saveAndUpdate();
    });

    function saveAndUpdate() {
        setDocumentTypesWithIds();

        if (!customerID) {
            customerID = $('#inquiryLogCustomerID').data('cusid');
        }
        var dutyManagerAutoInc = 0;
        $('.duty-manager-table tbody tr').each(function() {
            var su = $(this).data('empid');
            duManager[dutyManagerAutoInc] = su;
            dutyManagerAutoInc++;
        });
        formData = {
            inquiryLogId: inqLogId,
            cusName: customerID,
            customerProfileID: $("input[name='cProfile']:checked").parents('tr').data('cpid'),
            inquirylogType: inquiryTypeID,
            inquiryDate: $('#inquiryLogDateAndTime').val(),
            inquiryLogDescrip: $('#inquiryLogDescription').val(),
            inquiryLogUser: $('#inquirylogUser').data('userid'),
            inquiryLogReference: $('#inquiryLogReference').val(),
            inquiryLogContactNo1: $('#inquiryLogContactNo1').val(),
            inquiryLogContactNo2: $('#inquiryLogContactNo2').val(),
            inquiryLogAddress: $('#inquiryLogAddress').val(),
            inquiryLogStatus: inquiryStatusNameID,
            inquiryLogDutymanager: duManager,
            inquiryLogDutymanagerLength: duManager.length,
            inquiryComplainType: $('#inquiryComplainType').val(),
            documentTypeName: $('#documentTypeName').val(),
            documentTypeID: documentTypeID,
            datatypeListWithRefIds: datatypeListWithRefIds,
            projectID: projectID,
            jobID: jobID,
            activityID: activityID
        };
        if (validation(formData)) {
            if (inqLogId == null) {
                eb.ajax({
                    url: BASE_URL + '/inquiry-log-api/add',
                    method: 'post',
                    data: formData,
                    dataType: 'json',
                    success: function(data) {
                        if (data.status) {
                            p_notification(data.status, data.msg);
                            if (linkProject) {
                                var url = BASE_URL + '/project/create/' + customerID + '/' + $("input[name='cProfile']:checked").parents('tr').data('cpid') + '/' + data.data;
                                window.location.assign(url);
                            }
                            else if (linkJob) {
                                if (!projectID) {
                                    projectID = '0';
                                }
                                var url = BASE_URL + '/job/create/' + projectID + '/' + data.data;
                                window.location.assign(url);
                            }
                            else if (linkActivity) {
                                var url = BASE_URL + '/activity/create/' + jobID + '/' + data.data;
                                window.location.assign(url);
                            }
                            else {
                                window.setTimeout(function() {
                                    location.reload();
                                }, 1000);
                            }
                        } else {
                            p_notification(data.status, data.msg);
                        }
                    }
                });
            } else {
                eb.ajax({
                    url: BASE_URL + '/inquiry-log-api/update',
                    method: 'post',
                    data: formData,
                    dataType: 'json',
                    success: function(data) {
                        p_notification(data.status, data.msg);
                        if (data.status) {
                            if (linkProject) {
                                var url = BASE_URL + '/project/create/' + customerID + '/' + $("input[name='cProfile']:checked").parents('tr').data('cpid') + '/' + inqLogId;
                                window.location.assign(url);
                            }
                            else if (linkJob) {
                                if (!projectID) {
                                    projectID = '0';
                                }
                                var url = BASE_URL + '/job/create/' + projectID + '/' + inqLogId;
                                window.location.assign(url);
                            }
                            else if (linkActivity) {
                                var url = BASE_URL + '/activity/create/' + jobID + '/' + inqLogId;
                                window.location.assign(url);
                            }
                            else {
                                window.setTimeout(function() {
                                    window.history.back();
                                }, 1000);
                            }
                        }
                    }
                });
            }
        }
    }
    function multipleEmployee(key) {
        var flag = true;
        $('.duty-manager-table').children('tbody').find('tr').each(function() {
            if ($(this).data('empid') == key) {
                p_notification(false, eb.getMessage('ERR_DUTY_MANAGER_ALREDY_ADDED', EMPLOYE_LIST[key]));
                flag = false;
                return;
            }
        });
        if (flag) {
            $('.duty-manager-table').removeClass('hidden');
            var $newTableRow = "<tr class='emplist' data-empid='"
                    + key + "'><td>" + EMPLOYE_LIST[key]
                    + "</td>\n\
                    <td class='text-right'><a class='btn btn-default delete'><i class='pointer_cursor fa fa-trash-o'></i></a></td></tr>";
            $($('.duty-manager-table tbody')).append($newTableRow);
            $('#inquiryLogDutyManager').val('');
        }
    }
    $('.duty-manager-table').on('click', '.delete', function(e) {
        e.preventDefault();
        $(this).parents('tr').remove();
    });
    $('.customer-profiles tbody').on('click', '.cProfile', function() {
        var customerProfileID = $("input[name='cProfile']:checked").parents('tr').data('cpid');
        getCustomerProfileDetails(customerProfileID);
    });




    $(document).on('click', '.delete-inquiry-log', function(e) {
        e.preventDefault();
        var inquiryLogID = $(this).parents('tr').data('inquiry-log-id');
        bootbox.confirm('Are you sure you want to delete this Inquiry Log?', function(result) {
            if (result === true) {
                eb.ajax({
                    url: BASE_URL + '/inquiry-log-api/delete',
                    method: 'post',
                    data: {inquiryLogID: inquiryLogID},
                    dataType: 'json',
                    success: function(data) {
                        p_notification(data.status, data.msg);
                        if (data.status == true) {
                            window.location.reload();
                        }
                    }
                });
            }
        });
    });
    $('#inquiry-log-list-view').on('click', '#inq-lg-search', function(e) {
        e.preventDefault();
        var searchKey = $('#inquiry-log-search-keyword').val();
        var formData = {
            searchKey: searchKey,
        }
        if (formData.searchKey) {
            searchValue(formData);
        } else {
            p_notification(false, eb.getMessage('ERR_NO_KEY_WORD'));
        }

    });
    $('#inquiry-log-list-view').on('click', '#inq-lg-reset', function(e) {
        e.preventDefault();
        searchValue('');
        $('#inquiry-log-search-keyword').val('');
    });
    $('.reset').on('click', function() {
        window.setTimeout(function() {
            location.reload();
        }, 1000);
    });

    $('.jobEditBack').on('click', function() {
        window.history.back();
    });
    function searchValue(formData) {
        eb.ajax({
            url: BASE_URL + '/inquiry-log-api/getInquiryLogBySearchKey',
            method: 'post',
            data: formData,
            dataType: 'json',
            success: function(data) {
                if (data.status == true) {
                    if (data.status) {
                        $("#inquiry-log-list-view").html(data.html);
                    } else {
                        p_notification(data.status, data.msg);
                    }
                }
            }
        });
    }
//all documents(related to the complain) set to single array
    function setDocumentTypesWithIds() {
        var itemArrayIncr = 0;

        if (inqQuotDocumentTypeID) {
            for (var i = 0; i < inqQuotDocumentTypeID.length; i++) {
                datatypeListWithRefIds[itemArrayIncr] = [1, inqQuotDocumentTypeID[i]];
                itemArrayIncr++;
            }
        }
        if (inqSalesDocumentTypeID) {
            for (var i = 0; i < inqSalesDocumentTypeID.length; i++) {
                datatypeListWithRefIds[itemArrayIncr] = [2, inqSalesDocumentTypeID[i]];
                itemArrayIncr++;
            }
        }
        if (inqInvDocumentTypeID) {
            for (var i = 0; i < inqInvDocumentTypeID.length; i++) {
                datatypeListWithRefIds[itemArrayIncr] = [3, inqInvDocumentTypeID[i]];
                itemArrayIncr++;
            }
        }
        if (inqProDocumentTypeID) {
            for (var i = 0; i < inqProDocumentTypeID.length; i++) {
                datatypeListWithRefIds[itemArrayIncr] = [21, inqProDocumentTypeID[i]];
                itemArrayIncr++;
            }
        }
        if (inqJobDocumentTypeID) {
            for (var i = 0; i < inqJobDocumentTypeID.length; i++) {
                datatypeListWithRefIds[itemArrayIncr] = [22, inqJobDocumentTypeID[i]];
                itemArrayIncr++;
            }
        }
        if (inqActDocumentTypeID) {
            for (var i = 0; i < inqActDocumentTypeID.length; i++) {
                datatypeListWithRefIds[itemArrayIncr] = [23, inqActDocumentTypeID[i]];
                itemArrayIncr++;
            }
        }
        if (inqInquiryDocumentTypeID) {
            for (var i = 0; i < inqInquiryDocumentTypeID.length; i++) {
                datatypeListWithRefIds[itemArrayIncr] = [24, inqInquiryDocumentTypeID[i]];
                itemArrayIncr++;
            }
        }

    }

    $(document).on('click', "a.editsInactive", function(e) {
        p_notification(false, eb.getMessage('ERR_CUST_INACTIVE'));
    });

    $(document).on('click', '.edit', function(e) {
        e.preventDefault();
        var url = BASE_URL + '/inquiry-log/edit/' + this.id;
        window.location.assign(url);
    });

    $('#search-inquiry-log-list').on('click', '.link_disabled', function(e) {
        e.preventDefault();
    });

    //for load change status modal
    $(document).on('click', '.inquiryLogChangeStatus', function() {
        inquiryLodId = $(this).closest('tr').data('inquiry-log-id');
        var inquiryLogStatus = $(this).data('status');
        $('#inquiryLogStatueChangeModal').modal('show');
        $('#statusValue').val(inquiryLogStatus);
    });

    //change project status
    $(document).on('click', '#changeStatusBtn', function() {

        var inquiryLogStatus = $('#statusValue').val();
        var obj = {};
        obj.inquiryLogId = inquiryLodId;
        obj.statusId = inquiryLogStatus;

        eb.ajax({
            url: BASE_URL + '/inquiry-log-api/change-inquiry-log-status',
            method: 'post',
            data: obj,
            dataType: 'json',
            success: function(data) {
                if (data.status == true) {
                    var label = null;
                    var $selectedTr = $('tr[data-inquiry-log-id="' + inquiryLodId + '"]');
                    switch (inquiryLogStatus) {
                        case '3':
                            label = '<span class="label label-success">Open</span>';
                            break;
                        case '4':
                            label = '<span class="label label-danger">Closed</span>';
                            //disable btns
                            $selectedTr.find('.edit').removeClass('edit').addClass('disabled');
                            $selectedTr.find('.delete-inquiry-log').removeClass('delete-inquiry-log').addClass('disabled');
                            $selectedTr.find('.inquiryLogChangeStatus').removeClass('inquiryLogChangeStatus').addClass('disabled');
                            break;
                        default :
                            console.log('invalid option');
                    }
                    $('#inquiryLogStatueChangeModal').modal('hide');
                    //change label
                    $selectedTr.find('.jState').html(label);
                    //change data attribute
                    $selectedTr.find('.inquiryLogChangeStatus').data('status', inquiryLogStatus);
                }
            }
        });
    });
});

function setDataToHistoryModal(customerID) {
    $('#cus-history-table tbody tr').remove();
    $('#cus-history-table tfoot div').remove();
    $('#cus-history-table tbody div').remove();

    eb.ajax({
        type: 'POST',
        url: BASE_URL + '/inquiry-log-api/getAllDocumentDetailsByCustomerID',
        data: {customerID: customerID},
        success: function(respond) {
            if (respond.status == true) {
                $('#cus-history-table thead tr').removeClass('hidden');
                $.each(respond.data, function(index, value) {
                    if (value != null) {
                        $.each(value, function(index2, value2) {
                            if (value2['type'] == 'project') {
                                var tableBody = "<tr data-docId=" + value2['documentID'] + " data-doctype=" + value2['type'] + "><td class='' >" + value2['type'] + "</td><td>" + value2['code'] + "</td><td>" + value2['amount'] + "</td><td colspan='2'>" + value2['issuedDate'] + "</td></tr>";
                                $('#cus-history-table tbody').append(tableBody);
                            } else {
                                tableBody = "<tr data-docId=" + value2['documentID'] + " data-doctype=" + value2['type'] + "><td class='' >" + value2['type'] + "</td><td>" + value2['code'] + "</td><td>" + value2['amount'] + "</td><td>" + value2['issuedDate'] + "</td><td class='text-center'><span class='glyphicon glyphicon-eye-open openlist link_color document_view ' style='cursor:pointer'></span></td></tr>";
                                $('#cus-history-table tbody').append(tableBody);
                            }
                        });
                    }
                });
                var footer = " <div class='col-lg-12 col-md-12 col-sm-12 text-right'><button type='button' class='btn btn-primary col-lg-12 col-md-12 col-sm-12' data-dismiss='modal' aril-hidden='true'>Back</button></div>";
                $('#cus-history-table tfoot').append(footer);
            } else {
                $('#cus-history-table thead tr').addClass('hidden');
                var noDataFooter = "<div class= ' col-lg-9 text-right'><label><b>No Data</b></label>";
                $('#cus-history-table tbody').append(noDataFooter);
            }
        }
    });
}

function getPrintPreviewByDocumentTypeAndDocumentID(documentID, documentType) {
    $('#cus-document-view div').remove();
    var URL;
    if (documentType == 'quotation') {
        URL = BASE_URL + '/quotation/document/' + documentID;
    } else if (documentType == 'salesOrder') {
        URL = BASE_URL + '/salesOrders/document/' + documentID;
    }
    else if (documentType == 'salesOrder') {
        URL = BASE_URL + '/salesOrders/document/' + documentID;
    }
    else if (documentType == 'invoice') {
        URL = BASE_URL + '/invoice/document/' + documentID;
    }
    else if (documentType == 'payment') {
        URL = BASE_URL + '/customerPayments/document/' + documentID;
    }
    else if (documentType == 'salesReturn') {
        URL = BASE_URL + '/return/document/' + documentID;
    }
    else if (documentType == 'project') {
        URL = BASE_URL + '/project/document/' + documentID;
    }
    else if (documentType == 'job') {
        URL = BASE_URL + '/job/document/' + documentID;
    }
    else if (documentType == 'activity') {
        URL = BASE_URL + '/activity/view/' + documentID;
    }
    else if (documentType == 'inquiryLog') {
        URL = BASE_URL + '/inquiry-log/view/' + documentID;
    }

    eb.ajax({
        type: 'POST',
        url: URL,
        success: function(respond) {
            var division = "<div></div>";
            $('#cus-document-view').append(division);
            if (documentType == 'inquiryLog' || documentType == 'activity') {
                $('#cus-document-view div').append(respond.html);
            } else {
                $('#cus-document-view div').append(respond);
            }
        }
    });

}

function getCustomerDetails(custID) {
    setCustomerProfiles(custID);
}


function setCustomerProfiles(customerID, callback, profileId) {
    eb.ajax({
        type: 'POST',
        url: BASE_URL + '/customerAPI/getCustomerProfilesByCustomerID',
        data: {customerID: customerID},
        success: function(respond) {
            if (respond.status) {
                $('.customer-profiles tbody tr').remove();
                if (!(respond.data['customerDetails'][customerID].customerOther == '' || respond.data['customerDetails'][customerID].customerOther == null)) {
                    p_notification('cust details', respond.data['customerDetails'][customerID].customerOther, '', 3000000);
                }
                $.each(respond.data['customerProfiles'], function(index, value) {
                    $('.customer-profiles').removeClass('hidden');
                    if (value == 'Default') {
                        var $newDiv = "<tr class='customer-profile-list' data-cpid='" + index + "'><td>" + value + "</td><td class='text-center'><input type='radio' name='cProfile' class='cProfile' checked='true'/></td></tr>";
                        var customerProfileID = index;
                    } else {
                        $newDiv = "<tr class='customer-profile-list' data-cpid='" + index + "'><td>" + value + "</td><td class='text-center'><input type='radio' name='cProfile' class='cProfile'/></td></tr>";
                    }
                    $($('.customer-profiles tbody')).append($newDiv);
                    getCustomerProfileDetails(customerProfileID);
                });
                var rows = document.getElementById("cus-prof-table").getElementsByTagName("tbody")[0].getElementsByTagName("tr").length;
                if (rows == '1') {
                    $('.cProfile').attr('checked', true);
                    var customerProfileID = $("input[name='cProfile']:checked").parents('tr').data('cpid');
                    getCustomerProfileDetails(customerProfileID);
                }

                $('#inquiryLogCustomerID').
                        append($("<option></option>").
                                attr("value", customerID).
                                text(respond.data['customerDetails'][customerID].customerName + "-" + respond.data['customerDetails'][customerID].customerCode));
                $('#inquiryLogCustomerID').val(customerID).attr('data-cusid', customerID);
                $('#inquiryLogCustomerID').selectpicker('refresh');

            }
            if (callback) {
                callback(profileId);
            }
        },
    });
}
function getCustomerProfileDetails(ProfileID) {

    eb.ajax({
        type: 'POST',
        url: BASE_URL + '/customerAPI/getCustomerProfileDataByCustomerProfileID',
        data: {customerProfileID: ProfileID},
        success: function(respond) {
            if (respond.status) {
                $('#inquiryLogContactNo1').val(respond.data['firstPhoneNumber']);
                $('#inquiryLogContactNo2').val(respond.data['customerProfileLandTP1']);
                $('#inquiryLogAddress').val(respond.data['addressLine']);
            } else {
                $('#inquiryLogContactNo1').val('');
                $('#inquiryLogContactNo2').val('');
                $('#inquiryLogAddress').val('');
            }

        }
    });
}

function validation(formData) {

    if (formData.inquiryLogDescrip == '') {
        p_notification(false, eb.getMessage('ERR_INQ_LOG_DESCRIPTION_CANT_BE_NULL'));
        $('#inquiryLogDescription').focus();
        return false;
    } else if (formData.cusName == null) {
        p_notification(false, eb.getMessage('ERR_CUS_NAME_CANT_BE_NULL'));
        $('#inquiryLogCustomerID').focus();
        return false;
    }
    else if (!$("input[name='cProfile']").is(':checked')) {
        p_notification(false, eb.getMessage('ERR_CUST_PROF_NOT_SELECT'));
        $('#inquiryLogCustomerID').focus();
        return false;
    }
    else if (formData.inquiryLogContactNo1 == '' && formData.inquiryLogContactNo2 == '') {
        p_notification(false, eb.getMessage('ERR_CONTACT_NUM_CANT_BE_NULL'));
        $('#inquiryLogContactNo1').focus();
        return false;
    }
    else if (!isPhoneValid(formData.inquiryLogContactNo1)) {
        p_notification(false, eb.getMessage('ERR_TP_NUM'));
        $('#inquiryLogContactNo1').focus();
        return false;
    }
    else if (!isPhoneValid(formData.inquiryLogContactNo2)) {
        p_notification(false, eb.getMessage('ERR_TP_NUM'));
        $('#inquiryLogContactNo2').focus();
        return false;
    }
    else if (!formData.inquirylogType) {
        p_notification(false, eb.getMessage('ERR_INQ_TYPE_CANT_BE_NULL'));
        $('#inquiryTypeID').focus();
        return false;
    }
    else if (!formData.inquiryLogStatus) {
        p_notification(false, eb.getMessage('ERR_INQ_STATUS_CANT_BE_NULL'));
        $('#inquiryLogStatusID').focus();
        return false;
    }
    else {
        return true;
    }
}
var isPhoneValid = function(phoneNumber) {
    isValid = true;
    if (phoneNumber.length != 0) {
        if (isNaN(phoneNumber) || phoneNumber.length < 9 || phoneNumber.length > 15) {
            isValid = false;
        }
    }
    return isValid;
}
function setActivityReference(jobID) {
    eb.ajax({
        type: 'POST',
        url: BASE_URL + '/api/activity/getActivitiesByJobID',
        data: {jobID: jobID},
        success: function(respond) {
            if (respond.data['activityData'] && respond.data != 'undifine') {
                $.each(respond.data['activityData'], function(index, value) {
                    var abc = "<option value='" + index + "'>" + value + "</option>";
                    $('#actRef').append(abc);

                });
                $('#actRef').selectpicker('refresh');
            }

        }

    });
}
function setJobReference(projectID) {
    eb.ajax({
        type: 'POST',
        url: BASE_URL + '/job-api/getJobsByProjectID',
        data: {projectID: projectID},
        success: function(respond) {
            if (respond.data) {
                $.each(respond.data, function(index, value) {
                    var abc = "<option value='" + index + "'>" + value + "</option>";
                    $('#jobRef').append(abc);
                });
                $('#jobRef').selectpicker('refresh');
            }
        }

    });
}

function getCustomerData() {
    if (CUSTOMERS_NAMES == undefined) {
        eb.ajax({
            type: 'POST',
            url: BASE_URL + '/vars.js/customerListWithoutDefaultCustomer',
            data: {},
            success: function(respond) {
                CUSTOMERS_NAMES = respond.data['CUSTOMERS_NAMES'];
            }
        });
    }

}
