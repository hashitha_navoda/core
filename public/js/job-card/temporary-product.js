$(function() {
    // Change this to the location of your server-side upload handler:
    var url = BASE_URL + '/api/activity/save-image';
    $('#fileupload').fileupload({
        url: url,
        dataType: 'json',
        autoUpload: true,
        acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i,
        maxFileSize: 5000000, // 5 MB
        // Enable image resizing, except for Android and Opera,
        // which actually support image resizing, but fail to
        // send Blob objects via XHR requests:
        disableImageResize: /Android(?!.*Chrome)|Opera/
                .test(window.navigator.userAgent),
        previewMaxWidth: 100,
        previewMaxHeight: 100,
        previewCrop: true,
        dropZone: $('#tempProductModal')
    }).on('fileuploadadd', function(e, data) {
        $('#progress').show();

        // create delete button for each image
        var deleteIcon = $('<a>').addClass('info deleteImage');
        var deleteDiv = $('<div/>').addClass('mask second-effect');
        var deleteSet = $(deleteDiv).wrapInner(deleteIcon);

        data.context = $('<div/>').addClass('col-lg-4 imageContainer').appendTo('#files');
        data.myContext = data.context;
        $.each(data.files, function(index, file) {
            var node = $('<div/>').addClass(' view second-effect');

            node.append(deleteSet);
            node.appendTo(data.context).append($('<span/>').addClass('fileName').text(file.name));
        });
    }).on('fileuploadprocessalways', function(e, data) {
        var index = data.index,
                file = data.files[index],
                node = $(data.context.children()[index]);
        if (file.preview) {
            node.prepend(file.preview);
        }
        if (file.error) {
            node.append($('<span class="text-danger"/>').text(file.error));
        }
        if (index + 1 === data.files.length) {
            data.context.find('button')
                    .text('Upload')
                    .prop('disabled', !!data.files.error);
        }
    }).on('fileuploadprogressall', function(e, data) {
        var progress = parseInt(data.loaded / data.total * 100, 10);
        $('#progress .progress-bar').css('width', progress + '%');

        if (progress = 100) {
            $('#progress').fadeOut(2000);
        }
    }).on('fileuploaddone', function(e, data) {
        $.each(data.result.data.files, function(index, file) {
            if (file.url) {

                var child = $(data.myContext.children()[index]);
                child.find("span[class='fileName']").text(decodeURI(file.fileName));
                child.find(".deleteImage").prop('href', file.deleteAction);
                child.find(".deleteImage").attr('data-folderPath', file.folderPath);
                child.find(".deleteImage").attr('data-fileName', file.fileName);
                temporaryProductImages[file.folderPath + file.fileName] = {
                    imgName: file.fileName,
                    imgFolderPath: file.folderPath,
                    imgSize: file.size
                };
            } else if (file.error) {
                var error = $('<span class="text-danger"/>').text(file.error);
                $(data.context.children()[index]).append(error);
            }
        });
        return false;
    }).on('fileuploadfail', function(e, data) {
        $.each(data.files, function(index) {
            var error = $('<span class="text-danger"/>').text('File upload failed.');
            $(data.context.children()[index]).append(error);
        });
    }).prop('disabled', !$.support.fileInput)
            .parent().addClass($.support.fileInput ? undefined : 'disabled');

// remove image from modal
    $("#addTempProductModal").on("click", '.deleteImage', function(e) {
        e.preventDefault();
        var $deleteImageElement = $(this);
        var folderPath = $deleteImageElement.attr('data-folderPath');
        var fileName = $deleteImageElement.attr('data-fileName');
        fileName = decodeURIComponent(fileName);
        eb.ajax({
            type: 'POST',
            url: $deleteImageElement.attr('href'),
            data: {
                folderPath: folderPath,
                fileName: fileName
            },
            success: function(respond) {
                if (respond.data[fileName]) {
                    $deleteImageElement.parents('.imageContainer').remove();
                    delete temporaryProductImages[folderPath + fileName];
                } else {
                    p_notification(respond.data[fileName], fileName + ' Couldn\'t Deleted')
                }
            }
        });
        return false;
    });
});