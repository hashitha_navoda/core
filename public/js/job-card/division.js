$(function() {
    var divisionID;
    var divisionFlag = false;
    $('#division-list').on('click', 'a.edit', function(e) {
        e.preventDefault();
        $('.add').addClass('hidden');
        $('.update').removeClass('hidden');
        divisionID = $(this).parents('tr').data('divisionid');
        $('#divisionName').val($(this).parents('tr').find('.diviName').text());
        divisionFlag = true;
    });
    $('#create-division-form').on('submit', function(e) {
        e.preventDefault();
        if (divisionFlag) {
            update();
        } else {
            add();
        }
    });

    $('#division-list').on('click', 'a.status', function() {
        var msg;
        var status;
        var typeFlag = true;
        var divisionID = $(this).parents('tr').data('divisionid');
        var currentDiv = $(this).contents();
        if ($(this).children().hasClass('fa-check-square-o')) {
            msg = 'Are you sure you want to Deactivate this division';
            status = '0';
        }
        else if ($(this).children().hasClass('fa-square-o')) {
            msg = 'Are you sure you want to Activate this division';
            status = '1';
        }
        activeInactiveType(divisionID, status, currentDiv, msg, typeFlag);
    });

    $('#division-list').on('click', 'a.delete', function(e) {
        e.preventDefault();
        divisionID = $(this).parents('tr').data('divisionid');
        var currentDiv = $(this).parents('tr').find('a.status').contents();
        var typeFlag = false;
        var msg = 'This division cannot be deleted. Do you want to de-activate this division';
        var formData = {
            divisionID: divisionID,
        };
        bootbox.confirm('Are you sure you want to delete this Division?', function(result) {
            if (result === true) {
                eb.ajax({
                    url: BASE_URL + '/division_api/delete',
                    method: 'post',
                    data: formData,
                    dataType: 'json',
                    success: function(data) {
                        if (data.status == true) {
                            p_notification(data.status, data.msg);
                            window.location.reload();
                        } else if (data.status == false && data.data == 'deactive') {
                            activeInactiveType(divisionID, '0', currentDiv, msg, typeFlag);
                        } else {
                            p_notification(data.status, data.msg);
                        }
                    }
                });
            }
        });
    });

    $('#division-search').on('click', '#div-search', function(e) {
        e.preventDefault();
        searchKey = $('#division-search-keyword').val();
        var formData = {
            searchKey: searchKey,
        }
        if (formData.searchKey) {
            serchValue(formData);
        } else {
            p_notification(false, eb.getMessage('ERR_NO_KEY_WORD'));
        }

    });

    $('#division-search').on('click', '.reset', function(e) {
        e.preventDefault();
        serchValue('');
        $('#division-search-keyword').val('');
    });

    function validateFormData(formData) {

        if (formData.division == '') {
            p_notification(false, eb.getMessage('ERR_DIV_NAME_CNT_BE_NULL'));
            $('#divisionName').focus();
            return false;
        }
        return true;
    }
    function serchValue(searchKey) {
        eb.ajax({
            url: BASE_URL + '/division_api/getDivisionBySearchKey',
            method: 'post',
            data: searchKey,
            dataType: 'json',
            success: function(data) {
                if (data.status == true) {
                    if (data.status) {
                        $("#division-list").html(data.html);
                    } else {
                        p_notification(data.status, data.msg);
                    }
                }
            }
        });
    }
    //this function use to Send data to the update action in to the division api controller
    function update() {
        var formData = {
            division: $('#divisionName').val(),
            divisionID: divisionID,
        };
        if (validateFormData(formData)) {
            eb.ajax({
                url: BASE_URL + '/division_api/update',
                method: 'post',
                data: formData,
                dataType: 'json',
                success: function(data) {
                    p_notification(data.status, data.msg);
                    window.setTimeout(function() {
                        location.reload();
                    }, 1000);
                }
            });
        }
    }
    //this function use to Send data to the add action in to the division api controller
    function add() {
        var formData = {
            division: $('#divisionName').val(),
            divisionID: $('#divisionID').val(),
        };
        if (validateFormData(formData)) {
            eb.ajax({
                url: BASE_URL + '/division_api/add',
                method: 'post',
                data: formData,
                dataType: 'json',
                success: function(data) {
                    p_notification(data.status, data.msg);
                    if (data.status) {
                        window.setTimeout(function() {
                            location.reload();
                        }, 1000);
                    } else {
                        if (data.data == "DIVISION_EXIST") {
                            $('#divisionName').focus();
                        }
                    }
                }
            });
        }
    }
    /**
     *
     * use to change active state
     */
    function activeInactiveType(type, status, currentDiv, msg, typeFlag) {
        bootbox.confirm(msg, function($result) {
            if ($result == true) {
                eb.ajax({
                    url: BASE_URL + '/division_api/changeStatusID',
                    method: 'post',
                    dataType: 'json',
                    data: {
                        'divisionID': type,
                        'status': status,
                    },
                    success: function(data) {
                        if (data.status) {
                            p_notification(data.status, data.msg);
                            if (currentDiv.hasClass('fa-square-o') && typeFlag) {
                                currentDiv.addClass('fa-check-square-o');
                                currentDiv.removeClass('fa-square-o');

                            }
                            else {
                                currentDiv.addClass('fa-square-o');
                                currentDiv.removeClass('fa-check-square-o');
                            }
                        } else {
                            p_notification(data.status, data.msg);
                        }
                    }
                });
            }
        });
    }

});


