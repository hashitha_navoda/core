$(document).ready(function () {

    var mobileNumArray = [];
    var emailAddArray = [];
    var conID = null;
    var editMode = false;
    var contactArray = [];
    var contactObj = {};
    var conListID = null;
    var contactListID = null;
    var customerID;
    var editflag = 0;
    var invoice = false;
    var v;
    var ff = new Array();
    var columns;
    var gender = null;
    var custupdatemass = [];
    var profileID = 0;
    var customerProfileDataArray = {};
    var $clonedCustomerProfileDisplay;
    var nameregex = /^[\w\-\s]+$/;
    var searchkey;
    var cusID;
    var EDIT_MODE = false;
    var updatedCustomerProfiles = {};
    var selectedFieldArr = [];

    function loadContact(contactID) {
        searchUrl = BASE_URL + '/contactsAPI/getContactJsonFromSearchByContactID';
        var searchRequest = eb.post(searchUrl, {contactID: contactID});
        searchRequest.done(function(respond) {
            if (!respond.status) {
                p_notification(searchdata.status, searchdata.msg)
            }

            if (respond.data) {
                promoCustomersList = respond.data;
                $('#contactListTable').removeClass('hidden');
                $('#contactListTable').find(".promoContact").remove();

                for (var i in respond.data) {
                    var $customerSample = $('#contactSampleRow', '#contactListTable').clone();
                    $customerSample.find('.contactName').html(respond.data[i].name);
                    $customerSample.find('.contactDesignation').html(respond.data[i].designation);
                    $customerSample
                        .find(".contactPhoneNumber")
                        .html(respond.data[i].mobileNumber);
                    $customerSample.find('.contactEmail').html(respond.data[i].emailAddress);
                    $customerSample.find("input[name='contact[]']").val(respond.data[i].contactID);
                    if(contactArray.includes(respond.data[i].contactID)){
                        $customerSample.find("input[name='contact[]']").prop('checked', true);
                    }
                    $customerSample.attr('id', '');
                    $customerSample.addClass('promoContact');
                    $customerSample.removeClass('hidden');
                    $customerSample.removeClass('hidden');
                    $customerSample.insertBefore($('#contactSampleRow', '#contactListTable'));
                }
            } else {
                $('#contactListTable').addClass('hidden');
                $('#contactListTable').find(".promoContact").remove();
                p_notification('info', eb.getMessage('ERR_PROMO_EMAIL_CUST_LIST'));
            }

        });
    }
    loadContact(null);

    //for save dispatch note
    $(document).on('click', '#add-contact', function () {
        $('#listCode').val('');
        $('#listName').val('');
        contactArray = [];
        loadContact();
        $('.well_title').html("Add New Contact List"); 

        $('#saveButton').removeClass('hidden');
        $('#updateButton').addClass('hidden');
        $('#listCode').prop( "disabled", false );

        $('.createContactDiv').removeClass('hidden');
        $('.viewContactDiv').addClass('hidden');

        $("#contactViewDiv").removeClass("hidden");
        $("#csvUploadDiv").addClass("hidden");
        $("#btnSelectFromContact").addClass("hidden");

        $("#btnSelectFromContact").addClass("hidden");
        $("#btnCsvUpload").removeClass("hidden");

        $("#importListDiv").addClass("hidden");
        $("#importMappingDiv").addClass("hidden");
    });


    $(document).on('click', '#cancelbutton', function () {
        $('.viewContactDiv').removeClass('hidden');
        $('.createContactDiv').addClass('hidden');
    });

    $('#numberTable').on('click', '.addNum', function(e) {
        e.preventDefault();
        var $row = $(this).parents('tr');
        if (validateNumTypeRow($row)) {
            $row.removeClass('editable');

            $temp = {
                'numberTypeId' : $row.find('.numberTypeId').val(),
                'number': $row.find('.mobileNo').val()
            };

            mobileNumArray.push($temp);
            $row.find('.numberTypeId').attr('disabled', true);
            $row.find('.mobileNo').attr('disabled', true);
            $row.find('.addNum').addClass('hidden');
            $row.find('.deleteNum').removeClass('hidden');
            $row.addClass('added-num');
            var $cloneRow = $($('.sample').clone()).removeClass('sample hidden').addClass('documentRow editable');

            $cloneRow.find('.numberTypeId').selectpicker().attr('data-liver-search', true);
            $cloneRow.insertBefore($row);

            updateNumTypeDropDown();
        }
    });

     function validateNumTypeRow($row) {
        var flag = true;

        var numberTypeId = $row.find('.numberTypeId').val();
        console.log(numberTypeId);
        var number = $row.find('.mobileNo').val();
        var phoneno = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;

        if (number == '') {
            p_notification(false, eb.getMessage('ERR_CONTACT_VALID_PRI_TEL'));
            return false;
        }

        if (numberTypeId == '' || numberTypeId == null) {
            p_notification(false, eb.getMessage('ERR_CONTACT_NUM_TYPE'));
            return false;
        }

        if (number != '' && !phoneno.test(number)) {
             p_notification(false, eb.getMessage('ERR_CONTACT_VALID_TEL_NUM'));
             return false;

        }
        var dupVals = [];
        var dupType = [];
        $.each(mobileNumArray, function(index, value) {
            if (value.number == number) {
                dupVals.push(value.number);
            }

            if (value.numberTypeId == numberTypeId) {
                dupType.push(value.number);
            }
        });

        if (dupVals.length != 0) {
            p_notification(false, eb.getMessage('ERR_CONTACT_NO_DUP'));
            return false;
        }

        if (dupType.length != 0) {
            p_notification(false, eb.getMessage('ERR_CONTACT_TYPE_DUP'));
            return false;
        }

        return flag;
    }

    $('#numberTable').on('click', '.deleteNum', function(e) {
        e.preventDefault();
        var $row = $(this).parents('tr');
        var numberTypeId = $row.find('.numberTypeId').val();
        //this is for remove documentTypeID from the documentType array
        mobileNumArray = jQuery.grep(mobileNumArray, function(value) {
            return value.numberTypeId != numberTypeId;
        });
        $row.remove();
        updateNumTypeDropDown();
    });

    $('#clear-select-contact').on('click', function() {
        loadContact(null);
        $('#contactSearch').val('').trigger('change');
    });


    $('#emailTable').on('click', '.addEmail', function(e) {
        e.preventDefault();
        var $row = $(this).parents('tr');
        if (validateEmailTypeRow($row)) {
            $row.removeClass('editable');

            $temp = {
                'emailTypeId' : $row.find('.emailTypeId').val(),
                'address': $row.find('.emailAdd').val()
            };

            emailAddArray.push($temp);
            $row.find('.emailTypeId').attr('disabled', true);
            $row.find('.emailAdd').attr('disabled', true);
            $row.find('.addEmail').addClass('hidden');
            $row.find('.deleteEmail').removeClass('hidden');
            $row.addClass('added-email');
            var $cloneRow = $($('.sample-email').clone()).removeClass('sample-email hidden').addClass('emailRow editable');

            $cloneRow.find('.emailTypeId').selectpicker().attr('data-liver-search', true);
            $cloneRow.insertBefore($row);

            updateEmailTypeDropDown();
        }
    });

     function validateEmailTypeRow($row) {
        var flag = true;

        var emailTypeId = $row.find('.emailTypeId').val();
        var address = $row.find('.emailAdd').val();
        var emailRegex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;

        if (address == '') {
            p_notification(false, eb.getMessage('ERR_CONTACT_EMAIL'));
            return false;
        }

        if (emailTypeId == '' || emailTypeId == null ) {
            p_notification(false, eb.getMessage('ERR_CONTACT_EMAIL_TYPE'));
            return false;
        }

        if (address != '' && !emailRegex.test(address)) {
             p_notification(false, eb.getMessage('ERR_CONTACT_VALID_EMAIL'));
             return false;

        }
       
        var dupVals = [];
        $.each(emailAddArray, function(index, value) {
            if (value.address == address) {
                dupVals.push(value.address);
            }
        });

        if (dupVals.length != 0) {
            p_notification(false, eb.getMessage('ERR_CONTACT_EMAIL_DUP'));
            return false;
        }

        return flag;
    }

    $(document).on('click', '#saveButton', function () {
        contactObj.listCode = $('#listCode').val();
        contactObj.listName = $('#listName').val();
        contactObj.telNumbers = contactArray;
        
        if(validateContactListData(contactObj)){
            eb.ajax({
                type: 'POST',
                url: BASE_URL + '/contactsAPI/createContactList',
                data: contactObj,
                success: function (respond) {
                    p_notification(respond.status, respond.msg);
                    if(respond.status){
                        resetFields();

                        if(respond.status){
                            $('#contactListsList').html(respond.html);
                            $('#contactListSearch').val('').trigger('change');
        
                            window.setTimeout(function() {
                                $('.viewContactDiv').removeClass('hidden');
                                $('.createContactDiv').addClass('hidden');
                            }, 1000);
    
                        }
                    }
                }
            });
        }
    });

    loadDropDownFromDatabase('/contactsAPI/search-contacts-for-dropdown', "", '', '#contactSearch');
    $('#contactSearch').selectpicker('refresh');
    $('#contactSearch').on('change', function() {
        if ($(this).val() > 0 && conID != $(this).val()) {
            conID =  $(this).val() == 0 ? null : $(this).val();
            loadContact(conID);
        }
        return false;
    });
   
    function resetFields() {
        $('#listCode').val('');
        $('#listName').val('');
        $('#contactSearch').val('').trigger('change');
        $('input[type=checkbox][name=selectAll]').prop("checked", false);
        
        $('.promoCustomerSelect').each(function() {
            if (!$(this).parents('tr').hasClass('hidden')) {    
                if ($(this).is(':checked')) {
                    $(this).prop("checked", false);
                }
            }
        });
        contactArray = [];
    }

    function validateContactListData(input) {
        if (input.listCode == '') {
            p_notification(false, eb.getMessage('ERR_CONTACT_VALID_LIST_CODE'));
            $("input[name='listCode']").focus();
            return false;
        } else if (input.listName == '') {
            p_notification(false, eb.getMessage('ERR_CONTACT_VALID_LIST_NAME'));
            $("input[name='listName']").focus();
            return false;
        } else if (input.telNumbers.length == 0) {
            p_notification(false, eb.getMessage('ERR_CONTACT_LIST_NUM_EMPTY'));
            return false;
        }  else {
            return true;
        }
    }

    $(document).on('click', '.edit-contact', function (e){
        e.preventDefault();
        var $row = $(this).parents('tr');

        contactListID = $row.attr('data-contact-id');
        eb.ajax({
            type: 'POST',
            url: BASE_URL + '/contactsAPI/getContactListDetailsById',
            data: {contactListID: contactListID,isRequestContact:0},
            success: function (respond) {

                if (respond.status == true) {
                    editMode = true;
                    setEditModeDetails(respond.data);
                }
            }
        });
    });

    $(document).on('click', '.delete-contact-list', function (e){
        e.preventDefault();
        var $row = $(this).parents('tr');

        contactListID = $row.attr('data-contact-id');

        bootbox.confirm('Are you sure you want to remove this contact list?', function(result) {
            if (result == true) {
                eb.ajax({
                    type: 'POST',
                    url: BASE_URL + '/contactsAPI/deleteContactListById',
                    data: {contactListID: contactListID},
                    success: function (respond) {
                        p_notification(respond.status, respond.msg)
                        if (respond.status == true) {
                            window.location.reload();
                        }
                    }
                });
            }
        });   
    });

    $(document).on('click', '#updateButton', function () {

        contactObj.listCode = $('#listCode').val();
        contactObj.listName = $('#listName').val();
        contactObj.telNumbers = contactArray;
        contactObj.contactListID = contactListID;

        if(validateContactListData(contactObj)){
            eb.ajax({
                type: 'POST',
                url: BASE_URL + '/contactsAPI/updateContactList',
                data: contactObj,
                success: function (respond) {
                    p_notification(respond.status, respond.msg);

                    if(respond.status){
                        $('#contactListsList').html(respond.html);
                        $('#contactListSearch').val('').trigger('change');
    
                        window.setTimeout(function() {
                            $('.viewContactDiv').removeClass('hidden');
                            $('.createContactDiv').addClass('hidden');
                        }, 1000);

                    }
                }
            });
        }
    });

    $(document).on('change', '.promoCustomerSelect', function () {
        if($(this).is(":checked")){
            contactArray.push($(this).val());
        }
        else if($(this).is(":not(:checked)")){
            var index = contactArray.indexOf($(this).val());
            if (index !== -1) {
                contactArray.splice(index, 1);
            }
        }
    });

    $('input[type=checkbox][name=selectAll]').change(function() {
        if ($(this).is(':checked')) {
          $('.promoCustomerSelect').each(function() {
              if (!$(this).parents('tr').hasClass('hidden')) {    
                if (!$(this).is(':checked')) {
                    $(this).prop("checked", true);
                    contactArray.push($(this).val());
                }
              }
            });
        }
        else {
            $('.promoCustomerSelect').each(function() {
              if (!$(this).parents('tr').hasClass('hidden')) {    
                if ($(this).is(':checked')) {
                    $(this).prop("checked", false);
                    var index = contactArray.indexOf($(this).val());
                    if (index !== -1) {
                        contactArray.splice(index, 1);
                    }
                    
                }
              }
            });
        }
    });

    $(document).on('click', '.view-list', function () {
        var $row = $(this).parents('tr');
        setDataToHistoryModal($row.attr('data-contact-id'));
        $('#contactListViewModal').modal('show');
    });

    $(document).on('click', '#btnAddNewContact', function(){
        $('#contactCreateViewModal').modal('show');
    });

    loadDropDownFromDatabase('/contactsAPI/searchContactsListForDropdown', "", '', '#contactListSearch');
    $('#contactListSearch').selectpicker('refresh');
    $('#contactListSearch').on('change', function() { 
        if ($(this).val() > 0 && conListID != $(this).val()) {
            
            conListID = $(this).val();
            searchUrl = BASE_URL + '/contactsAPI/getCustomerListFromSearchByContactListID';
            var searchRequest = eb.post(searchUrl, {contactListID: conListID});
            searchRequest.done(function(searchData) {
                if (!searchData.status) {
                    p_notification(searchData.status, searchData.msg)
                }
                $('#contactListsList').html(searchData.html);
            });
        }
        return false;
    });

    $('#clear-search').on('click', function() {

        searchUrl = BASE_URL + '/contactsAPI/getCustomerListFromSearchByContactListID';
        var searchRequest = eb.post(searchUrl, {contactListID: null});
        searchRequest.done(function(searchData) {
            if (!searchData.status) {
                p_notification(searchData.status, searchData.msg)
            }
            $('#contactListsList').html(searchData.html);
            $('#contactListSearch').val('').trigger('change');
        });
    });

    function setEditModeDetails(data) {

        if (editMode) {
            $('#listCode').val(data.code);
            $('#listName').val(data.name);
            $('#listCode').prop( "disabled", true );
            contactArray = data.numbers;
            loadContact();
            $('.well_title').html("Update Contact List"); 

            $('#saveButton').addClass('hidden');
            $('#updateButton').removeClass('hidden');
            $('#saveAndCreateButton').addClass('hidden');
            $('.viewContactDiv').addClass('hidden');
            $('.createContactDiv').removeClass('hidden');

            $("#contactViewDiv").removeClass("hidden");
          
            $("#btnSelectFromContact").addClass("hidden");
            $("#btnCsvUpload").addClass("hidden");

            $("#importListDiv").addClass("hidden");
            $("#csvUploadDiv").addClass("hidden");
            $("#importMappingDiv").addClass("hidden");
        }

    }

    function setDataToHistoryModal(contactListId) {
        eb.ajax({
            type: 'POST',
            url: BASE_URL + '/contactsAPI/getContactListDetailsById',
            data: {contactListID: contactListId,isRequestContact:1},
            success: function(respond) {
                if (respond.data) {
                    $('#lblContactListCode').html(respond.data.code);
                    $('#lblContactListName').html(respond.data.name);

                    if(respond.data.contactList){
                        $('#contactTable').removeClass('hidden');
                        $('#contactTable').find(".promoContact").remove();
                        for (var i in respond.data.contactList) {
                            var $customerSample = $('#conSampleRow', '#contactTable').clone();
                            $customerSample.find('.contactName').html(respond.data.contactList[i].name);
                            $customerSample.find('.contactDesignation').html(respond.data.contactList[i].designation);
                            $customerSample
                                .find(".contactPhoneNumber")
                                .html(respond.data.contactList[i].mobileNumber);
                            $customerSample.find('.contactEmail').html(respond.data.contactList[i].emailAddress);
                            
                            $customerSample.attr('id', '');
                            $customerSample.addClass('promoContact');
                            $customerSample.removeClass('hidden');
                            $customerSample.insertBefore($('#conSampleRow', '#contactTable'));
                            
                        }
                    }
                } else {
                    $('#contactTable').addClass('hidden');
                    $('#contactTable').find(".promoContact").remove();
                    p_notification('info', eb.getMessage('ERR_PROMO_EMAIL_CUST_LIST'));
                }
            }
        });
    }

    $(document).on('click', '#cancelButtonContact', function(){
        resetMobileNum();
        resetEmail();
        resetFields();
        $('#contactCreateViewModal').modal('hide');
    });

    $('#emailTable').on('click', '.deleteEmail', function(e) {
        e.preventDefault();
        var $row = $(this).parents('tr');
        var emailTypeId = $row.find('.emailTypeId').val();
        //this is for remove documentTypeID from the documentType array
        emailAddArray = jQuery.grep(emailAddArray, function(value) {
            return value.emailTypeId != emailTypeId;
        });
        $row.remove();
        updateEmailTypeDropDown();
    });

    $('#btnCsvUpload').on('click', function(e) {
        $("#contactViewDiv").addClass("hidden");
        $("#csvUploadDiv").removeClass("hidden");
        $(this).addClass("hidden");
        $("#btnSelectFromContact").removeClass("hidden");
    });

    $('#btnSelectFromContact').on('click', function(e) {
        $("#contactViewDiv").removeClass("hidden");
        $("#csvUploadDiv").addClass("hidden");
        $(this).addClass("hidden");
        $("#btnCsvUpload").removeClass("hidden");
    });

    $("#Dataimport").submit(function(e) {
        e.preventDefault();
        var formData = new FormData(this);
        eb.ajax({
            type: 'POST',
            url: BASE_URL + '/contactsAPI/importContact',
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            success: function (respond) {
                if(respond.status){
                    $("#csvUploadDiv").addClass('hidden');
                    $("#importMappingDiv").removeClass('hidden');
                    $('#importMappingDiv').html(respond.html);
                    columns = $('#importcolumncount').html();
                    for (var k = 0; k < columns; k++) {
                        ff[k] = 'Unmapped';
                    }

                }else{
                    p_notification(respond.status, respond.msg);
                }
            }
        });
    });

    $(document).on('change','.customerimportselector', function(event,modalEnable) {
        var selectid = this.id;
        var selectedoption = $(this).val();
        enableoptions(ff[selectid]);
        ff[selectid] = $(this).val();
        var $tr = $(this).closest('tr');
        var column = $tr.data('column');
        var enumModalStatus = (modalEnable === undefined) ? true : modalEnable;        
        var html = '';

        
        if (selectedoption != 'Unmapped') {
            disableoptions(selectedoption);
        }

        $tr.find('.enum-modal-btn').addClass('hidden');

        selectedFieldArr = [];
        $('.customerimportselector').each(function(index,element){
            var selectedValue = $(this).val();
            if(selectedValue && $.inArray(selectedValue,selectedFieldArr) == -1){
                selectedFieldArr.push(selectedValue);
            }
        });
    });

    function disableoptions(selectedoption) {
        for (var i = 0; i < columns; i++) {
            //        $("#" + i + " option:contains(" + selectedoption + ")").attr('disabled', 'disabled');
            $("#" + i + ' option[value="' + selectedoption + '"]').attr('disabled', 'disabled');
        }
    }

    function enableoptions(selectedenableoption) {
        for (var j = 0; j < columns; j++) {
            $("#" + j + ' option[value="' + selectedenableoption + '"]').removeAttr('disabled');
        }
    }

    $(document).on('click', '#importDataButton', function(params) {
        var contactObj = {
            header: $('#importheader').html(),
            delim: $('#importdelim').html(),
            columncount: $('#importcolumncount').html(),
            col: ff,
        };
        if( validateMapFields(ff)){
            eb.ajax({
                type: 'POST',
                url: BASE_URL + '/contactsAPI/processContactForView',
                data: contactObj,
                success: function (respond) {
                    if(respond.status){
                        $("#csvUploadDiv").addClass('hidden');
                        $("#importMappingDiv").addClass('hidden');
                        $("#importListDiv").removeClass('hidden');
                        $('#importListDiv').html(respond.html);
                    }else{
                        p_notification(respond.status, respond.msg);
                    }
                }
            });
        }else{
            p_notification(false, eb.getMessage('ERR_SELECT_MAPPED'));
        }
    });

    function validateMapFields(mappedArray){
        return !mappedArray.some(r=> mappedArray.includes('Unmapped'))
    }

    $(document).on('click', '#contactImportLitViewBack', function(e) {
        $("#importMappingDiv").removeClass('hidden');
        $("#importListDiv").addClass('hidden'); 
    });

    $(document).on('click', '#saveContactListFromCsv', function (e) {
        var contactObj = {
            header: $('#importheader').html(),
            delim: $('#importdelim').html(),
            columncount: $('#importcolumncount').html(),
            col: ff,
            listCode : $('#listCode').val(),
            listName : $('#listName').val()
        };
        if(validateCsvContactList(contactObj)){
            eb.ajax({
                type: 'POST',
                url: BASE_URL + '/contactsAPI/saveImportContact',
                data: contactObj,
                success: function (respond) {
                    if(respond.status){
                        console.log(respond.data);
                        p_notification(respond.status, respond.msg);
                        if(respond.status){
                            resetFields();

                            if(respond.status){
                                $('#contactListsList').html(respond.html);
                                $('#contactListSearch').val('').trigger('change');
            
                                window.setTimeout(function() {
                                    $('.viewContactDiv').removeClass('hidden');
                                    $('.createContactDiv').addClass('hidden');
                                }, 1000);
        
                            }
                        }
                    }else{
                        p_notification(respond.status, respond.msg);
                    }
                }
            });
        }
    });

    function validateCsvContactList(input) {
        if (input.listCode == '') {
            p_notification(false, eb.getMessage('ERR_CONTACT_VALID_LIST_CODE'));
            $("input[name='listCode']").focus();
            return false;
        } else if (input.listName == '') {
            p_notification(false, eb.getMessage('ERR_CONTACT_VALID_LIST_NAME'));
            $("input[name='listName']").focus();
            return false;
        }else {
            return true;
        }
    }

    $('#customerimport_more').hide();

    //customer_import show_more_details button
    var importmoreflag = 0;
    $("#moredetailsimport").click(function() {
        if (importmoreflag == 0) {
            $("#customerimport_more").slideDown(function() {
                $("#moredetailsimport").html('Hide details ' + '<span class="glyphicon glyphicon-circle-arrow-up"></span>');
            });
            importmoreflag = 1;
        } else {
            $("#customerimport_more").slideUp(function() {
                $('#moredetailsimport').html('Show more details ' + '<span class="glyphicon glyphicon-circle-arrow-down"></span>');
            });
            importmoreflag = 0;
        }
    });

    $(document).on('click', '#contactImportMappingBack', function (e) {
        $("#importMappingDiv").addClass('hidden');
        $("#importListDiv").addClass('hidden');
        $("#csvUploadDiv").removeClass('hidden');
    })


    $(document).on('click', '#saveButtonContact', function () {

        contactObj.firstName = $('#firstName').val();
        contactObj.lastName = $('#lastName').val();
        contactObj.title = $('#title').val();
        contactObj.telNumbers = mobileNumArray;
        contactObj.emails = emailAddArray;
        contactObj.designation = $('#designation').val();

        var saveType = this.getAttribute('data-saveType');
        
        if(validateContactData(contactObj)){
            eb.ajax({
                type: 'POST',
                url: BASE_URL + '/contactsAPI/create',
                data: contactObj,
                success: function (respond) {
                    p_notification(respond.status, respond.msg);

                    if(respond.status){
                        resetMobileNum();
                        resetEmail();
                        resetFields();
                        loadContact();
                        $('#contactCreateViewModal').modal('hide');
                    }
                }
            });
        }
    });

    function resetMobileNum () {
        mobileNumArray = [];
        $('#numberTable tr.added-num').remove();
        updateNumTypeDropDown();
    }

    function resetFields() {
        $('#firstName').val('');
        $('#lastName').val('');
        $('#designation').val('');
        $('#title').val(0);
    }

    function resetEmail () {
        emailAddArray = [];
        $('#emailTable tr.added-email').remove();
        updateEmailTypeDropDown();
    }

    function updateEmailTypeDropDown() {
        $allRow = $('tr.emailRow.editable');
        $allRow.find('option').removeAttr('disabled')
        $.each(emailAddArray, function(index, value) {
            $allRow.find(".emailTypeId option[value='" + value.emailTypeId + "']").attr('disabled', 'disabled');
        });
        $allRow.find('.emailTypeId').selectpicker('render');
        $allRow.find('.emailTypeId').selectpicker('refresh');
    }

    function updateNumTypeDropDown() {
        $allRow = $('tr.documentRow.editable');
        $allRow.find('option').removeAttr('disabled')
        $.each(mobileNumArray, function(index, value) {
            $allRow.find(".numberTypeId option[value='" + value.numberTypeId + "']").attr('disabled', 'disabled');
        });
        $allRow.find('.numberTypeId').selectpicker('render');
        $allRow.find('.numberTypeId').selectpicker('refresh');
    }

    function validateContactData(input) {

        if (input.firstName == '') {
            p_notification(false, eb.getMessage('ERR_CONTACT_VALID_FIRST_NAME'));
            $("input[name='firstName']").focus();
            return false;
        } else if (input.telNumbers.length == 0) {
            p_notification(false, eb.getMessage('ERR_CONTACT_NUM_EMPTY'));
            return false;
        }  else {
            return true;
        }
    }
});