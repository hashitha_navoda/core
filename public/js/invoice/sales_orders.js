/**
 * @author Prathap Weerasinghe <prathap@thinkcube.com>
 * This file contains sales Order js
 */
var QIDSelected = false;
var qid;
var tmptotal, state = "new";
var tax_rates = new Array();
var products = new Array();
var taxdetails = new Array();
var taxDetailsFromQut = new Array();
var submit_data;
var quotationExpireDate;
var quotationIssuedDate;
var all_tx_val = new Array();
var quotationID = '';
var currentProducts = new Array();
var selectedItemID;
var customCurrencyRate = 0;
var cusProfID;
var priceListItems = [];
var documentTypeArray = [];

function product(pCode, pName, quantity, uom, uPrice, discount, dType, tax, tCost, taxDetails, productType, item_discription) {
    this.item_code = pCode;
    this.item = pName;
    this.quantity = quantity;
    this.discount = discount;
    this.discountType = dType;
    this.unit_price = uPrice;
    this.uom = uom;
    this.total_cost = tCost;
    this.taxes = tax;
    this.taxDetails = taxDetails;
    this.productType = productType;
    this.item_discription = item_discription;
}

$(document).ready(function() {

    var currentCreditLimit = 0;
    var totalTaxList = {};
    $(this).ready(function() {
        var preQuoVal = $('#pre_quo_no').val();
        callQuotaion(preQuoVal);
    });
    var all_tx_val = new Array();
    var cur_item_tax;
    var totaldiscount = 0, cust_disc = 0, decimalPoints = 0, itemDecimalPoints = 0;
    var priceListId = '';
    $('.deli_row, #deli_form_group, #tax_td').hide();
    $("#customer_more").hide();
    $("#currency").attr("disabled", "disabled");
    $("#moredetails").click(function() {
        $("#customer_more").slideDown();
        $('#moredetails').hide();
    });

    var documentType = 'salesOrder';

    $('#item_code').select2({
        minimumInputLength: 1,
        minimumResultsForSearch: 10,
        ajax: {
            url: BASE_URL + '/productAPI/search-location-products-for-dropdown',
            dataType: "json",
            type: "POST",
            data: function (params) {

                var queryParameters = {
                    searchKey: params.term,
                    documentType: documentType,
                    locationID: currentLocationID
                }
                return queryParameters;
            },
            processResults: function (data) {
                return {
                    results: $.map(data.data.list, function (item) {
                        return {
                            text: item.text,
                            id: item.value
                        }
                    })
                };
            }
        },
        placeholder: 'Enter Item Code Or Name',
    });

    $('#documentReference').on('click', function(e) {
        e.preventDefault();
            $('tr.documentRow.editable').find('.editDocument').addClass('hidden');
            $('tr.documentRow.editable').find('.documentTypeId').val(0);

            $('tr.documentRow.editable').find('.documentTypeId').selectpicker('render');
        $('tr.documentRow.editable').find('.documentTypeId').selectpicker('refresh');
        $('tr.documentRow.editable').find('.documentTypeId').trigger('change');
        $('#documentReferenceModal').modal('show');
        
    });


     $('#documentTable').on('change', '.documentTypeId', function() {
        var $dropDownIDParent = $(this).parents('tr');
        var documentTypeID = $(this).val();
        loadDocumentId($dropDownIDParent, documentTypeID);
    });

    function loadDocumentId($dropDownIDParent, documentTypeID) {
        var dropDownID = '.documentId';
        var url = '';
        if (documentTypeID == 46) {
            url = '/customerOrder-api/search-all-customer-order-for-dropdown';
            $('select.selectpicker.documentId', $dropDownIDParent).attr('multiple', false);
        } 
        $('div.bootstrap-select.documentId', $dropDownIDParent).remove();
        $('select.selectpicker.documentId', $dropDownIDParent).show().removeData().empty();
        loadDropDownFromDatabase(url, null, 0, dropDownID, $dropDownIDParent);
    }

    function updateDocTypeDropDown() {
        $allRow = $('tr.documentRow.editable');
        $allRow.find('option').removeAttr('disabled')
        $.each(documentTypeArray, function(index, value) {
            $allRow.find(".documentTypeId option[value='" + value + "']").attr('disabled', 'disabled');
        });
        $allRow.find('.documentTypeId').selectpicker('render');
        $allRow.find('.documentTypeId').selectpicker('refresh');
    }

    $('#documentTable').on('click', '.saveDocument', function(e) {
        e.preventDefault();
        var $row = $(this).parents('tr');
        if (validateDocumentTypeRow($row)) {
            $row.find('.editDocument').removeClass('hidden');
            $row.removeClass('editable');
            documentTypeArray.push($row.find('.documentTypeId').val());
            $row.find('.documentTypeId').attr('disabled', true);
            $row.find('.documentId').attr('disabled', true);
            $row.find('.saveDocument').addClass('hidden');
            $row.find('.deleteDocument').removeClass('hidden');
            var $cloneRow = $($('.sample').clone()).removeClass('sample hidden').addClass('documentRow editable');

            $cloneRow.find('.documentTypeId').selectpicker().attr('data-liver-search', true);
            $cloneRow.find('.editDocument').addClass('hidden');
            $cloneRow.insertBefore($row);

            updateDocTypeDropDown();
        }
    });

    $('#documentTable').on('click', '.deleteDocument', function(e) {
        e.preventDefault();
        var $row = $(this).parents('tr');
        var documentTypeID = $row.find('.documentTypeId').val();
        //this is for remove documentTypeID from the documentType array
        documentTypeArray = jQuery.grep(documentTypeArray, function(value) {
            return value != documentTypeID;
        });
        $row.remove();
        updateDocTypeDropDown();
    });

    $('#documentTable').on('click', '.editDocument', function(e) {
        e.preventDefault();
        var $row = $(this).parents('tr');
        $row.find('.documentId').attr('disabled', false);
        $row.find('.updateDocument').removeClass('hidden');
        $row.find('.editDocument').addClass('hidden');
    });

    $('#documentTable').on('click', '.updateDocument', function(e) {
        e.preventDefault();
        var $row = $(this).parents('tr');
        if (validateDocumentTypeRow($row)) {
            $row.find('.documentTypeId').attr('disabled', true);
            $row.find('.documentId').attr('disabled', true);
            $row.find('.updateDocument').addClass('hidden');
            $row.find('.editDocument').removeClass('hidden');
        }
    });

    function validateDocumentTypeRow($row) {
        var flag = true;
        var documentTypeID = $row.find('.documentTypeId').val();
        var documentID = $row.find('.documentId').val();
        if (documentTypeID == '' || documentTypeID == null) {
            p_notification(false, eb.getMessage('ERR_PO_DOC_TYPE_SELECT'));
            flag = false;
        } else if (documentID == '' || documentID == null) {
            p_notification(false, eb.getMessage('ERR_PO_DOC_ID_SELECT'));
            flag = false;
        }
        return flag;
    }

     $('#item_code').on('select2:select', function (e) { 
        var data = e.params.data;
        $('#qty', $(this).parents('tr')).parent().addClass('input-group');
        if (data.id > 0 && data.id != selectedItemID) {
            selectedItemID = data.id;
            itemChange(data.id);
        }
    });

    $('#cust_name').select2({
        minimumInputLength: 1,
        minimumResultsForSearch: 10,
        ajax: {
            url: BASE_URL + '/customerAPI/search-customers-for-dropdown',
            dataType: "json",
            type: "POST",
            data: function (params) {

                var queryParameters = {
                    searchKey: params.term
                }
                return queryParameters;
            },
            processResults: function (data) {
                return {
                    results: $.map(data.data.list, function (item) {
                        return {
                            text: item.text,
                            id: item.value
                        }
                    })
                };
            }
        },
        placeholder: 'Select Customer Name',
    });

    $('#quo_no').select2({
        minimumInputLength: 1,
        minimumResultsForSearch: 10,
        ajax: {
            url: BASE_URL + '/quotation-api/search-open-quotation-for-dropdown',
            dataType: "json",
            type: "POST",
            data: function (params) {

                var queryParameters = {
                    searchKey: params.term,
                    locationID: currentLocationID
                }
                return queryParameters;
            },
            processResults: function (data) {
                return {
                    results: $.map(data.data.list, function (item) {
                        return {
                            text: item.text,
                            id: item.value
                        }
                    })
                };
            }
        },
        placeholder: 'Select Quotation Number',
    });

    $('#quo_no').on('select2:select', function (e) { 
        var data = e.params.data;
        if (data.id > 0 && quotationID != data.id) {
            quotationID = data.id;
            QIDSelected = true;
            qid = quotationID;
            clearAllData();
            getQuotationDetails();
        }else if (data.id == 0 && quotationID != null){
            //$('#quo_no').val(quotationID).selectpicker('refresh');
            qid ='';
        }else if(quotationID == data.id){
            qid = quotationID;
        }
    });

    $('#cust_name').on('select2:select', function (e) { 
        var data = e.params.data;
        var key = data.id;
        if (data.id > 0 && customerID != data.id) {
            customerID = key;
            setCustomerData(key);
            getCustomerProfilesDetails(key);
        }
    });

    function callQuotaion(preQuoVal) {
        if (preQuoVal != '') {
            getQuotationDetails();
        }
    }

    function setItemCost(clonedAddNew) {
        var id = $('#item_code', clonedAddNew).val();
        var productType = $('#item_code', clonedAddNew).data('PT');
        var price = parseFloat(accounting.unformat($('#price', clonedAddNew).val()));
        var qty = parseFloat($('#qty', clonedAddNew).val());
        if (productType == 2 && (qty == '' || qty == 0 || isNaN(qty))) {
            qty = 1;
        }

        var disc = parseFloat(accounting.unformat($('#discount', clonedAddNew).val()));
        var tax = new Array();
        var taxA = 0;
        var tot = 0;
        taxdetails = new Array();
        $('#addTaxUl input:checked', clonedAddNew).each(function() {
            if (!$(this).is(':disabled')) {
                tax.push($(this).attr("value"));
            }
        });
        if (jQuery.isEmptyObject(tax)) {
            $('#taxApplied', clonedAddNew).addClass('glyphicon glyphicon-unchecked');
        } else {
            $('#taxApplied', clonedAddNew).removeClass('glyphicon glyphicon-unchecked');
            $('#taxApplied', clonedAddNew).addClass('glyphicon glyphicon-check');
        }
        tot = (price * qty) - calculateDiscount(id, price * qty, disc, qty, clonedAddNew);
        taxdetails = calculateItemCustomTax(tot, tax);
        var taxTotalAmount = taxdetails === null ? 0 : taxdetails.tTA;
        taxA = taxTotalAmount === undefined ? 0 : parseFloat(taxTotalAmount);
        $("#total", clonedAddNew).html(isNaN((tot + taxA)) ? "0.00" : accounting.formatMoney((tot + taxA).toFixed(2)));
        taxDetailsFromQut[id] = taxdetails;
    }

    function ItemTotalCost(pID, unitPrice, quontity, discount, discountType, tax, clonedAddNew) {

        var taxA = 0;
        var tot = 0;
        tot = (unitPrice * quontity) - calculateDiscount(pID, unitPrice * quontity, discount, quontity, clonedAddNew);
        taxdetails = calculateItemCustomTax(tot, tax);
        var taxTotalAmount = taxdetails === null ? 0 : taxdetails.tTA;
        taxA = taxTotalAmount === undefined ? 0 : parseFloat(taxTotalAmount);
        return isNaN((tot + taxA)) ? "0.00" : (tot + taxA).toFixed(2);
    }

    function setItemCostForTaxes(i_code, checkedTaxes) {
        var itemdata = products[i_code];
        var price = itemdata.unit_price;
        var qty = itemdata.quantity;
        var disc = itemdata.discount;
        var tax = 0;
        var tot = price * qty;
        if (disc !== "" || disc > 0) {
            tot -= (tot * disc) / 100;
        }

        var getTax_url = BASE_URL + '/taxAPI/getCustomTaxAmount';
        var data = {
            'item_id': i_code,
            'price': parseFloat(tot).toFixed(2),
            'checkedTaxes': checkedTaxes
        };
        cur_item_tax = new Array();
        eb.ajax({
            type: 'POST',
            url: getTax_url,
            data: data,
            success: function(taxdata) {
                for (var key in taxdata) {
                    if (!taxdata[key])
                        continue;
                    tax = tax + taxdata[key];
                    cur_item_tax[key] = taxdata[key]
                }
                products[i_code].taxes = cur_item_tax;
                products[i_code].total_cost = parseFloat(tot + tax).toFixed(2);
                $("#td_" + i_code + ">#ttl").html(accounting.formatMoney(products[i_code].total_cost));
                setTotalCost();
            },
            async: false
        });
    }

    function setTotalCost() {

        var total = 0, disc = 0;
        for (var i in products) {
            var tax = 0;
            for (var tx in products[i].taxes) {
                tax += products[i].taxes[tx];
            }
            total += parseFloat(products[i].total_cost);
        }
        var delivercharge = parseFloat(accounting.unformat($('#deli_amount').val()));
        if (delivercharge == '') {
            $('#finaltotal').html(accounting.formatMoney((total).toFixed(2)));
            $('#subtotal').html(accounting.formatMoney((total).toFixed(2)));
        } else {
            finalt = ((parseFloat(total) + parseFloat(delivercharge)).toFixed(2));
            $('#finaltotal').html(accounting.formatMoney(finalt));
        }
        setTotalTax();
    }

    function setTotalTax() {
        if (tax_rates.length < 1) {
            eb.ajax({
                type: 'POST',
                url: '/taxAPI/getTaxRates',
                success: function(res) {
                    tax_rates = res;
                },
                async: false
            });
        }

        totalTaxList = {};
        if (TAXSTATUS) {
            for (var k in products) {
                for (var l in products[k].taxDetails.tL) {
                    if (totalTaxList.hasOwnProperty(l)) {
                        var curTA = totalTaxList[l].tA;
                        var newTA = (toFloat(curTA) + toFloat(products[k].taxDetails.tL[l].tA));
                        totalTaxList[l].tA = newTA.toFixed(2);
                    } else {
                        totalTaxList[l] = {tN: products[k].taxDetails.tL[l].tN, tP: products[k].taxDetails.tL[l].tP, tA: products[k].taxDetails.tL[l].tA};
                    }
                }
            }
            var totalTaxHtml = "";
            for (var t in totalTaxList) {
                totalTaxHtml += totalTaxList[t].tN + "(" + totalTaxList[t].tP + "%)&nbsp" + (accounting.formatMoney(totalTaxList[t].tA)) + "<br>";
            }
            $("#tax_td").html(totalTaxHtml);
        }
    }

    function validateUserData(valid) {
        var user = valid[0];
        var pass = valid[1];
        if (user == null || user == "") {
            p_notification(false, eb.getMessage('ERR_VIEWPAY_UNAME'));
        } else if (pass == null || pass == "") {
            p_notification(false, eb.getMessage('ERR_VIEWPAY_PWD'));
        } else {
            return true;
        }
    }

    function saveSalesOrder(submitData) {
        var url = "/api/salesOrders/create";
        var request = eb.post(url, submitData);
        request.done(function(res) {
            p_notification(res.status, res.msg);
            if (res.status == true) {
                $('#so_no').val(res.data['soCode']);
                state = "saved";
                var fileInput = document.getElementById('documentFiles');
                if(fileInput.files.length > 0) {
                    var form_data = false;
                    if (window.FormData) {
                        form_data = new FormData();
                    }
                    form_data.append("documentID", res.data['soID']);
                    form_data.append("documentTypeID", 3);
                    
                    for (var i = 0; i < fileInput.files.length; i++) {
                        form_data.append("files[]", fileInput.files[i]);
                    }

                    eb.ajax({
                        url: BASE_URL + '/store-files',
                        type: 'POST',
                        processData: false,
                        contentType: false,
                        data: form_data,
                        success: function(res) {
                        }
                    });
                }

                if ($('#lightbox_overlay').length < 1) {

                    documentPreview(BASE_URL + '/salesOrders/salesOrdersView/' + res.data['soID'], 'documentpreview', "/api/salesOrders/send-email", function($preview) {
                        $preview.on('hidden.bs.modal', function() {
                            if (!$("#preview:visible").length) {
                                window.location.reload();
                            }
                        });
                    });
                }
            }
        });
    }

    $(this).ready(function() {
        var preQuoVal = $('#pre_quo_no').val();
        if (preQuoVal != "") {
            $('#quo_no').show();
            getQuotationDetails();
        }
    });
    $('#get-so-select').on('change', function() {
        if ($(this).val() == 'Quotation No') {
            $('#quo_no').show();
        } else if ($(this).val() == 'Recurrent SalesOrder No') {
            $('#quo_no').hide();
        }
    });
    $(document).on('click', '.dropdown-menu input, .dropdown-menu label', function(e) {
        e.stopPropagation();
    });
    $(document).on('mouseover', '#item_tbl #unit,#item_tbl #quantity,#item_tbl #disc,#item_tbl #name', function(e) {
        $(this).addClass('shadow');
    });
    $(document).on('mouseout', '#item_tbl #unit,#item_tbl #quantity,#item_tbl #disc,#item_tbl #name', function(e) {
        $(this).removeClass('shadow');
    });
    $(document).on('change', '.chkTaxes', function() {
        var trid = $(this).closest('tr').attr('id');
        var itemID = trid.split('td_')[1].trim();
        var checkedTaxes = Array();
        $("." + itemID + " input:checked").each(function() {
            checkedTaxes.push(this.id);
        });
        if (jQuery.isEmptyObject(checkedTaxes)) {
            $("#taxFilled_" + itemID).removeClass('glyphicon glyphicon-check');
            $("#taxFilled_" + itemID).addClass('glyphicon glyphicon-unchecked');
        } else {
            $("#taxFilled_" + itemID).removeClass('glyphicon glyphicon-unchecked');
            $("#taxFilled_" + itemID).addClass('glyphicon glyphicon-check');
        }
        setItemCostForTaxes(itemID, checkedTaxes);
    });
    function getQuotationDetails() {
        var q_id = $('#quo_no').val();

        req = eb.post(BASE_URL + '/quotation-api/retriveQuotation', {'quataionID': q_id, forInvoice: true, 'withDeactivated': true});
        req.done(function(res) {
            if (res == 'wrongquono') {
                p_notification(false, eb.getMessage('ERR_QUOT_VALID_QUOTA'));
            } else if(res == 'inactiveCustomer'){
                p_notification(false, eb.getMessage('ERR_QUOT_CUST_INACTIVE'));
                return;
            } else {
                if (res.inactiveItemFlag) {
                    p_notification(false, res.errorMsg + " cannot copy this quotation.");
                    setTimeout(function(){ 
                        window.location.assign(BASE_URL + "/salesOrders")
                    }, 3000);
                    return false;
                }
                var getCustomerDataUrl = BASE_URL + '/customerAPI/getcustomerByID';
                eb.ajax({
                    type: 'POST',
                    url: getCustomerDataUrl,
                    data: {customerID: res.customer_id},
                    success: function(req) {
                        if (req.status) {
                            var customerData = req.data.customerData;
                            $('#cust_name').empty();
                            $('#cust_name').
                                    append($("<option></option>").
                                            attr("value", customerData.customerID).
                                            text(customerData.customerName + '-' + customerData.customerCode));
                            $('#cust_name').selectpicker('refresh');
                            $('#cust_name').prop('disabled', true);
                            currentCreditLimit = customerData.customerCreditLimit;
                            $('#invoicecurrentBalance').val(accounting.formatMoney(customerData.customerCurrentBalance));
                        } else {
                            currentCreditLimit = 0;
                            $('#invoicecurrentBalance').val(0);
                        }
                    },
                    async: false
                });
                cusProfID = res.customer_prof_id;
                quotationIssuedDate = res.issue_date;
                quotationExpireDate = res.expire_date;

                $('#customCurrencyId').val(res.customCurrencyId).trigger('change');
                if (!(res.customCurrencyId == '' || res.customCurrencyId == 0)) {
                    $('#customCurrencyId').attr('disabled', true);
                }
                customCurrencyRate = (res.quotationCustomCurrencyRate != 0) ? parseFloat(res.quotationCustomCurrencyRate) : 1;
                companyCurrencySymbol = res.customCurrencySymbol;
                var ExpireDate = new Date(Date.parse(res.expire_date));
                var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);
                if (ExpireDate < now) {
                    p_notification(false, eb.getMessage('ERR_SALEORDERS_QUOT_EXPIRE'));
                }

                var cusProfName;
                if (res.customer_prof_name == '' || res.customer_prof_name == null) {
                    cusProfName = 'Default';
                } else {
                    cusProfName = res.customer_prof_name;
                }
                $('.cus_prof_div').removeClass('hidden');
                $('.cus-prof-select').addClass('hidden');
                $('.default-cus-prof').removeClass('hidden');
                $('.tooltip_for_default_cus').addClass('hidden');

                $('#cust_id').val(res.customer_id);
                $('#cus-prof-name').val(cusProfName);
                $('#payment_term').val(res.payment_term);
                $('#payment_term').trigger('change');
                $('#qot_no').val(res.quotation_no);
                $('#cmnt').val(res.comment);
                $('#salesOrderCurrentCredit').val(accounting.formatMoney(res.current_credit));
                $('#salesPersonID').val(res.salesPersonID);
                if (res.salesPersonID != 0) {
                    $('#salesPersonID').prop('disabled', true);
                }

                if (res.priceListId != 0) {
                    $('#priceListId').val(res.priceListId).attr('disabled', true);
                } else {
                    $('#priceListId').val('').attr('disabled', false);
                }
                $('#priceListId').selectpicker('refresh');
                $('#priceListId').trigger('change');
                $('#salesPersonID').selectpicker('refresh');
                $('#qot_no').prop('readonly', true);

                currentProducts = res.locationProducts;
                for (var i in res.product) {
                    var productID = res.product[i].productID;
                    var productCode = res.product[i].productCode;
                    var productName = res.product[i].productName;
                    var productType = res.product[i].productTypeID;
                    var item_details = _.where(currentProducts, {lPID: res.product[i].locationProductID});
                    var i_code = item_details[0].pC;
                    var item_desc = res.product[i].productName;
                    var qty = res.product[i].quotationProductQuantity - res.product[i].quotationProductCopiedQuantity;
                    var disc = parseFloat(res.product[i].quotationProductDiscount);
                    var dsc_type = res.product[i].quotationProductDiscountType;
                    var prc = parseFloat(res.product[i].quotationProductUnitPrice) / customCurrencyRate;
                    var pTotal = parseFloat(res.product[i].quotationProductTotal) / customCurrencyRate;
                    var proDescription = res.product[i].quotationProductDescription;
                    //get base uom of the product
                    for (var j in currentProducts[productID].uom) {
                        if (currentProducts[productID].uom[j].pUDisplay == 1) {
                            var uomPrice = ((res.product[i].quotationProductUnitPrice / customCurrencyRate) * currentProducts[productID].uom[j].uC).toFixed(2);
                            var uomQty = (res.product[i].quotationProductQuantity / currentProducts[productID].uom[j].uC).toFixed(currentProducts[productID].uom[j].uDP);
                            var uom = j;
                        }
                    }
                    var qpid = res.product[i].quotationProductID;
                    var taxesOfProduct = new Array();
                    if (res.productTaxes) {
                        for (var k in res.productTaxes[qpid]) {
                            taxesOfProduct.push(k);
                        }
                    }
                    //set product row
                    var clonedAddNew = $($('#item_tr').clone()).attr('id', 'soPro_' + productID).addClass('tempSoPro');
                    var itemData = {
                        id: productID,
                        text: productName + '-' + productCode
                    };
                    var newOption2 = new Option(itemData.text, itemData.id, false, false);
                    $('.select2-container', clonedAddNew).addClass('hidden');
                    $('#productfromqo', clonedAddNew).removeClass('hidden');
                    $('#productfromqo', clonedAddNew).val(productName + '-' + productCode);
                    $('#item_code', clonedAddNew).append(newOption2).trigger('change');
                    $('#item_code', clonedAddNew).data('PT', productType);
                    $('#item_code', clonedAddNew).prop('disabled', true);
                    $('div.bootstrap-select', clonedAddNew).remove();
                    if ($("#price", clonedAddNew).siblings().hasClass('uomPrice')) {
                        $("#price", clonedAddNew).siblings('.uomPrice').remove();
                        $("#price", clonedAddNew).siblings('.uom-price-select').remove();
                    }
                    $('#price', clonedAddNew).val(prc).addUomPrice(currentProducts[productID].uom);

                    if ($("#qty", clonedAddNew).siblings().hasClass('uomqty')) {
                        $("#qty", clonedAddNew).siblings('.uomqty').remove();
                        $("#qty", clonedAddNew).siblings('.uom-select').remove();
                    }
                    $('#qty', clonedAddNew).parent().addClass('input-group');
                    $('#qty', clonedAddNew).val(qty).addUom(currentProducts[productID].uom);
                    $('.uomqty', clonedAddNew).attr("id", "itemQuantity");
                    $('.uomPrice', clonedAddNew).attr("id", "itemPrice");
                        if (currentProducts[productID].pPDV != null) {
                            $('#discount', clonedAddNew).val((disc / customCurrencyRate).toFixed(2));
                            $('#discType', clonedAddNew).html(companyCurrencySymbol);
                        } else if (currentProducts[productID].pPDP != null){
                            $('#discount', clonedAddNew).val(disc.toFixed(2));
                            $('#discType', clonedAddNew).html('(%)');
                        } else {
                            $('#discount', clonedAddNew).attr('readonly', true);
                        }
                    $('.itemDescText', clonedAddNew).val(proDescription);
                    setTaxListForProduct(productID, clonedAddNew);
                    clonedAddNew.insertBefore('#item_tr');
                    setItemCost(clonedAddNew);

//                    ss = ItemTotalCost(productID, prc, qty, disc, dsc_type, taxesOfProduct,clonedAddNew);
//                    addProduct(i_code, item_desc, qty, prc, uom, dsc_type, disc, taxesOfProduct, ss, uomQty, productType, productID, uomPrice);
                }
//                setTotalCost();
            }
        });
    }

    $('#reset').on('click', function() {
        window.location.reload();
    });
    $(document).on('click', ".delete", function() {
        delete products[this.id];
        $(this).closest("tr").remove();
        // when all products deleted customcurrency symbal selection will be enable
        // if used any selection from 'start invoice by' then cann't enable
        setTotalCost();
    });
    $('#item_insert').on('click', '#add_item', function(e) {
        e.preventDefault();
        var oldrow = $(this).parents('tr');
        var code = currentProducts[$("#item_code", oldrow).val()].pC;
        var productID = $("#item_code", oldrow).val();
        var productType = $("#item_code", oldrow).data('PT');
        var pName = currentProducts[$("#item_code", oldrow).val()].pN;
        var quantity = $("#qty", oldrow).val();
        if (!quantity) {
            quantity = 0;
        }
        var uom = $("#qty", oldrow).siblings('.uom-select').children('button').find('.selected').data('uomID');
        var uomqty = $("#qty", oldrow).siblings('.uomqty').val();
        var uomPrice = $("#price", oldrow).siblings('.uomPrice').val();

        var price = accounting.unformat($("#price", oldrow).val());

        if (priceListItems[productID] != undefined && parseFloat(priceListItems[productID].itemPrice) > 0) {
            var itemDiscountType = priceListItems[productID].itemDiscountType;
            dsc_type = (itemDiscountType == "1") ? "vl" : "pr";
        } else {
            var dsc_type = "pr";
            if (currentProducts[productID] !== undefined && parseFloat(currentProducts[productID].dV) >= 0) {
                dsc_type = "vl";
            } else if (currentProducts[productID] !== undefined) {
                dsc_type = "pr";
            }
        }



        var item_discription = $(this).parents('tr').find('.itemDescText').val();
        var dsc_Pre = accounting.unformat($("#discount", oldrow).val());
        var tax = new Array();
        $('#addTaxUl input:checked', oldrow).each(function() {
            tax.push($(this).attr("value"));
        });


        var status = addProduct(code, pName, quantity, price, uom, dsc_type, dsc_Pre, tax, '', uomqty, productType, productID, uomPrice, oldrow, item_discription);
        //setTotalCost();


        if (status == false) {
            return false;
        } else {
            setTotalCost();
        }
        if (oldrow.hasClass('tempSoPro')) {
            oldrow.remove();
        }

        $('#item_tbl thead').css('display', 'table-header-group');
        $('#item_code', oldrow).val(0).trigger('change').empty().selectpicker('refresh');
        $('#customCurrencyId').attr('disabled', true);
        $('#priceListId').attr('disabled', true);
        selectedItemID = '';
    });
    $('#show_tax').on('click', function() {
        if (this.checked) {
            $('#tax_td').fadeIn();
        }
        else {
            $('#tax_td').fadeOut();
        }
    });
    $('#deli_charg').on('click', function() {
        if (this.checked) {
            $('.deli_row, #deli_form_group').fadeIn();
        }
        else {
            $("#deli_amount").val(0);
            $('.deli_row, #deli_form_group').fadeOut();
            setTotalCost();
        }
        $('#deli_amount').on('change keyup', function() {
            if (!$.isNumeric(accounting.unformat($('#deli_amount').val())) || accounting.unformat($(this).val()) < 0) {
                $(this).val(0);
                setTotalCost();
            } else {
                setTotalCost();
            }
        });
    });
    $(document).on('focusout', '#price, #qty, #discount, .uomqty,.uomPrice', function() {
        var oldrow = $(this).parents('tr');
        if ($('#item_code', oldrow).val() != '') {
            if ($(this).val().indexOf('.') > 0) {
                decimalPoints = $(this).val().split('.')[1].length;
            }
            if (this.id == 'price' && (!$.isNumeric(accounting.unformat($('#price', oldrow).val())) || accounting.unformat($('#price', oldrow).val()) < 0 || decimalPoints > 2)) {
                decimalPoints = 0;
                $('#price', oldrow).val('');
            }
            if ((this.id == 'qty') && (!$.isNumeric($('#qty', oldrow).val()) || $('#qty', oldrow).val() < 0 || decimalPoints > itemDecimalPoints)) {
                decimalPoints = 0;
                $('#qty').val('');
            }
            if ((this.id == 'discount') && (!$.isNumeric(accounting.unformat($('#discount', oldrow).val())) || accounting.unformat($('#discount', oldrow).val()) < 0 || (decimalPoints > 2))) {
                decimalPoints = 0;
                $('#discount', oldrow).val('');
            }
            setItemCost(oldrow);

        } else {
            $('#price, #qty, #discount', oldrow).val('');

        }
    });
    function itemChange(itemkey) {
        var data = new Array();
        itemDecimalPoints = 3;
        var $addrow = $('#item_tr');
        $('.tempLi', $addrow).remove();
        $("#discount", $addrow).attr('disabled', false);
        if (itemkey == '') {
            $('#qty', $addrow).val('');
            $('#price', $addrow).val('');
            $('#discount', $addrow).val('');
            $('#total', $addrow).html('');
            $('#uom', $addrow).html('');
            return false;
        }

        eb.ajax({
            type: 'POST',
            url: BASE_URL + '/productAPI/get-location-product-details',
            data: {productID: itemkey, locationID: currentLocationID},
            success: function(respond) {
                currentProducts[respond.data.pID] = respond.data;
                data[0] = respond.data;


                var baseUom;
                for (var j in data[0].uom) {
                    if (data[0].uom[j].pUBase == 1) {
                        baseUom = data[0].uom[j].uomID;
                    }
                }

                if (data[0] == 'error') {
                    $('#qty', $addrow).val('');
                    $('#price', $addrow).val('');
                    $('#discount', $addrow).val('');
                    $('#total', $addrow).html('');
                    $('#uom', $addrow).html('');
                    dbflag = false;
                } else {
                    dbflag = true;

                    if ($("#discount", $addrow).siblings().hasClass('uomPrice')) {
                        $("#discount", $addrow).siblings('.uomPrice').remove();
                        $("#discount", $addrow).siblings('.uom-price-select').remove();
                        $("#discount", $addrow).show();
                    }

                    if (priceListItems[itemkey] == undefined || (priceListItems[itemkey] != undefined && !(parseFloat(priceListItems[itemkey].itemPrice) > 0))) {
                        if (data[0].dEL === "0"  || (data[0].dV == null && data[0].dPR == null)) {
                            $('#discount', $addrow).val('');
                            $("#discount", $addrow).attr('disabled', true);
                        }
                        else if (data[0].dEL === "1" && data[0].dV != null)
                        {
                            $('#discount', $addrow).val(parseFloat(data[0].dV).toFixed(2)).addUomPrice(data[0].uom, baseUom);
                            $('#discount', $addrow).siblings('.uom-price-select').find('.uom_price_drop_down').attr('disabled', true);
                            $('#discType', $addrow).html(companyCurrencySymbol);
                        }
                        else if (data[0].dEL === "1" && data[0].dPR != null) {
                            $('#discount', $addrow).val(parseFloat(data[0].dPR).toFixed(2));
                            $('#discType', $addrow).html('(%)');
                        }
                    } else {
                        var itemDiscountType = priceListItems[itemkey].itemDiscountType;
                        if (itemDiscountType == 1) {
                            $('#discount').val(parseFloat(priceListItems[itemkey].itemDiscount).toFixed(2)).addUomPrice(data[0].uom);
                            $('#discType').html(companyCurrencySymbol);
                        } else if (itemDiscountType == 2) {
                            $('#discount').val(parseFloat(priceListItems[itemkey].itemDiscount).toFixed(2));
                            $('#discType').html('(%)');
                        }
                    }

                    $('#item_code', $addrow).data('PT', data[0].pT);
                    $('#item_code', $addrow).val(itemkey);
                    $('#item_code', $addrow).selectpicker('render');
                    if ($("#price", $addrow).siblings().hasClass('uomPrice')) {
                        $("#price", $addrow).siblings('.uomPrice').remove();
                        $("#price", $addrow).siblings('.uom-price-select').remove();
                    }
                    var newprice = 0;
                    if (priceListItems[itemkey] != undefined) {
                        newprice = priceListItems[itemkey].itemPrice;
                    } else {
                        newprice = data[0].dSP;
                    }
                    $('#price', $addrow).val(parseFloat(newprice).toFixed(2)).addUomPrice(data[0].uom);
                    $('#uom', $addrow).html(data[0].abbrevation);
                    if ($('#qty', $addrow).val() === '') {
                        $('#qty', $addrow).val('1');
                    }
                    if ($("#qty", $addrow).siblings().hasClass('uomqty')) {
                        $("#qty", $addrow).siblings('.uomqty').remove();
                        $("#qty", $addrow).siblings('.uom-select').remove();
                    }
                    $('#qty', $addrow).addUom(data[0].uom);
                    $('.uomqty', $addrow).attr("id", "itemQuantity");
                    $('.uomPrice', $addrow).attr("id", "itemPrice");
                    setTaxListForProduct(itemkey, $addrow);
                    itemDecimalPoints = data[0].decimalPlace;
                    setItemCost($addrow);
                }
            }
        });

    }

    $(document).on('click', '#exceed-creditLimit-approve-button', function() {
        var approveinfo = new Array($('#username').val(),
                $('#password').val()
                );
        if (validateUserData(approveinfo)) {
            var username = $('#username').val();
            var password = $('#password').val();
            var getApproval = BASE_URL + '/userAPI/creditLimitExceedApproval';
            eb.post(getApproval, {
                username: username,
                password: password
            }).done(function(reply) {
                if (reply.data == true) {
                    $('#exceedCreditLimit').modal('hide');
                    saveSalesOrder(submit_data)
                } else {
                    p_notification(reply.status, reply.msg);
                }
            });
        }
    });
    $('#sales_orders_form, #so_view').on('submit', function(e) {
        e.preventDefault();
        var selectedid = "#" + document.activeElement.id;
        var tableId = $(selectedid).closest('table').attr("id");
        var custanamefull = $('#cust_name option:selected').text();
        var customerID = $('#cust_name').val();

        if ($('#so_no').val() == '') {
            p_notification(false, eb.getMessage('ERR_SALEORDER_EDIT_VALID_NUM'));
        } else if(QIDSelected && qid == ''){
            p_notification(false, eb.getMessage('ERR_SALEORDERS_SELECT_QUOTNO'));
        } else if (custanamefull === '' || custanamefull == 'Select Customer Name' || customerID == '0') {
            p_notification(false, eb.getMessage('ERR_QUOT_VALID_CUST'));
        } else if ($('#payment_term').val() === null) {
            p_notification(false, eb.getMessage('ERR_SALEORDERS_SELECT_PAYTERM'));
        }
//        else if (!isValidDate($('#issue_date').val())) {
//            p_notification(false, eb.getMessage('ERR_QUOT_VALID_ISSDATE'));
//        } else if (!isValidDate($('#completion_date').val())) {
//            p_notification(false, eb.getMessage('ERR_SALEORDERS_ENTER_COMPLEDATE'));

        else if (Object.keys(products).length == 0) {
            if (tableId != "item_tbl") {
                p_notification(false, eb.getMessage('ERR_SALEORDER_EDIT_ADD_ITEM'));
            }
        }
        else {
            var items_ar = Array();
            totaldiscount = 0;
            var taxOfItems = new Array();
            for (var i in products) {
                var temptaxes = {}
                for (var tx in products[i].taxes) {
                    if (products[i].taxes[tx] != false) {
                        temptaxes[tx] = products[i].taxes[tx];
                    }
                }
                taxOfItems.push(JSON.stringify(temptaxes));
            }

            for (var i in products) {
                items_ar.push(products[i]);
                var qty = products[i].quotationProductQuantity;
                if (products[i].quotationProductDiscountType == 2 && qty == 0) {
                    qty = 1;
                }
                if (products[i].quotationProductDiscountType == 'pr') {
                    tmpdisc = (products[i].quotationProductDiscount * qty * products[i].quotationProductUnitPrice / 100);
                } else {
                    tmpdisc = (products[i].discount * qty);
                }
                if (tmpdisc != 0) {
                    totaldiscount += toFloat(tmpdisc);
                }
            }

            var documentReference = {};
            var flagfalse = false;
            $('.documentTypeBody > tr').each(function() {
                if ($(this).hasClass('documentRow')) {
                    var documentTypeID = $(this).find('.documentTypeId').val();
                    // var documentID = $(this).find('.documentId').val();
                    if (Array.isArray($(this).find('.documentId').val())) {
                        var documentID = $(this).find('.documentId').val();
                    } else {
                        var documentID = [];
                        documentID.push($(this).find('.documentId').val());
                    }
                    if (!(documentTypeID == null || documentTypeID == '')) {
                        if (documentID != null) {
                            documentReference[documentTypeID] = documentID;
                        } else {
                            flagfalse = true;
                        }
                    }
                }
            });

            var taxobj = {};
            for (var t in all_tx_val) {
                taxobj[t] = (all_tx_val[t]).toFixed(2);
            }
            var jsontax = JSON.stringify(taxobj);
            var custanamefull = $('#cust_name option:selected').text();
            submit_data = {
                'cust_name': custanamefull,
                'cust_id': customerID,
                'cust_prof_id': cusProfID,
                'payment_term': $('#payment_term').val(),
                'salesPersonID': $('#salesPersonID').val(),
                'invoicecurrentBalance': accounting.unformat($('#invoicecurrentBalance').val()),
                'issue_date': $('#issue_date').val(),
                'completion_date': $('#completion_date').val(),
                'so_no': $('#so_no').val(),
                'deli_amount': accounting.unformat($('#deli_amount').val()),
                'deli_charg': accounting.unformat($('#deli_charg').val()),
                'deli_address': $('#deli_address').val(),
                'cmnt': $('#cmnt').val(),
                'taxAmount': jsontax,
                'sub_total': accounting.unformat($("#subtotal").html()),
                'total_amount': accounting.unformat($('#finaltotal').html()),
                'total_discount': totaldiscount,
                'items': items_ar,
                'documentReference': documentReference,
                'itemTaxes': JSON.stringify(taxOfItems),
                'show_tax': ($('#show_tax').is(':checked')) ? 1 : 0,
                'state': '3',
                'quotationID': $('#quo_no').val(),
                'customCurrencyId': $('#customCurrencyId').val(),
                'customCurrencyRate': customCurrencyRate,
                'priceListId': priceListId
            };
            if (this.id == "sales_orders_form") {
                if (state == "new") {
                    var checkingCreditValue = parseFloat(accounting.unformat($('#finaltotal').html())) + parseFloat(accounting.unformat($('#invoicecurrentBalance').val()));
                    if (currentCreditLimit < checkingCreditValue && $('#payment_term').val() != '1') {
                        p_notification('info', eb.getMessage('ERR_SALEORDERS_CUS_CRDTLIMIT'));
                    }
                    saveSalesOrder(submit_data);
                } else if (state == "saved") {
                    window.location.replace(BASE_URL + "/salesOrders/edit/" + $('#so_no').val());
                }
            }
            else if (this.id == "so_view") {
                var li_box = document.createElement("div");
                var li_box_cont = document.createElement("div");
                var print = document.createElement("div");
                li_box.id = "lightbox_overlay";
                li_box_cont.id = "lightbox_container";
                print.id = "print_overlay";
                li_box.class = "disabl_when_print";
                li_box_cont.class = "disabl_when_print";
                print.class = "disabl_when_print";
                var view_req = eb.post('/salesOrders/viewSalesOrders?' + $.param(submit_data), '', {});
                view_req.done(function() {
                    $(".main_div").addClass("disabl_when_print");
                    $(li_box_cont).append(view_req.responseText);
                    $(li_box).append(li_box_cont);
                    $("body").append(li_box);
                    $("body").append(print);
                    if (state == "saved") {
                        $("#prv_edit").html(edit_btn);
                    } else {
                        $("#prv_edit").html(save);
                    }

                    $("#prv_edit").on("click", function() {
                        if (state == "new") {
                            $("#place_so").trigger(jQuery.Event("click"));
                            $("#prv_edit").html(edit_btn);
                        } else {
                            window.location.assign(BASE_URL + "/salesOrders/Edit/" + $('#so_no').val());
                        }
                    });
                    $("#q_print").on("click", function() {
                        window.print();
                    });
                    $("#q_email").on("click", function() {
                        if (state == "new") {
                            p_notification(false, eb.getMessage('ERR_QUOT_SAVE_SEND'));
                        } else {
                            $("#email-modal").modal("show");
                            $("#send").on("click", function() {
                                var state = validateEmail($("#email_to").val(), $("#email-body").html());
                                if (state) {
                                    var param = {
                                        qout_id: $(this).attr("data-qot_id"),
                                        to_email: $("#email_to").val(),
                                        subject: $("#email_sub").val(),
                                        body: $("#email-body").html()
                                    };
                                    var mail = eb.post("/salesOrdersAPI/sendSalesOrder", param);
                                    mail.done(function(rep) {
                                        if (rep.error) {
                                            p_notification(false, eb.getMessage('ERR_PAY_EMAIL_SENT'));
                                        } else {
                                            p_notification(true, eb.getMessage('SUCC_PAY_EMAIL_SENT'));
                                        }
                                    });
                                    $("#email-modal").modal("hide");
                                }
                            });
                        }
                    });
                    $("#to_rec_so").on("click", function() {
                        if (state == "new") {
                            p_notification(false, eb.getMessage('ERR_QUOT_SAVE_CONVERT'));
                        } else {
                            window.location.assign(BASE_URL + "/salesOrders/recurrentSalesOrder/" + $(this).attr("data-so_id"));
                        }
                    });
                    $("#cancel").on("click", function() {
                        if (state == "new") {
                            $(li_box_cont).remove();
                            $(li_box).remove();
                            $(print).remove();
                        } else {
                            $("#new_qtn").trigger(jQuery.Event("click"));
                        }
                    });
                    $("#new_qtn").on("click", function() { //                        window.location.assign(BASE_URL + "/salesOrders/index");
                        window.location.reload();
                    });
                    $("#to_inv").on("click", function() {
                        if (state == "saved") {
                            window.location.assign(BASE_URL + "/invoice/index/so/" + $('#so_no').val());
                        } else {
                            p_notification(false, eb.getMessage('ERR_SALEORDERS_SAVE_SALEORD'));
                        }
                    });
                });
            }
        }
    });

    $('table#item_tbl>tbody').on('keypress', 'input#qty, input#price, input#discount, input.uomqty', function(e) {

        if (e.which == 13) {
            $('#add_item', $(this).parents('tr')).trigger('click');
            e.preventDefault();
        }

    });

    function setCustomerData(customerID) {
        var url2 = BASE_URL + '/customerAPI/getcustomer';
        if ($('#cust_name').val() == "") {
            $('#invoicecurrentBalance').val('');
            $('#salesOrderCurrentCredit').val('');
            $('#payment_term').val(0);
        } else {
            var request2 = eb.post(url2, {customerID: customerID});
            request2.done(function(customerdata) {
                if (customerdata.status) {
                    var paymentTerm = customerdata.data[0].customerPaymentTerm == null ? 1 : customerdata.data[0].customerPaymentTerm;
                    $('#invoicecurrentBalance').val(accounting.formatMoney(customerdata.data[0].customerCurrentBalance));
                    $('#salesOrderCurrentCredit').val(accounting.formatMoney(customerdata.data[0].customerCurrentCredit));
                    $('#payment_term').val(paymentTerm);
                    $('#cust_id').val(customerdata.data[0].customerID);
                    $('#payment_term').trigger('change');
                } else {
                    p_notification(customerdata.status, customerdata.msg);
                    $('#payment_term').val(0);
                    $('#cur_credit').val('');
                }
                setItemCost();
            });
        }
    }

    $('#payment_term').on('change', function(e) {
        e.preventDefault();
        var days = 0;
        var i = parseInt($(this).val());
        switch (i) {
            case 2:
                days = 7;
                break;
            case 3:
                days = 14;
                break;
            case 4:
                days = 30;
                break;
            case 5:
                days = 60;
                break;
            case 6:
                days = 90;
                break;
            case 7:
                days = 21;
                break;
            case 8:
                days = 28;
                break;
            case 9:
                days = 45;
                break;
            case 10:
                days = 35;
                break;
            default:
                days = 0;
                break;
        }
        setDueDate(days);
    });
    function setDueDate(days) {
        var day = parseInt(days);
        var joindate = eb.convertDateFormat('#issue_date');
        joindate.setDate(joindate.getDate() + day);

        var dd = joindate.getDate() < 10 ? '0' + joindate.getDate() : joindate.getDate();
        var mm = (joindate.getMonth() + 1) < 10 ? '0' + (joindate.getMonth() + 1) : (joindate.getMonth() + 1);
        var y = joindate.getFullYear();
        var joinFormattedDate = eb.getDateForDocumentEdit('#completion_date', dd, mm, y);

        $("#completion_date").val(joinFormattedDate);
    }

    $('#add-cust_modal').on('hide.bs.modal', function() {
        $('#add-cust_modal').removeData();
        return true;
    });
    $(document).on('click', '#moredetails', function() {
        $("#customer_more").fadeIn();
        $(this).hide();
    });
    $('#item_tbl').on('change', '.addNewUomR', function() {
        var tmpUomOb = currentProducts[$('#item_code').val()].uom;
        var tmpUomID = $(".uomLi input[type='radio']:checked").val();
        $('#uomAb').html(tmpUomOb[tmpUomID].uA);
    });
    ///////DatePicker\\\\\\\
    var nowTemp = new Date();
    var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);
    var checkin = $('#issue_date').datepicker({
        onRender: function(date) {
            return date.valueOf() < now.valueOf() ? ((U_DATE_R_OVERRIDE)?'':'disabled') : '';
        }}).on('changeDate', function(ev) {
        if (ev.date.valueOf() > checkout.date.valueOf()) {
            var newDate = new Date(ev.date)
            newDate.setDate(newDate.getDate() + 1);
            checkout.setValue(newDate);
        }
        var QuotationIssuedDate = new Date(Date.parse(quotationIssuedDate));
        if (quotationIssuedDate != null) {
            if (ev.date < QuotationIssuedDate) {
                p_notification(false, eb.getMessage('ERR_SALEORDERS_ENTER_DATE', quotationIssuedDate));
                $(this).val('');
            }
        }
        checkin.hide();
    }).data('datepicker');
    checkin.setValue(now);
    var checkout = $('#completion_date').datepicker({
    }).on('changeDate', function(ev) {
        if (ev.date.valueOf() < checkin.date.valueOf()) {
            p_notification(false, eb.getMessage('ERR_SALEORDERS_AFT_ISSDATE'));
            $(this).val('');
        }
        checkout.hide();
    }).data('datepicker');
    checkout.setValue(now);
/////EndOFDatePicker\\\\\\\\\

    $("#item_tbl").on('change', '.taxChecks', function() {
        var oldrow = $(this).parents('tr');
        setItemCost(oldrow);
    });

    $("#item_tbl").on('click', '#selectAll', function() {
        $(this).parents('#addTaxUl').find('input:checkbox.addNewTaxCheck').each(function() {
            if (!this.checked) {
                this.checked = true;
            }
        });
        var oldrow = $(this).parents('tr');
        setItemCost(oldrow);
    });

    $("#item_tbl").on('click', '#deselectAll', function() {
        $(this).parents('#addTaxUl').find('input:checkbox.addNewTaxCheck').each(function() {
            if (this.checked) {
                this.checked = false;
            }
        });
        var oldrow = $(this).parents('tr');
        setItemCost(oldrow);
    });

    $("#item_tbl").on('click', 'ul.dropdown-menu', function(e) {
        e.stopPropagation();
    });
    //responsive layout issue fix
    $(window).bind('resize ready', function() {
        if ($(window).width() <= 480) {
            $('.quotation_button_set').removeClass('btn-group');
            $('#place_so, #so_view, #reset').addClass('col-xs-12').css('margin-bottom', '8px');
        } else {
            $('.quotation_button_set').removeClass('col-xs-12');
            $('#place_so, #so_view, #reset').removeClass('col-xs-12').css('margin-bottom', '0px');
        }
    });

    $('#customCurrencyId').on('change', function() {
        eb.ajax({
            type: 'POST',
            url: BASE_URL + '/currencySymbol/getCustomCurrencyByCustomCurrencyId',
            data: {customCurrencyId: $(this).val()},
            success: function(respond) {
                if (respond.status == true) {
                    $('.cCurrency').text(respond.data.currencySymbol);
                    companyCurrencySymbol = respond.data.currencySymbol;
                }
            }
        });
    });

    $('#priceListId').on('change', function() {
        priceListId = $(this).val();
        eb.ajax({
            type: 'POST',
            url: BASE_URL + '/currencySymbol/getPriceListWithDiscount',
            data: {priceListId: priceListId},
            success: function(respond) {
                if (respond.status == true) {
                    priceListItems = respond.data;
                } else {
                    priceListItems = [];
                }
            },
            async: false
        });
    });

    //Add item description
    $('#item_tbl').on('click', '.addItemDescription', function() {
        if ($('i', $(this)).hasClass('fa-comment-o')) {
            $('i', $(this)).removeClass('fa-comment-o').addClass('fa-comment');
            $(this).parents('tr').addClass('comment');
            $(this).parent().parent().siblings('.itemDescText').removeClass('hidden').focus();
        } else {
            $('i', $(this)).removeClass('fa-comment').addClass('fa-comment-o');
            $(this).parents('tr').removeClass('comment');
            $(this).parent().parent().siblings('.itemDescText').addClass('hidden');
        }
    });


	//show item description
    $('#item_tbl').on('click', '.showItemDescription', function() {
        if ($('i', $(this)).hasClass('fa-comment-o')) {
            $('i', $(this)).removeClass('fa-comment-o').addClass('fa-comment');
            $(this).parents('tr').addClass('comment');
            $(this).parent().parent().siblings('.showItemDescText').removeClass('hidden').focus();
        } else {
            $('i', $(this)).removeClass('fa-comment').addClass('fa-comment-o');
            $(this).parents('tr').removeClass('comment');
            $(this).parent().parent().siblings('.showItemDescText').addClass('hidden');
        }
    });


    $('#item_tbl').on('blur', '.itemDescText', function(e) {
        e.preventDefault();
        $(this).addClass('hidden');
        $(this).parent().find('.addItemDescription').find('i').removeClass('fa-comment').addClass('fa-comment-o');
    });

    $('#item_tbl').on('blur', '.showItemDescText', function(e) {
        e.preventDefault();
        $(this).addClass('hidden');
        $(this).parent().find('.showItemDescription').find('i').removeClass('fa-comment').addClass('fa-comment-o');
    });

});

function addProduct(code, pName, quantity, price, uom, dscType, dscAmount, tax, total, uomqty, productType, productID, uomPrice, oldrow, item_discription) {
    if (code === "" || code == undefined) {
        p_notification(false, eb.getMessage('ERR_QUOT_PRCODE_EMPTY'));
        return false;
    } else if (products[code+'_'+price] !== undefined) {
        p_notification(false, eb.getMessage('ERR_SAME_ITEM_IN_SO'));
        return false;
    } else if (pName === "") {
        p_notification(false, eb.getMessage('ERR_QUOT_PRNAME_EMPTY'));
        return false;
    } else if (productType == 1 && (uomqty === "" || uomqty <= 0)) {
        p_notification(false, eb.getMessage('ERR_QUOT_QUAN_VALUE'));
        return false;
    } else if (uom === undefined) {
        p_notification(false, eb.getMessage('ERR_QUOT_SELECT_UOM'));
        return false;
    } else if (price <= 0) {
        p_notification(false, eb.getMessage('ERR_QUOT_ENTER_PRICE'));
        return false;
    } else if (dscType === 'vl' && parseFloat(dscAmount) > price) {
        p_notification(false, eb.getMessage('ERR_QUOT_DISCVAL_PRODPRICE'));
        return false;
    } else {
    	var showDiscHtml = '<div class="pull-right input-group">'+
    	'<span class="input-group-btn">'+
    	'<button  type="button" class="btn btn-default showItemDescription" title="Show comment">'+
    	'<i class="fa fa-comment-o"></i></button></span></div>'+
    	'<textarea class="showItemDescText form-control hidden" readonly="true" placeholder="Enter item description">'+
    	'</textarea>';

        var itemTot = 0;
        var dummy = $($("#preSetSample").clone()).attr("id", "tr_" + code);
        dummy.children("#code").html(code + " - " + pName + " " + showDiscHtml);
        dummy.children("#quantity").html(uomqty);
        dummy.children("#unit").html(accounting.formatMoney(price * currentProducts[productID].uom[uom].uC) + " (" + currentProducts[productID].uom[uom].uA + ")");
        dummy.children().children('#uomName').html(currentProducts[productID].uom[uom].uA);
        if (dscType === 'vl' && dscAmount > 0) {
            dummy.children("#disc").html(accounting.formatMoney(dscAmount * currentProducts[productID].uom[uom].uC) + '&nbsp;(' + currentProducts[productID].uom[uom].uA + ')' + '&nbsp;(' + companyCurrencySymbol + ')');
        } else if (dscType === 'pr' && dscAmount > 0) {
            dummy.children("#disc").html(accounting.formatMoney(dscAmount) + '%');
        }

        dummy.children().children(".delete").attr("id", code);
        dummy.children("#code").find('.showItemDescText').val(item_discription);
        var clonedTax = $($('#addNewTaxDiv').clone()).attr('id', '');
        clonedTax.find('.tempLi').each(function() {
            if ($(this).children(".taxChecks").prop('checked') != true) {
                $(this).children(".taxName").addClass('crossText');
            }
        });
        clonedTax.children('#addNewTax').children('#taxApplied').attr('id', '');
        clonedTax.children('#addNewTax').attr('id', '');
        clonedTax.children('#addTaxUl').children().removeClass('tempLi');
        clonedTax.children('#addTaxUl').children().children().removeClass('addNewTaxCheck');
        clonedTax.children('#addTaxUl').children().children(".taxChecks").remove();
        clonedTax.children('#addTaxUl').children('#sampleLi').remove();
        clonedTax.children('#addTaxUl').attr('id', '');
        clonedTax.find('#toggleSelection').addClass('hidden');
        dummy.children("#tax").html(clonedTax);

        if (accounting.unformat($("#total", oldrow).html()) == "0.00" && parseFloat(total) > 0) {
            itemTot = total;
        } else {
            itemTot = accounting.unformat($("#total", oldrow).html());
        }
        dummy.children("#ttl").html(accounting.formatMoney(itemTot));
        dummy.removeClass("hide").insertBefore("#preSetSample");
        var qty = quantity;
        taxdetails = taxDetailsFromQut[productID];
        products[code+'_'+price] = new product(code, pName, qty, uom, price, dscAmount, dscType, tax, itemTot, taxdetails, productType, item_discription);
        clearTable(oldrow);
    }
}


function clearTable(oldrow) {
    $("#item_code", oldrow).val("");
    $('#item_code', oldrow).selectpicker('render');
    $("#qty", oldrow).val("");
    $("#qty", oldrow).siblings('.uomqty').remove();
    $("#qty", oldrow).siblings('.uom-select').remove();
    $('#qty', oldrow).parent().removeClass('input-group');
    $("#qty", oldrow).show();
    $("#price", oldrow).val("");
    $("#price", oldrow).siblings('.uomPrice').remove();
    $("#price", oldrow).siblings('.uom-price-select').remove();
    $("#price", oldrow).show();
    $("#discount", oldrow).val("");
    $("#discount", oldrow).siblings('.uomPrice').remove();
    $("#discount", oldrow).siblings('.uom-price-select').remove();
    $("#discount", oldrow).show();
    $("#total", oldrow).html("0.00");
    $("#discount", oldrow).attr('disabled', false);
    $("#discount", oldrow).val("");
    $("#dic_lbl", oldrow).html("Dis.nt");
    $('.uomLi', oldrow).remove();
    $('#taxApplied', oldrow).removeClass('glyphicon glyphicon-checked');
    $('#taxApplied', oldrow).addClass('glyphicon glyphicon-unchecked');
    $('#addNewTax', oldrow).attr('disabled', false);
    $('#discType', oldrow).html('');
    $('.tempLi', oldrow).remove();
    $('.itemDescText', oldrow).val('');
}


/**
 * Prepare tax list (Drop down)
 * @param {type} productCode
 */
function setTaxListForProduct(productID, clonedAddNew) {
    if ((!jQuery.isEmptyObject(currentProducts[productID].tax))) {
        productTax = currentProducts[productID].tax;
        $('#addNewTax', clonedAddNew).attr('disabled', false);
        $('#taxApplied', clonedAddNew).removeClass('glyphicon glyphicon-unchecked');
        $('#taxApplied', clonedAddNew).addClass('glyphicon glyphicon-check');
        $('.tempLi', clonedAddNew).remove();
        for (var i in productTax) {
            var clonedLi = $($('#sampleLi', clonedAddNew).clone()).removeClass('hidden').attr('id', 'li_' + i).addClass("tempLi");
            clonedLi.children(".taxChecks").attr('value', i).prop('checked', true).addClass('addNewTaxCheck');
            if (productTax[i].tS == 0) {
                clonedLi.children(".taxChecks").attr('disabled', 'disabled');
                clonedLi.children(".taxName").addClass('crossText');
            }
            clonedLi.children(".taxName").html('&nbsp&nbsp' + productTax[i].tN + '&nbsp&nbsp' + productTax[i].tP + '%').attr('for', productID + '_' + i);
            clonedLi.insertBefore($('#sampleLi', clonedAddNew));
        }
    } else {
        $('#taxApplied', clonedAddNew).addClass('glyphicon glyphicon-unchecked');
        $('#addNewTax', clonedAddNew).attr('disabled', 'disabled');
    }

}

/**
 *
 * @param {type} productCode
 * @param {type} price
 * @param {type} disc
 * @param {type} qty
 * @returns {unresolved}
 */
function calculateDiscount(productID, price, disc, qty, clonedAddNew) {
    var dis_amount = 0;
    if (currentProducts[productID] !== undefined && currentProducts[productID].dEL === "1") {

        if (priceListItems[productID] == undefined) {
            var pdp = parseFloat(currentProducts[productID].dPR);
            var pdv = parseFloat(currentProducts[productID].dV);
            if (pdv >= 0) {             //Check Customer discount against product discount
                if (disc > pdv && pdv != 0) {
                    p_notification(false, eb.getMessage('ERR_QUOT_DISC_VALUE'));
                    dis_amount = (qty * pdv);
                    $('#discount',clonedAddNew).val(accounting.formatMoney(pdv));
                } else if (pdv == 0){
                    if (disc > $('#itemPrice').val()) {
                        p_notification(false, eb.getMessage('ERR_INVO_DISCOUNT_VALUE_UNITE_P'));
                        dis_amount = (qty * $('#itemPrice').val());
                        $('#discount',clonedAddNew).val(accounting.formatMoney($('#itemPrice').val()));
                    } else {
                        dis_amount = (qty * disc);
                    }
                } else {
                    if (price > 0 && qty > 0 && disc > 0)
                        dis_amount = (qty * disc);
                }
            } else {
    //Check Customer discount against product discount
                if (pdp != 0) {
                    if (disc > pdp) {
                        p_notification(false, eb.getMessage('ERR_QUOT_DISC_VALUE'));
                        dis_amount = (price * pdp / 100);
                        $('#discount', clonedAddNew).val(accounting.formatMoney(pdp));
                    } else {
                        if (price > 0 && disc > 0)
                            dis_amount = (price * disc / 100);
                    }
                } else {
                    if (price > 0 && disc > 0)
                        dis_amount = (price * disc / 100);
                }

            }
        } else {
            var itemDiscountType = priceListItems[productID].itemDiscountType;
            if (itemDiscountType == 1) {
                dis_amount = (qty * disc);
            } else {
                dis_amount = (price * disc / 100);
            }
        }
    } else if (priceListItems[productID] != undefined && parseFloat(priceListItems[productID].itemPrice) > 0) {
        var itemDiscountType = priceListItems[productID].itemDiscountType;
        if (itemDiscountType == 1) {
            dis_amount = (qty * disc);
        } else {
            dis_amount = (price * disc / 100);
        }
    }
    return dis_amount;
}

function clearAllData() {
    currentProducts = new Array();
    $("#item_code").val("");
    $('#item_code').selectpicker('render');
    $('#subtotal').val('0.00');
    $('#finaltotal').val('0.00');
    $('#salesPersonID').val('');
    $('#salesPersonID').prop('disabled', false);
    $('#salesPersonID').selectpicker('refresh');
    tax_rates = new Array();
    products = new Array();
    taxdetails = new Array();
    all_tx_val = new Array();
    $('.itemlist').find('tr').each(function() {
        if (this.id != 'preSetSample') {
            $(this).remove();
        }
    });
    $('#item_insert').find('tr').each(function() {
        if ($(this).hasClass('tempSoPro')) {
            $(this).remove();
        }
    });
}

function getCustomerProfilesDetails(customerID) {
    eb.ajax({
        type: 'POST',
        url: BASE_URL + '/customerAPI/getCustomerProfilesByCustomerID',
        data: {customerID: customerID},
        success: function(respond) {
            if (respond.status == true) {
                $('.cus_prof_div').removeClass('hidden');
                $('.cus-prof-select').removeClass('hidden');
                $('.tooltip_for_default_cus').removeClass('hidden');
                $('.default-cus-prof').addClass('hidden');
                setCustomerProfilePicker(respond.data['customerProfileData']);
            } else {
                $('.cus_prof_div').addClass('hidden');
                $('.cus-prof-select').addClass('hidden');
                $('.tooltip_for_default_cus').addClass('hidden');
                $('.default-cus-prof').removeClass('hidden');

            }
        }
    });
}

function setCustomerProfilePicker(data) {
    $('#cusProfileSO').html("<option value=''>" + "Select a Customer Profile" + "</option>");
    $.each(data, function(index, value) {
        $('#cusProfileSO').append("<option value='" + index + "'>" + value['profName'] + "</option>");
        if (value['isPrimary'] == 1) {
            $('#cusProfileSO').html("<option value='" + index + "'>" + value['profName'] + "</option>");
            cusProfID = index;
        }
    });
    $('#cusProfileSO').selectpicker('refresh');

    $('#cusProfileSO').on('change', function(e) {
        e.preventDefault();
        if ($(this).val() > 0 && ($(this).val() != cusProfID)) {
            cusProfID = $(this).val();
        } else {
            cusProfID = '';
        }
    });
}