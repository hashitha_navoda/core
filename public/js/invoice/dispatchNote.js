$(document).ready(function () {

    var invoiceId = null;
    var $productTable = $("#productTable");
    var $addRowSample = $('tr.add-row.sample.hidden', $productTable);
    var invoiceProductId = null;
    var productTypeStatus = null;
    var productType = null;
    var subProducts = null;
    var dispatchQty = null;
    var saveBtnStatus = true;
    var dispatchObj = {};
    var productArr = {};
    var subProductArr = {};

    var dispatchDate = $('.datepicker').datepicker({
        format: 'yyyy-mm-dd'
    }).on('changeDate', function () {
        dispatchDate.hide();
    }).data('datepicker');

    //get invoice for serach box
    var posflag = 2;
    loadDropDownFromDatabase('/invoice-api/searchSalesInvoicesForDropdown', "", posflag, '#invoiceNo');

    //select invoice
    $('#invoiceNo').on('change', function () {
        invoiceId = $(this).val();
        //remove generated tr
        $('tr.add-row:not(.sample)').remove();
        if (invoiceId > 0) {
            getInvoiceDetails(invoiceId);
        } else {
            resetFields();
        }
    });

    //for validate dispatch quantity
    $(document).on('focusout', '.dispatchQuanity', function () {
        var dispatchQuanity = $(this).val();
        var invoiceQuantity = $(this).closest('tr').find('.invoiceQuantity').val();
        if (parseFloat(dispatchQuanity) > parseFloat(invoiceQuantity)) {
            p_notification(false, "Dispatch Quanity can't grater than Invoice Quantity");
            $(this).val(invoiceQuantity);
        } else if (parseFloat(dispatchQuanity) < 0) {
            p_notification(false, "Dispatch Quanity can't be minus value");
            $(this).val(invoiceQuantity);
        }
    });

    //for sub product modal
    $(document).on('click', '.sub-pro', function () {
        invoiceProductId = $(this).closest('tr').attr('data-id');
        productType = $(this).closest('tr').attr('data-product-type');
        dispatchQty = $(this).closest('tr').find('.dispatchQuanity').val();

        $('tr.remove_row').remove();
        $('#batch-save').attr("disabled", true);

        switch (productType) {
            case 'serialProduct':
                addSerialProduct(subProducts['serial'][invoiceProductId]);
                //for show selected sub product values
                if(!$.isEmptyObject(subProductArr) && subProductArr.hasOwnProperty(invoiceProductId)){
                    $.each(subProductArr[invoiceProductId], function(i,v){
                        $('#batch_data tr[data-id="'+i+'"]').find('.dispatchQuantityCheck').prop('checked', true);
                    });
                }
                break;

            case 'batchProduct':
                addBatchProduct(subProducts['batch'][invoiceProductId]);//for show selected sub product values
                //for show selected sub product values
                if(!$.isEmptyObject(subProductArr) && subProductArr.hasOwnProperty(invoiceProductId)){
                    $.each(subProductArr[invoiceProductId], function(i,v){
                        $('#batch_data tr[data-id="'+i+'"]').find('.dispatchQuantity').val(v.quantity);
                    });
                }
                break;

            default :
                console.log('invalid option');
        }

        $('#addDispatchNoteProductsModal').modal('show');
    });

    //for add product
    $(document).on('click', '.add', function () {
        productTypeStatus = $(this).closest('tr').attr('data-product-type-status');
        if (productTypeStatus == '1') {
            var $closestRow = $(this).closest('tr');
            $closestRow.find('.edit-modal,.add').addClass('hidden');
            $closestRow.find('.edit').removeClass('hidden');
            $closestRow.find('.delete').removeClass('disabled');
            $closestRow.find('.dispatchQuanity').attr('disabled', true);
            $closestRow.attr('data-product-save-status', '1');
        } else {
            var productName = $(this).closest('tr').find('#itemCode').val();
            p_notification(false, "Please select sub product for '" + productName + "' product.");
        }
    });

    //for save product
    $(document).on('click', '.save', function () {
        var $closestRow = $(this).closest('tr');
        $closestRow.find('.edit-modal,.save').addClass('hidden');
        $closestRow.find('.edit').removeClass('hidden');
        $closestRow.find('.delete').attr('disabled', false);
        $closestRow.find('.dispatchQuanity').attr('disabled', true);
        $closestRow.attr('data-product-save-status', '1');
    });

    //for edit product
    $(document).on('click', '.edit', function () {
        var $closestRow = $(this).closest('tr');
        $closestRow.find('.edit').addClass('hidden');
        $closestRow.find('.save').removeClass('hidden');
        $closestRow.find('.dispatchQuanity').attr('disabled', false);
        $closestRow.attr('data-product-save-status', '0');
        var productType = $(this).closest('tr').attr('data-product-type');
        if (productType != 'none') {
            $(this).closest('tr').find('.edit-modal').removeClass('hidden');
        } else {
            $(this).closest('tr').find('.edit-modal').addClass('hidden');
        }
    });

    //for delete product
    $(document).on('click', '.delete', function () {
        $(this).closest('tr').remove();
    });

    //for sub product save
    $(document).on('click', '#batch-save', function () {
        //for chenage sub product data attribute
        $('#dispatchNoteProductTable tr[data-id="' + invoiceProductId + '"]').attr('data-product-type-status', '1');
        var temp = {};
        if (productType == 'serialProduct') {
            $('.dispatchQuantityCheck:checked').each(function () {
                var thisSubProduct = {};
                var key = $(this).closest('tr').attr('data-id');
                thisSubProduct.serialId = $(this).closest('tr').attr('data-id');
                thisSubProduct.batchId = null;
                thisSubProduct.quantity = $(this).closest('tr').find('.availableQuantity').val();

                temp[key] = thisSubProduct;
            });
        } else {
            $('#batch_data tr.remove_row').each(function () {
                var thisSubProduct = {};
                var key = $(this).closest('tr').attr('data-id');
                thisSubProduct.serialId = null;
                thisSubProduct.batchId = $(this).attr('data-id');
                thisSubProduct.quantity = $(this).find('.dispatchQuantity').val();
                
                temp[key] = thisSubProduct;
            });
        }
        subProductArr[invoiceProductId] = temp;
        
        //close modal
        $('#addDispatchNoteProductsModal').modal('hide');
    });
    
    //for save dispatch note
    $(document).on('click', '#dispatchNoteSaveButton', function () {
        productArr = {};
        $('#add-new-item-row tr.add-row:not(.sample)').each(function () {
            var saveStatus = $(this).attr('data-product-save-status');
            if(saveStatus == '1'){
                var thisProduct = {};
                thisProduct.dispatchQuantity = $(this).find('.dispatchQuanity').val();
                var key = $(this).attr('data-id');
                productArr[key] = thisProduct;
            }
        }).promise().done(function () {
            dispatchObj.invoiceId = invoiceId;
            dispatchObj.dispatchNoteCode = $('#dispatchNoteCode').val();
            dispatchObj.dispatchNoteAddress = $('#dispatchNoteAddress').val();
            dispatchObj.dispatchNoteComment = $('#dispatchNoteComment').val();
            dispatchObj.dispatchNoteDate = $('#dispatchNoteDate').val();
            dispatchObj.salesPersonId = $('#salesPersonId').val();
            dispatchObj.products = productArr;
            dispatchObj.subProducts = subProductArr;
            
            if(validateDispatchForm(dispatchObj)){
                eb.ajax({
                    type: 'POST',
                    url: BASE_URL + '/dispatch-note-api/create',
                    data: dispatchObj,
                    success: function (respond) {
                        p_notification(respond.status, respond.msg);
                        if (respond.status) {
                            $('#dispatchNoteSaveButton').attr( 'disabled', true);
                            documentPreview(BASE_URL + '/dispatch-note/document-preview/' + respond.data, 'documentpreview', "/dispatch-note-api/send-email", function($preview) {
                                $preview.on('hidden.bs.modal', function() {
                                    if (!$("#preview:visible").length) {
                                        window.location.reload();
                                    }
                                });
                            });
                        }
                    }
                });
            }
        });
    });

    //for select serial product sub quantity
    $(document).on('click', '.dispatchQuantityCheck', function () {
        var serialCount = $('.dispatchQuantityCheck:checked').length;
        if (parseInt(dispatchQty) == serialCount) {
            $('#batch-save').attr("disabled", false);
        } else {
            $('#batch-save').attr("disabled", true);
        }
    });

    //for batch product dispatch quantity
    $(document).on('keyup', '.dispatchQuantity', function () {
        var batchDispatchQty = $(this).val();
        var availableBatchQty = $(this).closest('tr').find('.availableQuantity').val();
        if (parseFloat(availableBatchQty) < parseFloat(batchDispatchQty)) {
            $(this).val('0');
            p_notification(false, "Dispatch Quanity can't grater than Available Quantity");
        } else {
            var dispatchCount = 0.00;
            $('#batch_data tr.remove_row').each(function () {
                var count = $(this).closest('tr').find('.dispatchQuantity').val();
                dispatchCount = dispatchCount + parseFloat(count);
            });
            if (parseFloat(dispatchQty) == dispatchCount) {
                $('#batch-save').attr("disabled", false);
            } else {
                $('#batch-save').attr("disabled", true);
            }
        }
    });
    
    //for cancel btn
    $(document).on('click','#dispatchNoteCancelButton',function(){
        location.reload();
    });

    //get invoice details
    function getInvoiceDetails(invoiceId)
    {
        eb.ajax({
            type: 'POST',
            url: BASE_URL + '/dispatch-note-api/get-invoice-details-for-dispatch-note',
            data: {
                invoiceId: invoiceId
            },
            success: function (respond) {
                if (respond.status) {
                    subProductArr = {};
                    subProducts = respond.data.subProducts;

                    //set data to dispatch form
                    $('#customer').val(respond.data.invoiceDetails.customerName);
                    $('#location').val(respond.data.invoiceDetails.locationName);
                    $('#dispatchNoteAddress').val(respond.data.invoiceDetails.salesInvoiceDeliveryAddress);
                    $('#dispatchNoteSaveButton').attr('disabled',false);

                    //generate rows
                    $.each(respond.data.productDetails, function (index, value) {
                        var currentRow = $($addRowSample.clone()).removeClass('sample hidden');
                        currentRow.attr('data-id', value.salesInvoiceProductID);
                        //set product type
                        if (value.batchProduct == '1') {
                            currentRow.attr('data-product-type', 'batchProduct');
                            currentRow.attr('data-product-type-status', '0');
                        } else if (value.serialProduct == '1') {
                            currentRow.attr('data-product-type', 'serialProduct');
                            currentRow.attr('data-product-type-status', '0');
                        } else {
                            currentRow.attr('data-product-type', 'none');
                            currentRow.attr('data-product-type-status', '1');
                            currentRow.find('.sub-pro').addClass('hidden');
                        }
                        currentRow.attr('data-product-save-status', '0');
                        currentRow.find('#itemCode').val(value.productName);
                        currentRow.find('#invoiceQuantity').val(value.salesInvoiceProductQuantity);
                        currentRow.find('#dispatchQuanity').val(value.salesInvoiceProductQuantity);
                        currentRow.find('.uom').text(value.uomAbbr);
                        
                        if(parseFloat(value.salesInvoiceProductQuantity) == 0){
                            currentRow.find('#dispatchQuanity').attr('disabled',true);
                            currentRow.find('.edit-modal,.add').addClass('disabled');                            
                        } else {
                            saveBtnStatus = false;
                        }
                        $('#dispatchNoteSaveButton').attr('disabled',saveBtnStatus);
                        currentRow.appendTo($('#add-new-item-row'));
                    });

                } else {
                    p_notification(respond.status, respond.msg);
                }
            }
        });
    }

    function resetFields() {
        $('#customer').val('');
        $('#location').val('');
        $('#dispatchNoteAddress').val('');
    }

    function addSerialProduct(subProducts) {
        if ((!$.isEmptyObject(subProducts)))
        {
            $.each(subProducts, function (index, value) {
                var currentRow = $($('.batch_row').clone());
                currentRow.attr('data-id', value.productSerialID).removeAttr('id').removeClass('hidden batch_row').addClass('remove_row');
                currentRow.find(".dispatchQuantity,.dq-uom").addClass('hidden');
                currentRow.find(".serialId").text(value.productSerialCode);
                currentRow.find(".availableQuantity").val(parseFloat(value.salesInvoiceSubProductQuantity).toFixed(2));
                currentRow.find(".warranty").val(value.productSerialWarrantyPeriod);
                currentRow.find('.aq-uom').text(value.uomAbbr);

                currentRow.appendTo($('#batch_data'));
            });
        }
    }

    function addBatchProduct(subProducts) {
        if ((!$.isEmptyObject(subProducts)))
        {
            $.each(subProducts, function (index, value) {
                var currentRow = $($('.batch_row').clone());
                currentRow.attr('data-id', value.productBatchID).removeAttr('id').removeClass('hidden batch_row').addClass('remove_row');
                currentRow.find(".dispatchQuantityCheck").addClass('hidden');
                currentRow.find(".batchCode").text(value.productBatchCode);
                currentRow.find(".availableQuantity").val(parseFloat(value.salesInvoiceSubProductQuantity));
                currentRow.find(".dispatchQuantity").val('0');
                currentRow.find(".warranty").val(value.productSerialWarrantyPeriod);
                currentRow.find('.aq-uom,.dq-uom').text(value.uomAbbr);

                currentRow.appendTo($('#batch_data'));
            });
        }
    }

    function validateDispatchForm(input) {
        var savedRows = $('#add-new-item-row tr[data-product-save-status="1"]').length;
        if (savedRows <= 0) {
            p_notification(false, "Please add at least one item to dispatch note.");
        } else if (input.dispatchNoteDate == null || input.dispatchNoteDate == "") {
            p_notification(false, "Please enter a dispatch Date.");
        } else if (input.dispatchNoteAddress == null || input.dispatchNoteAddress == "") {
            p_notification(false, "Please enter delivery address.");
        } else {
            return true;
        }
    }

});