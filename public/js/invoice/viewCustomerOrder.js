var loadQuotationPreview;
var customerID;
var selectedQuID;

$(document).ready(function() {

    if (!$("#filter-button").length) {
        return false;
    }

    $('#searchType').val('Customer Order ID');

    function deletequotation(id) {
        $(document).on('click', "#delete-quotation-button", function() {
            eb.post(BASE_URL + '/customerOrder-api/deleteCustomerOrder', {
                id: id
            }).done(function(respond) {
                if (respond.status == true) {
                    window.location.reload();
                } else {
                    p_notification(respond.status, respond.msg);
                }
            });
        });
    }

    $(document).on('click', ".deletes", function(e) {
        e.preventDefault();
        var id = this.id;
        deletequotation(id);
    });

    $(document).on('click', "a.editsInactive", function(e) {
        p_notification(false, eb.getMessage('ERR_CUST_INACTIVE'));
    });

    loadDropDownFromDatabase('/customerAPI/search-customers-for-dropdown', "", 'withDeactivatedCustomers', '#cname');
    $('#cname').on('change', function() {
        if ($(this).val() > 0 && customerID != $(this).val()) {
            customerID = $(this).val();
            getCustomerWiseCustomerOrderListView($(this).val());
        }
    });
    $("#cname").selectpicker('hide');
    $("#corefid").selectpicker('hide');

    loadDropDownFromDatabase('/customerOrder-api/search-all-customer-order-for-dropdown', currentLocationID, 0, '#qid');
    $('#qid').on('change', function() {
        if ($(this).val() > 0 && selectedQuID != $(this).val()) {
            selectedQuID = $(this).val();
            getCustomerOrderListView($(this).val());
        }
    });

    loadDropDownFromDatabase('/customerOrder-api/search-all-customer-order-by-ref-no-for-dropdown', currentLocationID, 0, '#corefid');
    $('#corefid').on('change', function() {
        if ($(this).val() > 0 && selectedQuID != $(this).val()) {
            selectedQuID = $(this).val();
            getCustomerOrderListView($(this).val());
        }
    });

    function getCustomerOrderListView(customerOrderID) {
        if (customerOrderID == '') {
            $('#cqview').hide();
            $('#cqview').html('');
            $("#create-quotation-form-div").show();
        } else {
            var searchurl = BASE_URL + '/customerOrder-api/getCustomerOrderListFromSearch';
            var searchrequest = eb.post(searchurl, {customerOrderID: customerOrderID});
            searchrequest.done(function(respond) {

                $('#contactList').html(respond.html);
                // $("#quotation-list", "#create-quotation-form-div").html(searchdata);
            });
            return false;
        }

    }

    $('#reset-button').on('click', function(){
        window.location.reload();
    });


    function getCustomerOrderPaginatedListView() {
        var searchurl = BASE_URL + '/customerOrder-api/getCustomerOrderListFromSearch';
        var searchrequest = eb.post(searchurl, {customerOrderID: null});
        searchrequest.done(function(respond) {

            $('#contactList').html(respond.html);
            // $("#quotation-list", "#create-quotation-form-div").html(searchdata);
        });
        return false;

    }

    function getCustomerWiseCustomerOrderListView(customerID) {
        var url2 = '/customerOrder-api/getCustomerWiseCustomerOrder';
        var request1 = eb.post(url2, {
            customerID: customerID
        });
        request1.done(function(respond) {
            $('#contactList').html(respond.html)
        });
    }

    $('#searchType').on('change', function() {
        getCustomerOrderPaginatedListView();
        if ($(this).val() == 'Customer Order ID') {
            // $("#cqview").hide();
            // $("#create-quotation-form-div").show();
            $('#qid').selectpicker('show');
            $('#corefid').selectpicker('hide');
            $('#cname').selectpicker('hide');
            $('#qid').val('');
            $('#qid').selectpicker('render');
            $('#qid').trigger('change');
            selectedgetmethod = 'Customer Order ID';
        } else if ($(this).val() == 'Customer Name') {
            // $("#create-quotation-form-div").show();
            $('#qid').selectpicker('hide');
            $('#corefid').selectpicker('hide');
            $('#cname').selectpicker('show');
            $('#cname').val('');
            $('#cname').selectpicker('render');
            $('#cname').trigger('change');
            // $("#cqview").hide();
            selectedgetmethod = 'Customer Name';
        } else if ($(this).val() == 'Customer Order RefID') {
            $('#qid').selectpicker('hide');
            $('#corefid').selectpicker('show');
            $('#cname').selectpicker('hide');
            $('#corefid').val('');
            $('#corefid').selectpicker('render');
            $('#corefid').trigger('change');
        }
    });


    $('#filter-button').on('click', function() {
        if ($('#from-date').val() == '' || $('#to-date').val() == '') {
            p_notification(false, eb.getMessage('ERR_VIEWPAY_FILLDATA'));
        } else {
            var quotationfilter = BASE_URL + '/customerOrder-api/getCustomerOrderByDatefilter';
            if (!$("#cname").val()) {
                customerID = '';
            }
            var filterrequest = eb.post(quotationfilter, {
                'fromdate': $('#from-date').val(),
                'todate': $('#to-date').val(),
                'customerID': customerID
            });
            filterrequest.done(function(retdata) {
                $('#contactList').html(retdata.html)
            });
        }
    });

    ///////DatePicker\\\\\\\
    var nowTemp = new Date();
    var frmdate;
    var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);

    var checkin = $('#from-date').datepicker().on('changeDate', function(ev) {
        checkin.hide();
        frmdate = ev.date;
        $('#to-date')[0].focus();
    }).data('datepicker');
    var checkout = $('#to-date').datepicker({
        onRender: function(date) {
            return date.valueOf() < frmdate ? 'disabled' : '';
        }
    }).on('changeDate', function(ev) {
        checkout.hide();
    }).data('datepicker');
    /////EndOFDatePicker\\\\\\\\\


    //responsive issues
    $(window).bind('resize ready', function() {
        ($(window).width() <= 1200) ? $('#filter-button').addClass('margin_top_low') : $('#filter-button').removeClass('margin_top_low');
    });

    $(window).bind('resize ready', function() {
        ($(window).width() <= 480) ? $('#filter-button').addClass('col-xs-12') : $('#filter-button').removeClass('col-xs-12');
    });


    $(document).on('click', '.qtn_related_docs', function() {
        setDataToHistoryModal($(this).attr('data-qtn-related-id'));
        $('#addDocHistoryModal').modal('show');
    });

    $(document).on('click', '.doc_attachments', function() {
        setDataToAttachmentViewModal($(this).attr('data-qtn-related-id'));
        $('#viewAttachmentModal').modal('show');
    });

    $('#doc-history-table').on('click', '.document_view', function() {
        var documentId = $(this).parents('tr').attr('data-docid');
        var documentType = $(this).parents('tr').attr('data-doctype');
        $('#DocumentViewModal').modal('show');
        getPrintPreviewByDocumentTypeAndDocumentID(documentId, documentType);
    });
});

function setDataToAttachmentViewModal(documentID) {
    $('#doc-attach-table tbody tr').remove();
    $('#doc-attach-table tfoot div').remove();
    $('#doc-attach-table tbody div').remove();

    eb.ajax({
        type: 'POST',
        url: BASE_URL + '/get-document-related-attachement',
        data: {
            documentID: documentID,
            documentTypeID: 46
        },
        success: function(respond) {
            if (respond.status == true) {
                $('#doc-attach-table thead tr').removeClass('hidden');
                $.each(respond.data, function(index, value) {
                    tableBody = "<tr><td><a href="+value.link+">"+value.docName+"</a></td></tr>";
                    $('#doc-attach-table tbody').append(tableBody);
                });
            } else {
                $('#doc-attach-table thead tr').addClass('hidden');
                var noDataFooter = "<div class= ' col-lg-6 text-right'><label><b>No Data</b></label>";
                $('#doc-attach-table tbody').append(noDataFooter);
            }
        }
    });
}

function setDataToHistoryModal(cusOdrID) {
    $('#doc-history-table tbody tr').remove();
    $('#doc-history-table tfoot div').remove();
    $('#doc-history-table tbody div').remove();

    eb.ajax({
        type: 'POST',
        url: BASE_URL + '/customerOrder-api/getAllRelatedDocumentDetailsByCustomerOrderID',
        data: {customerOrderID: cusOdrID},
        success: function(respond) {
            if (respond.status == true) {
                $('#doc-history-table thead tr').removeClass('hidden');
                $.each(respond.data, function(index, value) {
                    if (value != null) {
                        $.each(value, function(index2, value2) {
                            tableBody = "<tr data-docId=" + value2['documentID'] + " data-doctype=" + value2['type'] + "><td class='' >" + value2['type'] + "</td><td>" + value2['code'] + "</td><td>" + value2['amount'] + "</td><td>" + value2['issuedDate'] + "</td><td class='text-center'><span class='glyphicon glyphicon-eye-open openlist link_color document_view ' style='cursor:pointer'></span></td></tr>";
                            $('#doc-history-table tbody').append(tableBody);
                        });
                    }
                });
                var footer = " <div class='col-lg-12 col-md-12 col-sm-12 text-right'><button type='button' class='btn btn-primary col-lg-12 col-md-12 col-sm-12' data-dismiss='modal' aril-hidden='true'>Back</button></div>";
                $('#doc-history-table tfoot').append(footer);
            } else {
                $('#doc-history-table thead tr').addClass('hidden');
                var noDataFooter = "<div class= ' col-lg-9 text-right'><label><b>No Data</b></label>";
                $('#doc-history-table tbody').append(noDataFooter);
            }
        }
    });
}

function getPrintPreviewByDocumentTypeAndDocumentID(documentID, documentType) {
    var $iframe = $('#related-document-view');
    $iframe.ready(function() {
        $iframe.contents().find("body div").remove();
    });
    var URL;
    if (documentType == 'Quotation') {
        URL = BASE_URL + '/quotation/document/' + documentID;
    } else if (documentType == 'SalesOrder') {
        URL = BASE_URL + '/salesOrders/document/' + documentID;
    }
    else if (documentType == 'DeliveryNote') {
        URL = BASE_URL + '/delivery-note/document/' + documentID;
    }
    else if (documentType == 'SalesReturn') {
        URL = BASE_URL + '/return/document/' + documentID;
    }
    else if (documentType == 'SalesInvoice') {
        URL = BASE_URL + '/invoice/document/' + documentID;
    }
    else if (documentType == 'CustomerPayment') {
        URL = BASE_URL + '/customerPayments/document/' + documentID;
    }
    else if (documentType == 'CreditNote') {
        URL = BASE_URL + '/credit-note/document/' + documentID;
    } 
    else if (documentType == 'CreditNotePayment') {
        URL = BASE_URL + '/credit-note-payments/document/' + documentID;
    }

    eb.ajax({
        type: 'POST',
        url: URL,
        success: function(respond) {
            var division = "<div></div>";
            $iframe.ready(function() {
                $iframe.contents().find("body").append(division);
            });
            $iframe.ready(function() {
                $iframe.contents().find("body div").append(respond);
            });
        }
    });

}

function deleteCustomerOrder(cusodrid) {
   
    bootbox.confirm("Are you sure you want to delete this customer order ?", function(result) {
        if (result == true) {
            eb.ajax({
                type: 'POST',
                url: BASE_URL + '/customerOrder-api/deleteCustomerOrder',
                data: {
                    id: cusodrid
                },
                success: function(respond) {
                    if (respond.status) {
                        p_notification(respond.status, respond.msg);
                        window.setTimeout(function() {
                            location.reload();
                        }, 1000);
                    } else {
                        
                        p_notification(respond.status, respond.msg);
                    }
                },
                async: false
            });
        }
    });
}