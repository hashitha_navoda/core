$(document).ready(function () {

    var invoiceId = null;
    var $productTable = $("#productTable");
    var $addRowSample = $('tr.add-row.sample.hidden', $productTable);
    var invoiceProductId = null;
    var productTypeStatus = null;
    var productType = null;
    var subProducts = null;
    var dispatchQty = null;
    var saveBtnStatus = true;
    var contactObj = {};
    var productArr = {};
    var subProductArr = {};
    var mobileNumArray = [];
    var emailAddArray = [];
    var conID = null;
    var contactID = null;
    var editMode = false;
    
    var customerID;
    var editflag = 0;
    var invoice = false;
    var v;
    var ff = new Array();
    var columns;
    var gender = null;
    var custupdatemass = [];
    var profileID = 0;
    var customerProfileDataArray = {};
    var $clonedCustomerProfileDisplay;
    var nameregex = /^[\w\-\s]+$/;
    var searchkey;
    var cusID;
    var EDIT_MODE = false;
    var updatedCustomerProfiles = {};
   
    //for save dispatch note
    $(document).on('click', '#add-contact', function () {

        $('.createContactDiv').removeClass('hidden');
        $('.viewContactDiv').addClass('hidden');


    });

    $(document).on('click', '#cancelbutton', function () {
        $('.viewContactDiv').removeClass('hidden');
        $('.createContactDiv').addClass('hidden');
    });

    function updateNumTypeDropDown() {
        $allRow = $('tr.documentRow.editable');
        $allRow.find('option').removeAttr('disabled')
        $.each(mobileNumArray, function(index, value) {
            $allRow.find(".numberTypeId option[value='" + value.numberTypeId + "']").attr('disabled', 'disabled');
        });
        $allRow.find('.numberTypeId').selectpicker('render');
        $allRow.find('.numberTypeId').selectpicker('refresh');
    }

    $('#numberTable').on('click', '.addNum', function(e) {
        e.preventDefault();
        var $row = $(this).parents('tr');
        if (validateNumTypeRow($row)) {
            $row.removeClass('editable');

            $temp = {
                'numberTypeId' : $row.find('.numberTypeId').val(),
                'number': $row.find('.mobileNo').val()
            };

            mobileNumArray.push($temp);
            $row.find('.numberTypeId').attr('disabled', true);
            $row.find('.mobileNo').attr('disabled', true);
            $row.find('.addNum').addClass('hidden');
            $row.find('.deleteNum').removeClass('hidden');
            $row.addClass('added-num');
            var $cloneRow = $($('.sample').clone()).removeClass('sample hidden').addClass('documentRow editable');

            $cloneRow.find('.numberTypeId').selectpicker().attr('data-liver-search', true);
            $cloneRow.insertBefore($row);

            updateNumTypeDropDown();
        }
    });


     function validateNumTypeRow($row) {
        var flag = true;

        var numberTypeId = $row.find('.numberTypeId').val();
        console.log(numberTypeId);
        var number = $row.find('.mobileNo').val();
        var phoneno = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;

        if (number == '') {
            p_notification(false, eb.getMessage('ERR_CONTACT_VALID_PRI_TEL'));
            return false;
        }

        if (numberTypeId == '' || numberTypeId == null) {
            p_notification(false, eb.getMessage('ERR_CONTACT_NUM_TYPE'));
            return false;
        }

        if (number != '' && !phoneno.test(number)) {
             p_notification(false, eb.getMessage('ERR_CONTACT_VALID_TEL_NUM'));
             return false;

        }
        var dupVals = [];
        var dupType = [];
        $.each(mobileNumArray, function(index, value) {
            if (value.number == number) {
                dupVals.push(value.number);
            }

            if (value.numberTypeId == numberTypeId) {
                dupType.push(value.number);
            }
        });

        if (dupVals.length != 0) {
            p_notification(false, eb.getMessage('ERR_CONTACT_NO_DUP'));
            return false;
        }

        if (dupType.length != 0) {
            p_notification(false, eb.getMessage('ERR_CONTACT_TYPE_DUP'));
            return false;
        }

        return flag;
    }

    $('#numberTable').on('click', '.deleteNum', function(e) {
        e.preventDefault();
        var $row = $(this).parents('tr');
        var numberTypeId = $row.find('.numberTypeId').val();
        //this is for remove documentTypeID from the documentType array
        mobileNumArray = jQuery.grep(mobileNumArray, function(value) {
            return value.numberTypeId != numberTypeId;
        });
        $row.remove();
        updateNumTypeDropDown();
    });

    $('#clear-search').on('click', function() {

        searchurl = BASE_URL + '/contactsAPI/get-contact-from-search-by-contactID';
        var searchrequest = eb.post(searchurl, {contactID: null});
        searchrequest.done(function(searchdata) {
            if (!searchdata.status) {
                p_notification(searchdata.status, searchdata.msg)
            }
            $('#contactList').html(searchdata.html);
            $('#contactSearch').val('').trigger('change');
        });
    });

    // email

    function updateEmailTypeDropDown() {
        $allRow = $('tr.emailRow.editable');
        $allRow.find('option').removeAttr('disabled')
        $.each(emailAddArray, function(index, value) {
            $allRow.find(".emailTypeId option[value='" + value.emailTypeId + "']").attr('disabled', 'disabled');
        });
        $allRow.find('.emailTypeId').selectpicker('render');
        $allRow.find('.emailTypeId').selectpicker('refresh');
    }

    $('#emailTable').on('click', '.addEmail', function(e) {
        e.preventDefault();
        var $row = $(this).parents('tr');
        if (validateEmailTypeRow($row)) {
            $row.removeClass('editable');

            $temp = {
                'emailTypeId' : $row.find('.emailTypeId').val(),
                'address': $row.find('.emailAdd').val()
            };

            emailAddArray.push($temp);
            $row.find('.emailTypeId').attr('disabled', true);
            $row.find('.emailAdd').attr('disabled', true);
            $row.find('.addEmail').addClass('hidden');
            $row.find('.deleteEmail').removeClass('hidden');
            $row.addClass('added-email');
            var $cloneRow = $($('.sample-email').clone()).removeClass('sample-email hidden').addClass('emailRow editable');

            $cloneRow.find('.emailTypeId').selectpicker().attr('data-liver-search', true);
            $cloneRow.insertBefore($row);

            updateEmailTypeDropDown();
        }
    });


     function validateEmailTypeRow($row) {
        var flag = true;

        var emailTypeId = $row.find('.emailTypeId').val();
        var address = $row.find('.emailAdd').val();
        var emailRegex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;

        if (address == '') {
            p_notification(false, eb.getMessage('ERR_CONTACT_EMAIL'));
            return false;
        }

        if (emailTypeId == '' || emailTypeId == null ) {
            p_notification(false, eb.getMessage('ERR_CONTACT_EMAIL_TYPE'));
            return false;
        }

        if (address != '' && !emailRegex.test(address)) {
             p_notification(false, eb.getMessage('ERR_CONTACT_VALID_EMAIL'));
             return false;

        }
       
        var dupVals = [];
        $.each(emailAddArray, function(index, value) {
            if (value.address == address) {
                dupVals.push(value.address);
            }
        });

        if (dupVals.length != 0) {
            p_notification(false, eb.getMessage('ERR_CONTACT_EMAIL_DUP'));
            return false;
        }

        return flag;
    }

    $('#emailTable').on('click', '.deleteEmail', function(e) {
        e.preventDefault();
        var $row = $(this).parents('tr');
        var emailTypeId = $row.find('.emailTypeId').val();
        //this is for remove documentTypeID from the documentType array
        emailAddArray = jQuery.grep(emailAddArray, function(value) {
            return value.emailTypeId != emailTypeId;
        });
        $row.remove();
        updateEmailTypeDropDown();
    });


    $(document).on('click', '#saveButton, #saveAndCreateButton', function () {

        contactObj.firstName = $('#firstName').val();
        contactObj.lastName = $('#lastName').val();
        contactObj.title = $('#title').val();
        contactObj.telNumbers = mobileNumArray;
        contactObj.emails = emailAddArray;
        contactObj.designation = $('#designation').val();

        var saveType = this.getAttribute('data-saveType');
        
        if(validateContactData(contactObj)){
            eb.ajax({
                type: 'POST',
                url: BASE_URL + '/contactsAPI/create',
                data: contactObj,
                success: function (respond) {
                    p_notification(respond.status, respond.msg);

                    if (saveType == 1) {
                        resetMobileNum();
                        resetEmail();
                        resetFields();

                        $('#contactList').html(respond.html);
                        $('#contactSearch').val('').trigger('change');

                        window.setTimeout(function() {
                            $('.viewContactDiv').removeClass('hidden');
                            $('.createContactDiv').addClass('hidden');
                        }, 1000);


                    } else {

                        $('#contactList').html(respond.html);
                        $('#contactSearch').val('').trigger('change');

                        window.setTimeout(function() {
                            resetMobileNum();
                            resetEmail();
                            resetFields();
                            $('.viewContactDiv').addClass('hidden');
                            $('.createContactDiv').removeClass('hidden');
                        }, 1000);
                    }
                }
            });
        }
    });

    function resetMobileNum () {
        mobileNumArray = [];
        $('#numberTable tr.added-num').remove();
        updateNumTypeDropDown();
    }

    function resetEmail () {
        emailAddArray = [];
        $('#emailTable tr.added-email').remove();
        updateEmailTypeDropDown();
    }

    loadDropDownFromDatabase('/contactsAPI/search-contacts-for-dropdown', "", '', '#contactSearch');
    $('#contactSearch').selectpicker('refresh');
    $('#contactSearch').on('change', function() {
        if ($(this).val() > 0 && conID != $(this).val()) {
            // var urlArr = window.location.pathname.split("/");
            // var isSale = (urlArr[2]) ? true: false;
            conID = $(this).val();
            searchurl = BASE_URL + '/contactsAPI/get-contact-from-search-by-contactID';
            var searchrequest = eb.post(searchurl, {contactID: conID});
            searchrequest.done(function(searchdata) {
                if (!searchdata.status) {
                    p_notification(searchdata.status, searchdata.msg)
                }
                $('#contactList').html(searchdata.html);
            });
        }
        return false;
    });
  
   
   
    function resetFields() {
        $('#firstName').val('');
        $('#lastName').val('');
        $('#designation').val('');
        $('#title').val(0);
    }

    function validateContactData(input) {
        var emailRegex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        var phoneno = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;

        if (input.firstName == '') {
            p_notification(false, eb.getMessage('ERR_CONTACT_VALID_FIRST_NAME'));
            $("input[name='firstName']").focus();
            return false;
        } else if (input.telNumbers.length == 0) {
            p_notification(false, eb.getMessage('ERR_CONTACT_NUM_EMPTY'));
            return false;
        }  else {
            return true;
        }
    }


    $(document).on('click', '.edit-contact', function (e){
        e.preventDefault();
        var $row = $(this).parents('tr');

        contactID = $row.attr('data-contact-id');
        eb.ajax({
            type: 'POST',
            url: BASE_URL + '/contactsAPI/getCotactDetailsById',
            data: {contactID: contactID},
            success: function (respond) {

                if (respond.status == true) {
                    editMode = true;
                    setEditModeDetails(respond.data);
                }


            }
        });
    });


    $(document).on('click', '.delete-contact', function (e){
        e.preventDefault();
        var $row = $(this).parents('tr');

        contactID = $row.attr('data-contact-id');

        bootbox.confirm('Are you sure you want to remove this contact ?', function(result) {
            if (result == true) {
                eb.ajax({
                    type: 'POST',
                    url: BASE_URL + '/contactsAPI/deleteContactById',
                    data: {contactID: contactID},
                    success: function (respond) {
                        p_notification(respond.status, respond.msg)
                        if (respond.status == true) {
                            window.location.reload();
                        }
                    }
                });
            }
        });   
    });


    function setEditModeDetails(data) {

        if (editMode) {
            $('#firstName').val(data.firstName);
            $('#lastName').val(data.lastName);
            $('#title').val(data.title);
            $('#designation').val(data.designation);
            emailAddArray = [];
            mobileNumArray = [];

                $('#numberTable .added-num').remove();
            if (Object.keys(data.numbers).length > 0) {
                $.each(data.numbers, function(index, value) {

                    addNum(index, value);

                });
            }
                    updateNumTypeDropDown();

                $('#emailTable .added-email').remove();
            if (Object.keys(data.emails).length > 0) {
                $.each(data.emails, function(index, value) {
                    addEmail(index, value);
                });
            }
                updateEmailTypeDropDown();



            $('#saveButton').addClass('hidden');
            $('#updateButton').removeClass('hidden');
            $('#saveAndCreateButton').addClass('hidden');
            $('.viewContactDiv').addClass('hidden');
            $('.createContactDiv').removeClass('hidden');
        }

    }

    function addNum(key, data) {

        var $cloneRow = $($('#numberTable .sample').clone()).removeClass('sample hidden').addClass('documentRow');

        //set document type
        $cloneRow.find('.numberTypeId').val(key);
        $cloneRow.find('.numberTypeId').selectpicker('render');
        $cloneRow.find('.numberTypeId').attr('disabled', true);

        $('#numberTable .editable .numberTypeId').val("");
        $('#numberTable .editable .numberTypeId').selectpicker('render');
        // documentTypeArray.push(key);
        $temp = {
            'numberTypeId' : key,
            'number': data
        };

        mobileNumArray.push($temp);

        $cloneRow.addClass('added-num');
        $cloneRow.find('.mobileNo').val(data);
        $cloneRow.find('.mobileNo').attr('disabled', true);

        $cloneRow.find('.addNum').addClass('hidden');
        $cloneRow.find('.deleteNum').removeClass('hidden');
        $( ".documentTypeBody" ).append($cloneRow);
    }

    function addEmail(key, data) {

        var $cloneRow = $($('#emailTable .sample-email').clone()).removeClass('sample-email hidden').addClass('emailRow');

        $cloneRow.find('.emailTypeId').val(key);
        $cloneRow.find('.emailTypeId').selectpicker('render');
        $cloneRow.find('.emailTypeId').attr('disabled', true);

       
        $temp = {
            'emailTypeId' : key,
            'address': data
        };
        emailAddArray.push($temp);
        
        $cloneRow.addClass('added-email');
        $cloneRow.find('.emailAdd').val(data);
        $cloneRow.find('.emailAdd').attr('disabled', true);

        $cloneRow.find('.addEmail').addClass('hidden');
        $cloneRow.find('.deleteEmail').removeClass('hidden');
        $( ".emailTypeBody" ).append($cloneRow);
    }

    $(document).on('click', '#updateButton', function () {

        contactObj.firstName = $('#firstName').val();
        contactObj.lastName = $('#lastName').val();
        contactObj.title = $('#title').val();
        contactObj.telNumbers = mobileNumArray;
        contactObj.emails = emailAddArray;
        contactObj.designation = $('#designation').val();
        contactObj.contactID = contactID;
        
        if(validateContactData(contactObj)){
            eb.ajax({
                type: 'POST',
                url: BASE_URL + '/contactsAPI/update',
                data: contactObj,
                success: function (respond) {
                    p_notification(respond.status, respond.msg);

                    resetMobileNum();
                    resetEmail();
                    resetFields();

                    $('#contactList').html(respond.html);
                    $('#contactSearch').val('').trigger('change');

                    window.setTimeout(function() {
                        $('.viewContactDiv').removeClass('hidden');
                        $('.createContactDiv').addClass('hidden');
                    }, 1000);

                }
            });
        }
    });

    $('#customerimport_more').hide();

    //customer_import show_more_details button
    var importmoreflag = 0;
    $("#moredetailsimport").click(function() {
        if (importmoreflag == 0) {
            $("#customerimport_more").slideDown(function() {
                $("#moredetailsimport").html('Hide details ' + '<span class="glyphicon glyphicon-circle-arrow-up"></span>');
            });
            importmoreflag = 1;
        } else {
            $("#customerimport_more").slideUp(function() {
                $('#moredetailsimport').html('Show more details ' + '<span class="glyphicon glyphicon-circle-arrow-down"></span>');
            });
            importmoreflag = 0;
        }
    });

    $('.customerimportselector').on('change', function(event, modalEnable) {
        var selectid = this.id;
        var selectedoption = $(this).val();
        enableoptions(ff[selectid]);
        ff[selectid] = $(this).val();
        var $tr = $(this).closest('tr');
        var column = $tr.data('column');
        var enumModalStatus = (modalEnable === undefined) ? true : modalEnable;        
        var html = '';
        if (selectedoption != 'Unmapped') {
            disableoptions(selectedoption);
        }

        if (selectedoption == "Customer Category") {
            generateEnumeratorModal( column, 'customerCategory', fileData, header, false, enumModalStatus, {}, function (res){
                if(res) {
                    $tr.find('.enum-modal-btn').removeClass('hidden');
                }
            });
        } else {
            $tr.find('.enum-modal-btn').addClass('hidden');
        }

        selectedFieldArr = [];
        $('.customerimportselector').each(function(index,element){
            var selectedValue = $(this).val();
            if(selectedValue && $.inArray(selectedValue,selectedFieldArr) == -1){
                selectedFieldArr.push(selectedValue);
            }
        });
    });

    function disableoptions(selectedoption) {
        
        for (var i = 0; i < columns; i++) {
            $("#" + i + ' option[value="' + selectedoption + '"]').attr('disabled', 'disabled');
        }
    }
    function enableoptions(selectedenableoption) {
        for (var j = 0; j < columns; j++) {
            $("#" + j + ' option[value="' + selectedenableoption + '"]').removeAttr('disabled');
        }
    }
    
    columns = $('#importcolumncount').html();
    for (var k = 0; k < columns; k++) {
        ff[k] = 'Unmapped';
    }

    var $importmodal = $('#customer-import-form-div');
    $importmodal.on('hide.bs.modal', function() {
        $importmodal.removeData();
        return true;
    });

    $('#importDataButton').on('click', function() {
        var cNameFlag = 0;
        // var cCodeFlag = 0;
        var cMobileFlag = 0;
        for (var t = 0; t < columns; t++) {
            if (ff[t] == 'Contact Name') {
                cNameFlag = 1;
            } else if (ff[t] == 'Telephone') {
                cMobileFlag = 1;
            }
        }
        
        if (cNameFlag == 1 && cMobileFlag == 1) {
            var importmodalurl = BASE_URL + '/contactsAPI/getImportConfirm';
            $importmodal.load(importmodalurl, '', function() {
                $importmodal.modal('show');
                $('#import-button').on('click', function() {
                    var radiovalue = $('input:radio[name=importmethod]:checked').val();
                    if (radiovalue == 'discard') {
                        contactimport('discard');
                    }
                    else if (radiovalue == 'replace') {
                        contactimport('replace');
                    } else {
                        p_notification(false, eb.getMessage('ERR_CUST_IMPORT'));
                    }
                });
            });
        } else {
            p_notification(false, eb.getMessage('ERR_CUST_DETAILS'));
        }
    });

    function contactimport(method) {
        var importurl = BASE_URL + '/contactsAPI/importContactSave';
        var importrequest = eb.post(importurl, {
            method: method,
            header: $('#importheader').html(),
            delim: $('#importdelim').html(),
            columncount: $('#importcolumncount').html(),
            col: ff,
        });
        importrequest.done(function(retdata) {
            p_notification(retdata.status, retdata.msg);
            if (retdata.status) {
                window.setTimeout(function() {
                    window.location.assign(BASE_URL + '/contacts');
                }, 1500);
            }

        });
    }

    $(document).on('click', '#import-contact',function(e) {
        window.location.assign(BASE_URL + '/contacts/import');
    })
    


});