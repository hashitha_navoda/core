/*
 * @author Prathap Weerasinghe <prathap@thinkcube.com>
 */

var tmptotal, state = "new";
var tax_rates = new Array();
var products = new Array();
var deleteProducts = new Array();
var taxdetails = new Array();
var allProductTax = new Array();
var currentProducts = new Array();
var selectedItemID;
var customCurrencyRate = 0;
var priceListItems = [];
var uploadedAttachments = {};
var deletedAttachmentIds = [];
var deletedAttachmentIdArray = [];
var documentTypeArray = [];

function product(pCode, pName, quantity, uom, uPrice, discount, dType, tax, tCost, taxDetails, productType, item_discription) {
    this.item_code = pCode;
    this.item = pName;
    this.quantity = quantity;
    this.discount = discount;
    this.discountType = dType;
    this.unit_price = uPrice;
    this.uom = uom;
    this.total_cost = tCost;
    this.taxes = tax;
    this.taxDetails = taxDetails;
    this.productType = productType;
    this.item_discription = item_discription;
}

$(document).ready(function() {
    var all_tx_val = new Array();
    var totaldiscount = 0, decimalPoints = 0, itemDecimalPoints = 0;
    var priceListId = '';
    $('.deli_row, #deli_form_group, #tax_td').hide();
    $("#customer_more").hide();
    $("#currency").attr("disabled", "disabled");
    $("#moredetails").click(function() {
        $("#customer_more").slideDown();
        $('#moredetails').hide();
    });

    function setItemCost() {
        var id = $('#item_code').val();
        var productType = $('#item_code').data('PT');
        var price = parseFloat(accounting.unformat($('#price').val()));
        var qty = parseFloat($('#qty').val());
        if (productType == 2 && (qty == '' || qty == 0 || isNaN(qty))) {
            qty = 1;
        }
        var disc = parseFloat(accounting.unformat($('#discount').val()));
        var tax = new Array();
        var taxA = 0;
        var tot = 0;
        taxdetails = new Array();
        $('#addTaxUl input:checked').each(function() {
            if (!$(this).is(':disabled')) {
                tax.push($(this).attr("value"));
            }
        });
        if (jQuery.isEmptyObject(tax)) {
            $('#taxApplied').addClass('glyphicon glyphicon-unchecked');
        } else {
            $('#taxApplied').removeClass('glyphicon glyphicon-unchecked');
            $('#taxApplied').addClass('glyphicon glyphicon-check');
        }
        tot = (price * qty) - calculateDiscount(id, price * qty, disc, qty);
        taxdetails = calculateItemCustomTax(tot, tax);
        var taxTotalAmount = taxdetails === null ? 0 : taxdetails.tTA;
        taxA = taxTotalAmount === undefined ? 0 : parseFloat(taxTotalAmount);

        $("#total").html(isNaN((tot + taxA)) ? "0.00" : accounting.formatMoney((tot + taxA).toFixed(2)));
    }

    $('#viewUploadedFiles').on('click', function(e) {
        $('#doc-attach-table tbody tr').remove();
        $('#doc-attach-table tfoot div').remove();
        $('#doc-attach-table tbody div').remove();

        if (!$.isEmptyObject(uploadedAttachments)) {
            $('#doc-attach-table thead tr').removeClass('hidden');
            $.each(uploadedAttachments, function(index, value) {
                if (!deletedAttachmentIds.includes(value['documentAttachemntMapID'])) {
                    tableBody = "<tr id="+value['documentAttachemntMapID']+" data-attachId=" + value['documentAttachemntMapID'] + "><td><a href="+value.docLink+">"+value.documentRealName+"</a></td><td class='text-center'><span class='glyphicon glyphicon-trash delete_attachment' style='cursor:pointer'></span></td></tr>";
                    $('#doc-attach-table tbody').append(tableBody);
                }
            });
        } else {
            $('#doc-attach-table thead tr').addClass('hidden');
            var noDataFooter = "<div class= ' col-lg-6 text-right'><label><b>No Data</b></label>";
            $('#doc-attach-table tbody').append(noDataFooter);
        }
        $('#attach_view_footer').removeClass('hidden');
        $('#viewAttachmentModal').modal('show');
    });

    $('#doc-attach-table').on('click', '.delete_attachment', function() {
        var attachementID = $(this).parents('tr').attr('data-attachid');
        bootbox.confirm('Are you sure you want to remove this attachemnet?', function(result) {
            if (result == true) {
                $('#' + attachementID).remove();
                deletedAttachmentIdArray.push(attachementID);
            }
        });
    });

    $('#viewAttachmentModal').on('click', '#saveAttachmentEdit', function(event) {
        event.preventDefault();

        $.each(deletedAttachmentIdArray, function(index, val) {
            deletedAttachmentIds.push(val);
        });

        $('#viewAttachmentModal').modal('hide');
    });

    function setTotalCost() {
        var total = 0, disc = 0;
        for (var i in products) {
            var tax = 0;
            for (var tx in products[i].taxes) {
                tax += products[i].taxes[tx];
            }
            total += parseFloat(products[i].total_cost);
        }
        var delivercharge = parseFloat(accounting.unformat($('#deli_amount').val()));
        if (delivercharge == '') {
            $('#finaltotal').html(accounting.formatMoney((total).toFixed(2)));
            $('#subtotal').html(accounting.formatMoney((total).toFixed(2)));
        } else {
            finalt = ((parseFloat(total) + parseFloat(delivercharge)).toFixed(2));
            $('#finaltotal').html(accounting.formatMoney(finalt));
        }
        setTotalTax();
    }

    function setTotalTax() {
        if (tax_rates.length < 1) {
            eb.ajax({
                type: 'POST',
                url: '/taxAPI/getTaxRates',
                success: function(res) {
                    tax_rates = res;
                },
                async: false
            });
        }

        totalTaxList = {};
        for (var k in products) {
            if (products[k].taxDetails != null) {
                for (var l in products[k].taxDetails.tL) {
                    if (totalTaxList.hasOwnProperty(l)) {
                        var curTA = totalTaxList[l].tA;
                        var newTA = (toFloat(curTA) + toFloat(products[k].taxDetails.tL[l].tA));
                        totalTaxList[l].tA = newTA.toFixed(2);
                    } else {
                        totalTaxList[l] = {tN: products[k].taxDetails.tL[l].tN, tP: products[k].taxDetails.tL[l].tP, tA: products[k].taxDetails.tL[l].tA};
                    }
                }
            }
        }
        var totalTaxHtml = "";
        for (var t in totalTaxList) {
            totalTaxHtml += totalTaxList[t].tN + "(" + totalTaxList[t].tP + "%)&nbsp" + accounting.formatMoney(totalTaxList[t].tA) + "<br>";
        }
        $("#tax_td").html(totalTaxHtml);

    }

    var documentType = 'salesOrder';
    $('#item_code').select2({
        minimumInputLength: 1,
        minimumResultsForSearch: 10,
        ajax: {
            url: BASE_URL + '/productAPI/search-location-products-for-dropdown',
            dataType: "json",
            type: "POST",
            data: function (params) {

                var queryParameters = {
                    searchKey: params.term,
                    documentType: documentType,
                    locationID: currentLocationID
                }
                return queryParameters;
            },
            processResults: function (data) {
                return {
                    results: $.map(data.data.list, function (item) {
                        return {
                            text: item.text,
                            id: item.value
                        }
                    })
                };
            }
        },
        placeholder: 'Enter Item Code Or Name',
    });

     $('#item_code').on('select2:select', function (e) { 
        var data = e.params.data;
         $('#qty').parent().addClass('input-group');
        if (data.id > 0 && data.id != selectedItemID) {
            selectedItemID = data.id;
            itemChange(data.id);
        }
    });



    $('#item_tbl').on('change', '.addNewUomR', function() {
        var tmpUomOb = currentProducts[$('#item_code').val()].uom;
        var tmpUomID = $(".uomLi input[type='radio']:checked").val();
        $('#uomAb').html(tmpUomOb[tmpUomID].uA);
    });

    $("#item_tbl").on('click', '#selectAll', function() {
        $(this).parents('#addTaxUl').find('input:checkbox.addNewTaxCheck').each(function() {
            if (!this.checked) {
                this.checked = true;
            }
        });
        setItemCost();
    });

    $("#item_tbl").on('click', '#deselectAll', function() {
        $(this).parents('#addTaxUl').find('input:checkbox.addNewTaxCheck').each(function() {
            if (this.checked) {
                this.checked = false;
            }
        });
        setItemCost();
    });

    $(document).on('click', 'ul.dropdown-menu, .dropdown-menu label', function(e) {
        e.stopPropagation();
    });

    $(document).on('mouseover', '#item_tbl #unit,#item_tbl #quantity,#item_tbl #disc,#item_tbl #name', function(e) {
        $(this).addClass('shadow');
    });

    $(document).on('mouseout', '#item_tbl #unit,#item_tbl #quantity,#item_tbl #disc,#item_tbl #name', function(e) {
        $(this).removeClass('shadow');
    });


    $(document).on('change', '.chkTaxes', function() {
        var trid = $(this).closest('tr').attr('id');
        var itemID = trid.split('td_')[1].trim();
        var checkedTaxes = Array();
        $("." + itemID + " input:checked").each(function() {
            checkedTaxes.push(this.id);
        });
        if (jQuery.isEmptyObject(checkedTaxes)) {
            $("#taxFilled_" + itemID).removeClass('glyphicon glyphicon-check');
            $("#taxFilled_" + itemID).addClass('glyphicon glyphicon-unchecked');
        } else {
            $("#taxFilled_" + itemID).removeClass('glyphicon glyphicon-unchecked');
            $("#taxFilled_" + itemID).addClass('glyphicon glyphicon-check');
        }
        setItemCostForTaxes(itemID, checkedTaxes);
    });

    $(document).on('click', ".delete", function() {
        deleteProducts[this.id] = products[this.id];
        delete products[this.id];
        $(this).closest("tr").remove();
        setTotalCost();
    });

    $('#add_item').on('click', function() {
        var productID = $("#item_code").val();
        var code = currentProducts[$("#item_code").val()].pC;
        var productType = $("#item_code").data('PT');
        var pName = currentProducts[$("#item_code").val()].pN;
        var quantity = $("#qty").val();
        if (!quantity) {
            quantity = 0;
        }
        var uomqty = $("#qty").siblings('.uomqty').val();
        var uomPrice = $("#price").siblings('.uomPrice').val();
        var uom = $("#qty").siblings('.uom-select').children('button').find('.selected').data('uomID');
        var price = accounting.unformat($("#price").val());

        if (priceListItems[productID] != undefined && parseFloat(priceListItems[productID].itemPrice) > 0) {
            var itemDiscountType = priceListItems[productID].itemDiscountType;
            dsc_type = (itemDiscountType == "1") ? "vl" : "pr";
        } else {
            var dsc_type = "pr";
            if (currentProducts[productID] !== undefined && parseFloat(currentProducts[productID].dV) > 0) {
                dsc_type = "vl";
            } else if (currentProducts[productID] !== undefined) {
                dsc_type = "pr";
            }
        }

        var item_discription = $(this).parents('tr').find('.itemDescText').val();
        var dsc_Pre = accounting.unformat($("#discount").val());
        var tax = new Array();
        $('#addTaxUl input:checked').each(function() {
            tax.push($(this).attr("value"));
        });

        var state = addProduct(code, pName, quantity, price, uom, dsc_type, dsc_Pre, tax, null, uomqty, productType, productID, uomPrice, item_discription);
        if(state){
            setTotalCost();
            $('#item_code').val(0).trigger('change').selectpicker('refresh');
            selectedItemID='';
        }
        $('#item_code').val(null).trigger("change");
    });

    $('#show_tax').on('click', function() {
        if (this.checked) {
            $('#tax_td').fadeIn();
        }
        else {
            $('#tax_td').fadeOut();
        }
    });

    $('table#item_tbl>tbody').on('keypress', 'input#qty, input#price, input#discount,.uomqty', function(e) {
        if (e.which == 13) {
            $('#add_item').trigger('click');
            e.preventDefault();
        }

    });

    $(document).on('focusout', '#price, #qty, #discount,.uomqty,.uomPrice', function() {
        if ($('#item_code').val() != '') {
            if ($(this).val().indexOf('.') > 0) {
                decimalPoints = $(this).val().split('.')[1].length;
            }
            if (this.id == 'price' && (!$.isNumeric(accounting.unformat($('#price').val())) || accounting.unformat($('#price').val()) < 0 || decimalPoints > 2)) {
                decimalPoints = 0;
                $('#price').val('');
            }
            if ((this.id == 'qty') && (!$.isNumeric($('#qty').val()) || $('#qty').val() < 0 || decimalPoints > itemDecimalPoints)) {
                decimalPoints = 0;
                $('#qty').val('');
            }
            if ((this.id == 'discount') && (!$.isNumeric(accounting.unformat($('#discount').val())) || accounting.unformat($('#discount').val()) < 0 || (decimalPoints > 2))) {
                decimalPoints = 0;
                $('#discount').val('');
            }
            setItemCost();
        } else {
            $('#price, #qty, #discount').val('');
            p_notification(false, eb.getMessage('ERR_QUOTEDIT_SETITEM'));
        }
    });

    function itemChange(itemkey) {
        var data = new Array();
        itemDecimalPoints = 3;
        $('.tempLi').remove();
        $("#discount").attr('disabled', false);
        if (itemkey == '') {
            $('#qty').val('');
            $('#price').val('');
            $('#discount').val('');
            $('#total').html('');
            $('#uom').html('');
            return false;
        }

        eb.ajax({
            type: 'POST',
            url: BASE_URL + '/productAPI/get-location-product-details',
            data: {productID: itemkey, locationID: currentLocationID},
            success: function(respond) {
                currentProducts[respond.data.pID] = respond.data;
                data[0] = respond.data;
                if (data[0] == 'error') {
                    $('#qty').val('');
                    $('#price').val('');
                    $('#discount').val('');
                    $('#total').html('');
                    $('#uom').html('');
                    dbflag = false;
                } else {
                    dbflag = true;

                    if ($("#discount").siblings().hasClass('uomPrice')) {
                        $("#discount").siblings('.uomPrice').remove();
                        $("#discount").siblings('.uom-price-select').remove();
                        $("#discount").show();
                    }

                    if (priceListItems[itemkey] == undefined || (priceListItems[itemkey] != undefined && !(parseFloat(priceListItems[itemkey].itemPrice) > 0))) {
                        if (data[0].dEL === "0") {
                            $('#discount').val('');
                            $("#discount").attr('disabled', true);
                        }else if (data[0].dEL === "1" && data[0].dV > 0){
                            $('#discount').val(accounting.formatMoney(data[0].dV)).addUomPrice(data[0].uom);
                            $('#discType').html(companyCurrencySymbol);
                        }else if (data[0].dEL === "1" && data[0].dP > 0) {
                            $('#discount').val(accounting.formatMoney(data[0].dP));
                            $('#discType').html('(%)');
                        }
                    } else {
                        var itemDiscountType = priceListItems[itemkey].itemDiscountType;
                        if (itemDiscountType == 1) {
                            $('#discount').val(parseFloat(priceListItems[itemkey].itemDiscount).toFixed(2)).addUomPrice(data[0].uom);
                            $('#discType').html(companyCurrencySymbol);
                        } else if (itemDiscountType == 2) {
                            $('#discount').val(parseFloat(priceListItems[itemkey].itemDiscount).toFixed(2));
                            $('#discType').html('(%)');
                        }
                    }

                    $('#item_code').data('PT', data[0].pT);

                    if ($("#price").siblings().hasClass('uomPrice')) {
                        $("#price").siblings('.uomPrice').remove();
                        $("#price").siblings('.uom-price-select').remove();
                    }
                    var newPrice = 0;
                    if (priceListItems[itemkey] != undefined) {
                        newPrice = priceListItems[itemkey].itemPrice;
                    } else {
                        newPrice = data[0].dSP;
                    }
                    $('#price').val(newPrice).addUomPrice(data[0].uom);
                    $('#uom').html(data[0].abbrevation);
                    if ($('#qty').val() === '') {
                        $('#qty').val('1');
                    }
                    if ($("#qty").siblings().hasClass('uomqty')) {
                        $("#qty").siblings('.uomqty').remove();
                        $("#qty").siblings('.uom-select').remove();
                    }
                    $('#qty').addUom(data[0].uom);
                    setTaxListForProduct(itemkey);
                    itemDecimalPoints = data[0].decimalPlace;

                    setItemCost();
                }
            }
        });
    }

    $('#salesOrder, #q_view').on('submit', function(e) {
        e.preventDefault();
        var selectedid = "#" + document.activeElement.id;
        var tableId = $(selectedid).closest('table').attr("id");
        if ($('#so_no').val() == '') {
            p_notification(false, eb.getMessage('ERR_SALEORDER_EDIT_VALID_NUM'));
        }
        else if ($('#cust_name').val() == '') {
            p_notification(false, eb.getMessage('ERR_QUOTEDIT_ENTER_CUSNAME'));
        } else if ($('#issue_date').val() == '') {
            p_notification(false, eb.getMessage('ERR_QUOTEDIT_SELECT_ISSDATE'));
        } else if ($('#expire_date').val() == '') {
            p_notification(false, eb.getMessage('ERR_QUOTEDIT_ENTER_EXPDATE'));
        } else if (Object.keys(products).length == 0) {
            if (tableId != "item_tbl") {
                p_notification(false, eb.getMessage('ERR_SALEORDER_EDIT_ADD_ITEM'));
            }
            return false;
        } else if ($('#issue_date').val() > $('#expire_date').val()) {
            p_notification(false, eb.getMessage('ERR_QUOT_ENTER_DATE'));
        } else if ($('#item_code').val() != '' && $('#item_code').val() != '0' && $('#item_code').val() != null) {
            p_notification(false, eb.getMessage('ERR_QUOT_ADD_LITEM'));
        } else if (tableId == "item_tbl") {
            $('#add_item').trigger('click');
        } else {

            var items_ar = Array();
            var delete_items_ar = Array();
            totaldiscount = 0;
            var taxOfItems = new Array();
            for (var i in products) {
                var temptaxes = {};
                for (var tx in products[i].taxes) {
                    if (products[i].taxes[tx] != false) {
                        temptaxes[tx] = products[i].taxes[tx];
                    }
                }
                taxOfItems.push(JSON.stringify(temptaxes));
            }

            for (var i in products) {
                items_ar.push(products[i]);
                var qty = products[i].quantity;
                if (products[i].productType == 2 && qty == 0) {
                    qty = 1;
                }
                if (products[i].discountType == 'pr') {
                    tmpdisc = (products[i].discount * qty * products[i].unit_price / 100);
                } else {
                    tmpdisc = (products[i].discount * qty);
                }
                if (tmpdisc != 0) {
                    totaldiscount += toFloat(tmpdisc);
                }
            }

            for (var i in deleteProducts) {
                delete_items_ar.push(deleteProducts[i]);
            }

            var taxobj = {};
            for (var t in all_tx_val) {
                taxobj[t] = (all_tx_val[t]).toFixed(2);
            }
            var jsontax = JSON.stringify(taxobj);
            var custanamefull = $('#cust_name').val();
            var documentReference = {};
            var flagfalse = false;
            $('.documentTypeBody > tr').each(function() {
                if ($(this).hasClass('documentRow')) {
                    var documentTypeID = $(this).find('.documentTypeId').val();
                    // var documentID = $(this).find('.documentId').val();
                    if (Array.isArray($(this).find('.documentId').val())) {
                        var documentID = $(this).find('.documentId').val();
                    } else {
                        var documentID = [];
                        documentID.push($(this).find('.documentId').val());
                    }
                    if (!(documentTypeID == null || documentTypeID == '')) {
                        if (documentID != null) {
                            documentReference[documentTypeID] = documentID;
                        } else {
                            flagfalse = true;
                        }
                    }
                }
            });

            submit_data = {
                'cust_name': custanamefull,
                'cust_id': $('#cust_id').val(),
                'payment_term': $('#payment_term').val(),
                'invoicecurrentBalance': accounting.unformat($('#invoicecurrentBalance').val()),
                'issue_date': $('#issue_date').val(),
                'expire_date': $('#expire_date').val(),
                'so_no': $('#so_no').val(),
                'so_id': $('#edit_so_ID').val(),
                'salesPersonID': $('#salesPersonID').val(),
                'deli_amount': accounting.unformat($('#deli_amount').val()),
                'deli_charg': (parseFloat(accounting.unformat($('#deli_charg').val()))).toFixed(2),
                'deli_address': $('#deli_address').val(),
                'cmnt': $('#cmnt').val(),
                'taxAmount': jsontax,
                'sub_total': accounting.unformat($("#subtotal").html()),
                'total_amount': accounting.unformat($('#finaltotal').html()),
                'total_discount': totaldiscount,
                'items': items_ar,
                'documentReference': documentReference,
                'deleteItems': delete_items_ar,
                'itemTaxes': JSON.stringify(taxOfItems),
                'show_tax': ($('#show_tax').is(':checked')) ? 1 : 0,
                'customCurrencyRate': customCurrencyRate,
                'customCurrencyId': $('#customCurrencyId').val(),
                'priceListId': priceListId,
            };

            var existingAttachemnts = {};
            var deletedAttachments = {};
            $.each(uploadedAttachments, function(index, val) {
                if (!deletedAttachmentIds.includes(val['documentAttachemntMapID'])) {
                    existingAttachemnts[index] = val;
                } else {
                    deletedAttachments[index] = val;
                }
            });

            if (this.id == "salesOrder") {
                var url = "/api/salesOrders/editSalesOrder";
                var request = eb.post(url, submit_data);
                request.done(function(res) {
                    p_notification(res.status, res.msg);
                    if (res.status == true) {
                        var documentID = "same";
                        var fileInput = document.getElementById('editDocumentFiles');
                        var form_data = false;
                        if (window.FormData) {
                            form_data = new FormData();
                        }
                        form_data.append("documentID", documentID);
                        form_data.append("documentTypeID", 3);
                        form_data.append("updateFlag", true);
                        form_data.append("editedDocumentID", $('#edit_so_ID').val());
                        form_data.append("documentCode", $('#so_no').val());
                        form_data.append("deletedAttachmentIds", deletedAttachmentIds);
                        
                        if(fileInput.files.length > 0) {
                            for (var i = 0; i < fileInput.files.length; i++) {
                                form_data.append("files[]", fileInput.files[i]);
                            }
                        }

                        eb.ajax({
                            url: BASE_URL + '/store-files',
                            type: 'POST',
                            processData: false,
                            contentType: false,
                            data: form_data,
                            success: function(res) {
                            }
                        });

                        if ($('#lightbox_overlay').length < 1) {
                            documentPreview(BASE_URL + '/salesOrders/salesOrdersView/' + res.data, 'documentpreview', "/api/salesOrders/send-email", function($preview) {
                                $preview.on('hidden.bs.modal', function() {
                                    if (!$("#preview:visible").length) {

                                        window.location.assign(BASE_URL + "/salesOrders/list/");
                                    }
                                });
                            });
                        }
                        state = "saved";
                    }
                });
            }
            else if (this.id == "q_view") {
                var li_box = document.createElement("div");
                var li_box_cont = document.createElement("div");
                var print = document.createElement("div");
                li_box.id = "lightbox_overlay";
                li_box.class = "disabl_when_print";
                li_box_cont.id = "lightbox_container";
                li_box_cont.class = "disabl_when_print";
                print.id = "print_overlay";
                print.class = "disabl_when_print";
                var view_req = eb.post('/quotation/viewQuotation?' + $.param(submit_data), '', {});
                view_req.done(function() {
                    $(".main_div").addClass("disabl_when_print");
                    $(li_box_cont).append(view_req.responseText);
                    $(li_box).append(li_box_cont);
                    $("body").append(li_box);
                    $("body").append(print);
                    if (state == "saved") {
                        $("#prv_edit").html(edit_btn);
                    } else {
                        $("#prv_edit").html(save);
                    }

                    $("#prv_edit").on("click", function() {
                        if (state == "new") {
                            $("#placequotation").trigger(jQuery.Event("click"));
                            $("#prv_edit").html(edit_btn);
                        } else {
                            window.location.assign(BASE_URL + "/quotation/Edit/" + $('#qot_no').val());
                        }
                    });
                    $("#q_print").on("click", function() {
                        window.print();
                    });
                    $("#q_email").on("click", function() {
                        if (state == "new") {
                            p_notification(false, eb.getMessage('ERR_QUOT_SAVE_SEND'));
                        } else {
                            $("#email-modal").modal("show");
                            $("#send").on("click", function() {
                                var state = validateEmail($("#email_to").val(), $("#email-body").html());
                                if (state) {
                                    var param = {
                                        qout_id: $(this).attr("data-qot_id"),
                                        to_email: $("#email_to").val(),
                                        subject: $("#email_sub").val(),
                                        body: $("#email-body").html()
                                    };
                                    var mail = eb.post("/quotationAPI/sendQuotation", param);
                                    mail.done(function(rep) {
                                        if (rep.error) {
                                            p_notification(false, eb.getMessage('ERR_PAY_EMAIL_SENT'));
                                        } else {
                                            p_notification(true, eb.getMessage('SUCC_PAY_EMAIL_SENT'));
                                        }
                                    });
                                    $("#email-modal").modal("hide");
                                }
                            });
                        }
                    });
                    $("#cancel").on("click", function() {
                        if (state == "new") {
                            $(li_box_cont).remove();
                            $(li_box).remove();
                            $(print).remove();
                        } else {
                            $("#new_qtn").trigger(jQuery.Event("click"));
                        }
                    });
                    $("#new_qtn").on("click", function() {
                        window.location.reload();
                    });
                    $("#to_so").on("click", function() {
                        if (state == "saved") {
                            window.location.assign(BASE_URL + "/salesOrders/index/" + $('#qot_no').val());
                        } else {
                            p_notification(false, eb.getMessage('ERR_QUOT_SAVE_CONVERT'));
                        }

                    });
                    $("#to_inv").on("click", function() {
                        if (state == "saved") {
                            window.location.assign(BASE_URL + "/invoice/index/qo/" + $('#qot_no').val());
                        } else {
                            p_notification(false, eb.getMessage('ERR_QUOT_SAVE_CONVERT'));
                        }
                    });
                });
            }
        }
    });


    $('#cust_name').on('change', function() {
        var url2 = BASE_URL + '/customerAPI/getcustomer';
        var customerID = $('#cust_id').val();
        if ($('#cust_name').val() == "") {
            $('#invoicecurrentBalance').val('');
            $('#salesOrderCurrentCredit').val('');
            $('#payment_term').val(0);
            cust_disc = 0;
        } else {
            var request2 = eb.post(url2, {customerID: customerID});
            request2.done(function(customerdata) {
                if (customerdata.status) {
                    var paymentTerm = customerdata.data[0].customerPaymentTerm == null ? 1 : customerdata[0].customerPaymentTerm;
                    $('#invoicecurrentBalance').val(accounting.formatMoney(customerdata.data[0].CustomerCurrentBalance));
                    $('#salesOrderCurrentCredit').val(accounting.formatMoney(customerdata.data[0].CustomerCurrentCredit));
                    $('#payment_term').val(paymentTerm);
                    $('#cust_id').val(customerdata.data[0].customerID);
                    cust_disc = customerdata.data[0].discount;
                } else {
                    $('#payment_term').val(0);
                    $('#cur_credit').val('');
                    cust_disc = 0;
                }
                setItemCost();
            });
        }
    });

    $("#reset").on('click', function() {
        window.location.reload()
    });

    $(document).on('click', '#moredetails', function() {
        $("#customer_more").fadeIn();
        $(this).hide();
    });


    ///////DatePicker\\\\\\\

    var nowTemp = new Date();
    var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);

    var checkin = $('#issue_date').datepicker({
        onRender: function(date) {
            return date.valueOf() < now.valueOf() ? 'disabled' : '';
        }
    }).on('changeDate', function(ev) {
        if (ev.date.valueOf() > checkout.date.valueOf()) {
            var newDate = new Date(ev.date);
            newDate.setDate(newDate.getDate() + 1);
            checkout.setValue(newDate);
        }
        checkin.hide();
        $('#expire_date')[0].focus();
    }).data('datepicker');
    checkin.setValue(now);
    var checkout = $('#expire_date').datepicker({
        onRender: function(date) {
            return date.valueOf() < checkin.date.valueOf() ? ((U_DATE_R_OVERRIDE)?'':'disabled')  : '';
        }
    }).on('changeDate', function(ev) {
        checkout.hide();
    }).data('datepicker');


/////EndOFDatePicker\\\\\\\\\

    $('#documentReference').on('click', function(e) {
        e.preventDefault();
        $('tr.documentRow.editable').find('.editDocument').addClass('hidden');
        $('tr.documentRow.editable').find('.documentTypeId').val(0);


        $('tr.documentRow.editable').find('.documentTypeId').selectpicker('render');
        $('tr.documentRow.editable').find('.documentTypeId').selectpicker('refresh');
        $('tr.documentRow.editable').find('.documentTypeId').trigger('change');
        $('#documentReferenceModal').modal('show');
        // if ($('#retrieveLocation').val()) {
        //     $('#retrieveLocation').attr('disabled', 'disabled');
        // } else {
        //     $('#documentReferenceModal').modal('hide');
        //     p_notification(false, eb.getMessage('ERR_PLEASE_SELECT_LOCTION_FOR_GRN'));
        // }
    });


     $('#documentTable').on('change', '.documentTypeId', function() {
        var $dropDownIDParent = $(this).parents('tr');
        var documentTypeID = $(this).val();
        loadDocumentId($dropDownIDParent, documentTypeID);
    });

    function loadDocumentId($dropDownIDParent, documentTypeID) {
        var dropDownID = '.documentId';
        // var locationID = $('#retrieveLocation').val();
        var url = '';
        if (documentTypeID == 46) {
            url = '/customerOrder-api/search-all-customer-order-for-dropdown';
            $('select.selectpicker.documentId', $dropDownIDParent).attr('multiple', false);
        } 
        $('div.bootstrap-select.documentId', $dropDownIDParent).remove();
        $('select.selectpicker.documentId', $dropDownIDParent).show().removeData().empty();
        loadDropDownFromDatabase(url, null, 0, dropDownID, $dropDownIDParent);
    }

    function updateDocTypeDropDown() {
        $allRow = $('tr.documentRow.editable');
        $allRow.find('option').removeAttr('disabled')
        $.each(documentTypeArray, function(index, value) {
            $allRow.find(".documentTypeId option[value='" + value + "']").attr('disabled', 'disabled');
        });
        $allRow.find('.documentTypeId').selectpicker('render');
        $allRow.find('.documentTypeId').selectpicker('refresh');
    }

    $('#documentTable').on('click', '.saveDocument', function(e) {
        e.preventDefault();
        var $row = $(this).parents('tr');
        if (validateDocumentTypeRow($row)) {
            $row.find('.editDocument').removeClass('hidden');
            $row.removeClass('editable');
            documentTypeArray.push($row.find('.documentTypeId').val());
            $row.find('.documentTypeId').attr('disabled', true);
            $row.find('.documentId').attr('disabled', true);
            $row.find('.saveDocument').addClass('hidden');
            $row.find('.deleteDocument').removeClass('hidden');
            var $cloneRow = $($('.sample').clone()).removeClass('sample hidden').addClass('documentRow editable');

            $cloneRow.find('.documentTypeId').selectpicker().attr('data-liver-search', true);
            $cloneRow.find('.editDocument').addClass('hidden');
            $cloneRow.insertBefore($row);

            updateDocTypeDropDown();
        }
    });

    $('#documentTable').on('click', '.deleteDocument', function(e) {
        e.preventDefault();
        var $row = $(this).parents('tr');
        var documentTypeID = $row.find('.documentTypeId').val();
        //this is for remove documentTypeID from the documentType array
        documentTypeArray = jQuery.grep(documentTypeArray, function(value) {
            return value != documentTypeID;
        });
        $row.remove();
        updateDocTypeDropDown();
    });

    $('#documentTable').on('click', '.editDocument', function(e) {
        e.preventDefault();
        var $row = $(this).parents('tr');
        $row.find('.documentId').attr('disabled', false);
        $row.find('.updateDocument').removeClass('hidden');
        $row.find('.editDocument').addClass('hidden');
    });

    function addDocRef(key, data) {

        var $cloneRow = $($('.sample').clone()).removeClass('sample hidden').addClass('documentRow');

        //set document type
        $cloneRow.find('.documentTypeId').val(key);
        $cloneRow.find('.documentTypeId').selectpicker('render');
        $cloneRow.find('.documentTypeId').attr('disabled', true);
        documentTypeArray.push(key);

        loadDocumentId($cloneRow, key);

        selectedDocID = [];

        $cloneRow.find('select.documentId').append('<optgroup label="Currently Selected"></optgroup>');
        $.each(data,function(index, value){
            $cloneRow.find('optgroup').append('<option class="slc" value="'+value.id+'">'+value.code+'</option>');
            selectedDocID.push(value.id);
        });

        $cloneRow.find('select.documentId').selectpicker('val', selectedDocID);
        $cloneRow.find('select.documentId').selectpicker('render');
        $cloneRow.find('select.documentId').selectpicker('refresh');
        $cloneRow.find('select.documentId').attr('disabled', true);

        $cloneRow.find('.saveDocument').addClass('hidden');
        $cloneRow.find('.deleteDocument').removeClass('hidden');
        $( ".documentTypeBody" ).append($cloneRow);
    }

    $('#documentTable').on('click', '.updateDocument', function(e) {
        e.preventDefault();
        var $row = $(this).parents('tr');
        if (validateDocumentTypeRow($row)) {
            $row.find('.documentTypeId').attr('disabled', true);
            $row.find('.documentId').attr('disabled', true);
            $row.find('.updateDocument').addClass('hidden');
            $row.find('.editDocument').removeClass('hidden');
        }
    });

    function validateDocumentTypeRow($row) {
        var flag = true;
        var documentTypeID = $row.find('.documentTypeId').val();
        var documentID = $row.find('.documentId').val();
        if (documentTypeID == '' || documentTypeID == null) {
            p_notification(false, eb.getMessage('ERR_PO_DOC_TYPE_SELECT'));
            flag = false;
        } else if (documentID == '' || documentID == null) {
            p_notification(false, eb.getMessage('ERR_PO_DOC_ID_SELECT'));
            flag = false;
        }
        return flag;
    }

    $("#item_tbl").on('change', '.taxChecks', function() {
        setItemCost();
    });

    //responsive layout issue fix
    $(window).bind('resize ready', function() {
        if ($(window).width() <= 480) {
            $('.salesOrder_button_set').removeClass('btn-group');
            $('#placeSalesOrder, #q_view, #reset').addClass('col-xs-12').css('margin-bottom', '8px');
        } else {
            $('.salesOrder_button_set').removeClass('col-xs-12');
            $('#placeSalesOrder, #q_view, #reset').removeClass('col-xs-12').css('margin-bottom', '0px');
        }
    });

    $('.deli_row, #deli_form_group, #tax_td').hide();
    var Edit_so_ID = $('#edit_so_ID').val();
    loadSalesOrderData(Edit_so_ID);

    function loadSalesOrderData(salesOrderID) {
        var req = eb.post(BASE_URL + '/api/salesOrders/retriveSalesOrder', {'salesOrderID': salesOrderID});
        req.done(function(res) {
            if (res == 'wrongsono') {
                p_notification(false, eb.getMessage('ERR_SALEORDER_EDIT_ENTER_VALIDNUM'));
            } else {
                if (res.inactiveItemFlag) {
                    p_notification(false, res.errorMsg);
                    setTimeout(function(){ 
                        window.location.assign(BASE_URL + "/salesOrders/list")
                    }, 3000);
                    return false;
                }

                if (!$.isEmptyObject(res.uploadedAttachments)) {
                    uploadedAttachments = res.uploadedAttachments;
                }

                var NowTemp = new Date();
                var Now = new Date(NowTemp.getFullYear(), NowTemp.getMonth(), NowTemp.getDate(), 0, 0, 0, 0);
                var issuedDate = new Date(Date.parse(res.issue_date));
                var oldExpireDate = new Date(Date.parse(res.expire_date));
                var period = oldExpireDate.getTime() - issuedDate.getTime();
                var NewExpireDate = new Date(Now);
                NewExpireDate.setTime(Now.getTime() + period);
                var NowMonth = (Now.getMonth() + 1);
                var NowDate = Now.getDate();
                if (NowMonth < 10) {
                    NowMonth = '0' + NowMonth;
                } else {
                    NowMonth;
                }
                if (NowDate < 10) {
                    NowDate = '0' + NowDate;
                } else {
                    NowDate;
                }

                var NWMonth = (NewExpireDate.getMonth() + 1);
                var NWDate = NewExpireDate.getDate();
                if (NWMonth < 10) {
                    NWMonth = '0' + NWMonth;
                } else {
                    NWMonth;
                }
                if (NWDate < 10) {
                    NWDate = '0' + NWDate;
                } else {
                    NWDate;
                }
                var cusProfName;
                if (res.customer_prof_name == '' || res.customer_prof_name == null) {
                    cusProfName = 'Default';
                } else {
                    cusProfName = res.customer_prof_name;
                }
                $('#issue_date').val(eb.getDateForDocumentEdit('#issue_date', NowDate, NowMonth, Now.getFullYear())).attr('disabled', true);
                $("#expire_date").val(eb.getDateForDocumentEdit('#expire_date', NWDate, NWMonth, NewExpireDate.getFullYear()));
                $('#cust_id').val(res.customer_id);
                $('#cust_prof_name').val(cusProfName);
                $('#cust_name').val(res.customer_name).attr('disabled', true);
                $('#payment_term').val(res.payment_term);
                $('#salesPersonID').val(res.salesPersonID);
                $('#salesPersonID').selectpicker('render');
                $('#so_no').val(res.salesOrder_no).attr('disabled', true);
                $('#cmnt').val(res.comment);
                $('#invoicecurrentBalance').val(accounting.formatMoney(res.current_balance));
                $('#salesOrderCurrentCredit').val(accounting.formatMoney(res.current_credit));
                $("#form_rwo").html("");
                $('#add-cust-modal-btn').hide();

                $.each(res.docRefData,function(key, value){
                    addDocRef(key, value);
                    updateDocTypeDropDown();
                });

                if (res.priceListId != 0) {
                    $('#priceListId').val(res.priceListId).attr('disabled', true).selectpicker('refresh');
                } else {
                    $('#priceListId').attr('disabled', false);
                }
                $('#priceListId').trigger('change');

                if (res.showTax == "1") {
                    $('#show_tax').trigger('click');
                }
                items = new Array();

                $('#customCurrencyId').val(res.customCurrencyId).trigger('change').attr('disabled', true);

                customCurrencyRate = (res.salesOrdersCustomCurrencyRate != 0) ? parseFloat(res.salesOrdersCustomCurrencyRate) : 1;
                currentProducts = res.locationProducts;
                companyCurrencySymbol=res.companyCurrencySymbol;
                for (var i in res.product) {
                    var productID = res.product[i].productID;
                    var productType = res.product[i].productTypeID;
                    var code = res.product[i].locationProductID;
                    var pName = res.product[i].productName;
                    var quantity = res.product[i].quantity;
                    var qty = quantity;
                    var price = parseFloat(res.product[i].unitPrice) / customCurrencyRate;
                    var discount = parseFloat(res.product[i].discount);
                    var itdiscount=discount/customCurrencyRate;
                    var uom = res.product[i].uom;
                    var item_details = _.where(currentProducts, {lPID: code});
                    var productCode = item_details[0].pC;
                    var dsc_type = "none";
                    var calDiscount = 0;
                    var proDescription = res.product[i].soProductDescription;
                    // No need of saved tax. New tax values will use for the editign sales Order

                    if (productType == 2 && quantity == 0) {
                        qty = 1;
                    }
                    if (res.product[i].discountType == 'pr') {
                        dsc_type = "pr";
                        if(res.product[i].discount != null){
                            calDiscount = price * qty * discount / 100;
                        }
                    }
                    else if (res.product[i].discountType == 'vl') {
                        dsc_type = "vl";
                        calDiscount = (discount * qty) / customCurrencyRate;
                    }
                    if (res.productTaxes) {
                        var tax = '';
                        for (var j in res.productTaxes) {
                            if (j == res.product[i].soProductID) {
                                tax = _.map(res.productTaxes[j], function(num, val) {
                                    return val;
                                });
                                $('#taxApplied').removeClass('glyphicon glyphicon-unchecked');
                                $('#taxApplied').addClass('glyphicon glyphicon-check');
                                $('.tempLi').remove();
                                for (var k in res.productTaxes[j]) {
                                    var clonedLi = $($('#sampleLi').clone()).removeClass('hidden').attr('id', productCode + '_' + k).addClass("tempLi");
                                    clonedLi.children(".taxChecks").attr('value', k).prop('checked', true).addClass('addNewTaxCheck');
                                    clonedLi.children(".taxName").html('&nbsp&nbsp' + res.productTaxes[j][k].taxName + '&nbsp&nbsp' + res.productTaxes[j][k].soTaxPrecentage + '%').attr('for', productCode + '_' + i);
                                    clonedLi.insertBefore('#sampleLi');
                                }
                            }
                        }
                    }
                    var conversion = 1;
                    conversion = currentProducts[productID].uom[uom].uC;
                    var uomQty = (quantity / conversion).toFixed(currentProducts[productID].uom[uom].uDP);
                    var uomPrice = (price * conversion).toFixed(2);
                    tot = (price * qty) - calDiscount;
                    taxdetails = calculateItemCustomTax(tot, tax);
                    if (taxdetails != null) {
                        _.each(taxdetails.tL, function(x) {
                            if (allProductTax[x.tN] === undefined) {
                                allProductTax[x.tN] = {tp: x.tP, tA: x.tA};
                            } else {
                                allProductTax[x.tN].tA += x.tA;
                            }
                        });
                    }

                    var taxTotalAmount = taxdetails === null ? 0 : taxdetails.tTA;
                    taxA = taxTotalAmount === undefined ? 0 : parseFloat(taxTotalAmount);

                    total = tot + taxA;
                    addProduct(item_details[0].pC, pName, res.product[i].quantity, price, uom, dsc_type, itdiscount, tax, total, uomQty, productType, productID, uomPrice, proDescription);
                    setTotalCost();
                }
            }
        });
    }

    $('#customCurrencyId').on('change', function() {
        eb.ajax({
            type: 'POST',
            url: BASE_URL + '/currencySymbol/getCustomCurrencyByCustomCurrencyId',
            data: {customCurrencyId: $(this).val()},
            success: function(respond) {
                if (respond.status == true) {
                    $('.cCurrency').text(respond.data.currencySymbol);
                }
            }
        });
    });

    $('#priceListId').on('change', function() {
        priceListId = $(this).val();
        eb.ajax({
            type: 'POST',
            url: BASE_URL + '/currencySymbol/getPriceListWithDiscount',
            data: {priceListId: priceListId},
            success: function(respond) {
                if (respond.status == true) {
                    priceListItems = respond.data;
                } else {
                    priceListItems = [];
                }
            },
            async: false
        });
    });

     //Add item description
    $('#item_tbl').on('click', '.addItemDescription', function() {
        if ($('i', $(this)).hasClass('fa-comment-o')) {
            $('i', $(this)).removeClass('fa-comment-o').addClass('fa-comment');
            $(this).parents('tr').addClass('comment');
            $(this).parent().parent().siblings('.itemDescText').removeClass('hidden').focus();
        } else {
            $('i', $(this)).removeClass('fa-comment').addClass('fa-comment-o');
            $(this).parents('tr').removeClass('comment');
            $(this).parent().parent().siblings('.itemDescText').addClass('hidden');
        }
    });


	//show item description
    $('#item_tbl').on('click', '.showItemDescription', function() {
        if ($('i', $(this)).hasClass('fa-comment-o')) {
            $('i', $(this)).removeClass('fa-comment-o').addClass('fa-comment');
            $(this).parents('tr').addClass('comment');
            $(this).parent().parent().siblings('.showItemDescText').removeClass('hidden').focus();
        } else {
            $('i', $(this)).removeClass('fa-comment').addClass('fa-comment-o');
            $(this).parents('tr').removeClass('comment');
            $(this).parent().parent().siblings('.showItemDescText').addClass('hidden');
        }
    });


    $('#item_tbl').on('blur', '.itemDescText', function(e) {
        e.preventDefault();
        $(this).addClass('hidden');
        $(this).parent().find('.addItemDescription').find('i').removeClass('fa-comment').addClass('fa-comment-o');
    });

    $('#item_tbl').on('blur', '.showItemDescText', function(e) {
        e.preventDefault();
        $(this).addClass('hidden');
        $(this).parent().find('.showItemDescription').find('i').removeClass('fa-comment').addClass('fa-comment-o');
    });

    $('#payment_term').on('change', function(e) {
        e.preventDefault();
        var days = 0;
        var i = parseInt($(this).val());
        switch (i) {
            case 2:
                days = 7;
                break;
            case 3:
                days = 14;
                break;
            case 4:
                days = 30;
                break;
            case 5:
                days = 60;
                break;
            case 6:
                days = 90;
                break;
            case 7:
                days = 21;
                break;
            case 8:
                days = 28;
                break;
            case 9:
                days = 45;
                break;
            case 10:
                days = 35;
                break;
            default:
                days = 0;
                break;
        }
        setDueDate(days);
    });
    function setDueDate(days) {
        var day = parseInt(days);
        var joindate = eb.convertDateFormat('#issue_date');
        joindate.setDate(joindate.getDate() + day);

        var dd = joindate.getDate() < 10 ? '0' + joindate.getDate() : joindate.getDate();
        var mm = (joindate.getMonth() + 1) < 10 ? '0' + (joindate.getMonth() + 1) : (joindate.getMonth() + 1);
        var y = joindate.getFullYear();
        var joinFormattedDate = eb.getDateForDocumentEdit('#expire_date', dd, mm, y);

        $("#expire_date").val(joinFormattedDate);
    }

});
/**
 * Prepare tax list (Drop down)
 * @param {type} productCode
 */
function setTaxListForProduct(productID) {
    if ((!jQuery.isEmptyObject(currentProducts[productID].tax))) {
        productTax = currentProducts[productID].tax;
        $('#addNewTax').attr('disabled', false);
        $('#taxApplied').removeClass('glyphicon glyphicon-unchecked');
        $('#taxApplied').addClass('glyphicon glyphicon-check');
        $('.tempLi').remove();
        for (var i in productTax) {
            var clonedLi = $($('#sampleLi').clone()).removeClass('hidden').attr('id', productID + '_' + i).addClass("tempLi");
            clonedLi.children(".taxChecks").attr('value', i).prop('checked', true).addClass('addNewTaxCheck');
            if (productTax[i].tS == 0) {
                clonedLi.children(".taxChecks").attr('disabled', 'disabled');
                clonedLi.children(".taxName").addClass('crossText');
            }
            clonedLi.children(".taxName").html('&nbsp&nbsp' + productTax[i].tN + '&nbsp&nbsp' + productTax[i].tP + '%').attr('for', productID + '_' + i);
            clonedLi.insertBefore('#sampleLi');
        }
    } else {
        $('#taxApplied').addClass('glyphicon glyphicon-unchecked');
        $('#addNewTax').attr('disabled', 'disabled');
    }

}

/**
 *
 * @param {type} productCode
 * @param {type} price
 * @param {type} disc
 * @returns {dis_amount}
 */
function calculateDiscount(productCode, price, disc, qty) {
    var dis_amount = 0;
    if (currentProducts[productCode] !== undefined && currentProducts[productCode].dEL === "1") {
        if (priceListItems[productCode] == undefined) {
            var pdp = currentProducts[productCode].dP;
            var pdv = parseFloat(currentProducts[productCode].dV);
            if (pdp > 0) {
                //Check Customer discount against product discount
                if (disc > pdp) {
                    p_notification(false, eb.getMessage('ERR_QUOT_DISC_VALUE'));
                } else {
                    if (price > 0 && disc > 0)
                        dis_amount = (price * disc / 100);
                }
            }
            else if (pdv > 0) {
                //Check Customer discount against product discount
                if (disc > pdv)
                {
                    p_notification(false, eb.getMessage('ERR_QUOT_DISC_VALUE'));
                }
                else
                {
                    if (price > 0 && qty > 0 && disc > 0)
                        dis_amount = (qty * disc);
                }
            }
        } else {
            var itemDiscountType = priceListItems[productCode].itemDiscountType;
            if (itemDiscountType == 1) {
                dis_amount = (qty * disc);
            } else {
                dis_amount = (price * disc / 100);
            }
        }
    } else if (priceListItems[productCode] != undefined && parseFloat(priceListItems[productCode].itemPrice) > 0) {
        var itemDiscountType = priceListItems[productCode].itemDiscountType;
        if (itemDiscountType == 1) {
            dis_amount = (qty * disc);
        } else {
            dis_amount = (price * disc / 100);
        }
    }
    return dis_amount;
}

function clearTable() {
    $("#item_code").val('');
    $('#item_code').selectpicker('render');
    $("#qty").val("");
    $("#qty").siblings('.uomqty').remove();
    $("#qty").siblings('.uom-select').remove();
    $('#qty').parent().removeClass('input-group');
    $("#qty").show();
    $("#price").val("");
    $("#price").siblings('.uomPrice').remove();
    $("#price").siblings('.uom-price-select').remove();
    $("#price").show();
    $("#total").html("0.00");
    $("#discount").attr('disabled', false);
    $("#discount").val("");
    $("#discount").siblings('.uomPrice').remove();
    $("#discount").siblings('.uom-price-select').remove();
    $("#discount").show();
    $("#dic_lbl").html("Dis.nt");
    $('.uomLi').remove();
    $("#uomAb").html("");
    $('#taxApplied').removeClass('glyphicon glyphicon-checked');
    $('#taxApplied').addClass('glyphicon glyphicon-unchecked');
    $('#addNewTax').attr('disabled', false);
    $('.tempLi').remove();
    $('.itemDescText').val('');
}

function addProduct(code, pName, quantity, price, uom, dscType, dscAmount, tax, total, uomQty, productType, productID, uomPrice, item_discription) {
    if (code === "") {
        p_notification(false, eb.getMessage('ERR_QUOT_PRCODE_EMPTY'));
    } else if (products[code+'_'+price] !== undefined) {
        p_notification(false, eb.getMessage('ERR_QUOT_PR_ALREADY_ADD'));
    } else if (pName === "") {
        p_notification(false, eb.getMessage('ERR_QUOT_PRNAME_EMPTY'));
    } else if (productType == 1 && (quantity === "" || quantity <= 0)) {
        p_notification(false, eb.getMessage('ERR_QUOT_QUAN_VALUE'));
    } else if (uom === undefined) {
        p_notification(false, eb.getMessage('ERR_QUOT_SELECT_UOM'));
    } else if (price <= 0) {
        p_notification(false, eb.getMessage('ERR_QUOT_ENTER_PRICE'));
    } else if (dscType === 'vl' && parseFloat(dscAmount) > price) {
        p_notification(false, eb.getMessage('ERR_QUOT_DISCVAL_PRODPRICE'));
        return false;
    } else {
        var showDiscHtml = '<div class="pull-right input-group">'+
    	'<span class="input-group-btn">'+
    	'<button  type="button" class="btn btn-default showItemDescription" title="Show comment">'+
    	'<i class="fa fa-comment-o"></i></button></span></div>'+
    	'<textarea class="showItemDescText form-control hidden" readonly="true" placeholder="Enter item description">'+
    	'</textarea>';

        tot = total == null ? accounting.unformat($("#total").html()) : total.toFixed(2);
        var dummy = $($("#preSetSample").clone()).attr("id", "tr_" + code);
        dummy.children("#code").html(code + ' - ' + pName + " " + showDiscHtml);
        dummy.children("#quantity").html(uomQty);
        dummy.children("#unit").html(accounting.formatMoney(price*currentProducts[productID].uom[uom].uC)+" ("+currentProducts[productID].uom[uom].uA+")");
        dummy.children().children('#uomName').html(currentProducts[productID].uom[uom].uA);
        
        if (dscType === 'vl' && dscAmount > 0) {
            dummy.children("#disc").html(accounting.formatMoney(dscAmount*currentProducts[productID].uom[uom].uC) + '&nbsp;(' + currentProducts[productID].uom[uom].uA + ')'+'&nbsp;(' + companyCurrencySymbol + ')');
        } else if (dscType === 'pr' && dscAmount > 0) {
            if (dscAmount != null || !Number.isNaN(dscAmount)) {
                dummy.children("#disc").html(accounting.formatMoney(dscAmount) + '&nbsp;(%)');
            }
        }
        dummy.children().children(".delete").attr("id", code+'_'+price);
        dummy.children("#code").find('.showItemDescText').val(item_discription);
        var clonedTax = $($('#addNewTaxDiv').clone()).attr('id', '');
        clonedTax.find('.tempLi').each(function() {
            if ($(this).children(".taxChecks").prop('checked') != true) {
                $(this).children(".taxName").addClass('crossText');
            }
        });
        clonedTax.children('#addNewTax').children('#taxApplied').attr('id', '');
        clonedTax.children('#addNewTax').attr('id', '');
        clonedTax.children('#addTaxUl').children().removeClass('tempLi');
        clonedTax.children('#addTaxUl').children().children().removeClass('addNewTaxCheck');
        clonedTax.children('#addTaxUl').children().children(".taxChecks").remove();
        clonedTax.children('#addTaxUl').children('#sampleLi').remove();
        clonedTax.children('#addTaxUl').attr('id', '');
        clonedTax.find('#toggleSelection').addClass('hidden');
        dummy.children("#tax").html(clonedTax);
        dummy.children("#ttl").html(accounting.formatMoney(tot));
        dummy.removeClass("hide").insertBefore("#preSetSample");
        if (deleteProducts[code] != undefined) {
            delete deleteProducts[code];
        }
        var qty = quantity;
        dscAmount = Number.isNaN(dscAmount) ? 0 : dscAmount;
        products[code+'_'+price] = new product(code, pName, qty, uom, price, dscAmount, dscType, tax, tot, taxdetails, productType, item_discription);
        clearTable();
        return true;
    }
}