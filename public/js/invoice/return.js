var DeliveryNoteID;
var returnProducts = {};
var batchProducts = {};
var serialProducts = {};
var productsTax = {};
var productsTotal = {};
var returnSubProducts = {};
var checkSubProduct = {};
var SubProductsQty = {};
var customerID = '';
var customCurrencyRate = 1;
var multipleDlnCopy = false;
var jobID = '';
var directReturnNoteType = false;
var ignoreBudgetLimitFlag = false;
var batchCount = 0;
var currentlySelectedProduct;
var selectedProduct;
var selectedProductQuantity;
var selectedProductUom;
var productLineTotal;
var locationProducts;
var dimensionData = {};
$(document).ready(function() {
    var rtnProducts = new Array();
    var directBatchProducts = {};
    var directSerialProducts = {};
    var rowincrementID = 1;
    locationID = $('#idOfLocation').val();
    var currentItemTaxResults;
    var crossLocationFlag = false;
    var $productTable = $("#productTable");
    var $addRowSample = $('tr.add-row.sample.hidden', $productTable);
    var batch  = {};
    var productSerial = {};
    var totalTaxList = {};
    var productBatchSerial = {};
    var batchSerial = {};
    var batchForReturn = {};
    var serialForReturn = {};
    var batchSerialForReturn = {};
    var dimensionArray = {};
    var dimensionTypeID = null;

    $('#dimensionDiv').addClass('hidden');
    $('#get-so-select').on('change', function(e) {
        e.preventDefault();
        var id = $(this).children(":selected").val();
        if (id == 'jobNo') {
            $('#jobNo').val('');
            $('#jobNo').selectpicker('show');
            $('#jobNo').selectpicker('render');
            $('div#jobSelectDiv').removeClass('hidden');            
            $('div#delSelectDiv').addClass('hidden');            
            $('div#delNumberSelectDiv').addClass('hidden');            
            $('#dlnAddress').addClass('hidden');
            $('#dlnDate').addClass('hidden');
            $('#copyMultipleDln').addClass('hidden');
            $('#directReturnDiv').addClass('hidden');
            $('#dimensionDiv').removeClass('hidden');
            $('#customCurrencyId').val(8).trigger('change');
        } else {
            $('#copyMultipleDln').removeClass('hidden');
            $('#directReturnDiv').removeClass('hidden');
            $('#dlnAddress').removeClass('hidden');
            $('#dlnDate').removeClass('hidden');
            $('div#delSelectDiv').removeClass('hidden');            
            $('div#jobSelectDiv').addClass('hidden');            
            $('#dimensionDiv').addClass('hidden');
        }
    });
    var getAddRow = function(productID) {
        if (productID != undefined) {

            var $row = $('table.retrunProductTable > tbody > tr', $productTable).filter(function() {
                return $(this).data("id") == productID;
            });
            return $row;
        }
        return $('tr.add-row:not(.sample)', $productTable);
    };
    if (!getAddRow().length) {
        $($addRowSample.clone()).removeClass('sample hidden').appendTo($('#add-new-item-row'));
    }

    $('#dimensionView').on('click', function(e) {
        clearDimensionModal();
        var returnNo = $('#returnNo').val();

        if (!$.isEmptyObject(dimensionData)) {
            $.each(dimensionData[returnNo], function(index, val) {
                dimensionArray[index] = val;                
            });
            $.each(dimensionArray, function(index, value) {
                var newTrID = 'tr_' + value.dimensionTypeId;
                var clonedRow = $($('#dimensionPreSample').clone()).attr('id', newTrID).addClass('addedDimensions');
                $("input[name='dimensionType']", clonedRow).val(value.dimensionTypeTxt);
                $("input[name='dimensionValue']", clonedRow).val(value.dimensionValueTxt);
                clonedRow.removeClass('hidden');
                clonedRow.insertBefore('#dimensionPreSample');
                clearAddedNewDimensionRow();
            });
        }
        $('#addDimensionModal').modal('show');
    });

    // dimension modal
    function clearDimensionModal()
    {
        $("#addDimensionModal #dimensionTable tbody .addedDimensions").remove();
        dimensionArray = {};
    }


    $('.dimenisonchange').on('change', function(e) {
        dimensionTypeID = this.value;
        getDimensionValueListByDimensionType(dimensionTypeID, function() {
        });
    });



    function getDimensionValueListByDimensionType(dimensionTypeID, callback) {
        switch (dimensionTypeID) {
            case "job":
                $('.dimensionVal').addClass('hidden');
                $('.dimensionProjectNo').addClass('hidden');
                $('.dimensionJobNo').removeClass('hidden');
                break;
            case "project":
                $('.dimensionVal').addClass('hidden');
                $('.dimensionProjectNo').removeClass('hidden');
                $('.dimensionJobNo').addClass('hidden');
                break;
           default:
                $('.dimensionVal').removeClass('hidden');
                $('.dimensionProjectNo').addClass('hidden');
                $('.dimensionJobNo').addClass('hidden');
                $.ajax({
                    type: 'POST',
                    url: BASE_URL + '/dimension-api/getRelatedDimensionValues',
                    data: {dimensionId: dimensionTypeID},
                    success: function(respond) {
                        //empty options
                        $('#dimensionVal').empty();
                        $('#dimensionVal').append($("<option>", {value: '', html: 'Select dimension value'}));
                        $(respond.data).each(function(index, dimValue) {
                            $.each(dimValue, function(ind, val) {
                                $('#dimensionVal').append($("<option>", {value: ind, html: val.value}));
                            });
                        });
                        callback();
                    }
                });
                break;
        }
    }

    $('#add_dimension').on('click', function(e) {
        e.preventDefault();
        //if plus button is clicked then disabled the button
        //if button is clicked more than one, then it does not continue
        if ($(this).is(':disabled')) {
            return false;
        } else {
            $(this).attr('disabled', true);
            var $thisRow = $(this).parents('tr');
            if ($('.dimenisonchange').val() == 0 || $('.dimenisonchange').val() == '' || $('.dimenisonchange').val() == null) {
                p_notification(false, eb.getMessage('ERR_DIMENSION_SELCET'));
                $(this).attr('disabled', false);
            } else {

                if ($('.dimenisonchange').val() != 'job' && $('.dimenisonchange').val() != 'project') {
                    if ($('.dimensionVal').val() == 0 || $('.dimensionVal').val() == '' || $('.dimensionVal').val() == null) {
                        p_notification(false, eb.getMessage('ERR_DIMENSION_VALUE_SELCET'));
                        $(this).attr('disabled', false);
                        return;
                    }
                }

                if ($('.dimenisonchange').val() == 'project') {
                    if ($('.dimensionProjectNo').val() == 0 || $('.dimensionProjectNo').val() == '' || $('.dimensionProjectNo').val() == null) {
                        p_notification(false, eb.getMessage('ERR_DIMENSION_VALUE_SELCET'));
                        $(this).attr('disabled', false);
                        return;
                    }
                }

                if ($('.dimenisonchange').val() == 'job') {
                    if ($('.dimensionJobNo').val() == 0 || $('.dimensionJobNo').val() == '' || $('.dimensionJobNo').val() == null) {
                        p_notification(false, eb.getMessage('ERR_DIMENSION_VALUE_SELCET'));
                        $(this).attr('disabled', false);
                        return;
                    }
                }

                
                if (dimensionArray[dimensionTypeID]) {
                    p_notification(false, eb.getMessage('ERR_DIMENSION_ALREADY_INSERTED'));
                    $('.dimenisonchange').val('');
                    $('.dimenisonchange').selectpicker('render');
                    $('.dimenisonchange').trigger('change');
                } else {
                    addNewDimensionRow($(this).parents('tr'));
                }
                $(this).attr('disabled', false);
            }
        }
    });

    function addNewDimensionRow(thisRow) {

        var dimensionTypeId = $('.dimenisonchange', thisRow).val();
        var dimensionTypeTxt = $('.dimenisonchange', thisRow).find("option:selected").text();

        switch (dimensionTypeID) {
            case "job":
                var dimensionValueTxt = $('.dimensionJobNo', thisRow).find("option:selected").text();
                var dimensionValueId = $('.dimensionJobNo', thisRow).val();
                break;
            case "project":
                var dimensionValueTxt = $('.dimensionProjectNo', thisRow).find("option:selected").text();
                var dimensionValueId = $('.dimensionProjectNo', thisRow).val();
                break;
           default:
                var dimensionValueTxt = $('.dimensionVal', thisRow).find("option:selected").text();
                var dimensionValueId = $('.dimensionVal', thisRow).val();
                break;
        }
       
        var dimensionDuplicateCheck = false;
        if (!$.isEmptyObject(dimensionArray)) {
            $.each(dimensionArray, function(index, value) {
                if (value.dimensionTypeId == dimensionTypeId) {
                    dimensionDuplicateCheck = true;
                }
            });   
        }
       
        if (dimensionDuplicateCheck) {
            p_notification(false, eb.getMessage("ERR_DIMENSION_ALREADY_INSERTED"));
            return false;
        }

        var newTrID = 'tr_' + dimensionTypeId;
        var clonedRow = $($('#dimensionPreSample').clone()).attr('id', newTrID).addClass('addedDimensions');
        $("input[name='dimensionType']", clonedRow).val(dimensionTypeTxt);
        $("input[name='dimensionValue']", clonedRow).val(dimensionValueTxt);
        clonedRow.removeClass('hidden');
        clonedRow.insertBefore('#dimensionPreSample');
        dimensionArray[dimensionTypeId] = new dimension(dimensionTypeId, dimensionValueId,dimensionTypeTxt , dimensionValueTxt);
        clearAddedNewDimensionRow();
    }


    function dimension(dimensionTypeId, dimensionValueId, dimensionTypeTxt, dimensionValueTxt) {
        this.dimensionTypeId = dimensionTypeId;
        this.dimensionValueId = dimensionValueId;
        this.dimensionTypeTxt = dimensionTypeTxt;
        this.dimensionValueTxt = dimensionValueTxt;
    }

    function clearAddedNewDimensionRow() {
        $('.dimenisonchange').val('');
        $('.dimenisonchange').selectpicker('render');
        $('.dimenisonchange').trigger('change');
    }

    $('.addDimensions').on('click', function(e) {
        e.preventDefault();

        // validate products before closing modal
        if (!dimensionModalValidate(e)) {
            e.stopPropagation();
            return false;
        } else {
            $('#addDimensionModal').modal('hide');
        }
    });

    function dimensionModalValidate(e) {
        var returnNo = $('#returnNo').val();
        if ($.isEmptyObject(dimensionArray)) {
            dimensionData = {};
            return true;   
        } else {
            var dimensionSingleData = {};
            $.each(dimensionArray, function(index, val) {
                dimensionSingleData[index] = val;
            });
            dimensionData[returnNo] = dimensionSingleData;
            return true;
        }
    }

    $("#addDimensionModal #dimensionTable").on('click', '.deleteDimensionType', function(e) {
        e.preventDefault();
        var deleteDTrID = $(this).closest('tr').attr('id');
        var deleteDID = deleteDTrID.split('tr_')[1].trim();
        bootbox.confirm('Are you sure you want to remove this dimension ?', function(result) {
            if (result == true) {
                delete dimensionArray[deleteDID];
                $('#' + deleteDTrID).remove();
            }
        });
    });

    loadDropDownFromDatabase('/job-api/searchJobsForDropdown', "", 0, '.dimensionJobNo');
    $('.dimensionJobNo').selectpicker('refresh');
    $('.dimensionJobNo').on('change', function() {
        var dimensionValue = $(this).val();
    });

    loadDropDownFromDatabase('/api/project/searchProjectsForDropdown', "", 0, '.dimensionProjectNo');
    $('.dimensionProjectNo').selectpicker('refresh');
    $('.dimensionProjectNo').on('change', function() {
        var dimensionValue = $(this).val();
    });

    loadDropDownFromDatabase('/customerAPI/search-customers-for-dropdown', "", 0, '#directCustomer');
    loadDropDownFromDatabase('/productAPI/search-location-products-for-dropdown', locationID, 0, '#item_code', '', '', 'purchaseInvoice');
    loadDropDownFromDatabase('/delivery-note-api/search-delivery-note-for-dropdown', "", 0, '#deliveryNoteNo');
    $('#deliveryNoteNo').selectpicker('refresh');
    $('#deliveryNoteNo').on('change', function() {
        if ($(this).val() > 0 && DeliveryNoteID != $(this).val()) {
            DeliveryNoteID = $(this).val();
            getDeliveryNoteDetails(DeliveryNoteID);
        }
    });

    $('#deliveryNoteNumbers').on('change', function() {
        resetReturnPage();
        if(!($(this).val() == null || $(this).val() == 0) && DeliveryNoteID != $(this).val()){
            DeliveryNoteID = $(this).val();
            getDeliveryDetails(DeliveryNoteID);
        }
    });

    $('#directReturn').on('click', function(){
        if($(this).is(':checked')){
            var locationProducts = {};
            clearProductDataForNewReturn();
            $('#get-so-select').attr('disabled',true);
            $('#deliveryNoteNo').attr('disabled', true);
            $('#multipleCopy').attr('disabled', true);
            $('.row direct-return').find('.directReturnProductTable').removeClass('hidden');
            $('#deliveryNoteProductTable').addClass('hidden');
            $('.direct-return').removeClass('hidden');
            $('.normal-return').addClass('hidden');
            $('#item_code', $addRowSample).selectpicker();
            $('.add-row-direct').removeClass('hidden');
            $('#drcCus').removeClass('hidden');
            $('#dimensionDiv').removeClass('hidden');
            $('#norCus').addClass('hidden');
            directReturnNoteType = true;
            $currentRow = $('#add-new-item-row-direct');
            $('#dlnAddress').addClass('hidden');
            $('#dlnDate').addClass('hidden');
            $('#customCurrencyId').val(8).trigger('change');
            var nowTemp = new Date();
            var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);
            var checkin = $('#returnDate').datepicker({onRender: function(date) {
                    return (date.valueOf() > now.valueOf() ? ((U_DATE_R_OVERRIDE) ? '' : 'disabled') : '');
                },
                //format: 'yyyy-mm-dd'
            }).on('changeDate', function(ev) {
                checkin.hide();
            }).data('datepicker');
            checkin.setValue(now);
        } else {
            $('#get-so-select').attr('disabled',false);
            $('#deliveryNoteNo').attr('disabled', false);
            $('#multipleCopy').attr('disabled', false);
            $('#deliveryNoteProductTable').removeClass('hidden');
            $('.direct-return').addClass('hidden');
            $('.normal-return').removeClass('hidden');
            $('#drcCus').addClass('hidden');
            $('#dimensionDiv').addClass('hidden');
            $('#norCus').removeClass('hidden');
            directReturnNoteType = false;
            $('#dlnAddress').removeClass('hidden');
            $('#dlnDate').removeClass('hidden');
        }
    });

    loadDropDownFromDatabase('/customerAPI/search-customers-for-dropdown', "", 0, '#multipleDeleCustomer');
    $('#multipleDeleCustomer').selectpicker('refresh');
    $('#multipleDeleCustomer').on('change', function() {
        if ($(this).val() > 0 && customerID != $(this).val()) {
            customerID = $(this).val();
            if($(this).val() && $('#customCurrencyId').val()){
                enableMultiDeliNoteDropdown();
            }
        }
    });


    loadDropDownFromDatabase('/job-api/search-jobs-for-dropdown', '', 'closed', '#jobNo', '', '', false);
    $('#jobNo').selectpicker('refresh');
    $('#jobNo').on('change', function() {
        clearProductDataForNewReturn();
        jobID = $(this).val();
        if (jobID > 0) {
            getJobDetails(jobID);
        }
    });

    //get job related delivery notes
    function getJobDetails(jobID) {
        eb.ajax({
            type: 'POST',
            url: BASE_URL + '/delivery-note-api/getJobRelatedDeliveryNotes',
            data: {
                jobID: jobID
            },
            success: function(respond) {
                if (respond.status) {
                    multipleDlnCopy = true;
                    DeliveryNoteID = respond.data.deliveryNoteIds;
                    getDeliveryDetails(DeliveryNoteID);
                } else {
                    p_notification(respond.status, "This job doesnt have any issued products");
                    return false;
                }
            }
        });
    }

    // this will get all delivery note related details
    function getDeliveryNoteDetails(deliveryNoteID) {
        eb.ajax({
            type: 'POST',
            url: BASE_URL + '/delivery-note-api/getDeliveryNoteDetails',
            data: {
                deliveryNoteID: deliveryNoteID
            },
            success: function(respond) {
                clearProductDataForNewReturn();
                if (respond.status) {
                    if (respond.data.inactiveItemFlag) {
                        p_notification(false, respond.data.errorMsg);
                        setTimeout(function(){ 
                            window.location.assign(BASE_URL + "/return")
                        }, 3000);
                        return false;
                    }
                    customerID = respond.data.deliveryNote.customerID;
                    var customer = respond.data.customer;
                    $('#customer').val(customer.customerName + '-' + customer.customerCode).attr('disabled', true);
                    Products = respond.data.locationProducts;
                    //start : make delivery date to standrd format for the validation
                    var deliverDate = respond.data.deliveryNote.deliveryNoteDeliveryDate
                    var params;
                    var format;
                    var dateformat = $('#deliveryNoteDate').data("date-format");
                    var slash = deliverDate.indexOf("/");
                    var dot = deliverDate.indexOf(".");
                    var dash = deliverDate.indexOf("-");
                    if (dot > 0) {
                        params = deliverDate.split(".");
                        format = dateformat.split(".");
                    }
                    if (slash > 0) {
                        params = deliverDate.split("/");
                        format = dateformat.split("/");
                    }
                    if (dash > 0) {
                        params = deliverDate.split("-");
                        format = dateformat.split("-");
                    }
                    // check same location or cross location
                    if (respond.data.deliveryNote.deliveryNoteLocationID != locationID) {
                        crossLocationFlag = true;
                    }

                    var joindate;
                    if (format[0] == 'yyyy') {
                        joindate = new Date(params[0], params[1] - 1, params[2]);
                    }
                    else if (format[0] == 'dd') {
                        joindate = new Date(params[2], params[1] - 1, params[0]);
                    }
                    else if (format[0] == 'mm') {
                        joindate = new Date(params[2], params[0] - 1, params[1]);
                    }
                    //date formation end

                    $('#deliveryNoteDate').val(respond.data.deliveryNote.deliveryNoteDeliveryDate);
                    var nowTemp = new Date();
                    var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);
//                    var dDate = new Date(Date.parse(respond.data.deliveryNote.deliveryNoteDeliveryDate));
                    var dDate = new Date(Date.parse(joindate));
                    //var newdDate = new Date(dDate.setDate(dDate.getDate() - 1));
                    var newdDate = new Date(dDate.setDate(dDate.getDate()));
                    if ($('#returnDate').data('datepicker')) {
                        $('#returnDate').removeData('datepicker');
                    }
                    var returnDate = $('#returnDate').datepicker({
                        onRender: function(date) {
                            return ((date.valueOf() < newdDate.valueOf()) ? ((U_DATE_R_OVERRIDE)?'':'disabled') : '' || (date.valueOf() > now.valueOf()) ? ((U_DATE_R_OVERRIDE)?'':'disabled') : '');
                        }
                    }).on('changeDate', function(ev) {
                        returnDate.hide();
                    }).data('datepicker');
                    $('#comment').val(respond.data.deliveryNote.comment);
                    $('#deliveryNoteAddress').val(respond.data.deliveryNote.deliveryNoteAddress);
                    locationProducts = respond.data.deliveryNoteProduct;
                    $('#customCurrencyId').val(respond.data.deliveryNote.customCurrencyId).trigger('change').attr('disabled', true);
                    var rate = respond.data.deliveryNote.deliveryNoteCustomCurrencyRate;
                    customCurrencyRate = (rate != 0) ? rate : 1;
                    selectProductForReturn(locationProducts, respond.data.deliveryNote);
                } else {
                    DeliveryNoteID = '';
                    p_notification(respond.status, respond.msg);
                    $('#deliveryNoteNo').val('');
                }
            }
        });
    }


    function clearProductDataForNewReturn() {
        var $currentRow = getAddRow();
        $currentRow.removeClass('hidden');
        $('#add-new-item-row', $productTable).children('tr:not(.add-row)').remove('tr');
        returnSubProducts = {};
        returnProducts = {};
        checkSubProduct = {};
        batchProducts = {};
        productsTax = {};
        productsTotal = {};
        $('#deliveryNoteAddress').val('');
        $('#comment').val('');
        $('#subtotal').html('0.00');
        $('#finaltotal').html('0.00');
    }

    function selectProductForReturn(locationProducts, deliveryNoteData) {
        var checkProduct = 0;
        var customCurrencyRate = parseFloat(deliveryNoteData.deliveryNoteCustomCurrencyRate);
        $.each(locationProducts, function(selectedProduct, value) {
            var productCode = value.productCode;
            var productName = value.productName;
            var availableQuantity = value.deliveryNoteProductQuantity;
            var deliveryNoteproductID = value.deliveryNoteProductID;
            var defaultSellingPrice = value.deliveryNoteProductPrice / customCurrencyRate;
            var productDiscountType = value.deliveryNoteProductDiscountType;
            var discount = (value.deliveryNoteProductDiscount) ? value.deliveryNoteProductDiscount : 0;
            var productDiscount = (productDiscountType == 'value') ? discount / customCurrencyRate : discount;
            productDiscount = cutDecimalPoint(productDiscount, 2);
            var productType = value.productType;
            var flag = false;
            if (productType == 2) {
                flag = true;
            }
            if (productType != 2 && availableQuantity != 0) {
                flag = true;
            }

            if (flag) {
                checkProduct = 1;
                var $currentRow = getAddRow();
                clearProductRow($currentRow);
                $("input[name='itemCode']", $currentRow).val(productCode + ' - ' + productName).prop('readonly', true);
                $("input[name='itemCode']", $currentRow).data('PT', productType);
                $("input[name='itemCode']", $currentRow).data('PC', productCode);
                $("input[name='itemCode']", $currentRow).data('PN', productName);
                $("input[name='returnQuanity']", $currentRow).parent().addClass('input-group');
                $("input[name='deliveredQuantity']", $currentRow).parent().addClass('input-group');
                $("input[name='returnQuanity']", $currentRow).val(availableQuantity).prop('readonly', true).addUom(Products[selectedProduct].uom);
                $("input[name='deliveredQuantity']", $currentRow).val(availableQuantity).prop('readonly', true).addUom(Products[selectedProduct].uom);
                $("input[name='unitPrice']", $currentRow).val(defaultSellingPrice).prop('readonly', true).addUomPrice(Products[selectedProduct].uom);
                $("input[name='discount']", $currentRow).val(productDiscount).prop('readonly', true);
                if (productDiscountType != 'precentage') {
                    $("input[name='discount']", $currentRow).addClass('value');
                    if ($("input[name='discount']", $currentRow).hasClass('precentage')) {
                        $("input[name='discount']", $currentRow).removeClass('precentage');
                    }
                } else {
                    $("input[name='discount']", $currentRow).addClass('precentage');
                    if ($("input[name='discount']", $currentRow).hasClass('value')) {
                        $("input[name='discount']", $currentRow).removeClass('value');
                    }
                    $(".discountSymbol", $currentRow).text('%');
                }


                $currentRow.data('id', selectedProduct);
                $currentRow.data('dPID', deliveryNoteproductID);
                // clear old rows
                $("#batch_data tr:not(.hidden)").remove();
                // check if any batch / serial products exist

                if ($.isEmptyObject(value.subProduct)) {
                    $currentRow.removeClass('subproducts');
                    $('.edit-modal', $currentRow).parent().addClass('hidden');
                    $("td[colspan]", $currentRow).attr('colspan', 2);
                    $("input[name='returnQuanity']", $currentRow).prop('readonly', false).change().focus();
                } else {
                    $currentRow.addClass('subproducts');
                    $('.edit-modal', $currentRow).parent().removeClass('hidden');
                    $("td[colspan]", $currentRow).attr('colspan', 1);
                    $("input[name='returnQuanity']", $currentRow).prop('readonly', true).change();
                }
                $currentRow.find('.delete').removeClass('disabled');
                $currentRow.addClass('edit-row');
                setTaxListForProduct(selectedProduct, $currentRow);
                $currentRow.attr('id', 'product' + selectedProduct);
                $("input[name='unitPrice']", $currentRow).trigger('focusout');
                $currentRow
                        .removeClass('add-row');
                $($addRowSample.clone()).removeClass('sample hidden').appendTo($('#add-new-item-row', $productTable));
            }
        });
        if (checkProduct == 0) {
            p_notification(false, eb.getMessage('ERR_RETURN_DELINOTE'));
        }
        var $currentRow = getAddRow();
        $currentRow.addClass('hidden');
        return;
    }

    $productTable.on('click', 'button.edit-modal', function(e) {

        var productID = $(this).parents('tr').data('id');
        var productCode = $(this).parents('tr').find('#itemCode').val();
        $("#batch_data tr:not(.hidden)").remove();
        $('#addReturnProductsModal').modal('show').data('id', productID);
        addSubProduct(productID, productCode, 0);
        $("tr", '#batch_data').each(function() {
            var $thisSubRow = $(this);
            for (var i in returnSubProducts[productID]) {
                if (returnSubProducts[productID][i].serialID != '') {
                    if ($(".serialID", $thisSubRow).data('id') == returnSubProducts[productID][i].serialID) {
                        $("input[name='returnQuantityCheck']", $thisSubRow).prop('checked', true);
                    }
                } else if (returnSubProducts[productID][i].batchID != '') {
                    if ($(".batchCode", $thisSubRow).data('id') == returnSubProducts[productID][i].batchID) {
                        $(this).find("input[name='returnQuantity']").val(returnSubProducts[productID][i].qtyByBase).change();
                    }
                }
            }
        });
        $('#addDeliveryNoteProductsModal').data('id', $(this).parents('tr').data('id')).modal('show');
        $('#addDeliveryNoteProductsModal').unbind('hide.bs.modal');
    });

    $productTable.on('click', 'button.add, button.save', function(e) {
        var $thisRow = $(this).parents('tr');
        var thisVals = getCurrentProductData($thisRow);
        $("input[name='availableQuantity']", $thisRow).prop('readonly', true);
        if ((thisVals.productCode.trim()) == '' || (thisVals.productName.trim()) == '') {
            p_notification(false, eb.getMessage('ERR_DELI_SELECT_PROD'));
            $("input[name='itemCode']", $thisRow).focus();
            return false;
        }

        if (thisVals.productType != 2 && (isNaN(parseFloat(thisVals.returnQuantity.qty)) || parseFloat(thisVals.returnQuantity.qty) <= 0)) {
            p_notification(false, eb.getMessage('ERR_RETURN_VALQUANTITY'));
            $("input[name='returnQuanity']", $thisRow).focus();
            return false;
        }
        if (parseFloat(thisVals.returnQuantity.qty) > parseFloat(thisVals.availableQuantity.qty)) {
            p_notification(false, eb.getMessage('ERR_RETURN_QUANTITY'));
            $("input[name='returnQuanity']", $thisRow).focus();
            if (checkSubProduct[thisVals.productID] == undefined) {
                return false;
            }
        }

        if (thisVals.productDiscount) {
            if (isNaN(parseFloat(thisVals.productDiscount)) || parseFloat(thisVals.productDiscount) < 0) {
                p_notification(false, eb.getMessage('ERR_DELI_VALID_DISCOUNT'));
                $("input[name='discount']", $thisRow).focus();
                return false;
            }
        }

        if ($thisRow.hasClass('subproducts')) {
            if (parseFloat(SubProductsQty[thisVals.productID]) != parseFloat(thisVals.returnQuantity.qty)) {
                p_notification(false, eb.getMessage('ERR_RETURN_SUB_PRODUCT_CHECK', thisVals.productCode));
                $('.edit-modal', $thisRow).focus();
                return false;
            }
        }

        // if add button is clicked
        $thisRow.removeClass('edit-row');
        if ($(this).hasClass('add')) {
            $thisRow
                    .removeClass('edit-row')
                    .find("input[name='itemCode'], input[name='itemName'], input[name='deliverQuanity'],input[name='availableQuantity'],input[name='unitPrice'],input[name='discount']").prop('readonly', true).end()
                    .find('td').find('#addNewTax').attr('disabled', true);
        } else if ($(this).hasClass('save')) {
            $thisRow
                    .find("input[name='itemCode'], input[name='itemName'], input[name='deliverQuanity'],input[name='availableQuantity'],input[name='unitPrice'],input[name='discount']").prop('readonly', true).end()
                    .find('td').find('#addNewTax').attr('disabled', true);
        } else { // if save button is clicked
            $thisRow.find("input[name='deliverQuanity']").prop('readonly', true);
        }

        // if batch product modal is available for this product
        $(this, $thisRow).parent().find('.edit').removeClass('hidden');
        $(this, $thisRow).parent().parent().find('.delete').removeClass('disabled');
        $(this, $thisRow).parent().find('.add').addClass('hidden');
        $(this, $thisRow).parent().find('.save').addClass('hidden');
        if ($thisRow.hasClass('subproducts')) {
            $('.edit-modal', $thisRow).parent().addClass('hidden');
            $("td[colspan]", $thisRow).attr('colspan', 2);
        } else {
            $("#returnQuanity", $thisRow).attr('disabled', true).change();
        }
        returnProducts[thisVals.productID] = thisVals;
        setReturnTotalCost();
    });

    $('table#deliveryNoteProductTable>tbody').on('keypress', 'input#itemCode, input.uomqty, input#unitPrice, input#discount, #deliveredQuantity, #returnQuanity ', function(e) {

        if (e.which == 13) {
            $('button.add', $(this).parents('tr.edit-row')).trigger('click');
            e.preventDefault();
        }

    });

    $productTable.on('click', 'button.edit', function(e) {

        var $thisRow = $(this).parents('tr');
        $thisRow.addClass('edit-row')
                .find('td').find('#addNewTax').attr('disabled', false);
        $(this, $thisRow).parent().find('.edit').addClass('hidden');
        $(this, $thisRow).parent().find('.save').removeClass('hidden');
        if ($thisRow.hasClass('subproducts')) {
            $('.edit-modal', $thisRow).parent().removeClass('hidden');
            $("td[colspan]", $thisRow).attr('colspan', 1);
        } else {
            $("input[name='returnQuanity']", $thisRow).prop('disabled', false).focus().change();
        }
    });

    $productTable.on('click', 'button.delete', function(e) {

        var $thisRow = $(this).parents('tr');
        var productID = $thisRow.data('id');
        delete returnProducts[productID];
        setReturnTotalCost();
        $thisRow.remove();
    });
    function addSubProduct(selectedProduct, productCode, deleted) {
        if ((!$.isEmptyObject(locationProducts[selectedProduct].subProduct))) {
            batchProduct = locationProducts[selectedProduct].subProduct;
            if (multipleDlnCopy == false) {
                selectedProduct = selectedProduct;
            } else {
                selectedProduct = selectedProduct.split("_")[1];
            }
            batch = Products[selectedProduct].batch;
            batchSerial = Products[selectedProduct].batchSerial;
            productBatchSerial = Products[selectedProduct].batchSerial;
            productSerial = Products[selectedProduct].serial;
            if (crossLocationFlag) {
                batch = {};
                productSerial = {};
                productBatchSerial = {};
                batchSerial = {};
            }

            for (var i in batchProduct) {
                if (deleted != 1 && batchProduct[i].productBatchID != null && batchProduct[i].productSerialID != null && crossLocationFlag ) {
                    
                    var tmp = {
                        'PBC': batchProduct[i].productBatchCode,
                        'PBID' : batchProduct[i].productBatchID,
                        'PBSExpD': batchProduct[i].batchExpireDate,
                        'PBSWoD': batchProduct[i].batchWarranty,
                        'PBUP': batchProduct[i].batchPrice,
                        'PSC': batchProduct[i].productSerialCode,
                        'PSID' : batchProduct[i].productSerialID,
                        'PSS': 1,
                    };
                    productBatchSerial[batchProduct[i].productSerialID] = tmp;
                    batchSerial[batchProduct[i].productSerialID] = tmp;
                    batchSerialForReturn[batchProduct[i].productSerialID] = tmp;
                } else if (deleted != 1 && batchProduct[i].productBatchID != null && crossLocationFlag) {
                    var temp = {
                        'PBC': batchProduct[i].productBatchCode,
                        'PBID' : batchProduct[i].productBatchID,
                        'PBExpD': batchProduct[i].batchExpireDate,
                        'PBManD': batchProduct[i].batchManuFtDate,
                        'BtPrice': batchProduct[i].batchPrice,
                        'PBWoD': batchProduct[i].batchWarranty,

                    };
                    
                    batch[batchProduct[i].productBatchID] = temp;
                    batchForReturn[batchProduct[i].productBatchID] = temp;
                    
                } else if (deleted != 1 && batchProduct[i].productSerialID != null && crossLocationFlag) {
                    var tmp = {
                        'PSC': batchProduct[i].productSerialCode,
                        'PSID' : batchProduct[i].productSerialID,
                        'PSS': 1,
                        'PSExpD': batchProduct[i].serialExpireDate,
                        'PSWoD' : batchProduct[i].serialWarranty

                    };
                    productSerial[batchProduct[i].productSerialID] = tmp;
                    serialForReturn[batchProduct[i].productSerialID] = tmp;
                }
            }

            for (var i in batchProduct) {
                if (batchProduct[i].deliveryProductSubQuantity != 0) {
                    if (deleted != 1 && batchProduct[i].productBatchID != null && batchProduct[i].productSerialID != null) {
                        
                        var serialKey = productCode + "-" + batchProduct[i].productSerialID;
                        var serialQty = (batchProduct[i].deliveryProductSubQuantity == 1) ? 1 : 0;
                        var $clonedRow = $($('.batch_row').clone());
                        $clonedRow
                                .data('id', batchProduct[i].productSerialID)
                                .removeAttr('id')
                                .removeClass('hidden batch_row')
                                .addClass('remove_row');
                        $(".serialID", $clonedRow)
                                .html(batchSerial[batchProduct[i].productSerialID].PSC)
                                .data('id', batchProduct[i].productSerialID);
                        $(".batchCode", $clonedRow)
                                .html(batchSerial[batchProduct[i].productSerialID].PBC)
                                .data('id', batchProduct[i].productBatchID);
                        $("input[name='availableQuantity']", $clonedRow).val(serialQty).addUom(Products[selectedProduct].uom);
                        if (serialQty = 0) {
                            $("input[name='returnQuantityCheck']", $clonedRow).prop('disabled', true);
                        }
                        $("input[name='returnQuantity']", $clonedRow).parent().remove();
                        if (batchProducts[serialKey]) {
                            if (batchProducts[serialKey].Qty == 1) {
                                $("input[name='deliveryQuantityCheck']", $clonedRow).attr('checked', true);
                            }
                        }

                        $clonedRow.insertBefore('.batch_row');
                    } else if (deleted != 1 && batchProduct[i].productBatchID != null) {
                        if ((!$.isEmptyObject(Products[selectedProduct].productIDs))) {
                            if (Products[selectedProduct].productIDs[batchProduct[i].productBatchID]) {
                                continue;
                            }
                        }
                        var batchKey = productCode + "-" + batchProduct[i].productBatchID;
                        var $clonedRow = $($('.batch_row').clone());
                        $clonedRow
                                .data('id', batchProduct[i].productBatchID)
                                .removeAttr('id')
                                .removeClass('hidden batch_row')
                                .addClass('remove_row');
                        $(".batchCode", $clonedRow).html(batch[batchProduct[i].productBatchID].PBC).data('id', batchProduct[i].productBatchID);
                        $("input[name='availableQuantity']", $clonedRow).val(batchProduct[i].deliveryProductSubQuantity).addUom(Products[selectedProduct].uom);
                        if (batchProduct[i].deliveryProductSubQuantity == 0) {
                            $("input[name='returnQuantity']", $clonedRow).prop('disabled', true);
                        }
                        $("input[name='returnQuantityCheck']", $clonedRow).remove();
                        if (batchProducts[batchKey]) {
                            $("input[name='returnQuantity']", $clonedRow).val(batchProducts[batchKey].Qty);
                        }
                        $("input[name='returnQuantity']", $clonedRow).addUom(Products[selectedProduct].uom);
                        $clonedRow.insertBefore('.batch_row');
                    } else if (deleted != 1 && batchProduct[i].productSerialID != null) {
                        var serialKey = productCode + "-" + batchProduct[i].productSerialID;
                        var serialQty = (batchProduct[i].deliveryProductSubQuantity == 1) ? 1 : 0;
                        var $clonedRow = $($('.batch_row').clone());
                        $clonedRow
                                .data('id', batchProduct[i].productSerialID)
                                .removeAttr('id')
                                .removeClass('hidden batch_row')
                                .addClass('remove_row');
                        $(".serialID", $clonedRow)
                                .html(productSerial[batchProduct[i].productSerialID].PSC)
                                .data('id', batchProduct[i].productSerialID);
                        $("input[name='availableQuantity']", $clonedRow).val(serialQty).addUom(Products[selectedProduct].uom);
                        if (serialQty = 0) {
                            $("input[name='returnQuantityCheck']", $clonedRow).prop('disabled', true);
                        }
                        $("input[name='returnQuantity']", $clonedRow).parent().remove();
                        if (batchProducts[serialKey]) {
                            if (batchProducts[serialKey].Qty == 1) {
                                $("input[name='returnQuantityCheck']", $clonedRow).attr('checked', true);
                            }
                        }

                        $clonedRow.insertBefore('.batch_row');
                    } else {
                        batchProducts[batchKey] = undefined;
                    }
                }
            }
        }
    }

    function setTaxListForProduct(productID, $currentRow, ItemTaxes) {
        if ((!jQuery.isEmptyObject(locationProducts[productID].tax))) {
            productTax = locationProducts[productID].tax;

            $currentRow.find('#taxApplied').removeClass('glyphicon glyphicon-unchecked');
            $currentRow.find('#taxApplied').addClass('glyphicon glyphicon-check');
            $currentRow.find('.tempLi').remove();
            for (var i in productTax) {
                var taxAmount = 0.00;
                if (!jQuery.isEmptyObject(ItemTaxes)) {
                    if(ItemTaxes[i] != undefined){
                        taxAmount = ItemTaxes[i].tA;
                    }
                }
                var clonedLi = $($currentRow.find('#sampleLi').clone()).removeClass('hidden').attr('id', 'li_' + i).addClass("tempLi");
                clonedLi.children(".taxChecks").attr('id', productID + '_' + i).prop('checked', true).prop('disabled', true).addClass('addNewTaxCheck');
                clonedLi.children(".taxName").html('&nbsp&nbsp' + productTax[i].deliveryNoteTaxName + '&nbsp&nbsp' + productTax[i].deliveryNoteTaxPrecentage + '% , ' + taxAmount).attr('for', productID + '_' + i);
                clonedLi.insertBefore($currentRow.find('#sampleLi'));
            }
        } else {
            $('#taxApplied').addClass('glyphicon glyphicon-unchecked');
            $('#addNewTax').attr('disabled', 'disabled');
        }

    }

    function clearProductRow($currentRow) {
        $("input[name='itemCode']", $currentRow).val('');
        $("input[name='availableQuantity']", $currentRow).val('');
        $("input[name='returnQuanity']", $currentRow).val('');
        $("input[name='unitPrice']", $currentRow).val('');
        $("input[name='discount']", $currentRow).val('');
        $(".tempLi", $currentRow).remove('');
        $("#taxApplied", $currentRow).removeClass('glyphicon-check');
        $("#taxApplied", $currentRow).addClass('glyphicon-unchecked');
        $(".selected", $currentRow).text('');
        $(".uomList", $currentRow).remove('');
    }

    $('#batch-save').on('click', function(e) {
        e.preventDefault();
        // validate batch / serial products before closing modal
        if (!batchModalValidate(e)) {
            e.stopPropagation();
            return false;
        } else {
            $('#addReturnProductsModal').modal('hide');
        }
    });
    function batchModalValidate(e) {

        var productID = $('#addReturnProductsModal').data('id');
        var $thisParentRow = getAddRow(productID);
        var thisVals = getCurrentProductData($thisParentRow);
        var $batchTable = $("#addReturnProductsModal .batch-table tbody");
        var qtyTotal = 0;
        var subProducts = [];

        $("input[name='returnQuantity'], input[name='returnQuantityCheck']:checked", $batchTable).each(function() {

            var $thisSubRow = $(this).parents('tr');
            var thisretrunQuantity = $(this).val();
            if ((thisretrunQuantity).trim() != "" && isNaN(parseFloat(thisretrunQuantity))) {
                p_notification(false, eb.getMessage('ERR_RETURN_VALQUANTITY'));
                $(this).focus();
                return qtyTotal = false;
            }

            thisretrunQuantity = (isNaN(parseFloat(thisretrunQuantity))) ? 0 : parseFloat(thisretrunQuantity);
            // if a conversion is not set, it is assumed to be the base (eg: serial checkbox doesnt have multiple UOMs)

            // check if trasnfer qty is greater than available qty
            var thisAvailableQuantity = $("input[name='availableQuantity']", $thisSubRow).val();
            thisAvailableQuantity = (isNaN(parseFloat(thisAvailableQuantity))) ? 0 : parseFloat(thisAvailableQuantity);
            var thisAvailableQuantityByBase = thisAvailableQuantity;
            var thisReturnQuantityByBase = thisretrunQuantity;
            if (thisReturnQuantityByBase > thisAvailableQuantityByBase) {
                p_notification(false, eb.getMessage('ERR_RETURN_QUANTITY'));
                $(this).focus();
                return qtyTotal = false;
            }

            qtyTotal = qtyTotal + (thisretrunQuantity);
            // if a product transfer is present, prepare array to be sent to backend

            if ((thisretrunQuantity) > 0) {

                var thisSubProduct = {};
                thisSubProduct.batchID = '';
                thisSubProduct.serialID = '';
                if ($(".batchCode", $thisSubRow).data('id')) {
                    thisSubProduct.batchID = $(".batchCode", $thisSubRow).data('id');
                }

                if ($(".serialID", $thisSubRow).data('id')) {
                    thisSubProduct.serialID = $(".serialID", $thisSubRow).data('id');
                }

                thisSubProduct.qtyByBase = thisretrunQuantity;
                subProducts.push(thisSubProduct);
            }

        });
        if (checkSubProduct[thisVals.productID] != undefined) {
            if (qtyTotal != $("input[name='returnQuanity']", $thisParentRow).val()) {
                p_notification(false, eb.getMessage('ERR_DELI_TOTAL_SUBQUAN'));
                return qtyTotal = false;
            } else {
                checkSubProduct[thisVals.productID] = 1;
            }
        }

        // to break out form $.each and exit function
        if (qtyTotal === false)
            return false;
        // ideally, since the individual batch/serial quantity is checked to be below the available qty,
        // the below condition should never become true
        if (qtyTotal > thisVals.availableQuantity.qty) {
            p_notification(false, eb.getMessage('ERR_DELI_TOTAL_DELIQUAN'));
            return false;
        }

        returnSubProducts[thisVals.productID] = subProducts;
        SubProductsQty[thisVals.productID] = qtyTotal;
        var $transferQ = $("input[name='returnQuanity']", $thisParentRow).prop('readonly', true);
        if (checkSubProduct[thisVals.productID] == undefined) {
            $transferQ.val(qtyTotal).change();
        }
        $("#unitPrice", $thisParentRow).trigger(jQuery.Event("focusout"));
        return true;
    }

    function getCurrentProductData($thisRow) {
        $("input.uomqty", $thisRow).change();
        var discountType = '';
        if ($("input[name='discount']", $thisRow).hasClass('value')) {
            discountType = 'value';
        } else if ($("input[name='discount']", $thisRow).hasClass('precentage')) {
            discountType = 'precentage';
        }
        var delPID = $thisRow.data('dPID');
        var availableqty = $("input[name='deliveredQuantity']", $thisRow).val();
        if (isNaN(availableqty)) {
            availableqty = 0;
        }
        var returnqty = $("input[name='returnQuanity']", $thisRow).val();
        if (returnqty == '') {
            returnqty = 0;
        }
        var thisVals = {
            productID: $thisRow.data('id'),
            deliveryNoteproductID: delPID,
            productCode: $("input[name='itemCode']", $thisRow).data('PC'),
            productName: $("input[name='itemCode']", $thisRow).data('PN'),
            productPrice: $("input[name='unitPrice']", $thisRow).val(),
            productDiscount: $("input[name='discount']", $thisRow).val(),
            productDiscountType: discountType,
            productTotal: productsTotal[$thisRow.data('id')],
            pTax: productsTax[$thisRow.data('id')],
            productType: $("input[name='itemCode']", $thisRow).data('PT'),
            availableQuantity: {
                qty: availableqty,
            },
            returnQuantity: {
                qty: returnqty,
            }
        };
        return thisVals;
    }

    $productTable.on('focusout', '#returnQuanity,#unitPrice,#discount,.uomqty, .uomPrice', function() {
        var $thisRow = $(this).parents('tr');
        var productID = $thisRow.data('id');
        var checkedTaxes = Array();
        $thisRow.find('input:checkbox.addNewTaxCheck').each(function() {
            if (this.checked) {
                var cliTID = this.id;
                if (multipleDlnCopy == false) {
                    var tTaxID = cliTID.split('_')[1];
                } else {
                    var tTaxID = cliTID.split('_')[2];
                }
                checkedTaxes.push(tTaxID);
            }
        });
        var productType = $(this).parents('tr').find('#itemCode').data('PT');
        var returnQty = $(this).parents('tr').find('#returnQuanity').val();
        if (productType == 2 && returnQty == 0) {
            returnQty = 1;
        }
        var tmpItemTotal = (toFloat($(this).parents('tr').find('#unitPrice').val()) * toFloat(returnQty));
        var tmpItemCost = tmpItemTotal;
        var tmpItemQuentity = returnQty;
        if ($(this).parents('tr').find('#discount').hasClass('value')) {
            var tmpPDiscountvalue = isNaN($(this).parents('tr').find('#discount').val()) ? 0 : $(this).parents('tr').find('#discount').val();
            if (tmpPDiscountvalue > 0) {
                tmpItemCost -= tmpPDiscountvalue * tmpItemQuentity;
            }
        } else {
            var tmpPDiscount = isNaN($(this).parents('tr').find('#discount').val()) ? 0 : $(this).parents('tr').find('#discount').val();
            if (tmpPDiscount > 0) {
                tmpItemCost -= (tmpItemTotal * toFloat(tmpPDiscount) / toFloat(100));
            }
        }
        if (checkedTaxes) {
            currentItemTaxResults = calculateItemCustomTax(tmpItemCost, checkedTaxes);
            currentTaxAmount = (currentItemTaxResults == null) ? null : currentItemTaxResults.tTA;
        } else {
            currentTaxAmount = 0;
            currentItemTaxResults = '';
        }

        var itemCost = toFloat(tmpItemCost) + toFloat(currentTaxAmount);
        productsTotal[productID] = itemCost;
        productsTax[productID] = currentItemTaxResults;
        $(this).parents('tr').find('#addNewTotal').html(accounting.formatMoney(itemCost));
        if (productsTax[productID] != null) {
            if (jQuery.isEmptyObject(productsTax[productID].tL)) {
                $(this).parents('tr').find('#taxApplied').addClass('glyphicon glyphicon-unchecked');
            } else {
                $(this).parents('tr').find('#taxApplied').removeClass('glyphicon glyphicon-unchecked');
                $(this).parents('tr').find('#taxApplied').addClass('glyphicon glyphicon-check');
            }

            setTaxListForProduct(productID, $thisRow, currentItemTaxResults.tL);
        }
    });
    function setReturnTotalCost() {
        var grnTotal = 0;
        for (var i in returnProducts) {
            grnTotal += toFloat(returnProducts[i].productTotal);
        }
        $('#subtotal').html(accounting.formatMoney(grnTotal));
        if ($('#deliveryChargeEnable').is(':checked')) {
            var deliveryAmount = $('#deliveryCharge').val();
            grnTotal += toFloat(deliveryAmount);
        }
        $('#finaltotal').html(accounting.formatMoney(grnTotal));
    }

    $('#returnForm').on('submit', function(e) {
        if (directReturnNoteType) {
            saveDirectReturnData();
            return false;
        } else {
            DeliverNoteID = $('#deliveryNoteNo').val();
            if ((DeliverNoteID === '' || DeliverNoteID === '0') && multipleDlnCopy == false) {
                p_notification(false, eb.getMessage('ERR_RETURN_ADD_DELEVERY_NOTE'));
                $('#deliveryNoteNo').siblings('.bootstrap-select').find('button').click();
                return false;
            }
            e.preventDefault();
            if (jQuery.isEmptyObject(returnProducts)) {
                p_notification(false, eb.getMessage('ERR_RETURN_ADD_PROD'));
                return false;
            }
            if ($("tr.edit-row", $productTable).length > 0) {
                p_notification(false, eb.getMessage('ERR_RETURN_SAVE_ALLPROD'));
                return false;
            }
            saveReturnData();
            return false;
        }
    });
    function saveReturnData() {
        for (var i in checkSubProduct) {
            var $currentRow = getAddRow(i);
            if (!$($currentRow).find('.edit-modal').parent('td').hasClass('hidden')) {
                if (checkSubProduct[i] == 0) {
                    p_notification(false, eb.getMessage('ERR_DELI_ADD_PROD', returnProducts[i].productCode));
                    return;
                }
            }
        }
        var formData = {
            returnCode: $('#returnNo').val(),
            location: $('#currentLocation').val(),
            date: $('#returnDate').val(),
            customer: $('#customer').val(),
            customerID: customerID
        };
        if (validateTransferForm(formData)) {
            $('#returnSaveButton').prop('disabled', true);
            eb.ajax({
                type: 'POST',
                url: BASE_URL + '/returnAPI/saveReturnDetails',
                data: {
                    returnCode: formData.returnCode,
                    locationID: locationID,
                    products: returnProducts,
                    subProducts: returnSubProducts,
                    date: formData.date,
                    customerID: customerID,
                    customerName: $('#customer').val(),
                    returnTotalPrice: $('#finaltotal').text().replace(/,/g, ''),
                    returnComment: $('#comment').val(),
                    deliveryNoteID: DeliveryNoteID,
                    customCurrencyId: $('#customCurrencyId').val(),
                    salesReturnCustomCurrencyRate: customCurrencyRate,
                    multipleDlnCopyFlag: multipleDlnCopy,
                    crossLocationFlag: crossLocationFlag,
                    delBatchDetails: batchForReturn,
                    delSerialDetails: serialForReturn,
                    delSerialBatchDetails: batchSerialForReturn,
                    dimensionData: dimensionData,
                    ignoreBudgetLimit: ignoreBudgetLimitFlag
                },
                success: function(respond) {
                    if (respond.status) {
                        p_notification(respond.status, respond.msg);
                        var fileInput = document.getElementById('documentFiles');
                        if(fileInput.files.length > 0) {
                            var form_data = false;
                            if (window.FormData) {
                                form_data = new FormData();
                            }
                            form_data.append("documentID", respond.data.returnID);
                            form_data.append("documentTypeID", 5);
                            
                            for (var i = 0; i < fileInput.files.length; i++) {
                                form_data.append("files[]", fileInput.files[i]);
                            }

                            eb.ajax({
                                url: BASE_URL + '/store-files',
                                type: 'POST',
                                processData: false,
                                contentType: false,
                                data: form_data,
                                success: function(res) {
                                }
                            });
                        }

                        documentPreview(BASE_URL + '/return/viewReturnReceipt/' + respond.data.returnID, 'documentpreview', "/returnAPI/send-email", function($preview) {
                            $preview.on('hidden.bs.modal', function() {
                                if (!$("#preview:visible").length) {
                                    window.location.reload();
                                }
                            });
                        });
                    } else {
                        if (respond.data == "NotifyBudgetLimit") {
                            bootbox.confirm(respond.msg+' ,Are you sure you want to save this return ?', function(result) {
                                if (result == true) {
                                    ignoreBudgetLimitFlag = true;
                                    saveReturnData();
                                } else {
                                    setTimeout(function(){ 
                                        location.reload();
                                    }, 3000);
                                }
                            });
                        } else {
                            p_notification(respond.status, respond.msg);
                        }
                    }
                }
            });
        }
    }

    function saveDirectReturnData() {
        var formData = {
            returnCode: $('#returnNo').val(),
            location: $('#currentLocation').val(),
            date: $('#returnDate').val(),
            customer: $('#directCustomer').val()
        };
        if (validateDirectReturnForm(formData)) {
            $('#returnSaveButton').prop('disabled', true);
            eb.ajax({
                type: 'POST',
                url: BASE_URL + '/returnAPI/saveDirectReturnDetails',
                data: {
                    returnCode: formData.returnCode,
                    locationID: locationID,
                    products: rtnProducts,
                    date: formData.date,
                    customerID: formData.customer,
                    returnTotalPrice: $('#drFinaltotal').text().replace(/,/g, ''),
                    returnComment: $('#comment').val(),
                    customCurrencyId: $('#customCurrencyId').val(),
                    directReturnFlag: directReturnNoteType,
                    dimensionData: dimensionData,
                    ignoreBudgetLimit: ignoreBudgetLimitFlag
                },
                success: function(respond) {
                    if (respond.status) {
                        p_notification(respond.status, respond.msg);
                        var fileInput = document.getElementById('documentFiles');
                        if(fileInput.files.length > 0) {
                            var form_data = false;
                            if (window.FormData) {
                                form_data = new FormData();
                            }
                            form_data.append("documentID", respond.data.returnID);
                            form_data.append("documentTypeID", 5);
                            
                            for (var i = 0; i < fileInput.files.length; i++) {
                                form_data.append("files[]", fileInput.files[i]);
                            }

                            eb.ajax({
                                url: BASE_URL + '/store-files',
                                type: 'POST',
                                processData: false,
                                contentType: false,
                                data: form_data,
                                success: function(res) {
                                }
                            });
                        }
                        
                        documentPreview(BASE_URL + '/return/viewReturnReceipt/' + respond.data.returnID, 'documentpreview', "/returnAPI/send-email", function($preview) {
                            $preview.on('hidden.bs.modal', function() {
                                if (!$("#preview:visible").length) {
                                    window.location.reload();
                                }
                            });
                        });
                    } else {
                        if (respond.data == "NotifyBudgetLimit") {
                            bootbox.confirm(respond.msg+' ,Are you sure you want to save this direct return ?', function(result) {
                                if (result == true) {
                                    ignoreBudgetLimitFlag = true;
                                    saveDirectReturnData();
                                } else {
                                    setTimeout(function(){ 
                                        location.reload();
                                    }, 3000);
                                }
                            });
                        } else {
                            p_notification(respond.status, respond.msg);
                        }
                    }
                }
            });
        }
    }

    $('#customCurrencyId').on('change', function() {
        eb.ajax({
            type: 'POST',
            url: BASE_URL + '/currencySymbol/getCustomCurrencyByCustomCurrencyId',
            data: {customCurrencyId: $(this).val()},
            success: function(respond) {
                if (respond.status == true) {
                    if ($('#discount').hasClass('value')) {
                        $('.discountSymbol').text(respond.data.currencySymbol);
                    }
                    $('.cCurrency').text(respond.data.currencySymbol);
                }
            }
        });
    });
    function validateTransferForm(formData) {
        var selectedid = "#" + document.activeElement.id;
        var tableId = $(selectedid).closest('table').attr("id");
        if (formData.location == null || formData.location == "") {
            p_notification(false, eb.getMessage('ERR_RETURN_SELECT_LOCAT'));
            $('#currentLocation').focus();
            return false;
        } else if (formData.returnCode == null || formData.returnCode == "") {
            p_notification(false, eb.getMessage('ERR_RETURN_NUM_BLANK'));
            return false;
        } else if (formData.date == null || formData.date == "") {
            p_notification(false, eb.getMessage('ERR_RETURN_DATE_BLANK'));
            $('#returnDate').focus();
            return false;
        } else if (formData.customer == null || formData.customer == "") {
            p_notification(false, eb.getMessage('ERR_DELI_SELECT_CUST'));
            $('#customer').focus();
            return false;
        } else if (formData.customerID == null || formData.customerID == "") {
            p_notification(false, eb.getMessage('ERR_INVOICE_INVALID_CUST'));
            $('#customer').focus();
            return false;
        } else if (jQuery.isEmptyObject(returnProducts)) {
            if (tableId != "deliveryNoteProductTable") {
                p_notification(false, eb.getMessage('ERR_RETURN_ADD_ONEPROD'));
            }
            return false;
        }

        return true;
    }
 
    function validateDirectReturnForm(formData) {
        if (formData.location == null || formData.location == "") {
            p_notification(false, eb.getMessage('ERR_RETURN_SELECT_LOCAT'));
            $('#currentLocation').focus();
            return false;
        } else if (formData.returnCode == null || formData.returnCode == "") {
            p_notification(false, eb.getMessage('ERR_RETURN_NUM_BLANK'));
            return false;
        } else if (formData.date == null || formData.date == "") {
            p_notification(false, eb.getMessage('ERR_RETURN_DATE_BLANK'));
            $('#returnDate').focus();
            return false;
        } else if (formData.customer == null || formData.customer == "") {
            p_notification(false, eb.getMessage('ERR_DELI_SELECT_CUST'));
            $('#customer').focus();
            return false;
        } else if (jQuery.isEmptyObject(rtnProducts)) {
            p_notification(false, eb.getMessage('ERR_RETURN_ADD_ONEPROD'));
            return false;
        }

        return true;
    }

    function isDate(txtDate)
    {
        var currVal = txtDate;
        if (currVal == '')
            return false;
        //Declare Regex
        var rxDatePattern = /^(\d{4})(\/|-)(\d{1,2})(\/|-)(\d{1,2})$/;
        var dtArray = currVal.match(rxDatePattern); // is format OK?

        if (dtArray == null)
            return false;
        //Checks for mm/dd/yyyy format.
        dtMonth = dtArray[3];
        dtDay = dtArray[5];
        dtYear = dtArray[1];
        if (dtMonth < 1 || dtMonth > 12)
            return false;
        else if (dtDay < 1 || dtDay > 31)
            return false;
        else if ((dtMonth == 4 || dtMonth == 6 || dtMonth == 9 || dtMonth == 11) && dtDay == 31)
            return false;
        else if (dtMonth == 2)
        {
            var isleap = (dtYear % 4 == 0 && (dtYear % 100 != 0 || dtYear % 400 == 0));
            if (dtDay > 29 || (dtDay == 29 && !isleap))
                return false;
        }
        return true;
    }

    $('#multipleCopy').on('click',function(){
        if($(this).is(':checked')){
            $('#delNumberSelectDiv').removeClass('hidden');
            $('#mulDelCus').removeClass('hidden');
            $('#dimensionDiv').removeClass('hidden');
            $('#delSelectDiv').addClass('hidden');
            $('#dlnAddress').addClass('hidden');
            $('#dlnDate').addClass('hidden');
            $('#norCus').addClass('hidden');
            $('#customCurrencyId').val(8).trigger('change');
            if(!($('#deliveryNoteNumbers option[value=""]').length > 0)){
                $('#deliveryNoteNumbers').append('<option value="">Select Delivery Note Numbers</option>');
                $('#deliveryNoteNumbers').val('').selectpicker('refresh');
            }

            if(!($('#multipleDeleCustomer').val() && $('#customCurrencyId').val())){
                $('#deliveryNoteNumbers').attr('disabled',true);
            }
            multipleDlnCopy = true;
            p_notification('info', eb.getMessage('INFO_SELCT_CUSTNAME_AND_CURRENCY'));
        }else{
            multipleDlnCopy = false;
            $('#delNumberSelectDiv').addClass('hidden');
            $('#delSelectDiv').removeClass('hidden');
            $('#mulDelCus').addClass('hidden');
            $('#norCus').removeClass('hidden');
            $('#dimensionDiv').addClass('hidden');
            $('#dlnAddress').removeClass('hidden');
            $('#dlnDate').removeClass('hidden');
            if(!($('#deliveryNoteNo option[value=""]').length > 0)){
                $('#deliveryNoteNo').append('<option value="">Select Delivery Note Number</option>');
                $('#deliveryNoteNo').val('').selectpicker('refresh');
            }
        }
        $('#customCurrencyId').attr('disabled', false);
        clearReturnForm();
    });

    function enableMultiDeliNoteDropdown(){
        $('#deliveryNoteNumbers').attr('disabled', false);
        var custID = $('#multipleDeleCustomer').val();
        var customCurrencyId = $('#customCurrencyId').val();
        $('#deliveryNoteNumbers').find('option').remove();
        $('#deliveryNoteNumbers').removeData('AjaxBootstrapSelect');
        $('#deliveryNoteNumbers').removeData('selectpicker');
        $('#deliveryNoteNumbers').siblings('.bootstrap-select').remove();
        $('#deliveryNoteNumbers').siblings('.bootstrap-select').find('.dropdown-menu').remove();
        $('#deliveryNoteNumbers').selectpicker('refresh');
        loadDropDownFromDatabase('/delivery-note-api/search-active-delivery-note-for-dropdown?customerID='+ customerID +'&customCurrencyId='+customCurrencyId, "", 0, '#deliveryNoteNumbers');
    }

    function clearReturnForm() {
        resetReturnPage();
        $('#multipleDeleCustomer').find('option').remove();
        $('#multipleDeleCustomer').removeData('AjaxBootstrapSelect');
        $('#multipleDeleCustomer').removeData('selectpicker');
        $('#multipleDeleCustomer').siblings('.bootstrap-select').remove();
        $('#multipleDeleCustomer').siblings('.bootstrap-select').find('.dropdown-menu').remove();
        $('#multipleDeleCustomer').append('<option value="">Select Customer</option>');
        $('#multipleDeleCustomer').attr('disabled',false).val('');
        $('#multipleDeleCustomer').selectpicker('refresh');
        loadDropDownFromDatabase('/customerAPI/search-customers-for-dropdown', "", 0, '#multipleDeleCustomer');
    }

    function resetReturnPage() {
        //    clearProductScreen();
        checkSubProduct = {};
        returnProducts = {};
        productsTotal = {};
        productsTax = {};
        returnSubProducts = {};
        SubProductsQty = {};
        $('#add-new-item-row').find('tr').each(function() {
            if (!$(this).hasClass('add-row')) {
                $(this).remove();
            }
        });
        var $currentRow = getAddRow();
        $currentRow.removeClass('hidden');
        $('#subtotal').val('');
        $('#finaltotal').val('');
    }

    function getDeliveryDetails(DNIDS) {
        eb.ajax({
            type: 'POST',
            url: BASE_URL + '/invoice-api/getDeliveryNoteDetails',
            data: {
                deliveryNoteIDs: DNIDS,
                customCurrencyID: $('#customCurrencyId').val(),
                forReturn: true
            },
            success: function(respond) {
                if (respond.status) {
                    if (respond.data.inactiveItemFlag) {
                        p_notification(false, respond.data.errorMsg);
                        setTimeout(function(){ 
                            window.location.assign(BASE_URL + "/return")
                        }, 3000);
                        return false;
                    }
                    customerID = respond.data.deliveryNote.customerID;
                    var customer = respond.data.customer;
                    $('#customer').val(customer.customerName + '-' + customer.customerCode).attr('disabled', true);
                    Products = respond.data.locationProducts;
                    var deliverDate = "2018-05-28";
                    var params;
                    var format;
                    var dateformat = $('#deliveryNoteDate').data("date-format");
                    var slash = deliverDate.indexOf("/");
                    var dot = deliverDate.indexOf(".");
                    var dash = deliverDate.indexOf("-");
                    if (dot > 0) {
                        params = deliverDate.split(".");
                        format = dateformat.split(".");
                    }
                    if (slash > 0) {
                        params = deliverDate.split("/");
                        format = dateformat.split("/");
                    }
                    if (dash > 0) {
                        params = deliverDate.split("-");
                        format = dateformat.split("-");
                    }

                    var joindate;
                    if (format[0] == 'yyyy') {
                        joindate = new Date(params[0], params[1] - 1, params[2]);
                    }
                    else if (format[0] == 'dd') {
                        joindate = new Date(params[2], params[1] - 1, params[0]);
                    }
                    else if (format[0] == 'mm') {
                        joindate = new Date(params[2], params[0] - 1, params[1]);
                    }
                    //date formation end

                    $('#deliveryNoteDate').val(respond.data.deliveryNote.deliveryNoteDeliveryDate);
                    var nowTemp = new Date();
                    var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);
                    var dDate = new Date(Date.parse(joindate));
                    var newdDate = new Date(dDate.setDate(dDate.getDate()));
                    if ($('#returnDate').data('datepicker')) {
                        $('#returnDate').removeData('datepicker');
                    }
                    var returnDate = $('#returnDate').datepicker({
                        onRender: function(date) {
                            return ((date.valueOf() < newdDate.valueOf()) ? ((U_DATE_R_OVERRIDE)?'':'disabled') : '' || (date.valueOf() > now.valueOf()) ? ((U_DATE_R_OVERRIDE)?'':'disabled') : '');
                        }
                    }).on('changeDate', function(ev) {
                        returnDate.hide();
                    }).data('datepicker');
                    locationProducts = respond.data.deliveryNoteProduct;
                    $('#customCurrencyId').val(respond.data.deliveryNote.customCurrencyId).trigger('change').attr('disabled', true);
                    var rate = respond.data.deliveryNote.deliveryNoteCustomCurrencyRate;
                    customCurrencyRate = (rate != 0) ? rate : 1;

                    var checkProduct = 0;
                    var customCurrencyRate = parseFloat(respond.data.deliveryNote.deliveryNoteCustomCurrencyRate);
                    $.each(locationProducts, function(selectedProduct, value) {
                        var selectedProductID = selectedProduct.split("_")[1];
                        var productCode = value.productCode;
                        var productName = value.productName;
                        var availableQuantity = value.deliveryNoteProductQuantity;
                        var deliveryNoteproductID = value.deliveryNoteProductID;
                        var defaultSellingPrice = value.deliveryNoteProductPrice / customCurrencyRate;
                        var productDiscountType = value.deliveryNoteProductDiscountType;
                        var discount = (value.deliveryNoteProductDiscount) ? value.deliveryNoteProductDiscount : 0;
                        var productDiscount = (productDiscountType == 'value') ? discount / customCurrencyRate : discount;
                        productDiscount = cutDecimalPoint(productDiscount, 2);
                        var productType = value.productType;
                        var flag = false;
                        if (productType == 2) {
                            flag = true;
                        }
                        if (productType != 2 && availableQuantity != 0) {
                            flag = true;
                        }

                        if (flag) {
                            checkProduct = 1;
                            var $currentRow = getAddRow();
                            clearProductRow($currentRow);
                            $("input[name='itemCode']", $currentRow).val(productCode + ' - ' + productName).prop('readonly', true);
                            $("input[name='itemCode']", $currentRow).data('PT', productType);
                            $("input[name='itemCode']", $currentRow).data('PC', productCode);
                            $("input[name='itemCode']", $currentRow).data('PN', productName);
                            $("input[name='returnQuanity']", $currentRow).parent().addClass('input-group');
                            $("input[name='deliveredQuantity']", $currentRow).parent().addClass('input-group');
                            $("input[name='returnQuanity']", $currentRow).val(availableQuantity).prop('readonly', true).addUom(Products[selectedProductID].uom);
                            $("input[name='deliveredQuantity']", $currentRow).val(availableQuantity).prop('readonly', true).addUom(Products[selectedProductID].uom);
                            $("input[name='unitPrice']", $currentRow).val(defaultSellingPrice).prop('readonly', true).addUomPrice(Products[selectedProductID].uom);
                            $("input[name='discount']", $currentRow).val(productDiscount).prop('readonly', true);
                            if (productDiscountType != 'precentage') {
                                $("input[name='discount']", $currentRow).addClass('value');
                                if ($("input[name='discount']", $currentRow).hasClass('precentage')) {
                                    $("input[name='discount']", $currentRow).removeClass('precentage');
                                }
                            } else {
                                $("input[name='discount']", $currentRow).addClass('precentage');
                                if ($("input[name='discount']", $currentRow).hasClass('value')) {
                                    $("input[name='discount']", $currentRow).removeClass('value');
                                }
                                $(".discountSymbol", $currentRow).text('%');
                            }


                            $currentRow.data('id', selectedProduct);
                            $currentRow.data('dPID', deliveryNoteproductID);
                            // clear old rows
                            $("#batch_data tr:not(.hidden)").remove();
                            // check if any batch / serial products exist

                            if ($.isEmptyObject(value.subProduct)) {
                                $currentRow.removeClass('subproducts');
                                $('.edit-modal', $currentRow).parent().addClass('hidden');
                                $("td[colspan]", $currentRow).attr('colspan', 2);
                                $("input[name='returnQuanity']", $currentRow).prop('readonly', false).change().focus();
                            } else {
                                $currentRow.addClass('subproducts');
                                $('.edit-modal', $currentRow).parent().removeClass('hidden');
                                $("td[colspan]", $currentRow).attr('colspan', 1);
                                $("input[name='returnQuanity']", $currentRow).prop('readonly', true).change();
                            }
                            $currentRow.find('.delete').removeClass('disabled');
                            $currentRow.addClass('edit-row');
                            setTaxListForProduct(selectedProduct, $currentRow);
                            $currentRow.attr('id', 'product' + selectedProduct);
                            $("input[name='unitPrice']", $currentRow).trigger('focusout');
                            $currentRow
                                    .removeClass('add-row');
                            $($addRowSample.clone()).removeClass('sample hidden').appendTo($('#add-new-item-row', $productTable));
                        }
                    });
                    if (checkProduct == 0) {
                        p_notification(false, eb.getMessage('ERR_RETURN_DELINOTE'));
                    }
                    var $currentRow = getAddRow();
                    $currentRow.addClass('hidden');
                } else {
                    DeliveryNoteID = '';
                    p_notification(respond.status, respond.msg);
                    $('#deliveryNoteNo').val('');
                }
            }
        });
    }

     $('#item_code').on('change', function() {
        $parentRow = $(this).parents('tr');
        clearAddNewRow($parentRow);
        clearModalWindow();
        $('#addNewTax',$parentRow).attr('disabled', false);
        if ($(this).val() > 0 && $(this).val() != currentlySelectedProduct) {
            currentlySelectedProduct = $(this).val();
            selectedProduct = $(this).val();
            eb.ajax({
                type: 'POST',
                url: BASE_URL + '/productAPI/get-location-product-details',
                data: {productID: selectedProduct, locationID: locationID},
                success: function(respond) {
                    var currentElem = new Array();
                    currentElem[selectedProduct] = respond.data;
                    locationProducts = $.extend(locationProducts, currentElem);
                    $('#addNewTax',$parentRow).attr('disabled', false);
                    setTaxListForProductForDirectReturn(selectedProduct,$parentRow);
                    var baseUom;
                    for (var j in locationProducts[selectedProduct].uom) {
                        if (locationProducts[selectedProduct].uom[j].pUBase == 1) {
                            baseUom = locationProducts[selectedProduct].uom[j].uomID;
                        }
                    }
                    $("input[name='directUnitPrice']",$parentRow).val(locationProducts[selectedProduct].dSP).addUomPrice(locationProducts[selectedProduct].uom);
                    if (locationProducts[selectedProduct].pPDV != null && locationProducts[selectedProduct].dPEL == 1) {
                        $('#directDiscount',$parentRow).val(locationProducts[selectedProduct].pPDV).addUomPrice(locationProducts[selectedProduct].uom, baseUom);
                        $('#directDiscount',$parentRow).siblings('.uom-price-select').find('.uom_price_drop_down').attr('disabled', true);
                        $(".sign", $parentRow).text('Rs');
                        $('#directDiscount',$parentRow).attr('disabled', false);
                    } else if (locationProducts[selectedProduct].pPDP != null && locationProducts[selectedProduct].dPEL == 1) {
                        $('#directDiscount',$parentRow).val(locationProducts[selectedProduct].pPDP);
                        $('#directDiscount',$parentRow).attr('disabled', false);
                        $(".sign", $parentRow).text('%');
                    } else if ((locationProducts[selectedProduct].pPDP == null && locationProducts[selectedProduct].pPDV == null) || locationProducts[selectedProduct].dPEL == 0) {
                        $('#directDiscount',$parentRow).attr('disabled', true);
                    }
                    $('.uomPrice',$parentRow).attr("id", "itemUnitPrice");
                    $('#directReturnQuanity',$parentRow).parent().addClass('input-group');
                    $('#directReturnQuanity',$parentRow).addUom(locationProducts[selectedProduct].uom);
                    $('.uomqty',$parentRow).attr("id", "itemQuantity");
                    $('#item_code',$parentRow).data('PN', locationProducts[selectedProduct].pN);
                    $("#item_code",$parentRow).data('PT', locationProducts[selectedProduct].pT);
                    $("#item_code",$parentRow).data('PC', locationProducts[selectedProduct].pC);
                    $("#item_code",$parentRow).data('pId', selectedProduct);
                }
            });

        }
    });

    function clearAddNewRow($parentRow) {
        $('#directUnitPrice',$parentRow).val('').siblings('.uomPrice,.uom-price-select').remove();
        $("#directUnitPrice",$parentRow).show();
        $("#directReturnQuanity",$parentRow).val('').siblings('.uomqty,.uom-select').remove();
        $("#directReturnQuanity",$parentRow).show();
        $('#directDiscount',$parentRow).val('').siblings('.uomPrice,.uom-price-select').remove();
        $('#directDiscount',$parentRow).show();
        $('#addNewTotal',$parentRow).html('0.00');
        $('#drAddNewTotal',$parentRow).html('0.00');
        $('#taxApplied',$parentRow).removeClass('glyphicon glyphicon-checked');
        $('#taxApplied',$parentRow).addClass('glyphicon glyphicon-unchecked');
        $('.tempLi',$parentRow).remove();
        $('.uomLi',$parentRow).remove();
        $('#uomAb',$parentRow).html('');
        selectedProduct = '';
        selectedProductQuantity = '';
        selectedProductUom = '';
    }

    function clearModalWindow() {
        $('.tempProducts').remove();
        $('.cloneSerials').remove();
        $('#returnProductCodeBatch').html('');
        $('#returnProductQuantityBatch').html('');
        $('#returnProductCodeSerial').html('');
        $('#returnProductQuantitySerial').html('');
        $('#batchAddScreen').addClass('hidden');
        $('#serialAddScreen').addClass('hidden');
        $('#addNewSerial input[type=text]').val('');
        $('#proBatchAddNew').removeClass('hidden');
        batchCount = 0;
        directBatchProducts = {};
        directSerialProducts = {};

    }

    function setTaxListForProductForDirectReturn(productID, $parentRow) {
        if ((!jQuery.isEmptyObject(locationProducts[productID].tax))) {
            productTax = locationProducts[productID].tax;
            $('#taxApplied',$parentRow).removeClass('glyphicon glyphicon-unchecked');
            $('#taxApplied',$parentRow).addClass('glyphicon glyphicon-check');
            $('.tempLi',$parentRow).remove();
            for (var i in productTax) {
                var clonedLi = $($('#sampleLi',$parentRow).clone()).removeClass('hidden').attr('id', 'li_' + i).addClass("tempLi");
                clonedLi.children(".taxChecks").attr('id', productID + '_' + i).prop('checked', true).addClass('addNewTaxCheck');
                if (productTax[i].tS == 0) {
                    clonedLi.children(".taxChecks").attr('disabled', 'disabled');
                    clonedLi.children(".taxName").addClass('crossText');
                }
                clonedLi.children(".taxName").html('&nbsp&nbsp' + productTax[i].tN + '&nbsp&nbsp' + productTax[i].tP + '%').attr('for', productID + '_' + i);
                clonedLi.insertBefore($('#sampleLi',$parentRow));

            }
        } else {
            $('#taxApplied',$parentRow).addClass('glyphicon glyphicon-unchecked');
            $('#addNewTax',$parentRow).attr('disabled', 'disabled');
        }

    }


    $('#drRetrunProductTable').on('focusout', '#item_code,#directReturnQuanity,#directUnitPrice,#directDiscount,.uomqty,.uomPrice', function() {
        var checkedTaxes = Array();
        if (directReturnNoteType) {
            var row = $(this).parents('tr');
            $('input:checkbox.addNewTaxCheck', $(this).parents('tr')).each(function() {
                if (this.checked) {
                    var cliTID = this.id;
                    var tTaxID = cliTID.split('_')[1];
                    checkedTaxes.push(tTaxID);
                }
            });
            var productID = $('#item_code', $(this).parents('tr')).data('pId');
            if (productID) {
                productID = productID;
            } else {
                productID = row.attr('id').split("_");
                productID = productID[1];            
            }

            var qty = $("input[name='directReturnQuanity']", $(this).parents('tr')).val();
            var unitPrice = $("input[name='directUnitPrice']", $(this).parents('tr')).val();
            var uP = $(this).parents('tr').find('.uomPrice').val();
            if (unitPrice != "" && uP != "") {
                $(this).parents('tr').find("input[name='directUnitPrice']").val(parseFloat(unitPrice));
                // $(this).parents('tr').find('.uomPrice').val(parseFloat(unitPrice));
            }
            var discount = $("input[name='directDiscount']", $(this).parents('tr')).val();
            if ($('#item_code', row).data('PT') == 2 && qty == 0) {
                qty = 1;
            }
            var discountType;
            if (locationProducts[productID]) {
                if (locationProducts[productID].pPDP != null) {
                    discountType = "Per";
                } else if (locationProducts[productID].pPDV != null) {
                    discountType = "Val";
                } else {
                   discountType = "Non";
                }
                if (discountType == "Val") {
                    var newDiscount = validateDiscount(productID,discountType,discount, unitPrice);
                    var tmpItemCost = calculateTaxFreeItemCostByValueDiscount(unitPrice, qty, newDiscount);
                    if (tmpItemCost < 0) {
                        tmpItemCost = 0;
                    }
                } else if (discountType == "Per") {
                    var newDiscount = validateDiscount(productID,discountType,discount, unitPrice);
                    var tmpItemCost = calculateTaxFreeItemCost(unitPrice, qty, newDiscount);
                } else {
                    var newDiscount = 0;
                    var tmpItemCost = calculateTaxFreeItemCost(unitPrice, qty, newDiscount);
                }

                $("input[name='directDiscount']", $(this).parents('tr')).val(newDiscount);
                currentItemTaxResults = calculateItemCustomTax(tmpItemCost, checkedTaxes);
                currentTaxAmount = (currentItemTaxResults == null) ? null : currentItemTaxResults.tTA;
                var itemCost = toFloat(tmpItemCost) + toFloat(currentTaxAmount);
                $('#drAddNewTotal', $(this).parents('tr')).html(accounting.formatMoney(itemCost));
                if (productID != undefined) {
                    var currentEl = new Array();
                    currentEl[productID+'_'+unitPrice] = itemCost;
                    productLineTotal = $.extend(productLineTotal, currentEl);
                }
            } else {
                $("input[name='directDiscount']", $(this).parents('tr')).val(discount);
                var tmpItemCost = calculateTaxFreeItemCost(unitPrice, qty, discount);
                currentItemTaxResults = calculateItemCustomTax(tmpItemCost, checkedTaxes);
                currentTaxAmount = (currentItemTaxResults == null) ? null : currentItemTaxResults.tTA;
                var itemCost = toFloat(tmpItemCost) + toFloat(currentTaxAmount);
                $('#drAddNewTotal', $(this).parents('tr')).html(accounting.formatMoney(itemCost));
                if (productID != undefined) {
                    var currentEl = new Array();
                    currentEl[productID+'_'+unitPrice] = itemCost;
                    productLineTotal = $.extend(productLineTotal, currentEl);
                }
            }
        }
    });

    function validateDiscount(productID, discountType, currentDiscount, unitPrice) {
        var defaultProductData = locationProducts[productID];
        var newDiscount = 0;
        if (defaultProductData.dPEL == 1) {
            if (discountType == 'Val') {
                if (toFloat(defaultProductData.pPDV) !=0 && toFloat(currentDiscount) > toFloat(defaultProductData.pPDV)) {
                        newDiscount = toFloat(defaultProductData.pPDV);
                        p_notification(false, eb.getMessage('ERR_INVO_DISCOUNT_VALUE'));
                } else if (toFloat(defaultProductData.pPDV) == 0) {
                    if (toFloat(currentDiscount) > toFloat(unitPrice)) {
                        newDiscount = toFloat(unitPrice);
                    } else {
                        newDiscount = toFloat(currentDiscount);    
                    }
                } else {
                    newDiscount = toFloat(currentDiscount);
                }

            } else {
                if (toFloat(defaultProductData.pPDP) != 0 && toFloat(currentDiscount) > toFloat(defaultProductData.pPDP)) {
                        newDiscount = toFloat(defaultProductData.pPDP);
                        p_notification(false, eb.getMessage('ERR_INVO_DISC_PERCENTAGE'));
                } else if (toFloat(defaultProductData.pPDP) == 0) {
                    if (toFloat(currentDiscount) > 100 ) {
                        newDiscount = 100;
                        p_notification(false, eb.getMessage('ERR_PI_DISC_PERCENTAGE'));   
                    } else {
                        newDiscount = toFloat(currentDiscount);    
                    }
                } else {
                    newDiscount = toFloat(currentDiscount);
                }
            }
        } 
        return newDiscount;
    }

    $('#drRetrunProductTable').on('change', '.taxChecks.addNewTaxCheck', function() {
        var allchecked = false;
        var $parentTaxSet = $(this).parents("td.taxSet");
        $parentTaxSet.find('input:checkbox.addNewTaxCheck').each(function() {
            if (this.checked && $(this).prop('disabled') === false) {
                allchecked = true;
            }
        });

        if (allchecked == false) {
            $parentTaxSet.find('#taxApplied').removeClass('glyphicon glyphicon-check');
            $parentTaxSet.find('#taxApplied').addClass('glyphicon glyphicon-unchecked');
        } else {
            $parentTaxSet.find('#taxApplied').removeClass('glyphicon glyphicon-unchecked');
            $parentTaxSet.find('#taxApplied').addClass('glyphicon glyphicon-check');
        }

        $(this).parents('tr').find(".uomPrice").trigger(jQuery.Event("focusout"));
        $(this).parents('tr').find("input[name='directReturnQuanity']").trigger(jQuery.Event("focusout"));
    });

    $('#drRetrunProductTable').on('click', '#selectAll', function() {
        $(this).parents('.dropdown-menu').find('input:checkbox.addNewTaxCheck').each(function() {
            if (!this.checked) {
                this.checked = true;
            }
        });

        $(this).parents('.dropdown-menu').find('input:checkbox.addNewTaxCheck').trigger(jQuery.Event("change"));

    });

    $('#drRetrunProductTable').on('click', '#deselectAll', function() {
        $(this).parents('.dropdown-menu').find('input:checkbox.addNewTaxCheck').each(function() {
            if (this.checked) {
                this.checked = false;
            }
        });

        $(this).parents('.dropdown-menu').find('input:checkbox.addNewTaxCheck').trigger(jQuery.Event("change"));

    });

    $('#drRetrunProductTable').on('change', '.addNewUomR', function() {
        $('#uomAb').html(locationProducts[selectedProduct].uom[$(this).val()].uA);
        selectedProductUom = $(this).val();
    });


    $('#addItem').on('click', function() {
        var row = $(this).parents('tr');
        var uomqty = $("#directReturnQuanity").siblings('.uomqty').val();
        $("#directReturnQuanity").siblings('.uomqty').change();
        $('.tempy').remove();
        idNumber = 1;
        quon = 0;
        $('#prefix').val('');
        //validate same item usage
        var sameItemFlag = false;
        for (var i in rtnProducts) {
            if(selectedProduct == rtnProducts[i].pID && toFloat($('#directUnitPrice').val()) == toFloat(rtnProducts[i].pUnitPrice) 
                && toFloat($('#directDiscount').val()) == toFloat(products[i].pDiscount)){
                sameItemFlag = true;

            }
        }


        if(sameItemFlag){
            p_notification(false, eb.getMessage('ERR_SAME_ITEM_IN_PI'));
            return false;
        }

        var issDate = eb.convertDateFormat('#returnDate');
        var expD = $('#expire_date_autoFill').val('').datepicker().on('changeDate', function(ev) {
            expD.hide();
        }).data('datepicker');
        $('#quontity').val('');
        $('#startNumber').val('');
        $('#batch_code').val('');
        $('#wrtyDays').val('');
        $('#mf_date_autoFill').val('');
        var mfD = $('#mf_date_autoFill').datepicker({
            onRender: function(date) {
                return date.valueOf() > issDate.valueOf() ? 'disabled' : '';
            }
        }).on('changeDate', function(ev) {
            mfD.hide();
        }).data('datepicker');
        $('#temp_1').find('input[name=submit]').attr('disabled', false);
        $('#temp_1').find('input[name=prefix]').attr('disabled', false);
        $('#temp_1').find('input[name=startNumber]').attr('disabled', false);
        $('#temp_1').find('input[name=quontity]').attr('disabled', false);
        $('#temp_1').find('input[name=expireDateAutoFill]').attr('disabled', false);
        $('#temp_1').find('input[name=batch_code]').attr('disabled', false);
        $('#temp_1').find('input[name=mfDateAutoFill]').attr('disabled', false);
        $('#temp_1').find('input[name=wrtyDays]').attr('disabled', false);

        selectedProductUom = $("#directReturnQuanity").siblings('.uom-select').children('button').find('.selected').data('uomID');
        if (typeof (selectedProduct) == 'undefined' || selectedProduct == '') {
            p_notification(false, eb.getMessage('ERR_GRN_SELECT_PROD'));
        }
        else if (typeof (selectedProductUom) == 'undefined' || selectedProductUom == '') {
            p_notification(false, eb.getMessage('ERR_GRN_SELECT_UOM'));
        }
        else {
            var checkingPrice = toFloat($('#directUnitPrice').val());
            if ($('#directUnitPrice').val() == '') {
                p_notification(false, eb.getMessage('ERR_GRN_ENTER_UNITPRI'));
                $('#directUnitPrice').focus();
            } else if ($('#item_code').data('PT') != 2 && $('#directReturnQuanity').val() == '') {
                p_notification(false, eb.getMessage('ERR_GRN_ENTER_QUANTITY'));
            } else if (((uomqty == 0) || (uomqty == '')) && $('#item_code').data('PT') != 2) {
                p_notification(false, eb.getMessage('ERR_GRN_ENTER_QUANTITY'));
            } else {
                clearModalWindow();
                $('.cloneSerials').remove();
                $('#serialSample').children().children('#newSerialNumber').removeClass('serialNumberList');
                $('#serialSample').children().children('#newSerialBatch').removeClass('serialBatchList');
                $('#serialSample').children().children('#newSerialBatch').prop('disabled', false);
                if ($('#itemCode').data('PT') == 2) {
                    locationProducts[selectedProduct].bP = 0;
                    locationProducts[selectedProduct].sP = 0;
                }
                if (locationProducts[selectedProduct].bP == 1) {
                    $('#batchAddScreen').removeClass('hidden');
                    $('#newBatchMDate').addClass('white_bg');
                    $('#newBatchEDate').addClass('white_bg');
                }
                if (locationProducts[selectedProduct].sP == 1) {
                    $('#serialAddScreen').removeClass('hidden');
                }
                if (locationProducts[selectedProduct].sP == 1 && locationProducts[selectedProduct].bP == 0) {
                    $('#temp_1').find('input[name=mfDateAutoFill]').attr('disabled', true).removeClass('white_bg');
                    $('#temp_1').find('input[name=batch_code]').closest('td').hide();
                    $('#btch_code').hide();
                    $('#bCodeT').hide();
                    $('#bCode').hide();
                    $('.price_field').hide();
                    $('#temp_1').find('input[name=expireDateAutoFill]').addClass('white_bg');
                } else {
                    $('#temp_1').find('input[name=batch_code]').closest('td').show();
                    $('#btch_code').show();
                    $('.price_field').show();
                    $('#bCodeT').show();
                    $('#bCode').show();
                }

                if (locationProducts[selectedProduct].sP == 1 && locationProducts[selectedProduct].bP == 1) {
                    $('#batchAddScreen').addClass('hidden');
                    $('#temp_1').find('input[name=batch_code]').attr('disabled', false);
                    $('#temp_1').find('input[name=mfDateAutoFill]').attr('disabled', false).addClass('white_bg');
                }

                if (locationProducts[selectedProduct].bP == 0 && locationProducts[selectedProduct].sP == 0) {
                    $('#directUnitPrice', row).trigger('focusout');
                    addNewProductRow({}, {});
                    $('#directReturnQuanity').parent().removeClass('input-group');
                    $('#item_code').focus();
                } else {
                    selectedProductQuantity = $('#directReturnQuanity').siblings('.uomqty').val();
                    if (locationProducts[selectedProduct].bP == 1) {
                        $('#returnProductCodeBatch').html(locationProducts[selectedProduct].pC);
                        $('#returnProductQuantityBatch').html(uomqty);
                    }
                    if (locationProducts[selectedProduct].sP == 1) {
                        $('#addNewSerial input[type=text]').val('');
                        $('#returnProductCodeSerial').html(locationProducts[selectedProduct].pC);
                        $('#returnProductQuantitySerial').html($('#directReturnQuanity').val());
                        $('#serialSample').children().children('#newSerialBatch').addClass('serialBatchList');
                        $('#serialSample').children().children('#newSerialNumber').addClass('serialNumberList');

                        if (locationProducts[selectedProduct].bP == 0) {
                            $('#serialSample').children().children('#newSerialBatch').prop('disabled', true);
                        }
                        for (i = 0; i < selectedProductQuantity - 1; i++) {
                            var serialAddRowClone = $('#serialSample').clone().attr('id', i).addClass('cloneSerials');
                            $('#serialSample').children().children('#newSerialNumber').addClass('serialNumberList');
                            $('#serialSample').children().children('#newSerialBatch').addClass('serialBatchList');
                            serialAddRowClone.children().children('#newSerialNumber').addClass('serialNumberList');
                            serialAddRowClone.children().children('#newSerialBatch').addClass('serialBatchList');
                            serialAddRowClone.children().children('#newSerialEDate').attr('id', 'sEDate' + i).addClass('sEDate');
                            if (locationProducts[selectedProduct].bP == 0) {
                                $('#serialSample').children().children('#newSerialBatch').prop('disabled', true);
                                serialAddRowClone.children().children('#newSerialBatch').prop('disabled', true);
                            }
                            serialAddRowClone.insertBefore('#serialSample');
                        }
                        if(selectedProductQuantity<=100){
                            var dDate = new Date(Date.parse($('#returnDate').val()));
                            $('.sEDate').datepicker();
                        }
                    }
                    $('#addDirectReturnProductsModal').modal('show');

                }
            }

        }
    });

    function addNewProductRow(directBatchProducts, directSerialProducts) {
        var $parentTr = '';
        $parentTr = $('#item_code').parents('tr');
        var productID = selectedProduct;
        var iC = $('#item_code').data('PC');
        var pT = $('#item_code').data('PT');
        var iN = $('#item_code').data('PN');
        var uPrice = accounting.formatMoney($('#directUnitPrice').val(), null, getDecimalPlaces($('#directUnitPrice').val()));
        var uFPrice = $('#directUnitPrice').val();
        var qt = $('#directReturnQuanity').val();
        if (qt == 0) {
            qt = 0;
        }
        var uomqty = $("#directReturnQuanity").siblings('.uomqty').val();
        var uRPrice = $("#directUnitPrice").val();
        var uomPrice = $("#directUnitPrice").siblings('.uomPrice').val();
        var nTotal = productLineTotal[locationProducts[selectedProduct].pID+'_'+uRPrice];
        var calNTotal = accounting.unformat(nTotal);
        var gD = $('#directDiscount').val();
        if ($('#directDiscount').val() == '')
            gD = 0;
        var locationProductID = locationProducts[productID].lPID;
        selectedProductQuantity = $('#directReturnQuanity').val();
        $("input[name=directDiscount]").trigger(jQuery.Event("focusout"));
        var newTrID = 'tr_' + locationProductID;
        var clonedRow = $($('#preSetSampleForDirectReturn').clone()).attr('id', newTrID).addClass('addedProducts').attr('data-id', locationProductID+'_'+uPrice);;
        clonedRow.children('#code').html(iC + ' - ' + iN);
        clonedRow.children('#quantity').html(uomqty);
        clonedRow.children('#unit').html(uomPrice);

        clonedRow.children().children('#uomName').html(locationProducts[productID].uom[selectedProductUom].uA);

        clonedRow.children('#disc').html(gD);
        clonedRow.children('#ttl').html(accounting.formatMoney(nTotal));

        if (locationProducts[productID].bP == 0 && locationProducts[productID].sP == 0) {
            clonedRow.children().children('#viewSubProducts').addClass('hidden');
        }
        var clonedTax = $($('#addNewTaxDivForDirectReturn').clone()).attr('id', '');
        clonedTax.children('#addNewTax').children('#taxApplied').attr('id', '');
        clonedTax.children('#addNewTax').attr('id', '');
        clonedTax.children('#addTaxUl').children().removeClass('tempLi');
        clonedTax.children('#addTaxUl').children().children().removeClass('addNewTaxCheck');
        clonedTax.children('#addTaxUl').children().children().attr('disabled', true);
        clonedTax.children('#addTaxUl').children('#sampleLi').remove();
        clonedTax.children('#addTaxUl').attr('id', '');

        clonedTax.find('#toggleSelection').addClass('hidden');
        clonedRow.children('#tax').html(clonedTax);
        clonedRow.removeClass('hidden');
        clonedRow.insertBefore('#preSetSampleForDirectReturn');
        var newProduct = locationProducts[productID];
        
        var discountType;
        if (locationProducts[productID].pPDP != null) {
            discountType = "precentage";
        } else if (locationProducts[productID].pPDV != null) {
            discountType = "value";
        } else {
           discountType = "Non";
        }
        rtnProducts.push(new rtnProduct(newProduct.lPID, productID, iC, iN, qt, gD, toFloat(uFPrice), selectedProductUom, calNTotal, currentItemTaxResults, directBatchProducts, directSerialProducts, pT, true, null,null ,discountType));

        setDirectReturnTotalCost();
        setTotalTax();
        clearAddNewRow($parentTr);
        $('#item_code').val(0).trigger('change').empty().selectpicker('refresh');
        clearModalWindow();
    }

    function rtnProduct(lPID, pID, pCode, pN, pQ, pD, pUP, pUom, pTotal, pTax, bProducts, sProducts, productType, stockUpdate, productindexID, returnSubProID = null, discountType) {
        this.locationPID = lPID;
        this.pID = pID;
        this.pCode = pCode;
        this.pName = pN;
        this.pQuantity = pQ;
        this.pDiscount = pD;
        this.pUnitPrice = pUP;
        this.pUom = pUom;
        this.pTotal = pTotal;
        this.pTax = pTax;
        this.bProducts = bProducts;
        this.sProducts = sProducts;
        this.productType = productType;
        this.stockUpdate = stockUpdate;
        this.productindexID = productindexID;
        this.returnSubProID = returnSubProID;
        this.discountType = discountType;
    }

    function rtnBatchProduct(bCode, bQty, mDate, eDate, warnty, bID) {
        this.bCode = bCode;
        this.bQty = bQty;
        this.mDate = mDate;
        this.eDate = eDate;
        this.warnty = warnty;
        this.bID = bID;
    }

    function rtnSerialProduct(sCode, sWarranty, sBCode, sEdate, sID,sWarrantyType) {
        this.sCode = sCode;
        this.sWarranty = sWarranty;
        this.sBCode = sBCode;
        this.sEdate = sEdate;
        this.sID = sID;
        this.sWarrantyType = sWarrantyType;
    }

    function setDirectReturnTotalCost() {
        var returnTotal = 0;
        for (var i in rtnProducts) {
            returnTotal += toFloat(rtnProducts[i].pTotal);
        }
        $('#drSubtotal').html(accounting.formatMoney(returnTotal));
        $('#drFinaltotal').html(accounting.formatMoney(returnTotal));
    }

    function setTotalTax() {
        totalTaxList = {};
        for (var k in rtnProducts) {
            if (rtnProducts[k].pTax != null) {
                for (var l in rtnProducts[k].pTax.tL) {
                    if (totalTaxList.hasOwnProperty(l)) {
                        var curTA = totalTaxList[l].tA;
                        var newTA = (toFloat(curTA) + toFloat(rtnProducts[k].pTax.tL[l].tA));
                        totalTaxList[l].tA = newTA.toFixed(2);
                    } else {
                        totalTaxList[l] = {tN: rtnProducts[k].pTax.tL[l].tN, tP: rtnProducts[k].pTax.tL[l].tP, tA: rtnProducts[k].pTax.tL[l].tA};
                    }
                }
            }
        }

        var totalTaxHtml = "";
        for (var t in totalTaxList) {
            totalTaxHtml += totalTaxList[t].tN + "(" + totalTaxList[t].tP + "%)&nbsp" + accounting.formatMoney((totalTaxList[t].tA)) + "<br>";
        }
        $('#totalTaxShow').html(totalTaxHtml);
    }

    $(document).on('keyup', '#newBatchQty,#newBatchWarrenty', function() {
        if ((this.id == 'newBatchQty') && ((!$.isNumeric($('#newBatchQty').val()) || $('#newBatchQty').val() <= -1))) {
            $('#newBatchQty').val('');
        }
        if ((this.id == 'newBatchWarrenty') && ((!$.isNumeric($('#newBatchWarrenty').val()) || $('#newBatchWarrenty').val() < 0))) {
            $('#newBatchWarrenty').val('');
        }
    });
    $(document).on('keyup', '.serialWarrenty', function() {
        if (!$.isNumeric($(this).val()) || $(this).val() < 0) {
            $(this).val('');
        }
    });
    
    $('#addBatchItem').on('click', function() {
        if ($('#newBatchNumber').val() == null || $('#newBatchQty').val() == '' || $('#newBatchNumber').val().trim() === '') {
            p_notification(false, eb.getMessage('ERR_GRN_BATCH_INFO'));
        } else {
            var newBCode = $('#newBatchNumber').val();
            var newBQty = $('#newBatchQty').val();
            var newBMDate = $('#newBatchMDate').val();
            var newBEDate = $('#newBatchEDate').val();
            var newBWrnty = $('#newBatchWarrenty').val();
            if ((toFloat(batchCount) + toFloat(newBQty)) > selectedProductQuantity) {
                p_notification(false, eb.getMessage('ERR_GRN_BATCH_COUNT'));
            } else {
                if (directBatchProducts[newBCode] != null) {
                    p_notification(false, eb.getMessage('ERR_GRN_BATCH_CODE'));
                } else {
                    if ((toFloat(batchCount) + toFloat(newBQty)) == selectedProductQuantity) {
                        $('#proBatchAddNew').addClass('hidden');
                    }
                    batchCount += toFloat(newBQty);
                    directBatchProducts[newBCode] = new rtnBatchProduct(newBCode, newBQty, newBMDate, newBEDate, newBWrnty, null);
                    var cloneBatchAdd = $($('#proBatchSample').clone()).attr('id', newBCode).removeClass('hidden').addClass('tempProducts');
                    cloneBatchAdd.children('#batchNmbr').html(newBCode);
                    cloneBatchAdd.children('#batchQty').html(newBQty);
                    cloneBatchAdd.children('#batchMDate').html(newBMDate);
                    cloneBatchAdd.children('#batchEDate').html(newBEDate);
                    cloneBatchAdd.children('#batchW').html(newBWrnty);
                    cloneBatchAdd.insertBefore('#proBatchSample');
                    clearAddBatchProductRow();
                }
            }
        }
    });

    $('#newBatchNumber').on('click', function(){
        $(this).parents('tr').find('#newBatchMDate').val('');
        $(this).parents('tr').find('#newBatchEDate').val('');
    });

    $('#newBatchNumber').typeahead({
        ajax: {
            url: BASE_URL + '/api/grn/get-batch-numbers-for-typeahead',
            timeout: 250,
            triggerLength: 1,
            method: "POST",
            preProcess: function(respond) {
                if (!respond.status) {
                    return false;
                }
                return respond.data;
            }
        }
    });

    $('#newBatchQty, #newBatchMDate, #newBatchEDate, #newBatchWarrenty').on('click',function(){
        var addedBatchCode = $(this).parents('tr').find('#newBatchNumber').val();
        var selectedRow = $(this).parents('tr');

        if(!($(this).parents('tr').find('#newBatchMDate').val() && $(this).parents('tr').find('#newBatchEDate').val())){
            eb.ajax({
            type: 'POST',
            url: BASE_URL + '/api/grn/get-man-date-and-exp-date-for-batch-item',
            data: {batchCode : addedBatchCode },
                success: function(respond) {
                    if (respond.status == true) {
                        if(!$('#newBatchMDate', selectedRow).val()){
                            $('#newBatchMDate', selectedRow).val(respond.data.manDate);
                        }
                        if(!$('#newBatchEDate', selectedRow).val()){
                            $('#newBatchEDate', selectedRow).val(respond.data.expDate);
                        }
                    }
                },

            });
        }

    });

    $('#addDirectReturnProductsModal').on('change', 'input.serialNumberList', function() {
        var matchSerialNumberCount = 0;
        var typedVal = $(this).val();
        var serialCheckStatus = false;
        eb.ajax({
            type: 'POST',
            url: BASE_URL + '/productAPI/checkSerialProductCodeExists',
            data: {serialCode: typedVal, productCode: locationProducts[selectedProduct].pC, },
            success: function(respond) {
                if (respond.data == true) {
                    serialCheckStatus = true;
                }
            },
            async: false
        });

        if (serialCheckStatus == true) {
            p_notification(false, eb.getMessage('ERR_GRN_SERIAL_CODE'));
            $(this).val('');
        } else {
            $('input.serialNumberList').each(function() {
                if ($(this).val() == typedVal) {
                    matchSerialNumberCount++;
                }
            });
            if (matchSerialNumberCount > 1) {
                p_notification(false, eb.getMessage('ERR_GRN_SAME_SERIAL_CODE'));
                $(this).val('');
            }
        }
    });

    $('#addDirectReturnProductsModal').on('change', 'input.serialBatchList,#newSerialBatch', function() {
        if (!directBatchProducts.hasOwnProperty($(this).val())) {
            p_notification(false, eb.getMessage('ERR_GRN_ENTER_BATCHNUM'));
            $(this).val('');
        } else {
            var enteredBatchCode = $(this).val();
            var sameBatchCodeCount = 0;
            $('input.serialBatchList,#newSerialBatch').each(function() {
                if ($(this).val() == enteredBatchCode) {
                    sameBatchCodeCount++;
                }
            });
            if (sameBatchCodeCount > directBatchProducts[enteredBatchCode].bQty) {
                p_notification(false, eb.getMessage('ERR_GRN_ENTER_PRODLIMIT'));
                $(this).val('');
            }
        }
    });

    $('#saveDirectReturnProducts').on('click', function(event) {
        var serialEmpty = false;
        $('input.serialNumberList').each(function() {
            if ($(this).val() == '') {
                serialEmpty = true;
                return false;
            }
        });
        if (serialEmpty == true) {
            p_notification(false, eb.getMessage('ERR_GRN_FILL_SERIALNUM'));
            return false;
        }

        var batchEmpty = false;
        if (locationProducts[selectedProduct].bP == 1 && locationProducts[selectedProduct].sP == 0) {
            if (batchCount < selectedProductQuantity) {
                batchEmpty = true;
            }
        }
        if (batchEmpty == true) {
            p_notification(false, eb.getMessage('ERR_GRN_ADD_BATCH_DETAILS'));
            return false;
        }
        var serialBatchEmpty = false;
        if ((locationProducts[selectedProduct].bP == 1) && (locationProducts[selectedProduct].sP == 1)) {
            $('input.serialBatchList').each(function() {
                if ($(this).val() == '') {
                    serialBatchEmpty = true;
                    return false;
                }
            });
        }
        if (serialBatchEmpty == true) {
            p_notification(false, eb.getMessage('ERR_INVENTADJUS_FILLBATCHNUM'));
            return false;
        }
        if (serialEmpty == false && batchEmpty == false) {
            $('#addNewSerial > tr').each(function() {
                var sNmbr = $(this).children().children('#newSerialNumber').val();
                var sWrnty = $(this).children().children('#newSerialWarranty').val();
                var sBt = $(this).children().children('#newSerialBatch').val();
                var sED = $(this).children().children('.serialEDate').val();
                var sWrntyType = $(this).children().children('#newSerialWarrantyType').val();

                if (sNmbr != '') {
                    directSerialProducts[sNmbr] = new rtnSerialProduct(sNmbr, sWrnty, sBt, sED, null,sWrntyType);
                }
            });
            $('#addDirectReturnProductsModal').modal('hide');
            addNewProductRow(directBatchProducts, directSerialProducts, '');
            clearModalWindow();
            $('#itemCode').focus();
        } else {
            p_notification(false, eb.getMessage('ERR_GRN_BATCH_PRODDETAILS'));
        }

    });

    $('#addDirectReturnProductsModal').on('click', '.batchDelete', function() {
        var deleteBID = $(this).closest('tr').attr('id');
        var deleteBQty = directBatchProducts[deleteBID].bQty;
        batchCount -= toFloat(deleteBQty);
        delete(directBatchProducts[deleteBID]);
        $('#' + deleteBID).remove();
        $('#proBatchAddNew').removeClass('hidden');
    });

    function clone() {
        var sendStartingValue = quon;
        var originalQuontity = $('#directReturnQuanity').val();
        var poQuontity = $('.poAddItem').parents("tr").find("input[name='directReturnQuanity']").val();
        if (originalQuontity == "") {
            originalQuontity = poQuontity;
        }
        id = '#temp_' + idNumber;
        var quonnn = $(id).find('input[name=quontity]').val();
        var number = $(id).find('input[name=startNumber]').val();
        var startNumberStr = $(id).find('input[name=startNumber]').val();
        var prefix = $(id).find('input[name=prefix]').val();
        var pCode = locationProducts[selectedProduct].pC;
        if (quonnn == '') {
            quonnn = 0;
        }
        if (number == '') {
            p_notification(false, eb.getMessage('NO_START_NO'));
            return false;
        }
        if (number == 0) {
            p_notification(false, eb.getMessage('START_NO_ZERO'));
            return false;
        }
        var validationValue = true;
        validationValue = validation(number, prefix, quonnn, startNumberStr, pCode, sendStartingValue, id);
        if (validationValue == true) {

            if (toFloat(quonnn) <= originalQuontity && toFloat(quon) + toFloat(quonnn) <= originalQuontity && quonnn !== 0) {
                quon = toFloat(quon) + toFloat(quonnn);
                if (toFloat(quonnn) == originalQuontity || toFloat(quon) == originalQuontity) {
                    $(id).find('input[name=submit]').attr('disabled', true);
                    $(id).find('input[name=prefix]').attr('disabled', true);
                    $(id).find('input[name=startNumber]').attr('disabled', true);
                    $(id).find('input[name=quontity]').attr('disabled', true);
                    $(id).find('input[name=expireDateAutoFill]').attr('disabled', true);
                    $(id).find('input[name=batch_code]').attr('disabled', true);
                    $(id).find('input[name=mfDateAutoFill]').attr('disabled', true);
                    $(id).find('input[name=wrtyDays]').attr('disabled', true);
                    setValue(sendStartingValue, quonnn, id);
                }
                else {
                    var row = document.getElementById('temp_' + idNumber);
                    var table = document.getElementById('tempTbody');
                    var clone = row.cloneNode(true);
                    idNumber += 1;
                    clone.id = 'temp_' + idNumber;
                    table.appendChild(clone);
                    nextId = '#temp_' + idNumber;
                    $(nextId).addClass('tempy');
                    $(nextId).find('input[name=prefix]').val('');
                    $(nextId).find('input[name=startNumber]').val('');
                    $(nextId).find('input[name=quontity]').val('');
                    $(nextId).find('input[name=wrtyDays]').val('');
                    var issuDate = new Date(Date.parse($('#issueDate').val()));
                    $(nextId).find('input[name=expireDateAutoFill]').val('');
                    $(nextId).find('input[name=batch_code]').val('');
                    $(nextId).find('input[name=mfDateAutoFill]').val('');
                    var cloneMfD = $(nextId).find('input[name=mfDateAutoFill]').datepicker({
                        onRender: function(date) {
                            return date.valueOf() > issuDate.valueOf() ? 'disabled' : '';
                        }
                    }).on('changeDate', function(ev) {
                        cloneMfD.hide();
                    }).data('datepicker');
                    $(id).find('input[name=submit]').attr('disabled', true);
                    $(id).find('input[name=prefix]').attr('disabled', true);
                    $(id).find('input[name=startNumber]').attr('disabled', true);
                    $(id).find('input[name=quontity]').attr('disabled', true);
                    $(id).find('input[name=expireDateAutoFill]').attr('disabled', true);
                    $(id).find('input[name=batch_code]').attr('disabled', true);
                    $(id).find('input[name=mfDateAutoFill]').attr('disabled', true);
                    $(id).find('input[name=wrtyDays]').attr('disabled', true);
                    if (locationProducts[selectedProduct].sP == 1 && locationProducts[selectedProduct].bP == 1) {
                        $(nextId).find('input[name=mfDateAutoFill]').attr('disabled', false);
                        $(nextId).find('input[name=batch_code]').attr('disabled', false);
                    }
                    var cloneExpD = $(nextId).find('input[name=expireDateAutoFill]').datepicker().on('changeDate', function(ev) {
                        cloneExpD.hide();
                    }).data('datepicker');

                    setValue(sendStartingValue, quonnn, id);
                }
            }
            else {
                p_notification(false, eb.getMessage('WRNG_QUON_PI'));
                return false;
            }
        }
    }

    $('#tempTbody').on('click', '#submit_serial_number', function()
    {
        if ((locationProducts[selectedProduct].bP == 1) && (locationProducts[selectedProduct].sP == 1)) {
            if (!$(this).parents("tr").find("input[name='batch_code']").val()) {
                p_notification(false, eb.getMessage('ERR_NO_GRN_ADD_SERIAL_BATCH_DETAILS'));
                return false;
            }
        }

        if(isNaN($(this).parents('tr').find("input[name='startNumber']").val())){
            p_notification(false, eb.getMessage('ERR_SERIAL_START_NO'));
            $(this).parents('tr').find("input[name='startNumber']").val("");
            return false;
        }


        clone();
        if ($('#submit_serial_number').attr('disabled')) {
            $(this).parent().parent().find('#expire_date_autoFill').removeClass('white_bg');
            $(this).parent().parent().find('#mf_date_autoFill').removeClass('white_bg');
        }
    });
    function validation(startNumber, prefix, bQuntity, startNumberStr, productCode, sendStartingValue, id) {
        var strNumberLength = startNumberStr.length;
        var matchSerialNumberCount = 0;
        var serialCheckStatus = false;
        eb.ajax({
            type: 'POST',
            url: BASE_URL + '/productAPI/checkSerialProductCode',
            data: {startingNumber: startNumber, prefix: prefix, serialQuntity: bQuntity, strNumberLength: strNumberLength, productCode: productCode, },
            success: function(respond) {
                if (respond.data == true) {
                    serialCheckStatus = true;
                }
            },
            async: false
        });
        if (serialCheckStatus == true) {
            p_notification(false, eb.getMessage('ERR_GRN_SERIAL_CODE'));
            return false;
        }
        else {
            $('input.serialNumberList').each(function() {
                for (i = startNumber; i < (parseInt(startNumber) + parseInt(bQuntity)); i++) {
                    if (prefix) {
                        var startingValue = prefix + pad(i, strNumberLength);

                        if ($(this).val() == startingValue) {
                            matchSerialNumberCount++;
                        }
                    } else {
                        var startingValue = pad(i, strNumberLength);
                        if ($(this).val() == startingValue) {
                            matchSerialNumberCount++;
                        }
                    }

                }

            });
            if (matchSerialNumberCount >= 1) {
                p_notification(false, eb.getMessage('ERR_GRN_SAME_SERIAL_CODE'));
                return false;
            }
            else {
                return true;
            }
        }
    }


    function setValue(quon, quonnn, id) {
        var abc = 0;
        var bQuntity = toFloat(quonnn);
        var max = toFloat(quon) + toFloat(quonnn);
        var prefix = $(id).find('input[name=prefix]').val();
        var startNumber = parseInt($(id).find('input[name=startNumber]').val());
        var startNumberString = $(id).find('input[name=startNumber]').val();
        var expire_date = $(id).find('input[name=expireDateAutoFill]').val();
        var mf_date = $(id).find('input[name=mfDateAutoFill]').val();
        var wrnty = $(id).find('input[name=wrtyDays]').val();
        var wrntyType = $(id).find('#wrtyType').val();
        if (locationProducts[selectedProduct].sP == 1 && locationProducts[selectedProduct].bP == 1) {
            var batchNumber = $(id).find('input[name=batch_code]').val();
            directBatchProducts[batchNumber] = new rtnBatchProduct(batchNumber, bQuntity, mf_date, expire_date, wrnty, null);

            if (startNumber) {
                $('#addNewSerial tr').each(function() {
                    if (abc >= toFloat(quon) && abc < toFloat(max)) {
                        var numberLength = startNumberString.length;
                        var startingValue = pad(startNumber, numberLength);
                        if (prefix == '') {
                            $(this).find('input[name=serialCode]:first').val(startingValue);
                            $(this).find('input[name=serialEDate]').val(expire_date);
                            $(this).find('input[name=batchCode]').val(batchNumber);
                            $(this).find('input[name=serialWarronty]').val(wrnty);
                            $(this).find("#newSerialWarrantyType").val(wrntyType).change();

                        } else if (prefix != '') {
                            var num = prefix + startingValue;
                            $(this).find('input[name=serialCode]:first').val(num);
                            $(this).find('input[name=serialEDate]').val(expire_date);
                            $(this).find('input[name=batchCode]').val(batchNumber);
                            $(this).find('input[name=serialWarronty]').val(wrnty);
                            $(this).find("#newSerialWarrantyType").val(wrntyType).change();
                        }
                        startNumber += 1;
                    }
                    abc++;
                });
            }
        }
        else {
            if (startNumber) {
                $('#addNewSerial tr').each(function() {
                    if (abc >= toFloat(quon) && abc < toFloat(max)) {
                        var numberLength = startNumberString.length;
                        var startingValue = pad(startNumber, numberLength);
                        if (prefix == '') {
                            $(this).find('input[name=serialCode]:first').val(startingValue);
                            $(this).find('input[name=serialEDate]').val(expire_date);
                            $(this).find('input[name=serialWarronty]').val(wrnty);
                            $(this).find("#newSerialWarrantyType").val(wrntyType).change();
                        } else if (prefix != '') {
                            var num = prefix + startingValue;
                            $(this).find('input[name=serialCode]:first').val(num);
                            $(this).find('input[name=serialEDate]').val(expire_date);
                            $(this).find('input[name=serialWarronty]').val(wrnty);
                            $(this).find("#newSerialWarrantyType").val(wrntyType).change();
                        }
                        startNumber += 1;
                    }
                    abc++;
                });
            }
        }
    }

    function pad(n, width, z) {
        z = z || '0';
        n = n + '';
        return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
    }

    function clearAddBatchProductRow() {
        $('#newBatchNumber').val('');
        $('#newBatchQty').val('');
        $('#newBatchMDate').val('');
        $('#newBatchEDate').val('');
        $('#newBatchWarrenty').val('');
    }

    $('#drRetrunProductTable').on('click', '.returnDeleteProduct', function() {
        var deletePTrID = $(this).closest('tr').attr('id');
        var uniqID2 = $(this).closest('tr').attr('data-id');
        var deletePID = deletePTrID.split("_");
        deletePID = deletePID[1];
        var deleteItemPrice = accounting.unformat($(this).closest('tr').find('#unit').html());
        var deleteProductIndex = rtnProducts.indexOf(_.findWhere(rtnProducts, {'locationPID': deletePID, 'pUnitPrice': deleteItemPrice}));
        bootbox.confirm('Are you sure you want to remove this product ?', function(result) {
            if (result == true) {
                rtnProducts.splice(deleteProductIndex, 1);
                $('#form_row tr').each(function(){
                    if($(this).attr('data-id') == uniqID2){
                        $(this).remove();
                    }
                });
                setDirectReturnTotalCost();
                setTotalTax();
            }
        });
    });

    $('#drRetrunProductTable').on('click', '.returnViewProducts', function() {
        var viewPTrID = $(this).closest('tr').attr('id');
        var viewPID = viewPTrID.split('tr_')[1].trim();
        var viewItemPrice = accounting.unformat($(this).closest('tr').find('#unit').html());
        var viewProductIndex = rtnProducts.indexOf(_.findWhere(rtnProducts, {'locationPID': viewPID, 'pUnitPrice': viewItemPrice}));
        var vPID = rtnProducts[viewProductIndex].pID;

        if (locationProducts[vPID].bP == 0 && locationProducts[vPID].sP == 0) {
            p_notification(false, eb.getMessage('ERR_GRN_SUBPROD'));
        } else {
            setProductsView(viewProductIndex);
            $('#viewPiSubProductsModal').modal('show');
        }
    });

    function setProductsView(viewProductID) {
        $('.tempView').remove();
        $('#batchProductsView').addClass('hidden');
        $('#serialViewScreen').addClass('hidden');
        if (!jQuery.isEmptyObject(rtnProducts[viewProductID].sProducts)) {
            $('#serialViewScreen').removeClass('hidden');
            $('#directReturnSerialCodeView').html(rtnProducts[viewProductID].pCode);
            $('#directReturnSerialQuantityView').html(rtnProducts[viewProductID].pQuantity);
            for (var h in rtnProducts[viewProductID].sProducts) {
                var viewSPr = rtnProducts[viewProductID].sProducts[h];
                var cloneSerialProductView = $($('#serialViewSample').clone()).attr('id', '').removeClass('hidden').addClass('tempView');
                cloneSerialProductView.children('#serialNumberView').html(viewSPr.sCode);
                cloneSerialProductView.children('#serialWarrentyView').html(viewSPr.sWarranty);
                cloneSerialProductView.children('#serialBIDView').html(viewSPr.sBCode);
                cloneSerialProductView.children('#serialExDView').html(viewSPr.sEdate);
                cloneSerialProductView.insertBefore('#serialViewSample');
            }
        } else if (!jQuery.isEmptyObject(rtnProducts[viewProductID].bProducts)) {
            $('#batchProductsView').removeClass('hidden');
            $('#batchCodeView').html(rtnProducts[viewProductID].pCode);
            $('#batchQuantityView').html(rtnProducts[viewProductID].pQuantity);
            for (var b in rtnProducts[viewProductID].bProducts) {
                var viewBPr = rtnProducts[viewProductID].bProducts[b];
                var cloneBatchProductView = $($('#proBatchViewSample').clone()).attr('id', '').removeClass('hidden').addClass('tempView');
                cloneBatchProductView.children('#batchNmbrView').html(viewBPr.bCode);
                cloneBatchProductView.children('#batchQtyView').html(viewBPr.bQty);
                cloneBatchProductView.children('#batchMDateView').html(viewBPr.mDate);
                cloneBatchProductView.children('#batchEDateView').html(viewBPr.eDate);
                cloneBatchProductView.children('#batchWView').html(viewBPr.warnty);
                cloneBatchProductView.insertBefore('#proBatchViewSample');
            }
        }
    }

});



