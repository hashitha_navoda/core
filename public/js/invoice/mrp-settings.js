$(document).ready(function() {
  var isSingleItemWise = false;
  var isBulkItemWise = true;
  var respondItemList = [];
  var selectedItemsForMrp = [];


  loadDropDownFromDatabase('/productAPI/searchUserActiveLocationProductsForDropdown', "", '', '#product');
  $('#product').selectpicker('refresh');
  $('#product').on('change', function() {
      if ($(this).val() > 0) {
          eb.ajax({
              type: 'POST',
              url: BASE_URL + '/productAPI/getProductBaseUom',
              data: {productID:$(this).val()},
              success: function(respond) {            
                  if(respond.status){
                    // $('#single-item #mrpPercentagediv .sign').val('Unit');
                    $(".sign",  '#mrpPercentagediv').text(respond.data.uomAbbr);    
                    $(".sign",  '#mrpValDiv').text(respond.data.uomAbbr);    
                  }                
              }
          });
      } 
  });

	if ($('#mrpStatus').val() == 0) {
		$('.mrpSetting_div').addClass('hidden');
		$("#smsEnable").prop('checked',false);
	} else {
		$("#smsEnable").prop('checked',true);
		$('.mrpSetting_div').removeClass('hidden');		
  }

  if (isBulkItemWise) {
      $('.bulk-item').removeClass('hidden');    
      $('#mrpType').val(1);
      $('#mrpType').prop('disabled', true);
      $('#mrpType').selectpicker('render');
      var currentLoc  = [$('#currentLoc').val()];    

      $('#location').val(currentLoc);
      $('#location').selectpicker('render');

  }

    
  $('#smsEnable').change(function() {
    if($(this).is(":checked")) {
        eb.ajax({
            type: 'POST',
            url: BASE_URL + '/mrp-setting-api/changeManageMrpSettingStatus',
            data: {state:true},
            success: function(respond) {
                p_notification(respond.status, respond.msg);                
                if(respond.status){
                  $('.mrpSetting_div').removeClass('hidden');    
                }                
            }
        });
    } else {
		    
      	eb.ajax({
            type: 'POST',
            url: BASE_URL + '/mrp-setting-api/changeManageMrpSettingStatus',
            data: {state:false},
            success: function(respond) {
                p_notification(respond.status, respond.msg);                
                if(respond.status){
                  $('.mrpSetting_div').addClass('hidden');
                }                
            }
        });
      }
  });

  $('.isSingleItemWise').on('click', function() {
      isSingleItemWise = true;
      isBulkItemWise = false;
      $('.single-item').removeClass('hidden');
      $('.bulk-item').addClass('hidden');   
      $("#isBulkItemWise").prop('checked', false);

      var currentLoc  = [$('#currentLoc').val()];    

      $('.single-item #single-location').val(currentLoc);
      $('.single-item #single-location').selectpicker('render');

  });


  $('.single-item #mrpType').on('change', function() {
 
    if ($('.single-item #mrpType').val() == 1) {
      $('.single-item #mrpPercentagediv').removeClass('hidden');
      $('.single-item #mrpValDiv').addClass('hidden');
    } else if($('.single-item #mrpType').val() == 2) {
      $('.single-item #mrpPercentagediv').addClass('hidden');
      $('.single-item #mrpValDiv').removeClass('hidden');
    }
  });

  $('.isBulkItemWise').on('click', function() {
      isBulkItemWise = $(this).val();
      isSingleItemWise = false;
      $('#mrpType').val(1);
      $('#mrpType').prop('disabled', true);
      $('#mrpType').selectpicker('render');

      $('.bulk-item').removeClass('hidden');    
      
      var currentLoc  = [$('#currentLoc').val()];    

      $('#location').val(currentLoc);
      $('#location').selectpicker('render');

      $('.single-item').addClass('hidden');
      $('.bulk-item').removeClass('hidden'); 
      $("#isSingleItemWise").prop('checked', false);
  });

  $('#save-mrp-settings').on('click',function(e){
  	e.preventDefault();

    selectedItemsForMrp = [];
    //     if ($('.promoCustomerSelect', '#promoCustomerTable').is(':checked')) {
      $('.promoCustomerSelect', '#promoCustomerTable').each(function() {
          if ($(this).is(':checked')) {
              var productID = $(this).parents('tr').attr('data-item-id');
              selectedItemsForMrp.push(productID);
          }
      });
      
      var formData = {
        locations: ($("#location").val() == null) ? [] : $("#location").val(),
        mrpType: ($("#mrpType").val()) ? $("#mrpType").val() : null,
        mrpPercentageVal: $("#mrpPercentageVal").val(),
        mrpValue: $("#mrpPercentageVal").val(),
        selectedProducts: selectedItemsForMrp,
        isBulkItemWise: isBulkItemWise,
        isSingleItemWise: isSingleItemWise
      };

      if (validateFormData(formData)) {
          eb.ajax({
              type: 'POST',
              url: BASE_URL + '/mrp-setting-api/saveMrpSettingsForItems',
              data: formData,
              success: function (respond) {
                  p_notification(respond.status, respond.msg);                
                  if(respond.status){
                      window.setTimeout(function() {
                              location.reload();
                      }, 1500);                    
                  }                
              }
          });
      } 
  });

   $('#save-single-mrp-settings').on('click',function(e){
    e.preventDefault();

      selectedItemsForMrp = [];
      //     if ($('.promoCustomerSelect', '#promoCustomerTable').is(':checked')) {
      $('.promoCustomerSelect', '#promoCustomerTable').each(function() {
          if ($(this).is(':checked')) {
              var productID = $(this).parents('tr').attr('data-item-id');
              selectedItemsForMrp.push(productID);
          }
      });
      
      var formData = {
        locations: ($("#single-location").val() == null) ? [] : $("#single-location").val(),
        mrpType: ($(".single-item #mrpType").val()) ? $(".single-item #mrpType").val() : null,
        mrpPercentageVal: $("#singlemrpPercentageVal").val(),
        mrpValue: $("#singlemrpVal").val(),
        selectedProducts: $('#product').val(),
        isBulkItemWise: isBulkItemWise,
        isSingleItemWise: isSingleItemWise
      };

      if (validateSingleItemFormData(formData)) {
          eb.ajax({
              type: 'POST',
              url: BASE_URL + '/mrp-setting-api/saveMrpSettingsForItems',
              data: formData,
              success: function (respond) {
                  p_notification(respond.status, respond.msg);                
                  if(respond.status){
                      window.setTimeout(function() {
                              location.reload();
                      }, 1500);                    
                  }                
              }
          });
      } 
  });

  function validateFormData(formData) {

      if (isBulkItemWise && formData.selectedProducts.length <= 1) {
        p_notification(false, eb.getMessage("ERR_ITEMS_NOT_ENOUGH_FOR_BULK_MRP"));
        $("#serviceProvider").focus();
        return false;
      } else if (formData.selectedProducts.length == 0) {
        p_notification(
          false,
          eb.getMessage("ERR_ITEMS_EMPTY_FOR_MRP")
        );
        return false;
      } else if (isBulkItemWise && formData.mrpPercentageVal == '') {
        p_notification(
          false,
          eb.getMessage("ERR_PERCENTAGE_EMPTY_FOR_MRP")
        );
        return false;
      }else if (formData.locations.length == 0) {
        p_notification(
          false,
          eb.getMessage("ERR_LOCATION_EMPTY_FOR_MRP")
        );
        return false;
      } 
      return true;
  }

  function validateSingleItemFormData(formData) {

      if (formData.selectedProducts == '') {
        p_notification(
          false,
          eb.getMessage("ERR_ITEMS_EMPTY_FOR_MRP")
        );
        return false;
      } else if (formData.mrpType == 0) {
        p_notification(
          false,
          eb.getMessage("ERR_PERCENTAGE_EMPTY_FOR_MRP_TYPE")
        );
        return false;
      } else if (formData.mrpType == 1 && formData.mrpPercentageVal == '') {
        p_notification(
          false,
          eb.getMessage("ERR_PERCENTAGE_EMPTY_FOR_MRP_SINGLE")
        );
        return false;
      }  else if (formData.mrpType == 2 && formData.mrpValue == '') {
        p_notification(
          false,
          eb.getMessage("ERR_VAL_EMPTY_FOR_MRP")
        );
        return false;
      } else if (formData.locations.length == 0) {
        p_notification(
          false,
          eb.getMessage("ERR_LOCATION_EMPTY_FOR_MRP")
        );
        return false;
      } 
      return true;
  }
    
  $('#category').on('change', function() {  
    selectedItemsForMrp = [];
    $('input[type=checkbox][name=selectAll]').prop('checked', false);
    var catID = $('#category').val();
      eb.ajax({
          type: 'POST',
          url: BASE_URL + '/productAPI/getItemsByCategoryID',
          data: {catID: catID},
          success: function(respond) {
              if (respond.data) {

                  respondItemList = respond.data;
                  $('#promoCustomerTable').removeClass('hidden');
                  $('.table-div').removeClass('hidden');
                  $('#promoCustomerTable').find(".promoCustomers").remove();

                  for (var i in respond.data) {
                      var $customerSample = $('#customerSampleRow', '#promoCustomerTable').clone();

                      $customerSample.find('.itemName').html(respond.data[i].productName);
                      $customerSample.find('.itemCode').html(respond.data[i].productCode);
                      
                      $customerSample.attr('data-item-id', respond.data[i].productID);
                      $customerSample.addClass('promoCustomers');
                      $customerSample.removeClass('hidden');
                      $('.table-div').addClass('test-body');
                      $('.select-all-div').removeClass('hidden');
                      $customerSample.insertBefore($('#customerSampleRow', '#promoCustomerTable'));
                  }
              } else {
                  $('.table-div').removeClass('hidden');
                  $('#promoCustomerTable').addClass('hidden');
                  $('#promoCustomerTable').find(".promoCustomers").remove();
                  p_notification('info', eb.getMessage('ERR_PROMO_EMAIL_CUST_LIST'));
              }
          }
      });
     
  });

  $('input[type=checkbox][name=selectAll]').change(function() {
      if ($(this).is(':checked')) {
        $('.promoCustomerSelect', '#promoCustomerTable').each(function() {
            if (!$(this).parents('tr').hasClass('hidden')) {    
              if (!$(this).is(':checked')) {
                  $(this).prop("checked", true);
                  var productID = $(this).parents('tr').attr('data-item-id');
                  selectedItemsForMrp.push(productID);    
              }
            }
          });
      }
      else {
          $('.promoCustomerSelect', '#promoCustomerTable').each(function() {
            if (!$(this).parents('tr').hasClass('hidden')) {    
              if ($(this).is(':checked')) {
                  $(this).prop("checked", false);
                  var productID = $(this).parents('tr').attr('data-item-id');
                  delete selectedItemsForMrp[productID]; 
              }
            }
          });
      }
  });

});