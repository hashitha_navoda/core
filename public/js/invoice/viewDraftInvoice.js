var tmptotal;
var customerID;
var vehicleNumber;
var invoiceID;
var profileName;
var ignoreBudgetLimitFlag = false;
var deleteAttmpInvID = null;
var deleteAttmpInvType = null;

$(document).ready(function() {
    var posFlag = $('#posFlag').val();

    $('.deli_row').hide();
    $('#deli_form_group').hide();
    $('#tax_td').hide();
    $('#inv-custa-search,#inv-cus-profile-search, #inv-vehicle-search').selectpicker('hide');
    $('#view-div').hide();
    $('#custadiv').hide();

    $('#inv-search-select').on('change', function() {
        $('#inv-custa-search,#inv-cus-profile-search,#inv-search').val('').trigger('change');
        customerID = invoiceID = profileName = '';
        if ($(this).val() == 'Invoice No') {
            $('#inv-custa-search,#inv-cus-profile-search, #inv-vehicle-search').selectpicker('hide');
            $('#inv-search').empty();
            $('#inv-search').selectpicker('refresh');
            $('#inv-search').selectpicker('show');
            $('#view-div').hide();
            $('#custadiv').hide();
            $('#invoice-list').show();
            selectedsearch = 'Invoice No';
        } else if ($(this).val() == 'Customer Name') {
            $('#inv-search,#inv-cus-profile-search, #inv-vehicle-search').selectpicker('hide');
            $('#inv-custa-search').empty();
            $('#inv-custa-search').selectpicker('refresh');
            $('#inv-custa-search').selectpicker('show');
            $('#invoice-list').show();
            $('#view-div').hide();
            $('#custadiv').hide();
            selectedsearch = 'Customer Name';
        } else if ($(this).val() == 'Customer Profile') {
            $('#inv-search,#inv-custa-search,#inv-vehicle-search').selectpicker('hide');
            $('#inv-cus-profile-search').empty();
            $('#inv-cus-profile-search').selectpicker('refresh');
            $('#inv-cus-profile-search').selectpicker('show');
            $('#invoice-list').show();
            $('#view-div').hide();
            $('#custadiv').hide();
        }  else if ($(this).val() == 'Vehicle Number') {
            $('#inv-custa-search').selectpicker('hide');
            $('#inv-search,#inv-cus-profile-search').selectpicker('hide');

            $('#inv-vehicle-search').empty();
            $('#inv-vehicle-search').selectpicker('refresh');
            $('#inv-vehicle-search').selectpicker('show');
            $('#invoice-list').show();
            $('#view-div').hide();
            $('#custadiv').hide();
        }
    });

    $(document).on('click', "a.editsInactive", function(e) {
        p_notification(false, eb.getMessage('ERR_CUST_INACTIVE'));
    });

    loadDropDownFromDatabase('/customerAPI/search-customers-for-dropdown', "", 'withDeactivatedCustomers', '#inv-custa-search');
    $('#inv-custa-search').selectpicker('refresh');
    $('#inv-custa-search').on('change', function() {
        if ($(this).val() > 0 && customerID != $(this).val()) {
            customerID = $(this).val();
            getCustomerInvoices(customerID);
        }
    });

    function getCustomerInvoices(customerID) {
        var getinvbycusturl = '/invoice-api/retriveCustomerInvoice';
        var requestinvbycust = eb.post(getinvbycusturl, {
            customerID: customerID
        });
        requestinvbycust.done(function(retdata) {
            if (retdata == "noinvoices") {
                $('#custadiv').hide();
                p_notification(false, eb.getMessage('ERR_VIEWINVO_NO_INVOCUS'));
            } else {
                $('#invoice-list').hide();
                $('#custadiv').fadeIn();
                $('#custadiv').html('');
                $('#custadiv').html(retdata);
            }
            $(".preview").on("click", function(e) {
                e.preventDefault();
                makePreview(BASE_URL + $(this).attr("href"));
            });
        });
    }

    loadDropDownFromDatabase('/service-job-api/searchVehiclesForDropdown', "", 0, '#inv-vehicle-search');
    $('#inv-vehicle-search').selectpicker('refresh');
    $('#inv-vehicle-search').on('change', function() {
        if ($(this).val() > 0 && vehicleNumber != $(this).val()) {
            vehicleNumber = $(this).val();
            getInvoiceByVehicleNumber(vehicleNumber);
        }
    });


    function getInvoiceByVehicleNumber(vehicleNumberID) {
        var getinvbycusturl = '/invoice-api/getInvoiceByVehicleNumber';
        var requestinvbycust = eb.post(getinvbycusturl, {
            vehicleNumberID: vehicleNumberID
        });

        requestinvbycust.done(function(retdata) {
            if (retdata == "noinvoices") {
                $('#custadiv').hide();
                p_notification(false, eb.getMessage('ERR_VIEWINVO_NO_INVOCUS'));
            } else {
                $('#invoice-list').hide();
                $('#custadiv').fadeIn();
                $('#custadiv').html('');
                $('#custadiv').html(retdata);
            }
            $(".preview").on("click", function(e) {
                e.preventDefault();
                makePreview(BASE_URL + $(this).attr("href"));
            });
        });
    }






    loadDropDownFromDatabase('/customerAPI/search-customer-profiles-for-dropdown', "", 0, '#inv-cus-profile-search');
    $('#inv-cus-profile-search').selectpicker('refresh');
    $('#inv-cus-profile-search').on('change', function() {
        if ($(this).val() != 0 && profileName != $(this).val()) {
            profileName = $(this).val();
            getInvoicesByCustomerProfileName(profileName);
        }
    });

    function getInvoicesByCustomerProfileName(profileName) {
        var url = '/invoice-api/get-invoices-by-customer-profile-name';
        var request = eb.post(url, {
            profileName: profileName
        });
        request.done(function(retdata) {
            if (retdata == "noinvoices") {
                $('#custadiv').hide();
                p_notification(false, eb.getMessage('ERR_VIEWINVO_NO_INVOCUS'));
            } else {
                $('#invoice-list').hide();
                $('#custadiv').fadeIn();
                $('#custadiv').html('');
                $('#custadiv').html(retdata);
            }
            $(".preview").on("click", function(e) {
                e.preventDefault();
                makePreview(BASE_URL + $(this).attr("href"));
            });
        });
    }
    // set second params as 1. for find this come from view
    loadDropDownFromDatabase('/invoice-api/searchSalesInvoicesForDropdown', 1, posFlag, '#inv-search');
    $('#inv-search').selectpicker('refresh');
    $('#inv-search').on('change', function() {
        if ($(this).val() > 0 && invoiceID != $(this).val()) {
            invoiceID = $(this).val();
            if ($('#inv-search').val() == '') {
                $('#custadiv').html('');
                $('#custadiv').hide('');
                $('#invoice-list').show();
            } else {
                var searchurl = BASE_URL + '/invoice-api/getInvoicesFromSearch';
                var searchrequest = eb.post(searchurl, {invoiceID: invoiceID});
                searchrequest.done(function(searchdata) {
                    $('#invoice-list').hide();
                    $('#custadiv').show();
                    $('#custadiv').html(searchdata);
                    $(".preview").on("click", function(e) {
                        e.preventDefault();
                        makePreview(BASE_URL + $(this).attr("href"));
                    });
                });
            }
        }
    });

    $('#filter-button').on('click', function() {
        if ($('#from-date').val() == '' || $('#to-date').val() == '') {
            p_notification(false, eb.getMessage('ERR_VIEWPAY_FILLDATA'));
        } else {
            var invoicefilter = BASE_URL + '/invoice-api/getInvoicesByDatefilter';
            var filterrequest = eb.post(invoicefilter, {
                'fromdate': $('#from-date').val(),
                'todate': $('#to-date').val(),
                'customerID': customerID,
                'profileName': profileName,
                'posFlag': posFlag
            });
            filterrequest.done(function(retdata) {
                if (retdata.msg == 'noinvoices') {
//                    p_notification(false, eb.getMessage('ERR_VIEWDELINOTE_NO_INVO'));
//                    $('#custadiv').html('');
                    $('#invoice-list').hide();
                    $('#custadiv').fadeIn();
                    $('#custadiv').html('');
                    $('#custadiv').html(retdata);

                } else {
                    $('#invoice-list').hide();
                    $('#custadiv').fadeIn();
                    $('#custadiv').html('');
                    $('#custadiv').html(retdata);
                }
                $(".preview").on("click", function(e) {
                    e.preventDefault();
                    makePreview(BASE_URL + $(this).attr("href"))
                });
            });
        }
    });


    $('#searchByComment').on('click', function() {
        if ($('#invoiceCommentSearch').val() == '') {
            p_notification(false, eb.getMessage('ERR_VIEWINV_COMMENTFIELD_FOR_SEARCH'));
        } else {
            var invoicefilter = BASE_URL + '/invoice-api/getInvoicesByInvoiceCommentfilter';
            var filterrequest = eb.post(invoicefilter, {
                'searchString': $('#invoiceCommentSearch').val(),
            });
            filterrequest.done(function(retdata) {
                $('#invoice-list').hide();
                    $('#custadiv').fadeIn();
                    $('#custadiv').html('');
                    $('#custadiv').html(retdata);
                $(".preview").on("click", function(e) {
                    e.preventDefault();
                    makePreview(BASE_URL + $(this).attr("href"))
                });
            });
        }
    });


    $('#searchBySerialCode').on('click', function() {
        if ($('#invoiceSerialCodeSearch').val() == '') {
            p_notification(false, eb.getMessage('ERR_VIEWINV_COMMENTFIELD_FOR_SEARCH'));
        } else {
            var invoicefilter = BASE_URL + '/invoice-api/getInvoicesBySerialCodefilter';
            var filterrequest = eb.post(invoicefilter, {
                'searchString': $('#invoiceSerialCodeSearch').val(),
                'posFlag': posFlag
            });
            filterrequest.done(function(retdata) {
                $('#invoice-list').hide();
                    $('#custadiv').fadeIn();
                    $('#custadiv').html('');
                    $('#custadiv').html(retdata);
                $(".preview").on("click", function(e) {
                    e.preventDefault();
                    makePreview(BASE_URL + $(this).attr("href"))
                });
            });
        }

    });


    $('#resetSerchByComment').on('click', function() {
        $('#invoiceCommentSearch').val('');
        $('#invoice-list').show();
        $('#custadiv').html('');
        $('#custadiv').hide('');
    });

    $('#resetSerchBySerialCode').on('click', function() {
        $('#invoiceSerialCodeSearch').val('');
        $('#invoice-list').show();
        $('#custadiv').html('');
        $('#custadiv').hide('');
    });

    ///////DatePicker\\\\\\\
    var nowTemp = new Date();
    var frmdate;
    var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);

    var checkin = $('#from-date').datepicker().on('changeDate', function(ev) {
        checkin.hide();
        frmdate = ev.date;
        $('#to-date')[0].focus();
    }).data('datepicker');

    var checkout = $('#to-date').datepicker({
        onRender: function(date) {
            return date.valueOf() < frmdate ? 'disabled' : '';
        }
    }).on('changeDate', function(ev) {
        checkout.hide();
    }).data('datepicker');
/////EndOFDatePicker\\\\\\\\\

    //responsive issue
    $(window).bind('resize ready', function() {
        ($(window).width() <= 1200) ? $('#filter-button').addClass('margin_top_low') : $('#filter-button').removeClass('margin_top_low');
    });

    $(window).bind('resize ready', function() {
        ($(window).width() <= 480) ? $('#filter-button').addClass('col-xs-12') : $('#filter-button').removeClass('col-xs-12');
    });

    $('#invoice-list').on('click', '.inv_related_docs', function() {
        setDataToHistoryModal($(this).attr('data-inv-related-id'));
        $('#addDocHistoryModal').modal('show');
    });

     $('#invoice-list').on('click', '.doc_attachments', function() {
        setDataToAttachmentViewModal($(this).attr('data-inv-related-id'));
        $('#viewAttachmentModal').modal('show');
    });

    $(document).on('click', '.inv_related_docs_in_search', function() {
        setDataToHistoryModal($(this).attr('data-inv-related-id'));
        $('#addDocHistoryModal').modal('show');
    });

    $('#doc-history-table').on('click', '.document_view', function() {
        var documentId = $(this).parents('tr').attr('data-docid');
        var documentType = $(this).parents('tr').attr('data-doctype');
        $('#DocumentViewModal').modal('show');
        getPrintPreviewByDocumentTypeAndDocumentID(documentId, documentType);
    });

    $('#normal-job-cancel').on('click', function(e) {
        $("#myModal").modal('hide');
        cancelInvoiceWithJob(false);
    });

    $('#job-cancel-with-item-return').on('click', function(e) {
        $("#myModal").modal('hide');
        cancelInvoiceWithJob(true);
    });

    $('#back-to-dashboard').on('click', function(e) {
        $("#myModal").modal('hide');
    });


});

function deleteInvoice(invoiceID, invoiceTypeFlag, isLinkWithJob) {

    if (!isLinkWithJob) {
        bootbox.confirm("Are you sure you want to delete this " + invoiceTypeFlag + " ?", function(result) {
            if (result == true) {
                eb.ajax({
                    type: 'POST',
                    url: BASE_URL + '/invoice-api/delete-draft-invoice',
                    data: {
                        invoiceID: invoiceID,
                        invoiceTypeFlag: invoiceTypeFlag,
                        ignoreBudgetLimit: ignoreBudgetLimitFlag,
                        returnDeliveryNote: false
                    },
                    success: function(respond) {
                        if (respond.status) {
                            p_notification(respond.status, respond.msg);
                            window.setTimeout(function() {
                                location.reload();
                            }, 1000);
                        } else {
                            if (respond.data == "NotifyBudgetLimit") {
                                bootbox.confirm(respond.msg+' ,Are you sure you want to cancel this invoice ?', function(result) {
                                    if (result == true) {
                                        ignoreBudgetLimitFlag = true;
                                        deleteInvoice(invoiceID, invoiceTypeFlag);
                                    } else {
                                        setTimeout(function(){ 
                                            location.reload();
                                        }, 3000);
                                    }
                                });
                            } else {
                                p_notification(respond.status, respond.msg);
                            }
                        }
                    },
                    async: false
                });
            }
        });
    } else {
        deleteAttmpInvID = invoiceID;
        deleteAttmpInvType = invoiceTypeFlag;
        $("#myModal").modal('show');
    }

}

function cancelInvoiceWithJob(returnJobDeli) {
    eb.ajax({
        type: 'POST',
        url: BASE_URL + '/invoice-api/delete-invoice',
        data: {
            invoiceID: deleteAttmpInvID,
            invoiceTypeFlag: deleteAttmpInvType,
            ignoreBudgetLimit: ignoreBudgetLimitFlag,
            returnDeliveryNote: returnJobDeli
        },
        success: function(respond) {
            if (respond.status) {
                p_notification(respond.status, respond.msg);
                window.setTimeout(function() {
                    location.reload();
                }, 1000);
            } else {
                if (respond.data == "NotifyBudgetLimit") {
                    ignoreBudgetLimitFlag = true;
                    $("#myModal").modal('show');
                    // bootbox.confirm(respond.msg+' ,Are you sure you want to cancel this invoice ?', function(result) {
                    //     if (result == true) {
                    //         ignoreBudgetLimitFlag = true;
                    //         deleteInvoice(invoiceID, invoiceTypeFlag);
                    //     } else {
                    //         setTimeout(function(){ 
                    //             location.reload();
                    //         }, 3000);
                    //     }
                    // });
                } else {
                    p_notification(respond.status, respond.msg);
                }
            }
        },
        async: false
    });
}

function setDataToHistoryModal(invoiceID) {
    $('#doc-history-table tbody tr').remove();
    $('#doc-history-table tfoot div').remove();
    $('#doc-history-table tbody div').remove();

    eb.ajax({
        type: 'POST',
        url: BASE_URL + '/invoice-api/getAllRelatedDocumentDetailsByInvoiceId',
        data: {invoiceID: invoiceID},
        success: function(respond) {
            if (respond.status == true) {
                $('#doc-history-table thead tr').removeClass('hidden');
                $.each(respond.data, function(index, value) {
                    if (value != null) {
                        $.each(value, function(index2, value2) {
                            tableBody = "<tr data-docId=" + value2['documentID'] + " data-doctype=" + value2['type'] + "><td class='' >" + value2['type'] + "</td><td>" + value2['code'] + "</td><td>" + value2['amount'] + "</td><td>" + value2['issuedDate'] + "</td><td class='text-center'><span class='glyphicon glyphicon-eye-open openlist link_color document_view ' style='cursor:pointer'></span></td></tr>";
                            $('#doc-history-table tbody').append(tableBody);
                        });
                    }
                });
                var footer = " <div class='col-lg-12 col-md-12 col-sm-12 text-right'><button type='button' class='btn btn-primary col-lg-12 col-md-12 col-sm-12' data-dismiss='modal' aril-hidden='true'>Back</button></div>";
                $('#doc-history-table tfoot').append(footer);
            } else {
                $('#doc-history-table thead tr').addClass('hidden');
                var noDataFooter = "<div class= ' col-lg-9 text-right'><label><b>No Data</b></label>";
                $('#doc-history-table tbody').append(noDataFooter);
            }
        }
    });
}

function setDataToAttachmentViewModal(invoiceID) {
    $('#doc-attach-table tbody tr').remove();
    $('#doc-attach-table tfoot div').remove();
    $('#doc-attach-table tbody div').remove();

    eb.ajax({
        type: 'POST',
        url: BASE_URL + '/get-document-related-attachement',
        data: {
            documentID: invoiceID,
            documentTypeID: 1
        },
        success: function(respond) {
            if (respond.status == true) {
                $('#doc-attach-table thead tr').removeClass('hidden');
                $.each(respond.data, function(index, value) {
                    tableBody = "<tr><td><a href="+value.link+">"+value.docName+"</a></td></tr>";
                    $('#doc-attach-table tbody').append(tableBody);
                });
            } else {
                $('#doc-attach-table thead tr').addClass('hidden');
                var noDataFooter = "<div class= ' col-lg-6 text-right'><label><b>No Data</b></label>";
                $('#doc-attach-table tbody').append(noDataFooter);
            }
        }
    });
}

function getPrintPreviewByDocumentTypeAndDocumentID(documentID, documentType) {
    var $iframe = $('#related-document-view');
    $iframe.ready(function() {
        $iframe.contents().find("body div").remove();
    });
    var URL;
    if (documentType == 'Quotation') {
        URL = BASE_URL + '/quotation/document/' + documentID;
    } else if (documentType == 'SalesOrder') {
        URL = BASE_URL + '/salesOrders/document/' + documentID;
    }
    else if (documentType == 'DeliveryNote') {
        URL = BASE_URL + '/delivery-note/document/' + documentID;
    }
    else if (documentType == 'SalesReturn') {
        URL = BASE_URL + '/return/document/' + documentID;
    }
    else if (documentType == 'SalesInvoice') {
        URL = BASE_URL + '/invoice/document/' + documentID;
    }
    else if (documentType == 'CustomerPayment') {
        URL = BASE_URL + '/customerPayments/document/' + documentID;
    }
    else if (documentType == 'CreditNote') {
        URL = BASE_URL + '/credit-note/document/' + documentID;
    } 
    else if (documentType == 'CreditNotePayment') {
        URL = BASE_URL + '/credit-note-payments/document/' + documentID;
    }

    eb.ajax({
        type: 'POST',
        url: URL,
        success: function(respond) {
            var division = "<div></div>";
            $iframe.ready(function() {
                $iframe.contents().find("body").append(division);
            });
            $iframe.ready(function() {
                $iframe.contents().find("body div").append(respond);
            });
        }
    });

}