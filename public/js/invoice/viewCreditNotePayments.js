var ignoreBudgetLimitFlag = false;
$(document).ready(function() {
    var customerID = '';
    var creditNotePaymentID;
    var deleteCreditNotePaymentID = '';
    $('#cn-pay-custa-search').selectpicker('hide');
    $('#cn-pay-search-select').on('change', function() {
        if ($(this).val() == 'Payments No') {
            $('#cn-pay-search').selectpicker('show');
            $('#cn-pay-custa-search').selectpicker('hide');
        } else {
            $('#cn-pay-custa-search').removeClass('hidden')
            $('#cn-pay-custa-search').selectpicker('show');
            $('#cn-pay-search').selectpicker('hide');
        }
        $('#cn-pay-search').selectpicker('refresh');
        $('#cn-pay-custa-search').selectpicker('refresh');
    });

    loadDropDownFromDatabase('/customerAPI/search-customers-for-dropdown', "", "withDeactivatedCustomers", '#cn-pay-custa-search');
    $('#cn-pay-custa-search').selectpicker('refresh');
    $('#cn-pay-custa-search').on('change', function() {
        if ($(this).val() > 0 && customerID != $(this).val()) {
            customerID = $(this).val();
            var getCustCNPUrl = '/credit-note-payments-api/retriveCustomerCreditNotePayments';
            var requestCustCNP = eb.post(getCustCNPUrl, {
                customerID: customerID
            });
            requestCustCNP.done(function(retdata) {
                if (retdata == "noCreditNotePayments") {
                    p_notification(false, eb.getMessage('ERR_VIEWCRNOTEPAY_NO_CRDNPAYCUS'));
                } else {
                    $('#credit-note-payment-list').html(retdata);
                }
            });
        }
    });

    loadDropDownFromDatabase('/credit-note-payments-api/search-all-credit-note-payments-for-dropdown', "", 0, '#cn-pay-search');
    $('#cn-pay-search').selectpicker('refresh');
    $('#cn-pay-search').on('change', function() {
        if ($(this).val() > 0 && creditNotePaymentID != $(this).val()) {
            creditNotePaymentID = $(this).val();
            var getCNPByCNIDUrl = '/credit-note-payments-api/retriveCreditNotePaymentByCreditNotePaymentID';
            var requestCNPIDCNP = eb.post(getCNPByCNIDUrl, {
                creditNotePaymentID: creditNotePaymentID
            });
            requestCNPIDCNP.done(function(retdata) {
                if (retdata == "notFindCreditNotePayment") {
                    p_notification(false, eb.getMessage('ERR_VIEWCRNOTEPAY_NO_CRDNPAYNOTFOUND'));
                } else {
                    $('#credit-note-payment-list').html(retdata);
                }
            });
        }
    });

    $(document).on('click', "a.editsInactive", function(e) {
        p_notification(false, eb.getMessage('ERR_CUST_INACTIVE'));
    });

    $('#filter-button').on('click', function() {
        if ($('#from-date').val() == '' || $('#to-date').val() == '') {
            p_notification(false, eb.getMessage('ERR_VIEWCRNOTEPAY_FILLDATA'));
        } else {
            var CNPFilterUrl = BASE_URL + '/credit-note-payments-api/retriveCreditNotePaymentsByDatefilter';
            var filterrequest = eb.post(CNPFilterUrl, {
                'fromdate': $('#from-date').val(),
                'todate': $('#to-date').val(),
                'customerID': customerID
            });
            filterrequest.done(function(retdata) {
                if (retdata == "notFindCreditNotePayment") {
                    p_notification(false, eb.getMessage('ERR_VIEWCRNOTEPAY_NO_CRDNPAYFOUND_BE_DATE'));
                } else {
                    $('#credit-note-payment-list').html(retdata);
                }
            });
        }
    });

    $(document).on('click', ' .del', function() {
        deleteCreditNotePaymentID = $(this).attr('id');
    });

    $(document).on('click', ' .doc_attachments', function() {
        setDataToAttachmentViewModal($(this).attr('data-cnp-related-id'));
        $('#viewAttachmentModal').modal('show');
    });

     $(document).on('click', ' .cn_payments_related_docs', function() {
        setDataToHistoryModal($(this).attr('data-cn-payment-related-id'));
        $('#addDocHistoryModal').modal('show');
    });

    $('#doc-history-table').on('click', '.document_view', function() {
        var documentId = $(this).parents('tr').attr('data-docid');
        var documentType = $(this).parents('tr').attr('data-doctype');
        $('#DocumentViewModal').modal('show');
        getPrintPreviewByDocumentTypeAndDocumentID(documentId, documentType);
    });


    $('#delete-cn-payment-button').on('click', function(e) {
        e.preventDefault();
        deleteCreditNotePayment();
    });


    function setDataToHistoryModal(creditNotePaymentID) {
        $('#doc-history-table tbody tr').remove();
        $('#doc-history-table tfoot div').remove();
        $('#doc-history-table tbody div').remove();

        eb.ajax({
            type: 'POST',
            url: BASE_URL + '/credit-note-payments-api/getAllRelatedDocumentDetailsByCreditNotePaymentID',
            data: {creditNotePaymentID: creditNotePaymentID},
            success: function(respond) {
                if (respond.status == true) {
                    $('#doc-history-table thead tr').removeClass('hidden');
                    $.each(respond.data, function(index, value) {
                        if (value != null) {
                            $.each(value, function(index2, value2) {
                                tableBody = "<tr data-docId=" + value2['documentID'] + " data-doctype=" + value2['type'] + "><td class='' >" + value2['type'] + "</td><td>" + value2['code'] + "</td><td>" + value2['amount'] + "</td><td>" + value2['issuedDate'] + "</td><td class='text-center'><span class='glyphicon glyphicon-eye-open openlist link_color document_view ' style='cursor:pointer'></span></td></tr>";
                                $('#doc-history-table tbody').append(tableBody);
                            });
                        }
                    });
                    var footer = " <div class='col-lg-12 col-md-12 col-sm-12 text-right'><button type='button' class='btn btn-primary col-lg-12 col-md-12 col-sm-12' data-dismiss='modal' aril-hidden='true'>Back</button></div>";
                    $('#doc-history-table tfoot').append(footer);
                } else {
                    $('#doc-history-table thead tr').addClass('hidden');
                    var noDataFooter = "<div class= ' col-lg-9 text-right'><label><b>No Data</b></label>";
                    $('#doc-history-table tbody').append(noDataFooter);
                }
            }
        });
    }

    function deleteCreditNotePayment()
    {
        var valid = new Array(
                $('#username').val(),
                $('#password').val()
                );
        if (validate_data(valid)) {
            var username = $("#username").val();
            var password = $('#password').val();
            var comment = $('#comment').val();
//                $('.main_div').addClass('Ajaxloading');
            var getpayurl = BASE_URL + '/credit-note-payments-api/deleteCreditNotePayment';
            var getpayrequest = eb.post(getpayurl, {
                creditNotePaymentID: deleteCreditNotePaymentID,
                username: username,
                password: password,
                creditNotePaymentCancelMessage: comment,
                ignoreBudgetLimit: ignoreBudgetLimitFlag
            });
            getpayrequest.done(function(data) {
                if (data.status) {
                    $('#delete_cn_payment').modal('hide');
                    p_notification(true, eb.getMessage('ERR_VIEWPAY_PAY_DELETE'));
                    setTimeout(function() {
                        window.location.reload();
                    }, 1000);
                } else if (data.data == 'admin') {
                    p_notification(false, eb.getMessage('ERR_VIEWCRNOTEPAY_USER_PRIVILEGE'));
                } else if (data.data == 'user' || data.data == 'pass') {
                    p_notification(false, eb.getMessage('ERR_VIEWCRNOTEPAY_INVALID_USRNAME_PWD'));
                } else if (data.data == false) {
                    p_notification(false, eb.getMessage('ERR_VIEWCRNOTEPAY_USER_AUTH'));
                } else if (data.data == "NotifyBudgetLimit") {
                    bootbox.confirm(data.msg+' ,Are you sure you want to delete ?', function(result) {
                        if (result == true) {
                            ignoreBudgetLimitFlag = true;
                            deleteCreditNotePayment();
                        } else {
                            setTimeout(function(){ 
                                location.reload();
                            }, 3000);
                        }
                    });
                } else {
                    p_notification(false, data.msg);
                }
            });
        }
    }

    function validate_data(valid) {
        var user = valid[0];
        var pass = valid[1];
        if (user == null || user == "") {
            p_notification(false, eb.getMessage('ERR_VIEWCRNOTEPAY_UNAME'));
        } else if (pass == null || pass == "") {
            p_notification(false, eb.getMessage('ERR_VIEWCRNOTEPAY_PWD'));
        } else {
            return true;
        }
    }

    function getPrintPreviewByDocumentTypeAndDocumentID(documentID, documentType) {
        var $iframe = $('#related-document-view');
        $iframe.ready(function() {
            $iframe.contents().find("body div").remove();
        });
        var URL;
        if (documentType == 'Quotation') {
            URL = BASE_URL + '/quotation/document/' + documentID;
        } else if (documentType == 'SalesOrder') {
            URL = BASE_URL + '/salesOrders/document/' + documentID;
        }
        else if (documentType == 'DeliveryNote') {
            URL = BASE_URL + '/delivery-note/document/' + documentID;
        }
        else if (documentType == 'SalesReturn') {
            URL = BASE_URL + '/return/document/' + documentID;
        }
        else if (documentType == 'SalesInvoice') {
            URL = BASE_URL + '/invoice/document/' + documentID;
        }
        else if (documentType == 'CustomerPayment') {
            URL = BASE_URL + '/customerPayments/document/' + documentID;
        }
        else if (documentType == 'CreditNote') {
            URL = BASE_URL + '/credit-note/document/' + documentID;
        } 
        else if (documentType == 'CreditNotePayment') {
            URL = BASE_URL + '/credit-note-payments/document/' + documentID;
        }

        eb.ajax({
            type: 'POST',
            url: URL,
            success: function(respond) {
                var division = "<div></div>";
                $iframe.ready(function() {
                    $iframe.contents().find("body").append(division);
                });
                $iframe.ready(function() {
                    $iframe.contents().find("body div").append(respond);
                });
            }
        });

    }

    function setDataToAttachmentViewModal(documentID) {
        $('#doc-attach-table tbody tr').remove();
        $('#doc-attach-table tfoot div').remove();
        $('#doc-attach-table tbody div').remove();

        eb.ajax({
            type: 'POST',
            url: BASE_URL + '/get-document-related-attachement',
            data: {
                documentID: documentID,
                documentTypeID: 17
            },
            success: function(respond) {
                if (respond.status == true) {
                    $('#doc-attach-table thead tr').removeClass('hidden');
                    $.each(respond.data, function(index, value) {
                        tableBody = "<tr><td><a href="+value.link+">"+value.docName+"</a></td></tr>";
                        $('#doc-attach-table tbody').append(tableBody);
                    });
                } else {
                    $('#doc-attach-table thead tr').addClass('hidden');
                    var noDataFooter = "<div class= ' col-lg-6 text-right'><label><b>No Data</b></label>";
                    $('#doc-attach-table tbody').append(noDataFooter);
                }
            }
        });
    }

///////DatePicker\\\\\\\
    var nowTemp = new Date();
    var frmdate;
    var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);

    var checkin = $('#from-date').datepicker().on('changeDate', function(ev) {
        checkin.hide();
        frmdate = ev.date;
        $('#to-date')[0].focus();
    }).data('datepicker');
    var checkout = $('#to-date').datepicker({
        onRender: function(date) {
            return date.valueOf() < frmdate ? 'disabled' : '';
        }
    }).on('changeDate', function(ev) {
        checkout.hide();
    }).data('datepicker');
/////EndOFDatePicker\\\\\\\\\
});