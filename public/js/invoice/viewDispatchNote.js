$(document).ready(function() {

    var frmdate = null;
    var dispatchNoteId = null;

    var checkin = $('#from-date').datepicker({
        format:'yyyy-mm-dd'
    }).on('changeDate', function(ev) {
        checkin.hide();
        frmdate = ev.date;
        $('#to-date')[0].focus();
    }).data('datepicker');
    var checkout = $('#to-date').datepicker({
        format:'yyyy-mm-dd',
        onRender: function(date) {
            return date.valueOf() < frmdate ? 'disabled' : '';
        }
    }).on('changeDate', function(ev) {
        checkout.hide();
    }).data('datepicker');

    $('#search-select').val('dispatch-note-code');
    $('#cust-search').selectpicker('hide');

    loadDropDownFromDatabase('/customerAPI/search-customers-for-dropdown', "", 'withDeactivatedCustomers', '#cust-search');
    loadDropDownFromDatabase('/dispatch-note-api/search-dispatch-code-for-dropdown', "", 0, '#dis-note-code-search');

    $('#search-select').on('change', function() {
        if ($(this).val() == 'dispatch-note-code') {
            $('#dis-note-code-search').selectpicker('show');
            $('#cust-search').selectpicker('hide');
            $('#cust-search').val('');
            $('#cust-search').selectpicker('render');
        } else if ($(this).val() == 'customer-name') {
            $('#dis-note-code-search').selectpicker('hide');
            $('#cust-search').selectpicker('show');
            $('#dis-note-code-search').val('');
            $('#dis-note-code-search').selectpicker('render');
        }
    });

    $('#search-btn').click( function(){
        var input = {};
        input.key = $('#search-select').val();
        input.customerId = $('#cust-search').val();
        input.dispatchNoteId = $('#dis-note-code-search').val();
        input.fromDate = $('#from-date').val();
        input.toDate   = $('#to-date').val();

        if(validateInput(input)){
            searchDispatchNotes(input);
        }
    });

    $(document).on('click','.dispatch-note-action', function(e){
        e.preventDefault();
        var action = $(this).data('action');
        var dispatchNoteCode = $(this).closest('tr').data('dispatch-note-code');
        dispatchNoteId = $(this).closest('tr').data('dispatch-note-id');
        selectedRow = $(this).closest('tr');

        switch (action){
            case 'view' :
                break;

            case 'delete' :
                $('.dispatchNoteDeleteModalBody').html('<p>Are you sure you want to delete <b>"'+dispatchNoteCode+'"</b> ?</p>');
                $('#dispatchNoteDeleteModal').modal('show');
                break;

            default :
                console.log('invlaid option');
        }
    });

    $(document).on('click','#btnDelete',function(){
        $.ajax({
            type: 'POST',
            url: BASE_URL + '/dispatch-note-api/delete',
            data: { dispatchNoteId : dispatchNoteId },
            success: function(respond) {
                if(respond.status){
                    $('#dispatchNoteDeleteModal').modal('hide');
                    searchDispatchNotes({key:'none'});
                } else {
                    p_notification(false, respond.msg);
                }
            }
        });
    });

    $(document).on('click', "a.editsInactive", function(e) {
        p_notification(false, eb.getMessage('ERR_CUST_INACTIVE'));
    });

    function searchDispatchNotes(input){
        eb.ajax({
            type: 'POST',
            url: BASE_URL + '/dispatch-note-api/search',
            data: input,
            success: function(respond) {
                $('#dispatch-note-list').html(respond);
            }
        });
    }

    function validateInput(input){
        if(input.fromDate && !input.toDate){
            p_notification(false, 'please fill start date');
        } else if((!input.fromDate && input.toDate)){
            p_notification(false, 'please fill end date');
        }else if(input.fromDate && input.toDate && (input.fromDate > input.toDate)){
            p_notification(false, 'invalid date range');
        } else {
            return true;
        }
    }

});