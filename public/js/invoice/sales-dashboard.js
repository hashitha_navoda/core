var colorPalette = ['#ab34eb','#FBAC33','#008ffb', '#FF4560', '#775DD0','#6023ea','#ea2385'];
$(document).ready(function() {
if (!$('.main_body').hasClass('hide-side-bar')) {
    $('.sidbar-toggle-menu').trigger('click');
}
var customerSales;
var currencySymbol;
var heigstSalesItemData;
    // getWidgetData('thisMonth');
    updateDashboardData('thisWeek');

    function updateDashboardData(period) {
        updateChartData(period, function() {
            getHeaderWidgetData(period, function () {
                updateFooterChartData(period, function () {

                });
            });
        });
    }

    function getHeaderWidgetData (period, callback) {
        eb.ajax({
            type: 'POST',
            url: BASE_URL + '/sales-dashboard-api/getHeaderWidgetData',
            data: {period: period},
            success: function(data) {
                $('#target-chart').html('');
                // $('#cash-chart').html('');
                currencySymbol = data.companyCurrencySymbol;
                $('#total-sales').each(function () {
                    $(this).prop('Counter',0).animate({
                        Counter: data.totalSales
                    }, {
                        duration: 500,
                        easing: 'swing',
                        step: function (now) {
                            $(this).text(data.companyCurrencySymbol+' '+accounting.formatMoney(now));
                        }
                    });
                });
                $('#gross-profit').each(function () {
                    $(this).prop('Counter',0).animate({
                        Counter: data.grossProfit
                    }, {
                        duration: 500,
                        easing: 'swing',
                        step: function (now) {
                            $(this).text(data.companyCurrencySymbol+' '+accounting.formatMoney(now));
                        }
                    });
                });
                $('#total-payments').each(function () {
                    $(this).prop('Counter',0).animate({
                        Counter: data.totalPayment
                    }, {
                        duration: 500,
                        easing: 'swing',
                        step: function (now) {
                            $(this).text(data.companyCurrencySymbol+' '+accounting.formatMoney(now));
                        }
                    });
                });
                $('#totalInvoiceCount').each(function () {
                    $(this).prop('Counter',0).animate({
                        Counter: data.totalInvoiceCount
                    }, {
                        duration: 500,
                        easing: 'swing',
                        step: function (now) {
                            $(this).text(Math.ceil(now));
                        }
                    });
                });

                if (data.totalSalesProfitLoss > 0) {
                    $('#total-sales-profit').removeClass('hidden');
                    $('#total-sales-loss').addClass('hidden');
                    $('#total-sales-profit').html('+'+data.totalSalesProfitLoss+'%<span><i class="glyphicon glyphicon-arrow-up"></i></span>');
                } else {
                    $('#total-sales-profit').addClass('hidden');
                    $('#total-sales-loss').removeClass('hidden');
                    var loss = data.totalSalesProfitLoss * -1;
                    $('#total-sales-loss').html('+'+loss+'%<span><i class="glyphicon glyphicon-arrow-down"></i></span>');
                }

                if (data.totalGrossProfitProfitLoss > 0) {
                    $('#total-gross-profit').removeClass('hidden');
                    $('#total-gross-loss').addClass('hidden');
                    $('#total-gross-profit').html('+'+data.totalGrossProfitProfitLoss+'%<span><i class="glyphicon glyphicon-arrow-up"></i></span>');
                } else {
                    $('#total-gross-profit').addClass('hidden');
                    $('#total-gross-loss').removeClass('hidden');
                    var loss = data.totalGrossProfitProfitLoss * -1;
                    $('#total-gross-loss').html('+'+loss+'%<span><i class="glyphicon glyphicon-arrow-down"></i></span>');
                }

                if (data.totalPaymentProfitLoss > 0) {
                    $('#total-payment-profit').removeClass('hidden');
                    $('#total-payment-loss').addClass('hidden');
                    $('#total-payment-profit').html('+'+data.totalPaymentProfitLoss+'%<span><i class="glyphicon glyphicon-arrow-up"></i></span>');
                } else {
                    $('#total-payment-profit').addClass('hidden');
                    $('#total-payment-loss').removeClass('hidden');
                    var loss = data.totalPaymentProfitLoss * -1;
                    $('#total-payment-loss').html('+'+loss+'%<span><i class="glyphicon glyphicon-arrow-down"></i></span>');
                }

                if (data.invoiceCountProfitLoss > 0) {
                    $('#total-invoice-count-profit').removeClass('hidden');
                    $('#total-invoice-count-loss').addClass('hidden');
                    $('#total-invoice-count-profit').html('+'+data.invoiceCountProfitLoss+'%<span><i class="glyphicon glyphicon-arrow-up"></i></span>');
                } else {
                    $('#total-invoice-count-profit').addClass('hidden');
                    $('#total-invoice-count-loss').removeClass('hidden');
                    var loss = data.invoiceCountProfitLoss * -1;
                    $('#total-invoice-count-loss').html('+'+loss+'%<span><i class="glyphicon glyphicon-arrow-down"></i></span>');
                }


                if (data.gpPercentage > 0) {
                    $('.gpPercentage').addClass('gpPercentageProfit');
                    $('.gpPercentage').removeClass('gpPercentageLoss');
                    var endAngleOfGpChart = (data.gpPercentage / 100) * 470;
                    $('#gpPercentage').text(data.gpPercentage+'%');
                } else {
                    var endAngleOfGpChart = 0;
                    $('.gpPercentage').removeClass('gpPercentageProfit');
                    $('.gpPercentage').addClass('gpPercentageLoss');
                    $('#gpPercentage').text(data.gpPercentage*-1+'%');
                }

                gpChart(endAngleOfGpChart.toFixed(2));
                salesAndGpSummaryChart(data.totalGrossValue, data.totalCost, data.grossProfit);

                switch (period) {
                    case 'thisYear':
                        $('#last-period-total-sales').text('Compared to ('+data.companyCurrencySymbol+' '+accounting.formatMoney(data.totalSalesLastYear)+' last year)');
                        $('#last-period-total-gross').text('Compared to ('+data.companyCurrencySymbol+' '+accounting.formatMoney(data.lastYearGrossProfit)+' last year)');
                        $('#last-period-total-payment').text('Compared to ('+data.companyCurrencySymbol+' '+accounting.formatMoney(data.lastPeriodTotalPayment)+' last year)');
                        $('#last-period-total-invoices').text('Compared to ('+data.totalInvoiceCountLastPeriod+' last year)');
                        break;
                    case 'thisMonth':
                        $('#last-period-total-sales').text('Compared to ('+data.companyCurrencySymbol+' '+accounting.formatMoney(data.totalSalesLastYear)+' last month)');
                        $('#last-period-total-gross').text('Compared to ('+data.companyCurrencySymbol+' '+accounting.formatMoney(data.lastYearGrossProfit)+' last month)');
                        $('#last-period-total-payment').text('Compared to ('+data.companyCurrencySymbol+' '+accounting.formatMoney(data.lastPeriodTotalPayment)+' last month)');
                        $('#last-period-total-invoices').text('Compared to ('+data.totalInvoiceCountLastPeriod+' last month)');
                        break;
                    case 'thisWeek':
                        $('#last-period-total-sales').text('Compared to ('+data.companyCurrencySymbol+' '+accounting.formatMoney(data.totalSalesLastYear)+' last week)');
                        $('#last-period-total-gross').text('Compared to ('+data.companyCurrencySymbol+' '+accounting.formatMoney(data.lastYearGrossProfit)+' last week)');
                        $('#last-period-total-payment').text('Compared to ('+data.companyCurrencySymbol+' '+accounting.formatMoney(data.lastPeriodTotalPayment)+' last week)');
                        $('#last-period-total-invoices').text('Compared to ('+data.totalInvoiceCountLastPeriod+' last week)');
                        break;
                    case 'thisDay':
                        $('#last-period-total-sales').text('Compared to ('+data.companyCurrencySymbol+' '+accounting.formatMoney(data.totalSalesLastYear)+' last day)');
                        $('#last-period-total-gross').text('Compared to ('+data.companyCurrencySymbol+' '+accounting.formatMoney(data.lastYearGrossProfit)+' last day)');
                        $('#last-period-total-payment').text('Compared to ('+data.companyCurrencySymbol+' '+accounting.formatMoney(data.lastPeriodTotalPayment)+' last day)');
                        $('#last-period-total-invoices').text('Compared to ('+data.totalInvoiceCountLastPeriod+' last day)');
                        break;
                    default:
                        // statements_def
                        break;
                }
                
            }
        });
        callback();
    }

    function updateChartData (period, callback) {
        eb.ajax({
            type: 'POST',
            url: BASE_URL + '/sales-dashboard-api/getBaseChartData',
            data: {period: period},
            success: function(data) {
                // $('#target-chart').html('');
                $('#cash-chart').html('');
                $('#gp-chart').html('');
        
                currencySymbol = data.companyCurrencySymbol;
                customerSales = data.customerSales;
                heigstSalesItemData = data.heigstSalesItemData;
                $('#cash-chart').html('');
                cashCreditChart(data.customerSales);
                $.each(data.heigstSalesItemData, function(index, val) {
                    if (index < 5) {
                        $('.productCode'+index).text(val.productName);
                        $('.productQty'+index).text(val.totalQty);
                        $('.productSales'+index).text(accounting.formatMoney(val.totalSales));
                    }
                });

                if (data.heigstSalesItemData.length < 5) {
                    for (var i = 0; i < 5; i++) {
                        if (i >= data.heigstSalesItemData.length) {
                            $('.productCode'+i).text('');
                            $('.productQty'+i).text('');
                            $('.productSales'+i).text('');
                        }
                    }
                }
                
            }
        });
        callback();
    }

    function updateFooterChartData (period, callback) {
         eb.ajax({
            type: 'POST',
            url: BASE_URL + '/sales-dashboard-api/updateFooterChartData',
            data: {period: period},
            success: function(data) {
                currencySymbol = data.companyCurrencySymbol;
                salesSummaryChart(data.openInvoiceTotal, data.overdueInvoiceTotal, data.closedInvoiceTotal);
                paymentSummaryChart(data);   
            }
        });
        callback();
    }




    function getWidgetData(period){
        eb.ajax({
            type: 'POST',
            url: BASE_URL + '/sales-dashboard-api/getWidgetData',
            data: {period: period},
            success: function(data) {
                $('#target-chart').html('');
                $('#cash-chart').html('');
                $('#gp-chart').html('');
                $('#total-sales').each(function () {
                    $(this).prop('Counter',0).animate({
                        Counter: data.totalSales
                    }, {
                        duration: 500,
                        easing: 'swing',
                        step: function (now) {
                            $(this).text('LKR '+accounting.formatMoney(now));
                        }
                    });
                });
                $('#gross-profit').each(function () {
                    $(this).prop('Counter',0).animate({
                        Counter: data.grossProfit
                    }, {
                        duration: 500,
                        easing: 'swing',
                        step: function (now) {
                            $(this).text('LKR '+accounting.formatMoney(now));
                        }
                    });
                });
                $('#total-payments').each(function () {
                    $(this).prop('Counter',0).animate({
                        Counter: data.totalPayment
                    }, {
                        duration: 500,
                        easing: 'swing',
                        step: function (now) {
                            $(this).text('LKR '+accounting.formatMoney(now));
                        }
                    });
                });
                $('#totalInvoiceCount').each(function () {
                    $(this).prop('Counter',0).animate({
                        Counter: data.totalInvoiceCount
                    }, {
                        duration: 500,
                        easing: 'swing',
                        step: function (now) {
                            $(this).text(Math.ceil(now));
                        }
                    });
                });

                if (data.totalSalesProfitLoss > 0) {
                    $('#total-sales-profit').removeClass('hidden');
                    $('#total-sales-loss').addClass('hidden');
                    $('#total-sales-profit').html('+'+data.totalSalesProfitLoss+'%<span><i class="glyphicon glyphicon-arrow-up"></i></span>');
                } else {
                    $('#total-sales-profit').addClass('hidden');
                    $('#total-sales-loss').removeClass('hidden');
                    var loss = data.totalSalesProfitLoss * -1;
                    $('#total-sales-loss').html('+'+loss+'%<span><i class="glyphicon glyphicon-arrow-down"></i></span>');
                }

                if (data.totalGrossProfitProfitLoss > 0) {
                    $('#total-gross-profit').removeClass('hidden');
                    $('#total-gross-loss').addClass('hidden');
                    $('#total-gross-profit').html('+'+data.totalGrossProfitProfitLoss+'%<span><i class="glyphicon glyphicon-arrow-up"></i></span>');
                } else {
                    $('#total-gross-profit').addClass('hidden');
                    $('#total-gross-loss').removeClass('hidden');
                    var loss = data.totalGrossProfitProfitLoss * -1;
                    $('#total-gross-loss').html('+'+loss+'%<span><i class="glyphicon glyphicon-arrow-down"></i></span>');
                }

                if (data.totalPaymentProfitLoss > 0) {
                    $('#total-payment-profit').removeClass('hidden');
                    $('#total-payment-loss').addClass('hidden');
                    $('#total-payment-profit').html('+'+data.totalPaymentProfitLoss+'%<span><i class="glyphicon glyphicon-arrow-up"></i></span>');
                } else {
                    $('#total-payment-profit').addClass('hidden');
                    $('#total-payment-loss').removeClass('hidden');
                    var loss = data.totalPaymentProfitLoss * -1;
                    $('#total-payment-loss').html('+'+loss+'%<span><i class="glyphicon glyphicon-arrow-down"></i></span>');
                }

                if (data.invoiceCountProfitLoss > 0) {
                    $('#total-invoice-count-profit').removeClass('hidden');
                    $('#total-invoice-count-loss').addClass('hidden');
                    $('#total-invoice-count-profit').html('+'+data.invoiceCountProfitLoss+'%<span><i class="glyphicon glyphicon-arrow-up"></i></span>');
                } else {
                    $('#total-invoice-count-profit').addClass('hidden');
                    $('#total-invoice-count-loss').removeClass('hidden');
                    var loss = data.invoiceCountProfitLoss * -1;
                    $('#total-invoice-count-loss').html('+'+loss+'%<span><i class="glyphicon glyphicon-arrow-down"></i></span>');
                }


                if (data.gpPercentage > 0) {
                    $('.gpPercentage').addClass('gpPercentageProfit');
                    $('.gpPercentage').removeClass('gpPercentageLoss');
                    var endAngleOfGpChart = (data.gpPercentage / 100) * 470;
                    $('#gpPercentage').text(data.gpPercentage+'%');
                } else {
                    var endAngleOfGpChart = 0;
                    $('.gpPercentage').removeClass('gpPercentageProfit');
                    $('.gpPercentage').addClass('gpPercentageLoss');
                    $('#gpPercentage').text(data.gpPercentage*-1+'%');
                }

                gpChart(endAngleOfGpChart.toFixed(2));
                salesAndGpSummaryChart(data.totalGrossValue, data.totalCost, data.grossProfit);
                salesSummaryChart(data.openInvoiceTotal, data.overdueInvoiceTotal, data.closedInvoiceTotal);
                paymentSummaryChart(data);
             
                customerSales = data.customerSales;
                heigstSalesItemData = data.heigstSalesItemData;
                $('#cash-chart').html('');
                cashCreditChart(data.customerSales);
                $.each(data.heigstSalesItemData, function(index, val) {
                    if (index < 5) {
                        $('.productCode'+index).text(val.productName);
                        $('.productQty'+index).text(val.totalQty);
                        $('.productSales'+index).text(accounting.formatMoney(val.totalSales));
                    }
                });

                if (data.heigstSalesItemData.length < 5) {
                    for (var i = 0; i < 5; i++) {
                        if (i >= data.heigstSalesItemData.length) {
                            $('.productCode'+i).text('');
                            $('.productQty'+i).text('');
                            $('.productSales'+i).text('');
                        }
                    }
                }

                switch (period) {
                    case 'thisYear':
                        $('#last-period-total-sales').text('Compared to (LKR '+accounting.formatMoney(data.totalSalesLastYear)+' last year)');
                        $('#last-period-total-gross').text('Compared to (LKR '+accounting.formatMoney(data.lastYearGrossProfit)+' last year)');
                        $('#last-period-total-payment').text('Compared to (LKR '+accounting.formatMoney(data.lastPeriodTotalPayment)+' last year)');
                        $('#last-period-total-invoices').text('Compared to ('+data.totalInvoiceCountLastPeriod+' last year)');
                        break;
                    case 'thisMonth':
                        $('#last-period-total-sales').text('Compared to (LKR '+accounting.formatMoney(data.totalSalesLastYear)+' last month)');
                        $('#last-period-total-gross').text('Compared to (LKR '+accounting.formatMoney(data.lastYearGrossProfit)+' last month)');
                        $('#last-period-total-payment').text('Compared to (LKR '+accounting.formatMoney(data.lastPeriodTotalPayment)+' last month)');
                        $('#last-period-total-invoices').text('Compared to ('+data.totalInvoiceCountLastPeriod+' last month)');
                        break;
                    case 'thisWeek':
                        $('#last-period-total-sales').text('Compared to (LKR '+accounting.formatMoney(data.totalSalesLastYear)+' last week)');
                        $('#last-period-total-gross').text('Compared to (LKR '+accounting.formatMoney(data.lastYearGrossProfit)+' last week)');
                        $('#last-period-total-payment').text('Compared to (LKR '+accounting.formatMoney(data.lastPeriodTotalPayment)+' last week)');
                        $('#last-period-total-invoices').text('Compared to ('+data.totalInvoiceCountLastPeriod+' last week)');
                        break;
                    case 'thisDay':
                        $('#last-period-total-sales').text('Compared to (LKR '+accounting.formatMoney(data.totalSalesLastYear)+' last day)');
                        $('#last-period-total-gross').text('Compared to (LKR '+accounting.formatMoney(data.lastYearGrossProfit)+' last day)');
                        $('#last-period-total-payment').text('Compared to (LKR '+accounting.formatMoney(data.lastPeriodTotalPayment)+' last day)');
                        $('#last-period-total-invoices').text('Compared to ('+data.totalInvoiceCountLastPeriod+' last day)');
                        break;
                    default:
                        // statements_def
                        break;
                }
                
            }
        });
    }

    $('.period').on('change', function(event) {
        event.preventDefault();
        // getWidgetData($(this).val());
        updateDashboardData($(this).val());
    });


    function gpChart(endAngle)
    {
        var options1 = {
            chart: {
                height: 200,
                type: 'radialBar',
                toolbar: {
                    show: false
                }
            },
            plotOptions: {
                radialBar: {
                    startAngle: 0,
                    endAngle: parseFloat(endAngle),
                    hollow: {
                        margin: 0,
                        size: '70%',
                        background: '#fff',
                        image: undefined,
                        imageOffsetX: 0,
                        imageOffsetY: 0,
                        position: 'front',
                        dropShadow: {
                            enabled: true,
                            top: 3,
                            left: 0,
                            blur: 4,
                            opacity: 0.24
                        }
                    },
                    track: {
                        background: '#fff',
                        strokeWidth: '70%',
                        margin: 0, // margin is in pixels
                        dropShadow: {
                            enabled: true,
                            top: -3,
                            left: 0,
                            blur: 4,
                            opacity: 0.35
                        }
                    },

                    dataLabels: {
                        showOn: 'always',
                        name: {
                            offsetY: 0,
                            show: true,
                            color: '#888',
                            fontSize: '15px'
                        },
                        value: {
                            formatter: function(val) {
                                return parseInt(val);
                            },
                            color: '#111',
                            fontSize: '20px',
                            show: false,
                        }
                    }
                }
            },
            fill: {
                type: 'gradient',
                gradient: {
                    shade: 'dark',
                    type: 'horizontal',
                    shadeIntensity: 0.5,
                    gradientToColors: ['#ABE5A1'],
                    inverseColors: true,
                    opacityFrom: 1,
                    opacityTo: 1,
                    stops: [0, 100]
                }
            },
            series: [75],
            stroke: {
                lineCap: 'round'
            },
            labels: ['Gross Profit'],
        }
        var chart1 = new ApexCharts(
            document.querySelector("#target-chart"),
            options1
        );
        chart1.render();
    }

    $('#sales-gross-profit').on('click', function(event) {
        event.preventDefault();
        $('#gp-chart').removeClass('hidden');
        $('#cash-chart').addClass('hidden');
        if ($('#gp-chart')[0].children.length > 0) {
            $('#gp-chart')[0].children[0].remove();
        }
        salsVsGpChart(heigstSalesItemData);
    });

    function salsVsGpChart(heigstSalesItemData)
    {
        var totalSales = [];
        var totalProfit = [];
        var products = [];
        $.each(heigstSalesItemData, function(index, val) {
            if (index < 10) {
                totalSales.push(val.totalSales);
                var profit = (val.profit == undefined) ? 0 : val.profit;
                totalProfit.push(profit);
                products.push(val.productName);
            }
        });

        var colWidth = '50%';

        if (products.length < 3) {
            colWidth = '10%';
        }

        var options = {
            chart: {
                height: 350,
                type: 'bar',
            },
            plotOptions: {
                bar: {
                    columnWidth: colWidth,   
                }
            },
            dataLabels: {
                enabled: false
            },
            stroke: {
                width: 2
            },
            series: [{
                name: 'Total Sales',
                data: totalSales
            }, {
                name: 'Total Profit',
                data: totalProfit
            }],
            grid: {
                row: {
                    colors: ['#fff', '#f2f2f2']
                }
            },
            xaxis: {
                labels: {
                    rotate: -45
                },
                categories: products,
            },
            yaxis: {
                labels: {
                    formatter: (value) => { return currencySymbol+" "+accounting.formatMoney(value) },
                }
            },
            tooltip: {
                x: {
                    format: 'dd/MM/yy HH:mm'
                },
                y: {
                    formatter: function(value, { series, seriesIndex, dataPointIndex, w }) {
                        return currencySymbol+" "+accounting.formatMoney(value)
                    }
                }
            },
            fill: {
                type: 'gradient',
                gradient: {
                    shade: 'light',
                    type: "horizontal",
                    shadeIntensity: 0.25,
                    gradientToColors: undefined,
                    inverseColors: true,
                    opacityFrom: 0.85,
                    opacityTo: 0.85,
                    stops: [50, 0, 100]
                },
            },
        }




        var chart = new ApexCharts(
            document.querySelector("#gp-chart"),
            options
        );

        chart.render();
    }



    $('#cash-credit').on('click', function(event) {
        event.preventDefault();
        $('#cash-chart').removeClass('hidden');
        $('#gp-chart').addClass('hidden');
        if ($('#cash-chart')[0].children.length > 0) {
            $('#cash-chart')[0].children[0].remove();
        }
        $('#gp-chart').html('');
        cashCreditChart(customerSales);
    });    

    function cashCreditChart(customerSales)
    {
        console.log(customerSales);
        $('#cash-chart').html('');
        var totalSales = [];
        var totalReturn = [];
        var customers = [];
        var options = {};
        var i = 0;
        
        $.each(customerSales, function(index, val) {
            if (i < 10) {
                var totalInvAmount = (val.totalInvAmount == undefined) ? 0 : val.totalInvAmount;
                totalSales.push(totalInvAmount);
                var totalReturnAmount = (val.totalReturnAmount == undefined) ? 0 : val.totalReturnAmount;
                totalReturn.push(totalReturnAmount);
                customers.push(val.customerName);
            }
            i++;
        });

        var colWidth = '50%';

        if (customers.length < 3) {
            colWidth = '10%';
        }

        options = {
            chart: {
                height: 350,
                type: 'bar',
            },
            plotOptions: {
                bar: {
                    columnWidth: colWidth,   
                }
            },
            dataLabels: {
                enabled: false
            },
            stroke: {
                width: 2
            },
            series: [{
                name: 'Total Sales',
                data: totalSales
            }, {
                name: 'Total Returns',
                data: totalReturn
            }],
            grid: {
                row: {
                    colors: ['#fff', '#f2f2f2']
                }
            },
            xaxis: {
                labels: {
                    rotate: -45
                },
                categories: customers,
            },
            yaxis: {
                title: {
                    text: 'Stock Value',
                },
                labels: {
                    formatter: (value) => { return currencySymbol+" "+accounting.formatMoney(value) },
                }
            },
            tooltip: {
                x: {
                    format: 'dd/MM/yy HH:mm'
                },
                y: {
                    formatter: function(value, { series, seriesIndex, dataPointIndex, w }) {
                        return "Rs "+accounting.formatMoney(value)
                    }
                }
            },
            fill: {
                type: 'gradient',
                gradient: {
                    shade: 'light',
                    type: "horizontal",
                    shadeIntensity: 0.25,
                    gradientToColors: undefined,
                    inverseColors: true,
                    opacityFrom: 0.85,
                    opacityTo: 0.85,
                    stops: [50, 0, 100]
                },
            },
        }

        renderChart(options, 'cash-chart');
    }


    function renderChart(options, chartID)
    {
        var chart = new ApexCharts(
            document.querySelector("#"+chartID),
            options
        );
        chart.render();
    }


    function salesAndGpSummaryChart(totalGrossValue, cost, gross)
    {

        let chartConfig = {
            graphset : [

            ]
        };

        var gdata = {
            x : 0*200,
            y : 0*200,
            width : '100%',
            height : '100%',
            type : 'pie',
            noData:{
                text:"Currently there is no data in the chart",
                backgroundColor: "#5081e7",
                fontSize:18,
                textAlpha:.9,
                alpha:.6,
                bold:true
            },
            plotarea : {
                margin : '15 5 5 5'
            },
            "scale":{
                "size-factor":0.9
            },
            labels:[
                {
                    text:currencySymbol+" "+accounting.formatMoney(totalGrossValue),
                    x:"50%", 
                    y:"47%",
                    anchor : 'c',
                    fontSize:"17px",
                    fontStyle : 'bold',
                    fontColor : 'black',
                    padding : "10px",
                },
                {
                    text:"Total <br> Sales by Gross Profit",
                    x:"50%", 
                    y:"58%",
                    fontSize:"13px",
                    anchor : 'c',
                    fontStyle : 'bold',
                    fontColor : 'grey',
                    padding : "10px",
                },
            ],
            plot : {
                borderWidth : 2,
                borderColor : 'white',
                shadow : true,
                shadowAlpha : 1,
                shadowAngle : -45,
                borderAlpha : 0.5,
                slice : '89%',
                pieTransform : 'flow=4',
                hoverState : {
                    visible : false    
                },
                tooltip : {
                    text : '%t <br> '+currencySymbol+' %node-value',
                    thousandsSeparator:','
                },
                'background-color': "black",
                'value-box': {
                    text: "%t <br>"+currencySymbol+" %v",
                    'font-color': "black",
                    'font-size': 10,
                    'background-color': "white",
                    borderColor : 'white',
                    shadow : true,
                    shadowAlpha : 1,
                    shadowAngle : -45,
                    borderAlpha : 0.5,
                    'border-width': 1,
                    padding: "5%",
                    borderRadius : '5px',
                    placement:'out',
                    thousandsSeparator:',',
                    offsetY: -10,
                }
            },
            series:[
            ]
        };

        if (cost > 0) {
            var series = {
                "values":[cost],  
                "text":"Cost",  
                "background-color":"#F76B1C",
            }
            gdata.series.push(series);
        }

        if (gross > 0) {
            var series = {
                "values":[gross],  
                "text":"Gross Profit",  
                "background-color":"#1298FF",
            }
            gdata.series.push(series);
        }



        chartConfig.graphset.push(gdata);

        zingchart.render({
            id: 'sales-gp-summary',
            width: '100%',
            height: '100%',
            data: chartConfig
        });

        if (cost == 0 && gross == 0) {
            $('#sales-gp-summary .zc-text').remove();
        }
    }


    function salesSummaryChart(openInvoiceTotal, overdueTotal, closedTotal)
    {
        let chartConfig = {
            graphset : [

            ]
        };

        var gdata = {
            x : 0*200,
            y : 0*200,
            width : '100%',
            height : '100%',
            type : 'pie',
            noData:{
                text:"Currently there is no data in the chart",
                backgroundColor: "#5081e7",
                fontSize:18,
                textAlpha:.9,
                alpha:.6,
                bold:true
            },
            plotarea : {
                margin : '15 5 5 5'
            },
            "scale":{
                "size-factor":0.9
            },
            labels:[
                {
                    text:currencySymbol+" "+accounting.formatMoney(openInvoiceTotal + overdueTotal + closedTotal),
                    x:"50%", 
                    y:"47%",
                    anchor : 'c',
                    fontSize:"17px",
                    fontStyle : 'bold',
                    fontColor : 'black',
                    padding : "10px",
                },
                {
                    text:"Total <br> Sales Summary",
                    x:"50%", 
                    y:"58%",
                    fontSize:"13px",
                    anchor : 'c',
                    fontStyle : 'bold',
                    fontColor : 'grey',
                    padding : "10px",
                },
            ],
            plot : {
                borderWidth : 2,
                borderColor : 'white',
                shadow : true,
                shadowAlpha : 1,
                shadowAngle : -45,
                borderAlpha : 0.5,
                slice : '89%',
                pieTransform : 'flow=4',
                hoverState : {
                    visible : false    
                },
                tooltip : {
                    text : '%t <br> '+currencySymbol+' %node-value',
                    thousandsSeparator:','
                },
                'background-color': "black",
                'value-box': {
                    text: "%t <br> "+currencySymbol+" %v",
                    'font-color': "black",
                    'font-size': 10,
                    'background-color': "white",
                    borderColor : 'white',
                    shadow : true,
                    shadowAlpha : 1,
                    shadowAngle : -45,
                    borderAlpha : 0.5,
                    'border-width': 1,
                    padding: "5%",
                    borderRadius : '5px',
                    placement:'out',
                    thousandsSeparator:',',
                    // offsetX: 15,
                    offsetY: -10,
                }
            },
            series:[  
            ]
        };

        if (closedTotal > 0) {
            var series = {
                "values":[closedTotal],  
                "text":"Completed",  
                "background-color":"#3023AE",
            }
            gdata.series.push(series);
        }

        if (openInvoiceTotal > 0) {
            var series = {
                "values":[openInvoiceTotal],  
                "text":"Open",  
                "background-color":"#F76B1C",
            }
            gdata.series.push(series);
        }

        if (overdueTotal > 0) {
            var series = {
                "values":[overdueTotal],  
                "text":"Due",  
                "background-color":"#1298FF",
            }
            gdata.series.push(series);
        }

        chartConfig.graphset.push(gdata);

        zingchart.render({
            id: 'sales-summary',
            width: '100%',
            height: '100%',
            data: chartConfig
        });

        if (closedTotal == 0 && openInvoiceTotal == 0 && overdueTotal == 0) {
            $('#sales-summary .zc-text').remove();
        }
    }

    function paymentSummaryChart(paymentData)
    {

        let chartConfig = {
            graphset : [

            ]
        };

        var gdata = {
            x : 0*200,
            y : 0*200,
            width : '100%',
            height : '100%',
            type : 'pie',
            noData:{
                text:"Currently there is no data in the chart",
                backgroundColor: "#5081e7",
                fontSize:18,
                textAlpha:.9,
                alpha:.6,
                bold:true
            },
            plotarea : {
                margin : '15 5 5 5'
            },
            "scale":{
                "size-factor":0.9
            },
            labels:[
                {
                    text:currencySymbol+" "+accounting.formatMoney(paymentData.cashPaymentAmount + paymentData.chequePaymentAmount + paymentData.cardPaymentAmount + paymentData.loyaltyPaymentAmount + paymentData.bankTransferPaymentAmount + paymentData.giftCardPaymentAmount + paymentData.lcPaymentAmount + paymentData.ttPaymentAmount + paymentData.creditPaymentAmount),
                    x:"50%", 
                    y:"47%",
                    anchor : 'c',
                    fontSize:"17px",
                    fontStyle : 'bold',
                    fontColor : 'black',
                    padding : "10px",
                },
                {
                    text:"Total <br> Payments",
                    x:"50%", 
                    y:"58%",
                    fontSize:"13px",
                    anchor : 'c',
                    fontStyle : 'bold',
                    fontColor : 'grey',
                    padding : "10px",
                },
            ],
            plot : {
                borderWidth : 2,
                borderColor : 'white',
                shadow : true,
                shadowAlpha : 1,
                shadowAngle : -45,
                borderAlpha : 0.5,
                slice : '89%',
                pieTransform : 'flow=4',
                hoverState : {
                    visible : false    
                },
                tooltip : {
                    text : '%t <br> '+currencySymbol+' %node-value',
                    thousandsSeparator:','
                },
                'background-color': "black",
                'value-box': {
                    text: "%t <br> "+currencySymbol+" %v",
                    'font-color': "black",
                    'font-size': 10,
                    'background-color': "white",
                    borderColor : 'white',
                    shadow : true,
                    shadowAlpha : 1,
                    shadowAngle : 45,
                    borderAlpha : 0.5,
                    'border-width': 1,
                    padding: "5%",
                    borderRadius : '5px',
                    placement:'out',
                    thousandsSeparator:',',
                }
            },
            series:[
            ]
        };


        if (paymentData.cashPaymentAmount > 0) {
            var series = {
                "values":[paymentData.cashPaymentAmount],  
                "text":"Cash",  
                "background-color":"#3023AE",
            }
            gdata.series.push(series);
        }

        if (paymentData.chequePaymentAmount > 0) {
            var series = {
                "values":[paymentData.chequePaymentAmount],  
                "text":"Cheque",  
                "background-color":"#F76B1C",
            }
            gdata.series.push(series);
        }

        if (paymentData.cardPaymentAmount > 0) {
            var series = {
                "values":[paymentData.cardPaymentAmount],  
                "text":"Card",  
                "background-color":"#1298FF",
            }
            gdata.series.push(series);
        }

        if (paymentData.loyaltyPaymentAmount > 0) {
            var series = {
                "values":[paymentData.loyaltyPaymentAmount],  
                "text":"Loyalty Card",  
                "background-color":"#ff6666",
            }
            gdata.series.push(series);
        }

        if (paymentData.bankTransferPaymentAmount > 0) {
            var series = {
                "values":[paymentData.bankTransferPaymentAmount],  
                "text":"Bank Transfer",  
                "background-color":"#ff99cc",
            }
            gdata.series.push(series);
        }

        if (paymentData.giftCardPaymentAmount > 0) {
            var series = {
                "values":[paymentData.giftCardPaymentAmount],  
                "text":"Gift Card",  
                "background-color":"#cc99ff",
            }
            gdata.series.push(series);
        }

        if (paymentData.lcPaymentAmount > 0) {
            var series = {
                "values":[paymentData.lcPaymentAmount],  
                "text":"LC",  
                "background-color":"#6699ff",
            }
            gdata.series.push(series);
        }

        
        if (paymentData.ttPaymentAmount > 0) {
            var series = {
                "values":[paymentData.ttPaymentAmount],  
                "text":"TT",  
                "background-color":"red",
            }
            gdata.series.push(series);
        }

        if (paymentData.creditPaymentAmount > 0) {
            var series = {
                "values":[paymentData.creditPaymentAmount],  
                "text":"Credit",  
                "background-color":"green",
            }
            gdata.series.push(series);
        }

        chartConfig.graphset.push(gdata);

        zingchart.render({
            id: 'sales-payment-summary',
            width: '100%',
            height: '100%',
            data: chartConfig
        });
        if (paymentData.cashPaymentAmount == 0 && paymentData.chequePaymentAmount == 0 && paymentData.cardPaymentAmount == 0 && paymentData.loyaltyPaymentAmount == 0 && paymentData.bankTransferPaymentAmount == 0 && paymentData.giftCardPaymentAmount == 0 && paymentData.lcPaymentAmount == 0 && paymentData.ttPaymentAmount == 0 && paymentData.creditPaymentAmount == 0) {
            $('#sales-payment-summary .zc-text').remove();
        }
    }
});


