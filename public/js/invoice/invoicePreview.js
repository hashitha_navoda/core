/**
 * @author Prathap Weerasinghe <prathap@thinkcube.com>
 * This js contain Quotatin view related functions
 */

$(document).ready(function() {
    $("#q_print").on("click", function() {
        window.print();
    });
    $("#q_email").on("click", function() {
        $("#email-modal").modal("show");
        $("#send").on("click", function() {
            var state = validateEmail($("#email_to").val(), $("#email-body").html());
            if (state) {
                var param = {
                    qout_id: $(this).attr("data-qot_id"),
                    to_email: $("#email_to").val(),
                    subject: $("#email_sub").val(),
                    body: $("#email-body").html()
                };
                var mail = eb.post("/invoiceAPI/sendInvoice", param);
                mail.done(function(rep) {
                    if (rep.error) {
                        p_notification(false, eb.getMessage('ERR_PAY_EMAIL_SENT'));
                    } else {
                        p_notification(true, eb.getMessage('SUCC_PAY_EMAIL_SENT'));
                    }
                });
                $("#email-modal").modal("hide");
            }
        });
    });
    $("#new_inv").on("click", function() {
        window.location.assign(BASE_URL + "/invoice/index");
    });
    $("#to_pay").on("click", function() {
        window.location.assign(BASE_URL + "/customerPayments/index/" + $(this).attr("data-inv_id"));
    });
    $("#rec_inv").on("click", function() {
        window.location.assign(BASE_URL + "/invoice/recurentInvoice/" + $(this).attr("data-inv_id"));
    });
    $("#cancel").on("click", function() {
        window.history.back(-1);
    });
    var invNO = $('#invoice_history_div').attr("invID");
    var invoiceHistoryURL = BASE_URL + "/invoiceAPI/getInvoiceHistory";
    eb.ajax({
        type: 'POST',
        url: invoiceHistoryURL,
        data: {invNo: invNO},
        success: function(res) {
            $('#invoice_history').html("");
            $('#invoice_Payement_history').html("");
            for (var i in res.invHistory) {
                if (jQuery.isArray(res.invHistory[i])) {
                    for (var j in res.invHistory[i]) {
                        $('#invoice_payment_history').append(res.invHistory[i][j]);
                        $('#invoice_payment_history').append("<br/>");
                    }
                } else {
                    $('#invoice_history').append(res.invHistory[i]);
                }
                $('#invoice_history').append("<br/>");
            }
        },
        async: false
    });
});