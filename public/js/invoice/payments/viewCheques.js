$(document).ready(function() {
    //for hide customer search
    $('#pay-custa-search').selectpicker('hide');

    //for datepicker
    var fromTimeStamp;
    var toTimeStamp;
    var customerId;
    var paymentId;

    var fromDate = $('#from-date').datepicker({
        format: 'yyyy-mm-dd'
    }).on('changeDate', function(e) {
        fromDate.hide();
        fromTimeStamp = e.date.valueOf();
        $('#to-date')[0].focus();
        $('#to-date').val($('#from-date').val());
    }).data('datepicker');

    var toDate = $('#to-date').datepicker({
        format: 'yyyy-mm-dd'
    }).on('changeDate', function(e) {
        toTimeStamp = e.date.valueOf();
        toDate.hide();
    }).data('datepicker');

    //for customer cheques search box
    $('#pay-search-select').on("change", function() {

        if (this.value == 'Payments No') {
            $('#pay-custa-search').selectpicker('hide')
            $('#pay-search').selectpicker('show');

        } else if (this.value == 'Customer Name') {
            $('#pay-search').selectpicker('hide');
            $('#pay-custa-search').selectpicker('show');
        }
    });

    //for load all the customers
    loadDropDownFromDatabase('/customerAPI/search-customers-for-dropdown', "", "withDeactivatedCustomers", '#pay-custa-search');
    $('#pay-custa-search').selectpicker('refresh');
    $('#pay-custa-search').on('change', function() {
        if ($(this).val() > 0 && customerId != $(this).val()) {
            customerId = $(this).val();
            eb.ajax({
                type: 'POST',
                url: BASE_URL + '/customerPaymentsAPI/getCustomersChequesByCustomerID',
                data: {customerId: customerId},
                success: function(data) {
                    $('#paymentsview').html(data);
                }
            });
        }
    });

    //for load
    loadDropDownFromDatabase('/customerPaymentsAPI/getCustomerChequePayments', "", 0, '#pay-search');
    $('#pay-search').selectpicker('refresh');
    $('#pay-search').on('change', function(e) {
        e.preventDefault();
        if ($(this).val() > 0 && paymentId != $(this).val()) {
            paymentId = $(this).val();
            eb.ajax({
                type: 'POST',
                url: BASE_URL + '/customerPaymentsAPI/getCustomersChequesByPaymentID',
                data: {paymentId: paymentId},
                success: function(data) {
                    $('#paymentsview').html(data);
                }
            });
        }
    });

    //for search filtered results
    $('#filter-button').on('click', function(e) {
        var fromDate = $('#from-date').val();
        var toDate = $('#to-date').val();

        if (fromTimeStamp > toTimeStamp) {
            p_notification(false, 'Please enter valid date range');
        } else {
            var filterURL = BASE_URL + '/customerPaymentsAPI/getCustomersChequesByFilter';
            var filterrequest = eb.post(filterURL, {
                'fromdate': fromDate,
                'todate': toDate,
            });
            filterrequest.done(function(data) {
                $('#paymentsview').html(data);
            });
        }
    });
    //for sort by
    $(".sortbtn").on('click',function() {
        eb.ajax({
            type: 'POST',
            url: BASE_URL + '/customerPaymentsAPI/getCustomersChequesByFilter',
            data: {sortValue: $(this).data('id')},
            success: function(data) {
                $('#paymentsview').html(data);
            }
        });
        if ($(this).data('id') == "Date") {
                    document.getElementById("dataBtn").disabled = true;
        } else if ($(this).data('id') == "Payment No") {
                    document.getElementById("paymentBtn").disabled = true;
        } else if ($(this).data('id') == "Cheque Date"){
                    document.getElementById("chequeBtn").disabled = true;
        }
    });
});