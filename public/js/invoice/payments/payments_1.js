
/**
 * @author ASHAN <ashan@thinkcube.com>
 * This file contains form validation
 */
var locationID = '';
var idInvoice = '';
var custIDPay = '';
var invocieID;
var selectedCustomerID;
var methods = [];
var loyalty = {};

loyalty.isAvailable = function() {
    return loyalty.cust_loyal_id != undefined ? true : false;
};

loyalty.calculateLoyaltyPoints = function(paidAmount, totalBill) {
    if (loyalty.min <= totalBill && loyalty.max >= totalBill) {
        loyalty.earned_points = totalBill / loyalty.earning_ratio;
    }
    else if (loyalty.max <= totalBill) {
        loyalty.earned_points = loyalty.max / loyalty.earning_ratio;
    }
};

loyalty.isValid = function(paidAmount, totalBill) {

    loyalty.earned_points = 0;
    loyalty.redeemed_points = 0;

    if (!loyalty.isAvailable) {
        return false;
    }


    if (loyalty.available_points * loyalty.redeem_ratio >= paidAmount) {
        loyalty.redeemed_points = paidAmount / loyalty.redeem_ratio;
        return true;
    }
    return false;
};

loyalty.getLoyaltySaveData = function() {
    if (!loyalty.isAvailable()) {
        return {};
    }

    return {
        cust_loyalty_id: loyalty.cust_loyal_id,
        earned_points: loyalty.earned_points,
        redeemed_points: loyalty.redeemed_points,
        current_points: loyalty.available_points,
    };
};

$(document).ready(function() {

    invoiceLeftToPay = new Array();
    var flag = 0;
    var cus_change = false;
    var addpayment = BASE_URL + '/customerPaymentsAPI/addAllPayment';
    var url = BASE_URL + '/customerPaymentsAPI/check-credit';
    var paymentTotal = 0.00;
    var restBalance = 0.00;
    var paymentMethodIncrement = 1;
    var oldDiscountAmount = 0.00;


    $('#InvoiceID').selectpicker('hide');
    $('#invoiceCustomer').hide();
    $('#invoiceSearchType').on('change', function(e) {
        $selection = $('#invoiceSearchType').val();
        if ($selection == 'Customer Name') {
            $('#customerID').selectpicker('show');
            $('#customerID').selectpicker('render');
            $('#InvoiceID').selectpicker('hide');
            $('#InvoiceID').selectpicker('render');
        } else if ($selection == 'Invoice ID') {
            $('#InvoiceID').selectpicker('show');
            $('#InvoiceID').selectpicker('render');
            $('#customerID').selectpicker('hide');
            $('#customerID').selectpicker('render');
        }

    });

    $('#reset-customer-invoiceId-button').on('click', function() {
        location.reload();
    });

    locationID = $('#idOfLocation').val();

    $("#paymentbill").hide();
    $("#creditGroup").hide();
    $("#checkGroup").hide();
    $("#bankTransferGroup").hide();
    $("#giftCardGroup").hide();

    $('#advance').on('click', function(e) {
        e.preventDefault();
        window.location.assign(BASE_URL + '/customerPayments/advancePayment');
    });


    loadDropDownFromDatabase('/invoice-api/search-open-sales-invoice-for-payments-dropdown', "", 0, '#InvoiceID');

    $('#InvoiceID').selectpicker('refresh');
    $('#InvoiceID').on('change', function() {
        if ($(this).val() > 0 && invocieID != $(this).val()) {
            invocieID = $(this).val();
            changeEvent(invocieID, "Invoice");
        }
    });


    loadDropDownFromDatabase('/customerAPI/search-customers-for-dropdown', "", 0, '#customerID');

    $('#customerID').selectpicker('refresh');
    $('#customerID').on('change', function() {
        if ($(this).val() > 0 && selectedCustomerID != $(this).val()) {
            selectedCustomerID = $(this).val();
            var customerID = $(this).val();
            changeEvent(customerID, "Customer");
            setLoyaltyData(customerID)
        }
    });

    function setLoyaltyData(custID) {
        eb.ajax({
            type: 'post',
            url: BASE_URL + '/customerPaymentsAPI/getCustomerLoyaltyCard',
            data: {customerID: custID},
            success: function(res) {
                if (res['status']) {
                    loyalty.available_points = res.data['available_points'];
                    loyalty.card_name = res.data['card_name'];
                    loyalty.code = res.data['code'];
                    loyalty.cust_loyal_id = res.data['cust_loyal_id'];
                    loyalty.earning_ratio = res.data['earning_ratio'];
                    loyalty.max = res.data['max'];
                    loyalty.min = res.data['min'];
                    loyalty.redeem_ratio = res.data['redeem_ratio'];

                    //
                    $('#loyalty_name').text(loyalty.card_name);
                    $('#loyalty_code').text(loyalty.code);
                    $('#loyalty_current_points').text(loyalty.available_points);
                    $('#loyalty_current_points_val').text(loyalty.available_points * loyalty.redeem_ratio);
                    $('#loyalty_details').removeClass('hidden');
                } else {
                    $('#loyalty_details').addClass('hidden');
                    loyalty.available_points = undefined;
                    loyalty.card_name = undefined;
                    loyalty.code = undefined;
                    loyalty.cust_loyal_id = undefined;
                    loyalty.earning_ratio = undefined;
                    loyalty.max = undefined;
                    loyalty.min = undefined;
                    loyalty.redeem_ratio = undefined;
//                                p_notification(false, "Wrong Invoice ID.");
                }
            },
            async: false
        });
    }


    function changeEvent(key, type) {
        clearPaymentScreen();
        flag++;
        $('#paymentbill').addClass('margin_top margin_bottom');
        $("#crnote").hide();
        if (type == 'Customer') {
            $('#InvoiceID').val('');
            value = "custome=" + key;
            customerID = key;
        } else if (type == 'Invoice') {
            $('#currentCredit').val('0.00');
            value = "invoice=" + key;
            if ($('#InvoiceID').val() != '') {
                var inv = BASE_URL + '/customerPaymentsAPI/checkInvoice';
                eb.ajax({
                    type: 'post',
                    url: inv,
                    data: {invoiceID: key},
                    success: function(invoicedata) {
                        if (invoicedata['state'] == 'true') {
                            if (invoicedata['value'].state == 'full') {
                                p_notification(false, eb.getMessage('SUCC_PAY_FULL_PAYMENT'));
                            } else {
                                customerID = invoicedata['value'].customerID;
                            }
                        } else {
//                                p_notification(false, "Wrong Invoice ID.");
                        }
                    },
                    async: false
                });
            }
        }

        $("#paymentbill").show();
        if (cus_change) {
            var s = document.getElementById('paymentbill');
            s.value = '';
        }
        cus_change = true;
        $('#appendtable').html('');
        eb.ajax({
            type: 'post',
            url: BASE_URL + '/customerPaymentsAPI/invoice?id=' + value,
            success: function(data) {
                $('#appendtable').html(data);
            },
            async: false
        });

        var amount;
        var discount;
        var totalamount;
        $('#discountAmount').keyup(function() {
            if ($('#amount').val()) {
                if (isNaN($('#discountAmount').val())) {
                    $('#discountAmount').val('');
                    p_notification(false, eb.getMessage('ERR_PAY_DISCOUNT_NUMR'));
                    $('#totalpayment').html(numberWithCommas(parseFloat($('#amount').val().replace(/,/g, '')).toFixed(2)));
                } else {
                    amount = $('#amount').val().replace(/,/g, '');
                    if (parseFloat($('#discountAmount').val()) >= parseFloat(amount)) {
                        p_notification(false, eb.getMessage('ERR_PAY_DISCOUNT_AMOUNT'));
                        $('#discountAmount').val('');
                        $('#totalpayment').html(numberWithCommas(parseFloat(amount).toFixed(2)));
                    } else {
                        discount = $('#discountAmount').val().replace(/,/g, '');
                        totalamount = amount - discount;
                        $('#totalpayment').html(numberWithCommas(parseFloat(totalamount).toFixed(2)));
                    }
                }
            } else {
                p_notification(false, eb.getMessage('ERR_PAY_ENTER_AMOUNT'));
                $('#discountAmount').val('');
            }
        }).attr('autocomplete', 'off');

        eb.ajax({
            type: 'post',
            url: url,
            data: {customerID: value},
            dataType: "json",
            async: false,
            success: function(values) {
                if (values[0] != null) {
                    customercredit = values[0];
                    $('#currentCredit').val(numberWithCommas(parseFloat(values[0]).toFixed(2)));
                } else {
                    $('#currentCredit').val("0.00");
                }

                if (values[3] != null) {
                    customerbalance = values[3];
                    $('#currentBalance').val(numberWithCommas(parseFloat(values[3]).toFixed(2)));
                } else {
                    $('#currentBalance').val("0.00");
                }
                if (values[1] !== null) {
                    $('#customerID').val(values[1]);
                    $("#invoiceCustomer").show();
                    $("#invoiceCustomerName").text(values[1]);
                } else {
                    p_notification(false, eb.getMessage('ERR_PAY_CUST_NOTEXIST'));
                    $('#customerID').val('');
                }
                if (values[2] !== null) {
                    $('#paymentTerm').val(values[2]);
                }
                creditnotevalue = 0;
                creditcustomerID = 0;
                currentcredit = 0;
                newbalance = 0;
            }
        });
        $('#creditNote').val('');
        $('#creditNotecheck').prop('checked', false);
    }

    $(document).on('click', '.edi', function() {
        id = (this.parentNode).parentNode.getAttribute('id');
        if (!invoiceLeftToPay[id]) {
            var newlaloc = $('#' + id + " .ttt").html().replace(/,/g, '');
            $(this).val(newlaloc);
            $('#' + id + " .ttt").html('0.00');
            invoiceLeftToPay[id] = newlaloc;
            setPaymentTotal();
        }
    });

    $(document).on('change', '.edi', function() {
        var amount = parseFloat($(this).val());
        id = (this.parentNode).parentNode.getAttribute('id');
        if (amount < 0 || isNaN(amount)) {
            p_notification(false, eb.getMessage('ERR_PAY_AMOUNT_SHOUDBE_NUMBER'));
            amount = 0.00;
        } else if (amount > parseFloat(invoiceLeftToPay[id])) {
            p_notification(false, eb.getMessage('ERR_PAY_TINVPAI_AM_CANT_BE_MORE_THAN_LEFTPAID'));
            amount = 0.00;
        }
        $(this).val(amount);
        var newlaloc = numberWithCommas(parseFloat(invoiceLeftToPay[id] - amount));
        $('#' + id + " .ttt").html(newlaloc);
        if (amount == 0.00) {
            delete invoiceLeftToPay[id];
        }
        setPaymentTotal();
    });

    function setPaymentTotal() {
        var totalOfPayment = 0.00;

        $('#paymentbill').find('tbody > tr').each(function() {
            if ($(this).find('.edi').val()) {
                totalOfPayment += parseFloat($(this).find('.edi').val());
            }
        });
        paymentTotal = totalOfPayment;
        oldDiscountAmount = $('#discountAmount').val();
        if (oldDiscountAmount != 0 && paymentTotal != 0.00) {
            paymentTotal = paymentTotal - oldDiscountAmount;
        }
        $('#amount').val(numberWithCommas(totalOfPayment.toFixed(2)));
        $('#restToPaid').val(numberWithCommas(paymentTotal.toFixed(2)));
        $('#totalpayment').html(numberWithCommas(paymentTotal.toFixed(2)));

        var customerCurrentBalance = customerbalance - totalOfPayment;
        $('#currentBalance').val(numberWithCommas(customerCurrentBalance.toFixed(2)));

        setCreditAmountAndRestToPaid();

    }

    $('#creditAmount').on('change', function() {
        var creditAmount = $(this).val();
        if (customercredit == 0.00) {
            p_notification(false, eb.getMessage('ERR_PAY_CUST_NOCRDT_BAL'));
            $(this).val(0.00);
        } else if (isNaN(creditAmount) || creditAmount < 0) {
            p_notification(false, eb.getMessage('ERR_PAY_CREDIT_AMOUNT_SHOUDBE_NUMBER'));
            $(this).val(0.00);
        } else {
            var totalPaidAmount = parseFloat(getTotalPaidAmount());
            var creditAmountCanPay = paymentTotal - totalPaidAmount;
            if (creditAmountCanPay < creditAmount) {
                p_notification(false, eb.getMessage('ERR_PAY_CAMOUNT_AM_CANT_BE_MORE_THAN_RESTPAID'));
                $(this).val(0.00);
            }
        }
        setCreditAmountAndRestToPaid();

    });

    $('#discountAmount').on('change', function() {
        var discount = $('#discountAmount').val();
        if (discount < 0 || isNaN(discount)) {
            p_notification(false, eb.getMessage('ERR_PAY_DISCOUNT_AMOUNT_SHOUDBE_NUMBER'));
            $(this).val(0.00);
        } else {
            if (paymentTotal <= discount) {
                p_notification(false, eb.getMessage('ERR_PAY_DISCOUNT_AMOUNT'));
                $(this).val(0.00);
            } else {
                var creditAmount = $('#creditAmount').val();
                var totalPaidAmount = parseFloat(getTotalPaidAmount());
                var discountCanGive = paymentTotal - totalPaidAmount - creditAmount + oldDiscountAmount;
                if (discountCanGive < discount) {
                    p_notification(false, eb.getMessage('ERR_PAY_DISC_AM_CANT_BE_MORE_THAN_RESTPAID'));
                    $(this).val(0.00);
                }
            }
        }
        setPaymentTotal();
    });

    function setCreditAmountAndRestToPaid() {
        var creditAmount = $('#creditAmount').val();
        var customerCreditAmount = 0.00;
        var paidAmount = 0.00;
        if (creditAmount > paymentTotal) {
            $('#creditAmount').val(paymentTotal);
            creditAmount = paymentTotal;
        }

        if (parseFloat(creditAmount) > parseFloat(customercredit)) {
            p_notification(false, eb.getMessage('ERR_PAY_CUST_CREDIT_CNT_MO_THAN_AMOUNT'));
            $('#creditAmount').val(0.00);
            creditAmount = 0.00;
            customerCreditAmount = customercredit;
        } else {
            customerCreditAmount = (customercredit - creditAmount).toFixed(2);
        }

//get the total of paid amount form many payment methods
        var totalPaidAmount = getTotalPaidAmount();

        paidAmount = paymentTotal - creditAmount - totalPaidAmount;
        $('#currentCredit').val(numberWithCommas(customerCreditAmount));
        $('#restToPaid').val(numberWithCommas(paidAmount.toFixed(2)));
    }

    $(document).on('change', '.paymentMethod', function() {
        var paymentMethods = $(this).val();
        var $payMethod = $(this).parents('.payMethod');
        $("#paidAmount", $payMethod).attr('disabled', false);
        $("#creditGroup", $payMethod).hide();
        $("#checkGroup", $payMethod).hide();
        $("#bankTransferGroup", $payMethod).hide();
        $("#giftCardGroup", $payMethod).hide();

        if (paymentMethods == 3) {
            $("#creditGroup", $payMethod).show();
        } else if (paymentMethods == 2) {
            $(this).parents('.payMethod').find("#creditGroup").hide();
            $(this).parents('.payMethod').find("#checkGroup").show();
            $(this).parents('.payMethod').find("#bankTransferGroup").hide();
        } else if (paymentMethods == 4) {

            if (loyalty.isAvailable()) {
                $(this).parents('.payMethod').find("#creditGroup").hide();
                $(this).parents('.payMethod').find("#checkGroup").hide();
                $(this).parents('.payMethod').find("#bankTransferGroup").hide();
            } else {
                p_notification(false, eb.getMessage('ERR_DOESNT_HAVE_LOYALTY_CARD'));
                $('.paymentMethod').val('1').change();

            }

            $("#checkGroup", $payMethod).show();
        } else if (paymentMethods == 5) {
            $("#bankTransferGroup", $payMethod).show();
        } else if (paymentMethods == 6) {
            $("#giftCardGroup", $payMethod).show();
            $("#paidAmount", $payMethod).val('').attr('disabled', true);
            $("#giftCardID", $payMethod).val('');
        }
    });

    $(document).on('change', '.bankID', function() {
        var bankdiv = $(this).parents('#bankTransferGroup');
        var bankID = $(this).val();
        var bank = $(this);
        var bankName = $("option:selected", $(this)).text();
        bankdiv.find('.accountID').find('option').each(function() {
            if ($(this).val()) {
                $(this).remove();
            }
        });
        if (bankID) {
            $('.main_div').addClass('Ajaxloading');
            eb.ajax({
                type: 'post',
                url: BASE_URL + '/account-api/bankAccountListForDropdown',
                data: {
                    bankId: bankID,
                },
                dataType: "json",
                async: false,
                success: function(data) {
                    if (data['status'] == true) {
                        if ($.isEmptyObject(data['data'].list)) {
                            p_notification(false, eb.getMessage('ERR_DOESNT_HAVE_ANY_ACC_FOR_THIS_BANK', bankName));
                            $(bank).val('');
                            bankdiv.find('.accountID').attr('disabled', true);
                        } else {
                            $.each(data['data'].list, function(key, value) {
                                var option = "<option value=" + value.value + ">" + value.text + "</option>";
                                bankdiv.find('.accountID').attr('disabled', false);
                                bankdiv.find('.accountID').append(option);
                            });
                        }
                    }
                    $('.main_div').removeClass('Ajaxloading');
                },
            });
        } else {
            bankdiv.find('.accountID').attr('disabled', true);
        }
    });

    $(document).on('change', '.giftCardID', function() {
        var giftCardID = $(this).val();
        var expireDate = GIFTCARDLIST[giftCardID].giftCardExpireDate;
        var value = GIFTCARDLIST[giftCardID].giftCardValue;
        var flag = 0;
        $('.giftCardID').each(function() {
            if (giftCardID == $(this).val()) {
                flag++;
            }
        });
        if (flag > 1) {
            p_notification(false, eb.getMessage('ERR_PAY_METHOD_GIFR_CARD_ALREDY_SELECT'));
            $(this).val('');
            value = 0.00;
        } else if (expireDate) {
            var exdate = new Date(expireDate);
            var today = new Date();
            if (exdate < today) {
                p_notification(false, eb.getMessage('ERR_PAY_METHOD_GIFR_CARD_EXPIRED'));
                $(this).val('');
                value = 0.00;
            }
        }
        $(this).parents('.payMethod').find(".paidAmount").val(value).trigger('change');
    });

    $('#paymentMethodAdd').on('click', function() {
        paymentMethodIncrement++;
        var NewPaymentMethodDiv = $('#payMethod_1').clone();
        NewPaymentMethodDiv.attr('id', 'payMethod_' + paymentMethodIncrement);
        $('.pamentMethodDelete', NewPaymentMethodDiv).removeClass('hidden');
        $('#creditGroup', NewPaymentMethodDiv).hide();
        $('#checkGroup', NewPaymentMethodDiv).hide();
        $('#bankTransferGroup', NewPaymentMethodDiv).hide();
        $('#giftCardGroup', NewPaymentMethodDiv).hide();
        $('#paidAmount', NewPaymentMethodDiv).val('').attr('disabled', false);
        $('.addPaymentMethod').append(NewPaymentMethodDiv);
    });

    $(document).on('click', '.pamentMethodDelete', function() {
        $(this).parents('.payMethod').remove();
        setCreditAmountAndRestToPaid();
    });

    $(document).on('change', '.paidAmount', function() {
        var paidAmount = $(this).val();
        if (isNaN(paidAmount) || paidAmount < 0) {
            p_notification(false, eb.getMessage('ERR_PAY_PAID_AMOUNT_SHOUDBE_NUMBER'));
            $(this).val(0.00);
        } else {
            var creditAmount = $('#creditAmount').val();
            var restToPaidTotal = paymentTotal - creditAmount;
            var totalPaidAmount = parseFloat(getTotalPaidAmount());
            if (restToPaidTotal < totalPaidAmount) {
                restBalance = totalPaidAmount - restToPaidTotal;
            }
        }
        setCreditAmountAndRestToPaid();

    });

    function getTotalPaidAmount() {
        var totalPaidAmount = 0.00;
        $('.paidAmount').each(function() {
            totalPaidAmount += ($(this).val()) ? parseFloat($(this).val()) : 0.00;
        });
        return totalPaidAmount;
    }

//save function of the payment
    $('#customer-payments-form').on('submit', function(e) {
        $("#addPayment").attr("disabled", true);
        e.preventDefault();
        var valid = {
            customerID: customerID,
            paymentReference: $('#paymentID').val(),
            date: $('#currentdate').data("current-date"), //$('#currentdate').html(),
            amount: $('#amount').val().replace(/,/g, ''),
            discountAmount: $('#discountAmount').val().replace(/,/g, ''),
        };
        var Inv_data = {};
        $('#paymentbill').find('tbody > tr').each(function() {
            var amount = $(this).find('.edi').val();
            if (amount) {
                var invoiceID = this.id;
                Inv_data[invoiceID] = amount;

            }
        });

        if (validate_form(valid)) {
            $('.main_div').addClass('Ajaxloading');
            eb.ajax({
                type: 'post',
                url: addpayment,
                data: {
                    customerID: customerID,
                    customerCredit: customercredit,
                    customerBalance: customerbalance,
                    invData: JSON.stringify(Inv_data),
                    paymentID: $('#paymentID').val().replace(/\s/g, ""),
                    date: $('#currentdate').data("current-date"), //$('#currentdate').html().replace(/\s/g, ""),
                    paymentTerm: $('#paymentTerm').val(),
                    amount: $('#amount').val().replace(/,/g, ''),
                    discount: $('#discountAmount').val().replace(/,/g, ''),
                    memo: $('#memo').val(),
                    paymentType: 'invoice',
                    locationID: locationID,
                    paymentMethods: methods,
                    creditAmount: $('#creditAmount').val(),
                    loyaltyData: loyalty.getLoyaltySaveData(),
                    restBalance: restBalance,
                },
                dataType: "json",
                async: false,
                success: function(data) {
                    if (data['state'] == true) {
                        p_notification(true, eb.getMessage('SUCC_PAYMENT_ADDPAY', data['value']));
                        documentPreview(BASE_URL + '/customerPayments/viewReceipt/' + data['id'], 'documentpreview', "/customerPaymentsAPI/send-email", function($preview) {
                            $preview.on('hidden.bs.modal', function() {
                                if (!$("#preview:visible").length) {
                                    window.location.reload();
                                }
                            });
                        });
                    } else if (data['state'] == false) {
                        p_notification(false, data['msg']);
                        $('.main_div').removeClass('Ajaxloading');
                    }
                },
            });
        } else {
            $("#addPayment").attr("disabled", false);
        }
    });

    function validate_form(valid) {
        if ($('#customerID').val() === '' && $('#InvoiceID').val() === '') {
            p_notification(false, eb.getMessage('ERR_PAY_CUSNAME&INVID'));
            return false;
        } else if (valid.paymentReference == '') {
            p_notification(false, eb.getMessage('ERR_PAY_REFNUM'));
            return false;
        } else if (valid.date == null || valid.date == "") {
            p_notification(false, eb.getMessage('ERR_ADVPAY_DATE'));
            return false;
        } else if (isNaN(valid.amount)) {
            p_notification(false, eb.getMessage('ERR_ADVPAY_AMOUNT_NUMR'));
            return false;
        } else if (valid.amount < 0) {
            p_notification(false, eb.getMessage('ERR_ADVPAY_AMOUNT_NEG'));
            return false;
        } else if (isNaN(valid.discountAmount)) {
            p_notification(false, eb.getMessage('ERR_PAY_DISCOUNT_NUMR'));
            return false;
        } else if (valid.discountAmount < 0) {
            p_notification(false, eb.getMessage('ERR_PAY_DISCOUNT_NEG'));
            return false;
        } else if ($('#restToPaid').val().replace(/,/g, '') > 0) {
            p_notification(false, eb.getMessage('ERR_PAY_TOTAL_SHOULD_BE_PAY'));
            return false;
        } else {
            var checked = true;
            methods = [];
            $('.payMethod').each(function() {
                var methodID = $('.paymentMethod', this).val();
                var paidAmount = $('.paidAmount', this).val();
                var checquenumber = $('.checquenumber', this).val();
                var bank = $('.bank', this).val();
                var reciptnumber = $('.reciptnumber', this).val();
                var cardnumber = $('.cardnumber', this).val();
                var bankID = $('.bankID', this).val();
                var accountID = $('.accountID', this).val();
                var customerBank = $('.customerBank', this).val();
                var customerAccountNumber = $('.customerAccountNumber', this).val();
                var giftCardID = $('.giftCardID', this).val();

                methods.push({
                    methodID: methodID,
                    paidAmount: paidAmount,
                    checkNumber: checquenumber,
                    bank: bank,
                    reciptnumber: reciptnumber,
                    cardnumber: cardnumber,
                    bankID: bankID,
                    accountID: accountID,
                    customerBank: customerBank,
                    customerAccountNumber: customerAccountNumber,
                    giftCardID: giftCardID,
                });
                if (!paidAmount) {
                    p_notification(false, eb.getMessage('ERR_PAY_PAID_AM_CAN_BE_EMTY'));
                    $('.paidAmount', this).focus();
                    checked = false;
                    return false;
                } else if (methodID == '') {
                    p_notification(false, eb.getMessage('ERR_PAY_PAID_METHOD_SHBE_SELECT'));
                    $('.advancepaymentMethod', this).focus();
                    checked = false;
                    return false;
                } else if (methodID == 2) {
//                    if (checquenumber == null || checquenumber == "") {
//                        p_notification(false, eb.getMessage('ERR_ADVPAY_CHEQUE'));
//                        $('.checquenumber', this).focus();
//                        checked = false;
//                        return false;
//                    } else

                    if (checquenumber != "" && checquenumber.length > 8) {
                        p_notification(false, eb.getMessage('ERR_PAY_CHEQUE_INVALID'));
                        $('.checquenumber', this).focus();
                        checked = false;
                        return false;
                    } else if (checquenumber != "" && isNaN(checquenumber)) {
                        p_notification(false, eb.getMessage('ERR_ADVPAY_CHEQUE_NUMR'));
                        $('.checquenumber', this).focus();
                        checked = false;
                        return false;
                    }
//                    else if (bank == null || bank == "") {
//                        p_notification(false, eb.getMessage('ERR_ADVPAY_BANK_NAME'));
//                        $('.bank', this).focus();
//                        checked = false;
//                        return false;
//                    }

                } else if (methodID == 3) {
                    if (reciptnumber == null || reciptnumber == "") {
                        p_notification(false, eb.getMessage('ERR_ADVPAY_RECNO'));
                        $('.reciptnumber', this).focus();
                        checked = false;
                        return false;
                    } else if (reciptnumber.length > 12) {
                        p_notification(false, eb.getMessage('ERR_PAY_RECIPTNO_VALIDITY'));
                        $('.reciptnumber', this).focus();
                        checked = false;
                        return false;
                    } else if (isNaN(cardnumber)) {
                        p_notification(false, eb.getMessage('ERR_ADVPAY_CRDTCARD_NUMR'));
                        $('.cardnumber', this).focus();
                        checked = false;
                        return false;
                    }
                } else if (methodID == 5) {
                    if (bankID == null || bankID == "") {
                        p_notification(false, eb.getMessage('ERR_PAY_METHOD_BANK_ID'));
                        $('.bankID', this).focus();
                        checked = false;
                        return false;
                    } else if (accountID == null || accountID == "") {
                        p_notification(false, eb.getMessage('ERR_PAY_METHOD_ACCOUNT_ID'));
                        $('.accountID', this).focus();
                        checked = false;
                        return false;
                    } else if (isNaN(customerAccountNumber)) {
                        p_notification(false, eb.getMessage('ERR_PAY_METHOD_CUST_ACCUNT'));
                        $('.customerAccountNumber', this).focus();
                        checked = false;
                        return false;
                    }
                } else if (methodID == 6) {
                    if (giftCardID == null || giftCardID == "") {
                        p_notification(false, eb.getMessage('ERR_PAY_METHOD_GIFT_CARD_SH_SELECT'));
                        $('.giftCardID', this).focus();
                        checked = false;
                        return false;
                    }
                }
                else if (methodID == 4) {
                    var credit = parseFloat($('#creditAmount').val().replace(/,/g, ''));
                    credit = isNaN(credit) ? 0 : credit;
                    var amount = parseFloat($('#amount').val().replace(/,/g, ''));
                    var totalBill = amount - credit;

                    if (!loyalty.isValid(paidAmount, totalBill)) {
                        p_notification(false, eb.getMessage('ERR_LOYALTY_REDEEM_EXCEED'));
                        return false;
                    }
                    loyalty.calculateLoyaltyPoints(0, totalBill);
                    return true;
                }

                if (methodID != 4) {
                    var credit = parseFloat($('#creditAmount').val().replace(/,/g, ''));
                    credit = isNaN(credit) ? 0 : credit;
                    var amount = parseFloat($('#amount').val().replace(/,/g, ''));

                    var totalBill = amount - credit;
                    loyalty.calculateLoyaltyPoints(0, totalBill);
                }
            });
            if (checked) {
                return true;
            } else {
                return false;
            }

        }
    }

    function clearPaymentScreen() {
        $('#amount').val('0.00');
        $('#totalpayment').text('0.00');
        $('#discountAmount').val('');
        $('#restToPaid').val('');
        $('#creditAmount').val('');
        $('#memo').val('');
        $('.paidAmount').val('');
        $('.payMethod').each(function() {
            if (this.id != 'payMethod_1') {
                $(this).remove();
            }
        });
        customerbalance = 0;
        customercredit = 0;
    }

    idInvoice = $('#idInvoice').val();
    if (idInvoice != "") {
        idInvoice = $('#idInvoice').val();
        var selectedInvoiceCode = $('#selectedInvoiceCode').val();
        $("#InvoiceID").empty();
        $("#InvoiceID").append($("<option></option>")
                .attr("value", idInvoice)
                .text(selectedInvoiceCode));
        $('#InvoiceID').selectpicker('render');
        $('#InvoiceID').show();

        $('#customerID').selectpicker('hide');
        $('#invoiceSearchType').val('Invoice ID').attr('disabled', true);
        changeEvent(idInvoice, "Invoice");
        $("#InvoiceID").attr("disabled", "disabled");
        $("#customerID").attr("disabled", "disabled");
        $(document).on("click", "#cancel", function() {
            window.history.back();
        });
    }
});
