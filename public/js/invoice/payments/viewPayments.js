var tmptotal;
var paymentID;
var customerID;

var loadPaymentsPreview;
var ignoreBudgetLimitFlag = false;
$(document).ready(function() {


    if (!$('#filter-button').length) {
        return false;
    }


    $('.deli_row').hide();
    $('#deli_form_group').hide();
    $('#tax_td').hide();
    $('#pay-custa-search').selectpicker('hide');
    $('#custadiv').hide();
    $('#back-pay-cust').hide();
    $('#button-print').hide();
    $('#button-email').hide();
    $('#back-pay-cust').on('click', function() {
        $('#custadiv').show();
        $('#back-pay-cust').hide();
        $('#button-print').hide();
    });
    $('#pay-search-select').on('change', function() {
        if ($(this).val() == 'Payments No') {
            $('#pay-custa-search').selectpicker('hide');
            $('#pay-search').selectpicker('show');
            $('#pay-custa-search').val('');
            $('#pay-custa-search').selectpicker('render');
            $('#custadiv').hide();
            $('#back-pay-cust').hide();
            $('#button-print').hide();
            $('#paymentsview').show();
            $('#button-email').hide();
        } else if ($(this).val() == 'Customer Name') {
            $('#pay-search').selectpicker('hide');
            $('#pay-custa-search').selectpicker('show');
            $('#pay-search').val('');
            $('#pay-search').selectpicker('render');
            $('#custadiv').hide();
            $('#back-pay-cust').hide();
            $('#button-print').hide();
            $('#paymentsview').show();
            $('#button-email').hide();
        }
    });
    var paymentType = $('#pay-search').attr('data-payType');
    loadDropDownFromDatabase('/customerPaymentsAPI/search-customer-payments-for-dropdown', "", paymentType, '#pay-search');
    $('#pay-search').selectpicker('refresh');
    $('#pay-search').on('change', function() {
        if ($(this).val() > 0 && paymentID != $(this).val()) {
            paymentID = $(this).val();
            $('#custadiv').hide();
            $('#button-email').hide();
            $('#pay-cust-view').html('');
            var getpaybycusturl = '/customerPaymentsAPI/RetrivePayments';
            var requestquobycust = eb.post(getpaybycusturl, {
                PaymentID: paymentID
            });
            requestquobycust.done(function(retdata) {
                $('#paymentsview').html(retdata);
            });
        }
    });

    var paymentsType = $('#pay-custa-search').attr('data-payType');
    loadDropDownFromDatabase('/customerAPI/search-customers-for-dropdown', "", "withDeactivatedCustomers", '#pay-custa-search');
    $('#pay-custa-search').selectpicker('refresh');
    $('#pay-custa-search').on('change', function() {
        if ($(this).val() > 0 && customerID != $(this).val()) {
            customerID = $(this).val();
            $('#custadiv').hide();
            $('#button-email').hide();
            $('#pay-cust-view').html('');
            var getpaybycusturl = '/customerPaymentsAPI/RetriveCustomerPayments';
            var requestquobycust = eb.post(getpaybycusturl, {
                customerID: customerID,
                payType: paymentsType
            });
            requestquobycust.done(function(retdata) {
                $('#paymentsview').html(retdata);
            });
        }
    });

    $(document).on('click', "a.editsInactive", function(e) {
        p_notification(false, eb.getMessage('ERR_CUST_INACTIVE'));
    });

    function printdiv(divID) {
        var DocumentContainer = document.getElementById(divID);
        var WindowObject = window.open('', 'PrintWindow', 'width=750,height=650,top=50,left=50,toolbars=no,scrollbars=yes,status=no,resizable=yes');
        WindowObject.document.writeln('<!DOCTYPE html>');
        WindowObject.document.writeln('<html><head><title></title>');
        WindowObject.document.writeln('<link rel="stylesheet" type="text/css" href="/assets/bootstrap/css/bootstrap.min.css">');
        WindowObject.document.writeln('</head><body>');
        WindowObject.document.writeln(DocumentContainer.innerHTML);
        WindowObject.document.writeln('</body></html>');
        WindowObject.document.close();
        WindowObject.focus();
        WindowObject.print();
        WindowObject.close();
    }

    $('#filter-button').on('click', function() {
        if ($('#from-date').val() == '' || $('#to-date').val() == '') {
            p_notification(false, eb.getMessage('ERR_VIEWPAY_FILLDATA'));
        } else {
            var paymentfilter = BASE_URL + '/customerPaymentsAPI/getPaymentsByDatefilter';
            var filterrequest = eb.post(paymentfilter, {
                'fromdate': $('#from-date').val(),
                'todate': $('#to-date').val(),
                'customerID': customerID,
                'paymentType': paymentType
            });
            filterrequest.done(function(retdata) {
                if (retdata.msg == 'nopayment') {
                    p_notification(false, eb.getMessage('ERR_VIEWPAY_NORANGE'));
                    $('#paymentsview').html('');
                } else {
                    $('.deli_row').hide();
                    $('#deli_form_group').hide();
                    $('#tax_td').hide();
                    $('#custadiv').show();
                    $('#back-pay-cust').hide();
                    $('#button-print').hide();
                    $('#button-email').hide();
                    $('#paymentsview').html(retdata);
                }
            });
        }
    });
    $(document).on('click', ' .del', function() {
        var input = $(this).attr('id');
        wrongPayment(input);
    });

    $(document).on('click', ' .doc_attachments', function() {
        setDataToAttachmentViewModal($(this).attr('data-pay-related-id'));
        $('#viewAttachmentModal').modal('show');
    });

    function setDataToAttachmentViewModal(documentID) {
        $('#doc-attach-table tbody tr').remove();
        $('#doc-attach-table tfoot div').remove();
        $('#doc-attach-table tbody div').remove();

        eb.ajax({
            type: 'POST',
            url: BASE_URL + '/get-document-related-attachement',
            data: {
                documentID: documentID,
                documentTypeID: 7
            },
            success: function(respond) {
                if (respond.status == true) {
                    $('#doc-attach-table thead tr').removeClass('hidden');
                    $.each(respond.data, function(index, value) {
                        tableBody = "<tr><td><a href="+value.link+">"+value.docName+"</a></td></tr>";
                        $('#doc-attach-table tbody').append(tableBody);
                    });
                } else {
                    $('#doc-attach-table thead tr').addClass('hidden');
                    var noDataFooter = "<div class= ' col-lg-6 text-right'><label><b>No Data</b></label>";
                    $('#doc-attach-table tbody').append(noDataFooter);
                }
            }
        });
    }

    function validate_data(valid) {
        var user = valid[0];
        var pass = valid[1];
        if (user == null || user == "") {
            p_notification(false, eb.getMessage('ERR_VIEWPAY_UNAME'));
        } else if (pass == null || pass == "") {
            p_notification(false, eb.getMessage('ERR_VIEWPAY_PWD'));
        } else {
            return true;
        }
    }

    function wrongPayment(payno) {
        $('#delete-payment-button').off('click').on('click', function(e) {
            e.preventDefault();
            deletePayment(payno);
        });
    }

    function deletePayment(payno)
    {
        var valid = new Array(
                $('#username').val(),
                $('#password').val()
                );
        if (validate_data(valid)) {
//                $('#button-email').show();
//                $("#form_rwo").html('');
            var username = $("#username").val();
            var password = $('#password').val();
            var message = $('#comment').val();
//                $('.main_div').addClass('Ajaxloading');
            var getpayurl = BASE_URL + '/customerPaymentsAPI/deletePayment';
            var getpayrequest = eb.post(getpayurl, {
                payno: payno,
                username: username,
                password: password,
                incomingPaymentCancelMessage: message,
                ignoreBudgetLimit: ignoreBudgetLimitFlag
            });
            getpayrequest.done(function(data) {
                if (data[0] == true) {
                    p_notification(true, eb.getMessage('ERR_VIEWPAY_PAY_DELETE'));
                    setTimeout(function() {
                        window.location.reload();
                    }, 1000);
                } else if (data[0] == 'chengeCredit') {
                    p_notification(false, eb.getMessage('ERR_VIEWPAY_CUS_MONEY'));
                } else if (data[0] == 'admin') {
                    p_notification(false, eb.getMessage('ERR_VIEWPAY_USER_PRIVILEGE'));
                } else if (data[0] == 'user' || data[0] == 'pass') {
                    p_notification(false, eb.getMessage('ERR_VIEWPAY_INVALID_USRNAME_PWD'));
                } else if (data[0] == false) {
                    p_notification(false, eb.getMessage('ERR_VIEWPAY_USER_AUTH'));
                } else if(data[0] == 'JEERROR'){
                    if (data[2] == "NotifyBudgetLimit") {
                        bootbox.confirm(data[1]+' ,Are you sure you want to cancel this payment ?', function(result) {
                            if (result == true) {
                                ignoreBudgetLimitFlag = true;
                                deletePayment(payno);
                            } else {
                                setTimeout(function(){ 
                                    location.reload();
                                }, 3000);
                            }
                        });
                    } else {
                        p_notification(false, data[1]);
                    }
                } else if(data[0] == 'maxRedeem'){
                    p_notification(false, eb.getMessage('ERR_LOYALTY_REDEEM_EXCEED'));
                } else if(data[0] == 'PAYMENTSTATUSERR'){
                    p_notification(false, data[1]);
                }
                $('#custadiv').hide();
            });
        }
    }

///////DatePicker\\\\\\\
    var nowTemp = new Date();
    var frmdate;
    var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);
    var checkin = $('#from-date').datepicker().on('changeDate', function(ev) {
        checkin.hide();
        frmdate = ev.date;
        $('#to-date')[0].focus();
    }).data('datepicker');
    var checkout = $('#to-date').datepicker({
        onRender: function(date) {
            return date.valueOf() < frmdate ? 'disabled' : '';
        }
    }).on('changeDate', function(ev) {
        checkout.hide();
    }).data('datepicker');
/////EndOFDatePicker\\\\\\\\\

    $(document).on('change', 'input[type=radio][name=original-print-opt]', function (){

        var isInitial = this.value;
        var origsrc = $('#documentpreview').data("origsrc");
        var params = origsrc.split("/");

        if(isInitial === '1') {

            $('#templateID').addClass('hidden');

            $.ajax({
                type: 'POST',
                url: BASE_URL + '/customerPaymentsAPI/get-payment-receipt-path',
                data: {
                    isInitial : isInitial,
                    paymentId : params[3],
                    templateId : null,
                    origsrc : origsrc
                },
                success: function(respond) {
                    if(respond.status) {
                        $("#documentpreview").attr("src", BASE_URL + respond.data);
                    }
                }
            });
        } else {
            $('#templateID').removeClass('hidden');
            $("#documentpreview").attr("src", BASE_URL + origsrc);
        }

    });
});