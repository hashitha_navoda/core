
/**
 * @author ASHAN <ashan@thinkcube.com>
 * This file contains form validation
 */
var locationID;
var checque = 0;
var creditcard = 0;
var customCurrencyRate = 0;
var ignoreBudgetLimitFlag = false;
var dimensionData = {};
$(document).ready(function() {

    var dimensionArray = {};

    loadDropDownFromDatabase('/accounts-api/search-active-accounts-for-dropdown', "", "", '#cashGlAccountID');
    loadDropDownFromDatabase('/accounts-api/search-active-accounts-for-dropdown', "", "", '#chequeGlAccountID');
    loadDropDownFromDatabase('/accounts-api/search-active-accounts-for-dropdown', "", "", '#creditGlAccountID');
    loadDropDownFromDatabase('/accounts-api/search-active-accounts-for-dropdown', "", "", '#bankTransferGlAccountID');
    loadDropDownFromDatabase('/accounts-api/search-active-accounts-for-dropdown', "", "", '#lcGlAccountID');
    loadDropDownFromDatabase('/accounts-api/search-active-accounts-for-dropdown', "", "", '#ttGlAccountID');


	$('#customCurrencyRate').val(customCurrencyRate).attr('disabled',true);
    $('#customCurrencyRate').on('change',function(){
    	if(isNaN($(this).val())){
    		p_notification(false, eb.getMessage('ERR_INVO_CURRENCY_RATE_SHBE_NUM'));
            $(this).val(customCurrencyRate);
            $('#customCurrencyRate').focus();
            return false;
    	}else{
    		customCurrencyRate = $(this).val(); 
    	}
    });

    var methods = [];
    var paymentMethodIncrement = 1;
    var totalPaidAmount = 0.00;
    $("#creditGroup").hide();
    $("#checkGroup").hide();
    $("#bankTransferGroup").hide();
    $("#giftCardGroup").hide();
    $("#postdatedChequeGroup").hide();
    $("#lcGroup").hide();
    $("#ttGroup").hide();
    $("#cashGroup").hide();
    $(".postdated-cheque-date").hide();

    var nowTemp = new Date();
    var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);
    var postdatedDate = $('.postdated-cheque-date').datepicker({
        format: 'yyyy-mm-dd',
        onRender: function(date) {
            return date.valueOf() < now.valueOf() ? 'disabled' : '';
        }
    }).on('changeDate', function(e) {
        postdatedDate.hide();
    }).data('datepicker');

    var day = nowTemp.getDate() < 10 ? '0' + nowTemp.getDate() : nowTemp.getDate();
    var month = (nowTemp.getMonth() + 1) < 10 ? '0' + (nowTemp.getMonth() + 1) : (nowTemp.getMonth() + 1);
    $("#advancecurrentdate").val(eb.getDateForDocumentEdit('#advancecurrentdate', day, month, nowTemp.getFullYear()));
     if(!U_DATE_R_OVERRIDE){
        $('#advancecurrentdate').attr('disabled',true);
    } else {
        $('#advancecurrentdate').attr('disabled',false);
    }
    var test = $('#advancecurrentdate').datepicker({
        format: 'yyyy-mm-dd',
        onRender: function(date) {
            return date.valueOf() < now.valueOf() ? ((U_DATE_R_OVERRIDE)?'':'disabled') : '';
        }
    }).on('changeDate', function(e) {
        test.hide();
    }).data('datepicker');


    $(document).on('change', '.postdated-cheque', function() {
        if ($(this).is(":checked")) {
            $(this).parents('.payMethod').find(".postdated-cheque-date").show();
        } else {
            $(this).parents('.payMethod').find(".postdated-cheque-date").hide();
        }
    });

    loadDropDownFromDatabase('/customerAPI/search-customers-for-dropdown', "", 0, '#advancecustomer');
    $('#advancecustomer').selectpicker('refresh');
    $('#advancecustomer').on('change', function() {
        if ($('#advancecustomer').val() > 0 && $('#advancecustomer').val() != customerID) {
            customerID = $('#advancecustomer').val();
        }
    });

    locationID = $('#idOfLocation').val();
    $('#adpaymentID').attr('disabled', true);
    var url = BASE_URL + '/customerPaymentsAPI/advance-payment';
    function validate_form(advalid) {
        var cus = advalid.customer;
        var date = advalid.date;
        var amount = advalid.totalAmount;
        var paymentCode = advalid.paymentCode;
        if (paymentCode == null || paymentCode == " ") {
            p_notification(false, eb.getMessage('ERR_ADVPAY_PAYNUM'));
        } else if (cus == null || cus == "" || cus == 'emptyCustomer') {
            p_notification(false, eb.getMessage('ERR_DELINOTEAPI_VALID_CUST'));
        } else if (date == null || date == "") {
            p_notification(false, eb.getMessage('ERR_ADVPAY_DATE'));
        } else if (amount == null || amount == "" || amount == 0) {
            p_notification(false, eb.getMessage('ERR_ADVPAY_AMOUNT'));
        } else if (isNaN(amount)) {
            p_notification(false, eb.getMessage('ERR_ADVPAY_AMOUNT_NUMR'));
        } else if (amount <= 0) {
            p_notification(false, eb.getMessage('ERR_ADVPAY_AMOUNT_NEG'));
        } else {
            var checked = true;
            methods = [];
            $('.payMethod').each(function() {
                var methodID = $('.advancepaymentMethod', this).val();
                var paidAmount = $('.advanceamount', this).val();
                var checquenumber = $('.checquenumber', this).val();
                var bank = $('.bank', this).val();
                var reciptnumber = $('.reciptnumber', this).val();
                var cardnumber = $('.cardnumber', this).val();
                var bankID = $('.bankID', this).val();
                var accountID = $('.accountID', this).val();
                var customerBank = $('.customerBank', this).val();
                var customerAccountNumber = $('.customerAccountNumber', this).val();
                var giftCardID = $('.giftCardID', this).val();
                var lcPaymentReference = $('.lcPaymentReference', this).val();
                var ttPaymentReference = $('.ttPaymentReference', this).val();
                var postdatedStatus = 0;
                var postdatedDate = null;
                var cashAccountID = $(this).find('#cashGlAccountID').val();
                var chequeAccountID = $(this).find('#chequeGlAccountID').val();
                var creditAccountID = $(this).find('#creditGlAccountID').val();
                var bankTransferAccountID = $(this).find('#bankTransferGlAccountID').val();
                var lcAccountID = $(this).find('#lcGlAccountID').val();
                var ttAccountID = $(this).find('#ttGlAccountID').val();
                var bankTransferRef = $(this).find('#bankTransferRef').val();
                //check whether postdated cheque
                if ($('.postdated-cheque', this).is(":checked")) {
                    postdatedStatus = 1;
                    postdatedDate = $('.postdated-cheque-date', this).val();
                }

                methods.push({
                    methodID: methodID,
                    paidAmount: paidAmount,
                    checkNumber: checquenumber,
                    bank: bank,
                    reciptnumber: reciptnumber,
                    cardnumber: cardnumber,
                    bankID: bankID,
                    accountID: accountID,
                    customerBank: customerBank,
                    customerAccountNumber: customerAccountNumber,
                    giftCardID: giftCardID,
                    postdatedStatus: postdatedStatus,
                    postdatedDate: postdatedDate,
                    lcPaymentReference: lcPaymentReference,
                    ttPaymentReference: ttPaymentReference,
                    cashAccountID : cashAccountID,
                    chequeAccountID : chequeAccountID,
                    creditAccountID : creditAccountID,
                    bankTransferAccountID : bankTransferAccountID, 
                    lcAccountID : lcAccountID,
                    ttAccountID : ttAccountID,
                    bankTransferRef: bankTransferRef
                });

                if (!paidAmount) {
                    p_notification(false, eb.getMessage('ERR_PAY_PAID_AM_CAN_BE_EMTY'));
                    $('.advanceamount', this).focus();
                    checked = false;
                    return false;
                } else if (methodID == '') {
                    p_notification(false, eb.getMessage('ERR_PAY_PAID_METHOD_SHBE_SELECT'));
                    $('.advancepaymentMethod', this).focus();
                    checked = false;
                    return false;
                } else if (methodID == 2) {
//                    if (checquenumber == null || checquenumber == "") {
//                        p_notification(false, eb.getMessage('ERR_ADVPAY_CHEQUE'));
//                        $('.checquenumber', this).focus();
//                        checked = false;
//                        return false;
//                    } else
//                    if (checquenumber != "" && checquenumber.length > 8) {
//                        p_notification(false, eb.getMessage('ERR_PAY_CHEQUE_INVALID'));
//                        $('.checquenumber', this).focus();
//                        checked = false;
//                        return false;
//                    } else if (checquenumber != "" && isNaN(checquenumber)) {
//                        p_notification(false, eb.getMessage('ERR_ADVPAY_CHEQUE_NUMR'));
//                        $('.checquenumber', this).focus();
//                        checked = false;
//                        return false;
//                    }
//                    else if (bank == null || bank == "") {
//                        p_notification(false, eb.getMessage('ERR_ADVPAY_BANK_NAME'));
//                        $('.bank', this).focus();
//                        checked = false;
//                        return false;
//                    }

                } else if (methodID == 3) {
                    if (reciptnumber == null || reciptnumber == "") {
                        p_notification(false, eb.getMessage('ERR_ADVPAY_RECNO'));
                        $('.reciptnumber', this).focus();
                        checked = false;
                        return false;
                    } else if (reciptnumber.length > 12) {
                        p_notification(false, eb.getMessage('ERR_PAY_RECIPTNO_VALIDITY'));
                        $('.reciptnumber', this).focus();
                        checked = false;
                        return false;
                    } else if (isNaN(cardnumber)) {
                        p_notification(false, eb.getMessage('ERR_ADVPAY_CRDTCARD_NUMR'));
                        $('.cardnumber', this).focus();
                        checked = false;
                        return false;
                    }
                } else if (methodID == 5) {
                    if (bankID == null || bankID == "") {
                        p_notification(false, eb.getMessage('ERR_PAY_METHOD_BANK_ID'));
                        $('.bankID', this).focus();
                        checked = false;
                        return false;
                    } else if (accountID == null || accountID == "") {
                        p_notification(false, eb.getMessage('ERR_PAY_METHOD_ACCOUNT_ID'));
                        $('.accountID', this).focus();
                        checked = false;
                        return false;
                    } else if (isNaN(customerAccountNumber)) {
                        p_notification(false, eb.getMessage('ERR_PAY_METHOD_CUST_ACCUNT'));
                        $('.customerAccountNumber', this).focus();
                        checked = false;
                        return false;
                    }
                } else if (methodID == 6) {
                    if (giftCardID == null || giftCardID == "") {
                        p_notification(false, eb.getMessage('ERR_PAY_METHOD_GIFT_CARD_SH_SELECT'));
                        $('.giftCardID', this).focus();
                        checked = false;
                        return false;
                    }
                }
            });
            if (checked) {
                return true;
            } else {
                return false;
            }

        }
    }

    $('#addAdvancePayment').on('click', function(e) {
        e.preventDefault();
        advancesave();
    });

    $('#advance-payments-form').on('submit', function(e) {
        e.preventDefault();
        advancesave();
    });

    $('#close').on('click', function(e) {
        window.history.back();
    });

    function advancesave() {
        var advalid = {
            customer: $('#advancecustomer').val(),
            date: $('#advancecurrentdate').val(),
            totalAmount: totalPaidAmount,
            paymentCode: $('#adpaymentID').val()
        };
        if (validate_form(advalid)) {
            $('.main_div').addClass('Ajaxloading');
            var post = eb.post(url, {
                paymentID: $('#adpaymentID').val().replace(/\s/g, ""),
                customerName: $("#advancecustomer option:selected").text(),
                date: $('#advancecurrentdate').val(),
                amount: totalPaidAmount,
                memo: $('#advancememo').val(),
                locationID: locationID,
                paymentType: 'advance',
                customerID: $('#advancecustomer').val(),
                paymentMethods: methods,
                totalPaidAmount: totalPaidAmount,
                customCurrencyId: $('#customCurrencyId').val(),
                customCurrencyRate: customCurrencyRate,
                cardType: $('#cardType').val(),
                ignoreBudgetLimit : ignoreBudgetLimitFlag,
                dimensionData: dimensionData,
            });
            post.done(function(values) {
                if (values['state'] == true) {
                    p_notification(true, values['msg']);
                    documentPreview(BASE_URL + '/customerPayments/viewReceipt/' + values['id'], 'documentpreview', "/customerPaymentsAPI/send-email", function($preview) {
                        $preview.on('hidden.bs.modal', function() {
                            if (!$("#preview:visible").length) {
                                window.location.reload();
                            }
                        });
                    });

                } else {
                    if (values['data'] == "NotifyBudgetLimit") {
                        bootbox.confirm(values['msg']+' ,Are you sure you want to save this advance payment ?', function(result) {
                            if (result == true) {
                                ignoreBudgetLimitFlag = true;
                                advancesave();
                            } else {
                                setTimeout(function(){ 
                                    location.reload();
                                }, 3000);
                            }
                        });
                    } else {
                        $('.main_div').removeClass('Ajaxloading');
                        p_notification(false, values['msg']);
                    }
                }
            });
        }
    }

    $('#dimensionView').on('click',function(){
        clearDimensionModal();
        var advPayCode = $('#adpaymentID').val().trim();

        if (!$.isEmptyObject(dimensionData)) {
            $.each(dimensionData[advPayCode], function(index, val) {
                dimensionArray[index] = val;                
            });
            $.each(dimensionArray, function(index, value) {
                var newTrID = 'tr_' + value.dimensionTypeId;
                var clonedRow = $($('#dimensionPreSample').clone()).attr('id', newTrID).addClass('addedDimensions');
                $("input[name='dimensionType']", clonedRow).val(value.dimensionTypeTxt);
                $("input[name='dimensionValue']", clonedRow).val(value.dimensionValueTxt);
                clonedRow.removeClass('hidden');
                clonedRow.insertBefore('#dimensionPreSample');
                clearAddedNewDimensionRow();
            });
        }
        $('#addDimensionModal').modal('show');

    });


    $('#add_dimension').on('click', function(e) {
        e.preventDefault();
        //if plus button is clicked then disabled the button
        //if button is clicked more than one, then it does not continue
        if ($(this).is(':disabled')) {
            return false;
        } else {
            $(this).attr('disabled', true);
            var $thisRow = $(this).parents('tr');
            if ($('.dimenisonchange').val() == 0 || $('.dimenisonchange').val() == '' || $('.dimenisonchange').val() == null) {
                p_notification(false, eb.getMessage('ERR_DIMENSION_SELCET'));
                $(this).attr('disabled', false);
            } else {

                if ($('.dimensionVal').val() == 0 || $('.dimensionVal').val() == '' || $('.dimensionVal').val() == null) {
                    p_notification(false, eb.getMessage('ERR_DIMENSION_VALUE_SELCET'));
                    $(this).attr('disabled', false);
                    return;
                }

                if (dimensionArray[dimensionTypeID]) {
                    p_notification(false, eb.getMessage('ERR_DIMENSION_ALREADY_INSERTED'));
                    $('.dimenisonchange').val('');
                    $('.dimenisonchange').selectpicker('render');
                    $('.dimenisonchange').trigger('change');
                } else {
                    addNewDimensionRow($(this).parents('tr'));
                }
                $(this).attr('disabled', false);
            }
        }
    });

    function addNewDimensionRow(thisRow) {

        var dimensionTypeId = $('.dimenisonchange', thisRow).val();
        var dimensionTypeTxt = $('.dimenisonchange', thisRow).find("option:selected").text();

        switch (dimensionTypeID) {
            case "job":
                var dimensionValueTxt = $('.dimensionJobNo', thisRow).find("option:selected").text();
                var dimensionValueId = $('.dimensionJobNo', thisRow).val();
                break;
            case "project":
                var dimensionValueTxt = $('.dimensionProjectNo', thisRow).find("option:selected").text();
                var dimensionValueId = $('.dimensionProjectNo', thisRow).val();
                break;
           default:
                var dimensionValueTxt = $('.dimensionVal', thisRow).find("option:selected").text();
                var dimensionValueId = $('.dimensionVal', thisRow).val();
                break;
        }
       
        var dimensionDuplicateCheck = false;
        if (!$.isEmptyObject(dimensionArray)) {
            $.each(dimensionArray, function(index, value) {
                if (value.dimensionTypeId == dimensionTypeId) {
                    dimensionDuplicateCheck = true;
                }
            });   
        }
       
        if (dimensionDuplicateCheck) {
            p_notification(false, eb.getMessage("ERR_DIMENSION_ALREADY_INSERTED"));
            return false;
        }

        var newTrID = 'tr_' + dimensionTypeId;
        var clonedRow = $($('#dimensionPreSample').clone()).attr('id', newTrID).addClass('addedDimensions');
        $("input[name='dimensionType']", clonedRow).val(dimensionTypeTxt);
        $("input[name='dimensionValue']", clonedRow).val(dimensionValueTxt);
        clonedRow.removeClass('hidden');
        clonedRow.insertBefore('#dimensionPreSample');
        dimensionArray[dimensionTypeId] = new dimension(dimensionTypeId, dimensionValueId,dimensionTypeTxt , dimensionValueTxt);
        clearAddedNewDimensionRow();
    }


    function dimension(dimensionTypeId, dimensionValueId, dimensionTypeTxt, dimensionValueTxt) {
        this.dimensionTypeId = dimensionTypeId;
        this.dimensionValueId = dimensionValueId;
        this.dimensionTypeTxt = dimensionTypeTxt;
        this.dimensionValueTxt = dimensionValueTxt;
    }

    function clearAddedNewDimensionRow() {
        $('.dimenisonchange').val('');
        $('.dimenisonchange').selectpicker('render');
        $('.dimenisonchange').trigger('change');
    }

    $('.addDimensions').on('click', function(e) {
        e.preventDefault();

        // validate products before closing modal
        if (!dimensionModalValidate(e)) {
            e.stopPropagation();
            return false;
        } else {
            $('#addDimensionModal').modal('hide');
        }
    });

    function dimensionModalValidate(e) {
        var advPayCode = $('#adpaymentID').val().trim();
        if ($.isEmptyObject(dimensionArray)) {
            dimensionData = {};
            return true;   
        } else {
            var dimensionSingleData = {};
            $.each(dimensionArray, function(index, val) {
                dimensionSingleData[index] = val;
            });
            dimensionData[advPayCode] = dimensionSingleData;
            return true;
        }
    }

    $("#addDimensionModal #dimensionTable").on('click', '.deleteDimensionType', function(e) {
        e.preventDefault();
        var deleteDTrID = $(this).closest('tr').attr('id');
        var deleteDID = deleteDTrID.split('tr_')[1].trim();
        bootbox.confirm('Are you sure you want to remove this dimension ?', function(result) {
            if (result == true) {
                delete dimensionArray[deleteDID];
                $('#' + deleteDTrID).remove();
            }
        });
    });

    loadDropDownFromDatabase('/job-api/searchJobsForDropdown', "", 0, '.dimensionJobNo');
    $('.dimensionJobNo').selectpicker('refresh');
    $('.dimensionJobNo').on('change', function() {
        var dimensionValue = $(this).val();
    });

    loadDropDownFromDatabase('/api/project/searchProjectsForDropdown', "", 0, '.dimensionProjectNo');
    $('.dimensionProjectNo').selectpicker('refresh');
    $('.dimensionProjectNo').on('change', function() {
        var dimensionValue = $(this).val();
    });

    // dimension modal
    function clearDimensionModal()
    {
        $("#addDimensionModal #dimensionTable tbody .addedDimensions").remove();
        dimensionArray = {};
    }

    $('.dimenisonchange').on('change', function(e) {
        dimensionTypeID = this.value;
        getDimensionValueListByDimensionType(dimensionTypeID, function() {
        });
    });

    function getDimensionValueListByDimensionType(dimensionTypeID, callback) {
        switch (dimensionTypeID) {
            case "job":
                $('.dimensionVal').addClass('hidden');
                $('.dimensionProjectNo').addClass('hidden');
                $('.dimensionJobNo').removeClass('hidden');
                break;
            case "project":
                $('.dimensionVal').addClass('hidden');
                $('.dimensionProjectNo').removeClass('hidden');
                $('.dimensionJobNo').addClass('hidden');
                break;
           default:
                $('.dimensionVal').removeClass('hidden');
                $('.dimensionProjectNo').addClass('hidden');
                $('.dimensionJobNo').addClass('hidden');
                $.ajax({
                    type: 'POST',
                    url: BASE_URL + '/dimension-api/getRelatedDimensionValues',
                    data: {dimensionId: dimensionTypeID},
                    success: function(respond) {
                        //empty options
                        $('#dimensionVal').empty();
                        $('#dimensionVal').append($("<option>", {value: '', html: 'Select dimension value'}));
                        $(respond.data).each(function(index, dimValue) {
                            $.each(dimValue, function(ind, val) {
                                $('#dimensionVal').append($("<option>", {value: ind, html: val.value}));
                            });
                        });
                        callback();
                    }
                });
                break;
        }
    }


    $(document).on('change', '.advancepaymentMethod', function() {
        var paymentMethods = $(this).val();
        var $payMethod = $(this).parents('.payMethod');
        $("#advanceamount", $payMethod).attr('disabled', false);
        $("#creditGroup", $payMethod).hide();
        $("#checkGroup", $payMethod).hide();
        $("#bankTransferGroup", $payMethod).hide();
        $("#giftCardGroup", $payMethod).hide();
        $("#postdatedChequeGroup", $payMethod).hide();
        $("#lcGroup", $payMethod).hide();
        $("#ttGroup", $payMethod).hide();
        $("#cashGroup", $payMethod).hide();

        if(paymentMethods == 1){
        	$("#cashGroup", $payMethod).show();
        }else if (paymentMethods == 3) {
            $("#creditGroup", $payMethod).show();
        } else if (paymentMethods == 2) {
            $("#checkGroup", $payMethod).show();
            //for postdated cheque
            $(this).parents('.payMethod').find("#postdatedChequeGroup").show();
        } else if (paymentMethods == 5) {
            $("#bankTransferGroup", $payMethod).show();
        } else if (paymentMethods == 6) {
            $("#giftCardGroup", $payMethod).show();
            $("#advanceamount", $payMethod).val(0.00).trigger('change');
            $("#advanceamount", $payMethod).attr('disabled', true);
            $('.payMethod', $payMethod).find("#giftCardID").val('');
        } else if (paymentMethods == 7) {
            $("#lcGroup", $payMethod).show();
        } else if (paymentMethods == 8) {
            $("#ttGroup", $payMethod).show();
        }
    });
    $(document).on('change', '.accountID', function() {
        var glAcc = $("option:selected", $(this)).attr('data-glaccount');
        var bankdiv = $(this).parents('#bankTransferGroup');

        if (glAcc) {
            $('.main_div').addClass('Ajaxloading');
            eb.ajax({
                type: 'post',
                url: BASE_URL + '/accounts-api/getFinanceAccountsByAccountsID',
                data: {
                    financeAccountsID: glAcc
                },
                dataType: "json",
                async: false,
                success: function(data) {
                    if (data['status'] == true) {
                        var fName= data.data.financeAccounts.financeAccountsCode+'_'+data.data.financeAccounts.financeAccountsName

                        bankdiv.find('#bankTransferGlAccountID').append("<option value='"+glAcc+"'>"+fName+"</option>")
                        bankdiv.find('#bankTransferGlAccountID').val(glAcc).selectpicker('refresh');
                        
                    }
                    $('.main_div').removeClass('Ajaxloading');
                },
            });
        } else {
            bankdiv.find('.accountID').attr('disabled', true);
        }
    });


    $(document).on('change', '.bankID', function() {
        var bankdiv = $(this).parents('#bankTransferGroup');
        var bankID = $(this).val();
        var bank = $(this);
        var bankName = $("option:selected", $(this)).text();
        bankdiv.find('.accountID').find('option').each(function() {
            if ($(this).val()) {
                $(this).remove();
            }
        });
        if (bankID) {
            $('.main_div').addClass('Ajaxloading');
            eb.ajax({
                type: 'post',
                url: BASE_URL + '/account-api/bankAccountListForDropdown',
                data: {
                    bankId: bankID,
                    withGLAccount: true
                },
                dataType: "json",
                async: false,
                success: function(data) {
                    if (data['status'] == true) {
                        if ($.isEmptyObject(data['data'].list)) {
                            p_notification(false, eb.getMessage('ERR_DOESNT_HAVE_ANY_ACC_FOR_THIS_BANK', bankName));
                            $(bank).val('');
                            $('.accountID', bankdiv).attr('disabled', true);
                        } else {
                            $.each(data['data'].list, function(key, value) {
                                var option = "<option data-glaccount="+value.glAccountID+" value=" + value.value + ">" + value.text + "</option>";
                                $('.accountID', bankdiv).attr('disabled', false);
                                $('.accountID', bankdiv).append(option);
                            });
                        }
                    }
                    $('.main_div').removeClass('Ajaxloading');
                },
            });
        } else {
            $('.accountID', bankdiv).attr('disabled', true);
        }
    });

    $('#paymentMethodAdd').on('click', function() {
        paymentMethodIncrement++;
        var NewPaymentMethodDiv = $('#payMethod_1').clone();
        NewPaymentMethodDiv.attr('id', 'payMethod_' + paymentMethodIncrement);
        var newID = 'payMethod_' + paymentMethodIncrement;
        $('.pamentMethodDelete', NewPaymentMethodDiv).removeClass('hidden');
        $('#creditGroup', NewPaymentMethodDiv).hide();
        $('#checkGroup', NewPaymentMethodDiv).hide();
        $('#bankTransferGroup', NewPaymentMethodDiv).hide();
        $('#giftCardGroup', NewPaymentMethodDiv).hide();
        $('#lcGroup', NewPaymentMethodDiv).hide();
        $('#ttGroup', NewPaymentMethodDiv).hide();
        $('#cashGroup', NewPaymentMethodDiv).hide();
        $('#advanceamount', NewPaymentMethodDiv).val('').attr('disabled', false);
        $('.addPaymentMethod').append(NewPaymentMethodDiv);


        //set selectpickers to the clonedPayment Methods
        $('#'+newID).find('#cashGlAccountID').removeData('AjaxBootstrapSelect');        
        $('#'+newID).find('#cashGlAccountID').removeData('selectpicker');           
        $('#'+newID).find('#cashGlAccountID').siblings('.bootstrap-select').remove();           
        $('#'+newID).find('#cashGlAccountID').siblings('.bootstrap-select').find('.dropdown-menu').remove();        
        loadDropDownFromDatabase('/accounts-api/search-active-accounts-for-dropdown', "", "", $('#'+newID).find('#cashGlAccountID'));

        $('#'+newID).find('#chequeGlAccountID').removeData('AjaxBootstrapSelect');        
        $('#'+newID).find('#chequeGlAccountID').removeData('selectpicker');           
        $('#'+newID).find('#chequeGlAccountID').siblings('.bootstrap-select').remove();           
        $('#'+newID).find('#chequeGlAccountID').siblings('.bootstrap-select').find('.dropdown-menu').remove();        
        loadDropDownFromDatabase('/accounts-api/search-active-accounts-for-dropdown', "", "", $('#'+newID).find('#chequeGlAccountID'));

        $('#'+newID).find('#creditGlAccountID').removeData('AjaxBootstrapSelect');        
        $('#'+newID).find('#creditGlAccountID').removeData('selectpicker');           
        $('#'+newID).find('#creditGlAccountID').siblings('.bootstrap-select').remove();           
        $('#'+newID).find('#creditGlAccountID').siblings('.bootstrap-select').find('.dropdown-menu').remove();        
        loadDropDownFromDatabase('/accounts-api/search-active-accounts-for-dropdown', "", "", $('#'+newID).find('#creditGlAccountID'));

        $('#'+newID).find('#bankTransferGlAccountID').removeData('AjaxBootstrapSelect');        
        $('#'+newID).find('#bankTransferGlAccountID').removeData('selectpicker');           
        $('#'+newID).find('#bankTransferGlAccountID').siblings('.bootstrap-select').remove();           
        $('#'+newID).find('#bankTransferGlAccountID').siblings('.bootstrap-select').find('.dropdown-menu').remove();        
        loadDropDownFromDatabase('/accounts-api/search-active-accounts-for-dropdown', "", "", $('#'+newID).find('#bankTransferGlAccountID'));

        $('#'+newID).find('#lcGlAccountID').removeData('AjaxBootstrapSelect');        
        $('#'+newID).find('#lcGlAccountID').removeData('selectpicker');           
        $('#'+newID).find('#lcGlAccountID').siblings('.bootstrap-select').remove();           
        $('#'+newID).find('#lcGlAccountID').siblings('.bootstrap-select').find('.dropdown-menu').remove();        
        loadDropDownFromDatabase('/accounts-api/search-active-accounts-for-dropdown', "", "", $('#'+newID).find('#lcGlAccountID'));

        $('#'+newID).find('#ttGlAccountID').removeData('AjaxBootstrapSelect');        
        $('#'+newID).find('#ttGlAccountID').removeData('selectpicker');           
        $('#'+newID).find('#ttGlAccountID').siblings('.bootstrap-select').remove();           
        $('#'+newID).find('#ttGlAccountID').siblings('.bootstrap-select').find('.dropdown-menu').remove();        
        loadDropDownFromDatabase('/accounts-api/search-active-accounts-for-dropdown', "", "", $('#'+newID).find('#ttGlAccountID'));



        //for postdated cheque
        $("#postdatedChequeGroup", NewPaymentMethodDiv).hide();
        $(".postdated-cheque-date", NewPaymentMethodDiv).hide();
        NewPaymentMethodDiv.find('.postdated-cheque').attr('checked', false);
        postdatedDate = $('.postdated-cheque-date').datepicker({
            format: 'yyyy-mm-dd',
            onRender: function(date) {
                return date.valueOf() < now.valueOf() ? 'disabled' : '';
            }
        }).on('changeDate', function(e) {
            postdatedDate.hide();
        }).data('datepicker');
    });

    $(document).on('click', '.pamentMethodDelete', function() {
        $(this).parents('.payMethod').remove();
        setTotalAdvancePayment();
    });

    $(document).on('change', '.advanceamount', function() {
        var paidAmount = $(this).val();
        if (isNaN(paidAmount) || paidAmount < 0) {
            p_notification(false, eb.getMessage('ERR_ADVPAY_AM_SH_INT_AND_POS'));
            $(this).val(0.00);
        }
        setTotalAdvancePayment();
    });

    $(document).on('change', '.giftCardID', function() {
        var giftCardID = $(this).val();
        var expireDate = GIFTCARDLIST[giftCardID].giftCardExpireDate;
        var value = GIFTCARDLIST[giftCardID].giftCardValue;
        var flag = 0;
        $('.giftCardID').each(function() {
            if (giftCardID == $(this).val()) {
                flag++;
            }
        });
        if (flag > 1) {
            p_notification(false, eb.getMessage('ERR_PAY_METHOD_GIFR_CARD_ALREDY_SELECT'));
            $(this).val('');
            value = 0.00;
        } else if (expireDate) {
            var exdate = new Date(expireDate);
            var today = new Date();
            if (exdate < today) {
                p_notification(false, eb.getMessage('ERR_PAY_METHOD_GIFR_CARD_EXPIRED'));
                $(this).val('');
                value = 0.00;
            }
        }
        $(this).parents('.payMethod').find(".advanceamount").val(value).trigger('change');
    });


    function setTotalAdvancePayment() {
        totalPaidAmount = 0.00;
        $('.payMethod').each(function() {
            var methodAmount = $(this).find('.advanceamount').val();
            totalPaidAmount += parseFloat(methodAmount);
        });
        $('#totalpayment').text(numberWithCommas(totalPaidAmount.toFixed(2)));
    }

    $('#customCurrencyId').on('change', function() {
    	
    	if($(this).data('baseid') == $(this).val()){
    		$('#customCurrencyRate').val(0);
    		$('#customCurrencyRate').attr('disabled',true);
    	}else{
    		$('#customCurrencyRate').attr('disabled',false);
    	}

        eb.ajax({
            type: 'POST',
            url: BASE_URL + '/currencySymbol/getCustomCurrencyByCustomCurrencyId',
            data: {customCurrencyId: $(this).val()},
            success: function(respond) {
                if (respond.status == true) {
                	$('#customCurrencyRate').val(respond.data.currencyRate);
                    $('.cCurrency').text(respond.data.currencySymbol);
                    customCurrencyRate =respond.data.currencyRate;
                }
            }
        });
    });

});
