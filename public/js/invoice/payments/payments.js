
/**
 * @author ASHAN <ashan@thinkcube.com>
 * This file contains form validation
 */
var locationID = '';
var idInvoice = '';
var custIDPay = '';
var invocieID;
var selectedCustomerID = 0;
var methods = [];
var loyalty = {};
var customCurrencyRate = 1;
var $selection = 'Customer Name';
var isCustomerNameEmpty=true;
var isInvoiceIdEmpty=true;
var tempInvID = false;
var ignoreBudgetLimitFlag = false;
var dimensionData = {};

loyalty.isAvailable = function() {
    return loyalty.cust_loyal_id != undefined ? true : false;
};

loyalty.calculateLoyaltyPoints = function(paidAmount, totalBill) {
    if (loyalty.min <= totalBill && loyalty.max >= totalBill) {
        loyalty.earned_points = totalBill / loyalty.earning_ratio;
    }
    else if (loyalty.max <= totalBill) {
        loyalty.earned_points = loyalty.max / loyalty.earning_ratio;
    }
};

loyalty.isValid = function(paidAmount, totalBill) {

    loyalty.earned_points = 0;
    loyalty.redeemed_points = 0;

    if (!loyalty.isAvailable) {
        return false;
    }


    if (loyalty.available_points * loyalty.redeem_ratio >= paidAmount) {
        loyalty.redeemed_points = paidAmount / loyalty.redeem_ratio;
        return true;
    }
    return false;
};

loyalty.getLoyaltySaveData = function() {
    if (!loyalty.isAvailable()) {
        return {};
    }

    return {
        cust_loyalty_id: loyalty.cust_loyal_id,
        earned_points: loyalty.earned_points,
        redeemed_points: loyalty.redeemed_points,
        current_points: loyalty.available_points,
    };
};

$(document).ready(function() {

    loadDropDownFromDatabase('/accounts-api/search-active-accounts-for-dropdown', "", "", '#cashGlAccountID');
    loadDropDownFromDatabase('/accounts-api/search-active-accounts-for-dropdown', "", "", '#chequeGlAccountID');
    loadDropDownFromDatabase('/accounts-api/search-active-accounts-for-dropdown', "", "", '#creditGlAccountID');
    loadDropDownFromDatabase('/accounts-api/search-active-accounts-for-dropdown', "", "", '#bankTransferGlAccountID');
    loadDropDownFromDatabase('/accounts-api/search-active-accounts-for-dropdown', "", "", '#lcGlAccountID');
    loadDropDownFromDatabase('/accounts-api/search-active-accounts-for-dropdown', "", "", '#ttGlAccountID');

    invoiceLeftToPay = new Array();
    var flag = 0;
    var cus_change = false;
    var addpayment = BASE_URL + '/customerPaymentsAPI/addAllPayment';
    var url = BASE_URL + '/customerPaymentsAPI/check-credit';
    var paymentTotal = 0.00;
    var restBalance = 0.00;
    var paymentMethodIncrement = 1;
    var oldDiscountAmount = 0.00;
    var dimensionArray = {};
    var dimensionTypeID = null;
    var autoDiemsnion =false;

    $('#customCurrencyRate').val('1.00').attr('disabled',true);
    $('#customCurrencyRate').on('change',function(){
    	if(isNaN($(this).val())){
    		p_notification(false, eb.getMessage('ERR_INVO_CURRENCY_RATE_SHBE_NUM'));
            $(this).val(customCurrencyRate);
            $('#customCurrencyRate').focus();
            return false;
    	}else{
    		customCurrencyRate = $(this).val();
    	}
    });

    $('#dimensionView').on('click', function(e) {
        clearDimensionModal();
        var paymentID = $('#paymentID').val();

        if (!$.isEmptyObject(dimensionData)) {
            $.each(dimensionData[paymentID], function(index, val) {
                dimensionArray[index] = val;                
            });
            $.each(dimensionArray, function(index, value) {
                var newTrID = 'tr_' + value.dimensionTypeId;
                var clonedRow = $($('#dimensionPreSample').clone()).attr('id', newTrID).addClass('addedDimensions');
                $("input[name='dimensionType']", clonedRow).val(value.dimensionTypeTxt);
                $("input[name='dimensionValue']", clonedRow).val(value.dimensionValueTxt);
                clonedRow.removeClass('hidden');
                clonedRow.insertBefore('#dimensionPreSample');
                clearAddedNewDimensionRow();
            });
        }
        $('#addDimensionModal').modal('show');
    });

    // dimension modal
    function clearDimensionModal()
    {
        $("#addDimensionModal #dimensionTable tbody .addedDimensions").remove();
        dimensionArray = {};
    }


    $('.dimenisonchange').on('change', function(e) {
        dimensionTypeID = this.value;
        getDimensionValueListByDimensionType(dimensionTypeID, function() {
        });
    });



    function getDimensionValueListByDimensionType(dimensionTypeID, callback) {
        switch (dimensionTypeID) {
            case "job":
                $('.dimensionVal').addClass('hidden');
                $('.dimensionProjectNo').addClass('hidden');
                $('.dimensionJobNo').removeClass('hidden');
                break;
            case "project":
                $('.dimensionVal').addClass('hidden');
                $('.dimensionProjectNo').removeClass('hidden');
                $('.dimensionJobNo').addClass('hidden');
                break;
           default:
                $('.dimensionVal').removeClass('hidden');
                $('.dimensionProjectNo').addClass('hidden');
                $('.dimensionJobNo').addClass('hidden');
                $.ajax({
                    type: 'POST',
                    url: BASE_URL + '/dimension-api/getRelatedDimensionValues',
                    data: {dimensionId: dimensionTypeID},
                    success: function(respond) {
                        //empty options
                        $('#dimensionVal').empty();
                        $('#dimensionVal').append($("<option>", {value: '', html: 'Select dimension value'}));
                        $(respond.data).each(function(index, dimValue) {
                            $.each(dimValue, function(ind, val) {
                                $('#dimensionVal').append($("<option>", {value: ind, html: val.value}));
                            });
                        });
                        callback();
                    }
                });
                break;
        }
    }

    $('#add_dimension').on('click', function(e) {
        e.preventDefault();
        //if plus button is clicked then disabled the button
        //if button is clicked more than one, then it does not continue
        if ($(this).is(':disabled')) {
            return false;
        } else {
            $(this).attr('disabled', true);
            var $thisRow = $(this).parents('tr');
            if ($('.dimenisonchange').val() == 0 || $('.dimenisonchange').val() == '' || $('.dimenisonchange').val() == null) {
                p_notification(false, eb.getMessage('ERR_DIMENSION_SELCET'));
                $(this).attr('disabled', false);
            } else {

                if ($('.dimenisonchange').val() != 'job' && $('.dimenisonchange').val() != 'project') {
                    if ($('.dimensionVal').val() == 0 || $('.dimensionVal').val() == '' || $('.dimensionVal').val() == null) {
                        p_notification(false, eb.getMessage('ERR_DIMENSION_VALUE_SELCET'));
                        $(this).attr('disabled', false);
                        return;
                    }
                }

                if ($('.dimenisonchange').val() == 'project') {
                    if ($('.dimensionProjectNo').val() == 0 || $('.dimensionProjectNo').val() == '' || $('.dimensionProjectNo').val() == null) {
                        p_notification(false, eb.getMessage('ERR_DIMENSION_VALUE_SELCET'));
                        $(this).attr('disabled', false);
                        return;
                    }
                }

                if ($('.dimenisonchange').val() == 'job') {
                    if ($('.dimensionJobNo').val() == 0 || $('.dimensionJobNo').val() == '' || $('.dimensionJobNo').val() == null) {
                        p_notification(false, eb.getMessage('ERR_DIMENSION_VALUE_SELCET'));
                        $(this).attr('disabled', false);
                        return;
                    }
                }

                if (dimensionArray[dimensionTypeID]) {
                    p_notification(false, eb.getMessage('ERR_DIMENSION_ALREADY_INSERTED'));
                    $('.dimenisonchange').val('');
                    $('.dimenisonchange').selectpicker('render');
                    $('.dimenisonchange').trigger('change');
                } else {
                    addNewDimensionRow($(this).parents('tr'));
                }
                $(this).attr('disabled', false);
            }
        }
    });

    function addNewDimensionRow(thisRow) {

        var dimensionTypeId = $('.dimenisonchange', thisRow).val();
        var dimensionTypeTxt = $('.dimenisonchange', thisRow).find("option:selected").text();

        switch (dimensionTypeID) {
            case "job":
                var dimensionValueTxt = $('.dimensionJobNo', thisRow).find("option:selected").text();
                var dimensionValueId = $('.dimensionJobNo', thisRow).val();
                break;
            case "project":
                var dimensionValueTxt = $('.dimensionProjectNo', thisRow).find("option:selected").text();
                var dimensionValueId = $('.dimensionProjectNo', thisRow).val();
                break;
           default:
                var dimensionValueTxt = $('.dimensionVal', thisRow).find("option:selected").text();
                var dimensionValueId = $('.dimensionVal', thisRow).val();
                break;
        }
       
        var dimensionDuplicateCheck = false;
        if (!$.isEmptyObject(dimensionArray)) {
            $.each(dimensionArray, function(index, value) {
                if (value.dimensionTypeId == dimensionTypeId) {
                    dimensionDuplicateCheck = true;
                }
            });   
        }
       
        if (dimensionDuplicateCheck) {
            p_notification(false, eb.getMessage("ERR_DIMENSION_ALREADY_INSERTED"));
            return false;
        }

        var newTrID = 'tr_' + dimensionTypeId;
        var clonedRow = $($('#dimensionPreSample').clone()).attr('id', newTrID).addClass('addedDimensions');
        $("input[name='dimensionType']", clonedRow).val(dimensionTypeTxt);
        $("input[name='dimensionValue']", clonedRow).val(dimensionValueTxt);
        clonedRow.removeClass('hidden');
        clonedRow.insertBefore('#dimensionPreSample');
        dimensionArray[dimensionTypeId] = new dimension(dimensionTypeId, dimensionValueId,dimensionTypeTxt , dimensionValueTxt);
        clearAddedNewDimensionRow();
    }


    function dimension(dimensionTypeId, dimensionValueId, dimensionTypeTxt, dimensionValueTxt) {
        this.dimensionTypeId = dimensionTypeId;
        this.dimensionValueId = dimensionValueId;
        this.dimensionTypeTxt = dimensionTypeTxt;
        this.dimensionValueTxt = dimensionValueTxt;
    }

    function clearAddedNewDimensionRow() {
        $('.dimenisonchange').val('');
        $('.dimenisonchange').selectpicker('render');
        $('.dimenisonchange').trigger('change');
    }

    $('.addDimensions').on('click', function(e) {
        e.preventDefault();

        // validate products before closing modal
        if (!dimensionModalValidate(e)) {
            e.stopPropagation();
            return false;
        } else {
            $('#addDimensionModal').modal('hide');
        }
    });

    function dimensionModalValidate(e) {
        var paymentID = $('#paymentID').val();
        if ($.isEmptyObject(dimensionArray)) {
            dimensionData = {};
            return true;   
        } else {
            var dimensionSingleData = {};
            $.each(dimensionArray, function(index, val) {
                dimensionSingleData[index] = val;
            });
            dimensionData[paymentID] = dimensionSingleData;
            return true;
        }
    }

    $("#addDimensionModal #dimensionTable").on('click', '.deleteDimensionType', function(e) {
        e.preventDefault();
        var deleteDTrID = $(this).closest('tr').attr('id');
        var deleteDID = deleteDTrID.split('tr_')[1].trim();
        bootbox.confirm('Are you sure you want to remove this dimension ?', function(result) {
            if (result == true) {
                delete dimensionArray[deleteDID];
                $('#' + deleteDTrID).remove();
            }
        });
    });

    loadDropDownFromDatabase('/job-api/searchJobsForDropdown', "", 0, '.dimensionJobNo');
    $('.dimensionJobNo').selectpicker('refresh');
    $('.dimensionJobNo').on('change', function() {
        var dimensionValue = $(this).val();
    });

    loadDropDownFromDatabase('/api/project/searchProjectsForDropdown', "", 0, '.dimensionProjectNo');
    $('.dimensionProjectNo').selectpicker('refresh');
    $('.dimensionProjectNo').on('change', function() {
        var dimensionValue = $(this).val();
    });

    //$('#InvoiceID').selectpicker('hide');
    $('#invoiceId').hide();
    $('#invoiceCustomer').hide();
    $('#invoiceSearchType').on('change', function(e) {
        /*$selection = $('#invoiceSearchType').val();
        if ($selection == 'Customer Name') {
            $('#customerID').selectpicker('show');
            $('#customerID').selectpicker('render');
            $('#InvoiceID').selectpicker('hide');
            $('#InvoiceID').selectpicker('render');
            $('#customCurrencyId').attr('disabled', false);
        } else if ($selection == 'Invoice ID') {
            $('#InvoiceID').selectpicker('show');
            $('#InvoiceID').selectpicker('render');
            $('#customerID').selectpicker('hide');
            $('#customerID').selectpicker('render');
            $('#customCurrencyId').val('');
            $('#customCurrencyId').selectpicker('render');
            $('#customCurrencyId').attr('disabled', true);
        }*/

        $selection = $('#invoiceSearchType').val();
        $('#customerName').hide();
        $('#invoiceId').hide();
        //$('#purchaseVoucherId').hide();
        //currentlySelectedInvoiceID = '';
        //currentlySelectedPVID = '';
        //supIDPay = '';
        if ($selection == 'Customer Name') {
            $('#customerName').show();
            $('#customCurrencyId').attr('disabled', false);
            $('#dimension_div').removeClass('hidden');
            autoDiemsnion = false;
        } else if ($selection == 'Invoice ID') {
            $('#dimension_div').addClass('hidden');
            autoDiemsnion = true;
            $('#invoiceId').show();
            $('#customCurrencyId').val('');
            $('#customCurrencyId').selectpicker('render');
            $('#customCurrencyId').attr('disabled', true);
        }
    });

    $('#reset-customer-invoiceId-button').on('click', function() {
        location.reload();
    });

    locationID = $('#idOfLocation').val();

    $("#paymentbill").hide();
    $("#creditGroup").hide();
    $("#checkGroup").hide();
    $("#bankTransferGroup").hide();
    $("#giftCardGroup").hide();
    $("#postdatedChequeGroup").hide();
    $("#lcGroup").hide();
    $("#ttGroup").hide();
    $(".postdated-cheque-date").hide();

    var nowTemp = new Date();
    var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);
    var postdatedDate = $('.postdated-cheque-date').datepicker({
        format: 'yyyy-mm-dd',
        onRender: function(date) {
            return date.valueOf() < now.valueOf() ? ((U_DATE_R_OVERRIDE)?'':'disabled') : '';
        }
    }).on('changeDate', function(e) {
        postdatedDate.hide();
    }).data('datepicker');

    var day = nowTemp.getDate() < 10 ? '0' + nowTemp.getDate() : nowTemp.getDate();
    var month = (nowTemp.getMonth() + 1) < 10 ? '0' + (nowTemp.getMonth() + 1) : (nowTemp.getMonth() + 1);
    $("#currentdate").val(eb.getDateForDocumentEdit('#currentdate', day, month, nowTemp.getFullYear()));
     if(!U_DATE_R_OVERRIDE){
        $('#currentdate').attr('disabled',true);
    } else {
        $('#currentdate').attr('disabled',false);
    }
    var test = $('#currentdate').datepicker({
        format: 'yyyy-mm-dd',
        onRender: function(date) {
            return date.valueOf() < now.valueOf() ? ((U_DATE_R_OVERRIDE)?'':'disabled') : '';
        }
    }).on('changeDate', function(e) {
        test.hide();
    }).data('datepicker');

    $(document).on('change', '.postdated-cheque', function() {
        if ($(this).is(":checked")) {
            $(this).parents('.payMethod').find(".postdated-cheque-date").show();
        } else {
            $(this).parents('.payMethod').find(".postdated-cheque-date").hide();
        }
    });

    $('#advance').on('click', function(e) {
        e.preventDefault();
        window.location.assign(BASE_URL + '/customerPayments/advancePayment');
    });


    loadDropDownFromDatabase('/invoice-api/search-open-sales-invoice-for-payments-dropdown', "", 0, '#InvoiceID');

    $('#InvoiceID').selectpicker('refresh');
    $('#InvoiceID').on('change', function() {

        if ($(this).val() > 0 && invocieID != $(this).val()) {
            invocieID = $(this).val();
            isInvoiceIdEmpty = false;
            changeEvent(invocieID, "Invoice");
        }else if($(this).val() == 0 || $(this).val() == 'undefined'){
            isInvoiceIdEmpty = true;
        }else if(invocieID == $(this).val()){
            isInvoiceIdEmpty = false;
        }
    });


    loadDropDownFromDatabase('/customerAPI/search-customers-for-dropdown', "", 0, '#customerID');

    $('#customerID').selectpicker('refresh');
    $('#customerID').on('change', function() {
        if ($(this).val() > 0 && selectedCustomerID != $(this).val()) {
            selectedCustomerID = $(this).val();
            var customerID = $(this).val();
            isCustomerNameEmpty = false;
            changeEvent(customerID, "Customer");
            setLoyaltyData(customerID);
        }else if($(this).val() == 0 || $(this).val() == 'undefined'){
            isCustomerNameEmpty = true;
        }else if(selectedCustomerID == $(this).val()){
            isCustomerNameEmpty= false;
        }
    });

    $('#customCurrencyId').on('change', function() {

    	if($(this).data('baseid') == $(this).val()){
    		$('#customCurrencyRate').val(0);
    		$('#customCurrencyRate').attr('disabled',true);
    	}else{
    		$('#customCurrencyRate').attr('disabled',false);
    	}

        if ($('#invoiceSearchType').val() == "Customer Name" && selectedCustomerID != 0) {
            var customerID = selectedCustomerID;
            changeEvent(customerID, "Customer");
            setLoyaltyData(customerID);
        }
        eb.ajax({
            type: 'POST',
            url: BASE_URL + '/currencySymbol/getCustomCurrencyByCustomCurrencyId',
            data: {customCurrencyId: $(this).val()},
            success: function(respond) {
                if (respond.status == true) {
                	$('#customCurrencyRate').val(respond.data.currencyRate);
                    $('.cCurrency').text(respond.data.currencySymbol);
                    customCurrencyRate = parseFloat(respond.data.currencyRate);
                }
            }
        });
    });

    function setLoyaltyData(custID) {
        eb.ajax({
            type: 'post',
            url: BASE_URL + '/customerPaymentsAPI/getCustomerLoyaltyCard',
            data: {customerID: custID},
            success: function(res) {
                if (res['status']) {
                    loyalty.available_points = (res.data['available_points'] == null ? 0 : res.data['available_points']);
                    loyalty.card_name = res.data['card_name'];
                    loyalty.code = res.data['code'];
                    loyalty.cust_loyal_id = res.data['cust_loyal_id'];
                    loyalty.earning_ratio = res.data['earning_ratio'];
                    loyalty.max = res.data['max'];
                    loyalty.min = res.data['min'];
                    loyalty.redeem_ratio = res.data['redeem_ratio'];

                    //
                    $('#loyalty_name').text(loyalty.card_name);
                    $('#loyalty_code').text(loyalty.code);
                    $('#loyalty_current_points').text(loyalty.available_points);
                    $('#loyalty_current_points_val').text(loyalty.available_points * loyalty.redeem_ratio);
                    $('#loyalty_details').removeClass('hidden');
                } else {
                    $('#loyalty_details').addClass('hidden');
                    loyalty.available_points = undefined;
                    loyalty.card_name = undefined;
                    loyalty.code = undefined;
                    loyalty.cust_loyal_id = undefined;
                    loyalty.earning_ratio = undefined;
                    loyalty.max = undefined;
                    loyalty.min = undefined;
                    loyalty.redeem_ratio = undefined;
//                                p_notification(false, "Wrong Invoice ID.");
                }
            },
            async: false
        });
    }


    function changeEvent(key, type) {
        clearPaymentScreen();
        flag++;
        $('#paymentbill').addClass('margin_top margin_bottom');
        $("#crnote").hide();
        if (type == 'Customer') {
            $('#InvoiceID').val('');
            value = "custome=" + key;
            customerID = key;
        } else if (type == 'Invoice') {
            $('#currentCredit').val('0.00');
            value = "invoice=" + key;
            if ($('#InvoiceID').val() != '') {
                var inv = BASE_URL + '/customerPaymentsAPI/checkInvoice';
                eb.ajax({
                    type: 'post',
                    url: inv,
                    data: {invoiceID: key},
                    success: function(invoicedata) {
                        if (invoicedata['state'] == 'true') {
                            if (invoicedata['value'].state == 'full') {
                                p_notification(false, eb.getMessage('SUCC_PAY_FULL_PAYMENT'));
                            } else {
                                customerID = invoicedata['value'].customerID;
                                $('#customCurrencyId').val(invoicedata['value'].customCurrencyId);
                                $('#customCurrencyId').selectpicker('render');
                                $('#customCurrencyId').attr('disabled', true);
                                $('#customCurrencyId').trigger('change');
                                setLoyaltyData(customerID);
                            }
                        } else if(invoicedata.value == 'inactiveCustomer'){
                                p_notification(false, invoicedata.msg);
                                setTimeout(function(){
                                    window.location.reload();
                                },2000);
                        }
                    },
                    async: false
                });
            }
        }
        //console.log("qwe");
        $("#paymentbill").show();
        if (cus_change) {
            var s = document.getElementById('paymentbill');
            s.value = '';
        }
        cus_change = true;
        $('#appendtable').html('');
        var customCurrencyID = $('#customCurrencyId').val();
        eb.ajax({
            type: 'post',
            url: BASE_URL + '/customerPaymentsAPI/invoice?id=' + value + '&cid=' + customCurrencyID,
            success: function(data) {
                $('#appendtable').html(data);
            },
            async: false
        });

        var amount;
        var discount;
        var totalamount;
        $('#discountAmount').keyup(function() {
            if ($('#amount').val()) {
                if (isNaN($('#discountAmount').val())) {
                    $('#discountAmount').val('');
                    p_notification(false, eb.getMessage('ERR_PAY_DISCOUNT_NUMR'));
                    $('#totalpayment').html(numberWithCommas(parseFloat($('#amount').val().replace(/,/g, '')).toFixed(2)));
                } else {
                    amount = $('#amount').val().replace(/,/g, '');
                    if (parseFloat($('#discountAmount').val()) >= parseFloat(amount)) {
                        p_notification(false, eb.getMessage('ERR_PAY_DISCOUNT_AMOUNT'));
                        $('#discountAmount').val('');
                        $('#totalpayment').html(numberWithCommas(parseFloat(amount).toFixed(2)));
                    } else {
                        discount = $('#discountAmount').val().replace(/,/g, '');
                        totalamount = amount - discount;
                        $('#totalpayment').html(numberWithCommas(parseFloat(totalamount).toFixed(2)));
                    }
                }
            } else {
                p_notification(false, eb.getMessage('ERR_PAY_ENTER_AMOUNT'));
                $('#discountAmount').val('');
            }
        }).attr('autocomplete', 'off');

        eb.ajax({
            type: 'post',
            url: url,
            data: {customerID: value},
            dataType: "json",
            async: false,
            success: function(values) {
                if (values[0] != null) {
                    customercredit = values[0];
                    $('#currentCredit').val(numberWithCommas(parseFloat(values[0]).toFixed(2)));
                } else {
                    $('#currentCredit').val("0.00");
                }

                if (values[3] != null) {
                    customerbalance = values[3];
                    $('#currentBalance').val(numberWithCommas(parseFloat(values[3]).toFixed(2)));
                } else {
                    $('#currentBalance').val("0.00");
                }

                if (values[1] != '') {
                    $('#customerID').val(values[1]);
                    $("#invoiceCustomer").show();
                    $("#invoiceCustomerName").text(values[1]);
                    $("#displayCustomer").val(values[1]);
                } else {
                    p_notification(false, eb.getMessage('ERR_PAY_CUST_NOTEXIST'));
                    $('#customerID').val('');
                }

                if (values[2] !== null) {
                    $('#paymentTerm').val(values[2]);
                }
                creditnotevalue = 0;
                creditcustomerID = 0;
                currentcredit = 0;
                newbalance = 0;
            }
        });
        $('#creditNote').val('');
        $('#creditNotecheck').prop('checked', false);
    }

    $(document).on('click', '.edi', function() {
        id = (this.parentNode).parentNode.getAttribute('id');
        if (!invoiceLeftToPay[id]) {
            var newlaloc = $('#' + id + " .ttt").html().replace(/,/g, '');
            $(this).val(newlaloc);
            $('#' + id + " .ttt").html('0.00');
            invoiceLeftToPay[id] = newlaloc;
            setPaymentTotal();
        }
    });

    $(document).on('change', '.edi', function() {
        var amount = parseFloat($(this).val());
        id = (this.parentNode).parentNode.getAttribute('id');
        if (amount < 0 || isNaN(amount)) {
            p_notification(false, eb.getMessage('ERR_PAY_AMOUNT_SHOUDBE_NUMBER'));
            amount = 0.00;
        } else if (amount > parseFloat(invoiceLeftToPay[id])) {
            p_notification(false, eb.getMessage('ERR_PAY_TINVPAI_AM_CANT_BE_MORE_THAN_LEFTPAID'));
            amount = 0.00;
        }
        $(this).val(amount);
        var newlaloc = numberWithCommas(parseFloat(invoiceLeftToPay[id] - amount));
        $('#' + id + " .ttt").html(newlaloc);
        if (amount == 0.00) {
            delete invoiceLeftToPay[id];
        }
        setPaymentTotal();
    });

    function setPaymentTotal() {
        var totalOfPayment = 0.00;

        $('#paymentbill').find('tbody > tr').each(function() {
            if ($(this).find('.edi').val()) {
                totalOfPayment += parseFloat($(this).find('.edi').val());
            }
        });
        paymentTotal = totalOfPayment;
        oldDiscountAmount = parseFloat(($('#discountAmount').val() != '') ? $('#discountAmount').val() : 0);
        if (oldDiscountAmount != 0 && paymentTotal != 0.00) {
            paymentTotal = paymentTotal - oldDiscountAmount;
        }
        $('#amount').val(numberWithCommas(totalOfPayment.toFixed(2)));
        $('#restToPaid').val(numberWithCommas(paymentTotal.toFixed(2)));
        $('#totalpayment').html(numberWithCommas(paymentTotal.toFixed(2)));

        var customerCurrentBalance = customerbalance - totalOfPayment;
        if ($('#customCurrencyId').val() == '') {
            $('#currentBalance').val(numberWithCommas(customerCurrentBalance.toFixed(2)));
        }

        setCreditAmountAndRestToPaid();

    }

    $('#creditAmount').on('change', function() {
        var creditAmount = $(this).val();
        if (customercredit == 0.00) {
            p_notification(false, eb.getMessage('ERR_PAY_CUST_NOCRDT_BAL'));
            $(this).val(0.00);
        } else if (isNaN(creditAmount) || creditAmount < 0) {
            p_notification(false, eb.getMessage('ERR_PAY_CREDIT_AMOUNT_SHOUDBE_NUMBER'));
            $(this).val(0.00);
        } else {
            var totalPaidAmount = parseFloat(getTotalPaidAmount());
            var creditAmountCanPay = paymentTotal - totalPaidAmount;
            if (creditAmountCanPay < creditAmount) {
                p_notification(false, eb.getMessage('ERR_PAY_CAMOUNT_AM_CANT_BE_MORE_THAN_RESTPAID'));
                $(this).val(0.00);
            }
        }
        setCreditAmountAndRestToPaid();

    });
    
    $('#paidAmount').on('change', function() {
        var paidAmount = $(this).val();
        var creditAmount = $('#creditAmount').val();
        if (creditAmount > 0) {
            var totalPaidAmount = parseFloat(getTotalPaidAmount());
            var restToPaid = paymentTotal - totalPaidAmount - creditAmount;
            if (restToPaid < 0) {
                p_notification('info', eb.getMessage('INFO_PAY_RESTPAID_CANT_BE_MORE_THAN_CAMOUNT'));
                var restToPaidValue = Math.abs(restToPaid);
                var newCreditAmount = creditAmount - restToPaidValue;
                if (newCreditAmount < 0) {
                    newCreditAmount = 0;
                }
                $('#creditAmount').val(newCreditAmount)
            }
        }
        setCreditAmountAndRestToPaid();
    });

    $('#discountAmount').on('change', function() {
        var discount = $('#discountAmount').val();
        if (discount < 0 || isNaN(discount)) {
            p_notification(false, eb.getMessage('ERR_PAY_DISCOUNT_AMOUNT_SHOUDBE_NUMBER'));
            $(this).val(0.00);
        } else {
            if (paymentTotal <= discount) {
                p_notification(false, eb.getMessage('ERR_PAY_DISCOUNT_AMOUNT'));
                $(this).val(0.00);
            } else {
                var creditAmount = parseFloat(($('#creditAmount').val() != '') ? $('#creditAmount').val() : 0);
                var totalPaidAmount = parseFloat(getTotalPaidAmount());
                var discountCanGive = parseFloat(paymentTotal) - totalPaidAmount - creditAmount + oldDiscountAmount;
                if (discountCanGive < discount) {
                    p_notification(false, eb.getMessage('ERR_PAY_DISC_AM_CANT_BE_MORE_THAN_RESTPAID'));
                    $(this).val(0.00);
                }
            }
        }
        setPaymentTotal();
    });

    function setCreditAmountAndRestToPaid() {
        var creditAmount = parseFloat(($('#creditAmount').val() != '') ? $('#creditAmount').val() : 0);
        var customerCreditAmount = 0.00;
        var paidAmount = 0.00;
        if (creditAmount > paymentTotal) {
            $('#creditAmount').val(paymentTotal);
            creditAmount = paymentTotal;
        }

        if ((creditAmount * customCurrencyRate) > parseFloat(customercredit)) {
            p_notification(false, eb.getMessage('ERR_PAY_CUST_CREDIT_CNT_MO_THAN_AMOUNT'));
            $('#creditAmount').val(0.00);
            creditAmount = 0.00;
            customerCreditAmount = customercredit;
        } else {
            customerCreditAmount = (customercredit - creditAmount).toFixed(2);
        }

//get the total of paid amount form many payment methods
        var totalPaidAmount = getTotalPaidAmount();

        paidAmount = paymentTotal - creditAmount - totalPaidAmount;
        if ($('#customCurrencyId').val() == '') {
            $('#currentCredit').val(numberWithCommas(customerCreditAmount));
        }
        $('#restToPaid').val(numberWithCommas(paidAmount.toFixed(2)));
    }

    $(document).on('change', '.paymentMethod', function() {
        $(".paymentMethod option[value='1']").attr('disabled', false);
        var paymentMethods = $(this).val();
        var $payMethod = $(this).parents('.payMethod');
        $("#cashGroup", $payMethod).hide();
        $("#paidAmount", $payMethod).attr('disabled', false);
        $("#creditGroup", $payMethod).hide();
        $("#checkGroup", $payMethod).hide();
        $("#bankTransferGroup", $payMethod).hide();
        $("#giftCardGroup", $payMethod).hide();
        $("#postdatedChequeGroup", $payMethod).hide();
        $("#lcGroup", $payMethod).hide();
        $("#ttGroup", $payMethod).hide();

        if(paymentMethods == 1){
        	$("#cashGroup", $payMethod).show();
        	  $(".paymentMethod option[value='1']").attr('disabled', 'disabled');
        }else if (paymentMethods == 3) {
            $("#creditGroup", $payMethod).show();
        } else if (paymentMethods == 2) {
            $(this).parents('.payMethod').find("#creditGroup").hide();
            $(this).parents('.payMethod').find("#bankTransferGroup").hide();
            //for postdated cheque
            $(this).parents('.payMethod').find("#postdatedChequeGroup").show();
            $(this).parents('.payMethod').find("#checkGroup").show();

        } else if (paymentMethods == 4) {

            if (loyalty.isAvailable()) {
                $(this).parents('.payMethod').find("#creditGroup").hide();
                $(this).parents('.payMethod').find("#checkGroup").hide();
                $(this).parents('.payMethod').find("#bankTransferGroup").hide();
//                $(this).parents('.payMethod').find("#loyalty_details").show();
            } else {
                p_notification(false, eb.getMessage('ERR_DOESNT_HAVE_LOYALTY_CARD'));
                $('.paymentMethod').val('1').change();
            }



        } else if (paymentMethods == 5) {
            $("#bankTransferGroup", $payMethod).show();
        } else if (paymentMethods == 6) {
            $("#giftCardGroup", $payMethod).show();
            $("#paidAmount", $payMethod).val('').attr('disabled', true);
            $("#giftCardID", $payMethod).val('');
        } else if (paymentMethods == 7) {
            $("#lcGroup", $payMethod).show();
        } else if (paymentMethods == 8) {
            $("#ttGroup", $payMethod).show();
        }
    });

    $(document).on('change', '.accountID', function() {
        var glAcc = $("option:selected", $(this)).attr('data-glaccount');
        var bankdiv = $(this).parents('#bankTransferGroup');

        if (glAcc) {
            $('.main_div').addClass('Ajaxloading');
            eb.ajax({
                type: 'post',
                url: BASE_URL + '/accounts-api/getFinanceAccountsByAccountsID',
                data: {
                    financeAccountsID: glAcc
                },
                dataType: "json",
                async: false,
                success: function(data) {
                    if (data['status'] == true) {
                        var fName= data.data.financeAccounts.financeAccountsCode+'_'+data.data.financeAccounts.financeAccountsName

                        bankdiv.find('#bankTransferGlAccountID').append("<option value='"+glAcc+"'>"+fName+"</option>")
                        bankdiv.find('#bankTransferGlAccountID').val(glAcc).selectpicker('refresh');
                        
                    }
                    $('.main_div').removeClass('Ajaxloading');
                },
            });
        } else {
            bankdiv.find('.accountID').attr('disabled', true);
        }
    });

    $(document).on('change', '.bankID', function() {
        var bankdiv = $(this).parents('#bankTransferGroup');
        var bankID = $(this).val();
        var bank = $(this);
        var bankName = $("option:selected", $(this)).text();
        bankdiv.find('.accountID').find('option').each(function() {
            if ($(this).val()) {
                $(this).remove();
            }
        });
        if (bankID) {
            $('.main_div').addClass('Ajaxloading');
            eb.ajax({
                type: 'post',
                url: BASE_URL + '/account-api/bankAccountListForDropdown',
                data: {
                    bankId: bankID,
                    withGLAccount: true
                },
                dataType: "json",
                async: false,
                success: function(data) {
                    if (data['status'] == true) {
                        if ($.isEmptyObject(data['data'].list)) {
                            p_notification(false, eb.getMessage('ERR_DOESNT_HAVE_ANY_ACC_FOR_THIS_BANK', bankName));
                            $(bank).val('');
                            bankdiv.find('.accountID').attr('disabled', true);
                        } else {
                            $.each(data['data'].list, function(key, value) {
                                var option = "<option data-glaccount="+value.glAccountID+" value=" + value.value + ">" + value.text + "</option>";
                                bankdiv.find('.accountID').attr('disabled', false);
                                bankdiv.find('.accountID').append(option);
                            });
                        }
                    }
                    $('.main_div').removeClass('Ajaxloading');
                },
            });
        } else {
            bankdiv.find('.accountID').attr('disabled', true);
        }
    });

    $(document).on('change', '.giftCardID', function() {
        var giftCardID = $(this).val();
        var expireDate = GIFTCARDLIST[giftCardID].giftCardExpireDate;
        var value = GIFTCARDLIST[giftCardID].giftCardValue;
        var flag = 0;
        $('.giftCardID').each(function() {
            if (giftCardID == $(this).val()) {
                flag++;
            }
        });
        if (flag > 1) {
            p_notification(false, eb.getMessage('ERR_PAY_METHOD_GIFR_CARD_ALREDY_SELECT'));
            $(this).val('');
            value = 0.00;
        } else if (expireDate) {
            var exdate = new Date(expireDate);
            var today = new Date();
            if (exdate < today) {
                p_notification(false, eb.getMessage('ERR_PAY_METHOD_GIFR_CARD_EXPIRED'));
                $(this).val('');
                value = 0.00;
            }
        }
        $(this).parents('.payMethod').find(".paidAmount").val(value).trigger('change');
    });

    $('#paymentMethodAdd').on('click', function() {
        paymentMethodIncrement++;
        var NewPaymentMethodDiv = $('#payMethod_1').clone();
        NewPaymentMethodDiv.attr('id', 'payMethod_' + paymentMethodIncrement);
        var newID = 'payMethod_' + paymentMethodIncrement;

        $('.pamentMethodDelete', NewPaymentMethodDiv).removeClass('hidden');
        $('#creditGroup', NewPaymentMethodDiv).hide();
        $('#checkGroup', NewPaymentMethodDiv).hide();
        $('#bankTransferGroup', NewPaymentMethodDiv).hide();
        $('#giftCardGroup', NewPaymentMethodDiv).hide();
        $('#lcGroup', NewPaymentMethodDiv).hide();
        $('#ttGroup', NewPaymentMethodDiv).hide();
        $('#paidAmount', NewPaymentMethodDiv).val('').attr('disabled', false);
        $('#cashGroup', NewPaymentMethodDiv).show();

        var flag = 0;
        $(".paymentMethod", ".addPaymentMethod").each(function() {
            if ($(this).val() == 1)
                flag = 1;
        });
        if (flag == 1) {
            $('#cashGroup', NewPaymentMethodDiv).hide();
            $(".paymentMethod option[value='1']", NewPaymentMethodDiv).attr('disabled', 'disabled');
            $(".paymentMethod", NewPaymentMethodDiv).val(2);
            $('#checkGroup', NewPaymentMethodDiv).show();
        } else {
            $(".paymentMethod option[value='1']", '.addPaymentMethod').attr('disabled', 'disabled');
        }
        $('.addPaymentMethod').append(NewPaymentMethodDiv);
        //set selectpickers to the clonedPayment Methods
        $('#'+newID).find('#cashGlAccountID').removeData('AjaxBootstrapSelect');
        $('#'+newID).find('#cashGlAccountID').removeData('selectpicker');
        $('#'+newID).find('#cashGlAccountID').siblings('.bootstrap-select').remove();
        $('#'+newID).find('#cashGlAccountID').siblings('.bootstrap-select').find('.dropdown-menu').remove();
        loadDropDownFromDatabase('/accounts-api/search-active-accounts-for-dropdown', "", "", $('#'+newID).find('#cashGlAccountID'));

        $('#'+newID).find('#chequeGlAccountID').removeData('AjaxBootstrapSelect');
        $('#'+newID).find('#chequeGlAccountID').removeData('selectpicker');
        $('#'+newID).find('#chequeGlAccountID').siblings('.bootstrap-select').remove();
        $('#'+newID).find('#chequeGlAccountID').siblings('.bootstrap-select').find('.dropdown-menu').remove();
        loadDropDownFromDatabase('/accounts-api/search-active-accounts-for-dropdown', "", "", $('#'+newID).find('#chequeGlAccountID'));

        $('#'+newID).find('#creditGlAccountID').removeData('AjaxBootstrapSelect');
        $('#'+newID).find('#creditGlAccountID').removeData('selectpicker');
        $('#'+newID).find('#creditGlAccountID').siblings('.bootstrap-select').remove();
        $('#'+newID).find('#creditGlAccountID').siblings('.bootstrap-select').find('.dropdown-menu').remove();
        loadDropDownFromDatabase('/accounts-api/search-active-accounts-for-dropdown', "", "", $('#'+newID).find('#creditGlAccountID'));

        $('#'+newID).find('#bankTransferGlAccountID').removeData('AjaxBootstrapSelect');
        $('#'+newID).find('#bankTransferGlAccountID').removeData('selectpicker');
        $('#'+newID).find('#bankTransferGlAccountID').siblings('.bootstrap-select').remove();
        $('#'+newID).find('#bankTransferGlAccountID').siblings('.bootstrap-select').find('.dropdown-menu').remove();
        loadDropDownFromDatabase('/accounts-api/search-active-accounts-for-dropdown', "", "", $('#'+newID).find('#bankTransferGlAccountID'));

        $('#'+newID).find('#lcGlAccountID').removeData('AjaxBootstrapSelect');
        $('#'+newID).find('#lcGlAccountID').removeData('selectpicker');
        $('#'+newID).find('#lcGlAccountID').siblings('.bootstrap-select').remove();
        $('#'+newID).find('#lcGlAccountID').siblings('.bootstrap-select').find('.dropdown-menu').remove();
        loadDropDownFromDatabase('/accounts-api/search-active-accounts-for-dropdown', "", "", $('#'+newID).find('#lcGlAccountID'));

        $('#'+newID).find('#ttGlAccountID').removeData('AjaxBootstrapSelect');
        $('#'+newID).find('#ttGlAccountID').removeData('selectpicker');
        $('#'+newID).find('#ttGlAccountID').siblings('.bootstrap-select').remove();
        $('#'+newID).find('#ttGlAccountID').siblings('.bootstrap-select').find('.dropdown-menu').remove();
        loadDropDownFromDatabase('/accounts-api/search-active-accounts-for-dropdown', "", "", $('#'+newID).find('#ttGlAccountID'));


        //for postdated cheque
        $("#postdatedChequeGroup", NewPaymentMethodDiv).hide();
        $(".postdated-cheque-date", NewPaymentMethodDiv).hide();
        NewPaymentMethodDiv.find('.postdated-cheque').attr('checked', false);
        postdatedDate = $('.postdated-cheque-date').datepicker({
            format: 'yyyy-mm-dd',
            onRender: function(date) {
                return date.valueOf() < now.valueOf() ? ((U_DATE_R_OVERRIDE)?'':'disabled') : '';
            }
        }).on('changeDate', function(e) {
            postdatedDate.hide();
        }).data('datepicker');

        //addjust alignment
        $('.payMethod')
                .not('#payMethod_1')
                .addClass('panel panel-default');

        $('.payMethod .form-horizontal:first-child')
                .not('#payMethod_1 .form-horizontal:first-child')
                .addClass('panel-body');

    });

    $(document).on('click', '.pamentMethodDelete', function() {
        $(this).parents('.payMethod').remove();
        setCreditAmountAndRestToPaid();
    });

    $('.addPaymentMethod').on('keypress', 'input#paidAmount', function(e) {
        if (e.which == 13) {
            e.preventDefault();
        }
    });

    $(document).on('change', '.paidAmount', function() {
        var paidAmount = $(this).val();
        if (isNaN(paidAmount) || paidAmount < 0) {
            p_notification(false, eb.getMessage('ERR_PAY_PAID_AMOUNT_SHOUDBE_NUMBER'));
            $(this).val(0.00);
        } else {
            var creditAmount = $('#creditAmount').val();
            var restToPaidTotal = paymentTotal - creditAmount;
            var totalPaidAmount = parseFloat(getTotalPaidAmount());
            if (restToPaidTotal < totalPaidAmount) {
                restBalance = totalPaidAmount - restToPaidTotal;
            }
        }
        setCreditAmountAndRestToPaid();

    });

    function getTotalPaidAmount() {
        var totalPaidAmount = 0.00;
        $('.paidAmount').each(function() {
            totalPaidAmount += ($(this).val()) ? parseFloat($(this).val()) : 0.00;
        });
        return totalPaidAmount;
    }

//save function of the payment
    $('#customer-payments-form').on('submit', function(e) {
        $("#addPayment").attr("disabled", true);
        e.preventDefault();
        var dates=[];
        $('#paymentbill tr').each(function() {
            if (!this.rowIndex) return;
            var date = this.cells[1].innerHTML;
            var formatedDate = formateDate(date);
            dates.push(new Date(formatedDate))    
        });
        var issueMinDate =new Date(Math.min.apply(null,dates));
        var paymentDate = new Date($('#currentdate').val());

        var valid = {
            customerID: customerID,
            issueMinDate: issueMinDate,
            paymentReference: $('#paymentID').val(),
            paymentDate: paymentDate,
            // date: $('#currentdate').data("current-date"), //$('#currentdate').html(),
            date: $('#currentdate').val(),
            amount: $('#amount').val().replace(/,/g, ''),
            discountAmount: $('#discountAmount').val().replace(/,/g, ''),
        };

        var Inv_data = {};
        $('#paymentbill').find('tbody > tr').each(function() {
            var amount = $(this).find('.edi').val();
            if (amount > 0) {
                var invoiceID = this.id;
                Inv_data[invoiceID] = amount;

            }
        });
        var tPayamount = 0.00;
        var balance = 0.00;
        var creditAmount = parseFloat(($('#creditAmount').val() != '') ? $('#creditAmount').val() : 0);

        $('.addPaymentMethod').find('.payMethod').each(function() {
            tPayamount += parseFloat($(this).find('.paidAmount').val());
        });

        balance = tPayamount - parseFloat(($('#totalpayment').text().replace(/,/g, ''))) + creditAmount;

        if (validate_form(valid) && !$.isEmptyObject(Inv_data)) {
            $('.main_div').addClass('Ajaxloading');
            var paymentPostData = {
                    customerID: customerID,
                    customerCredit: customercredit,
                    customerBalance: customerbalance,
                    invData: JSON.stringify(Inv_data),
                    paymentID: $('#paymentID').val().replace(/\s/g, ""),
                    // date: $('#currentdate').data("current-date"), //$('#currentdate').html().replace(/\s/g, ""),
                    date: $('#currentdate').val(),
                    paymentTerm: $('#paymentTerm').val(),
                    amount: $('#amount').val().replace(/,/g, ''),
                    discount: $('#discountAmount').val().replace(/,/g, ''),
                    memo: $('#memo').val(),
                    paymentType: 'invoice',
                    locationID: locationID,
                    paymentMethods: methods,
                    creditAmount: $('#creditAmount').val(),
                    loyaltyData: loyalty.getLoyaltySaveData(),
                    restBalance: restBalance,
                    totalPaidAmount: tPayamount,
                    balance: balance,
                    customCurrencyId: $('#customCurrencyId').val(),
                    customCurrencyRate: customCurrencyRate,
                    cardType: $('#cardType').val(),
                    autoDiemsnion : autoDiemsnion,
                    dimensionData : dimensionData,
                    ignoreBudgetLimit : ignoreBudgetLimitFlag
                };

            makePayment(paymentPostData);
        } else {
            $("#addPayment").attr("disabled", false);
            if ($.isEmptyObject(Inv_data)) {
                p_notification(false, eb.getMessage('ERR_PLEASE_SEL_ATLT_ONE_INVOICE'));
            }
        }
    });

    function makePayment(paymentPostData)
    {
        eb.ajax({
            type: 'post',
            url: addpayment,
            data: paymentPostData,
            dataType: "json",
            async: false,
            success: function(data) {
                if (data['state'] == true) {
                    var fileInput = document.getElementById('documentFiles');
                    if(fileInput.files.length > 0) {
                        var form_data = false;
                        if (window.FormData) {
                            form_data = new FormData();
                        }
                        form_data.append("documentID", data['id']);
                        form_data.append("documentTypeID", 7);
                        
                        for (var i = 0; i < fileInput.files.length; i++) {
                            form_data.append("files[]", fileInput.files[i]);
                        }

                        eb.ajax({
                            url: BASE_URL + '/store-files',
                            type: 'POST',
                            processData: false,
                            contentType: false,
                            data: form_data,
                            success: function(res) {
                            }
                        });
                    }

                    p_notification(true, eb.getMessage('SUCC_PAYMENT_ADDPAY', data['code']));
                    documentPreview(BASE_URL + '/customerPayments/viewReceipt/' + data['id'] + '/initial', 'documentpreview', "/customerPaymentsAPI/send-email", function($preview) {
                        $preview.on('hidden.bs.modal', function() {
                            if (!$("#preview:visible").length) {
                                window.location.reload();
                            }
                        });
                    });
                } else if (data['state'] == false) {
                    if (data['data'] == "NotifyBudgetLimit") {
                        bootbox.confirm(data['msg']+' ,Are you sure you want to save this payment ?', function(result) {
                            if (result == true) {
                                ignoreBudgetLimitFlag = true;
                                paymentPostData.ignoreBudgetLimit = true;
                                makePayment(paymentPostData);
                            } else {
                                setTimeout(function(){ 
                                    location.reload();
                                }, 3000);
                            }
                        });
                    } else {
                        p_notification(false, data['msg']);
                        $('.main_div').removeClass('Ajaxloading');
                    }
                }
            },
        });
    }


    function formateDate (date) {
        
        if (DISPLAYSETUPDETAILS['dateFormat'] == 'dd/mm/yyyy') {
            var res = date.split("/");
            var day = res[0];
            var month = res[1];
            var year = res[2];

            return month + '/' + day + '/' + year;
        } else if (DISPLAYSETUPDETAILS['dateFormat'] == 'dd-mm-yyyy') {
            var res = date.split("/");
            var day = res[0];
            var month = res[1];
            var year = res[2];

            return month + '/' + day + '/' + year;
        } else if (DISPLAYSETUPDETAILS['dateFormat'] == 'dd.mm.yyyy') {
            var res = date.split("/");
            var day = res[0];
            var month = res[1];
            var year = res[2];

            return month + '/' + day + '/' + year;
        } else {
            return date;
        }
    }

    function validate_form(valid) {
        if ($('#customerID').val() === '' && $('#InvoiceID').val() === '' && !tempInvID) {
            p_notification(false, eb.getMessage('ERR_PAY_CUSNAME&INVID'));
            return false;
        } else if(isCustomerNameEmpty && $selection == 'Customer Name' && !tempInvID){
            p_notification(false, eb.getMessage('ERR_PAY_CUSNAME&INVID'));
            return false;
        } else if(isInvoiceIdEmpty && $selection == 'Invoice ID' && !tempInvID){
            p_notification(false, eb.getMessage('ERR_PAY_CUSNAME&INVID'));
            return false;
        } else if (valid.paymentReference == '') {
            p_notification(false, eb.getMessage('ERR_PAY_REFNUM'));
            return false;
        } else if (valid.date == null || valid.date == "") {
            p_notification(false, eb.getMessage('ERR_ADVPAY_DATE'));
            return false;
        } else if (valid.paymentDate < valid.issueMinDate) {
            p_notification(false, eb.getMessage('ERR_PAY_DATE'));
            return false;
        } else if (isNaN(valid.amount)) {
            p_notification(false, eb.getMessage('ERR_ADVPAY_AMOUNT_NUMR'));
            return false;
        } else if (valid.amount < 0) {
            p_notification(false, eb.getMessage('ERR_ADVPAY_AMOUNT_NEG'));
            return false;
        } else if (isNaN(valid.discountAmount)) {
            p_notification(false, eb.getMessage('ERR_PAY_DISCOUNT_NUMR'));
            return false;
        } else if (valid.discountAmount < 0) {
            p_notification(false, eb.getMessage('ERR_PAY_DISCOUNT_NEG'));
            return false;
        } else if ($('#restToPaid').val().replace(/,/g, '') > 0) {
            p_notification(false, eb.getMessage('ERR_PAY_TOTAL_SHOULD_BE_PAY'));
            return false;
        } else {
            var checked = true;
            methods = [];
            var creditAmount = parseFloat($('#creditAmount').val().replace(/,/g, ''));
            $('.payMethod').each(function() {
                var methodID = $('.paymentMethod option:selected', this).val();
                var paidAmount = $('.paidAmount', this).val();
                var checquenumber = $('.checquenumber', this).val();
                var bank = $('.bank', this).val();
                var reciptnumber = $('.reciptnumber', this).val();
                var cardnumber = $('.cardnumber', this).val();
                var bankID = $('.bankID', this).val();
                var accountID = $('.accountID', this).val();
                var customerBank = $('.customerBank', this).val();
                var customerAccountNumber = $('.customerAccountNumber', this).val();
                var giftCardID = $('.giftCardID', this).val();
                var lcPaymentReference = $('.lcPaymentReference', this).text();
                var ttPaymentReference = $('.ttPaymentReference', this).text();
                var postdatedStatus = 0;
                var postdatedDate = null;
                var cashAccountID = $(this).find('#cashGlAccountID').val();
                var chequeAccountID = $(this).find('#chequeGlAccountID').val();
                var creditAccountID = $(this).find('#creditGlAccountID').val();
                var bankTransferAccountID = $(this).find('#bankTransferGlAccountID').val();
                var bankTransferRef = $(this).find('#bankTransferRef').val();
                var lcAccountID = $(this).find('#lcGlAccountID').val();
                var ttAccountID = $(this).find('#ttGlAccountID').val();
                //check whether postdated cheque
                if ($('.postdated-cheque', this).is(":checked")) {
                    postdatedStatus = 1;
                    postdatedDate = $('.postdated-cheque-date', this).val();
                }

                methods.push({
                    methodID: methodID,
                    paidAmount: paidAmount,
                    checkNumber: checquenumber,
                    bank: bank,
                    reciptnumber: reciptnumber,
                    cardnumber: cardnumber,
                    bankID: bankID,
                    accountID: accountID,
                    customerBank: customerBank,
                    customerAccountNumber: customerAccountNumber,
                    giftCardID: giftCardID,
                    postdatedStatus: postdatedStatus,
                    postdatedDate: postdatedDate,
                    lcPaymentReference: lcPaymentReference,
                    ttPaymentReference: ttPaymentReference,
                    cashAccountID : cashAccountID,
                    chequeAccountID : chequeAccountID,
                    creditAccountID : creditAccountID,
                    bankTransferAccountID : bankTransferAccountID,
                    lcAccountID : lcAccountID,
                    ttAccountID : ttAccountID,
                    bankTransferRef: bankTransferRef
                });

                if (!paidAmount && !creditAmount) {
                    p_notification(false, eb.getMessage('ERR_PAY_PAID_AM_CAN_BE_EMTY'));
                    $('.paidAmount', this).focus();
                    checked = false;
                    return false;
                } else if (methodID == '') {
                    p_notification(false, eb.getMessage('ERR_PAY_PAID_METHOD_SHBE_SELECT'));
                    $('.advancepaymentMethod', this).focus();
                    checked = false;
                    return false;
                } else if (methodID == 2) {
//                    if (checquenumber == null || checquenumber == "") {
//                        p_notification(false, eb.getMessage('ERR_ADVPAY_CHEQUE'));
//                        $('.checquenumber', this).focus();
//                        checked = false;
//                        return false;
//                    } else

//                    if (checquenumber != "" && checquenumber.length > 8) {
//                        p_notification(false, eb.getMessage('ERR_PAY_CHEQUE_INVALID'));
//                        $('.checquenumber', this).focus();
//                        checked = false;
//                        return false;
//                    } else if (checquenumber != "" && isNaN(checquenumber)) {
//                        p_notification(false, eb.getMessage('ERR_ADVPAY_CHEQUE_NUMR'));
//                        $('.checquenumber', this).focus();
//                        checked = false;
//                        return false;
//                    }
//                    else if (bank == null || bank == "") {
//                        p_notification(false, eb.getMessage('ERR_ADVPAY_BANK_NAME'));
//                        $('.bank', this).focus();
//                        checked = false;
//                        return false;
//                    }

                } else if (methodID == 3) {
//                    if (reciptnumber == null || reciptnumber == "") {
//                        p_notification(false, eb.getMessage('ERR_ADVPAY_RECNO'));
//                        $('.reciptnumber', this).focus();
//                        checked = false;
//                        return false;
//                    } else
                    if (reciptnumber != "" && reciptnumber.length > 12) {
                        p_notification(false, eb.getMessage('ERR_PAY_RECIPTNO_VALIDITY'));
                        $('.reciptnumber', this).focus();
                        checked = false;
                        return false;
                    } else if (cardnumber != "" && isNaN(cardnumber)) {
                        p_notification(false, eb.getMessage('ERR_ADVPAY_CRDTCARD_NUMR'));
                        $('.cardnumber', this).focus();
                        checked = false;
                        return false;
                    }
                } else if (methodID == 5) {
                   if (bankID == null || bankID == "") {
                       p_notification(false, eb.getMessage('ERR_PAY_METHOD_BANK_ID'));
                       $('.bankID', this).focus();
                       checked = false;
                       return false;
                   } else if (accountID == null || accountID == "") {
                       p_notification(false, eb.getMessage('ERR_PAY_METHOD_ACCOUNT_ID'));
                       $('.accountID', this).focus();
                       checked = false;
                       return false;
                   }
                    
                    if (customerAccountNumber != '' && isNaN(customerAccountNumber)) {
                        p_notification(false, eb.getMessage('ERR_PAY_METHOD_CUST_ACCUNT'));
                        $('.customerAccountNumber', this).focus();
                        checked = false;
                        return false;
                    }
                } else if (methodID == 6) {
                    if (giftCardID == null || giftCardID == "") {
                        p_notification(false, eb.getMessage('ERR_PAY_METHOD_GIFT_CARD_SH_SELECT'));
                        $('.giftCardID', this).focus();
                        checked = false;
                        return false;
                    }
                } else if (methodID == 4) {
                    var credit = parseFloat($('#creditAmount').val().replace(/,/g, ''));
                    credit = isNaN(credit) ? 0 : credit;
                    var amount = parseFloat($('#amount').val().replace(/,/g, ''));
                    var totalBill = amount - credit;
                    totalBill = totalBill * customCurrencyRate;
                    var pAmount = paidAmount * customCurrencyRate;

                    if (!loyalty.isValid(pAmount, totalBill)) {
                        p_notification(false, eb.getMessage('ERR_LOYALTY_REDEEM_EXCEED'));
                        checked = false;
                        return false;
                    }
                    loyalty.calculateLoyaltyPoints(0, totalBill);
                    return true;
                }

                if (methodID != 4) {
                    var credit = parseFloat($('#creditAmount').val().replace(/,/g, ''));
                    credit = isNaN(credit) ? 0 : credit;
                    var amount = parseFloat($('#amount').val().replace(/,/g, ''));

                    var totalBill = amount - credit;
                    totalBill = totalBill * customCurrencyRate;
                    loyalty.calculateLoyaltyPoints(0, totalBill);
                }
            });
            if (checked) {
                return true;
            } else {
                return false;
            }

        }
    }

    function clearPaymentScreen() {
        $('#amount').val('0.00');
        $('#totalpayment').text('0.00');
        $('#discountAmount').val('');
        $('#restToPaid').val('');
        $('#creditAmount').val('');
        $('#memo').val('');
        $('.paidAmount').val('');
        $('.payMethod').each(function() {
            if (this.id != 'payMethod_1') {
                $(this).remove();
            }
        });
        customerbalance = 0;
        customercredit = 0;
    }

    idInvoice = $('#idInvoice').val();
    if (idInvoice != "") {
        $('#dimension_div').addClass('hidden');
        autoDiemsnion = true;
        idInvoice = $('#idInvoice').val();
        tempInvID = true;
        var selectedInvoiceCode = $('#selectedInvoiceCode').val();
        $("#InvoiceID").empty();
        $("#InvoiceID").append($("<option></option>")
                .attr("value", idInvoice)
                .text(selectedInvoiceCode));
        $('#InvoiceID').selectpicker('render');
        $('#InvoiceID').show();

        $('#customerID').selectpicker('hide');
        $('#invoiceSearchType').val('Invoice ID').attr('disabled', true);
        changeEvent(idInvoice, "Invoice");
        $("#InvoiceID").attr("disabled", "disabled");
        $("#customerID").attr("disabled", "disabled");
        $(document).on("click", "#cancel", function() {
            window.history.back();
        });
    }

    // $('.addPaymentMethod').on('change', '.paymentMethod', function() {
    //     var methodID = $(this).val();
    //     if (methodID == 1) {
    //         $(".paymentMethod option[value='1']").attr('disabled', 'disabled');
    //     } else {
    //         var flag = 0;
    //         $(".paymentMethod", ".addPaymentMethod").each(function() {
    //             if ($(this).val() == 1)
    //                 flag = 1;
    //         });
    //         if (flag == 0)
    //             $(".paymentMethod option[value='1']").removeAttr('disabled');
    //     }
    // });
});
