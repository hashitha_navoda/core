
var gender = null;

$(document).ready(function() {

	loadDropDownFromDatabase('/accounts-api/search-active-accounts-for-dropdown', "", "", '#customerReceviableAccountID');
	loadDropDownFromDatabase('/accounts-api/search-active-accounts-for-dropdown', "", "", '#customerSalesAccountID');
	loadDropDownFromDatabase('/accounts-api/search-active-accounts-for-dropdown', "", "", '#customerSalesDiscountAccountID');
	loadDropDownFromDatabase('/accounts-api/search-active-accounts-for-dropdown', "", "", '#customerAdvancePaymentAccountID');


	var customerReceviableAccountID = $('#customerReceviableAccountID').data('id');
    if(customerReceviableAccountID!=''){
    	var customerReceviableAccountName = $('#customerReceviableAccountID').data('value');
    	$('#customerReceviableAccountID').append("<option value='"+customerReceviableAccountID+"'>"+customerReceviableAccountName+"</option>")
    	$('#customerReceviableAccountID').val(customerReceviableAccountID).selectpicker('refresh');
    }

    var customerSalesAccountID = $('#customerSalesAccountID').data('id');
    if(customerSalesAccountID!=''){
    	var customerSalesAccountName = $('#customerSalesAccountID').data('value');
    	$('#customerSalesAccountID').append("<option value='"+customerSalesAccountID+"'>"+customerSalesAccountName+"</option>")
    	$('#customerSalesAccountID').val(customerSalesAccountID).selectpicker('refresh');
    }

    var customerSalesDiscountAccountID = $('#customerSalesDiscountAccountID').data('id');
    if(customerSalesDiscountAccountID!=''){
    	var customerSalesDiscountAccountName = $('#customerSalesDiscountAccountID').data('value');
    	$('#customerSalesDiscountAccountID').append("<option value='"+customerSalesDiscountAccountID+"'>"+customerSalesDiscountAccountName+"</option>")
    	$('#customerSalesDiscountAccountID').val(customerSalesDiscountAccountID).selectpicker('refresh');
    }

    var customerAdvancePaymentAccountID = $('#customerAdvancePaymentAccountID').data('id');
    if(customerAdvancePaymentAccountID!=''){
    	var customerAdvancePaymentAccountName = $('#customerAdvancePaymentAccountID').data('value');
    	$('#customerAdvancePaymentAccountID').append("<option value='"+customerAdvancePaymentAccountID+"'>"+customerAdvancePaymentAccountName+"</option>")
    	$('#customerAdvancePaymentAccountID').val(customerAdvancePaymentAccountID).selectpicker('refresh');
    }

    $('#addCustomerModal').on('shown.bs.modal', function() {
        $(this).find("input[name='customerName']").focus();
    });
    var dateOfBirth = $("#customerDateOfBirth").datepicker({onRender: function(date) {
            return date.valueOf() > new Date() ? 'disabled' : '';
        }
    }).on('changeDate', function(ev) {
        dateOfBirth.hide();
    }).data('datepicker');

    $("#male").on('click', function(e) {
        gender = $("#male-id").val();
        $("#male-id").prop('checked', true);
        $("#female-id").prop('checked', false);
        $("#other-id").prop('checked', false);
    });

    $("#female").on('click', function(e) {
        gender = $("#female-id").val();
        $("#female-id").prop('checked', true);
        $("#male-id").prop('checked', false);
        $("#other-id").prop('checked', false);
    });

    $("#other").on('click', function(e) {
        gender = $("#other-id").val();
        $("#other-id").prop('checked', true);
        $("#female-id").prop('checked', false);
        $("#male-id").prop('checked', false);
    });
// add customer form addCustomerMadal
// added by sharmilan
    $('#customerFormsubmit').on('click', function(e) {
        e.preventDefault();
        var addurl = BASE_URL + '/customerAPI/add';
        var newCustomerData = {
            customerTitle: $('#customerTitle', '#addCustomerModal').val(),
            customerGender: gender,
            customerName: $('#customerName', '#addCustomerModal').val().trim(),
            customerCode: $('#customerCode', '#addCustomerModal').val().trim(),
            customerDateOfBirth: $('#customerDateOfBirth').val(),
            customerTelephoneNumber: $('#customerTelephoneNumber', '#addCustomerModal').val().trim(),
            customerCurrency: $('#customerCurrency', '#addCustomerModal').val(),
            customerEmail: $('#customerEmail', '#addCustomerModal').val().trim(),
            customerCategory: $('#customerCategory', '#addCustomerModal').val().trim(),
            customerPriceList: $('#customerPriceList', '#addCustomerModal').val().trim(),
            customreLocationNumber: $('#customreLocationNumber', '#addCustomerModal').val().trim(),
            customerLocationRoadName1: $('#customerLocationRoadName1', '#addCustomerModal').val().trim(),
            customerLocationTown: $('#customerLocationTown', '#addCustomerModal').val().trim(),
            customerLocationCountry: $('#customerLocationCountry', '#addCustomerModal').val(),
            customerCreditLimit: $('#customerCreditLimit', '#addCustomerModal').val().trim(),
            customerPaymentTerm: $('#customerPaymentTerm', '#addCustomerModal').val(),
            customerDiscount: $('#customerDiscount', '#addCustomerModal').val().trim(),
            customerEvent: $('#customerEvent', '#addCustomerModal').val(),
            customerPostalCode: $('#customerPostalCode', '#addCustomerModal').val(),
            customerReceviableAccountID: $('#customerReceviableAccountID', '#addCustomerModal').val(),
            customerSalesAccountID: $('#customerSalesAccountID', '#addCustomerModal').val(),
            customerSalesDiscountAccountID: $('#customerSalesDiscountAccountID', '#addCustomerModal').val(),
            customerAdvancePaymentAccountID: $('#customerAdvancePaymentAccountID', '#addCustomerModal').val(),
        };
        if (validateCustomerInputs(newCustomerData)) {
            var customerProfileDataArray = {};
            customerProfileDataArray.unsavedProfile = {
                customerProfileName: 'Default',
                customerProfileEmail: newCustomerData.customerEmail,
                customerProfileLocationNo: newCustomerData.customreLocationNumber,
                customerProfileLocationRoadName1: newCustomerData.customerLocationRoadName1,
                customerProfileLocationTown: newCustomerData.customerLocationTown,
                customerProfileLocationCountry: newCustomerData.customerLocationCountry,
                customerProfileLocationPostalCode: newCustomerData.customerPostalCode,
            };
            var params = {
                customerTitle: newCustomerData.customerTitle,
                customerName: newCustomerData.customerName,
                customerGender: newCustomerData.customerGender,
                customerCode: newCustomerData.customerCode,
                customerDateOfBirth: newCustomerData.customerDateOfBirth,
                customerTelephoneNumber: newCustomerData.customerTelephoneNumber,
                customerCurrency: newCustomerData.customerCurrency,
                customerCategory: newCustomerData.customerCategory,
                customerPriceList: newCustomerData.customerPriceList,
                customerCreditLimit: newCustomerData.customerCreditLimit,
                customerPaymentTerm: newCustomerData.customerPaymentTerm,
                customerDiscount: newCustomerData.customerDiscount,
                customerEvent: newCustomerData.customerEvent,
                customerPrimaryProfileID: 'unsavedProfile',
                customerProfile: customerProfileDataArray,
                customerReceviableAccountID: newCustomerData.customerReceviableAccountID,
                customerSalesAccountID: newCustomerData.customerSalesAccountID,
                customerSalesDiscountAccountID: newCustomerData.customerSalesDiscountAccountID,
                customerAdvancePaymentAccountID: newCustomerData.customerAdvancePaymentAccountID,
            };
            var addrequest = eb.post(addurl, params);
            addrequest.done(function(addretdata) {
                p_notification(addretdata.status, addretdata.msg);
                if (addretdata.status === true) {
                    customerID = addretdata.data.customerID;
                    clearCustomerdata();
                    $('#customerCode', '#addcustomerform').val(addretdata.data.newCustomerCode);
                    AddCustomerFlag = true;
                    if (typeof getCustomerDetails == 'function') {
                        getCustomerDetails(customerID);
                    }
                    if (!addCustomer) {
                        $('#addCustomerModal').modal('hide');
                    }
                    addCustomer = false;
                }
            });
        }
        return false;
    });
});
// added by sharmilan
function validateCustomerInputs(inputs) {
    var cName = inputs.customerName;
    var cCode = inputs.customerCode;
    var cTPNo = inputs.customerTelephoneNumber;
    var cCurrency = inputs.customerCurrency;
    var cEmail = inputs.customerEmail;
    var cCreditLimit = inputs.customerCreditLimit;
    var cDiscount = inputs.customerDiscount;
    var emailRegex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    var nameregex = /^(?!\s*$).+/;
    if (cName === null || cName === '') {
        p_notification(false, eb.getMessage('ERR_CUST_NAME'));
        return false;
    } else if (!nameregex.test(cName)) {
        p_notification(false, eb.getMessage('ERR_CUST_PROFILE_VALID_NAME'));
        return false;
    } else if (cTPNo === null || cTPNo === "") {
        p_notification(false, eb.getMessage('ERR_CUST_EMPTY_PRIMARY_TP'));
        return false;
    } else if (cCode === null || cCode === "") {
        p_notification(false, eb.getMessage('ERR_CUST_CODE_EMPTY'));
        return false;
    } else if (!isPhoneValid(cTPNo)) {
        p_notification(false, eb.getMessage('ERR_CUST_TPNUM_VALIDITY'));
        return false;
    } else if (cCurrency === null || cCurrency === "") {
        p_notification(false, eb.getMessage('ERR_CUST_CURRENCY'));
        return false;
    } else if (cEmail !== '' && !emailRegex.test(cEmail)) {
        p_notification(false, eb.getMessage('ERR_CUST_PROFILE_VALID_EMAIL'));
        return false;
    } else if (isNaN(cCreditLimit)) {
        p_notification(false, eb.getMessage('ERR_CUST_CRDTLIMIT_NUMR'));
        return false;
    } else if (cCreditLimit < 0) {
        p_notification(false, eb.getMessage('ERR_CUST_CRDTLIMIT_NEG'));
        return false;
    } else if (isNaN(cDiscount) || parseInt(cDiscount) > 100 || cDiscount < 0) {
        p_notification(false, eb.getMessage('ERR_CUST_DISCOUNT_VALIDITY'));
    } else {
        return true;
    }
}

function clearCustomerdata() {
    $('#customerTitle', '#addCustomerModal').val("");
    $('#customerName', '#addCustomerModal').val("");
    $('#customerShortName', '#addCustomerModal').val("");
    $('#customerCurrency').val(1);
    $('#customerTelephoneNumber', '#addCustomerModal').val("");
    $('#customerPaymentTerm').val(1);
    $('#customerCreditLimit', '#addCustomerModal').val("");
    $('#customerCurrentBalance').val('');
    $('#customerCurrentCredit').val('');
    $('#customerDiscount', '#addCustomerModal').val("");
    $('#customerPostalCode', '#addCustomerModal').val("");
}

var isPhoneValid = function(phoneNumber) {
    isValid = true;
    if (phoneNumber.length != 0) {
        if (isNaN(phoneNumber) || phoneNumber.length < 9 || phoneNumber.length > 15) {
            isValid = false;
        }
    }
    return isValid;
}