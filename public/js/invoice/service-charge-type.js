$(document).ready(function(){
    loadDropDownFromDatabase('/accounts-api/search-active-accounts-for-dropdown', "", "", '#serviceChargeTypeAccountID');


    var serviceChargeTypeAccountID = $('#serviceChargeTypeAccountID').data('id');

    $('#serviceChargeTypeAccountID').on('change', function (){
        if ($(this).val() != 0 || $(this).val() != ''){
            serviceChargeTypeAccountID = $(this).val();
        }
    });

    $('#save-service-charge-type').on('click', function () {
        var srCode = $('.service-charge-type-code').val();
        var srName = $('.service-charge-type-name').val();
        if (srCode == '' || srCode == null) {
            p_notification(false, eb.getMessage('ERR_NO_SERVICE_CHARGE_CODE'));
        } else if (srName == '' || srName == null) {
            p_notification(false, eb.getMessage('ERR_NO_SERVICE_CHARGE_NAME'));
        } else if (serviceChargeTypeAccountID == null || serviceChargeTypeAccountID == 0) {
            p_notification(false, eb.getMessage('ERR_NO_SERVICE_CHARGE_ACC'));
        } else {
            var savedDataSet = {
                srcCode: srCode,
                srcName: srName,
                serviceChargeTypeAccountID: serviceChargeTypeAccountID
            };

            eb.ajax({
                    type: 'POST',
                    url: BASE_URL + '/service-charge-type-api/save',
                    data: savedDataSet,
                    success: function(respond) {
                        p_notification(respond.status, respond.msg);
                        if(respond.status) {
                            window.location.reload();
                        }
                    }
            });
            
        }

    });

    $('#service-charge-type-list').on('click', '.delete', function(e) {
        e.preventDefault();
        var thisRowID = $(this).parents('tr').attr('data-srctypeid');

        bootbox.confirm('Are you sure you want to delete this service charge type ?', function(result) {
            if (result === true) {
                eb.ajax({
                    url: BASE_URL + '/service-charge-type-api/delete-service-charge-type',
                    method: 'post',
                    data: {
                        srcTypeID: thisRowID,
                    },
                    dataType: 'json',
                    success: function(data) {
                        p_notification(data.status, data.msg);
                        if (data.status == true) {
                            window.location.reload();
                        }
                       
                    }
                });
            }
        });

    });

    $('#service-charge-type-search-keyword').on('keyup', function(){
        var searchKey = $('#service-charge-type-search-keyword').val();
        eb.ajax({
                type: 'POST',
                url: BASE_URL + '/service-charge-type-api/search',
                data: { searchKey: searchKey},
                success: function(respond) {
                    if (respond.status) {
                        $("#service-charge-type-list").html(respond.html);
                    } else {
                        p_notification(respond.status, respond.msg);
                    }  
                }
        });
    });
    $('#service-charge-type-reset').on('click', function(){
        $('#service-charge-type-search-keyword').val('');
        eb.ajax({
                type: 'POST',
                url: BASE_URL + '/service-charge-type-api/search',
                data: { searchKey: null},
                success: function(respond) {
                    if (respond.status) {
                        $("#service-charge-type-list").html(respond.html);
                    } else {
                        p_notification(respond.status, respond.msg);
                    }  
                }
        });
    });


    $('#back').on('click', function(){
        $('.service-charge-type-code').val('');
        $('.service-charge-type-name').val('');

        $('#serviceChargeTypeAccountID').val('');
        $('#serviceChargeTypeAccountID').selectpicker('render');
        $('#serviceChargeTypeAccountID').trigger('change');
        
    });
});