/**
 * @author Prathap Weerasinghe <prathap@thinkcube.com>
 * This file contains sales Order js
 */
var tmptotal, state = "new";
var tax_rates = new Array();
var products = new Array();
var taxdetails = new Array();
var currentProducts = new Array();
var projectID;
var jobID;
var activityID;
var jobRefType;
var jobRefTypeID;
var selectedProductID;
var customerID;
var customCurrencySymbol = companyCurrencySymbol;
var cusProfID;
var addCustomer = false;
var priceListItems = [];
var $productTable = $("#item_tbl");
var uploadedAttachments = {};
var deletedAttachmentIds = [];
var deletedAttachmentIdArray = [];


function product(pCode, pName, quantity, uom, uPrice, discount, dType, tax, tCost, taxDetails, productType, item_discription) {
    this.item_code = pCode;
    this.item = pName;
    this.quantity = quantity;
    this.discount = discount;
    this.discountType = dType;
    this.unit_price = uPrice;
    this.uom = uom;
    this.total_cost = tCost;
    this.taxes = tax;
    this.taxDetails = taxDetails;
    this.productType = productType;
    this.item_discription = item_discription;
}

$(document).ready(function() {
    $('#disc_presentage').attr('checked', 'checked');
    invoice = true;
    var all_tx_val = new Array();
    var totalTaxList = {};
    var totaldiscount = 0, decimalPoints = 0, itemDecimalPoints = 0;
    var priceListId = '';

    locationOut = $('#idOfLocation').val();

    $('.deli_row, #deli_form_group, #tax_td').hide();
    $("#customer_more").hide();
    $("#currency").attr("disabled", "disabled");
    $("#moredetails").click(function() {
        $("#customer_more").slideDown();
        $('#moredetails').hide();
        $('#customerCurrentBalance', '#addcustomerform').attr('disabled', true);
        $('#customerCurrentCredit', '#addcustomerform').attr('disabled', true);
    });

    var getAddRow = function(proIncID) {
        return $('tr.add-row:not(.sample)', $productTable);
    };

    var Edit_Quo_ID = $('#edit_cus_order_ID').val();
    var isView = $('#is_view').val();
    loadCustomerOrderData(Edit_Quo_ID);

    function loadCustomerOrderData(CustomerOrderID) {
        var req = eb.post(BASE_URL + '/customerOrder-api/retriveCustomerOrder', {'customerOrderID': CustomerOrderID});
        req.done(function(res) {
            if (res['status']) {
                var customerOrderDate = new Date(Date.parse(res['data'].customerOrderDate));
                var amount = (res['data'].customerOrderAmount != null && res['data'].customerOrderAmount != '') ? parseFloat(res['data'].customerOrderAmount) : '';
                $('#cust_id').val(res['data'].customerID);
                $("#cust_name").empty()
                        .append($("<option></option>")
                                .attr("value", res['data'].customerID)
                                .text(res['data'].customerName +' - '+res['data'].customerCode))
                        .selectpicker('refresh');
                customerName = res['data'].customerName;
                customerID = res['data'].customerID;
                $('#cus_order_date').val(res['data'].customerOrderDate);
                $('#cus_order_no').val(res['data'].customerOrderCode);
                $('#reference').val(res['data'].customerOrderReference);
                $('#cus_order_amount').val(amount);
                $('#cmnt').val(res['data'].customerOrderComment);

                if (!$.isEmptyObject(res['data'].uploadedAttachments)) {
                    uploadedAttachments = res['data'].uploadedAttachments;
                }

                if (isView == 1) {
                    $('#cust_name').attr('disabled', true);
                    $('#cus_order_date').attr('disabled', true);
                    $('#cus_order_no').attr('disabled', true);
                    $('#reference').attr('disabled', true);
                    $('#cus_order_amount').attr('disabled', true);
                    $('#cmnt').attr('disabled', true);

                    $('#attach-div').addClass('hidden');
                    $('#viewAttachmentModal #saveAttachmentEdit').addClass('hidden');
                    $('#placequotation').addClass('hidden');
                }
            }
        });
    }

    $('#viewUploadedFiles').on('click', function(e) {
        $('#doc-attach-table tbody tr').remove();
        $('#doc-attach-table tfoot div').remove();
        $('#doc-attach-table tbody div').remove();

        if (!$.isEmptyObject(uploadedAttachments)) {
            $('#doc-attach-table thead tr').removeClass('hidden');
            $.each(uploadedAttachments, function(index, value) {
                if (!deletedAttachmentIds.includes(value['documentAttachemntMapID'])) {

                    if ($('#is_view').val() == 1) {

                        tableBody = "<tr id="+value['documentAttachemntMapID']+" data-attachId=" + value['documentAttachemntMapID'] + "><td><a href="+value.docLink+">"+value.documentRealName+"</a></td></tr>";
                    } else {
                        tableBody = "<tr id="+value['documentAttachemntMapID']+" data-attachId=" + value['documentAttachemntMapID'] + "><td><a href="+value.docLink+">"+value.documentRealName+"</a></td><td class='text-center'><span class='glyphicon glyphicon-trash delete_attachment' style='cursor:pointer'></span></td></tr>";

                    }
                    $('#doc-attach-table tbody').append(tableBody);
                }
            });
        } else {
            $('#doc-attach-table thead tr').addClass('hidden');
            var noDataFooter = "<div class= ' col-lg-6 text-right'><label><b>No Data</b></label>";
            $('#doc-attach-table tbody').append(noDataFooter);
        }
        $('#attach_view_footer').removeClass('hidden');
        $('#viewAttachmentModal').modal('show');
    });

    $('#viewAttachmentModal').on('click', '#saveAttachmentEdit', function(event) {
        event.preventDefault();

        $.each(deletedAttachmentIdArray, function(index, val) {
            deletedAttachmentIds.push(val);
        });

        $('#viewAttachmentModal').modal('hide');
    });

    $('#doc-attach-table').on('click', '.delete_attachment', function() {
        var attachementID = $(this).parents('tr').attr('data-attachid');
        bootbox.confirm('Are you sure you want to remove this attachemnet?', function(result) {
            if (result == true) {
                $('#' + attachementID).remove();
                deletedAttachmentIdArray.push(attachementID);
            }
        });
    });

    $('#backToList').on('click', function() {
        window.location.assign(BASE_URL + "/customerOrder/list");
    });


    loadDropDownFromDatabase('/customerAPI/search-customers-for-dropdown', "", 0, '#cust_name');
    $('#cust_name').selectpicker('refresh');
    $('#cust_name').on('change', function() {
        if ($('#cust_name').val() > 0 && $('#cust_name').val() != customerID) {
            customerID = $('#cust_name').val();
            getCustomerDetails(customerID);
            getCustomerProfilesDetails(customerID);
        }
    });

    $(document).on('click', '.dropdown-menu input, .dropdown-menu label', function(e) {
        e.stopPropagation();
    });

    $(document).on('mouseover', '#item_tbl #unit,#item_tbl #quantity,#item_tbl #disc,#item_tbl #name', function(e) {
        $(this).addClass('shadow');
    });

    $(document).on('mouseout', '#item_tbl #unit,#item_tbl #quantity,#item_tbl #disc,#item_tbl #name', function(e) {
        $(this).removeClass('shadow');
    });


    $('#disc_presentage,#disc_value').on('change', function() {
        setItemViceDiscount();
    });


    $('#customerOrder').on('submit', function(e) {
        e.preventDefault();

        updateCustomerOrder();

    });


    function updateCustomerOrder() {
        if ($('#cust_name').val() == '' || $('#cust_name').val() == '0') {
            p_notification(false, eb.getMessage('ERR_CUS_ORDER_VALID_CUST'));
            $('#cust_name').focus();
        }  else if ($('#cus_order_amount').val() != '' && isNaN($('#cus_order_amount').val())){
            p_notification(false, eb.getMessage('ERR_CUS_ORDER_AMOUNT_VALID'));
            $('#cus_order_amount').focus();

        } else {

            var existingAttachemnts = {};
            var deletedAttachments = {};
            $.each(uploadedAttachments, function(index, val) {
                if (!deletedAttachmentIds.includes(val['documentAttachemntMapID'])) {
                    existingAttachemnts[index] = val;
                } else {
                    deletedAttachments[index] = val;
                }
            });

            submit_data = {
                'cust_id': customerID,
                'cust_order_id': $('#edit_cus_order_ID').val(),
                'cus_order_date': $('#cus_order_date').val(),
                'cus_order_no': $('#cus_order_no').val(),
                'cus_order_amount': accounting.unformat($('#cus_order_amount').val()),
                'cmnt': $('#cmnt').val(),
                'reference': $('#reference').val(),
                // 'customCurrencyId': $('#customCurrencyId').val(),
            };

            eb.ajax({
                type: 'POST',
                url: BASE_URL + '/customerOrder-api/updateCustomerOrder',
                data: submit_data,
                success: function(respond) {

                    if (respond.status == true) {
                        p_notification(respond.status, respond.msg);
                        var fileInput = document.getElementById('cusOrderdocumentFiles');
                        var form_data = false;
                        if (window.FormData) {
                            form_data = new FormData();
                        }

                        form_data.append("documentID", 'same');
                        form_data.append("documentTypeID", 46);
                        form_data.append("updateFlag", true);
                        form_data.append("editedDocumentID", $('#edit_cus_order_ID').val());
                        form_data.append("documentCode", $('#cus_order_no').val());
                        form_data.append("deletedAttachmentIds", deletedAttachmentIds);

                        for (var i = 0; i < fileInput.files.length; i++) {
                            form_data.append("files[]", fileInput.files[i]);
                        }

                        eb.ajax({
                            url: BASE_URL + '/store-files',
                            type: 'POST',
                            processData: false,
                            contentType: false,
                            data: form_data,
                            success: function(res) {
                            }
                        });
                        
                        setTimeout(function(){ 
                            window.location.assign(BASE_URL + "/customerOrder/list");
                        }, 3000);
                    }
                },

            });


            
        }
    }

    

    
    function setDueDate(days) {
        var day = parseInt(days);
        var joindate = eb.convertDateFormat('#cus_order_date');
        joindate.setDate(joindate.getDate() + day);

        var dd = joindate.getDate() < 10 ? '0' + joindate.getDate() : joindate.getDate();
        var mm = (joindate.getMonth() + 1) < 10 ? '0' + (joindate.getMonth() + 1) : (joindate.getMonth() + 1);
        var y = joindate.getFullYear();
        var joinFormattedDate = eb.getDateForDocumentEdit('#expire_date', dd, mm, y);

        $("#expire_date").val(joinFormattedDate);
    }

    $("#reset").on('click', function() {
        window.location.reload()
    });

    
    ///////DatePicker\\\\\\\
    var nowTemp = new Date();
    var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);
    var checkin = $('#cus_order_date').datepicker({onRender: function(date) {
            return date.valueOf() < now.valueOf() ? ((U_DATE_R_OVERRIDE)?'':'disabled') : '';
        }
    }).on('changeDate', function(ev) {
        // if (ev.date.valueOf() > checkout.date.valueOf()) {
        //     var newDate = new Date(ev.date);
        //     newDate.setDate(newDate.getDate() + 1);
        //     checkout.setValue(newDate);
        // }
        checkin.hide();
        // $('#cus_order_date')[0].focus();
    }).data('datepicker');
    checkin.setValue(now);

    /////EndOFDatePicker\\\\\\\\\


    //responsive layout issue fix
    $(window).bind('resize ready', function() {
        if ($(window).width() <= 480) {
            $('.quotation_button_set').removeClass('btn-group');
            $('#placequotation, #q_view, #reset').addClass('col-xs-12').css('margin-bottom', '8px');
        } else {
            $('.quotation_button_set').removeClass('col-xs-12');
            $('#placequotation, #q_view, #reset').removeClass('col-xs-12').css('margin-bottom', '0px');
        }
    });

    $('#customCurrencyId').on('change', function() {
        eb.ajax({
            type: 'POST',
            url: BASE_URL + '/currencySymbol/getCustomCurrencyByCustomCurrencyId',
            data: {customCurrencyId: $(this).val()},
            success: function(respond) {
                if (respond.status == true) {
                    $('.cCurrency').text(respond.data.currencySymbol);
                    customCurrencySymbol = respond.data.currencySymbol;
                }
            }
        });
    });

    $('#priceListId').on('change', function() {
        priceListId = $(this).val();
        eb.ajax({
            type: 'POST',
            url: BASE_URL + '/currencySymbol/getPriceListWithDiscount',
            data: {priceListId: priceListId},
            success: function(respond) {
                if (respond.status == true) {
                    priceListItems = respond.data;
                } else {
                    priceListItems = [];
                }
            }
        });
    });


});



function getCustomerDetails(customerID) {
    eb.ajax({
        type: 'POST',
        url: BASE_URL + '/customerAPI/getcustomerByID',
        data: {customerID: customerID}, success: function(respond) {
            if (respond.status == true) {
                $('#invoicecurrentBalance').val(accounting.formatMoney(respond.data['customerData'].customerCurrentBalance));
                $('#quotationCurrentCredit').val(accounting.formatMoney(respond.data['customerData'].customerCurrentCredit));
                $('#cust_name').empty();
                $('#cust_name').append($("<option></option>")
                        .attr("value", customerID)
                        .text(respond.data['customerData'].customerName + '-' + respond.data['customerData'].customerCode));
                $('#cust_name').selectpicker('refresh');
                if (respond.data['customerData'].customerPaymentTerm == 'null') {
                    $('#payment_term').val(1);
                } else {
                    $('#payment_term').val(respond.data['customerData'].customerPaymentTerm);
                }
                $('#payment_term').trigger('change');
            } else {
                $('#customerCurrentBalance').val('');
                $('#customerCurrentCredit').val('');
                $('#paymentTerm').val('');
                p_notification(false, respond.msg);
            }
        }
    });
}

function getCustomerProfilesDetails(customerID) {
    eb.ajax({
        type: 'POST',
        url: BASE_URL + '/customerAPI/getCustomerProfilesByCustomerID',
        data: {customerID: customerID},
        success: function(respond) {
            if (respond.status == true) {
                $('.cus_prof_div').removeClass('hidden');
                setCustomerProfilePicker(respond.data['customerProfileData']);
            } else {
                $('.cus_prof_div').addClass('hidden');

            }
        }
    });
}

function setCustomerProfilePicker(data) {
    $.each(data, function(index, value) {
        $('#cusProfileQuot').append("<option value='" + index + "'>" + value['profName'] + "</option>");
        if (value['isPrimary'] == 1) {
            $('#cusProfileQuot').html("<option value='" + index + "'>" + value['profName'] + "</option>");
            cusProfID = index;
        }
    });
    $('#cusProfileQuot').selectpicker('refresh');

    $('#cusProfileQuot').on('change', function(e) {
        e.preventDefault();
        if ($(this).val() > 0 && ($(this).val() != cusProfID)) {
            cusProfID = $(this).val();
        } else {
            cusProfID = '';
        }
    });
}

