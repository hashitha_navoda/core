/**
 * @author ASHAN <ashan@thinkcube.com>
 * This file contains form validation
 */

var locationIn;
var customerID;
var deliveryNoteIDs = {};
var dimensionData = {};
var salesOrderID = '';
var activityID = '';
var jobID = '';
var projectID = '';
var quotationID = '';
var productRow = "";
var locationProducts = {};
var currentProducts = {};
var batchProducts = {};
var batchSerialProducts = {};
var serialProducts = {};
var checkSubProduct = {};
var deliverProducts = {};
var productsTotal = {};
var productsTax = {};
var deliverSubProducts = {};
var subProductsForCheck = {};
var deliveryNoteSubProducts = {};
var SubProductsQty = {};
var deliveryNoteSubProductsQty = {};
var activityProducts = {};
var promotions = {};
var promotionProducts = {};
var AddCustomerFlag = false;
var addCustomer = false;
var selectedId;
var totalTaxAmount = 0.00;
var totalSuspendedTaxAmount = 0.00;
var promotionID = '';
var promotionDiscountValue = 0.00;
var promotionDiscountValueInvoicedBased = 0.00;
var cancelPromotionFlag = false;
var triggeredByPromotion = false;
var invoiceRowEdit = false;
var ignoreBudgetLimitFlag = false;
var selectedSerialID = null;
var customCurrencyRate = 0.00;
var cusProfID;
var deliveryNoteProductList = {};
var discountEditedManually = [];
var salesPersonArray = [];
var epttInvoice = false;
var epttAttachemntFlag = false;
var selectedAttrTypeID = null;
var selectedAttrValID = null;
var selectedItemSalesPersonID = null;
var currentSelectedRow = null;
$(document).ready(function() {
    invoice = true;
    $("#customer_more").hide();
    $('#disc_presentage').attr('checked', 'checked');
    var invoiceTotalDiscountTrigger = false;
    var deliveryCEnable = false;
    var delDiscountTrigger = false;
    var invBasePromotionDiscount = false;
    var copyFromDel = false;
    var copyFromSalesOrd = false;
    var copyFromQuot = false;
    var copyFromAct = false;
    var copyFromPro = false;
    var copyFromJob = false;
    var invoiceTotalValue = 0.00;
    var counterForPromotion = 0;
    var invoiceTotalValueforPromotion = 0.00;
    var invoiceWisePromotionDiscountType = null;
    var nextInvoiceDate = null;
    var nextInvoiceDateComment = null;
    var promotionDiscountValue = 0.00;
    var promotionDiscountValueInvoicedBased = 0.00;
    var decimalPoints = 0;
    var realUnitePrice = 0;
    var priceListItems = [];
    var priceListId = '';
    var currentItemTaxResults;
    var currentTaxAmount;
    var totalTaxList = {};
    var $productTable = $("#deliveryNoteProductTable");
    var $addRowSample = $('tr.add-row.sample.hidden', $productTable);
    var rowincrementID = 1;
    var customCurrencyID = $('#customCurrencyId').val();
    var defaultSellingPriceData = [];
    var creditLimitApprovel = false;
    var inclusiveTax = false;
    var journalEntryData = {};
    var dimensionArray = {};
    var dimensionTypeID = null;
    var draftInvoiceID = null;
    var serviceChargeTypeId = null;
    var serviceChargeArr = {};
    var selectLocationProduct = {};
    var finaleServiceChargeArr = {};
    var addedServiceChargeAmount = 0;
    var $freeItemTable = $('#freeIssueItemModal #freeIssueRow');
    var tempFreeIssueItemArray = [];
    var selectedItemFreeIssueDetails = {};
    var addedFreeIssueItems = {};
    var freeIssueMajorProId = null;
    var freeIssueMajorIcid = null;
    var oldQty = null;


    if ($('#epttInvoice').val() != 0) {
        epttInvoice = true;
        draftInvoiceID = $('#epttInvoice').val();
        loadEpttInvoiceData(draftInvoiceID);
    }

    $('#customCurrencyRate').val('1.00').attr('disabled',true);
    $('#customCurrencyRate').on('change',function(){
    	if(isNaN($(this).val())){
    		p_notification(false, eb.getMessage('ERR_INVO_CURRENCY_RATE_SHBE_NUM'));
            $(this).val(customCurrencyRate);
            $('#customCurrencyRate').focus();
            return false;
    	}else{
    		customCurrencyRate = $(this).val();
    	}
    });

    var getAddRow = function(proIncID) {
        if (!(proIncID === undefined || proIncID == '')) {
            var $row = $('table#deliveryNoteProductTable > tbody#add-new-item-row > tr').filter(function() {
                return $(this).data("proIncID") == proIncID;
            });
            return $row;
        }

        return $('tr.add-row:not(.sample)', $productTable);
    };
    if (!getAddRow().length) {
        $($addRowSample.clone()).removeClass('sample hidden').appendTo($('#add-new-item-row'));
    }

    locationOut = $('#idOfLocation').val();
    if (TAXSTATUS == false) {
        p_notification('info', eb.getMessage('ERR_GRN_ENABLE_TAX')); // 'Tax has not been enabled for items.');//
    }

    clearProductScreen();
    $('#productAddOverlay').removeClass('overlay');
    $('#deliveryNoteNumbers').val('');
    $('#get-so-select').on('change', function(e) {
        e.preventDefault();
        var id = $(this).children(":selected").attr("id");
        deliveryNoteIDs = {};
        salesOrderID = '';
        quotationID = '';
        activityID = '';
        jobID = '';
        projectID = '';
        $('.multiDeliCheckBox').addClass('hidden');

        if (id == 'del') {
            $('#deliveryNoteNo').val('');
            $('#deliveryNoteNo').selectpicker('render');
            $('#deliveryNoteNo').selectpicker('show');
            $('#salesOrderNo').selectpicker('hide');
            $('#quotationNo').selectpicker('hide');
            $('#activityNo').selectpicker('hide');
            $('#jobNo').selectpicker('hide');
            $('#projectNo').selectpicker('hide');
            $('div#qotSelectDiv').addClass('hidden');
            $('div#solSelectDiv').addClass('hidden');
            $('div#actSelectDiv').addClass('hidden');
            $('div#jobSelectDiv').addClass('hidden');
            $('div#projSelectDiv').addClass('hidden');
            $('div#delSelectDiv').removeClass('hidden');
            $('.multiDeliCheckBox').removeClass('hidden');
        } else if (id == 'so') {
            $('#salesOrderNo').val('');
            $('#salesOrderNo').selectpicker('render');
            $('#salesOrderNo').selectpicker('show');
            $('#quotationNo').selectpicker('hide');
            $('#deliveryNoteNo').selectpicker('hide');
            $('#activityNo').selectpicker('hide');
            $('#jobNo').selectpicker('hide');
            $('#projectNo').selectpicker('hide');
            $('div#qotSelectDiv').addClass('hidden');
            $('div#delSelectDiv').addClass('hidden');
            $('div#actSelectDiv').addClass('hidden');
            $('div#joblSelectDiv').addClass('hidden');
            $('div#projSelectDiv').addClass('hidden');
            $('div#soSelectDiv').removeClass('hidden');
        } else if (id == 'qot') {
            $('#quotationNo').val('');
            $('#quotationNo').selectpicker('render');
            $('#quotationNo').selectpicker('show');
            $('#salesOrderNo').selectpicker('hide');
            $('#deliveryNoteNo').selectpicker('hide');
            $('#activityNo').selectpicker('hide');
            $('#jobNo').selectpicker('hide');
            $('#projectNo').selectpicker('hide');
            $('div#delSelectDiv').addClass('hidden');
            $('div#solSelectDiv').addClass('hidden');
            $('div#actSelectDiv').addClass('hidden');
            $('div#jobSelectDiv').addClass('hidden');
            $('div#projSelectDiv').addClass('hidden');
            $('div#qotSelectDiv').removeClass('hidden');
        } else if (id == 'acNum') {
            $('#activityNo').val('');
            $('#activityNo').selectpicker('show');
            $('#activityNo').selectpicker('render');
            $('#quotationNo').selectpicker('hide');
            $('#salesOrderNo').selectpicker('hide');
            $('#deliveryNoteNo').selectpicker('hide');
            $('#jobNo').selectpicker('hide');
            $('#projectNo').selectpicker('hide');
            $('div#delSelectDiv').addClass('hidden');
            $('div#solSelectDiv').addClass('hidden');
            $('div#qotSelectDiv').addClass('hidden');
            $('div#jobSelectDiv').addClass('hidden');
            $('div#projSelectDiv').addClass('hidden');
            $('div#actSelectDiv').removeClass('hidden');
        } else if (id == 'jobNum') {
            $('#jobNo').val('');
            $('#jobNo').selectpicker('show');
            $('#jobNo').selectpicker('render');
            $('#quotationNo').selectpicker('hide');
            $('#salesOrderNo').selectpicker('hide');
            $('#deliveryNoteNo').selectpicker('hide');
            $('#activityNo').selectpicker('hide');
            $('#projectNo').selectpicker('hide');
            $('div#delSelectDiv').addClass('hidden');
            $('div#soSelectDiv').addClass('hidden');
            $('div#qotSelectDiv').addClass('hidden');
            $('div#actSelectDiv').addClass('hidden');
            $('div#projSelectDiv').addClass('hidden');
            $('div#jobSelectDiv').removeClass('hidden');
        } else if (id == 'projectNum') {
            $('#projectNo').val('');
            $('#projectNo').selectpicker('show');
            $('#projectNo').selectpicker('render');
            $('#quotationNo').selectpicker('hide');
            $('#salesOrderNo').selectpicker('hide');
            $('#deliveryNoteNo').selectpicker('hide');
            $('#activityNo').selectpicker('hide');
            $('#jobNo').selectpicker('hide');
            $('div#delSelectDiv').addClass('hidden');
            $('div#soSelectDiv').addClass('hidden');
            $('div#qotSelectDiv').addClass('hidden');
            $('div#actSelectDiv').addClass('hidden');
            $('div#jobjSelectDiv').addClass('hidden');
            $('div#projSelectDiv').removeClass('hidden');
        }
    });

    loadDropDownFromDatabase('/service-charge-type-api/searchServiceChargesForDropdown', "", 0, '#serviceChargeTypeId');
    $('#serviceChargeTypeId').selectpicker('refresh');
    $('#serviceChargeTypeId').on('change', function() {
        serviceChargeTypeId = $(this).val();
    });

    $('#dimensionView').on('click', function(e) {
        clearDimensionModal();
        var invoiceNo = $('#invoiceNo').val();

        if (!$.isEmptyObject(dimensionData)) {
            $.each(dimensionData[invoiceNo], function(index, val) {
                dimensionArray[index] = val;                
            });
            $.each(dimensionArray, function(index, value) {
                var newTrID = 'tr_' + value.dimensionTypeId;
                var clonedRow = $($('#dimensionPreSample').clone()).attr('id', newTrID).addClass('addedDimensions');
                $("input[name='dimensionType']", clonedRow).val(value.dimensionTypeTxt);
                $("input[name='dimensionValue']", clonedRow).val(value.dimensionValueTxt);
                clonedRow.removeClass('hidden');
                clonedRow.insertBefore('#dimensionPreSample');
                clearAddedNewDimensionRow();
            });
        }
        $('#addDimensionModal').modal('show');
    });

    $('#addServiceChargeView').on('click', function(e) {
        $('#addServiceChargeModal #serviceChargeTable #formRow .addedSrc').remove();

        clearAddedNewServiceChargeRow();

        if (!$.isEmptyObject(finaleServiceChargeArr)) {
            $.each(finaleServiceChargeArr, function(index, val) {
                serviceChargeArr[index] = val;                
            });
        } else {
            serviceChargeArr = {};
        }


        if (!$.isEmptyObject(finaleServiceChargeArr)) {
            $.each(finaleServiceChargeArr, function(index1, value1) {
                loadNewRowForServiceCharge(value1);
            });
        }

        $('#addServiceChargeModal').modal({backdrop: 'static', keyboard: false})  
    });

    $('#viewMrpValues').on('click', function(e) {
        $('#viewMrpValuesModal #mrpTable #mrpRow .addedMrp').remove();


        if (!$.isEmptyObject(deliverProducts)) {
            $.each(deliverProducts, function(index1, value1) {
                loadNewRowForMrpValues(value1);
            });
            $('#viewMrpValuesModal').modal({backdrop: 'static', keyboard: false})  
        } else {
            p_notification(false, eb.getMessage("NO_ANY_ITEM_SELECTED_TO_VIEW_MRP"));
            return false;
        }

    });

    function loadNewRowForServiceCharge(data) {
        var srcTypeId = data['serviceChargeTypeId'];
        var srcTypeTxt = data['serviceChargeTypeTxt'];
        var srcTypeAmount = parseFloat(data['serviceChargeValueTxt']);

        
       
        var srcDuplicateCheck = [];
        if (!$.isEmptyObject(serviceChargeArr)) {
            $.each(serviceChargeArr, function(index, value) {
                if (value.serviceChargeTypeID == serviceChargeTypeId) {
                    dimensionDuplicateCheck.push(value);
                }
            });   
        }
       
        if (srcDuplicateCheck.length > 0) {
            p_notification(false, eb.getMessage("SRC_TYPE_ALREADY_INSERTED"));
            return false;
        }

        var newTrID = 'tr_' + data['serviceChargeTypeId'];
        var clonedRow = $($('#srcPreSample').clone()).attr('id', newTrID).addClass('addedSrc');
        $("input[name='serviceChargeType']", clonedRow).val(srcTypeTxt);
        $("input[name='ServiceChargeValue']", clonedRow).val(srcTypeAmount);
        clonedRow.removeClass('hidden');
        clonedRow.insertBefore('#srcPreSample');
        serviceChargeArr[data['serviceChargeTypeId']] = new serviceCharge(data['serviceChargeTypeId'], srcTypeTxt , srcTypeAmount);
        clearAddedNewServiceChargeRow();
    }

    function loadNewRowForMrpValues(data) {
        console.log(data);

        var mrpVal = (data.mrpValueDisplay != null) ? data.mrpValueDisplay : '-';
        
        var newTrID = 'tr_' + data['productID'];
        var clonedRow = $($('#viewMrpValuesModal #mrpPreSample').clone()).attr('id', newTrID).addClass('addedMrp');
        $("input[name='itemName']", clonedRow).val(data.productName);
        $("input[name='mrpValue']", clonedRow).val(mrpVal);
        $("input[name='ourPrice']", clonedRow).val(data.productDisplayPrice);
        clonedRow.find(".mrpsign").html(data.priceUomAbbr);
        clonedRow.find(".unitsign").html(data.priceUomAbbr);
        clonedRow.removeClass('hidden');
        clonedRow.insertBefore('#mrpPreSample');
        
    }

    function loadNewRowForFreeIssueItems(data, majorProId, icid) {

        var itemNameCode = data['productName']+' - '+data['productCode'];
        var key = majorProId+'_'+icid;
        
        var newTrID = 'tr_' + data['productID'];
        var clonedRow = $($('#freeIssueItemModal #freeIssueSample').clone()).attr('id', newTrID).addClass('relatedFreeItems');
        clonedRow.data('id',data['productID']);
        $("input[name='itemName']", clonedRow).val(itemNameCode);

        if (addedFreeIssueItems[key] !== undefined) {

            if (addedFreeIssueItems[key].hasOwnProperty(data['productID'])) {
                $("input[name='freeIssueItemSelection']", clonedRow).prop('checked', true)
                $("input[name='freeIssueItemSelection']", clonedRow).prop('disabled', true)
            }
        }


        // $("input[name='ourPrice']", clonedRow).val(data.productDisplayPrice);
        
        clonedRow.removeClass('hidden');
        clonedRow.insertBefore('#freeIssueSample');
        
    }

    $('#memberShipManagement').on('click', function(e) {
        if (nextInvoiceDate != null) {
            $('#membership-date').val(nextInvoiceDate);
        } else {
            $('#membership-date').val('');
        }

        if (nextInvoiceDateComment != null) {
            $('#message-membership').val(nextInvoiceDateComment);
        } else {
            $('#message-membership').val('');
        }
        
        $('#addMembershipModal').modal('show');
    });

    $('#saveMembership').on('click', function(event) {
        event.preventDefault();
        if ($('#membership-date').val() == "") {
            p_notification(false, eb.getMessage('ERR_NEXT_INVOICE_CANT_EMPTY'));
            return false;
        }
        nextInvoiceDate = $('#membership-date').val();
        nextInvoiceDateComment = $('#message-membership').val();
        $('#addMembershipModal').modal('hide');
    });
    // dimension modal
    function clearDimensionModal()
    {
        $("#addDimensionModal #dimensionTable tbody .addedDimensions").remove();
        dimensionArray = {};
    }


    $('.dimenisonchange').on('change', function(e) {
        dimensionTypeID = this.value;
        getDimensionValueListByDimensionType(dimensionTypeID, function() {
        });
    });



    function getDimensionValueListByDimensionType(dimensionTypeID, callback) {
        switch (dimensionTypeID) {
            case "job":
                $('.dimensionVal').addClass('hidden');
                $('.dimensionProjectNo').addClass('hidden');
                $('.dimensionJobNo').removeClass('hidden');
                break;
            case "project":
                $('.dimensionVal').addClass('hidden');
                $('.dimensionProjectNo').removeClass('hidden');
                $('.dimensionJobNo').addClass('hidden');
                break;
           default:
                $('.dimensionVal').removeClass('hidden');
                $('.dimensionProjectNo').addClass('hidden');
                $('.dimensionJobNo').addClass('hidden');
                $.ajax({
                    type: 'POST',
                    url: BASE_URL + '/dimension-api/getRelatedDimensionValues',
                    data: {dimensionId: dimensionTypeID},
                    success: function(respond) {
                        //empty options
                        $('#dimensionVal').empty();
                        $('#dimensionVal').append($("<option>", {value: '', html: 'Select dimension value'}));
                        $(respond.data).each(function(index, dimValue) {
                            $.each(dimValue, function(ind, val) {
                                $('#dimensionVal').append($("<option>", {value: ind, html: val.value}));
                            });
                        });
                        callback();
                    }
                });
                break;
        }
    }

    $('#add_dimension').on('click', function(e) {
        e.preventDefault();
        //if plus button is clicked then disabled the button
        //if button is clicked more than one, then it does not continue
        if ($(this).is(':disabled')) {
            return false;
        } else {
            $(this).attr('disabled', true);
            var $thisRow = $(this).parents('tr');
            if ($('.dimenisonchange').val() == 0 || $('.dimenisonchange').val() == '' || $('.dimenisonchange').val() == null) {
                p_notification(false, eb.getMessage('ERR_DIMENSION_SELCET'));
                $(this).attr('disabled', false);
            } else {

                if ($('.dimenisonchange').val() != 'job' && $('.dimenisonchange').val() != 'project') {
                    if ($('.dimensionVal').val() == 0 || $('.dimensionVal').val() == '' || $('.dimensionVal').val() == null) {
                        p_notification(false, eb.getMessage('ERR_DIMENSION_VALUE_SELCET'));
                        $(this).attr('disabled', false);
                        return;
                    }
                }

                if ($('.dimenisonchange').val() == 'project') {
                    if ($('.dimensionProjectNo').val() == 0 || $('.dimensionProjectNo').val() == '' || $('.dimensionProjectNo').val() == null) {
                        p_notification(false, eb.getMessage('ERR_DIMENSION_VALUE_SELCET'));
                        $(this).attr('disabled', false);
                        return;
                    }
                }

                if ($('.dimenisonchange').val() == 'job') {
                    if ($('.dimensionJobNo').val() == 0 || $('.dimensionJobNo').val() == '' || $('.dimensionJobNo').val() == null) {
                        p_notification(false, eb.getMessage('ERR_DIMENSION_VALUE_SELCET'));
                        $(this).attr('disabled', false);
                        return;
                    }
                }

                if (dimensionArray[dimensionTypeID]) {
                    p_notification(false, eb.getMessage('ERR_DIMENSION_ALREADY_INSERTED'));
                    $('.dimenisonchange').val('');
                    $('.dimenisonchange').selectpicker('render');
                    $('.dimenisonchange').trigger('change');
                } else {
                    addNewDimensionRow($(this).parents('tr'));
                }
                $(this).attr('disabled', false);
            }
        }
    });

    $('#add_service_charge').on('click', function(e) {
        e.preventDefault();
        //if plus button is clicked then disabled the button
        //if button is clicked more than one, then it does not continue
        if ($(this).is(':disabled')) {
            return false;
        } else {
            $(this).attr('disabled', true);
            var $thisRow = $(this).parents('tr');
            if ($('.servicechargetypechange').val() == 0 || $('.servicechargetypechange').val() == '' || $('.servicechargetypechange').val() == null) {
                p_notification(false, eb.getMessage('ERR_SRC_TYPE_SELCET'));
                $(this).attr('disabled', false);
            } else {

                if (serviceChargeArr[serviceChargeTypeId]) {
                    p_notification(false, eb.getMessage('SRC_TYPE_ALREADY_INSERTED'));
                    $('#serviceChargeValue').val('');
                    $('#serviceChargeTypeId').val('');
                    $('#serviceChargeTypeId').selectpicker('render');
                    $('#serviceChargeTypeId').trigger('change');
                } else {
                    addNewServiceChargeRow($(this).parents('tr'));
                }
                $(this).attr('disabled', false);
            }
        }
    });


    function addNewServiceChargeRow(thisRow) {

        var srcTypeId = $('.servicechargetypechange', thisRow).val();
        var srcTypeTxt = $('.servicechargetypechange', thisRow).find("option:selected").text();
        var srcTypeAmount = $(thisRow).find("#serviceChargeValue").val();

        
       
        var srcDuplicateCheck = [];
        if (!$.isEmptyObject(serviceChargeArr)) {
            $.each(serviceChargeArr, function(index, value) {
                if (value.serviceChargeTypeID == serviceChargeTypeId) {
                    dimensionDuplicateCheck.push(value);
                }
            });   
        }
       
        if (srcDuplicateCheck.length > 0) {
            p_notification(false, eb.getMessage("SRC_TYPE_ALREADY_INSERTED"));
            return false;
        }

        var newTrID = 'tr_' + serviceChargeTypeId;
        var clonedRow = $($('#srcPreSample').clone()).attr('id', newTrID).addClass('addedSrc');
        $("input[name='serviceChargeType']", clonedRow).val(srcTypeTxt);
        $("input[name='ServiceChargeValue']", clonedRow).val(srcTypeAmount);
        clonedRow.removeClass('hidden');
        clonedRow.insertBefore('#srcPreSample');
        serviceChargeArr[serviceChargeTypeId] = new serviceCharge(serviceChargeTypeId, srcTypeTxt , srcTypeAmount);
        clearAddedNewServiceChargeRow();
    }

    function addNewDimensionRow(thisRow) {

        var dimensionTypeId = $('.dimenisonchange', thisRow).val();
        var dimensionTypeTxt = $('.dimenisonchange', thisRow).find("option:selected").text();

        switch (dimensionTypeID) {
            case "job":
                var dimensionValueTxt = $('.dimensionJobNo', thisRow).find("option:selected").text();
                var dimensionValueId = $('.dimensionJobNo', thisRow).val();
                break;
            case "project":
                var dimensionValueTxt = $('.dimensionProjectNo', thisRow).find("option:selected").text();
                var dimensionValueId = $('.dimensionProjectNo', thisRow).val();
                break;
           default:
                var dimensionValueTxt = $('.dimensionVal', thisRow).find("option:selected").text();
                var dimensionValueId = $('.dimensionVal', thisRow).val();
                break;
        }
       
        var dimensionDuplicateCheck = false;
        if (!$.isEmptyObject(dimensionArray)) {
            $.each(dimensionArray, function(index, value) {
                if (value.dimensionTypeId == dimensionTypeId) {
                    dimensionDuplicateCheck = true;
                }
            });   
        }
       
        if (dimensionDuplicateCheck) {
            p_notification(false, eb.getMessage("ERR_DIMENSION_ALREADY_INSERTED"));
            return false;
        }

        var newTrID = 'tr_' + dimensionTypeId;
        var clonedRow = $($('#dimensionPreSample').clone()).attr('id', newTrID).addClass('addedDimensions');
        $("input[name='dimensionType']", clonedRow).val(dimensionTypeTxt);
        $("input[name='dimensionValue']", clonedRow).val(dimensionValueTxt);
        clonedRow.removeClass('hidden');
        clonedRow.insertBefore('#dimensionPreSample');
        dimensionArray[dimensionTypeId] = new dimension(dimensionTypeId, dimensionValueId,dimensionTypeTxt , dimensionValueTxt);
        clearAddedNewDimensionRow();
    }


    function dimension(dimensionTypeId, dimensionValueId, dimensionTypeTxt, dimensionValueTxt) {
        this.dimensionTypeId = dimensionTypeId;
        this.dimensionValueId = dimensionValueId;
        this.dimensionTypeTxt = dimensionTypeTxt;
        this.dimensionValueTxt = dimensionValueTxt;
    }

    function serviceCharge(serviceChargeTypeId, serviceChargeTypeTxt, serviceChargeValueTxt) {
        this.serviceChargeTypeId = serviceChargeTypeId;
        this.serviceChargeTypeTxt = serviceChargeTypeTxt;
        this.serviceChargeValueTxt = serviceChargeValueTxt;
    }

    function clearAddedNewDimensionRow() {
        $('.dimenisonchange').val('');
        $('.dimenisonchange').selectpicker('render');
        $('.dimenisonchange').trigger('change');
    }

    function clearAddedNewServiceChargeRow() {
        $('.servicechargetypechange').val('');
        $('.servicechargetypechange').selectpicker('render');
        $('.servicechargetypechange').trigger('change');
        $('#serviceChargeValue').val('');
    }

    $('.addDimensions').on('click', function(e) {
        e.preventDefault();

        // validate products before closing modal
        if (!dimensionModalValidate(e)) {
            e.stopPropagation();
            return false;
        } else {
            $('#addDimensionModal').modal('hide');
        }
    });

    $('.addServiceCharge').on('click', function(e) {
        e.preventDefault();

        calculateAddedServiceCharges();
       
        $('#addServiceChargeModal').modal('hide');
    });

    function calculateAddedServiceCharges(){

        finaleServiceChargeArr = {};

        if (!$.isEmptyObject(serviceChargeArr)) {
            $.each(serviceChargeArr, function(index, val) {
                finaleServiceChargeArr[index] = val;                
            });
        }

        var tempAmount = 0;
        if (!$.isEmptyObject(finaleServiceChargeArr)) {
            $.each(finaleServiceChargeArr, function(index, val) {
                tempAmount += parseFloat(val['serviceChargeValueTxt']);
            });
        }
        addedServiceChargeAmount = tempAmount;


        if (addedServiceChargeAmount > 0) {
            $('.added-service-charge-amount-label').removeClass('hidden');
            $('.added-service-charge-amount-value').removeClass('hidden');
            $('.added-service-charge-amount-value').text(accounting.unformat(addedServiceChargeAmount).toFixed(2));
        }

        setInvoiceTotalCost();
        setTotalTax();
        applyPromotionDiscounts();
    }

    function dimensionModalValidate(e) {
        var invoiceNo = $('#invoiceNo').val();
        if ($.isEmptyObject(dimensionArray)) {
            dimensionData = {};
            return true;   
        } else {
            var dimensionSingleData = {};
            $.each(dimensionArray, function(index, val) {
                dimensionSingleData[index] = val;
            });
            dimensionData[invoiceNo] = dimensionSingleData;
            return true;
        }
    }

    $("#addDimensionModal #dimensionTable").on('click', '.deleteDimensionType', function(e) {
        e.preventDefault();
        var deleteDTrID = $(this).closest('tr').attr('id');
        var deleteDID = deleteDTrID.split('tr_')[1].trim();
        bootbox.confirm('Are you sure you want to remove this dimension ?', function(result) {
            if (result == true) {
                delete dimensionArray[deleteDID];
                $('#' + deleteDTrID).remove();
            }
        });
    });


    $("#addServiceChargeModal #serviceChargeTable").on('click', '.deleteServiceCharge', function(e) {
        e.preventDefault();
        var deleteDTrID = $(this).closest('tr').attr('id');
        var deleteDID = deleteDTrID.split('tr_')[1].trim();
        bootbox.confirm('Are you sure you want to remove this service charge ?', function(result) {
            if (result == true) {
                delete serviceChargeArr[deleteDID];
                $('#' + deleteDTrID).remove();
            }
        });
    });

    loadDropDownFromDatabase('/job-api/searchJobsForDropdown', "", 0, '.dimensionJobNo');
    $('.dimensionJobNo').selectpicker('refresh');
    $('.dimensionJobNo').on('change', function() {
        var dimensionValue = $(this).val();
    });

    loadDropDownFromDatabase('/api/project/searchProjectsForDropdown', "", 0, '.dimensionProjectNo');
    $('.dimensionProjectNo').selectpicker('refresh');
    $('.dimensionProjectNo').on('change', function() {
        var dimensionValue = $(this).val();
    });

    loadDropDownFromDatabase('/customerAPI/search-customers-for-dropdown', "", 'Invoice', '#customer');
    $('#customer').selectpicker('refresh');
    $('#customer').on('change', function() {
        if ($(this).val() > 0 && customerID != $(this).val()) {
            customerID = $(this).val();
            getCustomerDetails(customerID);
            getCustomerProfilesDetails(customerID);
            if($(this).val() && $('#customCurrencyId').val()){
            	enableMultiDeliNoteDropdown();
            }
        } else {
            $('#customerCurrentBalance').val('');
            $('#customerCurrentCredit').val('');
            $('#paymentTerm').val('');
        }
    });

    function enableMultiDeliNoteDropdown(){
    	$('#deliveryNoteNumbers').attr('disabled', false);
    	var custID = $('#customer').val();
    	var customCurrencyId = $('#customCurrencyId').val();
    	$('#deliveryNoteNumbers').find('option').remove();
		$('#deliveryNoteNumbers').removeData('AjaxBootstrapSelect');
		$('#deliveryNoteNumbers').removeData('selectpicker');
		$('#deliveryNoteNumbers').siblings('.bootstrap-select').remove();
		$('#deliveryNoteNumbers').siblings('.bootstrap-select').find('.dropdown-menu').remove();
    	$('#deliveryNoteNumbers').selectpicker('refresh');
    	loadDropDownFromDatabase('/delivery-note-api/search-active-delivery-note-for-dropdown?customerID='+ customerID +'&customCurrencyId='+customCurrencyId, "", 0, '#deliveryNoteNumbers');
    }

    $('#multipleCopy').on('click',function(){
        if($(this).is(':checked')){
            $('#delNumberSelectDiv').removeClass('hidden');
            $('#delSelectDiv').addClass('hidden');
            if(!($('#deliveryNoteNumbers option[value=""]').length > 0)){
                $('#deliveryNoteNumbers').append('<option value="">Select Delivery Note Numbers</option>');
                $('#deliveryNoteNumbers').val('').selectpicker('refresh');
            }

            if(!($('#customer').val() && $('#customCurrencyId').val())){
                $('#deliveryNoteNumbers').attr('disabled',true);
            }
            p_notification('info', eb.getMessage('INFO_SELCT_CUSTNAME_AND_CURRENCY'));
        }else{
            $('#delNumberSelectDiv').addClass('hidden');
            $('#delSelectDiv').removeClass('hidden');
            if(!($('#deliveryNoteNo option[value=""]').length > 0)){
                $('#deliveryNoteNo').append('<option value="">Select Delivery Note Number</option>');
                $('#deliveryNoteNo').val('').selectpicker('refresh');
            }
        }
        $('#customCurrencyId').attr('disabled', false);
        clearInvoiceForm();
    });

    $('#inclusiveTax').on('click',function(){
    	if($(this).is(':checked')){
            inclusiveTax = true;            
            $('#priceLabel').addClass('hidden');
            $('#inclusiveTaxpriceLabel').removeClass('hidden');
            $('#deliveryNoteNo').attr('disabled',true);
            $('#get-so-select').attr('disabled',true);
            $('#multipleCopy').attr('disabled',true);
        }else{
            $('#priceLabel').removeClass('hidden');
            $('#inclusiveTaxpriceLabel').addClass('hidden');
            $('#deliveryNoteNo').attr('disabled',false);
            $('#get-so-select').attr('disabled',false);
            $('#multipleCopy').attr('disabled',false);
            inclusiveTax = false;    		
    	}

        if (!$.isEmptyObject(deliverProducts)) {
            $("table#deliveryNoteProductTable>tbody#add-new-item-row>tr").each(function() {
                if ($(this).hasClass("add-row sample hidden") == false && $(this).hasClass("add-row") == false) {
                    invoiceRowEdit = true;
                    var $thisRow = $(this);
                    //re enable the item description
                    $('.addItemDescription', $thisRow).prop('disabled', false);
                    $('.itemDescText', $thisRow).prop('disabled', false);
                    $thisRow.addClass('edit-row')
                            .find("input[name='deliverQuanity'],input[name='unitPrice'],input[name='discount']").prop('readonly', false).end()
                            .find('td').find('#addNewTax').attr('disabled', false)
                            .find("button.delete").addClass('disabled');
                    $thisRow.find("input[name='discount']").change();
                    $thisRow.find("input[name='deliverQuanity']").change();
                    $thisRow.find("input[name='unitPrice']").change();
                    $(this, $thisRow).find('.edit').addClass('hidden');
                    $(this, $thisRow).find('.save').removeClass('hidden');
                    if ($thisRow.hasClass('subproducts')) {
                        $("input[name='deliverQuanity']", $thisRow).prop('readonly', false).change();
                        $('.edit-modal', $thisRow).parent().removeClass('hidden');
                        $("td[colspan]", $thisRow).attr('colspan', 1);
                    } else {
                        if ($('#deliveryNoteNo').val() != '' || activityID != '' || jobID != '') {
                            $("input[name='deliverQuanity']", $thisRow).prop('readonly', false).change();
                        }
                    }
                }
            });
        }
    });

    loadDropDownFromDatabase('/delivery-note-api/search-active-delivery-note-for-dropdown', "", 0, '#deliveryNoteNo');
    $('#deliveryNoteNo').selectpicker('refresh');
    $('#deliveryNoteNo').on('change', function() {
        resetInvoicePage();
        if(!($(this).val() == null || $(this).val() == 0)){
        	deliveryNoteIDs = $(this).val();
        	getDeliveryDetails(deliveryNoteIDs);
            $('#inclusiveTax').attr('disabled',true);
        }
    });

    $('#deliveryNoteNumbers').on('change', function() {
        resetInvoicePage();
        if(!($(this).val() == null || $(this).val() == 0) && deliveryNoteIDs != $(this).val()){
        	deliveryNoteIDs = $(this).val();
        	getDeliveryDetails(deliveryNoteIDs);
            $('#inclusiveTax').attr('disabled',true);
        }
    });


    loadDropDownFromDatabase('/api/salesOrders/search-sales-orders-for-dropdown', "", 0, '#salesOrderNo', '', '', 'invoice');
    $('#salesOrderNo').selectpicker('refresh');
    $('#salesOrderNo').on('change', function() {
        resetInvoicePage();
        if ($(this).val() > 0) {
            salesOrderID = $(this).val();
            getSalesOrderDetails(salesOrderID);
            $('#inclusiveTax').attr('disabled',true);
        }
    });
    loadDropDownFromDatabase('/quotation-api/search-open-quotation-for-dropdown', $('#idOfLocation').val(), 0, '#quotationNo');
    $('#quotationNo').selectpicker('refresh');
    $('#quotationNo').on('change', function() {
        resetInvoicePage();
        if ($(this).val() > 0) {
            quotationID = $(this).val();
            getQuotationDetails(quotationID);
            $('#inclusiveTax').attr('disabled',true);
        }
    });
    loadDropDownFromDatabase('/api/activity/searchActivitiesForDropdown', "", 0, '#activityNo');
    $('#activityNo').selectpicker('refresh');
    $('#activityNo').on('change', function() {
        resetInvoicePage();
        activityID = $(this).val();
        if (activityID > 0) {
            getActivityDetails(activityID);
            $('#inclusiveTax').attr('disabled',true);
        }
    });
    loadDropDownFromDatabase('/job-api/searchJobsForDropdown', "", 0, '#jobNo');
    $('#jobNo').selectpicker('refresh');
    $('#jobNo').on('change', function() {
        resetInvoicePage();
        jobID = $(this).val();
        if (jobID > 0) {
            getJobDetails(jobID);
            $('#inclusiveTax').attr('disabled',true);
        }
    });
    loadDropDownFromDatabase('/project-api/searchProjectsForDropdown', "", 0, '#projectNo');
    $('#projectNo').selectpicker('refresh');
    $('#projectNo').on('change', function() {
        resetInvoicePage();
        projectID = $(this).val();
        if (projectID > 0) {
            getProjectDetails(projectID);
            $('#inclusiveTax').attr('disabled',true);
        }
    });
    //Add item description
    $('#productTable').on('click', '.addItemDescription', function() {
        if ($('i', $(this)).hasClass('fa-comment-o')) {
            $('i', $(this)).removeClass('fa-comment-o').addClass('fa-comment');
            $(this).parents('tr').addClass('comment');
            $(this).parent().parent().siblings('.itemDescText').removeClass('hidden').focus();
        } else {
            $('i', $(this)).removeClass('fa-comment').addClass('fa-comment-o');
            $(this).parents('tr').removeClass('comment');
            $(this).parent().parent().siblings('.itemDescText').addClass('hidden');
        }
    });
    $('#productTable').on('click', '.addItemFilter', function() {

        $('#searchFilterModal').modal({backdrop: false});
        $("#productTable").addClass("after_modal_appended");
    
        //appending modal background inside the blue div
        $('#searchFilterModal .modal-backdrop').appendTo('#productTable');   
    
        //remove the padding right and modal-open class from the body tag which bootstrap adds when a modal is shown
    
        $('#searchFilterModal body').removeClass("modal-open")
        $('#searchFilterModal body').css("padding-right","");

    });
    $('#productTable').on('click', '.add-user', function() {

        currentSelectedRow = $(this).parent().parent();
        if ($(this).parent().parent().hasClass('edit-row')) {
            resetItemSalesPersonDrodown();
            var prID = $(this).parent().parent().data('id');
            var ProIncID = $(this).parent().parent().data('proIncID');
            // var salesPID =  deliverProducts;
            if (deliverProducts[prID+'_'+ProIncID]['itemSalesPerson'] != null) {
                $("select[name='itemSalesPerson']", '#productTable #itemWiseSalesPersonModal').val(Number(deliverProducts[prID+'_'+ProIncID]['itemSalesPerson']));
                // $("select[name='itemSalesPerson']", '#productTable #itemWiseSalesPersonModal').attr('disabled', true);
                $("select[name='itemSalesPerson']", '#productTable #itemWiseSalesPersonModal').selectpicker('refresh');
                $(this).parent().parent().data('salesPersonID', Number(deliverProducts[prID+'_'+ProIncID]['itemSalesPerson']));
                selectedItemSalesPersonID = Number(deliverProducts[prID+'_'+ProIncID]['itemSalesPerson']);
            }
        } else {
            // $("select[name='itemSalesPerson']", '#productTable #itemWiseSalesPersonModal').attr('disabled', false);
            resetItemSalesPersonDrodown();
        }

        $('#itemWiseSalesPersonModal').modal({backdrop: false});
        $("#productTable").addClass("after_modal_appended");
    
        //appending modal background inside the blue div
        $('#itemWiseSalesPersonModal .modal-backdrop').appendTo('#productTable');   
    
        //remove the padding right and modal-open class from the body tag which bootstrap adds when a modal is shown
    
        $('#itemWiseSalesPersonModal body').removeClass("modal-open")
        $('#itemWiseSalesPersonModal body').css("padding-right","");


    });
//all delivery note related data append to invoice (copy delivery note to invoice)
    function getDeliveryDetails(DNIDS) {
        console.log(DNIDS);
        eb.ajax({
            type: 'POST',
            url: BASE_URL + '/invoice-api/getDeliveryNoteDetails',
            data: {
            	deliveryNoteIDs: DNIDS,
            	customCurrencyID: $('#customCurrencyId').val(),
                forReturn: false
            },
            success: function(respond) {
                if (respond.status) {
                    if (respond.data.inactiveItemFlag) {
                        p_notification(false, respond.data.errorMsg);
                        setTimeout(function(){ 
                            window.location.assign(BASE_URL + "/invoice/create")
                        }, 3000);
                        return false;
                    }
                    deliveryNoteProductList = respond.data.deliveryNoteProduct;
                    customerID = respond.data.deliveryNote.customerID;
                    cusProfID = respond.data.deliveryNote.customerProfileID;
                    setCustomerDetails(respond.data.customer);
                    $('#customer').attr('disabled', true);
                    $('#addCustomerBtn').prop('disabled', true);
                    $('#addCustomerBtn').on('click', function(e) {
                        e.preventDefault();
                    });

                    if (!$.isEmptyObject(respond.data.dimensionData)) {
                        var invoiceNo = $('#invoiceNo').val();
                        $.each(respond.data.dimensionData, function(index, val) {
                            dimensionData[invoiceNo] = val;
                        });
                    }

                    var cusProfName;
                    if (respond.data.customerProfName == '' || respond.data.customerProfName == null) {
                        cusProfName = 'Default';
                    } else {
                        cusProfName = respond.data.customerProfName;
                    }
                    $('.cus_prof_div').removeClass('hidden');
                    $('.cus-prof-select').addClass('hidden');
                    $('.tooltip_for_default_cus').addClass('hidden');
                    $('.default-cus-prof').removeClass('hidden');
                    $('#cus-prof-name').val(cusProfName);

                    $('#salesPersonID').val(respond.data.deliveryNote.salesPersonID).attr('disabled', true);
                    if (respond.data.deliveryNote.salesPersonID != 0) {
                        $('#salesPersonID').attr('disabled', false);
                    }
                    $('#salesPersonID').selectpicker('render');
                    $('#comment').val(respond.data.deliveryNote.deliveryNoteComment);

                    var custCurrencyId = respond.data.deliveryNote.customCurrencyId;
                    var custCurrencyRate = respond.data.deliveryNote.deliveryNoteCustomCurrencyRate;
                    customCurrencyRate = (custCurrencyRate != 0) ? parseFloat(custCurrencyRate) : 1;
                    currentProducts = respond.data.locationProducts;
                    locationProducts = respond.data.locationProducts;
                    companyCurrencySymbol = respond.data.customCurrencySymbol;

                    if (respond.data.deliveryNote.priceListId != 0) {
                        $('#priceListId').val(respond.data.deliveryNote.priceListId).attr('disabled', true).selectpicker('refresh');
                        $('#priceListId').trigger('change');
                    }

                    $.each(respond.data.deliveryNoteProduct, function(index, value) {
                        var $currentRow = getAddRow();
                        clearProductRow($currentRow);
                        var $flag = true;
                        if (value.productType != 2 && value.deliveryNoteProductQuantity <= 0 ) {
                            $flag = false;
                        }

                        if ($flag) {
                            //product related tax apply in here
                            $currentRow.find('#taxApplied').removeClass('glyphicon glyphicon-unchecked');
                            $currentRow.find('#taxApplied').addClass('glyphicon glyphicon-check');
                            $currentRow.find('.tempLi').remove();
                            for (var i in locationProducts[value.productID].tax) {
                                var clonedLi = $($currentRow.find('#sampleLi').clone()).removeClass('hidden').attr('id', 'li_' + i).addClass("tempLi");
                                clonedLi.children(".taxChecks").attr('id', value.productID + '_' + i + '_' +value.deliveryNoteID).prop('checked', false).addClass('addNewTaxCheck');
                                clonedLi.children(".taxName").html('&nbsp&nbsp' + locationProducts[value.productID].tax[i].tN + '&nbsp&nbsp' + locationProducts[value.productID].tax[i].tP + '%').attr('for', value.productID + '_' + i+ '_' +value.deliveryNoteID);
                                clonedLi.insertBefore($currentRow.find('#sampleLi'));
                            }

                            if (value.tax) {
                                for (var j in value.tax) {
                                    if (locationProducts[value.productID].tax[j]) {
                                        $("#"+value.productID+'_'+j+ '_' +value.deliveryNoteID).prop('checked', true);
                                    }
                                }
                            }


                            var mrpType = value.mrpType;
                            var mrpValue = value.mrpValue;
                            var mrpPercentage = value.mrpPercentageValue;
//product related data append in here
                            $currentRow.data('prodocuid', value.deliveryNoteID);
                            $currentRow.data('documenttypeid', value.documentTypeID);
                            $currentRow.data('locationid', value.locationID);
                            $currentRow.data('locationproductid', value.locationProductID);
                            $currentRow.data('id', value.productID);
                            $currentRow.data('mrpType', mrpType);
                            $currentRow.data('mrpValue', mrpValue);
                            $currentRow.data('mrpPercentageValue', mrpPercentage);
                            $currentRow.data('icid',rowincrementID);
                            var rowIncID = rowincrementID;
                            $currentRow.data('proIncID',rowincrementID);
                            $currentRow.addClass('copied');
                            rowincrementID++;
                            $currentRow.attr('id', 'product' + value.productID);
                            $currentRow.data('stockupdate', false);
                            $("#itemCode", $currentRow).empty();
                            $("#itemCode", $currentRow).
                                    append($("<option></option>").
                                            attr("value", value.productID).
                                            text(value.productName + '-' + value.productCode));
                            $("#itemCode", $currentRow).val(value.productID).prop('disabled', 'disabled');
                            $("#itemCode", $currentRow).data('PT', value.productType);
                            $("#itemCode", $currentRow).data('PC', value.productCode);
                            $("#itemCode", $currentRow).data('PN', value.productName);
                            $("#itemCode", $currentRow).selectpicker('refresh');
                            $("input[name='availableQuantity']", $currentRow).parent().addClass('input-group');
                            $("input[name='deliverQuanity']", $currentRow).parent().addClass('input-group');
                            $("input[name='availableQuantity']", $currentRow).val(currentProducts[value.productID].LPQ).prop('readonly', true).addUom(currentProducts[value.productID].uom);
                            $("input[name='unitPrice']", $currentRow).val(value.deliveryNoteProductPrice / customCurrencyRate).addUomPrice(currentProducts[value.productID].uom);
                            $currentRow.find(".addItemFilter").remove();

                            var baseUom;
                            for (var j in currentProducts[value.productID].uom) {
                                if (currentProducts[value.productID].uom[j].pUBase == 1) {
                                    baseUom = currentProducts[value.productID].uom[j].uomID;
                                }
                            }
                            value.deliveryNoteProductDiscount = value.deliveryNoteProductDiscount ? value.deliveryNoteProductDiscount : 0;
                            if (locationProducts[value.productID].dV != null && locationProducts[value.productID].dEL == 1) {
                                $("input[name='discount']", $currentRow).val((value.deliveryNoteProductDiscount / customCurrencyRate).toFixed(2)).addUomPrice(currentProducts[value.productID].uom, baseUom);
                                $("input[name='discount']", $currentRow).siblings('.uom-price-select').find('.uom_price_drop_down').attr('disabled', true);
                                $("input[name='discount']", $currentRow).addClass('value');
                                $(".sign", $currentRow).text(companyCurrencySymbol);
                            } else if (locationProducts[value.productID].dPR != null && locationProducts[value.productID].dEL == 1){
                                $("input[name='discount']", $currentRow).val(parseFloat(value.deliveryNoteProductDiscount).toFixed(2));
                                $("input[name='discount']", $currentRow).addClass('precentage');
                                $(".sign", $currentRow).text('%');
                            } else {
                                $("input[name='discount']", $currentRow).attr('readonly',true);
                            }
                            delDiscountTrigger = true;
                            copyFromDel = true;
                            copyFromSalesOrd = false;
                            copyFromQuot = false;
                            copyFromAct = false;
                            copyFromPro = false;
                            copyFromJob = false;
                            $("input[name='deliverQuanity']", $currentRow).trigger(jQuery.Event("focusout"));


                            //if product has sub products those was append i here to invoice
                            if (!$.isEmptyObject(value.subProduct)) {
                                var subProducts = [];
                                var qtyTotal = 0;
                                $.each(value.subProduct, function(ind, val) {
                                    var thisSubProduct = {};
                                    thisSubProduct.batchPrice = '';
                                    if (val.productBatchID) {
                                        thisSubProduct.batchID = val.productBatchID;
                                        thisSubProduct.batchCode = val.productBatchCode;
                                        if (val.productBatchExpDate) {
                                            thisSubProduct.expDate = val.productBatchExpDate;
                                            thisSubProduct.wrntyDate = val.deliverySubProdWrnty;
                                            thisSubProduct.wrntyType = val.deliverySubProdWrntyType;
                                            thisSubProduct.batchPrice = val.deliverySubProdBatchPrice;
                                        }
                                    } else {
                                        thisSubProduct.batchID = undefined;
                                        thisSubProduct.batchCode = undefined;
                                    }
                                    if (val.productSerialID) {
                                        thisSubProduct.serialID = val.productSerialID;
                                        thisSubProduct.serialCode = val.productSerialCode;
                                        thisSubProduct.wrntyDate = val.deliverySubProdWrnty;
                                        thisSubProduct.wrntyType = val.deliverySubProdWrntyType;
                                        if (val.productSerialExpDate) {
                                            thisSubProduct.expDate = val.productSerialExpDate;
                                        }
                                    } else {
                                        thisSubProduct.serialID = undefined;
                                        thisSubProduct.serialCode = undefined;
                                    }
                                    thisSubProduct.qtyByBase = val.deliveryProductSubQuantity;
                                    subProducts.push(thisSubProduct);
                                    qtyTotal += val.deliveryProductSubQuantity;
                                });
 								var subProIds = _.pluck(subProducts, 'serialID');
                                $(".edit-modal", $currentRow).data('selectedSids', subProIds);
                                deliveryNoteSubProductsQty[value.productID + "_" + rowIncID] = qtyTotal;
                                deliveryNoteSubProducts[value.productID + "_" + rowIncID] = subProducts;
                            } else {
                                $currentRow.removeClass('subproducts');
                                $("td[colspan]", $currentRow).attr('colspan', 2);
                                $("input[name='deliverQuantity']", $currentRow).prop('readonly', false).focus();
                            }
                            if ($.isEmptyObject(deliveryNoteSubProducts[value.productID + "_" + rowIncID])) {
                                $currentRow.removeClass('subproducts');
                                $('.edit-modal', $currentRow).parent().addClass('hidden');
                                $("td[colspan]", $currentRow).attr('colspan', 2);
                                $("input[name='deliverQuanity']", $currentRow)
                                        .val(value.deliveryNoteProductQuantity)
                                        .addUom(currentProducts[value.productID].uom);
                            } else {
                                $("input[name='deliverQuanity']", $currentRow)
                                        .val(value.deliveryNoteProductQuantity)
                                        .prop('readonly', true)
                                        .addUom(currentProducts[value.productID].uom);
                                checkSubProduct[value.productID + "_" + rowIncID] = undefined;
                                $currentRow.addClass('subproducts');
                            }
                            $currentRow.removeClass('add-row');
                            $currentRow.find("button.delete").removeClass('disabled');
                            $currentRow.find(".free-issue").addClass('hidden');
                            var $newRow = $($addRowSample.clone()).removeClass('sample hidden').appendTo($('#add-new-item-row', $productTable));
                            $newRow.find("#itemCode").focus();
                        }

                        custCurrencyId = (custCurrencyId != null) ? custCurrencyId : customCurrencyID;

                        if($('#customCurrencyId').val() != custCurrencyId){
                        	$('#customCurrencyId').val(custCurrencyId).trigger('change');
                        }
                        $('#customCurrencyRate').val(customCurrencyRate).trigger('change');

                        if (!(custCurrencyId == '' || custCurrencyId == 0)) {
                            $('#customCurrencyId').attr('disabled', true);
                        }
                    });
                    if (respond.data.deliveryNote.deliveryNoteCharge) {
                        $('#deliveryChargeEnable').prop('checked', true);
                        $('.deliCharges').removeClass('hidden');
                        $('.deliAddress').removeClass('hidden');
                        $('#deliveryCharge').val((accounting.unformat(respond.data.deliveryNote.deliveryNoteCharge)).toFixed(2));
                        setInvoiceTotalCost();
                    }
                    var $currentRow = getAddRow();
                    clearProductRow($currentRow);
                    setProductTypeahead();
                } else {
                    deliveryNoteIDs = '';
                    p_notification(respond.status, respond.msg);
                    if(respond.data['refresh']){
                    	window.location.reload();
                    }
                }
            }
        });
    }

// this will append all sales order related product to Invoice(copy to function)

    function loadEpttInvoiceData(ID) {
        eb.ajax({
            type: 'POST',
            url: BASE_URL + '/invoice-api/getDraftInvoiceData',
            data: {
                draftInvoiceID: ID
            },
            success: function(respond) {
                if (respond.status) {
                    customerID = respond.data.customerID;
                    setCustomerDetails(respond.data.customer);
                    $('#customer').attr('disabled', true);
                    $('#addCustomerBtn').prop('disabled', true);
                    $('#addCustomerBtn').on('click', function(e) {
                        e.preventDefault();
                    });
                    
                    cusProfName = 'Default';

                    if (respond.data.attachment != "") {
                        epttAttachemntFlag = true;
                        $('.draftInvoiceAttachmentDiv').removeClass('hidden');
                        var attac = "<a href='/"+respond.data.attachment+"'>Booking Vocher Detail.csv</a>"
                        $('#epttAttachemnt').append(attac);
                    }
                    
                    $('.cus_prof_div').removeClass('hidden');
                    $('.cus-prof-select').addClass('hidden');
                    $('.tooltip_for_default_cus').addClass('hidden');
                    $('.default-cus-prof').removeClass('hidden');
                    $('#cus-prof-name').val(cusProfName);

                    var $currentRow = getAddRow();
                    $('#itemCode', $currentRow).selectpicker();
                    var locationID = $('#idOfLocation').val();
                    currentProducts = respond.data.locationProducts;

                    eb.ajax({
                        type: 'POST',
                        url: BASE_URL + '/productAPI/get-location-product-details',
                        data: {productID: respond.data.productData.productID, locationID: locationID},
                        success: function(res) {
                            selectProductForTransfer(res.data, respond.data.productData.quantity, respond.data.productData.unitPrice);
                            $("#itemCode", $currentRow).
                                    append($("<option></option>").
                                            attr("value", respond.data.productData.productID).
                                            text(respond.data.productData.productName + '-' + respond.data.productData.productCode));
                            $("#itemCode", $currentRow).val(respond.data.productData.productID).prop('disabled', 'disabled');
                            $("#itemCode", $currentRow).selectpicker('render');
                            $("input[name='deliverQuanity']", $currentRow).trigger(jQuery.Event("focusout"));
                        }
                    });
                } 
            }
        });
    }

    function getSalesOrderDetails(SOID) {
        eb.ajax({
            type: 'POST',
            url: BASE_URL + '/api/salesOrders/getSalesOrderDetails',
            data: {
                salesOrderID: SOID
            },
            success: function(respond) {
                if (respond.status) {
                    if (respond.data.inactiveItemFlag) {
                        p_notification(false, respond.data.errorMsg);
                        setTimeout(function(){ 
                            window.location.assign(BASE_URL + "/invoice/create")
                        }, 3000);
                        return false;
                    }
                    customerID = respond.data.salesOrder.customerID;
                    cusProfID = respond.data.salesOrder.customerProfileID;
                    setCustomerDetails(respond.data.customer);
                    $('#customer').attr('disabled', true);
                    $('#addCustomerBtn').prop('disabled', true);
                    $('#addCustomerBtn').on('click', function(e) {
                        e.preventDefault();
                    });

                    if(respond.data.salesOrder.payementTerm != null){
                        $('#paymentTerm').val(respond.data.salesOrder.payementTerm);
                        $('#paymentTerm').trigger('change');
                    }

                    if (respond.data.relatedCusOrder != null) {
                        $("#cusOrderNum").val(respond.data.relatedCusOrder);
                        $("#cusOrderNum").attr('disabled', true);
                    } else {
                        $("#cusOrderNum").val('');
                        $("#cusOrderNum").attr('disabled', false);
                    }


                    var cusProfName;
                    if (respond.data.customerProfileName == '' || respond.data.customerProfileName == null) {
                        cusProfName = 'Default';
                    } else {
                        cusProfName = respond.data.customerProfileName;
                    }
                    $('.cus_prof_div').removeClass('hidden');
                    $('.cus-prof-select').addClass('hidden');
                    $('.tooltip_for_default_cus').addClass('hidden');
                    $('.default-cus-prof').removeClass('hidden');
                    $('#cus-prof-name').val(cusProfName);

                    $('#salesPersonID').val(respond.data.salesOrder.salesPersonID).attr('disabled', true);
                    if (respond.data.salesOrder.salesPersonID != 0) {
                        $('#salesPersonID').attr('disabled', true);
                    }
                    $('#salesPersonID').selectpicker('render');
                    $('#comment').val(respond.data.salesOrder.comment);

                    var custCurrencyId = respond.data.salesOrder.customCurrencyId;
                    var custCurrencyRate = respond.data.salesOrder.salesOrdersCustomCurrencyRate;
                    customCurrencyRate = (custCurrencyRate != 0) ? parseFloat(custCurrencyRate) : 1;
                    currentProducts = respond.data.locationProducts;
                    companyCurrencySymbol = respond.data.salesOrder.customCurrencySymbol;

                    if (respond.data.salesOrder.priceListId != 0) {
                        $('#priceListId').val(respond.data.salesOrder.priceListId).attr('disabled', true).selectpicker('refresh');
                        $('#priceListId').trigger('change');
                    }

                    $.each(respond.data.salesOrderProduct, function(index, value) {
                        var $currentRow = getAddRow();
                        var $flag = true;
                        if (value.productType != 2 && value.deliveryNoteProductQuantity == 0) {
                            $flag = false;
                        }

                         var baseUom;
                        for (var j in currentProducts[value.productID].uom) {
                            if (currentProducts[value.productID].uom[j].pUBase == 1) {
                                baseUom = currentProducts[value.productID].uom[j].uomID;
                            }
                        }
                        defaultSellingPriceData[value.productID] = value.unitPrice / customCurrencyRate;
                        if ($flag) {
                            //sales order product related tax apply in here
                            if (value.tax) {
                                $currentRow.find('#taxApplied').removeClass('glyphicon glyphicon-unchecked');
                                $currentRow.find('#taxApplied').addClass('glyphicon glyphicon-check');
                                $currentRow.find('.tempLi').remove();
                                for (var i in value.tax) {
                                    var clonedLi = $($currentRow.find('#sampleLi').clone()).removeClass('hidden').attr('id', 'li_' + i).addClass("tempLi");
                                    clonedLi.children(".taxChecks").attr('id', value.productID + '_' + i).prop('checked', true).addClass('addNewTaxCheck');
                                    clonedLi.children(".taxName").html('&nbsp&nbsp' + value.tax[i].salesOrderTaxName + '&nbsp&nbsp' + value.tax[i].salesOrderTaxPrecentage + '%').attr('for', value.productID + '_' + i);
                                    clonedLi.insertBefore($currentRow.find('#sampleLi'));
                                }
                            }



                            var mrpType = value.mrpType;
                            var mrpValue = value.mrpValue;
                            var mrpPercentage = value.mrpPercentageValue;

                            //sales order product realated data append in here
                            $currentRow.data('id', value.productID);
                            $currentRow.data('locationproductid', value.locationProductID);
                            $currentRow.attr('id', 'product' + value.productID);
                            $currentRow.data('icid',rowincrementID);
                            $currentRow.data('mrpType', mrpType);
                            $currentRow.data('mrpValue', mrpValue);
                            $currentRow.data('mrpPercentageValue', mrpPercentage);
                            $currentRow.data('proIncID',rowincrementID);
                            $currentRow.addClass('copied');
                            var rowIncID = rowincrementID;
                            rowincrementID++;
                            $currentRow.data('stockupdate', true);
                            $("#itemCode", $currentRow).empty();
                            $("#itemCode", $currentRow).
                                    append($("<option></option>").
                                            attr("value", value.productID).
                                            text(value.productName + '-' + value.productCode));
                            $("#itemCode", $currentRow).val(value.productID).prop('disabled', 'disabled');
                            $("#itemCode", $currentRow).data('PT', value.productType);
                            $("#itemCode", $currentRow).data('PC', value.productCode);
                            $("#itemCode", $currentRow).data('PN', value.productName);
                            $("#itemCode", $currentRow).selectpicker('render');
                            $("input[name='availableQuantity']", $currentRow).val(currentProducts[value.productID].LPQ).prop('readonly', true).addUom(currentProducts[value.productID].uom);
                            $("input[name='deliverQuanity']", $currentRow).val(value.quantity).addUom(currentProducts[value.productID].uom);
                            $("input[name='availableQuantity']", $currentRow).parent().addClass('input-group');
                            $("input[name='deliverQuanity']", $currentRow).parent().addClass('input-group');
                            $("input[name='unitPrice']", $currentRow).val(value.unitPrice / customCurrencyRate).addUomPrice(currentProducts[value.productID].uom);
                            if (value.DiscountType == 'value') {
                                $("input[name='discount']", $currentRow).val((value.discount / customCurrencyRate).toFixed(2)).addUomPrice(currentProducts[value.productID].uom, baseUom);
                                $("input[name='discount']", $currentRow).siblings('.uom-price-select').find('.uom_price_drop_down').attr('disabled', true);
                                $("input[name='discount']", $currentRow).addClass('value');
                                $(".sign", $currentRow).text(companyCurrencySymbol);
                            } else if (value.DiscountType == 'precentage') {
                                if (value.discount != null) {
                                    $("input[name='discount']", $currentRow).val(parseFloat(value.discount).toFixed(2));
                                    $("input[name='discount']", $currentRow).addClass('precentage');
                                    $(".sign", $currentRow).text('%');
                                }
                            }
                            delDiscountTrigger = true;

                            //check wheter sales order product have sub product or not
                            $("input[name='deliverQuanity']", $currentRow).trigger(jQuery.Event("focusout"));
                            if ($.isEmptyObject(currentProducts[value.productID].batch) &&
                                    $.isEmptyObject(currentProducts[value.productID].serial) &&
                                    $.isEmptyObject(currentProducts[value.productID].batchSerial)) {

                                $currentRow.removeClass('subproducts');
                                $('.edit-modal', $currentRow).parent().addClass('hidden');
                                $("td[colspan]", $currentRow).attr('colspan', 2);
                            } else {
                                checkSubProduct[value.productID + "_" + rowIncID] = 0;
                                $currentRow.addClass('subproducts');
                                $("input[name='deliverQuantity']", $currentRow).prop('readonly', true).change();
                            }
                            $currentRow.removeClass('add-row');
                            $currentRow.addClass('edit-row');
                            $currentRow.find("button.delete").removeClass('disabled');
                            $currentRow.find(".addItemFilter").remove();
                            var $newRow = $($addRowSample.clone()).removeClass('sample hidden').appendTo($('#add-new-item-row', $productTable));
                            setProductTypeahead();
                            $newRow.find("#itemCode").focus();
                        }
                    });
					custCurrencyId = (custCurrencyId != null) ? custCurrencyId : customCurrencyID;
                    $('#customCurrencyId').val(custCurrencyId).trigger('change');
                    $('#customCurrencyRate').val(customCurrencyRate).trigger('change');
                    if (!(custCurrencyId == '' || custCurrencyId == 0)) {
                        $('#customCurrencyId').attr('disabled', true);
                    }
                } else {
                    salesOrderID = '';
                    p_notification(respond.status, respond.msg);
                    $('#salesOrderNo').val('');
                }

            }
        });
        copyFromDel = false;
        copyFromSalesOrd = true;
        copyFromQuot = false;
        copyFromAct = false;
        copyFromPro = false;
        copyFromJob = false;
    }
// this will append all Quoatation related product to Invoice(copy to function)
    function getQuotationDetails(QOTID) {
        eb.ajax({
            type: 'POST',
            url: BASE_URL + '/quotation-api/retriveQuotation',
            data: {
                quataionID: QOTID, forInvoice: true
            },
            success: function(respond) {

                if (respond) {
                    if (respond.inactiveItemFlag) {
                        p_notification(false, respond.errorMsg + "cannot copy this Quoatation.");
                        setTimeout(function(){ 
                            window.location.assign(BASE_URL + "/invoice/create")
                        }, 3000);
                        return false;
                    }
                    if(respond == 'inactiveCustomer'){
                        p_notification(false, eb.getMessage('ERR_QUOT_CUST_INACTIVE'));
                        return;
                    }
                    customerID = respond.customer_id;
                    setCustomerDetails(respond.customer);
                    $('#customer').attr('disabled', true);
                    $('#addCustomerBtn').prop('disabled', true);
                    $('#addCustomerBtn').on('click', function(e) {
                        e.preventDefault();
                    });

                    if (respond.quotationDiscountRate > 0) {
                        $("#total_discount_rate").val(respond.quotationDiscountRate).trigger('keyup');
                    }

                    if (respond.quotationTotalDiscountType === "presentage") {
                        $("#disc_presentage").prop("checked", true);
                        setInvoiceTotalCost();
                    } else if (respond.quotationTotalDiscountType == 'Value') {
                        $("#disc_value").prop("checked", true);
                        $("#total_discount_rate").val(respond.quotationDiscountRate).trigger('keyup');
                        setInvoiceTotalCost();
                    }


                    if(respond.payment_term != null){
                        $('#paymentTerm').val(respond.payment_term);
                        $('#paymentTerm').trigger('change');
                    }

                    var cusProfName;
                    if (respond.customer_prof_name == '' || respond.customer_prof_name == null) {
                        cusProfName = 'Default';
                    } else {
                        cusProfName = respond.customer_prof_name;
                    }
                    $('.cus_prof_div').removeClass('hidden');
                    $('.cus-prof-select').addClass('hidden');
                    $('.tooltip_for_default_cus').addClass('hidden');
                    $('.default-cus-prof').removeClass('hidden');
                    $('#cus-prof-name').val(cusProfName);

                    $('#salesPersonID').val(respond.salesPersonID).attr('disabled', true);
                    if (respond.salesPersonID != 0) {
                        $('#salesPersonID').attr('disabled', true);
                    }
                    $('#salesPersonID').selectpicker('render');
                    $('#comment').val(respond.comment);
                    currentProducts = respond.locationProducts;

                    var custCurrencyId = respond.customCurrencyId;
                    var custCurrencyRate = respond.quotationCustomCurrencyRate;
                    customCurrencyRate = (custCurrencyRate != 0) ? parseFloat(custCurrencyRate) : 1;
                    companyCurrencySymbol = respond.customCurrencySymbol;
                    cusProfID = respond.customer_prof_id;

                    if (respond.priceListId != 0) {
                        $('#priceListId').val(respond.priceListId).attr('disabled', true).selectpicker('refresh');
                        $('#priceListId').trigger('change');
                    }

                    $.each(respond.product, function(index, value) {
                        var $currentRow = getAddRow();
                        var $flag = true;
                        if (value.productTypeID != 2 && value.quotationProductQuantity == 0) {
                            $flag = false;
                        }
                        defaultSellingPriceData[value.productID] = value.quotationProductUnitPrice / customCurrencyRate;
                        if ($flag) {
                            //Quotation product related tax apply in here
                            if (!jQuery.isEmptyObject(respond.productTaxes)) {
                                var currentProductTax = respond.productTaxes[value.quotationProductID];
                                $currentRow.find('#taxApplied').removeClass('glyphicon glyphicon-unchecked');
                                $currentRow.find('#taxApplied').addClass('glyphicon glyphicon-check');
                                $currentRow.find('.tempLi').remove();
                                for (var i in currentProductTax) {
                                    var clonedLi = $($currentRow.find('#sampleLi').clone()).removeClass('hidden').attr('id', 'li_' + currentProductTax[i].id).addClass("tempLi");
                                    clonedLi.children(".taxChecks").attr('id', value.productID + '_' + currentProductTax[i].id).prop('checked', true).addClass('addNewTaxCheck');
                                    clonedLi.children(".taxName").html('&nbsp&nbsp' + currentProductTax[i].taxName + '&nbsp&nbsp' + currentProductTax[i].taxPrecentage + '%').attr('for', value.productID + '_' + currentProductTax[i].id);
                                    clonedLi.insertBefore($currentRow.find('#sampleLi'));
                                }
                            }

                            var baseUom;
                            for (var j in currentProducts[value.productID].uom) {
                                if (currentProducts[value.productID].uom[j].pUBase == 1) {
                                    baseUom = currentProducts[value.productID].uom[j].uomID;
                                }
                            }

                            var mrpType = value.mrpType;
                            var mrpValue = value.mrpValue;
                            var mrpPercentage = value.mrpPercentageValue;

                            //quotation product realated data append in here
                            $currentRow.data('id', value.productID);
                            $currentRow.data('locationproductid', value.locationProductID);
                            $currentRow.attr('id', 'product' + value.productID);
                            $currentRow.data('icid',rowincrementID);
                            $currentRow.data('mrpType', mrpType);
                            $currentRow.data('mrpValue', mrpValue);
                            $currentRow.data('mrpPercentageValue', mrpPercentage);
                            $currentRow.data('proIncID',rowincrementID);
                            $currentRow.addClass('copied');
                            var rowIncID = rowincrementID;
                            rowincrementID++;
                            $currentRow.data('stockupdate', true);
                            $("#itemCode", $currentRow).empty();
                            $("#itemCode", $currentRow).
                                    append($("<option></option>").
                                            attr("value", value.productID).
                                            text(value.productName + '-' + value.productCode));
                            $("#itemCode", $currentRow).val(value.productID).prop('disabled', 'disabled');
                            $("#itemCode", $currentRow).data('PT', value.productTypeID);
                            $("#itemCode", $currentRow).data('PC', value.productCode);
                            $("#itemCode", $currentRow).data('PN', value.productName);
                            $("#itemCode", $currentRow).selectpicker('render');
                            $('.itemDescText',$currentRow).val(value.quotationProductDescription);
                            $("input[name='availableQuantity']", $currentRow).val(currentProducts[value.productID].LPQ).prop('readonly', true).addUom(currentProducts[value.productID].uom);
                            $("input[name='deliverQuanity']", $currentRow).val(value.quotationProductQuantity - value.quotationProductCopiedQuantity).addUom(currentProducts[value.productID].uom);
                            $("input[name='availableQuantity']", $currentRow).parent().addClass('input-group');
                            $("input[name='deliverQuanity']", $currentRow).parent().addClass('input-group');
                            $("input[name='unitPrice']", $currentRow).val(value.quotationProductUnitPrice / customCurrencyRate).addUomPrice(currentProducts[value.productID].uom);
                            if (value.quotationProductDiscountType == 'vl') {
                                $("input[name='discount']", $currentRow).val((value.quotationProductDiscount / customCurrencyRate).toFixed(2)).addUomPrice(currentProducts[value.productID].uom, baseUom);
                                $("input[name='discount']", $currentRow).siblings('.uom-price-select').find('.uom_price_drop_down').attr('disabled', true);

                                $("input[name='discount']", $currentRow).addClass('value');
                                $(".sign", $currentRow).text(companyCurrencySymbol);
                            } else {
                                if (value.quotationProductDiscount != null) {
                                    $("input[name='discount']", $currentRow).val(parseFloat(value.quotationProductDiscount).toFixed(2));
                                    $("input[name='discount']", $currentRow).addClass('precentage');
                                    $(".sign", $currentRow).text('%');
                                }
                            }
                            delDiscountTrigger = true;
                            //check wheter sales order product have sub product or not
                            $("input[name='deliverQuanity']", $currentRow).trigger(jQuery.Event("focusout"));
                            if ($.isEmptyObject(currentProducts[value.productID].batch) &&
                                    $.isEmptyObject(currentProducts[value.productID].serial) &&
                                    $.isEmptyObject(currentProducts[value.productID].batchSerial)) {

                                $currentRow.removeClass('subproducts');
                                $('.edit-modal', $currentRow).parent().addClass('hidden');
                                $("td[colspan]", $currentRow).attr('colspan', 2);
                            } else {
                                checkSubProduct[value.productID + "_" + rowIncID] = 0;
                                $currentRow.addClass('subproducts');
                                $("input[name='deliverQuantity']", $currentRow).prop('readonly', true).change();
                            }
                            $currentRow.removeClass('add-row');
                            $currentRow.addClass('edit-row');
                            $currentRow.find("button.delete").removeClass('disabled');
                            $currentRow.find(".addItemFilter").remove();
                            var $newRow = $($addRowSample.clone()).removeClass('sample hidden').appendTo($('#add-new-item-row', $productTable));
                            setProductTypeahead();
                            $newRow.find("#itemCode").focus();
                        }
                    });

					custCurrencyId = (custCurrencyId != null) ? custCurrencyId : customCurrencyID;
                    $('#customCurrencyId').val(custCurrencyId).trigger('change');
                    $('#customCurrencyRate').val(customCurrencyRate).trigger('change');
                    if (!(custCurrencyId == '' || custCurrencyId == 0)) {
                        $('#customCurrencyId').attr('disabled', true);
                    }
                } else {
                    quotationID = '';
                    $('#quotationNo').val('');
                    p_notification(false, eb.getMessage('ERR_INVO_WRNG_QOT'));
                }

            }
        });
        copyFromDel = false;
        copyFromSalesOrd = false;
        copyFromQuot = true;
        copyFromAct = false;
        copyFromPro = false;
        copyFromJob = false;
    }

// this will append all Activity related product to Invoice(copy to function)
    function getActivityDetails(ActiID) {
        eb.ajax({
            type: 'POST',
            url: BASE_URL + '/api/activity/getActivityDetailsByActivityID',
            data: {
                activityID: ActiID,
                activityCode: $('#activityNo option:selected').text()
            },
            success: function(respond) {
                if (respond.status) {
                    customerID = respond.data.customer.customerID;
                    cusProfID = respond.data.customerProfID;
                    setCustomerDetails(respond.data.customer);
                    $('#customer').attr('disabled', true);
                    $('#addCustomerBtn').prop('disabled', true);
                    $('#addCustomerBtn').on('click', function(e) {
                        e.preventDefault();
                    });
                    var cusProfName;
                    if (respond.data.customerProfName == '' || respond.data.customerProfName == null) {
                        cusProfName = 'Default';
                    } else {
                        cusProfName = respond.data.customerProfName;
                    }
                    $('.cus_prof_div').removeClass('hidden');
                    $('.cus-prof-select').addClass('hidden');
                    $('.tooltip_for_default_cus').addClass('hidden');
                    $('.default-cus-prof').removeClass('hidden');
                    $('#cus-prof-name').val(cusProfName);

                    currentProducts = respond.data.locationProducts;
                    activityProducts = respond.data.activityProducts;
//activity product related data apppend in here
                    $.each(respond.data.activityProducts, function(index, value) {
                        var $currentRow = getAddRow();
                        var $flag = true;
                        if (value.productType != 2 && value.quantity == 0) {
                            $flag = false;
                        }
                        if ($flag) {
//activity product related tax apply in here
                            if (currentProducts[value.productID].tax) {
                                $currentRow.find('#taxApplied').removeClass('glyphicon glyphicon-unchecked');
                                $currentRow.find('#taxApplied').addClass('glyphicon glyphicon-check');
                                $currentRow.find('.tempLi').remove();
                                for (var i in currentProducts[value.productID].tax) {
                                    var clonedLi = $($currentRow.find('#sampleLi').clone()).removeClass('hidden').attr('id', 'li_' + i).addClass("tempLi");
                                    clonedLi.children(".taxChecks").attr('id', value.productID + '_' + i).prop('checked', true).addClass('addNewTaxCheck');
                                    clonedLi.children(".taxName").html('&nbsp&nbsp' + currentProducts[value.productID].tax[i].tN + '&nbsp&nbsp' + currentProducts[value.productID].tax[i].tP + '%').attr('for', value.productID + '_' + i);
                                    clonedLi.insertBefore($currentRow.find('#sampleLi'));
                                }
                            }

//activity product related data append in here
                            $currentRow.data('id', value.productID);
                            $currentRow.attr('id', 'product' + value.productID);
                            $currentRow.data('locationproductid', value.locationProductID);
                            $currentRow.data('icid',rowincrementID);
                            $currentRow.data('proIncID',rowincrementID);
                            $currentRow.addClass('copied');
                            var rowIncID = rowincrementID;
                            rowincrementID++;
                            $currentRow.data('stockupdate', false);
                            $("#itemCode", $currentRow).empty();
                            $("#itemCode", $currentRow).
                                    append($("<option></option>").
                                            attr("value", value.productID).
                                            text(value.productName + '-' + value.productCode));
                            $("#itemCode", $currentRow).val(value.productID).prop('disabled', 'disabled');
                            $("#itemCode", $currentRow).data('PT', value.productType);
                            $("#itemCode", $currentRow).data('PC', value.productCode);
                            $("#itemCode", $currentRow).data('PN', value.productName);
                            $("#itemCode", $currentRow).selectpicker('refresh');
                            $("input[name='availableQuantity']", $currentRow).val(value.quantity).prop('readonly', true).addUom(currentProducts[value.productID].uom);
                            //value.quantity get the total quantity
                            $("input[name='deliverQuanity']", $currentRow).val(value.quantity).prop('readonly', true).addUom(currentProducts[value.productID].uom);
                            $("input[name='availableQuantity']", $currentRow).parent().addClass('input-group');
                            $("input[name='deliverQuanity']", $currentRow).parent().addClass('input-group');
                            $("input[name='unitPrice']", $currentRow).val(value.unitPrice).addUomPrice(currentProducts[value.productID].uom);
                            if (value.discount !== null) {
                                $("input[name='discount']", $currentRow).val(parseFloat(value.discount).toFixed(2));    
                            }
                            if (value.discountType == 'value') {
                                $("input[name='discount']", $currentRow).addClass('value');
                                $(".sign", $currentRow).text(companyCurrencySymbol);
                            } else {
                                $("input[name='discount']", $currentRow).addClass('precentage');
                                $(".sign", $currentRow).text('%');
                            }
                            $("input[name='deliverQuanity']", $currentRow).trigger(jQuery.Event("focusout"));
                            //if product has sub products those was append i here to invoice
                            if ($.isEmptyObject(activityProducts[value.productID].subProduct)) {
                                $currentRow.removeClass('subproducts');
                                $('.edit-modal', $currentRow).parent().addClass('hidden');
                                $("td[colspan]", $currentRow).attr('colspan', 2);
                            } else {
                                checkSubProduct[value.productID + "_" + rowIncID] = 0;
                                $currentRow.addClass('subproducts');
                                $("input[name='deliverQuantity']", $currentRow).prop('readonly', true).change();
                                var subProducts = [];
                                var qtyTotal = 0;
                                $.each(activityProducts[value.productID].subProduct, function(ind, val) {
                                    var thisSubProduct = {};
                                    if (val.productBatchID) {
                                        thisSubProduct.batchID = val.productBatchID;
                                    } else {
                                        thisSubProduct.batchID = undefined;
                                    }
                                    if (val.productSerialID) {
                                        thisSubProduct.serialID = val.productSerialID;
                                    } else {
                                        thisSubProduct.serialID = undefined;
                                    }
                                    thisSubProduct.qtyByBase = val.activitySubProductQuantity;
                                    subProducts.push(thisSubProduct);
                                    qtyTotal += parseFloat(val.activitySubProductQuantity);
                                });
                                SubProductsQty[index] = qtyTotal;
                                deliverSubProducts[index] = subProducts;
                                subProductsForCheck[index] = subProducts;
                            }

                            $currentRow.addClass('edit-row');
                            $currentRow.removeClass('add-row');
                            $currentRow.find("button.delete").removeClass('disabled');
                            $currentRow.find(".addItemFilter").remove();
                            var $newRow = $($addRowSample.clone()).removeClass('sample hidden').appendTo($('#add-new-item-row', $productTable));
                            $newRow.find("#itemCode").focus();
                        }
                    });
                    var $currentRow = getAddRow();
                    clearProductRow($currentRow);
                    setProductTypeahead();
                } else {
                    activityID = '';
                    $('#activityNo').val('').trigger('change');
                    p_notification(respond.status, respond.msg);
                }

            }
        });
        copyFromDel = false;
        copyFromSalesOrd = false;
        copyFromQuot = false;
        copyFromAct = true;
        copyFromPro = false;
        copyFromJob = false;
    }

// this will append Jobs related all product to Invoice(copy to function)
    function getJobDetails(jobID) {
        eb.ajax({
            type: 'POST',
            url: BASE_URL + '/job-api/getJobProductsDetailsByJobID',
            data: {
                jobID: jobID,
                jobCode: $('#jobNo option:selected').text()
            },
            success: function(respond) {
                if (respond.status) {
                    customerID = respond.data.customer.customerID;
                    cusProfID = respond.data.customerProfID;
                    setCustomerDetails(respond.data.customer);
                    $('#customer').attr('disabled', true);
                    $('#addCustomerBtn').prop('disabled', true);
                    $('#addCustomerBtn').on('click', function(e) {
                        e.preventDefault();
                    });

                    var cusProfName;
                    if (respond.data.customerProfName == '' || respond.data.customerProfName == null) {
                        cusProfName = 'Default';
                    } else {
                        cusProfName = respond.data.customerProfName;
                    }
                    $('.cus_prof_div').removeClass('hidden');
                    $('.cus-prof-select').addClass('hidden');
                    $('.tooltip_for_default_cus').addClass('hidden');
                    $('.default-cus-prof').removeClass('hidden');
                    $('#cus-prof-name').val(cusProfName);

                    currentProducts = respond.data.locationProducts;
                    activityProducts = respond.data.activityProducts;
//activity product related data apppend in here
                    $.each(respond.data.activityProducts, function(index, value) {

                        var $currentRow = getAddRow();
                        var $flag = true;
                        if (value.productType != 2 && value.quantity == 0) {
                            $flag = false;
                        }

                        var pid = value.productID;
                        if ($flag) {
//activity product related tax apply in here
                            if (currentProducts[value.productID].tax) {
                                $currentRow.find('#taxApplied').removeClass('glyphicon glyphicon-unchecked');
                                $currentRow.find('#taxApplied').addClass('glyphicon glyphicon-check');
                                $currentRow.find('.tempLi').remove();
                                for (var i in currentProducts[value.productID].tax) {
                                    var clonedLi = $($currentRow.find('#sampleLi').clone()).removeClass('hidden').attr('id', 'li_' + i).addClass("tempLi");
                                    clonedLi.children(".taxChecks").attr('id', value.productID + '_' + i).prop('checked', true).addClass('addNewTaxCheck');
                                    clonedLi.children(".taxName").html('&nbsp&nbsp' + currentProducts[value.productID].tax[i].tN + '&nbsp&nbsp' + currentProducts[value.productID].tax[i].tP + '%').attr('for', value.productID + '_' + i);
                                    clonedLi.insertBefore($currentRow.find('#sampleLi'));
                                }
                            }

//activity product related data append in here
                            $currentRow.data('id', pid);
                            $currentRow.attr('id', 'product' + pid);
                            $currentRow.data('locationproductid', value.locationProductID);
                            $currentRow.data('icid',rowincrementID);
                            $currentRow.data('proIncID',rowincrementID);
                            $currentRow.addClass('copied');
                            var rowIncID = rowincrementID;
                            rowincrementID++;
                            $currentRow.data('stockupdate', false);
                            $("#itemCode", $currentRow).empty();
                            $("#itemCode", $currentRow).
                                    append($("<option></option>").
                                            attr("value", pid).
                                            text(value.productName + '-' + value.productCode));
                            $("#itemCode", $currentRow).val(pid).prop('disabled', 'disabled');
                            $("#itemCode", $currentRow).data('PT', value.productType);
                            $("#itemCode", $currentRow).data('PC', value.productCode);
                            $("#itemCode", $currentRow).data('PN', value.productName);
                            $("#itemCode", $currentRow).selectpicker('refresh');
                            $("input[name='availableQuantity']", $currentRow).val(value.quantity).prop('readonly', true).addUom(currentProducts[value.productID].uom);
                            //value.quantity get the total quantity
                            $("input[name='deliverQuanity']", $currentRow).val(value.quantity).prop('readonly', true).addUom(currentProducts[value.productID].uom);
                            $("input[name='availableQuantity']", $currentRow).parent().addClass('input-group');
                            $("input[name='deliverQuanity']", $currentRow).parent().addClass('input-group');
                            $("input[name='unitPrice']", $currentRow).val(value.unitPrice).addUomPrice(currentProducts[value.productID].uom);
                            if (value.discount !== null) {
                                $("input[name='discount']", $currentRow).val(parseFloat(value.discount).toFixed(2));
                            }
                            if (value.discountType == 'value') {
                                $("input[name='discount']", $currentRow).addClass('value');
                                $(".sign", $currentRow).text(companyCurrencySymbol);
                            } else {
                                $("input[name='discount']", $currentRow).addClass('precentage');
                                $(".sign", $currentRow).text('%');
                            }
                            $("input[name='deliverQuanity']", $currentRow).trigger(jQuery.Event("focusout"));
                            //if product has sub products those was append i here to invoice
                            if ($.isEmptyObject(activityProducts[pid].subProduct)) {
                                $currentRow.removeClass('subproducts');
                                $('.edit-modal', $currentRow).parent().addClass('hidden');
                                $("td[colspan]", $currentRow).attr('colspan', 2);
                            } else {
                                checkSubProduct[pid + "_" + rowIncID] = 0;
                                $currentRow.addClass('subproducts');
                                $("input[name='deliverQuantity']", $currentRow).prop('readonly', true).change();
                                var subProducts = [];
                                var qtyTotal = 0;
                                $.each(activityProducts[pid].subProduct, function(ind, val) {
                                    var thisSubProduct = {};
                                    if (val.productBatchID) {
                                        thisSubProduct.batchID = val.productBatchID;
                                    } else {
                                        thisSubProduct.batchID = undefined;
                                    }
                                    if (val.productSerialID) {
                                        thisSubProduct.serialID = val.productSerialID;
                                    } else {
                                        thisSubProduct.serialID = undefined;
                                    }
                                    thisSubProduct.qtyByBase = val.activitySubProductQuantity;
                                    subProducts.push(thisSubProduct);
                                    qtyTotal += parseFloat(val.activitySubProductQuantity);
                                });
                                SubProductsQty[index] = qtyTotal;
                                deliverSubProducts[index] = subProducts;
                                subProductsForCheck[index] = subProducts;
                            }

                            $currentRow.addClass('edit-row');
                            $currentRow.removeClass('add-row');
                            $currentRow.find("button.delete").removeClass('disabled');
                            $currentRow.find(".addItemFilter").remove();
                            var $newRow = $($addRowSample.clone()).removeClass('sample hidden').appendTo($('#add-new-item-row', $productTable));
                            $newRow.find("#itemCode").focus();
                        }
                    });
                    var $currentRow = getAddRow();
                    clearProductRow($currentRow);
                    setProductTypeahead();
                } else {
                    jobID = '';
                    $('#jobNo').val('').trigger('change');
                    p_notification(respond.status, respond.msg);
                }

            }
        });
        copyFromDel = false;
        copyFromSalesOrd = false;
        copyFromQuot = false;
        copyFromAct = false;
        copyFromPro = false;
        copyFromJob = true;
    }

// this will append project related all product to Invoice(copy to function)
    function getProjectDetails(projectID) {
        eb.ajax({
            type: 'POST',
            url: BASE_URL + '/project-api/getProjectProductsDetailsByProjectID',
            data: {
                projectID: projectID,
                projectCode: $('#projectNo option:selected').text()
            },
            success: function(respond) {
                if (respond.status) {
                    customerID = respond.data.customer.customerID;
                    setCustomerDetails(respond.data.customer);
                    $('#customer').attr('disabled', true);
                    $('#addCustomerBtn').prop('disabled', true);
                    $('#addCustomerBtn').on('click', function(e) {
                        e.preventDefault();
                    });

                    var cusProfName;
                    if (respond.data.customerProfName == '' || respond.data.customerProfName == null) {
                        cusProfName = 'Default';
                    } else {
                        cusProfName = respond.data.customerProfName;
                    }
                    $('.cus_prof_div').removeClass('hidden');
                    $('.cus-prof-select').addClass('hidden');
                    $('.tooltip_for_default_cus').addClass('hidden');
                    $('.default-cus-prof').removeClass('hidden');
                    $('#cus-prof-name').val(cusProfName);

                    currentProducts = respond.data.locationProducts;
                    activityProducts = respond.data.activityProducts;
//activity product related data apppend in here
                    $.each(respond.data.activityProducts, function(index, value) {

                        var $currentRow = getAddRow();
                        var $flag = true;
                        if (value.productType != 2 && value.quantity == 0) {
                            $flag = false;
                        }

                        var pid = value.productID;
                        if ($flag) {
//activity product related tax apply in here
                            if (currentProducts[value.productID].tax) {
                                $currentRow.find('#taxApplied').removeClass('glyphicon glyphicon-unchecked');
                                $currentRow.find('#taxApplied').addClass('glyphicon glyphicon-check');
                                $currentRow.find('.tempLi').remove();
                                for (var i in currentProducts[value.productID].tax) {
                                    var clonedLi = $($currentRow.find('#sampleLi').clone()).removeClass('hidden').attr('id', 'li_' + i).addClass("tempLi");
                                    clonedLi.children(".taxChecks").attr('id', value.productID + '_' + i).prop('checked', true).addClass('addNewTaxCheck');
                                    clonedLi.children(".taxName").html('&nbsp&nbsp' + currentProducts[value.productID].tax[i].tN + '&nbsp&nbsp' + currentProducts[value.productID].tax[i].tP + '%').attr('for', value.productID + '_' + i);
                                    clonedLi.insertBefore($currentRow.find('#sampleLi'));
                                }
                            }

//activity product related data append in here
                            $currentRow.data('id', pid);
                            $currentRow.attr('id', 'product' + pid);
                            $currentRow.data('locationproductid', value.locationProductID);
                            $currentRow.data('icid',rowincrementID);
                            $currentRow.data('proIncID',rowincrementID);
                            $currentRow.addClass('copied');
                            var rowIncID = rowincrementID;
                            rowincrementID++;
                            $currentRow.data('stockupdate', false);
                            $("#itemCode", $currentRow).empty();
                            $("#itemCode", $currentRow).
                                    append($("<option></option>").
                                            attr("value", pid).
                                            text(value.productName + '-' + value.productCode));
                            $("#itemCode", $currentRow).val(pid).prop('disabled', 'disabled');
                            $("#itemCode", $currentRow).data('PT', value.productType);
                            $("#itemCode", $currentRow).data('PC', value.productCode);
                            $("#itemCode", $currentRow).data('PN', value.productName);
                            $("#itemCode", $currentRow).selectpicker('refresh');
                            $("input[name='availableQuantity']", $currentRow).val(value.quantity).prop('readonly', true).addUom(currentProducts[value.productID].uom);
                            //value.quantity get the total quantity
                            $("input[name='deliverQuanity']", $currentRow).val(value.quantity).prop('readonly', true).addUom(currentProducts[value.productID].uom);
                            $("input[name='availableQuantity']", $currentRow).parent().addClass('input-group');
                            $("input[name='deliverQuanity']", $currentRow).parent().addClass('input-group');
                            $("input[name='unitPrice']", $currentRow).val(value.unitPrice).addUomPrice(currentProducts[value.productID].uom);
                            if (value.discount !== null) {
                                $("input[name='discount']", $currentRow).val(parseFloat(value.discount).toFixed(2));
                            }
                            if (value.discountType == 'value') {
                                $("input[name='discount']", $currentRow).addClass('value');
                                $(".sign", $currentRow).text(companyCurrencySymbol);
                            } else {
                                $("input[name='discount']", $currentRow).addClass('precentage');
                                $(".sign", $currentRow).text('%');
                            }
                            $("input[name='deliverQuanity']", $currentRow).trigger(jQuery.Event("focusout"));
                            //if product has sub products those was append i here to invoice
                            if ($.isEmptyObject(activityProducts[pid].subProduct)) {
                                $currentRow.removeClass('subproducts');
                                $('.edit-modal', $currentRow).parent().addClass('hidden');
                                $("td[colspan]", $currentRow).attr('colspan', 2);
                            } else {
                                checkSubProduct[pid+ "_" + rowIncID] = 0;
                                $currentRow.addClass('subproducts');
                                $("input[name='deliverQuantity']", $currentRow).prop('readonly', true).change();
                                var subProducts = [];
                                var qtyTotal = 0;
                                $.each(activityProducts[pid].subProduct, function(ind, val) {
                                    var thisSubProduct = {};
                                    if (val.productBatchID) {
                                        thisSubProduct.batchID = val.productBatchID;
                                    } else {
                                        thisSubProduct.batchID = undefined;
                                    }
                                    if (val.productSerialID) {
                                        thisSubProduct.serialID = val.productSerialID;
                                    } else {
                                        thisSubProduct.serialID = undefined;
                                    }
                                    thisSubProduct.qtyByBase = val.activitySubProductQuantity;
                                    subProducts.push(thisSubProduct);
                                    qtyTotal += parseFloat(val.activitySubProductQuantity);
                                });
                                SubProductsQty[index] = qtyTotal;
                                deliverSubProducts[index] = subProducts;
                                subProductsForCheck[index] = subProducts;
                            }

                            $currentRow.addClass('edit-row');
                            $currentRow.removeClass('add-row');
                            $currentRow.find("button.delete").removeClass('disabled');
                            var $newRow = $($addRowSample.clone()).removeClass('sample hidden').appendTo($('#add-new-item-row', $productTable));
                            $newRow.find("#itemCode").focus();
                        }
                    });
                    var $currentRow = getAddRow();
                    clearProductRow($currentRow);
                    setProductTypeahead();
                } else {
                    jobID = '';
                    $('#jobNo').val('').trigger('change');
                    p_notification(respond.status, respond.msg);
                }

            }
        });
        copyFromDel = false;
        copyFromSalesOrd = false;
        copyFromQuot = false;
        copyFromAct = false;
        copyFromPro = true;
        copyFromJob = false;

    }

    $('#paymentTerm').on('change', function(e) {
        e.preventDefault();
        var days = 0;
        var i = parseInt($(this).val());
        switch (i) {
            case 2:
                days = 7;
                break;
            case 3:
                days = 14;
                break;
            case 4:
                days = 30;
                break;
            case 5:
                days = 60;
                break;
            case 6:
                days = 90;
                break;
            case 7:
                days = 21;
                break;
            case 8:
                days = 28;
                break;
            case 9:
                days = 45;
                break;
            case 10:
                days = 35;
                break;
            default:
                days = 0;
                break;
        }
        setDueDate(days);
    });
    function setDueDate(days) {
        var day = parseInt(days);
        var joindate = eb.convertDateFormat('#deliveryDate');
        joindate.setDate(joindate.getDate() + day);
        var dd = joindate.getDate() < 10 ? '0' + joindate.getDate() : joindate.getDate();
        var mm = (joindate.getMonth() + 1) < 10 ? '0' + (joindate.getMonth() + 1) : (joindate.getMonth() + 1);
        var y = joindate.getFullYear();
        var joinFormattedDate = eb.getDateForDocumentEdit('#dueDate', dd, mm, y);
        $("#dueDate").val(joinFormattedDate);
    }

    setProductTypeahead();
    function setProductTypeahead() {

        var $currentRow = getAddRow();
        $('#itemCode', $currentRow).selectpicker();
        var locationID = $('#idOfLocation').val();

        var documentType = 'invoice';
        var URL = '/productAPI/search-location-products-for-dropdown/serial-search'+'?attrID='+selectedAttrTypeID+'&attrValID='+selectedAttrValID;
        loadDropDownFromDatabase(URL, locationID, 1, '#itemCode', $currentRow, '', documentType);

        $($currentRow).on('click', '.addNewItem', function() {
            addNewItem($currentRow);
        });

        $('#itemCode', $currentRow).on('change', function() {
            if ($(this).val() == 'add') {
                addNewItem($currentRow);
            }
            if ($(this).val() != 'add' && $(this).val() != 0 && selectedId != $(this).val()) {
                selectedId = $(this).val();
                var itemMapper = $('#itemCode').data('extra');
                var pid = itemMapper[selectedId].pid;

                selectedSerialID = (itemMapper && itemMapper[selectedId]) ? itemMapper[selectedId].srl_id : undefined;
                eb.ajax({
                    type: 'POST',
                    url: BASE_URL + '/productAPI/get-location-product-details',
                    data: {productID: pid, locationID: locationID},
                    success: function(respond) {
                        selectLocationProduct = respond.data;
                        selectProductForTransfer(respond.data);
                        $('#itemUnitPrice', $currentRow).trigger('blur');
                    }
                });
            }
        });
    }
    function addNewItem(currentRow) {
        $('#createProduct').modal('show');
        var defaultCategory = $('#categoryParentID').attr('data-id');
        $('#categoryParentID').val(defaultCategory);
        $('#categoryParentID').selectpicker('refresh');
        var defaultUomID = $('#defaultUomID').val();
        $("[name='uomID[]']").val(defaultUomID);
        productRow = currentRow;
    }
    $('#createProduct').on('shown.bs.modal', function() {
        $(this).find("#productCode").focus();
    });

    function selectProductForTransfer(selectedlocationProduct, draftQty = null, draftUprice = null) {

    	var $currentRow = getAddRow();
        clearProductRow($currentRow);

    	if (selectedSerialID) {
    		if (!$.isEmptyObject(selectedlocationProduct.batchSerial)){
    			var expDate = selectedlocationProduct.batchSerial[selectedSerialID].PBSExpD;
    			if(!(expDate == null || expDate == '0000-00-00')){
    				var expireCheck = checkEpireData(expDate);
    				if(expireCheck){
    					p_notification(false, eb.getMessage('ERR_INVO_SELCTED_SERIAL_ITEM_IS_EXPIRED'));
    					return false;
    				}
    			}
    		}else if(!$.isEmptyObject(selectedlocationProduct.serial)){
    			var expDate = selectedlocationProduct.serial[selectedSerialID].PSExpD;
    			if(!(expDate == null || expDate == '0000-00-00')){
    				var expireCheck = checkEpireData(expDate);
    				if(expireCheck){
    					p_notification(false, eb.getMessage('ERR_INVO_SELCTED_SERIAL_ITEM_IS_EXPIRED'));
    					return false;
    				}
    			}
    		}
    	}

        var itemDuplicateCheck = false;
        if (!$.isEmptyObject(selectedlocationProduct.batch) ||
                !$.isEmptyObject(selectedlocationProduct.serial) ||
                !$.isEmptyObject(selectedlocationProduct.batchSerial)) {
            $.each(deliverProducts, function(index, value) {
                var chkProductID = index.split('_')[0];
                if (chkProductID === selectedlocationProduct.pID) {

//                    var isAdded = _.where(deliverSubProducts[chkProductID], {serialID: selectedSerialID})
//                    if (selectedSerialID == null || isAdded.length > 0) {
//                        itemDuplicateCheck = true;
//                        p_notification(false, eb.getMessage('ERR_DELI_ALREADY_SELECT_PROD'));
//                        return false;
//                    }
                }
            });
        }

        if (itemDuplicateCheck) {
            return false;
        }
        currentProducts[selectedlocationProduct.pID] = selectedlocationProduct;
        var productID = selectedlocationProduct.pID;
        var productCode = selectedlocationProduct.pC;
        var productType = selectedlocationProduct.pT;
        var productName = selectedlocationProduct.pN;
        var isBomItem = selectedlocationProduct.isBom;
        var mrpType = selectedlocationProduct.mrpType;
        var mrpValue = selectedlocationProduct.mrpValue;
        var mrpPercentage = selectedlocationProduct.mrpPercentageValue;
        var bomData = selectedlocationProduct.bomData;
        var giftCard = selectedlocationProduct.giftCard;
        var availableQuantity = (selectedlocationProduct.LPQ == null) ? 0 : selectedlocationProduct.LPQ;
        var defaultSellingPrice = selectedlocationProduct.dSP;
        if (selectedlocationProduct.dPR != null) {
            var productDiscountPrecentage = parseFloat(selectedlocationProduct.dPR).toFixed(2);
        } else {
            var productDiscountPrecentage = 0;
        }
        
        if (selectedlocationProduct.dV != null) {
            var productDiscountValue = parseFloat(selectedlocationProduct.dV).toFixed(2);
        } else {
            var productDiscountValue = 0;
        }

        if (priceListItems[productID] !== undefined && parseFloat(priceListItems[productID].itemPrice) > 0) {
            defaultSellingPrice = parseFloat(priceListItems[productID].itemPrice);
            var itemDiscountType = priceListItems[productID].itemDiscountType;
            productDiscountValue = (itemDiscountType == 1) ? priceListItems[productID].itemDiscount : 0;
            productDiscountPrecentage = (itemDiscountType == 2) ? priceListItems[productID].itemDiscount : 0;
        }
        defaultSellingPriceData[productID] = defaultSellingPrice;


        var baseUom;
        for (var j in selectedlocationProduct.uom) {
            if (selectedlocationProduct.uom[j].pUBase == 1) {
                baseUom = selectedlocationProduct.uom[j].uomID;
            }
        }



        $currentRow.data('id', selectedlocationProduct.pID);
        $currentRow.data('icid', rowincrementID);
        $currentRow.data('isBom', isBomItem);
        $currentRow.data('mrpType', mrpType);
        $currentRow.data('mrpValue', mrpValue);
        $currentRow.data('mrpPercentageValue', mrpPercentage);
        $currentRow.data('isBomJeSubItem', bomData['isJEPostSubItemWise']);
        $currentRow.data('locationproductid', selectedlocationProduct.lPID);
        $currentRow.data('proIncID', rowincrementID);
        $currentRow.data('stockupdate', true);
        $('#availableQuantity', $currentRow).parent().addClass('input-group');
        $('#deliverQuanity', $currentRow).parent().addClass('input-group');
        $("input[name='discount']", $currentRow).prop('readonly', false);
        $("#itemCode", $currentRow).data('PT', productType);
        $("#itemCode", $currentRow).data('PN', productName);
        $("#itemCode", $currentRow).data('PC', productCode);
        $("#itemCode", $currentRow).data('GiftCard', giftCard);
        $("input[name='availableQuantity']", $currentRow).val(availableQuantity).prop('readonly', true);
        if (draftUprice != null) {
            $("input[name='unitPrice']", $currentRow).val(parseFloat(draftUprice)).addUomPrice(selectedlocationProduct.uom);
        } else {
            $("input[name='unitPrice']", $currentRow).val(parseFloat(defaultSellingPrice)).addUomPrice(selectedlocationProduct.uom);
        }
        if (selectedlocationProduct.dEL == 1 || (priceListItems[productID] != undefined && parseFloat(priceListItems[productID].itemPrice) > 0)) {
            if (selectedlocationProduct.dV != null) {
                productDiscountValue = parseFloat(productDiscountValue).toFixed(2);
                $("input[name='discount']", $currentRow).val(productDiscountValue).addUomPrice(selectedlocationProduct.uom, baseUom);
                $("input[name='discount']", $currentRow).addClass('value');
                $("input[name='discount']", $currentRow).siblings('.uom-price-select').find('.uom_price_drop_down').attr('disabled', true);
                if ($("input[name='discount']", $currentRow).hasClass('precentage')) {
                    $("input[name='discount']", $currentRow).removeClass('precentage');
                }
                $(".sign", $currentRow).text(companyCurrencySymbol);
            } else if (selectedlocationProduct.dPR != null){
                productDiscountPrecentage = parseFloat(productDiscountPrecentage).toFixed(2);
                $("input[name='discount']", $currentRow).val(productDiscountPrecentage);
                $("input[name='discount']", $currentRow).addClass('precentage');
                if ($("input[name='discount']", $currentRow).hasClass('value')) {
                    $("input[name='discount']", $currentRow).removeClass('value');
                }
                $(".sign", $currentRow).text('%');
            }
        } else {
            $("input[name='discount']", $currentRow).prop('readonly', true);
        }
        $("input[name='deliverQuanity']", $currentRow).prop('readonly', false);


        $('.uomqty').attr("id", "itemQuantity");
        $('#unitPrice',$currentRow).siblings('.uomPrice').attr("id", "itemUnitPrice");
        if (isBomItem) {
            // add uom list
            if (productType != 2) {
                if (bomData['isJEPostSubItemWise'] == 1) {
                    $("#itemUnitPrice", $currentRow).prop('readonly', true);
                    if (draftQty != null) {
                        $("input[name='deliverQuanity']", $currentRow).val(draftQty).addUom(selectedlocationProduct.uom);
                    } else {
                        $("input[name='deliverQuanity']", $currentRow).addUom(selectedlocationProduct.uom);
                    }
                    $("input[name='availableQuantity']", $currentRow).css("display", "none");
                } else {
                    $("input[name='availableQuantity'], input[name='deliverQuanity']", $currentRow).addUom(selectedlocationProduct.uom);
                }

            } else {
                $("#itemUnitPrice", $currentRow).prop('readonly', true);
                if (draftQty != null) {
                    $("input[name='deliverQuanity']", $currentRow).val(draftQty).addUom(selectedlocationProduct.uom);
                } else {
                    $("input[name='deliverQuanity']", $currentRow).addUom(selectedlocationProduct.uom);
                }
                $("input[name='availableQuantity']", $currentRow).css("display", "none");
            }
        } else {
            // add uom list
            if (productType != 2) {
                $("input[name='availableQuantity'], input[name='deliverQuanity']", $currentRow).addUom(selectedlocationProduct.uom);
            } else {
                if (draftQty != null) {
                    $("input[name='deliverQuanity']", $currentRow).val(draftQty).addUom(selectedlocationProduct.uom);
                } else {
                    $("input[name='deliverQuanity']", $currentRow).addUom(selectedlocationProduct.uom);
                }
                $("input[name='availableQuantity']", $currentRow).css("display", "none");
            }
        }
        var deleted = 0;
        // clear old rows
        $("#batch_data tr:not(.hidden)").remove();
        addBatchProduct(productID, productCode, deleted);
        addSerialProduct(productID, productCode, deleted, []);
        addSerialBatchProduct(productID, productCode, deleted);

        //check this product serail or batch
        if (!$.isEmptyObject(selectedlocationProduct.serial)) {
            $('#product-batch-modal .serial-auto-select').removeClass('hidden');
            $('#numberOfRow').val('');
        } else {
            $('#product-batch-modal .serial-auto-select').addClass('hidden');
        }


        var rowincID = rowincrementID;
        $("tr", '#batch_data').each(function() {
            var $thisSubRow = $(this);
            for (var i in subProductsForCheck[productID]) {
                if (subProductsForCheck[productID][i].serialID !== undefined) {
                    if ($(".serialID", $thisSubRow).data('id') == subProductsForCheck[productID][i].serialID) {
                        $("input[name='deliverQuantityCheck']", $thisSubRow).prop('checked', true);
                        $("input[name='warranty']", $thisSubRow).val(subProductsForCheck[productID][i].warranty);
                        $("input[name='deliverQuantityCheck']", $thisSubRow).attr('disabled', true);
                    }

                } else if (subProductsForCheck[productID][i].batchID !== undefined && (rowincID == subProductsForCheck[productID][i].incrementID) ) {
                    if ($(".batchCode", $thisSubRow).data('id') == subProductsForCheck[productID][i].batchID) {
                        $(this).find("input[name='deliverQuantity']").val(subProductsForCheck[productID][i].qtyByBase).change();
                    }
                }
            }
        });

        //check this product serail or batch
        if (!$.isEmptyObject(selectedlocationProduct.serial)) {
            $('#add-product-batch .serial-auto-select').removeClass('hidden');
            $('#numberOfRow').val('');
        } else {
            $('#add-product-batch .serial-auto-select').addClass('hidden');
        }


        // check if any batch / serial products exist
        if ($.isEmptyObject(selectedlocationProduct.batch) &&
                $.isEmptyObject(selectedlocationProduct.serial) &&
                $.isEmptyObject(selectedlocationProduct.batchSerial)) {
            $currentRow.removeClass('subproducts');
            $('.edit-modal', $currentRow).parent().addClass('hidden');
            $("td[colspan]", $currentRow).attr('colspan', 2);
            $("input[name='deliverQuantity']", $currentRow).prop('readonly', false).focus();
        } else {
            $currentRow.addClass('subproducts');
            $('.edit-modal', $currentRow).parent().removeClass('hidden');
            $("td[colspan]", $currentRow).attr('colspan', 1);
            $("input[name='deliverQuantity']", $currentRow).prop('readonly', true);
            if (selectedSerialID) {
                batchModalValidate();
            } else {
                $('#addInvoiceProductsModal').data('icid', rowincrementID);
                $('#addInvoiceProductsModal').data('proIncID', rowincrementID);
                $('#addInvoiceProductsModal').modal('show');
            }
        }

        setTaxListForProduct(selectedlocationProduct.pID, $currentRow);
        $currentRow.attr('id', 'product' + selectedlocationProduct.pID);
    }

    $('#select_serials').on('click',function(e) {
        var numberOfRow = $('#numberOfRow').val();
        if (numberOfRow != '' || numberOfRow != 0) {
            $('#batch_data > tr').each(function(key, value){
                if (numberOfRow != 0) {
                    $("input[name='transferQuantityCheck']", value).prop('checked', true);
                    numberOfRow--;
                } else {
                    $("input[name='transferQuantityCheck']", value).prop('checked', false);
                }
            });
        }    
        
    });

    $('#search_key').on('keyup', function() {
        $('.serialID').each(function(id, row) {
            var txt = $(row).text();
            var search_key = $('#search_key').val();

            if (txt && txt.match(new RegExp('(' + search_key + ')', 'gi'))) {
                $(this).parent('tr').removeClass('hide');
            } else if (search_key != "") {
                $(this).parent('tr').addClass('hide');
            } else if (search_key == "") {
                $(this).parent('tr').removeClass('hide');
            }
        });
    });

    function clearProductRow($currentRow) {
        $("input[name='availableQuantity']", $currentRow).val('').siblings('.uomqty,.uom-select').remove().end().show();
        $("input[name='deliverQuanity']", $currentRow).val('').siblings('.uomqty,.uom-select').remove().end().show();
        $("input[name='unitPrice']", $currentRow).val('').siblings('.uomPrice,.uom-price-select').remove().end().show();
        $("input[name='discount']", $currentRow).val('').siblings('.uomPrice,.uom-price-select').remove().end().show();
        $(".tempLi", $currentRow).remove('');
        $("#taxApplied", $currentRow).removeClass('glyphicon-check');
        $("#taxApplied", $currentRow).addClass('glyphicon-unchecked');
        $(".uomList", $currentRow).remove('');
    }

    function getCurrentProductData($thisRow) {

        var productDesc = ($('.itemDescText', $thisRow).val() != '') ? $('.itemDescText', $thisRow).val() : '';
        var itemSalesPerson = ($('#itemSalesPersonVal', $thisRow).val() != null) ? $('#itemSalesPersonVal', $thisRow).val() : null;
        var discountType = '';
        if ($("input[name='discount']", $thisRow).hasClass('value')) {
            discountType = 'value';
        } else if ($("input[name='discount']", $thisRow).hasClass('precentage')) {
            discountType = 'precentage';
        }

        var selected_serials = $thisRow.find('.edit-modal').data('selectedSids');

        var pid = rowincrementID;
        if ($thisRow.data('proIncID') !== undefined) {
            pid = $thisRow.data('proIncID');
        }

        var currentRowUnitePrice = $("input[name='unitPrice']", $thisRow).val();
        var currentRowInclusiveTaxUnitePrice = null;
        if (inclusiveTax) {
            currentRowUnitePrice = realUnitePrice.toString();
            currentRowInclusiveTaxUnitePrice = $("input[name='unitPrice']", $thisRow).val();
        } 

        var conversion = $("input[name='unitPrice']", $thisRow).siblings('.uom-price-select').find('.selected').data('uc');
        var priceUomAbbr = $($thisRow).find(".uom-price-select").find("span.selected").html();
        var productDisplayPrice = currentRowUnitePrice * conversion;

        var actmrpValue = null;
        var mrpValueDisplay = null;
        if ($($thisRow).data('mrpType') != null) {
            if ($($thisRow).data('mrpType') == 1) {
                var perecentage = $($thisRow).data('mrpPercentageValue');
                mrpPercentageCalVal = ((currentRowUnitePrice * perecentage) / 100);
                actmrpValue = parseFloat(mrpPercentageCalVal) + parseFloat(currentRowUnitePrice);
                mrpValueDisplay = actmrpValue * conversion;

            } else if($($thisRow).data('mrpType') == 2) {
                actmrpValue = $($thisRow).data('mrpValue');
                mrpValueDisplay = actmrpValue * conversion;
            }
        }
        console.log(conversion);
        

        var thisVals = {
            productID: $thisRow.data('id'),
            documentTypeID: (typeof $thisRow.data('documenttypeid') != 'undefined') ? $thisRow.data('documenttypeid') : null,
            documentID: (typeof $thisRow.data('prodocuid') != 'undefined') ? $thisRow.data('prodocuid') : null,
            locationID: (typeof $thisRow.data('locationid') != 'undefined') ? $thisRow.data('locationid') : $('#idOfLocation').val(),
            locationProductID: (typeof $thisRow.data('locationproductid') != 'undefined') ? $thisRow.data('locationproductid') : null,
            productIncID: $thisRow.data('id') + '_' + pid,
            productCode: $("#itemCode", $thisRow).data('PC'),
            productName: $("#itemCode", $thisRow).data('PN'),
            productPrice: currentRowUnitePrice,
            actmrpValue: actmrpValue,
            mrpValueDisplay: mrpValueDisplay,
            productDisplayPrice: productDisplayPrice,
            mrpType: $($thisRow).data('mrpType'),
            mrpValue: $($thisRow).data('mrpValue'),
            mrpPercentageValue: $($thisRow).data('mrpPercentageValue'),
            priceUomAbbr: priceUomAbbr,
            inclusiveTaxProductPrice: currentRowInclusiveTaxUnitePrice,
            productDiscount: $("input[name='discount']", $thisRow).val(),
            productDiscountType: discountType,
            productTotal: productsTotal[$thisRow.data('id')],
            pTax: productsTax[$thisRow.data('id')],
            productType: $("#itemCode", $thisRow).data('PT'),
            isBom: $thisRow.data('isBom'),
            isBomJeSubItem: $thisRow.data('isBomJeSubItem'),
            productDescription: productDesc,
            stockUpdate: $thisRow.data('stockupdate'),
            isFreeItem: ($thisRow.hasClass('freeAddItem')) ? true : false,
            giftCard: $("#itemCode", $thisRow).data('GiftCard'),
            itemSalesPerson: itemSalesPerson,
            availableQuantity: {
                qty: $("input[name='availableQuantity']", $thisRow).val(),
            },
            deliverQuantity: {
                qty: $("input[name='deliverQuanity']", $thisRow).val(),
            },
            selected_serials: selected_serials,
            selectedUomId: $thisRow.find('#deliverQuanity').siblings('.uom-select').children('button').find('.selected').data('uomID')
        };
        return thisVals;
    }

    $productTable.on('click', 'button.add, button.save', function(e) {
        e.preventDefault();
        var $thisRow = $(this).parents('tr');
        var copied = false;
        if(!$thisRow.hasClass('copied')){
            copied = true;
            $('#promotion').trigger('change');
        }
        var thisVals = getCurrentProductData($thisRow);
        //disable the item description
        $('.addItemDescription', $thisRow).prop('disabled', true);
        $('.itemDescText', $thisRow).prop('disabled', true);
        $("input[name='availableQuantity']", $thisRow).prop('readonly', true);
        if ((thisVals.productCode) === undefined && (thisVals.productName) === undefined) {
            p_notification(false, eb.getMessage('ERR_DELI_SELECT_PROD'));
            $("#itemCode", $thisRow).focus();
            return false;
        }
//Zero Value Products can be added to the invoice - Edit (Damith)

        if ((thisVals.productPrice.trim()) < 0) {
            p_notification(false, eb.getMessage('ERR_INVO_SET_PROD'));
            $("input[name='unitPrice']", $thisRow).focus();
            return false;
        }
        if (thisVals.productType != 2 && (isNaN(parseFloat(thisVals.deliverQuantity.qty)) || parseFloat(thisVals.deliverQuantity.qty) <= 0)) {
            p_notification(false, eb.getMessage('ERR_DELI_ENTER_QUAN'));
            $("input[name='deliverQuantity']", $thisRow).focus();
            return false;
        }

        if (thisVals.isBom) {
            if (thisVals.productType != 2 && thisVals.stockUpdate && parseFloat(thisVals.deliverQuantity.qty) > parseFloat(thisVals.availableQuantity.qty) && !thisVals.isBomJeSubItem) {
                p_notification(false, eb.getMessage('ERR_INVO_QUAN'));
                $("input[name='deliverQuantity']", $thisRow).focus();
                if (checkSubProduct[thisVals.productIncID] === undefined) {
                    return false;
                }
            }
        } else {
            if (thisVals.productType != 2 && thisVals.stockUpdate && parseFloat(thisVals.deliverQuantity.qty) > parseFloat(thisVals.availableQuantity.qty)) {
                p_notification(false, eb.getMessage('ERR_INVO_QUAN'));
                $("input[name='deliverQuantity']", $thisRow).focus();
                if (checkSubProduct[thisVals.productIncID] === undefined) {
                    return false;
                }
            }
        }

        if (thisVals.productDiscount) {
            if (isNaN(parseFloat(thisVals.productDiscount)) || parseFloat(thisVals.productDiscount) < 0) {
                p_notification(false, eb.getMessage('ERR_DELI_VALID_DISCOUNT'));
                $("input[name='discount']", $thisRow).focus();
                return false;
            }
        }
//Zero Value Products can be added to the invoice - Edit (Damith)

        if (thisVals.productTotal < 0) {
            if (isNaN(parseFloat(thisVals.productTotal)) || parseFloat(thisVals.productTotal) <= 0) {
                p_notification(false, eb.getMessage('ERR_INVO_PROD_TOTAL'));
                $("input[name='unitPrice']", $thisRow).focus();
                return false;
            }
        }

        if ($thisRow.hasClass('subproducts')) {
            var seletedProdutQty = 0;
            if (!$.isEmptyObject(deliverSubProducts[thisVals.productIncID])) {
            	$.each(deliverSubProducts[thisVals.productIncID] , function(i, currSubProduct){
            		seletedProdutQty += currSubProduct.qtyByBase
            	});
            }
            if ($.isEmptyObject(deliverSubProducts[thisVals.productIncID])) {
                p_notification(false, eb.getMessage('ERR_DELI_SUB_PRODUCT_CHECK', thisVals.productCode));
                $('.edit-modal', $thisRow).focus();
                return false;
            }else if(seletedProdutQty != thisVals.deliverQuantity.qty){
            	p_notification(false, eb.getMessage('ERR_DELI_SUB_PRODUCT_CHECK_WITH_PQTY', thisVals.productCode));
                $('.edit-modal', $thisRow).focus();
                return false;
            }
        }

        if (Object.keys(deliveryNoteIDs).length != 0 && thisVals.stockUpdate == false) {
            if (parseFloat(thisVals.deliverQuantity.qty) > parseFloat(deliveryNoteProductList[thisVals.documentID+'_'+thisVals.productID].deliveryNoteProductQuantity)) {
                $thisRow.find("input[name='deliverQuanity']").val(deliveryNoteProductList[thisVals.documentID+'_'+thisVals.productID].deliveryNoteProductQuantity);
                $thisRow.find('.uomqty').trigger('focusout');
                p_notification(false, eb.getMessage('ERR_DELI_PRODUCT_QUAN_CHECK'));
                return false;
            }
        }

// if add button is clicked
        if ($(this).hasClass('add')) {
        	if(!$thisRow.hasClass('copied')){
        		$thisRow.data('icid', rowincrementID);
        	}
            var $currentRow = $thisRow;
            var $flag = false;
            if ($currentRow.hasClass('add-row')) {
                $flag = true;
            }

            if ($thisRow.hasClass('freeAddItem')) {
                console.log($thisRow.data('freeQty'));
                if (parseFloat($thisRow.data('freeQty')) < parseFloat($currentRow.find("input[name='deliverQuanity']").val())) {
                    p_notification(false, eb.getMessage('ERR_FREE_QUANTITY_CANNOT_CHANGE'));
                    return false;
                }
            }

            $currentRow
                    .removeClass('edit-row')
                    .find("#itemCode, input[name='deliverQuanity'],input[name='availableQuantity'],input[name='unitPrice'],input[name='discount'], input[name='uomqty']").prop('readonly', true).end()
                    .find('td').find('#addNewTax').attr('disabled', true)
                    .find("button.delete").removeClass('disabled');
            $currentRow.find("input[name='deliverQuanity']").change();
            $currentRow.find("input[name='unitPrice']").change();
            $currentRow.find("input[name='discount']").change();
            $currentRow.find('#deliveryNoteDiscount').trigger('focusout');
            $currentRow.removeClass('add-row');
            discountEditedManually[$thisRow.data('icid')] = true;
            $currentRow.find("#itemCode").prop('disabled', true).selectpicker('render');
            $currentRow.find(".addItemFilter").remove();
            $currentRow.find(".add-user").addClass('disabled');
            $currentRow.find("#itemSalesPersonVal").val($("select[name='itemSalesPerson']", '#productTable #itemWiseSalesPersonModal').val());
            if ($flag) {
                var $newRow = $($addRowSample.clone()).removeClass('sample hidden').appendTo($('#add-new-item-row', $productTable));
                setProductTypeahead();
                $newRow.find("#itemCode").focus();
                resetItemSalesPersonDrodown();
            }
        } else if ($(this).hasClass('save')) {
            var productId = $(this).parent().parent().data('id');
            var $currentRow = $thisRow;
            discountEditedManually[$thisRow.data('icid')] = true;

            var k = productId+'_'+$thisRow.data('proIncID');
            if (addedFreeIssueItems[k] !== undefined && !$.isEmptyObject(addedFreeIssueItems[k])) {
                if (oldQty != null && parseFloat(oldQty) != parseFloat($currentRow.find("input[name='deliverQuanity']").val())) {
                    p_notification(false, eb.getMessage('ERR_MAJOR_QUANTITY_CANNOT_CHANGE'));
                    return false;
                }
            }

            $currentRow
                    .removeClass('edit-row')
                    .find("#itemCode, input[name='deliverQuanity'],input[name='availableQuantity'],input[name='unitPrice'],input[name='discount'], input[name='uomqty']").prop('readonly', true).end()
                    .find('td').find('#addNewTax').attr('disabled', true)
                    .find("button.delete").removeClass('disabled');
            $currentRow.find("input[name='discount']").change();
            $currentRow.find("input[name='deliverQuanity']").change();
            $currentRow.find("input[name='unitPrice']").change();
            $currentRow.find(".add-user").addClass('disabled');
            //$currentRow.find('#deliveryNoteDiscount').trigger('focusout');
            invoiceRowEdit = false;
        } else { // if save button is clicked

            $thisRow.removeClass('edit-row');
            $thisRow.find("input[name='deliverQuanity']").prop('readonly', true);
        }

        $(this, $currentRow).parent().find('.edit').removeClass('hidden');
        $(this, $currentRow).parent().find('.add').addClass('hidden');
        $(this, $currentRow).parent().find('.save').addClass('hidden');
        $(this, $currentRow).parent().parent().find('.delete').removeClass('disabled');

        if (!$thisRow.hasClass('freeAddItem')) {
            $(this, $currentRow).parent().parent().find('.free-issue').removeClass('disabled');
        } 

        // if batch product modal is available for this product
        if ($thisRow.hasClass('subproducts')) {
            $('.edit-modal', $currentRow).parent().addClass('hidden');
            $("td[colspan]", $currentRow).attr('colspan', 2);
        }


        if ($thisRow.hasClass('freeAddItem')) {
            $thisRow.removeClass('add-free-issue');
            $(this, $currentRow).parent().find('.edit').addClass('disabled');
            $(this, $currentRow).parent().find('.free-issue').addClass('pppp');
        }

//append product increment ID to product ID
        if ($(this).hasClass('save')) {
            var saveProductID = $thisRow.data('id') + '_' + $thisRow.data('proIncID');
            deliverProducts[saveProductID] = thisVals;
        } else {
        	var increID = rowincrementID;
        	if($thisRow.data('proIncID')!== undefined){
        		increID = $thisRow.data('proIncID')
        	}
            thisVals = getCurrentProductData($thisRow);
            deliverProducts[thisVals.productID + '_' + increID] = thisVals;
            if(!$thisRow.hasClass('copied')){
            	$thisRow.data('proIncID', increID);
            	rowincrementID++;
        	}
        	$thisRow.removeClass('copied');
        }
        setTotalTax();
        setInvoiceTotalCost();
        delDiscountTrigger = false;
        $('#customCurrencyId').attr('disabled', true);
        $('#priceListId').attr('disabled', true);
        selectedId = null;
        if(copied){
 			$('#promotion').trigger('change');
        }
    });

    $('#productTable').on('blur', '.itemDescText', function(e) {
        e.preventDefault();
        $(this).addClass('hidden');
        $(this).parent().find('.addItemDescription').find('i').removeClass('fa-comment').addClass('fa-comment-o');
    });

    $productTable.on('click', 'button.edit', function(e) {
        invoiceRowEdit = true;
        var $thisRow = $(this).parents('tr');
        //re enable the item description
        $('.addItemDescription', $thisRow).prop('disabled', false);
        $('.itemDescText', $thisRow).prop('disabled', false);
        oldQty = $thisRow.find("input[name='deliverQuanity']").val();
        $thisRow.addClass('edit-row')
                .find("input[name='deliverQuanity'],input[name='unitPrice'],input[name='discount']").prop('readonly', false).end()
                .find('td').find('#addNewTax').attr('disabled', false)
                .find("button.delete").addClass('disabled');
        $thisRow.find("input[name='discount']").change();
        $thisRow.find("input[name='deliverQuanity']").change();
        $thisRow.find("input[name='unitPrice']").change();
        $thisRow.find(".add-user").removeClass('disabled');
        $(this, $thisRow).parent().find('.edit').addClass('hidden');
        $(this, $thisRow).parent().find('.save').removeClass('hidden');
        if ($thisRow.hasClass('subproducts')) {
            $("input[name='deliverQuanity']", $thisRow).prop('readonly', false).change();
            $('.edit-modal', $thisRow).parent().removeClass('hidden');
            $("td[colspan]", $thisRow).attr('colspan', 1);
        } else {
            if ($('#deliveryNoteNo').val() != '' || activityID != '' || jobID != '') {
                $("input[name='deliverQuanity']", $thisRow).prop('readonly', false).change();
            }
        }
    });

    $productTable.on('click', 'button.delete', function(e) {
        e.preventDefault();
        if (!$(this).hasClass('disabled')) {
            var $thisRow = $(this).parents('tr');

            var productID = $thisRow.data('id') + '_' + $thisRow.data('proIncID');
            var majorID = ($thisRow.data('majorFreeItemID')) ? $thisRow.data('majorFreeItemID') : null;
            var Icid = ($thisRow.data('freeIssueMajorIcid')) ? $thisRow.data('freeIssueMajorIcid') : null;


            if (addedFreeIssueItems[productID] !== undefined && !$.isEmptyObject(addedFreeIssueItems[productID])) {
                p_notification(false, eb.getMessage('CANNOT_REMOVE_ITEM_HAS_FREE_ISSUES'));
                return false;
            }


            var serial = $thisRow.find('button.edit-modal').data('selectedSids');

            if (serial) {
                for (var i in deliverSubProducts[productID]) {
                    if (serial.indexOf(deliverSubProducts[productID][i].serialID) != -1 || deliverSubProducts[productID][0].batchID != undefined ) {
                        delete deliverSubProducts[productID][i];
                        delete subProductsForCheck[$thisRow.data('id')][i];
                    }
                }
                delete deliverSubProducts[productID];
                // deliverProducts[productID]['deliverQuantity']['qty'] -= serial.length;
                // if (deliverProducts[productID]['deliverQuantity']['qty'] == 0) {
                    delete deliverProducts[productID];
                    checkSubProduct[productID] = undefined;
                // }
            } else {
                delete deliverProducts[productID];
                checkSubProduct[productID] = undefined;
            }

            if ($thisRow.hasClass('freeAddItem') && majorID != null && Icid != null) {
                var k = majorID+'_'+Icid;
                delete addedFreeIssueItems[k][$thisRow.data('id')];
            }

            // when all products deleted customcurrency symbal selection will be enable
            // if used any selection from 'start invoice by' then cann't enable
            if (_.size(deliverProducts) == 0 && !$('#deliveryNoteNo').val() && !$('#salesOrderNo').val() && !$('#quotationNo').val() && !$('#activityNo').val() && !$('#jobNo').val() && !$('#projectNo').val()) {
                $('#customCurrencyId').attr('disabled', false);
            }

            setInvoiceTotalCost();
            setTotalTax();
            $thisRow.remove();
            setItemViseTotal();
        }

    });

    $productTable.on('click', 'button.free-issue', function(e) {
        e.preventDefault();
        if (!$(this).hasClass('disabled')) {
            var $thisRow = $(this).parents('tr');
            var productKey = $thisRow.data('id') + '_' + $thisRow.data('proIncID');
            var productID = $thisRow.data('id');
            var icid = $thisRow.data('icid');

            freeIssueMajorProId = productID;
            freeIssueMajorIcid = icid;

            eb.ajax({
                type: 'POST',
                url: BASE_URL + '/productAPI/getFreeItemDetails',
                data: {
                    productID: productID,
                },
                success: function(respond) {
                    if (respond.status) {

                        var isApprovedTheCondition = false;

                        if (respond.data.majorConditionType == 1) {
                            if (parseFloat(deliverProducts[productKey].deliverQuantity.qty) > parseFloat(respond.data.majorQuantity)) {
                                isApprovedTheCondition = true;
                            }
                        } else if (respond.data.majorConditionType == 2) {
                            if (parseFloat(deliverProducts[productKey].deliverQuantity.qty) >= parseFloat(respond.data.majorQuantity)) {
                                isApprovedTheCondition = true;
                            }
                        } else if (respond.data.majorConditionType == 3) {

                            if (parseFloat(deliverProducts[productKey].deliverQuantity.qty) >= parseFloat(respond.data.majorQuantity)) {
                                var devideVal = parseFloat(deliverProducts[productKey].deliverQuantity.qty) / parseFloat(respond.data.majorQuantity);
                                var multiVal = null;
                                if ((devideVal - Math.floor(devideVal)) !== 0) {
                                    var devideArr = devideVal.toString().split(".");
                                    multiVal = parseFloat(devideArr[0]);
                                } else {
                                    multiVal = devideVal
                                }


                                $.each(respond.data.relatedFreeItems, function(index2, value2) {
                                    value2['quantity'] =  value2['quantity'] * multiVal;
                                });
                                isApprovedTheCondition = true;
                            }
                        }

                        if (isApprovedTheCondition) {
                            $thisRow.data('majorQuantity', respond.data.majorQuantity);
                            $('#freeIssueItemModal #freeIssueTable #freeIssueRow .relatedFreeItems').remove();
                            tempFreeIssueItemArray = [];
                            $.each(respond.data.relatedFreeItems, function(index1, value1) {
                                selectedItemFreeIssueDetails[value1['productID']] = value1;
                                loadNewRowForFreeIssueItems(value1, productID, icid);
                            });

                            $('#freeIssueItemModal').modal({backdrop: 'static', keyboard: false})  
                        } else {
                            p_notification(false, eb.getMessage('CANNOT_ISSUE_FREE_ITEMS'));
                        }
                        
                    } else {
                        p_notification(respond.status, respond.msg);
                    }

                }
            });

        }

    });

    $freeItemTable.on('click', '.addFreeItem', function(e) {
        var $thisRow = $(this).parents('tr');
        if(this.checked) {
            tempFreeIssueItemArray.push($thisRow.data('id'));
        } else {
            var index = tempFreeIssueItemArray.indexOf($thisRow.data('id'));
            if (index > -1) {
                tempFreeIssueItemArray.splice(index, 1);
            }
        }

    });


    $('#freeIssueItemModal').on('click', '#addFreeIssueItem', function(e){

        if ($.isEmptyObject(tempFreeIssueItemArray)) {
            p_notification(false, eb.getMessage('EMTY_SELECT_FREE_ISSUES'));
            return;
        }

        eb.ajax({
            type: 'POST',
            url: BASE_URL + '/productAPI/getFreeIssueItemsForSalesInvoice',
            data: {
                productIDs: tempFreeIssueItemArray,
                relatedFreeItems : selectedItemFreeIssueDetails
            },
            success: function(respond) {
                if (respond.status) {

                    if (respond.data.inactiveItemFlag) {
                        p_notification(false, respond.data.errorMsg);
                        setTimeout(function(){ 
                            window.location.assign(BASE_URL + "/invoice/create")
                        }, 3000);
                        return false;
                    }
                    
                    locationProducts = respond.data.locationProducts;

                    $.each(respond.data.locationProducts, function(index1, value1) {
                        currentProducts[value1['pID']] = value1;
                    });


                    $.each(respond.data.freeItemProduct, function(index, value) {
                        var $currentRow = getAddRow();
                        var $flag = true;
                        if (value.productType != 2 && value.freeItemProductQuantity <= 0 ) {
                            $flag = false;
                        }

                        if ($flag) {
                            $currentRow.find('#taxApplied').removeClass('glyphicon glyphicon-unchecked');
                            $currentRow.find('#taxApplied').addClass('glyphicon glyphicon-check');
                            $currentRow.find('.tempLi').remove();
                            

                            var mrpType = value.mrpType;
                            var mrpValue = value.mrpValue;
                            var mrpPercentage = value.mrpPercentageValue;
//product related data append in here
                            $currentRow.data('prodocuid', value.freeItemProductID);
                            $currentRow.data('locationid', value.locationID);
                            $currentRow.data('locationproductid', locationProducts[value.productID].lPID);
                            $currentRow.data('id', value.productID);
                            $currentRow.data('mrpType', mrpType);
                            $currentRow.data('mrpValue', mrpValue);
                            $currentRow.data('mrpPercentageValue', mrpPercentage);
                            $currentRow.data('icid',rowincrementID);
                            var rowIncID = rowincrementID;
                            $currentRow.data('proIncID',rowincrementID);
                            $currentRow.data('freeQty',value.freeItemProductQuantity);
                            $currentRow.data('majorFreeItemID',freeIssueMajorProId);
                            $currentRow.data('freeIssueMajorIcid',freeIssueMajorIcid);
                            $currentRow.data('selectedSids',[]);
                            $currentRow.addClass('freeAddItem');
                            $currentRow.addClass('add-free-issue');
                            $currentRow.data('stockupdate', true);
                            $currentRow.addClass('copied');
                            rowincrementID++;
                            $currentRow.attr('id', 'product' + value.productID);
                            $("#itemCode", $currentRow).empty();
                            $("#itemCode", $currentRow).
                                    append($("<option></option>").
                                            attr("value", value.productID).
                                            text(value.productName + '-' + value.productCode));
                            $("#itemCode", $currentRow).val(value.productID).prop('disabled', 'disabled');
                            $("#itemCode", $currentRow).data('PT', locationProducts[value.productID].pT);
                            $("#itemCode", $currentRow).data('PC', value.productCode);
                            $("#itemCode", $currentRow).data('PN', value.productName);
                            $("#itemCode", $currentRow).selectpicker('refresh');
                            $("input[name='availableQuantity']", $currentRow).parent().addClass('input-group');
                            $("input[name='deliverQuanity']", $currentRow).parent().addClass('input-group');
                            // $("input[name='deliverQuanity']", $currentRow).addUom(currentProducts[value.productID].uom);
                            $("input[name='availableQuantity']", $currentRow).val(currentProducts[value.productID].LPQ).prop('readonly', true).addUom(currentProducts[value.productID].uom);
                            $("input[name='unitPrice']", $currentRow).val(value.freeItemProductPrice).prop('readonly', true).addUomPrice(currentProducts[value.productID].uom);
                            $currentRow.find(".addItemFilter").remove();
                            value.deliveryNoteProductDiscount = value.deliveryNoteProductDiscount ? value.deliveryNoteProductDiscount : 0;
                            
                            
                            $("input[name='deliverQuanity']", $currentRow).trigger(jQuery.Event("focusout"));


                            if ($.isEmptyObject(currentProducts[value.productID].batch) &&
                                    $.isEmptyObject(currentProducts[value.productID].serial) &&
                                    $.isEmptyObject(currentProducts[value.productID].batchSerial)) {

                                $currentRow.removeClass('subproducts');
                                $('.edit-modal', $currentRow).parent().addClass('hidden');
                                $("td[colspan]", $currentRow).attr('colspan', 2);
                                $("input[name='deliverQuanity']", $currentRow)
                                        .val(value.freeItemProductQuantity)
                                        .addUom(currentProducts[value.productID].uom);
                            } else {
                                checkSubProduct[value.productID + "_" + rowIncID] = 0;
                                $currentRow.addClass('subproducts');
                                $("input[name='deliverQuantity']", $currentRow).prop('readonly', true).change();
                                $("input[name='deliverQuanity']", $currentRow)
                                        .val(value.freeItemProductQuantity)
                                        .prop('readonly', true)
                                        .addUom(currentProducts[value.productID].uom);
                                checkSubProduct[value.productID + "_" + rowIncID] = undefined;
                            }
                            
                            $currentRow.removeClass('add-row');
                            $currentRow.find("button.delete").removeClass('disabled');
                            var $newRow = $($addRowSample.clone()).removeClass('sample hidden').appendTo($('#add-new-item-row', $productTable));
                            $newRow.find("#itemCode").focus();

                        }

                        var key  = freeIssueMajorProId+'_'+freeIssueMajorIcid;


                        if (addedFreeIssueItems[key] === undefined) {
                            addedFreeIssueItems[key] = {}
                        };


                        addedFreeIssueItems[key][value.productID] = value;
                        addedFreeIssueItems[key][value.productID]['incVal'] = rowIncID;

             
                    });
                    
                    var $currentRow = getAddRow();
                    clearProductRow($currentRow);
                    setProductTypeahead();

                    $('#freeIssueItemModal').modal('hide');

                } else {
                    p_notification(respond.status, respond.msg);
                }

            }
        });

    });



    function batchModalValidate(e) {

        var productID = $('#addInvoiceProductsModal').data('id');
        var incid = $('#addInvoiceProductsModal').data('icid');
        var proInID = $('#addInvoiceProductsModal').data('proIncID');
        $('#addInvoiceProductsModal').data('id', '');
        var $thisParentRow = getAddRow(proInID);
        var thisVals = getCurrentProductData($thisParentRow);
        var $batchTable = $("#addInvoiceProductsModal .batch-table tbody");
        var qtyTotal = 0;
        var subProducts = [];
        var oldSerials = $('#addInvoiceProductsModal').data('selectedSIDs');
        var thisSelection = [];
        var deliveryUnitPrice;

        if ($thisParentRow.length > 1) {
            for (var row = 0; $thisParentRow.length > row; row++) {
                if (_.isEqual($('button.edit-modal', $thisParentRow[row]).data('selectedSids'), oldSerials)) {
                    var realParent = $thisParentRow[row];
                }
            }
            $thisParentRow = realParent;
        }


        $("input[name='deliverQuantity'], input[name='deliverQuantityCheck']:checked", $batchTable).each(function() {

            var $thisSubRow = $(this).parents('tr');
            var thisDeliverQuantityByBase = $(this).val();
            if ((thisDeliverQuantityByBase).trim() != "" && isNaN(parseFloat(thisDeliverQuantityByBase))) {
                p_notification(false, eb.getMessage('ERR_INVO_PROD_QUAN'));
                $(this).focus();
                return qtyTotal = false;
            }

            thisDeliverQuantityByBase = (isNaN(parseFloat(thisDeliverQuantityByBase))) ? 0 : parseFloat(thisDeliverQuantityByBase);
            // check if trasnfer qty is greater than available qty
            var thisAvailableQuantityByBase = $("input[name='availableQuantity']", $thisSubRow).val();
            thisAvailableQuantityByBase = (isNaN(parseFloat(thisAvailableQuantityByBase))) ? 0 : parseFloat(thisAvailableQuantityByBase);
            if (thisDeliverQuantityByBase > thisAvailableQuantityByBase) {
                p_notification(false, eb.getMessage('ERR_INVO_QUAN'));
                return qtyTotal = false;
                $(this).focus().select();
            }

             if(thisDeliverQuantityByBase != 0){
                deliveryUnitPrice = $("input[name='btPrice']", $thisSubRow).val();
            }

            var warranty = $thisSubRow.find('#warranty').val();
            if (isNaN(warranty)) {
                p_notification(false, eb.getMessage('ERR_INVO_WARRANTY_PERIOD_SHBE_NUM'));
                $thisSubRow.find('#warranty').focus().select();
                return qtyTotal = false;
            }
            if (selectedSerialID != null) {
                thisDeliverQuantityByBase = 1;
            }

            // if a product invoice is present, prepare array to be sent to backend
            if (thisDeliverQuantityByBase > 0) {
                var thisSubProduct = {};
                if ($(".batchCode", $thisSubRow).data('id')) {
                    thisSubProduct.batchID = $(".batchCode", $thisSubRow).data('id');
                }
                if ($(".serialID", $thisSubRow).data('id')) {
                    thisSubProduct.serialID = $(".serialID", $thisSubRow).data('id');
                }
                else if (selectedSerialID != null) {
                    thisSubProduct.serialID = selectedSerialID;
                    selectedSerialID = null;
                }

                thisSubProduct.qtyByBase = thisDeliverQuantityByBase;
                thisSubProduct.incrementID = incid;
                thisSubProduct.warranty = $thisSubRow.find('#warranty').val();
                let wrntyType=$thisSubRow.find('#warrantyType').val();
                if(wrntyType=="Years")
                 thisSubProduct.warrantyType =4;
                else if(wrntyType=="Months"){
                 thisSubProduct.warrantyType = 3;
                }else if(wrntyType=="Weeks"){
                    thisSubProduct.warrantyType = 2;
                }else{
                    thisSubProduct.warrantyType = 1;
                }


                if ($(this).attr('disabled') != 'disabled') {
                	if(thisSubProduct.serialID!==undefined){
	                    thisSelection.push(thisSubProduct.serialID);
                	}
                    subProducts.push($.extend({}, thisSubProduct));
                }
            }else{
            	if(deliverSubProducts[thisVals.productIncID] !== undefined){
            		var obj = _.findWhere(deliverSubProducts[thisVals.productIncID], {batchID: $(".batchCode", $thisSubRow).data('id')});
                    if(obj!==undefined){
                    	deliverSubProducts[thisVals.productIncID].splice(deliverSubProducts[thisVals.productIncID].indexOf(obj), 1);
                    }

            		if(subProductsForCheck[thisVals.productID] !== undefined){
            			var obj1 = _.findWhere(subProductsForCheck[thisVals.productID], {batchID: $(".batchCode", $thisSubRow).data('id'), incrementID: incid});
                    	if(obj1!==undefined){
                    		subProductsForCheck[thisVals.productID].splice(subProductsForCheck[thisVals.productID].indexOf(obj1), 1);

                    	}
            		}
            	}
            }

            //set total product quantity
            qtyTotal = qtyTotal + thisDeliverQuantityByBase;
            for (var old_sr in oldSerials) {
                if (deliverSubProducts[thisVals.productIncID]) {
                    var obj = _.findWhere(deliverSubProducts[thisVals.productIncID], {serialID: oldSerials[old_sr]});
                    deliverSubProducts[thisVals.productIncID].splice(deliverSubProducts[thisVals.productIncID].indexOf(obj), 1);
                }
                if (subProductsForCheck[thisVals.productID] && !(subProductsForCheck[thisVals.productID].serialID == '' || subProductsForCheck[thisVals.productID].serialID === undefined)) {
                    var obj1 = _.findWhere(subProductsForCheck[thisVals.productID], {serialID: oldSerials[old_sr], incrementID: incid });
                    subProductsForCheck[thisVals.productID].splice(subProductsForCheck[thisVals.productID].indexOf(obj1), 1);
                }
            }
        });

        if ($thisParentRow.hasClass('freeAddItem')) {
            if (thisSelection.length != 0) {
                if ($("input[name='deliverQuanity']", $thisParentRow).val() != thisSelection.length) {
                    p_notification(false, eb.getMessage('ERR_DELI_TOTAL_SUBQUAN'));
                    return false;
                }
            } else {
                if ($("input[name='deliverQuanity']", $thisParentRow).val() != qtyTotal) {
                    p_notification(false, eb.getMessage('ERR_DELI_TOTAL_SUBQUAN'));
                    return false;
                }
            }
        }

		$("input[name='deliverQuanity']", $thisParentRow).val(qtyTotal).change();

        // use to set unit price using batch price
        if(!copyFromDel){
            if(subProducts.length == 1 && subProducts[0].batchID != null && subProducts[0].serialID == null && deliveryUnitPrice != 0){
                $("input[name='unitPrice']", $thisParentRow).val(deliveryUnitPrice).change();
            } else if(subProducts[0].batchID != null && subProducts[0].serialID != null){
                var tempBatchIDs = [];
                var sameBatchFlag = true;
                $.each(subProducts, function(key, value){
                    tempBatchIDs[key] = value.batchID;
                });
                var tmpBtID;
                $.each(tempBatchIDs, function(key, value){

                    if(key == 0){
                        tmpBtID = value;
                    }
                    if(tmpBtID != value){
                        sameBatchFlag = false;
                    }
                });
                if(sameBatchFlag){
                    $("input[name='unitPrice']", $thisParentRow).val(deliveryUnitPrice).change();
                } else {
                    $("input[name='unitPrice']", $thisParentRow).val(defaultSellingPriceData[thisVals.productID]).change();
                }

            } else {
                $("input[name='unitPrice']", $thisParentRow).val(defaultSellingPriceData[thisVals.productID]).change();
            }
        }

        if ($thisParentRow.hasClass('freeAddItem')) {
            $("input[name='unitPrice']", $thisParentRow).val(0).change();
        }




        if (checkSubProduct[thisVals.productIncID] !== undefined) {
            if (qtyTotal != $("input[name='deliverQuanity']", $thisParentRow).val()) {
                p_notification(false, eb.getMessage('ERR_DELI_TOTAL_SUBQUAN'));
                return qtyTotal = false;
            } else {
                checkSubProduct[thisVals.productIncID] = 1;
            }
        }

        // to break out form $.each and exit function
        if (qtyTotal === false)
            return false;
        // ideally, since the individual batch/serial quantity is checked to be below the available qty,
        // the below condition should never become true
        if (thisVals.stockUpdate == true) {
            if (parseInt(qtyTotal) > parseInt(thisVals.availableQuantity.qty)) {
                p_notification(false, eb.getMessage('ERR_INVO_TOTAL_QUAN'));
                return false;
            }
        }

        for (var i in subProducts) {
            if (deliverSubProducts[thisVals.productIncID] === undefined) {
                deliverSubProducts[thisVals.productIncID] = new Array();
            }
            if (subProductsForCheck[thisVals.productID] === undefined) {
                subProductsForCheck[thisVals.productID] = new Array();
            }
             //obj is for get invoiceSubProducts object related to sub  Product Data
            //obj1 is for get subProductsForCheck object related to sub  Product Data
            if (subProducts[i].serialID == '' || subProducts[i].serialID === undefined) {
                var obj = _.findWhere(deliverSubProducts[thisVals.productIncID], {batchID: subProducts[i].batchID, incrementID: subProducts[i].incrementID});
                var obj1 = _.findWhere(subProductsForCheck[thisVals.productID], {batchID: subProducts[i].batchID, incrementID: subProducts[i].incrementID});
            } else {
                var obj = _.findWhere(deliverSubProducts[thisVals.productIncID], {serialID: subProducts[i].serialID, incrementID: subProducts[i].incrementID});
                var obj1 = _.findWhere(subProductsForCheck[thisVals.productID], {serialID: subProducts[i].serialID, incrementID: subProducts[i].incrementID});

            }
            if (obj === undefined) {
                deliverSubProducts[thisVals.productIncID].push(subProducts[i]);
            } else {
                deliverSubProducts[thisVals.productIncID][deliverSubProducts[thisVals.productIncID].indexOf(obj)].qtyByBase = subProducts[i].qtyByBase;
            }
            if (obj1 === undefined) {
                subProductsForCheck[thisVals.productID].push(subProducts[i]);
            } else {
                subProductsForCheck[thisVals.productID][subProductsForCheck[thisVals.productID].indexOf(obj1)].qtyByBase = subProducts[i].qtyByBase;
            }
        }

        SubProductsQty[thisVals.productIncID] = qtyTotal;
        var $transferQ = $("input[name='deliverQuanity']", $thisParentRow).prop('readonly', true);
        if (checkSubProduct[thisVals.productIncID] === undefined) {
            if (!$.isEmptyObject(subProducts[0]) && subProducts[0].serialID) {
                $transferQ.val(thisSelection.length).change();
            } else {
                $transferQ.val(qtyTotal).change();
            }
        }
        $('button.edit-modal', $thisParentRow).data('selectedSids', thisSelection);
        $("#unitPrice", $thisParentRow).trigger(jQuery.Event("focusout"));
        $("#itemUnitPrice", $thisParentRow).trigger(jQuery.Event("focusout"));
        return true;
    }


    $productTable.on('click', 'button.edit-modal', function(e) {
        var productID = $(this).parents('tr').data('id');
        var productincID = productID + "_" + $(this).parents('tr').data('proIncID');
        var rowincID = $(this).parents('tr').data('icid');
        var stockUpdate = $(this).parents('tr').data('stockupdate');
        var productCode = $(this).parents('tr').find('#itemCode').val();
        var selectedSIDs = ($(this).parents('tr').hasClass('freeAddItem') && $(this).data('selectedSids') == undefined) ? [] : $(this).data('selectedSids');

        $("#batch_data tr:not(.hidden)").remove();
        $('#addInvoiceProductsModal').data('icid', rowincID);
        $('#addInvoiceProductsModal').data('proIncID', rowincID);
        if (activityID != '' || jobID != '') {
            addSubProducts(productID, productCode, 0);
        } else if (deliveryNoteIDs != '' && stockUpdate == false) {
            addDeliveryNoteSubProduct(productincID, selectedSIDs);
        } else {
            addBatchProduct(productID, productCode, 0);
            addSerialProduct(productID, productCode, 0);
            addSerialBatchProduct(productID, productCode, 0);
        }

        //check this product serail or batch
        if (!$.isEmptyObject(currentProducts[productID].serial)) {
            $('#add-product-batch .serial-auto-select').removeClass('hidden');
            $('#numberOfRow').val('');
        } else {
            $('#add-product-batch .serial-auto-select').addClass('hidden');
        }
        $("tr", '#batch_data').each(function() {
            var $thisSubRow = $(this);
            for (var i in subProductsForCheck[productID]) {
                if (subProductsForCheck[productID][i].serialID !== undefined) {
                    if ($(".serialID", $thisSubRow).data('id') == subProductsForCheck[productID][i].serialID) {
                        $("input[name='deliverQuantityCheck']", $thisSubRow).prop('checked', true);
                        $("input[name='warranty']", $thisSubRow).val(subProductsForCheck[productID][i].warranty);
                        if (selectedSIDs && selectedSIDs.indexOf(subProductsForCheck[productID][i].serialID) == -1) {
                            $("input[name='deliverQuantityCheck']", $thisSubRow).attr('disabled', true);
                        }
                    }

                } else if (subProductsForCheck[productID][i].batchID !== undefined  && (rowincID == subProductsForCheck[productID][i].incrementID)) {
                    if ($(".batchCode", $thisSubRow).data('id') == subProductsForCheck[productID][i].batchID) {
                        $(this).find("input[name='deliverQuantity']").val(subProductsForCheck[productID][i].qtyByBase).change();
                    }
                }
            }
        });

        if (!$.isEmptyObject(deliverSubProducts[productincID])) {
            $('#addInvoiceProductsModal').data('id', $(this).parents('tr').data('id'));
        } else {
            if (salesOrderID != '' || activityID != '' || jobID != '' || quotationID != '' || deliveryNoteIDs != '') {
                $('#addInvoiceProductsModal').data('id', $(this).parents('tr').data('id'));
            }
        }
        $('#addInvoiceProductsModal').data('selectedSIDs', selectedSIDs);
        $('#addInvoiceProductsModal').modal('show');
        $('#search_key').val('').keyup();
        $('#addInvoiceProductsModal').unbind('hide.bs.modal');
    });

    $('#batch-save').on('click', function(e) {
        e.preventDefault();
        // validate batch / serial products before closing modal
        if (!batchModalValidate(e)) {
            e.stopPropagation();
            return false;
        } else {
            $('#addInvoiceProductsModal').modal('hide');
        }
    });
    $('.close').on('click', function() {
        $('#addInvoiceProductsModal').modal('hide');
    });
    $('#deliveryNoteForm').on('submit', function(e) {
        e.preventDefault();
        if ($("tr.edit-row", $productTable).length > 0) {
            p_notification(false, eb.getMessage('ERR_INVO_SAVE_ALLPROD'));
            return false;
        }
        if ($("tr.add-free-issue", $productTable).length > 0) {
            p_notification(false, eb.getMessage('ERR_INVO_SAVE_HAVE_UNADDED_ITEMS'));
            return false;
        }
        journalEntryData = {}
        saveInvoice(journalEntryData);
        return false;
    });

    $('table#deliveryNoteProductTable>tbody').on('keypress', 'input#itemQuantity, input#itemUnitPrice, input#deliveryNoteDiscount, #deliverQuanity, #availableQuantity ', function(e) {
        if (e.which == 13) {
            $(this).blur();
            $('button.add', $(this).parents('tr.add-row')).trigger('click');
            e.preventDefault();
        }
    });

    $('#deliveryNoteCancel').on('click', function(e) {
        e.preventDefault();
        window.location.reload();
    });

    $('#creditLimitValidationView').on('click','#exceed-creditLimit-approve-button', function() {
        var username = $('#username').val();
        var pswd = $('#password').val();
        if (username == '') {
            p_notification(false,eb.getMessage('ERR_NO_USNM'));
            return false; 
        } else if (pswd == '') {
            p_notification(false,eb.getMessage('ERR_NO_PSWD'));
            return false;
        } else {
            eb.ajax({
                type: 'POST',
                url: BASE_URL + '/invoice-api/validateUserNameAndPassword',
                data: {
                    username: username,
                    password: pswd
                },
                success: function(respond) {
                    if (respond.status) {
                        creditLimitApprovel = true;
                        $('#creditLimitValidationView').modal('hide');
                        saveInvoice(journalEntryData);
                    } else {
                        p_notification(false, respond.msg);
                        return false;
                    }                
                },
                async: false

            });
            
        }
       
    });
    function validateCreditLimit(customerID, totalValue) {
        eb.ajax({
                type: 'POST',
                url: BASE_URL + '/invoice-api/getCustomerValidateDetails',
                data: {
                    customerID: customerID,
                    invoiceAmount: totalValue
                },
                success: function(respond) {
                    if (respond.status) {
                        $('#creditLimitValidationView').modal('show');
                    } else {
                        creditLimitApprovel = true;
                    }                  
                },
                async: false

            });
    }

    function saveInvoice(journalEntryData) {
        for (var i in checkSubProduct) {
            var $currentRow = getAddRow(i);
            if (!$($currentRow).find('.edit-modal').parent('td').hasClass('hidden')) {
                if (checkSubProduct[i] == 0) {
                    p_notification(false, eb.getMessage('ERR_INVO_ADD_SUBPROD', deliverProducts[i].productCode));
                    return;
                }
            }
        }
        var deliveryAmount = $('#deliveryCharge').val();
        if (deliveryAmount == '') {
            deliveryAmount = 0;
        }

        var serviceChargeAmount = addedServiceChargeAmount;

        var addedServiceChargeArray = {};
        if (!$.isEmptyObject(finaleServiceChargeArr)) {
            addedServiceChargeArray = finaleServiceChargeArr;
        }


        var grandTotal = 0;
        for (var i in deliverProducts) {
            grandTotal += toFloat(deliverProducts[i].productTotal);
        }
        if ($('#tax_exempted').is(':checked')) {
            grandTotal = grandTotal - totalSuspendedTaxAmount;
        } 
        grandTotal += parseFloat(deliveryAmount);
        grandTotal += parseFloat(serviceChargeAmount);

        var invoiceTotalDisount = $('#total_discount_rate').val();
        if (invoiceTotalDisount == '') {
            invoiceTotalDisount = 0;
        }

        if ($('#disc_value').is(':checked')) {
            grandTotal -= parseFloat(invoiceTotalDisount);
        } 
        grandTotal -= parseFloat(promotionDiscountValueInvoicedBased);
        var formData = {invoiceCode: $('#invoiceNo').val(),
            location: $('#currentLocation').val(),
            date: $('#deliveryDate').val(),
            dueDate: $('#dueDate').val(),
            customer: $('#customer').text(),
            customerID: customerID,
            deliveryAddress: $('#deliveryNoteAddress').val()
        };
        
        var suspendedtax = $('#tax_exempted').is(':checked') ? 1 : 0;
        var showTax = 0;
        if ($('#showTax').is(':checked')) {
            showTax = 1;
        }
        // validate credit Limit
        if (!creditLimitApprovel) {
            validateCreditLimit(customerID,grandTotal);
        }

        var finalTotalWithInclusiveTax = null;
        if (inclusiveTax) {
            inclusiveTax = 1;
            finalTotalWithInclusiveTax = $('#finaltotal').text().replace(/,/g, '');
        }

        var isActiveWF = false;
        var wfID = null;

        var isActiveWF = $("#approvalWorkflowSelector").attr('data-isactive');
        if (isActiveWF && $('.approval-workflows').val() == "null") {
            p_notification(false, eb.getMessage('ERR_INV_APR_WF_EMPTY'));
            return;
        }
        var wfID = ($('.approval-workflows').val()) ? $('.approval-workflows').val() : null;

        var salesP = [];
        if (!$.isEmptyObject($('#salesPersonID').val())) {
            $.each($('#salesPersonID').val(), function(index1, val1) {
               
                if (val1 != "") {
                    salesP.push(val1);
                }

            });
        }


        if (validateTransferForm(formData) && creditLimitApprovel) {
            eb.ajax({
                type: 'POST',
                url: BASE_URL + '/invoice-api/saveInvoiceDetails',
                data: {
                    invoiceCode: formData.invoiceCode,
                    locationOutID: locationOut,
                    products: deliverProducts,
                    subProducts: deliverSubProducts,
                    invoiceDate: formData.date,
                    dueDate: $('#dueDate').val(),
                    customer: customerID,
                    customerProfID: cusProfID,
                    customerName: $('#customer').text(),
                    deliveryCharge: $('#deliveryCharge').val(),
                    invoiceTotalPrice: grandTotal,
                    invoiceComment: $('#comment').val(),
                    paymentTermID: $('#paymentTerm').val(),
                    invoiceDiscountType: $('input[name=discount_type]:checked').val(),
                    invoiceDiscountRate: $('#total_discount_rate').val(),
                    totalTaxValue: (totalTaxAmount - totalSuspendedTaxAmount),
                    susTaxAmount: totalSuspendedTaxAmount,
                    showTax: showTax,
                    finalTotalWithInclusiveTax: finalTotalWithInclusiveTax,
                    deliveryNoteID: deliveryNoteIDs,
                    salesOrderID: salesOrderID,
                    quotationID: quotationID,
                    activityID: activityID,
                    jobID: jobID,
                    projectID: projectID,
                    salesPersonID: (salesP.length == 0) ? null : salesP,
                    suspendedTax: suspendedtax,
                    deliveryAddress: $('#deliveryAddress').val(),
                    promotionID: promotionID,
                    promotionDiscountValue: promotionDiscountValue,
                    invoiceWisePromotionDiscountType: invoiceWisePromotionDiscountType,
                    customCurrencyId: $('#customCurrencyId').val(),
                    customCurrencyRate: customCurrencyRate,
                    priceListId: priceListId,
                    salesInvoiceDeliveryChargeEnable: ($('#deliveryChargeEnable').is(':checked')) ? '1' : '0',
                    journalEntryData:journalEntryData,
                    inclusiveTax:inclusiveTax,
                    dimensionData: dimensionData,
                    ignoreBudgetLimit : ignoreBudgetLimitFlag,
                    nextInvoiceDate : nextInvoiceDate,
                    nextInvoiceDateComment : nextInvoiceDateComment,
                    serviceChargeAmount: serviceChargeAmount,
                    addedServiceChargeArray: addedServiceChargeArray,
                    addedFreeIssueItems: addedFreeIssueItems,
                    isActiveWF: isActiveWF,
                    wfID: wfID,
                    linkedCusOrder: $('#cusOrderNum').val()

                },
                success: function(respond) {
                    if (respond.status) {
                        if (respond.data.isDraft) {
                            var fileInput = document.getElementById('invoiceFiles');
                            if(fileInput.files.length > 0) {
                                var form_data = false;
                                if (window.FormData) {
                                    form_data = new FormData();
                                }
                                form_data.append("documentID", respond.data.invID);
                                form_data.append("documentTypeID", 47);
                                form_data.append("draftInvoiceID", draftInvoiceID);
                                form_data.append("epttAttachemntFlag", epttAttachemntFlag);
                                
                                for (var i = 0; i < fileInput.files.length; i++) {
                                    form_data.append("files[]", fileInput.files[i]);
                                }

                                eb.ajax({
                                    url: BASE_URL + '/store-files',
                                    type: 'POST',
                                    processData: false,
                                    contentType: false,
                                    data: form_data,
                                    success: function(res) {
                                    }
                                });
                            }

                            documentPreview(BASE_URL + '/invoice/draftPreview/' + respond.data.invID, 'documentpreview', "/invoice-api/send-email", function($preview) {
                                $('iframe', $preview).bind('load', function() {
                                    $('#document_print').addClass('hidden');
                                    $('#prnt-via-pdf').addClass('hidden');
                                    $('#document_email').addClass('hidden');
                                    $('#document_sms').addClass('hidden');
                                    if (respond.data.approvers.length != 0) {
                                        $('iframe', $preview).unbind('load');
                                        sendEmailToApprover(respond.data.invID, respond.data.invCode, respond.data.approvers, respond.data.hashValue, true, $preview, function($preview) {
                                            $preview.on('hidden.bs.modal', function() {
                                                if (!$("#preview:visible").length) {
                                                    window.location.reload();
                                                }
                                            });
                                        }); 
                                    } else {
                                        $preview.on('hidden.bs.modal', function() {
                                            if (!$("#preview:visible").length) {
                                                window.location.reload();
                                            }
                                        });
                                    }

                                });
                            });
                        } else {
                            var fileInput = document.getElementById('invoiceFiles');
                            if(fileInput.files.length > 0) {
                                var form_data = false;
                                if (window.FormData) {
                                    form_data = new FormData();
                                }
                                form_data.append("documentID", respond.data.salesInvoiceID);
                                form_data.append("documentTypeID", 1);
                                form_data.append("draftInvoiceID", draftInvoiceID);
                                form_data.append("epttAttachemntFlag", epttAttachemntFlag);
                                
                                for (var i = 0; i < fileInput.files.length; i++) {
                                    form_data.append("files[]", fileInput.files[i]);
                                }

                                eb.ajax({
                                    url: BASE_URL + '/store-files',
                                    type: 'POST',
                                    processData: false,
                                    contentType: false,
                                    data: form_data,
                                    success: function(res) {
                                    }
                                });
                            } else if (epttAttachemntFlag) {
                                var form_data = false;
                                if (window.FormData) {
                                    form_data = new FormData();
                                }
                                form_data.append("documentID", respond.data.salesInvoiceID);
                                form_data.append("documentTypeID", 1);
                                form_data.append("draftInvoiceID", draftInvoiceID);
                                form_data.append("epttAttachemntFlag", epttAttachemntFlag);
                                
                                for (var i = 0; i < fileInput.files.length; i++) {
                                    form_data.append("files[]", fileInput.files[i]);
                                }

                                eb.ajax({
                                    url: BASE_URL + '/store-files',
                                    type: 'POST',
                                    processData: false,
                                    contentType: false,
                                    data: form_data,
                                    success: function(res) {
                                    }
                                });
                            }
                            $('#deliveryNoteSave').attr("disabled", true);
                            showInvoicePreview(BASE_URL + '/invoice/invoiceView/' + respond.data.salesInvoiceID);
                            p_notification(respond.status, respond.msg);
                        }
                        
                    } else {
                        if (respond.data == "NotifyBudgetLimit") {
                            bootbox.confirm(respond.msg+' ,Are you sure you want to save this invoice ?', function(result) {
                                if (result == true) {
                                    ignoreBudgetLimitFlag = true;
                                    saveInvoice();
                                } else {
                                    setTimeout(function(){ 
                                        location.reload();
                                    }, 3000);
                                }
                            });
                        } else {
                            $('#deliveryNoteSave').attr("disabled", false);
                            p_notification(respond.status, respond.msg);
                        }
                    }
                }
            });
        }
    }


    function sendEmailToApprover(invID, invCode, approvers, token, flag, preview = null, callback = null) {
        if (flag) {
            eb.ajax({
                type: 'POST',
                url: BASE_URL + '/invoice-api/sendApproverEmail',
                data: {
                    invID: invID,
                    invCode: invCode,
                    approvers: approvers,
                    token: token,
                },
                success: function(respond) {
                    if (respond.status == true) {
                        p_notification(respond.status, respond.msg);
                        if (callback != null) {
                            callback(preview);
                        } else {
                            var url = BASE_URL + '/invoice/list';
                            window.location.assign(url);
                        }
                    } else {
                        p_notification(respond.status, respond.msg);
                    }
                }

            });
        } else {
            callback(preview);
        }
    }


    $('table#deliveryNoteProductTable>tbody').on('keyup', '#deliverQuanity,#unitPrice,#deliveryNoteDiscount', function(e) {
        e.preventDefault();
        console.log(this.id);
        if ($(this).parents('tr').find('#itemCode').val() != '') {
            if ($(this).val().indexOf('.') > 0) {
                decimalPoints = $(this).val().split('.')[1].length;
            }
            if (this.id == 'unitPrice' && (!$.isNumeric($(this).parents('tr').find('#unitPrice').val()) || $(this).parents('tr').find('#unitPrice').val() < 0 || decimalPoints > 2)) {
                decimalPoints = 0;
                $(this).parents('tr').find('#unitPrice').val('');
            }
            if ((this.id == 'deliverQuanity') && (!$.isNumeric($(this).parents('tr').find('#deliverQuanity').val()) || $(this).parents('tr').find('#deliverQuanity').val() < 0)) {
                decimalPoints = 0;
                $(this).parents('tr').find('#deliverQuanity').val('');
            }
            if ((this.id == 'deliveryNoteDiscount')) {
                if ($(this).hasClass('precentage') && (!$.isNumeric($(this).parents('tr').find('#deliveryNoteDiscount').val()) || $(this).parents('tr').find('#deliveryNoteDiscount').val() < 0 || $(this).parents('tr').find('#deliveryNoteDiscount').val() > 100 || (decimalPoints > 2))) {
                    decimalPoints = 0;
                    $(this).parents('tr').find('#deliveryNoteDiscount').val('');
                } else {
                    decimalPoints = 0;
                }
            }
        }
    });
    $('#total_discount_rate').on('keyup', function() {
        if ($('#disc_value').is(":checked")) {
            if ((!$.isNumeric($(this).parents('tr').find('#total_discount_rate').val()) || $(this).parents('tr').find('#total_discount_rate').val() < 0)) {
                decimalPoints = 0;
                $(this).parents('tr').find('#total_discount_rate').val('');
            }
        } else {
            if ((!$.isNumeric($(this).parents('tr').find('#total_discount_rate').val()) || $(this).parents('tr').find('#total_discount_rate').val() < 0 || $(this).parents('tr').find('#total_discount_rate').val() > 100)) {
                decimalPoints = 0;
                $(this).parents('tr').find('#total_discount_rate').val('');
            }
        }

        setItemViseTotal();

    });
    function calculateItemTotal(thisRow, productID) {
        var thisRow = thisRow;
        var productID = productID;
        var checkedTaxes = Array();
        thisRow.find('input:checkbox.addNewTaxCheck').each(function() {
            if (this.checked) {
                var cliTID = this.id;
                var tTaxID = cliTID.split('_')[1];
                checkedTaxes.push(tTaxID);
            }
        });
        var productType = thisRow.find('#itemCode').data('PT');
        var tmpItemQuentity = thisRow.find('#deliverQuanity').val();
        if (productType == 2 && tmpItemQuentity == 0) {
            tmpItemQuentity = 1;
        }
        var tmpItemTotal = (toFloat(thisRow.find('#unitPrice').val()) * toFloat(tmpItemQuentity));
        var tmpItemCost = tmpItemTotal;
        var prompItemQtyFlag = false;
        triggeredByPromotion = false;
        var prompItemMaxQty = 0;
        var failPromo = false;
        //check promotion Discounts
        if (!$.isEmptyObject(promotions)) {
            if  (promotions.promoType == 1) {
                for (var j = 0; j < promotions.combinations.length; ++j) {
                    if (promotions.combinations[j]['relatedProductIds'].includes(productID)){
                        if (promotions.combinations[j]['conditionType'] == "AND") {
                            var trueRules = 0;
                            for (var k = 0; k < promotions.combinations[j]['ruleSet'].length; ++k) {
                                $.each(deliverProducts, function(index, value) {
                                    if (promotions.combinations[j]['ruleSet'][k]['productID'] == value['productID']) {
                                        var cartQty = Number(value['deliverQuantity']['qty']);
                                        var ruleQty = Number(promotions.combinations[j]['ruleSet'][k]['quantity']);
                                        
                                        switch (promotions.combinations[j]['ruleSet'][k]['rule']) {
                                            case "equal":
                                              if (cartQty == ruleQty) {
                                                  trueRules = trueRules + 1;
                                              }
                                              break;
                                            case "greater than or equal":
                                              if (cartQty >= ruleQty) {
                                                  trueRules = trueRules + 1;
                                              }
                                              break;
                                            case "less than or equal":
                                              if (cartQty <= ruleQty) {
                                                  trueRules = trueRules + 1;
                                              }
                                              break;
                                            case "greater than":
                                              if (cartQty > ruleQty) {
                                                  trueRules = trueRules + 1;
                                              }
                                              break;
                                            case "less than":
                                              if (cartQty < ruleQty) {
                                                  trueRules = trueRules + 1;
                                              }
                                              break;
                                            case "not equal":
                                              if (cartQty != ruleQty) {
                                                  trueRules = trueRules + 1;
                                              }
                                              break;
                                           
                                            default:
                                              // code...
                                              break;
                                        }
                                    }
                                    
                                });
                                // for (var l = 0; l < deliverProducts.length; ++l) {
                                //     console.log(deliverProducts[l]);
                                // }
                            }
                            if (trueRules == promotions.combinations[j]['ruleSet'].length) {
                                promotionProducts[productID] = productID;
                                currentProducts[productID]['dEL'] = 1;
                                // discount type is value
                                if (promotions.combinations[j]['discountType'] == 1) {
                                    // var tempDis = qty * $scope.promotion.combinations[j]['discountAmount'];

                                    // discount = discount + tempDis;
                                    thisRow.find('#deliveryNoteDiscount').val(promotions.combinations[j]['discountAmount']);
                                    thisRow.find('#deliveryNoteDiscount').addClass('value');
                                    if (thisRow.find('#deliveryNoteDiscount').hasClass('precentage')) {
                                        thisRow.find('#deliveryNoteDiscount').removeClass('precentage');
                                    }
                                    thisRow.find('.sign').text(companyCurrencySymbol);


                                }

                                if (promotions.combinations[j]['discountType'] == 2) {
                                    // var tempDis = ((price * $scope.promotion.combinations[j]['discountAmount']) / 100 ) *  qty;

                                    // discount = discount + tempDis;
                                    thisRow.find('#deliveryNoteDiscount').val(promotions.combinations[j]['discountAmount']);
                                    thisRow.find('#deliveryNoteDiscount').addClass('precentage');
                                    if (thisRow.find('#deliveryNoteDiscount').hasClass('value')) {
                                        thisRow.find('#deliveryNoteDiscount').removeClass('value');
                                    }
                                    thisRow.find('.sign').text('%');

                                }
                            } else {
                                failPromo = true;
                            }
                        } else if (promotions.combinations[j]['conditionType'] == "OR") {
                            var isAllow = false;
                            for (var m = 0; m < promotions.combinations[j]['ruleSet'].length; ++m) {
                                if (productID == promotions.combinations[j]['ruleSet'][m]['productID']) {
                                    let cartQty = Number(tmpItemQuentity);
                                    let ruleQty = Number(promotions.combinations[j]['ruleSet'][m]['quantity']);

                                    switch (promotions.combinations[j]['ruleSet'][m]['rule']) {
                                        case "equal":
                                          if (cartQty == ruleQty) {
                                            isAllow = true;
                                          }
                                          break;
                                        case "greater than or equal":
                                          if (cartQty >= ruleQty) {
                                            isAllow = true;
                                          }
                                          break;
                                        case "less than or equal":
                                          if (cartQty <= ruleQty) {
                                            isAllow = true;
                                          }
                                          break;
                                        case "greater than":
                                          if (cartQty > ruleQty) {
                                            isAllow = true;
                                          }
                                          break;
                                        case "less than":
                                          if (cartQty < ruleQty) {
                                            isAllow = true;
                                          }
                                          break;
                                        case "not equal":
                                          if (cartQty != ruleQty) {
                                            isAllow = true;
                                          }
                                          break;
                                       
                                        default:
                                          // code...
                                          break;
                                    }
                                }
                            }

                            if (isAllow) {
                                promotionProducts[productID] = productID;
                                currentProducts[productID]['dEL'] = 1;
                              //discount type is value
                                if (promotions.combinations[j]['discountType'] == 1) {
                                    // var tempDis = qty * $scope.promotion.combinations[j]['discountAmount'];

                                    // discount = discount + tempDis;
                                    thisRow.find('#deliveryNoteDiscount').val(promotions.combinations[j]['discountAmount']);
                                    thisRow.find('#deliveryNoteDiscount').addClass('value');
                                    if (thisRow.find('#deliveryNoteDiscount').hasClass('precentage')) {
                                        thisRow.find('#deliveryNoteDiscount').removeClass('precentage');
                                    }
                                    thisRow.find('.sign').text(companyCurrencySymbol);

                                }

                                if (promotions.combinations[j]['discountType'] == 2) {
                                    // var tempDis = ((price * $scope.promotion.combinations[j]['discountAmount']) / 100 ) *  qty;

                                    // discount = discount + tempDis;
                                    thisRow.find('#deliveryNoteDiscount').val(promotions.combinations[j]['discountAmount']);
                                    thisRow.find('#deliveryNoteDiscount').addClass('precentage');
                                    if (thisRow.find('#deliveryNoteDiscount').hasClass('value')) {
                                        thisRow.find('#deliveryNoteDiscount').removeClass('value');
                                    }
                                    thisRow.find('.sign').text('%');

                                }
                            } else {
                                failPromo = true;
                            }   
                        }
                    } else {
                        failPromo = true;
                    }
                }
            } else if (promotions.promoType == 3) {

                for (var j = 0; j < promotions.combinations.length; ++j) {
                    if (promotions.combinations[j]['relatedProductIds'].includes(productID)){ 
                        if (promotions.combinations[j]['conditionType'] == "AND") {
                            var isAllow = false;
                            var qtyRuleCount = 0;
                            var trueCount = 0;
                            for (var k = 0; k < promotions.combinations[j]['ruleSet'].length; ++k) {
                                if (promotions.combinations[j]['ruleSet'][k]['attributeID'] == 0 || promotions.combinations[j]['ruleSet'][k]['attributeID'] == null) {
                                    qtyRuleCount = qtyRuleCount +1;
                                    let cartQty = Number(tmpItemQuentity);
                                    let ruleQty = Number(promotions.combinations[j]['ruleSet'][k]['quantity']);

                                    switch (promotions.combinations[j]['ruleSet'][k]['rule']) {
                                        case "equal":
                                          if (cartQty == ruleQty) {
                                              trueCount = trueCount + 1;
                                          }
                                          break;
                                        case "greater than or equal":
                                          if (cartQty >= ruleQty) {
                                              trueCount = trueCount + 1;
                                          }
                                          break;
                                        case "less than or equal":
                                          if (cartQty <= ruleQty) {
                                              trueCount = trueCount + 1;
                                          }
                                          break;
                                        case "greater than":
                                          if (cartQty > ruleQty) {
                                              trueCount = trueCount + 1;
                                          }
                                          break;
                                        case "less than":
                                          if (cartQty < ruleQty) {
                                              trueCount = trueCount + 1;
                                          }
                                          break;
                                        case "not equal":
                                          if (cartQty != ruleQty) {
                                              trueCount = trueCount + 1;
                                          }
                                          break;
                                       
                                        default:
                                          // code...
                                          break;
                                    }
                                }
                            }
                            if (qtyRuleCount == trueCount) {
                              isAllow = true;
                            } else {
                              isAllow = false;
                            }

                            if (isAllow) {
                                promotionProducts[productID] = productID;
                                currentProducts[productID]['dEL'] = 1;
                                //discount type is value
                                if (promotions.combinations[j]['discountType'] == 1) {
                                    // var tempDis = qty *  $scope.promotion.combinations[j]['discountAmount'];

                                    // discount = discount + tempDis;
                                    thisRow.find('#deliveryNoteDiscount').val(promotions.combinations[j]['discountAmount']);
                                    thisRow.find('#deliveryNoteDiscount').addClass('value');
                                    if (thisRow.find('#deliveryNoteDiscount').hasClass('precentage')) {
                                        thisRow.find('#deliveryNoteDiscount').removeClass('precentage');
                                    }
                                    thisRow.find('.sign').text(companyCurrencySymbol);

                                }

                                if (promotions.combinations[j]['discountType'] == 2) {
                                    // var tempDis = ((price * $scope.promotion.combinations[j]['discountAmount']) / 100 ) * qty;

                                    // discount = discount + tempDis;
                                    thisRow.find('#deliveryNoteDiscount').val(promotions.combinations[j]['discountAmount']);
                                    thisRow.find('#deliveryNoteDiscount').addClass('precentage');
                                    if (thisRow.find('#deliveryNoteDiscount').hasClass('value')) {
                                        thisRow.find('#deliveryNoteDiscount').removeClass('value');
                                    }
                                    thisRow.find('.sign').text('%');

                                }
                            } else {
                                failPromo = true;
                            }
                        }
                        else if (promotions.combinations[j]['conditionType'] == "OR") {
                            promotionProducts[productID] = productID;
                            currentProducts[productID]['dEL'] = 1;
                            //discount type is value
                            if (promotions.combinations[j]['discountType'] == 1) {
                              // var tempDis = qty * $scope.promotion.combinations[j]['discountAmount'];

                              // discount = discount + tempDis;
                                thisRow.find('#deliveryNoteDiscount').val(promotions.combinations[j]['discountAmount']);
                                thisRow.find('#deliveryNoteDiscount').addClass('value');
                                if (thisRow.find('#deliveryNoteDiscount').hasClass('precentage')) {
                                    thisRow.find('#deliveryNoteDiscount').removeClass('precentage');
                                }
                                thisRow.find('.sign').text(companyCurrencySymbol);

                            }

                            if (promotions.combinations[j]['discountType'] == 2) {
                                // var tempDis = ((price * $scope.promotion.combinations[j]['discountAmount']) / 100 ) *  qty;

                                // discount = discount + tempDis;
                                thisRow.find('#deliveryNoteDiscount').val(promotions.combinations[j]['discountAmount']);
                                thisRow.find('#deliveryNoteDiscount').addClass('precentage');
                                if (thisRow.find('#deliveryNoteDiscount').hasClass('value')) {
                                    thisRow.find('#deliveryNoteDiscount').removeClass('value');
                                }
                                thisRow.find('.sign').text('%');

                            }
                        }
                    } else {
                        failPromo = true;
                    }
                }

            } else {
                failPromo = true;
            }

            if (failPromo) {
                if (currentProducts[productID].dEL == 1) {
                    if (currentProducts[productID].dV != 0) {
                        thisRow.find('#deliveryNoteDiscount').val(currentProducts[productID].dV);
                        thisRow.find('#deliveryNoteDiscount').addClass('value');
                        if (thisRow.find('#deliveryNoteDiscount').hasClass('precentage')) {
                            thisRow.find('#deliveryNoteDiscount').removeClass('precentage');
                        }
                        thisRow.find('.sign').text(companyCurrencySymbol);
                    } else {
                        thisRow.find('#deliveryNoteDiscount').val(currentProducts[productID].dPR);
                        thisRow.find('#deliveryNoteDiscount').addClass('precentage');
                        if (thisRow.find('#deliveryNoteDiscount').hasClass('value')) {
                            thisRow.find('#deliveryNoteDiscount').removeClass('value');
                        }
                        thisRow.find('.sign').text('%');
                    }
                } else {
                    thisRow.find('#deliveryNoteDiscount').prop('readonly', true);
                }
            }


            // if (tmpItemQuentity >= promotionProducts[productID].minQty) {
            //     // if (promotionProducts[productID].discountType == 1) {
            //     //     thisRow.find('#deliveryNoteDiscount').val(promotionProducts[productID].discountAmount);
            //     //     thisRow.find('#deliveryNoteDiscount').addClass('value');
            //     //     if (thisRow.find('#deliveryNoteDiscount').hasClass('precentage')) {
            //     //         thisRow.find('#deliveryNoteDiscount').removeClass('precentage');
            //     //     }
            //     //     thisRow.find('.sign').text(companyCurrencySymbol);
            //     // } else {
            //     //     thisRow.find('#deliveryNoteDiscount').val(promotionProducts[productID].discountAmount);
            //     //     thisRow.find('#deliveryNoteDiscount').addClass('precentage');
            //     //     if (thisRow.find('#deliveryNoteDiscount').hasClass('value')) {
            //     //         thisRow.find('#deliveryNoteDiscount').removeClass('value');
            //     //     }
            //     //     thisRow.find('.sign').text('%');
            //     // }
            // } else {
            //     if (currentProducts[productID].dEL == 1) {
            //         if (currentProducts[productID].dV != 0) {
            //             thisRow.find('#deliveryNoteDiscount').val(currentProducts[productID].dV);
            //             thisRow.find('#deliveryNoteDiscount').addClass('value');
            //             if (thisRow.find('#deliveryNoteDiscount').hasClass('precentage')) {
            //                 thisRow.find('#deliveryNoteDiscount').removeClass('precentage');
            //             }
            //             thisRow.find('.sign').text(companyCurrencySymbol);
            //         } else {
            //             thisRow.find('#deliveryNoteDiscount').val(currentProducts[productID].dPR);
            //             thisRow.find('#deliveryNoteDiscount').addClass('precentage');
            //             if (thisRow.find('#deliveryNoteDiscount').hasClass('value')) {
            //                 thisRow.find('#deliveryNoteDiscount').removeClass('value');
            //             }
            //             thisRow.find('.sign').text('%');
            //         }
            //     } else {
            //         thisRow.find('#deliveryNoteDiscount').prop('readonly', true);
            //     }
            // }

            // if (toFloat(promotionProducts[productID].maxQty) <= toFloat(tmpItemQuentity)) {
            //     prompItemMaxQty = promotionProducts[productID].maxQty;
            //     prompItemQtyFlag = true;
            // }
        } else if (priceListItems[productID] !== undefined && parseFloat(priceListItems[productID].itemPrice) > 0) {

        } else {
            if (currentProducts[productID].dEL == 1 && (currentProducts[productID].dV != null || currentProducts[productID].dPR != null)) {
                //current product dsiscount (May changed due to insertion of promotion but there's a validation below)
                var currentDiscount = 0;
                if (thisRow.hasClass('add-row') || (discountEditedManually[thisRow.data('icid')] !== undefined && discountEditedManually[thisRow.data('icid')] == true) || thisRow.hasClass('copied') ||  invoiceRowEdit || invoiceTotalDiscountTrigger || deliveryCEnable || delDiscountTrigger || invBasePromotionDiscount) {
                    currentDiscount = thisRow.find('#deliveryNoteDiscount').val();
                    discountEditedManually[thisRow.data('icid')] = false;
                } else {
                    if (currentProducts[productID].dV != 0) {
                        currentDiscount = currentProducts[productID].dV;
                    } else {
                        currentDiscount = currentProducts[productID].dPR;
                    }
                }
                if (currentProducts[productID].dV != null) {
                    thisRow.find('#deliveryNoteDiscount').val(currentDiscount);
                    thisRow.find('#deliveryNoteDiscount').addClass('value');
                    if (thisRow.find('#deliveryNoteDiscount').hasClass('precentage')) {
                        thisRow.find('#deliveryNoteDiscount').removeClass('precentage');
                    }
                    thisRow.find('.sign').text(companyCurrencySymbol);
                } 

                if (currentProducts[productID].dPR != null){
                    thisRow.find('#deliveryNoteDiscount').val(currentDiscount);
                    thisRow.find('#deliveryNoteDiscount').addClass('precentage');
                    if (thisRow.find('#deliveryNoteDiscount').hasClass('value')) {
                        thisRow.find('#deliveryNoteDiscount').removeClass('value');
                    }
                    thisRow.find('.sign').text('%');
                } 
            } else {
                thisRow.find('#deliveryNoteDiscount').prop('readonly', true);
            }
        }

        if (thisRow.find('#deliveryNoteDiscount').hasClass('value')) {
            var tmpPDiscountvalue = isNaN(thisRow.find('#deliveryNoteDiscount').val()) ? 0 : thisRow.find('#deliveryNoteDiscount').val();
            if (tmpPDiscountvalue > 0) {
                var isSchemeDiscount = false;
                var allocatedSchemeDiscount = null;
                if (thisRow.hasClass('discountSchemeApplied')) {
                    isSchemeDiscount = true;
                    allocatedSchemeDiscount = thisRow.data('discountSchemeVal');
                }

                var validatedDiscount = validateDiscount(productID, 'val', tmpPDiscountvalue, thisRow.find('#unitPrice').val(),isSchemeDiscount, allocatedSchemeDiscount);
                thisRow.find('#deliveryNoteDiscount').val(validatedDiscount.toFixed(2));
                //check max quantity of the promotion and apply max qty discount only
                if (prompItemQtyFlag) {
                    tmpItemCost -= prompItemMaxQty * validatedDiscount;
                } else {
                    tmpItemCost -= tmpItemQuentity * validatedDiscount;
                }
            }
        } else {
            var isSchemeDiscount = false;
            var allocatedSchemeDiscount = null;
            if (thisRow.hasClass('discountSchemeApplied')) {
                isSchemeDiscount = true;
                allocatedSchemeDiscount = thisRow.data('discountSchemeVal');
            }
            var tmpPDiscount = isNaN(thisRow.find('#deliveryNoteDiscount').val()) ? 0 : thisRow.find('#deliveryNoteDiscount').val();
            if (tmpPDiscount > 0) {
                var validatedDiscount = validateDiscount(productID, 'per', tmpPDiscount, tmpItemQuentity, isSchemeDiscount, allocatedSchemeDiscount);
                thisRow.find('#deliveryNoteDiscount').val(validatedDiscount.toFixed(2));
                if (prompItemQtyFlag) {
                    tmpItemCost -= ((tmpItemTotal * toFloat(validatedDiscount) / toFloat(100)) / tmpItemQuentity) * prompItemMaxQty;
                } else {
                    tmpItemCost -= (tmpItemTotal * toFloat(validatedDiscount) / toFloat(100));
                }
            }
        }
        if ($('#total_discount_rate').val() != '' && $('input[name=discount_type]:checked').val() == 'presentage') {
            var invoiceDiscountValue = $('#total_discount_rate').val();
            tmpItemCost -= (tmpItemCost * (toFloat(invoiceDiscountValue) / toFloat(100)));
        }
        var invoiceWisePromotionDiscountValue = 0.00;
        if (counterForPromotion == 0) {
            invoiceTotalValueforPromotion = invoiceTotalValue;
        }
        if (!$.isEmptyObject(promotions) && promotions.promoType == 2 && promotions.discountType == 2) {
            if (toFloat(promotions.minValue) <= toFloat(invoiceTotalValue) && toFloat(invoiceTotalValue) <= toFloat(promotions.maxValue)) {
                invoiceWisePromotionDiscountValue = toFloat(tmpItemCost) * (toFloat(promotions.discountAmount) / toFloat(100));
            } else if (toFloat(invoiceTotalValue) > toFloat(promotions.maxValue)) {
                //if invoice total value is greater than the promotion discount precentage, calculate the discount only for promotion max value
                invoiceWisePromotionDiscountValue = ((toFloat(tmpItemCost)) / 100) * (toFloat(promotions.maxValue) * toFloat(promotions.discountAmount) / toFloat(invoiceTotalValueforPromotion));
                counterForPromotion++;
            }
            promotionDiscountValue = promotions.discountAmount;
            invoiceWisePromotionDiscountType = promotions.discountType;
        }
        tmpItemCost = tmpItemCost - invoiceWisePromotionDiscountValue;
        if (checkedTaxes) {
            currentItemTaxResults = calculateItemCustomTax(tmpItemCost, checkedTaxes);
            currentTaxAmount = (currentItemTaxResults == null) ? null : currentItemTaxResults.tTA;
        } else {
            currentTaxAmount = 0;
            currentItemTaxResults = '';
        }
        var itemCost = toFloat(tmpItemCost) + toFloat(currentTaxAmount);
        productsTotal[productID] = itemCost;
        productsTax[productID] = currentItemTaxResults;
        //if unit price is zero or discount greater than unit price,Then total may be negative value
        //So Total should be zero

        thisRow.find('#addNewTotal').html(accounting.formatMoney(itemCost));
        if (productsTax[productID] != null) {
            if (jQuery.isEmptyObject(productsTax[productID].tL)) {
                thisRow.find('#taxApplied').addClass('glyphicon glyphicon-unchecked');
            } else {
                thisRow.find('#taxApplied').removeClass('glyphicon glyphicon-unchecked');
                thisRow.find('#taxApplied').addClass('glyphicon glyphicon-check');
            }
        }
        if (!$.isEmptyObject(promotions)) {
            if (!thisRow.hasClass('add-row')) {
                if (!invoiceRowEdit) {
                    thisRow.find('button.save').trigger('click');
                }
            }
        }
        if (cancelPromotionFlag == true) {
            if (!thisRow.hasClass('add-row') && !thisRow.hasClass('freeAddItem')) {
                if (!invoiceRowEdit) {
                    thisRow.find('button.save').trigger('click');
                }
            }
        }
    }

    $productTable.on('focusout', '#deliverQuanity,#unitPrice,#deliveryNoteDiscount,.uomqty,.uomPrice', function() {
        var $thisRow = $(this).parents('tr');
        var productID = $thisRow.data('id');

        if ($(this).hasClass('uomqty') || $(this).attr('id') == 'deliverQuanity') {
            if (Object.keys(deliveryNoteIDs).length == 0) {
                applyDiscountScheme($thisRow, productID);
            }
        }

        if (inclusiveTax) {
            calculateItemTotalForInclusiveTax($thisRow, productID);
        } else {
            calculateItemTotal($thisRow, productID);
        }
    });

    function applyDiscountScheme(thisRow, productID) {

        if (currentProducts[productID].dEL == 1 && currentProducts[productID].useDisScheme == 1) {
            var tmpItemQuentity = thisRow.find('#deliverQuanity').val();

            var discountVal = 0;
            var discountType = null;
            $(currentProducts[productID].schemeConditions).each(function(index, schemValue) {
                discountType = schemValue['discountType'];
                if (parseFloat(schemValue['minQty']) <= parseFloat(tmpItemQuentity) && parseFloat(tmpItemQuentity) < parseFloat(schemValue['maxQty'])) {
                    if (schemValue['discountType'] == "percentage") {
                        discountVal = parseFloat(schemValue['discountPercentage']);
                    } else if (schemValue['discountType'] == "value") {
                        discountVal = parseFloat(schemValue['discountValue']);
                    }
                }
                
            });

            var conversion = $(".discount-div .uomPrice", thisRow).siblings('.uom-price-select').find('.selected').data('uc');
          
            thisRow.addClass('discountSchemeApplied');
            if (discountType == 'value') {
                productDiscountValue = parseFloat(discountVal).toFixed(2);
                thisRow.find('#deliveryNoteDiscount').val(productDiscountValue);
                thisRow.find('.discount-div .uomPrice').val(productDiscountValue * parseFloat(conversion));
                thisRow.find('#deliveryNoteDiscount').addClass('value');
                if (thisRow.find('#deliveryNoteDiscount').hasClass('precentage')) {
                    thisRow.find('#deliveryNoteDiscount').removeClass('precentage');
                }
                thisRow.find('.sign').text(companyCurrencySymbol);
            } else if (discountType == 'percentage'){
                productDiscountPrecentage = parseFloat(discountVal).toFixed(2);
                thisRow.find('#deliveryNoteDiscount').val(productDiscountPrecentage);
                thisRow.find('.discount-div .uomPrice').val(productDiscountPrecentage * parseFloat(conversion));
                thisRow.find('#deliveryNoteDiscount').addClass('precentage');
                if (thisRow.find('#deliveryNoteDiscount').hasClass('value')) {
                    thisRow.find('#deliveryNoteDiscount').removeClass('value');
                }
                thisRow.find('.sign').text('%');
            }
            thisRow.data('discountSchemeVal', discountVal);
            
        }
    }

    $('#attributeType', '#productTable #searchFilterModal').on('change', function() {
        if ($(this).val() != '0') {
            selectedAttrTypeID = $(this).val();
            selectedAttrValID = null;
            eb.ajax({
                type: 'POST',
                url: BASE_URL + '/api/settings/item-attribute/getItemAttributeRelatedValues',
                data: {attributeID: $(this).val()},
                success: function(respond) {
                    $(respond.data).each(function(index, dimValue) {
                        $('#productTable #searchFilterModal #attributeValDiv #attributeVal').empty();
                        $('#productTable #searchFilterModal #attributeValDiv #attributeVal').append($("<option>", {value: 0, html: 'Select Value'}))
                        $.each(dimValue, function(ind, val) {
                               $('#productTable #searchFilterModal #attributeValDiv #attributeVal').append($("<option>", {value: ind, html: val.description}));
                        });
                    });
                    $('#productTable #searchFilterModal #attributeValDiv #attributeVal').selectpicker('refresh');
                }
            });
        }
    });

    $('#attributeVal', '#productTable #searchFilterModal').on('change', function() {
        if ($(this).val() != '0') {
            selectedAttrValID = $(this).val();
        }
    });

    $('#close', '#productTable #searchFilterModal').on('click', function(){
        if (selectedAttrTypeID != null && selectedAttrValID == null) {
            p_notification(false, eb.getMessage('ERR_ITM_ATTRVAL'));
            return;
        } else {
            var vv = getAddRow();
            var $newRow = $($addRowSample.clone()).removeClass('sample hidden').appendTo($('#add-new-item-row', $productTable));
            setProductTypeahead();
            $newRow.find("#itemCode").focus();
            
            vv.remove();

            // setProductTypeahead();
            $('#searchFilterModal').modal('hide');
        }

    });

    $('#add-person', '#productTable #itemWiseSalesPersonModal').on('click', function(){
        currentSelectedRow.find('#itemSalesPersonVal').val($("select[name='itemSalesPerson']", '#productTable #itemWiseSalesPersonModal').val());

        $('#itemWiseSalesPersonModal').modal('hide');

    });

    $('#close', '#productTable #itemWiseSalesPersonModal').on('click', function(){
        selectedItemSalesPersonID = null;

        resetItemSalesPersonDrodown();
        $('#itemWiseSalesPersonModal').modal('hide');
    });

    function resetItemSalesPersonDrodown (){
        $("select[name='itemSalesPerson']", '#productTable #itemWiseSalesPersonModal').val('0').trigger('change');
        $("select[name='itemSalesPerson']", '#productTable #itemWiseSalesPersonModal').selectpicker('refresh');
    }


    $('#resetFilter', '#productTable #searchFilterModal').on('click', function(){

        $("select[name='attributeType']", '#productTable #searchFilterModal').val('0').trigger('change');
        $("select[name='attributeType']", '#productTable #searchFilterModal').selectpicker('refresh');
        $('#productTable #searchFilterModal #attributeValDiv #attributeVal').empty();
        $('#productTable #searchFilterModal #attributeValDiv #attributeVal').append($("<option>", {value: 0, html: 'Select Value'}));
        $('#productTable #searchFilterModal #attributeValDiv #attributeVal').selectpicker('refresh');
        selectedAttrTypeID = null;
        selectedAttrValID = null;

    });

    $productTable.on('click', '#selectAll', function() {
        $(this).parents('#addTaxUl').find('input:checkbox.addNewTaxCheck').each(function() {
            if (!this.checked) {
                this.checked = true;
            }
        });
        $(this).parents('tr').find("#unitPrice").trigger(jQuery.Event("focusout"));
    });

    $productTable.on('click', '#deselectAll', function() {
        $(this).parents('#addTaxUl').find('input:checkbox.addNewTaxCheck').each(function() {
            if (this.checked) {
                this.checked = false;
            }
        });
        $(this).parents('tr').find("#unitPrice").trigger(jQuery.Event("focusout"));
    });

    $productTable.on('click', 'ul.dropdown-menu', function(e) {
        e.stopPropagation();
    });

    $('#disc_presentage,#disc_value').on('change', function() {
        setItemViseTotal();
    });
    $productTable.on('change', '.taxChecks.addNewTaxCheck', function() {
        $(this).parents('tr').find("#unitPrice").trigger(jQuery.Event("focusout"));
    });
    function validateTransferForm(formData) {

        var selectedid = "#" + document.activeElement.id;
        var tableId = $(selectedid).closest('table').attr("id");
        if ((formData.customer == null || formData.customer == "") && $('#customer').val() != '') {
            p_notification(false, eb.getMessage('ERR_DELI_SELECT_CUST'));
            $('#customer').focus();
            return false;
        } else if (formData.location == null || formData.location == "") {
            p_notification(false, eb.getMessage('ERR_DELI_SELECT_LOCAT'));
            $('#currentLocation').focus();
            return false;
        } else if (formData.invoiceCode == null || formData.invoiceCode == "") {
            p_notification(false, eb.getMessage('ERR_INVO_NUM_NOTBLANK'));
        } else if (formData.date == null || formData.date == "") {
            p_notification(false, eb.getMessage('ERR_INVO_DATE_NOTBLANK'));
            $('#deliveryDate').focus();
            return false;
        } else if (formData.dueDate == null || formData.dueDate == "") {
            p_notification(false, eb.getMessage('ERR_INVO_DUEDATE'));
            $('#dueDate').focus();
            return false;
        } else if (eb.convertDateFormat('#dueDate') < eb.convertDateFormat('#deliveryDate')) {
            p_notification(false, eb.getMessage('ERR_DATE_RANGE'));
            $('#dueDate').focus();
            return false;
        } else if (jQuery.isEmptyObject(deliverProducts)) {
            if (tableId != 'deliveryNoteProductTable') {
                p_notification(false, eb.getMessage('ERR_INVO_ADD_ATLEAST_ONEPROD'));
            }
            return false;
        }
        return true;
    }

    function isDate(txtDate)
    {
        var currVal = txtDate;
        if (currVal == '')
            return false;
        //Declare Regex
        var rxDatePattern = /^(\d{4})(\/|-)(\d{1,2})(\/|-)(\d{1,2})$/;
        var dtArray = currVal.match(rxDatePattern); // is format OK?

        if (dtArray == null)
            return false;
        //Checks for mm/dd/yyyy format.
        dtMonth = dtArray[3];
        dtDay = dtArray[5];
        dtYear = dtArray[1];
        if (dtMonth < 1 || dtMonth > 12)
            return false;
        else if (dtDay < 1 || dtDay > 31)
            return false;
        else if ((dtMonth == 4 || dtMonth == 6 || dtMonth == 9 || dtMonth == 11) && dtDay == 31)
            return false;
        else if (dtMonth == 2)
        {
            var isleap = (dtYear % 4 == 0 && (dtYear % 100 != 0 || dtYear % 400 == 0));
            if (dtDay > 29 || (dtDay == 29 && !isleap))
                return false;
        }
        return true;
    }

    function validateDiscount(productID, discountType, currentDiscount, unitPrice,isSchemeDiscount, allocatedSchemeDiscount) {
        var defaultProductData = currentProducts[productID];
        var newDiscount = 0;
        if (defaultProductData.dEL == 1 && isSchemeDiscount == false) {
            if (discountType == 'val') {
                if (toFloat(defaultProductData.dV) !=0 && toFloat(currentDiscount) > toFloat(defaultProductData.dV)) {
                    if (promotionProducts[productID] || (priceListItems[productID] && parseFloat(priceListItems[productID].itemPrice) > 0)) {
                        newDiscount = toFloat(currentDiscount);
                    } else {
                        newDiscount = toFloat(defaultProductData.dV);
                        p_notification(false, eb.getMessage('ERR_INVO_DISCOUNT_VALUE'));
                    }
                } else if (toFloat(defaultProductData.dV) ==0) {
                    if (toFloat(currentDiscount) > toFloat(unitPrice)) {
                        newDiscount = toFloat(unitPrice);
                        p_notification(false, eb.getMessage('ERR_INVO_DISCOUNT_VALUE_UNITE_P'));
                    } else {
                        newDiscount = toFloat(currentDiscount);    
                    }
                } else {
                    newDiscount = toFloat(currentDiscount);
                }

            } else {
                if (toFloat(defaultProductData.dPR) != 0 && toFloat(currentDiscount) > toFloat(defaultProductData.dPR)) {
                    if (promotionProducts[productID] || (priceListItems[productID] && parseFloat(priceListItems[productID].itemPrice) > 0)) {
                        newDiscount = toFloat(currentDiscount);
                    } else {
                        newDiscount = toFloat(defaultProductData.dPR);
                        p_notification(false, eb.getMessage('ERR_INVO_DISC_PERCENTAGE'));
                    }
                } else {
                    newDiscount = toFloat(currentDiscount);
                }
            }
        } else if (defaultProductData.dEL == 1 && isSchemeDiscount == true) {
            if (discountType == 'val') {
                if (toFloat(currentDiscount) > toFloat(allocatedSchemeDiscount)) {
                    if (promotionProducts[productID] || (priceListItems[productID] && parseFloat(priceListItems[productID].itemPrice) > 0)) {
                        newDiscount = toFloat(allocatedSchemeDiscount);
                    } else {
                        newDiscount = toFloat(allocatedSchemeDiscount);
                        p_notification(false, eb.getMessage('ERR_INVO_DISCOUNT_SCHEME_VALUE'));
                    }
                } else if (toFloat(allocatedSchemeDiscount) ==0) {
                    if (toFloat(currentDiscount) > toFloat(unitPrice)) {
                        newDiscount = toFloat(unitPrice);
                        p_notification(false, eb.getMessage('ERR_INVO_DISCOUNT_VALUE_UNITE_P'));
                    } else {
                        newDiscount = toFloat(currentDiscount);    
                    }
                } else {
                    newDiscount = toFloat(currentDiscount);
                }

            } else {
                if (toFloat(currentDiscount) > toFloat(allocatedSchemeDiscount)) {
                    if (promotionProducts[productID] || (priceListItems[productID] && parseFloat(priceListItems[productID].itemPrice) > 0)) {
                        newDiscount = toFloat(currentDiscount);
                    } else {
                        newDiscount = toFloat(allocatedSchemeDiscount);
                        p_notification(false, eb.getMessage('ERR_INVO_DISC_SCHEME_PERCENTAGE'));
                    }
                } else {
                    newDiscount = toFloat(currentDiscount);
                }
            }
        } else {
            if (priceListItems[productID] && parseFloat(priceListItems[productID].itemPrice) > 0) {
                newDiscount = toFloat(currentDiscount);
            } else {
                p_notification(false, eb.getMessage('ERR_INVO_DISABLE_DISC'));
                newDiscount = 0.00;
            }
        }
        return newDiscount;
    }
    /**
     * Inside Batch / Serial Product modal
     */

    function addBatchProduct(productID, productCode, deleted) {

        if ((!$.isEmptyObject(currentProducts[productID].batch))) {
            $('.warranty').removeClass('hidden');
            $('.expDate').removeClass('hidden');
            batchProduct = currentProducts[productID].batch;
            for (var i in batchProduct) {
                if (deleted != 1 && batchProduct[i].PBQ != 0) {
                    if ((!$.isEmptyObject(currentProducts[productID].productIDs))) {
                        if (currentProducts[productID].productIDs[i]) {
                            continue;
                        }
                    }

                    var batchKey = productCode + "-" + batchProduct[i].PBID;
                    var $clonedRow = $($('.batch_row').clone());
                    $clonedRow
                            .data('id', i)
                            .removeAttr('id')
                            .removeClass('hidden batch_row')
                            .addClass('remove_row');
                    $(".batchCode", $clonedRow).html(batchProduct[i].PBC).data('id', batchProduct[i].PBID);
                    $("input[name='availableQuantity']", $clonedRow).val(batchProduct[i].PBQ);
                    $("input[name='deliverQuantityCheck']", $clonedRow).remove();
                    $("input[name='warranty']", $clonedRow).prop('readonly', true);
                    $("input[name='warranty']", $clonedRow).val(batchProduct[i].PBWoD);
                    $("input[name='expireDate']", $clonedRow).val(batchProduct[i].PBExpD);
                    $("input[name='btPrice']", $clonedRow).val(parseFloat(batchProduct[i].PBUP).toFixed(2));
                    if (priceListItems[productID] != undefined && parseFloat(priceListItems[productID].itemPrice) > 0) {
                        $("input[name='btPrice']", $clonedRow).val(parseFloat(priceListItems[productID].itemPrice).toFixed(2));
                    }
                    if (batchProducts[batchKey]) {
                        $("input[name='deliverQuantity']", $clonedRow).val(batchProducts[batchKey].Qty);
                    }
                    if(!(batchProduct[i].PBExpD == null || batchProduct[i].PBExpD == '0000-00-00')){
                    	var expireCheck = checkEpireData(batchProduct[i].PBExpD);
                    	if(expireCheck){
                    		$("input[name='deliverQuantity']", $clonedRow).attr('disabled',true);
                    	}
                    }

                    $clonedRow.insertBefore('.batch_row');
                    $("input[name='availableQuantity'],input[name='deliverQuantity']", $clonedRow).addUom(currentProducts[productID].uom);
                    $("input[name='btPrice']", $clonedRow).addUomPrice(currentProducts[productID].uom);
                } else {
                    batchProducts[batchKey] = undefined;
                }
            }
        }
    }

    function addSerialProduct(productID, productCode, deleted) {
        if ((!$.isEmptyObject(currentProducts[productID].serial))) {
            productSerial = currentProducts[productID].serial;

            var subProList = deliverSubProducts;
            var list_order = new Array();
            for (var i in subProList) {
                if (subProList[i] !== undefined && productSerial[subProList[i][0]['serialID']]) {
                    list_order.push(subProList[i][0]['serialID']);
                }
            }

            for (var i in productSerial) {
                if (!_.where(list_order, i).length) {
                    list_order.push(i);
                } else {
                    continue;
                }
            }

            for (var id in list_order) {
                var i = list_order[id];
                if (deleted != 1) {
                    var serialKey = productCode + "-" + productSerial[i].PSID;
                    var serialQty = (productSerial[i].PSS == 1) ? 0 : 1;
                    var $clonedRow = $($('.batch_row').clone());
                    $clonedRow
                            .data('id', i)
                            .removeAttr('id')
                            .removeClass('hidden batch_row')
                            .addClass('remove_row');
                    $(".serialID", $clonedRow)
                            .html(productSerial[i].PSC)
                            .data('id', productSerial[i].PSID);
                    $("input[name='availableQuantity']", $clonedRow).val(serialQty);
                    $("input[name='deliverQuantity']", $clonedRow).parent().remove();
                    if (currentProducts[productID].giftCard == 0) {
                        $(".warranty").removeClass('hidden');
                        $(".warrant").removeClass('hidden');
                        $(".expDate").removeClass('hidden');
                        $("input[name='warranty']", $clonedRow).val(productSerial[i].PSWoD);
                        let wPeriod = "";
                        if(productSerial[i].PSWoT==='4'){
                            wPeriod = "Years"
                        }else if(productSerial[i].PSWoT==='3'){
                            wPeriod = "Months"
                        }else if(productSerial[i].PSWoT==='2'){
                            wPeriod = "Weeks"
                        }else{
                            wPeriod = "Days"
                        }
                        $("input[name='warrantyType']",$clonedRow).val(wPeriod).attr('disabled',true);
                        
                        $("input[name='expireDate']", $clonedRow).val(productSerial[i].PSExpD);
                        if(!(productSerial[i].PSExpD == null || productSerial[i].PSExpD == '0000-00-00')){
                        	var expireCheck = checkEpireData(productSerial[i].PSExpD);
                        	if(expireCheck){
                        		$("input[name='deliverQuantityCheck']", $clonedRow).attr('disabled',true);
                        	}
                        }
                    }

                    if (serialProducts[serialKey]) {
                        if (serialProducts[serialKey].Qty == 1) {
                            $("input[name='deliverQuantityCheck']", $clonedRow).attr('checked', true);
                        }
                    }

                    $clonedRow.insertBefore('.batch_row');
                    $("input[name='availableQuantity'],input[name='deliverQuantity']", $clonedRow).addUom(currentProducts[productID].uom);
                } else {
                    serialProducts[serialKey] = undefined;
                }
            }
        }
    }
    function addSerialBatchProduct(productID, productCode, deleted) {

        if ((!$.isEmptyObject(currentProducts[productID].batchSerial))) {
            productBatchSerial = currentProducts[productID].batchSerial;
            for (var i in productBatchSerial) {
                if (deleted != 1) {
                    var serialKey = productCode + "-" + productBatchSerial[i].PBID;
                    var serialQty = (productBatchSerial[i].PSS == 1) ? 0 : 1;
                    var $clonedRow = $($('.batch_row').clone());
                    $clonedRow
                            .data('id', i)
                            .removeAttr('id')
                            .removeClass('hidden batch_row')
                            .addClass('remove_row');
                    $(".serialID", $clonedRow)
                            .html(productBatchSerial[i].PSC)
                            .data('id', productBatchSerial[i].PSID);
                    $(".batchCode", $clonedRow)
                            .html(productBatchSerial[i].PBC)
                            .data('id', productBatchSerial[i].PBID);
                    $("input[name='availableQuantity']", $clonedRow).val(serialQty);
                    $("input[name='deliverQuantity']", $clonedRow).parent().remove();
                    $("input[name='btPrice']", $clonedRow).val(parseFloat(productBatchSerial[i].PBUP).toFixed(2));
                    if (priceListItems[productID] != undefined && parseFloat(priceListItems[productID].itemPrice) > 0) {
                        $("input[name='btPrice']", $clonedRow).val(parseFloat(priceListItems[productID].itemPrice).toFixed(2));
                    }
                    if (currentProducts[productID].giftCard == 0) {
                        $(".warranty").removeClass('hidden');
                        $(".expDate").removeClass('hidden');
                        $("input[name='warranty']", $clonedRow).val(productBatchSerial[i].PBSWoD);
                        $("input[name='expireDate']", $clonedRow).val(productBatchSerial[i].PBSExpD);
                    	if(!(productBatchSerial[i].PBSExpD == null || productBatchSerial[i].PBSExpD == '0000-00-00')){
                        	var expireCheck = checkEpireData(productBatchSerial[i].PBSExpD);
                        	if(expireCheck){
                        		$("input[name='deliverQuantityCheck']", $clonedRow).attr('disabled',true);
                        	}
                        }
                    }
                    if (batchProducts[serialKey]) {
                        if (batchProducts[serialKey].Qty == 1) {
                            $("input[name='deliveryQuantityCheck']", $clonedRow).attr('checked', true);
                        }
                    }

                    $clonedRow.insertBefore('.batch_row');
                    $("input[name='availableQuantity']", "input[name='deliverQuantity']", $clonedRow).addUom(currentProducts[productID].uom);
                } else {
                    batchProducts[serialKey] = undefined;
                }
            }
        }
    }

    function checkEpireData(ExpireDate){
    	var today = new Date();
    	var exDate = new Date(ExpireDate);

    	var ctoday = Date.UTC(today.getFullYear(), today.getMonth()+1, today.getDate());
      	var cexDate = Date.UTC(exDate.getFullYear(), exDate.getMonth()+1, exDate.getDate());

    	var flag = true;
    	if(cexDate >= ctoday){
    		flag = false;
    	}
    	return flag;
    }

//add delviery note sub product to invoice
    function addDeliveryNoteSubProduct(productID, selectedSIDs) {

        if (!$.isEmptyObject(deliveryNoteSubProducts[productID])) {
           
            for (var i in deliveryNoteSubProducts[productID]) {
                var batchId = deliveryNoteSubProducts[productID][i].batchID;
                var serialId = deliveryNoteSubProducts[productID][i].serialID;

                var serialQty = deliveryNoteSubProducts[productID][i].qtyByBase;
                var itemExpireDate = deliveryNoteSubProducts[productID][i].expDate;
                var itemWarrentyDates = deliveryNoteSubProducts[productID][i].wrntyDate;
                let wPeriod = "";
                if(deliveryNoteSubProducts[productID][i].wrntyType==='4'){
                    wPeriod = "Years"
                }else if(deliveryNoteSubProducts[productID][i].wrntyType==='3'){
                    wPeriod = "Months"
                }else if(deliveryNoteSubProducts[productID][i].wrntyType==='2'){
                    wPeriod = "Weeks"
                }else{
                    wPeriod = "Days"
                }

                var batchPrice = deliveryNoteSubProducts[productID][i].batchPrice;
                var key = (batchId != 'undefined') ? batchId : serialId;
                var $clonedRow = $($('.batch_row').clone());
                $clonedRow
                        .data('id', key)
                        .removeAttr('id')
                        .removeClass('hidden batch_row hide')
                        .addClass('remove_row');
                if (batchId !== undefined) {
                    $(".batchCode", $clonedRow)
                            .html(deliveryNoteSubProducts[productID][i].batchCode)
                            .data('id', batchId);

                }

                if (serialId !== undefined) {
                    $(".serialID", $clonedRow)
                            .html(deliveryNoteSubProducts[productID][i].serialCode)
                            .data('id', serialId);
                    $("input[name='deliverQuantity']", $clonedRow).parent().remove();
                    if (selectedSIDs.indexOf(serialId) != -1) {
                        $("input[name='deliverQuantityCheck']", $clonedRow).prop('checked', true);
                    }
                } else {
                    $("input[name='deliverQuantityCheck']", $clonedRow).remove();
                    $("input[name='deliverQuantity']", $clonedRow).val(serialQty);
                    $clonedRow.removeClass('hide');
                }
                $("input[name='availableQuantity']", $clonedRow).val(serialQty);
                $("input[name='warranty']", $clonedRow).val(itemWarrentyDates);
                $("input[name='warrantyType']",$clonedRow).val(wPeriod).attr('disabled',true);
                $("input[name='expireDate']", $clonedRow).val(itemExpireDate);
                $("input[name='btPrice']", $clonedRow).val(batchPrice);

                $clonedRow.insertBefore('.batch_row');
                $("input[name='availableQuantity'],input[name='deliverQuantity']", $clonedRow).addUom(currentProducts[productID.split('_')[0]].uom);
                $("input[name='btPrice']", $clonedRow).addUomPrice(currentProducts[productID.split('_')[0]].uom);
            }
        }
    }

    function addSubProducts(productID, productCode, deleted) {
        if ((!$.isEmptyObject(activityProducts[productID].subProduct))) {
            var activitySubProducts = activityProducts[productID].subProduct;
            for (var i in activitySubProducts) {
                if (deleted != 1) {
                    if (activitySubProducts[i].productBatchID != '' && activitySubProducts[i].productSerialID != '') {
                        var serialKey = productCode + "-" + activitySubProducts[i].productBatchID;
                        var serialQty = activitySubProducts[i].activitySubProductQuantity;
                        var $clonedRow = $($('.batch_row').clone());
                        $clonedRow
                                .data('id', i)
                                .removeAttr('id')
                                .removeClass('hidden batch_row')
                                .addClass('remove_row');
                        $(".serialID", $clonedRow)
                                .html(activitySubProducts[i].productSerialCode)
                                .data('id', activitySubProducts[i].productSerialID);
                        $(".batchCode", $clonedRow)
                                .html(activitySubProducts[i].productBatchCode)
                                .data('id', activitySubProducts[i].productBatchID);
                        $("input[name='availableQuantity']", $clonedRow).val(serialQty);
                        $("input[name='deliverQuantity']", $clonedRow).parent().remove();
                        if (batchProducts[serialKey]) {
                            if (batchProducts[serialKey].Qty == 1) {
                                $("input[name='deliveryQuantityCheck']", $clonedRow).attr('checked', true);
                            }
                        }
                    } else if (activitySubProducts[i].productSerialID != '') {
                        var serialKey = productCode + "-" + activitySubProducts[i].productSerialID;
                        var serialQty = activitySubProducts[i].activitySubProductQuantity;
                        var $clonedRow = $($('.batch_row').clone());
                        $clonedRow
                                .data('id', i)
                                .removeAttr('id')
                                .removeClass('hidden batch_row')
                                .addClass('remove_row');
                        $(".serialID", $clonedRow)
                                .html(activitySubProducts[i].productSerialCode)
                                .data('id', activitySubProducts[i].productSerialID);
                        $("input[name='availableQuantity']", $clonedRow).val(serialQty);
                        $("input[name='deliverQuantity']", $clonedRow).parent().remove();
                        if (serialProducts[serialKey]) {
                            if (serialProducts[serialKey].Qty == 1) {
                                $("input[name='deliverQuantityCheck']", $clonedRow).attr('checked', true);
                            }
                        }
                    } else if (activitySubProducts[i].productBatchID != '') {
                        var batchKey = productCode + "-" + activitySubProducts[i].productBatchID;
                        var $clonedRow = $($('.batch_row').clone());
                        $clonedRow
                                .data('id', i)
                                .removeAttr('id')
                                .removeClass('hidden batch_row')
                                .addClass('remove_row');
                        $(".batchCode", $clonedRow).html(activitySubProducts[i].productBatchCode).data('id', activitySubProducts[i].productBatchID);
                        $("input[name='availableQuantity']", $clonedRow).val(activitySubProducts[i].activitySubProductQuantity);
                        $("input[name='deliverQuantityCheck']", $clonedRow).remove();
                        if (batchProducts[batchKey]) {
                            $("input[name='deliverQuantity']", $clonedRow).val(batchProducts[batchKey].Qty);
                        }
                    }
                    $clonedRow.insertBefore('.batch_row');
                    $("input[name='availableQuantity']", "input[name='deliverQuantity']", $clonedRow).addUom(currentProducts[productID.split('-')[0]].uom);
                } else {
                    //                    batchProducts[serialKey] = undefined;

                }
            }
        }
    }


    function clearProductScreen() {
        products = {};
        currentProducts = {};
        setInvoiceTotalCost();
        setTotalTax();
        $('.addedProducts').remove();
        locationProducts = '';
        locProductCodes = '';
        locProductNames = '';
        selectedProduct = '';
        selectedProductQuantity = '';
        selectedProductUom = '';
        batchCount = '';
    }

    function clearInvoiceForm() {
        resetInvoicePage();
        $('#customer').find('option').remove();
		$('#customer').removeData('AjaxBootstrapSelect');
		$('#customer').removeData('selectpicker');
		$('#customer').siblings('.bootstrap-select').remove();
		$('#customer').siblings('.bootstrap-select').find('.dropdown-menu').remove();
        $('#customer').append('<option value="">Select Customer</option>');
        $('#customer').attr('disabled',false).val('');
        $('#customer').selectpicker('refresh');
        loadDropDownFromDatabase('/customerAPI/search-customers-for-dropdown', "", 0, '#customer');
        $('#dueDate').val('');
        $('#salesPersonID').val('');
        $('#customCurrencyId').val(customCurrencyID);
        $('#cus-prof-name').val('');
        $('.cus_prof_div').addClass('hidden');
        $('#paymentTerm').val(1);
        $('#customerCurrentBalance').val('');
        $('#customerCurrentCredit').val('');
        $('#priceListId').val('');
    }

    function setItemViseTotal() {
        invoiceTotalDiscountTrigger = true;
        $('#add-new-item-row tr').each(function() {
            if (!$(this).hasClass('sample') && $('#itemUnitPrice').val()) {
                var thisRow = $(this);
                var productID = thisRow.data('id');
                if (typeof productID == "undefined" || productID == "") {
                    //do nothing
                } else {
                    if (inclusiveTax) {
                        calculateItemTotalForInclusiveTax(thisRow, productID);
                    } else {
                        calculateItemTotal(thisRow, productID);
                    }
                    var deliverProductIndex = productID + '_' + thisRow.data('proIncID');
                    deliverProducts[deliverProductIndex]['productTotal'] = productsTotal[productID];
                    deliverProducts[deliverProductIndex]['pTax'] = productsTax[productID];
                    setTotalTax();
                    setInvoiceTotalCost();
                }
            } else if ((copyFromDel || copyFromSalesOrd || copyFromQuot || copyFromAct || copyFromPro || copyFromJob) && !$(this).hasClass('sample')) {
                var thisRow = $(this);
                var productID = thisRow.data('id');
                if (typeof productID == "undefined" || productID == "") {
                    //do nothing
                } else {
                    if (inclusiveTax) {
                        calculateItemTotalForInclusiveTax(thisRow, productID);
                    } else {
                        calculateItemTotal(thisRow, productID);
                    }
                    var deliverProductIndex = productID + '_' + thisRow.data('proIncID');
                    deliverProducts[deliverProductIndex]['productTotal'] = productsTotal[productID];
                    deliverProducts[deliverProductIndex]['pTax'] = productsTax[productID];
                    setTotalTax();
                    setInvoiceTotalCost();
                }
            }
        });
        deliveryCEnable = false;
        invoiceTotalDiscountTrigger = false;
    }

    function setInvoiceTotalCost() {
        var subTotal = 0;
        for (var i in deliverProducts) {
            if (inclusiveTax) {
                var subAmount = accounting.formatMoney(deliverProducts[i].productTotal);
                subTotal += parseFloat(subAmount.replace(/,/g, ''));
            } else {
                subTotal += toFloat(deliverProducts[i].productTotal);
            }
        }
        if (subTotal < 0) {
            subTotal = 0;
        }
        if ($('#total_discount_rate').val() != '' && $('input[name=discount_type]:checked').val() == 'Value') {


            var dicountVal = $('#total_discount_rate').val();

            if (parseFloat(dicountVal) > toFloat(subTotal)) {
                $('#total_discount_rate').val('');
                dicountVal = 0;
                decimalPoints = 0;
            }

            
            if (subTotal > 0) {
                subTotal = toFloat(subTotal) - toFloat($('#total_discount_rate').val());
            } else {
                subTotal = toFloat(subTotal);
            }
        }
        $('#subtotal').html(accounting.formatMoney(subTotal));
        var finalTotal = 0.00;
        if ($('#deliveryChargeEnable').is(':checked')) {
            //            var final_discount_precentage = $('#total_discount_rate').val();
//            var final_disc_created_value = ((toFloat(100) - toFloat(final_discount_precentage)) / 100) * subTotal;
            //            subTotal = toFloat(final_disc_created_value);
            var deliveryAmount = $('#deliveryCharge').val();
            finalTotal = subTotal + toFloat(deliveryAmount);
        } else {
            finalTotal = subTotal;
        }

        if (addedServiceChargeAmount > 0) {
            finalTotal = finalTotal + toFloat(addedServiceChargeAmount);
        }


//        if ($('#total_discount_rate').val() && !$('#deliveryChargeEnable').is(':checked')) {
//            var final_discount_precentage = $('#total_discount_rate').val();
//            var final_disc_created_value = ((toFloat(100) - toFloat(final_discount_precentage)) / 100) * subTotal;
//            subTotal = toFloat(final_disc_created_value);
        //        }
        if ($('#tax_exempted').is(':checked')) {
            finalTotal = finalTotal - totalSuspendedTaxAmount;
            $('#invoiceNo').val($('#stReferenceNumber').val());
        } else {
            //            finalTotal = finalTotal;
            $('#invoiceNo').val($('#refNumber').val());
        }

        $('#finaltotal').html(accounting.formatMoney(finalTotal));
        invoiceTotalValue = finalTotal;
        return  finalTotal;
    }

    function setTotalTax() {
        totalTaxList = {};
        if (TAXSTATUS) {
            for (var k in deliverProducts) {
                for (var l in deliverProducts[k].pTax.tL) {
                    if (totalTaxList.hasOwnProperty(l)) {
                        var curTA = totalTaxList[l].tA;
                        var newTA = (toFloat(curTA) + toFloat(deliverProducts[k].pTax.tL[l].tA));
                        totalTaxList[l].tA = newTA.toFixed(2);
                    } else {
                        totalTaxList[l] = {tN: deliverProducts[k].pTax.tL[l].tN, tP: deliverProducts[k].pTax.tL[l].tP, tA: deliverProducts[k].pTax.tL[l].tA, susTax: deliverProducts[k].pTax.tL[l].susTax};
                    }
                }
            }

            var totalTaxHtml = "";
            totalTaxAmount = 0.00;
            totalSuspendedTaxAmount = 0.00;
            for (var t in totalTaxList) {
                totalTaxHtml += totalTaxList[t].tN + "(" + totalTaxList[t].tP + "%)&nbsp" + accounting.formatMoney(totalTaxList[t].tA) + "<br>";
                totalTaxAmount += toFloat(totalTaxList[t].tA);
                if(totalTaxList[t].susTax==1){
                	totalSuspendedTaxAmount += toFloat(totalTaxList[t].tA);
                }
            }
            $('#totalTaxShow').html(totalTaxHtml);
        }
    }

    function setTaxListForProduct(productID, $currentRow) {

        if ((!jQuery.isEmptyObject(currentProducts[productID].tax))) {
            productTax = currentProducts[productID].tax;
            $currentRow.find('#taxApplied').removeClass('glyphicon glyphicon-unchecked');
            $currentRow.find('#taxApplied').addClass('glyphicon glyphicon-check');
            $currentRow.find('.tempLi').remove();
            $currentRow.find('#addNewTax').attr('disabled', false);
            for (var i in productTax) {
                var clonedLi = $($currentRow.find('#sampleLi').clone()).removeClass('hidden').attr('id', 'li_' + i).addClass("tempLi");
                clonedLi.children(".taxChecks").attr('id', productID + '_' + i).prop('checked', true).addClass('addNewTaxCheck');
                if (productTax[i].tS == 0) {
                    clonedLi.children(".taxChecks").attr('disabled', 'disabled');
                    clonedLi.children(".taxName").addClass('crossText');
                }
                clonedLi.children(".taxName").html('&nbsp&nbsp' + productTax[i].tN + '&nbsp&nbsp' + productTax[i].tP + '%').attr('for', productID + '_' + i);
                clonedLi.insertBefore($currentRow.find('#sampleLi'));
            }
        } else {
            $('#taxApplied').addClass('glyphicon glyphicon-unchecked');
            $currentRow.find('#addNewTax').attr('disabled', true);
        }

    }

    $('#showTax').on('click', function() {
        if (this.checked) {
            if (!jQuery.isEmptyObject(deliverProducts)) {
                $('#totalTaxShow').removeClass('hidden');
            } else {
                p_notification(false, eb.getMessage('ERR_GRN_PROD_TAX'));
            }

        } else {
            $('#totalTaxShow').addClass('hidden');
        }

    });
    //make show tax checked when loading

    $('#showTax').prop('checked', true);
    $('#totalTaxShow').removeClass('hidden');
    $('#deliveryChargeEnable').on('click', function() {
        deliveryCEnable = true;
        setInvoiceTotalCost();
        setTotalTax();
        applyPromotionDiscounts();
        if (this.checked) {
            $('.deliCharges').removeClass('hidden');
            $('.deliAddress').removeClass('hidden');
            $('#deliveryCharge').focus();
        }
        else {
            $('#deliveryCharge').val(0);
            $('.deliCharges').addClass('hidden');
            $('.deliAddress').addClass('hidden');
        }
    });
    $('#deliveryCharge').on('change keyup', function() {
        if (!$.isNumeric($('#deliveryCharge').val()) || $(this).val() < 0) {
            $(this).val('');
        }
        setInvoiceTotalCost();
        setTotalTax();
        applyPromotionDiscounts();
    });

    var nowTemp = new Date();
    var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);
    var nowTemp = new Date();
    var now1 = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);
    var myDate = new Date();
    var prettyDate = myDate.getFullYear() + '-' + (myDate.getMonth() + 1) + '-' + (myDate.getDate() < 10 ? '0' : '') + myDate.getDate();
    var day = myDate.getDate() < 10 ? '0' + myDate.getDate() : myDate.getDate();
    var month = (myDate.getMonth() + 1) < 10 ? '0' + (myDate.getMonth() + 1) : (myDate.getMonth() + 1);
    $("#deliveryDate").val(eb.getDateForDocumentEdit('#deliveryDate', day, month, myDate.getFullYear()));
    var checkin = $('#deliveryDate').datepicker({onRender: function(date) {
            return date.valueOf() < now.valueOf() ? ((U_DATE_R_OVERRIDE) ? '' : 'disabled') : '';
        },
        //  format: 'yyyy-mm-dd',
    }).on('changeDate', function(ev) {
        var days = 0;
        var i = parseInt($('#paymentTerm').val());
        switch (i) {
            case 2:
                days = 7;
                break;
            case 3:
                days = 14;
                break;
            case 4:
                days = 30;
                break;
            case 5:
                days = 60;
                break;
            default:
                days = 0;
                break;
        }
        setDueDate(days);
        checkin.hide();
    }).data('datepicker');
    var checkin1 = $('#dueDate').datepicker({onRender: function(date) {
            return date.valueOf() < now1.valueOf() ? ((U_DATE_R_OVERRIDE) ? '' : 'disabled') : '';
        },
        //format: 'yyyy-mm-dd'
    }).on('changeDate', function(ev) {
        checkin1.hide();
    }).data('datepicker');
    checkin1.setValue(now);
    var membership_date = $('#membership-date').datepicker({onRender: function(date) {
            return date.valueOf() < now1.valueOf() ? ((U_DATE_R_OVERRIDE) ? '' : 'disabled') : '';
        },
        //format: 'yyyy-mm-dd'
    }).on('changeDate', function(ev) {
        membership_date.hide();
    }).data('datepicker');
    function resetInvoicePage() {
        //    clearProductScreen();
        checkSubProduct = {};
        deliverProducts = {};
        productsTotal = {};
        productsTax = {};
        deliverSubProducts = {};
        SubProductsQty = {};
        $('#salesPersonID').attr('disabled', false);
        $('#salesPersonID').selectpicker('render');
        $('#add-new-item-row').find('tr').each(function() {
            if (!$(this).hasClass('add-row')) {
                $(this).remove();
            }
        });
        var $currentRow = getAddRow();
        $currentRow.removeClass('hidden');
        $('#subtotal').val('');
        $('#finaltotal').val('');
        $('#totalTaxShow').html('');
        $('#priceListId').val('').attr('disabled', false).selectpicker('refresh');
        $('#priceListId').trigger('change');
    }
    $('#addCustomerModal').on('shown.bs.modal', function() {
        $(this).find("input[name='customerName']").focus();
    });
    //more details button
    $("#moredetails").click(function() {
        $("#customer_more").slideDown();
        $('#moredetails').hide();
        $('#customerCurrentBalance', '#addcustomerform').attr('disabled', true);
        $('#customerCurrentCredit', '#addcustomerform').attr('disabled', true);
    });
    $('#tax_exempted').on('click', function() {
        var currentInvoiceTotal = accounting.unformat($('#finaltotal').html());
        var newTotal = toFloat(currentInvoiceTotal);
        if ($('#tax_exempted').is(":checked")) {
            newTotal = newTotal - toFloat(totalSuspendedTaxAmount);
            $('#invoiceNo').val($('#stReferenceNumber').val());
        } else {
            newTotal = newTotal + toFloat(totalSuspendedTaxAmount);
            $('#invoiceNo').val($('#refNumber').val());
        }
        $('#finaltotal').html(accounting.formatMoney(newTotal.toFixed(2)));
    });
    function applyPromotionDiscounts() {
        //if promotion is item base then add discount for items
        invBasePromotionDiscount = false;
        if (!$.isEmptyObject(promotions) && promotions.promoType == 1) {
            promotionDiscountValue = 0.00;
            promotionProducts = {};
            // $.each(promotions.promoProducts, function(index, value) {
            //     promotionProducts[value.productID] = value;
            // });
            $('tr', '#add-new-item-row').each(function() {
                var textid = this.id;
                if (textid) {
                    $(this).find('#deliveryNoteDiscount').trigger('focusout');
                }
            });
            invoiceWisePromotionDiscountType = null;
        }

        if (!$.isEmptyObject(promotions) && promotions.promoType == 3) {
            promotionDiscountValue = 0.00;
            promotionProducts = {};
            // $.each(promotions.promoProducts, function(index, value) {
            //     promotionProducts[value.productID] = value;
            // });
            $('tr', '#add-new-item-row').each(function() {
                var textid = this.id;
                if (textid) {
                    $(this).find('#deliveryNoteDiscount').trigger('focusout');
                }
            });
            invoiceWisePromotionDiscountType = null;
        }
        if ($.isEmptyObject(promotions)) {
            promotionDiscountValue = 0.00;
            invoiceWisePromotionDiscountType = null;
            promotionProducts = {};
            cancelPromotionFlag = true;
            $('tr', '#add-new-item-row').each(function() {
                var textid = this.id;
                if (textid) {
                    $(this).find('#deliveryNoteDiscount').trigger('focusout');
                }
            });
        }

        //if promotion is invoice promotion add discount for whole invoice
        if (!$.isEmptyObject(promotions) && promotions.promoType == 2) {
        	invBasePromotionDiscount = true;
            promotionDiscountValue == 0.00;
            promotionProducts = {};
            $('tr', '#add-new-item-row').each(function() {
                var textid = this.id;
                if (textid) {
                    $(this).find('#deliveryNoteDiscount').trigger('focusout');
                }
            });
            if (promotions.discountType == 1) {
                total = setInvoiceTotalCost();
                if (toFloat(promotions.minValue) <= toFloat(total)) {
                    promotionDiscountValue = toFloat(promotions.discountAmount);
                    promotionDiscountValueInvoicedBased = toFloat(promotions.discountAmount);
                }
                total = toFloat(total) - toFloat(promotionDiscountValue);
                $('#finaltotal').html(accounting.formatMoney(total));
            }
            invoiceWisePromotionDiscountType = promotions.discountType;
        }

    }

    $('#promotion').on('change', function(e) {
        e.preventDefault();

        var invBaseDisPrecntFlag = $(this).find(':selected').data('invbaseprecentage');
        //if invoice base promotion is a discount precentage and the items are not loaded to the table, new promotion is not apply.
        if(!(invBaseDisPrecntFlag == 1 && Object.keys(deliverProducts).length == 0)){
        	promotionID = $('#promotion').val();
        	eb.ajax({
            	type: 'POST',
            	url: BASE_URL + '/api/promotions/getPromotion',
            	data: {promotionID: promotionID},
            	success: function(respond) {
                	if (respond.status == true) {
                    	promotions = {};
                    	promotions = respond.data;
                    	applyPromotionDiscounts();

                    	getAddRow().find('#itemCode').attr('disabled',false);
                    	var $trForDisabled = $('#add-new-item-row').find('tr :not(.add-row)');
                    	$trForDisabled.find('.edit').attr('disabled',false);
                    	$trForDisabled.find('.delete').attr('disabled',false);

                    	if (invBaseDisPrecntFlag == 1) {
                    		getAddRow().find('#itemCode').attr('disabled',true);
                    		$trForDisabled.find('.edit').attr('disabled',true);
                    		$trForDisabled.find('.delete').attr('disabled',true);
                    	}
                	} else {
                    	p_notification(false, respond.msg);
                	}
            	}
        	});
        }else{
        	p_notification(false, eb.getMessage('ERR_ADD_ITEM_BEFO_ADD_THIS_PROM'));
        	$('#promotion').val(promotionID);
        }

    });
    $('#customCurrencyId').on('change', function() {

    	if($(this).data('baseid') == $(this).val()){
    		$('#customCurrencyRate').val(0);
    		$('#customCurrencyRate').attr('disabled',true);
    	}else{
    		$('#customCurrencyRate').attr('disabled',false);
    	}

        eb.ajax({
            type: 'POST',
            url: BASE_URL + '/currencySymbol/getCustomCurrencyByCustomCurrencyId',
            data: {customCurrencyId: $(this).val()},
            success: function(respond) {
                if (respond.status == true) {
                	$('#customCurrencyRate').val(respond.data.currencyRate);
                    $('.cCurrency').text(respond.data.currencySymbol);
                    companyCurrencySymbol = respond.data.currencySymbol;
                    enableMultiDeliNoteDropdown();

                }
            }
        });
    });

    $('#priceListId').on('change', function() {
        priceListId = $(this).val();
        eb.ajax({
            type: 'POST',
            url: BASE_URL + '/currencySymbol/getPriceListWithDiscount',
            data: {priceListId: priceListId},
            success: function(respond) {
                if (respond.status == true) {
                    priceListItems = respond.data;
                } else {
                    priceListItems = [];
                }
            },
            async: false
        });
    });

    $("#journal-entry-view").on('click',function(){

        var formData = {invoiceCode: $('#invoiceNo').val(),
            location: $('#currentLocation').val(),
            date: $('#deliveryDate').val(),
            dueDate: $('#dueDate').val(),
            customer: $('#customer').text(),
            customerID: customerID,
            deliveryAddress: $('#deliveryNoteAddress').val()
        };

        var suspendedtax = $('#tax_exempted').is(':checked') ? 1 : 0;
        var showTax = 0;
        if ($('#showTax').is(':checked')) {
            showTax = 1;
        }

        var serviceChargeAmount = addedServiceChargeAmount;
        var addedServiceChargeArray = {};
        if (!$.isEmptyObject(finaleServiceChargeArr)) {
            addedServiceChargeArray = finaleServiceChargeArr;
        }

        if (validateTransferForm(formData)) {
            eb.ajax({
                type: 'POST',
                url: BASE_URL + '/invoice-api/createJournalEntryTemplateForView',
                data: {
                    invoiceCode: formData.invoiceCode,
                    locationOutID: locationOut,
                    products: deliverProducts,
                    subProducts: deliverSubProducts,
                    invoiceDate: formData.date,
                    dueDate: $('#dueDate').val(),
                    customer: customerID,
                    customerProfID: cusProfID,
                    customerName: $('#customer').text(),
                    deliveryCharge: $('#deliveryCharge').val(),
                    invoiceTotalPrice: $('#finaltotal').text().replace(/,/g, ''),
                    invoiceComment: $('#comment').val(),
                    paymentTermID: $('#paymentTerm').val(),
                    invoiceDiscountType: $('input[name=discount_type]:checked').val(),
                    invoiceDiscountRate: $('#total_discount_rate').val(),
                    totalTaxValue: (totalTaxAmount - totalSuspendedTaxAmount),
                    susTaxAmount: totalSuspendedTaxAmount,
                    showTax: showTax,
                    deliveryNoteID: deliveryNoteIDs,
                    salesOrderID: salesOrderID,
                    quotationID: quotationID,
                    activityID: activityID,
                    jobID: jobID,
                    projectID: projectID,
                    salesPersonID: $('#salesPersonID').val(),
                    suspendedTax: suspendedtax,
                    deliveryAddress: $('#deliveryAddress').val(),
                    promotionID: promotionID,
                    promotionDiscountValue: promotionDiscountValue,
                    invoiceWisePromotionDiscountType: invoiceWisePromotionDiscountType,
                    customCurrencyId: $('#customCurrencyId').val(),
                    customCurrencyRate: customCurrencyRate,
                    priceListId: priceListId,
                    salesInvoiceDeliveryChargeEnable: ($('#deliveryChargeEnable').is(':checked')) ? '1' : '0',
                    addedServiceChargeArray: addedServiceChargeArray,
                    serviceChargeAmount:  serviceChargeAmount
                },
                success: function(respond) {
                    if (respond.status) {
                        setJournlaEntryData(respond.data);
                        $('#editJournalEntry').modal('show');
                    }else{
                        p_notification(respond.status, respond.msg);
                    }
                }
            });
        }
    });

    $('#saveInvoiceWithJournalEntry').on('click',function(){
        if ($("tr.edit-row", $productTable).length > 0) {
            p_notification(false, eb.getMessage('ERR_INVO_SAVE_ALLPROD'));
            return false;
        }
        var journalEntryAccounts = {};

        $i = 0;
        $('#journal-entry-body > tr').each(function(){
            if($(this).hasClass('add_acc')){
                journalEntryAccounts[$i] = {
                    financeAccountsID: $(this).find('.accountID').val(),
                    journalEntryAccountsCreditAmount: $(this).find('.creditAmount').val(),
                    journalEntryAccountsDebitAmount: $(this).find('.debitAmount').val(),
                    journalEntryAccountsMemo: $(this).find('.memo').val(),
                }
                $i++;
            }
        });
        journalEntryData ={};
        journalEntryData ={
            journalEntryDate: $('#journalEntryDate').val(),
            journalEntryCode: $('#journalEntryCode').val(),
            journalEntryComment: $('#journalEntryComment').val(),
            journalEntryAccounts: journalEntryAccounts,
        }

        saveInvoice(journalEntryData);
        $('#editJournalEntry').modal('hide');
    });

    $('#select_serials').on('click',function(e) {
        var numberOfRow = $('#numberOfRow').val();
        if (numberOfRow != '' || numberOfRow != 0) {
            $('#batch_data > tr').each(function(key, value){
                if (numberOfRow != 0 && !($("input[name='deliverQuantityCheck']", value).is('[disabled=disabled]'))) {
                    $("input[name='deliverQuantityCheck']", value).prop('checked', true);
                    numberOfRow--;
                } else {
                    $("input[name='deliverQuantityCheck']", value).prop('checked', false);
                }
            });
        }    
        
    });

    //This function is used to calculate inclusive tax item total
    function calculateItemTotalForInclusiveTax(thisRow, productID) {
        var thisRow = thisRow;
        var productID = productID;
        var checkedTaxes = Array();
        thisRow.find('input:checkbox.addNewTaxCheck').each(function() {
            if (this.checked) {
                var cliTID = this.id;
                var tTaxID = cliTID.split('_')[1];
                checkedTaxes.push(tTaxID);
            }
        });
        var productType = thisRow.find('#itemCode').data('PT');
        var tmpItemQuentity = thisRow.find('#deliverQuanity').val();
        if (productType == 2 && tmpItemQuentity == 0) {
            tmpItemQuentity = 1;
        }
        realUnitePrice = calculateUnitePriceByInclusiveTax(toFloat(thisRow.find('#unitPrice').val()), checkedTaxes);
        var tmpItemTotal = (toFloat(realUnitePrice) * toFloat(tmpItemQuentity));
        var tmpItemCost = tmpItemTotal;
        var tmpItemCostChangeFlag = false;
        var prompItemQtyFlag = false;
        triggeredByPromotion = false;
        var prompItemMaxQty = 0;
        var failPromo = false;
        //check promotion Discounts
        if (!$.isEmptyObject(promotions)) {
            if  (promotions.promoType == 1) {
                for (var j = 0; j < promotions.combinations.length; ++j) {
                    if (promotions.combinations[j]['relatedProductIds'].includes(productID)){
                        if (promotions.combinations[j]['conditionType'] == "AND") {
                            var trueRules = 0;
                            for (var k = 0; k < promotions.combinations[j]['ruleSet'].length; ++k) {
                                $.each(deliverProducts, function(index, value) {
                                    if (promotions.combinations[j]['ruleSet'][k]['productID'] == value['productID']) {
                                        var cartQty = Number(value['deliverQuantity']['qty']);
                                        var ruleQty = Number(promotions.combinations[j]['ruleSet'][k]['quantity']);
                                        
                                        switch (promotions.combinations[j]['ruleSet'][k]['rule']) {
                                            case "equal":
                                              if (cartQty == ruleQty) {
                                                  trueRules = trueRules + 1;
                                              }
                                              break;
                                            case "greater than or equal":
                                              if (cartQty >= ruleQty) {
                                                  trueRules = trueRules + 1;
                                              }
                                              break;
                                            case "less than or equal":
                                              if (cartQty <= ruleQty) {
                                                  trueRules = trueRules + 1;
                                              }
                                              break;
                                            case "greater than":
                                              if (cartQty > ruleQty) {
                                                  trueRules = trueRules + 1;
                                              }
                                              break;
                                            case "less than":
                                              if (cartQty < ruleQty) {
                                                  trueRules = trueRules + 1;
                                              }
                                              break;
                                            case "not equal":
                                              if (cartQty != ruleQty) {
                                                  trueRules = trueRules + 1;
                                              }
                                              break;
                                           
                                            default:
                                              // code...
                                              break;
                                        }
                                    }
                                    
                                });
                                // for (var l = 0; l < deliverProducts.length; ++l) {
                                //     console.log(deliverProducts[l]);
                                // }
                            }
                            if (trueRules == promotions.combinations[j]['ruleSet'].length) {
                                promotionProducts[productID] = productID;
                                currentProducts[productID]['dEL'] = 1;
                                // discount type is value
                                if (promotions.combinations[j]['discountType'] == 1) {
                                    // var tempDis = qty * $scope.promotion.combinations[j]['discountAmount'];

                                    // discount = discount + tempDis;
                                    thisRow.find('#deliveryNoteDiscount').val(promotions.combinations[j]['discountAmount']);
                                    thisRow.find('#deliveryNoteDiscount').addClass('value');
                                    if (thisRow.find('#deliveryNoteDiscount').hasClass('precentage')) {
                                        thisRow.find('#deliveryNoteDiscount').removeClass('precentage');
                                    }
                                    thisRow.find('.sign').text(companyCurrencySymbol);


                                }

                                if (promotions.combinations[j]['discountType'] == 2) {
                                    // var tempDis = ((price * $scope.promotion.combinations[j]['discountAmount']) / 100 ) *  qty;

                                    // discount = discount + tempDis;
                                    thisRow.find('#deliveryNoteDiscount').val(promotions.combinations[j]['discountAmount']);
                                    thisRow.find('#deliveryNoteDiscount').addClass('precentage');
                                    if (thisRow.find('#deliveryNoteDiscount').hasClass('value')) {
                                        thisRow.find('#deliveryNoteDiscount').removeClass('value');
                                    }
                                    thisRow.find('.sign').text('%');

                                }
                            } else {
                                failPromo = true;
                            }
                        } else if (promotions.combinations[j]['conditionType'] == "OR") {
                            var isAllow = false;
                            for (var m = 0; m < promotions.combinations[j]['ruleSet'].length; ++m) {
                                if (productID == promotions.combinations[j]['ruleSet'][m]['productID']) {
                                    let cartQty = Number(tmpItemQuentity);
                                    let ruleQty = Number(promotions.combinations[j]['ruleSet'][m]['quantity']);

                                    switch (promotions.combinations[j]['ruleSet'][m]['rule']) {
                                        case "equal":
                                          if (cartQty == ruleQty) {
                                            isAllow = true;
                                          }
                                          break;
                                        case "greater than or equal":
                                          if (cartQty >= ruleQty) {
                                            isAllow = true;
                                          }
                                          break;
                                        case "less than or equal":
                                          if (cartQty <= ruleQty) {
                                            isAllow = true;
                                          }
                                          break;
                                        case "greater than":
                                          if (cartQty > ruleQty) {
                                            isAllow = true;
                                          }
                                          break;
                                        case "less than":
                                          if (cartQty < ruleQty) {
                                            isAllow = true;
                                          }
                                          break;
                                        case "not equal":
                                          if (cartQty != ruleQty) {
                                            isAllow = true;
                                          }
                                          break;
                                       
                                        default:
                                          // code...
                                          break;
                                    }
                                }
                            }

                            if (isAllow) {
                                promotionProducts[productID] = productID;
                                currentProducts[productID]['dEL'] = 1;
                              //discount type is value
                                if (promotions.combinations[j]['discountType'] == 1) {
                                    // var tempDis = qty * $scope.promotion.combinations[j]['discountAmount'];

                                    // discount = discount + tempDis;
                                    thisRow.find('#deliveryNoteDiscount').val(promotions.combinations[j]['discountAmount']);
                                    thisRow.find('#deliveryNoteDiscount').addClass('value');
                                    if (thisRow.find('#deliveryNoteDiscount').hasClass('precentage')) {
                                        thisRow.find('#deliveryNoteDiscount').removeClass('precentage');
                                    }
                                    thisRow.find('.sign').text(companyCurrencySymbol);

                                }

                                if (promotions.combinations[j]['discountType'] == 2) {
                                    // var tempDis = ((price * $scope.promotion.combinations[j]['discountAmount']) / 100 ) *  qty;

                                    // discount = discount + tempDis;
                                    thisRow.find('#deliveryNoteDiscount').val(promotions.combinations[j]['discountAmount']);
                                    thisRow.find('#deliveryNoteDiscount').addClass('precentage');
                                    if (thisRow.find('#deliveryNoteDiscount').hasClass('value')) {
                                        thisRow.find('#deliveryNoteDiscount').removeClass('value');
                                    }
                                    thisRow.find('.sign').text('%');

                                }
                            } else {
                                failPromo = true;
                            }   
                        }
                    } else {
                        failPromo = true;
                    }
                }
            } else if (promotions.promoType == 3) {

                for (var j = 0; j < promotions.combinations.length; ++j) {
                    if (promotions.combinations[j]['relatedProductIds'].includes(productID)){ 
                        if (promotions.combinations[j]['conditionType'] == "AND") {
                            var isAllow = false;
                            var qtyRuleCount = 0;
                            var trueCount = 0;
                            for (var k = 0; k < promotions.combinations[j]['ruleSet'].length; ++k) {
                                if (promotions.combinations[j]['ruleSet'][k]['attributeID'] == 0 || promotions.combinations[j]['ruleSet'][k]['attributeID'] == null) {
                                    qtyRuleCount = qtyRuleCount +1;
                                    let cartQty = Number(tmpItemQuentity);
                                    let ruleQty = Number(promotions.combinations[j]['ruleSet'][k]['quantity']);

                                    switch (promotions.combinations[j]['ruleSet'][k]['rule']) {
                                        case "equal":
                                          if (cartQty == ruleQty) {
                                              trueCount = trueCount + 1;
                                          }
                                          break;
                                        case "greater than or equal":
                                          if (cartQty >= ruleQty) {
                                              trueCount = trueCount + 1;
                                          }
                                          break;
                                        case "less than or equal":
                                          if (cartQty <= ruleQty) {
                                              trueCount = trueCount + 1;
                                          }
                                          break;
                                        case "greater than":
                                          if (cartQty > ruleQty) {
                                              trueCount = trueCount + 1;
                                          }
                                          break;
                                        case "less than":
                                          if (cartQty < ruleQty) {
                                              trueCount = trueCount + 1;
                                          }
                                          break;
                                        case "not equal":
                                          if (cartQty != ruleQty) {
                                              trueCount = trueCount + 1;
                                          }
                                          break;
                                       
                                        default:
                                          // code...
                                          break;
                                    }
                                }
                            }
                            if (qtyRuleCount == trueCount) {
                              isAllow = true;
                            } else {
                              isAllow = false;
                            }

                            if (isAllow) {
                                promotionProducts[productID] = productID;
                                currentProducts[productID]['dEL'] = 1;
                                //discount type is value
                                if (promotions.combinations[j]['discountType'] == 1) {
                                    // var tempDis = qty *  $scope.promotion.combinations[j]['discountAmount'];

                                    // discount = discount + tempDis;
                                    thisRow.find('#deliveryNoteDiscount').val(promotions.combinations[j]['discountAmount']);
                                    thisRow.find('#deliveryNoteDiscount').addClass('value');
                                    if (thisRow.find('#deliveryNoteDiscount').hasClass('precentage')) {
                                        thisRow.find('#deliveryNoteDiscount').removeClass('precentage');
                                    }
                                    thisRow.find('.sign').text(companyCurrencySymbol);

                                }

                                if (promotions.combinations[j]['discountType'] == 2) {
                                    // var tempDis = ((price * $scope.promotion.combinations[j]['discountAmount']) / 100 ) * qty;

                                    // discount = discount + tempDis;
                                    thisRow.find('#deliveryNoteDiscount').val(promotions.combinations[j]['discountAmount']);
                                    thisRow.find('#deliveryNoteDiscount').addClass('precentage');
                                    if (thisRow.find('#deliveryNoteDiscount').hasClass('value')) {
                                        thisRow.find('#deliveryNoteDiscount').removeClass('value');
                                    }
                                    thisRow.find('.sign').text('%');

                                }
                            } else {
                                failPromo = true;
                            }
                        }
                        else if (promotions.combinations[j]['conditionType'] == "OR") {
                            promotionProducts[productID] = productID;
                            currentProducts[productID]['dEL'] = 1;
                            //discount type is value
                            if (promotions.combinations[j]['discountType'] == 1) {
                              // var tempDis = qty * $scope.promotion.combinations[j]['discountAmount'];

                              // discount = discount + tempDis;
                                thisRow.find('#deliveryNoteDiscount').val(promotions.combinations[j]['discountAmount']);
                                thisRow.find('#deliveryNoteDiscount').addClass('value');
                                if (thisRow.find('#deliveryNoteDiscount').hasClass('precentage')) {
                                    thisRow.find('#deliveryNoteDiscount').removeClass('precentage');
                                }
                                thisRow.find('.sign').text(companyCurrencySymbol);

                            }

                            if (promotions.combinations[j]['discountType'] == 2) {
                                // var tempDis = ((price * $scope.promotion.combinations[j]['discountAmount']) / 100 ) *  qty;

                                // discount = discount + tempDis;
                                thisRow.find('#deliveryNoteDiscount').val(promotions.combinations[j]['discountAmount']);
                                thisRow.find('#deliveryNoteDiscount').addClass('precentage');
                                if (thisRow.find('#deliveryNoteDiscount').hasClass('value')) {
                                    thisRow.find('#deliveryNoteDiscount').removeClass('value');
                                }
                                thisRow.find('.sign').text('%');

                            }
                        }
                    } else {
                        failPromo = true;
                    }
                }

            } else {
                failPromo = true;
            }

            if (failPromo) {
                if (currentProducts[productID].dEL == 1) {
                    if (currentProducts[productID].dV != 0) {
                        thisRow.find('#deliveryNoteDiscount').val(currentProducts[productID].dV);
                        thisRow.find('#deliveryNoteDiscount').addClass('value');
                        if (thisRow.find('#deliveryNoteDiscount').hasClass('precentage')) {
                            thisRow.find('#deliveryNoteDiscount').removeClass('precentage');
                        }
                        thisRow.find('.sign').text(companyCurrencySymbol);
                    } else {
                        thisRow.find('#deliveryNoteDiscount').val(currentProducts[productID].dPR);
                        thisRow.find('#deliveryNoteDiscount').addClass('precentage');
                        if (thisRow.find('#deliveryNoteDiscount').hasClass('value')) {
                            thisRow.find('#deliveryNoteDiscount').removeClass('value');
                        }
                        thisRow.find('.sign').text('%');
                    }
                } else {
                    thisRow.find('#deliveryNoteDiscount').prop('readonly', true);
                }
            }


            // if (tmpItemQuentity >= promotionProducts[productID].minQty) {
            //     // if (promotionProducts[productID].discountType == 1) {
            //     //     thisRow.find('#deliveryNoteDiscount').val(promotionProducts[productID].discountAmount);
            //     //     thisRow.find('#deliveryNoteDiscount').addClass('value');
            //     //     if (thisRow.find('#deliveryNoteDiscount').hasClass('precentage')) {
            //     //         thisRow.find('#deliveryNoteDiscount').removeClass('precentage');
            //     //     }
            //     //     thisRow.find('.sign').text(companyCurrencySymbol);
            //     // } else {
            //     //     thisRow.find('#deliveryNoteDiscount').val(promotionProducts[productID].discountAmount);
            //     //     thisRow.find('#deliveryNoteDiscount').addClass('precentage');
            //     //     if (thisRow.find('#deliveryNoteDiscount').hasClass('value')) {
            //     //         thisRow.find('#deliveryNoteDiscount').removeClass('value');
            //     //     }
            //     //     thisRow.find('.sign').text('%');
            //     // }
            // } else {
            //     if (currentProducts[productID].dEL == 1) {
            //         if (currentProducts[productID].dV != 0) {
            //             thisRow.find('#deliveryNoteDiscount').val(currentProducts[productID].dV);
            //             thisRow.find('#deliveryNoteDiscount').addClass('value');
            //             if (thisRow.find('#deliveryNoteDiscount').hasClass('precentage')) {
            //                 thisRow.find('#deliveryNoteDiscount').removeClass('precentage');
            //             }
            //             thisRow.find('.sign').text(companyCurrencySymbol);
            //         } else {
            //             thisRow.find('#deliveryNoteDiscount').val(currentProducts[productID].dPR);
            //             thisRow.find('#deliveryNoteDiscount').addClass('precentage');
            //             if (thisRow.find('#deliveryNoteDiscount').hasClass('value')) {
            //                 thisRow.find('#deliveryNoteDiscount').removeClass('value');
            //             }
            //             thisRow.find('.sign').text('%');
            //         }
            //     } else {
            //         thisRow.find('#deliveryNoteDiscount').prop('readonly', true);
            //     }
            // }

            // if (toFloat(promotionProducts[productID].maxQty) <= toFloat(tmpItemQuentity)) {
            //     prompItemMaxQty = promotionProducts[productID].maxQty;
            //     prompItemQtyFlag = true;
            // }
        } else if (priceListItems[productID] !== undefined && parseFloat(priceListItems[productID].itemPrice) > 0) {

        } else {
            if (currentProducts[productID].dEL == 1 && (currentProducts[productID].dV != null || currentProducts[productID].dPR != null)) {
                //current product dsiscount (May changed due to insertion of promotion but there's a validation below)
                var currentDiscount = 0;
                if (thisRow.hasClass('add-row') || (discountEditedManually[thisRow.data('icid')] !== undefined && discountEditedManually[thisRow.data('icid')] == true) || thisRow.hasClass('copied') ||  invoiceRowEdit || invoiceTotalDiscountTrigger || deliveryCEnable || delDiscountTrigger || invBasePromotionDiscount) {
                    currentDiscount = thisRow.find('#deliveryNoteDiscount').val();
                    discountEditedManually[thisRow.data('icid')] = false;
                } else {
                    if (currentProducts[productID].dV != 0) {
                        currentDiscount = currentProducts[productID].dV;
                    } else {
                        currentDiscount = currentProducts[productID].dPR;
                    }
                }
                if (currentProducts[productID].dV != null) {
                    thisRow.find('#deliveryNoteDiscount').val(currentDiscount);
                    thisRow.find('#deliveryNoteDiscount').addClass('value');
                    if (thisRow.find('#deliveryNoteDiscount').hasClass('precentage')) {
                        thisRow.find('#deliveryNoteDiscount').removeClass('precentage');
                    }
                    thisRow.find('.sign').text(companyCurrencySymbol);
                } 

                if (currentProducts[productID].dPR != null){
                    thisRow.find('#deliveryNoteDiscount').val(currentDiscount);
                    thisRow.find('#deliveryNoteDiscount').addClass('precentage');
                    if (thisRow.find('#deliveryNoteDiscount').hasClass('value')) {
                        thisRow.find('#deliveryNoteDiscount').removeClass('value');
                    }
                    thisRow.find('.sign').text('%');
                } 
            } else {
                thisRow.find('#deliveryNoteDiscount').prop('readonly', true);
            }
        }

        if (thisRow.find('#deliveryNoteDiscount').hasClass('value')) {
            var tmpPDiscountvalue = isNaN(thisRow.find('#deliveryNoteDiscount').val()) ? 0 : thisRow.find('#deliveryNoteDiscount').val();
            if (tmpPDiscountvalue > 0) {
                var isSchemeDiscount = false;
                var allocatedSchemeDiscount = null;
                if (thisRow.hasClass('discountSchemeApplied')) {
                    isSchemeDiscount = true;
                    allocatedSchemeDiscount = thisRow.data('discountSchemeVal');
                }
                var validatedDiscount = validateDiscount(productID, 'val', tmpPDiscountvalue, thisRow.find('#unitPrice').val(), isSchemeDiscount, allocatedSchemeDiscount);
                thisRow.find('#deliveryNoteDiscount').val(validatedDiscount.toFixed(2));
                //check max quantity of the promotion and apply max qty discount only
                if (prompItemQtyFlag) {
                    tmpItemCost -= prompItemMaxQty * validatedDiscount;
                } else {
                    tmpItemCost -= tmpItemQuentity * validatedDiscount;
                }
                tmpItemCostChangeFlag = true;
            }
        } else {
            var isSchemeDiscount = false;
            var allocatedSchemeDiscount = null;
            if (thisRow.hasClass('discountSchemeApplied')) {
                isSchemeDiscount = true;
                allocatedSchemeDiscount = thisRow.data('discountSchemeVal');
            }
            var tmpPDiscount = isNaN(thisRow.find('#deliveryNoteDiscount').val()) ? 0 : thisRow.find('#deliveryNoteDiscount').val();
            if (tmpPDiscount > 0) {
                var validatedDiscount = validateDiscount(productID, 'per', tmpPDiscount, tmpItemQuentity, isSchemeDiscount, allocatedSchemeDiscount);
                thisRow.find('#deliveryNoteDiscount').val(validatedDiscount.toFixed(2));
                if (prompItemQtyFlag) {
                    tmpItemCost -= ((tmpItemTotal * toFloat(validatedDiscount) / toFloat(100)) / tmpItemQuentity) * prompItemMaxQty;
                } else {
                    tmpItemCost -= (tmpItemTotal * toFloat(validatedDiscount) / toFloat(100));
                }
                tmpItemCostChangeFlag = true;
            }
        }
        if ($('#total_discount_rate').val() != '' && $('input[name=discount_type]:checked').val() == 'presentage') {
            var invoiceDiscountValue = $('#total_discount_rate').val();
            tmpItemCost -= (tmpItemCost * (toFloat(invoiceDiscountValue) / toFloat(100)));
            tmpItemCostChangeFlag = true;
        }
        var invoiceWisePromotionDiscountValue = 0.00;
        if (counterForPromotion == 0) {
            invoiceTotalValueforPromotion = invoiceTotalValue;
        }
        if (!$.isEmptyObject(promotions) && promotions.promoType == 2 && promotions.discountType == 2) {
            if (toFloat(promotions.minValue) <= toFloat(invoiceTotalValue) && toFloat(invoiceTotalValue) <= toFloat(promotions.maxValue)) {
                invoiceWisePromotionDiscountValue = toFloat(tmpItemCost) * (toFloat(promotions.discountAmount) / toFloat(100));
            } else if (toFloat(invoiceTotalValue) > toFloat(promotions.maxValue)) {
                //if invoice total value is greater than the promotion discount precentage, calculate the discount only for promotion max value
                invoiceWisePromotionDiscountValue = ((toFloat(tmpItemCost)) / 100) * (toFloat(promotions.maxValue) * toFloat(promotions.discountAmount) / toFloat(invoiceTotalValueforPromotion));
                counterForPromotion++;
            }
            tmpItemCostChangeFlag = true;
            promotionDiscountValue = promotions.discountAmount;
            invoiceWisePromotionDiscountType = promotions.discountType;
        }
        tmpItemCost = tmpItemCost - invoiceWisePromotionDiscountValue;
        if (checkedTaxes) {
            currentItemTaxResults = calculateItemCustomTax(tmpItemCost, checkedTaxes);
            currentTaxAmount = (currentItemTaxResults == null) ? null : currentItemTaxResults.tTA;
        } else {
            currentTaxAmount = 0;
            currentItemTaxResults = '';
        }
        var itemCost = toFloat(tmpItemCost) + toFloat(currentTaxAmount);
        productsTax[productID] = currentItemTaxResults;
        //if unit price is zero or discount greater than unit price,Then total may be negative value
        //So Total should be zero

        if (tmpItemCostChangeFlag) {
            productsTotal[productID] = itemCost;
            thisRow.find('#addNewTotal').html(accounting.formatMoney(itemCost));
        } else {
            var rowTotal = (toFloat(thisRow.find('#unitPrice').val()) * toFloat(tmpItemQuentity));
            productsTotal[productID] = rowTotal;
            thisRow.find('#addNewTotal').html(accounting.formatMoney(rowTotal));
        }

        if (productsTax[productID] != null) {
            if (jQuery.isEmptyObject(productsTax[productID].tL)) {
                thisRow.find('#taxApplied').addClass('glyphicon glyphicon-unchecked');
            } else {
                thisRow.find('#taxApplied').removeClass('glyphicon glyphicon-unchecked');
                thisRow.find('#taxApplied').addClass('glyphicon glyphicon-check');
            }
        }
        if (!$.isEmptyObject(promotions)) {
            if (!thisRow.hasClass('add-row')) {
                if (!invoiceRowEdit) {
                    thisRow.find('button.save').trigger('click');
                }
            }
        }
        if (cancelPromotionFlag == true) {
            if (!thisRow.hasClass('add-row') && !thisRow.hasClass('freeAddItem')) {
                if (!invoiceRowEdit) {
                    thisRow.find('button.save').trigger('click');
                }
            }
        }
    }

});


function setJournlaEntryData(JEData){
    $('#journalEntryDate').val(JEData.journalEntryDate).attr('disabled',true);
    $('#journalEntryCode').val(JEData.journalEntryCode).attr('disabled',true);
    $('#journalEntryComment').val(JEData.journalEntryComment);

    $("#journal-entry-body").find('.add_acc').remove();
    $.each(JEData.journalEntryAccounts, function(index, value){
        var $newRow = $($('.temp_acc').clone()).removeClass('temp_acc hidden').addClass('add_acc').appendTo('#journal-entry-body');
        loadDropDownFromDatabase('/accounts-api/search-active-accounts-for-dropdown', "", "", ".accountID", $newRow);

        $('#accountID',$newRow).append("<option value='"+value.financeAccountsID+"'>"+value.financeAccountsName+"</option>");
        $('#accountID',$newRow).val(value.financeAccountsID).selectpicker('refresh');
        $newRow.find('.creditAmount').val(value.journalEntryAccountsCreditAmount).attr('disabled',true);
        $newRow.find('.debitAmount').val(value.journalEntryAccountsDebitAmount).attr('disabled',true);
        $newRow.find('.memo').val(value.journalEntryAccountsMemo);
    });

}

function getCustomerDetails(custID) {
    eb.ajax({
        type: 'POST', url: BASE_URL + '/invoice-api/getCustomerDetails', data: {customerID: custID}, success: function(respond) {
            if (respond.status == true) {
                setCustomerDetails(respond.data);
            } else {
                $('#customerCurrentBalance').val('');
                $('#customerCurrentCredit').val('');
                $('#paymentTerm').val('');
                p_notification(false, respond.msg);
            }
        }
    });
}

var setCustomerDetails = function($customer) {
    if ($customer.customerID) {
        $('#deliveryAddress').val($customer.customerAddress);
        $('#customerCurrentBalance').val(parseFloat($customer.customerCurrentBalance).toFixed(2));
        $('#customerCurrentCredit').val(parseFloat($customer.customerCurrentCredit).toFixed(2));
        $('#customer').empty();
        if($('#customer').val()!= $customer.customerID){
        	$('#customer').
            	    append($("<option></option>").
                	        attr("value", $customer.customerID).
                    	    text($customer.customerName + '-' + $customer.customerCode));
        	$('#customer').selectpicker('refresh');
        }

        if ($customer.customerPaymentTerm == 'null') {
            $('#paymentTerm').val(1);
        } else {
            $('#paymentTerm').val($customer.customerPaymentTerm);
        }

        if ($customer.customerPriceList == null) {
            $('#priceListId').val('');
        } else {
            $('#priceListId').val($customer.customerPriceList);
        }
        $('#paymentTerm').trigger('change');
        $('#priceListId').trigger('change');
    } else {
        $('#customerCurrentBalance').val('');
        $('#customerCurrentCredit').val('');
        $('#paymentTerm').val('');
    }

};
var getSelectedProductDetails = function(locationProductID) {
    var temp;
    eb.ajax({
        type: 'POST', url: BASE_URL + '/productAPI/get-location-product-details',
        data: {locationProductID: locationProductID},
        success: function(respond) {
            temp = respond.data;
            selectProductForTransfer(respond.data);
        }
    });
    return temp;
};

function getCustomerProfilesDetails(customerID) {
    eb.ajax({
        type: 'POST',
        url: BASE_URL + '/customerAPI/getCustomerProfilesByCustomerID',
        data: {customerID: customerID},
        success: function(respond) {
            if (respond.status == true) {
                $('.cus_prof_div').removeClass('hidden');
                $('.cus-prof-select').removeClass('hidden');
                $('.tooltip_for_default_cus').removeClass('hidden');
                $('.default-cus-prof').addClass('hidden');
                setCustomerProfilePicker(respond.data['customerProfileData']);
            } else {
                $('.cus_prof_div').addClass('hidden');
                $('.cus-prof-select').addClass('hidden');
                $('.tooltip_for_default_cus').addClass('hidden');
                $('.default-cus-prof').removeClass('hidden');
            }
        }
    });
}

function setCustomerProfilePicker(data) {
    $('#cusProfileINV').html("<option value=''>" + "Select a Customer Profile" + "</option>");
    $.each(data, function(index, value) {
        // $('#cusProfileINV').append("<option value='" + index + "'>" + value['profName'] + "</option>");
        if (value['isPrimary'] == 1) {
            $('#cusProfileINV').html("<option value='" + index + "'>" + value['profName'] + "</option>");
            cusProfID = index;
        }
    });
    $.each(data, function(index, value) {
        if (value['isPrimary'] == 0) {
            $('#cusProfileINV').append("<option value='" + index + "'>" + value['profName'] + "</option>");
        }
    });
    $('#cusProfileINV').selectpicker('refresh');

    $('#cusProfileINV').on('change', function(e) {
        e.preventDefault();
        if ($(this).val() > 0 && ($(this).val() != cusProfID)) {
            cusProfID = $(this).val();
        } else {
            cusProfID = '';
        }
    });
}



