//js for rating type create view validate and edit functions.
$(document).ready(function() {

    var ratingTypesId = '';
    $('#ratingTypesMethod').on('change', function() {

        $('.starRating').removeClass('hidden');
        if ($(this).val() == 1) {
            $('.ratingSize').html('Number of Stars <font color="red">*</font> :');
            $('#ratingTypesMethodSize').attr('placeholder', 'Eg: 6')
        } else if ($(this).val() == 2) {
            $('.ratingSize').html('Number Range <font color="red">*</font> :');
            $('#ratingTypesMethodSize').attr('placeholder', 'Eg: 1 To 10')
        } else {
            $('.starRating').addClass('hidden');
        }
    });

    $('#ratingTypesList').on('click', '.edit', function() {
        var $row = $(this).parents('tr');
        ratingTypesId = $row.data('ratingtypesid');
        $('#ratingTypesCode').val($row.find('.rtCode').text()).attr('disabled', 'disabled');
        $('#ratingTypesName').val($row.find('.rtName').text());
        $('#ratingTypesDescription').val($row.data('ratingtypesdescription'));
        $('#ratingTypesMethod').val($row.data('ratingtypesmethod')).trigger('change');
        $('.starRating').removeClass('hidden');
        $('#ratingTypesMethodDefinition').val($row.data('ratingtypesmethoddefinition'));
        $('#ratingTypesState').val($row.data('ratingtypesstate'));
        $('#ratingTypesMethodSize').val($row.data('ratingtypessize'));
        $('#ratingTypesSubmit').addClass('hidden');
        $('#ratingTypesUpdate').removeClass('hidden');
    });

    $('#ratingTypesList').on('click', '.state', function() {
        var $this = $(this);
        var $row = $(this).parents('tr');
        var state_msg = "active";
        var state = 1;
        var newState = "glyphicon glyphicon-check";
        var OldState = "glyphicon glyphicon-unchecked";
        if ($(this).children().hasClass('glyphicon-check')) {
            state_msg = "inactive";
            state = 0;
            newState = "glyphicon glyphicon-unchecked";
            OldState = "glyphicon glyphicon-check";
        }
        var rtId = $row.data('ratingtypesid');
        bootbox.confirm('Are you sure you want to ' + state_msg + ' this Rating Types?', function(result) {
            if (result === true) {
                eb.ajax({
                    type: 'POST',
                    url: BASE_URL + '/rating-types-api/updateStateOfRatingTypes',
                    data: {
                        ratingTypesId: rtId,
                        ratingTypesState: state,
                    },
                    success: function(respond) {
                        if (respond.status) {
                            $this.children().removeClass(OldState);
                            $this.children().addClass(newState);
                            $row.data('ratingtypesstate', state)
                        }
                        p_notification(respond.status, respond.msg);
                    }
                });
            }
        });
    });

    $('#ratingTypesList').on('click', '.delete', function() {
        var $this = $(this);
        var $row = $(this).parents('tr');
        var rtId = $row.data('ratingtypesid');
        bootbox.confirm('Are you sure you want to Delete this Rating Types?', function(result) {
            if (result === true) {
                eb.ajax({
                    type: 'POST',
                    url: BASE_URL + '/rating-types-api/deleteRatingTypes',
                    data: {
                        ratingTypesId: rtId,
                    },
                    success: function(respond) {
                        if (respond.status) {
                            $('#ratingTypesList').html(respond.html);
                        }
                        p_notification(respond.status, respond.msg);
                    }
                });
            }
        });
    });

    $('#rating-types-search-form').submit(function(e) {
        e.preventDefault();
        eb.ajax({
            type: 'POST',
            url: BASE_URL + '/rating-types-api/searchRatingTypesBySearchKey',
            data: {
                searchKey: $('#rating-types-search-keyword').val(),
            },
            success: function(respond) {
                if (respond.status) {
                    $('#ratingTypesList').html(respond.html);
                    $('#ratingTypesReset').trigger('click');
                }
            }
        });
    });

    $('#rating-types-form').submit(function(e) {
        e.preventDefault();
        if (ratingTypesValidation()) {
            var ratingTypesCode = $('#ratingTypesCode').val();
            var ratingTypesName = $('#ratingTypesName').val();
            var ratingTypesDescription = $('#ratingTypesDescription').val();
            var ratingTypesMethod = $('#ratingTypesMethod').val();
            var ratingTypesMethodSize = $('#ratingTypesMethodSize').val();
            var ratingTypesMethodDefinition = $('#ratingTypesMethodDefinition').val();
            var ratingTypesState = $('#ratingTypesState').val();

            var URL = BASE_URL + '/rating-types-api/saveRatingTypes';
            if ($('#ratingTypesSubmit').hasClass('hidden')) {
                URL = BASE_URL + '/rating-types-api/updateRatingTypes';
            }
            eb.ajax({
                type: 'POST',
                url: URL,
                data: {
                    ratingTypesId: ratingTypesId,
                    ratingTypesCode: ratingTypesCode,
                    ratingTypesName: ratingTypesName,
                    ratingTypesDescription: ratingTypesDescription,
                    ratingTypesMethod: ratingTypesMethod,
                    ratingTypesMethodSize: ratingTypesMethodSize,
                    ratingTypesMethodDefinition: ratingTypesMethodDefinition,
                    ratingTypesState: ratingTypesState,
                },
                success: function(respond) {
                    if (respond.status) {
                        $('#ratingTypesList').html(respond.html);
                        $('#ratingTypesReset').trigger('click');
                    }
                    p_notification(respond.status, respond.msg);
                }
            });
        }
    });

    $('#ratingTypesReset').on('click', function() {
        $('#ratingTypesSubmit').removeClass('hidden');
        $('#ratingTypesUpdate').addClass('hidden');
        $('.starRating').addClass('hidden');
        $('#ratingTypesCode').attr('disabled', false);
        ratingTypesId = '';
    });

    /**
     * validation of rating types create and update
     */
    function ratingTypesValidation() {
        var ratingTypesCode = $('#ratingTypesCode').val();
        var ratingTypesName = $('#ratingTypesName').val();
        var ratingTypesMethod = $('#ratingTypesMethod').val();
        var ratingTypesMethodSize = $('#ratingTypesMethodSize').val();
        var ratingTypesMethodDefinition = $('#ratingTypesMethodDefinition').val();
        if (ratingTypesCode == "" || ratingTypesCode == null) {
            p_notification(false, eb.getMessage('RATING_TYPES_CODE_CNT_BE_EMPTY'));
            return false;
        } else if (ratingTypesName == "" || ratingTypesName == null) {
            p_notification(false, eb.getMessage('RATING_TYPES_NAME_CNT_BE_EMPTY'));
            return false;
        } else if (ratingTypesMethod == '' || ratingTypesMethod == null) {
            p_notification(false, eb.getMessage('RATING_TYPES_METHOD_CNT_BE_EMPTY'));
            return false;
        } else if (ratingTypesMethod != '' && (ratingTypesMethodSize == null || ratingTypesMethodSize == '')) {
            p_notification(false, eb.getMessage('RATING_TYPES_METHOD_SIZE_CNT_BE_EMPTY'));
            return false;
        } else if(ratingTypesMethod != '' && isNaN(ratingTypesMethodSize)){
        	p_notification(false, eb.getMessage('RATING_TYPES_METHOD_SIZE_SHUD_BE_NUMERIC'));
            return false;
        }else if (ratingTypesMethodDefinition == '' || ratingTypesMethodDefinition == null) {
            p_notification(false, eb.getMessage('RATING_TYPES_METHOD_DEFINITION_CNT_BE_EMPTY'));
            return false;
        } else {
            return true;
        }
    }
});
