var customerOldCreditBalance = 0;
var customerNewCreditBalance = 0;
creditAmounts = new Array();
var customerID = 0;
var dimensionData = {};
var ignoreBudgetLimitFlag = false;


$(document).ready(function() {
    var dimensionArray = {};
    var dimensionTypeID = null;
    var creditNotePaymentsSearch = 'Credit Note';
    function creditNotes() {
        this.creditNoteID = '';
        this.payAmount = '';
        this.settledAmount = '';
        this.paymentMethod = '';
        this.cCurrencyRate = '';
    }

    $('#dimensionView').on('click', function(e) {
        clearDimensionModal();
        var creditNotePaymentID = $('#creditNotePaymentID').val();

        if (!$.isEmptyObject(dimensionData)) {
            $.each(dimensionData[creditNotePaymentID], function(index, val) {
                dimensionArray[index] = val;                
            });
            $.each(dimensionArray, function(index, value) {
                var newTrID = 'tr_' + value.dimensionTypeId;
                var clonedRow = $($('#dimensionPreSample').clone()).attr('id', newTrID).addClass('addedDimensions');
                $("input[name='dimensionType']", clonedRow).val(value.dimensionTypeTxt);
                $("input[name='dimensionValue']", clonedRow).val(value.dimensionValueTxt);
                clonedRow.removeClass('hidden');
                clonedRow.insertBefore('#dimensionPreSample');
                clearAddedNewDimensionRow();
            });
        }
        $('#addDimensionModal').modal('show');
    });

    // dimension modal
    function clearDimensionModal()
    {
        $("#addDimensionModal #dimensionTable tbody .addedDimensions").remove();
        dimensionArray = {};
    }


    $('.dimenisonchange').on('change', function(e) {
        dimensionTypeID = this.value;
        getDimensionValueListByDimensionType(dimensionTypeID, function() {
        });
    });



    function getDimensionValueListByDimensionType(dimensionTypeID, callback) {
        switch (dimensionTypeID) {
            case "job":
                $('.dimensionVal').addClass('hidden');
                $('.dimensionProjectNo').addClass('hidden');
                $('.dimensionJobNo').removeClass('hidden');
                break;
            case "project":
                $('.dimensionVal').addClass('hidden');
                $('.dimensionProjectNo').removeClass('hidden');
                $('.dimensionJobNo').addClass('hidden');
                break;
           default:
                $('.dimensionVal').removeClass('hidden');
                $('.dimensionProjectNo').addClass('hidden');
                $('.dimensionJobNo').addClass('hidden');
                $.ajax({
                    type: 'POST',
                    url: BASE_URL + '/dimension-api/getRelatedDimensionValues',
                    data: {dimensionId: dimensionTypeID},
                    success: function(respond) {
                        //empty options
                        $('#dimensionVal').empty();
                        $('#dimensionVal').append($("<option>", {value: '', html: 'Select dimension value'}));
                        $(respond.data).each(function(index, dimValue) {
                            $.each(dimValue, function(ind, val) {
                                $('#dimensionVal').append($("<option>", {value: ind, html: val.value}));
                            });
                        });
                        callback();
                    }
                });
                break;
        }
    }

    $('#add_dimension').on('click', function(e) {
        e.preventDefault();
        //if plus button is clicked then disabled the button
        //if button is clicked more than one, then it does not continue
        if ($(this).is(':disabled')) {
            return false;
        } else {
            $(this).attr('disabled', true);
            var $thisRow = $(this).parents('tr');
            if ($('.dimenisonchange').val() == 0 || $('.dimenisonchange').val() == '' || $('.dimenisonchange').val() == null) {
                p_notification(false, eb.getMessage('ERR_DIMENSION_SELCET'));
                $(this).attr('disabled', false);
            } else {
                if (dimensionArray[dimensionTypeID]) {
                    p_notification(false, eb.getMessage('ERR_DIMENSION_ALREADY_INSERTED'));
                    $('.dimenisonchange').val('');
                    $('.dimenisonchange').selectpicker('render');
                    $('.dimenisonchange').trigger('change');
                } else {
                    addNewDimensionRow($(this).parents('tr'));
                }
                $(this).attr('disabled', false);
            }
        }
    });

    function addNewDimensionRow(thisRow) {

        var dimensionTypeId = $('.dimenisonchange', thisRow).val();
        var dimensionTypeTxt = $('.dimenisonchange', thisRow).find("option:selected").text();

        switch (dimensionTypeID) {
            case "job":
                var dimensionValueTxt = $('.dimensionJobNo', thisRow).find("option:selected").text();
                var dimensionValueId = $('.dimensionJobNo', thisRow).val();
                break;
            case "project":
                var dimensionValueTxt = $('.dimensionProjectNo', thisRow).find("option:selected").text();
                var dimensionValueId = $('.dimensionProjectNo', thisRow).val();
                break;
           default:
                var dimensionValueTxt = $('.dimensionVal', thisRow).find("option:selected").text();
                var dimensionValueId = $('.dimensionVal', thisRow).val();
                break;
        }
       
        var dimensionDuplicateCheck = false;
        if (!$.isEmptyObject(dimensionArray)) {
            $.each(dimensionArray, function(index, value) {
                if (value.dimensionTypeId == dimensionTypeId) {
                    dimensionDuplicateCheck = true;
                }
            });   
        }
       
        if (dimensionDuplicateCheck) {
            p_notification(false, eb.getMessage("ERR_DIMENSION_ALREADY_INSERTED"));
            return false;
        }

        var newTrID = 'tr_' + dimensionTypeId;
        var clonedRow = $($('#dimensionPreSample').clone()).attr('id', newTrID).addClass('addedDimensions');
        $("input[name='dimensionType']", clonedRow).val(dimensionTypeTxt);
        $("input[name='dimensionValue']", clonedRow).val(dimensionValueTxt);
        clonedRow.removeClass('hidden');
        clonedRow.insertBefore('#dimensionPreSample');
        dimensionArray[dimensionTypeId] = new dimension(dimensionTypeId, dimensionValueId,dimensionTypeTxt , dimensionValueTxt);
        clearAddedNewDimensionRow();
    }


    function dimension(dimensionTypeId, dimensionValueId, dimensionTypeTxt, dimensionValueTxt) {
        this.dimensionTypeId = dimensionTypeId;
        this.dimensionValueId = dimensionValueId;
        this.dimensionTypeTxt = dimensionTypeTxt;
        this.dimensionValueTxt = dimensionValueTxt;
    }

    function clearAddedNewDimensionRow() {
        $('.dimenisonchange').val('');
        $('.dimenisonchange').selectpicker('render');
        $('.dimenisonchange').trigger('change');
    }

    $('.addDimensions').on('click', function(e) {
        e.preventDefault();

        // validate products before closing modal
        if (!dimensionModalValidate(e)) {
            e.stopPropagation();
            return false;
        } else {
            $('#addDimensionModal').modal('hide');
        }
    });

    function dimensionModalValidate(e) {
        var creditNotePaymentID = $('#creditNotePaymentID').val();
        if ($.isEmptyObject(dimensionArray)) {
            dimensionData = {};
            return true;   
        } else {
            var dimensionSingleData = {};
            $.each(dimensionArray, function(index, val) {
                dimensionSingleData[index] = val;
            });
            dimensionData[creditNotePaymentID] = dimensionSingleData;
            return true;
        }
    }

    $("#addDimensionModal #dimensionTable").on('click', '.deleteDimensionType', function(e) {
        e.preventDefault();
        var deleteDTrID = $(this).closest('tr').attr('id');
        var deleteDID = deleteDTrID.split('tr_')[1].trim();
        bootbox.confirm('Are you sure you want to remove this dimension ?', function(result) {
            if (result == true) {
                delete dimensionArray[deleteDID];
                $('#' + deleteDTrID).remove();
            }
        });
    });

    loadDropDownFromDatabase('/job-api/searchJobsForDropdown', "", 0, '.dimensionJobNo');
    $('.dimensionJobNo').selectpicker('refresh');
    $('.dimensionJobNo').on('change', function() {
        var dimensionValue = $(this).val();
    });

    loadDropDownFromDatabase('/api/project/searchProjectsForDropdown', "", 0, '.dimensionProjectNo');
    $('.dimensionProjectNo').selectpicker('refresh');
    $('.dimensionProjectNo').on('change', function() {
        var dimensionValue = $(this).val();
    });

    $('#customerID').selectpicker('hide');
    $('#checkGroup').hide();
    $('#creditGroup').hide();
    $('#bankTransferGroup').hide();
    $('#customCurrencyId').attr('disabled', true);

    $('#creditNotePaymentsSearch').on('change', function() {
        if ($(this).val() == "Credit Note") {
            $('#creditNoteID').selectpicker('show');
            $('#customerID').selectpicker('hide');
            $('#customCurrencyId').val('');
            $('#customCurrencyId').attr('disabled', true);
            $('#customCurrencyId').selectpicker('refresh');
            creditNotePaymentsSearch = 'Credit Note';
            $('#dimensionDiv').addClass('hidden');
        } else {
            $('#dimensionDiv').removeClass('hidden');
            $('#customerID').selectpicker('show');
            $('#creditNoteID').selectpicker('hide');
            $('#customCurrencyId').attr('disabled', false);
            $('#customCurrencyId').selectpicker('refresh');
            creditNotePaymentsSearch = 'Customer Name';
        }
    });

    loadDropDownFromDatabase('/customerAPI/search-customers-for-dropdown', "", 0, '#customerID');
    $('#customerID').selectpicker('refresh');

    $('#customerID').on('change', function() {
        if ($(this).val() > 0 && customerID != $(this).val()) {
            customerID = $(this).val();
            getCreditNoteDetails('customer', customerID);
        } else if($(this).val() == 0 && customerID != null){
            customerID = '';
        }
    });

    var locationID = $('#locationID').val();
    loadDropDownFromDatabase('/credit-note-api/search-active-credit-note-for-dropdown', locationID, 0, '#creditNoteID');

    $('#creditNoteID').selectpicker('refresh');

    $('#creditNoteID').on('change', function() {
        if ($(this).val() > 0 && creditNoteID != $(this).val()) {
            creditNoteID = $(this).val();
            getCreditNoteDetails('creditNote', creditNoteID);
        }else if($(this).val() == 0 && customerID != null){
            creditNoteID = '';
        }
    });


    $(document).on('click', '.edi', function() {
        if ($(this).val() == '') {
            var cNoteID = $(this).parents('tr').attr('id');
            var ccrate = $(this).parents('tr').data('ccrate');
            var leftToPay = accounting.unformat($(this).parent().parent().find('.leftToPay').text());
            customerNewCreditBalance = customerNewCreditBalance - accounting.unformat(leftToPay * ccrate);
            if (customerNewCreditBalance >= 0) {
                creditAmounts[cNoteID] = leftToPay;
                $(this).val(accounting.formatMoney(leftToPay));
                $('#currentCredit').val(customerNewCreditBalance);
                var amount = accounting.unformat($('#amount').val());
                amount = amount + leftToPay;
                $('#amount').val(accounting.formatMoney(amount));
                $('#totalpayment').text(accounting.formatMoney(amount));
            } else {
                customerNewCreditBalance = customerNewCreditBalance + accounting.unformat(leftToPay);
                p_notification(false, eb.getMessage('ERR_CUSTCDBAL_NOT_ENOUGH'));
                $(this).val('');
                creditAmounts[cNoteID] = 0;
            }
        }
    });

    $(document).on('keyup', '.edi', function() {
        var cNoteID = $(this).parents('tr').attr('id');
        var leftToPay = accounting.unformat($(this).parent().parent().find('.leftToPay').text());
        var ccrate = $(this).parents('tr').data('ccrate');
        var givenAmount = $(this).val();
        var amount = 0;
        if (givenAmount == "") {
            givenAmount = 0;
        }
        if (givenAmount < 0) {
            givenAmount = 0.00;
            p_notification(false, eb.getMessage('ERR_CNPAY_CANT_BE_NEGATIVE'));
            $(this).val('');
        }
        if (givenAmount <= leftToPay) {
            customerNewCreditBalance = customerNewCreditBalance + parseFloat(creditAmounts[cNoteID] * ccrate) - givenAmount * ccrate;
            if (customerNewCreditBalance >= 0) {
                amount = accounting.unformat($('#amount').val());
                amount = amount + parseFloat(givenAmount) - parseFloat(creditAmounts[cNoteID]);
                creditAmounts[cNoteID] = givenAmount;
            } else {
                customerNewCreditBalance = customerNewCreditBalance + parseFloat(givenAmount * ccrate);
                amount = accounting.unformat($('#amount').val());
                amount = amount - parseFloat(creditAmounts[cNoteID]);
                p_notification(false, eb.getMessage('ERR_CUSTCDBAL_NOT_ENOUGH'));
                creditAmounts[cNoteID] = 0.00;
                $(this).val('');
            }
        } else {
            customerNewCreditBalance = customerNewCreditBalance + parseFloat(creditAmounts[cNoteID] * ccrate);
            amount = accounting.unformat($('#amount').val());
            amount = amount - parseFloat(creditAmounts[cNoteID]);
            creditAmounts[cNoteID] = 0.00;
            $(this).val('');
            if (isNaN(givenAmount)) {
                p_notification(false, eb.getMessage('ERR_CNPAYAMOUNT_NUMERIC'));
            } else {
                p_notification(false, eb.getMessage('ERR_CNPAYAMOUNT_CANT_BE_MORE_CNAMOUNT'));
            }
        }
        $('#currentCredit').val(customerNewCreditBalance);
        $('#amount').val(accounting.formatMoney(amount));
        $('#totalpayment').text(accounting.formatMoney(amount));
    });

    $('#discountAmount').on('keyup', function() {
        var discount = $(this).val();
        var tamount = accounting.unformat($('#amount').val());
        if (isNaN(discount) || discount < 0) {
            $(this).val('');
            p_notification(false, eb.getMessage('ERR_DISCAMOUNT_SHOUD_POSITIVE_AND_NUMERIC'));
        } else {
            $('#totalpayment').text(accounting.formatMoney(tamount - discount));
        }
    });

    $(document).on('change', '.sel', function() {
        var checkCheque = 0;
        var checkCredit = 0;
        var checkBankRef = 0;
        $('#creditNotesDetails').find('tbody tr').each(function() {
            var paymethod = $(this).find('#paymentMethod').val();
            if (paymethod == 2) {
                checkCheque = 1;
            } else if (paymethod == 3) {
                checkCredit = 1;
            } else if (paymethod == 5) {
                checkBankRef = 1;
            }
        });
        if (checkCheque) {
            $("#checkGroup").show();
        } else {
            $("#checkGroup").hide();
        }

        if (checkBankRef) {
            $('#bankTransferGroup').show();
        } else {
            $('#bankTransferGroup').hide();
        }

        if (checkCredit) {
            $("#creditGroup").show();
        } else {
            $("#creditGroup").hide();
        }
    });

    $('#reset-credit-note-button').on('click', function() {
        $("#customerID").val('').selectpicker('render');
        $("#creditNoteID").val('').selectpicker('render');
        $("#appendtable").children().remove();
        $("#currentBalance").val('');
        $("#currentCredit").val('');
        $("#discountAmount").val('');
        $("#amount").val('');
        $("#memo").val('');
        $("#totalpayment").text('0.00');
        customerID = '';
        creditNoteID = '';
    });

    $('#crediNotePayments').on('click', function() {
        saveCreditNotePayment();
    });

    function saveCreditNotePayment()
    {
        var paymentUrl = BASE_URL + '/credit-note-payments-api/addCreditNotePayment';
        creditNotePayment = {};
        var methods = new Array();
        var flag = true;
        var checkCheque = 0;
        var checkCredit = 0;
        var checkCash = 0;
        var checkBankTransfer = 0;
        var checkLC = 0;
        var checkTT = 0;
        $('#creditNotesDetails').find('tbody tr').each(function() {
            var creditNoteID = this.id;
            if ($(this).find('.edi').val() != '') {
                if ($(this).find('#paymentMethod').val() != 0) {
                    creditNotePayment[creditNoteID] = new creditNotes();
                    creditNotePayment[creditNoteID].creditNoteID = creditNoteID;
                    creditNotePayment[creditNoteID].payAmount = accounting.unformat($(this).find('.payamount').text());
                    creditNotePayment[creditNoteID].settledAmount = accounting.unformat($(this).find('.edi').val());
                    creditNotePayment[creditNoteID].paymentMethod = $(this).find('#paymentMethod').val();
                    creditNotePayment[creditNoteID].cCurrencyRate = $(this).data('ccrate');
                    if($(this).find('#paymentMethod').val() == 1){
                        checkCash = 1;
                    }else if ($(this).find('#paymentMethod').val() == 2) {
                        checkCheque = 1;
                    } else if ($(this).find('#paymentMethod').val() == 3) {
                        checkCredit = 1;
                    }else if($(this).find('#paymentMethod').val() == 5){
                        checkBankTransfer = 1;
                    }else if($(this).find('#paymentMethod').val() == 7){
                        checkLC = 1;
                    }else if($(this).find('#paymentMethod').val() == 8){
                        checkTT = 1;
                    }
                } else {
                    p_notification(false, eb.getMessage('ERR_PLS_SELECT_PAYMETHOD', $(this).find('.code').text()));
                    flag = false;
                }
            }
        });

        if(checkCash == 1){
            methods.push({
                'paymentMethodID':'1',
            });
        }

        if(checkCheque == 1){
            methods.push({
                'paymentMethodID':'2',
                'creditNotePaymentMethodReferenceNumber': $('#checquenumber').val(),
                'creditNotePaymentMethodBank': $('#bank').val(),
            });
        }

        if(checkCredit == 1){
            methods.push({
                'paymentMethodID':'3',
                'creditNotePaymentMethodReferenceNumber':$('#reciptnumber').val(),
                'creditNotePaymentMethodCardID':$('#cardnumber').val(),
            });
        }

        if(checkBankTransfer == 1){
            methods.push({
                'paymentMethodID':'5',
                'creditNotePaymentMethodReferenceNumber': $('#refnumber').val(),
            });
        }

        if(checkLC == 1){
            methods.push({
                'paymentMethodID':'7',
            });
        }

        if(checkTT == 1){
            methods.push({
                'paymentMethodID':'8',
            });
        }

        if ( creditNotePaymentsSearch == 'Credit Note' && ( $('#creditNoteID').val() == '' || $('#creditNoteID').val() == '0') || $('#creditNoteID').val() == null){
            p_notification(false, eb.getMessage('ERR_CNPAY_SELECT_CNCODE'));
            flag = false;
        }else if( creditNotePaymentsSearch == 'Customer Name' && ($('#customerID').val() == '' || $('#customerID').val() == '0' || $('#customerID').val() == null)){
            p_notification(false, eb.getMessage('ERR_CNPAY_SELECT_CUSNAME'));
            flag = false;
        }else if (flag && jQuery.isEmptyObject(creditNotePayment)) {
            p_notification(false, eb.getMessage('ERR_PLS_SET_ONE_CN_FOR_PAYMENT'));
            flag = false;
        }

        if (checkCheque) {
            var checNumber = $('#checquenumber').val();
            var bank = $('#bank').val();
            if (checNumber == null || checNumber == "") {
                p_notification(false, eb.getMessage('ERR_CNPAY_CHEQUE'));
                flag = false;
            } else if (checNumber.length > 8) {
                p_notification(false, eb.getMessage('ERR_CNPAY_CHEQUE_INVALID'));
                flag = false;
            } else if (isNaN(checNumber)) {
                p_notification(false, eb.getMessage('ERR_CNPAY_CHEQUE_NUMR'));
                flag = false;
            } else if (bank == null || bank == "") {
                p_notification(false, eb.getMessage('ERR_CNPAY_BANK_NAME'));
                flag = false;
            }
        }
        if (checkCredit) {
            var reciptNumber = $('#reciptnumber').val();
            var cardNumber = $('#cardnumber').val();
            if (reciptNumber == null || reciptNumber == "") {
                p_notification(false, eb.getMessage('ERR_CNPAY_RECNO'));
                flag = false;
            } else if (reciptNumber.length > 12) {
                p_notification(false, eb.getMessage('ERR_CNPAY_RECIPTNO_VALIDITY'));
                flag = false;
            } else if (isNaN(cardNumber)) {
                p_notification(false, eb.getMessage('ERR_CNPAY_CRDTCARD_NUMR'));
                flag = false;
            }
        }
        if (flag && $('#amount').val().replace(/,/g, '') <= 0) {
            p_notification(false, eb.getMessage('ERR_PAY_FILL_AMOUNT'));
            return false;
        }
        if (flag) {
            $('.main_div').addClass('Ajaxloading');
            eb.ajax({
                type: 'post',
                url: paymentUrl,
                data: {
                    customerID: customerID,
                    customerCredit: customerNewCreditBalance,
                    creditNotePaymentsData: creditNotePayment,
                    creditNotepaymentCode: $('#creditNotePaymentID').val().replace(/\s/g, ""),
                    date: $('#currentdate').html().replace(/\s/g, ""),
                    paymentTerm: $('#paymentTerm').val(),
                    amount: $('#amount').val().replace(/,/g, ''),
                    discount: $('#discountAmount').val().replace(/,/g, ''),
                    memo: $('#memo').val(),
                    locationID: $('#locationID').val(),
                    customCurrencyId: $('#customCurrencyId').val(),
                    PaymentMethodDetails : JSON.stringify(methods),
                    dimensionData: dimensionData,
                    ignoreBudgetLimit : ignoreBudgetLimitFlag
                },
                dataType: "json",
                async: false,
                success: function(data) {
                    if (data.status == true) {
                        var fileInput = document.getElementById('documentFiles');
                        if(fileInput.files.length > 0) {
                            var form_data = false;
                            if (window.FormData) {
                                form_data = new FormData();
                            }
                            form_data.append("documentID", data.data['creditNotePaymetID']);
                            form_data.append("documentTypeID", 17);
                            
                            for (var i = 0; i < fileInput.files.length; i++) {
                                form_data.append("files[]", fileInput.files[i]);
                            }

                            eb.ajax({
                                url: BASE_URL + '/store-files',
                                type: 'POST',
                                processData: false,
                                contentType: false,
                                data: form_data,
                                success: function(res) {
                                }
                            });
                        }

                        if (checkCash != 0 || checkCredit != 0 || checkCheque != 0 || checkBankTransfer != 0 || checkLC != 0 || checkTT != 0) {
                            var addpayurl = BASE_URL + '/credit-note-payments-api/addCreditNotePaymentMethodNumbers';
                            var param = {
                                creditNotePaymentID: data.data['creditNotePaymetID'],
                                creditNotePaymentMethodDetails: JSON.stringify(methods)
                            };
                            var addcreditcardandchecque = eb.post(addpayurl, param);
                            addcreditcardandchecque.done(function(value) {
                                p_notification(true, eb.getMessage('SUCC_CNPAY_ADDPAY', data.data['creditNotePaymentCode']));
                                documentPreview(BASE_URL + '/credit-note-payments/viewCreditNotePaymentReceipt/' + data.data['creditNotePaymetID'], 'documentpreview', "/credit-note-payments-api/send-email", function($preview) {
                                    $preview.on('hidden.bs.modal', function() {
                                        if (!$("#preview:visible").length) {
                                            window.location.reload();
                                        }
                                    });
                                });
                            });
                        } else {
                            p_notification(true, eb.getMessage('SUCC_CNPAY_ADDPAY', data.data['creditNotePaymentCode']));
                            documentPreview(BASE_URL + '/credit-note-payments/viewCreditNotePaymentReceipt/' + data.data['creditNotePaymetID'], 'documentpreview', "/credit-note-payments-api/send-email", function($preview) {
                                $preview.on('hidden.bs.modal', function() {
                                    if (!$("#preview:visible").length) {
                                        window.location.reload();
                                    }
                                });
                            });
                        }
                    } else {
                        if (data.data == "NotifyBudgetLimit") {
                            bootbox.confirm(data.msg+' ,Are you sure you want to save this creditNote payment ?', function(result) {
                                if (result == true) {
                                    ignoreBudgetLimitFlag = true;
                                    saveCreditNotePayment();
                                } else {
                                    setTimeout(function(){ 
                                        location.reload();
                                    }, 3000);
                                }
                            });
                        } else {
                            p_notification(false, data.msg);
                            $('.main_div').removeClass('Ajaxloading');
                        }
                    }
                },
            });
        }
    }

    function getCreditNoteDetails(type, id) {
        $("#appendtable").children().remove();
        $("#currentBalance").val('');
        $("#currentCredit").val('');
        $("#discountAmount").val('');
        $("#memo").val('');
        $('#amount').val('');
        $('#totalpayment').text('0.00');
        var getDetailsUrl;
        var data;
        if (type == 'customer') {
            getDetailsUrl = BASE_URL + '/credit-note-payments-api/getCustomerRelatedCreditNotes';
            data = {
                customerID: id,
                customCurrencyId: $('#customCurrencyId').val(),
            };
        } else if (type == 'creditNote') {
            getDetailsUrl = BASE_URL + '/credit-note-payments-api/getCreditNoteDetailsBycreditNoteID';
            data = {
                creditNoteID: id,
            };
        }
        eb.ajax({
            type: 'post',
            url: getDetailsUrl,
            data: data,
            success: function(value) {
                if (value.status) {
                    $('#appendtable').html(value.html);
                    $('#currentBalance').val(accounting.formatMoney(value.data.customer.customerCurrentBalance));
                    $('#currentCredit').val(accounting.formatMoney(value.data.customer.customerCurrentCredit));
                    customerID = value.data.customer.customerID;
                    customerNewCreditBalance = customerOldCreditBalance = value.data.customer.customerCurrentCredit;
                    if (type == 'creditNote') {
                        $('#customCurrencyId').val(value.data.customCurrencyId);
                        $('#customCurrencyId').selectpicker('render');
                        $('#customCurrencyId').trigger('change');
                    }
                } else {
                    p_notification(false, value.msg);
                }
            },
            async: false
        });
    }

    $('#customCurrencyId').on('change', function() {
        if ($('#creditNotePaymentsSearch').val() == "Customer Name" && customerID != 0) {
            getCreditNoteDetails('customer', customerID);
        }
        eb.ajax({
            type: 'POST',
            url: BASE_URL + '/currencySymbol/getCustomCurrencyByCustomCurrencyId',
            data: {customCurrencyId: $(this).val()},
            success: function(respond) {
                if (respond.status == true) {
                    $('.cCurrency').text(respond.data.currencySymbol);
                    customCurrencyRate = parseFloat(respond.data.currencyRate);
                }
            }
        });
    });

});