/**
 * @author ASHAN <ashan@thinkcube.com>
 * This file contains delivey note edit js code
 */

var locationIn;
var customerID;
var batchProducts = {};
var batchSerialProducts = {};
var serialProducts = {};
var checkSubProduct = {};
// var checkDeliverySubProduct = {};
var deliverProducts = {};
var checkDeliverProducts = {};
var productsTotal = {};
var productsTax = {};
var deliverSubProducts = {};
var SubProductsQty = {};
var currentProducts = {};
var selectedPID;
var locationID;
var customCurrencyRate = 0;
var cusProfID;
var insertedProducts = {};
var insertedSubProducts = {};
var previousDeliveryNoteTotal = 0;
var salesOrderID;
var deletedBatchProducts = {};
var dimensionData = {};
var uploadedAttachments = {};
var deletedAttachmentIds = [];
var deletedAttachmentIdArray = [];
var ignoreBudgetLimitFlag = false;

$(document).ready(function() {
	var decimalPoints = 0;
    var currentItemTaxResults;
    var currentTaxAmount;
    var totalTaxList = {};
    var $productTable = $("#productTable");
    var $addRowSample = $('tr.add-row.sample.hidden', $productTable);
    var $scanSubmit = false;
    var priceListItems = [];
    var priceListId = '';
    var rowincrementID = 1;
    var dimensionArray = {};
    var dimensionTypeID = null;

	var deliveryNoteId = $('#deliveryNoteId').val();

	loadDeliveryNoteData(deliveryNoteId);

    $('#dimensionView').on('click', function(e) {
        clearDimensionModal();
        var deliveryNoteNo = $('#deliveryNoteNo').val();

        if (!$.isEmptyObject(dimensionData)) {
            $.each(dimensionData[deliveryNoteNo], function(index, val) {
                dimensionArray[index] = val;                
            });
            $.each(dimensionArray, function(index, value) {
                var newTrID = 'tr_' + value.dimensionTypeId;
                var clonedRow = $($('#dimensionPreSample').clone()).attr('id', newTrID).addClass('addedDimensions');
                $("input[name='dimensionType']", clonedRow).val(value.dimensionTypeTxt);
                $("input[name='dimensionValue']", clonedRow).val(value.dimensionValueTxt);
                clonedRow.removeClass('hidden');
                clonedRow.insertBefore('#dimensionPreSample');
                clearAddedNewDimensionRow();
            });
        }
        $('#addDimensionModal').modal('show');
    });

    // dimension modal
    function clearDimensionModal()
    {
        $("#addDimensionModal #dimensionTable tbody .addedDimensions").remove();
        dimensionArray = {};
    }


    $('.dimenisonchange').on('change', function(e) {
        dimensionTypeID = this.value;
        getDimensionValueListByDimensionType(dimensionTypeID, function() {
        });
    });

    $('#viewUploadedFiles').on('click', function(e) {
        $('#doc-attach-table tbody tr').remove();
        $('#doc-attach-table tfoot div').remove();
        $('#doc-attach-table tbody div').remove();

        if (!$.isEmptyObject(uploadedAttachments)) {
            $('#doc-attach-table thead tr').removeClass('hidden');
            $.each(uploadedAttachments, function(index, value) {
                if (!deletedAttachmentIds.includes(value['documentAttachemntMapID'])) {
                    tableBody = "<tr id="+value['documentAttachemntMapID']+" data-attachId=" + value['documentAttachemntMapID'] + "><td><a href="+value.docLink+">"+value.documentRealName+"</a></td><td class='text-center'><span class='glyphicon glyphicon-trash delete_attachment' style='cursor:pointer'></span></td></tr>";
                    $('#doc-attach-table tbody').append(tableBody);
                }
            });
        } else {
            $('#doc-attach-table thead tr').addClass('hidden');
            var noDataFooter = "<div class= ' col-lg-6 text-right'><label><b>No Data</b></label>";
            $('#doc-attach-table tbody').append(noDataFooter);
        }
        $('#attach_view_footer').removeClass('hidden');
        $('#viewAttachmentModal').modal('show');
    });

    $('#doc-attach-table').on('click', '.delete_attachment', function() {
        var attachementID = $(this).parents('tr').attr('data-attachid');
        bootbox.confirm('Are you sure you want to remove this attachemnet?', function(result) {
            if (result == true) {
                $('#' + attachementID).remove();
                deletedAttachmentIdArray.push(attachementID);
            }
        });
    });

    $('#viewAttachmentModal').on('click', '#saveAttachmentEdit', function(event) {
        event.preventDefault();

        $.each(deletedAttachmentIdArray, function(index, val) {
            deletedAttachmentIds.push(val);
        });

        $('#viewAttachmentModal').modal('hide');
    });


    function getDimensionValueListByDimensionType(dimensionTypeID, callback) {
        switch (dimensionTypeID) {
            case "job":
                $('.dimensionVal').addClass('hidden');
                $('.dimensionProjectNo').addClass('hidden');
                $('.dimensionJobNo').removeClass('hidden');
                break;
            case "project":
                $('.dimensionVal').addClass('hidden');
                $('.dimensionProjectNo').removeClass('hidden');
                $('.dimensionJobNo').addClass('hidden');
                break;
           default:
                $('.dimensionVal').removeClass('hidden');
                $('.dimensionProjectNo').addClass('hidden');
                $('.dimensionJobNo').addClass('hidden');
                $.ajax({
                    type: 'POST',
                    url: BASE_URL + '/dimension-api/getRelatedDimensionValues',
                    data: {dimensionId: dimensionTypeID},
                    success: function(respond) {
                        //empty options
                        $('#dimensionVal').empty();
                        $('#dimensionVal').append($("<option>", {value: '', html: 'Select dimension value'}));
                        $(respond.data).each(function(index, dimValue) {
                            $.each(dimValue, function(ind, val) {
                                $('#dimensionVal').append($("<option>", {value: ind, html: val.value}));
                            });
                        });
                        callback();
                    }
                });
                break;
        }
    }

    $('#add_dimension').on('click', function(e) {
        e.preventDefault();
        //if plus button is clicked then disabled the button
        //if button is clicked more than one, then it does not continue
        if ($(this).is(':disabled')) {
            return false;
        } else {
            $(this).attr('disabled', true);
            var $thisRow = $(this).parents('tr');
            if ($('.dimenisonchange').val() == 0 || $('.dimenisonchange').val() == '' || $('.dimenisonchange').val() == null) {
                p_notification(false, eb.getMessage('ERR_DIMENSION_SELCET'));
                $(this).attr('disabled', false);
            } else {
                if (dimensionArray[dimensionTypeID]) {
                    p_notification(false, eb.getMessage('ERR_DIMENSION_ALREADY_INSERTED'));
                    $('.dimenisonchange').val('');
                    $('.dimenisonchange').selectpicker('render');
                    $('.dimenisonchange').trigger('change');
                } else {
                    addNewDimensionRow($(this).parents('tr'));
                }
                $(this).attr('disabled', false);
            }
        }
    });

    function addNewDimensionRow(thisRow) {

        var dimensionTypeId = $('.dimenisonchange', thisRow).val();
        var dimensionTypeTxt = $('.dimenisonchange', thisRow).find("option:selected").text();

        switch (dimensionTypeID) {
            case "job":
                var dimensionValueTxt = $('.dimensionJobNo', thisRow).find("option:selected").text();
                var dimensionValueId = $('.dimensionJobNo', thisRow).val();
                break;
            case "project":
                var dimensionValueTxt = $('.dimensionProjectNo', thisRow).find("option:selected").text();
                var dimensionValueId = $('.dimensionProjectNo', thisRow).val();
                break;
           default:
                var dimensionValueTxt = $('.dimensionVal', thisRow).find("option:selected").text();
                var dimensionValueId = $('.dimensionVal', thisRow).val();
                break;
        }
       
        var dimensionDuplicateCheck = false;
        if (!$.isEmptyObject(dimensionArray)) {
            $.each(dimensionArray, function(index, value) {
                if (value.dimensionTypeId == dimensionTypeId) {
                    dimensionDuplicateCheck = true;
                }
            });   
        }
       
        if (dimensionDuplicateCheck) {
            p_notification(false, eb.getMessage("ERR_DIMENSION_ALREADY_INSERTED"));
            return false;
        }

        var newTrID = 'tr_' + dimensionTypeId;
        var clonedRow = $($('#dimensionPreSample').clone()).attr('id', newTrID).addClass('addedDimensions');
        $("input[name='dimensionType']", clonedRow).val(dimensionTypeTxt);
        $("input[name='dimensionValue']", clonedRow).val(dimensionValueTxt);
        clonedRow.removeClass('hidden');
        clonedRow.insertBefore('#dimensionPreSample');
        dimensionArray[dimensionTypeId] = new dimension(dimensionTypeId, dimensionValueId,dimensionTypeTxt , dimensionValueTxt);
        clearAddedNewDimensionRow();
    }


    function dimension(dimensionTypeId, dimensionValueId, dimensionTypeTxt, dimensionValueTxt) {
        this.dimensionTypeId = dimensionTypeId;
        this.dimensionValueId = dimensionValueId;
        this.dimensionTypeTxt = dimensionTypeTxt;
        this.dimensionValueTxt = dimensionValueTxt;
    }

    function clearAddedNewDimensionRow() {
        $('.dimenisonchange').val('');
        $('.dimenisonchange').selectpicker('render');
        $('.dimenisonchange').trigger('change');
    }

    $('.addDimensions').on('click', function(e) {
        e.preventDefault();

        // validate products before closing modal
        if (!dimensionModalValidate(e)) {
            e.stopPropagation();
            return false;
        } else {
            $('#addDimensionModal').modal('hide');
        }
    });

    function dimensionModalValidate(e) {
        var deliveryNoteNo = $('#deliveryNoteNo').val();
        if ($.isEmptyObject(dimensionArray)) {
            dimensionData = {};
            return true;   
        } else {
            var dimensionSingleData = {};
            $.each(dimensionArray, function(index, val) {
                dimensionSingleData[index] = val;
            });
            dimensionData[deliveryNoteNo] = dimensionSingleData;
            return true;
        }
    }

    $("#addDimensionModal #dimensionTable").on('click', '.deleteDimensionType', function(e) {
        e.preventDefault();
        var deleteDTrID = $(this).closest('tr').attr('id');
        var deleteDID = deleteDTrID.split('tr_')[1].trim();
        bootbox.confirm('Are you sure you want to remove this dimension ?', function(result) {
            if (result == true) {
                delete dimensionArray[deleteDID];
                $('#' + deleteDTrID).remove();
            }
        });
    });

    loadDropDownFromDatabase('/job-api/searchJobsForDropdown', "", 0, '.dimensionJobNo');
    $('.dimensionJobNo').selectpicker('refresh');
    $('.dimensionJobNo').on('change', function() {
        var dimensionValue = $(this).val();
    });

    loadDropDownFromDatabase('/api/project/searchProjectsForDropdown', "", 0, '.dimensionProjectNo');
    $('.dimensionProjectNo').selectpicker('refresh');
    $('.dimensionProjectNo').on('change', function() {
        var dimensionValue = $(this).val();
    });

	function loadDeliveryNoteData(deliveryNoteId){
		eb.ajax({
            type: 'POST',
            url: BASE_URL + '/delivery-note-api/getDeliveryNoteEditDetails',
            data: {deliveryNoteID: deliveryNoteId},
            success: function(respond) {
            	if (respond.status) {
                    if (respond.data.inactiveItemFlag) {
                        p_notification(false, respond.data.errorMsg);
                        setTimeout(function(){ 
                            window.location.assign(BASE_URL + "/delivery-note/view")
                        }, 3000);
                        return false;
                    }
            		priceListId = respond.data.deliveryNote.priceListId;
                    companyCurrencySymbol = respond.data.currencySymbol;
                    if (!$.isEmptyObject(respond.data.dimensionData)) {
                        dimensionData = respond.data.dimensionData;
                    }

                    if (!$.isEmptyObject(respond.data.uploadedAttachments)) {
                        uploadedAttachments = respond.data.uploadedAttachments;
                    }

                    $('#priceListId').val(priceListId).attr('disabled','disabled').selectpicker('refresh').trigger('change');
                    $('#customer').attr('disabled','disabled');
                    $('.cCurrency').text(companyCurrencySymbol);
                    $('#deliveryNoteNo').attr('readonly', true);

                    deliveryNoteTotalAmount = respond.data.deliveryNote.deliveryNotePriceTotal;
                    deliveryNoteID = (respond.data.deliveryNote.deliveryNoteID == null)?'':respond.data.deliveryNote.deliveryNoteID;
                    var custCurrencyId = respond.data.deliveryNote.customCurrencyId;
                    var custCurrencyRate = respond.data.deliveryNote.deliveryNoteCustomCurrencyRate;
                    customCurrencyRate = (custCurrencyRate != 0) ? parseFloat(custCurrencyRate) : 1;

                    $('#customCurrencyId').val(custCurrencyId).trigger('change');
                    $('#customCurrencyId').attr('disabled', true);
                    $("#deliveryNoteAddress").val(respond.data.deliveryNote.deliveryNoteAddress);

                    customerID = respond.data.deliveryNote.customerID;
                    salesOrderID = respond.data.deliveryNote.salesOrderID;
                    $('#customer').empty();
        			$('#customer').
                		append($("<option></option>").
                        attr("value", customerID).
                        text(respond.data.customer.customerName + '-' + respond.data.customer.customerCode));
        			$('#customer').selectpicker('refresh');

        			$('.cus_prof_div').removeClass('hidden');
                	$('.cus-prof-select').removeClass('hidden');
                	$('.default-cus-prof').addClass('hidden');
                	$('.tooltip_for_default_cus').removeClass('hidden');
        			setCustomerProfilePicker(respond.data.customer.customerProfiles);
        			$('#cusProfileDN').val(respond.data.deliveryNote.customerProfileID).selectpicker('refresh');
                    cusProfID = respond.data.deliveryNote.customerProfileID;


                    $("#deliveryNoteNo").val(respond.data.deliveryNote.deliveryNoteCode);
                    $("#deliveryDate").val(respond.data.deliveryNote.deliveryNoteDeliveryDate);

                    $('#salesPersonID').val(respond.data.deliveryNote.salesPersonID).attr('disabled', false);
                    if (respond.data.deliveryNote.salesPersonID != 0) {
                        $('#salesPersonID').attr('disabled', false);
                    }

                    $('#salesPersonID').selectpicker('refresh');

                    $('#comment').val(respond.data.deliveryNote.deliveryNoteComment);
                    previousDeliveryNoteTotal = respond.data.deliveryNote.deliveryNotePriceTotal/customCurrencyRate;

                    currentProducts = respond.data.products;
                    deliveryNoteProducts = respond.data.deliveryNoteProduct;

                    var deliveryCharge = parseFloat(respond.data.deliveryNote.deliveryNoteCharge).toFixed(2) / customCurrencyRate;
                    var thisPreviousTopVals = {
                        customer: customerID,
                        customerProfileID: respond.data.deliveryNote.customerProfileID,
                        deliveryNoteIssueDate: respond.data.deliveryNote.deliveryNoteDeliveryDate,
                        salesPerson: respond.data.deliveryNote.salesPersonID,
                        custCurrency: custCurrencyId,
                        deliveryCharge: deliveryCharge,
                        deliveryNoteDeliveryAddress: respond.data.deliveryNote.deliveryNoteAddress,
                        deliveryNoteComment: respond.data.deliveryNote.deliveryNoteComment,
                    };
                    insertedDeliveryNoteTopVals = thisPreviousTopVals;

                    //product related data apppend in here
                    $.each(respond.data.deliveryNoteProduct, function(index, value) {
                        $.each(currentProducts, function(pIDD, dataVal) {
                            var res = index.split("_");
                            if (res[0]==pIDD) {
                                if (dataVal.batch != "") {
                                    $.each(dataVal.batch, function(bId, bVal) {
                                        $.each(value.subProduct, function(sId, sVal) {
                                            if (sVal.productBatchID == bId) {
                                               bVal.PBQ = parseFloat(bVal.PBQ )+ parseFloat(sVal.deliveryNoteSubProductQuantity);
                                            }
                                        });
                                    });
                                }
                            }
                        });

                        var $currentRow = getAddRow();
                        $currentRow.addClass('deledit');
                        clearProductRow($currentRow);
                        var Row_inc_id = rowincrementID;
                        $currentRow.data('proIncID',Row_inc_id);
        				rowincrementID++;
        				var productTax = new Array();
        				for (var i in currentProducts[value.productID].tax) {
        					if (value.tax) {
        						for (var t in value.tax) {
        							if (value.tax.hasOwnProperty(i)) {
        								productTax[t] = {
        									checked: true,
        									deliveryNoteTaxAmount: currentProducts[value.productID].tax[t].tP,
        									deliveryNoteTaxName: currentProducts[value.productID].tax[t].tN,
        									deliveryNoteTaxPrecentage: currentProducts[value.productID].tax[t].tP,
        								}
        							} else {
        								productTax[i] = {
        									checked: false,
        									deliveryNoteTaxAmount: currentProducts[value.productID].tax[i].tP,
        									deliveryNoteTaxName: currentProducts[value.productID].tax[i].tN,
        									deliveryNoteTaxPrecentage: currentProducts[value.productID].tax[i].tP,
        								}
        							}
        						}
        					} else {
        						productTax[i] = {
        							checked: false,
        							deliveryNoteTaxAmount: currentProducts[value.productID].tax[i].tP,
        							deliveryNoteTaxName: currentProducts[value.productID].tax[i].tN,
        							deliveryNoteTaxPrecentage: currentProducts[value.productID].tax[i].tP,
        						}
        					}
        				}

                        //product related tax apply in here
                        if (productTax) {
                        	$currentRow.find('#taxApplied').removeClass('glyphicon glyphicon-unchecked');
                        	$currentRow.find('#taxApplied').addClass('glyphicon glyphicon-check');
                        	$currentRow.find('.tempLi').remove();
                        	for (var i in productTax) {
                        		var clonedLi = $($currentRow.find('#sampleLi').clone()).removeClass('hidden').attr('id', 'li_' + i).addClass("tempLi");
                        		if (productTax[i].checked) {
                        			clonedLi.children(".taxChecks").attr('id', value.productID + '_' + i).prop('checked', true).addClass('addNewTaxCheck');
                        		} else {
                        			clonedLi.children(".taxChecks").attr('id', value.productID + '_' + i).prop('checked', false).addClass('addNewTaxCheck');
                        		}
                        		clonedLi.children(".taxName").html('&nbsp&nbsp' + productTax[i].deliveryNoteTaxName + '&nbsp&nbsp' + productTax[i].deliveryNoteTaxPrecentage + '%').attr('for', value.productID + '_' + i);
                        		clonedLi.insertBefore($currentRow.find('#sampleLi'));
                        	}
                        }

                        var deliveryNoteQuantity = getProductQuantityToBeDeliveryNote(respond.data.deliveryNoteProduct,value.productID);
                        //product related data append in here
                        $currentRow.data('id', value.productID);
                        $currentRow.data('stockupdate', true);
                        $currentRow.data('doctypeid', value.documentTypeID);
                        $currentRow.data('docid', value.deliveryNoteProductDocumentID);
                        $("#itemCode", $currentRow).empty();
                        $("#itemCode", $currentRow).
                        append($("<option></option>").
                        	attr("value", value.productID).
                        	text(value.productName + '-' + value.productCode));
                        $("#itemCode", $currentRow).val(value.productID).prop('disabled', 'disabled');
                        $("#itemCode", $currentRow).data('PT', value.productType);
                        $("#itemCode", $currentRow).data('PC', value.productCode);
                        $("#itemCode", $currentRow).data('PN', value.productName);
                        // $("#itemCode", $currentRow).data('locationproductid', locationProductID);
                        // $("#itemCode", $currentRow).data('locationid', locationID);
                        // $("#itemCode", $currentRow).data('GiftCard', giftCard);
                        // $("#itemCode", $currentRow).selectpicker('refresh');
                        $("input[name='availableQuantity']", $currentRow).parent().addClass('input-group');
                        $("input[name='deliverQuanity']", $currentRow).parent().addClass('input-group');
                        //this calculation is done because of available qty show deducting deliverQuanity from availableQuantity
                        //when loading products
                        var availableQty = parseFloat(currentProducts[value.productID].LPQ) + parseFloat(deliveryNoteQuantity);
                        availableQty = (value.productType == 2) ? 0 : availableQty;

                        if (value.productType != 2) {
                        	$("input[name='availableQuantity']", $currentRow).val(availableQty).prop('readonly', true).addUom(currentProducts[value.productID].uom);
                        } else {
                        	$("input[name='availableQuantity']", $currentRow).css("display", "none");
                        }

                        $("input[name='deliverQuanity']", $currentRow).val(value.deliveryNoteProductQuantity).addUom(currentProducts[value.productID].uom);
                        $("input[name='unitPrice']", $currentRow).val(value.deliveryNoteProductPrice / customCurrencyRate).addUomPrice(currentProducts[value.productID].uom);
                        // $(".itemDescText", $currentRow).text(value.productDescription);

                        var baseUom;
                        for (var j in currentProducts[value.productID].uom) {
                            if (currentProducts[value.productID].uom[j].pUBase == 1) {
                                baseUom = currentProducts[value.productID].uom[j].uomID;
                            }
                        }

                        if (value.deliveryNoteProductDiscountType == 'value') {
                        	$("input[name='discount']", $currentRow).val(value.deliveryNoteProductDiscount / customCurrencyRate).addUomPrice(currentProducts[value.productID].uom, baseUom);
                            $("input[name='discount']", $currentRow).siblings('.uom-price-select').find('.uom_price_drop_down').attr('disabled', true);
                        	$("input[name='discount']", $currentRow).addClass('value');
                        	$(".sign", $currentRow).text(companyCurrencySymbol);
                        } else if (value.deliveryNoteProductDiscountType == 'precentage') {
                        	$("input[name='discount']", $currentRow).val(value.deliveryNoteProductDiscount);
                        	$("input[name='discount']", $currentRow).addClass('precentage');
                        	$(".sign", $currentRow).text('%');
                        }
                        $('#unitPrice',$currentRow).siblings('.uomPrice').attr("id", "itemUnitPrice")

                        delDiscountTrigger = true;
                        var previousProductDiscount = value.deliveryNoteProductDiscount ? value.deliveryNoteProductDiscount : null;
                        calculateItemTotal($currentRow,value.productID);

                        var thisVals = {
                        	productID: $currentRow.data('id'),
                        	productIncID: $currentRow.data('proIncID'),
                        	productCode: $("#itemCode", $currentRow).data('PC'),
                        	productName: $("#itemCode", $currentRow).data('PN'),
                        	productPrice: $("input[name='unitPrice']", $currentRow).val(),
                        	productDiscount: $("input[name='discount']", $currentRow).val(),
                        	productDiscountType: value.deliveryNoteProductDiscountType,
                        	productTotal: productsTotal[Row_inc_id],
                        	pTax: productsTax[Row_inc_id],
                        	productType: $("#itemCode", $currentRow).data('PT'),
                        	stockUpdate: $currentRow.data('stockupdate'),
                        	documentTypeID: value.documentTypeID,
            				documentID: value.deliveryNoteProductDocumentID,
                            availableQuantity: {
                            	qty: $("input[name='availableQuantity']", $currentRow).val(),
                            },
                            deliverQuantity: {
                            	qty: $("input[name='deliverQuanity']", $currentRow).val(),
                            },
                        };
                        insertedProducts[index] = thisVals;

                        //if product has sub products those was append in here to deliveryNote
                        if ($.isEmptyObject(currentProducts[value.productID].batch) &&
                        	$.isEmptyObject(currentProducts[value.productID].serial) &&
                        	$.isEmptyObject(currentProducts[value.productID].batchSerial) && $.isEmptyObject(value.subProduct)) {
                        	$currentRow.removeClass('subproducts');
                        	$("td[colspan]", $currentRow).attr('colspan', 2);
                        	$('.edit-modal', $currentRow).parent().addClass('hidden');
                   		} else {
                    		var subProducts = [];
                    		var selectedProducts = [];
                    		var subProBatchIds = [];
                    		var qtyTotal = 0;
                    		$.each(value.subProduct, function(ind, val) {
                    			var thisSubProduct = {};

                    			if (val.productBatchID) {
                    				thisSubProduct.batchID = val.productBatchID;
                    			} else {
                    				thisSubProduct.batchID = undefined;
                    			}

                    			if (val.productSerialID) {
                    				thisSubProduct.serialID = val.productSerialID;
                    				subProBatchIds.push(thisSubProduct.serialID);
                    			} else {
                    				thisSubProduct.serialID = undefined;
                    			}
                    			thisSubProduct.qtyByBase = val.deliveryNoteSubProductQuantity;
                    			thisSubProduct.addedQty = val.deliveryNoteSubProductQuantity;
                    			thisSubProduct.warranty = val.deliveryNoteSubProductsWarranty;

                    			subProducts.push($.extend({}, thisSubProduct));
                    			selectedProducts.push($.extend({}, thisSubProduct));
                    			qtyTotal += val.deliveryNoteSubProductQuantity;

                    			if (deliverSubProducts[index] === undefined) {
                    				deliverSubProducts[index] = new Array();
	                    		}

	                    		// if (deliverSubProducts[index] === undefined) {
                    			// 	deliverSubProducts[index] = new Array();
	                    		// }
	                    		// checkDeliverySubProduct
                    			// if(subProductsForCheck[value.productID] === undefined){
                    			// 	subProductsForCheck[value.productID] = new Array();
                    			// 	invoicedsubProductsForCheck[value.productID] = new Array();
                    			// }

                    			deliverSubProducts[index].push($.extend({}, thisSubProduct));
                    			// subProductsForCheck[value.productID].push($.extend({}, thisSubProduct));
                    			// invoicedsubProductsForCheck[value.productID].push($.extend({}, thisSubProduct));
                    		});

							// thisVals.selected_serials = subProBatchIds;
							// checkSubProduct[thisVals.productID] = 1;
							SubProductsQty[Row_inc_id] = qtyTotal;
							insertedSubProducts[index] = selectedProducts;
							$currentRow.addClass('subproducts');
							$("input[name='deliverQuanity']", $currentRow).prop('readonly', true).change();
						}

						deliverProducts[index] = thisVals;
						checkDeliverProducts[value.productID] = thisVals;
						$currentRow
						.removeClass('add-row')
						.removeClass('edit-row')
						.find("#itemCode, input[name='deliverQuanity'],input[name='availableQuantity'],input[name='unitPrice'],input[name='discount'], input[name='uomqty']").prop('readonly', true).end()
						.find('td').find('#addNewTax').attr('disabled', true)
						.find("button.delete").removeClass('disabled');
						$currentRow.find("input[name='deliverQuanity']").change();
						$currentRow.find("input[name='unitPrice']").change();
						$currentRow.find("input[name='discount']").change();
						$currentRow.find("#itemCode").prop('disabled', true);
						$currentRow.parent().find('.edit').removeClass('hidden');
						$currentRow.parent().find('.add').addClass('hidden');
						$currentRow.parent().find('.save').addClass('hidden');
						$currentRow.parent().parent().find('.delete').removeClass('disabled');
						if ($currentRow.hasClass('subproducts')) {
							$('.edit-modal', $currentRow).parent().addClass('hidden');
							$("td[colspan]", $currentRow).attr('colspan', 2);
						}
						var $newRow = $($addRowSample.clone()).removeClass('sample hidden').appendTo($('#add-new-item-row', $productTable));
						$newRow.find('.add').removeClass('hidden');
						$newRow.find('.edit').addClass('hidden');
						$newRow.find('.delete').addClass('disabled');
						$newRow.find("#itemCode").focus();
                    });

                    if (respond.data.deliveryNote.deliveryNoteCharge > 0) {
                        $('#deliveryChargeEnable').prop('checked', true);
                        $('.deliCharges').removeClass('hidden');
                        $('.deliAddress').removeClass('hidden');
                        $('#deliveryCharge').val(parseFloat(respond.data.deliveryNote.deliveryNoteCharge).toFixed(2) / customCurrencyRate);
                    }

                    setTotalTax();
                    setDeliveryNoteTotalCost();
                    var $currentRow = getAddRow();
                    clearProductRow($currentRow);
                    setProductTypeahead();
                } else {
                    deliveryNoteId = '';
                    p_notification(respond.status, respond.msg);
                }
            }
        });
	}

	function getProductQuantityToBeDeliveryNote(deliveryNoteProduct,pID){
		var quantity = 0;
		$.each(deliveryNoteProduct,function(index,value){
			if(value.productID == pID){
				quantity+=value.deliveryNoteProductQuantity;
			}
		})
		return quantity;
	}

    function calculateItemTotal(thisRow, productID) {
        var thisRow = thisRow;
        var Row_inc_id = thisRow.data('proIncID');
        var productID = productID;
        var checkedTaxes = Array();
        thisRow.find('input:checkbox.addNewTaxCheck').each(function() {
            if (this.checked) {
                var cliTID = this.id;
                var tTaxID = cliTID.split('_')[1];
                checkedTaxes.push(tTaxID);
            }
        });
        var productType = thisRow.find('#itemCode').data('PT');
        var tmpItemQuentity = thisRow.find('#deliverQuanity').val();
        if (productType == 2 && tmpItemQuentity == 0) {
            tmpItemQuentity = 1;
        }
        var tmpItemTotal = (toFloat(thisRow.find('#unitPrice').val()) * toFloat(tmpItemQuentity));
        var tmpItemCost = tmpItemTotal;

        //check price list enabled for product
        if (!priceListId)  {
            if (currentProducts[productID].dEL == 1) {
                //current product dsiscount (May changed due to insertion of promotion but there's a validation below)
                var currentDiscount = 0;
                if (thisRow.hasClass('add-row')) {
                    currentDiscount = thisRow.find('#deliveryNoteDiscount').val();
                } else {
                    if (currentProducts[productID].dV != null) {
                            currentDiscount = currentProducts[productID].dV;
                    } else {
                            currentDiscount = currentProducts[productID].dPR;
                    }
                }
                if (currentProducts[productID].dV != null) {
                    thisRow.find('#deliveryNoteDiscount').val(currentDiscount);
                    thisRow.find('#deliveryNoteDiscount').addClass('value');
                    if (thisRow.find('#deliveryNoteDiscount').hasClass('precentage')) {
                            thisRow.find('#deliveryNoteDiscount').removeClass('precentage');
                    }
                    thisRow.find('.sign').text(companyCurrencySymbol);
                } else {
                    thisRow.find('#deliveryNoteDiscount').val(currentDiscount);
                    thisRow.find('#deliveryNoteDiscount').addClass('precentage');
                    if (thisRow.find('#deliveryNoteDiscount').hasClass('value')) {
                            thisRow.find('#deliveryNoteDiscount').removeClass('value');
                    }
                    thisRow.find('.sign').text('%');
                }
            } else {
                thisRow.find('#deliveryNoteDiscount').prop('readonly', true);
            }
        }

        if (thisRow.find('#deliveryNoteDiscount').hasClass('value')) {
            var tmpPDiscountvalue = isNaN(thisRow.find('#deliveryNoteDiscount').val()) ? 0 : thisRow.find('#deliveryNoteDiscount').val();
            if (tmpPDiscountvalue > 0) {
                var validatedDiscount = validateDiscount(productID, 'val', tmpPDiscountvalue,thisRow.find('#unitPrice').val());
                thisRow.find('#deliveryNoteDiscount').val(validatedDiscount.toFixed(2));
                tmpItemCost -= tmpItemQuentity * validatedDiscount;
            }
        } else {
            var tmpPDiscount = isNaN(thisRow.find('#deliveryNoteDiscount').val()) ? 0 : thisRow.find('#deliveryNoteDiscount').val();
            if (tmpPDiscount > 0) {
                var validatedDiscount = validateDiscount(productID, 'per', tmpPDiscount, tmpItemQuentity);
                thisRow.find('#deliveryNoteDiscount').val(validatedDiscount.toFixed(2));
                tmpItemCost -= (tmpItemTotal * toFloat(validatedDiscount) / toFloat(100));
            }
        }

        if (checkedTaxes) {
            currentItemTaxResults = calculateItemCustomTax(tmpItemCost, checkedTaxes);
            currentTaxAmount = (currentItemTaxResults == null) ? null : currentItemTaxResults.tTA;
        } else {
            currentTaxAmount = 0;
            currentItemTaxResults = '';
        }
        var itemCost = toFloat(tmpItemCost) + toFloat(currentTaxAmount);
        productsTotal[Row_inc_id] = itemCost;
        productsTax[Row_inc_id] = currentItemTaxResults;
        //if unit price is zero or discount greater than unit price,Then total may be negative value
        //So Total should be zero

        thisRow.find('#addNewTotal').html(accounting.formatMoney(itemCost));
        if (productsTax[Row_inc_id] != null) {
            if (jQuery.isEmptyObject(productsTax[Row_inc_id].tL)) {
                thisRow.find('#taxApplied').addClass('glyphicon glyphicon-unchecked');
            } else {
                thisRow.find('#taxApplied').removeClass('glyphicon glyphicon-unchecked');
                thisRow.find('#taxApplied').addClass('glyphicon glyphicon-check');
            }
        }
    }

    function validateDiscount(productID, discountType, currentDiscount,unitPrice) {
        if (priceListId) {
            return toFloat(currentDiscount);
        }
        var defaultProductData = currentProducts[productID];
        var newDiscount = 0;
        if (defaultProductData.dEL == 1) {
            if (discountType == 'val') {
                if (toFloat(currentDiscount) > toFloat(defaultProductData.dV) && toFloat(defaultProductData.dV) !=0) {
                    newDiscount = toFloat(defaultProductData.dV);
                    p_notification(false, eb.getMessage('ERR_INVO_DISCOUNT_VALUE'));
                } else if (toFloat(defaultProductData.dV) == 0) {
                    if (toFloat(currentDiscount) > toFloat(unitPrice)) {
                        newDiscount = toFloat(unitPrice);
                        p_notification(false, eb.getMessage('ERR_INVO_DISCOUNT_VALUE_UNITE_P'));
                    } else {
                        newDiscount = toFloat(currentDiscount);    
                    }
                } else {
                    newDiscount = toFloat(currentDiscount);
                }

            } else {
                if (toFloat(defaultProductData.dPR) != 0 && toFloat(currentDiscount) > toFloat(defaultProductData.dPR)) {
                   	newDiscount = toFloat(defaultProductData.dPR);
                    p_notification(false, eb.getMessage('ERR_INVO_DISC_PERCENTAGE'));
                } else {
                    newDiscount = toFloat(currentDiscount);
                }
            }
        } else {
            p_notification(false, eb.getMessage('ERR_INVO_DISABLE_DISC'));
            newDiscount = 0.00;
        }
        return newDiscount;
    }

	var setCustomerDetails = function($customer) {
    if ($customer.customerID) {
        $('#customer').empty();
        $('#customer').
                append($("<option></option>").
                        attr("value", $customer.customerID).
                        text($customer.customerName + '-' + $customer.customerCode));
        $('#customer').selectpicker('refresh');
        if ($customer.customerPaymentTerm == 'null') {
            $('#paymentTerm').val(1);
        } else {
            $('#paymentTerm').val($customer.customerPaymentTerm);
        }
        $('#paymentTerm').trigger('change');
    } else {
        $('#customerCurrentBalance').val('');
        $('#customerCurrentCredit').val('');
        $('#paymentTerm').val('');
    }

};

    var productID = $("table.deliveryNoteProductTable > tbody#add-new-item-row > tr.add-row").attr('id');

    var getAddRow = function(productID) {

        if (productID != undefined) {

            var $row = $('table.deliveryNoteProductTable > tbody > tr', $productTable).filter(function() {
                return $(this).data("id") == productID;
            });
            return $row;
        }

        return $('tr.add-row:not(.sample)', $productTable);
    };
    if (!getAddRow().length) {
        $($addRowSample.clone()).removeClass('sample hidden').appendTo($('#add-new-item-row'));
    }
    locationOut = $('#idOfLocation').val();
    clearProductScreen();
    setProductTypeahead();

    function setProductTypeahead() {

        var $currentRow = getAddRow();
        // $('#itemCode', $currentRow).selectpicker();
        locationID = $('#idOfLocation').val();
        var documentType = 'deliveryNote';

        $('#itemCode',$currentRow).select2({

            minimumInputLength: 1,
            minimumResultsForSearch: 10,
            ajax: {
                url: BASE_URL + '/productAPI/search-location-products-for-dropdown',
                dataType: "json",
                type: "POST",
                data: function (params) {

                    var queryParameters = {
                        searchKey: params.term,
                        documentType: documentType,
                        locationID: locationID
                    }
                    return queryParameters;
                },
                processResults: function (data) {
                    return {
                        results: $.map(data.data.list, function (item) {
                            return {
                                text: item.text,
                                id: item.value
                            }
                        })
                    };
                }
            },
            placeholder: 'Enter Item Code Or Name',
        });


        $('#itemCode',$currentRow).on('select2:select', function (e) { 
            var data = e.params.data;

            if ($(this).val() > 0 && selectedPID != $(this).val()) {
                selectedPID = $(this).val();
                eb.ajax({
                    type: 'POST',
                    url: BASE_URL + '/productAPI/get-location-product-details',
                    data: {productID: selectedPID, locationID: locationID},
                    success: function(respond) {
                        selectProductForTransfer(respond.data);
                    }
                });
            }
            $('#availableQuantity', $currentRow).parent().addClass('input-group');
            $('#deliverQuanity', $currentRow).parent().addClass('input-group');
        });
    }

    function selectProductForTransfer(selectedlocationProduct) {
// check if product is already selected
		var $currentRow = getAddRow();
        if (checkDeliverProducts[selectedlocationProduct.pID] != undefined) {
            p_notification(false, eb.getMessage('ERR_DELI_ALREADY_SELECT_PROD'));
            clearProductRow($currentRow);
            return false;
        }

        currentProducts[selectedlocationProduct.pID] = selectedlocationProduct;
        if (deletedBatchProducts != "") {
            $.each(currentProducts, function(pIDD, dataVal) {
                if (pIDD == selectedlocationProduct.pID) {
                    if (dataVal.batch != "") {
                        $.each(dataVal.batch, function(bId, bVal) {
                            $.each(deletedBatchProducts, function(sId, sVal) {
                                $.each(sVal, function(bbId, ssVal) {
                                    if (ssVal.batchID == bId) {
                                       bVal.PBQ = parseFloat(bVal.PBQ ) + parseFloat(ssVal.addedQty);
                                       selectedlocationProduct.LPQ = parseFloat(selectedlocationProduct.LPQ) + parseFloat(ssVal.addedQty);
                                    }
                                });
                            });
                        });
                    }
                }
            });
        }

        var productID = selectedlocationProduct.pID;
        var productCode = selectedlocationProduct.pC;
        var productType = selectedlocationProduct.pT;
        var productName = selectedlocationProduct.pN;
        var availableQuantity = (selectedlocationProduct.LPQ == null) ? 0 : selectedlocationProduct.LPQ;
        var defaultSellingPrice = selectedlocationProduct.dSP;
        if (selectedlocationProduct.dPR != null) {
            var productDiscountPrecentage = parseFloat(selectedlocationProduct.dPR).toFixed(2);
        } else {
            var productDiscountPrecentage = 0;
        }
        
        if (selectedlocationProduct.dV != null) {
            var productDiscountValue = parseFloat(selectedlocationProduct.dV).toFixed(2);
        } else {
            var productDiscountValue = 0;
    
        }
        if (priceListItems[productID] != undefined && parseFloat(priceListItems[productID].itemPrice) > 0) {
            defaultSellingPrice = priceListItems[productID].itemPrice;
            var itemDiscountType = priceListItems[productID].itemDiscountType;
            productDiscountValue = (itemDiscountType == 1) ? priceListItems[productID].itemDiscount : 0;
            productDiscountPrecentage = (itemDiscountType == 2) ? priceListItems[productID].itemDiscount : 0;
        }
        var Row_inc_id = rowincrementID;
        clearProductRow($currentRow);
        $("input[name='discount']", $currentRow).prop('readonly', false);
        $("#itemCode", $currentRow).data('PT', productType);
        $("#itemCode", $currentRow).data('PN', productName);
        $("#itemCode", $currentRow).data('PC', productCode);
        $("input[name='availableQuantity']", $currentRow).val(availableQuantity).prop('readonly', true);
        $("input[name='unitPrice']", $currentRow).val(defaultSellingPrice).addUomPrice(selectedlocationProduct.uom);
        if (selectedlocationProduct.dEL == 1 || (priceListItems[productID] != undefined && parseFloat(priceListItems[productID].itemPrice) > 0)) {
            if (selectedlocationProduct.dV != null) {
                productDiscountValue = parseFloat(productDiscountValue).toFixed(2);
                $("input[name='discount']", $currentRow).val(productDiscountValue).addUomPrice(selectedlocationProduct.uom);
                $("input[name='discount']", $currentRow).addClass('value');
                if ($("input[name='discount']", $currentRow).hasClass('precentage')) {
                    $("input[name='discount']", $currentRow).removeClass('precentage');
                }
                $(".sign", $currentRow).text(companyCurrencySymbol);
            } else if (selectedlocationProduct.dPR != null) {
                productDiscountPrecentage = parseFloat(productDiscountPrecentage).toFixed(2);
                $("input[name='discount']", $currentRow).val(productDiscountPrecentage);
                $("input[name='discount']", $currentRow).addClass('precentage');
                if ($("input[name='discount']", $currentRow).hasClass('value')) {
                    $("input[name='discount']", $currentRow).removeClass('value');
                }
                $(".sign", $currentRow).text('%');
            }
        } else {
            $("input[name='discount']", $currentRow).prop('readonly', true);
        }
        $("input[name='deliverQuanity']", $currentRow).prop('readonly', false);
        $currentRow.data('id', productID);
        $currentRow.data('proIncID', Row_inc_id);
        $currentRow.data('doctypeid', '');
        $currentRow.data('docid', '');
        // add uom list
        $("input[name='availableQuantity'],input[name='deliverQuanity']", $currentRow).addUom(selectedlocationProduct.uom);
        $('.uomqty').attr("id", "itemQuantity");
        $('.uomPrice').attr("id", "itemUnitPrice");
        var deleted = 0;
        // clear old rows
        $("#batch_data tr:not(.hidden)").remove();
        addBatchProduct(productID, productCode, deleted, Row_inc_id);
        addSerialProduct(productID, productCode, deleted, Row_inc_id);
        addSerialBatchProduct(productID, productCode, deleted, Row_inc_id);        

        //check this product serail or batch
        if (!$.isEmptyObject(selectedlocationProduct.serial)) {
            $('#product-batch-modal .serial-auto-select').removeClass('hidden');
            $('#numberOfRow').val('');
        } else {
            $('#product-batch-modal .serial-auto-select').addClass('hidden');
        }

         // check if any batch / serial products exist
        if ($.isEmptyObject(selectedlocationProduct.batch) &&
                $.isEmptyObject(selectedlocationProduct.serial) &&
                $.isEmptyObject(selectedlocationProduct.batchSerial)) {

            $currentRow.removeClass('subproducts');
            $('.edit-modal', $currentRow).parent().addClass('hidden');
            $("td[colspan]", $currentRow).attr('colspan', 2);
            $("input[name='deliverQuantity']", $currentRow).prop('readonly', false).focus();
        } else {
            $currentRow.addClass('subproducts');
            $('.edit-modal', $currentRow).parent().removeClass('hidden');
            $("td[colspan]", $currentRow).attr('colspan', 1);
            $("input[name='deliverQuantity']", $currentRow).prop('readonly', true);
            $('#addDeliveryNoteProductsModal').data('id',productID);
            $('#addDeliveryNoteProductsModal').modal('show');
        }
        setTaxListForProduct(productID, $currentRow);
        $currentRow.attr('id', 'product' + productID);
    }

    function clearProductRow($currentRow) {
        $("input[name='availableQuantity']", $currentRow).val('').siblings('.uomqty,.uom-select').remove().end().show();
        $("input[name='deliverQuanity']", $currentRow).val('').siblings('.uomqty,.uom-select').remove().end().show();
        $("input[name='unitPrice']", $currentRow).val('').siblings('.uomPrice,.uom-price-select').remove().end().show();
        $("input[name='discount']", $currentRow).val('').siblings('.uomPrice,.uom-price-select').remove().end().show();
        $(".tempLi", $currentRow).remove('');
        $("#taxApplied", $currentRow).removeClass('glyphicon-check');
        $("#taxApplied", $currentRow).addClass('glyphicon-unchecked');
        $(".uomList", $currentRow).remove('');
    }

    $('#batch-save').on('click', function(e) {
        e.preventDefault();
        // validate batch / serial products before closing modal
        if (!batchModalValidate(e)) {
            e.stopPropagation();
            return false;
        } else {
            $('#addDeliveryNoteProductsModal').modal('hide');
        }

    });

    $('#select_serials').on('click',function(e) {
        var numberOfRow = $('#numberOfRow').val();
        if (numberOfRow != '' || numberOfRow != 0) {
            $('#batch_data > tr').each(function(key, value){
                if (numberOfRow != 0 && !($("input[name='deliverQuantityCheck']", value).is('[disabled=disabled]'))) {
                    $("input[name='deliverQuantityCheck']", value).prop('checked', true);
                    numberOfRow--;
                } else {
                    $("input[name='deliverQuantityCheck']", value).prop('checked', false);
                }
            });
        }    
        
    });

    $('.close').on('click', function() {
        $('#addDeliveryNoteProductsModal').modal('hide');
    });
    function getCurrentProductData($thisRow) {
        $("input.uomqty", $thisRow).change();
        var discountType = '';
        if ($("input[name='discount']", $thisRow).hasClass('value')) {
            discountType = 'value';
        } else if ($("input[name='discount']", $thisRow).hasClass('precentage')) {
            discountType = 'precentage';
        }
        var availableQuantity = $("input[name='availableQuantity']", $thisRow).val();
        if (!availableQuantity) {
            availableQuantity = 0;
        }
        var thisVals = {
            productID: $thisRow.data('id'),
            productIncID: $thisRow.data('proIncID'),
            productCode: $("#itemCode", $thisRow).data('PC'),
            productName: $("#itemCode", $thisRow).data('PN'),
            productPrice: $("input[name='unitPrice']", $thisRow).val(),
            productDiscount: $("input[name='discount']", $thisRow).val(),
            productDiscountType: discountType,
            productTotal: productsTotal[$thisRow.data('proIncID')],
            pTax: productsTax[$thisRow.data('proIncID')],
            productType: $("#itemCode", $thisRow).data('PT'),
            documentTypeID: $thisRow.data('doctypeid'),
            documentID: $thisRow.data('docid'),
            availableQuantity: {
                qty: availableQuantity,
            },
            deliverQuantity: {
                qty: $("input[name='deliverQuanity']", $thisRow).val(),
            }
        };

        return thisVals;
    }


    $productTable.on('click', '#add_item, button.save', function(e) {
        e.preventDefault();

        var $thisRow = $(this).parents('tr');
        var thisVals = getCurrentProductData($thisRow);
        var proIncID = thisVals.productID+'_'+thisVals.productIncID

        $('.itemDescText', $thisRow).prop('disabled', true);
        $("input[name='availableQuantity']", $thisRow).prop('readonly', true);

        if (!$(this).hasClass('save')) {
            if (checkDeliverProducts[thisVals.productID] != undefined) {
                p_notification(false, eb.getMessage('ERR_DELI_ALREADY_SELECT_PROD'));
                $("#itemCode", $thisRow).focus().select();
                $thisRow.remove();
                var $newRow = $($addRowSample.clone()).removeClass('sample hidden').appendTo($('#add-new-item-row', $productTable));
                setProductTypeahead();
                $newRow.find("#itemCode").focus();
                return false;
            }
        }

        if (($('#itemCode', $thisRow).val()) == undefined || ($('#itemCode', $thisRow).val() == '')) {
            p_notification(false, eb.getMessage('ERR_DELI_SELECT_PROD'));
            $("#itemCode", $thisRow).focus().select();
            return false;
        }

        if (thisVals.productType != 2 && (isNaN(parseFloat(thisVals.deliverQuantity.qty)) || parseFloat(thisVals.deliverQuantity.qty) <= 0)) {
            p_notification(false, eb.getMessage('ERR_DELI_ENTER_QUAN'));
            $("input[name='deliverQuantity']", $thisRow).focus().select();
            return false;
        }

        if (thisVals.productType != 2 && (parseFloat(thisVals.deliverQuantity.qty) > parseFloat(thisVals.availableQuantity.qty))) {
            p_notification(false, eb.getMessage('ERR_DELI_QUAN'));
            $("input[name='deliverQuantity']", $thisRow).focus().select();
            return false;

        }

        if (thisVals.productDiscount) {
            if (isNaN(parseFloat(thisVals.productDiscount)) || parseFloat(thisVals.productDiscount) < 0) {
                p_notification(false, eb.getMessage('ERR_DELI_VALID_DISCOUNT'));
                $("input[name='discount']", $thisRow).focus().select();
                return false;
            }
        }

        if (thisVals.productTotal < 0) {
            if (isNaN(parseFloat(thisVals.productTotal)) || parseFloat(thisVals.productTotal) <= 0) {
                p_notification(false, eb.getMessage('ERR_INVO_PROD_TOTAL'));
                $("input[name='unitPrice']", $thisRow).focus().select();
                return false;
            }
        }

        if ($thisRow.hasClass('subproducts')) {
            if (parseFloat(SubProductsQty[thisVals.productIncID]) != parseFloat(thisVals.deliverQuantity.qty)) {
                p_notification(false, eb.getMessage('ERR_DELI_SUB_PRODUCT_CHECK', thisVals.productCode));
                $('.edit-modal', $thisRow).focus();
                return false;
            }
        }

// if add button is clicked
        if ($(this).hasClass('add')) {
            var $currentRow = $thisRow;
            var $flag = false;
            if ($currentRow.hasClass('add-row')) {
                $flag = true;
            }
            $currentRow
                    .removeClass('add-row')
                    .find("#itemCode, input[name='deliverQuanity'],input[name='availableQuantity'],input[name='unitPrice'],input[name='discount']").prop('readonly', true).end()
                    .find('td').find('#addNewTax').attr('disabled', true)
                    .find("button.delete").removeClass('disabled');
            $currentRow.find("input[name='discount']").change();
            $currentRow.find("input[name='deliverQuanity']").change();
            $currentRow.find("input[name='unitPrice']").change();
            $currentRow.find("#itemCode").prop('disabled', true);
            if ($flag) {
                var $newRow = $($addRowSample.clone()).removeClass('sample hidden').appendTo($('#add-new-item-row', $productTable));
                setProductTypeahead();
                $newRow.find("#itemCode").focus();
                $newRow.find('#add_item').removeClass('hidden');
                $newRow.find('.edit').addClass('hidden');
                $newRow.find('.delete').addClass('disabled');
                rowincrementID++;
            }
        } else if ($(this).hasClass('save')) {
            var productId = $(this).parent().parent().data('id');
            var $currentRow = getAddRow(productId);
            $currentRow
                    .removeClass('edit-row')
                    .find("#itemCode, input[name='deliverQuanity'],input[name='availableQuantity'],input[name='unitPrice'],input[name='discount']").prop('readonly', true).end()
                    .find('td').find('#addNewTax').attr('disabled', true)
                    .find("button.delete").removeClass('disabled');
            $currentRow.find("input[name='discount']").change();
            $currentRow.find("input[name='deliverQuanity']").change();
            $currentRow.find("input[name='unitPrice']").change();
        } else { // if save button is clicked
            $thisRow.removeClass('edit-row');
            $thisRow.find("input[name='deliverQuanity']").prop('readonly', true);
        }
        // if batch product modal is available for this product
        $(this, $currentRow).parent().find('.edit').removeClass('hidden');
        $(this, $currentRow).parent().find('.add').addClass('hidden');
        $(this, $currentRow).parent().find('.save').addClass('hidden');
        $(this, $currentRow).parent().parent().find('.delete').removeClass('disabled');
        if ($thisRow.hasClass('subproducts')) {
            $('.edit-modal', $currentRow).parent().addClass('hidden');
            $("td[colspan]", $currentRow).attr('colspan', 2);
        }
        deliverProducts[proIncID] = thisVals;
        checkDeliverProducts[thisVals.productID] = thisVals;
        setDeliveryNoteTotalCost();
        setTotalTax();
        $('#customCurrencyId').attr('disabled', true);
        $('#priceListId').attr('disabled', true);
    });

    $productTable.on('click', 'button.edit', function(e) {
        var $thisRow = $(this).parents('tr');
        var produtID = $thisRow.data('id');
        $thisRow.addClass('edit-row')
                .find("input[name='deliverQuanity'],input[name='unitPrice'],input[name='discount']").prop('readonly', false).end()
                .find('td').find('#addNewTax').attr('disabled', false)
                .find("button.delete").addClass('disabled');
        if (currentProducts[produtID].dEL != 1) {
            $thisRow.find("input[name='discount']").prop('readonly', true);
        }
        $thisRow.find("input[name='discount']").change();
        $thisRow.find("input[name='deliverQuanity']").change();
        $thisRow.find("input[name='unitPrice']").change();
        $(this, $thisRow).parent().find('.edit').addClass('hidden');
        $(this, $thisRow).parent().find('.save').removeClass('hidden');
        if ($thisRow.hasClass('subproducts')) {
            $("input[name='deliverQuanity']", $thisRow).prop('readonly', true);
            $('.edit-modal', $thisRow).parent().removeClass('hidden');
            $("td[colspan]", $thisRow).attr('colspan', 1);
        } else {
            $("input[name='deliverQuanity']", $thisRow).prop('readonly', false).focus();
        }
    });
    $productTable.on('click', 'button.delete', function(e) {
        e.preventDefault();
        if (!$(this).hasClass('disabled')) {
            var $thisRow = $(this).parents('tr');
            var productID = $thisRow.data('id');
            var proIncID = productID+'_'+$thisRow.data('proIncID');
            delete deliverProducts[proIncID];
            delete checkDeliverProducts[productID];
            
            $.each(deliverSubProducts, function(sId, sVal) {
                if (sId == proIncID) {
                    deletedBatchProducts[proIncID] = sVal;
                }
            });

            // when all products deleted customcurrency symbal selection will be enable
            // if used any selection from 'start invoice by' then cann't enable
            if (_.size(deliverProducts) == 0 && !$('#salesOrderNo').val()) {
                $('#customCurrencyId').attr('disabled', false);
            }
            setDeliveryNoteTotalCost();
            setTotalTax();
            selectedPID = '';
            $thisRow.remove();
        }
    });

    function batchModalValidate(e) {
        var productID = $('#addDeliveryNoteProductsModal').data('id');
         productID = (productID == 'undefined')? undefined :productID;
        var $thisParentRow = getAddRow(productID);
        var thisVals = getCurrentProductData($thisParentRow);
        var proIncID = productID+'_'+thisVals.productIncID;
        var $batchTable = $("#addDeliveryNoteProductsModal .batch-table tbody");
        var qtyTotal = 0;
        var subProducts = [];
        $("input[name='deliverQuantity'], input[name='deliverQuantityCheck']:checked", $batchTable).each(function() {

            var $thisSubRow = $(this).parents('tr');
            var thisDeliverQuantity = $(this).val();
            if ((thisDeliverQuantity).trim() != "" && isNaN(parseFloat(thisDeliverQuantity))) {
                p_notification(false, eb.getMessage('ERR_DELI_ENTER_QUAN'));
                $(this).focus();
                return qtyTotal = false;
            }

            thisDeliverQuantity = (isNaN(parseFloat(thisDeliverQuantity))) ? 0 : parseFloat(thisDeliverQuantity);
            var thisAvailableQuantity = $("input[name='availableQuantity']", $thisSubRow).val();
            thisAvailableQuantity = (isNaN(parseFloat(thisAvailableQuantity))) ? 0 : parseFloat(thisAvailableQuantity);
            if (thisDeliverQuantity > thisAvailableQuantity) {
                p_notification(false, eb.getMessage('ERR_DELI_QUAN'));
                $(this).focus();
                return qtyTotal = false;
            }

            var warranty = $thisSubRow.find('#warranty').val();
            if (isNaN(warranty)) {
                p_notification(false, eb.getMessage('ERR_INVO_WARRANTY_PERIOD_SHBE_NUM'));
                $thisSubRow.find('#warranty').focus().select();
                return qtyTotal = false;
            }

            qtyTotal = qtyTotal + thisDeliverQuantity;
            // if a product transfer is present, prepare array to be sent to backend

            if (thisDeliverQuantity > 0) {

                var thisSubProduct = {};
                if ($(".batchCode", $thisSubRow).data('id')) {
                    thisSubProduct.batchID = $(".batchCode", $thisSubRow).data('id');
                }

                if ($(".serialID", $thisSubRow).data('id')) {
                    thisSubProduct.serialID = $(".serialID", $thisSubRow).data('id');
                }

                thisSubProduct.qtyByBase = thisDeliverQuantity;
                thisSubProduct.warranty = $thisSubRow.find('#warranty').val();
                subProducts.push(thisSubProduct);
            }

        });
        if (checkSubProduct[thisVals.productID] != undefined) {
            if (qtyTotal != $("input[name='deliverQuanity']", $thisParentRow).val()) {
                p_notification(false, eb.getMessage('ERR_DELI_TOTAL_SUBQUAN'));
                return qtyTotal = false;
            } else {
                checkSubProduct[thisVals.productID] = 1;
            }
        }

        // to break out form $.each and exit function
        if (qtyTotal === false)
            return false;
        // ideally, since the individual batch/serial quantity is checked to be below the available qty,
        // the below condition should never become true
        if (qtyTotal > thisVals.availableQuantity.qty) {
            p_notification(false, eb.getMessage('ERR_DELI_TOTAL_DELIQUAN'));
            return false;
        }
        deliverSubProducts[proIncID] = subProducts;
        SubProductsQty[thisVals.productIncID] = qtyTotal;
        var $transferQ = $("input[name='deliverQuanity']", $thisParentRow).prop('readonly', true);
        if (checkSubProduct[thisVals.productID] == undefined) {
            $transferQ.val(qtyTotal).change();
        }
        $("#unitPrice", $thisParentRow).trigger(jQuery.Event("focusout"));
        $('#addDeliveryNoteProductsModal').data('id',"undefined");
        return true;
    }

    $productTable.on('click', 'button.edit-modal', function(e) {
        var productID = $(this).parents('tr').data('id');
        var incrementID = $(this).parents('tr').data('proIncID');
        var proIncID = productID+"_"+incrementID;
        var productCode = $(this).parents('tr').find('#itemCode').val();
        $("#batch_data tr:not(.hidden)").remove();
        addBatchProduct(productID, productCode, 0, incrementID);
        addSerialProduct(productID, productCode, 0, incrementID);
        addSerialBatchProduct(productID, productCode, 0, incrementID);

        //check this product serail or batch
        if (!$.isEmptyObject(currentProducts[productID].serial)) {
            $('#product-batch-modal .serial-auto-select').removeClass('hidden');
            $('#numberOfRow').val('');
        } else {
            $('#product-batch-modal .serial-auto-select').addClass('hidden');
        }

        $("tr", '#batch_data').each(function() {
            var $thisSubRow = $(this);
            for (var i in deliverSubProducts[proIncID]) {
                if (deliverSubProducts[proIncID][i].serialID != undefined) {
                    if ($(".serialID", $thisSubRow).data('id') == deliverSubProducts[proIncID][i].serialID) {
                        $("input[name='deliverQuantityCheck']", $thisSubRow).prop('checked', true);
                        $("input[name='warranty']", $thisSubRow).val(deliverSubProducts[proIncID][i].warranty);
                    }
                    
                } else if (deliverSubProducts[proIncID][i].batchID != undefined) {
                    if ($(".batchCode", $thisSubRow).data('id') == deliverSubProducts[proIncID][i].batchID) {
                        $(this).find("input[name='deliverQuantity']").val(deliverSubProducts[proIncID][i].qtyByBase).change();
                    }
                }
            }
        });
        $('#addDeliveryNoteProductsModal').data('id', $(this).parents('tr').data('id')).modal('show');
        $('#addDeliveryNoteProductsModal').unbind('hide.bs.modal');
    });
    $('form#deliveryNoteForm').on('submit', function() {
        if ($("tr.edit-row", $productTable).length > 0) {
            p_notification(false, eb.getMessage('ERR_DELI_SAVE_ALLPROD'));
            return false;
        }

        updateDeliveryNote();
        return false;
    });
    function updateDeliveryNote() {
        for (var i in checkSubProduct) {
            var $currentRow = getAddRow(i);
            var proIncID = i+"_"+$currentRow.data('proIncID');
            if ($($currentRow).find('.add').parent('td').hasClass('hidden') && !$($currentRow).find('.edit-modal').parent('td').hasClass('hidden')) {
                if (checkSubProduct[i] == 0) {
                    p_notification(false, eb.getMessage('ERR_DELI_ADD_PROD', deliverProducts[proIncID].productCode));
                    return;
                }
            }
        }
        var formData = {
            deliveryCode: $('#deliveryNoteNo').val(),
            location: $('#currentLocation').val(),
            date: $('#deliveryDate').val(),
            customer: $('#customer option:selected').text(),
            deliveryAddress: $('#deliveryNoteAddress').val(),
            customerID: customerID,
        };

        var existingAttachemnts = {};
        var deletedAttachments = {};
        $.each(uploadedAttachments, function(index, val) {
            if (!deletedAttachmentIds.includes(val['documentAttachemntMapID'])) {
                existingAttachemnts[index] = val;
            } else {
                deletedAttachments[index] = val;
            }
        });

        if (validateTransferForm(formData)) {
            eb.ajax({
                type: 'POST',
                url: BASE_URL + '/delivery-note-api/updateDeliveryNoteDetails',
                data: {
                	deliveryNoteID:deliveryNoteId,
                    deliverCode: formData.deliveryCode,
                    locationOutID: locationOut,
                    products: deliverProducts,
                    subProducts: deliverSubProducts,
                    date: formData.date,
                    customer: customerID,
                    customer_prof_ID: cusProfID,
                    salesOrderID : salesOrderID,
                    customerName: $('#customer option:selected').text(),
                    deliveryCharge: $('#deliveryCharge').val(),
                    deliveryTotalPrice: $('#finaltotal').text().replace(/,/g, ''),
                    deliveryAddress: formData.deliveryAddress,
                    deliveryComment: $('#comment').val(),
                    salesPersonID: $('#salesPersonID').val(),
                    customCurrencyId: $('#customCurrencyId').val(),
                    customCurrencyRate: customCurrencyRate,
                    priceListId: priceListId,
                    insertedProducts : insertedProducts,
                    insertedSubProducts :insertedSubProducts,
                    previousDeliveryNoteTotal : previousDeliveryNoteTotal,
                    insertedDeliveryNoteTopVals : insertedDeliveryNoteTopVals,
                    dimensionData : dimensionData,
                    ignoreBudgetLimit: ignoreBudgetLimitFlag
                },
                success: function(respond) {
                    if (respond.status) {
                        p_notification(respond.status, respond.msg);
                        if(respond.data.deliveryNoteID == deliveryNoteId) {
                            var documentID = "same";
                        } else {
                            var documentID = respond.data.deliveryNoteID;
                        }

                        var fileInput = document.getElementById('editDocumentFiles');
                        var form_data = false;
                        if (window.FormData) {
                            form_data = new FormData();
                        }
                        form_data.append("documentID", documentID);
                        form_data.append("documentTypeID", 4);
                        form_data.append("updateFlag", true);
                        form_data.append("editedDocumentID", deliveryNoteId);
                        form_data.append("documentCode", formData.deliveryCode);
                        form_data.append("deletedAttachmentIds", deletedAttachmentIds);
                        
                        if(fileInput.files.length > 0) {
                            for (var i = 0; i < fileInput.files.length; i++) {
                                form_data.append("files[]", fileInput.files[i]);
                            }
                        }

                        eb.ajax({
                            url: BASE_URL + '/store-files',
                            type: 'POST',
                            processData: false,
                            contentType: false,
                            data: form_data,
                            success: function(res) {
                            }
                        });

                        documentPreview(BASE_URL + '/delivery-note/preview/' + respond.data.deliveryNoteID, 'documentpreview', "/delivery-note-api/send-email", function($preview) {
                            $preview.on('hidden.bs.modal', function() {
                                if (!$("#preview:visible").length) {
                                    window.location.assign(BASE_URL + '/delivery-note/view');
                                }
                            });
                        });
                    }else {
                        if(respond.data == 'UPDATE_FAIL'){
                                window.location.assign(BASE_URL + '/delivery-note/view');
                        } else if (respond.data == "NotifyBudgetLimit") {
                            bootbox.confirm(respond.msg + ' ,Are you sure you want to update this delivery note ?', function(result) {
                                if (result == true) {
                                    ignoreBudgetLimitFlag = true;
                                    updateDeliveryNote();
                                } else {
                                    setTimeout(function(){ 
                                        location.reload();
                                    }, 3000);
                                }
                            });
                        } else {
                            p_notification(respond.status, respond.msg);
                        }
                    }

                }
            });
        }
    }

    $('table#deliveryNoteProductTable>tbody').on('keypress', 'input#itemQuantity, input#unitPrice, input#deliveryNoteDiscount, #deliverQuanity,#availableQuantity ', function(e) {

        if (e.which == 13) {
            $('#add_item', $(this).parents('tr.add-row')).trigger('click');
            e.preventDefault();
        }

    });

    if ($(this).parents('tr').find('#itemCode').val() != '') {
        if ($(this).val().indexOf('.') > 0) {
            decimalPoints = $(this).val().split('.')[1].length;
        }
        if (this.id == 'unitPrice' && (!$.isNumeric($(this).parents('tr').find('#unitPrice').val()) || $(this).parents('tr').find('#unitPrice').val() < 0 || decimalPoints > 2)) {
            decimalPoints = 0;
            $(this).parents('tr').find('#unitPrice').val('');
        }
        if ((this.id == 'deliverQuanity') && (!$.isNumeric($(this).parents('tr').find('#deliverQuanity').val()) || $(this).parents('tr').find('#deliverQuanity').val() < 0)) {
            decimalPoints = 0;
            $(this).parents('tr').find('#deliverQuanity').val('');
        }
        if ((this.id == 'deliveryNoteDiscount')) {
            if ($(this).hasClass('precentage') && (!$.isNumeric($(this).parents('tr').find('#deliveryNoteDiscount').val()) || $(this).parents('tr').find('#deliveryNoteDiscount').val() < 0 || $(this).parents('tr').find('#deliveryNoteDiscount').val() >= 100 || (decimalPoints > 2))) {
                decimalPoints = 0;
                $(this).parents('tr').find('#deliveryNoteDiscount').val('');
            } else {
                decimalPoints = 0;
            }
        }
    }

    $productTable.on('focusout', '#deliverQuanity,#unitPrice,#deliveryNoteDiscount,.uomqty,.uomPrice', function() {
        var $thisRow = $(this).parents('tr');
        var Row_inc_id = $thisRow.data('proIncID');
        var productID = $thisRow.data('id');
        var checkedTaxes = Array();
        $thisRow.find('input:checkbox.addNewTaxCheck').each(function() {
            if (this.checked) {
                var cliTID = this.id;
                var tTaxID = cliTID.split('_')[1];
                checkedTaxes.push(tTaxID);
            }
        });
        var tmpItemQuentity = $(this).parents('tr').find('#deliverQuanity').val();
        var productType = $(this).parents('tr').find("#itemCode").data('PT');
        if (productType == 2 && tmpItemQuentity == 0) {
            tmpItemQuentity = 1;
        }
        var tmpItemTotal = (toFloat($(this).parents('tr').find('#unitPrice').val()) * toFloat(tmpItemQuentity));
        var tmpItemCost = tmpItemTotal;
        if ($(this).parents('tr').find('#deliveryNoteDiscount').hasClass('value')) {
            var tmpPDiscountvalue = isNaN($(this).parents('tr').find('#deliveryNoteDiscount').val()) ? 0 : $(this).parents('tr').find('#deliveryNoteDiscount').val();
            if (tmpPDiscountvalue > 0) {
                tmpItemCost -= tmpPDiscountvalue * tmpItemQuentity;
            }
        } else {
            var tmpPDiscount = isNaN($(this).parents('tr').find('#deliveryNoteDiscount').val()) ? 0 : $(this).parents('tr').find('#deliveryNoteDiscount').val();
            if (tmpPDiscount > 0) {
                tmpItemCost -= (tmpItemTotal * toFloat(tmpPDiscount) / toFloat(100));
            }
        }
        if (checkedTaxes) {
            currentItemTaxResults = calculateItemCustomTax(tmpItemCost, checkedTaxes);
            currentTaxAmount = (currentItemTaxResults == null) ? null : currentItemTaxResults.tTA;
        } else {
            currentTaxAmount = 0;
            currentItemTaxResults = '';
        }
        var itemCost = toFloat(tmpItemCost) + toFloat(currentTaxAmount);
        productsTotal[Row_inc_id] = itemCost;
        productsTax[Row_inc_id] = currentItemTaxResults;
        //if unit price is zero or discount greater than unit price,Then total may be negative value
        //So Total should be zero
//        if (itemCost < 0) {
//            itemCost = 0;
//        }
        $(this).parents('tr').find('#addNewTotal').html(accounting.formatMoney(itemCost));
        if (productsTax[Row_inc_id] != null) {
            if (jQuery.isEmptyObject(productsTax[Row_inc_id].tL)) {
                $(this).parents('tr').find('#taxApplied').addClass('glyphicon glyphicon-unchecked');
            } else {
                $(this).parents('tr').find('#taxApplied').removeClass('glyphicon glyphicon-unchecked');
                $(this).parents('tr').find('#taxApplied').addClass('glyphicon glyphicon-check');
            }
        }
    });
    $productTable.on('change', '.taxChecks.addNewTaxCheck', function() {
        $(this).parents('tr').find("#unitPrice").trigger(jQuery.Event("focusout"));
    });

    $productTable.on('click', '#selectAll', function() {
        $(this).parents('#addTaxUl').find('input:checkbox.addNewTaxCheck').each(function() {
            if (!this.checked) {
                this.checked = true;
            }
        });
         $(this).parents('tr').find("#unitPrice").trigger(jQuery.Event("focusout"));
    });

    $productTable.on('click', '#deselectAll', function() {
        $(this).parents('#addTaxUl').find('input:checkbox.addNewTaxCheck').each(function() {
            if (this.checked) {
                this.checked = false;
            }
        });
         $(this).parents('tr').find("#unitPrice").trigger(jQuery.Event("focusout"));
    });

    $productTable.on('click', 'ul.dropdown-menu', function(e) {
        e.stopPropagation();
    });
    function validateTransferForm(formData) {
        var selectedid = "#" + document.activeElement.id;
        var tableId = $(selectedid).closest('table').attr("id");

        if (formData.location == null || formData.location == "") {
            p_notification(false, eb.getMessage('ERR_DELI_SELECT_LOCAT'));
            $('#currentLocation').focus();
            return false;
        } else if (formData.deliveryCode == null || formData.deliveryCode == "") {
            p_notification(false, eb.getMessage('ERR_DELI_NUM_NOTBLANK'));
            return false;
        } else if (formData.customer == null || formData.customer == "") {
            p_notification(false, eb.getMessage('ERR_DELI_SELECT_CUST'));
            $('#customer').focus();
            return false;
        } else if (formData.date == null || formData.date == "") {
            p_notification(false, eb.getMessage('ERR_DELI_DATE_NOTBLANK'));
            $('#deliveryDate').focus();
            return false;
        } else if (formData.customer == null || formData.customer == "") {
            p_notification(false, eb.getMessage('ERR_DELI_SELECT_CUST'));
            $('#customer').focus();
            return false;
        } else if (formData.customerID == null || formData.customerID == "") {
            p_notification(false, eb.getMessage('ERR_INVOICE_INVALID_CUST'));
            $('#customer').focus();
            return false;
        } else if (formData.deliveryAddress == null || formData.deliveryAddress == "") {
            p_notification(false, eb.getMessage('ERR_DELI_ADDR_NOTBLANK'));
            $('#deliveryNoteAddress').focus();
            return false;
        } else if (jQuery.isEmptyObject(deliverProducts)) {
            if (tableId != "deliveryNoteProductTable") {
                p_notification(false, eb.getMessage('ERR_DELI_ADD_ATLEAST_ONEPROD'));
            }
            return false;
        } else if ((tableId == "deliveryNoteProductTable") && ((selectedid != '#showTax') || (selectedid != '#deliveryChargeEnable') || (selectedid != '#deliveryCharge'))) {
            return  true;
        }
        return true;
    }

    function isDate(txtDate)
    {
        var currVal = txtDate;
        if (currVal == '')
            return false;
        //Declare Regex
        var rxDatePattern = /^(\d{4})(\/|-|.)(\d{1,2})(\/|-|.)(\d{1,2})$/;
        var dtArray = currVal.match(rxDatePattern); // is format OK?

        if (dtArray == null)
            return false;
        //Checks for mm/dd/yyyy format.
        dtMonth = dtArray[3];
        dtDay = dtArray[5];
        dtYear = dtArray[1];
        if (dtMonth < 1 || dtMonth > 12)
            return false;
        else if (dtDay < 1 || dtDay > 31)
            return false;
        else if ((dtMonth == 4 || dtMonth == 6 || dtMonth == 9 || dtMonth == 11) && dtDay == 31)
            return false;
        else if (dtMonth == 2) {
            var isleap = (dtYear % 4 == 0 && (dtYear % 100 != 0 || dtYear % 400 == 0));
            if (dtDay > 29 || (dtDay == 29 && !isleap))
                return false;
        }
        return true;
    }

    /**
     * Inside Batch / Serial Product modal
     */

    function addBatchProduct(productID, productCode, deleted, incrementID) {
        if ((!$.isEmptyObject(currentProducts[productID].batch))) {
            batchProduct = currentProducts[productID].batch;
            var incPID = productID+"_"+incrementID;
            for (var i in batchProduct) {
            	if (batchProduct[i].PBQ == 0) {
            		if (incrementID != '') {
            			if(insertedSubProducts[incPID]!==undefined){
    						$.map(insertedSubProducts[incPID], function(obj) {
    							if(obj.batchID == batchProduct[i].PBID){
    								previousQty =(obj.qtyByBase > 0)?obj.qtyByBase: 0;
    							}
    						});
    					}
            			batchProduct[i].PBQ != previousQty;
            		}
            	}

                if (deleted != 1 && batchProduct[i].PBQ != 0) {
                    if ((!$.isEmptyObject(currentProducts[productID].productIDs))) {
                        if (currentProducts[productID].productIDs[i]) {
                            continue;
                        }
                    }

                    //get previous selected batch product quantity for batch modal
                    var previousQty = 0;
    				if(deliverSubProducts[incPID]!==undefined){
    					$.map(deliverSubProducts[incPID], function(obj) {
    						if(obj.batchID == batchProduct[i].PBID){
    							previousQty =(obj.qtyByBase > 0)?obj.qtyByBase: 0;
    						}
    					});
    				}
                   
    				var availableBatchQty = parseFloat(batchProduct[i].PBQ);

                    var batchKey = productCode + "-" + batchProduct[i].PBID;
                    var $clonedRow = $($('.batch_row').clone());
                    $clonedRow
                            .data('id', i).removeAttr('id')
                            .removeClass('hidden batch_row')
                            .addClass('remove_row');
                    $(".batchCode", $clonedRow).html(batchProduct[i].PBC).data('id', batchProduct[i].PBID);
                    $("input[name='availableQuantity']", $clonedRow).val(availableBatchQty);
                    if (batchProduct[i].PBQ == 0) {
                        $("input[name='deliverQuantity']", $clonedRow).prop('disabled', true);
                    }
                    $("input[name='deliverQuantityCheck']", $clonedRow).remove();
                    $("input[name='warranty']", $clonedRow).prop('readonly', true);
                    $("input[name='warranty']", $clonedRow).val(batchProduct[i].PBWoD);
                    $("input[name='expireDate']", $clonedRow).val(batchProduct[i].PBExpD);
                    $("input[name='btPrice']", $clonedRow).val(parseFloat(batchProduct[i].PBUP).toFixed(2));
                    if (priceListItems[productID] != undefined && parseFloat(priceListItems[productID].itemPrice) > 0) {
                        $("input[name='btPrice']", $clonedRow).val(parseFloat(priceListItems[productID].itemPrice).toFixed(2));
                    }
                    if (batchProducts[batchKey]) {
                        $("input[name='deliverQuantity']", $clonedRow).val(batchProducts[batchKey].Qty);
                    }
                    if(!(batchProduct[i].PBExpD == null || batchProduct[i].PBExpD == '0000-00-00')){
                    	var expireCheck = checkEpireData(batchProduct[i].PBExpD);
                    	if(expireCheck){
                    		$("input[name='deliverQuantity']", $clonedRow).attr('disabled',true);
                    	}
                    }
                    $clonedRow.insertBefore('.batch_row');
                    $("input[name='deliverQuantity'],input[name='availableQuantity']", $clonedRow).addUom(currentProducts[productID].uom);
                    $("input[name='btPrice']", $clonedRow).addUomPrice(currentProducts[productID].uom);
                } else {
                    batchProducts[batchKey] = undefined;
                }
            }
        }
    }

    function addSerialProduct(productID, productCode, deleted, incrementID) {
        if ((!$.isEmptyObject(currentProducts[productID].serial))) {
            productSerial = currentProducts[productID].serial;
            for (var i in productSerial) {
                if (deleted != 1) {
                    var serialKey = productCode + "-" + productSerial[i].PSID;
                    var serialQty = (productSerial[i].PSS == 1) ? 0 : 1;
                    if (serialQty == 0 ) {
                    	if (incrementID != '') {
                    		var incPID = productID+"_"+incrementID;
                    		$.map(insertedSubProducts[incPID], function(obj) {
                    			if(obj.serialID == productSerial[i].PSID){
                    				serialQty=1;
                    			}
                    		});
                    	}
                    }
                    if (serialQty != 0) {
                    	var $clonedRow = $($('.batch_row').clone());
                    	$clonedRow
                    	.data('id', i).removeAttr('id')
                    	.removeClass('hidden batch_row')
                    	.addClass('remove_row');
                    	$(".serialID", $clonedRow)
                    	.html(productSerial[i].PSC)
                    	.data('id', productSerial[i].PSID);
                    	$("input[name='availableQuantity']", $clonedRow).val(serialQty);
                    	$("input[name='warranty']", $clonedRow).val(productSerial[i].PSWoD);
                        let wPeriod = "";
                        if(productSerial[i].PSWoT==='4'){
                            wPeriod = "Years"
                        }else if(productSerial[i].PSWoT==='3'){
                            wPeriod = "Months"
                        }else if(productSerial[i].PSWoT==='2'){
                            wPeriod = "Weeks"
                        }else{
                            wPeriod = "Days"
                        }
                        $("input[name='warrantyType']",$clonedRow).val(wPeriod).attr('disabled',true);
    
                    	$("input[name='expireDate']", $clonedRow).val(productSerial[i].PSExpD);
                    	if(!(productSerial[i].PSExpD == null || productSerial[i].PSExpD == '0000-00-00')){
                    		var expireCheck = checkEpireData(productSerial[i].PSExpD);
                    		if(expireCheck){
                    			$("input[name='deliverQuantityCheck']", $clonedRow).attr('disabled',true);
                    		}
                    	}
                    	if (serialQty == 0) {
                    		$("input[name='deliverQuantityCheck']", $clonedRow).prop('disabled', true);
                    	}
                    	$("input[name='deliverQuantity']", $clonedRow).parent().remove();
                    	if (serialProducts[serialKey]) {
                    		if (serialProducts[serialKey].Qty == 1) {
                    			$("input[name='deliverQuantityCheck']", $clonedRow).attr('checked', true);
                    		}
                    	}
                    	$clonedRow.insertBefore('.batch_row');
                    	$("input[name='deliverQuantity'],input[name='availableQuantity']", $clonedRow).addUom(currentProducts[productID].uom);
                    }

                } else {
                    serialProducts[serialKey] = undefined;
                }
            }
        }
    }

    function addSerialBatchProduct(productID, productCode, deleted, incrementID) {
        if ((!$.isEmptyObject(currentProducts[productID].batchSerial))) {
            productBatchSerial = currentProducts[productID].batchSerial;
            for (var i in productBatchSerial) {
                if (deleted != 1) {
                    var serialKey = productCode + "-" + productBatchSerial[i].PBID;
                    var serialQty = (productBatchSerial[i].PSS == 1) ? 0 : 1;
                    if (serialQty == 0) {
                    	if (incrementID != '') {
                    		var incPID = productID+"_"+incrementID;
                    		$.map(insertedSubProducts[incPID], function(obj) {
                    			if(obj.serialID == productBatchSerial[i].PSID){
                    				serialQty=1;
                    			}
                    		});
                    	}
                    }
                    if (serialQty !=0 ) {
                    	var $clonedRow = $($('.batch_row').clone());
                    	$clonedRow
                    	.data('id', i).removeAttr('id')
                    	.removeClass('hidden batch_row')
                    	.addClass('remove_row');
                    	$(".serialID", $clonedRow)
                    	.html(productBatchSerial[i].PSC)
                    	.data('id', productBatchSerial[i].PSID);
                    	$(".batchCode", $clonedRow)
                    	.html(productBatchSerial[i].PBC)
                    	.data('id', productBatchSerial[i].PBID);
                    	$("input[name='availableQuantity']", $clonedRow).val(serialQty);
                    	$("input[name='warranty']", $clonedRow).val(productBatchSerial[i].PBSWoD);
                        $("input[name='btPrice']", $clonedRow).val(parseFloat(productBatchSerial[i].PBUP).toFixed(2));
                        if (priceListItems[productID] != undefined && parseFloat(priceListItems[productID].itemPrice) > 0) {
                            $("input[name='btPrice']", $clonedRow).val(parseFloat(priceListItems[productID].itemPrice).toFixed(2));
                        }
                        if(!(productBatchSerial[i].PBSExpD == null || productBatchSerial[i].PBSExpD == '0000-00-00')){
                        	var expireCheck = checkEpireData(productBatchSerial[i].PBSExpD);
                        	if(expireCheck){
                        		$("input[name='deliverQuantityCheck']", $clonedRow).attr('disabled',true);
                        	}
                        }
                    	if (serialQty = 0) {
                    		$("input[name='deliverQuantityCheck']", $clonedRow).prop('disabled', true);
                    	}
                    	$("input[name='deliverQuantity']", $clonedRow).parent().remove();
                    	$("input[name='expireDate']", $clonedRow).val(productBatchSerial[i].PBSExpD);
                    	if (batchProducts[serialKey]) {
                    		if (batchProducts[serialKey].Qty == 1) {
                    			$("input[name='deliveryQuantityCheck']", $clonedRow).attr('checked', true);
                    		}
                    	}

                    	$clonedRow.insertBefore('.batch_row');
                    	$("input[name='deliverQuantity'],input[name='availableQuantity']", $clonedRow).addUom(currentProducts[productID].uom);
                    }
                } else {
                    batchProducts[serialKey] = undefined;
                }
            }
        }
    }

    function checkEpireData(ExpireDate){
    	var today = new Date();
    	var exDate = new Date(ExpireDate);

    	var ctoday = Date.UTC(today.getFullYear(), today.getMonth()+1, today.getDate());
      	var cexDate = Date.UTC(exDate.getFullYear(), exDate.getMonth()+1, exDate.getDate());

    	var flag = true;
    	if(cexDate >= ctoday){
    		flag = false;
    	}
    	return flag;
    }


    function clearProductScreen() {
        products = {};
        setDeliveryNoteTotalCost();
        setTotalTax();
        $('.addedProducts').remove();
        locProductCodes = '';
        locProductNames = '';
        selectedProduct = '';
        selectedProductQuantity = '';
        selectedProductUom = '';
        batchCount = '';
    }
    function setDeliveryNoteTotalCost() {
        var dNTotal = 0;
        for (var i in deliverProducts) {
            dNTotal += toFloat(deliverProducts[i].productTotal);
        }
        if (dNTotal < 0) {
            dNTotal = 0;
        }
        $('#subtotal').html(accounting.formatMoney(dNTotal));
        if ($('#deliveryChargeEnable').is(':checked')) {
            var deliveryAmount = $('#deliveryCharge').val();
            dNTotal += toFloat(deliveryAmount);
        }
        $('#finaltotal').html(accounting.formatMoney(dNTotal));
    }

    function setTotalTax() {
        totalTaxList = {};
        if (TAXSTATUS) {
            for (var k in deliverProducts) {
                for (var l in deliverProducts[k].pTax.tL) {
                    if (totalTaxList.hasOwnProperty(l)) {
                        var curTA = totalTaxList[l].tA;
                        var newTA = (toFloat(curTA) + toFloat(deliverProducts[k].pTax.tL[l].tA));
                        totalTaxList[l].tA = newTA.toFixed(2);
                    } else {
                        totalTaxList[l] = {tN: deliverProducts[k].pTax.tL[l].tN, tP: deliverProducts[k].pTax.tL[l].tP, tA: deliverProducts[k].pTax.tL[l].tA};
                    }
                }
            }

            var totalTaxHtml = "";
            for (var t in totalTaxList) {
                totalTaxHtml += totalTaxList[t].tN + "(" + totalTaxList[t].tP + "%)&nbsp" + accounting.formatMoney(totalTaxList[t].tA) + "<br>";
            }
            $('#totalTaxShow').html(totalTaxHtml);
        }
    }

    function setTaxListForProduct(productID, $currentRow) {
        if ((!jQuery.isEmptyObject(currentProducts[productID].tax))) {
            productTax = currentProducts[productID].tax;
            $currentRow.find('#taxApplied').removeClass('glyphicon glyphicon-unchecked');
            $currentRow.find('#taxApplied').addClass('glyphicon glyphicon-check');
            $currentRow.find('.tempLi').remove();
            $currentRow.find('#addNewTax').attr('disabled', false);
            for (var i in productTax) {
                var clonedLi = $($currentRow.find('#sampleLi').clone()).removeClass('hidden').attr('id', 'li_' + i).addClass("tempLi");
                clonedLi.children(".taxChecks").attr('id', productID + '_' + i).prop('checked', true).addClass('addNewTaxCheck');
                if (productTax[i].tS == 0) {
                    clonedLi.children(".taxChecks").attr('disabled', 'disabled');
                    clonedLi.children(".taxName").addClass('crossText');
                }
                clonedLi.children(".taxName").html('&nbsp&nbsp' + productTax[i].tN + '&nbsp&nbsp' + productTax[i].tP + '%').attr('for', productID + '_' + i);
                clonedLi.insertBefore($currentRow.find('#sampleLi'));
            }
        } else {
            $('#taxApplied').addClass('glyphicon glyphicon-unchecked');
            $currentRow.find('#addNewTax').attr('disabled', true);
        }
    }

    $('#deliveryNoteCancel').on('click', function() {
        window.location.reload();
    });

    $('#showTax').on('click', function() {
        if (this.checked) {
            if (!jQuery.isEmptyObject(deliverProducts)) {
                $('#totalTaxShow').removeClass('hidden');
            } else {
                p_notification(false, eb.getMessage('ERR_GRN_PROD_TAX'));
            }

        } else {
            $('#totalTaxShow').addClass('hidden');
        }

    });
    $('#deliveryChargeEnable').on('click', function() {
        setDeliveryNoteTotalCost();
        if (this.checked) {
            $('.deliCharges').removeClass('hidden');
            $('#deliveryCharge').focus();
        }
        else {
            $('#deliveryCharge').val(0);
            $('.deliCharges').addClass('hidden');
        }
    });
    $('#deliveryCharge').on('change keyup', function() {
        if (!$.isNumeric($('#deliveryCharge').val()) || $(this).val() < 0) {
            $(this).val('');
            setDeliveryNoteTotalCost();
        } else {
            setDeliveryNoteTotalCost();
        }
    });
    var nowTemp = new Date();
    var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);
    var checkin = $('#deliveryDate').datepicker({onRender: function(date) {
            return (date.valueOf() > now.valueOf() ? ((U_DATE_R_OVERRIDE) ? '' : 'disabled') : '');
        },
        //format: 'yyyy-mm-dd'
    }).on('changeDate', function(ev) {
        checkin.hide();
    }).data('datepicker');
    function resetDeliveryNotePage() {
        clearProductScreen();
        deliverProducts = {};
        checkDeliverProducts = {};
        deliverSubProducts = {};
        $('#salesPersonID').attr('disabled', false);
        $('#salesPersonID').selectpicker('render');
        $('#add-new-item-row').find('tr').each(function() {
            if (!$(this).hasClass('add-row')) {
                $(this).remove();
            }
        });
        $('#subtotal').val('');
        $('#finaltotal').val('');
        $('#totalTaxShow').html('');
        $('#priceListId').val('').attr('disabled', false).selectpicker('refresh');
        $('#priceListId').trigger('change');
    }


    $('#customCurrencyId').on('change', function() {
        eb.ajax({
            type: 'POST',
            url: BASE_URL + '/currencySymbol/getCustomCurrencyByCustomCurrencyId',
            data: {customCurrencyId: $(this).val()},
            success: function(respond) {
                if (respond.status == true) {
                    $('.cCurrency').text(respond.data.currencySymbol);
                }
            }
        });
    });

    $('#priceListId').on('change', function() {
        priceListId = $(this).val();
        if (priceListId) {
            eb.ajax({
                type: 'POST',
                url: BASE_URL + '/currencySymbol/getPriceListWithDiscount',
                data: {priceListId: priceListId},
                success: function(respond) {
                    if (respond.status == true) {
                        priceListItems = respond.data;
                    } else {
                        priceListItems = [];
                    }
                },
                async: false
            });
        }

    });

});

function getCustomerProfilesDetails(customerID) {
    eb.ajax({
        type: 'POST',
        url: BASE_URL + '/customerAPI/getCustomerProfilesByCustomerID',
        data: {customerID: customerID},
        success: function(respond) {
            if (respond.status == true) {
                $('.cus_prof_div').removeClass('hidden');
                $('.cus-prof-select').removeClass('hidden');
                $('.default-cus-prof').addClass('hidden');
                $('.tooltip_for_default_cus').removeClass('hidden');
                setCustomerProfilePicker(respond.data['customerProfiles']);
            } else {
                $('.cus_prof_div').addClass('hidden');
                $('.cus-prof-select').addClass('hidden');
                $('.default-cus-prof').removeClass('hidden');
                $('.tooltip_for_default_cus').addClass('hidden');
            }
        }
    });
}

function setCustomerProfilePicker(data) {
    $('#cusProfileDN').html("<option value=''>" + "Select a Customer Profile" + "</option>");
    $.each(data, function(index, value) {
        $('#cusProfileDN').append("<option value='" + index + "'>" + value + "</option>");

    });
    $('#cusProfileDN').selectpicker('refresh');

    $('#cusProfileDN').on('change', function(e) {
        e.preventDefault();
        cusProfID = $(this).val();

    });
}
