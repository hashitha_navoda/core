var customerID;
var editflag = 0;
var invoice = false;
var v;
var ff = new Array();
var columns;
var gender = null;
var custupdatemass = [];
var profileID = 0;
var customerProfileDataArray = {};
var $clonedCustomerProfileDisplay;
var nameregex = /^[\w\-\s]+$/;
var searchkey;
var cusID;
var EDIT_MODE = false;
var updatedCustomerProfiles = {};
$(document).ready(function() {
    var subCategory = {};
    var selectedFieldArr = [];
    var obj = {};
    var fileData = $('#fileData').val();
    var header = $('#fileHeader').val();
    var pathName = window.location.pathname.split('/');
    if (pathName[2] == "edit" || pathName[2] == "sales-customer-edit") {
        EDIT_MODE = true;
    } else {
        EDIT_MODE = false;
    }
    if($('#customerLoyaltyNo').attr('data-id') == 'editCus' && ($('#customerLoyaltyNo').val() != 0)){
        $('#customerLoyaltyNo').prop('disabled', true);
    }

	loadDropDownFromDatabase('/accounts-api/search-active-accounts-for-dropdown', "", "", '#customerReceviableAccountID');
	loadDropDownFromDatabase('/accounts-api/search-active-accounts-for-dropdown', "", "", '#customerSalesAccountID');
	loadDropDownFromDatabase('/accounts-api/search-active-accounts-for-dropdown', "", "", '#customerSalesDiscountAccountID');
	loadDropDownFromDatabase('/accounts-api/search-active-accounts-for-dropdown', "", "", '#customerAdvancePaymentAccountID');

    if (EDIT_MODE) {
        gender =  ($('.cusGender').val() == "") ? "" : $('.cusGender').val();
        

        if ($('.cusGender').val() == 'male') {
            $("#other-id").prop('checked', false);
            $("#female-id").prop('checked', false);
            $("#male-id").prop('checked', true);
        }

        if ($('.cusGender').val() == '' || $('.cusGender').val() == null) {
            $("#other-id").prop('checked', false);
            $("#female-id").prop('checked', false);
            $("#male-id").prop('checked', false);
        }

        if ($('.cusGender').val() == 'female') {
            $("#other-id").prop('checked', false);
            $("#male-id").prop('checked', false);
            $("#female-id").prop('checked', true);
        }

        if ($('.cusGender').val() == 'other') {
            $("#male-id").prop('checked', false);
            $("#female-id").prop('checked', false);
            $("#other-id").prop('checked', true);
        }
    }

	var customerReceviableAccountID = $('#customerReceviableAccountID').data('id');
    if(customerReceviableAccountID!=''){
    	var customerReceviableAccountName = $('#customerReceviableAccountID').data('value');
    	$('#customerReceviableAccountID').append("<option value='"+customerReceviableAccountID+"'>"+customerReceviableAccountName+"</option>")
    	$('#customerReceviableAccountID').val(customerReceviableAccountID).selectpicker('refresh');
    }

    var customerSalesAccountID = $('#customerSalesAccountID').data('id');
    if(customerSalesAccountID!=''){
    	var customerSalesAccountName = $('#customerSalesAccountID').data('value');
    	$('#customerSalesAccountID').append("<option value='"+customerSalesAccountID+"'>"+customerSalesAccountName+"</option>")
    	$('#customerSalesAccountID').val(customerSalesAccountID).selectpicker('refresh');
    }

    var customerSalesDiscountAccountID = $('#customerSalesDiscountAccountID').data('id');
    if(customerSalesDiscountAccountID!=''){
    	var customerSalesDiscountAccountName = $('#customerSalesDiscountAccountID').data('value');
    	$('#customerSalesDiscountAccountID').append("<option value='"+customerSalesDiscountAccountID+"'>"+customerSalesDiscountAccountName+"</option>")
    	$('#customerSalesDiscountAccountID').val(customerSalesDiscountAccountID).selectpicker('refresh');
    }

    var customerAdvancePaymentAccountID = $('#customerAdvancePaymentAccountID').data('id');
    if(customerAdvancePaymentAccountID!=''){
    	var customerAdvancePaymentAccountName = $('#customerAdvancePaymentAccountID').data('value');
    	$('#customerAdvancePaymentAccountID').append("<option value='"+customerAdvancePaymentAccountID+"'>"+customerAdvancePaymentAccountName+"</option>")
    	$('#customerAdvancePaymentAccountID').val(customerAdvancePaymentAccountID).selectpicker('refresh');
    }


    //  Set initial customer Profile btn div and set data-id
    $clonedCustomerProfileDisplay = $('#customerProfileDisplay').clone();

    var dateOfBirth = $("#customerDateOfBirth").datepicker({onRender: function(date) {
            return date.valueOf() > new Date() ? 'disabled' : '';
        }
    }).on('changeDate', function(ev) {
        dateOfBirth.hide();
    }).data('datepicker');

    $('.selectpicker').selectpicker({
    });

    $("#customerloading").hide();
    $(document).ajaxStart(function() {
        $("#customerloading").show();
    });
    $(document).ajaxStop(function() {
        $("#customerloading").hide();
    });
    $('#addcustomerbutton').show();
    $('#customerupdate').hide();
    $('#errorlog').hide();
    $("#customer_more").hide();
    $('#customerimport_more').hide();
    if (typeof customerRatingTypes != "undefined") {
        $.each(customerRatingTypes, function(index, value) {
            $('.customerRatingDiv').removeClass('hidden');
            var ratingTypeId = value.ratingTypesId;
            var ratingTypeValue = value.ratingTypesSize;
            var ratingTypeMethod = value.ratingTypesMethod;
            var ratingTypeName = value.ratingTypesCode + " - " + value.ratingTypesName;
            var $cloneRow = $($('.sampleCustRatingTypeRow').clone()).removeClass('hidden sampleCustRatingTypeRow').addClass('crRow');
            $cloneRow.find('.crtName').text(ratingTypeName);
            for (i = 1; i <= ratingTypeValue; i++) {
                $cloneRow.find('.custRatingValue').append($("<option/>", {
                    value: i,
                    text: i
                }));
            }
            var methodCss = "fontawesome-stars";
            var methodFlag = false;
            if (ratingTypeMethod == 2) {
                methodCss = "bars-square";
                methodFlag = true;
            }

            $cloneRow.data('ratingtypeid', ratingTypeId);
            $cloneRow.find('.custRatingValue').val(value.customerRatingsRatingValue);
            $cloneRow.find('.custRatingValue').barrating({
                theme: methodCss,
                showValues: methodFlag,
                showSelectedRating: false
            });
            $("#cRTtable tbody").append($cloneRow);
        });
    }

    //more details button
    $("#moredetails").click(function() {
        $("#customer_more").slideDown();
        $('#moredetails').hide();
        $('#customerCurrentBalance', '#addcustomerform').attr('disabled', true);
        $('#customerCurrentCredit', '#addcustomerform').attr('disabled', true);
    });

    // $('#customerName').on('change', function() {
    //     if (!invoice) {
    //         if ($('#searchcustomer').val() != undefined) {
    //             if ($('#customerName').val()) {
    //                 $('#customerupdate').show();
    //                 $('#customerFormsubmit').hide();
    //             }
    //         } else {
    //             $('#customerupdate').hide();
    //             $('#customerFormsubmit').show();
    //         }
    //     }

    // });

    $('#customerimportcancel').on('click', function() {
        window.location.reload();
    });

    var url2 = BASE_URL + '/customerAPI/getcustomerByID';
    loadDropDownFromDatabase('/customerAPI/search-customers-for-dropdown', "", 'withDeactivatedCustomers', '#searchcustomer');
    $('#searchcustomer').selectpicker('refresh');
    $('#searchcustomer').on('change', function() {
        if ($('#searchcustomer').val() > 0 && searchkey != $('#searchcustomer').val()) {
            searchkey = $('#searchcustomer').val();
            $('#errorlog').html("");
            var request2 = eb.post(url2, {customerID: searchkey});
            request2.done(function(customerdata) {
                if (customerdata.status) {
                    customerID = customerdata.data['customerData'].customerID;
                    window.location.href = BASE_URL + '/customer/sales-customer-edit/'+customerID;
                }
            });
        }
        return false;
    });

    $('#clear-select-customer').on('click', function() {
        $('#searchcustomer').val(0).trigger('change').empty().selectpicker('refresh');
        cleardata();
        $('#customerCode').val($('#customerCode').data('cuscode'));
        $('#customerupdate').hide();
        $('#customerFormsubmit').show();
    });

    if (typeof selectedCustomerProfileData !== 'undefined' && !jQuery.isEmptyObject(selectedCustomerProfileData)) {
        customerID = selectedCustomerID;
        $('#searchcustomer').val(customerID);
        $('#searchcustomer').selectpicker('render');
        setCustomerProfiles(selectedCustomerProfileData);
        $('#customerupdate').show();
        $('#customerFormsubmit').hide();
    } else {
        setCustomerProfiles();
    }

    $("#male").on('click', function(e) {
        gender = $("#male-id").val();
        $("#male-id").prop('checked', true);
        $("#female-id").prop('checked', false);
        $("#other-id").prop('checked', false);
    });

    $("#female").on('click', function(e) {
        gender = $("#female-id").val();
        $("#female-id").prop('checked', true);
        $("#male-id").prop('checked', false);
        $("#other-id").prop('checked', false);
    });

    $("#other").on('click', function(e) {
        gender = $("#other-id").val();
        $("#other-id").prop('checked', true);
        $("#female-id").prop('checked', false);
        $("#male-id").prop('checked', false);
    });


    var addurl = BASE_URL + '/customerAPI/add';
    $('#customerFormsubmit').on('click', function(e) {
        e.preventDefault();
        var customerRatingTypes = {};
        var flag = true;
        $('#cRTtable tbody tr').each(function() {
            if ($(this).hasClass('crRow')) {
                var ratingTypeRatingValue = $(this).find('.custRatingValue').val();
                var ratingTypeName = $(this).find('.crtName').text();
                if (ratingTypeRatingValue == "" || ratingTypeRatingValue == null) {
                    p_notification(false, eb.getMessage('ERR_CUST_RT_VALUE_CNT_NULL'));
                    flag = false;
                    return false;
                } else if (isNaN(ratingTypeRatingValue) || ratingTypeRatingValue < 0) {
                    p_notification(false, eb.getMessage('ERR_CUST_RT_VALUE_SHBE_INT'));
                    flag = false;
                    return false;
                } else {
                    customerRatingTypes[$(this).data('ratingtypeid')] = {ratingTypesRatingValue: ratingTypeRatingValue, ratingTypesName: ratingTypeName};
                }
            }
        });
        if (flag) {
            var params = new Array();
            params = {
                customerTitle: $('#customerTitle').val(),
                customerCode: $('#customerCode').val(),
                customerName: $('#customerName').val(),
                customerGender: gender,
                customerShortName: $('#customerShortName').val(),
                customerTelephoneNumber: $('#customerTelephoneNumber').val(),
                customerDateOfBirth: $('#customerDateOfBirth').val(),
                customerIdentityNumber: $('#customerIdentityNumber').val(),
                customerCategory: $('#customerCategory').val(),
                customerPriceList: $('#customerPriceList').val(),
                customerCurrency: $('#customerCurrency').val(),
                customerVatNumber: $('#customerVatNumber').val(),
                customerSVatNumber: $('#customerSVatNumber').val(),
                customerCreditLimit: $('#customerCreditLimit').val(),
                customerPaymentTerm: $('#customerPaymentTerm').val(),
                customerDiscount: $('#customerDiscount').val(),
                customerOther: $('#customerOther').val(),
                customerLoyaltyNo: $('#customerLoyaltyNo').val(),
                customerLoyaltyCode: $('#customerLoyaltyCode').val(),
                customerEvent: $('#customerEvent').val(),
                customerReceviableAccountID: $('#customerReceviableAccountID').val(),
                customerSalesAccountID: $('#customerSalesAccountID').val(),
                customerSalesDiscountAccountID: $('#customerSalesDiscountAccountID').val(),
                customerAdvancePaymentAccountID: $('#customerAdvancePaymentAccountID').val(),
                customerRatingTypes: customerRatingTypes,
            };
            var cusaddname = $('#customerName').val();
            var cusCode = $('#customerCode').val();
            if (cusaddname == null || cusaddname == "") {
                p_notification(false, eb.getMessage('ERR_CUST_ENTER_NAME'));
            } else if (cusCode == null || cusCode == "") {
                p_notification(false, eb.getMessage('ERR_CUST_EMPTY_CODE'));
            } else {
                if (validteinput(params)) {
                    params.customerProfile = customerProfileDataArray;
                    params.customerPrimaryProfileID = $('#primaryCustomerProfile').val();
                    var addrequest = eb.post(addurl, params);
                    addrequest.done(function(addretdata) {
                        p_notification(addretdata.status, addretdata.msg);
                        if (addretdata.status == true) {
                            customerID = addretdata.data;
                            if (invoice) {
                                AddCustomerFlag = true;
                                getCustomerDetails(customerID);
                            }
                            cleardata();
                            if ($('#addCustomerModal').length) {
                                $('#addCustomerModal').modal('hide');
                            } else {
                                window.location.reload();
                            }
                        }
                    });
                }

            }
        }
    });
    $('#cancelbutton').click(function() {
        location.reload();
    });

    loadDropDownFromDatabase('/customerAPI/search-customers-for-dropdown', "", 'withDeactivatedCustomers', '#customerSearch');
    $('#customerSearch').selectpicker('refresh');
    $('#customerSearch').on('change', function() {
        if ($(this).val() > 0 && cusID != $(this).val()) {
            var urlArr = window.location.pathname.split("/");
            var isSale = (urlArr[2]) ? true: false;
            cusID = $(this).val();
            searchurl = BASE_URL + '/customerAPI/get-customer-from-search-by-customerID';
            var searchrequest = eb.post(searchurl, {customerID: cusID, isSale: isSale});
            searchrequest.done(function(searchdata) {
                if (!searchdata.status) {
                    p_notification(searchdata.status, searchdata.msg)
                }
                $('#customer-list').html(searchdata.html);
            });
        }
        return false;
    });

    $('#clear-search-customer').on('click', function() {
        var searchurl = BASE_URL + '/customer/index';
        var searchrequest = eb.post(searchurl, {search: true});
        searchrequest.done(function(searchdata) {
            $('#customer-list').html(searchdata);
            $('#customerSearch').val('').trigger('change');
            cusID = null;
        });
    });

    $(document).on('click', '#customerupdate', function(e) {
        var loyaltyCode = $('#customerLoyaltyCode').val();
        if($('#customerLoyaltyNo').attr('data-id') == 'editCus' && ($('#customerLoyaltyNo').attr('data-code') != '')){
            loyaltyCode = $('#customerLoyaltyNo').attr('data-code');
        }

        $('#errorlog').html = "";
        e.preventDefault();
        var updateurl = BASE_URL + '/customerAPI/updatecustomer';

        var customerRatingTypes = {};
        var flag = true;
        $('#cRTtable tbody tr').each(function() {
            if ($(this).hasClass('crRow')) {
                var ratingTypeRatingValue = $(this).find('.custRatingValue').val();
                var ratingTypeName = $(this).find('.crtName').text();
                if (ratingTypeRatingValue == "" || ratingTypeRatingValue == null) {
                    p_notification(false, eb.getMessage('ERR_CUST_RT_VALUE_CNT_NULL'));
                    flag = false;
                    return false;
                } else if (isNaN(ratingTypeRatingValue) || ratingTypeRatingValue < 0) {
                    p_notification(false, eb.getMessage('ERR_CUST_RT_VALUE_SHBE_INT'));
                    flag = false;
                    return false;
                } else {
                    customerRatingTypes[$(this).data('ratingtypeid')] = {ratingTypesRatingValue: ratingTypeRatingValue, ratingTypesName: ratingTypeName};
                }
            }
        });
        if (flag) {
            var params = {};
            params = {
                customerID: customerID,
                customerTitle: $('#customerTitle').val(),
                customerCode: $('#customerCode').val(),
                customerName: $('#customerName').val(),
                customerShortName: $('#customerShortName').val(),
                customerGender: gender,
                customerTelephoneNumber: $('#customerTelephoneNumber').val(),
                customerDateOfBirth: $('#customerDateOfBirth').val(),
                customerIdentityNumber: $('#customerIdentityNumber').val(),
                customerCategory: $('#customerCategory').val(),
                customerPriceList: $('#customerPriceList').val(),
                customerCurrency: $('#customerCurrency').val(),
                customerVatNumber: $('#customerVatNumber').val(),
                customerSVatNumber: $('#customerSVatNumber').val(),
                customerCreditLimit: $('#customerCreditLimit').val(),
                customerPaymentTerm: $('#customerPaymentTerm').val(),
                customerDiscount: $('#customerDiscount').val(),
                customerOther: $('#customerOther').val(),
                customerLoyaltyNo: $('#customerLoyaltyNo').val(),
                customerLoyaltyCode: loyaltyCode,
                customerEvent: $('#customerEvent').val(),
                customerRatingTypes: customerRatingTypes,
                customerReceviableAccountID: $('#customerReceviableAccountID').val(),
                customerSalesAccountID: $('#customerSalesAccountID').val(),
                customerSalesDiscountAccountID: $('#customerSalesDiscountAccountID').val(),
                customerAdvancePaymentAccountID: $('#customerAdvancePaymentAccountID').val(),
                customerPrimaryProfileID: $('#primaryCustomerProfile').val(),
//                customerProfile: customerProfileDataArray,
            };
            if (validteinput(params)) {
                params.customerProfile = updatedCustomerProfiles;
                var updaterequest = eb.post(updateurl, params);
                updaterequest.done(function(getdata) {
                    p_notification(getdata.status, getdata.msg);
                    if (getdata.status == true) {
                        $('#create-customer-form-div').modal('hide');
                        window.setTimeout(function() {
                            if (document.URL.indexOf('sales-customer') > 0) {
                                window.location.href = BASE_URL + '/customer/sales-customer-index';
                            } else {
                                window.location.href = BASE_URL + '/customer/index';
                            }
                        }, 1000);
                    }
                });
            }
        }

    });
    var $customermodal = $('#create-customer-form-div');
    $customermodal.on('hide.bs.modal', function() {
        $customermodal.removeData();
        return true;
    });

     $('#customer-list').on('click', 'a.status', function() {
        var msg;
        var status;
        var flag = true;
        var customerID = this.id
        var currentDiv = $(this).contents();
        if ($(this).children().hasClass('fa-check-square-o')) {
            msg = 'Are you sure you want to Deactivate this customer';
            status = '2';
        }
        else if ($(this).children().hasClass('fa-square-o')) {
            msg = 'Are you sure you want to Activate this customer';
            status = '1';
        }
        activeInactiveType(customerID, status, currentDiv, msg, flag);
    });

    function activeInactiveType(customerID, status, currentDiv, msg, flag) {
        bootbox.confirm(msg, function($result) {
            if ($result == true) {
                eb.ajax({
                    url: BASE_URL + '/customerAPI/changeStatusID',
                    method: 'post',
                    dataType: 'json',
                    data: {
                        'customerID': customerID,
                        'status': status,
                    },
                    success: function(data) {
                        if (data.status) {
                            p_notification(data.status, data.msg);
                            if (currentDiv.hasClass('fa-square-o') && flag) {
                                currentDiv.addClass('fa-check-square-o');
                                currentDiv.removeClass('fa-square-o');
                            }
                            else {
                                currentDiv.addClass('fa-square-o');
                                currentDiv.removeClass('fa-check-square-o');
                            }
                        } else {
                            p_notification(data.status, data.msg);
                        }
                    },
                    sync: 'false'
                });
            }
        });
    }

    $(document).on('click', '.deleteCustomer', function(e) {
        e.preventDefault();
        var id = this.id;
        $(document).on('click', '#delete-customer-button', function() {
            var deleteCustomerUrl = BASE_URL + '/customerAPI/deleteCustomer';
            eb.post(deleteCustomerUrl, {customerID: id}).done(function(data) {
                p_notification(data.status, data.msg);
                window.setTimeout(function() {
                    location.reload();
                }, 2000);
            });
        });
    });

    $('#editclose').on('click', function() {
        if (editflag == 1) {
            editflag = 0;
            location.reload();
        }
    });

    //customer_import show_more_details button
    var importmoreflag = 0;
    $("#moredetailsimport").click(function() {
        if (importmoreflag == 0) {
            $("#customerimport_more").slideDown(function() {
                $("#moredetailsimport").html('Hide details ' + '<span class="glyphicon glyphicon-circle-arrow-up"></span>');
            });
            importmoreflag = 1;
        } else {
            $("#customerimport_more").slideUp(function() {
                $('#moredetailsimport').html('Show more details ' + '<span class="glyphicon glyphicon-circle-arrow-down"></span>');
            });
            importmoreflag = 0;
        }
    });

    columns = $('#importcolumncount').html();
    for (var k = 0; k < columns; k++) {
        ff[k] = 'Unmapped';
    }


    var $importmodal = $('#customer-import-form-div');
    $importmodal.on('hide.bs.modal', function() {
        $importmodal.removeData();
        return true;
    });
    $('#importdatabutton').on('click', function() {
        var cnameFlag = 0;
        var ctitleFlag = 0;
        var ccategoryFlag = 0;
        for (var t = 0; t < columns; t++) {
            if (ff[t] == 'Customer Name') {
                cnameFlag = 1;
            } else if (ff[t] == 'Telephone') {
                ctitleFlag = 1;
            } else if (ff[t] == 'Customer Category') {
                ccategoryFlag = 1;
            }
        }
        
        if (cnameFlag == 1 && ctitleFlag == 1) {
            if (ccategoryFlag == 1) {
                if (Object.keys(subCategory).length > 0) {
                    var importmodalurl = BASE_URL + '/customerAPI/getImportConfirm';
                    $importmodal.load(importmodalurl, '', function() {
                        $importmodal.modal('show');
                        $('#import-button').on('click', function() {
                            var radiovalue = $('input:radio[name=importmethod]:checked').val();
                            if (radiovalue == 'discard') {
                                customerimport('discard');
                            }
                            else if (radiovalue == 'replace') {
                                customerimport('replace');
                            } else {
                                p_notification(false, eb.getMessage('ERR_CUST_IMPORT'));
                            }
                        });
                    });    
                } else {
                    p_notification(false, eb.getMessage('ERR_CUST_CAT_SUB'));
                }
            } else {
                var importmodalurl = BASE_URL + '/customerAPI/getImportConfirm';
                $importmodal.load(importmodalurl, '', function() {
                    $importmodal.modal('show');
                    $('#import-button').on('click', function() {
                        var radiovalue = $('input:radio[name=importmethod]:checked').val();
                        if (radiovalue == 'discard') {
                            customerimport('discard');
                        }
                        else if (radiovalue == 'replace') {
                            customerimport('replace');
                        } else {
                            p_notification(false, eb.getMessage('ERR_CUST_IMPORT'));
                        }
                    });
                });
            }
        } else {
            p_notification(false, eb.getMessage('ERR_CUST_DETAILS'));
        }
    });
    function customerimport(method) {
        var importurl = BASE_URL + '/customerAPI/importcustomer';
        var importrequest = eb.post(importurl, {
            method: method,
            header: $('#importheader').html(),
            delim: $('#importdelim').html(),
            columncount: $('#importcolumncount').html(),
            col: ff,
            subCategory: subCategory,
        });
        importrequest.done(function(retdata) {
            p_notification(retdata.status, retdata.msg);
            if (retdata.status) {
                window.setTimeout(function() {
                    window.location.assign(BASE_URL + '/customer/import');
                }, 1500);
            }

        });
    }

    $('.customerimportselector').on('change', function(event, modalEnable) {
        var selectid = this.id;
        var selectedoption = $(this).val();
        enableoptions(ff[selectid]);
        ff[selectid] = $(this).val();
        var $tr = $(this).closest('tr');
        var column = $tr.data('column');
        var enumModalStatus = (modalEnable === undefined) ? true : modalEnable;        
        var html = '';
        
        if (selectedoption != 'Unmapped') {
            disableoptions(selectedoption);
        }

        if (selectedoption == "Customer Category") {
            generateEnumeratorModal( column, 'customerCategory', fileData, header, false, enumModalStatus, {}, function (res){
                if(res) {
                    $tr.find('.enum-modal-btn').removeClass('hidden');
                }
            });
        } else {
            $tr.find('.enum-modal-btn').addClass('hidden');
        }

        selectedFieldArr = [];
        $('.customerimportselector').each(function(index,element){
            var selectedValue = $(this).val();
            if(selectedValue && $.inArray(selectedValue,selectedFieldArr) == -1){
                selectedFieldArr.push(selectedValue);
            }
        });
    });

    $('#selectAllcustomer').on('click', function() {
        custselectedrows = [];
        var rowCount = $('#customerdatatablebody tr').length;
        if (this.checked) {
            $(".selectcustomer").prop('checked', true);
            for (var c = 0; c < rowCount; c++) {
                custselectedrows.push(c);
            }
        }
        else {
            $(".selectcustomer").prop('checked', false);
            for (var c = 0; c < rowCount; c++) {
                custselectedrows.splice(custselectedrows.indexOf(c), 1);
            }
        }
    });

    var custselectedrows = [];
    $('.selectcustomer').on('click', function() {
        var ctrid = parseInt($(this).closest('tr').attr('id'));
        if (this.checked) {
            custselectedrows.push(ctrid);
        } else {
            $('#selectAllcustomer').attr('checked', false);
            custselectedrows.splice(custselectedrows.indexOf(ctrid), 1);
        }
    });

    $('#custpayment').on('click', function() {
        if ($(this).val() != 'Payment') {
            for (var l = 0; l < custselectedrows.length; l++) {
                $('#' + custselectedrows[l]).find('td').eq(10).text($(this).val());
            }
        }
    });
    $('#custcurrency').on('click', function() {
        if ($(this).val() != 'Currency') {
            for (var l = 0; l < custselectedrows.length; l++) {
                $('#' + custselectedrows[l]).find('td').eq(5).text($(this).val());
            }
        }
    });


    $('body').on('focusout', '[contenteditable]', function() {
        var $this = $(this);
        if ($this.hasClass('massTelephoneNumber') || $this.hasClass('massAdditionalTelephoneNumber')) {
            var ctelephoneNumber = $(this).text();
            isPhone = false;
            if (ctelephoneNumber.length != 0) {
                if (ctelephoneNumber.length < 9 || ctelephoneNumber.length > 15) {
                    isPhone = true;
                }
            }
            if (isNaN(ctelephoneNumber) || isPhone) {
                p_notification(false, eb.getMessage('ERR_CUST_TPNUM_VALIDITY'));
                ctelephoneNumber = "";
                $(this).text('');
            }

        } else if ($this.hasClass('massEmail')) {
            var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
            var cemail = $(this).text();
            var isEmail = true;
            if (cemail == '') {
                isEmail = false;
            }
            if (isEmail && !regex.test(cemail)) {
                p_notification(false, eb.getMessage('ERR_CUST_VALID_EMAIL'));
                $(this).text('');
            }
        } else if ($this.hasClass('massCreditLimit')) {
            var creditLimit = $(this).text();
            if (isNaN(creditLimit)) {
                p_notification(false, eb.getMessage('ERR_CUST_CRDTLIMIT_NUMR'));
                $(this).text('');
            } else if (creditLimit < 0) {
                p_notification(false, eb.getMessage('ERR_CUST_CRDTLIMIT_NEG'));
                $(this).text('');
            }
        } else if ($this.hasClass('massDiscount')) {
            var discount = $(this).text();
            if (isNaN(discount) || parseInt(discount) > 100 || discount < 0) {
                p_notification(false, eb.getMessage('ERR_CUST_DISCOUNT_VALIDITY'));
                $(this).text('');
            }
        }
    });

//    add new customer Category
    $('#customerCategory').on('change', function() {
        if ($(this).val() === 'addNewCustomerCategory') {
            $('#customerCategory').val('');
            $("input[name='newCustomerCategoryName']", $('#addNewCustomerCategoryModal')).val('');
            $('#addNewCustomerCategoryModal').modal('show');
        }
    });
    $('#addNewCustomerCategoryModal').on('shown.bs.modal', function() {
        $(this).find("input[name='newCustomerCategoryName']").focus();
    });
//    new customer Category save
    $('#addNewCustomerCategoryModal').on('submit', function() {
        var newCustomerCategoryName = $("input[name='newCustomerCategoryName']", $(this)).val();
        if (newCustomerCategoryName === '') {
            p_notification(false, eb.getMessage('ERR_CUST_PROFILE_EMPTY_NAME'));
            $("input[name='newCustomerCategoryName']", $(this)).focus();
            return false;
        }
        var addNewCustomerCategoryUrl = BASE_URL + '/customerAPI/add-new-customer-category';
        var addNewCustomerCategoryRequest = eb.post(addNewCustomerCategoryUrl, {
            customerCategoryName: newCustomerCategoryName
        });
        addNewCustomerCategoryRequest.done(function(response) {
            p_notification(response.status, response.msg);
            if (response.status) {
                $("input[name='newCustomerCategoryName']", $(this)).val('');
                $('#addNewCustomerCategoryModal').modal('hide');
                $('#customerCategory').append($("<option value='" + response.data[0].customerCategoryID + "'>" + response.data[0].customerCategoryName + "</option>"));
                $('#customerCategory').val(response.data[0].customerCategoryID);
            } else {

                $("input[name='newCustomerCategoryName']", '#addNewCustomerCategoryModal').focus();
            }
        });
        return false;
    });

    $('#addcustomerform').on('click', '.customerProfileEditBtn', function() {
        var currentProfileID = $(this).parents('.customerProfileDisplay').data('id');
        $('#customerProfileModal').attr('data-id', currentProfileID);
        setDataToCustomerProfileForm(customerProfileDataArray[currentProfileID]);
        $('#customerProfileModal').modal('show');
    });

    $('#addcustomerform').on('click', '.customerProfileDeleteBtn', function() {
        var currentProfileDisplay = $(this).parents('.customerProfileDisplay');
        bootbox.confirm("Are you sure you want to delete this Customer Profile?", function(result) {
            if (result === true) {
                if ($('.customerProfileDeleteBtn').length > 2) {
                    var currentProfileID = currentProfileDisplay.data('id');
                    currentProfileDisplay.remove();
                    if (EDIT_MODE) {
                        updatedCustomerProfiles['delete_'+currentProfileID] = customerProfileDataArray[currentProfileID];
                    } else {
                        delete customerProfileDataArray[currentProfileID];
                    }
                    setPrimaryProfileDropDown(customerProfileDataArray);
                } else {
                    p_notification(false, eb.getMessage('ERR_CUST_PROFILE_DEL'));
                }
            }
        });
    });


    $('#customerProfileForm', '#customerProfileModal').on('reset', function() {
        var profileID = $('#customerProfileModal').attr('data-id');
        setDataToCustomerProfileForm(customerProfileDataArray[profileID]);
        return false;
    });
    $('#customerProfileForm', '#customerProfileModal').on('submit', function() {
        var customerProfileRatingTypes = {};
        var flag = true;
        $('#cProfileRTtable tbody tr').each(function() {
            if ($(this).hasClass('cProfilerRow')) {
                var ratingTypeRatingValue = $(this).find('.custProfileRatingValue').val();
                var ratingTypeName = $(this).find('.cProfilertName').text();
                var ratingTypeMethod = $(this).data('ratingTypeMethod');
                if (ratingTypeRatingValue == "" || ratingTypeRatingValue == null) {
                    p_notification(false, eb.getMessage('ERR_CUST_PROFILE_RT_VALUE_CNT_NULL'));
                    flag = false;
                    return false;
                } else if (isNaN(ratingTypeRatingValue) || ratingTypeRatingValue < 0) {
                    p_notification(false, eb.getMessage('ERR_CUST_PROFILE_RT_VALUE_SHBE_INT'));
                    flag = false;
                    return false;
                } else {
                    customerProfileRatingTypes[$(this).data('ratingtypeid')] = {ratingTypesRatingValue: ratingTypeRatingValue, ratingTypesName: ratingTypeName, ratingTypeMethod: ratingTypeMethod};
                }

            }
        });
        if (flag) {
            tempDataArray = {
                customerProfileName: $("input[name='customerProfileName']", '#customerProfileModal').val().trim(),
                customerProfileMobileTP1: $("input[name='customerProfileMobileTP1']", '#customerProfileModal').val().trim(),
                customerProfileMobileTP2: $("input[name='customerProfileMobileTP2']", '#customerProfileModal').val().trim(),
                customerProfileLandTP1: $("input[name='customerProfileLandTP1']", '#customerProfileModal').val().trim(),
                customerProfileLandTP2: $("input[name='customerProfileLandTP2']", '#customerProfileModal').val().trim(),
                customerProfileFaxNo: $("input[name='customerProfileFaxNo']", '#customerProfileModal').val().trim(),
                customerProfileEmail: $("input[name='customerProfileEmail']", '#customerProfileModal').val().trim(),
                customerProfileContactPersonName: $("input[name='customerProfileContactPersonName']", '#customerProfileModal').val().trim(),
                customerProfileContactPersonNumber: $("input[name='customerProfileContactPersonNumber']", '#customerProfileModal').val().trim(),
                customerProfileLocationNo: $("input[name='customerProfileLocationNo']", '#customerProfileModal').val().trim(),
                customerProfileLocationRoadName1: $("input[name='customerProfileLocationRoadName1']", '#customerProfileModal').val().trim(),
                customerProfileLocationRoadName2: $("input[name='customerProfileLocationRoadName2']", '#customerProfileModal').val().trim(),
                customerProfileLocationRoadName3: $("input[name='customerProfileLocationRoadName3']", '#customerProfileModal').val().trim(),
                customerProfileLocationSubTown: $("input[name='customerProfileLocationSubTown']", '#customerProfileModal').val().trim(),
                customerProfileLocationTown: $("input[name='customerProfileLocationTown']", '#customerProfileModal').val().trim(),
                customerProfileLocationPostalCode: $("input[name='customerProfileLocationPostalCode']", '#customerProfileModal').val().trim(),
                customerProfileLocationCountry: $("select[name='customerProfileLocationCountry']", '#customerProfileModal').val(),
                customerProfileRatingTypes: customerProfileRatingTypes,
            };
            if (validateCustomerProfileForm(tempDataArray)) {
                var currentProfileName = tempDataArray.customerProfileName;
                var currentProfileID = $('#customerProfileModal').attr('data-id');
//          Check alreay exists same in another profile for same customer
                for (var key in customerProfileDataArray) {
                    if (customerProfileDataArray[key].customerProfileName == currentProfileName && key !== currentProfileID) {
                        p_notification(false, eb.getMessage('ERR_CUST_PROFILE_EXIST_NAME'));
                        return false;
                    }
                }
                $(".customerProfileDisplay[data-id='" + currentProfileID + "']").find("input[name='customerProfileName']", '#addcustomerform').val(currentProfileName);
                customerProfileDataArray[currentProfileID] = tempDataArray;
                if (EDIT_MODE) {
                    updatedCustomerProfiles[currentProfileID] = customerProfileDataArray[currentProfileID]; 
                }
                clearCustomerProfileForm();
                setPrimaryProfileDropDown(customerProfileDataArray);
                $('#customerProfileModal').modal('hide');
            }
        }

        return false;
    });
    $('#addcustomerform').on('click', '#newCustomerProfile', function() {
        var $newProfileName = $(this).parents('.newProfileName').find('#newProfileName');
        var checkExistsName = _.findWhere(customerProfileDataArray, {customerProfileName: $newProfileName.val()});
        if ($newProfileName.val() === null || $newProfileName.val() === '') {
            $newProfileName.focus();
            return false;
        } else if (checkExistsName) {
            p_notification(false, eb.getMessage('ERR_CUST_PROFILE_EXIST_NAME'));
            $newProfileName.focus();
            return false;
        }

        profileID++;
        var clonedCustomerProfileDisplay = $clonedCustomerProfileDisplay.clone();
        $("input[name='customerProfileName']", clonedCustomerProfileDisplay).val($newProfileName.val());
        customerProfileDataArray['unsavedProfile' + profileID] = {customerProfileName: $newProfileName.val()};
        clonedCustomerProfileDisplay.attr('data-id', 'unsavedProfile' + profileID);
        clonedCustomerProfileDisplay.removeAttr('id');
        clonedCustomerProfileDisplay.removeClass('hidden');
        clonedCustomerProfileDisplay.insertAfter('.customerProfileDisplay:last');
        setPrimaryProfileDropDown(customerProfileDataArray)
        clonedCustomerProfileDisplay.find('.customerProfileEditBtn').click();
        $newProfileName.val('');
    });

    $('.cMEA_btn').on('click', function() {
        var cID = $(this).data('id');
        $('#cMEA_Modal').data('id', cID);
        $("input[name='cMEA_locationNo']", '#cMEA_Modal').val(customersAddress[cID].cMEA_locationNo);
        $("input[name='cMEA_roadName1']", '#cMEA_Modal').val(customersAddress[cID].cMEA_roadName1);
        $("input[name='cMEA_roadName2']", '#cMEA_Modal').val(customersAddress[cID].cMEA_roadName2);
        $("input[name='cMEA_roadName3']", '#cMEA_Modal').val(customersAddress[cID].cMEA_roadName3);
        $("input[name='cMEA_subTown']", '#cMEA_Modal').val(customersAddress[cID].cMEA_subTown);
        $("input[name='cMEA_town']", '#cMEA_Modal').val(customersAddress[cID].cMEA_town);
        $("input[name='cMEA_postalCode']", '#cMEA_Modal').val(customersAddress[cID].cMEA_postalCode);
        $("select[name='cMEA_country']", '#cMEA_Modal').val(customersAddress[cID].cMEA_country);
        $("select[name='cMEA_country']", '#cMEA_Modal').selectpicker('render');
        $('#cMEA_Modal').modal('show');
    });

    $('#cMEA_Form', '#cMEA_Modal').on('submit', function() {
        var cID = $('#cMEA_Modal').data('id');
        customersAddress[cID].cMEA_locationNo = $("input[name='cMEA_locationNo']", '#cMEA_Modal').val();
        customersAddress[cID].cMEA_roadName1 = $("input[name='cMEA_roadName1']", '#cMEA_Modal').val();
        customersAddress[cID].cMEA_roadName2 = $("input[name='cMEA_roadName2']", '#cMEA_Modal').val();
        customersAddress[cID].cMEA_roadName3 = $("input[name='cMEA_roadName3']", '#cMEA_Modal').val();
        customersAddress[cID].cMEA_subTown = $("input[name='cMEA_subTown']", '#cMEA_Modal').val();
        customersAddress[cID].cMEA_town = $("input[name='cMEA_town']", '#cMEA_Modal').val();
        customersAddress[cID].cMEA_postalCode = $("input[name='cMEA_postalCode']", '#cMEA_Modal').val();
        customersAddress[cID].cMEA_country = $("select[name='cMEA_country']", '#cMEA_Modal').val();
        $('#cMEA_Modal').modal('hide');
        return false;
    });


    $('#customer-savemassedit-button').on('click', function() {
        $('#customerdatatable tbody tr').each(function() {
            var custupdate = {};
            var tdc = $(this).find('td');
            custupdate['customerID'] = tdc.eq(0).text();
            custupdate['customerTitle'] = tdc.eq(1).find('select').val();
            custupdate['customerName'] = tdc.eq(2).text();
            custupdate['customerCode'] = tdc.eq(3).text();
            custupdate['customerShortName'] = tdc.eq(4).text();
            custupdate['customerCurrency'] = tdc.eq(5).text();
            custupdate['customerTelephoneNumber'] = tdc.eq(6).text();
            custupdate['customerEmail'] = tdc.eq(7).text();
            custupdate['customerVatNumber'] = tdc.eq(9).text();
            custupdate['customerSVatNumber'] = tdc.eq(10).text();
            custupdate['customerPaymentTerm'] = tdc.eq(11).text();
            custupdate['customerCategory'] = tdc.eq(12).find('select').val();
            custupdate['customerDateOfBirth'] = tdc.eq(13).text();
            custupdate['customerIdentityNumber'] = tdc.eq(14).text();
            custupdate['customerCreditLimit'] = tdc.eq(15).text();
            custupdate['customerDiscount'] = tdc.eq(18).text();
            custupdatemass.push(custupdate);
        });

        var custupdateurl = BASE_URL + '/customerAPI/massupdate';
        $('.main_div').addClass('Ajaxloading');
        var customerupdaterequest = eb.post(custupdateurl, {
            cupd: custupdatemass,
            custAddress: customersAddress
        });
        customerupdaterequest.done(function(retupdate) {
            p_notification(retupdate.status, retupdate.msg);
            if (retupdate.status) {
                window.setTimeout(function() {
                    location.reload();
                }, 1000);
            }
        });
    });

    $('#customerLoyaltyNo').on('change', function() {
        var url = BASE_URL + '/loyalty/getAvailableLoyaltyCodes';
        var code_list = eb.post(url, {loyalty_type: $(this).val()});

        code_list.done(function(code_list) {
            $('#customerLoyaltyCode').html('');

            $.each(code_list.data, function(key, value) {
                $('#customerLoyaltyCode')
                        .append($('<option>', {value: key})
                                .text(value));
            });

            $('#customerLoyaltyCode').selectpicker('refresh');
        });
    });


    $('#customerRatingTypes').on('change', function() {
        $('.customerRatingDiv').removeClass('hidden');
        var ratingTypeId = $(this).val();
        var ratingTypeValue = $(this).find(':selected').data('rtvalue');
        var ratingTypeMethod = $(this).find(':selected').data('rtmethod');
        var notAlredyExist = true;
        $("#cRTtable tbody tr").each(function() {
            if ($(this).hasClass('crRow')) {
                if ($(this).data('ratingtypeid') == ratingTypeId) {
                    notAlredyExist = false;
                    p_notification(false, eb.getMessage('ERR_CUST_RATING_TYPES_ALREDY_EXIXT'));
                    return false;
                }
            }
        });
        if (notAlredyExist) {
            var ratingTypeName = $("#customerRatingTypes option[value=" + ratingTypeId + "]").text();
            var $cloneRow = $($('.sampleCustRatingTypeRow').clone()).removeClass('hidden sampleCustRatingTypeRow').addClass('crRow');
            $cloneRow.find('.crtName').text(ratingTypeName);

            for (i = 1; i < ratingTypeValue + 1; i++) {
                $cloneRow.find('.custRatingValue').append($("<option/>", {
                    value: i,
                    text: i
                }));
            }
            var methodCss = "fontawesome-stars";
            var methodFlag = false;
            if (ratingTypeMethod == 2) {
                methodCss = "bars-square";
                methodFlag = true;
            }

            $cloneRow.data('ratingtypeid', ratingTypeId);
            $cloneRow.find('.custRatingValue').barrating({
                theme: methodCss,
                showValues: methodFlag,
                showSelectedRating: false
            });
            $("#cRTtable tbody").append($cloneRow);

        }
        $('#customerRatingTypes').val('').selectpicker('refresh');
    });

    $('#cRTtable tbody').on('click', '.deleteCRT', function() {
        var $row = $(this).parents('tr');
        $row.remove();
    });

    $('#clearbutton').on('click', function() {
        $('.crRow').remove();
        $('.customerRatingDiv').addClass('hidden');
    });

    $('#customerProfileRatingTypes').on('change', function() {
        $('.customerProfileRatingDiv').removeClass('hidden');
        var ratingTypeId = $(this).val();
        var ratingTypeValue = $(this).find(':selected').data('rtvalue');
        var ratingTypeMethod = $(this).find(':selected').data('rtmethod');
        var notAlredyExist = true;
        $("#cProfileRTtable tbody tr").each(function() {
            if ($(this).hasClass('cProfilerRow')) {
                if ($(this).data('ratingtypeid') == ratingTypeId) {
                    notAlredyExist = false;
                    p_notification(false, eb.getMessage('ERR_CUST_RATING_TYPES_ALREDY_EXIXT'));
                    return false;
                }
            }
        });
        if (notAlredyExist) {
            var ratingTypeName = $("#customerProfileRatingTypes option[value=" + ratingTypeId + "]").text();
            var $cloneRow = $($('.sampleCustProfileRatingTypeRow').clone()).removeClass('hidden sampleCustProfileRatingTypeRow').addClass('cProfilerRow');
            $cloneRow.find('.cProfilertName').text(ratingTypeName);
            $cloneRow.data('ratingTypeMethod', ratingTypeMethod);

            for (i = 1; i < ratingTypeValue + 1; i++) {
                $cloneRow.find('.custProfileRatingValue').append($("<option/>", {
                    value: i,
                    text: i
                }));
            }
            var methodCss = "fontawesome-stars";
            var methodFlag = false;
            if (ratingTypeMethod == 2) {
                methodCss = "bars-square";
                methodFlag = true;
            }

            $cloneRow.find('.custProfileRatingValue').barrating({
                theme: methodCss,
                showValues: methodFlag,
                showSelectedRating: false
            });

            $cloneRow.data('ratingtypeid', ratingTypeId);
            $("#cProfileRTtable tbody").append($cloneRow);
        }
        $('#customerProfileRatingTypes').val('').selectpicker('refresh');
    });

    $('#cProfileRTtable tbody').on('click', '.deleteCProfileRT', function() {
        var $row = $(this).parents('tr');
        $row.remove();
    });

    //for save sub set values
    $('#enumeratorModal').on( 'click', '.enum-save-btn', function(){
        var subSet = {}; 
        var field = $(this).data('field');
        $('#enumeratorModal .enum-tr').each(function( index, element){
            var $tr = $(this);            
            var key = $tr.data('id');
            var value = $tr.find('.enumSelector').val();
            
            if(value == 0){//for new sub set
                value = 'new_category-'+key;
            }
            subSet[key] = value;
        });
        
        if (field == 'customerCategory') {
            subCategory = subSet;
        }
        $('#enumeratorModal').modal('hide');
    });

    //for toggle enum modal 
    $('.enum-modal-btn').on( 'click', function (){
        var $tr = $(this).closest('tr');
        var columnId = $tr.data('column');
        var field = 'customerCategory';
        var subSet;
        var subCategory = {};
        subSet = subCategory;
        generateEnumeratorModal( columnId, field, fileData, header, true, true, subSet, function(){});
        $('#enumeratorModal').modal('show');
    });

});
function disableoptions(selectedoption) {
    for (var i = 0; i < columns; i++) {
        //        $("#" + i + " option:contains(" + selectedoption + ")").attr('disabled', 'disabled');
        $("#" + i + ' option[value="' + selectedoption + '"]').attr('disabled', 'disabled');
    }
}
function enableoptions(selectedenableoption) {
    for (var j = 0; j < columns; j++) {
        $("#" + j + ' option[value="' + selectedenableoption + '"]').removeAttr('disabled');
    }
}

function setCustomerProfiles(profileData) {

    if (typeof profileData === 'undefined' || profileData === '' || profileData.length === 0)
    {
        $('.customerProfileDisplay').not('#customerProfileDisplay').remove();
        var clonedCustomerProfileDisplay = $clonedCustomerProfileDisplay.clone();
        clonedCustomerProfileDisplay.removeClass('hidden');
        customerProfileDataArray['unsavedProfile' + profileID] = {customerProfileName: 'Default'};
        $("input[name='customerProfileName']", clonedCustomerProfileDisplay).val(customerProfileDataArray['unsavedProfile' + profileID]['customerProfileName']);
        clonedCustomerProfileDisplay.attr('data-id', 'unsavedProfile' + profileID);
        clonedCustomerProfileDisplay.removeAttr('id');
        clonedCustomerProfileDisplay.insertBefore('.newProfile');
        setPrimaryProfileDropDown(customerProfileDataArray);
    } else {
        $('.customerProfileDisplay').not('#customerProfileDisplay').remove();
        $.each(customerProfileRatingTypes, function(index, value) {
            var cprt = {};
            $.each(value, function(index1, value1) {
                cprt[value1.ratingTypesId] = {ratingTypesRatingValue: value1.customerRatingsRatingValue, ratingTypesName: value1.ratingTypesCode + ' - ' + value1.ratingTypesName, ratingTypeMethod: value1.ratingTypesMethod};
            });
            profileData[index].customerProfileRatingTypes = cprt;
        });
        customerProfileDataArray = profileData;
        setPrimaryProfileDropDown(customerProfileDataArray);
        for (var key in profileData) {
            var clonedCustomerProfileDisplay = $clonedCustomerProfileDisplay.clone();
            $("input[name='customerProfileName']", clonedCustomerProfileDisplay).val(profileData[key]['customerProfileName']);
            clonedCustomerProfileDisplay.removeClass('hidden');
            clonedCustomerProfileDisplay.attr('data-id', key);
            clonedCustomerProfileDisplay.removeAttr('id');
            clonedCustomerProfileDisplay.insertBefore('.newProfile');
            if (profileData[key]['isPrimary'] === '1') {
                $('#primaryCustomerProfile').val(key);
            }
        }
    }
}

function binddata(data) {
    customerID = data.customerID;
    $('#customerTitle').val(data.customerTitle);
    $('#customerName').val(data.customerName);
    $('#customerCode').val(data.customerCode);
    $('#customerShortName').val(data.customerShortName);
    if (data.customerCurrencyID) {
        $('#customerCurrency').val(data.customerCurrencyID);
    }
    $('#customerTelephoneNumber').val(data.customerTelephoneNumber);
    $('#customerAdditionalTelephoneNumber').val(data.customerAdditionalTelephoneNumber);
    $('#customerEmail').val(data.customerEmail);
    $('#customerAddress').val(data.customerAddress);
    $('#customerCategory').val(data.customerCategory);
    $('#customerVatNumber').val(data.customerVatNumber);
    $('#customerSVatNumber').val(data.customerSVatNumber);
    if (data.customerPaymentTerm) {
        $('#customerPaymentTerm').val(data.customerPaymentTerm);
    }
    $('#customerCreditLimit').val(data.customerCreditLimit);
    $('#customerCurrentBalance').val(data.customerCurrentBalance);
    $('#customerDiscount').val(data.customerDiscount);
    $('#customerOther').val(data.customerOther);
    $('#customerCurrentCredit').val(data.customerCurrentCredit);
    $('#customerEvent').val(data.customerEvent);
}
function cleardata() {
    $('#customerTitle').val("");
    $('#customerName').val("");
    $('#customerShortName').val("");
    $('#customerCurrency').val(0);
    $('#customerTelephoneNumber').val('');
    $('#customerIdentityNumber').val('');
    $('#customerCategory').val('');
    $('#customerVatNumber').val('');
    $('#customerSVatNumber').val('');
    $('#customerPaymentTerm').val(1);
    $('#customerCreditLimit').val('');
    $('#customerCurrentBalance').val('');
    $('#customerCurrentCredit').val('');
    $('#customerDiscount').val('');
    $('#customerOther').val('');
    $('#customerEvent').val('');

}
function validteinput(inputs) {
    var cname = inputs.customerName;
    var customerCode = inputs.customerCode;
    var cshrtname = inputs.customerShortName;
    var cIDNo = inputs.customerIdentityNumber;
    var cvatnumber = inputs.customerVatNumber;
    var ccreditlimit = inputs.customerCreditLimit;
    var cdiscount = inputs.customerDiscount;
    var ctelephoneNumber = inputs.customerTelephoneNumber;
    var ccurrentcredit = inputs.customerCredit;
    var customerCurrency = inputs.customerCurrency;
    var customerCategory = inputs.customerCategory;
    var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    var shortNameregex = /^[a-zA-Z]+[a-zA-Z0-9]+/;
    var cusaddsnameVal = /^[a-z0-9A-Z]+$/i;
    var nameregex = /\d/g;

    isPhone = false;
    if (ctelephoneNumber.length != 0) {
        if (ctelephoneNumber.length < 9 || ctelephoneNumber.length > 15) {
            isPhone = true;
        }
    }
    if (customerCode == null || customerCode == "") {
        p_notification(false, eb.getMessage('ERR_CUST_EMPTY_CODE'));
    } else if (cname == null || cname == "") {
        p_notification(false, eb.getMessage('ERR_CUST_NAME'));
    } else if (customerCurrency == null || customerCurrency == "") {
        p_notification(false, eb.getMessage('ERR_CUST_CURRENCY'));
    } else if (isNaN(ccreditlimit)) {
        p_notification(false, eb.getMessage('ERR_CUST_CRDTLIMIT_NUMR'));
    } else if (ccreditlimit < 0) {
        p_notification(false, eb.getMessage('ERR_CUST_CRDTLIMIT_NEG'));
    } else if (ccurrentcredit < 0) {
        p_notification(false, eb.getMessage('ERR_CUST_CRDTBAL_NEG'));
    } else if (isNaN(cdiscount) || parseInt(cdiscount) > 100 || cdiscount < 0) {
        p_notification(false, eb.getMessage('ERR_CUST_DISCOUNT_VALIDITY'));
    } else if (ctelephoneNumber === null || ctelephoneNumber === "") {
        p_notification(false, eb.getMessage('ERR_CUST_EMPTY_PRIMARY_TP'));
    } else if (isNaN(ctelephoneNumber) || isPhone) {
        p_notification(false, eb.getMessage('ERR_CUST_VALID_PRIMARY_TP'));
    } else if (cIDNo !== '' && !cusaddsnameVal.test(cIDNo)) {
        p_notification(false, eb.getMessage('ERR_CUST_VALID_IDENTY'));
    } else if (isNaN(customerCategory)) {
        p_notification(false, eb.getMessage('ERR_CUST_VALID_CATEG'));
    } else {
        return true;
    }
}

var validateCustomerProfileForm = function(tempDataArray) {
    pN = tempDataArray.customerProfileName;
    pM1 = tempDataArray.customerProfileMobileTP1;
    pM2 = tempDataArray.customerProfileMobileTP2;
    pL1 = tempDataArray.customerProfileLandTP1;
    pL2 = tempDataArray.customerProfileLandTP2;
    pF = tempDataArray.customerProfileFaxNo;
    pE = tempDataArray.customerProfileEmail;
    pLPC = tempDataArray.customerProfileLocationPostalCode;
    pCN = tempDataArray.customerProfileContactPersonName;
    pCNo = tempDataArray.customerProfileContactPersonNumber;

    var emailRegex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    var addressRegex = /^[a-zA-Z\s\d\/]+$/;
    var nameregex = /^[\w\-\s]+$/;
    if (pN === null || pN === '' || !nameregex.test(pN)) {
        p_notification(false, eb.getMessage('ERR_CUST_PROFILE_VALID_NAME'));
        $("input[name='customerProfileName']", '#customerProfileModal').focus();
        return false;
    } else if (!isPhoneValid(pM1)) {
        p_notification(false, eb.getMessage('ERR_CUST_PROFILE_VALID_MOB_NUM'));
        $("input[name='customerProfileMobileTP1']", '#customerProfileModal').focus();
        return false;
    } else if (!isPhoneValid(pM2)) {
        p_notification(false, eb.getMessage('ERR_CUST_PROFILE_VALID_MOB_NUM'));
        $("input[name='customerProfileMobileTP2']", '#customerProfileModal').focus();
        return false;
    } else if (!isPhoneValid(pL1)) {
        p_notification(false, eb.getMessage('ERR_CUST_PROFILE_VALID_FIX_LINE_NUM'));
        $("input[name='customerProfileLandTP1']", '#customerProfileModal').focus();
        return false;
    } else if (!isPhoneValid(pL2)) {
        p_notification(false, eb.getMessage('ERR_CUST_PROFILE_VALID_FIX_LINE_NUM'));
        $("input[name='customerProfileLandTP2']", '#customerProfileModal').focus();
        return false;
    } else if (!isPhoneValid(pF)) {
        p_notification(false, eb.getMessage('ERR_CUST_PROFILE_VALID_FAX'));
        $("input[name='customerProfileFaxNo']", '#customerProfileModal').focus();
        return false;
    } else if (!isPhoneValid(pCNo)) {
        p_notification(false, eb.getMessage('ERR_CUST_PROFILE_VALID_CONT_TP'));
        $("input[name='customerProfileContactPersonNumber']", '#customerProfileModal').focus();
        return false;
    } else if (pE !== '' && !emailRegex.test(pE)) {
        p_notification(false, eb.getMessage('ERR_CUST_PROFILE_VALID_EMAIL'));
        $("input[name='customerProfileEmail']", '#customerProfileModal').focus();
        return false;
    } else if (pLPC !== '' && !nameregex.test(pLPC)) {
        p_notification(false, eb.getMessage('ERR_CUST_PROFILE_VALID_POSTAL'));
        $("input[name='customerProfileLocationPostalCode']", '#customerProfileModal').focus();
        return false;
    } else {
        return true;
    }
}

var isPhoneValid = function(phoneNumber) {
    isValid = true;
    if (phoneNumber.length != 0) {
        if (isNaN(phoneNumber) || phoneNumber.length < 9 || phoneNumber.length > 15) {
            isValid = false;
        }
    }
    return isValid;
}

var clearCustomerProfileForm = function() {
    $("input[name='customerProfileName']", '#customerProfileModal').val('');
    $("input[name='customerProfileMobileTP1']", '#customerProfileModal').val('');
    $("input[name='customerProfileMobileTP2']", '#customerProfileModal').val('');
    $("input[name='customerProfileLandTP1']", '#customerProfileModal').val('');
    $("input[name='customerProfileLandTP2']", '#customerProfileModal').val('');
    $("input[name='customerProfileFaxNo']", '#customerProfileModal').val('');
    $("input[name='customerProfileEmail']", '#customerProfileModal').val('');
    $("input[name='customerProfileContactPersonName']", '#customerProfileModal').val('');
    $("input[name='customerProfileContactPersonNumber']", '#customerProfileModal').val('');
    $("input[name='customerProfileLocationNo']", '#customerProfileModal').val('');
    $("input[name='customerProfileLocationRoadName1']", '#customerProfileModal').val('');
    $("input[name='customerProfileLocationRoadName2']", '#customerProfileModal').val('');
    $("input[name='customerProfileLocationRoadName3']", '#customerProfileModal').val('');
    $("input[name='customerProfileLocationSubTown']", '#customerProfileModal').val('');
    $("input[name='customerProfileLocationTown']", '#customerProfileModal').val('');
    $("input[name='customerProfileLocationPostalCode']", '#customerProfileModal').val('');
    $("select[name='customerProfileLocationCountry']", '#customerProfileModal').val('');
    $("#customerProfileRatingTypes").val('').selectpicker('refresh');
    $('.cProfilerRow').remove();
    $('.customerProfileRatingDiv').addClass('hidden');
}

var setDataToCustomerProfileForm = function(profileData) {
    $("input[name='customerProfileName']", '#customerProfileModal').val(profileData.customerProfileName);
    $("input[name='customerProfileMobileTP1']", '#customerProfileModal').val(profileData.customerProfileMobileTP1);
    $("input[name='customerProfileMobileTP2']", '#customerProfileModal').val(profileData.customerProfileMobileTP2);
    $("input[name='customerProfileLandTP1']", '#customerProfileModal').val(profileData.customerProfileLandTP1);
    $("input[name='customerProfileLandTP2']", '#customerProfileModal').val(profileData.customerProfileLandTP2);
    $("input[name='customerProfileFaxNo']", '#customerProfileModal').val(profileData.customerProfileFaxNo);
    $("input[name='customerProfileEmail']", '#customerProfileModal').val(profileData.customerProfileEmail);
    $("input[name='customerProfileContactPersonName']", '#customerProfileModal').val(profileData.customerProfileContactPersonName);
    $("input[name='customerProfileContactPersonNumber']", '#customerProfileModal').val(profileData.customerProfileContactPersonNumber);
    $("input[name='customerProfileLocationNo']", '#customerProfileModal').val(profileData.customerProfileLocationNo);
    $("input[name='customerProfileLocationRoadName1']", '#customerProfileModal').val(profileData.customerProfileLocationRoadName1);
    $("input[name='customerProfileLocationRoadName2']", '#customerProfileModal').val(profileData.customerProfileLocationRoadName2);
    $("input[name='customerProfileLocationRoadName3']", '#customerProfileModal').val(profileData.customerProfileLocationRoadName3);
    $("input[name='customerProfileLocationSubTown']", '#customerProfileModal').val(profileData.customerProfileLocationSubTown);
    $("input[name='customerProfileLocationTown']", '#customerProfileModal').val(profileData.customerProfileLocationTown);
    $("input[name='customerProfileLocationPostalCode']", '#customerProfileModal').val(profileData.customerProfileLocationPostalCode);
    $("select[name='customerProfileLocationCountry']", '#customerProfileModal').val(profileData.customerProfileLocationCountry);
    $("select[name='customerProfileLocationCountry']", '#customerProfileModal').selectpicker('render');
    $('.cProfilerRow').remove();
    $('.customerProfileRatingDiv').addClass('hidden');

    if (!$.isEmptyObject(profileData.customerProfileRatingTypes)) {
        $('.customerProfileRatingDiv').removeClass('hidden');
        $.each(profileData.customerProfileRatingTypes, function(index, value) {
            var ratingTypeId = index;
            var ratingTypeName = value.ratingTypesName;
            var ratingTypesRatingValue = value.ratingTypesRatingValue;
            var $cloneRow = $($('.sampleCustProfileRatingTypeRow').clone()).removeClass('hidden sampleCustProfileRatingTypeRow').addClass('cProfilerRow');
            $cloneRow.find('.cProfilertName').text(ratingTypeName);
            $cloneRow.find('.custProfileRatingValue').val(ratingTypesRatingValue);
            $cloneRow.data('ratingtypeid', ratingTypeId);

            var ratingTypeValue = $("#customerProfileRatingTypes option[value='" + ratingTypeId + "']").data('rtvalue');
            var ratingTypeMethod = $("#customerProfileRatingTypes option[value='" + ratingTypeId + "']").data('rtmethod');
            for (i = 1; i < ratingTypeValue + 1; i++) {
                $cloneRow.find('.custProfileRatingValue').append($("<option/>", {
                    value: i,
                    text: i
                }));
            }
            var methodCss = "fontawesome-stars";
            var methodFlag = false;
            if (ratingTypeMethod == 2) {
                methodCss = "bars-square";
                methodFlag = true;
            }
            $cloneRow.find('.custProfileRatingValue').val(ratingTypesRatingValue);
            $cloneRow.find('.custProfileRatingValue').barrating({
                theme: methodCss,
                showValues: methodFlag,
                showSelectedRating: false,
            });

            $("#cProfileRTtable tbody").append($cloneRow);
        });
    }
}

var setPrimaryProfileDropDown = function(customerProfileDataArray) {
    $('#primaryCustomerProfile').empty();
    for (var key in customerProfileDataArray) {
        $('#primaryCustomerProfile').append($("<option value='" + key + "'>" + customerProfileDataArray[key]['customerProfileName'] + "</option>"));
    }
}

function generateEnumeratorModal( columnId, field, fileData, header, isUpdate, enumModalStatus, obj, callback){
    $.ajax({
        type: 'POST',
        url: BASE_URL + '/customer/enumerate-modal',
        data: {columnId: columnId, field: field, fileData: fileData, header: header},
        success: function(data) {
            if (data.status == true) {
                if(parseInt(data.data) === 0){
                    p_notification(false, eb.getMessage('ERR_UPLOADED_FILE_EMPTY'));
                    return;
                }
                $('#enumeratorModalBody').html(data.html);
                if(isUpdate){
                    $.each(obj, function( key, value){
                        if(field === 'customerCategory' && $.inArray(value, categoryIdArr) === -1){
                            value = 0;
                        }
                        $('#enumeratorModal tr[data-id="'+key+'"]').find('.enumSelector').val(value);
                    });                   
                }
                $('#enumeratorModal').find('.enum-save-btn').data('field',field);
                if(enumModalStatus){
                    $('#enumeratorModal').modal('show');
                }                
                callback(true);                
            } else {
                p_notification(false, data.msg);
                callback(false);
            }
        }
    });
}

