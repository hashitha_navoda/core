/*
 * @author Prathap Weerasinghe <prathap@thinkcube.com>
 */

var tmptotal, state = "new";
var tax_rates = new Array();
var products = new Array();
var taxdetails = new Array();
var allProductTax = new Array();
var currentProducts = new Array();
var selectedItemCode;
var customCurrencyRate = 0;
var customerProfileID;
var customCurrencySymbol = companyCurrencySymbol;
var priceListItems = [];
var uploadedAttachments = {};
var deletedAttachmentIds = [];
var deletedAttachmentIdArray = [];
var $productTable = $("#item_tbl");

function product(pCode, pName, quantity, uom, uPrice, discount, dType, tax, tCost, taxDetails, productType, item_discription, productID) {
    this.item_code = pCode;
    this.item = pName;
    this.quantity = quantity;
    this.discount = discount;
    this.discountType = dType;
    this.unit_price = uPrice;
    this.uom = uom;
    this.total_cost = tCost;
    this.taxes = tax;
    this.taxDetails = taxDetails;
    this.productType = productType;
    this.item_discription = item_discription;
    this.itemID = productID;
}

$(document).ready(function() {
    var all_tx_val = new Array();
    var totalTaxList = {};
    var totaldiscount = 0, decimalPoints = 0, itemDecimalPoints = 0;
    var priceListId = '';
    customerProfileID = $('.cus_prof_div').attr('data-cusproid');
    $('.deli_row, #deli_form_group, #tax_td').hide();
    $("#customer_more").hide();
    $("#currency").attr("disabled", "disabled");
    $("#moredetails").click(function() {
        $("#customer_more").slideDown();
        $('#moredetails').hide();
    });

    locationOut = $('#idOfLocation').val();

    var getAddRow = function(proIncID) {
        return $('tr.add-row:not(.sample)', $productTable);
    };


    function setItemCost() {
        var id = $('#item_code').val();
        var productType = $('#item_code').data('PT');
        var price = parseFloat(accounting.unformat($('#price').val()));
        var qty = parseFloat($('#qty').val());
        if (productType == 2 && (qty == '' || qty == 0 || isNaN(qty))) {
            qty = 1;
        }
        var disc = parseFloat(accounting.unformat($('#discount').val()));
        var tax = new Array();
        var taxA = 0;
        var tot = 0;
        taxdetails = new Array();
        $('#addTaxUl input:checked').each(function() {
            if (!$(this).is(':disabled')) {
                tax.push($(this).attr("value"));
            }
        });
        if (jQuery.isEmptyObject(tax)) {
            $('#taxApplied').addClass('glyphicon glyphicon-unchecked');
        } else {
            $('#taxApplied').removeClass('glyphicon glyphicon-unchecked');
            $('#taxApplied').addClass('glyphicon glyphicon-check');
        }


        tot = (price * qty) - calculateDiscount(id, price * qty, disc, qty);
        taxdetails = calculateItemCustomTax(tot, tax);
        var taxTotalAmount = taxdetails === null ? 0 : taxdetails.tTA;
        taxA = taxTotalAmount === undefined ? 0 : parseFloat(taxTotalAmount);

        $("#total").html(isNaN((tot + taxA)) ? "0.00" : accounting.formatMoney((tot + taxA).toFixed(2)));
    }

    $('#viewUploadedFiles').on('click', function(e) {
        $('#doc-attach-table tbody tr').remove();
        $('#doc-attach-table tfoot div').remove();
        $('#doc-attach-table tbody div').remove();

        if (!$.isEmptyObject(uploadedAttachments)) {
            $('#doc-attach-table thead tr').removeClass('hidden');
            $.each(uploadedAttachments, function(index, value) {
                if (!deletedAttachmentIds.includes(value['documentAttachemntMapID'])) {
                    tableBody = "<tr id="+value['documentAttachemntMapID']+" data-attachId=" + value['documentAttachemntMapID'] + "><td><a href="+value.docLink+">"+value.documentRealName+"</a></td><td class='text-center'><span class='glyphicon glyphicon-trash delete_attachment' style='cursor:pointer'></span></td></tr>";
                    $('#doc-attach-table tbody').append(tableBody);
                }
            });
        } else {
            $('#doc-attach-table thead tr').addClass('hidden');
            var noDataFooter = "<div class= ' col-lg-6 text-right'><label><b>No Data</b></label>";
            $('#doc-attach-table tbody').append(noDataFooter);
        }
        $('#attach_view_footer').removeClass('hidden');
        $('#viewAttachmentModal').modal('show');
    });

    $('#item_code').on('change', function() {
        if ($('#item_code').val() == 'add') {
            var currentRow = getAddRow();
            $('#createProduct #itmQty').addClass('hidden');
            $('#createProduct').modal('show');
            var defaultCategory = $('#categoryParentID').attr('data-id');
            $('#categoryParentID').val(defaultCategory);
            $('#categoryParentID').selectpicker('refresh');
            var defaultUomID = $('#defaultUomID').val();
            $("[name='uomID[]']").val(defaultUomID);
            productRow = currentRow;
        }
    });
    
    $('#createProduct').on('shown.bs.modal', function() {
        $(this).find("#productCode").focus();
    });


    $('#doc-attach-table').on('click', '.delete_attachment', function() {
        var attachementID = $(this).parents('tr').attr('data-attachid');
        bootbox.confirm('Are you sure you want to remove this attachemnet?', function(result) {
            if (result == true) {
                $('#' + attachementID).remove();
                deletedAttachmentIdArray.push(attachementID);
            }
        });
    });

    $('#viewAttachmentModal').on('click', '#saveAttachmentEdit', function(event) {
        event.preventDefault();

        $.each(deletedAttachmentIdArray, function(index, val) {
            deletedAttachmentIds.push(val);
        });

        $('#viewAttachmentModal').modal('hide');
    });
    $('#disc_presentage,#disc_value').on('change', function() {
        setItemViceDiscount();
    });

    $('#total_discount_rate').on('keyup', function() {
        if ($('#disc_value').is(":checked")) {
            if ((!$.isNumeric($(this).parents('tr').find('#total_discount_rate').val()) || $(this).parents('tr').find('#total_discount_rate').val() < 0)) {
                decimalPoints = 0;
                $(this).parents('tr').find('#total_discount_rate').val('');
            }
        } else {

            if ((!$.isNumeric($(this).parents('tr').find('#total_discount_rate').val()) || $(this).parents('tr').find('#total_discount_rate').val() < 0 || $(this).parents('tr').find('#total_discount_rate').val() > 100)) {
                decimalPoints = 0;
                $(this).parents('tr').find('#total_discount_rate').val('');
            }
        }

        setItemViceDiscount();
    });

    function setTotalCost() {
        var total = 0, disc = 0;
        for (var i in products) {
            var tax = 0;
            for (var tx in products[i].taxes) {
                tax += products[i].taxes[tx];
            }
            total += parseFloat(products[i].total_cost);
        }
        var delivercharge = parseFloat(accounting.unformat($('#deli_amount').val()));
        if (delivercharge == '') {
            $('#finaltotal').html(accounting.formatMoney((total).toFixed(2)));
            $('#subtotal').html(accounting.formatMoney((total).toFixed(2)));
        } else {
            finalt = ((parseFloat(total) + parseFloat(delivercharge)).toFixed(2));
            $('#finaltotal').html(accounting.formatMoney(finalt));
        }
        if ($('#total_discount_rate').val() != '' && $('input[name=discount_type]:checked').val() == 'Value') {

            var dicountVal = $('#total_discount_rate').val();

            if (parseFloat(dicountVal) > parseFloat(total)) {
                $('#total_discount_rate').val('');
                dicountVal = 0;
                decimalPoints = 0;
            }

            finalt = ((parseFloat(total) - parseFloat(dicountVal)).toFixed(2));
            $('#finaltotal').html(accounting.formatMoney(finalt));
            $('#subtotal').html(accounting.formatMoney(finalt));
        }
        setTotalTax();
    }

    function setItemViceDiscount () {
        
        $('.itemlist tr').each(function() {
            var thisRow = $(this);
            var productID = thisRow.attr('id');
            if (productID != '' && productID != 'preSetSample') {
                var invoiceDiscountValue = $('#total_discount_rate').val();

                var unitArr = thisRow.find('#unit').text().split(" ");
                var unitcost = parseFloat(unitArr[0].replace(/,/g, ''));
                var itemQuantity = parseFloat(thisRow.find('#quantity').text());
                var itemCost = unitcost * itemQuantity;
                var codeArr = productID.split("_");
                var itemId = thisRow.attr('data-itemId');

                var tax = new Array();
                var taxA = 0;
                var disc = 0;
                taxdetails = new Array();
                for (var i in products) {
                    if (i == codeArr[1]) {
                        var tax = products[i]['taxes'];
                        disc = products[i]['discount'];
                    }
                }

                //calculate product vise discount
                // itemCost -= (itemCost * (toFloat(disc) / toFloat(100)));


                var conversion = currentProducts[itemId].uom[products[i]['uom']].uC;

                itemQuantity = itemQuantity * conversion;
                // console.log(calculateDiscount(itemId, itemCost, disc, itemQuantity));

                itemCost = itemCost - calculateDiscount(itemId, itemCost, disc, itemQuantity);



                //calculate quotation vise discount
                if ($('input[name=discount_type]:checked').val() == 'presentage') {
                    itemCost -= (itemCost * (toFloat(invoiceDiscountValue) / toFloat(100)));
                } 


                taxdetails = calculateItemCustomTax(itemCost, tax);
                var taxTotalAmount = taxdetails === null ? 0 : taxdetails.tTA;
                taxA = taxTotalAmount === undefined ? 0 : parseFloat(taxTotalAmount);

                itemCost = itemCost + taxA;
                itemCost = accounting.formatMoney(itemCost);
                for (var i in products) {
                    if (i == codeArr[1]) {
                        var item_cost = parseFloat(itemCost.replace(/,/g, ''));
                        products[i]['total_cost'] = item_cost;
                    }
                }
                thisRow.find('#ttl').text(itemCost);
            }
        });
        
        setTotalCost();
    }

    $('#show_addi').on('click', function() {
        if (this.checked) {
            $('#additional_div').removeClass('hidden');
        }
        else {
            $('#additional_div').addClass('hidden');
        }
    });

    function setTotalTax() {
        if (tax_rates.length < 1) {
            eb.ajax({
                type: 'POST',
                url: '/taxAPI/getTaxRates',
                success: function(res) {
                    tax_rates = res;
                },
                async: false
            });
        }
        totalTaxList = {};
        for (var k in products) {
            if (products[k].taxDetails != null) {
                for (var l in products[k].taxDetails.tL) {
                    if (totalTaxList.hasOwnProperty(l)) {
                        var curTA = totalTaxList[l].tA;
                        var newTA = (toFloat(curTA) + toFloat(products[k].taxDetails.tL[l].tA));
                        totalTaxList[l].tA = newTA.toFixed(2);
                    } else {
                        totalTaxList[l] = {tN: products[k].taxDetails.tL[l].tN, tP: products[k].taxDetails.tL[l].tP, tA: products[k].taxDetails.tL[l].tA};
                    }
                }
            }
        }
        var totalTaxHtml = "";
        for (var t in totalTaxList) {
            totalTaxHtml += totalTaxList[t].tN + "(" + totalTaxList[t].tP + "%)&nbsp" + (accounting.formatMoney(totalTaxList[t].tA)) + "<br>";
        }
        $("#tax_td").html(totalTaxHtml);
    }

    var documentType = 'quotation';
    loadDropDownFromDatabase('/productAPI/search-location-products-for-dropdown', currentLocationID, "", '#item_code', '', '', documentType);
    $('#item_code').on('change', function() {
        $('#qty').parent().addClass('input-group');
        if ($(this).val() > 0 && selectedItemCode != $(this).val()) {
            selectedItemCode = $(this).val();
            itemChange($(this).val());
        }
    });

    /* added by Sandun Dissanayake
     * because boostrap select
     * */
    setDataInitialLoading();

    loadDropDownFromDatabase('/customerAPI/search-customers-for-dropdown', "", 0, '#cust_name');
    $('#cust_name').selectpicker('refresh');
    $('#cust_name').on('change', function() {
        if ($('#cust_name').val() > 0 && $('#cust_name').val() != customerID) {
            var key = $('#cust_name').val();
            customerID = key;
            customerName = $('#cust_name option:selected').text();
            customerProfileID = '';
            getCustomerProfilesDetails(key);
        }
    });


    $('#item_tbl').on('change', '.addNewUomR', function() {
        var tmpUomOb = currentProducts[$('#item_code').val()].uom;
        var tmpUomID = $(".uomLi input[type='radio']:checked").val();
        $('#uomAb').html(tmpUomOb[tmpUomID].uA);
    });

    $("#item_tbl").on('click', '#selectAll', function() {
        $(this).parents('#addTaxUl').find('input:checkbox.addNewTaxCheck').each(function() {
            if (!this.checked) {
                this.checked = true;
            }
        });
        $(this).parents('tr').find("#qty").trigger(jQuery.Event("focusout"));
    });

    $("#item_tbl").on('click', '#deselectAll', function() {
        $(this).parents('#addTaxUl').find('input:checkbox.addNewTaxCheck').each(function() {
            if (this.checked) {
                this.checked = false;
            }
        });
        $(this).parents('tr').find("#qty").trigger(jQuery.Event("focusout"));
    });

    $(document).on('click', 'ul.dropdown-menu, .dropdown-menu label', function(e) {
        e.stopPropagation();
    });

    $(document).on('mouseover', '#item_tbl #unit,#item_tbl #quantity,#item_tbl #disc,#item_tbl #name', function(e) {
        $(this).addClass('shadow');
    });

    $(document).on('mouseout', '#item_tbl #unit,#item_tbl #quantity,#item_tbl #disc,#item_tbl #name', function(e) {
        $(this).removeClass('shadow');
    });

    $(document).on('change', '.chkTaxes', function() {
        var trid = $(this).closest('tr').attr('id');
        var itemID = trid.split('td_')[1].trim();
        var checkedTaxes = Array();
        $("." + itemID + " input:checked").each(function() {
            checkedTaxes.push(this.id);
        });
        if (jQuery.isEmptyObject(checkedTaxes)) {
            $("#taxFilled_" + itemID).removeClass('glyphicon glyphicon-check');
            $("#taxFilled_" + itemID).addClass('glyphicon glyphicon-unchecked');
        } else {
            $("#taxFilled_" + itemID).removeClass('glyphicon glyphicon-unchecked');
            $("#taxFilled_" + itemID).addClass('glyphicon glyphicon-check');
        }
        setItemCostForTaxes(itemID, checkedTaxes);
    });

    $(document).on('click', ".delete", function() {
        delete products[this.id];
        $(this).closest("tr").remove();
        setTotalCost();
        //if all products removed enable custom currency field
        var itemCount = $(".itemlist tr:not(.hide)").length;
        if (itemCount == 0) {
            $('#customCurrencyId').attr('disabled', false);
        }
    });

    $('#add_item').on('click', function() {
        var code = currentProducts[$("#item_code").val()].pC;
        var productID = $("#item_code").val();
        var productType = $("#item_code").data('PT');
        var pName = currentProducts[$("#item_code").val()].pN;
        var quantity = $("#qty").val();
        var uomqty = $("#qty").siblings('.uomqty').val();
        var uomPrice = $("#price").siblings('.uomPrice').val();
        var uom = $("#qty").siblings('.uom-select').children('button').find('.selected').data('uomID');
        var price = accounting.unformat($("#price").val());
        var dsc_type = "none";
        var item_discription = $(this).parents('tr').find('.itemDescText').val();

        if (currentProducts[productID].dPR !== undefined && parseFloat(currentProducts[productID].dPR) > 0) {
            dsc_type = "pr";
        }
        else if (currentProducts[productID].dPR !== undefined && parseFloat(currentProducts[productID].dV) > 0) {
            dsc_type = "vl";
        }

        if (priceListItems[productID] != undefined && parseFloat(priceListItems[productID].itemPrice) > 0) {
            var itemDiscountType = priceListItems[productID].itemDiscountType;
            if (itemDiscountType == 1) {
                dsc_type = "vl";
            } else {
                dsc_type = "pr";
            }
        }

        var dsc_Pre = accounting.formatMoney($("#discount").val());
        var tax = new Array();
        $('#addTaxUl input:checked').each(function() {
            tax.push($(this).attr("value"));
        });

        addProduct(code, pName, quantity, price, uom, dsc_type, dsc_Pre, tax, null, uomqty, productType, productID, uomPrice, item_discription);
        setTotalCost();
        $('#priceListId').attr('disabled', true);
        $('#customCurrencyId').attr('disabled', true);
        selectedItemCode = '';

    });

    $('#show_tax').on('click', function() {
        if (this.checked) {
            $('#tax_td').fadeIn();
        }
        else {
            $('#tax_td').fadeOut();
        }
    });

    $('table#item_tbl>tbody').on('keypress', 'input#qty, input#price, input#discount,.uomqty', function(e) {
        if (e.which == 13) {
            $('#add_item').trigger('click');
            e.preventDefault();
        }

    });

    $(document).on('focusout', '#price, #qty, #discount,.uomqty,.uomPrice', function() {
        if ($('#item_code').val() != '') {
            if ($(this).val().indexOf('.') > 0) {
                decimalPoints = $(this).val().split('.')[1].length;
            }
            if (this.id == 'price' && (!$.isNumeric(accounting.unformat($('#price').val())) || accounting.unformat($('#price').val()) < 0 || decimalPoints > 2)) {
                decimalPoints = 0;
                $('#price').val('');
            }
            if ((this.id == 'qty') && (!$.isNumeric($('#qty').val()) || $('#qty').val() < 0 || decimalPoints > itemDecimalPoints)) {
                decimalPoints = 0;
                $('#qty').val('');
            }
            if ((this.id == 'discount') && (!$.isNumeric(accounting.unformat($('#discount').val())) || accounting.unformat($('#discount').val()) < 0 || (decimalPoints > 2))) {
                decimalPoints = 0;
                $('#discount').val('');
            }
            setItemCost();
        }
        else {
            $('#price, #qty, #discount').val('');
            p_notification(false, eb.getMessage('ERR_QUOTEDIT_SETITEM'));
        }
    });

    function itemChange(itemkey) {
        var data = new Array();
        itemDecimalPoints = 3;
        $('.tempLi').remove();
        $("#discount").attr('disabled', false);
        if (itemkey == '') {
            $('#qty').val('');
            $('#price').val('');
            $('#discount').val('');
            $('#total').html('');
            $('#uom').html('');
            return false;
        }
        eb.ajax({
            type: 'POST',
            url: BASE_URL + '/productAPI/get-location-product-details',
            data: {productID: itemkey, locationID: currentLocationID},
            success: function(respond) {
                currentProducts[respond.data.pID] = respond.data;
                data[0] = respond.data;
                if (data[0] == 'error') {
                    $('#qty').val('');
                    $('#price').val('');
                    $('#discount').val('');
                    $('#total').html('');
                    $('#uom').html('');
                    dbflag = false;
                } else {
                    dbflag = true;
                    if ($("#discount").siblings().hasClass('uomPrice')) {
                        $("#discount").siblings('.uomPrice').remove();
                        $("#discount").siblings('.uom-price-select').remove();
                        $("#discount").show();
                    }

                    if (priceListItems[itemkey] == undefined || (priceListItems[itemkey] != undefined && !(parseFloat(priceListItems[itemkey].itemPrice) > 0))) {
                        if (data[0].dEL === "0") {
                            $('#discount').val('');
                            $("#discount").attr('disabled', true);
                        }
                        else if (data[0].dEL === "1" && data[0].dV > 0)
                        {
                            $('#discount').val(parseFloat(data[0].dV).toFixed(2)).addUomPrice(data[0].uom);
                            $('#discType').html(customCurrencySymbol);
                        }
                        else if (data[0].dEL === "1") {
                            $('#discount').val(parseFloat(data[0].dPR).toFixed(2));
                            $('#discType').html('(%)');
                        }
                    }

                    if ($("#price").siblings().hasClass('uomPrice')) {
                        $("#price").siblings('.uomPrice').remove();
                        $("#price").siblings('.uom-price-select').remove();
                    }

                    $('#item_code').data('PT', data[0].pT);
                    if (priceListItems[itemkey] != undefined && parseFloat(priceListItems[itemkey].itemPrice) > 0) {
                        $('#price').val(priceListItems[itemkey].itemPrice).addUomPrice(data[0].uom);
                        //enable discount
                        $("#discount").attr('disabled', false);
                        var itemDiscountType = priceListItems[itemkey].itemDiscountType;
                        if (itemDiscountType == 1) {
                            $('#discount').val(parseFloat(priceListItems[itemkey].itemDiscount).toFixed(2)).addUomPrice(data[0].uom);
                            $('#discType').html(customCurrencySymbol);
                        } else if (itemDiscountType == 2) {
                            $('#discount').val(parseFloat(priceListItems[itemkey].itemDiscount).toFixed(2));
                            $('#discType').html('(%)');
                        }
                    } else {
                        $('#price').val((parseFloat(data[0].dSP)).toFixed(2)).addUomPrice(data[0].uom);
                    }
                    $('#uom').html(data[0].abbrevation);
                    if ($('#qty').val() === '') {
                        $('#qty').val('1');
                    }
                    if ($("#qty").siblings().hasClass('uomqty')) {
                        $("#qty").siblings('.uomqty').remove();
                        $("#qty").siblings('.uom-select').remove();
                    }

                    $('#qty').addUom(data[0].uom);
                    setTaxListForProduct(itemkey);
                    itemDecimalPoints = data[0].decimalPlace;

                    setItemCost();
                }
            }
        });
    }

    $('#quatation, #q_view').on('submit', function(e) {
        e.preventDefault();

        var selectedid = "#" + document.activeElement.id;
        var tableId = $(selectedid).closest('table').attr("id");
        //vlidate last not added item
        if ($('#item_code').val() != null) {
            var itemSameFlag = false;
                for (var i in products){
                        if( products[i].itemID == $('#item_code').val()){
                            itemSameFlag = true;
                        }
                }
            // if last item alredy in the selected item list then user can save. othrewise cannot save.
            if(!itemSameFlag){
                p_notification(false, eb.getMessage('ERR_QUOT_ADD_LITEM'));
                return false;
            }
        }


        if ($('#qot_no').val() == '') {
            p_notification(false, eb.getMessage('ERR_QUOT_VALID_QUOTA'));
        }
        else if ($('#cust_id').val() == '') {
            p_notification(false, eb.getMessage('ERR_QUOTEDIT_ENTER_CUSNAME'));
            return false;
        } else if ($('#issue_date').val() == '') {
            p_notification(false, eb.getMessage('ERR_QUOTEDIT_SELECT_ISSDATE'));
        } else if ($('#expire_date').val() == '') {
            p_notification(false, eb.getMessage('ERR_QUOTEDIT_ENTER_EXPDATE'));
        } else if (Object.keys(products).length == 0) {
            if (tableId != "item_tbl") {
                p_notification(false, eb.getMessage('ERR_QUOT_ADD_NOITEM'));
            }
            return false;
        } else if ($('#issue_date').val() > $('#expire_date').val()) {
            p_notification(false, eb.getMessage('ERR_QUOT_ENTER_DATE'));
        } else if (tableId == "item_tbl") {
            $('#add_item').trigger('click');
        } else {


            var items_ar = Array();
            totaldiscount = 0;
            var taxOfItems = new Array();
            for (var i in products) {
                var temptaxes = {};
                for (var tx in products[i].taxes) {
                    if (products[i].taxes[tx] != false) {
                        temptaxes[tx] = products[i].taxes[tx];
                    }
                }
                taxOfItems.push(JSON.stringify(temptaxes));
            }

            for (var i in products) {
                items_ar.push(products[i]);
                var qty = products[i].quantity;
                if (products[i].productType == 2 && qty == 0) {
                    qty = 1;
                }
                if (products[i].discountType == 'pr') {
                    tmpdisc = (products[i].discount * qty * products[i].unit_price / 100);
                } else {
                    tmpdisc = (products[i].discount * qty);
                }
                if (tmpdisc != 0) {
                    totaldiscount += toFloat(tmpdisc);
                }
            }

            var taxobj = {};
            for (var t in all_tx_val) {
                taxobj[t] = (all_tx_val[t]).toFixed(2);
            }
            var jsontax = JSON.stringify(taxobj);

            var existingAttachemnts = {};
            var deletedAttachments = {};
            $.each(uploadedAttachments, function(index, val) {
                if (!deletedAttachmentIds.includes(val['documentAttachemntMapID'])) {
                    existingAttachemnts[index] = val;
                } else {
                    deletedAttachments[index] = val;
                }
            });

            submit_data = {
                'cust_name': $('#cust_name option:selected').text(),
                'cust_id': $('#cust_id').val(),
                'payment_term': $('#payment_term').val(),
                'invoicecurrentBalance': accounting.unformat($('#invoicecurrentBalance').val()),
                'issue_date': $('#issue_date').val(),
                'expire_date': $('#expire_date').val(),
                'qot_no': $('#qot_no').val(),
                'salesPersonID': $('#salesPersonID').val(),
                'deli_amount': accounting.unformat($('#deli_amount').val()),
                'deli_charg': (parseFloat(accounting.unformat($('#deli_charg').val()))).toFixed(2),
                'deli_address': $('#deli_address').val(),
                'cmnt': $('#cmnt').val(),
                'taxAmount': jsontax,
                'sub_total': accounting.unformat($("#subtotal").html()),
                'total_amount': accounting.unformat($('#finaltotal').html()),
                'total_discount': totaldiscount,
                'items': items_ar,
                'itemTaxes': JSON.stringify(taxOfItems),
                'show_tax': ($('#show_tax').is(':checked')) ? 1 : 0,
                'customCurrencyRate': customCurrencyRate,
                'customCurrencyId': $('#customCurrencyId').val(),
                'priceListId': priceListId,
                'cust_pro_id': customerProfileID,
                'editedQuotationID' : $('#edit_quo_ID').val(),
                'additionalDetail1' : $('#additionalDetail1').val(),
                'additionalDetail2' : $('#additionalDetail2').val(),
                'quotationTotalDiscountType': $('input[name=discount_type]:checked').val(),
                'quotationDiscountRate': $('#total_discount_rate').val(),
            };

            if (this.id == "quatation") {
                if (state == "new") {
                    bootbox.confirm("Are you sure, you want to save it as new quotation?", function(result) {
                        if (result == true) {
                            submit_data.newQuotationNumber = true;
                            var url = "/quotation-api/createQuotation";
                            var request = eb.post(url, submit_data);
                            request.done(function(res) {
                                p_notification(res.status, res.msg);
                                if (res.status == true) {
                                    var fileInput = document.getElementById('editDocumentFiles');
                                    var form_data = false;
                                    if (window.FormData) {
                                        form_data = new FormData();
                                    }
                                    form_data.append("documentID", res.data);
                                    form_data.append("documentTypeID", 2);
                                    form_data.append("updateFlag", true);
                                    form_data.append("editedDocumentID", $('#edit_quo_ID').val());
                                    form_data.append("documentCode", $('#qot_no').val());
                                    form_data.append("deletedAttachmentIds", deletedAttachmentIds);
                                    
                                    if(fileInput.files.length > 0) {
                                        for (var i = 0; i < fileInput.files.length; i++) {
                                            form_data.append("files[]", fileInput.files[i]);
                                        }
                                    }

                                    eb.ajax({
                                        url: BASE_URL + '/store-files',
                                        type: 'POST',
                                        processData: false,
                                        contentType: false,
                                        data: form_data,
                                        success: function(res) {
                                        }
                                    });

                                    $('#qot_no').val(res.data);
                                    if ($('#lightbox_overlay').length < 1) {
                                        documentPreview(BASE_URL + '/quotation/quotationView/' + res.data, 'documentpreview', "/quotation-api/send-email", function($preview) {
                                            $preview.on('hidden.bs.modal', function() {
                                                if (!$("#preview:visible").length) {
                                                    window.location.reload();
                                                }
                                                window.location.assign(BASE_URL + "/quotation/list");
                                            });
                                        });
                                    }
                                    state = "saved";

                                }
                            });
                        } else {
                            submit_data.newQuotationNumber = false;
                            var url = "/quotation-api/createQuotation";
                            var request = eb.post(url, submit_data);
                            request.done(function(res) {
                                p_notification(res.status, res.msg);
                                if (res.status == true) {
                                    var fileInput = document.getElementById('editDocumentFiles');
                                    var form_data = false;
                                    if (window.FormData) {
                                        form_data = new FormData();
                                    }
                                    form_data.append("documentID", res.data);
                                    form_data.append("documentTypeID", 2);
                                    form_data.append("updateFlag", true);
                                    form_data.append("editedDocumentID", $('#edit_quo_ID').val());
                                    form_data.append("documentCode", $('#qot_no').val());
                                    form_data.append("deletedAttachmentIds", deletedAttachmentIds);
                                    
                                    if(fileInput.files.length > 0) {
                                        for (var i = 0; i < fileInput.files.length; i++) {
                                            form_data.append("files[]", fileInput.files[i]);
                                        }
                                    }

                                    eb.ajax({
                                        url: BASE_URL + '/store-files',
                                        type: 'POST',
                                        processData: false,
                                        contentType: false,
                                        data: form_data,
                                        success: function(res) {
                                        }
                                    });
                                    
                                    $('#qot_no').val(res.data);
                                    if ($('#lightbox_overlay').length < 1) {
                                        documentPreview(BASE_URL + '/quotation/quotationView/' + res.data, 'documentpreview', "/quotation-api/send-email", function($preview) {
                                            $preview.on('hidden.bs.modal', function() {
                                                if (!$("#preview:visible").length) {
                                                    window.location.reload();
                                                }
                                                window.location.assign(BASE_URL + "/quotation/list");
                                            });
                                        });
                                    }
                                    state = "saved";
                                    
                                }
                            });
                        }
                    });

                }
            }
            else if (this.id == "q_view") {
                var li_box = document.createElement("div");
                var li_box_cont = document.createElement("div");
                var print = document.createElement("div");
                li_box.id = "lightbox_overlay";
                li_box.class = "disabl_when_print";
                li_box_cont.id = "lightbox_container";
                li_box_cont.class = "disabl_when_print";
                print.id = "print_overlay";
                print.class = "disabl_when_print";
                var view_req = eb.post('/quotation/viewQuotation?' + $.param(submit_data), '', {});
                view_req.done(function() {
                    $(".main_div").addClass("disabl_when_print");
                    $(li_box_cont).append(view_req.responseText);
                    $(li_box).append(li_box_cont);
                    $("body").append(li_box);
                    $("body").append(print);
                    if (state == "saved") {
                        $("#prv_edit").html(edit_btn);
                    } else {
                        $("#prv_edit").html(save);
                    }

                    $("#prv_edit").on("click", function() {
                        if (state == "new") {
                            $("#placequotation").trigger(jQuery.Event("click"));
                            $("#prv_edit").html(edit_btn);
                        } else {
                            window.location.assign(BASE_URL + "/quotation/Edit/" + $('#qot_no').val());
                        }
                    });
                    $("#q_print").on("click", function() {
                        window.print();
                    });
                    $("#q_email").on("click", function() {
                        if (state == "new") {
                            p_notification(false, eb.getMessage('ERR_QUOT_SAVE_SEND'));
                        } else {
                            $("#email-modal").modal("show");
                            $("#send").on("click", function() {
                                var state = validateEmail($("#email_to").val(), $("#email-body").html());
                                if (state) {
                                    var param = {
                                        qout_id: $(this).attr("data-qot_id"),
                                        to_email: $("#email_to").val(),
                                        subject: $("#email_sub").val(),
                                        body: $("#email-body").html()
                                    };
                                    var mail = eb.post("/quotationAPI/sendQuotation", param);
                                    mail.done(function(rep) {
                                        if (rep.error) {
                                            p_notification(false, eb.getMessage('ERR_PAY_EMAIL_SENT'));
                                        } else {
                                            p_notification(true, eb.getMessage('SUCC_PAY_EMAIL_SENT'));
                                        }
                                    });
                                    $("#email-modal").modal("hide");
                                }
                            });
                        }
                    });
                    $("#cancel").on("click", function() {
                        if (state == "new") {
                            $(li_box_cont).remove();
                            $(li_box).remove();
                            $(print).remove();
                        } else {
                            $("#new_qtn").trigger(jQuery.Event("click"));
                        }
                    });
                    $("#new_qtn").on("click", function() {
                        window.location.reload();
                    });
                    $("#to_so").on("click", function() {
                        if (state == "saved") {
                            window.location.assign(BASE_URL + "/salesOrders/index/" + $('#qot_no').val());
                        } else {
                            p_notification(false, eb.getMessage('ERR_QUOT_SAVE_CONVERT'));
                        }

                    });
                    $("#to_inv").on("click", function() {
                        if (state == "saved") {
                            window.location.assign(BASE_URL + "/invoice/index/qo/" + $('#qot_no').val());
                        } else {
                            p_notification(false, eb.getMessage('ERR_QUOT_SAVE_CONVERT'));
                        }
                    });
                });
            }
        }
    });

    $('#cust_name').on('change', function() {
        var url2 = BASE_URL + '/customerAPI/getcustomer';
        if ($('#cust_name').val() == "") {
            $('#invoicecurrentBalance').val('');
            $('#quotationCurrentCredit').val('');
            $('#payment_term').val(0);
            cust_disc = 0;
        } else {
            var request2 = eb.post(url2, {customerID: customerID});
            request2.done(function(customerdata) {
                if (customerdata.status) {
                    var paymentTerm = customerdata.data[0].customerPaymentTerm == null ? 1 : customerdata.data[0].customerPaymentTerm;
                    $('#invoicecurrentBalance').val(accounting.formatMoney(customerdata.data[0].customerCurrentBalance));
                    $('#quotationCurrentCredit').val(accounting.formatMoney(customerdata.data[0].customerCurrentCredit));
                    $('#payment_term').val(paymentTerm);
                    $('#cust_id').val(customerdata.data[0].customerID);
                    cust_disc = customerdata.data[0].discount;
                } else {
                    $('#payment_term').val(0);
                    $('#cur_credit').val('');
                    cust_disc = 0;
                }
                setItemCost();
            });
        }
    });

    $("#reset").on('click', function() {
        window.location.reload()
    });

    $(document).on('click', '#moredetails', function() {
        $("#customer_more").fadeIn();
        $(this).hide();
    });

//    $('#placequotation').on('click', function() {
//        $('#quatation').trigger('submit');
//    });

    ///////DatePicker\\\\\\\

    var nowTemp = new Date();
    var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);

    var checkin = $('#issue_date').datepicker({
        onRender: function(date) {
            return date.valueOf() < now.valueOf() ? ((U_DATE_R_OVERRIDE)?'':'disabled') : '';
        }
    }).on('changeDate', function(ev) {
        if (ev.date.valueOf() > checkout.date.valueOf()) {
            var newDate = new Date(ev.date);
            newDate.setDate(newDate.getDate() + 1);
            checkout.setValue(newDate);
        }
        checkin.hide();
        $('#expire_date')[0].focus();
    }).data('datepicker');
    checkin.setValue(now);
    var checkout = $('#expire_date').datepicker({
        onRender: function(date) {
            return date.valueOf() < checkin.date.valueOf() ? ((U_DATE_R_OVERRIDE)?'':'disabled'): '';
        }
    }).on('changeDate', function(ev) {
        checkout.hide();
    }).data('datepicker');


/////EndOFDatePicker\\\\\\\\\

    $("#item_tbl").on('change', '.taxChecks', function() {
        setItemCost();
    });

    //responsive layout issue fix
    $(window).bind('resize ready', function() {
        if ($(window).width() <= 480) {
            $('.quotation_button_set').removeClass('btn-group');
            $('#placequotation, #q_view, #reset').addClass('col-xs-12').css('margin-bottom', '8px');
        } else {
            $('.quotation_button_set').removeClass('col-xs-12');
            $('#placequotation, #q_view, #reset').removeClass('col-xs-12').css('margin-bottom', '0px');
        }
    });

    $('.deli_row, #deli_form_group, #tax_td').hide();
    var Edit_Quo_ID = $('#edit_quo_ID').val();
    loadQuotationData(Edit_Quo_ID);

    function loadQuotationData(QuotationID) {
        var req = eb.post(BASE_URL + '/quotation-api/retriveQuotation', {'quataionID': QuotationID});
        req.done(function(res) {
            if (res == 'wrongquono') {
                p_notification(false, eb.getMessage('ERR_QUOTEDIT_ENTER_QUONUM'));//"Please Enter a valid Quotation No to Edit.");
            } else {
                if (res.inactiveItemFlag) {
                    p_notification(false, res.errorMsg + " cannot edit this Quotation.");
                    setTimeout(function(){ 
                        window.location.assign(BASE_URL + "/quotation/list")
                    }, 3000);
                    return false;
                }
                var NowTemp = new Date();
                var Now = new Date(NowTemp.getFullYear(), NowTemp.getMonth(), NowTemp.getDate(), 0, 0, 0, 0);
                var issuedDate = new Date(Date.parse(res.issue_date));
                var oldExpireDate = new Date(Date.parse(res.expire_date));
                var period = oldExpireDate.getTime() - issuedDate.getTime();
                var NewExpireDate = new Date(Now);
                NewExpireDate.setTime(Now.getTime() + period);
                var NowMonth = (Now.getMonth() + 1);
                var NowDate = Now.getDate();
                if (NowMonth < 10) {
                    NowMonth = '0' + NowMonth;
                } else {
                    NowMonth;
                }
                if (NowDate < 10) {
                    NowDate = '0' + NowDate;
                } else {
                    NowDate;
                }

                var NWMonth = (NewExpireDate.getMonth() + 1);
                var NWDate = NewExpireDate.getDate();
                if (NWMonth < 10) {
                    NWMonth = '0' + NWMonth;
                } else {
                    NWMonth;
                }
                if (NWDate < 10) {
                    NWDate = '0' + NWDate;
                } else {
                    NWDate;
                }

                if (!$.isEmptyObject(res.uploadedAttachments)) {
                    uploadedAttachments = res.uploadedAttachments;
                }

                var issueDate = eb.getDateForDocumentEdit('#issue_date', NowDate, NowMonth, Now.getFullYear());
                var expireDate = eb.getDateForDocumentEdit("#expire_date", NWDate, NWMonth, NewExpireDate.getFullYear());
                $('#issue_date').val(issueDate);
                $("#expire_date").val(expireDate);
                $('#cust_id').val(res.customer_id);
                $("#cust_name").empty()
                        .append($("<option></option>")
                                .attr("value", res.customer_id)
                                .text(res.customer_name))
                        .selectpicker('refresh');
                customerName = res.customer_name;
                customerID = res.customer_id;
                $('#payment_term').val(res.payment_term);
                $('#quo_no').val(res.quotation_no);
                $('#salesPersonID').val(res.salesPersonID);
                $('#salesPersonID').selectpicker('render');
                $('#so_no').val('');
                $('#cmnt').val(res.comment);
                if (res.quotationAdditionalDetail1 != null || res.quotationAdditionalDetail2 != null) {
                    $('#show_addi').trigger('click');
                    $('#additionalDetail1').val(res.quotationAdditionalDetail1);
                    $('#additionalDetail2').val(res.quotationAdditionalDetail2);
                }
                $('#invoicecurrentBalance').val(accounting.formatMoney(res.current_balance));
                $('#quotationCurrentCredit').val(accounting.formatMoney(res.current_credit));
                $("#form_rwo").html("");
                $('#add-cust-modal-btn').hide();
                if (res.priceListId != 0) {
                    $('#priceListId').val(res.priceListId).attr('disabled', true).selectpicker('refresh');
                } else {
                    $('#priceListId').attr('disabled', false);
                }
                $('#priceListId').trigger('change');
                if (res.showTax == "1") {
                    $('#show_tax').trigger('click');
                }
                $('#customCurrencyId').val(res.customCurrencyId).trigger('change');
                $('#customCurrencyId').attr('disabled', true);
                customCurrencyRate = (res.quotationCustomCurrencyRate != 0) ? parseFloat(res.quotationCustomCurrencyRate) : 1;
                items = new Array();
                currentProducts = res.locationProducts;
                customCurrencySymbol = res.customCurrencySymbol;
                for (var i in res.product) {

                    var productID = res.product[i].productID;
                    var code = res.product[i].locationProductID;
                    var productType = res.product[i].productTypeID
                    var pName = res.product[i].productName;
                    var productDescription = res.product[i].quotationProductDescription;
                    var quantity = res.product[i].quotationProductQuantity;
                    var qty = quantity;
                    var price = parseFloat(res.product[i].quotationProductUnitPrice) / customCurrencyRate;
                    if (res.product[i].quotationProductDiscountType == "vl") {
                        var discount = parseFloat(res.product[i].quotationProductDiscount / customCurrencyRate);
                    } else {
                        var discount = parseFloat(res.product[i].quotationProductDiscount);
                    }
                    var uom = res.product[i].uom;
                    var item_details = _.where(currentProducts, {lPID: code});
                    var productCode = item_details[0].pC;
                    var dsc_type = "none";
                    var calDiscount = 0;

                    // No need of saved tax. New tax values will use for the editign quotation
                    //taxesOfProduct = JSON.parse(res.product[i].tax);

                    if (productType == 2 && quantity == 0) {
                        qty = 1;
                    }

                    if (res.product[i].quotationProductDiscountType == 'pr') {
                        dsc_type = "pr";
                        calDiscount = price * qty * discount / 100;
                    }
                    else if (res.product[i].quotationProductDiscountType == 'vl') {
                        dsc_type = "vl";
                        calDiscount = (discount * qty);
                    }

                    var qpid = res.product[i].quotationProductID;

                    var tax = new Array();
                    if (!jQuery.isEmptyObject(res.productTaxes) && res.productTaxes[qpid]) {
                        for (var k in res.productTaxes[qpid]) {
                            tax.push(k);
                        }
                        $('#taxApplied').removeClass('glyphicon glyphicon-unchecked');
                        $('#taxApplied').addClass('glyphicon glyphicon-check');
                        $('.tempLi').remove();
                        for (var k in res.productTaxes[qpid]) {
                            var clonedLi = $($('#sampleLi').clone()).removeClass('hidden').attr('id', productCode + '_' + k).addClass("tempLi");
                            clonedLi.children(".taxChecks").attr('value', k).prop('checked', true).addClass('addNewTaxCheck');
                            clonedLi.children(".taxName").html('&nbsp&nbsp' + res.productTaxes[qpid][k].taxName + '&nbsp&nbsp' + res.productTaxes[qpid][k].quotTaxPrecentage + '%').attr('for', productCode + '_' + i);
                            clonedLi.insertBefore('#sampleLi');
                        }
                    }
                    var conversion = 1;
                    conversion = currentProducts[productID].uom[uom].uC;
                    var uomQty = (quantity / conversion).toFixed(currentProducts[productID].uom[uom].uDP);
                    var uomPrice = price * conversion;
                    tot = (price * qty) - calDiscount;
                    taxdetails = calculateItemCustomTax(tot, tax);
                    if (taxdetails != null) {
                        _.each(taxdetails.tL, function(x) {
                            if (allProductTax[x.tN] === undefined) {
                                allProductTax[x.tN] = {tp: x.tP, tA: x.tA};
                            } else {
                                allProductTax[x.tN].tA += x.tA;
                            }
                        });
                    }
                    var taxTotalAmount = taxdetails === null ? 0 : taxdetails.tTA;

                    taxA = taxTotalAmount === undefined ? 0 : parseFloat(taxTotalAmount);
                    total = tot + taxA;
                    addProduct(item_details[0].pC, pName, quantity, price, uom, dsc_type, discount, tax, total, uomQty, productType, productID, uomPrice, productDescription);
                    setTotalCost();

                }
                if (res.quotationDiscountRate > 0) {
                    $("#total_discount_rate").val(res.quotationDiscountRate);
                }

                if (res.quotationTotalDiscountType === "presentage" || res.quotationTotalDiscountType == null) {
                    $("#disc_presentage").prop("checked", true);
                    setItemViceDiscount();
                } else if (res.quotationTotalDiscountType == 'Value') {
                    $("#disc_value").prop("checked", true);
                    setItemViceDiscount();
                }
            }
        });
    }

    $('#customCurrencyId').on('change', function() {
        eb.ajax({
            type: 'POST',
            url: BASE_URL + '/currencySymbol/getCustomCurrencyByCustomCurrencyId',
            data: {customCurrencyId: $(this).val()},
            success: function(respond) {
                if (respond.status == true) {
                    $('.cCurrency').text(respond.data.currencySymbol);
                    customCurrencySymbol = respond.data.currencySymbol;
                }
            }
        });
    });

    $('#priceListId').on('change', function() {
        priceListId = $(this).val();
        eb.ajax({
            type: 'POST',
            url: BASE_URL + '/currencySymbol/getPriceListWithDiscount',
            data: {priceListId: priceListId},
            success: function(respond) {
                if (respond.status == true) {
                    priceListItems = respond.data;
                } else {
                    priceListItems = [];
                }
            },
            async: false
        });
    });

    //Add item description
    $('#item_tbl').on('click', '.addItemDescription', function() {
        if ($('i', $(this)).hasClass('fa-comment-o')) {
            $('i', $(this)).removeClass('fa-comment-o').addClass('fa-comment');
            $(this).parents('tr').addClass('comment');
            $(this).parent().parent().siblings('.itemDescText').removeClass('hidden').focus();
        } else {
            $('i', $(this)).removeClass('fa-comment').addClass('fa-comment-o');
            $(this).parents('tr').removeClass('comment');
            $(this).parent().parent().siblings('.itemDescText').addClass('hidden');
        }
    });


	//show item description
    $('#item_tbl').on('click', '.showItemDescription', function() {
        if ($('i', $(this)).hasClass('fa-comment-o')) {
            $('i', $(this)).removeClass('fa-comment-o').addClass('fa-comment');
            $(this).parents('tr').addClass('comment');
            $(this).parent().parent().siblings('.showItemDescText').removeClass('hidden').focus();
        } else {
            $('i', $(this)).removeClass('fa-comment').addClass('fa-comment-o');
            $(this).parents('tr').removeClass('comment');
            $(this).parent().parent().siblings('.showItemDescText').addClass('hidden');
        }
    });


    $('#item_tbl').on('blur', '.itemDescText', function(e) {
        e.preventDefault();
        $(this).addClass('hidden');
        $(this).parent().find('.addItemDescription').find('i').removeClass('fa-comment').addClass('fa-comment-o');
    });

     $('#item_tbl').on('blur', '.showItemDescText', function(e) {
        e.preventDefault();
        $(this).addClass('hidden');
        $(this).parent().find('.showItemDescription').find('i').removeClass('fa-comment').addClass('fa-comment-o');
    });

    $('#payment_term').on('change', function(e) {
        e.preventDefault();
        var days = 0;
        var i = parseInt($(this).val());
        switch (i) {
            case 2:
                days = 7;
                break;
            case 3:
                days = 14;
                break;
            case 4:
                days = 30;
                break;
            case 5:
                days = 60;
                break;
            case 6:
                days = 90;
                break;
            case 7:
                days = 21;
                break;
            case 8:
                days = 28;
                break;
            case 9:
                days = 45;
                break;
            default:
                days = 0;
                break;
        }
        setDueDate(days);
    });
	function setDueDate(days) {
        var day = parseInt(days);
        var joindate = eb.convertDateFormat('#issue_date');
        joindate.setDate(joindate.getDate() + day);

        var dd = joindate.getDate() < 10 ? '0' + joindate.getDate() : joindate.getDate();
        var mm = (joindate.getMonth() + 1) < 10 ? '0' + (joindate.getMonth() + 1) : (joindate.getMonth() + 1);
        var y = joindate.getFullYear();
        var joinFormattedDate = eb.getDateForDocumentEdit('#expire_date', dd, mm, y);

        $("#expire_date").val(joinFormattedDate);
	}
});

/**
 * Prepare tax list (Drop down)
 * @param {type} productCode
 */
function setTaxListForProduct(productID) {
    if ((!jQuery.isEmptyObject(currentProducts[productID].tax))) {
        productTax = currentProducts[productID].tax;
        $('#addNewTax').attr('disabled', false);
        $('#taxApplied').removeClass('glyphicon glyphicon-unchecked');
        $('#taxApplied').addClass('glyphicon glyphicon-check');
        $('.tempLi').remove();
        for (var i in productTax) {
            var clonedLi = $($('#sampleLi').clone()).removeClass('hidden').attr('id', productID + '_' + i).addClass("tempLi");
            clonedLi.children(".taxChecks").attr('value', i).prop('checked', true).addClass('addNewTaxCheck');
            if (productTax[i].tS == 0) {
                clonedLi.children(".taxChecks").attr('disabled', 'disabled');
                clonedLi.children(".taxName").addClass('crossText');
            }
            clonedLi.children(".taxName").html('&nbsp&nbsp' + productTax[i].tN + '&nbsp&nbsp' + productTax[i].tP + '%').attr('for', productID + '_' + i);
            clonedLi.insertBefore('#sampleLi');
        }
    } else {
        $('#taxApplied').addClass('glyphicon glyphicon-unchecked');
        $('#addNewTax').attr('disabled', 'disabled');
    }

}

/**
 *
 * @param {type} productCode
 * @param {type} price
 * @param {type} disc
 * @returns {dis_amount}
 */
function calculateDiscount(productCode, price, disc, qty) {
    var dis_amount = 0;
    if (currentProducts[productCode] !== undefined && currentProducts[productCode].dEL === "1") {

        if (priceListItems[productCode] == undefined) {
            var pdp = currentProducts[productCode].dPR;
            var pdv = parseFloat(currentProducts[productCode].dV);
            if (pdp > 0) {
                //Check Customer discount against product discount
                if (disc > pdp) {
                    p_notification(false, eb.getMessage('ERR_QUOT_DISC_VALUE'));
                } else {
                    if (price > 0 && disc > 0)
                        dis_amount = (price * disc / 100);
                }
            }else if (pdp == 0 && !pdv) {
                //Check Customer discount against 100
                if (disc > 100) {
                    p_notification(false, eb.getMessage('ERR_PRODUCT_DISCPERCENT_LIMIT'));
                } else {
                    if (price > 0 && disc > 0)
                        dis_amount = (price * disc / 100);
                }
            }
            else if (pdv >= 0)
            {
                //Check Customer discount against product discount
                if (disc > pdv && pdv != 0)
                {
                    p_notification(false, eb.getMessage('ERR_QUOT_DISC_VALUE'));
                }
                else
                {
                    if (price > 0 && qty > 0 && disc > 0)
                        dis_amount = (qty * disc);
                }
            }
        } else {
            var itemDiscountType = priceListItems[productCode].itemDiscountType;
            if (itemDiscountType == 1) {
                dis_amount = (qty * disc);
            } else {
                dis_amount = (price * disc / 100);
            }
        }
    } else if (priceListItems[productCode] != undefined && parseFloat(priceListItems[productCode].itemPrice) > 0) {
        var itemDiscountType = priceListItems[productCode].itemDiscountType;
        if (itemDiscountType == 1) {
            dis_amount = (qty * disc);
        } else {
            dis_amount = (price * disc / 100);
        }
    }
    return dis_amount;
}

function clearTable() {


    $("#item_code").empty().selectpicker('refresh');

    if($("#idOfcanCreateItem").val() == 0) {
        $("#item_code").append($("<option></option>")
                                .attr("value", 0)
                                .text('Select a Item'))
                                .selectpicker('refresh');
    } else {

        $("#item_code").append($("<option></option>")
                                    .attr("value", 0)
                                    .text('Select a Item'))
                        .append($("<option></option>")
                                    .attr("value", 'add')
                                    .text('Add New Item'))
                                    .addClass('addNewItem')
                            .selectpicker('refresh');
    }


    $("#item_code").val(null);
    $("#qty").val("");
    $("#qty").siblings('.uomqty').remove();
    $("#qty").siblings('.uom-select').remove();
    $('#qty').parent().removeClass('input-group');
    $("#qty").show();
    $("#price").val("");
    $("#price").siblings('.uomPrice').remove();
    $("#price").siblings('.uom-price-select').remove();
    $("#price").show();
    $("#total").html("0.00");
    $("#discount").attr('disabled', false);
    $("#discount").val("");
    $("#discount").siblings('.uomPrice').remove();
    $("#discount").siblings('.uom-price-select').remove();
    $("#discount").show();
    $("#dic_lbl").html("Dis.nt");
    $('.uomLi').remove();
    $("#uomAb").html("");
    $('#taxApplied').removeClass('glyphicon glyphicon-checked');
    $('#taxApplied').addClass('glyphicon glyphicon-unchecked');
    $('#addNewTax').attr('disabled', false);
    $('.tempLi').remove();
}

function addProduct(code, pName, quantity, price, uom, dscType, dscAmount, tax, total, uomQty, productType, productID, uomPrice, item_discription) {

    if (code === "") {
        p_notification(false, eb.getMessage('ERR_QUOT_PRCODE_EMPTY'));
    } else if (products[code] !== undefined) {
        p_notification(false, eb.getMessage('ERR_QUOT_PR_ALREADY_ADD'));
    } else if (pName === "") {
        p_notification(false, eb.getMessage('ERR_QUOT_PRNAME_EMPTY'));
    } else if (productType == 1 && (quantity === "" || quantity <= 0)) {
        p_notification(false, eb.getMessage('ERR_QUOT_QUAN_VALUE'));
    } else if (uom === undefined) {
        p_notification(false, eb.getMessage('ERR_QUOT_SELECT_UOM'));
    } else if (price <= 0) {
        p_notification(false, eb.getMessage('ERR_QUOT_ENTER_PRICE'));
    } else if (dscType === 'vl' && parseFloat(dscAmount) > price) {
        p_notification(false, eb.getMessage('ERR_QUOT_DISCVAL_PRODPRICE'));
    } else {
    	var showDiscHtml = '<div class="pull-right input-group">'+
    	'<span class="input-group-btn">'+
    	'<button  type="button" class="btn btn-default showItemDescription" title="Show comment">'+
    	'<i class="fa fa-comment-o"></i></button></span></div>'+
    	'<textarea class="showItemDescText form-control hidden" readonly="true" placeholder="Enter item description">'+
    	'</textarea>';

        tot = total == null ? accounting.unformat($("#total").html()) : total.toFixed(2);
        var dummy = $($("#preSetSample").clone()).attr("id", "tr_" + code);
        dummy.attr("data-itemId", productID);
        dummy.children("#code").html(code + ' - ' + pName + ' ' + showDiscHtml);
        dummy.children("#quantity").html(uomQty);
        dummy.children("#unit").html(accounting.formatMoney(price * currentProducts[productID].uom[uom].uC) + " (" + currentProducts[productID].uom[uom].uA + ")");
        dummy.children().children('#uomName').html(currentProducts[productID].uom[uom].uA);
        if (dscType === 'vl' && dscAmount > 0) {
            dummy.children("#disc").html((dscAmount * currentProducts[productID].uom[uom].uC) + '&nbsp;(' + currentProducts[productID].uom[uom].uA + ')' + '&nbsp;(' + customCurrencySymbol + ')');
        } else if (dscType === 'pr' && dscAmount > 0) {
            dummy.children("#disc").html(accounting.formatMoney(dscAmount) + '&nbsp;(%)');
        } else {
            dscType = 'na';
        }
        dummy.children("#code").find('.showItemDescText').val(item_discription);
        dummy.children().children(".delete").attr("id", code);
        var clonedTax = $($('#addNewTaxDiv').clone()).attr('id', '');
        clonedTax.find('.tempLi').each(function() {
            if ($(this).children(".taxChecks").prop('checked') != true) {
                $(this).children(".taxName").addClass('crossText');
            }
        });
        clonedTax.children('#addNewTax').children('#taxApplied').attr('id', '');
        clonedTax.children('#addNewTax').attr('id', '');
        clonedTax.children('#addTaxUl').children().removeClass('tempLi');
        clonedTax.children('#addTaxUl').children().children().removeClass('addNewTaxCheck');
        clonedTax.children('#addTaxUl').children().children(".taxChecks").remove();
        clonedTax.children('#addTaxUl').children('#sampleLi').remove();
        clonedTax.children('#addTaxUl').attr('id', '');
        clonedTax.find('#toggleSelection').addClass('hidden');
        dummy.children("#tax").html(clonedTax);
        dummy.children("#ttl").html(accounting.formatMoney(tot));
        dummy.removeClass("hide").insertBefore("#preSetSample");
        var qty = quantity;
        dscAmount = Number.isNaN(dscAmount) ? 0 : dscAmount;
        products[code] = new product(code, pName, qty, uom, price, dscAmount, dscType, tax, tot, taxdetails, productType, item_discription, productID);
        clearTable();
    }

}
function setDataInitialLoading() {
    var key = $('#cust_name option:selected').val();
    customerID = key;
    customerName = $('#cust_name option:selected').text();
}

function getCustomerProfilesDetails(customerID) {
    eb.ajax({
        type: 'POST',
        url: BASE_URL + '/customerAPI/getCustomerProfilesByCustomerID',
        data: {customerID: customerID},
        success: function(respond) {
            if (respond.status == true) {
                $('.cus-prof-select').removeClass('hidden');
                $('.tooltip_for_default_cus').removeClass('hidden');
                $('.default-cus-prof').addClass('hidden');
                setCustomerProfilePicker(respond.data['customerProfileData']);
            } else {
                $('.cus-prof-select').addClass('hidden');
                $('.tooltip_for_default_cus').addClass('hidden');
                $('.default-cus-prof').removeClass('hidden');

            }
        }
    });
}

function setCustomerProfilePicker(data) {
    $.each(data, function(index, value) {
        $('#cusProfileQuot').append("<option value='" + index + "'>" + value['profName'] + "</option>");
        if (value['isPrimary'] == 1) {
            $('#cusProfileQuot').html("<option value='" + index + "'>" + value['profName'] + "</option>");
            customerProfileID = index;
        }
    });
    $('#cusProfileQuot').selectpicker('refresh');

    $('#cusProfileQuot').on('change', function(e) {
        e.preventDefault();
        if ($(this).val() > 0 && ($(this).val() != customerProfileID)) {
            customerProfileID = $(this).val();
        } else {
            customerProfileID = '';
        }
    });
}