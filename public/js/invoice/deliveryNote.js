/**
 * @author ASHAN <ashan@thinkcube.com>
 * This file contains form validation
 */

var locationIn;
var customerID;
var salesOrderID = '';
var batchProducts = {};
var batchSerialProducts = {};
var serialProducts = {};
var checkSubProduct = {};
var deliverProducts = {};
var productsTotal = {};
var productsTax = {};
var deliverSubProducts = {};
var SubProductsQty = {};
var currentProducts = {};
var selectedPID;
var locationID;
var customCurrencyRate = 0;
var cusProfID;
var locationProducts;
var dimensionData = {};
var ignoreBudgetLimitFlag = false;

$(document).ready(function() {
    var decimalPoints = 0;
    var currentItemTaxResults;
    var currentTaxAmount;
    var totalTaxList = {};
    var $productTable = $("#productTable");
    var $addRowSample = $('tr.add-row.sample.hidden', $productTable);
    var $scanSubmit = false;
    var priceListItems = [];
    var priceListId = '';
    var defaultSellingPriceData = [];
    var dimensionArray = {};
    var dimensionTypeID = null;

    var productID = $("table.deliveryNoteProductTable > tbody#add-new-item-row > tr.add-row").attr('id');


    var getAddRow = function(productID) {
        if (productID != undefined) {

            var $row = $('table.deliveryNoteProductTable > tbody > tr', $productTable).filter(function() {
                return $(this).data("id") == productID;
            });
            return $row;
        }

        return $('tr.add-row:not(.sample)', $productTable);
    };
    if (!getAddRow().length) {
        $($addRowSample.clone()).removeClass('sample hidden').appendTo($('#add-new-item-row'));
    }
    locationOut = $('#idOfLocation').val();
    clearProductScreen();


    $('#cusProfileDN').on('change', function(){
        var customerFID;
        if(customerFID != $(this).val())
            customerFID = $(this).val();
            eb.ajax({
                type: 'POST',
                url: BASE_URL + '/customerAPI/getCustomerProfileDataByCustomerProfileID',
                data: {customerProfileID: customerFID},
                    success: function(respond) {
                        if (respond.status == true) {
                            $('#deliveryNoteAddress').val(respond.data.addressLine);
                        } else {
                            $('#deliveryNoteAddress').val("");
                        }
                    }
            });
    });

    $('#dimensionView').on('click', function(e) {
        clearDimensionModal();
        var deliveryNoteNo = $('#deliveryNoteNo').val();

        if (!$.isEmptyObject(dimensionData)) {
            $.each(dimensionData[deliveryNoteNo], function(index, val) {
                dimensionArray[index] = val;                
            });
            $.each(dimensionArray, function(index, value) {
                var newTrID = 'tr_' + value.dimensionTypeId;
                var clonedRow = $($('#dimensionPreSample').clone()).attr('id', newTrID).addClass('addedDimensions');
                $("input[name='dimensionType']", clonedRow).val(value.dimensionTypeTxt);
                $("input[name='dimensionValue']", clonedRow).val(value.dimensionValueTxt);
                clonedRow.removeClass('hidden');
                clonedRow.insertBefore('#dimensionPreSample');
                clearAddedNewDimensionRow();
            });
        }
        $('#addDimensionModal').modal('show');
    });

    // dimension modal
    function clearDimensionModal()
    {
        $("#addDimensionModal #dimensionTable tbody .addedDimensions").remove();
        dimensionArray = {};
    }


    $('.dimenisonchange').on('change', function(e) {
        dimensionTypeID = this.value;
        getDimensionValueListByDimensionType(dimensionTypeID, function() {
        });
    });

    $('#customer').select2({
        minimumInputLength: 1,
        minimumResultsForSearch: 10,
        ajax: {
            url: BASE_URL + '/customerAPI/search-customers-for-dropdown',
            dataType: "json",
            type: "POST",
            data: function (params) {

                var queryParameters = {
                    searchKey: params.term
                }
                return queryParameters;
            },
            processResults: function (data) {
                return {
                    results: $.map(data.data.list, function (item) {
                        return {
                            text: item.text,
                            id: item.value
                        }
                    })
                };
            }
        },
        placeholder: 'Select Customer Name',
    });

    $('#customer').on('select2:select', function (e) { 
        var data = e.params.data;
        var key = data.id;
        if (data.id > 0 && customerID != data.id) {
            customerID = key;
            // setCustomerData(key);
            // getCustomerProfilesDetails(key);
            getCustomerProfilesDetails(customerID);
            eb.ajax({
                type: 'POST',
                url: BASE_URL + '/invoice-api/getCustomerDetails', data: {customerID: customerID}, success: function(respond) {
                    if (respond.status == true) {
                        $('#deliveryNoteAddress').val(respond.data.customerAddress);
                    } else {
                        $('#deliveryNoteAddress').val();
                    }
                }
            });
        }
    });


    $('#salesOrderNo').select2({
        minimumInputLength: 1,
        minimumResultsForSearch: 10,
        ajax: {
            url: BASE_URL + '/api/salesOrders/search-sales-orders-for-dropdown',
            dataType: "json",
            type: "POST",
            data: function (params) {
                console.log(params.term);
                var queryParameters = {
                    searchKey: params.term,
                    locationID: locationID
                }
                return queryParameters;
            },
            processResults: function (data) {
                return {
                    results: $.map(data.data.list, function (item) {
                        return {
                            text: item.text,
                            id: item.value
                        }
                    })
                };
            }
        },
        placeholder: 'Select Sales Order Number',
    });

    $('#salesOrderNo').on('select2:select', function (e) { 
        var data = e.params.data;
        resetDeliveryNotePage();
        if (data.id > 0) {
            salesOrderID = $(this).val();
            getSalesOrderDetailsForDrpdown($(this).val());
        }
    });



    function getDimensionValueListByDimensionType(dimensionTypeID, callback) {
        switch (dimensionTypeID) {
            case "job":
                $('.dimensionVal').addClass('hidden');
                $('.dimensionProjectNo').addClass('hidden');
                $('.dimensionJobNo').removeClass('hidden');
                break;
            case "project":
                $('.dimensionVal').addClass('hidden');
                $('.dimensionProjectNo').removeClass('hidden');
                $('.dimensionJobNo').addClass('hidden');
                break;
           default:
                $('.dimensionVal').removeClass('hidden');
                $('.dimensionProjectNo').addClass('hidden');
                $('.dimensionJobNo').addClass('hidden');
                $.ajax({
                    type: 'POST',
                    url: BASE_URL + '/dimension-api/getRelatedDimensionValues',
                    data: {dimensionId: dimensionTypeID},
                    success: function(respond) {
                        //empty options
                        $('#dimensionVal').empty();
                        $('#dimensionVal').append($("<option>", {value: '', html: 'Select dimension value'}));
                        $(respond.data).each(function(index, dimValue) {
                            $.each(dimValue, function(ind, val) {
                                $('#dimensionVal').append($("<option>", {value: ind, html: val.value}));
                            });
                        });
                        callback();
                    }
                });
                break;
        }
    }

    $('#add_dimension').on('click', function(e) {
        e.preventDefault();
        //if plus button is clicked then disabled the button
        //if button is clicked more than one, then it does not continue
        if ($(this).is(':disabled')) {
            return false;
        } else {
            $(this).attr('disabled', true);
            var $thisRow = $(this).parents('tr');
            if ($('.dimenisonchange').val() == 0 || $('.dimenisonchange').val() == '' || $('.dimenisonchange').val() == null) {
                p_notification(false, eb.getMessage('ERR_DIMENSION_SELCET'));
                $(this).attr('disabled', false);
            } else {

                if ($('.dimenisonchange').val() != 'job' && $('.dimenisonchange').val() != 'project') {
                    if ($('.dimensionVal').val() == 0 || $('.dimensionVal').val() == '' || $('.dimensionVal').val() == null) {
                        p_notification(false, eb.getMessage('ERR_DIMENSION_VALUE_SELCET'));
                        $(this).attr('disabled', false);
                        return;
                    }
                }

                if ($('.dimenisonchange').val() == 'project') {
                    if ($('.dimensionProjectNo').val() == 0 || $('.dimensionProjectNo').val() == '' || $('.dimensionProjectNo').val() == null) {
                        p_notification(false, eb.getMessage('ERR_DIMENSION_VALUE_SELCET'));
                        $(this).attr('disabled', false);
                        return;
                    }
                }

                if ($('.dimenisonchange').val() == 'job') {
                    if ($('.dimensionJobNo').val() == 0 || $('.dimensionJobNo').val() == '' || $('.dimensionJobNo').val() == null) {
                        p_notification(false, eb.getMessage('ERR_DIMENSION_VALUE_SELCET'));
                        $(this).attr('disabled', false);
                        return;
                    }
                }
                
                if (dimensionArray[dimensionTypeID]) {
                    p_notification(false, eb.getMessage('ERR_DIMENSION_ALREADY_INSERTED'));
                    $('.dimenisonchange').val('');
                    $('.dimenisonchange').selectpicker('render');
                    $('.dimenisonchange').trigger('change');
                } else {
                    addNewDimensionRow($(this).parents('tr'));
                }
                $(this).attr('disabled', false);
            }
        }
    });

    function addNewDimensionRow(thisRow) {

        var dimensionTypeId = $('.dimenisonchange', thisRow).val();
        var dimensionTypeTxt = $('.dimenisonchange', thisRow).find("option:selected").text();

        switch (dimensionTypeID) {
            case "job":
                var dimensionValueTxt = $('.dimensionJobNo', thisRow).find("option:selected").text();
                var dimensionValueId = $('.dimensionJobNo', thisRow).val();
                break;
            case "project":
                var dimensionValueTxt = $('.dimensionProjectNo', thisRow).find("option:selected").text();
                var dimensionValueId = $('.dimensionProjectNo', thisRow).val();
                break;
           default:
                var dimensionValueTxt = $('.dimensionVal', thisRow).find("option:selected").text();
                var dimensionValueId = $('.dimensionVal', thisRow).val();
                break;
        }
       
        var dimensionDuplicateCheck = false;
        if (!$.isEmptyObject(dimensionArray)) {
            $.each(dimensionArray, function(index, value) {
                if (value.dimensionTypeId == dimensionTypeId) {
                    dimensionDuplicateCheck = true;
                }
            });   
        }
       
        if (dimensionDuplicateCheck) {
            p_notification(false, eb.getMessage("ERR_DIMENSION_ALREADY_INSERTED"));
            return false;
        }

        var newTrID = 'tr_' + dimensionTypeId;
        var clonedRow = $($('#dimensionPreSample').clone()).attr('id', newTrID).addClass('addedDimensions');
        $("input[name='dimensionType']", clonedRow).val(dimensionTypeTxt);
        $("input[name='dimensionValue']", clonedRow).val(dimensionValueTxt);
        clonedRow.removeClass('hidden');
        clonedRow.insertBefore('#dimensionPreSample');
        dimensionArray[dimensionTypeId] = new dimension(dimensionTypeId, dimensionValueId,dimensionTypeTxt , dimensionValueTxt);
        clearAddedNewDimensionRow();
    }


    function dimension(dimensionTypeId, dimensionValueId, dimensionTypeTxt, dimensionValueTxt) {
        this.dimensionTypeId = dimensionTypeId;
        this.dimensionValueId = dimensionValueId;
        this.dimensionTypeTxt = dimensionTypeTxt;
        this.dimensionValueTxt = dimensionValueTxt;
    }

    function clearAddedNewDimensionRow() {
        $('.dimenisonchange').val('');
        $('.dimenisonchange').selectpicker('render');
        $('.dimenisonchange').trigger('change');
    }

    $('.addDimensions').on('click', function(e) {
        e.preventDefault();

        // validate products before closing modal
        if (!dimensionModalValidate(e)) {
            e.stopPropagation();
            return false;
        } else {
            $('#addDimensionModal').modal('hide');
        }
    });

    function dimensionModalValidate(e) {
        var deliveryNoteNo = $('#deliveryNoteNo').val();
        if ($.isEmptyObject(dimensionArray)) {
            dimensionData = {};
            return true;   
        } else {
            var dimensionSingleData = {};
            $.each(dimensionArray, function(index, val) {
                dimensionSingleData[index] = val;
            });
            dimensionData[deliveryNoteNo] = dimensionSingleData;
            return true;
        }
    }

    $("#addDimensionModal #dimensionTable").on('click', '.deleteDimensionType', function(e) {
        e.preventDefault();
        var deleteDTrID = $(this).closest('tr').attr('id');
        var deleteDID = deleteDTrID.split('tr_')[1].trim();
        bootbox.confirm('Are you sure you want to remove this dimension ?', function(result) {
            if (result == true) {
                delete dimensionArray[deleteDID];
                $('#' + deleteDTrID).remove();
            }
        });
    });

    loadDropDownFromDatabase('/job-api/searchJobsForDropdown', "", 0, '.dimensionJobNo');
    $('.dimensionJobNo').selectpicker('refresh');
    $('.dimensionJobNo').on('change', function() {
        var dimensionValue = $(this).val();
    });

    loadDropDownFromDatabase('/api/project/searchProjectsForDropdown', "", 0, '.dimensionProjectNo');
    $('.dimensionProjectNo').selectpicker('refresh');
    $('.dimensionProjectNo').on('change', function() {
        var dimensionValue = $(this).val();
    });

    // this will append all sales order related product to delivery note(copy to function)
    function getSalesOrderDetails(SOID) {
        eb.ajax({
            type: 'POST',
            url: BASE_URL + '/api/salesOrders/getSalesOrderDetails',
            data: {
                salesOrderID: SOID
            },
            success: function(respond) {
                if (respond.status) {
                    if (respond.data.inactiveItemFlag) {
                        p_notification(false, respond.data.errorMsg);
                        setTimeout(function(){ 
                            window.location.assign(BASE_URL + "/delivery-note")
                        }, 3000);
                        return false;
                    }
                    customerID = respond.data.salesOrder.customerID;
                    cusProfID = respond.data.salesOrder.customerProfileID;
                    var customer = respond.data.customer;
                    $('#deliveryNoteAddress').val(customer.customerAddress);
                    $('#customer').empty();
                    $('#customer').
                            append($("<option></option>").
                                    attr("value", customer.customerID).
                                    text(customer.customerName + '-' + customer.customerCode))
                            .attr('disabled', true);
                    $('#customer').selectpicker('render');
                    var cusProfName;
                    if (respond.data.customerProfileName == '' || respond.data.customerProfileName == null) {
                        cusProfName = 'Default';
                    } else {
                        cusProfName = respond.data.customerProfileName;
                    }
                    $('.cus_prof_div').removeClass('hidden');
                    $('.cus-prof-select').addClass('hidden');
                    $('.default-cus-prof').removeClass('hidden');
                    $('.tooltip_for_default_cus').addClass('hidden');
                    $('#cus-prof-name').val(cusProfName);
                    $('#salesPersonID').val(respond.data.salesOrder.salesPersonID);
                    if (respond.data.salesOrder.salesPersonID != 0) {
                        $('#salesPersonID').attr('disabled', true);
                    }
                    $('#salesPersonID').selectpicker('render');
                    $('#comment').val(respond.data.salesOrder.comment);
                    currentProducts = respond.data.locationProducts;
                    locationProducts = respond.data.locationProducts;

                    var custCurrencyId = respond.data.salesOrder.customCurrencyId;
                    var custCurrencyRate = respond.data.salesOrder.salesOrdersCustomCurrencyRate;

                    customCurrencyRate = (custCurrencyRate != 0) ? parseFloat(custCurrencyRate) : 1;
                    companyCurrencySymbol = respond.data.salesOrder.customCurrencySymbol;

                    if (respond.data.salesOrder.priceListId != 0) {
                        $('#priceListId').val(respond.data.salesOrder.priceListId).attr('disabled', true).selectpicker('refresh');
                        $('#priceListId').trigger('change');
                    }
                    $.each(respond.data.salesOrderProduct, function(index, value) {
                        var $currentRow = getAddRow();
                        defaultSellingPriceData[value.productID] = value.unitPrice;
                        //sales order product related tax apply in here
                        if (value.tax) {
                            $currentRow.find('#taxApplied').removeClass('glyphicon glyphicon-unchecked');
                            $currentRow.find('#taxApplied').addClass('glyphicon glyphicon-check');
                            $currentRow.find('.tempLi').remove();
                            for (var i in value.tax) {
                                var clonedLi = $($currentRow.find('#sampleLi').clone()).removeClass('hidden').attr('id', 'li_' + i).addClass("tempLi");
                                clonedLi.children(".taxChecks").attr('id', value.productID + '_' + i).prop('checked', true).addClass('addNewTaxCheck');
                                clonedLi.children(".taxName").html('&nbsp&nbsp' + value.tax[i].salesOrderTaxName + '&nbsp&nbsp' + value.tax[i].salesOrderTaxPrecentage + '%').attr('for', value.productID + '_' + i);
                                clonedLi.insertBefore($currentRow.find('#sampleLi'));
                            }
                        }

                        var baseUom;
                        for (var j in locationProducts[value.productID].uom) {
                            if (locationProducts[value.productID].uom[j].pUBase == 1) {
                                baseUom = locationProducts[value.productID].uom[j].uomID;
                            }
                        }

                        //sales order product realated data append in here
                        $currentRow.data('id', value.productID);
                        $currentRow.data('copied', true);
                        $currentRow.attr('id', 'product' + value.productID);
                        $("#itemCode", $currentRow).empty();
                        $("#itemCode", $currentRow).
                                append($("<option></option>").
                                        attr("value", value.productID).
                                        text(value.productName + '-' + value.productCode));
                        $("#itemCode", $currentRow).val(value.productID).prop('disabled', true);
                        $("#itemCode", $currentRow).selectpicker('render')
                        $("#itemCode", $currentRow).data('PT', value.productType);
                        $("#itemCode", $currentRow).data('PC', value.productCode);
                        $("#itemCode", $currentRow).data('PN', value.productName);
                        $("input[name='availableQuantity']", $currentRow).val(currentProducts[value.productID].LPQ).prop('readonly', true).addUom(currentProducts[value.productID].uom);
                        $("input[name='deliverQuanity']", $currentRow).val(value.quantity).addUom(currentProducts[value.productID].uom);
                        $("input[name='availableQuantity']", $currentRow).parent().addClass('input-group');
                        $("input[name='deliverQuanity']", $currentRow).parent().addClass('input-group');
                        $("input[name='unitPrice']", $currentRow).val(value.unitPrice / customCurrencyRate).addUomPrice(currentProducts[value.productID].uom);
                        if (locationProducts[value.productID].dEL == 1) {
                            if (locationProducts[value.productID].dV != null) {
                                $("input[name='discount']", $currentRow).val(parseFloat(value.discount / customCurrencyRate).toFixed(2)).addUomPrice(currentProducts[value.productID].uom, baseUom);
                                $("input[name='discount']", $currentRow).siblings('.uom-price-select').find('.uom_price_drop_down').attr('disabled', true);
                                $("input[name='discount']", $currentRow).addClass('value');
                                $(".sign", $currentRow).text(companyCurrencySymbol);
                            } else if (locationProducts[value.productID].dPR != null){
                                $("input[name='discount']", $currentRow).val(parseFloat(value.discount).toFixed(2));
                                $("input[name='discount']", $currentRow).addClass('precentage');
                                $(".sign", $currentRow).text('%');
                            } else {
                                $("input[name='discount']", $currentRow).attr('readonly', true);
                            }
                        } else {
                                $("input[name='discount']", $currentRow).attr('readonly', true);
                        }

                        //check wheter sales order product have sub product or not
                        $("input[name='deliverQuanity']", $currentRow).trigger(jQuery.Event("focusout"));
                        if ($.isEmptyObject(currentProducts[value.productID].batch) &&
                                $.isEmptyObject(currentProducts[value.productID].serial) &&
                                $.isEmptyObject(currentProducts[value.productID].batchSerial)) {

                            $currentRow.removeClass('subproducts');
                            $('.edit-modal', $currentRow).parent().addClass('hidden');
                            $("td[colspan]", $currentRow).attr('colspan', 2);
                        } else {
                            checkSubProduct[value.productID] = 0;
                            $currentRow.addClass('subproducts');
                            SubProductsQty[value.productID] = value.quantity;
                            $("input[name='deliverQuantity']", $currentRow).prop('readonly', true).change();
                        }
                        $currentRow.removeClass('add-row');
                        $currentRow.find("button.delete").removeClass('disabled');
                        var $newRow = $($addRowSample.clone()).removeClass('sample hidden').appendTo($('#add-new-item-row', $productTable));
                        setProductTypeahead();
                        $newRow.find("#itemCode").focus();
                    });
                    $('#customCurrencyId').val(custCurrencyId).trigger('change');
                    if (!(custCurrencyId == '' || custCurrencyId == 0)) {
                        $('#customCurrencyId').attr('disabled', true);
                    }
                } else {
                    salesOrderID = '';
                    p_notification(respond.status, respond.msg);
                    $('#salesOrderNo').val('');
                }

            }
        });
    }
    function getSalesOrderDetailsForDrpdown(SOID) {
        eb.ajax({
            type: 'POST',
            url: BASE_URL + '/api/salesOrders/getSalesOrderDetails',
            data: {
                salesOrderID: SOID
            },
            success: function(respond) {
                if (respond.status) {
                    if (respond.data.inactiveItemFlag) {
                        p_notification(false, respond.data.errorMsg);
                        setTimeout(function(){ 
                            window.location.assign(BASE_URL + "/delivery-note")
                        }, 3000);
                        return false;
                    }
                    customerID = respond.data.salesOrder.customerID;
                    cusProfID = respond.data.salesOrder.customerProfileID;
                    var customer = respond.data.customer;
                    $('#deliveryNoteAddress').val(customer.customerAddress);
                    $('#customer').empty();
                    $('#customer').
                            append($("<option></option>").
                                    attr("value", customer.customerID).
                                    text(customer.customerName + '-' + customer.customerCode))
                            .attr('disabled', true);
                    $('#customer').selectpicker('render');
                    var cusProfName;
                    if (respond.data.customerProfileName == '' || respond.data.customerProfileName == null) {
                        cusProfName = 'Default';
                    } else {
                        cusProfName = respond.data.customerProfileName;
                    }
                    $('.cus_prof_div').removeClass('hidden');
                    $('.cus-prof-select').addClass('hidden');
                    $('.default-cus-prof').removeClass('hidden');
                    $('.tooltip_for_default_cus').addClass('hidden');
                    $('#cus-prof-name').val(cusProfName);
                    $('#salesPersonID').val(respond.data.salesOrder.salesPersonID);
                    if (respond.data.salesOrder.salesPersonID != 0) {
                        $('#salesPersonID').attr('disabled', true);
                    }
                    $('#salesPersonID').selectpicker('render');
                    $('#comment').val(respond.data.salesOrder.comment);
                    currentProducts = respond.data.locationProducts;
                    locationProducts = respond.data.locationProducts;

                    var custCurrencyId = respond.data.salesOrder.customCurrencyId;
                    var custCurrencyRate = respond.data.salesOrder.salesOrdersCustomCurrencyRate;

                    customCurrencyRate = (custCurrencyRate != 0) ? parseFloat(custCurrencyRate) : 1;
                    companyCurrencySymbol = respond.data.salesOrder.customCurrencySymbol;

                    if (respond.data.salesOrder.priceListId != 0) {
                        $('#priceListId').val(respond.data.salesOrder.priceListId).attr('disabled', true).selectpicker('refresh');
                        $('#priceListId').trigger('change');
                    }
                    $.each(respond.data.salesOrderProduct, function(index, value) {
                        var $currentRow = getAddRow();
                        defaultSellingPriceData[value.productID] = value.unitPrice;
                        //sales order product related tax apply in here
                        if (value.tax) {
                            $currentRow.find('#taxApplied').removeClass('glyphicon glyphicon-unchecked');
                            $currentRow.find('#taxApplied').addClass('glyphicon glyphicon-check');
                            $currentRow.find('.tempLi').remove();
                            for (var i in value.tax) {
                                var clonedLi = $($currentRow.find('#sampleLi').clone()).removeClass('hidden').attr('id', 'li_' + i).addClass("tempLi");
                                clonedLi.children(".taxChecks").attr('id', value.productID + '_' + i).prop('checked', true).addClass('addNewTaxCheck');
                                clonedLi.children(".taxName").html('&nbsp&nbsp' + value.tax[i].salesOrderTaxName + '&nbsp&nbsp' + value.tax[i].salesOrderTaxPrecentage + '%').attr('for', value.productID + '_' + i);
                                clonedLi.insertBefore($currentRow.find('#sampleLi'));
                            }
                        }

                        var baseUom;
                        for (var j in locationProducts[value.productID].uom) {
                            if (locationProducts[value.productID].uom[j].pUBase == 1) {
                                baseUom = locationProducts[value.productID].uom[j].uomID;
                            }
                        }

                        //sales order product realated data append in here
                        $currentRow.data('id', value.productID);
                        $currentRow.data('copied', true);
                        $currentRow.attr('id', 'product' + value.productID);
                        $("#itemCode", $currentRow).empty();
                        $("#itemCode", $currentRow).
                                append($("<option></option>").
                                        attr("value", value.productID).
                                        text(value.productName + '-' + value.productCode));
                        $("#itemCode", $currentRow).val(value.productID).prop('disabled', true);
                        // $("#itemCode", $currentRow).selectpicker('render')
                        $("#itemCode", $currentRow).data('PT', value.productType);
                        $("#itemCode", $currentRow).data('PC', value.productCode);
                        $("#itemCode", $currentRow).data('PN', value.productName);
                        $("input[name='availableQuantity']", $currentRow).val(currentProducts[value.productID].LPQ).prop('readonly', true).addUom(currentProducts[value.productID].uom);
                        $("input[name='deliverQuanity']", $currentRow).val(value.quantity).addUom(currentProducts[value.productID].uom);
                        $("input[name='availableQuantity']", $currentRow).parent().addClass('input-group');
                        $("input[name='deliverQuanity']", $currentRow).parent().addClass('input-group');
                        $("input[name='unitPrice']", $currentRow).val(value.unitPrice / customCurrencyRate).addUomPrice(currentProducts[value.productID].uom);
                        if (locationProducts[value.productID].dEL == 1) {
                            if (locationProducts[value.productID].dV != null) {
                                $("input[name='discount']", $currentRow).val(parseFloat(value.discount / customCurrencyRate).toFixed(2)).addUomPrice(currentProducts[value.productID].uom, baseUom);
                                $("input[name='discount']", $currentRow).siblings('.uom-price-select').find('.uom_price_drop_down').attr('disabled', true);
                                $("input[name='discount']", $currentRow).addClass('value');
                                $(".sign", $currentRow).text(companyCurrencySymbol);
                            } else if (locationProducts[value.productID].dPR != null){
                                $("input[name='discount']", $currentRow).val(parseFloat(value.discount).toFixed(2));
                                $("input[name='discount']", $currentRow).addClass('precentage');
                                $(".sign", $currentRow).text('%');
                            } else {
                                $("input[name='discount']", $currentRow).attr('readonly', true);
                            }
                        } else {
                                $("input[name='discount']", $currentRow).attr('readonly', true);
                        }

                        //check wheter sales order product have sub product or not
                        $("input[name='deliverQuanity']", $currentRow).trigger(jQuery.Event("focusout"));
                        if ($.isEmptyObject(currentProducts[value.productID].batch) &&
                                $.isEmptyObject(currentProducts[value.productID].serial) &&
                                $.isEmptyObject(currentProducts[value.productID].batchSerial)) {

                            $currentRow.removeClass('subproducts');
                            $('.edit-modal', $currentRow).parent().addClass('hidden');
                            $("td[colspan]", $currentRow).attr('colspan', 2);
                        } else {
                            checkSubProduct[value.productID] = 0;
                            $currentRow.addClass('subproducts');
                            SubProductsQty[value.productID] = value.quantity;
                            $("input[name='deliverQuantity']", $currentRow).prop('readonly', true).change();
                        }
                        $currentRow.removeClass('add-row');
                        $currentRow.find("button.delete").removeClass('disabled');
                        var $newRow = $($addRowSample.clone()).removeClass('sample hidden').appendTo($('#add-new-item-row', $productTable));
                        setProductTypeahead();
                        $newRow.find("#itemCode").focus();
                    });
                    $('#customCurrencyId').val(custCurrencyId).trigger('change');
                    if (!(custCurrencyId == '' || custCurrencyId == 0)) {
                        $('#customCurrencyId').attr('disabled', true);
                    }
                } else {
                    salesOrderID = '';
                    p_notification(respond.status, respond.msg);
                    $('#salesOrderNo').val('');
                }

            }
        });
    }
    setProductTypeahead();
    function setProductTypeahead() {

        var $currentRow = getAddRow();
        // $('#itemCode', $currentRow).selectpicker();
        locationID = $('#idOfLocation').val();
        var documentType = 'deliveryNote';

        $('#itemCode',$currentRow).select2({

            minimumInputLength: 1,
            minimumResultsForSearch: 10,
            ajax: {
                url: BASE_URL + '/productAPI/search-location-products-for-dropdown',
                dataType: "json",
                type: "POST",
                data: function (params) {

                    var queryParameters = {
                        searchKey: params.term,
                        documentType: documentType,
                        locationID: locationID
                    }
                    return queryParameters;
                },
                processResults: function (data) {
                    return {
                        results: $.map(data.data.list, function (item) {
                            return {
                                text: item.text,
                                id: item.value
                            }
                        })
                    };
                }
            },
            placeholder: 'Enter Item Code Or Name',
        });


        $('#itemCode',$currentRow).on('select2:select', function (e) { 
            var data = e.params.data;

            if (data.id > 0 && selectedPID != data.id) {
                selectedPID = data.id;
                eb.ajax({
                    type: 'POST',
                    url: BASE_URL + '/productAPI/get-location-product-details',
                    data: {productID: selectedPID, locationID: locationID},
                    success: function(respond) {
                        selectProductForTransfer(respond.data);
                        var currentElem = new Array();
                        currentElem[selectedPID] = respond.data;
                        console.log(currentElem);
                        locationProducts = $.extend(locationProducts, currentElem);
                    }
                });
            }
            $('#availableQuantity', $currentRow).parent().addClass('input-group');
            $('#deliverQuanity', $currentRow).parent().addClass('input-group');
        });

    }

    function selectProductForTransfer(selectedlocationProduct) {
// check if product is already selected
        if (deliverProducts[selectedlocationProduct.pID] != undefined) {
            p_notification(false, eb.getMessage('ERR_DELI_ALREADY_SELECT_PROD'));
        }
        currentProducts[selectedlocationProduct.pID] = selectedlocationProduct;
        var productID = selectedlocationProduct.pID;
        var productCode = selectedlocationProduct.pC;
        var productType = selectedlocationProduct.pT;
        var productName = selectedlocationProduct.pN;
        var availableQuantity = (selectedlocationProduct.LPQ == null) ? 0 : selectedlocationProduct.LPQ;
        var defaultSellingPrice = parseFloat(selectedlocationProduct.dSP);
        if (selectedlocationProduct.dPR != null) {
            var productDiscountPrecentage = parseFloat(selectedlocationProduct.dPR).toFixed(2);
        } else {
            var productDiscountPrecentage = 0;
        }
        
        if (selectedlocationProduct.dV != null) {
            var productDiscountValue = parseFloat(selectedlocationProduct.dV).toFixed(2);
        } else {
            var productDiscountValue = 0;
    
        }

        if (priceListItems[productID] != undefined && parseFloat(priceListItems[productID].itemPrice) > 0) {
            defaultSellingPrice = priceListItems[productID].itemPrice;
            var itemDiscountType = priceListItems[productID].itemDiscountType;
            productDiscountValue = (itemDiscountType == 1) ? priceListItems[productID].itemDiscount : 0;
            productDiscountPrecentage = (itemDiscountType == 2) ? priceListItems[productID].itemDiscount : 0;
        }
        defaultSellingPriceData[productID] = defaultSellingPrice;

        var baseUom;
        for (var j in selectedlocationProduct.uom) {
            if (selectedlocationProduct.uom[j].pUBase == 1) {
                baseUom = selectedlocationProduct.uom[j].uomID;
            }
        }

        var $currentRow = getAddRow();
        clearProductRow($currentRow);
        $("input[name='discount']", $currentRow).prop('readonly', false);
        $("#itemCode", $currentRow).data('PT', productType);
        $("#itemCode", $currentRow).data('PN', productName);
        $("#itemCode", $currentRow).data('PC', productCode);
        $("input[name='availableQuantity']", $currentRow).val(availableQuantity).prop('readonly', true);
        $("input[name='unitPrice']", $currentRow).val(defaultSellingPrice).addUomPrice(selectedlocationProduct.uom);
        if (selectedlocationProduct.dEL == 1 || (priceListItems[productID] != undefined && parseFloat(priceListItems[productID].itemPrice) > 0)) {
            if (selectedlocationProduct.dV != null) {
                productDiscountValue = parseFloat(productDiscountValue).toFixed(2);
                $("input[name='discount']", $currentRow).val(productDiscountValue).addUomPrice(selectedlocationProduct.uom, baseUom);
                $("input[name='discount']", $currentRow).siblings('.uom-price-select').find('.uom_price_drop_down').attr('disabled', true);
                $("input[name='discount']", $currentRow).addClass('value');
                if ($("input[name='discount']", $currentRow).hasClass('precentage')) {
                    $("input[name='discount']", $currentRow).removeClass('precentage');
                }
                $(".sign", $currentRow).text(companyCurrencySymbol);
            } else if (selectedlocationProduct.dPR != null) {
                productDiscountPrecentage = parseFloat(productDiscountPrecentage).toFixed(2);
                $("input[name='discount']", $currentRow).val(productDiscountPrecentage);
                $("input[name='discount']", $currentRow).addClass('precentage');
                if ($("input[name='discount']", $currentRow).hasClass('value')) {
                    $("input[name='discount']", $currentRow).removeClass('value');
                }
                $(".sign", $currentRow).text('%');
            }
        } else {
            $("input[name='discount']", $currentRow).prop('readonly', true);
        }
        $("input[name='deliverQuanity']", $currentRow).prop('readonly', false);
        $currentRow.data('id', productID);
        $currentRow.data('copied', false);
        // add uom list
        $("input[name='availableQuantity'],input[name='deliverQuanity']", $currentRow).addUom(selectedlocationProduct.uom);
        $('.uomqty').attr("id", "itemQuantity");
        $('.uomPrice').attr("id", "itemUnitPrice");
        var deleted = 0;
        // clear old rows
        $("#batch_data tr:not(.hidden)").remove();
        addBatchProduct(productID, productCode, deleted);
        addSerialProduct(productID, productCode, deleted);
        addSerialBatchProduct(productID, productCode, deleted);        

        //check this product serail or batch
        if (!$.isEmptyObject(selectedlocationProduct.serial)) {
            $('#product-batch-modal .serial-auto-select').removeClass('hidden');
            $('#numberOfRow').val('');
        } else {
            $('#product-batch-modal .serial-auto-select').addClass('hidden');
        }

         // check if any batch / serial products exist
        if ($.isEmptyObject(selectedlocationProduct.batch) &&
                $.isEmptyObject(selectedlocationProduct.serial) &&
                $.isEmptyObject(selectedlocationProduct.batchSerial)) {

            $currentRow.removeClass('subproducts');
            $('.edit-modal', $currentRow).parent().addClass('hidden');
            $("td[colspan]", $currentRow).attr('colspan', 2);
            $("input[name='deliverQuantity']", $currentRow).prop('readonly', false).focus();
        } else {
            $currentRow.addClass('subproducts');
            $('.edit-modal', $currentRow).parent().removeClass('hidden');
            $("td[colspan]", $currentRow).attr('colspan', 1);
            $("input[name='deliverQuantity']", $currentRow).prop('readonly', true);
            $('#addDeliveryNoteProductsModal').modal('show');
        }
        setTaxListForProduct(productID, $currentRow);
        $currentRow.attr('id', 'product' + productID);
    }

    function clearProductRow($currentRow) {
        $("input[name='availableQuantity']", $currentRow).val('').siblings('.uomqty,.uom-select').remove().show();
        $("input[name='deliverQuanity']", $currentRow).val('').siblings('.uomqty,.uom-select').remove().show();
        $("input[name='unitPrice']", $currentRow).val('').siblings('.uomPrice,.uom-price-select').remove().show();
        $("input[name='discount']", $currentRow).val('');
        $(".tempLi", $currentRow).remove('');
        $("#taxApplied", $currentRow).removeClass('glyphicon-check');
        $("#taxApplied", $currentRow).addClass('glyphicon-unchecked');
        $(".uomList", $currentRow).remove('');
    }

    $('#batch-save').on('click', function(e) {
        e.preventDefault();
        // validate batch / serial products before closing modal
        if (!batchModalValidate(e)) {
            e.stopPropagation();
            return false;
        } else {
            $('#addDeliveryNoteProductsModal').modal('hide');
        }

    });

    $('#select_serials').on('click',function(e) {
        var numberOfRow = $('#numberOfRow').val();
        if (numberOfRow != '' || numberOfRow != 0) {
            $('#batch_data > tr').each(function(key, value){
                if (numberOfRow != 0 && !($("input[name='deliverQuantityCheck']", value).is('[disabled=disabled]'))) {
                    $("input[name='deliverQuantityCheck']", value).prop('checked', true);
                    numberOfRow--;
                } else {
                    $("input[name='deliverQuantityCheck']", value).prop('checked', false);
                }
            });
        }    
        
    });

    $('.close').on('click', function() {
        $('#addDeliveryNoteProductsModal').modal('hide');
    });
    function getCurrentProductData($thisRow) {
        $("input.uomqty", $thisRow).change();
        var discountType = '';
        if ($("input[name='discount']", $thisRow).hasClass('value')) {
            discountType = 'value';
        } else if ($("input[name='discount']", $thisRow).hasClass('precentage')) {
            discountType = 'precentage';
        }
        var availableQuantity = $("input[name='availableQuantity']", $thisRow).val();
        if (!availableQuantity) {
            availableQuantity = 0;
        }
        var thisVals = {
            productID: $thisRow.data('id'),
            productCode: $("#itemCode", $thisRow).data('PC'),
            productName: $("#itemCode", $thisRow).data('PN'),
            productPrice: $("input[name='unitPrice']", $thisRow).val(),
            productDiscount: $("input[name='discount']", $thisRow).val(),
            productDiscountType: discountType,
            productTotal: productsTotal[$thisRow.data('id')],
            pTax: productsTax[$thisRow.data('id')],
            productType: $("#itemCode", $thisRow).data('PT'),
            copied: $thisRow.data('copied'),
            availableQuantity: {
                qty: availableQuantity,
            },
            deliverQuantity: {
                qty: $("input[name='deliverQuanity']", $thisRow).val(),
            }
        };

        return thisVals;
    }


    $productTable.on('click', '#add_item, button.save', function(e) {
        e.preventDefault();

        var $thisRow = $(this).parents('tr');
        var thisVals = getCurrentProductData($thisRow);

        $('.itemDescText', $thisRow).prop('disabled', true);
        $("input[name='availableQuantity']", $thisRow).prop('readonly', true);

        if (!$(this).hasClass('save')) {
            if (deliverProducts[thisVals.productID] != undefined) {
                p_notification(false, eb.getMessage('ERR_DELI_ALREADY_SELECT_PROD'));
                $("#itemCode", $thisRow).focus().select();
                $thisRow.remove();
                var $newRow = $($addRowSample.clone()).removeClass('sample hidden').appendTo($('#add-new-item-row', $productTable));
                setProductTypeahead();
                $newRow.find("#itemCode").focus();
                return false;
            }
        }

        if (($('#itemCode', $thisRow).val()) == undefined || ($('#itemCode', $thisRow).val() == '')) {
            p_notification(false, eb.getMessage('ERR_DELI_SELECT_PROD'));
            $("#itemCode", $thisRow).focus().select();
            return false;
        }

        if (thisVals.productType != 2 && (isNaN(parseFloat(thisVals.deliverQuantity.qty)) || parseFloat(thisVals.deliverQuantity.qty) <= 0)) {
            p_notification(false, eb.getMessage('ERR_DELI_ENTER_QUAN'));
            $("input[name='deliverQuantity']", $thisRow).focus().select();
            return false;
        }

        if (thisVals.productType != 2 && (parseFloat(thisVals.deliverQuantity.qty) > parseFloat(thisVals.availableQuantity.qty))) {
            p_notification(false, eb.getMessage('ERR_DELI_QUAN'));
            $("input[name='deliverQuantity']", $thisRow).focus().select();
            return false;

        }

        if (thisVals.productDiscount) {
            if (isNaN(parseFloat(thisVals.productDiscount)) || parseFloat(thisVals.productDiscount) < 0) {
                p_notification(false, eb.getMessage('ERR_DELI_VALID_DISCOUNT'));
                $("input[name='discount']", $thisRow).focus().select();
                return false;
            }
        }

        if (thisVals.productTotal < 0) {
            if (isNaN(parseFloat(thisVals.productTotal)) || parseFloat(thisVals.productTotal) <= 0) {
                p_notification(false, eb.getMessage('ERR_INVO_PROD_TOTAL'));
                $("input[name='unitPrice']", $thisRow).focus().select();
                return false;
            }
        }
        // console.log(thisVals.deliverQuantity.qty);
        // return;
        // if ($thisRow.hasClass('subproducts')) {
        //     if (parseFloat(SubProductsQty[thisVals.productID]) != parseFloat(thisVals.deliverQuantity.qty)) {
        //         p_notification(false, eb.getMessage('ERR_DELI_SUB_PRODUCT_CHECK', thisVals.productCode));
        //         $('.edit-modal', $thisRow).focus();
        //         return false;
        //     }
        // }

        if ($thisRow.hasClass('subproducts')) {
            var seletedProdutQty = 0;
            if (!$.isEmptyObject(deliverSubProducts[thisVals.productID])) {
                $.each(deliverSubProducts[thisVals.productID] , function(i, currSubProduct){
                    seletedProdutQty += currSubProduct.qtyByBase
                });
            }
            if ($.isEmptyObject(deliverSubProducts[thisVals.productID])) {
                p_notification(false, eb.getMessage('ERR_DELI_SUB_PRODUCT_CHECK', thisVals.productCode));
                $('.edit-modal', $thisRow).focus();
                return false;
            }else if(seletedProdutQty != thisVals.deliverQuantity.qty){
                p_notification(false, eb.getMessage('ERR_DELI_SUB_PRODUCT_CHECK_WITH_PQTY', thisVals.productCode));
                $('.edit-modal', $thisRow).focus();
                return false;
            }
        }

// if add button is clicked
        if ($(this).hasClass('add')) {
            var $currentRow = $thisRow;
            var $flag = false;
            if ($currentRow.hasClass('add-row')) {
                $flag = true;
            }
            $currentRow
                    .removeClass('add-row')
                    .find("#itemCode, input[name='deliverQuanity'],input[name='availableQuantity'],input[name='unitPrice'],input[name='discount']").prop('readonly', true).end()
                    .find('td').find('#addNewTax').attr('disabled', true)
                    .find("button.delete").removeClass('disabled');
            $currentRow.find("input[name='discount']").change();
            $currentRow.find("input[name='deliverQuanity']").change();
            $currentRow.find("input[name='unitPrice']").change();
            $currentRow.find("#itemCode").prop('disabled', true);
            if ($flag) {
                var $newRow = $($addRowSample.clone()).removeClass('sample hidden').appendTo($('#add-new-item-row', $productTable));
                setProductTypeahead();
                $newRow.find("#itemCode").focus();
            }
        } else if ($(this).hasClass('save')) {
            var productId = $(this).parent().parent().data('id');
            var $currentRow = getAddRow(productId);
            $currentRow
                    .removeClass('edit-row')
                    .find("#itemCode, input[name='deliverQuanity'],input[name='availableQuantity'],input[name='unitPrice'],input[name='discount']").prop('readonly', true).end()
                    .find('td').find('#addNewTax').attr('disabled', true)
                    .find("button.delete").removeClass('disabled');
            $currentRow.find("input[name='discount']").change();
            $currentRow.find("input[name='deliverQuanity']").change();
            $currentRow.find("input[name='unitPrice']").change();
        } else { // if save button is clicked
            $thisRow.removeClass('edit-row');
            $thisRow.find("input[name='deliverQuanity']").prop('readonly', true);
        }
        // if batch product modal is available for this product
        $(this, $currentRow).parent().find('.edit').removeClass('hidden');
        $(this, $currentRow).parent().find('.add').addClass('hidden');
        $(this, $currentRow).parent().find('.save').addClass('hidden');
        $(this, $currentRow).parent().parent().find('.delete').removeClass('disabled');
        if ($thisRow.hasClass('subproducts')) {
            $('.edit-modal', $currentRow).parent().addClass('hidden');
            $("td[colspan]", $currentRow).attr('colspan', 2);
        }
        deliverProducts[thisVals.productID] = thisVals;
        setGrnTotalCost();
        setTotalTax();
        $('#customCurrencyId').attr('disabled', true);
        $('#priceListId').attr('disabled', true);
    });

    $productTable.on('click', 'button.edit', function(e) {
        var $thisRow = $(this).parents('tr');
        var produtID = $thisRow.data('id');
        $thisRow.addClass('edit-row')
                .find("input[name='deliverQuanity'],input[name='unitPrice'],input[name='discount']").prop('readonly', false).end()
                .find('td').find('#addNewTax').attr('disabled', false)
                .find("button.delete").addClass('disabled');
        if (currentProducts[produtID].dEL != 1) {
            $thisRow.find("input[name='discount']").prop('readonly', true);
        }
        $thisRow.find("input[name='discount']").change();
        $thisRow.find("input[name='deliverQuanity']").change();
        $thisRow.find("input[name='unitPrice']").change();
        $(this, $thisRow).parent().find('.edit').addClass('hidden');
        $(this, $thisRow).parent().find('.save').removeClass('hidden');
        if ($thisRow.hasClass('subproducts')) {
            $("input[name='deliverQuanity']", $thisRow).prop('readonly', true);
            $('.edit-modal', $thisRow).parent().removeClass('hidden');
            $("td[colspan]", $thisRow).attr('colspan', 1);
        } else {
            $("input[name='deliverQuanity']", $thisRow).prop('readonly', false).focus();
        }
    });
    $productTable.on('click', 'button.delete', function(e) {
        e.preventDefault();
        if (!$(this).hasClass('disabled')) {
            var $thisRow = $(this).parents('tr');
            var productID = $thisRow.data('id');
            delete deliverProducts[productID];
            // when all products deleted customcurrency symbal selection will be enable
            // if used any selection from 'start invoice by' then cann't enable
            if (_.size(deliverProducts) == 0 && !$('#salesOrderNo').val()) {
                $('#customCurrencyId').attr('disabled', false);
            }
            setGrnTotalCost();
            setTotalTax();
            selectedPID = '';
            $thisRow.remove();
        }
    });

    function batchModalValidate(e) {
        var productID = $('#addDeliveryNoteProductsModal').data('id');
        productID = (productID == 'undefined')? undefined :productID;
        var $thisParentRow = getAddRow(productID);
        var thisVals = getCurrentProductData($thisParentRow);
        var $batchTable = $("#addDeliveryNoteProductsModal .batch-table tbody");
        var qtyTotal = 0;
        var subProducts = [];
        var deliveryUnitPrice;
        $("input[name='deliverQuantity'], input[name='deliverQuantityCheck']:checked", $batchTable).each(function() {

            var $thisSubRow = $(this).parents('tr');
            var thisDeliverQuantity = $(this).val();
            if ((thisDeliverQuantity).trim() != "" && isNaN(parseFloat(thisDeliverQuantity))) {
                p_notification(false, eb.getMessage('ERR_DELI_ENTER_QUAN'));
                $(this).focus();
                return qtyTotal = false;
            }

            thisDeliverQuantity = (isNaN(parseFloat(thisDeliverQuantity))) ? 0 : parseFloat(thisDeliverQuantity);
            var thisAvailableQuantity = $("input[name='availableQuantity']", $thisSubRow).val();
            if(thisDeliverQuantity != 0){
                deliveryUnitPrice = $("input[name='btPrice']", $thisSubRow).val();
            }
            thisAvailableQuantity = (isNaN(parseFloat(thisAvailableQuantity))) ? 0 : parseFloat(thisAvailableQuantity);
            if (thisDeliverQuantity > thisAvailableQuantity) {
                p_notification(false, eb.getMessage('ERR_DELI_QUAN'));
                $(this).focus();
                return qtyTotal = false;
            }

            var warranty = $thisSubRow.find('#warranty').val();
            if (isNaN(warranty)) {
                p_notification(false, eb.getMessage('ERR_INVO_WARRANTY_PERIOD_SHBE_NUM'));
                $thisSubRow.find('#warranty').focus().select();
                return qtyTotal = false;
            }

            qtyTotal = qtyTotal + thisDeliverQuantity;
            // if a product transfer is present, prepare array to be sent to backend

            if (thisDeliverQuantity > 0) {

                var thisSubProduct = {};
                if ($(".batchCode", $thisSubRow).data('id')) {
                    thisSubProduct.batchID = $(".batchCode", $thisSubRow).data('id');
                }

                if ($(".serialID", $thisSubRow).data('id')) {
                    thisSubProduct.serialID = $(".serialID", $thisSubRow).data('id');
                }

                thisSubProduct.qtyByBase = thisDeliverQuantity;
                thisSubProduct.warranty = $thisSubRow.find('#warranty').val();

                let wrntyType=$thisSubRow.find('#warrantyType').val();
                if(wrntyType=="Years")
                    thisSubProduct.warrantyType =4;
                else if(wrntyType=="Months"){
                    thisSubProduct.warrantyType = 3;
                }else if(wrntyType=="Weeks"){
                    thisSubProduct.warrantyType = 2;
                }else{
                    thisSubProduct.warrantyType = 1;
                }

                subProducts.push(thisSubProduct);
            }


        });
        if (checkSubProduct[thisVals.productID] != undefined) {
            if (qtyTotal != $("input[name='deliverQuanity']", $thisParentRow).val()) {
                p_notification(false, eb.getMessage('ERR_DELI_TOTAL_SUBQUAN'));
                return qtyTotal = false;
            } else {
                checkSubProduct[thisVals.productID] = 1;
            }
        }

        // to break out form $.each and exit function
        if (qtyTotal === false)
            return false;
        // ideally, since the individual batch/serial quantity is checked to be below the available qty,
        // the below condition should never become true
        if (qtyTotal > thisVals.availableQuantity.qty) {
            p_notification(false, eb.getMessage('ERR_DELI_TOTAL_DELIQUAN'));
            return false;
        }
        deliverSubProducts[thisVals.productID] = subProducts;
        SubProductsQty[thisVals.productID] = qtyTotal;
        var $transferQ = $("input[name='deliverQuanity']", $thisParentRow).prop('readonly', true);
        if (checkSubProduct[thisVals.productID] == undefined) {
            $transferQ.val(qtyTotal).change();
        }


       // use to set unit price using batch price
        if(subProducts.length == 1 && subProducts[0].batchID != null && subProducts[0].serialID == null && deliveryUnitPrice != 0 ){
            $("input[name='unitPrice']", $thisParentRow).val(deliveryUnitPrice).change();
        } else if(subProducts[0].batchID != null && subProducts[0].serialID != null){
            var tempBatchIDs = [];
            var sameBatchFlag = true;
            $.each(subProducts, function(key, value){
                tempBatchIDs[key] = value.batchID;
            });
            var tmpBtID;
            $.each(tempBatchIDs, function(key, value){

                if(key == 0){
                    tmpBtID = value;
                }
                if(tmpBtID != value){
                    sameBatchFlag = false;
                }
            });
            if(sameBatchFlag){
                $("input[name='unitPrice']", $thisParentRow).val(deliveryUnitPrice).change();
            } else {
                $("input[name='unitPrice']", $thisParentRow).val(defaultSellingPriceData[thisVals.productID]).change();
            }

        } else {
            $("input[name='unitPrice']", $thisParentRow).val(defaultSellingPriceData[thisVals.productID]).change();
        }

        $("#unitPrice", $thisParentRow).trigger(jQuery.Event("focusout"));
        $('#addDeliveryNoteProductsModal').data('id',"undefined");
        return true;
    }

    $productTable.on('click', 'button.edit-modal', function(e) {
        var productID = $(this).parents('tr').data('id');
        var productCode = $(this).parents('tr').find('#itemCode').val();
        $("#batch_data tr:not(.hidden)").remove();
        addBatchProduct(productID, productCode, 0);
        addSerialProduct(productID, productCode, 0);
        addSerialBatchProduct(productID, productCode, 0);

        //check this product serail or batch
        if (!$.isEmptyObject(currentProducts[productID].serial)) {
            $('#product-batch-modal .serial-auto-select').removeClass('hidden');
            $('#numberOfRow').val('');
        } else {
            $('#product-batch-modal .serial-auto-select').addClass('hidden');
        }
        
        $("tr", '#batch_data').each(function() {
            var $thisSubRow = $(this);
            for (var i in deliverSubProducts[productID]) {
                if (deliverSubProducts[productID][i].serialID != undefined) {
                    if ($(".serialID", $thisSubRow).data('id') == deliverSubProducts[productID][i].serialID) {
                        $("input[name='deliverQuantityCheck']", $thisSubRow).prop('checked', true);
                        $("input[name='warranty']", $thisSubRow).val(deliverSubProducts[productID][i].warranty);
                        $("input[name='warrantyType']", $thisSubRow).val(deliverSubProducts[productID][i].warrantyType);
                    }
                    
                } else if (deliverSubProducts[productID][i].batchID != undefined) {
                    if ($(".batchCode", $thisSubRow).data('id') == deliverSubProducts[productID][i].batchID) {
                        $(this).find("input[name='deliverQuantity']").val(deliverSubProducts[productID][i].qtyByBase).change();
                    }
                }
            }
        });
        $('#addDeliveryNoteProductsModal').data('id', $(this).parents('tr').data('id')).modal('show');
        $('#addDeliveryNoteProductsModal').unbind('hide.bs.modal');
    });
    $('form#deliveryNoteForm').on('submit', function() {
        if ($("tr.edit-row", $productTable).length > 0) {
            p_notification(false, eb.getMessage('ERR_DELI_SAVE_ALLPROD'));
            return false;
        }

        saveDeliveryNote();
        return false;
    });
    function saveDeliveryNote() {
        for (var i in checkSubProduct) {
            var $currentRow = getAddRow(i);
            if ($($currentRow).find('.add').parent('td').hasClass('hidden') && !$($currentRow).find('.edit-modal').parent('td').hasClass('hidden')) {
                if (checkSubProduct[i] == 0) {
                    p_notification(false, eb.getMessage('ERR_DELI_ADD_PROD', deliverProducts[i].productCode));
                    return;
                }
            }
        }
        var grandTotal = 0;
        for (var i in deliverProducts) {
            grandTotal += toFloat(deliverProducts[i].productTotal);
        }
        var formData = {
            deliveryCode: $('#deliveryNoteNo').val(),
            location: $('#currentLocation').val(),
            date: $('#deliveryDate').val(),
            customer: $('#customer option:selected').text(),
            deliveryAddress: $('#deliveryNoteAddress').val(),
            customerID: customerID,
        };
        if (validateTransferForm(formData)) {
            eb.ajax({
                type: 'POST',
                url: BASE_URL + '/delivery-note-api/saveDeliverDetails',
                data: {
                    deliverCode: formData.deliveryCode,
                    locationOutID: locationOut,
                    products: deliverProducts,
                    subProducts: deliverSubProducts,
                    date: formData.date,
                    customer: customerID,
                    customer_prof_ID: cusProfID,
                    customerName: $('#customer option:selected').text(),
                    deliveryCharge: $('#deliveryCharge').val(),
                    salesOrderID: salesOrderID,
                    deliveryTotalPrice: grandTotal,
                    deliveryAddress: formData.deliveryAddress,
                    deliveryComment: $('#comment').val(),
                    salesPersonID: $('#salesPersonID').val(),
                    customCurrencyId: $('#customCurrencyId').val(),
                    customCurrencyRate: customCurrencyRate,
                    priceListId: priceListId,
                    dimensionData: dimensionData,
                    ignoreBudgetLimit: ignoreBudgetLimitFlag
                },
                success: function(respond) {
                    if (respond.status) {
                        p_notification(respond.status, respond.msg);
                        var fileInput = document.getElementById('documentFiles');
                        if(fileInput.files.length > 0) {
                            var form_data = false;
                            if (window.FormData) {
                                form_data = new FormData();
                            }
                            form_data.append("documentID", respond.data.deliveryNoteID);
                            form_data.append("documentTypeID", 4);
                            
                            for (var i = 0; i < fileInput.files.length; i++) {
                                form_data.append("files[]", fileInput.files[i]);
                            }

                            eb.ajax({
                                url: BASE_URL + '/store-files',
                                type: 'POST',
                                processData: false,
                                contentType: false,
                                data: form_data,
                                success: function(res) {
                                }
                            });
                        }

                        documentPreview(BASE_URL + '/delivery-note/preview/' + respond.data.deliveryNoteID, 'documentpreview', "/delivery-note-api/send-email", function($preview) {
                            $preview.on('hidden.bs.modal', function() {
                                if (!$("#preview:visible").length) {
                                    window.location.reload();
                                }
                            });
                        });
                    } else {
                        if (respond.data == "NotifyBudgetLimit") {
                            bootbox.confirm(respond.msg + ' ,Are you sure you want to save this delivery note ?', function(result) {
                                if (result == true) {
                                    ignoreBudgetLimitFlag = true;
                                    saveDeliveryNote();
                                } else {
                                    setTimeout(function(){ 
                                        location.reload();
                                    }, 3000);
                                }
                            });
                        } else {
                            p_notification(respond.status, respond.msg);
                        }
                    }
                }
            });
        }
    }

    $('table#deliveryNoteProductTable>tbody').on('keypress', 'input#itemQuantity, input#itemUnitPrice, input#unitPrice, input#deliveryNoteDiscount, #deliverQuanity,#availableQuantity ', function(e) {

        if (e.which == 13) {
            $('#add_item', $(this).parents('tr.add-row')).trigger('click');
            e.preventDefault();
        }

    });

    if ($(this).parents('tr').find('#itemCode').val() != '') {
        if ($(this).val().indexOf('.') > 0) {
            decimalPoints = $(this).val().split('.')[1].length;
        }
        if (this.id == 'unitPrice' && (!$.isNumeric($(this).parents('tr').find('#unitPrice').val()) || $(this).parents('tr').find('#unitPrice').val() < 0 || decimalPoints > 2)) {
            decimalPoints = 0;
            $(this).parents('tr').find('#unitPrice').val('');
        }
        if ((this.id == 'deliverQuanity') && (!$.isNumeric($(this).parents('tr').find('#deliverQuanity').val()) || $(this).parents('tr').find('#deliverQuanity').val() < 0)) {
            decimalPoints = 0;
            $(this).parents('tr').find('#deliverQuanity').val('');
        }
        if ((this.id == 'deliveryNoteDiscount')) {
            if ($(this).hasClass('precentage') && (!$.isNumeric($(this).parents('tr').find('#deliveryNoteDiscount').val()) || $(this).parents('tr').find('#deliveryNoteDiscount').val() < 0 || $(this).parents('tr').find('#deliveryNoteDiscount').val() >= 100 || (decimalPoints > 2))) {
                decimalPoints = 0;
                $(this).parents('tr').find('#deliveryNoteDiscount').val('');
            } else {
                decimalPoints = 0;
            }
        }
    }

    $productTable.on('focusout', '#deliverQuanity,#unitPrice,#deliveryNoteDiscount,.uomqty,.uomPrice', function() {
        var $thisRow = $(this).parents('tr');
        var productID = $thisRow.data('id');
        var checkedTaxes = Array();
        $thisRow.find('input:checkbox.addNewTaxCheck').each(function() {
            if (this.checked) {
                var cliTID = this.id;
                var tTaxID = cliTID.split('_')[1];
                checkedTaxes.push(tTaxID);
            }
        });
        var tmpItemQuentity = $(this).parents('tr').find('#deliverQuanity').val();
        var productType = $(this).parents('tr').find("#itemCode").data('PT');
        if (productType == 2 && tmpItemQuentity == 0) {
            tmpItemQuentity = 1;
        }
        var tmpItemTotal = (toFloat($(this).parents('tr').find('#unitPrice').val()) * toFloat(tmpItemQuentity));
        var tmpItemCost = tmpItemTotal;
        if ($(this).parents('tr').find('#deliveryNoteDiscount').hasClass('value')) {
            var discountType = "Val";
            var discount = $(this).parents('tr').find('#deliveryNoteDiscount').val();
            var unitPrice = $(this).parents('tr').find('#unitPrice').val();
            var tmpPDiscountvalue = validateDiscount(productID,discountType,discount,unitPrice);
            if (tmpPDiscountvalue >= 0) {
                tmpItemCost -= tmpPDiscountvalue * tmpItemQuentity;
            }
        } else {
            var discountType = "Per";
            var discount = $(this).parents('tr').find('#deliveryNoteDiscount').val();
            var tmpPDiscount = validateDiscount(productID,discountType,discount);
            if (tmpPDiscount >= 0) {
                tmpItemCost -= (tmpItemTotal * toFloat(tmpPDiscount) / toFloat(100));
            }
        }
        if (checkedTaxes) {
            currentItemTaxResults = calculateItemCustomTax(tmpItemCost, checkedTaxes);
            currentTaxAmount = (currentItemTaxResults == null) ? null : currentItemTaxResults.tTA;
        } else {
            currentTaxAmount = 0;
            currentItemTaxResults = '';
        }
        var itemCost = toFloat(tmpItemCost) + toFloat(currentTaxAmount);
        productsTotal[productID] = itemCost;
        productsTax[productID] = currentItemTaxResults;
        //if unit price is zero or discount greater than unit price,Then total may be negative value
        //So Total should be zero
//        if (itemCost < 0) {
//            itemCost = 0;
//        }
        $(this).parents('tr').find('#addNewTotal').html(accounting.formatMoney(itemCost));
        if (productsTax[productID] != null) {
            if (jQuery.isEmptyObject(productsTax[productID].tL)) {
                $(this).parents('tr').find('#taxApplied').addClass('glyphicon glyphicon-unchecked');
            } else {
                $(this).parents('tr').find('#taxApplied').removeClass('glyphicon glyphicon-unchecked');
                $(this).parents('tr').find('#taxApplied').addClass('glyphicon glyphicon-check');
            }
        }
    });
    $productTable.on('change', '.taxChecks.addNewTaxCheck', function() {
        $(this).parents('tr').find("#unitPrice").trigger(jQuery.Event("focusout"));
    });
    $productTable.on('click', '#selectAll', function() {
        $(this).parents('#addTaxUl').find('input:checkbox.addNewTaxCheck').each(function() {
            if (!this.checked) {
                this.checked = true;
            }
        });
         $(this).parents('tr').find("#unitPrice").trigger(jQuery.Event("focusout"));
    });

    $productTable.on('click', '#deselectAll', function() {
        $(this).parents('#addTaxUl').find('input:checkbox.addNewTaxCheck').each(function() {
            if (this.checked) {
                this.checked = false;
            }
        });
         $(this).parents('tr').find("#unitPrice").trigger(jQuery.Event("focusout"));
    });

    $productTable.on('click', 'ul.dropdown-menu', function(e) {
        e.stopPropagation();
    });
    function validateTransferForm(formData) {
        var selectedid = "#" + document.activeElement.id;
        var tableId = $(selectedid).closest('table').attr("id");

        if (formData.location == null || formData.location == "") {
            p_notification(false, eb.getMessage('ERR_DELI_SELECT_LOCAT'));
            $('#currentLocation').focus();
            return false;
        } else if (formData.deliveryCode == null || formData.deliveryCode == "") {
            p_notification(false, eb.getMessage('ERR_DELI_NUM_NOTBLANK'));
            return false;
        } else if (formData.customer == null || formData.customer == "") {
            p_notification(false, eb.getMessage('ERR_DELI_SELECT_CUST'));
            $('#customer').focus();
            return false;
        } else if (formData.date == null || formData.date == "") {
            p_notification(false, eb.getMessage('ERR_DELI_DATE_NOTBLANK'));
            $('#deliveryDate').focus();
            return false;
        } else if (formData.customer == null || formData.customer == "") {
            p_notification(false, eb.getMessage('ERR_DELI_SELECT_CUST'));
            $('#customer').focus();
            return false;
        } else if (formData.customerID == null || formData.customerID == "") {
            p_notification(false, eb.getMessage('ERR_INVOICE_INVALID_CUST'));
            $('#customer').focus();
            return false;
        } else if (formData.deliveryAddress == null || formData.deliveryAddress == "") {
            p_notification(false, eb.getMessage('ERR_DELI_ADDR_NOTBLANK'));
            $('#deliveryNoteAddress').focus();
            return false;
        } else if (jQuery.isEmptyObject(deliverProducts)) {
            if (tableId != "deliveryNoteProductTable") {
                p_notification(false, eb.getMessage('ERR_DELI_ADD_ATLEAST_ONEPROD'));
            }
            return false;
        } else if ((tableId == "deliveryNoteProductTable") && ((selectedid != '#showTax') || (selectedid != '#deliveryChargeEnable') || (selectedid != '#deliveryCharge'))) {
            return  true;
        }
        return true;
    }

    function isDate(txtDate)
    {
        var currVal = txtDate;
        if (currVal == '')
            return false;
        //Declare Regex
        var rxDatePattern = /^(\d{4})(\/|-|.)(\d{1,2})(\/|-|.)(\d{1,2})$/;
        var dtArray = currVal.match(rxDatePattern); // is format OK?

        if (dtArray == null)
            return false;
        //Checks for mm/dd/yyyy format.
        dtMonth = dtArray[3];
        dtDay = dtArray[5];
        dtYear = dtArray[1];
        if (dtMonth < 1 || dtMonth > 12)
            return false;
        else if (dtDay < 1 || dtDay > 31)
            return false;
        else if ((dtMonth == 4 || dtMonth == 6 || dtMonth == 9 || dtMonth == 11) && dtDay == 31)
            return false;
        else if (dtMonth == 2) {
            var isleap = (dtYear % 4 == 0 && (dtYear % 100 != 0 || dtYear % 400 == 0));
            if (dtDay > 29 || (dtDay == 29 && !isleap))
                return false;
        }
        return true;
    }

    /**
     * Inside Batch / Serial Product modal
     */

    function addBatchProduct(productID, productCode, deleted) {
        if ((!$.isEmptyObject(currentProducts[productID].batch))) {
            batchProduct = currentProducts[productID].batch;
            for (var i in batchProduct) {
                if (deleted != 1 && batchProduct[i].PBQ != 0) {
                    if ((!$.isEmptyObject(currentProducts[productID].productIDs))) {
                        if (currentProducts[productID].productIDs[i]) {
                            continue;
                        }
                    }

                    var batchKey = productCode + "-" + batchProduct[i].PBID;
                    var $clonedRow = $($('.batch_row').clone());
                    $clonedRow
                            .data('id', i).removeAttr('id')
                            .removeClass('hidden batch_row')
                            .addClass('remove_row');
                    $(".batchCode", $clonedRow).html(batchProduct[i].PBC).data('id', batchProduct[i].PBID);
                    $("input[name='availableQuantity']", $clonedRow).val(batchProduct[i].PBQ);
                    if (batchProduct[i].PBQ == 0) {
                        $("input[name='deliverQuantity']", $clonedRow).prop('disabled', true);
                    }
                    $("input[name='deliverQuantityCheck']", $clonedRow).remove();
                    $("input[name='warranty']", $clonedRow).prop('readonly', true);
                    $("input[name='warranty']", $clonedRow).val(batchProduct[i].PBWoD);
                    $("input[name='expireDate']", $clonedRow).val(batchProduct[i].PBExpD);
                    $("input[name='btPrice']", $clonedRow).val(parseFloat(batchProduct[i].PBUP).toFixed(2));
                    if (priceListItems[productID] != undefined && parseFloat(priceListItems[productID].itemPrice) > 0) {
                        $("input[name='btPrice']", $clonedRow).val(parseFloat(priceListItems[productID].itemPrice).toFixed(2));
                    }
                    if (batchProducts[batchKey]) {
                        $("input[name='deliverQuantity']", $clonedRow).val(batchProducts[batchKey].Qty);
                    }
                    if(!(batchProduct[i].PBExpD == null || batchProduct[i].PBExpD == '0000-00-00')){
                    	var expireCheck = checkEpireData(batchProduct[i].PBExpD);
                    	if(expireCheck){
                    		$("input[name='deliverQuantity']", $clonedRow).attr('disabled',true);
                    	}
                    }
                    $clonedRow.insertBefore('.batch_row');
                    $("input[name='deliverQuantity'],input[name='availableQuantity']", $clonedRow).addUom(currentProducts[productID].uom);
                    $("input[name='btPrice']", $clonedRow).addUomPrice(currentProducts[productID].uom);
                } else {
                    batchProducts[batchKey] = undefined;
                }
            }
        }
    }

    function addSerialProduct(productID, productCode, deleted) {
        if ((!$.isEmptyObject(currentProducts[productID].serial))) {
            productSerial = currentProducts[productID].serial;
            for (var i in productSerial) {
                if (deleted != 1) {
                    var serialKey = productCode + "-" + productSerial[i].PSID;
                    var serialQty = (productSerial[i].PSS == 1) ? 0 : 1;
                    var $clonedRow = $($('.batch_row').clone());
                    $clonedRow
                            .data('id', i).removeAttr('id')
                            .removeClass('hidden batch_row')
                            .addClass('remove_row');
                    $(".serialID", $clonedRow)
                            .html(productSerial[i].PSC)
                            .data('id', productSerial[i].PSID);
                    $("input[name='availableQuantity']", $clonedRow).val(serialQty);
                    $("input[name='warranty']", $clonedRow).val(productSerial[i].PSWoD);
                    let wPeriod = "";
                    if(productSerial[i].PSWoT==='4'){
                        wPeriod = "Years"
                    }else if(productSerial[i].PSWoT==='3'){
                        wPeriod = "Months"
                    }else if(productSerial[i].PSWoT==='2'){
                        wPeriod = "Weeks"
                    }else{
                        wPeriod = "Days"
                    }
                    $("input[name='warrantyType']",$clonedRow).val(wPeriod).attr('disabled',true);

                    $("input[name='expireDate']", $clonedRow).val(productSerial[i].PSExpD);
                    if(!(productSerial[i].PSExpD == null || productSerial[i].PSExpD == '0000-00-00')){
                    	var expireCheck = checkEpireData(productSerial[i].PSExpD);
                    	if(expireCheck){
                    		$("input[name='deliverQuantityCheck']", $clonedRow).attr('disabled',true);
                    	}
                    }
                    if (serialQty = 0) {
                        $("input[name='deliverQuantityCheck']", $clonedRow).prop('disabled', true);
                    }
                    $("input[name='deliverQuantity']", $clonedRow).parent().remove();
                    if (serialProducts[serialKey]) {
                        if (serialProducts[serialKey].Qty == 1) {
                            $("input[name='deliverQuantityCheck']", $clonedRow).attr('checked', true);
                        }
                    }

                    $clonedRow.insertBefore('.batch_row');
                    $("input[name='deliverQuantity'],input[name='availableQuantity']", $clonedRow).addUom(currentProducts[productID].uom);
                } else {
                    serialProducts[serialKey] = undefined;
                }
            }
        }
    }

    function addSerialBatchProduct(productID, productCode, deleted) {
        if ((!$.isEmptyObject(currentProducts[productID].batchSerial))) {
            productBatchSerial = currentProducts[productID].batchSerial;
            for (var i in productBatchSerial) {
                if (deleted != 1) {
                    var serialKey = productCode + "-" + productBatchSerial[i].PBID;
                    var serialQty = (productBatchSerial[i].PSS == 1) ? 0 : 1;
                    var $clonedRow = $($('.batch_row').clone());
                    $clonedRow
                            .data('id', i).removeAttr('id')
                            .removeClass('hidden batch_row')
                            .addClass('remove_row');
                    $(".serialID", $clonedRow)
                            .html(productBatchSerial[i].PSC)
                            .data('id', productBatchSerial[i].PSID);
                    $(".batchCode", $clonedRow)
                            .html(productBatchSerial[i].PBC)
                            .data('id', productBatchSerial[i].PBID);
                    $("input[name='availableQuantity']", $clonedRow).val(serialQty);
                    $("input[name='warranty']", $clonedRow).val(productBatchSerial[i].PBSWoD);
                    if (serialQty = 0) {
                        $("input[name='deliverQuantityCheck']", $clonedRow).prop('disabled', true);
                    }
                    $("input[name='deliverQuantity']", $clonedRow).parent().remove();
                    $("input[name='expireDate']", $clonedRow).val(productBatchSerial[i].PBSExpD);
                    $("input[name='btPrice']", $clonedRow).val(parseFloat(productBatchSerial[i].PBUP).toFixed(2));
                    if (priceListItems[productID] != undefined && parseFloat(priceListItems[productID].itemPrice) > 0) {
                        $("input[name='btPrice']", $clonedRow).val(parseFloat(priceListItems[productID].itemPrice).toFixed(2));
                    }
                    if(!(productBatchSerial[i].PBSExpD == null || productBatchSerial[i].PBSExpD == '0000-00-00')){
                    	var expireCheck = checkEpireData(productBatchSerial[i].PBSExpD);
                    	if(expireCheck){
                    		$("input[name='deliverQuantityCheck']", $clonedRow).attr('disabled',true);
                    	}
                    }
                    if (batchProducts[serialKey]) {
                        if (batchProducts[serialKey].Qty == 1) {
                            $("input[name='deliveryQuantityCheck']", $clonedRow).attr('checked', true);
                        }
                    }

                    $clonedRow.insertBefore('.batch_row');
                    $("input[name='deliverQuantity'],input[name='availableQuantity']", $clonedRow).addUom(currentProducts[productID].uom);
                } else {
                    batchProducts[serialKey] = undefined;
                }
            }
        }
    }

    function checkEpireData(ExpireDate){
    	var today = new Date();
    	var exDate = new Date(ExpireDate);

    	var ctoday = Date.UTC(today.getFullYear(), today.getMonth()+1, today.getDate());
      	var cexDate = Date.UTC(exDate.getFullYear(), exDate.getMonth()+1, exDate.getDate());

    	var flag = true;
    	if(cexDate >= ctoday){
    		flag = false;
    	}
    	return flag;
    }

    function clearProductScreen() {
        products = {};
        setGrnTotalCost();
        setTotalTax();
        $('.addedProducts').remove();
        locProductCodes = '';
        locProductNames = '';
        selectedProduct = '';
        selectedProductQuantity = '';
        selectedProductUom = '';
        batchCount = '';
    }
    function setGrnTotalCost() {
        var grnTotal = 0;
        for (var i in deliverProducts) {
            grnTotal += toFloat(deliverProducts[i].productTotal);
        }
        if (grnTotal < 0) {
            grnTotal = 0;
        }
        $('#subtotal').html(accounting.formatMoney(grnTotal));
        if ($('#deliveryChargeEnable').is(':checked')) {
            var deliveryAmount = $('#deliveryCharge').val();
            grnTotal += toFloat(deliveryAmount);
        }
        $('#finaltotal').html(accounting.formatMoney(grnTotal));
    }

    function setTotalTax() {
        totalTaxList = {};
        if (TAXSTATUS) {
            for (var k in deliverProducts) {
                for (var l in deliverProducts[k].pTax.tL) {
                    if (totalTaxList.hasOwnProperty(l)) {
                        var curTA = totalTaxList[l].tA;
                        var newTA = (toFloat(curTA) + toFloat(deliverProducts[k].pTax.tL[l].tA));
                        totalTaxList[l].tA = newTA.toFixed(2);
                    } else {
                        totalTaxList[l] = {tN: deliverProducts[k].pTax.tL[l].tN, tP: deliverProducts[k].pTax.tL[l].tP, tA: deliverProducts[k].pTax.tL[l].tA};
                    }
                }
            }

            var totalTaxHtml = "";
            for (var t in totalTaxList) {
                totalTaxHtml += totalTaxList[t].tN + "(" + totalTaxList[t].tP + "%)&nbsp" + accounting.formatMoney(totalTaxList[t].tA) + "<br>";
            }
            $('#totalTaxShow').html(totalTaxHtml);
        }
    }

    function setTaxListForProduct(productID, $currentRow) {
        if ((!jQuery.isEmptyObject(currentProducts[productID].tax))) {
            productTax = currentProducts[productID].tax;
            $currentRow.find('#taxApplied').removeClass('glyphicon glyphicon-unchecked');
            $currentRow.find('#taxApplied').addClass('glyphicon glyphicon-check');
            $currentRow.find('.tempLi').remove();
            $currentRow.find('#addNewTax').attr('disabled', false);
            for (var i in productTax) {
                var clonedLi = $($currentRow.find('#sampleLi').clone()).removeClass('hidden').attr('id', 'li_' + i).addClass("tempLi");
                clonedLi.children(".taxChecks").attr('id', productID + '_' + i).prop('checked', true).addClass('addNewTaxCheck');
                if (productTax[i].tS == 0) {
                    clonedLi.children(".taxChecks").attr('disabled', 'disabled');
                    clonedLi.children(".taxName").addClass('crossText');
                }
                clonedLi.children(".taxName").html('&nbsp&nbsp' + productTax[i].tN + '&nbsp&nbsp' + productTax[i].tP + '%').attr('for', productID + '_' + i);
                clonedLi.insertBefore($currentRow.find('#sampleLi'));
            }
        } else {
            $('#taxApplied').addClass('glyphicon glyphicon-unchecked');
            $currentRow.find('#addNewTax').attr('disabled', true);
        }

    }

    $('#deliveryNoteCancel').on('click', function() {
        window.location.reload();
    });

    $('#showTax').on('click', function() {
        if (this.checked) {
            if (!jQuery.isEmptyObject(deliverProducts)) {
                $('#totalTaxShow').removeClass('hidden');
            } else {
                p_notification(false, eb.getMessage('ERR_GRN_PROD_TAX'));
            }

        } else {
            $('#totalTaxShow').addClass('hidden');
        }

    });
    $('#deliveryChargeEnable').on('click', function() {
        setGrnTotalCost();
        if (this.checked) {
            $('.deliCharges').removeClass('hidden');
            $('#deliveryCharge').focus();
        }
        else {
            $('#deliveryCharge').val(0);
            $('.deliCharges').addClass('hidden');
        }
    });
    $('#deliveryCharge').on('change keyup', function() {
        if (!$.isNumeric($('#deliveryCharge').val()) || $(this).val() < 0) {
            $(this).val('');
            setGrnTotalCost();
        } else {
            setGrnTotalCost();
        }
    });
    var nowTemp = new Date();
    var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);
    var checkin = $('#deliveryDate').datepicker({onRender: function(date) {
            return (date.valueOf() > now.valueOf() ? ((U_DATE_R_OVERRIDE) ? '' : 'disabled') : '');
        },
        //format: 'yyyy-mm-dd'
    }).on('changeDate', function(ev) {
        checkin.hide();
    }).data('datepicker');
    checkin.setValue(now);
    function resetDeliveryNotePage() {
        clearProductScreen();
        deliverProducts = {};
        deliverSubProducts = {};
        $('#salesPersonID').attr('disabled', false);
        $('#salesPersonID').selectpicker('render');
        $('#add-new-item-row').find('tr').each(function() {
            if (!$(this).hasClass('add-row')) {
                $(this).remove();
            }
        });
        $('#subtotal').val('');
        $('#finaltotal').val('');
        $('#totalTaxShow').html('');
        $('#priceListId').val('').attr('disabled', false).selectpicker('refresh');
        $('#priceListId').trigger('change');
    }


    $('#customCurrencyId').on('change', function() {
        eb.ajax({
            type: 'POST',
            url: BASE_URL + '/currencySymbol/getCustomCurrencyByCustomCurrencyId',
            data: {customCurrencyId: $(this).val()},
            success: function(respond) {
                if (respond.status == true) {
                    $('.cCurrency').text(respond.data.currencySymbol);
                }
            }
        });
    });

    $('#priceListId').on('change', function() {
        priceListId = $(this).val();
        eb.ajax({
            type: 'POST',
            url: BASE_URL + '/currencySymbol/getPriceListWithDiscount',
            data: {priceListId: priceListId},
            success: function(respond) {
                if (respond.status == true) {
                    priceListItems = respond.data;
                } else {
                    priceListItems = [];
                }
            }
        });
    });

});

function getCustomerProfilesDetails(customerID) {
    eb.ajax({
        type: 'POST',
        url: BASE_URL + '/customerAPI/getCustomerProfilesByCustomerID',
        data: {customerID: customerID},
        success: function(respond) {
            if (respond.status == true) {
                $('.cus_prof_div').removeClass('hidden');
                $('.cus-prof-select').removeClass('hidden');
                $('.default-cus-prof').addClass('hidden');
                $('.tooltip_for_default_cus').removeClass('hidden');
                setCustomerProfilePicker(respond.data['customerProfileData']);
            } else {
                $('.cus_prof_div').addClass('hidden');
                $('.cus-prof-select').addClass('hidden');
                $('.default-cus-prof').removeClass('hidden');
                $('.tooltip_for_default_cus').addClass('hidden');
            }
        }
    });
}

function setCustomerProfilePicker(data) {
    $('#cusProfileDN').html("<option value=''>" + "Select a Customer Profile" + "</option>");
    $.each(data, function(index, value) {
        $('#cusProfileDN').append("<option value='" + index + "'>" + value['profName'] + "</option>");
        if (value['isPrimary'] == 1) {
            $('#cusProfileDN').html("<option value='" + index + "'>" + value['profName'] + "</option>");
            cusProfID = index;
        }
    });
    $('#cusProfileDN').selectpicker('refresh');

    $('#cusProfileDN').on('change', function(e) {
        e.preventDefault();
        cusProfID = $(this).val();

    });
}

function validateDiscount(productID, discountType, currentDiscount, unitPrice) {
    var defaultProductData = locationProducts[productID];
    var newDiscount = 0;
    if (defaultProductData.dEL == 1) {
        if (discountType == 'Val') {
            if (toFloat(defaultProductData.dV) !=0 && toFloat(currentDiscount) > toFloat(defaultProductData.dV)) {
                    newDiscount = toFloat(defaultProductData.dV);
                    p_notification(false, eb.getMessage('ERR_INVO_DISCOUNT_VALUE'));
            } else if (toFloat(defaultProductData.dV) == 0) {
                if (toFloat(currentDiscount) > toFloat(unitPrice)) {
                    newDiscount = toFloat(unitPrice);
                } else {
                    newDiscount = toFloat(currentDiscount);    
                }
            } else {
                newDiscount = toFloat(currentDiscount);
            }

        } else {
            if (toFloat(defaultProductData.dPR) != 0 && toFloat(currentDiscount) > toFloat(defaultProductData.dPR)) {
                    newDiscount = toFloat(defaultProductData.dPR);
                    p_notification(false, eb.getMessage('ERR_INVO_DISC_PERCENTAGE'));
            } else if (toFloat(defaultProductData.dPR) == 0) {
                if (toFloat(currentDiscount) > 100 ) {
                    newDiscount = 100;
                    p_notification(false, eb.getMessage('ERR_PI_DISC_PERCENTAGE'));   
                } else {
                    newDiscount = toFloat(currentDiscount);    
                }
            } else {
                newDiscount = toFloat(currentDiscount);
            }
        }
    } 
    return newDiscount;
}
