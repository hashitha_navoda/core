var InvoiceID;
var ReturnInvoiceID;
var creditNoteProducts = {};
var batchProducts = {};
var productsTax = {};
var productsTotal = {};
var creditNoteSubProducts = {};
var checkSubProduct = {};
var SubProductsQty = {};
var customerID = '';
var customCurrencyRate = 1;
var copiedCreditNoteDiscountAmount = 0;
var copiedCrditNotePromotionDiscountAmount = 0;
var invoiceDate;
var selectedPID;
var productBeingEdited = [];
var currentProducts = [];
var comCurrSyml;
var invCloseFlag = false;
var creditNoteDirectType = false;
var creditNoteBuyBackType = false;
var ignoreBudgetLimitFlag = false;
var locationProducts;
var serialProducts = {};
var batchCount = 0;
var currentlySelectedProduct;
var selectedProduct;
var selectedProductQuantity;
var selectedProductUom;
var productLineTotal;
var dimensionData = {};
var useInvoiceReturn = false;

$(document).ready(function() {
    var directCreditProducts = new Array();
    var directBatchProducts = {};
    var directSerialProducts = {};
    var totalTaxList = {};
    var currentItemTaxResults;
    var $productTable = $("#productTable");
    var $directPTable = $('#directCNProductTable');
    var $addRowSample = $('#creditNoteProductTable tr.add-row.sample.hidden', $productTable);
    var invFlag = false;
    var dimensionArray = {};
    var dimensionTypeID = null;

    $('#dimensionDiv').addClass('hidden');   
    locationID = $('#idOfLocation').val();
    loadDropDownFromDatabase('/productAPI/search-location-products-for-dropdown', locationID, 0, '#item_code', '', '', 'purchaseInvoice');
    loadDropDownFromDatabase('/customerAPI/search-customers-for-dropdown', "", 'Invoice', '#directCustomer');
    $('#item_code').selectpicker('refresh');
    // $('#directCustomer').selectpicker('refresh');
    
    
    var getAddRow = function(invoiceProductID) {

        if (invoiceProductID != undefined) {

            var $row = $('table.creditNoteProductTable > tbody > tr', $productTable).filter(function() {
                return $(this).data("dPID") == invoiceProductID;
            });

            return $row;
        }
        return $('tr.add-row:not(.sample)', $productTable);
    };
    $('#directCreditNote').on('click', function(){

        if ($('#buyBackCreditNote').is(':checked') && $(this).is(':checked')) {
            $('#buyBackCreditNote').checked = false;
            $('#buyBackCreditNote').trigger('click');
        }


        if($(this).is(':checked')){
            var locationProducts = {};
            //if this is direct credit note, then need to frees invoice select fields.
            clearProductDataForNewCreditNote();
            $('#get-invoice-select').attr('disabled',true);
            $('#invoiceNo').attr('disabled', true);
            $('#invoiceNoForReturn').attr('disabled', true);
            $('#get-invoice-return-data').attr('disabled', true);
            $('#inactiveItmsFlag').attr('disabled', true);
            $('.row direct-credit').find('.directReturnProductTable').removeClass('hidden');
            $('#creditNoteProductTable').addClass('hidden');
            $('.direct-credit').removeClass('hidden');
            $('.normal-credit').addClass('hidden');
            $('#item_code', $addRowSample).selectpicker();
            $('.add-row-direct').removeClass('hidden');
            $('#drcCus').removeClass('hidden');
            $('#norCus').addClass('hidden');
            creditNoteDirectType = true;
            $currentRow = $('#add-new-item-row-direct');
            $('#directCredInv').addClass('hidden');
            $('#closeInvoTick').addClass('hidden');
            $('#dimensionDiv').removeClass('hidden');
            var nowTemp = new Date();
            var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);
            var checkin = $('#creditNoteDate').datepicker({onRender: function(date) {
                    return (date.valueOf() > now.valueOf() ? ((U_DATE_R_OVERRIDE) ? '' : 'disabled') : '');
                },
                //format: 'yyyy-mm-dd'
            }).on('changeDate', function(ev) {
                checkin.hide();
            }).data('datepicker');
            checkin.setValue(now);


        } else {
            $('#inactiveItmsFlag').attr('disabled', false);
            $('#get-invoice-select').attr('disabled',false);
            $('#invoiceNo').attr('disabled', false);
            $('#creditNoteProductTable').removeClass('hidden');
            $('#invoiceNoForReturn').attr('disabled', false);
            $('#get-invoice-return-data').attr('disabled', false);
            // $('#directCreditNoteProductTable').addClass('hidden');
            $('.direct-credit').addClass('hidden');
            $('.normal-credit').removeClass('hidden');
            $('#drcCus').addClass('hidden');
            $('#norCus').removeClass('hidden');
            creditNoteDirectType = false;
            $('#dimensionDiv').addClass('hidden');
            $('#directCredInv').addClass('hidden');
            $('#closeInvoTick').addClass('hidden');
        }
    });

    $('#buyBackCreditNote').on('click', function(){


        if ($('#directCreditNote').is(':checked') && $(this).is(':checked')) {
            $('#directCreditNote').checked = false;
            $('#directCreditNote').trigger('click');
        }


        if($(this).is(':checked')){
            var locationProducts = {};
            //if this is direct credit note, then need to frees invoice select fields.
            clearProductDataForNewCreditNote();
            $('#get-invoice-select').attr('disabled',true);
            $('#invoiceNo').attr('disabled', true);
            $('#inactiveItmsFlag').attr('disabled', true);
            $('.row direct-credit').find('.directReturnProductTable').removeClass('hidden');
            $('#creditNoteProductTable').addClass('hidden');
            $('.direct-credit').removeClass('hidden');
            $('.normal-credit').addClass('hidden');
            $('#item_code', $addRowSample).selectpicker();
            $('.add-row-direct').removeClass('hidden');
            $('#drcCus').removeClass('hidden');
            $('#norCus').addClass('hidden');
            creditNoteBuyBackType = true;
            $currentRow = $('#add-new-item-row-direct');
            $('#directCredInv').addClass('hidden');
            $('#closeInvoTick').addClass('hidden');
            $('#dimensionDiv').removeClass('hidden');
            var nowTemp = new Date();
            var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);
            var checkin = $('#creditNoteDate').datepicker({onRender: function(date) {
                    return (date.valueOf() > now.valueOf() ? ((U_DATE_R_OVERRIDE) ? '' : 'disabled') : '');
                },
                //format: 'yyyy-mm-dd'
            }).on('changeDate', function(ev) {
                checkin.hide();
            }).data('datepicker');
            checkin.setValue(now);


        } else {
            $('#inactiveItmsFlag').attr('disabled', false);
            $('#get-invoice-select').attr('disabled',false);
            $('#invoiceNo').attr('disabled', false);
            $('#creditNoteProductTable').removeClass('hidden');
            // $('#directCreditNoteProductTable').addClass('hidden');
            $('.direct-credit').addClass('hidden');
            $('.normal-credit').removeClass('hidden');
            $('#drcCus').addClass('hidden');
            $('#norCus').removeClass('hidden');
            creditNoteBuyBackType = false;
            $('#dimensionDiv').addClass('hidden');
            $('#directCredInv').addClass('hidden');
            $('#closeInvoTick').addClass('hidden');
        }
    });
    //when click invlid item tick
    $('#inactiveItmsFlag').on('click', function(){
        if($(this).is(':checked')){
            invFlag = true;
        } else {
            invFlag = false;
        }
    });

     $('#closeInvoiceID').on('click', function(){
        if($(this).is(':checked')){
            invCloseFlag = true;
        } else {
            invCloseFlag = false;
        }
     });

    
    //add customer to direct credit credit note
    $('#drcCus').on('change','#directCustomer', function() {
        if ($(this).val() > 0 && customerID != $(this).val()) {
            customerID = $(this).val();
            $('#customer').val('directCreditNote');
            //if this is direct credit note then user can link with invoice that related to the given customer id

            if (!creditNoteBuyBackType) {
                $('#directCredInv').removeClass('hidden');
                $('#closeInvoTick').removeClass('hidden');
                loadDropDownFromDatabase('/invoice-api/search-sales-invoices-for-document-dropdown', locationID, '', '#directInvoiceNo','','',customerID);
            }

        }
    });
    //add invoice id to the direct credit note
    $('#directCredInv').on('change', '#directInvoiceNo', function(){
        if ($(this).val() > 0 && InvoiceID != $(this).val()) {
            InvoiceID = $(this).val();
        }
    });

    if (!getAddRow().length) {
        $($addRowSample.clone()).removeClass('sample hidden').appendTo($('#add-new-item-row'));
    }

    //if pos flag equal 2 means load pos invices also to the dropdown
    var posFlag = 0;
    loadDropDownFromDatabase('/invoice-api/search-sales-invoices-for-dropdown', "", posFlag, '#invoiceNo');

    $('#invoiceNo').selectpicker('refresh');

    $('#invoiceNo').on('change', function() {
        if ($(this).val() > 0 && InvoiceID != $(this).val()) {
            InvoiceID = $(this).val();
            getInvoiceDetails(InvoiceID, invFlag);
        } else if ($(this).val() == 0 && InvoiceID != null){
            InvoiceID = '';
        }
    });



    //if pos flag equal 2 means load pos invices also to the dropdown
    var posFlag = 0;
    loadDropDownFromDatabase('/invoice-api/search-sales-invoices-for-dropdown', "", posFlag, '#invoiceNoForReturn');

    $('#invoiceNoForReturn').selectpicker('refresh');

    $('#invoiceNoForReturn').on('change', function() {
        if ($(this).val() > 0 && ReturnInvoiceID != $(this).val()) {
            ReturnInvoiceID = $(this).val();
            getInvoiceRelatedInvoiceRetunrs(ReturnInvoiceID, invFlag);
        } else if ($(this).val() == 0 && ReturnInvoiceID != null){
            ReturnInvoiceID = '';
        }
    });



    $('#dimensionView').on('click', function(e) {
        clearDimensionModal();
        var creditNoteNo = $('#creditNoteNo').val();

        if (!$.isEmptyObject(dimensionData)) {
            $.each(dimensionData[creditNoteNo], function(index, val) {
                dimensionArray[index] = val;                
            });
            $.each(dimensionArray, function(index, value) {
                var newTrID = 'tr_' + value.dimensionTypeId;
                var clonedRow = $($('#dimensionPreSample').clone()).attr('id', newTrID).addClass('addedDimensions');
                $("input[name='dimensionType']", clonedRow).val(value.dimensionTypeTxt);
                $("input[name='dimensionValue']", clonedRow).val(value.dimensionValueTxt);
                clonedRow.removeClass('hidden');
                clonedRow.insertBefore('#dimensionPreSample');
                clearAddedNewDimensionRow();
            });
        }
        $('#addDimensionModal').modal('show');
    });

    // dimension modal
    function clearDimensionModal()
    {
        $("#addDimensionModal #dimensionTable tbody .addedDimensions").remove();
        dimensionArray = {};
    }


    $('.dimenisonchange').on('change', function(e) {
        dimensionTypeID = this.value;
        getDimensionValueListByDimensionType(dimensionTypeID, function() {
        });
    });



    function getDimensionValueListByDimensionType(dimensionTypeID, callback) {
        switch (dimensionTypeID) {
            case "job":
                $('.dimensionVal').addClass('hidden');
                $('.dimensionProjectNo').addClass('hidden');
                $('.dimensionJobNo').removeClass('hidden');
                break;
            case "project":
                $('.dimensionVal').addClass('hidden');
                $('.dimensionProjectNo').removeClass('hidden');
                $('.dimensionJobNo').addClass('hidden');
                break;
           default:
                $('.dimensionVal').removeClass('hidden');
                $('.dimensionProjectNo').addClass('hidden');
                $('.dimensionJobNo').addClass('hidden');
                $.ajax({
                    type: 'POST',
                    url: BASE_URL + '/dimension-api/getRelatedDimensionValues',
                    data: {dimensionId: dimensionTypeID},
                    success: function(respond) {
                        //empty options
                        $('#dimensionVal').empty();
                        $('#dimensionVal').append($("<option>", {value: '', html: 'Select dimension value'}));
                        $(respond.data).each(function(index, dimValue) {
                            $.each(dimValue, function(ind, val) {
                                $('#dimensionVal').append($("<option>", {value: ind, html: val.value}));
                            });
                        });
                        callback();
                    }
                });
                break;
        }
    }

    $('#add_dimension').on('click', function(e) {
        e.preventDefault();
        //if plus button is clicked then disabled the button
        //if button is clicked more than one, then it does not continue
        if ($(this).is(':disabled')) {
            return false;
        } else {
            $(this).attr('disabled', true);
            var $thisRow = $(this).parents('tr');
            if ($('.dimenisonchange').val() == 0 || $('.dimenisonchange').val() == '' || $('.dimenisonchange').val() == null) {
                p_notification(false, eb.getMessage('ERR_DIMENSION_SELCET'));
                $(this).attr('disabled', false);
            } else {

                if ($('.dimenisonchange').val() != 'job' && $('.dimenisonchange').val() != 'project') {
                    if ($('.dimensionVal').val() == 0 || $('.dimensionVal').val() == '' || $('.dimensionVal').val() == null) {
                        p_notification(false, eb.getMessage('ERR_DIMENSION_VALUE_SELCET'));
                        $(this).attr('disabled', false);
                        return;
                    }
                }

                if ($('.dimenisonchange').val() == 'project') {
                    if ($('.dimensionProjectNo').val() == 0 || $('.dimensionProjectNo').val() == '' || $('.dimensionProjectNo').val() == null) {
                        p_notification(false, eb.getMessage('ERR_DIMENSION_VALUE_SELCET'));
                        $(this).attr('disabled', false);
                        return;
                    }
                }

                if ($('.dimenisonchange').val() == 'job') {
                    if ($('.dimensionJobNo').val() == 0 || $('.dimensionJobNo').val() == '' || $('.dimensionJobNo').val() == null) {
                        p_notification(false, eb.getMessage('ERR_DIMENSION_VALUE_SELCET'));
                        $(this).attr('disabled', false);
                        return;
                    }
                }

                if (dimensionArray[dimensionTypeID]) {
                    p_notification(false, eb.getMessage('ERR_DIMENSION_ALREADY_INSERTED'));
                    $('.dimenisonchange').val('');
                    $('.dimenisonchange').selectpicker('render');
                    $('.dimenisonchange').trigger('change');
                } else {
                    addNewDimensionRow($(this).parents('tr'));
                }
                $(this).attr('disabled', false);
            }
        }
    });

    function addNewDimensionRow(thisRow) {

        var dimensionTypeId = $('.dimenisonchange', thisRow).val();
        var dimensionTypeTxt = $('.dimenisonchange', thisRow).find("option:selected").text();

        switch (dimensionTypeID) {
            case "job":
                var dimensionValueTxt = $('.dimensionJobNo', thisRow).find("option:selected").text();
                var dimensionValueId = $('.dimensionJobNo', thisRow).val();
                break;
            case "project":
                var dimensionValueTxt = $('.dimensionProjectNo', thisRow).find("option:selected").text();
                var dimensionValueId = $('.dimensionProjectNo', thisRow).val();
                break;
           default:
                var dimensionValueTxt = $('.dimensionVal', thisRow).find("option:selected").text();
                var dimensionValueId = $('.dimensionVal', thisRow).val();
                break;
        }
       
        var dimensionDuplicateCheck = false;
        if (!$.isEmptyObject(dimensionArray)) {
            $.each(dimensionArray, function(index, value) {
                if (value.dimensionTypeId == dimensionTypeId) {
                    dimensionDuplicateCheck = true;
                }
            });   
        }
       
        if (dimensionDuplicateCheck) {
            p_notification(false, eb.getMessage("ERR_DIMENSION_ALREADY_INSERTED"));
            return false;
        }

        var newTrID = 'tr_' + dimensionTypeId;
        var clonedRow = $($('#dimensionPreSample').clone()).attr('id', newTrID).addClass('addedDimensions');
        $("input[name='dimensionType']", clonedRow).val(dimensionTypeTxt);
        $("input[name='dimensionValue']", clonedRow).val(dimensionValueTxt);
        clonedRow.removeClass('hidden');
        clonedRow.insertBefore('#dimensionPreSample');
        dimensionArray[dimensionTypeId] = new dimension(dimensionTypeId, dimensionValueId,dimensionTypeTxt , dimensionValueTxt);
        clearAddedNewDimensionRow();
    }


    function dimension(dimensionTypeId, dimensionValueId, dimensionTypeTxt, dimensionValueTxt) {
        this.dimensionTypeId = dimensionTypeId;
        this.dimensionValueId = dimensionValueId;
        this.dimensionTypeTxt = dimensionTypeTxt;
        this.dimensionValueTxt = dimensionValueTxt;
    }

    function clearAddedNewDimensionRow() {
        $('.dimenisonchange').val('');
        $('.dimenisonchange').selectpicker('render');
        $('.dimenisonchange').trigger('change');
    }

    $('.addDimensions').on('click', function(e) {
        e.preventDefault();

        // validate products before closing modal
        if (!dimensionModalValidate(e)) {
            e.stopPropagation();
            return false;
        } else {
            $('#addDimensionModal').modal('hide');
        }
    });

    function dimensionModalValidate(e) {
        var creditNoteNo = $('#creditNoteNo').val();
        if ($.isEmptyObject(dimensionArray)) {
            dimensionData = {};
            return true;   
        } else {
            var dimensionSingleData = {};
            $.each(dimensionArray, function(index, val) {
                dimensionSingleData[index] = val;
            });
            dimensionData[creditNoteNo] = dimensionSingleData;
            return true;
        }
    }

    $("#addDimensionModal #dimensionTable").on('click', '.deleteDimensionType', function(e) {
        e.preventDefault();
        var deleteDTrID = $(this).closest('tr').attr('id');
        var deleteDID = deleteDTrID.split('tr_')[1].trim();
        bootbox.confirm('Are you sure you want to remove this dimension ?', function(result) {
            if (result == true) {
                delete dimensionArray[deleteDID];
                $('#' + deleteDTrID).remove();
            }
        });
    });

    loadDropDownFromDatabase('/job-api/searchJobsForDropdown', "", 0, '.dimensionJobNo');
    $('.dimensionJobNo').selectpicker('refresh');
    $('.dimensionJobNo').on('change', function() {
        var dimensionValue = $(this).val();
    });

    loadDropDownFromDatabase('/api/project/searchProjectsForDropdown', "", 0, '.dimensionProjectNo');
    $('.dimensionProjectNo').selectpicker('refresh');
    $('.dimensionProjectNo').on('change', function() {
        var dimensionValue = $(this).val();
    });

    // this will get all invocie related details
    function getInvoiceDetails(InvoiceID, invFlag) {
        eb.ajax({
            type: 'POST',
            url: BASE_URL + '/invoice-api/getInvoiceDetails',
            data: {
                invoiceID: InvoiceID,
                invalidFlag: invFlag
            },
            success: function(respond) {
                clearProductDataForNewCreditNote();
                if (respond.status) {
                    
                    if (respond.data.invoice.jobID != null) {

                        p_notification(false, eb.getMessage('ERR_CREDIT_NOTE_JOB_INVOICE'));
                        setTimeout(function(){ 
                            window.location.assign(BASE_URL + "/credit-note")
                        }, 1000);
                        return false;
                    }

                    if (respond.data.inactiveItemFlag == true && invFlag == false) {
                        p_notification(false, respond.data.errorMsg);
                        setTimeout(function(){ 
                            window.location.assign(BASE_URL + "/credit-note")
                        }, 3000);
                        return false;
                    }
                    var promotionID = respond.data.invoice.promotionID;
                	var promotionDiscountAmount = respond.data.invoice.salesInvoicePromotionDiscount;
                	var promotionType = respond.data.invoice.promotionType;
                	var discountType = respond.data.invoice.salesInvoiceTotalDiscountType;
                	var discountAmount = respond.data.invoice.salesInvoiceDiscountRate;
                    invoiceDate = respond.data.invoice.salesInvoiceIssuedDate;
                	$('#promotion').val(promotionID).attr('disabled',true).selectpicker('refresh');
                	$('#total_discount_rate').val(discountAmount);
                    $('#creditNoteDate').val('');
                    if (discountType == "presentage") {
                        $('#total_discount_rate').attr('disabled',true);
                        $('#disc_presentage').attr('checked',true);
                    } else if(discountType == "Value") {
                        if (respond.data.invoice.salesInvoiceRemainingDiscValue != null) {
                            discountAmount = respond.data.invoice.salesInvoiceRemainingDiscValue;
                            $('#total_discount_rate').val(discountAmount).attr('disabled',false);
                        }
                        $('#disc_value').attr('checked',true);
                    }
                    copiedCreditNoteDiscountAmount = discountAmount;
                	$("input[name='discount_type']").attr('disabled',true);

                	if (promotionType == 2) {
                		if (respond.data.invoice.salesInvoiceRemainingPromotionDiscValue != null) {
                            promotionDiscountAmount = respond.data.invoice.salesInvoiceRemainingPromotionDiscValue;
                		}
                		$('#promotion_discount_rate').val(promotionDiscountAmount).attr('disabled',false);
                		$('#promo_disc_value').attr('checked',true);
                        copiedCrditNotePromotionDiscountAmount = promotionDiscountAmount;
                	}

                	$("input[name='promotion_discount_type']").attr('disabled',true);

                    var customer = respond.data.customer;
                    customerID = customer.customerID;
                    $('#customer').val(customer.customerName + '-' + customer.customerCode).attr('disabled', true);
                    $('#comment').val(respond.data.invoice.salesInvoiceComment);
//                    $('#deliveryNoteAddress').val(respond.data.invoice.deliveryNoteAddress);
                    Products = respond.data.products;
                    locationProducts = respond.data.invoiceProduct;
                    var rate = respond.data.invoice.salesInvoiceCustomCurrencyRate;
                    customCurrencyRate = (rate != 0) ? rate : 1;
                    selectProductForCreditNote(locationProducts, respond.data.invoice);
                    $('#customCurrencyId').val(respond.data.invoice.customCurrencyId).trigger('change').attr('disabled', true);
                } else {
                    InvoiceID = '';
                    p_notification(respond.status, respond.msg);
                    $('#invoiceNo').val('');
                }
            }
        });
    }

    function getInvoiceRelatedInvoiceRetunrs(ReturnInvoiceID, invFlag) {
        eb.ajax({
            type: 'POST',
            url: BASE_URL + '/invoice-return-api/getInvoiceRelatedInvoiceRetunrs',
            data: {
                invoiceID: ReturnInvoiceID,
                invalidFlag: invFlag
            },
            success: function(respond) {
                if (respond.status) {
                    $('#invoiceReturnNo').empty();
                    $('#invoiceReturnNo').append($("<option>", {value: '', html: 'Select Invoice Return', disabled:true}));

                    $(respond.data.list).each(function(index, invRtnValue) {
                        $('#invoiceReturnNo').append($("<option>", {value: invRtnValue.value, html: invRtnValue.text}));
                    });

                    $('#invoiceReturnNo').selectpicker('refresh');
                }
            }
        });
    }

    $('#get-invoice-return-data').on('click', function() {

        if ((ReturnInvoiceID == '' || ReturnInvoiceID == null) && !creditNoteDirectType && !creditNoteBuyBackType){
            p_notification(false, eb.getMessage('ERR_CREDIT_SELECT_INVOICENO'));
            return false;
        }


        if ($('#invoiceReturnNo').val() == null) {
            p_notification(false, eb.getMessage('ERR_NO_INVOICE_RETURN_SELECT'));
            return false;
        }



        eb.ajax({
            type: 'POST',
            url: BASE_URL + '/invoice-api/getInvoiceDetailsAccordingToInvoiceReturns',
            data: {
                invoiceID: ReturnInvoiceID,
                invoiceReturns: $('#invoiceReturnNo').val(),
                invalidFlag: invFlag
            },
            success: function(respond) {
                clearProductDataForNewCreditNote();
                if (respond.status) {
                    useInvoiceReturn = true;
                    if (respond.data.invoice.jobID != null) {

                        p_notification(false, eb.getMessage('ERR_CREDIT_NOTE_JOB_INVOICE'));
                        setTimeout(function(){ 
                            window.location.assign(BASE_URL + "/credit-note")
                        }, 1000);
                        return false;
                    }

                    if (respond.data.inactiveItemFlag == true && invFlag == false) {
                        p_notification(false, respond.data.errorMsg);
                        setTimeout(function(){ 
                            window.location.assign(BASE_URL + "/credit-note")
                        }, 3000);
                        return false;
                    }
                    var promotionID = respond.data.invoice.promotionID;
                    var promotionDiscountAmount = respond.data.invoice.salesInvoicePromotionDiscount;
                    var promotionType = respond.data.invoice.promotionType;
                    var discountType = respond.data.invoice.salesInvoiceTotalDiscountType;
                    var discountAmount = respond.data.invoice.salesInvoiceDiscountRate;
                    invoiceDate = respond.data.invoice.salesInvoiceIssuedDate;
                    $('#promotion').val(promotionID).attr('disabled',true).selectpicker('refresh');
                    $('#total_discount_rate').val(discountAmount);
                    $('#creditNoteDate').val('');
                    if (discountType == "presentage") {
                        $('#total_discount_rate').attr('disabled',true);
                        $('#disc_presentage').attr('checked',true);
                    } else if(discountType == "Value") {
                        if (respond.data.invoice.salesInvoiceRemainingDiscValue != null) {
                            discountAmount = respond.data.invoice.salesInvoiceRemainingDiscValue;
                            $('#total_discount_rate').val(discountAmount).attr('disabled',false);
                        }
                        $('#disc_value').attr('checked',true);
                    }
                    copiedCreditNoteDiscountAmount = discountAmount;
                    $("input[name='discount_type']").attr('disabled',true);

                    if (promotionType == 2) {
                        if (respond.data.invoice.salesInvoiceRemainingPromotionDiscValue != null) {
                            promotionDiscountAmount = respond.data.invoice.salesInvoiceRemainingPromotionDiscValue;
                        }
                        $('#promotion_discount_rate').val(promotionDiscountAmount).attr('disabled',false);
                        $('#promo_disc_value').attr('checked',true);
                        copiedCrditNotePromotionDiscountAmount = promotionDiscountAmount;
                    }

                    $("input[name='promotion_discount_type']").attr('disabled',true);

                    var customer = respond.data.customer;
                    customerID = customer.customerID;
                    $('#customer').val(customer.customerName + '-' + customer.customerCode).attr('disabled', true);
                    $('#comment').val(respond.data.invoice.salesInvoiceComment);
//                    $('#deliveryNoteAddress').val(respond.data.invoice.deliveryNoteAddress);
                    Products = respond.data.products;
                    locationProducts = respond.data.invoiceProduct;
                    var rate = respond.data.invoice.salesInvoiceCustomCurrencyRate;
                    customCurrencyRate = (rate != 0) ? rate : 1;
                    selectProductForCreditNoteFromInvoiceReturn(locationProducts, respond.data.invoice);
                    $('#customCurrencyId').val(respond.data.invoice.customCurrencyId).trigger('change').attr('disabled', true);
                } else {
                    // ReturnInvoiceID = '';
                    p_notification(respond.status, respond.msg);
                    $('#invoiceNo').val('');
                }
            }
        });

        return false;
    });


    function clearProductDataForNewCreditNote() {
        var $currentRow = getAddRow();
        $currentRow.removeClass('hidden');
        $('#add-new-item-row', $productTable).children('tr:not(.add-row)').remove('tr');
        creditNoteSubProducts = {};
        creditNoteProducts = {};
        checkSubProduct = {};
        batchProducts = {};
        productsTax = {};
        productsTotal = {};
        $('#deliveryNoteAddress').val('');
        $('#comment').val('');
        $('#subtotal').html('0.00');
        $('#finaltotal').html('0.00');

    }
    function clearDirectCreditNoteRow($curntRw)
    {
        $('.directCQty').children('.uomqty').val('');
        $('.dirUPrc').children('.uomPrice').val('');
        $("#discount", $curntRw).removeClass('precentage');
        $("#discount", $curntRw).removeClass('value');
    }

    function selectProductForCreditNote(locationProducts, invoiceData) {
        var checkProduct = 0;

        $.each(locationProducts, function(selectedProduct, value) {
            var productID = value.productID;
            var documentTypeID = value.documentTypeID;
            var documentID = value.documentID;
            var productCode = value.productCode;
            var productName = value.productName;
            var availableQuantity = value.invoiceProductQuantity;
            var invoiceproductID = value.invoiceProductID;
            var defaultSellingPrice = value.invoiceProductPrice / customCurrencyRate;
            var productDiscountType = value.invoiceProductDiscountType;
            var productDiscount = (productDiscountType == 'value') ? value.invoiceProductDiscount / customCurrencyRate : value.invoiceProductDiscount;
            var productType = value.productType;
            var locationProductID = value.locationProductID;
            var giftCard = value.giftCard;
            var flag = false;
            if (productType == 2) {
                flag = true;
            }
            if (productType != 2 && availableQuantity != 0) {
                flag = true;
            }

            var baseUom;
            for (var j in Products[selectedProduct].uom) {
                if (Products[selectedProduct].uom[j].pUBase == 1) {
                    baseUom = Products[selectedProduct].uom[j].uomID;
                }
            }

            if (flag) {
                checkProduct = 1;
                var $currentRow = getAddRow();
                clearProductRow($currentRow);
                $("input[name='itemCode']", $currentRow).val(productCode + ' - ' + productName).prop('readonly', true);
                $("input[name='itemCode']", $currentRow).data('PT', productType);
                $("input[name='itemCode']", $currentRow).data('PC', productCode);
                $("input[name='itemCode']", $currentRow).data('PN', productName);
                $("input[name='itemCode']", $currentRow).data('GC', giftCard);
                $("input[name='creditNoteQuanity']", $currentRow).parent().addClass('input-group');
                $("input[name='invoicedQuantity']", $currentRow).parent().addClass('input-group');
                $("input[name='creditNoteQuanity']", $currentRow).val(availableQuantity).prop('readonly', true).addUom(Products[selectedProduct].uom);
                $("input[name='invoicedQuantity']", $currentRow).val(availableQuantity).prop('readonly', true).addUom(Products[selectedProduct].uom);
                $("input[name='unitPrice']", $currentRow).val(defaultSellingPrice).prop('readonly', true).addUomPrice(Products[selectedProduct].uom);
                if (productDiscountType != 'precentage') {
                    $("input[name='discount']", $currentRow).val(productDiscount).prop('readonly', true).addUomPrice(Products[selectedProduct].uom, baseUom);
                    $("input[name='discount']", $currentRow).siblings('.uom-price-select').find('.uom_price_drop_down').attr('disabled', true);
                    $("input[name='discount']", $currentRow).addClass('value');
                    if ($("input[name='discount']", $currentRow).hasClass('precentage')) {
                        $("input[name='discount']", $currentRow).removeClass('precentage');
                    }
                } else {
                    $("input[name='discount']", $currentRow).val(productDiscount).prop('readonly', true);
                    $("input[name='discount']", $currentRow).addClass('precentage');
                    if ($("input[name='discount']", $currentRow).hasClass('value')) {
                        $("input[name='discount']", $currentRow).removeClass('value');
                    }
                    $(".discountSymbol", $currentRow).text('%');
                }

                $currentRow.data('id', productID);
                $currentRow.data('dPID', invoiceproductID);
                $currentRow.data('locationproductid', locationProductID);
                $currentRow.data('dtid', documentTypeID);
                $currentRow.data('did', documentID);
                $currentRow.parents().find('.delete').removeClass('disabled');

                // clear old rows
                $("#batch_data tr:not(.hidden)").remove();

                // check if any batch / serial products exist
                if ($.isEmptyObject(value.subProduct)) {
                    $currentRow.removeClass('subproducts');
                    $('.edit-modal', $currentRow).parent().addClass('hidden');
                    $("td[colspan]", $currentRow).attr('colspan', 2);
                    $("input[name='creditNoteQuanity']", $currentRow).prop('readonly', false).change().focus();
                } else {
                    $currentRow.addClass('subproducts');
                    $('.edit-modal', $currentRow).parent().removeClass('hidden');
                    $("td[colspan]", $currentRow).attr('colspan', 1);
                    $("input[name='creditNoteQuanity']", $currentRow).prop('readonly', true).change();
                }
                $currentRow.addClass('edit-row');
                setTaxListForProduct(productID, $currentRow);
                $currentRow.attr('id', 'product' + productID);
                $("input[name='unitPrice']", $currentRow).trigger('focusout');
                $currentRow
                        .removeClass('add-row');
                $($addRowSample.clone()).removeClass('sample hidden').appendTo($('#add-new-item-row', $productTable));
            }
        });
        if (checkProduct == 0) {
            p_notification(false, eb.getMessage('ERR_CREDIT_NOTE_INVOICE'));
        }
        var $currentRow = getAddRow();
        $currentRow.addClass('hidden');
        return;

    }


    function selectProductForCreditNoteFromInvoiceReturn(locationProducts, invoiceData) {
        var checkProduct = 0;

        $.each(locationProducts, function(selectedProduct, value) {
            var productID = value.productID;
            var documentTypeID = value.documentTypeID;
            var documentID = value.documentID;
            var productCode = value.productCode;
            var productName = value.productName;
            var availableQuantity = value.invoiceProductQuantity;
            var invoiceReturnQuantity = value.invoiceReturnQuantity;
            var invoiceproductID = value.invoiceProductID;
            var defaultSellingPrice = value.invoiceProductPrice / customCurrencyRate;
            var productDiscountType = value.invoiceProductDiscountType;
            var productDiscount = (productDiscountType == 'value') ? value.invoiceProductDiscount / customCurrencyRate : value.invoiceProductDiscount;
            var productType = value.productType;
            var locationProductID = value.locationProductID;
            var giftCard = value.giftCard;
            var flag = false;
            if (productType == 2) {
                flag = true;
            }
            if (productType != 2 && availableQuantity != 0) {
                flag = true;
            }


            if (flag) {
                checkProduct = 1;
                var $currentRow = getAddRow();
                clearProductRow($currentRow);
                $("input[name='itemCode']", $currentRow).val(productCode + ' - ' + productName).prop('readonly', true);
                $("input[name='itemCode']", $currentRow).data('PT', productType);
                $("input[name='itemCode']", $currentRow).data('PC', productCode);
                $("input[name='itemCode']", $currentRow).data('PN', productName);
                $("input[name='itemCode']", $currentRow).data('GC', giftCard);
                $("input[name='creditNoteQuanity']", $currentRow).parent().addClass('input-group');
                $("input[name='invoicedQuantity']", $currentRow).parent().addClass('input-group');
                $("input[name='creditNoteQuanity']", $currentRow).val(invoiceReturnQuantity).prop('readonly', true).addUom(Products[selectedProduct].uom);
                $("input[name='invoicedQuantity']", $currentRow).val(availableQuantity).prop('readonly', true).addUom(Products[selectedProduct].uom);
                $("input[name='unitPrice']", $currentRow).val(defaultSellingPrice).prop('readonly', true).addUomPrice(Products[selectedProduct].uom);
                $("input[name='discount']", $currentRow).val(productDiscount).prop('readonly', true);
                if (productDiscountType != 'precentage') {
                    $("input[name='discount']", $currentRow).addClass('value');
                    if ($("input[name='discount']", $currentRow).hasClass('precentage')) {
                        $("input[name='discount']", $currentRow).removeClass('precentage');
                    }
                } else {
                    $("input[name='discount']", $currentRow).addClass('precentage');
                    if ($("input[name='discount']", $currentRow).hasClass('value')) {
                        $("input[name='discount']", $currentRow).removeClass('value');
                    }
                    $(".discountSymbol", $currentRow).text('%');
                }

                $currentRow.data('id', productID);
                $currentRow.data('dPID', invoiceproductID);
                $currentRow.data('locationproductid', locationProductID);
                $currentRow.data('dtid', documentTypeID);
                $currentRow.data('did', documentID);
                // $currentRow.parents().find('.delete').removeClass('disabled');

                // clear old rows
                $("#batch_data tr:not(.hidden)").remove();

                // check if any batch / serial products exist
                if ($.isEmptyObject(value.subProduct)) {
                    $currentRow.removeClass('subproducts');
                    $('.edit-modal', $currentRow).parent().addClass('hidden');
                    $("td[colspan]", $currentRow).attr('colspan', 2);
                    $("input[name='creditNoteQuanity']", $currentRow).prop('readonly', true).change().focus();
                } else {
                    $currentRow.addClass('subproducts');
                    $('.edit-modal', $currentRow).parent().removeClass('hidden');
                    $("td[colspan]", $currentRow).attr('colspan', 1);
                    $("input[name='creditNoteQuanity']", $currentRow).prop('readonly', true).change();
                }
                $currentRow.addClass('edit-row');
                setTaxListForProduct(productID, $currentRow);
                $currentRow.attr('id', 'product' + productID);
                $("input[name='unitPrice']", $currentRow).trigger('focusout');
                $currentRow
                        .removeClass('add-row');
                $($addRowSample.clone()).removeClass('sample hidden').appendTo($('#add-new-item-row', $productTable));
            }
        });
        if (checkProduct == 0) {
            p_notification(false, eb.getMessage('ERR_CREDIT_NOTE_INVOICE'));
        }
        var $currentRow = getAddRow();
        $currentRow.addClass('hidden');
        return;

    }

    $productTable.on('click', 'button.edit-modal', function(e) {

        var productID = $(this).parents('tr').data('id');
        var invoiceProductID = $(this).parents('tr').data('dPID');
        var productCode = $(this).parents('tr').find('#itemCode').val();
        $("#batch_data tr:not(.hidden)").remove();
        $('#addCreditNoteProductsModal').modal('show').data('id', productID).data('dPID', invoiceProductID);
        addSubProduct(invoiceProductID, productCode, 0);
        $("tr", '#batch_data').each(function() {
            var $thisSubRow = $(this);
            for (var i in creditNoteSubProducts[invoiceProductID]) {
                if (creditNoteSubProducts[invoiceProductID][i].serialID != '') {
                    if ($(".serialID", $thisSubRow).data('id') == creditNoteSubProducts[invoiceProductID][i].serialID) {
                        $("input[name='creditNoteProductSubQuantityCheck']", $thisSubRow).prop('checked', true);
                    }
                } else if (creditNoteSubProducts[invoiceProductID][i].batchID != '') {
                    if ($(".batchCode", $thisSubRow).data('id') == creditNoteSubProducts[invoiceProductID][i].batchID) {
                        $(this).find("input[name='creditNoteProductSubQuantity']").val(creditNoteSubProducts[invoiceProductID][i].qtyByBase).change();
                    }
                }
            }
        });

        $('#addDeliveryNoteProductsModal').data('id', $(this).parents('tr').data('id')).modal('show');
        $('#addDeliveryNoteProductsModal').unbind('hide.bs.modal');
    });

    $productTable.on('click', 'button.add, button.save', function(e) {
        var $thisRow = $(this).parents('tr');
        var thisVals = getCurrentProductData($thisRow);
        $("input[name='invoicedQuantity']", $thisRow).prop('readonly', true);
        if ((thisVals.productCode.trim()) == '' || (thisVals.productName.trim()) == '') {
            p_notification(false, eb.getMessage('ERR_CREDIT_SELECT_PROD'));
            $("input[name='itemCode']", $thisRow).focus();
            return false;
        }

        if (thisVals.productType != 2 && (isNaN(parseFloat(thisVals.creditNoteQuantity.qty)) || parseFloat(thisVals.creditNoteQuantity.qty) <= 0)) {
            p_notification(false, eb.getMessage('ERR_CREDIT_VALQUANTITY'));
            $("input[name='creditNoteQuanity']", $thisRow).focus();
            return false;
        }
        if (parseFloat(thisVals.creditNoteQuantity.qty) > parseFloat(thisVals.availableQuantity.qty)) {
            p_notification(false, eb.getMessage('ERR_CREDIT_QUANTITY'));
            $("input[name='creditNoteQuanity']", $thisRow).focus();
            if (checkSubProduct[thisVals.invoiceproductID] == undefined) {
                return false;
            }
        }

        if ((thisVals.productType == 2 && parseFloat(thisVals.availableQuantity.qty) > 0) && (parseFloat(thisVals.creditNoteQuantity.qty) == 0)) {
            p_notification(false, eb.getMessage('ERR_CREDIT_VALQUANTITY'));
            $("input[name='creditNoteQuanity']", $thisRow).focus();
            return false;
        }

        if (thisVals.productDiscount) {
            if (isNaN(parseFloat(thisVals.productDiscount)) || parseFloat(thisVals.productDiscount) < 0) {
                p_notification(false, eb.getMessage('ERR_CREDIT_VALID_DISCOUNT'));
                $("input[name='discount']", $thisRow).focus();
                return false;
            }
        }

        if ($thisRow.hasClass('subproducts')) {
            if (SubProductsQty[thisVals.invoiceproductID] != '' && (parseFloat(SubProductsQty[thisVals.invoiceproductID]) != parseFloat(thisVals.creditNoteQuantity.qty))) {
                p_notification(false, eb.getMessage('ERR_CREDIT_SUB_PRODUCT_CHECK', thisVals.productCode));
                $('.edit-modal', $thisRow).focus();
                return false;
            }
        }

        // if add button is clicked

        if(productBeingEdited.indexOf($(this).parents('tr').data('id')) >= 0){
            productBeingEdited.splice(productBeingEdited.indexOf($(this).parents('tr').data('id')),1);
        }

        $thisRow.removeClass('edit-row');
        if ($(this).hasClass('add')) {
            $thisRow
                    .removeClass('add-row')
                    .find("input[name='itemCode'], input[name='itemName'], input[name='invoicedQuantity'],input[name='creditNoteQuanity'],input[name='unitPrice'],input[name='discount']").prop('readonly', true).end()
                    .find('td').find('#addNewTax').attr('disabled', true);
        } else if ($(this).hasClass('save')) {
            $thisRow
                    .find("input[name='itemCode'], input[name='itemName'], input[name='invoicedQuantity'],input[name='creditNoteQuanity'],input[name='unitPrice'],input[name='discount']").prop('readonly', true).end()
                    .find('td').find('#addNewTax').attr('disabled', true);
        } else { // if save button is clicked
            $thisRow.find("input[name='invoicedQuantity']").prop('readonly', true);
        }

        // if batch product modal is available for this product
        $(this, $thisRow).parent().find('.edit').removeClass('hidden');
        $(this, $thisRow).parent().find('.add').addClass('hidden');
        $(this, $thisRow).parent().find('.save').addClass('hidden');
        if ($thisRow.hasClass('subproducts')) {
            $('.edit-modal', $thisRow).parent().addClass('hidden');
            $("td[colspan]", $thisRow).attr('colspan', 2);
        } else {
            $("#creditNoteQuanity", $thisRow).attr('disabled', true).change();
        }
        creditNoteProducts[thisVals.invoiceproductID] = thisVals;
        setCreditNoteTotalCost();
    });

    $productTable.on('click', 'button.edit', function(e) {

        var $thisRow = $(this).parents('tr');
        productBeingEdited.push($(this).parents('tr').data('id'));
        $thisRow.addClass('edit-row')
                .find('td').find('#addNewTax').attr('disabled', false);
        $(this, $thisRow).parent().find('.edit').addClass('hidden');
        $(this, $thisRow).parent().find('.save').removeClass('hidden');
        if ($thisRow.hasClass('subproducts')) {
            $('.edit-modal', $thisRow).parent().removeClass('hidden');
            $("td[colspan]", $thisRow).attr('colspan', 1);
        } else {
            $("#creditNoteQuanity", $thisRow).attr('disabled', false).prop('readonly', false).change().focus();
        }
    });

    $productTable.on('click', 'button.delete', function(e) {

        if(productBeingEdited.indexOf($(this).parents('tr').data('id')) >= 0){
            productBeingEdited.splice(productBeingEdited.indexOf($(this).parents('tr').data('id')),1);
        }
        var $thisRow = $(this).parents('tr');
        var deletePID = $thisRow.data('dPID');
        delete creditNoteProducts[deletePID];
        setCreditNoteTotalCost();
        $thisRow.remove();
    });

    function addSubProduct(selectedProduct, productCode, deleted) {
        if ((!$.isEmptyObject(locationProducts[selectedProduct].subProduct))) {

            batchProduct = locationProducts[selectedProduct].subProduct;
            returnBatchProduct = locationProducts[selectedProduct].invoiceReturnBatchDetails;
            returnSerialProduct = locationProducts[selectedProduct].invoiceReturnSerialDetails;
            batch = Products[selectedProduct].batch;
            batchSerial = Products[selectedProduct].batchSerial;
            productBatchSerial = Products[selectedProduct].batchSerial;
            productSerial = Products[selectedProduct].serial;
            for (var i in batchProduct) {
                if (batchProduct[i].invoiceSubProductQuantity != 0) {
                    if (deleted != 1 && batchProduct[i].productBatchID != null && batchProduct[i].productSerialID != null) {
                        var serialKey = productCode + "-" + batchProduct[i].productSerialID;
                        var serialQty = (batchProduct[i].invoiceSubProductQuantity == 1) ? 1 : 0;

                        var $clonedRow = $($('.batch_row').clone());
                        $clonedRow
                                .data('id', batchProduct[i].productSerialID)
                                .removeAttr('id')
                                .removeClass('hidden batch_row')
                                .addClass('remove_row');
                        $(".serialID", $clonedRow)
                                .html(batchSerial[batchProduct[i].productSerialID].PSC)
                                .data('id', batchProduct[i].productSerialID);
                        $(".batchCode", $clonedRow)
                                .html(batchSerial[batchProduct[i].productSerialID].PBC)
                                .data('id', batchProduct[i].productBatchID);
                        $("input[name='availableProductSubQuantity']", $clonedRow).val(serialQty).addUom(Products[selectedProduct].uom);
                        if (serialQty == 0) {
                            $("input[name='creditNoteProductSubQuantityCheck']", $clonedRow).prop('disabled', true);
                        }
                        $("input[name='creditNoteProductSubQuantity']", $clonedRow).parent().remove();

                        if (batchProducts[serialKey]) {
                            if (batchProducts[serialKey].Qty == 1) {
                                $("input[name='creditNoteProductSubQuantityCheck']", $clonedRow).attr('checked', true);
                            }
                        }

                        $clonedRow.insertBefore('.batch_row');

                    } else if (deleted != 1 && batchProduct[i].productBatchID != null) {
                        if ((!$.isEmptyObject(Products[selectedProduct].productIDs))) {
                            if (Products[selectedProduct].productIDs[batchProduct[i].productBatchID]) {
                                continue;
                            }
                        }
                        var batchKey = productCode + "-" + batchProduct[i].productBatchID;

                        var $clonedRow = $($('.batch_row').clone());
                        $clonedRow
                                .data('id', batchProduct[i].productBatchID)
                                .removeAttr('id')
                                .removeClass('hidden batch_row')
                                .addClass('remove_row');
                        $(".batchCode", $clonedRow).html(batch[batchProduct[i].productBatchID].PBC).data('id', batchProduct[i].productBatchID);
                        $("input[name='availableProductSubQuantity']", $clonedRow).val(batchProduct[i].invoiceSubProductQuantity).addUom(Products[selectedProduct].uom);
                        if (batchProduct[i].invoiceSubProductQuantity == 0) {
                            $("input[name='creditNoteProductSubQuantity']", $clonedRow).prop('disabled', true);
                        }
                        $("input[name='creditNoteProductSubQuantityCheck']", $clonedRow).remove();
                        if (batchProducts[batchKey]) {
                            $("input[name='creditNoteProductSubQuantity']", $clonedRow).val(batchProducts[batchKey].Qty);
                        }

                        if (useInvoiceReturn) {
                            var invoiceReturnBatchQty = returnBatchProduct[batchProduct[i].productBatchID];
                            $("input[name='creditNoteProductSubQuantity']", $clonedRow).val(invoiceReturnBatchQty).addUom(Products[selectedProduct].uom);
                            $("input[name='creditNoteProductSubQuantity']", $clonedRow).prop('readonly', true);
                            $(".uomqty", $clonedRow).prop('readonly', true);
                        } else {
                            $("input[name='creditNoteProductSubQuantity']", $clonedRow).addUom(Products[selectedProduct].uom);

                        }

                        $clonedRow.insertBefore('.batch_row');

                    } else if (deleted != 1 && batchProduct[i].productSerialID != null) {
                        var serialKey = productCode + "-" + batchProduct[i].productSerialID;
                        var serialQty = (batchProduct[i].invoiceSubProductQuantity == 1) ? 1 : 0;

                        var $clonedRow = $($('.batch_row').clone());
                        $clonedRow
                                .data('id', batchProduct[i].productSerialID)
                                .removeAttr('id')
                                .removeClass('hidden batch_row')
                                .addClass('remove_row');
                        $(".serialID", $clonedRow)
                                .html(productSerial[batchProduct[i].productSerialID].PSC)
                                .data('id', batchProduct[i].productSerialID);
                        $("input[name='availableProductSubQuantity']", $clonedRow).val(serialQty).addUom(Products[selectedProduct].uom);

                        if (useInvoiceReturn) {
                            if (returnSerialProduct[batchProduct[i].productSerialID]) {
                                $("input[name='creditNoteProductSubQuantityCheck']", $clonedRow).attr('checked', true);
                            }
                            $("input[name='creditNoteProductSubQuantityCheck']", $clonedRow).prop('disabled', true);
                        }


                        if (serialQty = 0) {
                            $("input[name='creditNoteProductSubQuantityCheck']", $clonedRow).prop('disabled', true);
                        }
                        $("input[name='creditNoteProductSubQuantity']", $clonedRow).parent().remove();

                        if (batchProducts[serialKey]) {
                            if (batchProducts[serialKey].Qty == 1) {
                                $("input[name='creditNoteProductSubQuantityCheck']", $clonedRow).attr('checked', true);
                            }
                        }

                        $clonedRow.insertBefore('.batch_row');

                    } else {
                        batchProducts[batchKey] = undefined;
                    }
                }
            }
        }
    }

    function setTaxListForProduct(productID, $currentRow, ItemTaxes) {
        
    	var salesInvoiceID = $currentRow.data('dPID');
        if ((!jQuery.isEmptyObject(locationProducts[salesInvoiceID].tax))) {
            productTax = locationProducts[salesInvoiceID].tax;
            $currentRow.find('#taxApplied').removeClass('glyphicon glyphicon-unchecked');
            $currentRow.find('#taxApplied').addClass('glyphicon glyphicon-check');
            $currentRow.find('.tempLi').remove();
            for (var i in productTax) {
                var taxAmount = 0.00;

                if (!jQuery.isEmptyObject(ItemTaxes)) {
                    if(ItemTaxes[i] != undefined){
                        taxAmount = ItemTaxes[i].tA;
                        
                    }
                }
                var clonedLi = $($currentRow.find('#sampleLi').clone()).removeClass('hidden').attr('id', 'li_' + i).addClass("tempLi");
                clonedLi.children(".taxChecks").attr('id', productID + '_' + i).prop('checked', true).prop('disabled', true).addClass('addNewTaxCheck');
                clonedLi.children(".taxName").html('&nbsp&nbsp' + productTax[i].invoiceTaxName + '&nbsp&nbsp' + productTax[i].invoiceTaxPrecentage + '% , ' + taxAmount + ' RS').attr('for', productID + '_' + i);
                clonedLi.insertBefore($currentRow.find('#sampleLi'));
            }
        } else {
            $('#taxApplied').addClass('glyphicon glyphicon-unchecked');
            $currentRow.find('#addNewTax').attr('disabled', 'disabled');
        }

    }

    function clearProductRow($currentRow) {
        $("input[name='itemCode']", $currentRow).val('');
        $("input[name='availableQuantity']", $currentRow).val('');
        $("input[name='creditNoteQuanity']", $currentRow).val('');
        $("input[name='unitPrice']", $currentRow).val('');
        $("input[name='discount']", $currentRow).val('');
        $(".tempLi", $currentRow).remove('');
        $("#taxApplied", $currentRow).removeClass('glyphicon-check');
        $("#taxApplied", $currentRow).addClass('glyphicon-unchecked');
        $(".selected", $currentRow).text('');
        $(".uomList", $currentRow).remove('');
    }

    $('#batch-save').on('click', function(e) {
        e.preventDefault();
        // validate batch / serial products before closing modal
        if (!batchModalValidate(e)) {
            e.stopPropagation();
            return false;
        } else {
            $('#addCreditNoteProductsModal').modal('hide');
        }
    });

    function batchModalValidate(e) {

        var productID = $('#addCreditNoteProductsModal').data('id');
        var invoiceProductID = $('#addCreditNoteProductsModal').data('dPID');
        var $thisParentRow = getAddRow(invoiceProductID);
        var thisVals = getCurrentProductData($thisParentRow);
        var $batchTable = $("#addCreditNoteProductsModal .batch-table tbody");
        var qtyTotal = 0;
        var subProducts = [];

        $("input[name='creditNoteProductSubQuantity'], input[name='creditNoteProductSubQuantityCheck']:checked", $batchTable).each(function() {

            var $thisSubRow = $(this).parents('tr');
            var thisCreditNoteQuantity = $(this).val();
            if ((thisCreditNoteQuantity).trim() != "" && isNaN(parseFloat(thisCreditNoteQuantity))) {
                p_notification(false, eb.getMessage('ERR_CREDIT_VALQUANTITY'));
                $(this).focus();
                return qtyTotal = false;
            }

            thisCreditNoteQuantity = (isNaN(parseFloat(thisCreditNoteQuantity))) ? 0 : parseFloat(thisCreditNoteQuantity);

            // if a conversion is not set, it is assumed to be the base (eg: serial checkbox doesnt have multiple UOMs)

            // check if trasnfer qty is greater than available qty
            var thisAvailableQuantity = $("input[name='availableProductSubQuantity']", $thisSubRow).val();
            thisAvailableQuantity = (isNaN(parseFloat(thisAvailableQuantity))) ? 0 : parseFloat(thisAvailableQuantity);
            var thisAvailableQuantityByBase = thisAvailableQuantity;

            var thisCreditNoteQuantityByBase = thisCreditNoteQuantity;

            if (thisCreditNoteQuantityByBase > thisAvailableQuantityByBase) {
                p_notification(false, eb.getMessage('ERR_CREDIT_QUANTITY'));
                $(this).focus();

                return qtyTotal = false;
            }

            qtyTotal = qtyTotal + (thisCreditNoteQuantity);

            // if a product transfer is present, prepare array to be sent to backend

            if ((thisCreditNoteQuantity) > 0) {

                var thisSubProduct = {};
                thisSubProduct.batchID = '';
                thisSubProduct.serialID = '';
                thisSubProduct.bathchCode = '';
                thisSubProduct.serialCode = '';

                if ($(".batchCode", $thisSubRow).data('id')) {
                    thisSubProduct.batchID = $(".batchCode", $thisSubRow).data('id');
                	thisSubProduct.bathchCode = $(".batchCode", $thisSubRow).html();
                }

                if ($(".serialID", $thisSubRow).data('id')) {
                    thisSubProduct.serialID = $(".serialID", $thisSubRow).data('id');
                	thisSubProduct.serialCode = $(".serialID", $thisSubRow).html();
                }

                thisSubProduct.qtyByBase = thisCreditNoteQuantity;

                subProducts.push(thisSubProduct);
            }

        });

        if (checkSubProduct[thisVals.invoiceproductID] != undefined) {
            if (qtyTotal != $("input[name='creditNoteQuanity']", $thisParentRow).val()) {
                p_notification(false, eb.getMessage('ERR_CREDIT_TOTAL_SUBQUAN'));
                return qtyTotal = false;
            } else {
                checkSubProduct[thisVals.invoiceproductID] = 1;
            }
        }

        // to break out form $.each and exit function
        if (qtyTotal === false)
            return false;

        // ideally, since the individual batch/serial quantity is checked to be below the available qty,
        // the below condition should never become true
        if (qtyTotal > thisVals.availableQuantity.qty) {
            p_notification(false, eb.getMessage('ERR_CREDIT_TOTAL_INVQUAN'));
            return false;
        }

        creditNoteSubProducts[thisVals.invoiceproductID] = subProducts;
        SubProductsQty[thisVals.invoiceproductID] = qtyTotal;
        var $creditNoteQty = $("input[name='creditNoteQuanity']", $thisParentRow).prop('readonly', true);
        if (checkSubProduct[thisVals.invoiceproductID] == undefined) {
            $creditNoteQty.val(qtyTotal).change();
        }
        $("#unitPrice", $thisParentRow).trigger(jQuery.Event("focusout"));

        return true;
    }

    function getCurrentProductData($thisRow) {
        var discountType = '';
        if ($("input[name='discount']", $thisRow).hasClass('value')) {
            discountType = 'value';
        } else if ($("input[name='discount']", $thisRow).hasClass('precentage')) {
            discountType = 'precentage';
        }
        var invPID = $thisRow.data('dPID');
        var availableqty = $("input[name='invoicedQuantity']", $thisRow).val();
        if (isNaN(availableqty)) {
            availableqty = 0;
        }
        var creditNoteqty = $("input[name='creditNoteQuanity']", $thisRow).val();
        if (creditNoteqty == '') {
            creditNoteqty = 0;
        }
        var thisVals = {
            productID: $thisRow.data('id'),
            invoiceproductID: invPID,
            locationProductID: $thisRow.data('locationproductid'),
            documentTypeID: $thisRow.data('dtid'),
            documentID: $thisRow.data('did'),
            productCode: $("input[name='itemCode']", $thisRow).data('PC'),
            productName: $("input[name='itemCode']", $thisRow).data('PN'),
            giftCard: $("input[name='itemCode']", $thisRow).data('GC'),
            productPrice: $("input[name='unitPrice']", $thisRow).val(),
            productDiscount: $("input[name='discount']", $thisRow).val(),
            productDiscountType: discountType,
            productTotal: productsTotal[invPID],
            pTax: productsTax[invPID],
            productType: $("input[name='itemCode']", $thisRow).data('PT'),
            availableQuantity: {
                qty: availableqty,
            },
            creditNoteQuantity: {
                qty: creditNoteqty,
            }
        };

        return thisVals;
    }

    function getDirectCrdtNoteCurrentProductData($thisRow) {
        
        var discountType = '';
        if ($("input[name='discount']", $thisRow).hasClass('value')) {
            discountType = 'value';
        } else if ($("input[name='discount']", $thisRow).hasClass('precentage')) {
            discountType = 'precentage';
        }
        var invPID = $thisRow.data('dPID');
        var availableqty = $("input[name='invoicedQuantity']", $thisRow).val();
        if (isNaN(availableqty)) {
            availableqty = 0;
        }
        var creditNoteqty = $("input[name='creditNoteQuanity']", $thisRow).val();
        if (creditNoteqty == '') {
            creditNoteqty = 0;
        }
        var thisVals = {
            productID: $("#item_code", $thisRow).data('PID'),
            invoiceproductID: invPID,
            locationProductID: $thisRow.data('locationproductid'),
            documentTypeID: $thisRow.data('dtid'),
            documentID: $thisRow.data('did'),
            productCode: $("#item_code", $thisRow).data('PC'),
            productName: $("#item_code", $thisRow).data('PN'),
            giftCard: $("#item_code", $thisRow).data('GC'),
            productPrice: $("input[name='unitPrice']", $thisRow).val(),
            productDiscount: $("input[name='discount']", $thisRow).val(),
            productDiscountType: discountType,
            productTotal: productsTotal[$("#item_code", $thisRow).data('PID')],
            pTax: productsTax[$("#item_code", $thisRow).data('PID')],
            productType: $("#item_code", $thisRow).data('PT'),
            availableQuantity: {
                qty: availableqty,
            },
            creditNoteQuantity: {
                qty: creditNoteqty,
            }
        };
        

        return thisVals;
    }



    $productTable.on('focusout', '#creditNoteQuanity,#unitPrice,#discount,.uomqty, .uomPrice', function() {
        var $thisRow = $(this).parents('tr');
        var productID = $thisRow.data('id');
        var invoieProductID = $thisRow.data('dPID');
        var checkedTaxes = Array();
        $thisRow.find('input:checkbox.addNewTaxCheck').each(function() {
            if (this.checked) {
                var cliTID = this.id;
                var tTaxID = cliTID.split('_')[1];
                checkedTaxes.push(tTaxID);
            }
        });
        var productType = $(this).parents('tr').find('#itemCode').data('PT');
        var creditNoteQty = $(this).parents('tr').find('#creditNoteQuanity').val();
        if (productType == 2 && creditNoteQty == 0) {
            creditNoteQty = 1;
        }
        var tmpItemTotal = (toFloat($(this).parents('tr').find('#unitPrice').val()) * toFloat(creditNoteQty));
        var tmpItemCost = tmpItemTotal;
        var tmpItemQuentity = creditNoteQty;

        if ($(this).parents('tr').find('#discount').hasClass('value')) {
            var tmpPDiscountvalue = isNaN($(this).parents('tr').find('#discount').val()) ? 0 : $(this).parents('tr').find('#discount').val();
            if (tmpPDiscountvalue > 0) {
                tmpItemCost -= tmpPDiscountvalue * tmpItemQuentity;
            }
        } else {
            var tmpPDiscount = isNaN($(this).parents('tr').find('#discount').val()) ? 0 : $(this).parents('tr').find('#discount').val();
            if (tmpPDiscount > 0) {
                tmpItemCost -= (tmpItemTotal * toFloat(tmpPDiscount) / toFloat(100));
            }
        }

		if ($('#total_discount_rate').val() != '' && $('input[name=discount_type]:checked').val() == 'presentage') {
            var invoiceDiscountValue = $('#total_discount_rate').val();
            tmpItemCost -= (tmpItemCost * (toFloat(invoiceDiscountValue) / toFloat(100)));
        }
        if ($('#promotion_discount_rate').val() != '' && $('input[name=promotion_discount_type]:checked').val() == 'presentage') {
            var promotionDiscountValue = $('#promotion_discount_rate').val();
            tmpItemCost -= (tmpItemCost * (toFloat(promotionDiscountValue) / toFloat(100)));
        }

        if (checkedTaxes) {
            currentItemTaxResults = calculateItemCustomTax(tmpItemCost, checkedTaxes);
            currentTaxAmount = (currentItemTaxResults == null) ? null : currentItemTaxResults.tTA;
        } else {
            currentTaxAmount = 0;
            currentItemTaxResults = '';
        }

        var itemCost = toFloat(tmpItemCost) + toFloat(currentTaxAmount);
        productsTotal[invoieProductID] = itemCost;
        productsTax[invoieProductID] = currentItemTaxResults;
        $(this).parents('tr').find('#addNewTotal').html(accounting.formatMoney(itemCost));
        if (productsTax[invoieProductID] != null) {
            if (jQuery.isEmptyObject(productsTax[invoieProductID].tL)) {
                $(this).parents('tr').find('#taxApplied').addClass('glyphicon glyphicon-unchecked');
            } else {
                $(this).parents('tr').find('#taxApplied').removeClass('glyphicon glyphicon-unchecked');
                $(this).parents('tr').find('#taxApplied').addClass('glyphicon glyphicon-check');
            }
            setTaxListForProduct(productID, $thisRow, currentItemTaxResults.tL);
        }
    });

    function clearDirectCreditNoteAddLine($thisRow)
    {
        $('.directCQty').children('.uomqty').val('');
        $('.dirUPrc').children('.uomPrice').val('');
        $("input[name='discount']", $currentRow).removeClass('precentage');
        $("input[name='discount']", $currentRow).removeClass('value');
        $("input[name='discount']", $currentRow).val('');
        $('#addTaxUl',$currentRow).find('li.addedTax').remove();
        
        $currentRow.find('#taxApplied').removeClass('glyphicon glyphicon-check');
        $currentRow.find('#taxApplied').addClass('glyphicon glyphicon-unchecked')

        $('#addNewTotal',$currentRow).html(0.00);
        $('#item_code').remove('selectpicker')
        $('#item_code').selectpicker();
        loadDropDownFromDatabase('/productAPI/search-location-products-for-dropdown', locationID, 0, '#item_code', '', '', 'purchaseInvoice');
        $('#item_code').selectpicker('refresh');
    }

    function setDirectCreditNoteTotalCost() {
        var subTotal = 0;
        for (var i in creditNoteProducts) {
            subTotal += toFloat(creditNoteProducts[i].productTotal);
        }
        $('#directCNProductTable #directSubtotal').html(accounting.formatMoney(subTotal));
        if ($('#deliveryChargeEnable').is(':checked')) {
            var deliveryAmount = $('#deliveryCharge').val();
            subTotal += toFloat(deliveryAmount);
        }

        $('#directFinaltotal').html(accounting.formatMoney(subTotal));
    }

    function setCreditNoteTotalCost() {
        var subTotal = 0;
        for (var i in creditNoteProducts) {
            subTotal += toFloat(creditNoteProducts[i].productTotal);
        }
        $('#subtotal').html(accounting.formatMoney(subTotal));
        if ($('#deliveryChargeEnable').is(':checked')) {
            var deliveryAmount = $('#deliveryCharge').val();
            subTotal += toFloat(deliveryAmount);
        }

        if ($('#total_discount_rate').val() != '' && $('input[name=discount_type]:checked').val() == 'Value') {
            subTotal = toFloat(subTotal) - toFloat($('#total_discount_rate').val());
        }
        if ($('#promotion_discount_rate').val() != '' && $('input[name=promotion_discount_type]:checked').val() == 'Value') {
            subTotal = toFloat(subTotal) - toFloat($('#promotion_discount_rate').val());
        }

        $('#finaltotal').html(accounting.formatMoney(subTotal));
    }

    $('#total_discount_rate').on('keyup',function(){
        if($('input[name=discount_type]:checked').val() == 'Value'){
            if(toFloat($(this).val()) > toFloat(copiedCreditNoteDiscountAmount)){
                p_notification(false, eb.getMessage('ERR_CREDIT_NOTE_TOTAL_DISC'));
                $('#total_discount_rate').val(toFloat(copiedCreditNoteDiscountAmount));
            }
            setCreditNoteTotalCost();

        }
    });

    $('#promotion_discount_rate').on('keyup',function(){
       if($('input[name=promotion_discount_type]:checked').val() == 'Value'){
            if(toFloat($(this).val()) > toFloat(copiedCrditNotePromotionDiscountAmount)){
                p_notification(false, eb.getMessage('ERR_CREDIT_NOTE_PROMO_TOTAL_DISC'));
                $('#promotion_discount_rate').val(toFloat(copiedCrditNotePromotionDiscountAmount));
            }
            setCreditNoteTotalCost();
        }
    });


    $('#creditNoteSaveButton').on('click', function(e) {
        e.preventDefault();

        if (creditNoteDirectType || creditNoteBuyBackType) {
            if (jQuery.isEmptyObject(directCreditProducts)) {
                p_notification(false, eb.getMessage('ERR_CREDIT_ADD_PROD'));
                return false;
            }
            creditNoteProducts = directCreditProducts;
            saveDirectCreditNoteData();
        } else {

            if (useInvoiceReturn) {
                InvoiceID = ReturnInvoiceID;
                var editCount = 0;


                $('#add-new-item-row > tr').each(function(){
                     if ($(this).hasClass('edit-row')) {
                         editCount ++;
                     }
                });

                if (editCount > 0) {
                    p_notification(false, eb.getMessage('ERR_ALL_LOADED_ITEMS_TO_ADD'));
                    return false;
                }


            }

            if ((InvoiceID == '' || InvoiceID == null) && !creditNoteDirectType && !creditNoteBuyBackType){
                p_notification(false, eb.getMessage('ERR_CREDIT_SELECT_INVOICENO'));
                return false;
            } else if (jQuery.isEmptyObject(creditNoteProducts)) {
                p_notification(false, eb.getMessage('ERR_CREDIT_ADD_PROD'));
                return false;
            }else if(productBeingEdited.length != 0){
                p_notification(false, eb.getMessage('ERR_CREDIT_SAVE_PROD'));
                return false;
            }
            saveCreditNoteData();
        }
        return false;
    });

    function saveCreditNoteData() {

        for (var i in checkSubProduct) {
            var $currentRow = getAddRow(i);
            if (!$($currentRow).find('.edit-modal').parent('td').hasClass('hidden')) {
                if (checkSubProduct[i] == 0) {
                    p_notification(false, eb.getMessage('ERR_CREDIT_ADD_SUBPROD', creditNoteProducts[i].productCode));
                    return;
                }
            }
        }
        var formData = {
            creditNoteCode: $('#creditNoteNo').val(),
            location: $('#currentLocation').val(),
            date: $('#creditNoteDate').val(),
            customer: $('#customer').val(),
            customerID: customerID,
            creditNoteTotal:$('#finaltotal').text().replace(/,/g, '')
        };
        var discountType = '';
        if ($('#disc_value').is(":checked")) {
        	discountType = 'value';
        } else if($('#disc_presentage').is(":checked")) {
        	discountType = 'precentage';
        }

        var discountValue = 0.00;
        if ($('#total_discount_rate').val() != '') {
        	discountValue = $('#total_discount_rate').val();
        }

        var promotionDiscountType = '';
        if ($('#promo_disc_value').is(":checked")) {
        	promotionDiscountType = 'value';
        } else if($('#promo_disc_presentage').is(":checked")) {
        	promotionDiscountType = 'precentage';
        }

        var promotionDiscountValue = 0.00;
        if ($('#promotion_discount_rate').val() != '') {
        	promotionDiscountValue = $('#promotion_discount_rate').val();
        }

        relatedInvoiceReturns = [];

        if (useInvoiceReturn) {
            relatedInvoiceReturns = $('#invoiceReturnNo').val();
        }


        if (validateTransferForm(formData)) {
            $('#creditNoteSaveButton').prop('disabled', true);
            eb.ajax({
                type: 'POST',
                url: BASE_URL + '/credit-note-api/saveCreditNoteDetails',
                data: {
                    creditNoteCode: formData.creditNoteCode,
                    locationID: locationID,
                    products: creditNoteProducts,
                    subProducts: creditNoteSubProducts,
                    date: formData.date,
                    customerID: customerID,
                    customerName: $('#customer').val(),
                    creditNoteTotalPrice: $('#finaltotal').text().replace(/,/g, ''),
                    creditNoteComment: $('#comment').val(),
                    paymentTermID: $('#paymentTerm').val(),
                    invoiceID: InvoiceID,
                    locationProduct: locationProducts,
                    customCurrencyId: $('#customCurrencyId').val(),
                    creditNoteCustomCurrencyRate: customCurrencyRate,
                    creditNoteInvoiceDiscountType: discountType,
                    creditNoteInvoiceDiscountAmount: discountValue,
                    creditNotePromotionDiscountType: promotionDiscountType,
                    creditNotePromotionDiscountAmount: promotionDiscountValue,
                    creditNoteDirectType : creditNoteDirectType,
                    creditNoteBuyBackType: creditNoteBuyBackType,
                    invFlag: invFlag,
                    invCloseFlag: invCloseFlag,
                    ignoreBudgetLimit: ignoreBudgetLimitFlag,
                    useInvoiceReturn: useInvoiceReturn,
                    relatedInvoiceReturns: relatedInvoiceReturns
                },
                success: function(respond) {
                    if (respond.status) {
                        p_notification(respond.status, respond.msg);
                        var fileInput = document.getElementById('documentFiles');
                        if(fileInput.files.length > 0) {
                            var form_data = false;
                            if (window.FormData) {
                                form_data = new FormData();
                            }
                            form_data.append("documentID", respond.data.creditNoteID);
                            form_data.append("documentTypeID", 6);
                            
                            for (var i = 0; i < fileInput.files.length; i++) {
                                form_data.append("files[]", fileInput.files[i]);
                            }

                            eb.ajax({
                                url: BASE_URL + '/store-files',
                                type: 'POST',
                                processData: false,
                                contentType: false,
                                data: form_data,
                                success: function(res) {
                                }
                            });
                        }
                        documentPreview(BASE_URL + '/credit-note/viewCreditNoteReceipt/' + respond.data.creditNoteID, 'documentpreview', "/credit-note-api/send-email", function($preview) {
                            $preview.on('hidden.bs.modal', function() {
                                if (!$("#preview:visible").length) {
                                    window.location.reload();
                                }
                            });
                        });
                    } else {
                        if (respond.data == "NotifyBudgetLimit") {
                            bootbox.confirm(respond.msg+' ,Are you sure you want to save this credit note ?', function(result) {
                                if (result == true) {
                                    ignoreBudgetLimitFlag = true;
                                    saveCreditNoteData();
                                } else {
                                    setTimeout(function(){ 
                                        location.reload();
                                    }, 3000);
                                }
                            });
                        } else {
                            p_notification(respond.status, respond.msg);
                            $('#creditNoteSaveButton').prop('disabled', false);
                        }
                    }
                }
            });
        }
    }

    function saveDirectCreditNoteData() {
        var formData = {
            creditNoteCode: $('#creditNoteNo').val(),
            location: $('#currentLocation').val(),
            date: $('#creditNoteDate').val(),
            customer: $('#customer').val(),
            customerID: customerID
        };
        var discountType = '';
        if ($('#disc_value').is(":checked")) {
            discountType = 'value';
        } else if($('#disc_presentage').is(":checked")) {
            discountType = 'precentage';
        }

        var discountValue = 0.00;
        if ($('#total_discount_rate').val() != '') {
            discountValue = $('#total_discount_rate').val();
        }

        var promotionDiscountType = '';
        if ($('#promo_disc_value').is(":checked")) {
            promotionDiscountType = 'value';
        } else if($('#promo_disc_presentage').is(":checked")) {
            promotionDiscountType = 'precentage';
        }

        var promotionDiscountValue = 0.00;
        if ($('#promotion_discount_rate').val() != '') {
            promotionDiscountValue = $('#promotion_discount_rate').val();
        }

        if (validateTransferForm(formData)) {
            $('#creditNoteSaveButton').prop('disabled', true);
            eb.ajax({
                type: 'POST',
                url: BASE_URL + '/credit-note-api/saveCreditNoteDetails',
                data: {
                    creditNoteCode: formData.creditNoteCode,
                    locationID: locationID,
                    products: directCreditProducts,
                    subProducts: creditNoteSubProducts,
                    date: formData.date,
                    customerID: customerID,
                    customerName: $('#customer').val(),
                    creditNoteTotalPrice: $('#drFinaltotal').text().replace(/,/g, ''),
                    creditNoteComment: $('#comment').val(),
                    paymentTermID: $('#paymentTerm').val(),
                    invoiceID: InvoiceID,
                    locationProduct: locationProducts,
                    customCurrencyId: $('#customCurrencyId').val(),
                    creditNoteCustomCurrencyRate: customCurrencyRate,
                    creditNoteInvoiceDiscountType: discountType,
                    creditNoteInvoiceDiscountAmount: discountValue,
                    creditNotePromotionDiscountType: promotionDiscountType,
                    creditNotePromotionDiscountAmount: promotionDiscountValue,
                    creditNoteDirectType : creditNoteDirectType,
                    creditNoteBuyBackType : creditNoteBuyBackType,
                    invFlag: invFlag,
                    invCloseFlag: invCloseFlag,
                    dimensionData: dimensionData,
                    ignoreBudgetLimit: ignoreBudgetLimitFlag
                },
                success: function(respond) {
                    if (respond.status) {
                        p_notification(respond.status, respond.msg);
                        var fileInput = document.getElementById('documentFiles');
                        if(fileInput.files.length > 0) {
                            var form_data = false;
                            if (window.FormData) {
                                form_data = new FormData();
                            }
                            form_data.append("documentID", respond.data.creditNoteID);
                            form_data.append("documentTypeID", 6);
                            
                            for (var i = 0; i < fileInput.files.length; i++) {
                                form_data.append("files[]", fileInput.files[i]);
                            }

                            eb.ajax({
                                url: BASE_URL + '/store-files',
                                type: 'POST',
                                processData: false,
                                contentType: false,
                                data: form_data,
                                success: function(res) {
                                }
                            });
                        }
                        
                        documentPreview(BASE_URL + '/credit-note/viewCreditNoteReceipt/' + respond.data.creditNoteID, 'documentpreview', "/credit-note-api/send-email", function($preview) {
                            $preview.on('hidden.bs.modal', function() {
                                if (!$("#preview:visible").length) {
                                    window.location.reload();
                                }
                            });
                        });
                    } else {
                        if (respond.data == "NotifyBudgetLimit") {
                            bootbox.confirm(respond.msg+' ,Are you sure you want to save this direct credit note ?', function(result) {
                                if (result == true) {
                                    ignoreBudgetLimitFlag = true;
                                    saveDirectCreditNoteData();
                                } else {
                                    setTimeout(function(){ 
                                        location.reload();
                                    }, 3000);
                                }
                            });
                        } else {
                            p_notification(respond.status, respond.msg);
                            $('#creditNoteSaveButton').prop('disabled', false);
                        }
                    }
                }
            });
        }
    }

    $('#customCurrencyId').on('change', function() {
        eb.ajax({
            type: 'POST',
            url: BASE_URL + '/currencySymbol/getCustomCurrencyByCustomCurrencyId',
            data: {customCurrencyId: $(this).val()},
            success: function(respond) {
                if (respond.status == true) {
                    $('#add-new-item-row > tr').each(function(){
                         if ($('#discount',this).hasClass('value')) {
                             $('.discountSymbol',this).text(respond.data.currencySymbol);
                         }
                    });
                    $('.cCurrency').text(respond.data.currencySymbol);
                }
            }
        });
    });

    function validateTransferForm(formData) {

        if (formData.location == null || formData.location == "") {
            p_notification(false, eb.getMessage('ERR_CREDIT_SELECT_LOCAT'));
            $('#currentLocation').focus();
            return false;
        } else if (formData.creditNoteCode == null || formData.creditNoteCode == "") {
            p_notification(false, eb.getMessage('ERR_CREDIT_NUM_BLANK'));
            return false;
        } else if (formData.date == null || formData.date == "") {
            p_notification(false, eb.getMessage('ERR_CREDIT_DATE_BLANK'));
            $('#creditNoteDate').focus();
            return false;
        } else if (!isDate(formData.date)) {
            p_notification(false, eb.getMessage('ERR_CREDIT_DATE_FORMAT'));
            $('#creditNoteDate').focus();
            return false;
        } else if (formData.customer == null || formData.customer == "") {
            p_notification(false, eb.getMessage('ERR_CREDIT_SELECT_CUST'));
            $('#customer').focus();
            return false;
        } else if (formData.customerID == null || formData.customerID == "") {
            p_notification(false, eb.getMessage('ERR_CREDIT_INVALID_CUST'));
            $('#customer').focus();
            return false;
        } else if (jQuery.isEmptyObject(creditNoteProducts)) {
            p_notification(false, eb.getMessage('ERR_CREDIT_ADD_ONEPROD'));
            return false;
        } else if (parseFloat(formData.creditNoteTotal) < 0) {
            p_notification(false, eb.getMessage('ERR_CREDIT_TOTAL_MINUS'));
            return false;
        }

        return true;
    }

    function isDate(txtDate)
    {
        var currVal = txtDate;
        if (currVal == '')
            return false;

        //Declare Regex
        var rxDatePattern = /^(\d{4})(\/|-)(\d{1,2})(\/|-)(\d{1,2})$/;
        var dtArray = currVal.match(rxDatePattern); // is format OK?

        if (dtArray == null)
            return false;

        //Checks for mm/dd/yyyy format.
        dtMonth = dtArray[3];
        dtDay = dtArray[5];
        dtYear = dtArray[1];
        if (dtMonth < 1 || dtMonth > 12)
            return false;
        else if (dtDay < 1 || dtDay > 31)
            return false;
        else if ((dtMonth == 4 || dtMonth == 6 || dtMonth == 9 || dtMonth == 11) && dtDay == 31)
            return false;
        else if (dtMonth == 2)
        {
            var isleap = (dtYear % 4 == 0 && (dtYear % 100 != 0 || dtYear % 400 == 0));
            if (dtDay > 29 || (dtDay == 29 && !isleap))
                return false;
        }
        return true;
    }

    var nowTemp = new Date();
    var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);

    var checkin = $('#creditNoteDate').datepicker({onRender: function(date) {
            return date.valueOf() < now.valueOf() ? ((U_DATE_R_OVERRIDE)?'':'disabled') : '';
        },
        format: 'yyyy-mm-dd'
    }).on('changeDate', function(ev) {
        var invDate=new Date(Date.parse(invoiceDate));
        invDate.setDate(invDate.getDate()-1);
        if(ev.date < invDate){
            p_notification(false, eb.getMessage('ERR_INVOICE_ENTER_DATE', invoiceDate));
            $('#creditNoteDate').val('');
        }
        checkin.hide();
    }).data('datepicker');


    $('#item_code').on('change', function() {
        $parentRow = $(this).parents('tr');
        clearAddNewRow($parentRow);
        clearModalWindow();
        $('#addNewTax',$parentRow).attr('disabled', false);
        if ($(this).val() > 0 && $(this).val() != currentlySelectedProduct) {
            currentlySelectedProduct = $(this).val();
            selectedProduct = $(this).val();
            eb.ajax({
                type: 'POST',
                url: BASE_URL + '/productAPI/get-location-product-details',
                data: {productID: selectedProduct, locationID: locationID},
                success: function(respond) {
                    if (InvoiceID != undefined) {
                        if (respond.data.pT != 2) {
                            p_notification(false, eb.getMessage('ERR_CREDIT_LINK_INVOICE'));
                            return false;
                        }
                    }

                    if (respond.data.pT == 1) {
                        $('#directInvoiceNo').attr('disabled', true);
                        $('#closeInvoiceID').attr('disabled', true);
                    }
                    var currentElem = new Array();
                    currentElem[selectedProduct] = respond.data;
                    locationProducts = $.extend(locationProducts, currentElem);
                    $('#addNewTax',$parentRow).attr('disabled', false);

                    var baseUom;
                    for (var j in locationProducts[selectedProduct].uom) {
                        if (locationProducts[selectedProduct].uom[j].pUBase == 1) {
                            baseUom = locationProducts[selectedProduct].uom[j].uomID;
                        }
                    }

                    setTaxListForProductForDirectCredit(selectedProduct,$parentRow);
                    $('#directUnitPrice',$parentRow).val(locationProducts[selectedProduct].dSP).addUomPrice(locationProducts[selectedProduct].uom);
                    if (locationProducts[selectedProduct].pPDV != null && locationProducts[selectedProduct].dPEL == 1) {
                        $('#directDiscount',$parentRow).val(locationProducts[selectedProduct].pPDV).addUomPrice(locationProducts[selectedProduct].uom, baseUom);
                        $('#directDiscount',$parentRow).siblings('.uom-price-select').find('.uom_price_drop_down').attr('disabled', true);
                        $(".sign", $parentRow).text('Rs');
                        $('#directDiscount',$parentRow).attr('disabled', false);
                        $('#directDiscount',$parentRow).addClass('value');
                    } else if (locationProducts[selectedProduct].pPDP != null && locationProducts[selectedProduct].dPEL == 1) {
                        $('#directDiscount',$parentRow).val(locationProducts[selectedProduct].pPDP);
                        $('#directDiscount',$parentRow).attr('disabled', false);
                        $(".sign", $parentRow).text('%');
                        $('#directDiscount',$parentRow).addClass('precentage');
                    } else if ((locationProducts[selectedProduct].pPDP == null && locationProducts[selectedProduct].pPDV == null) || locationProducts[selectedProduct].dPEL == 0) {
                        $('#directDiscount',$parentRow).attr('disabled', true);
                    }
                    $('.uomPrice',$parentRow).attr("id", "itemUnitPrice");
                    $('#directCreditQuanity',$parentRow).parent().addClass('input-group');
                    $('#directCreditQuanity',$parentRow).addUom(locationProducts[selectedProduct].uom);
                    $('.uomqty',$parentRow).attr("id", "itemQuantity");
                    $('#item_code',$parentRow).data('PN', locationProducts[selectedProduct].pN);
                    $("#item_code",$parentRow).data('PT', locationProducts[selectedProduct].pT);
                    $("#item_code",$parentRow).data('PC', locationProducts[selectedProduct].pC);
                    $("#item_code",$parentRow).data('pId', selectedProduct);
                }
            });

        }
    });

    function clearAddNewRow($parentRow) {
        $('#directUnitPrice',$parentRow).val('').siblings('.uomPrice,.uom-price-select').remove();
        $("#directUnitPrice",$parentRow).show();
        $("#directCreditQuanity",$parentRow).val('').siblings('.uomqty,.uom-select').remove();
        $("#directCreditQuanity",$parentRow).show();
        $('#directDiscount',$parentRow).val('').siblings('.uomPrice,.uom-price-select').remove();
        $('#directDiscount',$parentRow).show();
        $('#drAddNewTotal',$parentRow).html('0.00');
        $('#taxApplied',$parentRow).removeClass('glyphicon glyphicon-checked');
        $('#taxApplied',$parentRow).addClass('glyphicon glyphicon-unchecked');
        $('.tempLi',$parentRow).remove();
        $('.uomLi',$parentRow).remove();
        $('#uomAb',$parentRow).html('');
        selectedProduct = '';
        selectedProductQuantity = '';
        selectedProductUom = '';
    }

    function clearModalWindow() {
        $('.tempProducts').remove();
        $('.cloneSerials').remove();
        $('#returnProductCodeBatch').html('');
        $('#returnProductQuantityBatch').html('');
        $('#returnProductCodeSerial').html('');
        $('#returnProductQuantitySerial').html('');
        $('#batchAddScreen').addClass('hidden');
        $('#serialAddScreen').addClass('hidden');
        $('#addNewSerial input[type=text]').val('');
        $('#proBatchAddNew').removeClass('hidden');
        batchCount = 0;
        directBatchProducts = {};
        directSerialProducts = {};

    }

    function setTaxListForProductForDirectCredit(productID, $parentRow) {
        if ((!jQuery.isEmptyObject(locationProducts[productID].tax))) {
            productTax = locationProducts[productID].tax;
            $('#taxApplied',$parentRow).removeClass('glyphicon glyphicon-unchecked');
            $('#taxApplied',$parentRow).addClass('glyphicon glyphicon-check');
            $('.tempLi',$parentRow).remove();
            for (var i in productTax) {
                var clonedLi = $($('#sampleLi',$parentRow).clone()).removeClass('hidden').attr('id', 'li_' + i).addClass("tempLi");
                clonedLi.children(".taxChecks").attr('id', productID + '_' + i).prop('checked', true).addClass('addNewTaxCheck');
                if (productTax[i].tS == 0) {
                    clonedLi.children(".taxChecks").attr('disabled', 'disabled');
                    clonedLi.children(".taxName").addClass('crossText');
                }
                clonedLi.children(".taxName").html('&nbsp&nbsp' + productTax[i].tN + '&nbsp&nbsp' + productTax[i].tP + '%').attr('for', productID + '_' + i);
                clonedLi.insertBefore($('#sampleLi',$parentRow));

            }
        } else {
            $('#taxApplied',$parentRow).addClass('glyphicon glyphicon-unchecked');
            $('#addNewTax',$parentRow).attr('disabled', 'disabled');
        }

    }


    $('#drCreditProductTable').on('focusout', '#item_code,#directCreditQuanity,#directUnitPrice,#directDiscount,.uomqty,.uomPrice', function() {
        var checkedTaxes = Array();
        if (creditNoteDirectType || creditNoteBuyBackType) {
            var row = $(this).parents('tr');
            $('input:checkbox.addNewTaxCheck', $(this).parents('tr')).each(function() {
                if (this.checked) {
                    var cliTID = this.id;
                    var tTaxID = cliTID.split('_')[1];
                    checkedTaxes.push(tTaxID);
                }
            });
            var productID = $('#item_code', $(this).parents('tr')).data('pId');
            if (productID) {
                productID = productID;
            } else {
                productID = row.attr('id').split("_");
                productID = productID[1];            
            }

            var qty = $("input[name='directCreditQuanity']", $(this).parents('tr')).val();
            var unitPrice = $("input[name='directUnitPrice']", $(this).parents('tr')).val();
            var uP = $(this).parents('tr').find('.uomPrice').val();
            if (unitPrice != "" && uP != "") {
                // $("input[name='directUnitPrice']", $(this).parents('tr')).val(parseFloat(unitPrice));
                // $(this).parents('tr').find('.uomPrice').val(parseFloat(unitPrice));
                $(this).parents('tr').find("input[name='directUnitPrice']").val(parseFloat(unitPrice));
            }
            var discount = $("input[name='directDiscount']", $(this).parents('tr')).val();
            if ($('#item_code', row).data('PT') == 2 && qty == 0) {
                qty = 1;
            }
            var discountType;
            if (locationProducts[productID]) {
                if (locationProducts[productID].pPDP != null) {
                    discountType = "Per";
                } else if (locationProducts[productID].pPDV != null) {
                    discountType = "Val";
                } else {
                   discountType = "Non";
                }
                if (discountType == "Val") {
                    var newDiscount = validateDiscount(productID,discountType,discount, unitPrice);
                    var tmpItemCost = calculateTaxFreeItemCostByValueDiscount(unitPrice, qty, newDiscount);
                    if (tmpItemCost < 0) {
                        tmpItemCost = 0;
                    }
                } else if (discountType == "Per") {
                    var newDiscount = validateDiscount(productID,discountType,discount, unitPrice);
                    var tmpItemCost = calculateTaxFreeItemCost(unitPrice, qty, newDiscount);
                } else {
                    var newDiscount = 0;
                    var tmpItemCost = calculateTaxFreeItemCost(unitPrice, qty, newDiscount);
                }

                $("input[name='directDiscount']", $(this).parents('tr')).val(newDiscount);
                currentItemTaxResults = calculateItemCustomTax(tmpItemCost, checkedTaxes);
                currentTaxAmount = (currentItemTaxResults == null) ? null : currentItemTaxResults.tTA;
                var itemCost = toFloat(tmpItemCost) + toFloat(currentTaxAmount);
                $('#drAddNewTotal', $(this).parents('tr')).html(accounting.formatMoney(itemCost));
                if (productID != undefined) {
                    var currentEl = new Array();
                    currentEl[productID+'_'+unitPrice] = itemCost;
                    productLineTotal = $.extend(productLineTotal, currentEl);
                }
            } else {
                $("input[name='directDiscount']", $(this).parents('tr')).val(discount);
                var tmpItemCost = calculateTaxFreeItemCost(unitPrice, qty, discount);
                currentItemTaxResults = calculateItemCustomTax(tmpItemCost, checkedTaxes);
                currentTaxAmount = (currentItemTaxResults == null) ? null : currentItemTaxResults.tTA;
                var itemCost = toFloat(tmpItemCost) + toFloat(currentTaxAmount);
                $('#drAddNewTotal', $(this).parents('tr')).html(accounting.formatMoney(itemCost));
                if (productID != undefined) {
                    var currentEl = new Array();
                    currentEl[productID+'_'+unitPrice] = itemCost;
                    productLineTotal = $.extend(productLineTotal, currentEl);
                }
            }
        }
    });

    function validateDiscount(productID, discountType, currentDiscount, unitPrice) {
        var defaultProductData = locationProducts[productID];
        var newDiscount = 0;
        if (defaultProductData.dPEL == 1) {
            if (discountType == 'Val') {
                if (toFloat(defaultProductData.pPDV) !=0 && toFloat(currentDiscount) > toFloat(defaultProductData.pPDV)) {
                        newDiscount = toFloat(defaultProductData.pPDV);
                        p_notification(false, eb.getMessage('ERR_INVO_DISCOUNT_VALUE'));
                } else if (toFloat(defaultProductData.pPDV) == 0) {
                    if (toFloat(currentDiscount) > toFloat(unitPrice)) {
                        newDiscount = toFloat(unitPrice);
                    } else {
                        newDiscount = toFloat(currentDiscount);    
                    }
                } else {
                    newDiscount = toFloat(currentDiscount);
                }

            } else {
                if (toFloat(defaultProductData.pPDP) != 0 && toFloat(currentDiscount) > toFloat(defaultProductData.pPDP)) {
                        newDiscount = toFloat(defaultProductData.pPDP);
                        p_notification(false, eb.getMessage('ERR_INVO_DISC_PERCENTAGE'));
                } else if (toFloat(defaultProductData.pPDP) == 0) {
                    if (toFloat(currentDiscount) > 100 ) {
                        newDiscount = 100;
                        p_notification(false, eb.getMessage('ERR_PI_DISC_PERCENTAGE'));   
                    } else {
                        newDiscount = toFloat(currentDiscount);    
                    }
                } else {
                    newDiscount = toFloat(currentDiscount);
                }
            }
        } 
        return newDiscount;
    }

    $('#drCreditProductTable').on('change', '.taxChecks.addNewTaxCheck', function() {
        var allchecked = false;
        var $parentTaxSet = $(this).parents("td.taxSet");
        $parentTaxSet.find('input:checkbox.addNewTaxCheck').each(function() {
            if (this.checked && $(this).prop('disabled') === false) {
                allchecked = true;
            }
        });

        if (allchecked == false) {
            $parentTaxSet.find('#taxApplied').removeClass('glyphicon glyphicon-check');
            $parentTaxSet.find('#taxApplied').addClass('glyphicon glyphicon-unchecked');
        } else {
            $parentTaxSet.find('#taxApplied').removeClass('glyphicon glyphicon-unchecked');
            $parentTaxSet.find('#taxApplied').addClass('glyphicon glyphicon-check');
        }

        $(this).parents('tr').find(".uomPrice").trigger(jQuery.Event("focusout"));
        $(this).parents('tr').find("input[name='directCreditQuanity']").trigger(jQuery.Event("focusout"));
    });

    $('#drCreditProductTable').on('click', '#selectAll', function() {
        $(this).parents('.dropdown-menu').find('input:checkbox.addNewTaxCheck').each(function() {
            if (!this.checked) {
                this.checked = true;
            }
        });

        $(this).parents('.dropdown-menu').find('input:checkbox.addNewTaxCheck').trigger(jQuery.Event("change"));

    });

    $('#drCreditProductTable').on('click', '#deselectAll', function() {
        $(this).parents('.dropdown-menu').find('input:checkbox.addNewTaxCheck').each(function() {
            if (this.checked) {
                this.checked = false;
            }
        });

        $(this).parents('.dropdown-menu').find('input:checkbox.addNewTaxCheck').trigger(jQuery.Event("change"));

    });

    $('#drCreditProductTable').on('change', '.addNewUomR', function() {
        $('#uomAb').html(locationProducts[selectedProduct].uom[$(this).val()].uA);
        selectedProductUom = $(this).val();
    });


    $('#addItem').on('click', function() {
        var row = $(this).parents('tr');
        var uomqty = $("#directCreditQuanity").siblings('.uomqty').val();
        $("#directCreditQuanity").siblings('.uomqty').change();
        $('.tempy').remove();
        idNumber = 1;
        quon = 0;
        $('#prefix').val('');
        //validate same item usage
        var sameItemFlag = false;
        for (var i in directCreditProducts) {
            if(selectedProduct == directCreditProducts[i].pID && toFloat($('#directUnitPrice').val()) == toFloat(directCreditProducts[i].pUnitPrice) 
                && toFloat($('#directDiscount').val()) == toFloat(directCreditProducts[i].pDiscount)){
                sameItemFlag = true;

            }
        }

        if(sameItemFlag){
            p_notification(false, eb.getMessage('ERR_SAME_ITEM_IN_PI'));
            return false;
        }

        var issDate = eb.convertDateFormat('#creditNoteDate');
        var expD = $('#expire_date_autoFill').val('').datepicker().on('changeDate', function(ev) {
            expD.hide();
        }).data('datepicker');
        $('#quontity').val('');
        $('#startNumber').val('');
        $('#batch_code').val('');
        $('#wrtyDays').val('');
        $('#mf_date_autoFill').val('');
        var mfD = $('#mf_date_autoFill').datepicker({
            onRender: function(date) {
                return date.valueOf() > issDate.valueOf() ? 'disabled' : '';
            }
        }).on('changeDate', function(ev) {
            mfD.hide();
        }).data('datepicker');
        $('#temp_1').find('input[name=submit]').attr('disabled', false);
        $('#temp_1').find('input[name=prefix]').attr('disabled', false);
        $('#temp_1').find('input[name=startNumber]').attr('disabled', false);
        $('#temp_1').find('input[name=quontity]').attr('disabled', false);
        $('#temp_1').find('input[name=expireDateAutoFill]').attr('disabled', false);
        $('#temp_1').find('input[name=batch_code]').attr('disabled', false);
        $('#temp_1').find('input[name=mfDateAutoFill]').attr('disabled', false);
        $('#temp_1').find('input[name=wrtyDays]').attr('disabled', false);

        selectedProductUom = $("#directCreditQuanity").siblings('.uom-select').children('button').find('.selected').data('uomID');
        if (typeof (selectedProduct) == 'undefined' || selectedProduct == '') {
            p_notification(false, eb.getMessage('ERR_GRN_SELECT_PROD'));
        }
        else if (typeof (selectedProductUom) == 'undefined' || selectedProductUom == '') {
            p_notification(false, eb.getMessage('ERR_GRN_SELECT_UOM'));
        }
        else {
            var checkingPrice = toFloat($('#directUnitPrice').val());
            if ($('#directUnitPrice').val() == '') {
                p_notification(false, eb.getMessage('ERR_GRN_ENTER_UNITPRI'));
                $('#directUnitPrice').focus();
            } else if ($('#item_code').data('PT') != 2 && $('#directCreditQuanity').val() == '') {
                p_notification(false, eb.getMessage('ERR_GRN_ENTER_QUANTITY'));
            } else if (((uomqty == 0) || (uomqty == '')) && $('#item_code').data('PT') != 2) {
                p_notification(false, eb.getMessage('ERR_GRN_ENTER_QUANTITY'));
            } else {
                clearModalWindow();
                $('.cloneSerials').remove();
                $('#serialSample').children().children('#newSerialNumber').removeClass('serialNumberList');
                $('#serialSample').children().children('#newSerialBatch').removeClass('serialBatchList');
                $('#serialSample').children().children('#newSerialBatch').prop('disabled', false);
                if ($('#itemCode').data('PT') == 2) {
                    locationProducts[selectedProduct].bP = 0;
                    locationProducts[selectedProduct].sP = 0;
                }
                if (locationProducts[selectedProduct].bP == 1) {
                    $('#batchAddScreen').removeClass('hidden');
                    $('#newBatchMDate').addClass('white_bg');
                    $('#newBatchEDate').addClass('white_bg');
                }
                if (locationProducts[selectedProduct].sP == 1) {
                    $('#serialAddScreen').removeClass('hidden');
                }
                if (locationProducts[selectedProduct].sP == 1 && locationProducts[selectedProduct].bP == 0) {
                    $('#temp_1').find('input[name=mfDateAutoFill]').attr('disabled', true).removeClass('white_bg');
                    $('#temp_1').find('input[name=batch_code]').closest('td').hide();
                    $('#btch_code').hide();
                    $('#bCodeT').hide();
                    $('#bCode').hide();
                    $('.price_field').hide();
                    $('#temp_1').find('input[name=expireDateAutoFill]').addClass('white_bg');
                } else {
                    $('#temp_1').find('input[name=batch_code]').closest('td').show();
                    $('#btch_code').show();
                    $('.price_field').show();
                    $('#bCodeT').show();
                    $('#bCode').show();
                }

                if (locationProducts[selectedProduct].sP == 1 && locationProducts[selectedProduct].bP == 1) {
                    $('#batchAddScreen').addClass('hidden');
                    $('#temp_1').find('input[name=batch_code]').attr('disabled', false);
                    $('#temp_1').find('input[name=mfDateAutoFill]').attr('disabled', false).addClass('white_bg');
                }

                if (locationProducts[selectedProduct].bP == 0 && locationProducts[selectedProduct].sP == 0) {
                    $('#directUnitPrice', row).trigger('focusout');
                    addNewProductRow({}, {});
                    $('#directCreditQuanity').parent().removeClass('input-group');
                    $('#item_code').focus();
                } else {
                    selectedProductQuantity = $('#directCreditQuanity').siblings('.uomqty').val();
                    if (locationProducts[selectedProduct].bP == 1) {
                        $('#returnProductCodeBatch').html(locationProducts[selectedProduct].pC);
                        $('#returnProductQuantityBatch').html($('#directCreditQuanity').siblings('.uomqty').val());
                    }
                    if (locationProducts[selectedProduct].sP == 1) {
                        $('#addNewSerial input[type=text]').val('');
                        $('#returnProductCodeSerial').html(locationProducts[selectedProduct].pC);
                        $('#returnProductQuantitySerial').html($('#directCreditQuanity').val());
                        $('#serialSample').children().children('#newSerialBatch').addClass('serialBatchList');
                        $('#serialSample').children().children('#newSerialNumber').addClass('serialNumberList');

                        if (locationProducts[selectedProduct].bP == 0) {
                            $('#serialSample').children().children('#newSerialBatch').prop('disabled', true);
                        }
                        for (i = 0; i < selectedProductQuantity - 1; i++) {
                            var serialAddRowClone = $('#serialSample').clone().attr('id', i).addClass('cloneSerials');
                            $('#serialSample').children().children('#newSerialNumber').addClass('serialNumberList');
                            $('#serialSample').children().children('#newSerialBatch').addClass('serialBatchList');
                            serialAddRowClone.children().children('#newSerialNumber').addClass('serialNumberList');
                            serialAddRowClone.children().children('#newSerialBatch').addClass('serialBatchList');
                            serialAddRowClone.children().children('#newSerialEDate').attr('id', 'sEDate' + i).addClass('sEDate');
                            if (locationProducts[selectedProduct].bP == 0) {
                                $('#serialSample').children().children('#newSerialBatch').prop('disabled', true);
                                serialAddRowClone.children().children('#newSerialBatch').prop('disabled', true);
                            }
                            serialAddRowClone.insertBefore('#serialSample');
                        }
                        if(selectedProductQuantity<=100){
                            var dDate = new Date(Date.parse($('#returnDate').val()));
                            $('.sEDate').datepicker();
                        }
                    }
                    $('#addDirectCreditProductsModal').modal('show');

                }
            }

        }
    });

    function addNewProductRow(directBatchProducts, directSerialProducts) {
        var $parentTr = '';
        $parentTr = $('#item_code').parents('tr');
        var productID = selectedProduct;
        var iC = $('#item_code').data('PC');
        var pT = $('#item_code').data('PT');
        var iN = $('#item_code').data('PN');
        var uPrice = accounting.formatMoney($('#directUnitPrice').val(), null, getDecimalPlaces($('#directUnitPrice').val()));
        var uFPrice = $('#directUnitPrice').val();
        var qt = $('#directCreditQuanity').val();
        if (qt == 0) {
            qt = 0;
        }
        var uomqty = $("#directCreditQuanity").siblings('.uomqty').val();
        var uomPrice = $("#directUnitPrice").siblings('.uomPrice').val();
        var uBPrice = $("#directUnitPrice").val();
        var nTotal = productLineTotal[locationProducts[selectedProduct].pID+'_'+uBPrice];
        var calNTotal = accounting.unformat(nTotal);
        
        var gD = $('#directDiscount').val();
        if ($('#directDiscount').val() == '')
            gD = 0;
        var locationProductID = locationProducts[productID].lPID;
        selectedProductQuantity = $('#directCreditQuanity').val();
        $("input[name=directDiscount]").trigger(jQuery.Event("focusout"));
        var newTrID = 'tr_' + locationProductID;
        var clonedRow = $($('#preSetSampleForDirectCredit').clone()).attr('id', newTrID).addClass('addedProducts').attr('data-id', locationProductID+'_'+uPrice);;
        clonedRow.children('#code').html(iC + ' - ' + iN);
        clonedRow.children('#quantity').html(uomqty);
        clonedRow.children('#unit').html(uomPrice);

        clonedRow.children().children('#uomName').html(locationProducts[productID].uom[selectedProductUom].uA);

        clonedRow.children('#disc').html(gD);
        clonedRow.children('#ttl').html(accounting.formatMoney(nTotal));

        if (locationProducts[productID].bP == 0 && locationProducts[productID].sP == 0) {
            clonedRow.children().children('#viewSubProducts').addClass('hidden');
        }
        var clonedTax = $($('#addNewTaxDivForDirectCredit').clone()).attr('id', '');
        clonedTax.children('#addNewTax').children('#taxApplied').attr('id', '');
        clonedTax.children('#addNewTax').attr('id', '');
        clonedTax.children('#addTaxUl').children().removeClass('tempLi');
        clonedTax.children('#addTaxUl').children().children().removeClass('addNewTaxCheck');
        clonedTax.children('#addTaxUl').children().children().attr('disabled', true);
        clonedTax.children('#addTaxUl').children('#sampleLi').remove();
        clonedTax.children('#addTaxUl').attr('id', '');

        clonedTax.find('#toggleSelection').addClass('hidden');
        clonedRow.children('#tax').html(clonedTax);
        clonedRow.removeClass('hidden');
        clonedRow.insertBefore('#preSetSampleForDirectCredit');
        var newProduct = locationProducts[productID];
        var discountType = '';
        if (newProduct.pPDV != null) {
            discountType = 'value';
        } else if (newProduct.pPDP != null) {
            discountType = 'precentage';
        }
        directCreditProducts.push(new dCreditProduct(newProduct.lPID, productID, iC, iN, qt, gD, discountType, toFloat(uFPrice), selectedProductUom, calNTotal, currentItemTaxResults, directBatchProducts, directSerialProducts, pT, true, null,null ,false, false));

        setDirectCreditTotalCost();
        setTotalTax();
        clearAddNewRow($parentTr);
        $('#item_code').val(0).trigger('change').empty().selectpicker('refresh');
        clearModalWindow();
    }

    function dCreditProduct(lPID, pID, pCode, pN, pQ, pD, discountType, pUP, pUom, pTotal, pTax, bProducts, sProducts, productType, stockUpdate, productindexID, creditSubProID = null) {
        this.locationPID = lPID;
        this.pID = pID;
        this.pCode = pCode;
        this.pName = pN;
        this.pQuantity = pQ;
        this.pDiscount = pD;
        this.discountType = discountType;
        this.pUnitPrice = pUP;
        this.pUom = pUom;
        this.pTotal = pTotal;
        this.pTax = pTax;
        this.bProducts = bProducts;
        this.sProducts = sProducts;
        this.productType = productType;
        this.stockUpdate = stockUpdate;
        this.productindexID = productindexID;
        this.creditSubProID = creditSubProID;
    }

    function crdtBatchProduct(bCode, bQty, mDate, eDate, warnty, bID) {
        this.bCode = bCode;
        this.bQty = bQty;
        this.mDate = mDate;
        this.eDate = eDate;
        this.warnty = warnty;
        this.bID = bID;
    }

    function crdtSerialProduct(sCode, sWarranty, sBCode, sEdate, sID,sWarrantyType) {
        this.sCode = sCode;
        this.sWarranty = sWarranty;
        this.sBCode = sBCode;
        this.sEdate = sEdate;
        this.sID = sID;
        this.sWarrantyType = sWarrantyType;

    }

    function setDirectCreditTotalCost() {
        var returnTotal = 0;
        for (var i in directCreditProducts) {
            returnTotal += toFloat(directCreditProducts[i].pTotal);
        }
        $('#drSubtotal').html(accounting.formatMoney(returnTotal));
        $('#drFinaltotal').html(accounting.formatMoney(returnTotal));
    }

    function setTotalTax() {
        totalTaxList = {};
        for (var k in directCreditProducts) {
            if (directCreditProducts[k].pTax != null) {
                for (var l in directCreditProducts[k].pTax.tL) {
                    if (totalTaxList.hasOwnProperty(l)) {
                        var curTA = totalTaxList[l].tA;
                        var newTA = (toFloat(curTA) + toFloat(directCreditProducts[k].pTax.tL[l].tA));
                        totalTaxList[l].tA = newTA.toFixed(2);
                    } else {
                        totalTaxList[l] = {tN: directCreditProducts[k].pTax.tL[l].tN, tP: directCreditProducts[k].pTax.tL[l].tP, tA: directCreditProducts[k].pTax.tL[l].tA};
                    }
                }
            }
        }

        var totalTaxHtml = "";
        for (var t in totalTaxList) {
            totalTaxHtml += totalTaxList[t].tN + "(" + totalTaxList[t].tP + "%)&nbsp" + accounting.formatMoney((totalTaxList[t].tA)) + "<br>";
        }
        $('#totalTaxShow').html(totalTaxHtml);
    }

    $(document).on('keyup', '#newBatchQty,#newBatchWarrenty', function() {
        if ((this.id == 'newBatchQty') && ((!$.isNumeric($('#newBatchQty').val()) || $('#newBatchQty').val() <= -1))) {
            $('#newBatchQty').val('');
        }
        if ((this.id == 'newBatchWarrenty') && ((!$.isNumeric($('#newBatchWarrenty').val()) || $('#newBatchWarrenty').val() < 0))) {
            $('#newBatchWarrenty').val('');
        }
    });
    $(document).on('keyup', '.serialWarrenty', function() {
        if (!$.isNumeric($(this).val()) || $(this).val() < 0) {
            $(this).val('');
        }
    });
    
    $('#addBatchItem').on('click', function() {
        if ($('#newBatchNumber').val() == '' || $('#newBatchQty').val() == '') {
            p_notification(false, eb.getMessage('ERR_GRN_BATCH_INFO'));
        } else {
            var newBCode = $('#newBatchNumber').val();
            var newBQty = $('#newBatchQty').val();
            var newBMDate = $('#newBatchMDate').val();
            var newBEDate = $('#newBatchEDate').val();
            var newBWrnty = $('#newBatchWarrenty').val();
            if ((toFloat(batchCount) + toFloat(newBQty)) > selectedProductQuantity) {
                p_notification(false, eb.getMessage('ERR_GRN_BATCH_COUNT'));
            } else {
                if (directBatchProducts[newBCode] != null) {
                    p_notification(false, eb.getMessage('ERR_GRN_BATCH_CODE'));
                } else {
                    if ((toFloat(batchCount) + toFloat(newBQty)) == selectedProductQuantity) {
                        $('#proBatchAddNew').addClass('hidden');
                    }
                    batchCount += toFloat(newBQty);
                    directBatchProducts[newBCode] = new crdtBatchProduct(newBCode, newBQty, newBMDate, newBEDate, newBWrnty, null);
                    var cloneBatchAdd = $($('#proBatchSample').clone()).attr('id', newBCode).removeClass('hidden').addClass('tempProducts');
                    cloneBatchAdd.children('#batchNmbr').html(newBCode);
                    cloneBatchAdd.children('#batchQty').html(newBQty);
                    cloneBatchAdd.children('#batchMDate').html(newBMDate);
                    cloneBatchAdd.children('#batchEDate').html(newBEDate);
                    cloneBatchAdd.children('#batchW').html(newBWrnty);
                    cloneBatchAdd.insertBefore('#proBatchSample');
                    clearAddBatchProductRow();
                }
            }
        }
    });

    $('#newBatchNumber').on('click', function(){
        $(this).parents('tr').find('#newBatchMDate').val('');
        $(this).parents('tr').find('#newBatchEDate').val('');
    });

    $('#newBatchNumber').typeahead({
        ajax: {
            url: BASE_URL + '/api/grn/get-batch-numbers-for-typeahead',
            timeout: 250,
            triggerLength: 1,
            method: "POST",
            preProcess: function(respond) {
                if (!respond.status) {
                    return false;
                }
                return respond.data;
            }
        }
    });

    $('#newBatchQty, #newBatchMDate, #newBatchEDate, #newBatchWarrenty').on('click',function(){
        var addedBatchCode = $(this).parents('tr').find('#newBatchNumber').val();
        var selectedRow = $(this).parents('tr');

        if(!($(this).parents('tr').find('#newBatchMDate').val() && $(this).parents('tr').find('#newBatchEDate').val())){
            eb.ajax({
            type: 'POST',
            url: BASE_URL + '/api/grn/get-man-date-and-exp-date-for-batch-item',
            data: {batchCode : addedBatchCode },
                success: function(respond) {
                    if (respond.status == true) {
                        if(!$('#newBatchMDate', selectedRow).val()){
                            $('#newBatchMDate', selectedRow).val(respond.data.manDate);
                        }
                        if(!$('#newBatchEDate', selectedRow).val()){
                            $('#newBatchEDate', selectedRow).val(respond.data.expDate);
                        }
                    }
                },

            });
        }

    });

    $('#addDirectCreditProductsModal').on('change', 'input.serialNumberList', function() {
        var matchSerialNumberCount = 0;
        var typedVal = $(this).val();
        var serialCheckStatus = false;
        eb.ajax({
            type: 'POST',
            url: BASE_URL + '/productAPI/checkSerialProductCodeExists',
            data: {serialCode: typedVal, productCode: locationProducts[selectedProduct].pC, },
            success: function(respond) {
                if (respond.data == true) {
                    serialCheckStatus = true;
                }
            },
            async: false
        });

        if (serialCheckStatus == true) {
            p_notification(false, eb.getMessage('ERR_GRN_SERIAL_CODE'));
            $(this).val('');
        } else {
            $('input.serialNumberList').each(function() {
                if ($(this).val() == typedVal) {
                    matchSerialNumberCount++;
                }
            });
            if (matchSerialNumberCount > 1) {
                p_notification(false, eb.getMessage('ERR_GRN_SAME_SERIAL_CODE'));
                $(this).val('');
            }
        }
    });

    $('#addDirectCreditProductsModal').on('change', 'input.serialBatchList,#newSerialBatch', function() {
        if (!directBatchProducts.hasOwnProperty($(this).val())) {
            p_notification(false, eb.getMessage('ERR_GRN_ENTER_BATCHNUM'));
            $(this).val('');
        } else {
            var enteredBatchCode = $(this).val();
            var sameBatchCodeCount = 0;
            $('input.serialBatchList,#newSerialBatch').each(function() {
                if ($(this).val() == enteredBatchCode) {
                    sameBatchCodeCount++;
                }
            });
            if (sameBatchCodeCount > directBatchProducts[enteredBatchCode].bQty) {
                p_notification(false, eb.getMessage('ERR_GRN_ENTER_PRODLIMIT'));
                $(this).val('');
            }
        }
    });

    $('#saveDirectReturnProducts').on('click', function(event) {
        var serialEmpty = false;
        $('input.serialNumberList').each(function() {
            if ($(this).val() == '') {
                serialEmpty = true;
                return false;
            }
        });
        if (serialEmpty == true) {
            p_notification(false, eb.getMessage('ERR_GRN_FILL_SERIALNUM'));
            return false;
        }

        var batchEmpty = false;
        if (locationProducts[selectedProduct].bP == 1 && locationProducts[selectedProduct].sP == 0) {
            if (batchCount < selectedProductQuantity) {
                batchEmpty = true;
            }
        }
        if (batchEmpty == true) {
            p_notification(false, eb.getMessage('ERR_GRN_ADD_BATCH_DETAILS'));
            return false;
        }
        var serialBatchEmpty = false;
        if ((locationProducts[selectedProduct].bP == 1) && (locationProducts[selectedProduct].sP == 1)) {
            $('input.serialBatchList').each(function() {
                if ($(this).val() == '') {
                    serialBatchEmpty = true;
                    return false;
                }
            });
        }
        if (serialBatchEmpty == true) {
            p_notification(false, eb.getMessage('ERR_INVENTADJUS_FILLBATCHNUM'));
            return false;
        }
        if (serialEmpty == false && batchEmpty == false) {
            $('#addNewSerial > tr').each(function() {
                var sNmbr = $(this).children().children('#newSerialNumber').val();
                var sWrnty = $(this).children().children('#newSerialWarranty').val();
                var sBt = $(this).children().children('#newSerialBatch').val();
                var sED = $(this).children().children('.serialEDate').val();
                var sWrntyType = $(this).children().children('#newSerialWarrantyType').val();

                if (sNmbr != '') {
                    directSerialProducts[sNmbr] = new crdtSerialProduct(sNmbr, sWrnty, sBt, sED, null,sWrntyType);
                }
            });
            $('#addDirectCreditProductsModal').modal('hide');
            addNewProductRow(directBatchProducts, directSerialProducts, '');
            clearModalWindow();
            $('#itemCode').focus();
        } else {
            p_notification(false, eb.getMessage('ERR_GRN_BATCH_PRODDETAILS'));
        }

    });

    $('#addDirectCreditProductsModal').on('click', '.batchDelete', function() {
        var deleteBID = $(this).closest('tr').attr('id');
        var deleteBQty = directBatchProducts[deleteBID].bQty;
        batchCount -= toFloat(deleteBQty);
        delete(directBatchProducts[deleteBID]);
        $('#' + deleteBID).remove();
        $('#proBatchAddNew').removeClass('hidden');
    });

    function clone() {
        var sendStartingValue = quon;
        var originalQuontity = $('#directCreditQuanity').val();
        var poQuontity = $('.poAddItem').parents("tr").find("input[name='directCreditQuanity']").val();
        if (originalQuontity == "") {
            originalQuontity = poQuontity;
        }
        id = '#temp_' + idNumber;
        var quonnn = $(id).find('input[name=quontity]').val();
        var number = $(id).find('input[name=startNumber]').val();
        var startNumberStr = $(id).find('input[name=startNumber]').val();
        var prefix = $(id).find('input[name=prefix]').val();
        var pCode = locationProducts[selectedProduct].pC;
        if (quonnn == '') {
            quonnn = 0;
        }
        if (number == '') {
            p_notification(false, eb.getMessage('NO_START_NO'));
            return false;
        }
        if (number == 0) {
            p_notification(false, eb.getMessage('START_NO_ZERO'));
            return false;
        }
        var validationValue = true;
        validationValue = validation(number, prefix, quonnn, startNumberStr, pCode, sendStartingValue, id);
        if (validationValue == true) {

            if (toFloat(quonnn) <= originalQuontity && toFloat(quon) + toFloat(quonnn) <= originalQuontity && quonnn !== 0) {
                quon = toFloat(quon) + toFloat(quonnn);
                if (toFloat(quonnn) == originalQuontity || toFloat(quon) == originalQuontity) {
                    $(id).find('input[name=submit]').attr('disabled', true);
                    $(id).find('input[name=prefix]').attr('disabled', true);
                    $(id).find('input[name=startNumber]').attr('disabled', true);
                    $(id).find('input[name=quontity]').attr('disabled', true);
                    $(id).find('input[name=expireDateAutoFill]').attr('disabled', true);
                    $(id).find('input[name=batch_code]').attr('disabled', true);
                    $(id).find('input[name=mfDateAutoFill]').attr('disabled', true);
                    $(id).find('input[name=wrtyDays]').attr('disabled', true);
                    setValue(sendStartingValue, quonnn, id);
                }
                else {
                    var row = document.getElementById('temp_' + idNumber);
                    var table = document.getElementById('tempTbody');
                    var clone = row.cloneNode(true);
                    idNumber += 1;
                    clone.id = 'temp_' + idNumber;
                    table.appendChild(clone);
                    nextId = '#temp_' + idNumber;
                    $(nextId).addClass('tempy');
                    $(nextId).find('input[name=prefix]').val('');
                    $(nextId).find('input[name=startNumber]').val('');
                    $(nextId).find('input[name=quontity]').val('');
                    $(nextId).find('input[name=wrtyDays]').val('');
                    var issuDate = new Date(Date.parse($('#issueDate').val()));
                    $(nextId).find('input[name=expireDateAutoFill]').val('');
                    $(nextId).find('input[name=batch_code]').val('');
                    $(nextId).find('input[name=mfDateAutoFill]').val('');
                    var cloneMfD = $(nextId).find('input[name=mfDateAutoFill]').datepicker({
                        onRender: function(date) {
                            return date.valueOf() > issuDate.valueOf() ? 'disabled' : '';
                        }
                    }).on('changeDate', function(ev) {
                        cloneMfD.hide();
                    }).data('datepicker');
                    $(id).find('input[name=submit]').attr('disabled', true);
                    $(id).find('input[name=prefix]').attr('disabled', true);
                    $(id).find('input[name=startNumber]').attr('disabled', true);
                    $(id).find('input[name=quontity]').attr('disabled', true);
                    $(id).find('input[name=expireDateAutoFill]').attr('disabled', true);
                    $(id).find('input[name=batch_code]').attr('disabled', true);
                    $(id).find('input[name=mfDateAutoFill]').attr('disabled', true);
                    $(id).find('input[name=wrtyDays]').attr('disabled', true);
                    if (locationProducts[selectedProduct].sP == 1 && locationProducts[selectedProduct].bP == 1) {
                        $(nextId).find('input[name=mfDateAutoFill]').attr('disabled', false);
                        $(nextId).find('input[name=batch_code]').attr('disabled', false);
                    }
                    var cloneExpD = $(nextId).find('input[name=expireDateAutoFill]').datepicker().on('changeDate', function(ev) {
                        cloneExpD.hide();
                    }).data('datepicker');

                    setValue(sendStartingValue, quonnn, id);
                }
            }
            else {
                p_notification(false, eb.getMessage('WRNG_QUON_PI'));
                return false;
            }
        }
    }

    $('#tempTbody').on('click', '#submit_serial_number', function()
    {
        if ((locationProducts[selectedProduct].bP == 1) && (locationProducts[selectedProduct].sP == 1)) {
            if (!$(this).parents("tr").find("input[name='batch_code']").val()) {
                p_notification(false, eb.getMessage('ERR_NO_GRN_ADD_SERIAL_BATCH_DETAILS'));
                return false;
            }
        }

        if(isNaN($(this).parents('tr').find("input[name='startNumber']").val())){
            p_notification(false, eb.getMessage('ERR_SERIAL_START_NO'));
            $(this).parents('tr').find("input[name='startNumber']").val("");
            return false;
        }


        clone();
        if ($('#submit_serial_number').attr('disabled')) {
            $(this).parent().parent().find('#expire_date_autoFill').removeClass('white_bg');
            $(this).parent().parent().find('#mf_date_autoFill').removeClass('white_bg');
        }
    });
    function validation(startNumber, prefix, bQuntity, startNumberStr, productCode, sendStartingValue, id) {
        var strNumberLength = startNumberStr.length;
        var matchSerialNumberCount = 0;
        var serialCheckStatus = false;
        eb.ajax({
            type: 'POST',
            url: BASE_URL + '/productAPI/checkSerialProductCode',
            data: {startingNumber: startNumber, prefix: prefix, serialQuntity: bQuntity, strNumberLength: strNumberLength, productCode: productCode, },
            success: function(respond) {
                if (respond.data == true) {
                    serialCheckStatus = true;
                }
            },
            async: false
        });
        if (serialCheckStatus == true) {
            p_notification(false, eb.getMessage('ERR_GRN_SERIAL_CODE'));
            return false;
        }
        else {
            $('input.serialNumberList').each(function() {
                for (i = startNumber; i < (parseInt(startNumber) + parseInt(bQuntity)); i++) {
                    if (prefix) {
                        var startingValue = prefix + pad(i, strNumberLength);

                        if ($(this).val() == startingValue) {
                            matchSerialNumberCount++;
                        }
                    } else {
                        var startingValue = pad(i, strNumberLength);
                        if ($(this).val() == startingValue) {
                            matchSerialNumberCount++;
                        }
                    }

                }

            });
            if (matchSerialNumberCount >= 1) {
                p_notification(false, eb.getMessage('ERR_GRN_SAME_SERIAL_CODE'));
                return false;
            }
            else {
                return true;
            }
        }
    }


    function setValue(quon, quonnn, id) {
        var abc = 0;
        var bQuntity = toFloat(quonnn);
        var max = toFloat(quon) + toFloat(quonnn);
        var prefix = $(id).find('input[name=prefix]').val();
        var startNumber = parseInt($(id).find('input[name=startNumber]').val());
        var startNumberString = $(id).find('input[name=startNumber]').val();
        var expire_date = $(id).find('input[name=expireDateAutoFill]').val();
        var mf_date = $(id).find('input[name=mfDateAutoFill]').val();
        var wrnty = $(id).find('input[name=wrtyDays]').val();
        var wrntyType = $(id).find('#wrtyType').val();
        if (locationProducts[selectedProduct].sP == 1 && locationProducts[selectedProduct].bP == 1) {
            var batchNumber = $(id).find('input[name=batch_code]').val();
            directBatchProducts[batchNumber] = new crdtBatchProduct(batchNumber, bQuntity, mf_date, expire_date, wrnty, null);

            if (startNumber) {
                $('#addNewSerial tr').each(function() {
                    if (abc >= toFloat(quon) && abc < toFloat(max)) {
                        var numberLength = startNumberString.length;
                        var startingValue = pad(startNumber, numberLength);
                        if (prefix == '') {
                            $(this).find('input[name=serialCode]:first').val(startingValue);
                            $(this).find('input[name=serialEDate]').val(expire_date);
                            $(this).find('input[name=batchCode]').val(batchNumber);
                            $(this).find('input[name=serialWarronty]').val(wrnty);
                            $(this).find("#newSerialWarrantyType").val(wrntyType).change();

                        } else if (prefix != '') {
                            var num = prefix + startingValue;
                            $(this).find('input[name=serialCode]:first').val(num);
                            $(this).find('input[name=serialEDate]').val(expire_date);
                            $(this).find('input[name=batchCode]').val(batchNumber);
                            $(this).find('input[name=serialWarronty]').val(wrnty);
                            $(this).find("#newSerialWarrantyType").val(wrntyType).change();
                        }
                        startNumber += 1;
                    }
                    abc++;
                });
            }
        }
        else {
            if (startNumber) {
                $('#addNewSerial tr').each(function() {
                    if (abc >= toFloat(quon) && abc < toFloat(max)) {
                        var numberLength = startNumberString.length;
                        var startingValue = pad(startNumber, numberLength);
                        if (prefix == '') {
                            $(this).find('input[name=serialCode]:first').val(startingValue);
                            $(this).find('input[name=serialEDate]').val(expire_date);
                            $(this).find('input[name=serialWarronty]').val(wrnty);
                            $(this).find("#newSerialWarrantyType").val(wrntyType).change();
                        } else if (prefix != '') {
                            var num = prefix + startingValue;
                            $(this).find('input[name=serialCode]:first').val(num);
                            $(this).find('input[name=serialEDate]').val(expire_date);
                            $(this).find('input[name=serialWarronty]').val(wrnty);
                            $(this).find("#newSerialWarrantyType").val(wrntyType).change();
                        }
                        startNumber += 1;
                    }
                    abc++;
                });
            }
        }
    }

    function pad(n, width, z) {
        z = z || '0';
        n = n + '';
        return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
    }

    function clearAddBatchProductRow() {
        $('#newBatchNumber').val('');
        $('#newBatchQty').val('');
        $('#newBatchMDate').val('');
        $('#newBatchEDate').val('');
        $('#newBatchWarrenty').val('');
    }

    $('#drCreditProductTable').on('click', '.dCreditDeleteProduct', function() {
        var deletePTrID = $(this).closest('tr').attr('id');
        var uniqID2 = $(this).closest('tr').attr('data-id');
        var deletePID = deletePTrID.split("_");
        deletePID = deletePID[1];
        var deleteItemPrice = accounting.unformat($(this).closest('tr').find('#unit').html());
        var deleteProductIndex = directCreditProducts.indexOf(_.findWhere(directCreditProducts, {'locationPID': deletePID, 'pUnitPrice': deleteItemPrice}));
        bootbox.confirm('Are you sure you want to remove this product ?', function(result) {
            if (result == true) {
                directCreditProducts.splice(deleteProductIndex, 1);
                $('#form_row tr').each(function(){
                    if($(this).attr('data-id') == uniqID2){
                        $(this).remove();
                    }
                });
                setDirectCreditTotalCost();
                setTotalTax();
            }
        });
    });

    $('#drCreditProductTable').on('click', '.dCreditViewProducts', function() {
        var viewPTrID = $(this).closest('tr').attr('id');
        var viewPID = viewPTrID.split('tr_')[1].trim();
        var viewItemPrice = accounting.unformat($(this).closest('tr').find('#unit').html());
        var viewProductIndex = directCreditProducts.indexOf(_.findWhere(directCreditProducts, {'locationPID': viewPID, 'pUnitPrice': viewItemPrice}));
        var vPID = directCreditProducts[viewProductIndex].pID;

        if (locationProducts[vPID].bP == 0 && locationProducts[vPID].sP == 0) {
            p_notification(false, eb.getMessage('ERR_GRN_SUBPROD'));
        } else {
            setProductsView(viewProductIndex);
            $('#viewPiSubProductsModal').modal('show');
        }
    });

    function setProductsView(viewProductID) {
        $('.tempView').remove();
        $('#batchProductsView').addClass('hidden');
        $('#serialViewScreen').addClass('hidden');
        if (!jQuery.isEmptyObject(directCreditProducts[viewProductID].sProducts)) {
            $('#serialViewScreen').removeClass('hidden');
            $('#directReturnSerialCodeView').html(directCreditProducts[viewProductID].pCode);
            $('#directReturnSerialQuantityView').html(directCreditProducts[viewProductID].pQuantity);
            for (var h in directCreditProducts[viewProductID].sProducts) {
                var viewSPr = directCreditProducts[viewProductID].sProducts[h];
                var cloneSerialProductView = $($('#serialViewSample').clone()).attr('id', '').removeClass('hidden').addClass('tempView');
                cloneSerialProductView.children('#serialNumberView').html(viewSPr.sCode);
                cloneSerialProductView.children('#serialWarrentyView').html(viewSPr.sWarranty);
                cloneSerialProductView.children('#serialBIDView').html(viewSPr.sBCode);
                cloneSerialProductView.children('#serialExDView').html(viewSPr.sEdate);
                cloneSerialProductView.insertBefore('#serialViewSample');
            }
        } else if (!jQuery.isEmptyObject(directCreditProducts[viewProductID].bProducts)) {
            $('#batchProductsView').removeClass('hidden');
            $('#batchCodeView').html(directCreditProducts[viewProductID].pCode);
            $('#batchQuantityView').html(directCreditProducts[viewProductID].pQuantity);
            for (var b in directCreditProducts[viewProductID].bProducts) {
                var viewBPr = directCreditProducts[viewProductID].bProducts[b];
                var cloneBatchProductView = $($('#proBatchViewSample').clone()).attr('id', '').removeClass('hidden').addClass('tempView');
                cloneBatchProductView.children('#batchNmbrView').html(viewBPr.bCode);
                cloneBatchProductView.children('#batchQtyView').html(viewBPr.bQty);
                cloneBatchProductView.children('#batchMDateView').html(viewBPr.mDate);
                cloneBatchProductView.children('#batchEDateView').html(viewBPr.eDate);
                cloneBatchProductView.children('#batchWView').html(viewBPr.warnty);
                cloneBatchProductView.insertBefore('#proBatchViewSample');
            }
        }
    }

});



