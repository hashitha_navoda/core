
$(document).ready(function() {

//    Search Customer Category
    $('#category-search-form').submit(function(e) {
        var searchKey = $("#customer-category-search-keyword", $(this)).val().trim();
        if (!searchKey) {
            p_notification(false, eb.getMessage('ERR_CUSTAPI_CUST_CATEG_EMPTY_SEARCH'));
            $("#customer-category-search-keyword", $(this)).focus();
            return false;
        }
        searchCustomerCategory(searchKey);
        e.stopPropagation();
        return false;
    });

    $('#category-search-form').on('click', '.reset', function() {
        searchCustomerCategory();
    });

//  update or Save customer Category
    $('#customer-category-form').submit(function(e) {
        var form_data = {
            customerCategoryName: $("[name='customerCategoryName']", this).val(),
        };

        if ($("[name='customerCategoryName']", this).val().trim() === '' || $("[name='customerCategoryName']", this).val().trim() === null) {
            $("#customerCategoryName").focus();
            p_notification(false, eb.getMessage('ERR_CUST_EMPTY_CATEG'));
            return false;
        }

        var submitted_btn = $("[type='submit']:visible", $(this));
        var update = submitted_btn.hasClass('update');
        if (update) {
            form_data = $.extend(form_data, {customerCategoryID: $("[name='customerCategoryID']", this).val()});
        }

        var action = (update) ? 'update-customer-category' : 'save-customer-category';
        eb.ajax({
            url: '/customer-category/' + action + '/' + getCurrPage(),
            method: 'post',
            data: form_data,
            success: function(data) {
                p_notification(data.status, data.msg);
                if (data.status == true) {
                    $("#customerCategoryName").val('');
                    $(".category-form-container").removeClass('update');
                    $("#customerCategoryList").html(data.html);
                }
            }
        });
        e.stopPropagation();
        return false;
    });

//  Edit Customer Category
    $("#customerCategoryList").on('click', 'a.edit', function(e) {
        var customerCategoryID = $(this).parents('tr').data('categoryrow');

        $(".category-form-container").addClass('update');
        $("html, body").animate({scrollTop: $(".category-form-container").offset().top - 80});
        eb.ajax({
            url: '/customer-category/get-customer-category',
            method: 'post',
            data: {customerCategoryID: customerCategoryID},
            dataType: 'json',
            success: function(response) {
                if (response.status == true) {
                    var categoryData = response.data;
                    $("[name='customerCategoryID']", '#customer-category-form').val(categoryData.customerCategoryID);
                    $("[name='customerCategoryName']", '#customer-category-form').val(categoryData.customerCategoryName).focus();
                } else {

                }
            }
        });

        e.stopPropagation();
        return false;
    });

//    Delete Customer Category
    $("#customerCategoryList").on('click', 'a.delete', function(e) {
        var customerCategoryID = $(this).parents('tr').data('categoryrow');

        bootbox.confirm('Are you sure you want to delete this Category?', function(result) {
            if (result === true) {
                eb.ajax({
                    url: '/customer-category/delete-customer-category/' + getCurrPage(),
                    method: 'post',
                    data: {customerCategoryID: customerCategoryID, },
                    dataType: 'json',
                    success: function(data) {
                        p_notification(data.status, data.msg);
                        if (data.status == true) {
                            $("#customerCategoryList").html(data.html);
                        }
                    }
                });
            }
        });

        e.stopPropagation();
        return false;
    });

    $("#customerCategoryReset").click(function() {
        $(".category-form-container")
                .removeClass('update')
                .find("#customerCategoryName")
                .focus();
    });

});

function searchCustomerCategory(searchKey) {
    eb.ajax({
        url: '/customer-category/search-customer-category',
        method: 'POST',
        data: {searchKey: searchKey},
        success: function(response) {
            if (response.status) {
                $("#customerCategoryList").html(response.html);
            } else {
                p_notification(response.status, response.msg);
            }
        }
    });
}