/**
 * @author Prathap Weerasinghe <prathap@thinkcube.com>
 * This js contain Quotatin view related functions
 */

$(document).ready(function() {
    $("#prv_edit").on("click", function() {
        window.location.assign(BASE_URL + "/quotation/Edit/" + $(this).attr("data-qot_id"));
    });

    $("#q_print").on("click", function() {
        window.print();
    });
    $("#q_email").on("click", function() {
        $("#email-modal").modal("show");
        $("#send").on("click", function() {
            var state = validateEmail($("#email_to").val(), $("#email-body").html());
            if (state) {
                var param = {
                    qout_id: $(this).attr("data-qot_id"),
                    to_email: $("#email_to").val(),
                    subject: $("#email_sub").val(),
                    body: $("#email-body").html()
                };
                var mail = eb.post("/quotationAPI/sendQuotation", param);
                mail.done(function(rep) {
                    if (rep.error) {
                        p_notification(false, eb.getMessage('ERR_PAY_EMAIL_SENT'));
                    } else {
                        p_notification(true, eb.getMessage('SUCC_PAY_EMAIL_SENT'));
                    }
                });
                $("#email-modal").modal("hide");
            }
        });
    });
    $("#new_qtn").on("click", function() {
        window.location.assign(BASE_URL + "/quotation/index");
    });
    $("#to_so").on("click", function() {
        window.location.assign(BASE_URL + "/salesOrders/index/" + $(this).attr("data-qot_id"));
    });
    $("#to_inv").on("click", function() {
        window.location.assign(BASE_URL + "/invoice/index/qo/" + $(this).attr("data-qot_id"));
    });
    $("#cancel").on("click", function() {
        window.history.back(-1);
    });
});