var loadQuotationPreview;
var customerID;
var selectedQuID;

$(document).ready(function() {

   
    $('#category').on('change', function() {
        if ($(this).val() > 0 && customerID != $(this).val()) {
            customerID = $(this).val();
            getCategoryWiseItemMrpDetails($(this).val());
        } else {
            $('#cqview').html('');
            $('#contactList').removeClass('hidden');
        }
    });

    $('#searchItem').on('click', function() {
        searchItemMrpDetails($(this).val());
    });

    $("form.product-search").submit(function(e) {
        var keyword = $('#productSearch').val();
        var fixedAsset = false;
        var param = {searchProductString: keyword, fixedAssetFlag : fixedAsset};
        getViewAndLoad('/productAPI/getProductsForSearch', 'productList', param);
        e.stopPropagation();
        return false;
    });
    
    function getCategoryWiseItemMrpDetails(categoryID) {

        if (categoryID == 0) {
            $('#cqview').html('');
            $('#contactList').removeClass('hidden');
        } else {
            var searchurl = BASE_URL + '/mrp-setting-api/getCategoryWiseItemMrpDetails';
            var searchrequest = eb.post(searchurl, {categoryID: categoryID});
            searchrequest.done(function(respond) {

                $('#cqview').html(respond.html);
                $('#contactList').addClass('hidden');
                // $("#quotation-list", "#create-quotation-form-div").html(searchdata);
            });
            return false;
        }

    }

    function searchItemMrpDetails(categoryID) {
        var searchKey = $('#productSearch').val();
        var searchurl = BASE_URL + '/mrp-setting-api/searchItemMrpDetailsByKeyword';
        var searchrequest = eb.post(searchurl, {searchKey: searchKey});
        searchrequest.done(function(respond) {

            $('#cqview').html(respond.html);
            $('#contactList').addClass('hidden');
            // $("#quotation-list", "#create-quotation-form-div").html(searchdata);
        });
        return false;

    }

    $('#reset-button').on('click', function(){
        window.location.reload();
    });    
});
