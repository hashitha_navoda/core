var tmptotal;
var customerID;
var salesOrderID;

var loadSalesOrderPreview;

$(document).ready(function() {

    if (!$("#filter-button").length) {
        return false;
    }

    $('#inv-search-select').val('Sales Order No');
    $('.deli_row').hide();
    $('#deli_form_group').hide();
    $('#tax_td').hide();
    $('#view-div').hide();
    $('#custadiv').hide();

    $('#inv-search-select').on('change', function() {
        if ($(this).val() == 'Sales Order No') {
            $('#inv-custa-search').selectpicker('hide');
            $('#inv-search').selectpicker('show');
            $('#inv-custa-search').val('');
            $('#view-div').hide();
            $('#custadiv').hide();
            $('#invoice-list').show();
            selectedsearch = 'Invoice No';
        } else if ($(this).val() == 'Customer Name') {
            $('#inv-search').selectpicker('hide');
            $('#inv-custa-search').selectpicker('show');
            $('#inv-search').val('');
            $('#view-div').hide();
            $('#custadiv').hide();
            selectedsearch = 'Customer Name';
        }
    });

    loadDropDownFromDatabase('/customerAPI/search-customers-for-dropdown', "", 'withDeactivatedCustomers', '#inv-custa-search');
    $('#inv-custa-search').on('change', function() {
        if ($(this).val() > 0 && customerID != $(this).val()) {
            customerID = $(this).val();
            var getinvbycusturl = '/api/salesOrders/retriveCustomerSalesOrders';
            var requestinvbycust = eb.post(getinvbycusturl, {
                customerID: customerID
            });
            requestinvbycust.done(function(retdata) {
                if (retdata == "noinvoices") {
                    $('#salesorder-list').html('');
                    p_notification(false, eb.getMessage('ERR_VIEWSORDER_NO_SORDERCUS'));
                } else {
                    $('#salesorder-list').html(retdata);
                }
            });
        }
    });

    loadDropDownFromDatabase('/api/salesOrders/search-sales-orders-for-dropdown', "", 0, '#inv-search');
    $('#inv-search').on('change', function() {
        if ($(this).val() > 0 && salesOrderID != $(this).val()) {
            salesOrderID = $(this).val();
            var searchurl = BASE_URL + '/api/salesOrders/getSalesOrderFromSearch';
            var searchrequest = eb.post(searchurl, {salesOrderID: salesOrderID});
            searchrequest.done(function(searchdata) {
                $('#salesorder-list').html(searchdata);
            });
        } else {
            $('#custadiv').html('');
            $('#custadiv').hide('');
            $('#salesorder-list').show();
        }
    });
    $('#inv-custa-search').selectpicker('hide');

    $('#filter-button').on('click', function() {
        if ($('#from-date').val() == '' || $('#to-date').val() == '') {
            p_notification(false, eb.getMessage('ERR_VIEWPAY_FILLDATA'));
        } else {
            var invoicefilter = BASE_URL + '/api/salesOrders/getSalesOrdersByDatefilter';
            var filterrequest = eb.post(invoicefilter, {
                'fromdate': $('#from-date').val(),
                'todate': $('#to-date').val(),
                'customerID': customerID
            });
            filterrequest.done(function(retdata) {
                if (retdata.msg == 'noinvoices') {
                    $('#salesorder-list').html('');
                    p_notification(false, eb.getMessage('ERR_VIEWSORDER_NO_SORDER'));
                } else {
                    $('#salesorder-list').html(retdata);
                }
            });
        }
    });

    $(document).on('click', "a.editsInactive", function(e) {
        p_notification(false, eb.getMessage('ERR_CUST_INACTIVE'));
    });

    ///////DatePicker\\\\\\\
    var nowTemp = new Date();
    var frmdate;
    var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);

    var checkin = $('#from-date').datepicker().on('changeDate', function(ev) {
        checkin.hide();
        frmdate = ev.date;
        $('#to-date')[0].focus();
    }).data('datepicker');
    var checkout = $('#to-date').datepicker({
        onRender: function(date) {
            return date.valueOf() < frmdate ? 'disabled' : '';
        }
    }).on('changeDate', function(ev) {
        checkout.hide();
    }).data('datepicker');
/////EndOFDatePicker\\\\\\\\\

    //responsive issue
    $(window).bind('resize ready', function() {
        ($(window).width() <= 1200) ? $('#filter-button').addClass('margin_top_low') : $('#filter-button').removeClass('margin_top_low');
    });

    $(window).bind('resize ready', function() {
        ($(window).width() <= 480) ? $('#filter-button').addClass('col-xs-12') : $('#filter-button').removeClass('col-xs-12');
    });

    $('#salesorder-list').on('click', '.so_related_docs', function() {
        setDataToHistoryModal($(this).attr('data-so-related-id'));
        $('#addDocHistoryModal').modal('show');
    });

    $('#salesorder-list').on('click', '.doc_attachments', function() {
        setDataToAttachmentViewModal($(this).attr('data-so-related-id'));
        $('#viewAttachmentModal').modal('show');
    }); 

    $('#salesorder-list').on('click', '.so_related_cus_po', function() {
        setDataToLinkedCusPOViewModal($(this).attr('data-so-related-id'));
        $('#viewLinkedCustomerOrderModal').modal('show');
    });

    $('#doc-history-table').on('click', '.document_view', function() {
        var documentId = $(this).parents('tr').attr('data-docid');
        var documentType = $(this).parents('tr').attr('data-doctype');
        $('#DocumentViewModal').modal('show');
        getPrintPreviewByDocumentTypeAndDocumentID(documentId, documentType);
    });

});

function setDataToAttachmentViewModal(documentID) {
    $('#doc-attach-table tbody tr').remove();
    $('#doc-attach-table tfoot div').remove();
    $('#doc-attach-table tbody div').remove();

    eb.ajax({
        type: 'POST',
        url: BASE_URL + '/get-document-related-attachement',
        data: {
            documentID: documentID,
            documentTypeID: 3
        },
        success: function(respond) {
            if (respond.status == true) {
                $('#doc-attach-table thead tr').removeClass('hidden');
                $.each(respond.data, function(index, value) {
                    tableBody = "<tr><td><a href="+value.link+">"+value.docName+"</a></td></tr>";
                    $('#doc-attach-table tbody').append(tableBody);
                });
            } else {
                $('#doc-attach-table thead tr').addClass('hidden');
                var noDataFooter = "<div class= ' col-lg-6 text-right'><label><b>No Data</b></label>";
                $('#doc-attach-table tbody').append(noDataFooter);
            }
        }
    });
}

function setDataToLinkedCusPOViewModal(documentID) {
    $('#doc-attach-table tbody tr').remove();
    $('#doc-attach-table tfoot div').remove();
    $('#doc-attach-table tbody div').remove();
    
    $('#linked-co-table tbody tr').remove();
    $('#linked-co-table tfoot div').remove();
    $('#linked-co-table tbody div').remove();

    eb.ajax({
        type: 'POST',
        url: BASE_URL + '/get-document-related-linked-cus-po',
        data: {
            documentID: documentID,
            documentTypeID: 46
        },
        success: function(respond) {
            if (respond.status == true) {
                $('#doc-attach-table thead tr').removeClass('hidden');
                tableBody = "<tr><td>"+respond.data['customerOrderData']['customerOrderCode']+"</td><td>"+respond.data['customerOrderData']['customerOrderDate']+"</td><td>"+respond.data['customerOrderData']['customerOrderReference']+"</td></tr>";
                $('#linked-co-table tbody').append(tableBody);


                $.each(respond.data['fileData'], function(index, value) {
                    tableBody = "<tr><td><a href="+value.link+">"+value.docName+"</a></td></tr>";
                    $('#doc-attach-table tbody').append(tableBody);
                });
            } else {
                $('#doc-attach-table thead tr').addClass('hidden');
                var noDataFooter = "<div class= ' col-lg-6 text-right'><label><b>No Data</b></label>";
                $('#doc-attach-table tbody').append(noDataFooter);
            }
        }
    });
}

function setDataToHistoryModal(salesOrderID) {
    $('#doc-history-table tbody tr').remove();
    $('#doc-history-table tfoot div').remove();
    $('#doc-history-table tbody div').remove();

    eb.ajax({
        type: 'POST',
        url: BASE_URL + '/api/salesOrders/getAllRelatedDocumentDetailsBySalesOrderId',
        data: {salesOrderID: salesOrderID},
        success: function(respond) {
            if (respond.status == true) {
                $('#doc-history-table thead tr').removeClass('hidden');
                $.each(respond.data, function(index, value) {
                    if (value != null) {
                        $.each(value, function(index2, value2) {
                            tableBody = "<tr data-docId=" + value2['documentID'] + " data-doctype=" + value2['type'] + "><td class='' >" + value2['type'] + "</td><td>" + value2['code'] + "</td><td>" + value2['amount'] + "</td><td>" + value2['issuedDate'] + "</td><td class='text-center'><span class='glyphicon glyphicon-eye-open openlist link_color document_view ' style='cursor:pointer'></span></td></tr>";
                            $('#doc-history-table tbody').append(tableBody);
                        });
                    }
                });
                var footer = " <div class='col-lg-12 col-md-12 col-sm-12 text-right'><button type='button' class='btn btn-primary col-lg-12 col-md-12 col-sm-12' data-dismiss='modal' aril-hidden='true'>Back</button></div>";
                $('#doc-history-table tfoot').append(footer);
            } else {
                $('#doc-history-table thead tr').addClass('hidden');
                var noDataFooter = "<div class= ' col-lg-9 text-right'><label><b>No Data</b></label>";
                $('#doc-history-table tbody').append(noDataFooter);
            }
        }
    });
}

function getPrintPreviewByDocumentTypeAndDocumentID(documentID, documentType) {
    var $iframe = $('#related-document-view');
    $iframe.ready(function() {
        $iframe.contents().find("body div").remove();
    });
    var URL;
    if (documentType == 'Quotation') {
        URL = BASE_URL + '/quotation/document/' + documentID;
    } else if (documentType == 'SalesOrder') {
        URL = BASE_URL + '/salesOrders/document/' + documentID;
    }
    else if (documentType == 'DeliveryNote') {
        URL = BASE_URL + '/delivery-note/document/' + documentID;
    }
    else if (documentType == 'SalesReturn') {
        URL = BASE_URL + '/return/document/' + documentID;
    }
    else if (documentType == 'SalesInvoice') {
        URL = BASE_URL + '/invoice/document/' + documentID;
    }
    else if (documentType == 'CustomerPayment') {
        URL = BASE_URL + '/customerPayments/document/' + documentID;
    }
    else if (documentType == 'CreditNote') {
        URL = BASE_URL + '/credit-note/document/' + documentID;
    } 
    else if (documentType == 'CreditNotePayment') {
        URL = BASE_URL + '/credit-note-payments/document/' + documentID;
    }

    eb.ajax({
        type: 'POST',
        url: URL,
        success: function(respond) {
            var division = "<div></div>";
            $iframe.ready(function() {
                $iframe.contents().find("body").append(division);
            });
            $iframe.ready(function() {
                $iframe.contents().find("body div").append(respond);
            });
        }
    });

}