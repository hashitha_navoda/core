var colorPalette = ['#ab34eb','#FBAC33','#008ffb', '#FF4560', '#775DD0','#6023ea','#ea2385'];
$(document).ready(function() {
if (!$('.main_body').hasClass('hide-side-bar')) {
    $('.sidbar-toggle-menu').trigger('click');
}
var customerSales;
var customerVisitByPromotionData;
    // getWidgetData('thisYear');

    updateDashboardData('thisWeek');

    function updateDashboardData(period) {
        updateChartData(period, function() {
            getCustomerVisitData(period, function () {
                updateFooterChartData(period, function () {

                });
            });
        });
    }

    function getCustomerVisitData(period, callback){
        eb.ajax({
            type: 'POST',
            url: BASE_URL + '/crm-dashboard-api/getCustomerVisitData',
            data: {period: period},
            success: function(data) {
               
                $('#total-sales').each(function () {
                    $(this).prop('Counter',0).animate({
                        Counter: data.totalCustomer
                    }, {
                        duration: 1000,
                        easing: 'swing',
                        step: function (now) {
                            $(this).text(Math.ceil(now));
                        }
                    });
                });
                $('#gross-profit').each(function () {
                    $(this).prop('Counter',0).animate({
                        Counter: data.totalCustomerVisits
                    }, {
                        duration: 1000,
                        easing: 'swing',
                        step: function (now) {
                            $(this).text(Math.ceil(now));
                        }
                    });
                });

                if (data.customerVisitProfitLoss > 0) {
                    $('#total-gross-profit').removeClass('hidden');
                    $('#total-gross-loss').addClass('hidden');
                    $('#total-gross-profit').html('+'+data.customerVisitProfitLoss+'%<span><i class="glyphicon glyphicon-arrow-up"></i></span>');
                } else {
                    $('#total-gross-profit').addClass('hidden');
                    $('#total-gross-loss').removeClass('hidden');
                    var loss = data.customerVisitProfitLoss * -1;
                    $('#total-gross-loss').html('+'+loss+'%<span><i class="glyphicon glyphicon-arrow-down"></i></span>');
                }
                $.each(data.mostCustomerVisits, function(index, val) {
                    if (index < 10) {
                        $('.customerName'+index).text(val.customerName);
                        $('.customerVisits'+index).text(val.invCount);
                    }
                });

                if (data.mostCustomerVisits.length < 10) {
                    for (var i = 0; i < 10; i++) {
                        if (i >= data.mostCustomerVisits.length) {
                            $('.customerName'+i).text('');
                            $('.customerVisits'+i).text('');
                        }
                    }
                }

                switch (period) {
                    case 'thisYear':
                        $('#last-period-total-gross').text('Compared to ('+data.lastPeriodTotalCustomerVisits+' last year)');
                        
                        break;
                    case 'thisMonth':
                        $('#last-period-total-gross').text('Compared to ('+data.lastPeriodTotalCustomerVisits+' last month)');
                       
                        break;
                    case 'thisWeek':
                        $('#last-period-total-gross').text('Compared to ('+data.lastPeriodTotalCustomerVisits+' last week)');
                       
                        break;
                    case 'thisDay':
                        $('#last-period-total-gross').text('Compared to ('+data.lastPeriodTotalCustomerVisits+' last day)');
                        
                        break;
                    default:
                        // statements_def
                        break;
                }
                
            }
        }).then(function () {
            getLoyalityPointsData(period);
        });
        callback();
    }

    function getLoyalityPointsData(period){
        eb.ajax({
            type: 'POST',
            url: BASE_URL + '/crm-dashboard-api/getLoyalityPointsData',
            data: {period: period},
            success: function(data) {
                $('#total-payments').each(function () {
                    $(this).prop('Counter',0).animate({
                        Counter: data.totalRedeemedPoints
                    }, {
                        duration: 1000,
                        easing: 'swing',
                        step: function (now) {
                            $(this).text(accounting.formatMoney(now));
                        }
                    });
                });
                $('#totalInvoiceCount').each(function () {
                    $(this).prop('Counter',0).animate({
                        Counter: data.totalCollectedPoints
                    }, {
                        duration: 1000,
                        easing: 'swing',
                        step: function (now) {
                            $(this).text(accounting.formatMoney(now));
                        }
                    });
                });

                if (data.loyaltyRedeemProfitLoss > 0) {
                    $('#total-payment-profit').removeClass('hidden');
                    $('#total-payment-loss').addClass('hidden');
                    $('#total-payment-profit').html('+'+data.loyaltyRedeemProfitLoss+'%<span><i class="glyphicon glyphicon-arrow-up"></i></span>');
                } else {
                    $('#total-payment-profit').addClass('hidden');
                    $('#total-payment-loss').removeClass('hidden');
                    var loss = data.loyaltyRedeemProfitLoss * -1;
                    $('#total-payment-loss').html('+'+loss+'%<span><i class="glyphicon glyphicon-arrow-down"></i></span>');
                }

                if (data.loyaltyEarnedProfitLoss > 0) {
                    $('#total-invoice-count-profit').removeClass('hidden');
                    $('#total-invoice-count-loss').addClass('hidden');
                    $('#total-invoice-count-profit').html('+'+data.loyaltyEarnedProfitLoss+'%<span><i class="glyphicon glyphicon-arrow-up"></i></span>');
                } else {
                    $('#total-invoice-count-profit').addClass('hidden');
                    $('#total-invoice-count-loss').removeClass('hidden');
                    var loss = data.loyaltyEarnedProfitLoss * -1;
                    $('#total-invoice-count-loss').html('+'+loss+'%<span><i class="glyphicon glyphicon-arrow-down"></i></span>');
                }


                switch (period) {
                    case 'thisYear':
                        $('#last-period-total-payment').text('Compared to ('+data.lastPeriodtotalRedeemedPoints+' last year)');
                        $('#last-period-total-invoices').text('Compared to ('+data.lastPeriodtotalCollectedPoints+' last year)');
                        break;
                    case 'thisMonth':
                        $('#last-period-total-payment').text('Compared to ('+data.lastPeriodtotalRedeemedPoints+' last month)');
                        $('#last-period-total-invoices').text('Compared to ('+data.lastPeriodtotalCollectedPoints+' last month)');
                        break;
                    case 'thisWeek':
                        $('#last-period-total-payment').text('Compared to ('+data.lastPeriodtotalRedeemedPoints+' last week)');
                        $('#last-period-total-invoices').text('Compared to ('+data.lastPeriodtotalCollectedPoints+' last week)');
                        break;
                    case 'thisDay':
                        $('#last-period-total-payment').text('Compared to ('+data.lastPeriodtotalRedeemedPoints+' last day)');
                        $('#last-period-total-invoices').text('Compared to ('+data.lastPeriodtotalCollectedPoints+' last day)');
                        break;
                    default:
                        // statements_def
                        break;
                }    
            }
        });
    }
    function updateChartData(period, callback){
        eb.ajax({
            type: 'POST',
            url: BASE_URL + '/crm-dashboard-api/updateChartData',
            data: {period: period},
            success: function(data) {
                $('#target-chart').html('');
                $('#cash-chart').html('');
                $('#gp-chart').html('');
                
                customerVisitByPromotionData = data.customerVisitByPromotionData;
                salsVsGpChart(customerVisitByPromotionData);
                
                
            }
        });
        callback();
    }

    function updateFooterChartData(period, callback){
        eb.ajax({
            type: 'POST',
            url: BASE_URL + '/crm-dashboard-api/updateFooterChartData',
            data: {period: period},
            success: function(data) {
                

                var salesPromoTotal = 0;
                $.each(data.salesPromoData, function(index, val) {
                     if (index < 5) {
                        salesPromoTotal += parseFloat(val.invTotal);
                     }
                });

                salesAndGpSummaryChart(data.salesPromoData,salesPromoTotal);
                salesSummaryChart(data.oldCustomerAquisitionData, data.newCustomerAquisitionData);   
            }
        });
        callback();
    }

    function getWidgetData(period){
        eb.ajax({
            type: 'POST',
            url: BASE_URL + '/crm-dashboard-api/getWidgetData',
            data: {period: period},
            success: function(data) {
                $('#target-chart').html('');
                $('#cash-chart').html('');
                $('#gp-chart').html('');
                $('#total-sales').each(function () {
                    $(this).prop('Counter',0).animate({
                        Counter: data.totalCustomer
                    }, {
                        duration: 1000,
                        easing: 'swing',
                        step: function (now) {
                            $(this).text(Math.ceil(now));
                        }
                    });
                });
                $('#gross-profit').each(function () {
                    $(this).prop('Counter',0).animate({
                        Counter: data.totalCustomerVisits
                    }, {
                        duration: 1000,
                        easing: 'swing',
                        step: function (now) {
                            $(this).text(Math.ceil(now));
                        }
                    });
                });
                $('#total-payments').each(function () {
                    $(this).prop('Counter',0).animate({
                        Counter: data.totalRedeemedPoints
                    }, {
                        duration: 1000,
                        easing: 'swing',
                        step: function (now) {
                            $(this).text(accounting.formatMoney(now));
                        }
                    });
                });
                $('#totalInvoiceCount').each(function () {
                    $(this).prop('Counter',0).animate({
                        Counter: data.totalCollectedPoints
                    }, {
                        duration: 1000,
                        easing: 'swing',
                        step: function (now) {
                            $(this).text(accounting.formatMoney(now));
                        }
                    });
                });

                if (data.customerVisitProfitLoss > 0) {
                    $('#total-gross-profit').removeClass('hidden');
                    $('#total-gross-loss').addClass('hidden');
                    $('#total-gross-profit').html('+'+data.customerVisitProfitLoss+'%<span><i class="glyphicon glyphicon-arrow-up"></i></span>');
                } else {
                    $('#total-gross-profit').addClass('hidden');
                    $('#total-gross-loss').removeClass('hidden');
                    var loss = data.customerVisitProfitLoss * -1;
                    $('#total-gross-loss').html('+'+loss+'%<span><i class="glyphicon glyphicon-arrow-down"></i></span>');
                }

                if (data.loyaltyRedeemProfitLoss > 0) {
                    $('#total-payment-profit').removeClass('hidden');
                    $('#total-payment-loss').addClass('hidden');
                    $('#total-payment-profit').html('+'+data.loyaltyRedeemProfitLoss+'%<span><i class="glyphicon glyphicon-arrow-up"></i></span>');
                } else {
                    $('#total-payment-profit').addClass('hidden');
                    $('#total-payment-loss').removeClass('hidden');
                    var loss = data.loyaltyRedeemProfitLoss * -1;
                    $('#total-payment-loss').html('+'+loss+'%<span><i class="glyphicon glyphicon-arrow-down"></i></span>');
                }

                if (data.loyaltyEarnedProfitLoss > 0) {
                    $('#total-invoice-count-profit').removeClass('hidden');
                    $('#total-invoice-count-loss').addClass('hidden');
                    $('#total-invoice-count-profit').html('+'+data.loyaltyEarnedProfitLoss+'%<span><i class="glyphicon glyphicon-arrow-up"></i></span>');
                } else {
                    $('#total-invoice-count-profit').addClass('hidden');
                    $('#total-invoice-count-loss').removeClass('hidden');
                    var loss = data.loyaltyEarnedProfitLoss * -1;
                    $('#total-invoice-count-loss').html('+'+loss+'%<span><i class="glyphicon glyphicon-arrow-down"></i></span>');
                }

                var salesPromoTotal = 0;
                $.each(data.salesPromoData, function(index, val) {
                	 if (index < 5) {
                	 	salesPromoTotal += parseFloat(val.invTotal);
                	 }
                });

                salesAndGpSummaryChart(data.salesPromoData,salesPromoTotal);
                salesSummaryChart(data.oldCustomerAquisitionData, data.newCustomerAquisitionData);
             
                customerSales = data.customerSales;
                customerVisitByPromotionData = data.customerVisitByPromotionData;
                // $('#cash-chart').html('');
                salsVsGpChart(customerVisitByPromotionData);
                $.each(data.mostCustomerVisits, function(index, val) {
                    if (index < 10) {
                        $('.customerName'+index).text(val.customerName);
                        $('.customerVisits'+index).text(val.invCount);
                    }
                });

                if (data.mostCustomerVisits.length < 10) {
                    for (var i = 0; i < 10; i++) {
                        if (i >= data.mostCustomerVisits.length) {
                            $('.customerName'+i).text('');
                            $('.customerVisits'+i).text('');
                        }
                    }
                }

                switch (period) {
                    case 'thisYear':
                        $('#last-period-total-gross').text('Compared to ('+data.lastPeriodTotalCustomerVisits+' last year)');
                        $('#last-period-total-payment').text('Compared to ('+data.lastPeriodtotalRedeemedPoints+' last year)');
                        $('#last-period-total-invoices').text('Compared to ('+data.lastPeriodtotalCollectedPoints+' last year)');
                        break;
                    case 'thisMonth':
                        $('#last-period-total-gross').text('Compared to ('+data.lastPeriodTotalCustomerVisits+' last month)');
                        $('#last-period-total-payment').text('Compared to ('+data.lastPeriodtotalRedeemedPoints+' last month)');
                        $('#last-period-total-invoices').text('Compared to ('+data.lastPeriodtotalCollectedPoints+' last month)');
                        break;
                    case 'thisWeek':
                        $('#last-period-total-gross').text('Compared to ('+data.lastPeriodTotalCustomerVisits+' last week)');
                        $('#last-period-total-payment').text('Compared to ('+data.lastPeriodtotalRedeemedPoints+' last week)');
                        $('#last-period-total-invoices').text('Compared to ('+data.lastPeriodtotalCollectedPoints+' last week)');
                        break;
                    case 'thisDay':
                        $('#last-period-total-gross').text('Compared to ('+data.lastPeriodTotalCustomerVisits+' last day)');
                        $('#last-period-total-payment').text('Compared to ('+data.lastPeriodtotalRedeemedPoints+' last day)');
                        $('#last-period-total-invoices').text('Compared to ('+data.lastPeriodtotalCollectedPoints+' last day)');
                        break;
                    default:
                        // statements_def
                        break;
                }
                
            }
        });
    }

    $('.period').on('change', function(event) {
        event.preventDefault();
        // getWidgetData($(this).val());
        updateDashboardData($(this).val());
    });


    function gpChart(endAngle)
    {
        var options1 = {
            chart: {
                height: 200,
                type: 'radialBar',
                toolbar: {
                    show: false
                }
            },
            plotOptions: {
                radialBar: {
                    startAngle: 0,
                    endAngle: parseFloat(endAngle),
                    hollow: {
                        margin: 0,
                        size: '70%',
                        background: '#fff',
                        image: undefined,
                        imageOffsetX: 0,
                        imageOffsetY: 0,
                        position: 'front',
                        dropShadow: {
                            enabled: true,
                            top: 3,
                            left: 0,
                            blur: 4,
                            opacity: 0.24
                        }
                    },
                    track: {
                        background: '#fff',
                        strokeWidth: '70%',
                        margin: 0, // margin is in pixels
                        dropShadow: {
                            enabled: true,
                            top: -3,
                            left: 0,
                            blur: 4,
                            opacity: 0.35
                        }
                    },

                    dataLabels: {
                        showOn: 'always',
                        name: {
                            offsetY: 0,
                            show: true,
                            color: '#888',
                            fontSize: '15px'
                        },
                        value: {
                            formatter: function(val) {
                                return parseInt(val);
                            },
                            color: '#111',
                            fontSize: '20px',
                            show: false,
                        }
                    }
                }
            },
            fill: {
                type: 'gradient',
                gradient: {
                    shade: 'dark',
                    type: 'horizontal',
                    shadeIntensity: 0.5,
                    gradientToColors: ['#ABE5A1'],
                    inverseColors: true,
                    opacityFrom: 1,
                    opacityTo: 1,
                    stops: [0, 100]
                }
            },
            series: [75],
            stroke: {
                lineCap: 'round'
            },
            labels: ['Gross Profit'],
        }
        var chart1 = new ApexCharts(
            document.querySelector("#target-chart"),
            options1
        );
        chart1.render();
    }

    $('#sales-gross-profit').on('click', function(event) {
        event.preventDefault();
        $('#gp-chart').removeClass('hidden');
        $('#cash-chart').addClass('hidden');
        if ($('#gp-chart')[0].children.length > 0) {
            $('#gp-chart')[0].children[0].remove();
        }
        salsVsGpChart(customerVisitByPromotionData);
    });

    function salsVsGpChart(customerVisitByPromotionData)
    {
        var oldCustomerVisits = [];
        var newCustomerVisits = [];
        var promotionName = [];
        var i = 0;
        $.each(customerVisitByPromotionData, function(index, val) {
            if (i < 10) {
                var oldCustomerVisit = (val.oldCustomerVisits == null) ? 0 : val.oldCustomerVisits;
                oldCustomerVisits.push(oldCustomerVisit);
                var newCustomerVisit = (val.newCustomerVisits == null) ? 0 : val.newCustomerVisits;
                newCustomerVisits.push(newCustomerVisit);
                promotionName.push(val.promotionName);
            }
            i++;
        });

        var colWidth = '50%';

        if (promotionName.length < 3) {
            colWidth = '10%';
        }

        var options = {
            chart: {
                height: 350,
                type: 'bar',
            },
            plotOptions: {
                bar: {
                    columnWidth: colWidth,   
                }
            },
            dataLabels: {
                enabled: false
            },
            stroke: {
                width: 2
            },
            series: [{
                name: 'Old Customer Visits',
                data: oldCustomerVisits
            }, {
                name: 'New Customer Visits',
                data: newCustomerVisits
            }],
            grid: {
                row: {
                    colors: ['#fff', '#f2f2f2']
                }
            },
            xaxis: {
                labels: {
                    rotate: -45
                },
                type: 'category',
                categories: promotionName,
            },
            tooltip: {
                x: {
                    format: 'dd/MM/yy HH:mm'
                }
            },
            fill: {
                type: 'gradient',
                gradient: {
                    shade: 'light',
                    type: "horizontal",
                    shadeIntensity: 0.25,
                    gradientToColors: undefined,
                    inverseColors: true,
                    opacityFrom: 0.85,
                    opacityTo: 0.85,
                    stops: [50, 0, 100]
                },
            },
        }

        var chart = new ApexCharts(
            document.querySelector("#gp-chart"),
            options
        );

        chart.render();
    }



    $('#cash-credit').on('click', function(event) {
        event.preventDefault();
        $('#cash-chart').removeClass('hidden');
        $('#gp-chart').addClass('hidden');
        if ($('#cash-chart')[0].children.length > 0) {
            $('#cash-chart')[0].children[0].remove();
        }
        $('#gp-chart').html('');
        salesAndReturnGraph(customerSales);
    });    

    function salesAndReturnGraph(customerSales)
    {
        $('#cash-chart').html('');
        var totalSales = [];
        var totalReturn = [];
        var customers = [];
        var options = {};
        var i = 0;
        
        $.each(customerSales, function(index, val) {
            if (i < 10) {
                var totalInvAmount = (val.totalInvAmount == undefined) ? 0 : val.totalInvAmount;
                totalSales.push(totalInvAmount);
                var totalReturnAmount = (val.totalReturnAmount == undefined) ? 0 : val.totalReturnAmount;
                totalReturn.push(totalReturnAmount);
                customers.push(val.customerName);
            }
            i++;
        });

        options = {
            chart: {
                height: 350,
                type: 'area',
            },
            dataLabels: {
                enabled: false
            },
            stroke: {
                curve: 'smooth'
            },
            series: [{
                name: 'Total Sales',
                data: totalSales
            }, {
                name: 'Total Returns',
                data: totalReturn
            }],

            xaxis: {
                type: 'category',
                categories: customers,                
            },
            yaxis: {
                labels: {
                    formatter: (value) => { return "Rs "+accounting.formatMoney(value) },
                }
            },
            tooltip: {
                y: {
                    formatter: function(value, { series, seriesIndex, dataPointIndex, w }) {
                        return "Rs "+accounting.formatMoney(value)
                    }
                }
            }
        }
        renderChart(options, 'cash-chart');
    }


    function renderChart(options, chartID)
    {
        var chart = new ApexCharts(
            document.querySelector("#"+chartID),
            options
        );
        chart.render();
    }


    function salesAndGpSummaryChart(salesPromoData, salesPromoTotal)
    {

        let chartConfig = {
            graphset : [

            ]
        };

        var gdata = {
            x : 0*200,
            y : 0*200,
            width : '100%',
            height : '100%',
            type : 'pie',
            noData:{
                text:"Currently there is no data in the chart",
                backgroundColor: "#5081e7",
                fontSize:18,
                textAlpha:.9,
                alpha:.6,
                bold:true
            },
            plotarea : {
                margin : '15 5 5 5'
            },
            "scale":{
                "size-factor":0.9
            },
            labels:[
                {
                    text:"Rs "+accounting.formatMoney(salesPromoTotal),
                    x:"50%", 
                    y:"47%",
                    anchor : 'c',
                    fontSize:"17px",
                    fontStyle : 'bold',
                    fontColor : 'black',
                    padding : "10px",
                },
                {
                    text:"Total <br> Sales by Promotion",
                    x:"50%", 
                    y:"58%",
                    fontSize:"13px",
                    anchor : 'c',
                    fontStyle : 'bold',
                    fontColor : 'grey',
                    padding : "10px",
                },
            ],
            plot : {
                borderWidth : 2,
                borderColor : 'white',
                shadow : true,
                shadowAlpha : 1,
                shadowAngle : -45,
                borderAlpha : 0.5,
                slice : '89%',
                pieTransform : 'flow=4',
                hoverState : {
                    visible : false    
                },
                tooltip : {
                    text : '%t <br> Rs %node-value',
                    thousandsSeparator:','
                },
                'background-color': "black",
                'value-box': {
                    text: "%t <br> Rs %v",
                    'font-color': "black",
                    'font-size': 10,
                    'background-color': "white",
                    borderColor : 'white',
                    shadow : true,
                    shadowAlpha : 1,
                    shadowAngle : -45,
                    borderAlpha : 0.5,
                    'border-width': 1,
                    padding: "5%",
                    borderRadius : '5px',
                    placement:'out',
                    thousandsSeparator:',',
                }
            },
            series:[
            ]
        };

        $.each(salesPromoData, function(index, val) {
        	if (index < 5) {
        		if (index == 0) {
        			var series = {
		                "values":[parseFloat(val.invTotal)],  
		                "text":val.promotionName,  
		                "background-color":"#3023AE",
		            }
		            gdata.series.push(series);
        		} else if (index == 1) {
        			var series = {
		                "values":[parseFloat(val.invTotal)],  
		                "text":val.promotionName,  
		                "background-color":"#F76B1C",
		            }
		            gdata.series.push(series);
        		} else if (index == 2) {
					var series = {
		                "values":[parseFloat(val.invTotal)],  
		                "text":val.promotionName,  
		                "background-color":"#1298FF",
		            }
		            gdata.series.push(series);
        		} else if (index == 3) {
        			var series = {
		                "values":[parseFloat(val.invTotal)],  
		                "text":val.promotionName,  
		                "background-color":"#ff6666",
		            }
		            gdata.series.push(series);
        		} else if (index == 4) {
        			var series = {
		                "values":[parseFloat(val.invTotal)],  
		                "text":val.promotionName,  
		                "background-color":"#ff99cc",
		            }
		            gdata.series.push(series);
        		}
        	}
        });
        chartConfig.graphset.push(gdata);

        setTimeout(function(){ 
	        zingchart.render({
	            id: 'sales-gp-summary',
	            width: '100%',
	            height: '100%',
	            data: chartConfig
	        });
            if (salesPromoTotal == 0) {
                $('#sales-gp-summary .zc-text').remove();
            }
        }, 500);

    }


    function salesSummaryChart(oldCustomerAquisitionData, newCustomerAquisitionData)
    {
        let chartConfig = {
            graphset : [

            ]
        };

        var gdata = {
            x : 0*200,
            y : 0*200,
            width : '100%',
            height : '100%',
            type : 'pie',
            noData:{
                text:"Currently there is no data in the chart",
                backgroundColor: "#5081e7",
                fontSize:18,
                textAlpha:.9,
                alpha:.6,
                bold:true
            },
            plotarea : {
                margin : '15 5 5 5'
            },
            "scale":{
                "size-factor":0.9
            },
            labels:[
                {
                    text:(parseFloat(oldCustomerAquisitionData.invCount) + parseFloat(newCustomerAquisitionData.invCount)),
                    x:"50%", 
                    y:"47%",
                    anchor : 'c',
                    fontSize:"17px",
                    fontStyle : 'bold',
                    fontColor : 'black',
                    padding : "10px",
                },
                {
                    text:"Total <br> Visits",
                    x:"50%", 
                    y:"58%",
                    fontSize:"13px",
                    anchor : 'c',
                    fontStyle : 'bold',
                    fontColor : 'grey',
                    padding : "10px",
                },
            ],
            plot : {
                borderWidth : 2,
                borderColor : 'white',
                shadow : true,
                shadowAlpha : 1,
                shadowAngle : -45,
                borderAlpha : 0.5,
                slice : '89%',
                pieTransform : 'flow=4',
                hoverState : {
                    visible : false    
                },
                tooltip : {
                    text : '%t <br> %node-value',
                    thousandsSeparator:','
                },
                'background-color': "black",
                'value-box': {
                    text: "%t <br> %v",
                    'font-color': "black",
                    'font-size': 10,
                    'background-color': "white",
                    borderColor : 'white',
                    shadow : true,
                    shadowAlpha : 1,
                    shadowAngle : -45,
                    borderAlpha : 0.5,
                    'border-width': 1,
                    padding: "5%",
                    borderRadius : '5px',
                    placement:'out',
                    thousandsSeparator:',',
                    // offsetX: 15,
                    offsetY: -15,
                }
            },
            series:[  
            ]
        };

        if (parseFloat(oldCustomerAquisitionData.invCount) > 0) {
            var series = {
                "values":[parseFloat(oldCustomerAquisitionData.invCount)],  
                "text":"Repeat Customers",  
                "background-color":"#3023AE",
            }
            gdata.series.push(series);
        }

        if (parseFloat(newCustomerAquisitionData.invCount) > 0) {
            var series = {
                "values":[parseFloat(newCustomerAquisitionData.invCount)],  
                "text":"New Customers",  
                "background-color":"#F76B1C",
            }
            gdata.series.push(series);
        }

        chartConfig.graphset.push(gdata);

        zingchart.render({
            id: 'sales-summary',
            width: '100%',
            height: '100%',
            data: chartConfig
        });

        if (parseFloat(oldCustomerAquisitionData.invCount) == 0 && parseFloat(newCustomerAquisitionData.invCount) == 0) {
            $('#sales-summary .zc-text').remove();
        }
    }

    
});


