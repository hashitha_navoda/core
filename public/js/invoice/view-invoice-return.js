var tmptotal;
var invoiceReturnID;
var customerID;
var ignoreBudgetLimitFlag = false;

$(document).ready(function() {

    $('#invoice-return-search-select').val('Invoice Return No');
    $('#invoice-return-custa-search').selectpicker('hide');
    $('#invoice-return-search-select').on('change', function() {
        if ($(this).val() == 'Invoice Return No') {
            $('#invoice-return-custa-search').selectpicker('hide');
            $('#invoice-return-search').selectpicker('show');
            $('#invoice-return-custa-search').val('');
            $('#invoice-return-custa-search').selectpicker('render');
            $('#invoice-return-list').show();
            selectedsearch = 'Invoice No';
            customerID = '';
        } else if ($(this).val() == 'Customer Name') {
            $('#invoice-return-search').selectpicker('hide');
            $('#invoice-return-custa-search').selectpicker('show');
            $('#invoice-return-search').val('');
            $('#invoice-return-search').selectpicker('render');
            selectedsearch = 'Customer Name';
            invoiceReturnID = '';
        }
    });

    loadDropDownFromDatabase('/customerAPI/search-customers-for-dropdown', "", 'withDeactivatedCustomers', '#invoice-return-custa-search');
    $('#invoice-return-custa-search').selectpicker('refresh');
    $('#invoice-return-custa-search').on('change', function() {
        if ($(this).val() > 0 && customerID != $(this).val()) {
            customerID = $(this).val();
            var getreturnsbycusturl = '/invoice-return-api/retriveCustomerInvoiceReturn';
            var requestinvbycust = eb.post(getreturnsbycusturl, {
                customerID: customerID
            });
            requestinvbycust.done(function(retdata) {
                if (retdata == "noCreditNote") {
                    p_notification(false, eb.getMessage('ERR_VIEWCRNOTE_NO_CRDNCUS'));
                } else {
                    $('#invoice-return-list').html(retdata);
                }
            });
        }
    });

    $("#invoice-return-list").on("click", '.preview', function (e) {
        e.preventDefault();

        showInvoiceReturnPreview(BASE_URL + $(this).data("href"));

        return false;
    });

    showInvoiceReturnPreview = function (url) {
        var iframe = 'documentpreview';
        var path = "/invoice-return-api/send-email";
        documentPreview(url, iframe, path);
    }

    loadDropDownFromDatabase('/invoice-return-api/search-all-invoice-return-for-dropdown', locationID, 0, '#invoice-return-search');
    $('#invoice-return-search').selectpicker('refresh');
    $('#invoice-return-search').on('change', function() {
        if ($(this).val() > 0 && invoiceReturnID != $(this).val()) {
            invoiceReturnID = $(this).val();
            var searchurl = BASE_URL + '/invoice-return-api/retriveInvoiceReturnFromInvoiceReturnID';
            var searchrequest = eb.post(searchurl, {invoiceReturnID: invoiceReturnID});
            searchrequest.done(function(searchdata) {
                $('#invoice-return-list').html(searchdata);
            });
        }
    });

    $('#filter-button').on('click', function() {
        if ($('#from-date').val() == '' || $('#to-date').val() == '') {
            p_notification(false, eb.getMessage('ERR_VIEWCRNOTE_FILLDATA'));
        } else {
            var creditNoteFilter = BASE_URL + '/invoice-return-api/retriveInvoiceReturnByDatefilter';
            var filterrequest = eb.post(creditNoteFilter, {
                'fromdate': $('#from-date').val(),
                'todate': $('#to-date').val(),
                'customerID': customerID
            });
            filterrequest.done(function(retdata) {
                if (retdata.msg == 'noReturns') {
                    p_notification(false, eb.getMessage('ERR_VIEWCRNOTE_NO_CRDDATE'));
                } else {
                    $('#invoice-return-list').html(retdata);
                }
            });
        }
    });

    ///////DatePicker\\\\\\\
    var nowTemp = new Date();
    var frmdate;
    var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);

    var checkin = $('#from-date').datepicker().on('changeDate', function(ev) {
        checkin.hide();
        frmdate = ev.date;
        $('#to-date')[0].focus();
    }).data('datepicker');
    var checkout = $('#to-date').datepicker({
        onRender: function(date) {
            return date.valueOf() < frmdate ? 'disabled' : '';
        }
    }).on('changeDate', function(ev) {
        checkout.hide();
    }).data('datepicker');
/////EndOFDatePicker\\\\\\\\\

    //responsive issue
    $(window).bind('resize ready', function() {
        ($(window).width() <= 1200) ? $('#filter-button').addClass('margin_top_low') : $('#filter-button').removeClass('margin_top_low');
    });

    $(window).bind('resize ready', function() {
        ($(window).width() <= 480) ? $('#filter-button').addClass('col-xs-12') : $('#filter-button').removeClass('col-xs-12');
    });

    $("#invoice-return-list").on('click', '.deleteInvoiceReturn', function(event) {
        var invoiceReturnId = $(this).attr('data-invoiceReturnID');
        deleteInvoiceReturn(invoiceReturnId);        
    });

    function deleteInvoiceReturn(invoiceReturnId)
    {
        bootbox.confirm("Are you sure you want to cancel this Invoice Return ?", function(result) {
            if (result == true) {
                eb.ajax({
                    type: 'POST',
                    url: BASE_URL + '/invoice-return-api/delete-invoice-return',
                    data: {
                        invoiceReturnID: invoiceReturnId,
                        creditNoteTypeFlag: "CreditNote",
                        ignoreBudgetLimit: ignoreBudgetLimitFlag
                    },
                    success: function(respond) {
                        if (respond.status) {
                            p_notification(respond.status, respond.msg);
                            window.setTimeout(function() {
                                location.reload();
                            }, 1000);
                        } else {
                            if (respond.data == "NotifyBudgetLimit") {
                                bootbox.confirm(respond.msg+' ,Are you sure you want to cancel this Invoice Return ?', function(result) {
                                    if (result == true) {
                                        ignoreBudgetLimitFlag = true;
                                        deleteInvoiceReturn(invoiceReturnId);
                                    } else {
                                        setTimeout(function(){ 
                                            location.reload();
                                        }, 3000);
                                    }
                                });
                            } else {
                                p_notification(respond.status, respond.msg);
                            }
                        }
                    },
                    async: false
                });
            }
        });
    }


    $("#draft-invoice-return-list").on('click', '.deleteDraftInvoiceReturn', function(event) {
        var draftInvoiceReturnID = $(this).attr('data-draftInvoiceReturnID');
        deleteDraftInvoiceReturn(draftInvoiceReturnID);        
    });

    function deleteDraftInvoiceReturn(draftInvoiceReturnID)
    {
        bootbox.confirm("Are you sure you want to cancel this Draft Invoice Return ?", function(result) {
            if (result == true) {
                eb.ajax({
                    type: 'POST',
                    url: BASE_URL + '/invoice-return-api/delete-draft-invoice-return',
                    data: {
                        draftInvoiceReturnID: draftInvoiceReturnID,
                        creditNoteTypeFlag: "CreditNote",
                        ignoreBudgetLimit: ignoreBudgetLimitFlag
                    },
                    success: function(respond) {
                        if (respond.status) {
                            p_notification(respond.status, respond.msg);
                            window.setTimeout(function() {
                                location.reload();
                            }, 1000);
                        } else {
                            if (respond.data == "NotifyBudgetLimit") {
                                bootbox.confirm(respond.msg+' ,Are you sure you want to cancel this Invoice Return ?', function(result) {
                                    if (result == true) {
                                        ignoreBudgetLimitFlag = true;
                                        deleteInvoiceReturn(invoiceReturnId);
                                    } else {
                                        setTimeout(function(){ 
                                            location.reload();
                                        }, 3000);
                                    }
                                });
                            } else {
                                p_notification(respond.status, respond.msg);
                            }
                        }
                    },
                    async: false
                });
            }
        });
    }


    $('#invoice-return-list').on('click', '.cn_related_docs', function() {
        setDataToHistoryModal($(this).attr('data-cn-related-id'));
        $('#addDocHistoryModal').modal('show');
    });

    $('#invoice-return-list').on('click', '.doc_attachments', function() {
        setDataToAttachmentViewModal($(this).attr('data-cn-related-id'));
        $('#viewAttachmentModal').modal('show');
    });

    $('#doc-history-table').on('click', '.document_view', function() {
        var documentId = $(this).parents('tr').attr('data-docid');
        var documentType = $(this).parents('tr').attr('data-doctype');
        $('#DocumentViewModal').modal('show');
        getPrintPreviewByDocumentTypeAndDocumentID(documentId, documentType);
    });
});

function setDataToAttachmentViewModal(documentID) {
    $('#doc-attach-table tbody tr').remove();
    $('#doc-attach-table tfoot div').remove();
    $('#doc-attach-table tbody div').remove();

    eb.ajax({
        type: 'POST',
        url: BASE_URL + '/get-document-related-attachement',
        data: {
            documentID: documentID,
            documentTypeID: 6
        },
        success: function(respond) {
            if (respond.status == true) {
                $('#doc-attach-table thead tr').removeClass('hidden');
                $.each(respond.data, function(index, value) {
                    tableBody = "<tr><td><a href="+value.link+">"+value.docName+"</a></td></tr>";
                    $('#doc-attach-table tbody').append(tableBody);
                });
            } else {
                $('#doc-attach-table thead tr').addClass('hidden');
                var noDataFooter = "<div class= ' col-lg-6 text-right'><label><b>No Data</b></label>";
                $('#doc-attach-table tbody').append(noDataFooter);
            }
        }
    });
}

function setDataToHistoryModal(invoiceReturnID) {
    $('#doc-history-table tbody tr').remove();
    $('#doc-history-table tfoot div').remove();
    $('#doc-history-table tbody div').remove();

    eb.ajax({
        type: 'POST',
        url: BASE_URL + '/credit-note-api/getAllRelatedDocumentDetailsByCreditNoteID',
        data: {invoiceReturnID: invoiceReturnID},
        success: function(respond) {
            if (respond.status == true) {
                $('#doc-history-table thead tr').removeClass('hidden');
                $.each(respond.data, function(index, value) {
                    if (value != null) {
                        $.each(value, function(index2, value2) {
                            tableBody = "<tr data-docId=" + value2['documentID'] + " data-doctype=" + value2['type'] + "><td class='' >" + value2['type'] + "</td><td>" + value2['code'] + "</td><td>" + value2['amount'] + "</td><td>" + value2['issuedDate'] + "</td><td class='text-center'><span class='glyphicon glyphicon-eye-open openlist link_color document_view ' style='cursor:pointer'></span></td></tr>";
                            $('#doc-history-table tbody').append(tableBody);
                        });
                    }
                });
                var footer = " <div class='col-lg-12 col-md-12 col-sm-12 text-right'><button type='button' class='btn btn-primary col-lg-12 col-md-12 col-sm-12' data-dismiss='modal' aril-hidden='true'>Back</button></div>";
                $('#doc-history-table tfoot').append(footer);
            } else {
                $('#doc-history-table thead tr').addClass('hidden');
                var noDataFooter = "<div class= ' col-lg-9 text-right'><label><b>No Data</b></label>";
                $('#doc-history-table tbody').append(noDataFooter);
            }
        }
    });
}

function getPrintPreviewByDocumentTypeAndDocumentID(documentID, documentType) {
    var $iframe = $('#related-document-view');
    $iframe.ready(function() {
        $iframe.contents().find("body div").remove();
    });
    var URL;
    if (documentType == 'Quotation') {
        URL = BASE_URL + '/quotation/document/' + documentID;
    } else if (documentType == 'SalesOrder') {
        URL = BASE_URL + '/salesOrders/document/' + documentID;
    }
    else if (documentType == 'DeliveryNote') {
        URL = BASE_URL + '/delivery-note/document/' + documentID;
    }
    else if (documentType == 'SalesReturn') {
        URL = BASE_URL + '/return/document/' + documentID;
    }
    else if (documentType == 'SalesInvoice') {
        URL = BASE_URL + '/invoice/document/' + documentID;
    }
    else if (documentType == 'CustomerPayment') {
        URL = BASE_URL + '/customerPayments/document/' + documentID;
    }
    else if (documentType == 'CreditNote') {
        URL = BASE_URL + '/credit-note/document/' + documentID;
    } 
    else if (documentType == 'Sales Invoice Return') {
        URL = BASE_URL + '/invoice-return/document/' + documentID;
    } 
    else if (documentType == 'CreditNotePayment') {
        URL = BASE_URL + '/credit-note-payments/document/' + documentID;
    }

    eb.ajax({
        type: 'POST',
        url: URL,
        success: function(respond) {
            var division = "<div></div>";
            $iframe.ready(function() {
                $iframe.contents().find("body").append(division);
            });
            $iframe.ready(function() {
                $iframe.contents().find("body div").append(respond);
            });
        }
    });

}