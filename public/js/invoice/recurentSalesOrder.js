/**
 * @author ASHAN <ashan@thinkcube.com>
 * This file contains recurrent Invoice js
 */
$(document).ready(function() {

    $('#st').hide();
    $('#ty').hide();
    $('#Type').val('SalesOrder');

//    getSalesOrdersList();
    $('#salesOrderID').typeahead({source: sales_orders_list}).attr('autocomplete', 'off');

    $('#checkEndDate').hide();
    $('#selectEndDate').on('click', function() {
        if ($(this).is(":checked")) {
            $('#checkEndDate').slideDown();
        } else {
            $('#checkEndDate').slideUp();
        }
    });

    function validate_form(valid) {
        var inv = valid[0];
        var name = valid[1];
        var t_value = valid[2];
        var t_format = valid[3];
        var s_date = valid[4];
        var e_date = valid[5];
        if (inv == null || inv == "") {
            p_notification(false, eb.getMessage('ERR_RECSALES_ORDERNUM'));
        } else if (name == null || name == "") {
            p_notification(false, eb.getMessage('ERR_QUOTEDIT_NAME_BLANK'));
        } else if (t_value == null || t_value == "") {
            p_notification(false, eb.getMessage('ERR_RECINVO_VAL_BLANK'));
        } else if (isNaN(t_value)) {
            p_notification(false, eb.getMessage('ERR_RECINVO_VAL_NUMR'));
        } else if (t_format == null || t_format == "") {
            p_notification(false, eb.getMessage('ERR_RECINVO_TIMEFORAT'));
        } else if (s_date == null || s_date == "") {
            p_notification(false, eb.getMessage('ERR_RECINVO_STARTDATE'));
        } else if ($('#selectEndDate').is(":checked")) {
            if (e_date == null || e_date == "") {
                p_notification(false, eb.getMessage('ERR_RECINVO_ENDDATE'));
            } else {
                return true;
            }
        } else {
            $('#endDate').val('');
            return true;
        }
    }

    $('#recurrent_sales_order').on('click', function(e) {
        e.preventDefault();
        var url1 = BASE_URL + '/salesOrdersAPI/getRecurrentSalesOrder';
        var id;
        id = $('#salesOrderID').val();
        eb.ajax({
            type: 'post',
            url: url1,
            data: {
                id: id,
            },
            dataType: "json",
            async: false,
            success: function(data) {
                if (data['result'] == 'null') {
                    if (data['salesOrder'] != null) {
                        var val;
                        val = $('#salesOrderID').val();
                        var valid = new Array(
                                val,
                                $('#name').val(),
                                $('#timeValue').val(),
                                $('#timeFormat').val(),
                                $('#startDate').val(),
                                $('#endDate').val()
                                );
                        if (validate_form(valid)) {
                            var url = BASE_URL + '/salesOrdersAPI/saveRecurrentSalesOrder';
                            var submitForm = eb.post(url, {
                                id: val,
                                state: 'Active',
                                name: $('#name').val(),
                                timeValue: $('#timeValue').val(),
                                timeFormat: $('#timeFormat').val(),
                                startDate: $('#startDate').val(),
                                endDate: $('#endDate').val(),
                                Type: $('#Type').val(),
                            });
                            submitForm.done(function(data) {
                                if (data['result'] == true) {
                                    p_notification(true, eb.getMessage('SUCC_RECINVO_RECUR_INVO'));
                                    window.setTimeout(function() {
                                        location.reload();
                                    }, 1000);
                                } else if (data['result'] == false) {
                                    p_notification(false, eb.getMessage('ERR_RECINVO_CORRECT_DATE'));
                                }
                            });
                        }
                    } else {
                        p_notification(false, eb.getMessage('ERR_RECINVO_INVO_NOTEXIST',id));
                    }
                } else {
                    p_notification(false, eb.getMessage('ERR_RECINVO_INVO_EXIST',id));
                }
            }
        });

    });


    var nowTemp = new Date();
    var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);

    var checkin = $('#startDate').datepicker({
        onRender: function(date) {
            return date.valueOf() < now.valueOf() ? 'disabled' : '';
        }
    }).on('changeDate', function(ev) {
        if (ev.date.valueOf() > checkout.date.valueOf()) {
            var newDate = new Date(ev.date)
            newDate.setDate(newDate.getDate() + 1);
            checkout.setValue(newDate);
        }
        checkin.hide();
        $('#endDate')[0].focus();
    }).data('datepicker');
    var checkout = $('#endDate').datepicker({
        onRender: function(date) {
            return date.valueOf() <= checkin.date.valueOf() ? 'disabled' : '';
        }
    }).on('changeDate', function(ev) {
        checkout.hide();
    }).data('datepicker');


    //layout issue
    $(window).bind('resize ready', function() {
        ($(window).width()) <= 480 ?
                $('.time_value , .time_format').removeClass('col-xs-6').addClass('col-xs-12').css('margin-bottom', '10px')
                :
                $('.time_value, .time_format').removeClass('col-xs-12').addClass('col-xs-6').css('margin-bottom', '0');

        if ($(window).width() <= 480) {
            $('.recurent_button').removeClass('text-right');
            $('#recurrent_sales_order').addClass('col-xs-12').css('margin-bottom', '8px');
            $('.reset_button').addClass('col-xs-12');
            $('.h1_no_margin').addClass('recurrent_sales_order_style');
            $('.nav-tabs > li').css('float', 'none');
        } else {
            $('.recurent_button').addClass('text-right');
            $('#recurrent_sales_order').removeClass('col-xs-12').css('margin-bottom', '0px');
            $('.reset_button').removeClass('col-xs-12');
            $('.h1_no_margin').removeClass('recurrent_sales_order_style');
            $('.nav-tabs > li').css('float', 'left');
        }
    });

});