/**
 * @author Prathap Weerasinghe <prathap@thinkcube.com>
 * This file contains sales Order js
 */
var tmptotal, state = "new";
var tax_rates = new Array();
var products = new Array();
var taxdetails = new Array();
var currentProducts = new Array();
var projectID;
var jobID;
var activityID;
var jobRefType;
var jobRefTypeID;
var selectedProductID;
var customerID;
var customCurrencySymbol = companyCurrencySymbol;
var cusProfID;
var addCustomer = false;
var priceListItems = [];
var $productTable = $("#item_tbl");

function product(pCode, pName, quantity, uom, uPrice, discount, dType, tax, tCost, taxDetails, productType, item_discription) {
    this.item_code = pCode;
    this.item = pName;
    this.quantity = quantity;
    this.discount = discount;
    this.discountType = dType;
    this.unit_price = uPrice;
    this.uom = uom;
    this.total_cost = tCost;
    this.taxes = tax;
    this.taxDetails = taxDetails;
    this.productType = productType;
    this.item_discription = item_discription;
}

$(document).ready(function() {
    $('#disc_presentage').attr('checked', 'checked');
    invoice = true;
    var all_tx_val = new Array();
    var totalTaxList = {};
    var totaldiscount = 0, decimalPoints = 0, itemDecimalPoints = 0;
    var priceListId = '';

    locationOut = $('#idOfLocation').val();

    $('.deli_row, #deli_form_group, #tax_td').hide();
    $("#customer_more").hide();
    $("#currency").attr("disabled", "disabled");
    $("#moredetails").click(function() {
        $("#customer_more").slideDown();
        $('#moredetails').hide();
        $('#customerCurrentBalance', '#addcustomerform').attr('disabled', true);
        $('#customerCurrentCredit', '#addcustomerform').attr('disabled', true);
    });

    var getAddRow = function(proIncID) {
        return $('tr.add-row:not(.sample)', $productTable);
    };

    function  setItemCost() {
        var id = $('#item_code').val();
        var productType = $('#item_code').data('PT');
        var price = parseFloat(accounting.unformat($('#price').val()));
        var qty = parseFloat($('#qty').val());
        if (productType == 2 && (qty == '' || qty == 0 || isNaN(qty))) {
            qty = 1;
        }
        var disc = parseFloat(accounting.unformat($('#discount').val()));
        var tax = new Array();
        var taxA = 0;
        var tot = 0;
        taxdetails = new Array();
        $('#addTaxUl input:checked').each(function() {
            if (!$(this).is(':disabled')) {
                tax.push($(this).attr("value"));
            }
        });
        if (jQuery.isEmptyObject(tax)) {
            $('#taxApplied').addClass('glyphicon glyphicon-unchecked');
        } else {
            $('#taxApplied').removeClass('glyphicon glyphicon-unchecked');
            $('#taxApplied').addClass('glyphicon glyphicon-check');
        }
        tot = (price * qty) - calculateDiscount(id, price * qty, disc, qty);
        if ($('#total_discount_rate').val() != '' && $('input[name=discount_type]:checked').val() == 'presentage') {
            var invoiceDiscountValue = $('#total_discount_rate').val();
            tot -= (tot * (toFloat(invoiceDiscountValue) / toFloat(100)));
        }
        taxdetails = calculateItemCustomTax(tot, tax);
        var taxTotalAmount = taxdetails === null ? 0 : taxdetails.tTA;
        taxA = taxTotalAmount === undefined ? 0 : parseFloat(taxTotalAmount);

        $("#total").html(isNaN((tot + taxA)) ? "0.00" : accounting.formatMoney((tot + taxA).toFixed(2)));

    }

    $('#show_addi').on('click', function() {
        if (this.checked) {
            $('#additional_div').removeClass('hidden');
        }
        else {
            $('#additional_div').addClass('hidden');
        }
    });


    function setTotalCost() {
        var total = 0, disc = 0;
        for (var i in products) {
            var tax = 0;
            for (var tx in products[i].taxes) {
                tax += products[i].taxes[tx];
            }
            total += parseFloat(products[i].total_cost);
        }
        var delivercharge = parseFloat(accounting.unformat($('#deli_amount').val()));
        if (delivercharge == '') {
            $('#finaltotal').html(accounting.formatMoney((total).toFixed(2)));
            $('#subtotal').html(accounting.formatMoney((total).toFixed(2)));
        } else {
            finalt = ((parseFloat(total) + parseFloat(delivercharge)).toFixed(2));
            $('#finaltotal').html(accounting.formatMoney(finalt));
        }
        if ($('#total_discount_rate').val() != '' && $('input[name=discount_type]:checked').val() == 'Value') {

            var dicountVal = $('#total_discount_rate').val();

            if (parseFloat(dicountVal) > parseFloat(total)) {
                $('#total_discount_rate').val('');
                dicountVal = 0;
                decimalPoints = 0;
            }

            finalt = ((parseFloat(total) - parseFloat(dicountVal)).toFixed(2));
            $('#finaltotal').html(accounting.formatMoney(finalt));
            $('#subtotal').html(accounting.formatMoney(finalt));
        }
        setTotalTax();
    }

    function setTotalTax() {
        if (tax_rates.length < 1) {
            eb.ajax({
                type: 'POST',
                url: '/taxAPI/getTaxRates',
                success: function(res) {
                    tax_rates = res;
                },
                async: false
            });
        }
        totalTaxList = {};
        if (TAXSTATUS) {
            for (var k in products) {
                for (var l in products[k].taxDetails.tL) {
                    if (totalTaxList.hasOwnProperty(l)) {
                        var curTA = totalTaxList[l].tA;
                        var newTA = (toFloat(curTA) + toFloat(products[k].taxDetails.tL[l].tA));
                        totalTaxList[l].tA = newTA.toFixed(2);
                    } else {
                        totalTaxList[l] = {tN: products[k].taxDetails.tL[l].tN, tP: products[k].taxDetails.tL[l].tP, tA: products[k].taxDetails.tL[l].tA};
                    }
                }
            }
            var totalTaxHtml = "";
            for (var t in totalTaxList) {
                totalTaxHtml += totalTaxList[t].tN + "(" + totalTaxList[t].tP + "%)&nbsp" + (accounting.formatMoney(totalTaxList[t].tA)) + "<br>";
            }
            $("#tax_td").html(totalTaxHtml);
        }
    }

    var documentType = 'quotation';
    loadDropDownFromDatabase('/productAPI/search-location-products-for-dropdown', locationID, "", '#item_code', '', '', documentType);
    $('#item_code').on('change', function() {
        if ($(this).val() > 0 && selectedProductID != $(this).val()) {
            selectedProductID = $(this).val();
            $('#qty').parent().addClass('input-group');
            itemChange(selectedProductID);
        }
    });


    loadDropDownFromDatabase('/customerAPI/search-customers-for-dropdown', "", 0, '#cust_name');
    $('#cust_name').selectpicker('refresh');
    $('#cust_name').on('change', function() {
        if ($('#cust_name').val() > 0 && $('#cust_name').val() != customerID) {
            customerID = $('#cust_name').val();
            getCustomerDetails(customerID);
            getCustomerProfilesDetails(customerID);
        }
    });
//load all project details
    loadDropDownFromDatabase('/project-api/search-projects-for-dropdown', "", 0, '#projectReference');
    $('#projectReference').selectpicker('refresh');
    $('#projectReference').on('change', function() {
        jobRefTypeID = $('#projectReference').val();

    });
    //load all job details
    loadDropDownFromDatabase('/job-api/search-jobs-for-dropdown', "", 0, '#jobReference');
    $('#jobReference').selectpicker('refresh');
    $('#jobReference').on('change', function() {
        jobRefTypeID = $('#jobReference').val();

    });
    //load all activity details
    loadDropDownFromDatabase('/api/activity/search-activities-for-dropdown', "", 0, '#activityReference');
    $('#activityReference').selectpicker('refresh');
    $('#activityReference').on('change', function() {
        jobRefTypeID = $('#activityReference').val();

    });

    $('#jobCardType').on('change', function() {
        if ($(this).val() == '1') {
            $('.pro-ref').removeClass('hidden');
            $('.job-ref').addClass('hidden');
            $('.act-ref').addClass('hidden');
        }
        else if ($(this).val() == '2') {
            $('.job-ref').removeClass('hidden');
            $('.pro-ref').addClass('hidden');
            $('.act-ref').addClass('hidden');
        }
        else if ($(this).val() == '3') {
            $('.act-ref').removeClass('hidden');
            $('.job-ref').addClass('hidden');
            $('.pro-ref').addClass('hidden');
        }
    });


    $('#total_discount_rate').on('keyup', function() {
        if ($('#disc_value').is(":checked")) {
            if ((!$.isNumeric($(this).parents('tr').find('#total_discount_rate').val()) || $(this).parents('tr').find('#total_discount_rate').val() < 0)) {
                decimalPoints = 0;
                $(this).parents('tr').find('#total_discount_rate').val('');
            }
        } else {

            if ((!$.isNumeric($(this).parents('tr').find('#total_discount_rate').val()) || $(this).parents('tr').find('#total_discount_rate').val() < 0 || $(this).parents('tr').find('#total_discount_rate').val() > 100)) {
                decimalPoints = 0;
                $(this).parents('tr').find('#total_discount_rate').val('');
            }
        }

        setItemViceDiscount();
    });


    $('#item_tbl').on('change', '.addNewUomR', function() {
        var tmpUomOb = currentProducts[$('#item_code').val()].uom;
        var tmpUomID = $(".uomLi input[type='radio']:checked").val();
        $('#uomAb').html(tmpUomOb[tmpUomID].uA);
    });

    $(document).on('click', '.dropdown-menu input, .dropdown-menu label', function(e) {
        e.stopPropagation();
    });

    $(document).on('mouseover', '#item_tbl #unit,#item_tbl #quantity,#item_tbl #disc,#item_tbl #name', function(e) {
        $(this).addClass('shadow');
    });

    $(document).on('mouseout', '#item_tbl #unit,#item_tbl #quantity,#item_tbl #disc,#item_tbl #name', function(e) {
        $(this).removeClass('shadow');
    });

    $(document).on('change', '.chkTaxes', function() {
        var trid = $(this).closest('tr').attr('id');
        var itemID = trid.split('td_')[1].trim();
        var checkedTaxes = Array();
        $("." + itemID + " input:checked").each(function() {
            checkedTaxes.push(this.id);
        });
        if (jQuery.isEmptyObject(checkedTaxes)) {
            $("#taxFilled_" + itemID).removeClass('glyphicon glyphicon-check');
            $("#taxFilled_" + itemID).addClass('glyphicon glyphicon-unchecked');
        } else {
            $("#taxFilled_" + itemID).removeClass('glyphicon glyphicon-unchecked');
            $("#taxFilled_" + itemID).addClass('glyphicon glyphicon-check');
        }
        setItemCostForTaxes(itemID, checkedTaxes);
    });

    $(document).on('click', ".delete", function() {
        delete products[this.id];
        $(this).closest("tr").remove();
        setTotalCost();
        //if all products removed enable custom currency field
        var itemCount = $("#form_rwo tr:not(.hide)").length;
        if (itemCount == 0) {
            $('#customCurrencyId').attr('disabled', false);
        }
    });

    $('.addNewItem').on('click', function(e) {
        var currentRow = getAddRow();
        $('#createProduct #itmQty').addClass('hidden');
        $('#createProduct').modal('show');
        var defaultCategory = $('#categoryParentID').attr('data-id');
        $('#categoryParentID').val(defaultCategory);
        $('#categoryParentID').selectpicker('refresh');
        var defaultUomID = $('#defaultUomID').val();
        $("[name='uomID[]']").val(defaultUomID);
        productRow = currentRow;
    });
    
    $('#createProduct').on('shown.bs.modal', function() {
        $(this).find("#productCode").focus();
    });


    $('#add_item').on('click', function(e) {
        e.preventDefault();
        var productID = $("#item_code").val();
        var code = currentProducts[productID].pC;
        var productType = $("#item_code").data('PT');
        var pName = currentProducts[productID].pN;
        var quantity = $("#qty").val();
        var uomqty = $("#qty").siblings('.uomqty').val();
        var uomPrice = $("#price").siblings('.uomPrice').val();
        var uom = $("#qty").siblings('.uom-select').children('button').find('.selected').data('uomID');
        var price = accounting.unformat($("#price").val());
        var dsc_type = "pr";
        var item_discription = $(this).parents('tr').find('.itemDescText').val();

        if (currentProducts[productID] !== undefined && parseFloat(currentProducts[productID].dV) >= 0) {
            dsc_type = "vl";
        }

        if (priceListItems[productID] != undefined && parseFloat(priceListItems[productID].itemPrice) > 0) {
            var itemDiscountType = priceListItems[productID].itemDiscountType;
            if (itemDiscountType == 1) {
                dsc_type = "vl";
            } else {
                dsc_type = "pr";
            }
        }

        var dsc_Pre = accounting.unformat($("#discount").val());
        var tax = new Array();
        $('#addTaxUl input:checked').each(function() {
            if (!$(this).is(':disabled')) {
                tax.push($(this).attr("value"));
            }
        });
        var status = addProduct(code, pName, quantity, price, uom, dsc_type, dsc_Pre, tax, uomqty, productType, productID, uomPrice, item_discription);
        if (status == false) {
            return false;
        } else {
            setTotalCost();
        }
        setItemViceDiscount();

        $('.quotation_table thead').css('display', 'table-header-group');
        $('#customCurrencyId').attr('disabled', true);
        $('#priceListId').attr('disabled', true);
    });

    $('#show_tax').on('click', function() {
        if (this.checked) {
            $('#tax_td').show();
        }
        else {
            $('#tax_td').hide();
        }
    });

    $('#disc_presentage,#disc_value').on('change', function() {
        setItemViceDiscount();
    });

    $(document).on('focusout', '#price, #qty, #discount,.uomqty,.uomPrice', function() {

        if ($('#item_code').val() != '') {
            if ($(this).val().indexOf('.') > 0) {
                decimalPoints = $(this).val().split('.')[1].length;
            }
            if (this.id == 'price' && (!$.isNumeric(accounting.unformat($('#price').val())) || accounting.unformat($('#price').val()) < 0 || decimalPoints > 2)) {
                decimalPoints = 0;
                $('#price').val('');
            }
            if ((this.id == 'qty') && (!$.isNumeric($('#qty').val()) || $('#qty').val() < 0 || decimalPoints > itemDecimalPoints)) {
                decimalPoints = 0;
                $('#qty').val('');
            }
            if ((this.id == 'discount') && (!$.isNumeric(accounting.unformat($('#discount').val())) || accounting.unformat($('#discount').val()) < 0 || (decimalPoints > 2))) {
                decimalPoints = 0;
                $('#discount').val('');
            }
            setItemCost();

        } else {
            $('#price, #qty, #discount').val('');

        }

    });

    function setItemViceDiscount () {
        
        $('#form_rwo tr').each(function() {
            var thisRow = $(this);
            var productID = thisRow.attr('id');
            if (productID != '' && productID != 'preSetSample') {
                var invoiceDiscountValue = $('#total_discount_rate').val();

                var unitArr = thisRow.find('#unit').text().split(" ");
                var unitcost = parseFloat(unitArr[0].replace(/,/g, ''));
                var itemQuantity = parseFloat(thisRow.find('#quantity').text());
                var itemCost = unitcost * itemQuantity;
                var codeArr = productID.split("_");
                var itemId = thisRow.attr('data-itemId');

                var tax = new Array();
                var taxA = 0;
                var disc = 0;
                taxdetails = new Array();
                for (var i in products) {
                    if (i == codeArr[1]) {
                        var tax = products[i]['taxes'];
                        disc = products[i]['discount'];
                    }
                }

                var conversion = currentProducts[itemId].uom[products[i]['uom']].uC;

                itemQuantity = itemQuantity * conversion;
                console.log(itemCost);

                itemCost = itemCost - calculateDiscount(itemId, itemCost, disc, itemQuantity);

                if ($('input[name=discount_type]:checked').val() == 'presentage') {
                    itemCost -= (itemCost * (toFloat(invoiceDiscountValue) / toFloat(100)));
                } 


                taxdetails = calculateItemCustomTax(itemCost, tax);
                var taxTotalAmount = taxdetails === null ? 0 : taxdetails.tTA;
                taxA = taxTotalAmount === undefined ? 0 : parseFloat(taxTotalAmount);


                itemCost = itemCost + taxA;
                itemCost = accounting.formatMoney(itemCost);
                for (var i in products) {
                    if (i == codeArr[1]) {
                        var item_cost = parseFloat(itemCost.replace(/,/g, ''));
                        products[i]['total_cost'] = item_cost;
                    }
                }
                thisRow.find('#ttl').text(itemCost);
            }
        });
        
        setTotalCost();
    }


    function itemChange(itemkey) {
        var data = new Array();
        itemDecimalPoints = 3;
        $('.tempLi').remove();
        $("#discount").attr('disabled', false);
        if (itemkey == '') {
            $('#qty').val('');
            $('#price').val('');
            $('#discount').val('');
            $('#total').html('');
            $('#uom').html('');
            return false;
        }
        eb.ajax({
            type: 'POST',
            url: BASE_URL + '/productAPI/get-location-product-details',
            data: {productID: itemkey, locationID: locationID},
            success: function(respond) {
                currentProducts[respond.data.pID] = respond.data;
                data[0] = respond.data;
                if (data[0] == 'error') {
                    $('#qty').val('');
                    $('#price').val('');
                    $('#discount').val('');
                    $('#total').html('');
                    $('#uom').html('');
                    dbflag = false;
                } else {
                    dbflag = true;

                    if ($("#discount").siblings().hasClass('uomPrice')) {
                        $("#discount").siblings('.uomPrice').remove();
                        $("#discount").siblings('.uom-price-select').remove();
                        $("#discount").show();
                    }
                    var baseUom;
                    for (var j in data[0].uom) {
                        if (data[0].uom[j].pUBase == 1) {
                            baseUom = data[0].uom[j].uomID;
                        }
                    }

                    if (priceListItems[itemkey] == undefined || (priceListItems[itemkey] != undefined && !(parseFloat(priceListItems[itemkey].itemPrice) > 0))) {
                        if (data[0].dEL === "0" || (data[0].dV == null && data[0].dPR == null)) {
                            $('#discount').val('');
                            $("#discount").attr('disabled', true);
                        }
                        else if (data[0].dEL === "1" && data[0].dV != null)
                        {
                            $('#discount').val(parseFloat(data[0].dV).toFixed(2)).addUomPrice(data[0].uom, baseUom);
                            $('#discount').siblings('.uom-price-select').find('.uom_price_drop_down').attr('disabled', true);
                            $('#discType').html(customCurrencySymbol);
                        }
                        else if (data[0].dEL === "1" && data[0].dPR != null) {
                            $('#discount').val(parseFloat(data[0].dPR).toFixed(2));
                            $('#discType').html('(%)');
                        }
                    }


                    $('#item_code').data('PT', data[0].pT);
                    $('#item_code').val(itemkey);
                    $('#item_code').selectpicker('render');
                    if ($("#price").siblings().hasClass('uomPrice')) {
                        $("#price").siblings('.uomPrice').remove();
                        $("#price").siblings('.uom-price-select').remove();
                    }
                    if (priceListItems[itemkey] != undefined && parseFloat(priceListItems[itemkey].itemPrice) > 0) {
                        $('#price').val(parseFloat(priceListItems[itemkey].itemPrice).toFixed(2)).addUomPrice(data[0].uom);
                        var itemDiscountType = priceListItems[itemkey].itemDiscountType;
                        if (itemDiscountType == 1) {
                            $('#discount').val(parseFloat(priceListItems[itemkey].itemDiscount).toFixed(2)).addUomPrice(data[0].uom, baseUom);
                            $('#discount').siblings('.uom-price-select').find('.uom_price_drop_down').attr('disabled', true);
                            $('#discType').html(customCurrencySymbol);
                        } else if (itemDiscountType == 2) {
                            $('#discount').val(parseFloat(priceListItems[itemkey].itemDiscount));
                            $('#discType').html('(%)');
                        }

                    } else {
                        $('#price').val(parseFloat(data[0].dSP).toFixed(2)).addUomPrice(data[0].uom);
                    }
                    $('#uom').html(data[0].abbrevation);
                    if ($('#qty').val() === '') {
                        $('#qty').val('1');
                    }
                    if ($("#qty").siblings().hasClass('uomqty')) {
                        $("#qty").siblings('.uomqty').remove();
                        $("#qty").siblings('.uom-select').remove();
                    }
                    $('#qty').addUom(data[0].uom);
//                    $('#price').addUomPrice(data[0].uom);
                    $('.uomqty').attr("id", "itemQuantity");
                    $('.uomPrice').attr("id", "itemUnitPrice");
                    setTaxListForProduct(itemkey);
                    itemDecimalPoints = data[0].decimalPlace;

                    setItemCost();
                }
            }
        });
    }

    $('form#quatation, #q_view').on('submit', function(e) {
        e.preventDefault();
        var selectedid = "#" + document.activeElement.id;
        var tableId = $(selectedid).closest('table').attr("id");

        if ($('#qot_no').val() == '') {
            p_notification(false, eb.getMessage('ERR_QUOT_VALID_QUOTA'));
        } else if ($('#cust_name').val() == '' || $('#cust_name').val() == '0') {
            p_notification(false, eb.getMessage('ERR_QUOT_VALID_CUST'));
        } else if (Object.keys(products).length == 0) {
            if (tableId != "item_tbl") {
                p_notification(false, eb.getMessage('ERR_QUOT_ADD_NOITEM'));
            }
        } else {
            var items_ar = Array();
            totaldiscount = 0;
            var taxOfItems = new Array();
            for (var i in products) {
                var temptaxes = {};
                for (var tx in products[i].taxes) {
                    if (products[i].taxes[tx] != false) {
                        temptaxes[tx] = products[i].taxes[tx];
                    }
                }
                taxOfItems.push(JSON.stringify(temptaxes));
            }
            for (var i in products) {
                items_ar.push(products[i]);
                var qty = products[i].quantity;
                if (products[i].productType == 2 && qty == 0) {
                    qty = 1;
                }
                if (products[i].discountType == 'pr') {
                    tmpdisc = (products[i].discount * qty * products[i].unit_price / 100);
                } else {
                    tmpdisc = (products[i].discount * qty);
                }
                if (tmpdisc != 0) {
                    totaldiscount += toFloat(tmpdisc);
                }
            }
            var taxobj = {};
            for (var t in all_tx_val) {
                taxobj[t] = (all_tx_val[t]).toFixed(2);
            }
            var jsontax = JSON.stringify(taxobj);
            var custanamefull = $('#cust_name option:selected').text();
            submit_data = {
                'cust_name': custanamefull,
                'cust_id': customerID,
                'cust_pro_id': cusProfID,
                'payment_term': $('#payment_term').val(),
                'invoicecurrentBalance': accounting.unformat($('#invoicecurrentBalance').val()),
                'issue_date': $('#issue_date').val(),
                'expire_date': $('#expire_date').val(),
                'qot_no': $('#qot_no').val(),
                'salesPersonID': $('#salesPersonID').val(),
                'deli_amount': accounting.unformat($('#deli_amount').val()),
                'deli_charg': (parseFloat(accounting.unformat($('#deli_charg').val()))).toFixed(2),
                'deli_address': $('#deli_address').val(),
                'cmnt': $('#cmnt').val(),
                'taxAmount': jsontax,
                'sub_total': accounting.unformat($("#subtotal").html()),
                'total_amount': accounting.unformat($('#finaltotal').html()),
                'total_discount': totaldiscount,
                'items': items_ar,
                'itemTaxes': JSON.stringify(taxOfItems),
                'show_tax': ($('#show_tax').is(':checked')) ? 1 : 0,
                'jobRefType': $('#jobCardType').val(),
                'jobRefTypeID': jobRefTypeID,
                'customCurrencyId': $('#customCurrencyId').val(),
                'additionalDetail1': $('#additionalDetail1').val(),
                'additionalDetail2': $('#additionalDetail2').val(),
                'priceListId': priceListId,
                'quotationTotalDiscountType': $('input[name=discount_type]:checked').val(),
                'quotationDiscountRate': $('#total_discount_rate').val(),
            };

            if (this.id == "quatation") {
                if (state == "new") {
                    var url = "/quotation-api/createQuotation";
                    var request = eb.post(url, submit_data);
                    request.done(function(res) {
                        p_notification(res.status, res.msg);
                        if (res.status == true) {
                            $('#qot_no').val(res.data);
                            var fileInput = document.getElementById('documentFiles');
                            if(fileInput.files.length > 0) {
                                var form_data = false;
                                if (window.FormData) {
                                    form_data = new FormData();
                                }
                                form_data.append("documentID", res.data);
                                form_data.append("documentTypeID", 2);
                                
                                for (var i = 0; i < fileInput.files.length; i++) {
                                    form_data.append("files[]", fileInput.files[i]);
                                }

                                eb.ajax({
                                    url: BASE_URL + '/store-files',
                                    type: 'POST',
                                    processData: false,
                                    contentType: false,
                                    data: form_data,
                                    success: function(res) {
                                    }
                                });
                            }

                            if ($('#lightbox_overlay').length < 1) {
                                documentPreview(BASE_URL + '/quotation/quotationView/' + res.data, 'documentpreview', "/quotation-api/send-email", function($preview) {
                                    $preview.on('hidden.bs.modal', function() {
                                        if (!$("#preview:visible").length) {
                                            window.location.reload();
                                        }
                                    });
                                });
                            }
                            state = "saved";
                        }
                    });
                }
            }
            else if (this.id == "q_view") {
                var li_box = document.createElement("div");
                var li_box_cont = document.createElement("div");
                var print = document.createElement("div");
                li_box.id = "lightbox_overlay";
                li_box.class = "disabl_when_print";
                li_box_cont.id = "lightbox_container";
                li_box_cont.class = "disabl_when_print";
                print.id = "print_overlay";
                print.class = "disabl_when_print";
                var view_req = eb.post('/quotation/viewQuotation?' + $.param(submit_data), '', {});
                view_req.done(function() {
                    $(".main_div").addClass("disabl_when_print");
                    $(li_box_cont).append(view_req.responseText);
                    $(li_box).append(li_box_cont);
                    $("body").append(li_box);
                    $("body").append(print);
                    if (state == "saved") {
                        $("#prv_edit").html(edit_btn);
                    } else {
                        $("#prv_edit").html(save);
                    }

                    $("#prv_edit").on("click", function() {
                        if (state == "new") {
                            $("#placequotation").trigger(jQuery.Event("click"));
                            $("#prv_edit").html(edit_btn);
                        } else {
                            window.location.assign(BASE_URL + "/quotation/Edit/" + $('#qot_no').val());
                        }
                    });
                    $("#q_print").on("click", function() {
                        window.print();
                    });
                    $("#q_email").on("click", function() {
                        if (state == "new") {
                            p_notification(false, eb.getMessage('ERR_QUOT_SAVE_SEND'));
                        } else {
                            $("#email-modal").modal("show");
                            $("#send").on("click", function() {
                                var state = validateEmail($("#email_to").val(), $("#email-body").html());
                                if (state) {
                                    var param = {qout_id: $(this).attr("data-qot_id"),
                                        to_email: $("#email_to").val(),
                                        subject: $("#email_sub").val(),
                                        body: $("#email-body").html()
                                    };
                                    var mail = eb.post("/quotationAPI/sendQuotation", param);
                                    mail.done(function(rep) {
                                        if (rep.error) {
                                            p_notification(false, eb.getMessage('ERR_PAY_EMAIL_SENT'));
                                        } else {
                                            p_notification(true, eb.getMessage('SUCC_PAY_EMAIL_SENT'));
                                        }
                                    });
                                    $("#email-modal").modal("hide");
                                }
                            });
                        }
                    });
                    $("#cancel").on("click", function() {
                        if (state == "new") {
                            $(li_box_cont).remove();
                            $(li_box).remove();
                            $(print).remove();
                        } else {
                            $("#new_qtn").trigger(jQuery.Event("click"));
                        }
                    });
                    $("#new_qtn").on("click", function() {
                        window.location.reload();
                    });
                    $("#to_so").on("click", function() {
                        if (state == "saved") {
                            window.location.assign(BASE_URL + "/salesOrders/index/" + $('#qot_no').val());
                        } else {
                            p_notification(false, eb.getMessage('ERR_QUOT_SAVE_CONVERT'));
                        }

                    });
                    $("#to_inv").on("click", function() {
                        if (state == "saved") {
                            window.location.assign(BASE_URL + "/invoice/index/qo/" + $('#qot_no').val());
                        } else {
                            p_notification(false, eb.getMessage('ERR_QUOT_SAVE_CONVERT'));
                        }
                    });
                });
            }


        }

    });

    $('table#item_tbl>tbody').on('keypress', 'input#qty, input#price, input#itemUnitPrice, input#discount,.uomqty', function(e) {

        if (e.which == 13) {
            $(this).blur();
            $('#add_item', $(this).parents('tr')).trigger('click');
            e.preventDefault();
        }

    });

    $('#payment_term').on('change', function(e) {
        e.preventDefault();
        var days = 0;
        var i = parseInt($(this).val());
        switch (i) {
            case 1:
                days = 0;
                break;
            case 2:
                days = 7;
                break;
            case 3:
                days = 14;
                break;
            case 4:
                days = 30;
                break;
            case 5:
                days = 60;
                break;
            case 6:
                days = 90;
                break;
            case 7:
                days = 21;
                break;
            case 8:
                days = 28;
                break;
            case 9:
                days = 45;
                break;
            case 10:
                days = 35;
                break;
            default:
                days = 0;
                break;
        }
        setDueDate(days);
    });
    function setDueDate(days) {
        var day = parseInt(days);
        var joindate = eb.convertDateFormat('#issue_date');
        joindate.setDate(joindate.getDate() + day);

        var dd = joindate.getDate() < 10 ? '0' + joindate.getDate() : joindate.getDate();
        var mm = (joindate.getMonth() + 1) < 10 ? '0' + (joindate.getMonth() + 1) : (joindate.getMonth() + 1);
        var y = joindate.getFullYear();
        var joinFormattedDate = eb.getDateForDocumentEdit('#expire_date', dd, mm, y);

        $("#expire_date").val(joinFormattedDate);
    }

    $("#reset").on('click', function() {
        window.location.reload()
    });

    $(document).on('click', '#moredetails', function() {
        $("#customer_more").fadeIn();
        $(this).hide();
    });
    ///////DatePicker\\\\\\\
    var nowTemp = new Date();
    var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);
    var checkin = $('#issue_date').datepicker({onRender: function(date) {
            return date.valueOf() < now.valueOf() ? ((U_DATE_R_OVERRIDE)?'':'disabled') : '';
        }
    }).on('changeDate', function(ev) {
        if (ev.date.valueOf() > checkout.date.valueOf()) {
            var newDate = new Date(ev.date);
            newDate.setDate(newDate.getDate() + 1);
            checkout.setValue(newDate);
        }
        checkin.hide();
        $('#expire_date')[0].focus();
    }).data('datepicker');
    checkin.setValue(now);
    var checkout = $('#expire_date').datepicker({
        onRender: function(date) {
            return date.valueOf() < checkin.date.valueOf() ? ((U_DATE_R_OVERRIDE)?'':'disabled') : '';
        }}).on('changeDate', function(ev) {
        checkout.hide();
    }).data('datepicker');
    checkout.setValue(now);

    /////EndOFDatePicker\\\\\\\\\

    $("#item_tbl").on('change', '.taxChecks', function() {
        setItemCost();
    });

    $("#item_tbl").on('click', '#selectAll', function() {
        $(this).parents('#addTaxUl').find('input:checkbox.addNewTaxCheck').each(function() {
            if (!this.checked) {
                this.checked = true;
            }
        });
        $(this).parents('tr').find("#qty").trigger(jQuery.Event("focusout"));
    });

    $("#item_tbl").on('click', '#deselectAll', function() {
        $(this).parents('#addTaxUl').find('input:checkbox.addNewTaxCheck').each(function() {
            if (this.checked) {
                this.checked = false;
            }
        });
        $(this).parents('tr').find("#qty").trigger(jQuery.Event("focusout"));
    });

    $("#item_tbl").on('click', 'ul.dropdown-menu', function(e) {
        e.stopPropagation();
    });

    //responsive layout issue fix
    $(window).bind('resize ready', function() {
        if ($(window).width() <= 480) {
            $('.quotation_button_set').removeClass('btn-group');
            $('#placequotation, #q_view, #reset').addClass('col-xs-12').css('margin-bottom', '8px');
        } else {
            $('.quotation_button_set').removeClass('col-xs-12');
            $('#placequotation, #q_view, #reset').removeClass('col-xs-12').css('margin-bottom', '0px');
        }
    });

    $('#customCurrencyId').on('change', function() {
        eb.ajax({
            type: 'POST',
            url: BASE_URL + '/currencySymbol/getCustomCurrencyByCustomCurrencyId',
            data: {customCurrencyId: $(this).val()},
            success: function(respond) {
                if (respond.status == true) {
                    $('.cCurrency').text(respond.data.currencySymbol);
                    customCurrencySymbol = respond.data.currencySymbol;
                }
            }
        });
    });

    $('#priceListId').on('change', function() {
        priceListId = $(this).val();
        eb.ajax({
            type: 'POST',
            url: BASE_URL + '/currencySymbol/getPriceListWithDiscount',
            data: {priceListId: priceListId},
            success: function(respond) {
                if (respond.status == true) {
                    priceListItems = respond.data;
                } else {
                    priceListItems = [];
                }
            }
        });
    });

    //Add item description
    $('#item_tbl').on('click', '.addItemDescription', function() {
        if ($('i', $(this)).hasClass('fa-comment-o')) {
            $('i', $(this)).removeClass('fa-comment-o').addClass('fa-comment');
            $(this).parents('tr').addClass('comment');
            $(this).parent().parent().siblings('.itemDescText').removeClass('hidden').focus();
        } else {
            $('i', $(this)).removeClass('fa-comment').addClass('fa-comment-o');
            $(this).parents('tr').removeClass('comment');
            $(this).parent().parent().siblings('.itemDescText').addClass('hidden');
        }
    });


	//show item description
    $('#item_tbl').on('click', '.showItemDescription', function() {
        if ($('i', $(this)).hasClass('fa-comment-o')) {
            $('i', $(this)).removeClass('fa-comment-o').addClass('fa-comment');
            $(this).parents('tr').addClass('comment');
            $(this).parent().parent().siblings('.showItemDescText').removeClass('hidden').focus();
        } else {
            $('i', $(this)).removeClass('fa-comment').addClass('fa-comment-o');
            $(this).parents('tr').removeClass('comment');
            $(this).parent().parent().siblings('.showItemDescText').addClass('hidden');
        }
    });


    $('#item_tbl').on('blur', '.itemDescText', function(e) {
        e.preventDefault();
        $(this).addClass('hidden');
        $(this).parent().find('.addItemDescription').find('i').removeClass('fa-comment').addClass('fa-comment-o');
    });

    $('#item_tbl').on('blur', '.showItemDescText', function(e) {
        e.preventDefault();
        $(this).addClass('hidden');
        $(this).parent().find('.showItemDescription').find('i').removeClass('fa-comment').addClass('fa-comment-o');
    });

});

/**
 * Prepare tax list (Drop down)
 * @param {type} productCode
 */
function setTaxListForProduct(productID) {
    if ((!jQuery.isEmptyObject(currentProducts[productID].tax))) {
        productTax = currentProducts[productID].tax;
        $('#addNewTax').attr('disabled', false);
        $('#taxApplied').removeClass('glyphicon glyphicon-unchecked');
        $('#taxApplied').addClass('glyphicon glyphicon-check');
        $('.tempLi').remove();
        for (var i in productTax) {
            var clonedLi = $($('#sampleLi').clone()).removeClass('hidden').attr('id', 'li_' + i).addClass("tempLi");
            clonedLi.children(".taxChecks").attr('value', i).prop('checked', true).addClass('addNewTaxCheck');
            if (productTax[i].tS == 0) {
                clonedLi.children(".taxChecks").attr('disabled', 'disabled');
                clonedLi.children(".taxName").addClass('crossText');
            }
            clonedLi.children(".taxName").html('&nbsp&nbsp' + productTax[i].tN + '&nbsp&nbsp' + productTax[i].tP + '%').attr('for', productID + '_' + i);
            clonedLi.insertBefore('#sampleLi');
        }
    } else {
        $('#taxApplied').addClass('glyphicon glyphicon-unchecked');
        $('#addNewTax').attr('disabled', 'disabled');
    }

}

/**
 *
 * @param {type} productCode
 * @param {type} price
 * @param {type} disc
 * @returns {dis_amount}
 */
function calculateDiscount(productID, price, disc, qty) {
    var dis_amount = 0;
    if (currentProducts[productID] !== undefined && currentProducts[productID].dEL === "1") {

        if (priceListItems[productID] == undefined) {
            var pdp = currentProducts[productID].dPR;
            var pdv = parseFloat(currentProducts[productID].dV);

            if (pdv >= 0)
            {
                //Check Customer discount against product discount
                if (disc > pdv && pdv != 0) {
                    p_notification(false, eb.getMessage('ERR_QUOT_DISC_VALUE'));
                    dis_amount = (qty * pdv);
                    $('#discount').val(accounting.formatMoney(pdv));
                } else if (pdv == 0){
                    if (disc > $('#itemUnitPrice').val()) {
                        p_notification(false, eb.getMessage('ERR_INVO_DISCOUNT_VALUE_UNITE_P'));
                        dis_amount = (qty * $('#itemUnitPrice').val());
                        $('#discount').val(accounting.formatMoney($('#itemUnitPrice').val()));
                    } else {
                        dis_amount = (qty * disc);
                        $('#discount').val(accounting.formatMoney(disc));
                    }
                } else {
                    if (price > 0 && qty > 0 && disc > 0)
                        dis_amount = (qty * disc);
                }
            } else {             //Check Customer discount against product discount
                if (pdp != 0) {
                    if (disc > pdp) {
                        p_notification(false, eb.getMessage('ERR_QUOT_DISC_VALUE'));
                        dis_amount = (price * pdp / 100);
                        $('#discount').val(accounting.formatMoney(pdp));
                    } else {
                        if (price > 0 && disc > 0)
                            dis_amount = (price * disc / 100);
                    }
                } else {
                    if (price > 0 && disc > 0)
                        dis_amount = (price * disc / 100);
                }
            }
        } else {
            var itemDiscountType = priceListItems[productID].itemDiscountType;
            if (itemDiscountType == 1) {
                dis_amount = (qty * disc);
            } else {
                dis_amount = (price * disc / 100);
            }
        }
    } else if (priceListItems[productID] != undefined && parseFloat(priceListItems[productID].itemPrice) > 0) {
        var itemDiscountType = priceListItems[productID].itemDiscountType;
        if (itemDiscountType == 1) {
            dis_amount = (qty * disc);
        } else {
            dis_amount = (price * disc / 100);
        }
    }
    return dis_amount;
}

function clearTable() {
    $('#item_code').val(0).trigger('change').empty().selectpicker('refresh');
    selectedProductID = '';
    $("#qty").val("");
    $("#qty").siblings('.uomqty').remove();
    $("#qty").siblings('.uom-select').remove();
    $('#qty').parent().removeClass('input-group');
    $("#qty").show();
    $("#price").val("");
    $("#price").siblings('.uomPrice').remove();
    $("#price").siblings('.uom-price-select').remove();
    $("#price").show();
    $("#total").html("0.00");
    $("#discount").attr('disabled', false);
    $("#discount").val("").siblings('.uomPrice').remove();;
    $("#discount").val("").siblings('.uom-price-select').remove();
    $("#discount").show();
    $("#dic_lbl").html("Dis.nt");
    $('.uomLi').remove();
    $("#uomAb").html("");
    $('#taxApplied').removeClass('glyphicon glyphicon-checked');
    $('#taxApplied').addClass('glyphicon glyphicon-unchecked');
    $('#addNewTax').attr('disabled', false);
    $('#discType').html('');
    $('.tempLi').remove();
    $('.itemDescText').val('');
}

function addProduct(code, pName, quantity, price, uom, dscType, dscAmount, tax, uomqty, productType, productID, uomPrice, item_discription) {
    if (code === "" || code == undefined) {
        p_notification(false, eb.getMessage('ERR_QUOT_PRCODE_EMPTY'));
        return false;
    } else if (products[code] !== undefined) {
        p_notification(false, eb.getMessage('ERR_QUOT_PR_ALREADY_ADD'));
        return false;
    } else if (pName === "") {
        p_notification(false, eb.getMessage('ERR_QUOT_PRNAME_EMPTY'));
        return false;
    } else if (productType == 1 && (uomqty === "" || uomqty <= 0)) {
        p_notification(false, eb.getMessage('ERR_QUOT_QUAN_VALUE'));
        return false;
    } else if (uom === undefined) {
        p_notification(false, eb.getMessage('ERR_QUOT_SELECT_UOM'));
        return false;
    } else if (price <= 0) {
        p_notification(false, eb.getMessage('ERR_QUOT_ENTER_PRICE'));
        return false;
    } else if (dscType === 'vl' && parseFloat(dscAmount) > price) {
        p_notification(false, eb.getMessage('ERR_QUOT_DISCVAL_PRODPRICE'));
        return false;
    } else {
    	var showDiscHtml = '<div class="pull-right input-group">'+
    	'<span class="input-group-btn">'+
    	'<button  type="button" class="btn btn-default showItemDescription" title="Show comment">'+
    	'<i class="fa fa-comment-o"></i></button></span></div>'+
    	'<textarea class="showItemDescText form-control hidden" readonly="true" placeholder="Enter item description">'+
    	'</textarea>';

        var dummy = $($("#preSetSample").clone()).attr("id", "tr_" + code);
        dummy.attr("data-itemId", productID);
        dummy.children("#code").html(code + ' - ' + pName + " "+ showDiscHtml);
        dummy.children("#quantity").html(uomqty);
        dummy.children("#unit").html(accounting.formatMoney(price * currentProducts[productID].uom[uom].uC) + " (" + currentProducts[productID].uom[uom].uA + ")");
        dummy.children().children('#uomName').html(currentProducts[productID].uom[uom].uA);
        if (dscType === 'vl' && dscAmount > 0) {
            dummy.children("#disc").html(accounting.formatMoney(dscAmount * currentProducts[productID].uom[uom].uC) + '&nbsp;(' + currentProducts[productID].uom[uom].uA + ')' + '&nbsp;(' + customCurrencySymbol + ')');
        } else if (dscType === 'pr' && dscAmount > 0) {
            dummy.children("#disc").html(accounting.formatMoney(dscAmount) + '&nbsp;(%)');
        } else {
            dscType = 'pr';
        }

        dummy.children().children(".delete").attr("id", code);
        dummy.children("#code").find('.showItemDescText').val(item_discription);
        var clonedTax = $($('#addNewTaxDiv').clone()).attr('id', '');
        clonedTax.find('.tempLi').each(function() {
            if ($(this).children(".taxChecks").prop('checked') != true) {
                $(this).children(".taxName").addClass('crossText');
            }
        });
        clonedTax.children('#addNewTax').children('#taxApplied').attr('id', '');
        clonedTax.children('#addNewTax').attr('id', '');
        clonedTax.children('#addTaxUl').children().removeClass('tempLi');
        clonedTax.children('#addTaxUl').children().children().removeClass('addNewTaxCheck');
        clonedTax.children('#addTaxUl').children().children(".taxChecks").remove();
        clonedTax.children('#addTaxUl').children('#sampleLi').remove();
        clonedTax.children('#addTaxUl').attr('id', '');
        clonedTax.find('#toggleSelection').addClass('hidden');
        dummy.children("#tax").html(clonedTax);
        $("#qty").siblings('.uomqty').change();
        dummy.children("#ttl").html(accounting.formatMoney(accounting.unformat($("#total").html())));
        dummy.removeClass("hide").insertBefore("#preSetSample");
        var qty = quantity;
        products[code] = new product(code, pName, qty, uom, price, dscAmount, dscType, tax, accounting.unformat($("#total").html()), taxdetails, productType, item_discription);
        clearTable();
    }
}
function getCustomerDetails(customerID) {
    eb.ajax({
        type: 'POST',
        url: BASE_URL + '/customerAPI/getcustomerByID',
        data: {customerID: customerID}, success: function(respond) {
            if (respond.status == true) {
                $('#invoicecurrentBalance').val(accounting.formatMoney(respond.data['customerData'].customerCurrentBalance));
                $('#quotationCurrentCredit').val(accounting.formatMoney(respond.data['customerData'].customerCurrentCredit));
                $('#cust_name').empty();
                $('#cust_name').append($("<option></option>")
                        .attr("value", customerID)
                        .text(respond.data['customerData'].customerName + '-' + respond.data['customerData'].customerCode));
                $('#cust_name').selectpicker('refresh');
                if (respond.data['customerData'].customerPaymentTerm == 'null') {
                    $('#payment_term').val(1);
                } else {
                    $('#payment_term').val(respond.data['customerData'].customerPaymentTerm);
                }
                $('#payment_term').trigger('change');
            } else {
                $('#customerCurrentBalance').val('');
                $('#customerCurrentCredit').val('');
                $('#paymentTerm').val('');
                p_notification(false, respond.msg);
            }
        }
    });
}

function getCustomerProfilesDetails(customerID) {
    eb.ajax({
        type: 'POST',
        url: BASE_URL + '/customerAPI/getCustomerProfilesByCustomerID',
        data: {customerID: customerID},
        success: function(respond) {
            if (respond.status == true) {
                $('.cus_prof_div').removeClass('hidden');
                setCustomerProfilePicker(respond.data['customerProfileData']);
            } else {
                $('.cus_prof_div').addClass('hidden');

            }
        }
    });
}

function setCustomerProfilePicker(data) {
    $.each(data, function(index, value) {
        $('#cusProfileQuot').append("<option value='" + index + "'>" + value['profName'] + "</option>");
        if (value['isPrimary'] == 1) {
            $('#cusProfileQuot').html("<option value='" + index + "'>" + value['profName'] + "</option>");
            cusProfID = index;
        }
    });
    $('#cusProfileQuot').selectpicker('refresh');

    $('#cusProfileQuot').on('change', function(e) {
        e.preventDefault();
        if ($(this).val() > 0 && ($(this).val() != cusProfID)) {
            cusProfID = $(this).val();
        } else {
            cusProfID = '';
        }
    });
}

