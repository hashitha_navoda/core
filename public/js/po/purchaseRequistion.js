/**
 * @author Damith Thamara <damith@thinkcube.com>
 * This javascript contains PO related functions
 */
var currentProduct;
var locationProducts;
var locProductCodes;
var locProductNames;
var selectedProductID;
var selectedSupplier;
var selectedProductQuantity;
var selectedProductUom;
var batchCount = 0;
var currentlySelectedProductID;
var locationProductData;
var uploadedAttachments = {};
var deletedAttachmentIds = [];
var deletedAttachmentIdArray = [];
$(document).ready(function() {
    var selectedLocation;
    var decimalPoints = 0;
    var currentItemTaxResults;
    var currentTaxAmount;
    var products = {};
    var totalTaxList = {};
    var proValue;
    var mRPLocationFlag = false;
    var selectedLocationFromMRP;
    var mRPProductFlag = false;
    var proValueFromMRP;
    var fromMRPFlag = false;
    var locationproductsByPoDraft;
    var poDrfatFlag = false;
    var poDraftUnitPrice;
    var poDraftQty;
    var poDraftTotal;
    var currentProductByDraft;
    var allDraftPo;
    var totalTaxObject = {};
    var sameItemFlag = false;
    var sameItemErrorFlag = false;
    var editedFlag = false;
    var rowincrementID = 1;
    var purchaseReqType;
    // $('#retrieveLocation').val

    function product(lPID, pID, pCode, pN, pQ, pD, pUP, pUom, pTotal, pTax, bProducts, sProducts, rowincrementID) {
        this.locationPID = lPID;
        this.pID = pID;
        this.pCode = pCode;
        this.pName = pN;
        this.pQuantity = pQ;
        this.pDiscount = pD;
        this.pUnitPrice = pUP;
        this.pUom = pUom;
        this.pTotal = pTotal;
        this.pTax = pTax;
        this.rowincrementID = rowincrementID;
    }
    
//  Load supplier from database
    loadDropDownFromDatabase('/supplierAPI/search-suppliers-for-dropdown', "", 0, '#supplier');

    $('#supplier').on('change', function() {
        selectedSupplier = $(this).val();
    });

    $.each(LOCATION, function(index, value) {
        $('#retrieveLocation').append($("<option value='" + index + "'>" + value + "</option>"));
    });

    $('#retrieveLocation').selectpicker('refresh');

    $('#retrieveLocation').on('change', function() {
        clearProductScreen();
        selectedLocation = $(this).val();
        if(mRPLocationFlag){
            selectedLocation = selectedLocationFromMRP;
        }
        if (selectedLocation > 0) {
            var documentType = 'purchaseOrder';
            loadDropDownFromDatabase('/productAPI/search-location-products-for-dropdown', selectedLocation, 0, '#itemCode', '', '', documentType);
        }
        getProductsFromLocation(selectedLocation);
        if (TAXSTATUS == false) {
            p_notification('info', eb.getMessage('ERR_GRN_ENABLE_TAX'));
        }
        $('#productAddOverlay').removeClass('overlay');
        mRPLocationFlag = false;
        $('#comment').removeClass('frommrp')
    });
    
    var currentLocation = $('#navBarSelectedLocation').attr('data-locationid');
    $('#retrieveLocation').val(currentLocation).change();

    $('#retrieveLocation').on('focusout', function() {
        if ($(this).val() == '') {
            selectedLocation = ''
            clearProductScreen();
            $('#productAddOverlay').addClass('overlay');
            $('#poNo').val('');
            $('#locRefID').val('');
        }
    });

    $('#itemCode').on('change', function() {
        proValue = $(this).val();
        if(mRPProductFlag){
            proValue = proValueFromMRP;
        }

        // if ($(this).val() > 0 && selectedLocation > 0 && $(this).val() != currentlySelectedProductID) {
           if (proValue > 0 && selectedLocation > 0 && proValue != currentlySelectedProductID) {
            $parentRow = $(this).parents('tr');
            clearAddNewRow($parentRow);
            currentlySelectedProductID = proValue;
            selectedProductID = proValue;
            $('#addNewTax').attr('disabled', false);
            eb.ajax({
                type: 'POST',
                url: BASE_URL + '/productAPI/get-location-product-details',
                data: {productID: selectedProductID, locationID: selectedLocation},
                success: function(respond) {
                    currentProduct = respond.data;
                    var currentElem = new Array();
                    currentElem[selectedProductID] = respond.data;
                    locationProductData = $.extend(locationProductData, currentElem);
                    setTaxListForProduct(respond.data);
                    $('#unitPrice').val(parseFloat(respond.data.dPP).toFixed(2)).addUomPrice(respond.data.uom);
                    $('.uomPrice').attr("id", "itemUnitPrice");
                    $('#qty').parent().addClass('input-group');
                    $('#qty').addUom(respond.data.uom);
                    $('.uomqty').attr("id", "itemQuantity");
                    $("#itemCode").data('PT', respond.data.pT);
                    $("#itemCode").data('PC', respond.data.pC);
                    $("#itemCode").data('PN', respond.data.pN);
                    $("#itemCode", $parentRow).data('pID', respond.data.pID);


                    var baseUom;
                    for (var j in respond.data.uom) {
                        if (respond.data.uom[j].pUBase == 1) {
                            baseUom = respond.data.uom[j].uomID;
                        }
                    }

                    if (locationProductData[selectedProductID].pPDV) {
                        $('#poDiscount', $parentRow).val(locationProductData[selectedProductID].pPDV).addUomPrice(respond.data.uom, baseUom);
                        $('#poDiscount', $parentRow).siblings('.uom-price-select').find('.uom_price_drop_down').attr('disabled', true);
                        $(".sign").text('Rs');
                        $('#poDiscount', $parentRow).attr('readonly', false);
                    }
                    if (locationProductData[selectedProductID].pPDP) {
                        $('#poDiscount', $parentRow).val(locationProductData[selectedProductID].pPDP);
                        $(".sign", $parentRow).text('%');
                        $('#poDiscount', $parentRow).attr('readonly', false);
                    }

                    if (locationProductData[selectedProductID].pPDP == null && locationProductData[selectedProductID].pPDV == null || locationProductData[selectedProductID].dPEL == 0) {
                        $('#poDiscount', $parentRow).attr('readonly', true);
                    }
                }
            });
            mRPProductFlag =false;
        }

    });

    $('#viewUploadedFiles').on('click', function(e) {
        $('#doc-attach-table tbody tr').remove();
        $('#doc-attach-table tfoot div').remove();
        $('#doc-attach-table tbody div').remove();

        if (!$.isEmptyObject(uploadedAttachments)) {
            $('#doc-attach-table thead tr').removeClass('hidden');
            $.each(uploadedAttachments, function(index, value) {
                if (!deletedAttachmentIds.includes(value['documentAttachemntMapID'])) {
                    tableBody = "<tr id="+value['documentAttachemntMapID']+" data-attachId=" + value['documentAttachemntMapID'] + "><td><a href="+value.docLink+">"+value.documentRealName+"</a></td><td class='text-center'><span class='glyphicon glyphicon-trash delete_attachment' style='cursor:pointer'></span></td></tr>";
                    $('#doc-attach-table tbody').append(tableBody);
                }
            });
        } else {
            $('#doc-attach-table thead tr').addClass('hidden');
            var noDataFooter = "<div class= ' col-lg-6 text-right'><label><b>No Data</b></label>";
            $('#doc-attach-table tbody').append(noDataFooter);
        }
        $('#attach_view_footer').removeClass('hidden');
        $('#viewAttachmentModal').modal('show');
    });

    $('#doc-attach-table').on('click', '.delete_attachment', function() {
        var attachementID = $(this).parents('tr').attr('data-attachid');
        bootbox.confirm('Are you sure you want to remove this attachemnet?', function(result) {
            if (result == true) {
                $('#' + attachementID).remove();
                deletedAttachmentIdArray.push(attachementID);
            }
        });
    });

    $('#viewAttachmentModal').on('click', '#saveAttachmentEdit', function(event) {
        event.preventDefault();

        $.each(deletedAttachmentIdArray, function(index, val) {
            deletedAttachmentIds.push(val);
        });

        $('#viewAttachmentModal').modal('hide');
    });

    var pathName = window.location.pathname.split('/');
    if (pathName.length == 4 && pathName[2] != "edit") {
        $('#comment').addClass('frommrp');
    }

    if($('#comment').hasClass('frommrp')){
        mRPLocationFlag = true;
        mRPProductFlag = true;
        fromMRPFlag = true;
        tmpCloneRow = $('#form_row tr#preSetSample').clone();

        selectedLocationFromMRP = $('#comment').attr('data-locationid');
        $('#retrieveLocation').trigger('change');
        $('#retrieveLocation').val(selectedLocationFromMRP);
        $('#retrieveLocation').prop('disabled', true);
        $('#retrieveLocation').selectpicker('render');


        proValueFromMRP = $('#comment').attr('data-productid');
        $('#itemCode').trigger('change');
        $("#itemCode").empty().
                    append($("<option></option>").
                            attr("value", proValueFromMRP).
                            text($('#comment').attr('data-proname')));
        $('#itemCode').val(proValueFromMRP);

        $('#itemCode').selectpicker('render');
        $('#itemCode').selectpicker('render');
        $('#qty').val($('#comment').attr('data-proqty'));

    }

    if($('#poDraft').hasClass('frommrp')){

        allDraftPo = true;
        eb.ajax({
            type: 'POST',
            url: BASE_URL + '/api/purchase-requistion/get-preq-details',
            data: {poID: $('#poDraft').val()},
            success: function(respond) {
                if(respond.status){
                    $('#retrieveLocation').val(respond.data.poData.poRLID);
                    $('#retrieveLocation').trigger('change');
                    $('#retrieveLocation').prop('disabled', true);
                    $('#retrieveLocation').selectpicker('render');
                    selectedLocation = respond.data.poData.poRLID;
                    if (respond.data.poData.purchaseRequisitionType != null) {
                        $('#purchaseReqType').selectpicker('val', respond.data.poData.purchaseRequisitionType);
                    }
                    locationproductsByPoDraft = respond.data.locationProducts;
                    locationProductData = locationproductsByPoDraft;
                    var incrementID = 1;
                    editedFlag = true;
                    if ($('#poDraft').hasClass('fromedit')) {
                        $('.createPoDiv').addClass('hidden');
                        $('.updatePoDiv').removeClass('hidden');
                        $('.updatePoDiv_view').removeClass('hidden');
                        if (!$.isEmptyObject(respond.data.uploadedAttachments)) {
                            uploadedAttachments = respond.data.uploadedAttachments;
                        }

                        locationProductData = respond.data.locationProducts;
                        // set supplier id
                        selectedSupplier = respond.data.poData.poSID;
                        // add delivery charge
                        if (respond.data.poData.poDC != 0) {
                            $('#deliveryChargeEnable').attr('checked', true);
                             $('.deliCharges').removeClass('hidden');
                             $('#deliveryCharge').val(respond.data.poData.poDC);
                        } else {
                            $('#deliveryChargeEnable').attr('checked', false);
                             $('.deliCharges').addClass('hidden');
                        }
                        // add comment
                        if (respond.data.poData.poDes != null) {
                            $('#comment').val(respond.data.poData.poDes);
                        }
                        // add reference number
                        $('#poNo').val(respond.data.poData.poC);
                        $('#expectedDeliveryDate').val(respond.data.poData.poD);
                        $('#purchaseOrderDate').val(respond.data.poData.purchaseOrderDate);
                        $("#supplier").empty().
                        append($("<option></option>").
                                attr("value", respond.data.poData.poSID).
                                text(respond.data.poData.poSN));
                        $('#supplier').val(respond.data.poData.poSID);
                        $('#supplier').prop('disabled', true);
                        $('#supplier').selectpicker('render');
                        // hidden supplier add button
                        $('#addSupplierBtn').addClass('hidden');
                        $('.supplier-select').removeClass('form-control');
                        $.each(respond.data.poData.poProducts,function(key, value){
                            addNewProductRowByEdit(value, incrementID);
                            incrementID++;
                        });

                        $.each(respond.data.docRefData,function(key, value){
                            addDocRef(key, value);
                            updateDocTypeDropDown();
                        });

                        $('#poSave').val('Edit');

                    } else {
                        locationProductData = respond.data.locationProducts;
                        selectedSupplier = respond.data.poData.poSID;
                        $("#supplier").empty().
                        append($("<option></option>").
                                attr("value", respond.data.poData.poSID).
                                text(respond.data.poData.poSN));
                        $('#supplier').selectpicker('refresh');
                        $('#supplier').val(respond.data.poData.poSID);
                        $('#supplier').prop('disabled', true);
                        $('#supplier').selectpicker('render');
                        $('#addSupplierBtn').addClass('hidden');
                        $('.supplier-select').removeClass('form-control');
                        $.each(respond.data.poData.poProducts, function(key, value){
                            addNewTmpProductRowByMRP(value, incrementID);
                            incrementID++;
                        });
                        
                    }
                }

            }
        });
    }

    $('#documentReference').on('click', function(e) {
        e.preventDefault();
        if ($('#retrieveLocation').val()) {
            $('#documentReferenceModal').modal('show');
            $('#retrieveLocation').attr('disabled', 'disabled');
        } else {
            $('#documentReferenceModal').modal('hide');
            p_notification(false, eb.getMessage('ERR_PLEASE_SELECT_LOCTION'));
        }
    });

    $('#documentTable').on('change', '.documentTypeId', function() {
        var $dropDownIDParent = $(this).parents('tr');
        var documentTypeID = $(this).val();
        loadDocumentId($dropDownIDParent, documentTypeID);
    });

    function loadDocumentId($dropDownIDParent, documentTypeID) {
        var dropDownID = '.documentId';
        var locationID = $('#retrieveLocation').val();
        var url = '';
        if (documentTypeID == 1) {
            url = '/invoice-api/searchSalesInvoicesForDocumentDropdown';
        } else if (documentTypeID == 2) {
            url = '/quotation-api/searchAllQuotationForDropdown';
        } else if (documentTypeID == 3) {
            url = '/api/salesOrders/search-sales-orders-for-dropdown';
        } else if (documentTypeID == 4) {
            url = '/delivery-note-api/searchDeliveryNoteForDocumentDropdown';
        } else if (documentTypeID == 7) {
            url = '/customerPaymentsAPI/searchPaymentsForDocumentDropdown';
        } else if (documentTypeID == 9) {
            url = '/api/purchase-requistion/search-all-open-po-for-dropdown';
        } else if (documentTypeID == 10) {
            url = '/api/grn/search-all-grn-for-dropdown';
        } else if (documentTypeID == 12) {
            url = '/api/pi/search-allPV-for-dropdown';
        } else if (documentTypeID == 15) {
            url = '/transferAPI/search-transfer-for-dropdown';
        } else if (documentTypeID == 19) {
            url = '/job-api/searchAllJobsForDropdown';
        } else if (documentTypeID == 20) {
            url = '/expense-purchase-invoice-api/getPaymentVoucherForDropDown';
        } else if (documentTypeID == 22) {
            url = '/api/project/searchProjectsForDropdown';
        } else if (documentTypeID == 23) {
            url = '/api/activity/get-all-activities-for-dropdown';
        }
        $('div.bootstrap-select.documentId', $dropDownIDParent).remove();
        $('select.selectpicker.documentId', $dropDownIDParent).show().removeData().empty();
        loadDropDownFromDatabase(url, locationID, 0, dropDownID, $dropDownIDParent);
    }

    function updateDocTypeDropDown() {
        $allRow = $('tr.documentRow.editable');
        $allRow.find('option').removeAttr('disabled')
        $.each(documentTypeArray, function(index, value) {
            $allRow.find(".documentTypeId option[value='" + value + "']").attr('disabled', 'disabled');
        });
        $allRow.find('.documentTypeId').selectpicker('render');
        $allRow.find('.documentTypeId').selectpicker('refresh');
    }

    var documentTypeArray = [];
    $('#documentTable').on('click', '.saveDocument', function(e) {
        e.preventDefault();
        var $row = $(this).parents('tr');
        if (validateDocumentTypeRow($row)) {
            documentTypeArray.push($row.find('.documentTypeId').val());
            $row.removeClass('editable');
            $row.find('.documentTypeId').attr('disabled', true);
            $row.find('.documentId').attr('disabled', true);
            $row.find('.saveDocument').addClass('hidden');
            $row.find('.deleteDocument').removeClass('hidden');
            var $cloneRow = $($('.sample').clone()).removeClass('sample hidden').addClass('documentRow editable');

            $cloneRow.find('.documentTypeId').selectpicker().attr('data-liver-search', true);
            $cloneRow.insertBefore($row);

            updateDocTypeDropDown();
        }
    })

    $('#documentTable').on('click', '.deleteDocument', function(e) {
        e.preventDefault();
        var $row = $(this).parents('tr');
        var documentTypeID = $row.find('select.documentTypeId').val();
        //this is for remove documentTypeID from the documentType array
        documentTypeArray = jQuery.grep(documentTypeArray, function(value) {
            return value != documentTypeID;
        });
        $row.remove();
        updateDocTypeDropDown();
    });

    $('#documentTable').on('click', '.editDocument', function(e) {
        e.preventDefault();
        var $row = $(this).parents('tr');
        $row.find('.documentId').attr('disabled', false);
        $row.find('.updateDocument').removeClass('hidden');
        $row.find('.editDocument').addClass('hidden');
    });

    $('#documentTable').on('click', '.updateDocument', function(e) {
        e.preventDefault();
        var $row = $(this).parents('tr');
        if (validateDocumentTypeRow($row)) {
            $row.find('.documentTypeId').attr('disabled', true);
            $row.find('.documentId').attr('disabled', true);
            $row.find('.updateDocument').addClass('hidden');
            $row.find('.editDocument').removeClass('hidden');
        }
    });

    function validateDocumentTypeRow($row) {
        var flag = true;
        var documentTypeID = $row.find('.documentTypeId').val();
        var documentID = $row.find('.documentId').val();
        if (documentTypeID == '' || documentTypeID == null) {
            p_notification(false, eb.getMessage('ERR_PO_DOC_TYPE_SELECT'));
            flag = false;
        } else if (documentID == '' || documentID == null) {
            p_notification(false, eb.getMessage('ERR_PO_DOC_ID_SELECT'));
            flag = false;
        }
        return flag;
    }

    $('#add-new-item-row').on('click', '#addItem', function() {
        var row = $(this).parents('tr');
        var uomqty = $("#qty").siblings('.uomqty').val();
        $("#qty").siblings('.uomqty').change();

        selectedProductUom = $("#qty").siblings('.uom-select').children('button').find('.selected').data('uomID');
        if (typeof (selectedProductID) == 'undefined' || selectedProductID == '') {
            p_notification(false, eb.getMessage('ERR_GRN_SELECT_PROD'));
        }
        else if (typeof (selectedProductUom) == 'undefined' || selectedProductUom == '') {
            p_notification(false, eb.getMessage('ERR_GRN_SELECT_UOM'));

        // } else if (products[currentProduct.lPID]) {
        //     p_notification(false, eb.getMessage('ERR_GRN_INSERT_PROD'));
        } else {
            if ($('#unitPrice').val() == '') {
                $('#unitPrice').focus();
                p_notification(false, eb.getMessage('ERR_GRN_ENTER_UNITPRI'));
            } else if ($('#itemCode').data('PT') != 2 && $('#qty').val() == '') {
                p_notification(false, eb.getMessage('ERR_GRN_ENTER_QUANTITY'));
                $('.uomqty').focus();
            } else if ((uomqty == 0) || (uomqty == '')) {
                p_notification(false, eb.getMessage('ERR_GRN_ENTER_QUANTITY'));
                $('.uomqty').focus();
            } else {
                $("#qty").siblings('.uomqty').change();
                $('#unitPrice', row).trigger('focusout');
                
                addNewProductRow();
                if (sameItemErrorFlag == false) {
                    $('#qty').parent().removeClass('input-group');
                }

                $('#itemCode').val(0).trigger('change').empty().selectpicker('refresh');
                $('#itemCode').focus();
            }
        }
        //in mobile view show the insert data row after the (+) click event
        $('#poProductTable thead').css('display', 'table-header-group');


    });

    $('#add-new-item-row').on('click', '#addItemByDraft', function() {
        var rowID = $(this).parents('tr').attr('id');
        var productID = $(this).parents('tr').attr('data-id');
        var row = $(this).parents('tr');
        poDrfatFlag = true;
        currentProductByDraft = locationproductsByPoDraft[productID];
        poDraftUnitPrice = $(row).find('.uomPrice').val();
        poDraftQty = $(row).find('.uomqty').val();
        poDraftTotal = $(row).find('#addNewTotal').html();

        //calculate taxes
        var checkedTaxes = Array();
        $.each($(this).parents('tr').find('input:checkbox.addNewTaxCheck'), function(){
            if (this.checked) {
                var cliTID = this.id;
                var tTaxID = cliTID.split('_')[1];
                checkedTaxes.push(tTaxID);
            }
        });
        var qty = $(this).parents('tr').find('.uomqty').val();
        var unitPrice = $(this).parents('tr').find('.uomPrice').val();

        if ($('#itemCode').data('PT') == 2 && qty == 0) {
            qty = 1;
        }

        var tmpItemCost = calculateTaxFreeItemCost(unitPrice, qty, $('#poDiscount').val());
        currentItemTaxResults = calculateItemCustomTax(tmpItemCost, checkedTaxes);
        currentTaxAmount = (currentItemTaxResults == null) ? null : currentItemTaxResults.tTA;
        var itemCost = toFloat(tmpItemCost) + toFloat(currentTaxAmount);
        $(this).parents('tr').find('#addNewTotal').html(accounting.formatMoney(itemCost));

        ///////
        addNewProductRow();
        $('#'+rowID).remove();


    });

    $('#poProductTable').on('click', 'ul.dropdown-menu, .dropdown-menu label', function(e) {
        e.stopPropagation();
    });

    $('#poProductTable').on('keyup', '#qty, #unitPrice, #poDiscount,.uomqty, .uomPrice', function() {
        if ($('#itemCode').val() != '') {
            if ($(this).val().indexOf('.') > 0) {
                decimalPoints = $(this).val().split('.')[1].length;
            }
            if (this.id == 'unitPrice' && (!$.isNumeric($('#unitPrice').val()) || $('#unitPrice').val() < 0 || decimalPoints > 2)) {
                decimalPoints = 0;
                $('#unitPrice').val('');
            }
            if ((this.id == 'qty') && (!$.isNumeric($('#qty').val()) || $('#qty').val() < 0)) {
                decimalPoints = 0;
                $('#qty').val('');
            }
            if ((this.id == 'poDiscount') && (!$.isNumeric($('#poDiscount').val()) || $('#poDiscount').val() < 0 || (decimalPoints > 2))) {
                decimalPoints = 0;
                $('#poDiscount').val('');
            }
        } else {
            $('#unitPrice, #qty, #poDiscount').val('');
        }
    });

    $('#poProductTable').on('focusout', '#qty,#unitPrice,#poDiscount,.uomqty,.uomPrice, .current-edit-disc, .disc-line,input[name="discount"]', function() {
        var checkedTaxes = Array();
        currentItemTaxResults = null;
        currentTaxAmount = null;
        $.each($(this).parents('tr').find('input:checkbox.addNewTaxCheck'), function(){
            if (this.checked) {
                var cliTID = this.id;
                var tTaxID = cliTID.split('_')[1];
                checkedTaxes.push(tTaxID);
            }
        });
        var qty = $(this).parents('tr').find('.uomqty').val();
        var bqty = $(this).parents('tr').find('#qty').val();
        var unitPrice = $(this).parents('tr').find('.uomPrice').val();
        var uPrice = $(this).parents('tr').find('#unitPrice').val();
        var uP = $("input[name='unitPrice']", $(this).parents('tr')).val();
        if ($('#itemCode').data('PT') == 2 && qty == 0) {
            qty = 1;
        }
        var productID = $('#itemCode', $(this).parents('tr')).data('pID');

        var discount = $("input[name='discount']", $(this).parents('tr')).val();
        if ($(this).parents('tr').hasClass('edit-mode')) {
            productID = $(this).parents('tr').data('id');
            discount = $('.current-edit-disc').val();
        }

        if ($(this).parents('tr').hasClass('current-edited-line')) {
            productID = $(this).parents('tr').data('id');
            
        }
        if (productID == undefined && $(this).parents('tr').hasClass('draftProduct')) {
            productID = $(this).parents('tr').data('id');

        }
        if (unitPrice != "" && uP != "") {
            // console.log(uPrice);
            // $(this).parents('tr').find('.uomPrice').val(parseFloat(unitPrice).toFixed(2));
            // $(this).parents('tr').find("input[name='unitPrice']").val(parseFloat(uPrice).toFixed(2));
        }
        if (productID != undefined) {

            if ($(this).parents('tr').hasClass('draftProduct') 
                    || $(this).parents('tr').hasClass('edit-mode') 
                        || $(this).parents('tr').hasClass('from-mrp-edit')) {
                var crreItmDetais = locationproductsByPoDraft[productID];
            } else {
                var crreItmDetais  = locationProductData[productID];
            }
            if (crreItmDetais) {
                if (crreItmDetais.pPDP != null) {
                    discountType = "Per";
                } else if (crreItmDetais.pPDV != null) {
                    discountType = "Val";
                } else {
                    discountType = "Non"
                }
                if (discountType == "Val") {
                    var newDiscount = validateDiscount(productID,discountType,discount);
                    console.log(unitPrice);
                    var tmpItemCost = calculateTaxFreeItemCostByValueDiscount(uPrice, bqty, newDiscount);
                    if (tmpItemCost < 0) {
                        tmpItemCost = 0;
                    }
                } else if (discountType == "Per"){
                    var newDiscount = validateDiscount(productID,discountType,discount);
                    var tmpItemCost = calculateTaxFreeItemCost(unitPrice, qty, newDiscount);
                } else {
                    newDiscount = 0;
                    var tmpItemCost = calculateTaxFreeItemCost(unitPrice, qty, newDiscount);
                }
                $("input[name='discount']", $(this).parents('tr')).val(newDiscount);
                currentItemTaxResults = calculateItemCustomTax(tmpItemCost, checkedTaxes);
                currentTaxAmount = (currentItemTaxResults == null) ? null : currentItemTaxResults.tTA;
                var itemCost = toFloat(tmpItemCost) + toFloat(currentTaxAmount);
                $(this).parents('tr').find('.newTotalVal').html(accounting.formatMoney(itemCost));
            } else {
                $("input[name='discount']", $(this).parents('tr')).val(discount);
                var tmpItemCost = calculateTaxFreeItemCost(unitPrice, qty, discount);
                currentItemTaxResults = calculateItemCustomTax(tmpItemCost, checkedTaxes);
                currentTaxAmount = (currentItemTaxResults == null) ? null : currentItemTaxResults.tTA;
                var itemCost = toFloat(tmpItemCost) + toFloat(currentTaxAmount);
                $(this).parents('tr').find('.newTotalVal').html(accounting.formatMoney(itemCost));
            }
        }
        $(this).parents('tr').find('.uomPrice').attr('disabled', false);


        // var tmpItemCost = calculateTaxFreeItemCost(unitPrice, qty, $('#poDiscount').val());
        // currentItemTaxResults = calculateItemCustomTax(tmpItemCost, checkedTaxes);
        // currentTaxAmount = (currentItemTaxResults == null) ? null : currentItemTaxResults.tTA;
        // var itemCost = toFloat(tmpItemCost) + toFloat(currentTaxAmount);
        // $(this).parents('tr').find('#addNewTotal').html(accounting.formatMoney(itemCost));
    });

    $('#poProductTable').on('change', '.taxChecks.addNewTaxCheck', function() {
        var allchecked = false;
        $('input:checkbox.addNewTaxCheck').each(function() {
            if (this.checked && $(this).prop('disabled') === false) {
                allchecked = true;
            }
        });
        var currentRowID = $(this).parents('tr').attr('id');
        if (allchecked == false) {
            if ($(this).parents('tr').hasClass('current-edited-line')) {
                $('.current-edited-line').find('#poTaxApplied').removeClass('glyphicon glyphicon-check');
                $('.current-edited-line').find('#poTaxApplied').addClass('glyphicon glyphicon-unchecked');

            } else if (editedFlag) {
                $('#'+currentRowID).find('#poTaxApplied').removeClass('glyphicon glyphicon-check');
                $('#'+currentRowID).find('#poTaxApplied').addClass('glyphicon glyphicon-unchecked');
            } else {
                $('#taxApplied').removeClass('glyphicon glyphicon-check');
                $('#taxApplied').addClass('glyphicon glyphicon-unchecked');
                
            }
        } else {
            if ($(this).parents('tr').hasClass('current-edited-line')) {
                $('.current-edited-line').find('#poTaxApplied').removeClass('glyphicon glyphicon-unchecked');
                $('.current-edited-line').find('#poTaxApplied').addClass('glyphicon glyphicon-check');
            } else if (editedFlag) {
                $('#'+currentRowID).find('#poTaxApplied').removeClass('glyphicon glyphicon-unchecked');
                $('#'+currentRowID).find('#poTaxApplied').addClass('glyphicon glyphicon-check');
            } else {
                $('#taxApplied').removeClass('glyphicon glyphicon-unchecked');
                $('#taxApplied').addClass('glyphicon glyphicon-check');
                
            }
        }
            $("#"+currentRowID).find('.uomqty').trigger(jQuery.Event("focusout"));
            $("#unitPrice").trigger(jQuery.Event("focusout"));
            
    });

    $('#poProductTable').on('click', '#selectAll', function() {
        var desTaxLine = $(this).parents('#addTaxUl');
        if (editedFlag || $(this).parents('tr').hasClass('current-edited-line') || $(this).parents('tr').hasClass('draftProduct')) {
            desTaxLine = $(this).parents('#poAddTaxUl');
            desTaxLine.find('#poTaxApplied').removeClass('glyphicon glyphicon-unchecked');
            desTaxLine.find('#poTaxApplied').addClass('glyphicon glyphicon-check');
            if ($(this).parents('tr').hasClass('from-mrp-edit')) {
                $(this).parents('#poTaxDiv').find('#poTaxApplied').removeClass('glyphicon glyphicon-unchecked');
                $(this).parents('#poTaxDiv').find('#poTaxApplied').addClass('glyphicon glyphicon-check');
            }      
        } else {
            desTaxLine.find('#taxApplied').removeClass('glyphicon glyphicon-unchecked');
            desTaxLine.find('#taxApplied').addClass('glyphicon glyphicon-check');
        }
        
        desTaxLine.find('input:checkbox.addNewTaxCheck').each(function() {
            if (!this.checked) {
                this.checked = true;
            }
        });
        $("#unitPrice").trigger(jQuery.Event("focusout"));
        $(this).parents('tr').find(".uomPrice").trigger(jQuery.Event("focusout"));
    });

    $('#poProductTable').on('click', '#deselectAll', function() {
        var desTaxLine = $(this).parents('#addTaxUl');
        if (editedFlag || $(this).parents('tr').hasClass('current-edited-line')|| $(this).parents('tr').hasClass('draftProduct')) {
            desTaxLine = $(this).parents('#poAddTaxUl');
            desTaxLine.find('#poTaxApplied').removeClass('glyphicon glyphicon-check');
            desTaxLine.find('#poTaxApplied').addClass('glyphicon glyphicon-unchecked');
            if ($(this).parents('tr').hasClass('from-mrp-edit')) {
                $(this).parents('#poTaxDiv').find('#poTaxApplied').removeClass('glyphicon glyphicon-check');
                $(this).parents('#poTaxDiv').find('#poTaxApplied').addClass('glyphicon glyphicon-unchecked');
            } 
        } else {
            desTaxLine.find('#taxApplied').removeClass('glyphicon glyphicon-check');
            desTaxLine.find('#taxApplied').addClass('glyphicon glyphicon-unchecked');
        }
        desTaxLine.find('input:checkbox.addNewTaxCheck').each(function() {
            if (this.checked) {
                this.checked = false;
            }
        });
        
        $(this).parents('tr').find(".uomPrice").trigger(jQuery.Event("focusout"));
        $("#unitPrice").trigger(jQuery.Event("focusout"));

    });

    $('#poProductTable').on('change', '.addNewUomR', function() {
        $('#uomAb').html(currentProduct.uom[$(this).val()].uA);
        selectedProductUom = $(this).val();
    });

    $('#deliveryChargeEnable').on('click', function() {
        if (this.checked) {
            $('.deliCharges').removeClass('hidden');
            $('#deliveryCharge').focus();
        }
        else {
            $('#deliveryCharge').val('');
            $('.deliCharges').addClass('hidden');
        }
        setPoTotalCost();
    });

    $('#deliveryCharge').on('change keyup', function() {
        if (!$.isNumeric($('#deliveryCharge').val()) || $(this).val() < 0) {
            $(this).val('');
            setPoTotalCost();
        } else {
            setPoTotalCost();
        }
    });

    $('#poProductTable').on('click', '.poDeleteProduct', function() {
        var deletePTrID = $(this).closest('tr').attr('id');
        var deletePID = deletePTrID.split('tr_')[1].trim();
        bootbox.confirm('Are you sure you want to remove this product ?', function(result) {
            if (result == true) {
                delete(products[deletePID]);
                $('#' + deletePTrID).remove();
                setPoTotalCost();
                setTotalTax();
                currentlySelectedProductID = '';
                selectedProductID = '';
            }
        });
    });

    $('#showTax').on('click', function() {
        if (this.checked) {
            if (!jQuery.isEmptyObject(products)) {
                $('#totalTaxShow').removeClass('hidden');
            } else {
                p_notification(false, eb.getMessage('ERR_PO_ADDPRO_VIEWTAX'));
            }

        } else {
            $('#totalTaxShow').addClass('hidden');
        }

    });

    $('#poCancel').on('click', function() {
        window.location.reload();
    });

    $('#grnForm').on('submit', function(e) {
        e.preventDefault();

        var documentReference = {};
        var flagfalse = false;
        $('.documentTypeBody > tr').each(function() {
            if ($(this).hasClass('documentRow')) {
                var documentTypeID = $(this).find('.documentTypeId').val();
                var documentID = $(this).find('.documentId').val();
                if (!(documentTypeID == null || documentTypeID == '')) {
                    if (documentID != null) {
                        documentReference[documentTypeID] = documentID;
                    } else {
                        flagfalse = true;
                    }
                }
            }
        });

        var selectedid = "#" + document.activeElement.id;
        var tableId = $(selectedid).closest('table').attr("id");
        if (flagfalse) {
            p_notification(false, eb.getMessage('ERR_PLEASE_FILL_ALL_DOCUMENT_REFERENCE'));
        } else if (typeof (selectedSupplier) == 'undefined' || selectedSupplier == '' || selectedSupplier == 0) {
            p_notification(false, eb.getMessage('ERR_GRN_SELECT_SUPP'));
        }else if( typeof (selectedLocation) == 'undefined' || selectedLocation == 0){
            p_notification(false, eb.getMessage('ERR_GRN_SELECT_LOCAT_REFNO'));
        }
        else if (!($('#expectedDeliveryDate').val())) {
            p_notification(false, eb.getMessage('ERR_GRN_SET_DELIDATE'));
        }
        else if ($('#poNo').val() == '') {
            p_notification(false, eb.getMessage('ERR_GRN_SELECT_LOCAT_REFNO'));
        } else if (Date.parse($('#expectedDeliveryDate').val()) < Date.parse($('#purchaseOrderDate').val())) {
            p_notification(false, eb.getMessage('ERR_PREQ_DELIVERY_DATE'));
        } else if (jQuery.isEmptyObject(products)) {
            if (tableId != 'poProductTable') {
                p_notification(false, eb.getMessage('ERR_PREQ_NOPRO_ADDPO'));
            }
        } else {
            $('#poSave').prop('disabled', true);
            var poFT = accounting.unformat($('#finaltotal').html());
            var sID = selectedSupplier;
            var showTax = 0;
            if ($('#showTax').is(':checked')) {
                showTax = 1;
            }

            var productSortedArray = [];
            $.each(products, function(index, val) {
                productSortedArray[val.rowincrementID] = val;
            });

            var poPostData = {
                sID: sID,
                poC: $('#poNo').val(),
                eDD: $('#expectedDeliveryDate').val(),
                poD: $('#purchaseOrderDate').val(),
                sR: $('#supplierReference').val(),
                rL: selectedLocation,
                cm: $('#comment').val(),
                pr: productSortedArray,
                realProductArray: products,
                dC: $('#deliveryCharge').val(),
                fT: poFT,
                sT: showTax,
                purchaseReqType: $('#purchaseReqType').val(),
                lRID: $('#locRefID').val(),
                documentReference: documentReference,
            };
            var URL = BASE_URL + '/api/purchase-requistion/savePurchaseRequisition';
            if($('#poDraft').hasClass('frommrp')){
                poPostData['poID'] = $('#poDraft').val();
                URL = BASE_URL + '/api/purchase-requistion/updatePurchaseRequisition';
                if (editedFlag) {
                    URL = BASE_URL + '/api/purchase-requistion/updatePurchaseRequisition';
                }
            }

            if(editedFlag) {
                var existingAttachemnts = {};
                var deletedAttachments = {};
                $.each(uploadedAttachments, function(index, val) {
                    if (!deletedAttachmentIds.includes(val['documentAttachemntMapID'])) {
                        existingAttachemnts[index] = val;
                    } else {
                        deletedAttachments[index] = val;
                    }
                });
            }

            eb.ajax({
                type: 'POST',
                url: URL ,
                data: poPostData,
                success: function(respond) {
                    if (respond.status == true) {
                        if (respond.data.approversFlag) {
                            p_notification(true, eb.getMessage('APPROVER_WORKFLOW_SUCESS'));

                        } else {
                            p_notification(true, respond.msg);
                        }
                        if (!editedFlag) {
                            var fileInput = document.getElementById('documentFiles');
                            if(fileInput.files.length > 0) {
                                var form_data = false;
                                if (window.FormData) {
                                    form_data = new FormData();
                                }
                                form_data.append("documentID", respond.data.poID);
                                form_data.append("documentTypeID", 38);
                                
                                for (var i = 0; i < fileInput.files.length; i++) {
                                    form_data.append("files[]", fileInput.files[i]);
                                }

                                eb.ajax({
                                    url: BASE_URL + '/store-files',
                                    type: 'POST',
                                    processData: false,
                                    contentType: false,
                                    data: form_data,
                                    success: function(res) {
                                    }
                                });
                            }

                            var purchReqPath = '/api/purchase-requistion/sendPurchaseRequisitionEmail';
                            documentPreview(BASE_URL + '/purchase-requistion/preview/' + respond.data.poID, 'documentpreview', purchReqPath, function($preview) {
                                $('iframe', $preview).bind('load', function() {

                                    // unbind event because if user changes template, then this event
                                    // will be triggered again and send the email multiple times
                                    $('iframe', $preview).unbind('load');
                                    sendEmailToApprover(respond.data.poID, respond.data.purchaseRequisitionCode, respond.data.approver, respond.data.hashValue, respond.data.approversFlag, $preview, function($preview) {
                                        $preview.on('hidden.bs.modal', function() {
                                            if (!$("#preview:visible").length) {
                                                window.location.reload();
                                            }
                                        });
                                    });
                                });
                            });
                        } else {
                            var fileInput = document.getElementById('editDocumentFiles');
                            var form_data = false;
                            if (window.FormData) {
                                form_data = new FormData();
                            }
                            form_data.append("documentID", respond.data.poID);
                            form_data.append("documentTypeID", 38);
                            form_data.append("updateFlag", true);
                            form_data.append("editedDocumentID", $('#poDraft').val());
                            form_data.append("documentCode", $('#poNo').val());
                            form_data.append("deletedAttachmentIds", deletedAttachmentIds);
                            
                            if(fileInput.files.length > 0) {
                                for (var i = 0; i < fileInput.files.length; i++) {
                                    form_data.append("files[]", fileInput.files[i]);
                                }
                            }

                            eb.ajax({
                                url: BASE_URL + '/store-files',
                                type: 'POST',
                                processData: false,
                                contentType: false,
                                data: form_data,
                                success: function(res) {
                                }
                            }).then(function(){
                                if (respond.data.approversFlag) {
                                   sendEmailToApprover(respond.data.poID, respond.data.purchaseRequisitionCode, respond.data.approver, respond.data.hashValue, respond.data.approversFlag, null, null);
                                } else {
                                    var url = BASE_URL + '/purchase-requistion/list';
                                    window.location.assign(url);
                                }
                            });

                        }
                    } else {
                        p_notification(false, respond.msg);
                        $('#poSave').prop('disabled', false);
                    }
                },
                async: false
            });

        }
    });

    function sendEmailToApprover(pReqId, pReqCode, approvers, token, flag, preview = null, callback = null) {
        if (flag) {
            eb.ajax({
                type: 'POST',
                url: BASE_URL + '/api/purchase-requistion/sendApproverEmail',
                data: {
                    pReqId: pReqId,
                    pReqCode: pReqCode,
                    approvers: approvers,
                    token: token,
                },
                success: function(respond) {
                    if (respond.status == true) {
                        p_notification(respond.status, respond.msg);
                        if (callback != null) {
                            callback(preview);
                        } else {
                            var url = BASE_URL + '/purchase-requistion/list';
                            window.location.assign(url);
                        }
                    } else {
                        p_notification(respond.status, respond.msg);
                    }
                }

            });
        } else {
            callback(preview);
        }
    }

    $('table#poProductTable>tbody').on('keypress', 'input#qty, input#unitPrice, input#poDiscount, .uomqty', function(e) {

        if (e.which == 13) {
            $('#addItem', $(this).parents('tr')).trigger('click');
            e.preventDefault();
        }

    });


///////DatePicker\\\\\\\
    var nowTemp = new Date();
    var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);

    var checkin1 = $('#expectedDeliveryDate').datepicker({
        onRender: function(date) {
            return date.valueOf() < now.valueOf() ? ((U_DATE_R_OVERRIDE)?'':'disabled') : '';
        }
    }).on('changeDate', function(ev) {
        checkin1.hide();
    }).data('datepicker');
    checkin1.setValue(now);


    var checkin2 = $('#purchaseOrderDate').datepicker({
        onRender: function(date) {
            return date.valueOf() < now.valueOf() ? ((U_DATE_R_OVERRIDE)?'':'disabled') : '';
        }
    }).on('changeDate', function(ev) {
        checkin2.hide();
    }).data('datepicker');
    checkin2.setValue(now);
/////EndOFDatePicker\\\\\\\\\

    function setTotalTax() {
        totalTaxList = {};
        for (var k in products) {
            if (products[k].pTax != null) {
                for (var l in products[k].pTax.tL) {
                    if (totalTaxList.hasOwnProperty(l)) {
                        var curTA = totalTaxList[l].tA;
                        var newTA = (toFloat(curTA) + toFloat(products[k].pTax.tL[l].tA));
                        totalTaxList[l].tA = newTA.toFixed(2);
                    } else {
                        totalTaxList[l] = {tN: products[k].pTax.tL[l].tN, tP: products[k].pTax.tL[l].tP, tA: products[k].pTax.tL[l].tA};
                    }
                }
            }
        }
        var totalTaxHtml = "";
        for (var t in totalTaxList) {
            totalTaxHtml += totalTaxList[t].tN + "(" + totalTaxList[t].tP + "%)&nbsp" + accounting.formatMoney(totalTaxList[t].tA) + "<br>";
        }
        $('#totalTaxShow').html(totalTaxHtml);
    }

    function setPoTotalCost() {
        var grnTotal = 0;
        for (var i in products) {
            grnTotal += toFloat(products[i].pTotal);
        }
        $('#subtotal').html(accounting.formatMoney(grnTotal));
        var deliveryAmount = $('#deliveryCharge').val();
        grnTotal += toFloat(deliveryAmount);
        $('#finaltotal').html(accounting.formatMoney(grnTotal));
    }

    function addNewProductRow() {

        sameItemFlag = false;
        sameItemErrorFlag = false;
        totalTaxObject = {};
        if(poDrfatFlag){
            var iC = currentProductByDraft.pC;
            var iN = currentProductByDraft.pN;
            var uPrice = accounting.formatMoney(poDraftUnitPrice);
            var uFPrice = poDraftUnitPrice;
            var qt = poDraftQty;
            selectedProductID = currentProductByDraft.pID;
            if (qt == 0) {
                qt = 0;
            }

            var uomqty = poDraftQty;
            var uomPrice = poDraftUnitPrice;
            var nTotal = poDraftTotal;
            var calNTotal = accounting.unformat(nTotal);
            var gD = $('input[name="discount"]').val();
            if ($('input[name="discount"]').val() == '')
                gD = 0;
            var locationProductID = currentProductByDraft.lPID;
            selectedProductQuantity = poDraftQty;
            var newTrID = 'tr_' + locationProductID;
            if (products[currentProduct.lPID] != undefined) {
                sameItemFlag = true;
                sameItemErrorFlag = true;
            }

        } else {
           var iC = currentProduct.pC;
            var iN = currentProduct.pN;
            var uPrice = accounting.formatMoney($('#unitPrice').val());
            var uFPrice = $('#unitPrice').val();
            var qt = $('#qty').val();
            if (qt == 0) {
                qt = 0;
            }

            var uomqty = $("#qty").siblings('.uomqty').val();
            var uomPrice = $("#unitPrice").siblings('.uomPrice').val();
            var nTotal = $('#addNewTotal').html();
            var calNTotal = accounting.unformat(nTotal);
            var gD = $('#poDiscount').val();
            if ($('#poDiscount').val() == '')
                gD = 0;
            var locationProductID = currentProduct.lPID;
            selectedProductQuantity = $('#qty').val();
            var newTrID = 'tr_' + locationProductID;
            sameItemErrorFlag = false;
            if (products[currentProduct.lPID] != undefined) {
                sameItemFlag = true;
                addCumalativeProduct(currentItemTaxResults, currentProduct.lPID, gD, uFPrice, calNTotal);
            }
        }

        if (sameItemFlag == false) {

            if(poDrfatFlag){
                var newProduct = currentProductByDraft;
            } else {
                var newProduct = currentProduct;
            }
            products[newProduct.lPID] = new product(newProduct.lPID, selectedProductID, iC, iN, qt, gD, uFPrice, selectedProductUom, calNTotal, currentItemTaxResults,null,null, rowincrementID);
            addItemLines(newProduct.lPID);
            setPoTotalCost();
            setTotalTax();
            clearAddNewRow();
            poDrfatFlag = false;
            currentProductByDraft = null;
            poDraftUnitPrice = null;
            poDraftQty = null;
            poDraftTotal = null;
            rowincrementID++;

        } else {
            if (sameItemErrorFlag == true) {
                if (poDrfatFlag) {
                    p_notification(false, eb.getMessage('ERR_GRN_INSERT_PROD'));
                } else {
                    p_notification(false, eb.getMessage('ERR_PO_SAME_PRO_DIFF_PRICE'));

                }
            } else {
                products[currentProduct.lPID].pQuantity = parseFloat(products[currentProduct.lPID].pQuantity) + parseFloat(qt);
                products[currentProduct.lPID].pTotal = parseFloat(products[currentProduct.lPID].pTotal) + parseFloat(calNTotal);
                products[currentProduct.lPID].pTax = totalTaxObject;
                $('#tr_'+currentProduct.lPID).children().find('.uomqty').val(parseFloat(products[currentProduct.lPID].pQuantity));
                $('#tr_'+currentProduct.lPID).children().find('.newTotalVal').html(parseFloat(products[currentProduct.lPID].pTotal).toFixed(2));
               if (editedFlag) {
                $('#trE_'+currentProduct.lPID).children().find('.uomqty').val(parseFloat(products[currentProduct.lPID].pQuantity));
                $('#trE_'+currentProduct.lPID).children().find('.newTotalVal').html(parseFloat(products[currentProduct.lPID].pTotal).toFixed(2));

               }
                setPoTotalCost();
                setTotalTax();
                clearAddNewRow();
                poDrfatFlag = false;
                currentProductByDraft = null;
                poDraftUnitPrice = null;
                poDraftQty = null;
                poDraftTotal = null;
                
            }
        }
    }

    function addItemLines(locationProductId) {
        var savedItemDetails = products[locationProductId];
        var productID = savedItemDetails.pID;
        var nmCls = 'normal';
        if (poDrfatFlag) {
            var savedItemCommnDetails = locationproductsByPoDraft[productID];
            nmCls = 'mrp';
        } else {
            var savedItemCommnDetails = locationProductData[productID];

        }
        var poLocProID = locationProductId;
        var newTrID = 'tr_'+poLocProID;
        var clonedRow = $($('#add-new-po-record').clone()).attr('id', newTrID).attr('data-id',productID).addClass(nmCls);
        $('div.bootstrap-select', clonedRow).remove();
        $("#itemCode", clonedRow).empty().
            append($("<option></option>").
                attr("value", savedItemDetails.pID).
                    text(savedItemDetails.pCode + ' - ' + savedItemDetails.pName));
        $('#itemCode', clonedRow).val(savedItemDetails.pID);
        $('#itemCode', clonedRow).data('PT', savedItemCommnDetails.pT);
        $('#itemCode', clonedRow).data('PC', savedItemDetails.pCode);
        $('#itemCode', clonedRow).data('PN', savedItemDetails.pName);
        $('#itemCode', clonedRow).prop('disabled', true);
        $('#itemCode', clonedRow).selectpicker('render');
        $('#itemCode', clonedRow).attr('id', '');

        $('#qty', clonedRow).val(savedItemDetails.pQuantity);
        $('#qty', clonedRow).addUom(savedItemCommnDetails.uom);
        $('#qty', clonedRow).parent().addClass('input-group');
        $('#qty', clonedRow).attr('id', '');
        $('.uomqty', clonedRow).prop('disabled', true);
        $('#itemQuantity', clonedRow).attr('class', 'hidden');

        $('#addNewUomDiv', clonedRow).attr('id', '');
        $('#unitPrice', clonedRow).val(savedItemDetails.pUnitPrice);
        $('#unitPrice', clonedRow).addUomPrice(savedItemCommnDetails.uom);
        $('#unitPrice', clonedRow).prop('disabled', true);
        $('#unitPrice', clonedRow).attr('id', '');
        $('.uomPrice', clonedRow).prop('disabled', true);
        $('#itemUnitPrice', clonedRow).attr('class', 'hidden');
        if (savedItemDetails.pDiscount == null) {
            savedItemDetails.pDiscount = 0;
        }
        $('#poDiscount', clonedRow).val(savedItemDetails.pDiscount);
        $('#poDiscount', clonedRow).addClass('po-disc');
        $('.po-disc', clonedRow).attr('disabled', true);
        $('#poDiscount', clonedRow).attr('id', '');
        $('.newTotalVal', clonedRow).html(toFloat(savedItemDetails.pTotal).toFixed(2));
        $('.newTotalVal', clonedRow).attr('id','addedAmo');
        $('#addNewUom', clonedRow).prop('disabled', true);
        $('#addNewUom', clonedRow).attr('id', '');
        $('#uomAb', clonedRow).html(savedItemDetails.pUom);
        $('#uomAb', clonedRow).attr('id', '');
        $('#addUomUl', clonedRow).attr('id', '');
        $('#addItem', clonedRow).attr('id', '').addClass('po-item-add').parent().addClass('hidden');
        $('#editItem', clonedRow).attr('id', '').addClass('po-item-edit').parent().removeClass('hidden');
        $('#deleteItem', clonedRow).attr('id', '').addClass('po-item-delete').parent().removeClass('hidden');
        //setting tax for products
        var checkedTaxes = Array();
        if (!jQuery.isEmptyObject(savedItemDetails.pTax)) {
            var currentProductTax = savedItemDetails.pTax.tL;
            $('#addNewTaxDiv', clonedRow).attr('id', "poTaxDiv");
            $('#addNewTax', clonedRow).attr('id', "poAddTaxDiv");
            $('#taxApplied', clonedRow).removeClass('glyphicon glyphicon-unchecked');
            $('#taxApplied', clonedRow).addClass('glyphicon glyphicon-check');
            $('#taxApplied', clonedRow).attr('id', 'poTaxApplied')
            $('#addTaxUl', clonedRow).attr('id', 'poAddTaxUl');
            $('.tempLi', clonedRow).remove();
            for (var i in currentProductTax) {
                var clonedLi = $($('#sampleLi', clonedRow).clone()).removeClass('hidden').attr('id', 'li_' + i).addClass("poTempLi");
                clonedLi.children(".taxChecks").attr('id', savedItemDetails.pID + '_' + i).prop('checked', true).addClass('addNewTaxCheck');
                clonedLi.children(".taxName").html('&nbsp&nbsp' + currentProductTax[i].tN + '&nbsp&nbsp' + currentProductTax[i].tP + '%').attr('for', savedItemDetails.pID + '_' + i);
                clonedLi.insertBefore($('#sampleLi', clonedRow));
                
            }
            $('#sampleLi', clonedRow).attr('id', 'poSampleLi');
        } else {
            $('#addNewTaxDiv', clonedRow).attr('id', "poTaxDiv");
            $('#addNewTax', clonedRow).attr('id', "poAddTaxDiv");
            $('#taxApplied', clonedRow).attr('id', 'poTaxApplied');
            $('#addTaxUl', clonedRow).attr('id', 'poAddTaxUl');
            $('#sampleLi', clonedRow).remove();
            $('.tempLi', clonedRow).remove();
        }
        $('#poAddTaxDiv', clonedRow).prop('disabled', true);
        clonedRow.insertBefore('#add-new-po-record');
    }

    function addCumalativeProduct(currentTax, currentProductLPID, discValue, UPrice, Total) {
        
        if (parseFloat(products[currentProductLPID].pDiscount) == parseFloat(discValue) && parseFloat(products[currentProductLPID].pUnitPrice) == parseFloat(UPrice)) {
            if (Object.keys(products[currentProductLPID].pTax).length != 0 && Object.keys(currentTax).length) {
                checkTaxEqulity(products[currentProductLPID].pTax,currentTax);                
            } else if (Object.keys(products[currentProductLPID].pTax).length == 0 && Object.keys(currentTax).length || Object.keys(products[currentProductLPID].pTax).length != 0 && !(Object.keys(currentTax).length)){
                sameItemErrorFlag = true;    
            }
        } else {
            sameItemErrorFlag = true;
        }
    }
    function checkTaxEqulity(prevTax,currentTax) {
        var newTax = {};
        var validatePrevTax = {};
        var validateCurrTax = {};
        totalTaxObject = {};
        if (Object.keys(prevTax.tL).length == Object.keys(currentTax.tL).length) {
            var totalTaxAmo = 0;
            for(var i in prevTax.tL) {
                for (var j in currentTax.tL) {
                    if (prevTax.tL[i].tN == currentTax.tL[j].tN) {
                        var tmpTax = {};
                        tmpTax.tN = prevTax.tL[i].tN;
                        tmpTax.tP = parseFloat(prevTax.tL[i].tP);
                        tmpTax.tA = parseFloat(prevTax.tL[i].tA) + parseFloat(currentTax.tL[j].tA);
                        totalTaxAmo += parseFloat(prevTax.tL[i].tA) + parseFloat(currentTax.tL[j].tA);
                        tmpTax.susTax = prevTax.tL[i].susTax;
                        newTax[i] = tmpTax;
                        validatePrevTax[i] = prevTax.tL[i];
                        validateCurrTax[i] = currentTax.tL[j];
                    }
                }
            }
            if (Object.keys(validatePrevTax).length == Object.keys(prevTax.tL).length && 
                    Object.keys(validateCurrTax).length == Object.keys(currentTax.tL).length) {
                totalTaxObject.tTA = totalTaxAmo;
                totalTaxObject.tL = newTax;
                sameItemErrorFlag = false;
            } else {
                sameItemErrorFlag = true;
            } 
            
        } else {
            sameItemErrorFlag = true;

        }  
        
    }


    ////////////////////////////////
    function addNewTmpProductRowByMRP(data, incID) {
        var productID = data.poPID;
        var incrementID = incID;
        var locationProductID = data.lPID;
        locationproductsByPoDraft[data.poPID]['selectedUomID'] = data.poPUomID;
        var newTrID = incrementID+'_'+locationProductID;
        var clonedRow = $($('#add-new-po-record').clone()).attr('id', newTrID).addClass('draftProduct').attr('data-id',productID);
        $('div.bootstrap-select', clonedRow).remove();
        $("#itemCode", clonedRow).empty().
            append($("<option></option>").
                attr("value", data.poPID).
                    text(data.poPC + ' - ' + data.poPN));
        $('#itemCode', clonedRow).val(data.poPID);
        $('#itemCode', clonedRow).data('PT', data.productType);
        $("#itemCode", clonedRow).data('pID', data.poPID);
        $('#itemCode', clonedRow).data('PC', data.poPC);
        $('#itemCode', clonedRow).data('PN', data.poPN);
        $('#itemCode', clonedRow).prop('disabled', true);
        $('#itemCode', clonedRow).selectpicker('render');
        $('#itemCode', clonedRow).attr('id', '');

        $('#qty', clonedRow).val(data.poPQ);
        $('#qty', clonedRow).addUom(locationproductsByPoDraft[data.poPID].uom);
        $('#qty', clonedRow).parent().addClass('input-group');
        $('#qty', clonedRow).attr('id', '');
        $('#addNewUomDiv', clonedRow).attr('id', '');
        $('#unitPrice', clonedRow).val(data.poPP);
        $('#unitPrice', clonedRow).addUomPrice(locationproductsByPoDraft[data.poPID].uom);
        $('#unitPrice', clonedRow).prop('disabled', true);
        $('#unitPrice', clonedRow).attr('id', '');
        if (data.poPD == null) {
            data.poPD = 0;
        }
        if (locationproductsByPoDraft[data.poPID]['dPEL'] == "1") {
            if (locationproductsByPoDraft[data.poPID]['pPDP'] != null) {
                $('#poDiscount', clonedRow).val(locationproductsByPoDraft[data.poPID]['pPDP']);
                $('#poDiscount', clonedRow).attr('id', '');
            } else {
                $('#poDiscount', clonedRow).val(locationproductsByPoDraft[data.poPID]['pPDV']);
                $('#poDiscount', clonedRow).attr('id', '');
            }
        } else {
            $('#poDiscount', clonedRow).prop('disabled', true);
        }
        $('#addNewTotal', clonedRow).html(toFloat(data.poPT).toFixed(2));
        $('#addNewTotal', clonedRow).attr('id', 'addNewTotal');
        $('#addNewUom', clonedRow).prop('disabled', true);
        $('#addNewUom', clonedRow).attr('id', '');
        $('#uomAb', clonedRow).html(data.poPUom);
        $('#uomAb', clonedRow).attr('id', '');
        $('#addUomUl', clonedRow).attr('id', '');
        $('#addItem', clonedRow).attr('id', 'addItemByDraft').addClass('poAddItem');
        $('#removeItem', clonedRow).attr('id', '').addClass('poRemoveItem').removeClass('hidden');
        //setting tax for products
        if (!jQuery.isEmptyObject(data.pT)) {
            var currentProductTax = data.pT;
            $('#addNewTaxDiv', clonedRow).attr('id', "poTaxDiv");
            $('#addNewTax', clonedRow).attr('id', "poAddTaxDiv");
            $('#taxApplied', clonedRow).removeClass('glyphicon glyphicon-unchecked');
            $('#taxApplied', clonedRow).addClass('glyphicon glyphicon-check');
            $('#taxApplied', clonedRow).attr('id', 'poTaxApplied')
            $('#addTaxUl', clonedRow).attr('id', 'poAddTaxUl');
            $('.tempLi', clonedRow).remove();
            for (var i in currentProductTax) {
                var clonedLi = $($('#sampleLi', clonedRow).clone()).removeClass('hidden').attr('id', 'li_' + i).addClass("poTempLi");
                clonedLi.children(".taxChecks").attr('id', data.poPID + '_' + i).prop('checked', true).addClass('addNewTaxCheck');
                clonedLi.children(".taxName").html('&nbsp&nbsp' + currentProductTax[i].pTN + '&nbsp&nbsp' + currentProductTax[i].pTP + '%').attr('for', data.poPID + '_' + i);
                clonedLi.insertBefore($('#sampleLi', clonedRow));
            }
            $('#sampleLi', clonedRow).attr('id', 'poSampleLi');
        } else {
            $('#addNewTaxDiv', clonedRow).attr('id', "poTaxDiv");
            $('#addNewTax', clonedRow).attr('id', "poAddTaxDiv");
            $('#taxApplied', clonedRow).attr('id', 'poTaxApplied');
            $('#addTaxUl', clonedRow).attr('id', 'poAddTaxUl');
            $('#sampleLi', clonedRow).remove();
            $('.tempLi', clonedRow).remove();
        }

        clonedRow.insertBefore('#add-new-po-record');
    }
    // this function use to load existing items to the po create screen
    function addNewProductRowByEdit(data, incID) {
        var productID = data.poPID;
        var poLocProID = data.lPID;
        var incrementID = incID;
        var newTrID = 'trE_'+poLocProID;
        var clonedRow = $($('#add-new-po-record').clone()).attr('id', newTrID).attr('data-id',productID).addClass('edit-mode');
        $('div.bootstrap-select', clonedRow).remove();
        $("#itemCode", clonedRow).empty().
            append($("<option></option>").
                attr("value", data.poPID).
                    text(data.poPC + ' - ' + data.poPN));
        $('#itemCode', clonedRow).val(data.poPID);
        $('#itemCode', clonedRow).data('PT', data.productType);
        $('#itemCode', clonedRow).data('PC', data.poPC);
        $('#itemCode', clonedRow).data('PN', data.poPN);
        $('#itemCode', clonedRow).prop('disabled', true);
        $('#itemCode', clonedRow).selectpicker('render');
        $('#itemCode', clonedRow).attr('id', '');

        $('#qty', clonedRow).val(data.poPQ);
        $('#qty', clonedRow).addUom(locationproductsByPoDraft[data.poPID].uom);
        $('#qty', clonedRow).parent().addClass('input-group');
        $('#qty', clonedRow).attr('id', '');
        $('.uomqty', clonedRow).prop('disabled', true);

        $('#addNewUomDiv', clonedRow).attr('id', '');
        $('#unitPrice', clonedRow).val(data.poPP);
        $('#unitPrice', clonedRow).addUomPrice(locationproductsByPoDraft[data.poPID].uom);
        $('#unitPrice', clonedRow).prop('disabled', true);
        $('#unitPrice', clonedRow).attr('id', '');
        $('.uomPrice', clonedRow).prop('disabled', true);
        if (data.poPD == null) {
            data.poPD = 0;
        }
        $('#poDiscount', clonedRow).val(data.poPD);
        
        // set discounts
        if (locationproductsByPoDraft[data.poPID].pPDV) {
            $(".sign", clonedRow).text('Rs');
            $('#poDiscount', clonedRow).attr('disabled', false);
        }
        if (locationproductsByPoDraft[data.poPID].pPDP) {
            $(".sign", clonedRow).text('%');
            $('#poDiscount', clonedRow).attr('disabled', false);
        }

        if (locationproductsByPoDraft[data.poPID].pPDP == null && locationproductsByPoDraft[data.poPID].pPDV == null || locationproductsByPoDraft[data.poPID].dPEL == 0) {
            $('#poDiscount', clonedRow).attr('readonly', true);
        }

        $('#poDiscount', clonedRow).addClass('po-disc');
        $('#poDiscount', clonedRow).attr('id', '');
        $('.po-disc', clonedRow).attr('disabled', true);
        $('.newTotalVal', clonedRow).html(toFloat(data.poPT).toFixed(2));
        $('#addNewTotal', clonedRow).attr('id', 'addedItem');
        $('#addNewUom', clonedRow).prop('disabled', true);
        $('#addNewUom', clonedRow).attr('id', '');
        $('#uomAb', clonedRow).html(data.poPUom);
        $('#uomAb', clonedRow).attr('id', '');
        $('#addUomUl', clonedRow).attr('id', '');
        $('#addItem', clonedRow).attr('id', '').addClass('po-item-add').parent().addClass('hidden');
         $('#editItem', clonedRow).attr('id', '').addClass('po-item-edit').parent().removeClass('hidden');
        $('#deleteItem', clonedRow).attr('id', '').addClass('po-item-delete').parent().removeClass('hidden');
        //setting tax for products
        var checkedTaxes = Array();
        if (!jQuery.isEmptyObject(data.pT)) {
            var currentProductTax = data.pT;
            $('#addNewTaxDiv', clonedRow).attr('id', "poTaxDiv");
            $('#addNewTax', clonedRow).attr('id', "poAddTaxDiv");
            $('#taxApplied', clonedRow).removeClass('glyphicon glyphicon-unchecked');
            $('#taxApplied', clonedRow).addClass('glyphicon glyphicon-check');
            $('#taxApplied', clonedRow).attr('id', 'poTaxApplied')
            $('#addTaxUl', clonedRow).attr('id', 'poAddTaxUl');
            $('.tempLi', clonedRow).remove();
            for (var i in currentProductTax) {
                var clonedLi = $($('#sampleLi', clonedRow).clone()).removeClass('hidden').attr('id', 'li_' + i).addClass("poTempLi");
                clonedLi.children(".taxChecks").attr('id', data.poPID + '_' + i).prop('checked', true).addClass('addNewTaxCheck');
                clonedLi.children(".taxName").html('&nbsp&nbsp' + currentProductTax[i].pTN + '&nbsp&nbsp' + currentProductTax[i].pTP + '%').attr('for', data.poPID + '_' + i);
                clonedLi.insertBefore($('#sampleLi', clonedRow));
                var tTaxID = i;
                checkedTaxes.push(tTaxID);
            }
            $('#sampleLi', clonedRow).attr('id', 'poSampleLi');
        } else {
            $('#addNewTaxDiv', clonedRow).attr('id', "poTaxDiv");
            $('#addNewTax', clonedRow).attr('id', "poAddTaxDiv");
            $('#taxApplied', clonedRow).attr('id', 'poTaxApplied');
            $('#addTaxUl', clonedRow).attr('id', 'poAddTaxUl');
            $('#sampleLi', clonedRow).remove();
            $('.tempLi', clonedRow).remove();
        }
        $('#poAddTaxDiv', clonedRow).prop('disabled', true);
        clonedRow.insertBefore('#add-new-po-record');
        
        var tmpItemCost = calculateTaxFreeItemCost(data.poPP, data.poPQ, data.poPD);
        currentItemTaxResults = calculateItemCustomTax(tmpItemCost, checkedTaxes);

        products[data.lPID] = new product(data.lPID, data.poPID, data.poPC, data.poPN, data.poPQ, data.poPD, data.poPP, data.poPUomID, data.poPT, currentItemTaxResults, null, null, incrementID);
        rowincrementID = incrementID + 1;
        setPoTotalCost();
        setTotalTax();
        poDrfatFlag = false;
        currentProductByDraft = null;
        poDraftUnitPrice = null;
        poDraftQty = null;
        poDraftTotal = null;

    }

    function addDocRef(key, data) {

        var $cloneRow = $($('.sample').clone()).removeClass('sample hidden').addClass('documentRow');

        //set document type
        $cloneRow.find('.documentTypeId').val(key);
        $cloneRow.find('.documentTypeId').selectpicker('render');
        $cloneRow.find('.documentTypeId').attr('disabled', true);
        documentTypeArray.push(key);

        loadDocumentId($cloneRow, key);

        selectedDocID = [];

        $cloneRow.find('select.documentId').append('<optgroup label="Currently Selected"></optgroup>');
        $.each(data,function(index, value){
            $cloneRow.find('optgroup').append('<option class="slc" value="'+value.id+'">'+value.code+'</option>');
            selectedDocID.push(value.id);
        });

        $cloneRow.find('select.documentId').selectpicker('val', selectedDocID);
        $cloneRow.find('select.documentId').selectpicker('render');
        $cloneRow.find('select.documentId').selectpicker('refresh');
        $cloneRow.find('select.documentId').attr('disabled', true);

        $cloneRow.find('.saveDocument').addClass('hidden');
        $cloneRow.find('.deleteDocument').removeClass('hidden');
        $( ".documentTypeBody" ).append($cloneRow);
    }

    $('#add-new-item-row').on('click', '.po-item-edit', function(e) {
        e.preventDefault();
        // give permission to edit price and quntity
        $(this).parents('tr').find('.uomqty').attr('disabled', false);
        $(this).parents('tr').find('.uomPrice').attr('disabled', false);
        $(this).parents('tr').find('#poAddTaxDiv').attr('disabled', false);
        $(this).parents('tr').find('.edit-po').addClass('hidden');
        $(this).parents('tr').find('.add-po').removeClass('hidden');
        $(this).parents('tr').find('.po-disc').attr('disabled', false);
        $(this).parents('tr').find('.po-disc').addClass('current-edit-disc');
        if (!editedFlag) {
            $(this).parents('tr').addClass('current-edited-line');
            $(this).parents('tr').find('.disct').addClass('disc-line');

        }
        if ($(this).parents('tr').hasClass('mrp')) {
            $(this).parents('tr').addClass('from-mrp-edit');
            $(this).parents('tr').removeClass('mrp');
        }
    });

    $('#add-new-item-row').on('click','.po-item-add', function(e) {
        e.preventDefault();
        
        if ($(this).parents('tr').find('.uomqty').val() == 0 || $(this).parents('tr').find('.uomqty').val() == 0) {
             p_notification(false, eb.getMessage('ERR_GRN_ENTER_QUANTITY'));
        } else {
            $(this).parents('tr').find('.uomqty').attr('disabled', true);
            $(this).parents('tr').find('.uomPrice').attr('disabled', true);
            $(this).parents('tr').find('#poAddTaxDiv').attr('disabled', true);
            $(this).parents('tr').find('.edit-po').removeClass('hidden');
            $(this).parents('tr').find('.add-po').addClass('hidden');
            $(this).parents('tr').find('.po-disc').attr('disabled', true);
            if (!editedFlag) {
                $(this).parents('tr').removeClass('current-edited-line');
                $(this).parents('tr').find('.disct').attr('disabled', true);
                $(this).parents('tr').find('.disct').removeClass('disc-line');
            }
            if ($(this).parents('tr').hasClass('from-mrp-edit')) {
                $(this).parents('tr').addClass('mrp');
                $(this).parents('tr').removeClass('from-mrp-edit');
            }
            poEditedTotal = $(this).parents('tr').find('.newTotalVal').html();
            var thisRowLocPro = $(this).parents('tr').attr('id');
            var realId = thisRowLocPro.split('_')[1];
            var crntQty = $(this).parents('tr').find('.uomqty').val();
            var crntPrice = $(this).parents('tr').find('.uomPrice').val();
            var crntDisc = $(this).parents('tr').find('.current-edit-disc').val();
            if (crntDisc == '') {
                crntDisc = 0;
            }
            $(this).parents('tr').find('.po-disc').removeClass('current-edit-disc');
            var checkedTaxes = Array();
            $.each($(this).parents('tr').find('input:checkbox.addNewTaxCheck'), function(){
                if (this.checked) {
                    var cliTID = this.id;
                    var tTaxID = cliTID.split('_')[1];
                    checkedTaxes.push(tTaxID);
                }
            });
            var tmpItemCost = calculateTaxFreeItemCost(crntPrice, crntQty, crntDisc);
            currentItemTaxResults = calculateItemCustomTax(tmpItemCost, checkedTaxes);
            var calNTotal = accounting.unformat(poEditedTotal);
            products[realId].pDiscount = crntDisc;
            products[realId].pQuantity = crntQty;
            products[realId].pUnitPrice = crntPrice;
            products[realId].pTotal = calNTotal;
            products[realId].pTax = currentItemTaxResults;

            setPoTotalCost();
            setTotalTax();
            poDrfatFlag = false;
            currentProductByDraft = null;
            poDraftUnitPrice = null;
            poDraftQty = null;
            poDraftTotal = null;
            
        }
    });

    $('#add-new-item-row').on('click','.po-item-delete',function(e) {
        e.preventDefault();
        var thisRowLocPro = $(this).parents('tr').attr('id');
        var realId = thisRowLocPro.split('_')[1];
        // products.splice(realId, 1);
        delete(products[realId]);
        $(this).parents('tr').remove();
        setPoTotalCost();
        setTotalTax();
    });

    ////////////////////////////////

    function clearProductScreen() {
        products = {};
        clearAddNewRow();
        setPoTotalCost();
        setTotalTax();
        $('.addedProducts').remove();
        locationProducts = '';
        locProductCodes = '';
        locProductNames = '';
        selectedProduct = '';
        selectedProductID = '';
        selectedProductQuantity = '';
        selectedProductUom = '';
        batchCount = '';
        currentProduct = '';
    }

    //po layout responsive changes
    $(window).bind('ready resize', function() {
        if ($(window).width() < 480) {
            $('.grn_button_grp').removeClass('btn-group');
        } else {
            $('.grn_button_grp').addClass('btn-group');
        }
    });





});

function clearAddNewRow() {
    $('#unitPrice').val('').siblings('.uomPrice,.uom-price-select').remove();
    $("#qty").val('').siblings('.uomqty,.uom-select').remove();
    $("#qty").show();
    $('#unitPrice').show();
    $('#poDiscount').val('').siblings('.uomPrice,.uom-price-select').remove();
    $('#poDiscount').show();
    $('#addNewTotal').html('0.00');
    $('#taxApplied').removeClass('glyphicon glyphicon-checked');
    $('#taxApplied').addClass('glyphicon glyphicon-unchecked')
    $('.tempLi').remove();
    $('.uomLi').remove();
    $('#uomAb').html('');
    selectedProduct = '';
    selectedProductID = '';
    selectedProductQuantity = '';
    selectedProductUom = '';
    currentProduct = '';
}

function getProductsFromLocation(locationID) {
    eb.ajax({
        type: 'POST',
        url: BASE_URL + '/api/purchase-requistion/getPoNoForLocation',
        data: {locationID: locationID},
        success: function(respond) {
            if (respond.status == false) {
                p_notification(false, respond.msg);
                $('#poNo').val('');
                $('#refNotSet').modal('show');
            }
            else if (respond.status == true) {
                $('#poNo').val(respond.data.refNo);
                $('#locRefID').val(respond.data.locRefID);
            }
        },
        async: false
    });
}

function setAddedSupplierToTheList(addedSupplier) {
    if (!jQuery.isEmptyObject(addedSupplier)) {
        var newSupplier = addedSupplier.supplierCode + '-' + addedSupplier.supplierName;
        $('#supplier').append($("<option value='" + addedSupplier.supplierID + "'>" + newSupplier + "</option>"));
        $('#supplier').selectpicker('refresh');
        $('#supplier').val(addedSupplier.supplierID);
        $('#supplier').selectpicker('render')
        selectedSupplier = addedSupplier.supplierID;
        $('#addSupplierModal').modal('hide');

    }
}

function setTaxListForProduct(currentProduct) {
    if ((!jQuery.isEmptyObject(currentProduct.tax))) {
        productTax = currentProduct.tax;
        $('#taxApplied').removeClass('glyphicon glyphicon-unchecked');
        $('#taxApplied').addClass('glyphicon glyphicon-check');
        $('.tempLi').remove();
        for (var i in productTax) {
            var clonedLi = $($('#sampleLi').clone()).removeClass('hidden').attr('id', 'li_' + i).addClass("tempLi");
            clonedLi.children(".taxChecks").attr('id', currentProduct.productID + '_' + i).prop('checked', true).addClass('addNewTaxCheck');
            if (productTax[i].tS == 0) {
                clonedLi.children(".taxChecks").attr('disabled', 'disabled');
                clonedLi.children(".taxName").addClass('crossText');
            }
            clonedLi.children(".taxName").html('&nbsp&nbsp' + productTax[i].tN + '&nbsp&nbsp' + productTax[i].tP + '%').attr('for', currentProduct.productID + '_' + i);
            clonedLi.insertBefore('#sampleLi');

        }
    } else {
        $('#taxApplied').addClass('glyphicon glyphicon-unchecked');
        $('#addNewTax').attr('disabled', 'disabled');
    }

}

function validateDiscount(productID, discountType, currentDiscount) {
    var defaultProductData = locationProductData[productID];
    var newDiscount = 0;
    if (defaultProductData.dPEL == 1) {
        if (discountType == 'Val') {
            if (toFloat(defaultProductData.pPDV) !=0 && toFloat(currentDiscount) > toFloat(defaultProductData.pPDV)) {
                    newDiscount = toFloat(defaultProductData.pPDV);
                    p_notification(false, eb.getMessage('ERR_INVO_DISCOUNT_VALUE'));
            } else if (toFloat(defaultProductData.pPDV) == 0) {
                if (toFloat(currentDiscount) > toFloat($('#itemUnitPrice').val())) {
                    newDiscount = toFloat($('#itemUnitPrice').val());
                } else {
                    newDiscount = toFloat(currentDiscount);    
                }
            } else {
                newDiscount = toFloat(currentDiscount);
            }

        } else {
            if (toFloat(defaultProductData.pPDP) != 0 && toFloat(currentDiscount) > toFloat(defaultProductData.pPDP)) {
                    newDiscount = toFloat(defaultProductData.pPDP);
                    p_notification(false, eb.getMessage('ERR_INVO_DISC_PERCENTAGE'));
            } else if (toFloat(defaultProductData.pPDP) == 0) {
                if (toFloat(currentDiscount) > 100 ) {
                    newDiscount = 100;
                    p_notification(false, eb.getMessage('ERR_PI_DISC_PERCENTAGE'));   
                } else {
                    newDiscount = toFloat(currentDiscount);    
                }
            } else {
                newDiscount = toFloat(currentDiscount);
            }
        }
    } 
    return newDiscount;
}
