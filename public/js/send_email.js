/**
 * @author Prathap Weerasinghe <prathap@thinckcube.com>
 * This js contain e-mail validation for email-form.phtml
 */

var validateEmail;
var showInvoicePreview;
var showQuotationPreview;
var showSalesOrderPreview;
var showDeliveryNotePreview;
var showCustomerPaymentsPreview;
var showReturnPreview;
var showPOPreview;
var showPVPreview;
var showExpPVPreview;
var showSupplierPaymentsPreview;
var showGrnPreview;
var showPurchaseReturnsPreview;


$(function () {
    var iframe = 'documentpreview';

    validateEmail = function (to, body) {

        var valid_email_body = true;
        if (body == "") {
            p_notification(false, eb.getMessage('ERR_SENDMAIL_EMPTY'));
            valid_email_body = false;
        }

        return eb.validateEmailAddr(to) && valid_email_body;

    }

//    $('#notification_body a.over_due_inv, a.over_due_purchase_inv').on('click', function(e) {
    $(document.body).on('click', '#notification_body a.over_due_inv, a.over_due_purchase_inv', function (e) {
        e.preventDefault();

        showInvoicePreview(BASE_URL + $(this).attr("href"));
        return false;

    });

    $("#invoice-list").parents('.margin_top').on("click", '.invoice-preview', function (e) {
        e.preventDefault();

        showInvoicePreview(BASE_URL + $(this).data("href"));

        return false;
    });


    $("#quotation-list").on("click", '.preview', function (e) {
        e.preventDefault();

        showQuotationPreview(BASE_URL + $(this).data("href"));

        return false;
    });

    $("#salesorder-list").on("click", '.preview', function (e) {
        e.preventDefault();

        showSalesOrderPreview(BASE_URL + $(this).data("href"), $(this).attr("data-soid"));

        return false;
    });

    $("#deliverynote-list").on("click", '.preview', function (e) {
        e.preventDefault();

        showDeliveryNotePreview(BASE_URL + $(this).data("href"));

        return false;
    });

    $("#paymentsview").on("click", '.preview', function (e) {
        e.preventDefault();

        showCustomerPaymentsPreview(BASE_URL + $(this).data("href"));

        return false;
    });

    $("#return-list").on("click", '.preview', function (e) {
        e.preventDefault();

        showReturnPreview(BASE_URL + $(this).data("href"));

        return false;
    });

    $("#poList").on("click", '.printViewPo', function (e) {
        e.preventDefault();

        showPOPreview(BASE_URL + $(this).data("href"));

        return false;
    });

    $("#piList").on("click", '.printViewPi', function (e) {
        e.preventDefault();

        showPVPreview(BASE_URL + $(this).data("href"));

        return false;
    });

    $("#petty-cash-voucher-list").on("click", '.printViewPi', function (e) {
        e.preventDefault();

        showPCVPreview(BASE_URL + $(this).data("href"));

        return false;
    });

    $("#expPVList").on("click", '.printViewPi', function (e) {
        e.preventDefault();

        showExpPVPreview(BASE_URL + $(this).data("href"));

        return false;
    });


    $("#suppPaymentsview").on("click", '.preview', function (e) {
        e.preventDefault();

        showSupplierPaymentsPreview(BASE_URL + $(this).data("href"));

        return false;
    });

    $("#grnList").on("click", '.printViewGrn', function (e) {
        e.preventDefault();

        showGrnPreview(BASE_URL + $(this).data("href"));

        return false;
    });

    $("#prList").on("click", '.printViewpr', function (e) {
        e.preventDefault();

        showPurchaseReturnsPreview(BASE_URL + $(this).data("href"));

        return false;
    });

    $("#credit-note-payment-list").on("click", '.preview', function (e) {
        e.preventDefault();

        showCreditNotePaymentPreview(BASE_URL + $(this).data("href"));

        return false;
    });


    $("#credit-note-list").on("click", '.preview', function (e) {
        e.preventDefault();

        showCreditNotePreview(BASE_URL + $(this).data("href"));

        return false;
    });

    $("#debit-note-list").on("click", '.preview', function (e) {
        e.preventDefault();

        showDebitNotePreview(BASE_URL + $(this).data("href"));

        return false;
    });

    $("#debit-note-payment-list").on("click", '.preview', function (e) {
        e.preventDefault();

        showDebitNotePaymentPreview(BASE_URL + $(this).data("href"));

        return false;
    });

    $("#job-list-view").on("click", '.preview', function (e) {
        e.preventDefault();

        showJobPreview(BASE_URL + $(this).data("href"));

        return false;
    });
    $("#adjustmentList").on("click", '.preview', function (e) {
        e.preventDefault();

        showAdjustmetPreview(BASE_URL + $(this).data("href"));

        return false;
    });
    $("#transfer-list").on("click", '.transfer-preview ', function (e) {
        e.preventDefault();

        showTransferPreview(BASE_URL + $(this).data("href"));

        return false;
    });
    $("#dispatch-note-list").on("click", '.preview', function (e) {
        e.preventDefault();

        showTransferPreview(BASE_URL + $(this).data("href"));

        return false;
    });

    showInvoicePreview = function (url) {
        var path = "/invoice-api/send-email";
        documentPreview(url, iframe, path, function ($preview) {
            $("button.invoice-history-close", $preview).on("click", function () {
                $("#inv_history", $preview).modal("hide");
            });

            if ($("#deliveryNoteSave").length && $('#currentLocation').length) { // mamke sure we are in Invoice form
                $preview.on('hidden.bs.modal', function () {

                    // since this event is being called when any modal is closed, even
                    // when the email modal is closed, this even gets triggered
                    // so check whether the main modal is visible
                    if (!$preview.is(':visible')) {
                        var pathName = window.location.pathname.split('/');
                        if (pathName.length == 4 && pathName[1] == "invoice" && pathName[2] == "create") {
                            window.location.assign(BASE_URL + "/invoice/create");
                        } else {
                            window.location.reload();
                        }
                    }
                });
            }
        });
    }



    showQuotationPreview = function (url) {

        var path = "/quotation-api/send-email";
        documentPreview(url, iframe, path);
    }

    showSalesOrderPreview = function (url) {

        var path = "/api/salesOrders/send-email";
        documentPreview(url, iframe, path);
    }

    showDeliveryNotePreview = function (url) {

        var path = "/delivery-note-api/send-email";
        documentPreview(url, iframe, path);
    }

    showCustomerPaymentsPreview = function (url) {

        var path = "/customerPaymentsAPI/send-email";
        documentPreview(url, iframe, path);
    }

    showReturnPreview = function (url) {

        var path = "/returnAPI/send-email";
        documentPreview(url, iframe, path);
    }

    showPOPreview = function (url) {

        var path = "/api/po/send-po-email";
        documentPreview(url, iframe, path);
    }
    showPVPreview = function (url) {

        var path = "/api/pi/send-pi-email";
        documentPreview(url, iframe, path);
    }
    showExpPVPreview = function (url) {

        var path = "/expense-purchase-invoice-api/send-pV-email";
        documentPreview(url, iframe, path);
    }
    showSupplierPaymentsPreview = function (url) {
        var path = "/supplierPaymentsAPI/send-supplier-payment-email";
        documentPreview(url, iframe, path);
    }
    showGrnPreview = function (url) {

        var path = "/api/grn/send-grn-email";
        documentPreview(url, iframe, path);
    }
    showPurchaseReturnsPreview = function (url) {

        var path = "/api/pr/send-pr-email";
        documentPreview(url, iframe, path);
    }
    showCreditNotePreview = function (url) {

        var path = "/credit-note-api/send-email";
        documentPreview(url, iframe, path);
    }
    showCreditNotePaymentPreview = function (url) {

        var path = "/credit-note-payments-api/send-email";
        documentPreview(url, iframe, path);
    }
    showDebitNotePreview = function (url) {

        var path = "/debit-note-api/send-email";
        documentPreview(url, iframe, path);
    }

    showDebitNotePaymentPreview = function (url) {

        var path = "/debit-note-payments-api/send-email";
        documentPreview(url, iframe, path);
    }

    showJobPreview = function (url) {

        var path = "/job-api/sendEmail";
        documentPreview(url, iframe, path);
    }
//use to get inventory adjustment print view.
    showAdjustmetPreview = function (url) {
        var path = "/api/inventory-adjustment/send-email";
        documentPreview(url, iframe, path);
    }

    showTransferPreview = function (url) {
        var path = "/transferAPI/send-email";
        documentPreview(url, iframe, path);
    }

    showDispatchNotePreview = function (url) {
        var path = "/dispatch-api/send-email";
        documentPreview(url, iframe, path);
    }

    showPCVPreview = function (url) {
        var path = "/api/petty-cash-voucher/send-petty-cash-voucher-email";
        documentPreview(url, iframe, path);
    };
});

