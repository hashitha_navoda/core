/**
 * @author Damith Thamara <damith@thinkcube.com>
 * This javascript contains GRN related functions
 */
var locationProducts;
var locProductCodes;
var locProductNames;
var selectedProduct;
var selectedSupplier;
var selectedProductQuantity;
var selectedProductUom;
var batchCount = 0;
var rowCount = 0;
var currentlySelectedProduct;
var batchFlag = false;
var serialFlag = false;
var batchItemIncre = 0;
var batchSerialFlag = false;
var ignoreBudgetLimitFlag = false;
var productLineTotal;
var documentTypeArray = [];
var dimensionData = {};
var uploadedAttachments = {};
var deletedAttachmentIds = [];
var deletedAttachmentIdArray = [];

$(document).ready(function() {
    var selectedLocation;
    var selectedPO;
    var decimalPoints = 0;
    var currentItemTaxResults;
    var currentTaxAmount;
    var products = new Array();
    var batchProducts = {};
    var serialProducts = {};
    var newSerialProducts = {};
    var totalTaxList = {};
    var locRefID;
    var thisIsPoAddFlag = false;
    var compoundCostAddFlag = false;
    var poProducts = {};
    var addingPoPro;
    var idNumber = 1;
    var quon = 0;
    var dataType;
    var productDelimiter;
    var batchSerialType ;
    var locationIDForUpload;
    var productHeader = false;
    var uploadDocumentFlag = false;
    var machineInputFlag = false;
    var onlySerialFlag = false;
    var uomMapArray = [];
    var poIdsDataSet = [];
    var discTypeArray = [];
    var serialNumberArray = {};
    var fromCompoundCostingFlag = false;
    var addedProForCost;
    var rowNumber = -1;
    var dimensionArray = {};
    var dimensionTypeID = null;

    function product(lPID, pID, pCode, pN, pQ, pD, pUP, pUom, pTotal, pTax, bProducts, sProducts, productType, pDType, poID = null, newTrID = null, grnProductID = null, isEdit = false) {
        this.locationPID = lPID;
        this.pID = pID;
        this.pCode = pCode;
        this.pName = pN;
        this.pQuantity = pQ;
        this.pDiscount = pD;
        this.pUnitPrice = pUP;
        this.pUom = pUom;
        this.pTotal = pTotal;
        this.pTax = pTax;
        this.bProducts = bProducts;
        this.sProducts = sProducts;
        this.productType = productType;
        this.pDType = pDType;
        this.poID = poID;
        this.newTrID = newTrID;
        this.grnProductID = grnProductID;
        this.isEdit = isEdit;
    }

    function batchProduct(bCode, bQty, mDate, eDate, warnty, price, bRow) {
        this.bCode = bCode;
        this.bQty = bQty;
        this.mDate = mDate;
        this.eDate = eDate;
        this.warnty = warnty;
        this.price = price;
        this.bRow = bRow;
    }

    function serialProduct(sCode, sWarranty, sBCode, sEdate , sWarrantyType) {
        this.sCode = sCode;
        this.sWarranty = sWarranty;
        this.sBCode = sBCode;
        this.sEdate = sEdate;
        this.sWarrantyType = sWarrantyType;

    }
    
    $('#dimensionView').on('click', function(e) {
        clearDimensionModal();
        var grnNo = $('#grnNo').val();

        if (!$.isEmptyObject(dimensionData)) {
            $.each(dimensionData[grnNo], function(index, val) {
                dimensionArray[index] = val;                
            });
            $.each(dimensionArray, function(index, value) {
                var newTrID = 'tr_' + value.dimensionTypeId;
                var clonedRow = $($('#dimensionPreSample').clone()).attr('id', newTrID).addClass('addedDimensions');
                $("input[name='dimensionType']", clonedRow).val(value.dimensionTypeTxt);
                $("input[name='dimensionValue']", clonedRow).val(value.dimensionValueTxt);
                clonedRow.removeClass('hidden');
                clonedRow.insertBefore('#dimensionPreSample');
                clearAddedNewDimensionRow();
            });
        }
        $('#addDimensionModal').modal('show');
    });

    // dimension modal
    function clearDimensionModal()
    {
        $("#addDimensionModal #dimensionTable tbody .addedDimensions").remove();
        dimensionArray = {};
    }


    $('.dimenisonchange').on('change', function(e) {
        dimensionTypeID = this.value;
        getDimensionValueListByDimensionType(dimensionTypeID, function() {
        });
    });



    function getDimensionValueListByDimensionType(dimensionTypeID, callback) {
        switch (dimensionTypeID) {
            case "job":
                $('.dimensionVal').addClass('hidden');
                $('.dimensionProjectNo').addClass('hidden');
                $('.dimensionJobNo').removeClass('hidden');
                break;
            case "project":
                $('.dimensionVal').addClass('hidden');
                $('.dimensionProjectNo').removeClass('hidden');
                $('.dimensionJobNo').addClass('hidden');
                break;
           default:
                $('.dimensionVal').removeClass('hidden');
                $('.dimensionProjectNo').addClass('hidden');
                $('.dimensionJobNo').addClass('hidden');
                $.ajax({
                    type: 'POST',
                    url: BASE_URL + '/dimension-api/getRelatedDimensionValues',
                    data: {dimensionId: dimensionTypeID},
                    success: function(respond) {
                        //empty options
                        $('#dimensionVal').empty();
                        $('#dimensionVal').append($("<option>", {value: '', html: 'Select dimension value'}));
                        $(respond.data).each(function(index, dimValue) {
                            $.each(dimValue, function(ind, val) {
                                $('#dimensionVal').append($("<option>", {value: ind, html: val.value}));
                            });
                        });
                        callback();
                    }
                });
                break;
        }
    }

    $('#add_dimension').on('click', function(e) {
        e.preventDefault();
        //if plus button is clicked then disabled the button
        //if button is clicked more than one, then it does not continue
        if ($(this).is(':disabled')) {
            return false;
        } else {
            $(this).attr('disabled', true);
            var $thisRow = $(this).parents('tr');
            if ($('.dimenisonchange').val() == 0 || $('.dimenisonchange').val() == '' || $('.dimenisonchange').val() == null) {
                p_notification(false, eb.getMessage('ERR_DIMENSION_SELCET'));
                $(this).attr('disabled', false);
            } else {
                if ($('.dimenisonchange').val() != 'job' && $('.dimenisonchange').val() != 'project') {
                    if ($('.dimensionVal').val() == 0 || $('.dimensionVal').val() == '' || $('.dimensionVal').val() == null) {
                        p_notification(false, eb.getMessage('ERR_DIMENSION_VALUE_SELCET'));
                        $(this).attr('disabled', false);
                        return;
                    }
                }

                if ($('.dimenisonchange').val() == 'project') {
                    if ($('.dimensionProjectNo').val() == 0 || $('.dimensionProjectNo').val() == '' || $('.dimensionProjectNo').val() == null) {
                        p_notification(false, eb.getMessage('ERR_DIMENSION_VALUE_SELCET'));
                        $(this).attr('disabled', false);
                        return;
                    }
                }

                if ($('.dimenisonchange').val() == 'job') {
                    if ($('.dimensionJobNo').val() == 0 || $('.dimensionJobNo').val() == '' || $('.dimensionJobNo').val() == null) {
                        p_notification(false, eb.getMessage('ERR_DIMENSION_VALUE_SELCET'));
                        $(this).attr('disabled', false);
                        return;
                    }
                }

               
                if (dimensionArray[dimensionTypeID]) {
                    p_notification(false, eb.getMessage('ERR_DIMENSION_ALREADY_INSERTED'));
                    $('.dimenisonchange').val('');
                    $('.dimenisonchange').selectpicker('render');
                    $('.dimenisonchange').trigger('change');
                } else {
                    addNewDimensionRow($(this).parents('tr'));
                }
                $(this).attr('disabled', false);
            }
        }
    });

    function addNewDimensionRow(thisRow) {

        var dimensionTypeId = $('.dimenisonchange', thisRow).val();
        var dimensionTypeTxt = $('.dimenisonchange', thisRow).find("option:selected").text();

        switch (dimensionTypeID) {
            case "job":
                var dimensionValueTxt = $('.dimensionJobNo', thisRow).find("option:selected").text();
                var dimensionValueId = $('.dimensionJobNo', thisRow).val();
                break;
            case "project":
                var dimensionValueTxt = $('.dimensionProjectNo', thisRow).find("option:selected").text();
                var dimensionValueId = $('.dimensionProjectNo', thisRow).val();
                break;
           default:
                var dimensionValueTxt = $('.dimensionVal', thisRow).find("option:selected").text();
                var dimensionValueId = $('.dimensionVal', thisRow).val();
                break;
        }
       
        var dimensionDuplicateCheck = false;
        if (!$.isEmptyObject(dimensionArray)) {
            $.each(dimensionArray, function(index, value) {
                if (value.dimensionTypeId == dimensionTypeId) {
                    dimensionDuplicateCheck = true;
                }
            });   
        }
       
        if (dimensionDuplicateCheck) {
            p_notification(false, eb.getMessage("ERR_DIMENSION_ALREADY_INSERTED"));
            return false;
        }

        var newTrID = 'tr_' + dimensionTypeId;
        var clonedRow = $($('#dimensionPreSample').clone()).attr('id', newTrID).addClass('addedDimensions');
        $("input[name='dimensionType']", clonedRow).val(dimensionTypeTxt);
        $("input[name='dimensionValue']", clonedRow).val(dimensionValueTxt);
        clonedRow.removeClass('hidden');
        clonedRow.insertBefore('#dimensionPreSample');
        dimensionArray[dimensionTypeId] = new dimension(dimensionTypeId, dimensionValueId,dimensionTypeTxt , dimensionValueTxt);
        clearAddedNewDimensionRow();
    }


    function dimension(dimensionTypeId, dimensionValueId, dimensionTypeTxt, dimensionValueTxt) {
        this.dimensionTypeId = dimensionTypeId;
        this.dimensionValueId = dimensionValueId;
        this.dimensionTypeTxt = dimensionTypeTxt;
        this.dimensionValueTxt = dimensionValueTxt;
    }

    function clearAddedNewDimensionRow() {
        $('.dimenisonchange').val('');
        $('.dimenisonchange').selectpicker('render');
        $('.dimenisonchange').trigger('change');
    }

    $('.addDimensions').on('click', function(e) {
        e.preventDefault();

        // validate products before closing modal
        if (!dimensionModalValidate(e)) {
            e.stopPropagation();
            return false;
        } else {
            $('#addDimensionModal').modal('hide');
        }
    });

    function dimensionModalValidate(e) {
        var grnNo = $('#grnNo').val();
        if ($.isEmptyObject(dimensionArray)) {
            dimensionData = {};
            return true;   
        } else {
            var dimensionSingleData = {};
            $.each(dimensionArray, function(index, val) {
                dimensionSingleData[index] = val;
            });
            dimensionData[grnNo] = dimensionSingleData;
            return true;
        }
    }

    $("#addDimensionModal #dimensionTable").on('click', '.deleteDimensionType', function(e) {
        e.preventDefault();
        var deleteDTrID = $(this).closest('tr').attr('id');
        var deleteDID = deleteDTrID.split('tr_')[1].trim();
        bootbox.confirm('Are you sure you want to remove this dimension ?', function(result) {
            if (result == true) {
                delete dimensionArray[deleteDID];
                $('#' + deleteDTrID).remove();
            }
        });
    });

    loadDropDownFromDatabase('/job-api/searchJobsForDropdown', "", 0, '.dimensionJobNo');
    $('.dimensionJobNo').selectpicker('refresh');
    $('.dimensionJobNo').on('change', function() {
        var dimensionValue = $(this).val();
    });

    loadDropDownFromDatabase('/api/project/searchProjectsForDropdown', "", 0, '.dimensionProjectNo');
    $('.dimensionProjectNo').selectpicker('refresh');
    $('.dimensionProjectNo').on('change', function() {
        var dimensionValue = $(this).val();
    });

    $('#viewUploadedFiles').on('click', function(e) {
        $('#doc-attach-table tbody tr').remove();
        $('#doc-attach-table tfoot div').remove();
        $('#doc-attach-table tbody div').remove();

        if (!$.isEmptyObject(uploadedAttachments)) {
            $('#doc-attach-table thead tr').removeClass('hidden');
            $.each(uploadedAttachments, function(index, value) {
                if (!deletedAttachmentIds.includes(value['documentAttachemntMapID'])) {
                    tableBody = "<tr id="+value['documentAttachemntMapID']+" data-attachId=" + value['documentAttachemntMapID'] + "><td><a href="+value.docLink+">"+value.documentRealName+"</a></td><td class='text-center'><span class='glyphicon glyphicon-trash delete_attachment' style='cursor:pointer'></span></td></tr>";
                    $('#doc-attach-table tbody').append(tableBody);
                }
            });
        } else {
            $('#doc-attach-table thead tr').addClass('hidden');
            var noDataFooter = "<div class= ' col-lg-6 text-right'><label><b>No Data</b></label>";
            $('#doc-attach-table tbody').append(noDataFooter);
        }
        $('#attach_view_footer').removeClass('hidden');
        $('#viewAttachmentModal').modal('show');
    });

    $('#doc-attach-table').on('click', '.delete_attachment', function() {
        var attachementID = $(this).parents('tr').attr('data-attachid');
        bootbox.confirm('Are you sure you want to remove this attachemnet?', function(result) {
            if (result == true) {
                $('#' + attachementID).remove();
                deletedAttachmentIdArray.push(attachementID);
            }
        });
    });

    $('#viewAttachmentModal').on('click', '#saveAttachmentEdit', function(event) {
        event.preventDefault();

        $.each(deletedAttachmentIdArray, function(index, val) {
            deletedAttachmentIds.push(val);
        });

        $('#viewAttachmentModal').modal('hide');
    });

    //system currency symbol
    var currencySymbol  = $('#supplierReference').data('curencysymbol');

    loadDropDownFromDatabase('/api/po/search-po-for-dropdown', "", 0, '#startByCode');
    $('#startByCode').trigger('change');
    $('#startByCode').on('change', function() {
        if ($(this).val() > 0 && selectedPO != $(this).val()) {
            selectedPO = $(this).val();
            $('#startByCode').prop('disabled', true);
            loadPO(selectedPO);
        }
    });

    loadDropDownFromDatabase('/supplierAPI/search-suppliers-for-dropdown', "", 0, '#supplier');
    $('#supplier').trigger('change');
    $('#supplier').on('change', function() {
        if ($(this).val() > 0) {
            selectedSupplier = $(this).val();
        }else if ($(this).val() == 0) {
            selectedSupplier = $(this).val();
        }
    });

    // check from Compound Costing
    var compoundCostID = $('#comment').attr('data-id');
    if (compoundCostID != '') {
        loadDataFromCompoundCost(compoundCostID);
    }



    // for update operations
    var editModeFlag = false;

    if($('#editMode').val() == '1'){
        var grnID = $('#editMode').attr('data-grnid');
        $('#grnSave').addClass('hidden');
        $('#approvalWorkflowSelector').addClass('hidden');
        $('#grnEdit').removeClass('hidden');
        loadGrnForUpdate(grnID);
        $('.grnEditProductTd').removeClass('hidden');
        editModeFlag = true;
    }

    if (editModeFlag) {
        $('#startByCode').prop('disabled', true);
    }
    $.each(LOCATION, function(index, value) {
        $('#retrieveLocation').append($("<option value='" + index + "'>" + value + "</option>"));
    });

    $('#retrieveLocation').selectpicker('refresh');

    $('#retrieveLocation').on('change', function() {
        setDataForLocation($(this).val());
        if ($(this).val() == '') {
            selectedLocation = ''
            clearProductScreen();
            $('#productAddOverlay').addClass('overlay');
            $('#grnNo').val('');
            $('#locRefID').val('');
        }
    });

    var currentLocation = $('#navBarSelectedLocation').attr('data-locationid');
    if (!$('#retrieveLocation').prop('disabled')) {
        $('#retrieveLocation').val(currentLocation).change();
    }

    $('#documentReference').on('click', function(e) {
        e.preventDefault();
        if ($('#retrieveLocation').val()) {
            $('#documentReferenceModal').modal('show');
            $('#retrieveLocation').attr('disabled', 'disabled');
        } else {
            $('#documentReferenceModal').modal('hide');
            p_notification(false, eb.getMessage('ERR_PLEASE_SELECT_LOCTION_FOR_GRN'));
        }
    });

    $('#itemUploadView').on('click', function(e){
        e.preventDefault();
        if(!$('#retrieveLocation').val()){
            $('#itemImportModal').modal('hide');
            p_notification(false, eb.getMessage('ERR_GRN_SELECT_LOCAT'));
        } else if(!$('#deliveryDate').val()) {
            $('#itemImportModal').modal('hide');
            p_notification(false, eb.getMessage('ERR_GRN_SELECT_DELIDATE'));
        } else {
            $('#itemImportModal').modal('show');
            $('#retrieveLocation').attr('disabled', 'disabled');
            locationIDForUpload = $('#retrieveLocation').val();
        }

    });


    $('#documentTable').on('change', '.documentTypeId', function() {
        var $dropDownIDParent = $(this).parents('tr');
        var documentTypeID = $(this).val();
        loadDocumentId($dropDownIDParent, documentTypeID);
    });

    function loadDocumentId($dropDownIDParent, documentTypeID) {
        var dropDownID = '.documentId';
        var locationID = $('#retrieveLocation').val();
        var url = '';
        if (documentTypeID == 1) {
            url = '/invoice-api/searchSalesInvoicesForDocumentDropdown';
        } else if (documentTypeID == 2) {
            url = '/quotation-api/searchAllQuotationForDropdown';
        } else if (documentTypeID == 3) {
            url = '/api/salesOrders/search-sales-orders-for-dropdown';
        } else if (documentTypeID == 4) {
            url = '/delivery-note-api/searchDeliveryNoteForDocumentDropdown';
        } else if (documentTypeID == 7) {
            url = '/customerPaymentsAPI/searchPaymentsForDocumentDropdown';
        } else if (documentTypeID == 9) {
            url = '/api/po/search-all-open-po-for-dropdown';
        } else if (documentTypeID == 10) {
            url = '/api/grn/search-all-grn-for-dropdown';
        } else if (documentTypeID == 12) {
            url = '/api/pi/search-allPV-for-dropdown';
        } else if (documentTypeID == 15) {
            url = '/transferAPI/search-transfer-for-dropdown';
        } else if (documentTypeID == 19) {
            url = '/job-api/searchAllJobsForDropdown';
        } else if (documentTypeID == 20) {
            url = '/expense-purchase-invoice-api/getPaymentVoucherForDropDown';
        } else if (documentTypeID == 22) {
            url = '/project-api/searchProjectsForDropdown';
        } else if (documentTypeID == 23) {
            url = '/api/activity/get-all-activities-for-dropdown';
        } else if (documentTypeID == 11) {
            url = '/api/pr/searchPurchaseReturnsForDropdownByPRCode';
        }
        $('div.bootstrap-select.documentId', $dropDownIDParent).remove();
        $('select.selectpicker.documentId', $dropDownIDParent).show().removeData().empty();
        loadDropDownFromDatabase(url, locationID, 0, dropDownID, $dropDownIDParent);
    }

    function updateDocTypeDropDown() {
        $allRow = $('tr.documentRow.editable');
        $allRow.find('option').removeAttr('disabled')
        $.each(documentTypeArray, function(index, value) {
            $allRow.find(".documentTypeId option[value='" + value + "']").attr('disabled', 'disabled');
        });
        $allRow.find('.documentTypeId').selectpicker('render');
        $allRow.find('.documentTypeId').selectpicker('refresh');
    }

    $('#documentTable').on('click', '.saveDocument', function(e) {
        e.preventDefault();
        var $row = $(this).parents('tr');
        if (validateDocumentTypeRow($row)) {
            $row.removeClass('editable');
            documentTypeArray.push($row.find('.documentTypeId').val());
            $row.find('.documentTypeId').attr('disabled', true);
            $row.find('.documentId').attr('disabled', true);
            $row.find('.saveDocument').addClass('hidden');
            $row.find('.deleteDocument').removeClass('hidden');
            var $cloneRow = $($('.sample').clone()).removeClass('sample hidden').addClass('documentRow editable');

            $cloneRow.find('.documentTypeId').selectpicker().attr('data-liver-search', true);
            $cloneRow.insertBefore($row);

            updateDocTypeDropDown();
        }
    });

    $('#documentTable').on('click', '.deleteDocument', function(e) {
        e.preventDefault();
        var $row = $(this).parents('tr');
        var documentTypeID = $row.find('.documentTypeId').val();
        //this is for remove documentTypeID from the documentType array
        documentTypeArray = jQuery.grep(documentTypeArray, function(value) {
            return value != documentTypeID;
        });
        $row.remove();
        updateDocTypeDropDown();
    });

    $('#documentTable').on('click', '.editDocument', function(e) {
        e.preventDefault();
        var $row = $(this).parents('tr');
        $row.find('.documentId').attr('disabled', false);
        $row.find('.updateDocument').removeClass('hidden');
        $row.find('.editDocument').addClass('hidden');
    });

    $('#documentTable').on('click', '.updateDocument', function(e) {
        e.preventDefault();
        var $row = $(this).parents('tr');
        if (validateDocumentTypeRow($row)) {
            $row.find('.documentTypeId').attr('disabled', true);
            $row.find('.documentId').attr('disabled', true);
            $row.find('.updateDocument').addClass('hidden');
            $row.find('.editDocument').removeClass('hidden');
        }
    });

    function validateDocumentTypeRow($row) {
        var flag = true;
        var documentTypeID = $row.find('.documentTypeId').val();
        var documentID = $row.find('.documentId').val();
        if (documentTypeID == '' || documentTypeID == null) {
            p_notification(false, eb.getMessage('ERR_PO_DOC_TYPE_SELECT'));
            flag = false;
        } else if (documentID == '' || documentID == null) {
            p_notification(false, eb.getMessage('ERR_PO_DOC_ID_SELECT'));
            flag = false;
        }
        return flag;
    }

    $('#itemCode').on('change', function() {
        clearAddNewRow();
        clearModalWindow();
        if ($(this).val() > 0 && $(this).val() != currentlySelectedProduct) {
            currentlySelectedProduct = $(this).val();
            selectedProduct = $(this).val();
            eb.ajax({
                type: 'POST',
                url: BASE_URL + '/productAPI/get-location-product-details',
                data: {productID: selectedProduct, locationID: selectedLocation},
                success: function(respond) {
                    var currentElem = new Array();
                    currentElem[selectedProduct] = respond.data;
                    locationProducts = $.extend(locationProducts, currentElem);

                    var baseUom;
                    for (var j in locationProducts[selectedProduct].uom) {
                        if (locationProducts[selectedProduct].uom[j].pUBase == 1) {
                            baseUom = locationProducts[selectedProduct].uom[j].uomID;
                        }
                    }


                    $('#addNewTax').attr('disabled', false);
                    setTaxListForProduct(selectedProduct);
                    $('#unitPrice').parent().addClass('input-group');
                    $('#unitPrice').val(locationProducts[selectedProduct].dPP).addUomPrice(locationProducts[selectedProduct].uom);
                    $('#qty').parent().addClass('input-group');
                    $('#qty').addUom(locationProducts[selectedProduct].uom);
                    $('.uomqty').attr("id", "itemQuantity");
                    $('#itemCode').data('PT', locationProducts[selectedProduct].pT);
                    $('#itemCode').data('PN', locationProducts[selectedProduct].pN);
                    $('#itemCode').data('pID', locationProducts[selectedProduct].pID);
                    if (locationProducts[selectedProduct].pPDP != null && locationProducts[selectedProduct].dPEL == 1) {
                        $('#grnDiscount').val(parseFloat(locationProducts[selectedProduct].pPDP).toFixed(2));
                        $('#grnDiscount').addClass('precentage');
                        $('#grnDiscount').removeClass('value');
                        $(".sign").text('%');
                        $('#grnDiscount').attr('readonly',false);
                    } else if (locationProducts[selectedProduct].pPDV != null && locationProducts[selectedProduct].dPEL == 1){
                        $('#grnDiscount').val(parseFloat(locationProducts[selectedProduct].pPDV).toFixed(2)).addUomPrice(locationProducts[selectedProduct].uom, baseUom);
                        $('#grnDiscount').siblings('.uom-price-select').find('.uom_price_drop_down').attr('disabled', true);
                        $('#grnDiscount').addClass('value');
                        $('#grnDiscount').removeClass('precentage');
                        $('.sign').text(currencySymbol);
                        $('#grnDiscount').attr('readonly',false);
                    } else {
                        $('#grnDiscount').attr('readonly',true);
                        $(".sign").text('%');
                        $('#grnDiscount').removeClass('value');
                        $('#grnDiscount').addClass('precentage');

                    }
                }
            });


        }
    });

    $('#refreshStartBy').on('click', function() {
        selectedPO = '';
        $('#itemCode').empty().val('').selectpicker('refresh');
        $('#supplier').empty().selectpicker('refresh');
        $('#supplier').prop('disabled', false).selectpicker('refresh');
        $('#startByCode').empty().selectpicker('refresh');
        $('#startByCode').prop('disabled', false).selectpicker('refresh');
        $('#deliveryDate').prop('disabled', false);
        $('#deliveryDate').val('');
        $('#supplierReference').prop('disabled', false);
        $('#supplierReference').val('');
        $('#comment').prop('disabled', false);
        $('#comment').val('');
        $('#retrieveLocation').prop('disabled', false);
        $('#retrieveLocation').val('').selectpicker('render');
        $('#grnNo').val('');
        $('.tempPoPro').remove();
        selectedSupplier = '';
        selectedLocation = '';
        $('#deliveryCharge').val('');
        $('.deliCharges').addClass('hidden');
        $('#deliveryChargeEnable').prop('checked', false);
        clearProductScreen();
        $('#productAddOverlay').addClass('overlay');
        thisIsPoAddFlag = false;
        compoundCostAddFlag = false;
        poProducts = {};
        addingPoPro = '';
        $('#addSupplierBtn').prop('disabled', false);
        $('#addSupplierBtn').unbind('click');
    });
    $('#addItem').on('click', function() {
        var row = $(this).parents('tr');
        var uomqty = $("#qty").siblings('.uomqty').val();
        $("#qty").siblings('.uomqty').change();
        $("#unitPrice").siblings('.uomPrice').change();
        $('.tempy').remove();
        serialDataSet = [];
        machineInput = false;
        var sameItemFlag = false;
        for (var i in products) {
            if(selectedProduct == products[i].pID && toFloat($('#unitPrice').val()) == toFloat(products[i].pUnitPrice)){
                sameItemFlag = true;
            }
        }


        if(sameItemFlag){
            p_notification(false, eb.getMessage('ERR_SAME_ITEM_IN_GRN'));
            return false;
        }


        if (locationProducts[selectedProduct].pPDP == null && locationProducts[selectedProduct].pPDV != null) {
            if (toFloat($('#grnDiscount').val()) > toFloat($('#unitPrice').val())) {
                p_notification(false, eb.getMessage('ERR_GRN_DISC_VAL'));
                return false;
            }
        }

//        $('#serialAddTable').addClass('hidden');
        idNumber = 1;
        quon = 0;
        $('#prefix').val('');

        var dDate = new Date(Date.parse($('#deliveryDate').val()));
        var epd = $('#expire_date_autoFill').val('').datepicker({
            onRender: function(date) {
                return date.valueOf() < dDate.valueOf() ? '' : '';
            }
        }).on('changeDate', function(ev) {
            epd.hide();
        }).data('datepicker');
        $('#quontity').val('');
        $('#startNumber').val('');
        $('#wrtyDays').val('');
        $('#wrtyType').val('');
        $('#batch_code').val('');
        $('#bchPrice').val('');
        var mfd = $('#mf_date_autoFill').val('').datepicker({
            onRender: function(date) {
                return date.valueOf() > dDate.valueOf() ? '' : '';
            }
        }).on('changeDate', function(ev) {
            mfd.hide();
        }).data('datepicker');

        var dExpDate = $('#defaultSerialEDate').val('').datepicker({
            onRender: function(date) {
                return date.valueOf() > dDate.valueOf() ? '' : '';
            }
        }).on('changeDate', function(ev) {
            dExpDate.hide();
        }).data('datepicker');

        $('#temp_1').find('input[name=submit]').attr('disabled', false);
        $('#temp_1').find('input[name=prefix]').attr('disabled', false);
        $('#temp_1').find('input[name=startNumber]').attr('disabled', false);
        $('#temp_1').find('input[name=quontity]').attr('disabled', false);
        $('#temp_1').find('input[name=expireDateAutoFill]').attr('disabled', false);
        $('#temp_1').find('input[name=batch_code]').attr('disabled', false);
        $('#temp_1').find('input[name=mfDateAutoFill]').attr('disabled', false);
        $('#temp_1').find('input[name=wrtyDays]').attr('disabled', false);
        $('#temp_1').find('input[name=wrtyType]').attr('disabled', false);
        $('#temp_1').find('input[name=bPrice]').attr('disabled', false);
        selectedProductUom = $("#qty").siblings('.uom-select').children('button').find('.selected').data('uomID');
        if (($('#itemCode').val() == 'undefined') || ($('#itemCode').val() == '')) {
            p_notification(false, eb.getMessage('ERR_GRN_SELECT_PROD'));
            return false;
        // } else if (products[locationProducts[selectedProduct].lPID]) {
        //     p_notification(false, eb.getMessage('ERR_GRN_INSERT_PROD'));
        //     return false;
        } else if (typeof (selectedProductUom) == 'undefined' || selectedProductUom == '') {
            p_notification(false, eb.getMessage('ERR_GRN_SELECT_UOM'));
            return false;
        } else {
            if ($('#itemCode').data('PT') != 2 && ($('#qty').val() == '' || $('#qty').val() == 0)) {
                p_notification(false, eb.getMessage('ERR_GRN_ENTER_QUANTITY'));
                return false;
            } else if ((uomqty == 0) || (uomqty == '')) {
                p_notification(false, eb.getMessage('ERR_GRN_ENTER_QUANTITY'));
                return false;
            } else if ($('#unitPrice').val() == '') {
                p_notification(false, eb.getMessage('ERR_GRN_ENTER_UNITPRI'));
                $('#unitPrice', row).focus();

            } else if ($('#deliveryDate').val() == '') {
                p_notification(false, eb.getMessage('ERR_GRN_SELECT_DELIDATE'));
                return false;
            } else {
                clearModalWindow();
                $('.cloneSerials').remove();
                $('#serialSample').children().children('#newSerialNumber').removeClass('serialNumberList');
                $('#serialSample').children().children('#newSerialBatch').prop('disabled', false);

                $('#unitPrice').trigger('focusout');

                if ($('#itemCode').data('PT') == 2) {
                    locationProducts[selectedProduct].bP = 0
                    locationProducts[selectedProduct].sP = 0
                }

                if (locationProducts[selectedProduct].bP == 1) {
                    $('#batchAddScreen').removeClass('hidden');
                    $('#newBatchMDate').addClass('white_bg');
                    $('#newBatchEDate').addClass('white_bg');
                    batchFlag = true;
                }
                if (locationProducts[selectedProduct].sP == 1) {
                    $('#serialAddScreen').removeClass('hidden');
                    serialFlag = true;
                }
                if (locationProducts[selectedProduct].sP == 0 ){
                    serialFlag = false;
                }
                if (locationProducts[selectedProduct].bP == 0 ){
                    batchFlag = false;
                }
                if (locationProducts[selectedProduct].sP == 1 && locationProducts[selectedProduct].bP == 0) {
                    $('#temp_1').find('input[name=mfDateAutoFill]').attr('disabled', true).removeClass('white_bg');
                    $('#temp_1').find('input[name=batch_code]').closest('td').hide();
                    $('#btch_code').hide();
                    $('#bCodeT').hide();
                    $('#bCode').hide();
                    $('.price_field').hide();
                    $('#temp_1').find('input[name=expireDateAutoFill]').addClass('white_bg');
                    $('#temp_1').find('input[name=bPrice]').closest('td').hide();
                } else {
                    $('#temp_1').find('input[name=batch_code]').closest('td').show();
                    $('#temp_1').find('input[name=batch_code]').attr('disabled', true);
                    $('#temp_1').find('input[name=bPrice]').closest('td').show();
                    $('#temp_1').find('input[name=bPrice]').attr('disabled', true).removeClass('white_bg');
                    $('#btch_code').show();
                    $('.price_field').show();
                    $('#bCodeT').show();
                    $('#bCode').show();
                }

                if (locationProducts[selectedProduct].sP == 1 && locationProducts[selectedProduct].bP == 1) {
                    batchSerialFlag = true;
                    $('#batchAddScreen').addClass('hidden');
                    $('#serNumDiv').addClass('hidden');
                    $('#temp_1').find('input[name=batch_code]').attr('disabled', false);
                    $('#temp_1').find('input[name=mfDateAutoFill]').attr('disabled', false).addClass('white_bg');
                    $('#temp_1').find('input[name=mfDateAutoFill]').datepicker();
                    $('#temp_1').find('input[name=bPrice]').attr('disabled', false).addClass('white_bg');

                } else {
                    $('#serNumDiv').removeClass('hidden');
                }

                if (locationProducts[selectedProduct].bP == 0 && locationProducts[selectedProduct].sP == 0) {
                    $('#unitPrice', row).trigger('focusout');
                    addNewProductRow({}, {});
                    $('#qty').parent().removeClass('input-group');
                    $('#itemCode').focus();
                    selectedProduct = "";
                } else {
                    selectedProductQuantity = uomqty;
                    if (locationProducts[selectedProduct].bP == 1) {
                        $('#grnProductCodeBatch').html(locationProducts[selectedProduct].pC);
                        $('#grnProductQuantityBatch').html(uomqty);
                        var deliDate = eb.convertDateFormat('#deliveryDate');
                        var dDate = new Date(Date.parse(deliDate));
                        var batchED = $('#newBatchEDate').datepicker({
                            onRender: function(date) {
                                return date.valueOf() < dDate.valueOf() ? ((U_DATE_R_OVERRIDE)?'':'disabled') : '';
                            }
                        }).on('changeDate', function(ev) {
                            batchED.hide();
                        }).data('datepicker');
                    }
                    if (locationProducts[selectedProduct].sP == 1) {
                        $('#addNewSerial input[type=text]').val('');
                        $('#grnProductCodeSerial').html(locationProducts[selectedProduct].pC);
                        $('#grnProductQuantitySerial').html($('#qty').val());
                        $('#serialSample').children().children('#newSerialBatch').addClass('serialBatchList');
                        $('#serialSample').children().children('#newSerialNumber').addClass('serialNumberList');
                        if (locationProducts[selectedProduct].bP == 0) {
                            $('#serialSample').children().children('#newSerialBatch').prop('disabled', true);
                        } else if (locationProducts[selectedProduct].bP == 1) {
                            $('#serialSample').children().children('#newSerialBatch').prop('disabled', false);
                        }
                        for (i = 0; i < $('#qty').val() - 1; i++) {
                            var serialAddRowClone = $('#serialSample').clone().attr('id', i).addClass('cloneSerials');
                            $('#serialSample').children().children('#newSerialNumber').addClass('serialNumberList');
                            $('#serialSample').children().children('#newSerialBatch').addClass('serialBatchList');
                            serialAddRowClone.children().children('#newSerialNumber').addClass('serialNumberList');
                            serialAddRowClone.children().children('#newSerialBatch').addClass('serialBatchList');
                            serialAddRowClone.children().children('#serialWarrenty').attr('id', 'sEW' + i).addClass('sEW');
                            serialAddRowClone.children().children('#newSerialEDate').attr('id', 'sEDate' + i).addClass('sEDate');
                            if (locationProducts[selectedProduct].bP == 0) {
                                $('#serialSample').children().children('#newSerialBatch').prop('disabled', true);
                                serialAddRowClone.children().children('#newSerialBatch').prop('disabled', true);
                            }
                            serialAddRowClone.insertBefore('#serialSample');
                        }
                        if($('#qty').val()<=100){
                        	var dDate = new Date(Date.parse($('#deliveryDate').val()));
                        	$('.sEDate').datepicker({
                        		onRender: function(date) {
                        			return date.valueOf() < dDate.valueOf() ? '' : '';
                        		}
                        	});
                        }
                        var serialED = $('#newSerialEDate').datepicker({
                            onRender: function(date) {
                                return date.valueOf() < dDate.valueOf() ? '' : '';
                            }
                        }).on('changeDate', function(ev) {
                            serialED.hide();
                        }).data('datepicker');

                    }
                    $('#addGrnProductsModal').modal('show');
                }
                currentlySelectedProduct = null;
            }
        }
    });

    $('#grnProductTable').on('click', '.dropdown-menu, .dropdown-menu label', function(e) {
        e.stopPropagation();
    });

    $('#grnProductTable').on('change', $("#qty"), function() {
        $('#unitPrice').trigger('focusout');
    });

    $('#grnProductTable').on('keyup', '#qty,#unitPrice,#grnDiscount, #itemQuantity', function() {
        if ($('#itemCode').val() != '') {
            if ($(this).val().indexOf('.') > 0) {
                decimalPoints = $(this).val().split('.')[1].length;
            }
            if (this.id == 'unitPrice' && (!$.isNumeric($('#unitPrice').val()) || $('#unitPrice').val() < 0 || decimalPoints > 2)) {
                decimalPoints = 0;
                $('#unitPrice').val('');
            }
            if ((this.id == 'qty') && (!$.isNumeric($('#qty').val()) || $('#qty').val() < 0)) {
                decimalPoints = 0;
                $('#qty').val('');
            }
            if ((this.id == 'grnDiscount') && (!$.isNumeric($('#grnDiscount').val()) || $('#grnDiscount').val() < 0 || (decimalPoints > 2))) {
                decimalPoints = 0;
                $('#grnDiscount').val('');
            }
        }
    });
    var normalDiscountType;
    $('#grnProductTable').on('focusout', '#qty,#unitPrice,#grnDiscount,.uomqty,.uomPrice,input[name=discount]', function() {
        normalDiscountType = null;
        var row = $(this).parents('tr');
        var checkedTaxes = Array();
        $('input:checkbox.addNewTaxCheck', $(this).parents('tr')).each(function() {
            if (this.checked) {
                var cliTID = this.id;
                var tTaxID = cliTID.split('_')[1];
                checkedTaxes.push(tTaxID);
            }
        });
        
        var productID = $('#itemCode', $(this).parents('tr')).data('pID');
        if (productID) {
            productID = productID;
        } else {
            productID = row.attr('id').split("_");
            productID = productID[1];            
        }

        var qty = $("input[name='qty']", $(this).parents('tr')).val();
        var unitPrice = $("input[name='unitPrice']", $(this).parents('tr')).val();
        if (unitPrice != "") {
            $("input[name='unitPrice']", $(this).parents('tr')).val(unitPrice);
        }
        var discount = $("input[name='discount']", $(this).parents('tr')).val();
        var uomqty = $("#qty").siblings('.uomqty').val();

        if (locationProducts[productID]) {
            if(locationProducts[productID].pPDV != null){
                var discountType = "Val";
                var newDiscount = validateDiscount(productID,discountType,discount, unitPrice);
                var tmpItemCost = calculateTaxFreeItemCostByValueDiscount(unitPrice, qty, newDiscount);
                normalDiscountType = 'value';
            } else if (locationProducts[productID].pPDP != null) {
                var discountType = "Per";
                var newDiscount = validateDiscount(productID,discountType,discount, unitPrice);
                var tmpItemCost = calculateTaxFreeItemCost(unitPrice, qty, newDiscount);
                normalDiscountType = 'percentage';
            } else {
                var newDiscount = 0;
                var tmpItemCost = calculateTaxFreeItemCost(unitPrice, qty, newDiscount);
            }
        }

        if ($('#itemCode', $(this).parents('tr')).data('PT') == 2 && qty == 0) {
            qty = 1;
        }

        // var tmpItemCost = calculateTaxFreeItemCost(unitPrice, qty, discount);
        currentItemTaxResults = calculateItemCustomTax(tmpItemCost, checkedTaxes);
        currentTaxAmount = (currentItemTaxResults == null) ? null : currentItemTaxResults.tTA;
        var itemCost = toFloat(tmpItemCost) + toFloat(currentTaxAmount);
        $('.itemLineTotal', $(this).parents('tr')).html(accounting.formatMoney(itemCost));
        if (productID != undefined) {
            var currentEl = new Array();
            currentEl[productID+'_'+unitPrice] = itemCost;
            productLineTotal = $.extend(productLineTotal, currentEl);
        }
    });

    $('#grnProductTable').on('change', '.taxChecks.addNewTaxCheck', function() {
        var allchecked = false;
        var $parentTaxSet = $(this).parents("td.taxSet");
        $parentTaxSet.find('input:checkbox.addNewTaxCheck').each(function() {
            if (this.checked && $(this).prop('disabled') === false) {
                allchecked = true;
            }
        });

        if (allchecked == false) {
            if ($(this).parents('tr').hasClass('tempPoPro')) {
                $parentTaxSet.find('#poTaxApplied').removeClass('glyphicon glyphicon-check');
                $parentTaxSet.find('#poTaxApplied').addClass('glyphicon glyphicon-unchecked');
            } else {
                $parentTaxSet.find('#taxApplied').removeClass('glyphicon glyphicon-check');
                $parentTaxSet.find('#taxApplied').addClass('glyphicon glyphicon-unchecked');
            }
        } else {
            if ($(this).parents('tr').hasClass('tempPoPro')) {
                $parentTaxSet.find('#poTaxApplied').removeClass('glyphicon glyphicon-unchecked');
                $parentTaxSet.find('#poTaxApplied').addClass('glyphicon glyphicon-check');
            } else {
                $parentTaxSet.find('#taxApplied').removeClass('glyphicon glyphicon-unchecked');
                $parentTaxSet.find('#taxApplied').addClass('glyphicon glyphicon-check');
            }
        }
        $(this).parents('tr').find(".uomPrice").trigger(jQuery.Event("focusout"));
        $(this).parents('tr').find("input[name='qty']").trigger(jQuery.Event("focusout"));
    });

    $('#grnProductTable').on('change', '.addNewUomR', function() {
        $('#uomAb').html(locationProducts[selectedProduct].uom[$(this).val()].uA);
        selectedProductUom = $(this).val();
    });

    $('#grnProductTable').on('click', '#selectAll', function() {
        $(this).parents('.dropdown-menu').find('input:checkbox.addNewTaxCheck').each(function() {
            if (!this.checked) {
                this.checked = true;
            }
        });
        $(this).parents('tr').find(".taxChecks.addNewTaxCheck").trigger(jQuery.Event("change"));
    });

    $('#grnProductTable').on('click', '#deselectAll', function() {
        $(this).parents('.dropdown-menu').find('input:checkbox.addNewTaxCheck').each(function() {
            if (this.checked) {
                this.checked = false;
            }
        });
        $(this).parents('tr').find(".taxChecks.addNewTaxCheck").trigger(jQuery.Event("change"));
    });
    $('#deliveryChargeEnable').on('click', function() {
        if (this.checked) {
            $('.deliCharges').removeClass('hidden');
            $('#deliveryCharge').focus();
        }
        else {
            $('#deliveryCharge').val('');
            $('.deliCharges').addClass('hidden');
        }
        setGrnTotalCost();
    });
    $('#deliveryCharge').on('change keyup', function() {
        if (!$.isNumeric($('#deliveryCharge').val()) || $(this).val() < 0) {
            $(this).val('');
            setGrnTotalCost();
        } else {
            setGrnTotalCost();
        }
    });
    $('#grnProductTable').on('click', '.grnDeleteProduct', function(e) {
        e.preventDefault(e);
        var deletePTrID = $(this).closest('tr').attr('id');
        var deletePID = deletePTrID.split('tr_')[1].trim();
        var deletedProID = deletePID.split('_')[1].trim();
        delete serialNumberArray[deletedProID];
        bootbox.confirm('Are you sure you want to remove this product ?', function(result) {
            if (result == true) {
                delete(products[deletedProID]);
                $('#' + deletePTrID).remove();
                setGrnTotalCost();
                setTotalTax();
            }
        });
        $('#itemCode').val('').trigger('change');
    });
    $('#grnProductTable').on('click', '.grnViewProducts', function(e) {
        e.preventDefault();
        var viewPTrID = $(this).closest('tr').attr('id');
        var viewPID = viewPTrID.split('tr_')[1].trim();
        var viewline = viewPID.split('_')[1].trim();
        var vPID = products[viewline].pID;
//        if (locationProducts[vPID].bP == 0 && locationProducts[vPID].sP == 0) {
//            p_notification(false, eb.getMessage('ERR_GRN_SUBPROD'));
//        } else {
        setProductsView(viewline);
        $('#viewGrnSubProductsModal').modal('show');
//        }

    });
    $('#showTax').on('click', function() {
        if (this.checked) {
            if (!jQuery.isEmptyObject(products)) {
                $('#totalTaxShow').removeClass('hidden');
            } else {
                p_notification(false, eb.getMessage('ERR_GRN_PROD_TAX'));
            }

        } else {
            $('#totalTaxShow').addClass('hidden');
        }

    });
    $('#grnCancel').on('click', function() {
        window.location.href = BASE_URL + '/grn/create';
    });

    //update line items
    $('#form_row').on('click', '.grnEditProduct', function(e){
        e.preventDefault();
        var rowID = $(this).parents('tr').attr('id');
        var grnProduclineID = $(this).parents('tr').attr('grnproid');
        $.each(products, function(key, value){
            if(value != undefined){
                if(rowID == value.newTrID){
                    $('#' + rowID).remove();
                    setGrnProductsForEdit(value,grnProduclineID);
                }
            }
        });
    });
    /////////////////////
    
    $('#addGrnProductsModal').on('click','#serFlag', function(){
        if($(this).prop('checked') == true){
            $('#addNewSerial tr').each(function(){
                $(this).find('.serialWarrenty').attr('disabled', true);
                $(this).find('.serialBatchList').attr('disabled', true);
                $(this).find('.serialEDate').attr('disabled', true);
                $('#serialDefaultData').removeClass('hidden');
                machineInputFlag = true;
                onlySerialFlag = true;
                $("#machiIn").prop("checked", true);
                $('.machine-redio').removeClass('hidden');
            });
        } else {
            $('#addNewSerial tr').each(function(){
                $(this).find('.serialWarrenty').attr('disabled', false);
                $(this).find('.serialBatchList').attr('disabled', false);
                $(this).find('.serialEDate').attr('disabled', false);
                machineInputFlag = false;
                onlySerialFlag = false;
                $('#serialDefaultData').addClass('hidden');
                $("#humIn").prop("checked", true);
                $('.machine-redio').addClass('hidden'); 
            });
        }
    });

    $('#addGrnProductsModal').on('change', '.machiHumaIn', function(){
        if($('input[name=inType]:checked').val() == 'machine') {
            machineInputFlag = true;
        } else { 
            machineInputFlag = false;
        }
    });
    


    //////////////////////

    function editedProducts(gPC, gPD, gPDT, gPN, gPP, gPQ, gPT, gPUom, gPUomDecimal, lPID, pT, productID, productPoID, productUomID, productType, bP, sP, grnProductID, GpUom) {
        this.gPC = gPC;
        this.gPD = gPD;
        this.gPDT = gPDT;
        this.gPN = gPN;
        this.gPP = gPP;
        this.gPQ = gPQ;
        this.gPT = gPT;
        this.gPUom = gPUom;
        this.gPUomDecimal = gPUomDecimal;
        this.lPID = lPID;
        this.pT = pT;
        this.productID = productID;
        this.productPoID = productPoID;
        this.productUomID = productUomID;
        this.productType = productType;
        this.bP = bP;
        this.sP = sP;
        this.grnProductID = grnProductID;
        this.GpUom = GpUom;
    }
    function taxes(TXID, pTA, pTN, pTP){
        this.TXID = TXID;
        this.pTA = pTA;
        this.pTN = pTN;
        this.pTP = pTP;
    }
    function editedBatch(bC, bED, bMD, bPrice, bQ, bW){
        this.bC = bC;
        this.bED = bED;
        this.bMD = bMD;
        this.bPrice = bPrice;
        this.bQ = bQ;
        this.bW = bW;
    }
    function editedSerial(sBC, sC, sED, sW, sWT){
        this.sBC = sBC;
        this.sC = sC;
        this.sED = sED;
        this.sW = sW;
        this.sWt= sWT;
    }
    $('#add-new-item-row').on('click', '.grnEditItemAdd', function(e) {
        e.preventDefault();
        var proLineID = $(this).parents('tr').attr('grnprid');
        $('#grnProductTable').find(".uomPrice").trigger('focusout');
        var sameItemFlag = false;
        var tr = $(this).parents("tr");
        var trID = $(this).parents("tr").attr('id');
        var productListKey = null;
        var selectedProductID = null;
        $.each(products, function(key, value){
            if(value != undefined){
                if(trID == value.newTrID){
                    if (tr.find('#editedGrnDisc').val() == "") {
                        var dis = 0;
                    } else {
                        var dis = tr.find('#editedGrnDisc').val();
                    }
                    productListKey = key;
                    selectedProductID = value.pID;
                    products[key].pDiscount = dis;
                    products[key].pQuantity = tr.find("input[name='qty']").val();
                    products[key].pUnitPrice = tr.find("input[name='unitPrice']").val();
                    products[key].pTotal = tr.find('.itemLineTotal').html();
                    products[key].pDType = tr.find('.sign').html();
                    products[key].pUom = tr.find('.uom-select').children('button').find('.selected').data('uomID');
                    products[key].GpUom = tr.find('.uom-price-select').children('button').find('.selected').data('uomID');
                }
            }
        });

        var uomDecimal = 1;
        var uomName = '';
        var editedBatchArray = new Object();
        var editedSerialArray = new Object();
        if(products[productListKey].pTax != undefined){

            var checkedTaxes = Array();
            $('input:checkbox.addNewTaxCheck', $(this).parents('tr')).each(function() {
                if (this.checked) {
                    var cliTID = this.id;
                    var tTaxID = cliTID.split('_')[1];
                    checkedTaxes.push(tTaxID);
                }
            });
            if(checkedTaxes.length > 0){
                var editedTaxArray = new Object();
                $.each(products[productListKey].pTax.tL, function(key, value){
                    if(checkedTaxes.indexOf(key) > -1){
                        editedTaxArray[key] = new taxes(key, value.tA, value.tN, value.tP);
                    }
                });
            }
        }

        if(products[productListKey].bProducts != null){
            $.each(products[productListKey].bProducts, function(key, value){
                editedBatchArray[key] = new editedBatch(value.bCode, value.eDate, value.mDate, value.price, value.bQty, value.warnty);
            });
        }
        if(products[productListKey].sProducts != null){
            $.each(products[productListKey].sProducts, function(key, value){
                editedSerialArray[key] = new editedSerial(value.sBCode, value.sCode, value.sEdate, value.sWarranty, value.sWarrantyType);

            });
        }

        // create data set for update
        //remove records from products array
        var dataSet = new editedProducts(products[productListKey].pCode, products[productListKey].pDiscount, products[productListKey].pDType, products[productListKey].pName, products[productListKey].pUnitPrice, products[productListKey].pQuantity, products[productListKey].pTotal, uomName, uomDecimal, products[productListKey].locationPID, editedTaxArray, products[productListKey].pID, products[productListKey].poID, products[productListKey].pUom, products[productListKey].pT, editedBatchArray, editedSerialArray, proLineID, products[productListKey].GpUom);
        for (var i in products) {
            if(selectedProductID == products[i].pID && toFloat(tr.find('.uomPrice').val()) == toFloat(products[i].pUnitPrice) && trID != products[i].newTrID){
                sameItemFlag = true;
            }
        }

        if(sameItemFlag){
            p_notification(false, eb.getMessage('ERR_SAME_ITEM_IN_GRN'));
            return false;
        } else {
            products.splice(productListKey, 1);
            $('#' + trID).remove();
            addNewProductRowByGrnEdit(dataSet, locationProducts, true);
        }



        /**
        this code part skip for some resons
        if(locationProducts[products[productListKey].pID].bP == 1 && locationProducts[products[productListKey].pID].sP == 1){

        } else if(locationProducts[products[productListKey].pID].bP == 1){

        } else if (locationProducts[products[productListKey].pID].sP == 1) {

        } else {
            products.splice(productListKey, 1);
            $('#' + trID).remove();
            addNewProductRowByGrnEdit(dataSet, locationProducts);
        }
        **/

    });


    $('#addInvoiceProductsModal').on('click', '#batch-save', function(e){
        e.preventDefault();
        var curProID = $(this).attr('data-productid');
        var editedLineID = $(this).attr('data-lineid');
        var $batchTable = $("#addInvoiceProductsModal .batch-table tbody");
        if($(this).hasClass('batchAdd')){
            var totalQty = 0;
            var validFlag = true;
            $("input[name='deliverQuantity']", $batchTable).each(function() {
                var row = $(this).parents('tr');
                if(row.attr('id') != 'defaultRow'){
                    var exitQty = $("input[name='availableQuantity']", row).val();

                    if(toFloat($(this).val()) > toFloat(exitQty)){
                        p_notification(false, eb.getMessage('ERR_BATCH_QTY'));
                        validFlag = false;
                        return false;
                    } else {
                        var bCode = row.attr('data-btchcode');
                        var Qty = $(this).val();
                        var wrnty = $("input[name='warranty']", row).val();
                        var expDate = $("input[name='expireDate']", row).val();
                        var uprice = $("input[name='btPrice']", row).val();
                       batchProducts[bCode] = new batchProduct(bCode,Qty, '', expDate, wrnty, row.attr('id'));
                    }
                }
                totalQty += toFloat(Qty);
            });
            if(toFloat(totalQty) == 0){
                p_notification(false, eb.getMessage('ERR_BATCH_QTY_CANNOT_BE_NULL'));
                validFlag = false;
                return false;
            }

            if(validFlag){
                var taxResult = calculateTaxForUpdatePi(editedPiBatachSerialRow, totalQty, piEditedDataSet.piPP, piEditedDataSet.piPD);
                piEditedDataSet.currentItemTaxResult = taxResult;
                currentTaxAmount = (taxResult == null) ? null : taxResult.tTA;
                var singleItemCost = calculateTaxFreeItemCost(piEditedDataSet.piPP, totalQty, piEditedDataSet.piPD);
                piEditedDataSet.piPT = toFloat(singleItemCost) + toFloat(currentTaxAmount);
                piEditedDataSet.piPQ = totalQty;
                addNewProductRowByEdit(batchProducts, {}, piEditedDataSet, false);
                $('#'+removedLineID).remove();
                $('#addInvoiceProductsModal').modal('hide');
                itemEditOpenFlag = false;

            }

        } else if($(this).hasClass('serialAdd')){
            var totalQty = 0;
            $("input[name='deliverQuantityCheck']:checked", $batchTable).each(function() {
                var row = $(this).parents('tr');
                if(row.attr('id') != 'defaultRow'){
                    totalQty++;
                    var sCode = row.attr('data-serialcode');
                    var bCode = row.attr('data-btchcode');
                    var Qty = 1;
                    var wrnty = $("input[name='warranty']", row).val();
                    var expDate = $("input[name='expireDate']", row).val();
                    var uprice = $("input[name='btPrice']", row).val();
                    serialProducts[sCode] = new serialProduct(sCode, wrnty, bCode, expDate, row.attr('id'));
                }

            });
            if(toFloat(totalQty) == 0){
                p_notification(false, eb.getMessage('ERR_SERIAL_QTY_CANNOT_BE_NULL'));
                validFlag = false;
                return false;
            } else {

                var taxResult = calculateTaxForUpdatePi(editedPiBatachSerialRow, totalQty, piEditedDataSet.piPP, piEditedDataSet.piPD);
                piEditedDataSet.currentItemTaxResult = taxResult;
                currentTaxAmount = (taxResult == null) ? null : taxResult.tTA;
                var singleItemCost = calculateTaxFreeItemCost(piEditedDataSet.piPP, totalQty, piEditedDataSet.piPD);
                piEditedDataSet.piPT = toFloat(singleItemCost) + toFloat(currentTaxAmount);
                piEditedDataSet.piPQ = totalQty;
                addNewProductRowByEdit({}, serialProducts, piEditedDataSet, false);
                $('#'+removedLineID).remove();
                $('#addInvoiceProductsModal').modal('hide');
                itemEditOpenFlag = false;

            }

        }

    });
    //////////////////////




    $('form#grnForm').on('submit', function(e) {
        e.preventDefault();
        var selectedid = "#" + document.activeElement.id;
        var tableId = $(selectedid).closest('table').attr("id");

        var documentReference = {};
        var flagfalse = false;
        $('.documentTypeBody > tr').each(function() {
            if ($(this).hasClass('documentRow')) {
                var documentTypeID = $(this).find('.documentTypeId').val();
                var documentID = $(this).find('.documentId').val();
                if (!(documentTypeID == null || documentTypeID == '')) {
                    if (documentID != null) {
                        documentReference[documentTypeID] = documentID;
                    } else {
                        flagfalse = true;
                    }
                }
            }
        });
        var editItemOpenFlag = false;
        $('#add-new-item-row tr').each(function(){
            if($(this).hasClass('editOpen')){
                editItemOpenFlag = true;
            }
        });

        if (flagfalse) {
            p_notification(false, eb.getMessage('ERR_PLEASE_FILL_ALL_DOCUMENT_REFERENCE'));
        } else if (typeof (selectedSupplier) == 'undefined' || selectedSupplier == '' || selectedSupplier == 0) {
            p_notification(false, eb.getMessage('ERR_GRN_SELECT_SUPP'));
        } else if (typeof (selectedLocation) == 'undefined' || selectedLocation == '') {
            p_notification(false, eb.getMessage('ERR_GRN_SELECT_LOCAT'));////eb.getMessage('ERR_GRN_SELECT_LOCAT'));
        }
//        else if (!isValidDate($('#deliveryDate').val())) {
//            p_notification(false, eb.getMessage('ERR_GRN_SET_DELIDATE'));
//        }
        else if (eb.convertDateFormat('#deliveryDate') > $('#todayDate').val()) {
            p_notification(false, eb.getMessage('ERR_GRN_SET_FUTUREDATE'));
        } else if ($('#grnNo').val() == '') {
            p_notification(false, eb.getMessage('ERR_GRN_SELECT_LOCAT_REFNO'));//eb.getMessage('ERR_GRN_SELECT_LOCAT_REFNO'));
        } else if (jQuery.isEmptyObject(products)) {
            if (tableId != "grnProductTable") {
                p_notification(false, eb.getMessage('ERR_GRN_PROD_ADD'));
            }
        } else if(editItemOpenFlag) {
            p_notification(false, eb.getMessage('ERR_PLEASE_ADD_EDITED_ITEM'));
        } else {
            if($('#editMode').val() != '1'){

                var isActiveWF = $("#approvalWorkflowSelector").attr('data-isactive');
                if (isActiveWF && $('.approval-workflows').val() == "null") {
                    p_notification(false, eb.getMessage('ERR_GRN_APR_WF_EMPTY'));
                    return;
                }
                var wfID = ($('.approval-workflows').val()) ? $('.approval-workflows').val() : null;
            } else {
                var isActiveWF = false;
                var wfID = null;
            }

            $('#grnSave').prop('disabled', true);
            $('#grnEdit').prop('disabled', true);
            //$("#unitPrice", $thisRow).focusout();
            var gFT = 0;
            for (var i in products) {
                gFT += toFloat(products[i].pTotal);
            }
            var sID = selectedSupplier;
            var showTax = 0;
            if ($('#showTax').is(':checked')) {
                showTax = 1;
            }

            if ($('#deliveryCharge').val() > 0) {
                gFT += parseFloat($('#deliveryCharge').val());    
            }
            

            // if($('#editMode').val() == '1'){
            //     $.each(products, function(index, val) {
            //         if (val) {
            //             var uomII = val.pUom;
            //             if (val.isEdit && locationProducts[val.pID].uom[uomII]['pUDisplay'] == 1) {
            //                 val.pUnitPrice = val.pUnitPrice / locationProducts[val.pID].uom[uomII]['uC'];
            //                 val.pQuantity = val.pQuantity * locationProducts[val.pID].uom[uomII]['uC'];


            //             }
            //         }
            //     });
            // }
            var grnPostData = {
                sID: sID,
                gC: $('#grnNo').val(),
                dD: $('#deliveryDate').val(),
                sR: $('#supplierReference').val(),
                rL: selectedLocation,
                cm: $('#comment').val(),
                pr: JSON.stringify(products),
                // pr: products,
                dC: $('#deliveryCharge').val(),
                fT: gFT,
                sT: showTax,
                lRID: $('#locRefID').val(),
                startByPoID: selectedPO,
                documentReference: documentReference,
                uploadDocFlag: uploadDocumentFlag,
                compoundCostID: compoundCostID,
                dimensionData: dimensionData,
                ignoreBudgetLimit: ignoreBudgetLimitFlag,
                isActiveWF: isActiveWF,
                wfID : wfID
            };
            var usedUrl = BASE_URL + '/api/grn/saveGrn';
            var editedMode = false;
            if($('#editMode').val() == '1'){
                var existingAttachemnts = {};
                var deletedAttachments = {};
                $.each(uploadedAttachments, function(index, val) {
                    if (!deletedAttachmentIds.includes(val['documentAttachemntMapID'])) {
                        existingAttachemnts[index] = val;
                    } else {
                        deletedAttachments[index] = val;
                    }
                });

                grnPostData['grnID'] = $('#editMode').attr('data-grnid');
                var usedUrl = BASE_URL + '/api/grn/updateGrn';
                editedMode = true;
            }

            saveAndUpdateGrn(grnPostData);
        }
    });

    function saveAndUpdateGrn(grnPostData)
    {
        var usedUrl = BASE_URL + '/api/grn/saveGrn';
        var editedMode = false;
        if($('#editMode').val() == '1'){
            var existingAttachemnts = {};
            var deletedAttachments = {};
            $.each(uploadedAttachments, function(index, val) {
                if (!deletedAttachmentIds.includes(val['documentAttachemntMapID'])) {
                    existingAttachemnts[index] = val;
                } else {
                    deletedAttachments[index] = val;
                }
            });

            var usedUrl = BASE_URL + '/api/grn/updateGrn';
            editedMode = true;
        }
   
        eb.ajax({
            type: 'POST',
            url: usedUrl,
            data: grnPostData,
            success: function(respond) {
                if (respond.status == true && !editedMode) {
                    p_notification(respond.status, respond.msg);
                    var fileInput = document.getElementById('documentFiles');
                    if(fileInput.files.length > 0) {
                        var form_data = false;
                        if (window.FormData) {
                            form_data = new FormData();
                        }
                        form_data.append("documentID", respond.data.grnID);
                        form_data.append("documentTypeID", 10);
                        
                        for (var i = 0; i < fileInput.files.length; i++) {
                            form_data.append("files[]", fileInput.files[i]);
                        }

                        eb.ajax({
                            url: BASE_URL + '/store-files',
                            type: 'POST',
                            processData: false,
                            contentType: false,
                            data: form_data,
                            success: function(res) {
                            }
                        });
                    }
                    if (respond.data.isDraft) {
                        
                        documentPreview(BASE_URL + '/grn/draftPreview/' + respond.data.grnID, 'documentpreview', "/api/grn/send-grn-email", function($preview) {
                            $('iframe', $preview).bind('load', function() {
                                
                                if (respond.data.approvers.length != 0) {
                                    $('iframe', $preview).unbind('load');
                                    sendEmailToApprover(respond.data.grnID, respond.data.grnCode, respond.data.approvers, respond.data.hashValue, true, $preview, function($preview) {
                                        $preview.on('hidden.bs.modal', function() {
                                            if (!$("#preview:visible").length) {
                                                window.location.reload();
                                            }
                                        });
                                    }); 
                                } else {
                                    $preview.on('hidden.bs.modal', function() {
                                        if (!$("#preview:visible").length) {
                                            window.location.reload();
                                        }
                                    });
                                }

                            });
                        });
                    } else {
                        documentPreview(BASE_URL + '/grn/preview/' + respond.data.grnID, 'documentpreview', "/api/grn/send-grn-email", function($preview) {
                            $preview.on('hidden.bs.modal', function() {
                                if (!$("#preview:visible").length) {
                                    window.location.assign(BASE_URL + "/grn/create");
                                }
                            });
                        });
                    }

                } else if(respond.status == true && editedMode) {
                    p_notification(respond.status, respond.msg);
                    if(respond.data == null) {
                        var documentID = "same";
                    } else {
                        var documentID = respond.data;
                    }

                    var fileInput = document.getElementById('editDocumentFiles');
                    var form_data = false;
                    if (window.FormData) {
                        form_data = new FormData();
                    }
                    form_data.append("documentID", documentID);
                    form_data.append("documentTypeID", 10);
                    form_data.append("updateFlag", true);
                    form_data.append("editedDocumentID", $('#editMode').attr('data-grnid'));
                    form_data.append("documentCode", $('#grnNo').val());
                    form_data.append("deletedAttachmentIds", deletedAttachmentIds);
                    
                    if(fileInput.files.length > 0) {
                        for (var i = 0; i < fileInput.files.length; i++) {
                            form_data.append("files[]", fileInput.files[i]);
                        }
                    }

                    eb.ajax({
                        url: BASE_URL + '/store-files',
                        type: 'POST',
                        processData: false,
                        contentType: false,
                        data: form_data,
                        success: function(res) {
                        }
                    });

                    window.location.assign(BASE_URL + "/grn/list");
                }else {
                    if (respond.data == "NotifyBudgetLimit") {
                        bootbox.confirm(respond.msg+' ,Are you sure you want to continue ?', function(result) {
                            if (result == true) {
                                ignoreBudgetLimitFlag = true;
                                grnPostData.ignoreBudgetLimit = true;
                                saveAndUpdateGrn(grnPostData);
                            } else {
                                setTimeout(function(){ 
                                    location.reload();
                                }, 3000);
                            }
                        });
                    } else {
                        $('#grnSave').prop('disabled', false);
                        $('#grnEdit').prop('disabled', false);
                        p_notification(respond.status, respond.msg);
                    }
                }
            },
            // async: false
        });
    }

    function sendEmailToApprover(grnId, grnCode, approvers, token, flag, preview = null, callback = null) {
        if (flag) {
            eb.ajax({
                type: 'POST',
                url: BASE_URL + '/api/grn/sendApproverEmail',
                data: {
                    grnId: grnId,
                    grnCode: grnCode,
                    approvers: approvers,
                    token: token,
                },
                success: function(respond) {
                    if (respond.status == true) {
                        p_notification(respond.status, respond.msg);
                        if (callback != null) {
                            callback(preview);
                        } else {
                            var url = BASE_URL + '/grn/list';
                            window.location.assign(url);
                        }
                    } else {
                        p_notification(respond.status, respond.msg);
                    }
                }

            });
        } else {
            callback(preview);
        }
    }


    $('#grnProductTable').on('click', '.poRemoveItem', function() {
        var tr = $(this).parents("tr");
        var removeProductID = tr.attr('id');
        $('#' + removeProductID).remove();
    });
    $('#grnProductTable').on('click', '.grnEditItemDel', function() {
        var rowID = $(this).parents('tr').attr('id');
        $.each(products, function(key, value){
            if(rowID == value.newTrID){
                products.splice(key, 1);
            }
        });
        setGrnTotalCost();
        setTotalTax();
        $('#' + rowID).remove();
    });

    $('#grnProductTable').on('click', '.poAddItem', function() {
        thisIsPoAddFlag = true;
        if ($(this).parents('tr').hasClass('compound-cost')) {
            thisIsPoAddFlag = false;
            compoundCostAddFlag = true;
            
        }
        var tr = $(this).parents("tr");
        tr.find(".uomPrice").trigger('focusout');
        var addProductID = tr.attr('id').split('poPro_')[1].trim();
        addedProForCost = addProductID;
        addingPoPro = locationProducts[addProductID].pC;

        var itemDuplicateCheck = false;
        if (!$.isEmptyObject(products)) {
            $.each(products, function(index, value) {
                if (value.locationPID == locationProducts[addProductID].lPID) {
                    itemDuplicateCheck = true;
                }
            });   
        }
       
        if (itemDuplicateCheck) {
            p_notification(false, eb.getMessage('ERR_GRN_INSERT_PROD'));
            return false;
        }

        selectedProduct = addProductID;

        var uomI =  tr.find("input[name='qty']").siblings('.uom-select').children('button').find('.selected').data('uomID');

        var conversion = locationProducts[addProductID].uom[uomI].uC;

        var addProductQty = tr.find("input[name='qty']").val() / parseFloat(conversion);
        $('.tempy').remove();
        idNumber = 1;
        quon = 0;
        $('#prefix').val('');
        var dDate = new Date(Date.parse($('#deliveryDate').val()));
        var epd = $('#expire_date_autoFill').val('').datepicker({
            onRender: function(date) {
                return date.valueOf() < dDate.valueOf() ? '' : '';
            }
        }).on('changeDate', function(ev) {
            epd.hide();
        }).data('datepicker');
        $('#quontity').val('');
        $('#startNumber').val('');
        $('#wrtyDays').val('');
        $('#wrtyType').val('');
        $('#batch_code').val('');
        var mfd = $('#mf_date_autoFill').val('').datepicker({
            onRender: function(date) {
                return date.valueOf() > dDate.valueOf() ? '' : '';
            }
        }).on('changeDate', function(ev) {
            mfd.hide();
        }).data('datepicker');

        $('#temp_1').find('input[name=submit]').attr('disabled', false);
        $('#temp_1').find('input[name=prefix]').attr('disabled', false);
        $('#temp_1').find('input[name=startNumber]').attr('disabled', false);
        $('#temp_1').find('input[name=quontity]').attr('disabled', false);
        $('#temp_1').find('input[name=expireDateAutoFill]').attr('disabled', false);
        $('#temp_1').find('input[name=batch_code]').attr('disabled', false);
        $('#temp_1').find('input[name=mfDateAutoFill]').attr('disabled', false);
        $('#temp_1').find('input[name=wrtyDays]').attr('disabled', false);
        $('#temp_1').find('input[name=wrtyType]').attr('disabled', false);

        clearModalWindow();
        $('.cloneSerials').remove();
        $('#serialSample').children().children('#newSerialNumber').removeClass('serialNumberList');
        $('#serialSample').children().children('#newSerialBatch').removeClass('serialBatchList');
        $('#serialSample').children().children('#newSerialBatch').prop('disabled', false);
        if (tr.find("input[name='itemCode']").data('PT') == 2) {
            locationProducts[addProductID].bP = 0;
            locationProducts[addProductID].sP = 0;
        }
        if (locationProducts[addProductID].bP == 1) {
            $('#batchAddScreen').removeClass('hidden');
            batchFlag = true;
        }
        if (locationProducts[addProductID].sP == 1) {
            $('#serialAddScreen').removeClass('hidden');
            serialFlag = true;
        }
        if (locationProducts[addProductID].sP == 0) {
            serialFlag = false;
        }
        if (locationProducts[addProductID].bP == 0) {
            batchFlag = false;
        }
        if (locationProducts[selectedProduct].sP == 1 && locationProducts[selectedProduct].bP == 0) {
            $('#temp_1').find('input[name=mfDateAutoFill]').attr('disabled', true).removeClass('white_bg');
            $('#temp_1').find('input[name=batch_code]').attr('disabled', true);
            $('#temp_1').find('input[name=expireDateAutoFill]').addClass('white_bg');
        }
        if (locationProducts[addProductID].bP == 1 && locationProducts[addProductID].sP == 1) {
            $('#batchAddScreen').addClass('hidden');
            $('#temp_1').find('input[name=batch_code]').attr('disabled', false);
            $('#temp_1').find('input[name=mfDateAutoFill]').attr('disabled', false).addClass('white_bg').datepicker();
            $('#temp_1').find('input[name=expireDateAutoFill]').addClass('white_bg');

        }

        if (locationProducts[addProductID].bP == 0 && locationProducts[addProductID].sP == 0) {
            addNewProductRow({}, {});
            $('#itemCode').focus();
        } else {
            selectedProductQuantity = addProductQty;
            if (locationProducts[addProductID].bP == 1) {
                $('#grnProductCodeBatch').html(locationProducts[addProductID].pC);
                $('#grnProductQuantityBatch').html(addProductQty);
                var dDate = new Date(Date.parse($('#deliveryDate').val()));
                var batchED = $('#newBatchEDate').datepicker({
                    onRender: function(date) {
                        return date.valueOf() < dDate.valueOf() ? ((U_DATE_R_OVERRIDE)?'':'disabled') : '';
                    }
                }).on('changeDate', function(ev) {
                    batchED.hide();
                }).data('datepicker');
            }
            if (locationProducts[addProductID].sP == 1) {
                $('#addNewSerial input[type=text]').val('');
                $('#grnProductCodeSerial').html(locationProducts[addProductID].pC);
                $('#grnProductQuantitySerial').html(addProductQty);
                $('#serialSample').children().children('#newSerialBatch').addClass('serialBatchList');
                $('#serialSample').children().children('#newSerialNumber').addClass('serialNumberList');
                for (i = 0; i < addProductQty - 1; i++) {
                    var serialAddRowClone = $('#serialSample').clone().attr('id', i).addClass('cloneSerials');
                    $('#serialSample').children().children('#newSerialNumber').addClass('serialNumberList');
                    $('#serialSample').children().children('#newSerialBatch').addClass('serialBatchList');
                    serialAddRowClone.children().children('#newSerialNumber').addClass('serialNumberList');
                    serialAddRowClone.children().children('#newSerialBatch').addClass('serialBatchList');
                    serialAddRowClone.children().children('#newSerialEDate').attr('id', 'sEDate' + i).addClass('sEDate');
                    serialAddRowClone.children().children('#serialWarrenty').attr('id', 'sEW' + i).addClass('sEW');
                    if (locationProducts[addProductID].bP == 0) {
                        $('#serialSample').children().children('#newSerialBatch').prop('disabled', true);
                        serialAddRowClone.children().children('#newSerialBatch').prop('disabled', true);
                    }
                    serialAddRowClone.insertBefore('#serialSample');
                }
                if(addProductQty<=100){
                	var dDate = new Date(Date.parse($('#deliveryDate').val()));
                	$('.sEDate').datepicker({
                		onRender: function(date) {
                			return date.valueOf() < dDate.valueOf() ? ((U_DATE_R_OVERRIDE)?'':'disabled') : '';
                		}
                	});
                }
                var serialED = $('#newSerialEDate').datepicker({
                    onRender: function(date) {
                        return date.valueOf() < dDate.valueOf() ? ((U_DATE_R_OVERRIDE)?'':'disabled') : '';
                    }
                }).on('changeDate', function(ev) {
                    serialED.hide();
                }).data('datepicker');
            }
            $('#addGrnProductsModal').modal('show');
        }
    });
///////////Add batch & serial products //////////////
    $(document).on('keyup', '#newBatchQty,#newBatchWarrenty', function() {
        if ((this.id == 'newBatchQty') && ((!$.isNumeric($('#newBatchQty').val()) || $('#newBatchQty').val() <= -1))) {
            $('#newBatchQty').val('');
        }
        if ((this.id == 'newBatchWarrenty') && ((!$.isNumeric($('#newBatchWarrenty').val()) || $('#newBatchWarrenty').val() < 0))) {
            $('#newBatchWarrenty').val('');
        }
    });
    $(document).on('keyup', '.serialWarrenty', function() {
        if (!$.isNumeric($(this).val()) || $(this).val() < 0) {
            $(this).val('');
        }
    });

    $('#newBatchNumber').on('click', function(){
        $(this).parents('tr').find('#newBatchMDate').val('');
        $(this).parents('tr').find('#newBatchEDate').val('');
    });

    $('#newBatchNumber').typeahead({
        ajax: {
            url: BASE_URL + '/api/grn/get-batch-numbers-for-typeahead',
            timeout: 250,
            triggerLength: 1,
            method: "POST",
            preProcess: function(respond) {
                if (!respond.status) {
                    return false;
                }
                return respond.data;
            }
        }
    });
    $('#addNewBatch').on('focusout', '#newBatchQty' ,function(){
        var addedBatchCode = $(this).parents('tr').find('#newBatchNumber').val();
        var selectedRow = $(this).parents('tr');
        SetMDateAndEDateToBatchItems(addedBatchCode, selectedRow);
    });

    function SetMDateAndEDateToBatchItems(addedBatchCode, selectedRow)
    {
         if(!(selectedRow.find('#newBatchMDate').val() && selectedRow.find('#newBatchEDate').val())){
            eb.ajax({
            type: 'POST',
            url: BASE_URL + '/api/grn/get-man-date-and-exp-date-for-batch-item',
            data: {batchCode : addedBatchCode },
                success: function(respond) {
                    if (respond.status == true) {
                        if(!$('#newBatchMDate', selectedRow).val()){
                            $('#newBatchMDate', selectedRow).val(respond.data.manDate);
                        }
                        if(!$('#newBatchEDate', selectedRow).val()){
                            $('#newBatchEDate', selectedRow).val(respond.data.expDate);
                        }
                    }
                },

            });
        }
    }


    $('#addBatchItem').on('click', function() {
        
        if ($('#newBatchNumber').val() == null || $('#newBatchQty').val() == '' || $('#newBatchNumber').val().trim() === '') {
            p_notification(false, eb.getMessage('ERR_GRN_BATCH_INFO'));
        } else {
            if ($('#newBatchMDate') != '') {
                if ($('#newBatchMDate').val() > $('#deliveryDate').val()) {
                    p_notification(false, eb.getMessage('ERR_GRN_MANUFAC_DATE'));
                    return false;
                }
            }
            if ($('#newBatchEDate').val() != '') {
                if ($('#newBatchMDate').val() != '') {
                    var newBatchEDate = eb.convertDateFormat('#newBatchEDate');
                    var newBatchMDate = eb.convertDateFormat('#newBatchMDate');
                    if (newBatchEDate < newBatchMDate) {
                        p_notification(false, eb.getMessage('ERR_GRN_EXPIRE_DATE'));
                        return false;
                    }
                }

            }
            var newBCode = $('#newBatchNumber').val();
            var newBQty = $('#newBatchQty').val();
            var newBMDate = $('#newBatchMDate').val();
            var newBEDate = $('#newBatchEDate').val();
            var newBWrnty = $('#newBatchWarrenty').val();
            var newBPrice = $('#newBatchPrice').val();
            if ((toFloat(batchCount) + toFloat(newBQty)) > selectedProductQuantity) {
                p_notification(false, eb.getMessage('ERR_GRN_BATCH_COUNT'));
            } else {
                if (batchProducts[newBCode] != null) {
                    p_notification(false, eb.getMessage('ERR_GRN_BATCH_CODE'));
                } else {
                    if ((toFloat(batchCount) + toFloat(newBQty)) == selectedProductQuantity) {
                        $('#proBatchAddNew').addClass('hidden');
                    }
                    batchCount += toFloat(newBQty);
                    batchProducts[newBCode] = new batchProduct(newBCode, newBQty, newBMDate, newBEDate, newBWrnty, newBPrice, null);
                    var cloneBatchAdd = $($('#proBatchSample').clone()).attr('id', newBCode).removeClass('hidden').addClass('tempProducts');
                    cloneBatchAdd.children('#batchNmbr').html(newBCode);
                    cloneBatchAdd.children('#batchQty').html(newBQty);
                    cloneBatchAdd.children('#batchMDate').html(newBMDate);
                    cloneBatchAdd.children('#batchEDate').html(newBEDate);
                    cloneBatchAdd.children('#batchW').html(newBWrnty);
                    cloneBatchAdd.children('#btchPrice').html(newBPrice);
                    cloneBatchAdd.insertBefore('#proBatchSample');
                    clearAddBatchProductRow();
                }
            }


        }

    });

    $('#addNewBatch').on('keyup','#newBatchPrice', function(){
        if(!$.isNumeric($(this).val()) || $(this).val() < 0 ){
            p_notification(false, eb.getMessage('ERR_PRICE_DATA_TYPE'));
            $('#newBatchPrice').val('');
        }
    });
    
    var lasttime = 0;
    var timeDiff = 0;
    var machineInput = false;
    var serialVal = '';
    var lastString = '';
    var manualInputFlag = false;
    var serialDataSet = [];
    // var newTime;
    $('#addGrnProductsModal').on('keyup','input.serialNumberList', function() {

        if(machineInputFlag){
            var lineQty = selectedProductQuantity;
            var newDate = new Date();
            var newTime = newDate.getTime();
            timeDiff = newTime - lasttime;
            var rowID = $(this).parents('tr').attr('id');

            if(timeDiff > 100 && machineInput){
                
                serialVal = $(this).val();
                var newStr = serialVal.substring(0, serialVal.length-1);
                var currentValue = $(this).val();
                var currentValue = serialVal.substr(serialVal.length - 1);
                if(serialDataSet.includes(newStr)){
                    $(this).val(currentValue);

                } else {
                    $(this).val(newStr);    
                    serialDataSet.push(newStr);
                    if(rowID == 'serialSample'){
                        $(this).val(currentValue);
                    } else {

                        var newID;
                        if(parseInt(rowID)+1 == parseInt(lineQty)-1){
                            newID = '#serialSample';
                        } else {
                            newID = '#'+ ++rowID;
                            
                        }
                        
                        $(newID).find('input.serialNumberList').focus().val(currentValue); 
                        
                    }
                    
                }
                serialVal = '';
                lasttime = 0;
                machineInput = false;
                
            } else if (timeDiff > 100){
                serialVal = $(this).val();
                lasttime = newTime;

            } else if (timeDiff < 100){
                            
                //this is machine input
                serialVal = $(this).val();
                lasttime = newTime;
                machineInput = true;
            
            } else {
                machineInput = false;
            }        
        }
        
    });
    
    $('#addGrnProductsModal').on('change', 'input.serialNumberList', function() {
        if(!machineInput){
            var matchSerialNumberCount = 0;
            var typedVal = $(this).val();
            var serialCheckStatus = false;
            eb.ajax({
                type: 'POST',
                url: BASE_URL + '/productAPI/checkSerialProductCodeExists',
                data: {serialCode: typedVal, productCode: locationProducts[selectedProduct].pC, },
                success: function(respond) {
                    if (respond.data == true) {
                        serialCheckStatus = true;
                    }
                },
                async: false
            });
            if (serialCheckStatus == true) {
                p_notification(false, eb.getMessage('ERR_GRN_SERIAL_CODE'));
                $(this).val('');
            } else {
                $('input.serialNumberList').each(function() {
                    if ($(this).val() == typedVal) {
                        matchSerialNumberCount++;
                    }
                });
                if (matchSerialNumberCount > 1) {
                    p_notification(false, eb.getMessage('ERR_GRN_SAME_SERIAL_CODE'));
                    $(this).val('');
                }
            }
            
        }
    });
    $('#addGrnProductsModal').on('change', 'input.serialBatchList,#newSerialBatch', function() {
        if (!$.isEmptyObject(batchProducts[$(this).val()]) && !batchProducts.hasOwnProperty($(this).val())) {
            p_notification(false, eb.getMessage('ERR_GRN_ENTER_BATCHNUM'));
            $(this).val('');
        } else {
            var enteredBatchCode = $(this).val();
            var sameBatchCodeCount = 0;
            $('input.serialBatchList,#newSerialBatch').each(function() {
                if ($(this).val() == enteredBatchCode) {
                    sameBatchCodeCount++;
                }
            });

            if (!$.isEmptyObject(batchProducts[enteredBatchCode]) && sameBatchCodeCount > batchProducts[enteredBatchCode].bQty) {
                p_notification(false, eb.getMessage('ERR_GRN_ENTER_PRODLIMIT'));
                $(this).val('');
            }
        }
    });
    $('#saveGrnProducts').on('click', function(event) {
        var serialEmpty = false;
        var serialCodeSet = [];
        rowNumber = rowNumber + 1;
        $('input.serialNumberList').each(function() {
            serialCodeSet.push($(this).val());
            if ($(this).val() == '') {
                serialEmpty = true;
                return false;
            }
        });

        serialNumberArray[rowNumber] = serialCodeSet;

        var serialNumberArrayKeys = Object.keys(serialNumberArray);
        var finalArray = [];
        for (var i = 0; i < serialNumberArrayKeys.length; i++) {
            var index = serialNumberArrayKeys[i];
            for (var j = 0; j < serialNumberArray[index].length; j++) {
                finalArray.push(serialNumberArray[index][j]);
            }
        }

        var intailLength = finalArray.length;

        var uniqueArray = finalArray.filter(function(item, pos) {
            return finalArray.indexOf(item) == pos;
        })

        var lengthWithoutDuplications = uniqueArray.length;

        if (intailLength != lengthWithoutDuplications) {
            serialNumberArray[serialNumberArrayKeys.length - 1] = [];
            p_notification(false, eb.getMessage('ERR_GRN_SERIAL_CODE'));
            return false;
        }

        if (serialEmpty == true) {
            p_notification(false, eb.getMessage('ERR_GRN_FILL_SERIALNUM'));
            return false;
        }

        var batchEmpty = false;
        if (locationProducts[selectedProduct].bP == 1 && locationProducts[selectedProduct].sP == 0) {
            if (batchCount < selectedProductQuantity) {
                batchEmpty = true;
            }
        }
        if (batchEmpty == true) {
            p_notification(false, eb.getMessage('ERR_GRN_ADD_BATCH_DETAILS'));
            return false;
        }
        var serialBatchEmpty = false;
        if ((locationProducts[selectedProduct].bP == 1) && (locationProducts[selectedProduct].sP == 1)) {
            $('input.serialBatchList').each(function() {
                if ($(this).val() == '') {
                    serialBatchEmpty = true;
                    return false;
                }
            });
        }
        if (serialBatchEmpty == true) {
            p_notification(false, eb.getMessage('ERR_GRN_ADD_SERIAL_BATCH_DETAILS'));
            return false;
        }
        
        //validate serial values in serial bulk upload process
        var normalInputValidationFlag = false;
        if(onlySerialFlag){
            eb.ajax({
                type: 'POST',
                url: BASE_URL + '/productAPI/checkSerialProductCodeExists',
                data: {serialCode: serialCodeSet, productCode: locationProducts[selectedProduct].pC,bulk: true, dataSet: serialCodeSet},
                success: function(respond) {
                    if (respond.status == true) {
                        errorMsg = respond.data;
                        p_notification(false, eb.getMessage('ERR_GRN_SERIAL_BULK', [errorMsg]));
                        normalInputValidationFlag = false;
                        serialEmpty = true;    
                    } else {
                        
                        normalInputValidationFlag = true;
                        
                    }        
                },
                async: false
            });
        } else {
            normalInputValidationFlag = true;
        }
       
       

              
        if (serialEmpty == false && batchEmpty == false && normalInputValidationFlag == true) {
            var serialEDateValidation = true;
            var serialCurrentDataSetValidFlag = true;
            var currentlyAddedSerials = [];
            $('#addNewSerial > tr').each(function() {
                var sNmbr = $(this).children().children('#newSerialNumber').val();
                var sWrnty = $(this).children().children('#newSerialWarranty').val();
                var sWrntyT = $(this).children().children('#newSerialWarrantyType').val();
                var sBt = $(this).children().children('#newSerialBatch').val();
                var sED = $(this).children().children('.serialEDate').val();
                var bRow = $(this).children().children('#newSerialBatchPrice').data('rowno');
                if(onlySerialFlag){
                    currentlyAddedSerials.push(sNmbr);
                    sWrnty = $('#addGrnProductsModal').find('#defaultSerialWarranty').val();
                    sED = $('#defaultSerialEDate').val();

                }



                if (sNmbr != '') {
                    if (sBt != '') {
                        if (sED != '') {
                            if (!$.isEmptyObject(batchProducts[sBt]) && Date.parse(sED) < Date.parse(batchProducts[sBt].mDate)) {
                                serialEDateValidation = false;
                                return false;
                            } else {
                                serialProducts[sNmbr] = new serialProduct(sNmbr, sWrnty, sBt, sED, sWrntyT);
                            }
                        } else {
                            serialProducts[sNmbr] = new serialProduct(sNmbr, sWrnty,sBt, sED,sWrntyT);
                        }
                    } else {
                        serialProducts[sNmbr] = new serialProduct(sNmbr, sWrnty, sBt, sED, sWrntyT);
                    }
                }
            });
            
            if (onlySerialFlag) {
                var duplicatedResults = [];
                var duplicateRecordsFlag = false;
                var sorted_arr = currentlyAddedSerials.slice().sort();
                for (var i = 0; i < currentlyAddedSerials.length -1; i++) {
                    if (parseInt(sorted_arr[i + 1]) == parseInt(sorted_arr[i])) {
                        duplicatedResults.push(sorted_arr[i]);
                        duplicateRecordsFlag = true;
                    }
                }
                if(duplicateRecordsFlag == true){
                    var eroMsg = duplicatedResults.toString();
                    p_notification(false, eb.getMessage('ERR_GRN_SERIAL_BULK_CURRENT', [eroMsg]));
                    serialCurrentDataSetValidFlag = false;
                } else {
                    serialCurrentDataSetValidFlag = true;
                }
            }
            if (serialEDateValidation != true) {
                p_notification(false, eb.getMessage('ERR_GRN_BATCH_EXPIREDATE'));
                return false;
            }
            if(serialCurrentDataSetValidFlag == true){
                $('#addGrnProductsModal').modal('hide');
                addNewProductRow(batchProducts, serialProducts);
                clearModalWindow();
                $('#itemCode').focus();
                selectedProduct = '';
                
            }
        } else {
            p_notification(false, eb.getMessage('ERR_GRN_BATCH_PRODDETAILS'));
        }

    });
    $('#addGrnProductsModal').on('click', '.batchDelete', function() {
        var deleteBID = $(this).closest('tr').attr('id');
        var deleteBQty = batchProducts[deleteBID].bQty;
        batchCount -= toFloat(deleteBQty);
        delete(batchProducts[deleteBID]);
        $('#' + deleteBID).remove();
        $('#proBatchAddNew').removeClass('hidden');
    });
/////////////////////////////////////////////////////

    function clone() {
        var sendStartingValue = quon;
        var originalQuontity = $('#qty').val();
        var poQuontity = $('.poAddItem').parents("tr").find("input[name='qty']").val();
        if (originalQuontity == "") {
            originalQuontity = poQuontity;
        }
        id = '#temp_' + idNumber;
        var quonnn = $(id).find('input[name=quontity]').val();
        var number = $(id).find('input[name=startNumber]').val();
        var startNumberStr = $(id).find('input[name=startNumber]').val();
        var prefix = $(id).find('input[name=prefix]').val();
        var pCode = locationProducts[selectedProduct].pC;
        if (quonnn == '') {
            quonnn = 0;
        }
        if (number == '') {
            p_notification(false, eb.getMessage('NO_START_NO'));
            return false;
        }
        if (number == 0) {
            p_notification(false, eb.getMessage('START_NO_ZERO'));
            return false;
        }
       
        var validationValue = true;
        validationValue = validation(number, prefix, quonnn, startNumberStr, pCode, sendStartingValue, id);
        if (validationValue == true) {
            if (toFloat(quonnn) <= originalQuontity && toFloat(quon) + toFloat(quonnn) <= originalQuontity && quonnn !== 0) {
                quon = toFloat(quon) + toFloat(quonnn);
                if (toFloat(quonnn) == originalQuontity || toFloat(quon) == originalQuontity) {

                    fixedAutoFillRows(id);
                    setValue(sendStartingValue, quonnn, id);
                }
                else {
                    var row = document.getElementById('temp_' + idNumber);
                    var table = document.getElementById('tempTbody');
                    var clone = row.cloneNode(true);
                    idNumber += 1;
                    clone.id = 'temp_' + idNumber;
                    table.appendChild(clone);
                    nextId = '#temp_' + idNumber;
                    clearAutoFillModal(nextId);
                    var dDate = new Date(Date.parse($('#deliveryDate').val()));
                    var cloneMfD = $(nextId).find('input[name=mfDateAutoFill]').datepicker({
                        onRender: function(date) {
                            return date.valueOf() > dDate.valueOf() ? ((U_DATE_R_OVERRIDE)?'':'disabled') : '';
                        }
                    }).on('changeDate', function(ev) {
                        cloneMfD.hide();
                    }).data('datepicker');

                    fixedAutoFillRows(id);

                    if (locationProducts[selectedProduct].sP == 1 && locationProducts[selectedProduct].bP == 1) {
                        $(nextId).find('input[name=mfDateAutoFill]').attr('disabled', false);
                        $(nextId).find('input[name=batch_code]').attr('disabled', false);
                    }

                    var cloneExpD = $(nextId).find('input[name=expireDateAutoFill]').datepicker({
                        onRender: function(date) {
                            return date.valueOf() < dDate.valueOf() ? ((U_DATE_R_OVERRIDE)?'':'disabled'): '';
                        }
                    }).on('changeDate', function(ev) {
                        cloneExpD.hide();
                    }).data('datepicker');

                    setValue(sendStartingValue, quonnn, id);

                }
            }
            else {
                p_notification(false, eb.getMessage('WRNG_QUON_PI'));
                return false;
            }
        }
    }

    function removeItems(arr, item) {
        for ( var i = 0; i < item; i++ ) {
            arr.pop();
        }
    }

    function clearAutoFillModal(nextId)
    {
        $(nextId).addClass('tempy');
        $(nextId).find('input[name=prefix]').val('');
        $(nextId).find('input[name=startNumber]').val('');
        $(nextId).find('input[name=quontity]').val('');
        $(nextId).find('input[name=expireDateAutoFill]').val('');
        $(nextId).find('input[name=batch_code]').val('');
        $(nextId).find('input[name=mfDateAutoFill]').val('');
        $(nextId).find('input[name=wrtyDays]').val('');
        $(nextId).find('input[name=wrtyType]').val('');
        $(nextId).find('input[name=bPrice]').val('');
    }

    function fixedAutoFillRows(id)
    {
        $(id).find('input[name=submit]').attr('disabled', true);
        $(id).find('input[name=prefix]').attr('disabled', true);
        $(id).find('input[name=startNumber]').attr('disabled', true);
        $(id).find('input[name=quontity]').attr('disabled', true);
        $(id).find('input[name=expireDateAutoFill]').attr('disabled', true);
        $(id).find('input[name=batch_code]').attr('disabled', true);
        $(id).find('input[name=mfDateAutoFill]').attr('disabled', true);
        $(id).find('input[name=wrtyDays]').attr('disabled', true);
        $(id).find('input[name=wrtyType]').attr('disabled', true);
        $(id).find('input[name=bPrice]').attr('disabled', true);
    }

    $('#tempTbody').on('click', '#submit_serial_number', function()
    {
        if ((locationProducts[selectedProduct].bP == 1) && (locationProducts[selectedProduct].sP == 1)) {
            if (!$(this).parents("tr").find("input[name='batch_code']").val()) {
                p_notification(false, eb.getMessage('ERR_NO_GRN_ADD_SERIAL_BATCH_DETAILS'));
                return false;
            }
        }
        if(isNaN($(this).parents('tr').find("input[name='startNumber']").val())){
            p_notification(false, eb.getMessage('ERR_SERIAL_START_NO'));
            $(this).parents('tr').find("input[name='startNumber']").val("");
            return false;
        }

        clone();

        if ($('#submit_serial_number').attr('disabled')) {
            $(this).parent().parent().find('#expire_date_autoFill').removeClass('white_bg');
            $(this).parent().parent().find('#mf_date_autoFill').removeClass('white_bg');
        }
    });
    function validation(startNumber, prefix, bQuntity, startNumberStr, productCode, sendStartingValue, id) {
        var strNumberLength = startNumberStr.length;
        var matchSerialNumberCount = 0;
        var serialCheckStatus = false;
        eb.ajax({
            type: 'POST',
            url: BASE_URL + '/productAPI/checkSerialProductCode',
            data: {startingNumber: startNumber, prefix: prefix, serialQuntity: bQuntity, strNumberLength: strNumberLength, productCode: productCode, },
            success: function(respond) {
                if (respond.data == true) {
                    serialCheckStatus = true;
                }
            },
            async: false
        });
        if (serialCheckStatus == true) {
            p_notification(false, eb.getMessage('ERR_GRN_SERIAL_CODE'));
            return false;
        }
        else {
            $('input.serialNumberList').each(function() {
                for (i = startNumber; i < (parseInt(startNumber) + parseInt(bQuntity)); i++) {
                    if (prefix) {
                        var startingValue = prefix + pad(i, strNumberLength);
                        if ($(this).val() == startingValue) {
                            matchSerialNumberCount++;
                        }
                    } else {
                        var startingValue = pad(i, strNumberLength);
                        if ($(this).val() == startingValue) {
                            matchSerialNumberCount++;
                        }
                    }
                }

            });
            if (matchSerialNumberCount >= 1) {
                p_notification(false, eb.getMessage('ERR_GRN_SAME_SERIAL_CODE'));
                return false;
            }
            else {
                return true;
            }


        }
    }


    function setValue(quon, quonnn, id) {

        var abc = 0;
        var bQuntity = toFloat(quonnn);
        var max = toFloat(quon) + toFloat(quonnn);
        var prefix = $(id).find('input[name=prefix]').val();
        var startNumber = parseInt($(id).find('input[name=startNumber]').val());
        var startNumberString = $(id).find('input[name=startNumber]').val();
        var expire_date = $(id).find('input[name=expireDateAutoFill]').val();
        var mf_date = $(id).find('input[name=mfDateAutoFill]').val();
        var wrnty = $(id).find('input[name=wrtyDays]').val();
        var wrntyType = $(id).find('#wrtyType').val();
        var bPrice = $(id).find('input[name=bPrice]').val();

        if (locationProducts[selectedProduct].sP == 1 && locationProducts[selectedProduct].bP == 1) {
            batchFlag = true;
            batchSerialFlag = true;
            var batchNumber = $(id).find('input[name=batch_code]').val();
            if(batchNumber in batchProducts){
                var newRowBtchQty = parseFloat(batchProducts[batchNumber].bQty) + parseFloat(bQuntity);
                batchProducts[batchNumber].bQty = newRowBtchQty;
            } else {
                batchProducts[batchNumber] = new batchProduct(batchNumber, bQuntity, mf_date, expire_date, wrnty, bPrice, batchItemIncre);
            }
            if (startNumber) {
                $('#addNewSerial tr').each(function() {
                    if (abc >= toFloat(quon) && abc < toFloat(max)) {
                        var numberLength = startNumberString.length;
                        var startingValue = pad(startNumber, numberLength);
                        if (prefix == '') {
                            $(this).find('input[name=serialCode]:first').val(startingValue);
                        } else if (prefix != '') {
                            var num = prefix + startingValue;
                            $(this).find('input[name=serialCode]:first').val(num);
                        }

                        $(this).find('input[name=serialEDate]').val(expire_date);
                        $(this).find('input[name=batchCode]').val(batchNumber);
                        $(this).find('input[name=serialWarronty]').val(wrnty);
                        $(this).find("#newSerialWarrantyType").val(wrntyType).change();
                        $(this).find('#newSerialBatchPrice').attr('data-rowNo', batchItemIncre);
                        startNumber += 1;
                    }
                    abc++;
                });
            }
            batchItemIncre++;
        }
        else {
            if (startNumber) {
                $('#addNewSerial tr').each(function() {
                    if (abc >= toFloat(quon) && abc < toFloat(max)) {
                        var numberLength = startNumberString.length;
                        var startingValue = pad(startNumber, numberLength);
                        if (prefix == '') {
                            $(this).find('input[name=serialCode]:first').val(startingValue);

                        } else if (prefix != '') {
                            var num = prefix + startingValue;
                            $(this).find('input[name=serialCode]:first').val(num);
                        }
                        $(this).find('input[name=serialEDate]').val(expire_date);
                        $(this).find('input[name=serialWarronty]').val(wrnty);
                        $(this).find("#newSerialWarrantyType").val(wrntyType).change();
                        startNumber += 1;
                    }
                    abc++;
                });
            }
        }
    }

    function pad(n, width, z) {
        z = z || '0';
        n = n + '';
        return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
    }



///////DatePicker\\\\\\\
    var nowTemp = new Date();
    var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);
    var todayDate = new Date($('#todayDate').val());


    var checkin = $('#deliveryDate').datepicker({
        onRender: function(date) {
            return date.valueOf() > todayDate.valueOf() ? ((U_DATE_R_OVERRIDE)?'':'disabled') : '';
        }
    }).on('changeDate', function(ev) {
        checkin.hide();
//        $('#deliveryDate').prop('disabled', true);
    }).data('datepicker');


    $('#deliveryDate').datepicker().on('click', function(e) {
        if (!jQuery.isEmptyObject(products) && !editModeFlag) {
            $('#deliveryDate').datepicker('hide');
            p_notification('false', eb.getMessage('ERR_GRN_CHANGE_DATE'));
        }
    });

    var batchMD = $('#newBatchMDate').datepicker({
        onRender: function(date) {
            return date.valueOf() > now.valueOf() ? ((U_DATE_R_OVERRIDE)?'':'disabled')  : '';
        }
    }).on('changeDate', function(ev) {
        batchMD.hide();
    }).data('datepicker');
    checkin.setValue(now);


/////EndOFDatePicker\\\\\\\\\

    function setDataForLocation(locationID) {
        clearProductScreen();
        selectedLocation = locationID;
        var documentType = 'GRN';
        loadDropDownFromDatabase('/productAPI/search-location-products-for-dropdown', locationID, 0, '#itemCode', '', '', documentType);
        getProductsFromLocation(selectedLocation);
        if (TAXSTATUS == false) {
            p_notification('info', eb.getMessage('ERR_GRN_ENABLE_TAX'));
        }
        $('#productAddOverlay').removeClass('overlay');
    }

    //////////////////////////////////////////

    function setProductsView(viewProductID) {
        $('.tempView').remove();
        $('#batchProductsView').addClass('hidden');
        $('#serialViewScreen').addClass('hidden');
        if (!jQuery.isEmptyObject(products[viewProductID].sProducts)) {
            $('#serialViewScreen').removeClass('hidden');
            $('#grnserialCodeView').html(products[viewProductID].pCode);
            $('#grnSerialQuantityView').html(products[viewProductID].pQuantity);
            for (var h in products[viewProductID].sProducts) {
                var viewSPr = products[viewProductID].sProducts[h];
                var cloneSerialProductView = $($('#serialViewSample').clone()).attr('id', '').removeClass('hidden').addClass('tempView');
                cloneSerialProductView.children('#serialNumberView').html(viewSPr.sCode);
                cloneSerialProductView.children('#serialWarrentyView').html(viewSPr.sWarranty);
                cloneSerialProductView.children('#serialBIDView').html(viewSPr.sBCode);
                cloneSerialProductView.children('#serialExDView').html(viewSPr.sEdate);
                cloneSerialProductView.insertBefore('#serialViewSample');
            }
        } else if (!jQuery.isEmptyObject(products[viewProductID].bProducts)) {
            $('#batchProductsView').removeClass('hidden');
            $('#batchCodeView').html(products[viewProductID].pCode);
            var batchViewQty = products[viewProductID].pQuantity / locationProducts[products[viewProductID].pID].uom[products[viewProductID].pUom].uC;
            $('#batchQuantityView').html(batchViewQty);
            for (var b in products[viewProductID].bProducts) {
                var viewBPr = products[viewProductID].bProducts[b];
                var cloneBatchProductView = $($('#proBatchViewSample').clone()).attr('id', '').removeClass('hidden').addClass('tempView');
                cloneBatchProductView.children('#batchNmbrView').html(viewBPr.bCode);
                console.log(locationProducts[products[viewProductID].pID].uom[products[viewProductID].pUom].uC);
                cloneBatchProductView.children('#batchQtyView').html(viewBPr.bQty);
                cloneBatchProductView.children('#batchMDateView').html(viewBPr.mDate);
                cloneBatchProductView.children('#batchEDateView').html(viewBPr.eDate);
                cloneBatchProductView.children('#batchWView').html(viewBPr.warnty);
                cloneBatchProductView.children('#batchUnitPrice').html(viewBPr.price);
                cloneBatchProductView.insertBefore('#proBatchViewSample');
            }
        }
        
    }


    //////////////////////////////////////////


    function setTotalTax() {
        totalTaxList = {};
        for (var k in products) {
            if (products[k].pTax != null) {
                for (var l in products[k].pTax.tL) {
                    if (totalTaxList.hasOwnProperty(l)) {
                        var curTA = totalTaxList[l].tA;
                        var newTA = (toFloat(curTA) + toFloat(products[k].pTax.tL[l].tA));
                        totalTaxList[l].tA = newTA.toFixed(2);
                    } else {
                        totalTaxList[l] = {tN: products[k].pTax.tL[l].tN, tP: products[k].pTax.tL[l].tP, tA: products[k].pTax.tL[l].tA};
                    }
                }
            }
        }
        var totalTaxHtml = "";
        for (var t in totalTaxList) {
            totalTaxHtml += totalTaxList[t].tN + "(" + totalTaxList[t].tP + "%)&nbsp" + accounting.formatMoney(totalTaxList[t].tA) + "<br>";
        }
        $('#totalTaxShow').html(totalTaxHtml);
    }

    function setGrnTotalCost() {
        var grnTotal = 0;
        for (var i in products) {
            grnTotal += toFloat(products[i].pTotal);
        }
        $('#subtotal').html(accounting.formatMoney(grnTotal));
        var deliveryAmount = $('#deliveryCharge').val();
        grnTotal += toFloat(deliveryAmount);
        $('#finaltotal').html(accounting.formatMoney(grnTotal));
    }

    $('#grnProductTable').on('keypress', function(e) {
        if (e.which == 13) {
            e.preventDefault();
            var $row = $(this);
            $row.find('#addItem').trigger('click');
        }
    });

    ////////////////////////////////////////////////////

    function addNewProductRow(batchProducts, serialProducts) {
        if (thisIsPoAddFlag == true) {
            var iC = poProducts[addingPoPro].poPC;
            var iN = poProducts[addingPoPro].poPN;
            var pT = poProducts[addingPoPro].productType;
            var poTrItemId = '#poPro_' + poProducts[addingPoPro].poPID;
            var $poItemTr = $(poTrItemId, '#grnProductTable');

            var qty = $poItemTr.find("input[name='qty']").val();
            var unitPrice = $poItemTr.find("input[name='unitPrice']").val();
            var uomConversionRate = $poItemTr.find("input[name='qty']").siblings('.uom-select').find('.selected').data('uc');
            var itemDiscount = $poItemTr.find("input[name='discount']").val();
            var itemLineTotal = $poItemTr.find('.itemLineTotal').html();
            var uomAbr = $poItemTr.find(".uom-select").find("span.selected").html();
            var uomId = $poItemTr.find(".uom-select").find("span.selected").data('uomID');
            var uPrice = (unitPrice);
            var calculatedUnitPrice = (parseFloat(uPrice) * parseFloat(uomConversionRate)).toFixed(getDecimalPlaces(uPrice));
            var uFPrice = unitPrice;
            var qt = qty;
            var uomqty = $poItemTr.find("input[name='qty']").siblings('.uomqty').val();
            var nTotal = itemLineTotal;
            var calNTotal = accounting.unformat(nTotal);
            var gD = itemDiscount;
            if (itemDiscount == '')
                gD = 0;
            var locationProductID = poProducts[addingPoPro].lPID;
            selectedProductQuantity = qty;
        } else if (compoundCostAddFlag == true) {
            var iC = locationProducts[addedProForCost].pC;
            var iN = locationProducts[addedProForCost].pN;
            var pT = locationProducts[addedProForCost].pT;
            var poTrItemId = '#poPro_' + locationProducts[addedProForCost].pID;
            var $poItemTr = $(poTrItemId, '#grnProductTable');

            var qty = $poItemTr.find("input[name='qty']").val();
            var unitPrice = $poItemTr.find("input[name='unitPrice']").val();
            var uomConversionRate = $poItemTr.find("input[name='qty']").siblings('.uom-select').find('.selected').data('uc');
            var itemDiscount = $poItemTr.find("input[name='discount']").val();
            var itemLineTotal = $poItemTr.find('.itemLineTotal').html();
            var uomAbr = $poItemTr.find(".uom-select").find("span.selected").html();
            var uomId = $poItemTr.find(".uom-select").find("span.selected").data('uomID');
            var uPrice = (unitPrice);
            var calculatedUnitPrice = (parseFloat(uPrice) * parseFloat(uomConversionRate)).toFixed(getDecimalPlaces(uPrice));
            var uFPrice = unitPrice;
            var qt = qty;
            var uomqty = $poItemTr.find("input[name='qty']").siblings('.uomqty').val();
            var nTotal = itemLineTotal;
            var calNTotal = accounting.unformat(nTotal);
            var gD = itemDiscount;
            if (itemDiscount == '')
                gD = 0;
            var locationProductID = locationProducts[addedProForCost].lPID;
            selectedProductQuantity = qty;
        } else {
            var iC = locationProducts[selectedProduct].pC;
            var iN = locationProducts[selectedProduct].pN;
            var pT = locationProducts[selectedProduct].pT;
            var uPrice = $('#unitPrice').val();
            var uomConversionRate = $("#qty").siblings('.uom-select').find('.selected').data('uc');
            var uomId = $("#qty").siblings('.uom-select').find('.selected').data('uomID');
            var calculatedUnitPrice = (parseFloat(uPrice) * parseFloat(uomConversionRate)).toFixed(getDecimalPlaces(uPrice));
            var uFPrice = $('#unitPrice').val();
            var qt = $('#qty').val();
            var uomqty = $("#qty").siblings('.uomqty').val();
            var nTotal = productLineTotal[locationProducts[selectedProduct].pID+'_'+uPrice];
            var calNTotal = accounting.unformat(nTotal);
            var gD = $('#grnDiscount').val();
            if ($('#grnDiscount').val() == '')
                gD = 0;
            var locationProductID = locationProducts[selectedProduct].lPID;
            selectedProductQuantity = $('#qty').val();
        }
        var newTrID = 'tr_' + locationProductID + '_' + rowCount;
        // var newTrID = 'tr_' + locationProductID;
        var clonedRow = $($('#preSetSample').clone()).attr('id', newTrID).addClass('addedProducts');
        clonedRow.children('#code').html(iC + ' - ' + iN);
        clonedRow.children('#quantity').html(uomqty);
        clonedRow.children('#unit').html(accounting.formatMoney(calculatedUnitPrice, null, getDecimalPlaces(calculatedUnitPrice)));
        if (thisIsPoAddFlag == true || compoundCostAddFlag == true) {
            clonedRow.children().children('#uomName').html(uomAbr);
        } else {
            clonedRow.children().children('#uomName').html(locationProducts[selectedProduct].uom[selectedProductUom].uA);
        }
        if(normalDiscountType == 'value'){
            normalDiscSymbol = $('#supplierReference').data('curencysymbol');
        } else {
            normalDiscSymbol = '%';
        }
        clonedRow.children('#disc').html(gD);
        clonedRow.children().children('#disctype').html(normalDiscSymbol);
        clonedRow.children('#ttl').html(accounting.formatMoney(nTotal));
        if (locationProducts[selectedProduct].bP == 0 && locationProducts[selectedProduct].sP == 0) {
            clonedRow.children().children('#viewSubProducts').addClass('hidden');
        }
        if (thisIsPoAddFlag == true) {
            var clonedTax = $poItemTr.find('#poTaxDiv').attr('id', '');
            clonedTax.children('#poAddTaxDiv').children('#poTaxApplied').attr('id', '');
            clonedTax.children('#poAddTaxDiv').attr('id', '');
            clonedTax.children('#poAddTaxUl').children().removeClass('poTempLi');
            clonedTax.children('#poAddTaxUl').children().children().removeClass('addNewTaxCheck');
            clonedTax.children('#poAddTaxUl').children().children().attr('disabled', true);
            clonedTax.children('#poAddTaxUl').children('#poSampleLi').remove();
            clonedTax.children('#poAddTaxUl').attr('id', '');
        } else {
            var clonedTax = $($('#addNewTaxDiv').clone()).attr('id', '');
            clonedTax.children('#addNewTax').children('#taxApplied').attr('id', '');
            clonedTax.children('#addNewTax').attr('id', '');
            clonedTax.children('#addTaxUl').children().removeClass('tempLi');
            clonedTax.children('#addTaxUl').children().children().removeClass('addNewTaxCheck');
            clonedTax.children('#addTaxUl').children().children().attr('disabled', true);
            clonedTax.children('#addTaxUl').children('#sampleLi').remove();
            clonedTax.children('#addTaxUl').attr('id', '');
        }
        clonedTax.find('#toggleSelection').addClass('hidden');
        clonedRow.children('#tax').html(clonedTax);
        clonedRow.removeClass('hidden');
        clonedRow.insertBefore('#preSetSample');
        var newProduct = locationProducts[selectedProduct];
        if (thisIsPoAddFlag == true || compoundCostAddFlag == true) {
            products[rowCount] = new product(newProduct.lPID, selectedProduct, iC, iN, qt, gD, uFPrice, uomId, calNTotal, currentItemTaxResults, batchProducts, serialProducts, pT, normalDiscountType);
            $('#poPro_' + selectedProduct).remove();
            thisIsPoAddFlag = false;
            compoundCostAddFlag = false;
        } else {
            products[rowCount] = new product(newProduct.lPID, selectedProduct, iC, iN, qt, gD, uFPrice, uomId, calNTotal, currentItemTaxResults, batchProducts, serialProducts, pT, normalDiscountType, null, newTrID);
        }
        rowCount++;
        setGrnTotalCost();
        setTotalTax();
        clearAddNewRow();
        $('#itemCode').val('').selectpicker('render');
        normalDiscSymbol = '';
    }

    /////////////////////////////////////////////////////

    function clearModalWindow() {
        $('.tempProducts').remove();
        $('.cloneSerials').remove();
        $('#grnProductCodeBatch').html('');
        $('#grnProductQuantityBatch').html('');
        $('#grnProductCodeSerial').html('');
        $('#grnProductQuantitySerial').html('');
        $('#batchAddScreen').addClass('hidden');
        $('#serialAddScreen').addClass('hidden');
        $('#addNewSerial input[type=text]').val('');
        $('#proBatchAddNew').removeClass('hidden');
        batchCount = 0;
        batchProducts = {};
        serialProducts = {};
    }

    function clearProductScreen() {
        products = new Array();
        clearAddBatchProductRow();
        clearAddNewRow();
        clearModalWindow();
        setGrnTotalCost();
        setTotalTax();
        $('.addedProducts').remove();
        locationProducts = '';
        locProductCodes = '';
        locProductNames = '';
        selectedProduct = '';
        selectedProductQuantity = '';
        selectedProductUom = '';
        batchCount = '';
    }
    ////////////////////////////////////

    function addNewProductRowByUploadDetails(data) {
        //uomConversionRate alrady 1. beacuse when upload Grn data UOM already base UOM.
        var uomConversionRate = 1;
        var uplaodedDiscType = null;
        var gD = 0;
        if(poIdsDataSet[data.locProduct.pID] != undefined && poIdsDataSet[data.locProduct.pID].poProDiscount != undefined){
            gD = poIdsDataSet[data.locProduct.pID].poProDiscount;
        }

        var productUom;
        if(data.batchData != null){
            $.each(data.batchData, function(key, value){
                batchProducts[key] = new batchProduct(value.newBCode, value.newBQty, value.newBMDate, value.newBEDate, value.newBWrnty, value.newBPrice, null);

            });
        }

        if(data.serilaData != null){
            $.each(data.serilaData, function(key, value){
                serialProducts[key] = new serialProduct(value.newSCode, value.newBWrnty, value.newBCode, value.newBEDate);
            });
        }

        $.each(data.locProduct.uom, function(key, value){
            if(data.uomID != null && data.uomID == value.uomID){
                productUom = data.uomID;
                uomConversionRate = value.uC;
            } else {
                if(value.pUDisplay == '1'){
                    productUom = value.uomID;
                    uomConversionRate = value.uC;
                }
            }

        });
    

        var uomSet = data.locProduct.uom[productUom];
        var uomId = productUom;

        var iC = data.locProduct.pC;
        var iN = data.locProduct.pN;
        var pT = data.locProduct.pT;
        var uPrice = data.uPrice;
        if (uPrice < 0) {
            p_notification(false, eb.getMessage('ERR_UNIT_PRICE_NEGATIVE'));
            return false;
        }
        var calculatedUnitPrice = (parseFloat(uPrice) * parseFloat(uomConversionRate)).toFixed(2);
        var uFPrice = data.uPrice;
        var qt = data.qnty;
        if (qt < 0) {
            p_notification(false, eb.getMessage('ERR_QNTY_NEGATIVE'));
            return false;
        }
        var uomqty = data.qnty;
        var discSymbol = '%';
        var nTotal = (parseFloat(uFPrice) * parseFloat(qt));
        if(data.discType != null){
            gD = parseFloat(data.discValue);
            if(data.discType == 1){
                nTotal = parseFloat(nTotal*(100-parseFloat(data.discValue))/100);
                discSymbol = '%';
                uplaodedDiscType = 'percentage';
                if(data.locProduct.pPDV != null){
                    p_notification(false, eb.getMessage('ERR_DISC_TYPE'));
                    return false;
                }

            } else {
                nTotal = (parseFloat(uFPrice) - parseFloat(data.discValue))* parseFloat(qt);
                discSymbol = $('#supplierReference').data('curencysymbol');
                uplaodedDiscType = 'value';
                if(data.locProduct.pPDP != null){
                    p_notification(false, eb.getMessage('ERR_DISC_TYPE'));
                    return false;
                }
            }
        } else {
            if(data.locProduct.pPDV != null){
                nTotal = (parseFloat(uFPrice) - parseFloat(data.locProduct.pPDV))* parseFloat(qt);
                discSymbol = $('#supplierReference').data('curencysymbol');
                gD = parseFloat(data.locProduct.pPDV);
                uplaodedDiscType = 'value';
            } else if(data.locProduct.pPDP != null) {
                nTotal = parseFloat(nTotal*(100-parseFloat(data.locProduct.pPDP))/100);
                discSymbol = '%';
                uplaodedDiscType = 'percentage';
                gD = parseFloat(data.locProduct.pPDP);
            }
        }
        //if this item has tax anable then need to calculate tax values
        currentItemTaxResults = null;
        if(data.locProduct.proTaxFlag == 1){
            var taxList = [];
            //need to calculate unit price for calculating tax. but we need to get unit price after the discount calculations.
            var uPriceForTax = parseFloat(nTotal);
            for(i in data.locProduct.tax){
                if(data.locProduct.tax[i] != undefined){
                    taxList.push(i);
                }
            }
            currentItemTaxResults = calculateItemCustomTax(uPriceForTax, taxList);
            //calculate total price again
            nTotal = (parseFloat(uPriceForTax) + parseFloat(currentItemTaxResults.tTA));
        }
        var calNTotal = accounting.unformat(nTotal);
        var locationProductID = data.locProduct.lPID;
        var currentPID = data.locProduct.pID;
        selectedProductQuantity = data.qnty;


        var newTrID = 'tr_' + locationProductID + '_' + rowCount;
        var clonedRow = $($('#preSetSample').clone()).attr('id', newTrID).addClass('addedProducts');
        clonedRow.children('#code').html(iC + ' - ' + iN);
        clonedRow.children('#quantity').html(uomqty);
        clonedRow.children('#unit').html(accounting.formatMoney(calculatedUnitPrice));
        clonedRow.children().children('#uomName').html(uomSet.uA);
        
        clonedRow.children('#disc').html(gD);
        clonedRow.children().children('#disctype').html(discSymbol);
        clonedRow.children('#ttl').html(accounting.formatMoney(nTotal));
        // if (locationProducts[selectedProduct].bP == 0 && locationProducts[selectedProduct].sP == 0) {
        //     clonedRow.children().children('#viewSubProducts').addClass('hidden');
        // }
        if (data.batchData == null && data.serialCode == null) {
            clonedRow.children().children('#viewSubProducts').addClass('hidden');
        }
        var clonedTax = $($('#addNewTaxDiv').clone()).attr('id', '');

        var clonedTax = $($('#addNewTaxDiv').clone()).attr('id', '');
        if (currentItemTaxResults != null) {
            clonedTax.children('#addNewTax').find('#taxApplied').removeClass('glyphicon glyphicon-unchecked');
            clonedTax.children('#addNewTax').find('#taxApplied').addClass('glyphicon glyphicon-check');
            // clonedTax.children('#addTaxUl').children('#sampleLi').remove();

            for (var i in currentItemTaxResults.tL) {
                var clonedLi = $(clonedTax.find('#sampleLi').clone()).removeClass('hidden').attr('id', 'li_' + i).addClass("tempLi");
                clonedLi.children(".taxChecks").attr('id', data.locProduct.pID + '_' + i).prop('checked', true).addClass('addNewTaxCheck');

                clonedLi.children(".taxName").html('&nbsp&nbsp' + currentItemTaxResults.tL[i].tN + '&nbsp&nbsp' + currentItemTaxResults.tL[i].tP + '%').attr('for', data.locProduct.pID + '_' + i);
                clonedLi.insertBefore(clonedTax.find('#sampleLi'));
            }
        }

        clonedTax.children('#addNewTax').children('#taxApplied').attr('id', '');
        clonedTax.children('#addNewTax').attr('id', '');
        clonedTax.children('#addTaxUl').children().removeClass('tempLi');
        clonedTax.children('#addTaxUl').children().children().removeClass('addNewTaxCheck');
        clonedTax.children('#addTaxUl').children().children().attr('disabled', true);
        clonedTax.children('#addTaxUl').children('#sampleLi').remove();
        clonedTax.children('#addTaxUl').attr('id', '');

        clonedRow.children('#tax').html(clonedTax);
        clonedRow.removeClass('hidden');
        clonedRow.insertBefore('#preSetSample');
        var newProduct = data.locProduct;

        products[rowCount] = new product(newProduct.lPID, currentPID, iC, iN, qt, gD, uFPrice, uomId, calNTotal, currentItemTaxResults, batchProducts, serialProducts, pT, uplaodedDiscType);
        rowCount++;
        setGrnTotalCost();
        setTotalTax();
        clearAddNewRow();
        $('#itemCode').val('').selectpicker('render');
        newSerialProducts = {};
        batchProducts = {};
        serialProducts = {};

    }

    ///////////////////////////////////
     function addNewProductRowByGrnEdit(productDetails, locProduct, isLoad) {
        var uplaodedDiscType = null;
        var gD = 0;
        var productUom;
        if(productDetails.bP != null){
            $.each(productDetails.bP, function(key, value){
                batchProducts[key] = new batchProduct(value.bC, value.bQ, value.bMD, value.bED, value.bW, value.bPrice, null);

            });
        }

        if(productDetails.sP != null){
            $.each(productDetails.sP, function(key, value){
                serialProducts[key] = new serialProduct(value.sC, value.sW, value.sBC, value.sED, value.sWT);
            });
        }

        // $.each(locProduct[productDetails.productID].uom, function(key, value){
        //     if(productDetails.productUomID != null && productDetails.productUomID == value.uomID){
        //         productUom = productDetails.productUomID;
        //         uomConversionRate = value.uC;
        //     } else {
        //         if(value.pUDisplay == '1'){
        //             productUom = value.uomID;
        //             uomConversionRate = value.uC;
        //         }
        //     }

        // });

        // console.log(productDetails);
        // return;

        var uomSet = locProduct[productDetails.productID].uom[productDetails.productUomID];
        uomConversionRate  = locProduct[productDetails.productID].uom[productDetails.productUomID]['uC'];
        var uomId = productDetails.productUomID;

        if (productDetails.productUomID == productDetails.GpUom) {
            if (isLoad) {
                if (uomSet['pUDisplay'] == 1) {
                    var qt = productDetails.gPQ / uomConversionRate;
                    var uomqty = parseFloat(productDetails.gPQ / uomConversionRate);
                    var uPrice = parseFloat(productDetails.gPP * uomConversionRate);
                    var calculatedUnitPrice = parseFloat(uPrice);
                    var uFPrice = parseFloat(productDetails.gPP * uomConversionRate);
                } else {
                    var qt = productDetails.gPQ;
                    var uomqty = parseFloat(productDetails.gPQ);
                    var uPrice = parseFloat(productDetails.gPP);
                    var calculatedUnitPrice = parseFloat(uPrice);
                    var uFPrice = parseFloat(productDetails.gPP);
                }
            } else {

                var qt = productDetails.gPQ;
                var uomqty = parseFloat(productDetails.gPQ);
                var uPrice = parseFloat(productDetails.gPP);
                var calculatedUnitPrice = parseFloat(uPrice);
                var uFPrice = parseFloat(productDetails.gPP);
            }
            
        } else {
            if (uomSet['pUDisplay'] == 1) {
                var qt = productDetails.gPQ  / uomConversionRate;
                var uomqty = parseFloat(productDetails.gPQ) / uomConversionRate;
                var uPrice = parseFloat(productDetails.gPP) * uomConversionRate;
                var calculatedUnitPrice = parseFloat(uPrice);
                var uFPrice = parseFloat(productDetails.gPP) * uomConversionRate;
            } else {
                puomConversionRate  = locProduct[productDetails.productID].uom[productDetails.GpUom]['uC'];
                var qt = productDetails.gPQ;
                var uomqty = parseFloat(productDetails.gPQ);
                var uPrice = parseFloat(productDetails.gPP) / uomSet['uC'];
                var calculatedUnitPrice = parseFloat(uPrice);
                var uFPrice = parseFloat(productDetails.gPP) / uomSet['uC'];
            }


        }

        var iC = productDetails.gPC;
        var iN = productDetails.gPN;
        var pT = productDetails.productType; // check
        
        var discSymbol = productDetails.gPDT;
        var nTotal = (parseFloat(uFPrice) * parseFloat(qt));
        if(productDetails.gPD != null){
            gD = parseFloat(productDetails.gPD);
            if(discSymbol == '%'){
                nTotal = parseFloat(nTotal*(100-parseFloat(productDetails.gPD))/100);
                discSymbol = '%';
                uplaodedDiscType = 'percentage';
            } else {
                console.log(productDetails.gPP);
                nTotal = ((parseFloat(productDetails.gPP)-parseFloat(gD)) * parseFloat(productDetails.gPQ));
            }

        } else {
            nTotal = (parseFloat(uFPrice))* parseFloat(qt);
            gD = 0.00;
        }
        //if tax exist. then recalcutate total value and unitprice
        currentItemTaxResults = null;
        if(productDetails.pT != null){
            var taxList = [];
            var uPriceForTax = parseFloat(nTotal);
            for(i in productDetails.pT){
                taxList.push(productDetails.pT[i].TXID);
            }
            currentItemTaxResults = calculateItemCustomTax(uPriceForTax, taxList);
            //calculate total price again
            nTotal = (parseFloat(uPriceForTax) + parseFloat(currentItemTaxResults.tTA));
        }
        var calNTotal = accounting.unformat(nTotal);
        var locationProductID = productDetails.lPID;
        var currentPID = productDetails.productID;
        selectedProductQuantity = productDetails.gPQ;

        var newTrID = 'tr_' + currentPID + '_' + rowCount;
        var clonedRow = $($('#preSetSample').clone()).attr('id', newTrID).addClass('addedProducts').attr('grnProID', productDetails.grnProductID);
        clonedRow.children('#code').html(iC + ' - ' + iN);
        clonedRow.children('#quantity').html(uomqty);
        clonedRow.children('#unit').html(calculatedUnitPrice);
        clonedRow.children().children('#uomName').html(uomSet.uA);


        clonedRow.children('#disc').html(gD);
        clonedRow.children().children('#disctype').html(discSymbol);
        clonedRow.children('#ttl').html(accounting.formatMoney(nTotal));
        if (locProduct[productDetails.productID].bP == 0 && locProduct[productDetails.productID].sP == 0) {
            clonedRow.children().children('#viewSubProducts').addClass('hidden');
        }

        var clonedTax = $($('#addNewTaxDiv').clone()).attr('id', '');
        if (currentItemTaxResults != null) {
            clonedTax.children('#addNewTax').find('#taxApplied').removeClass('glyphicon glyphicon-unchecked');
            clonedTax.children('#addNewTax').find('#taxApplied').addClass('glyphicon glyphicon-check');
            // clonedTax.children('#addTaxUl').children('#sampleLi').remove();

            for (var i in currentItemTaxResults.tL) {
                var clonedLi = $(clonedTax.find('#sampleLi').clone()).removeClass('hidden').attr('id', 'li_' + i).addClass("tempLi");
                clonedLi.children(".taxChecks").attr('id', productDetails.productID + '_' + i).prop('checked', true).addClass('addNewTaxCheck');

                clonedLi.children(".taxName").html('&nbsp&nbsp' + currentItemTaxResults.tL[i].tN + '&nbsp&nbsp' + currentItemTaxResults.tL[i].tP + '%').attr('for', productDetails.productID + '_' + i);
                clonedLi.insertBefore(clonedTax.find('#sampleLi'));
            }
        }


        clonedTax.children('#addNewTax').children('#taxApplied').attr('id', '');
        clonedTax.children('#addNewTax').attr('id', '');
        clonedTax.children('#addTaxUl').children().removeClass('tempLi');
        clonedTax.children('#addTaxUl').children().children().removeClass('addNewTaxCheck');
        clonedTax.children('#addTaxUl').children().children().attr('disabled', true);
        clonedTax.children('#addTaxUl').children('#sampleLi').remove();
        clonedTax.children('#addTaxUl').attr('id', '');


        clonedRow.children('#tax').html(clonedTax);
        clonedRow.removeClass('hidden');
        clonedRow.insertBefore('#preSetSample');

        var isEdit = true;
        products[rowCount] = new product(locationProductID, currentPID, iC, iN, productDetails.gPQ, gD, productDetails.gPP, uomId, calNTotal, currentItemTaxResults, batchProducts, serialProducts, pT, uplaodedDiscType, productDetails.productPoID,newTrID, productDetails.grnProductID, isEdit);
        rowCount++;
        setGrnTotalCost();
        setTotalTax();
        clearAddNewRow();
        $('#itemCode').val('').selectpicker('render');
        newSerialProducts = {};
        batchProducts = {};
        serialProducts = {};

    }

    function addDocRef(key, data) {

        var $cloneRow = $($('.sample').clone()).removeClass('sample hidden').addClass('documentRow');

        //set document type
        $cloneRow.find('.documentTypeId').val(key);
        $cloneRow.find('.documentTypeId').selectpicker('render');
        $cloneRow.find('.documentTypeId').attr('disabled', true);
        documentTypeArray.push(key);

        loadDocumentId($cloneRow, key);

        selectedDocID = [];

        $cloneRow.find('select.documentId').append('<optgroup label="Currently Selected"></optgroup>');
        $.each(data,function(index, value){
            $cloneRow.find('optgroup').append('<option class="slc" value="'+value.id+'">'+value.code+'</option>');
            selectedDocID.push(value.id);
        });

        $cloneRow.find('select.documentId').selectpicker('val', selectedDocID);
        $cloneRow.find('select.documentId').selectpicker('render');
        $cloneRow.find('select.documentId').selectpicker('refresh');
        $cloneRow.find('select.documentId').attr('disabled', true);

        $cloneRow.find('.saveDocument').addClass('hidden');
        $cloneRow.find('.deleteDocument').removeClass('hidden');
        $( ".documentTypeBody" ).append($cloneRow);
    }

    function setGrnProductsForEdit(preProduct, grnProduclineID) {
            var clonedAddNew = $($('#addNewItemTr').clone()).attr('id', preProduct.newTrID).addClass('tempGrnLine editOpen').attr('grnPrID',grnProduclineID);
            $('div.bootstrap-select', clonedAddNew).remove();
            $("#itemCode", clonedAddNew).empty().
                    append($("<option></option>").
                            attr("value", preProduct.pID).
                            text(preProduct.pName + '-' + preProduct.pCode));
            $('#itemCode', clonedAddNew).val(preProduct.pID);
            $('#itemCode', clonedAddNew).data('PT', preProduct.productType);
            $('#itemCode', clonedAddNew).data('PC', preProduct.pCode);
            $('#itemCode', clonedAddNew).data('PN', preProduct.pName);
            $('#itemCode', clonedAddNew).prop('disabled', true);
            $('#itemCode', clonedAddNew).selectpicker('render');
            if (preProduct.pQuantity == null) {
                preProduct.pQuantity = 0;
            }
            $('#itemCode', clonedAddNew).attr('id', '');

            console.log(preProduct);

            if (preProduct.isEdit  && locationProducts[preProduct.pID].uom[preProduct.pUom]['pUDisplay'] == 1) {
                // var qty = preProduct.pQuantity * locationProducts[preProduct.pID].uom[preProduct.pUom]['uC'];
                // var uniprice = preProduct.pUnitPrice / locationProducts[preProduct.pID].uom[preProduct.pUom]['uC'];
                var qty = preProduct.pQuantity;
                var uniprice = preProduct.pUnitPrice;
            } else {
                var qty = preProduct.pQuantity;
                var uniprice = preProduct.pUnitPrice;
            }

            $('#qty', clonedAddNew).val(qty);
            $('#qty', clonedAddNew).addUom(locationProducts[preProduct.pID].uom);
            $('#qty', clonedAddNew).parent().addClass('input-group');
            $('#qty', clonedAddNew).attr('id', '');
            //for temporary hide qty
            $('.uomqty', clonedAddNew).attr('disabled', true);
            $('#addNewUomDiv', clonedAddNew).attr('id', '');
            $('#unitPrice', clonedAddNew).val(uniprice);
            $('#unitPrice', clonedAddNew).addUomPrice(locationProducts[preProduct.pID].uom);
            // $('#unitPrice', clonedAddNew).prop('disabled', true);
            $('#unitPrice', clonedAddNew).attr('id', '');
            if (preProduct.pDiscount == null) {
                preProduct.pDiscount = 0;
            }

            var baseUom;
            for (var j in locationProducts[preProduct.pID].uom) {
                if (locationProducts[preProduct.pID].uom[j].pUBase == 1) {
                    baseUom = locationProducts[preProduct.pID].uom[j].uomID;
                }
            }

            if (locationProducts[preProduct.pID].pPDV) {
                $('#grnDiscount',clonedAddNew).val(preProduct.pDiscount).addUomPrice(locationProducts[preProduct.pID].uom, baseUom);
                $('#grnDiscount',clonedAddNew).siblings('.uom-price-select').find('.uom_price_drop_down').attr('disabled', true);
                $(".sign", clonedAddNew).text('Rs');
                $('#grnDiscount', clonedAddNew).attr('id', 'editedGrnDisc');
            }
            if (locationProducts[preProduct.pID].pPDP) {
                $('#grnDiscount',clonedAddNew).val(preProduct.pDiscount);
                $(".sign", clonedAddNew).text('%');
                $('#grnDiscount', clonedAddNew).attr('id', 'editedGrnDisc');
            }

            if (locationProducts[preProduct.pID].pPDP == null && locationProducts[preProduct.pID].pPDV == null) {
                $('#grnDiscount',clonedAddNew).attr('readonly', true);
                $('#grnDiscount', clonedAddNew).attr('id', 'editedGrnDisc');
            }
            $('#addNewTotal', clonedAddNew).html(toFloat(preProduct.pTotal).toFixed(2));
            $('#addNewTotal', clonedAddNew).attr('id', '');
            $('#addNewUom', clonedAddNew).prop('disabled', true);
            $('#addNewUom', clonedAddNew).attr('id', '');
            $('#uomAb', clonedAddNew).html(preProduct.pUom);
            $('#uomAb', clonedAddNew).attr('id', '');
            $('#addUomUl', clonedAddNew).attr('id', '');
            $('#addItem', clonedAddNew).attr('id', '').addClass('grnEditItemAdd');
            $('#removeItem', clonedAddNew).attr('id', '').addClass('grnEditItemDel').removeClass('hidden');
            //setting tax for products
            if (!jQuery.isEmptyObject(preProduct.pTax)) {
                var currentProductTax = preProduct.pTax.tL;
                $('#addNewTaxDiv', clonedAddNew).attr('id', "poTaxDiv");
                $('#addNewTax', clonedAddNew).attr('id', "poAddTaxDiv");
                $('#taxApplied', clonedAddNew).removeClass('glyphicon glyphicon-unchecked');
                $('#taxApplied', clonedAddNew).addClass('glyphicon glyphicon-check');
                $('#taxApplied', clonedAddNew).attr('id', 'poTaxApplied')
                $('#addTaxUl', clonedAddNew).attr('id', 'poAddTaxUl');
                $('.tempLi', clonedAddNew).remove();
                for (var i in currentProductTax) {
                    var clonedLi = $($('#sampleLi', clonedAddNew).clone()).removeClass('hidden').attr('id', 'li_' + i).addClass("poTempLi");
                    clonedLi.children(".taxChecks").attr('id', preProduct.pID + '_' + i).prop('checked', true).addClass('addNewTaxCheck');
                    clonedLi.children(".taxName").html('&nbsp&nbsp' + currentProductTax[i].tN + '&nbsp&nbsp' + currentProductTax[i].tP + '%').attr('for', preProduct.pID + '_' + i);
                    clonedLi.insertBefore($('#sampleLi', clonedAddNew));
                }
                $('#sampleLi', clonedAddNew).attr('id', 'poSampleLi');
            } else {
                $('#addNewTaxDiv', clonedAddNew).attr('id', "poTaxDiv");
                $('#addNewTax', clonedAddNew).attr('id', "poAddTaxDiv");
                $('#taxApplied', clonedAddNew).attr('id', 'poTaxApplied');
                $('#addTaxUl', clonedAddNew).attr('id', 'poAddTaxUl');
                $('#sampleLi', clonedAddNew).remove();
                $('.tempLi', clonedAddNew).remove();
            }
            clonedAddNew.insertBefore('#addNewItemTr');

        // //trigger focustout to poProducts-avoid recalculating costs here
        // $('tr.tempPoPro', '#grnProductTable').find(".uomPrice").trigger('focusout');

    }


    ////////////////////////////////////

    function setCompoundCostingItems(costingDetails,locationProductDetails) {
        for (var p in locationProductDetails) {
            var locPro = locationProductDetails[p];
            var costPro = costingDetails[p];
            var clonedAddNew = $($('#addNewItemTr').clone()).attr('id', 'poPro_' + locPro.pID).addClass('tempPoPro compound-cost');
            $('div.bootstrap-select', clonedAddNew).remove();
            $("#itemCode", clonedAddNew).empty().
                    append($("<option></option>").
                            attr("value", locPro.pID).
                            text(locPro.pN + '-' + locPro.pC));
            $('#itemCode', clonedAddNew).val(locPro.pID);
            $('#itemCode', clonedAddNew).data('PT', locPro.pT);
            $('#itemCode', clonedAddNew).data('PC', locPro.pC);
            $('#itemCode', clonedAddNew).data('PN', locPro.pN);
            $('#itemCode', clonedAddNew).prop('disabled', true);
            $('#itemCode', clonedAddNew).selectpicker('render');
            $('#itemCode', clonedAddNew).attr('id', '');

            
            $('#qty', clonedAddNew).val(costPro.compoundCostItemQty);
            $('#qty', clonedAddNew).addUom(locPro.uom);
            $('#qty', clonedAddNew).parent().addClass('input-group');
            $('#qty', clonedAddNew).attr('id', '');
            $('.uomqty', clonedAddNew).attr('disabled', true);
            $('#addNewUomDiv', clonedAddNew).attr('id', '');
            $('#unitPrice', clonedAddNew).val(costPro.compoundCostItemTotalUnitCost);
            $('#unitPrice', clonedAddNew).addUomPrice(locPro.uom);
            $('#unitPrice', clonedAddNew).prop('disabled', true);
            $('#unitPrice', clonedAddNew).attr('id', '');
            $('.uomPrice', clonedAddNew).attr('disabled', true);
            
            if (locPro.pPDV) {
                $('#grnDiscount',clonedAddNew).val(locPro.pPDV);
                $(".sign", clonedAddNew).text('Rs');
                $('#grnDiscount', clonedAddNew).attr('id', '');
            }
            if (locPro.pPDP) {
                $('#grnDiscount',clonedAddNew).val(locPro.pPDP);
                $(".sign", clonedAddNew).text('%');
                $('#grnDiscount', clonedAddNew).attr('id', '');
            }

            if (locPro.pPDP == null && locPro.pPDV == null) {
                $('#grnDiscount',clonedAddNew).attr('readonly', true);
                $('#grnDiscount', clonedAddNew).attr('id', '');
            }
            $('#addNewTotal', clonedAddNew).html(toFloat(0).toFixed(2));
            $('#addNewTotal', clonedAddNew).attr('id', '');
            $('#addNewUom', clonedAddNew).prop('disabled', true);
            $('#addNewUom', clonedAddNew).attr('id', '');
            $('#uomAb', clonedAddNew).html(locPro.poPUom);
            $('#uomAb', clonedAddNew).attr('id', '');
            $('#addUomUl', clonedAddNew).attr('id', '');
            $('#addItem', clonedAddNew).attr('id', '').addClass('poAddItem');
            $('#removeItem', clonedAddNew).attr('id', '').addClass('poRemoveItem').removeClass('hidden');
            //setting tax for products
            if ((!jQuery.isEmptyObject(locPro.tax))) {
                productTax = locPro.tax;
                $('#taxApplied', clonedAddNew).removeClass('glyphicon glyphicon-unchecked');
                $('#taxApplied', clonedAddNew).addClass('glyphicon glyphicon-check');
                $('.tempLi', clonedAddNew).remove();
                for (var i in productTax) {
                    var clonedLi = $($('#sampleLi', clonedAddNew).clone()).removeClass('hidden').attr('id', 'li_' + i).addClass("tempLi");
                    clonedLi.children(".taxChecks").attr('id', locPro.pID + '_' + i).prop('checked', true).addClass('addNewTaxCheck');
                    if (productTax[i].tS == 0) {
                        clonedLi.children(".taxChecks").attr('disabled', 'disabled');
                        clonedLi.children(".taxName").addClass('crossText');
                    }
                    clonedLi.children(".taxName").html('&nbsp&nbsp' + productTax[i].tN + '&nbsp&nbsp' + productTax[i].tP + '%').attr('for', locPro.pID + '_' + i);
                    clonedLi.insertBefore($('#sampleLi', clonedAddNew));
                }
            } else {
                $('#taxApplied', clonedAddNew).addClass('glyphicon glyphicon-unchecked');
                $('#addNewTax', clonedAddNew).attr('disabled', 'disabled');
            }
            clonedAddNew.insertBefore('#addNewItemTr');
        }
        //trigger focustout to poProducts-avoid recalculating costs here
        $('tr.tempPoPro', '#grnProductTable').find(".uomPrice").trigger('focusout');
    }

    function setPoProducts(preProducts) {
        for (var p in preProducts) {
            var preProduct = preProducts[p];
            var clonedAddNew = $($('#addNewItemTr').clone()).attr('id', 'poPro_' + preProduct.poPID).addClass('tempPoPro');
            $('div.bootstrap-select', clonedAddNew).remove();
            $("#itemCode", clonedAddNew).empty().
                    append($("<option></option>").
                            attr("value", preProduct.poPID).
                            text(preProduct.poPN + '-' + preProduct.poPC));
            $('#itemCode', clonedAddNew).val(preProduct.poPID);
            $('#itemCode', clonedAddNew).data('PT', preProduct.productType);
            $('#itemCode', clonedAddNew).data('PC', preProduct.poPC);
            $('#itemCode', clonedAddNew).data('PN', preProduct.poPN);
            $('#itemCode', clonedAddNew).prop('disabled', true);
            $('#itemCode', clonedAddNew).selectpicker('render');
            $('#itemCode', clonedAddNew).attr('id', '');

            if (preProduct.poPQ == null) {
                preProduct.poPQ = 0;
            }
            var poRemainQty = toFloat(preProduct.poPQ) - toFloat(preProduct.copiedQty);
            $('#qty', clonedAddNew).val(poRemainQty);
            $('#qty', clonedAddNew).addUom(locationProducts[preProduct.poPID].uom, preProduct.poPUomID);
            $('#qty', clonedAddNew).parent().addClass('input-group');
            $('#qty', clonedAddNew).attr('id', '');
            $('#addNewUomDiv', clonedAddNew).attr('id', '');
            $('#unitPrice', clonedAddNew).val(preProduct.poPP);
            $('#unitPrice', clonedAddNew).addUomPrice(locationProducts[preProduct.poPID].uom, preProduct.poPUomID);
            $('#unitPrice', clonedAddNew).prop('disabled', true);
            $('#unitPrice', clonedAddNew).attr('id', '');
            if (preProduct.poPD == null) {
                preProduct.poPD = 0;
            }
           
            if (locationProducts[preProduct.poPID].pPDV) {
                $('#grnDiscount',clonedAddNew).val(preProduct.poPD);
                $(".sign", clonedAddNew).text('Rs');
                $('#grnDiscount', clonedAddNew).attr('id', '');
            }
            if (locationProducts[preProduct.poPID].pPDP) {
                $('#grnDiscount',clonedAddNew).val(preProduct.poPD);
                $(".sign", clonedAddNew).text('%');
                $('#grnDiscount', clonedAddNew).attr('id', '');
            }

            if (locationProducts[preProduct.poPID].pPDP == null && locationProducts[preProduct.poPID].pPDV == null) {
                $('#grnDiscount',clonedAddNew).attr('readonly', true);
                $('#grnDiscount', clonedAddNew).attr('id', '');
            }
            $('#addNewTotal', clonedAddNew).html(toFloat(preProduct.poPT).toFixed(2));
            $('#addNewTotal', clonedAddNew).attr('id', '');
            $('#addNewUom', clonedAddNew).prop('disabled', true);
            $('#addNewUom', clonedAddNew).attr('id', '');
            $('#uomAb', clonedAddNew).html(preProduct.poPUom);
            $('#uomAb', clonedAddNew).attr('id', '');
            $('#addUomUl', clonedAddNew).attr('id', '');
            $('#addItem', clonedAddNew).attr('id', '').addClass('poAddItem');
            $('#removeItem', clonedAddNew).attr('id', '').addClass('poRemoveItem').removeClass('hidden');
            //setting tax for products
            if (!jQuery.isEmptyObject(preProduct.pT)) {
                var currentProductTax = preProduct.pT;
                $('#addNewTaxDiv', clonedAddNew).attr('id', "poTaxDiv");
                $('#addNewTax', clonedAddNew).attr('id', "poAddTaxDiv");
                $('#taxApplied', clonedAddNew).removeClass('glyphicon glyphicon-unchecked');
                $('#taxApplied', clonedAddNew).addClass('glyphicon glyphicon-check');
                $('#taxApplied', clonedAddNew).attr('id', 'poTaxApplied')
                $('#addTaxUl', clonedAddNew).attr('id', 'poAddTaxUl');
                $('.tempLi', clonedAddNew).remove();
                for (var i in currentProductTax) {
                    var clonedLi = $($('#sampleLi', clonedAddNew).clone()).removeClass('hidden').attr('id', 'li_' + i).addClass("poTempLi");
                    clonedLi.children(".taxChecks").attr('id', preProduct.poPID + '_' + i).prop('checked', true).addClass('addNewTaxCheck');
                    clonedLi.children(".taxName").html('&nbsp&nbsp' + currentProductTax[i].pTN + '&nbsp&nbsp' + currentProductTax[i].pTP + '%').attr('for', preProduct.poPID + '_' + i);
                    clonedLi.insertBefore($('#sampleLi', clonedAddNew));
                }
                $('#sampleLi', clonedAddNew).attr('id', 'poSampleLi');
            } else {
                $('#addNewTaxDiv', clonedAddNew).attr('id', "poTaxDiv");
                $('#addNewTax', clonedAddNew).attr('id', "poAddTaxDiv");
                $('#taxApplied', clonedAddNew).attr('id', 'poTaxApplied');
                $('#addTaxUl', clonedAddNew).attr('id', 'poAddTaxUl');
                $('#sampleLi', clonedAddNew).remove();
                $('.tempLi', clonedAddNew).remove();
            }
            clonedAddNew.insertBefore('#addNewItemTr');
        }
        //trigger focustout to poProducts-avoid recalculating costs here
        $('tr.tempPoPro', '#grnProductTable').find(".uomPrice").trigger('focusout');

    }

    function loadPO(poID) {
        $('#addSupplierBtn').prop('disabled', 1).click(function(e) {
            e.stopPropagation;
            return false;
        });
        var myDate = new Date();
        var day = myDate.getDate() < 10 ? '0' + myDate.getDate() : myDate.getDate();
        var month = (myDate.getMonth() + 1) < 10 ? '0' + (myDate.getMonth() + 1) : (myDate.getMonth() + 1);
        var year = myDate.getFullYear();
        var deliDate = eb.getDateForDocumentEdit('#deliveryDate', day, month, year);
        eb.ajax({type: 'POST',
            url: BASE_URL + '/api/po/getPoDetails',
            data: {poID: poID, toGrn: true},
            success: function(respond) {
                if (respond.data.inactiveItemFlag) {
                    p_notification(false, respond.data.errorMsg);
                    setTimeout(function(){ 
                        window.location.assign(BASE_URL + "/grn/create")
                    }, 3000);
                    return false;
                }
                var poData = respond.data.poData;
                $("#supplier").empty().
                        append($("<option></option>").
                                attr("value", poData.poSID).
                                text(poData.poSN));
                $('#supplier').val(poData.poSID);
                $('#supplier').prop('disabled', true);
                $('#supplier').selectpicker('render');
                selectedSupplier = poData.poSID;
                $('#deliveryDate').val(deliDate);//($('#todayDate').val());
                $('#supplierReference').val(poData.poSR);
                $('#supplierReference').prop('disabled', true);
                $('#comment').val(poData.poDes);
                $('#retrieveLocation').val(poData.poRLID);
                $('#retrieveLocation').prop('disabled', true);
                $('#retrieveLocation').selectpicker('render');
                if (toFloat(poData.poDC) > 0) {
                    $('#deliveryChargeEnable').prop('checked', true);
                    $('.deliCharges').removeClass('hidden');
                    $('#deliveryCharge').val(accounting.unformat(poData.poDC).toFixed(2));
                    setGrnTotalCost();
                }
                setDataForLocation(poData.poRLID);
                var currentElem = new Array();
                $.each(respond.data.locationProducts, function(key, valueObj) {
                    if (valueObj.purchase) {
                        currentElem[key] = valueObj;
                    }
                });
                locationProducts = $.extend(locationProducts, currentElem);
                setPoProducts(poData.poProducts);
                poProducts = poData.poProducts;

                $.each(respond.data.docRefData,function(key, value){
                    // alert(key);
                    addDocRef(key, value);
                    updateDocTypeDropDown();
                });
            },
            async: false
        });
    }
///////////////////////

    function setProducts(preProducts) {
        for (var p in preProducts) {
            var preProduct = preProducts[p];
            var clonedAddNew = $($('#addNewItemTr').clone()).attr('id', 'poPro_' + preProduct.poPID).addClass('tempPoPro');
            $('div.bootstrap-select', clonedAddNew).remove();
            $("#itemCode", clonedAddNew).empty().
                    append($("<option></option>").
                            attr("value", preProduct.poPID).
                            text(preProduct.poPN + '-' + preProduct.poPC));
            $('#itemCode', clonedAddNew).val(preProduct.poPID);
            $('#itemCode', clonedAddNew).data('PT', preProduct.productType);
            $('#itemCode', clonedAddNew).data('PC', preProduct.poPC);
            $('#itemCode', clonedAddNew).data('PN', preProduct.poPN);
            $('#itemCode', clonedAddNew).prop('disabled', true);
            $('#itemCode', clonedAddNew).selectpicker('render');
            $('#itemCode', clonedAddNew).attr('id', '');

            if (preProduct.poPQ == null) {
                preProduct.poPQ = 0;
            }
            var poRemainQty = toFloat(preProduct.poPQ) - toFloat(preProduct.copiedQty);
            $('#qty', clonedAddNew).val(poRemainQty);
            $('#qty', clonedAddNew).addUom(locationProducts[preProduct.poPID].uom);
            $('#qty', clonedAddNew).parent().addClass('input-group');
            $('#qty', clonedAddNew).attr('id', '');
            $('#addNewUomDiv', clonedAddNew).attr('id', '');
            $('#unitPrice', clonedAddNew).val(preProduct.poPP);
            $('#unitPrice', clonedAddNew).addUomPrice(locationProducts[preProduct.poPID].uom);
            $('#unitPrice', clonedAddNew).prop('disabled', true);
            $('#unitPrice', clonedAddNew).attr('id', '');
            if (preProduct.poPD == null) {
                preProduct.poPD = 0;
            }
            $('#grnDiscount', clonedAddNew).val(preProduct.poPD);
            $('#grnDiscount', clonedAddNew).attr('id', '');
            $('#addNewTotal', clonedAddNew).html(toFloat(preProduct.poPT).toFixed(2));
            $('#addNewTotal', clonedAddNew).attr('id', '');
            $('#addNewUom', clonedAddNew).prop('disabled', true);
            $('#addNewUom', clonedAddNew).attr('id', '');
            $('#uomAb', clonedAddNew).html(preProduct.poPUom);
            $('#uomAb', clonedAddNew).attr('id', '');
            $('#addUomUl', clonedAddNew).attr('id', '');
            $('#addItem', clonedAddNew).attr('id', '').addClass('poAddItem');
            $('#removeItem', clonedAddNew).attr('id', '').addClass('poRemoveItem').removeClass('hidden');
            //setting tax for products
            if (!jQuery.isEmptyObject(preProduct.pT)) {
                var currentProductTax = preProduct.pT;
                $('#addNewTaxDiv', clonedAddNew).attr('id', "poTaxDiv");
                $('#addNewTax', clonedAddNew).attr('id', "poAddTaxDiv");
                $('#taxApplied', clonedAddNew).removeClass('glyphicon glyphicon-unchecked');
                $('#taxApplied', clonedAddNew).addClass('glyphicon glyphicon-check');
                $('#taxApplied', clonedAddNew).attr('id', 'poTaxApplied')
                $('#addTaxUl', clonedAddNew).attr('id', 'poAddTaxUl');
                $('.tempLi', clonedAddNew).remove();
                for (var i in currentProductTax) {
                    var clonedLi = $($('#sampleLi', clonedAddNew).clone()).removeClass('hidden').attr('id', 'li_' + i).addClass("poTempLi");
                    clonedLi.children(".taxChecks").attr('id', preProduct.poPID + '_' + i).prop('checked', true).addClass('addNewTaxCheck');
                    clonedLi.children(".taxName").html('&nbsp&nbsp' + currentProductTax[i].pTN + '&nbsp&nbsp' + currentProductTax[i].pTP + '%').attr('for', preProduct.poPID + '_' + i);
                    clonedLi.insertBefore($('#sampleLi', clonedAddNew));
                }
                $('#sampleLi', clonedAddNew).attr('id', 'poSampleLi');
            } else {
                $('#addNewTaxDiv', clonedAddNew).attr('id', "poTaxDiv");
                $('#addNewTax', clonedAddNew).attr('id', "poAddTaxDiv");
                $('#taxApplied', clonedAddNew).attr('id', 'poTaxApplied');
                $('#addTaxUl', clonedAddNew).attr('id', 'poAddTaxUl');
                $('#sampleLi', clonedAddNew).remove();
                $('.tempLi', clonedAddNew).remove();
            }
            clonedAddNew.insertBefore('#addNewItemTr');
        }
        //trigger focustout to poProducts-avoid recalculating costs here
        $('tr.tempPoPro', '#grnProductTable').find(".uomPrice").trigger('focusout');

    }
    function loadDataFromCompoundCost(compoundCostID) {
        var postData = {
            id: compoundCostID
        }
        eb.ajax({
            type: 'POST',
            url: BASE_URL + '/api/grn/get-compound-cost-details',
            data: postData,
            success: function(respond) {
                if (respond.status) {
                    $("#retrieveLocation").empty().
                            append($("<option></option>").
                                    attr("value", respond.data.locationDetails.locationID).
                                    text(respond.data.locationDetails.locationName));
                    $('#retrieveLocation').val(respond.data.locationDetails.locationID);
                    $('#retrieveLocation').selectpicker('render');
                    $('#retrieveLocation').prop('disabled', true);
                    locationProducts = $.extend(locationProducts, respond.data.locationProductDetails);
                    setCompoundCostingItems(respond.data.costingDetails,respond.data.locationProductDetails);
                }
                        
            }
        });
    }


    function loadGrnForUpdate(grnID) {
         eb.ajax({type: 'POST',
            url: BASE_URL + '/grn/createdGrnDataSetByGrnID',
            data: {grnID: grnID},
            success: function(respond) {
                if(respond.status == true){
                    if (respond.data.inactiveItemFlag) {
                        p_notification(false, respond.data.errorMsg);
                        setTimeout(function(){ 
                            window.location.assign(BASE_URL + "/grn/list")
                        }, 3000);
                        return false;
                    }
                    var grnData = respond.data;
                    if (!$.isEmptyObject(respond.data.dimensionData)) {
                        dimensionData = respond.data.dimensionData;
                    }

                    $('.createPoDiv').addClass('hidden');
                    $('.updatePoDiv').removeClass('hidden');
                    $('.updatePoDiv_view').removeClass('hidden');
                    if (!$.isEmptyObject(respond.data.uploadedAttachments)) {
                        uploadedAttachments = respond.data.uploadedAttachments;
                    }
                    if (respond.data.isApproved == 1) {
                        $("#grnEdit").addClass('hidden');
                    } else {
                        $("#grnEdit").removeClass('hidden');

                    }

                    $("#supplier").empty().
                            append($("<option></option>").
                                    attr("value", grnData.gSID).
                                    text(grnData.gSN));
                    $('#supplier').val(grnData.gSID);
                    if (grnData.status != 3) {
                        $('#supplier').prop('disabled', true);
                    }
                    $('#supplier').selectpicker('render');
                    $('#addSupplierBtn').addClass('hidden');
                    selectedSupplier = grnData.gSID;
                    $('#deliveryDate').val(grnData.gD);//($('#todayDate').val());
                    $('#supplierReference').val(grnData.gSR);
                    $('#supplierReference').prop('disabled', true);
                    $('#comment').val(grnData.gC);
                    $("#retrieveLocation").empty().
                            append($("<option></option>").
                                    attr("value", grnData.retriveLocation).
                                    text(grnData.gRL));
                    $('#retrieveLocation').val(grnData.retriveLocation);
                    $('#retrieveLocation').selectpicker('render');
                    $('#retrieveLocation').prop('disabled', true);
                    if (toFloat(grnData.gDC) > 0) {
                        $('#deliveryChargeEnable').prop('checked', true);
                        $('.deliCharges').removeClass('hidden');
                        $('#deliveryCharge').val(accounting.unformat(grnData.gDC).toFixed(2));
                        setGrnTotalCost();
                    }
                    setDataForLocation(grnData.retriveLocation);
                    $('#grnNo').val(grnData.gCd);
                    $('#itemUploadView').attr('disabled', true);
                    var currentElem = new Array();
                    $.each(respond.data.locationProducts, function(key, valueObj) {
                        if (valueObj.purchase) {
                            currentElem[key] = valueObj;
                        }
                    });
                    locationProducts = $.extend(locationProducts, currentElem);
                    $.each(grnData.gProducts, function(key, value){
                        value.GpUom = value.productUomID;

                        if (!jQuery.isEmptyObject(value.bP)) {
                            for (var k in value.bP) {
                                value.bP[k].bQ = value.bP[k].bQ / parseFloat(locationProducts[value.productID].uom[value.GpUom].uC)
                            }
                        }

                       addNewProductRowByGrnEdit(value, grnData.locationProducts, true);
                    });
                    return;
                    $.each(respond.data.docRefData,function(key, value){
                        addDocRef(key, value);
                        updateDocTypeDropDown();
                    });
                } else {
                    p_notification(respond.status, respond.msg);
                }

            },
            async: false
        });
    }
///////////////////////

    //grn layout responsive changes
    $(window).bind('ready resize', function() {
        if ($(window).width() < 480) {
            $('.grn_button_grp').removeClass('btn-group');
        } else {
            $('.grn_button_grp').addClass('btn-group');
        }
    });
    if ($('#copyToPoID').val() != '') {
        selectedPO = $('#copyToPoID').val();
        $('#startByCode').prop('disabled', true);
        loadPO($('#copyToPoID').val());
    }

    var uploadedDatafieldLength;
    //grn Item upload process
    $('#productUploadBtn').on('click', function(){

        var file = document.getElementById("productFile").files[0];
        if (file) {
            var reader = new FileReader();
            productHeader = false;
            dataType = $('#productCharacterEncoding').val();
            productDelimiter = $('#productDelimiter').val();
            batchSerialType = $('#productBatchSerialType').val();
            if ($('#productHeader').is(":checked")){
                productHeader = true;
            }
            if(batchSerialType == 'none'){
                p_notification(false, eb.getMessage('ERR_GRN_UPLOAD_TYPE_SELECT'));
                return false;
            }
            reader.readAsText(file, dataType);
            reader.onload = function (evt) {
               var dataSet = evt.target.result;

                eb.ajax({type: 'POST',
                    url: BASE_URL + '/api/grn/grn-item-import',
                    data: {dataType: dataType, productDelimiter: productDelimiter, dataSet: dataSet, batchSerialType: batchSerialType, productHeader: productHeader},
                    success: function(respond) {
                        if(respond.status){
                            $('#itemImportModal').modal('hide');
                            $('#itemImportSecondModal').modal('show');
                            $('#grnItemUploadTable #grnItemUploadBody tr').remove();
                            if(respond.data['headerFlag'] == 'true'){
                                var incrementID = 0;
                                uploadedDatafieldLength = 0;
                                $.each(respond.data['dataSet'], function(key, value){
                                    var newRow = "<tr>\n\
                                                    <td class='col-lg-3'>"+ value[0] +"</td>\n\
                                                    <td class='col-lg-3'>"+ value[1] +"</td>\n\
                                                    <td class='col-lg-3'><select class='form-control grnproductimportselector selectpicker grnItemUploadSelect' data-live-search='true' id='selectID_"+incrementID+"'  name='select'>\n\
                                                        <option value='none'> Select Type </option>\n\
                                                        <option value='itemCode'> Item Code </option>\n\
                                                        <option value='batchCode'> Batch Code </option>\n\
                                                        <option value='serialCode'> Serial Code </option>\n\
                                                        <option value='quantity'> Quantity </option>\n\
                                                        <option value='unitPrice'> Unit Price </option>\n\
                                                        <option value='exDate'> Expire Date </option>\n\
                                                        <option value='manDate'> Manufacturing Date </option>\n\
                                                        <option value='wDays'> Warranty Days </option>\n\
                                                        <option value='uomVal'> UOM </option>\n\
                                                        <option value='discValue'> Discount </option>\n\
                                                        <option value='discType'> DiscountType </option>\n\
                                                        <option value='batchSalePrice'> BatchSalesPrice </option>\n\
                                                    </select> </td>\n\
                                                  </tr>";
                                    $('#grnItemUploadTable #grnItemUploadBody').append(newRow);
                                    incrementID++;
                                    uploadedDatafieldLength = incrementID;
                                });
                                $('#uploadedDataSet').val(respond.data['hiddenDataSet']);
                                $('#grn-product-importdata-button').attr('data-id', incrementID);
                            } else {
                                var incrementID = 0;
                                uploadedDatafieldLength = 0;
                                $.each(respond.data['dataSet'], function(key, value){
                                    var newRow = "<tr>\n\
                                                    <td class='col-lg-3'> No Header</td>\n\
                                                    <td class='col-lg-3'>"+ value[0] +"</td>\n\
                                                    <td class='col-lg-3'><select class='form-control grnproductimportselector selectpicker grnItemUploadSelect' data-live-search='true' id='selectID_"+incrementID+"'  name='select'>\n\
                                                        <option value='none'> Select Type </option>\n\
                                                        <option value='itemCode'> Item Code </option>\n\
                                                        <option value='batchCode'> Batch Code </option>\n\
                                                        <option value='serialCode'> Serial Code </option>\n\
                                                        <option value='quantity'> Quantity </option>\n\
                                                        <option value='unitPrice'> Unit Price </option>\n\
                                                        <option value='exDate'> Expire Date </option>\n\
                                                        <option value='manDate'> Manufacturing Date </option>\n\
                                                        <option value='wDays'> Warranty Days </option>\n\
                                                        <option value='uomVal'> UOM </option>\n\
                                                        <option value='discValue'> Discount </option>\n\
                                                        <option value='discType'> DiscountType </option>\n\
                                                        <option value='batchSalePrice'> BatchSalesPrice </option>\n\
                                                    </select> </td>\n\
                                                  </tr>";
                                    $('#grnItemUploadTable #grnItemUploadBody').append(newRow);
                                    incrementID++;
                                    uploadedDatafieldLength = incrementID;
                                });
                                $('#uploadedDataSet').val(respond.data['hiddenDataSet']);
                                $('#grn-product-importdata-button').attr('data-id', incrementID);
                            }
                        }else{
                            p_notification(respond.status, respond.msg);
                        }
                    }
                });

                // document.getElementById("fileContents").innerHTML = evt.target.result;
            }
            reader.onerror = function (evt) {
            document.getElementById("fileContents").innerHTML = "error reading file";
            }
        }
    });

    $('#grn-product-importcancel-button').on('click', function(){
        $('#itemImportSecondModal').modal('hide');
        $('#itemImportModal').modal('show');
    });


    $('#grnItemUploadTable #grnItemUploadBody').on('change','.grnproductimportselector', function() {
        $('option').prop('disabled', false); //reset all the disabled options on every change event
        $('select').each(function() { //loop through all the select elements
            var val = this.value;
            if(val != 'none'){
                $('select').not(this).find('option').filter(function() { //filter option elements having value as selected option
                    return this.value === val;
                }).prop('disabled', true); //disable those option elements
            }
        });
        var keyVal = $(this).val();
        if(keyVal == 'uomVal'){
            eb.ajax({type: 'POST',
                url: BASE_URL + '/api/grn/uom-mapping',
                data: {
                    dataArray: $('#uploadedDataSet').val(),
                    id: $(this).attr('id'),
                },
                success: function(respond) {
                    $('#itemImportUomModal').modal('show');
                    var optionsAsString;
                    $.each(respond.data.systemUomList, function(key, value){
                        optionsAsString += "<option value='" + value['uomID'] + "'>" + value['uomName'] + "</option>";
                    });
                    var uomRowCount = 0;
                    $('#grnItemUomModal #grnUploadUom tr').remove();
                    $.each(respond.data.uploadedUomList, function(key, value){
                        if(productHeader && key == 0){

                        } else {
                            var newRow = "<tr>\n\
                                        <td class='col-lg-3'>"+ value +"</td>\n\
                                        <td class='col-lg-3'><select class='form-control  selectpicker grnUOMUploadSelect' data-live-search='true' id='uom_"+uomRowCount+"' data-id='"+value+"' name='select'>\n\
                                            "+optionsAsString+"\n\
                                            </select> </td>\n\
                                        </tr>";
                            $('#grnItemUomModal #grnUploadUom').append(newRow);
                            uomRowCount++;
                        }

                    });
                    $('#saveUploadUom').attr('data-uomcount', uomRowCount);
                }
            });

        }
        if(keyVal == 'discType'){
            var discFlag = false;
            for (var i = 0; i <= uploadedDatafieldLength; i++) {
                var value = $('#selectID_'+i).val();
                if(value == 'discValue'){
                    discFlag = true;
                }
            }
            if(discFlag){
                eb.ajax({type: 'POST',
                    url: BASE_URL + '/api/grn/discount-type-mapping',
                    data: {
                        dataArray: $('#uploadedDataSet').val(),
                        id: $(this).attr('id'),
                    },
                    success: function(respond) {
                        $('#itemImportDiscTypeModal').modal('show');
                        var optionsAsString;
                        $.each(respond.data.systemDataList, function(key, value){
                            optionsAsString += "<option value='" + value['DiscID'] + "'>" + value['DiskType'] + "</option>";
                        });
                        var discTypeRowCount = 0;
                        $('#grnDiscountModal #grnUploadDiscountType tr').remove();
                        $.each(respond.data.uploadedDataList, function(key, value){
                            if(productHeader && key == 0){

                            } else {
                                var newRow = "<tr>\n\
                                            <td class='col-lg-3'>"+ value +"</td>\n\
                                            <td class='col-lg-3'><select class='form-control  selectpicker grnDiscTypeUploadSelect' data-live-search='true' id='discType_"+discTypeRowCount+"' data-id='"+value+"' name='select'>\n\
                                                "+optionsAsString+"\n\
                                                </select> </td>\n\
                                            </tr>";
                                $('#grnDiscountModal #grnUploadDiscountType').append(newRow);
                                discTypeRowCount++;
                            }

                        });
                        $('#saveUploadDiscType').attr('data-disctypecount', discTypeRowCount);
                    }
                });

            } else {
                p_notification(false, eb.getMessage('ERR_SET_DISC_VALUE'));
                var relatedId = $(this).attr('id');
                $('#'+ relatedId).val('none');
                return false;
            }

        }
    }).change();

    $('#grnUomModal1').on('click','#saveUploadUom', function(){
        var uomRows = $(this).attr('data-uomcount');

        for (var i = 0; i < uomRows; i++) {
            var value = $('#uom_'+i).val();
            var uploadedUom = $('#uom_'+i).attr('data-id');
            uomMapArray[uploadedUom] = value;
        };

        $('#itemImportUomModal').modal('hide');
    });

    $('#grnDiscountUpload').on('click','#saveUploadDiscType', function(){
        var discountRow = $(this).attr('data-disctypecount');
        for (var i = 0; i < discountRow; i++) {
            var value = $('#discType_'+i).val();
            var uploadedDiscType = $('#discType_'+i).attr('data-id');
            if(!Number.isInteger(parseInt(uploadedDiscType))){
                p_notification(false, eb.getMessage('ERR_DISC_TYPE_SELECT'));
                return false;
            }
            discTypeArray[uploadedDiscType] = value;
        };

        $('#itemImportDiscTypeModal').modal('hide');
    });

    $('#grn-product-importdata-button').on('click', function(){
        var rowCount = $(this).attr('data-id');
        var dataSet = $('#uploadedDataSet').val();
        var choices = {};
        for (var i = 0; i <= rowCount; i++) {
            var value = $('#selectID_'+i).val();
            choices[value] = i;
        };
        poIdsDataSet = [];
        if(selectedPO != undefined || selectedPO != ''){
            $.each(poProducts, function(key, value){
                poIdsDataSet[value.poPID] = {
                    locProID: value.lPID,
                    prodID: value.poPID,
                    poProDiscount: value.poPD,
                };

            });

        }

        var validStepTwo = validateGrnUploadStepTwo(choices, batchSerialType);
        if(validStepTwo){
            eb.ajax({type: 'POST',
            url: BASE_URL + '/api/grn/load-imported-data-to-grn-create',
            data: {
                dataArray: choices,
                batchSerialType: batchSerialType,
                productDelimiter: productDelimiter,
                dataType: dataType,
                dataSet: dataSet,
                locationID: locationIDForUpload,
                productHeader: productHeader,
                discTypeArray: discTypeArray,
                uomMapArray: uomMapArray,
            },
            success: function(respond) {
                if(respond.status && batchSerialType != 'batchSerialItem' && batchSerialType != 'normalItem'){
                    $.each(respond.data, function(key, value){
                        if(selectedPO == undefined || selectedPO == ''){
                            addNewProductRowByUploadDetails(value);
                        } else {
                            if(poIdsDataSet[value.locProduct.pID] != undefined){
                                $('#add-new-item-row').find('#poPro_' + value.locProduct.pID).remove();
                                addNewProductRowByUploadDetails(value);
                            }

                        }
                    });

                } else if(respond.status && batchSerialType == 'normalItem'){
                    $.each(respond.data, function(key, value){

                        var sameItemFlag = false;
                        for (var i in products) {
                            if(value.locProduct.pID == products[i].pID && toFloat(value.uPrice) == toFloat(products[i].pUnitPrice)){
                                sameItemFlag = true;
                            }
                        }

                        if(!sameItemFlag){
                            if(selectedPO == undefined || selectedPO == ''){
                                addNewProductRowByUploadDetails(value);
                            } else {
                                if(poIdsDataSet[value.locProduct.pID] != undefined){
                                    $('#add-new-item-row').find('#poPro_' + value.locProduct.pID).remove();
                                    addNewProductRowByUploadDetails(value);
                                }
                            }
                        } 

                    });

                } else if(respond.status && batchSerialType == 'batchSerialItem'){
                    $.each(respond.data, function(key, value){
                        $.each(value, function(keys, val){
                            if(selectedPO == undefined || selectedPO == ''){
                                addNewProductRowByUploadDetails(val);
                            } else {
                                if(poIdsDataSet[val.locProduct.pID] != undefined){
                                    $('#add-new-item-row').find('#poPro_' + val.locProduct.pID).remove();
                                    addNewProductRowByUploadDetails(val);
                                }
                            }
                        });
                    });
                }
                $('#itemImportSecondModal').modal('hide');
                if(respond.msg){
                    p_notification('info', respond.msg);
                }
                productHeader = false;
                uploadDocumentFlag = true;
                $('#itemUploadView').attr('disabled',true);
                }
            });
        }
    });
    $('#productUploadCancelBtn').on('click', function(){
        $('#itemImportModal').modal('hide');
    });


});

function validateGrnUploadStepTwo(choices, batchSerialType)
{
    if(choices.itemCode == undefined){
        p_notification(false, eb.getMessage('ERR_GRN_UPLOAD_ITEM_CODE_SELECT'));
        return false;
    } else if(choices.unitPrice == undefined && batchSerialType != 'serialItem') {
        p_notification(false, eb.getMessage('ERR_GRN_UPLOAD_UNIT_PRICE_SELECT'));
        return false;
    } else if(choices.uomVal == undefined) {
        p_notification(false, eb.getMessage('ERR_GRN_UPLOAD_UOM_SELECT'));
        return false;
    } else if (batchSerialType == 'batchItem' && choices.quantity == undefined){
        p_notification(false, eb.getMessage('ERR_GRN_UPLOAD_QTY_SELECT'));
        return false;
    } else if (batchSerialType == 'normalItem' && choices.quantity == undefined){
        p_notification(false, eb.getMessage('ERR_GRN_UPLOAD_QTY_SELECT'));
        return false;
    } else if(batchSerialType == 'batchItem' && choices.batchCode == undefined){
        p_notification(false, eb.getMessage('ERR_GRN_UPLOAD_BATCH_CODE_SELECT'));
        return false;
    } else if (batchSerialType == 'serialItem' && choices.serialCode == undefined){
        p_notification(false, eb.getMessage('ERR_GRN_UPLOAD_SERIAL_CODE_SELECT'));
        return false;
    } else if (batchSerialType == 'batchSerialItem' && choices.serialCode == undefined){
        p_notification(false, eb.getMessage('ERR_GRN_UPLOAD_SERIAL_CODE_SELECT'));
        return false;
    } else if(batchSerialType == 'batchSerialItem' && choices.batchCode == undefined){
        p_notification(false, eb.getMessage('ERR_GRN_UPLOAD_BATCH_CODE_SELECT'));
        return false;
    } else {
        return true;
    }
}


function clearAddBatchProductRow() {
    $('#newBatchNumber').val('');
    $('#newBatchQty').val('');
    $('#newBatchMDate').val('');
    $('#newBatchEDate').val('');
    $('#newBatchWarrenty').val('');
    $('#newBatchPrice').val('');
}
function clearAddNewRow() {
    $("#qty").val('').siblings('.uomqty,.uom-select').remove();
    $("#qty").show();
    $("#unitPrice").val('').siblings('.uomPrice,.uom-price-select').remove();
    $("#unitPrice").show();
    $('#grnDiscount').val('').siblings('.uomPrice,.uom-price-select').remove();
    $('#grnDiscount').show();
    $('#addNewTotal').html('0.00');
    $('#taxApplied', '#addNewItemTr').removeClass('glyphicon glyphicon-checked');
    $('#taxApplied', '#addNewItemTr').addClass('glyphicon glyphicon-unchecked');
    $('.tempLi', '#addNewItemTr').remove();
    $('.uomLi', '#addNewItemTr').remove();
    $('#uomAb', '#addNewItemTr').html('');
    selectedProductQuantity = '';
    selectedProductUom = '';
}
function getProductsFromLocation(locationID) {
    eb.ajax({
        type: 'POST',
        url: BASE_URL + '/api/grn/getGrnNoForLocation',
        data: {locationID: locationID},
        success: function(respond) {
            if (respond.status == false) {
                p_notification(false, respond.msg);
                $('#grnNo').val('');
                $('#refNotSet').modal('show');
            }
            else if (respond.status == true) {
                $('#grnNo').val(respond.data.refNo);
                $('#locRefID').val(respond.data.locRefID);
            }
        },
        async: false
    });
}

function setProductTypeahead(locationProducts) {
    if (!jQuery.isEmptyObject(locationProducts)) {
        $('#itemCode').children().not('.itemCodeDefaultSelect').remove();
        $.each(locationProducts, function(index, value) {
            $('#itemCode').append($("<option value='" + index + "'>" + value.pC + ' - ' + value.pN + "</option>"));
        });

        $('#itemCode').selectpicker('refresh');
    }
}

function setAddedSupplierToTheList(addedSupplier) {
    if (!jQuery.isEmptyObject(addedSupplier)) {
        var newSupplier = addedSupplier.supplierCode + '-' + addedSupplier.supplierName;
        $('#supplier').append($("<option value='" + addedSupplier.supplierID + "'>" + newSupplier + "</option>"));
        $('#supplier').selectpicker('refresh');
        $('#supplier').val(addedSupplier.supplierID);
        $('#supplier').selectpicker('render');
        selectedSupplier = addedSupplier.supplierID;
        $('#addSupplierModal').modal('hide');
    }
}

function setTaxListForProduct(productID) {
    if ((!jQuery.isEmptyObject(locationProducts[productID].tax))) {
        productTax = locationProducts[productID].tax;
        $('#taxApplied').removeClass('glyphicon glyphicon-unchecked');
        $('#taxApplied').addClass('glyphicon glyphicon-check');
        $('.tempLi').remove();
        for (var i in productTax) {
            var clonedLi = $($('#sampleLi').clone()).removeClass('hidden').attr('id', 'li_' + i).addClass("tempLi");
            clonedLi.children(".taxChecks").attr('id', productID + '_' + i).prop('checked', true).addClass('addNewTaxCheck');
            if (productTax[i].tS == 0) {
                clonedLi.children(".taxChecks").attr('disabled', 'disabled');
                clonedLi.children(".taxName").addClass('crossText');
            }
            clonedLi.children(".taxName").html('&nbsp&nbsp' + productTax[i].tN + '&nbsp&nbsp' + productTax[i].tP + '%').attr('for', productID + '_' + i);
            clonedLi.insertBefore('#sampleLi');
        }
    } else {
        $('#taxApplied').addClass('glyphicon glyphicon-unchecked');
        $('#addNewTax').attr('disabled', 'disabled');
    }

}

function validateDiscount(productID, discountType, currentDiscount, unitPrice) {
    var defaultProductData = locationProducts[productID];
    var newDiscount = 0;
    if (defaultProductData.dPEL == 1) {
        if (discountType == 'Val') {
            if (toFloat(defaultProductData.pPDV) !=0 && toFloat(currentDiscount) > toFloat(defaultProductData.pPDV)) {
                    newDiscount = toFloat(defaultProductData.pPDV);
                    p_notification(false, eb.getMessage('ERR_INVO_DISCOUNT_VALUE'));
            } else if (toFloat(defaultProductData.pPDV) == 0) {
                if (toFloat(currentDiscount) > toFloat(unitPrice)) {
                    newDiscount = toFloat(unitPrice);
                } else {
                    newDiscount = toFloat(currentDiscount);    
                }
            } else {
                newDiscount = toFloat(currentDiscount);
            }

        } else {
            if (toFloat(defaultProductData.pPDP) != 0 && toFloat(currentDiscount) > toFloat(defaultProductData.pPDP)) {
                    newDiscount = toFloat(defaultProductData.pPDP);
                    p_notification(false, eb.getMessage('ERR_INVO_DISC_PERCENTAGE'));
            } else if (toFloat(defaultProductData.pPDP) == 0) {
                if (toFloat(currentDiscount) > 100 ) {
                    newDiscount = 100;
                    p_notification(false, eb.getMessage('ERR_PI_DISC_PERCENTAGE'));   
                } else {
                    newDiscount = toFloat(currentDiscount);    
                }
            } else {
                newDiscount = toFloat(currentDiscount);
            }
        }
    } 
    return newDiscount;
}
