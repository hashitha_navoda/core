/**
 * @author ASHAN <ashan@thinkcube.com>
 * This file contains form validation
 */

var locationIn;
var locationOut;
var batchProducts = {};
var batchSerialProducts = {};
var serialProducts = {};
var transferProducts = {};
var transferSubProducts = {};
var locationProducts = {};
var enableProductInLocation = {};
var selectedPID;
var selectedTransferID;
var selectedSerialID = null;
var selectedSerialPID;

$(document).ready(function() {
    $('#date').attr('readonly', true);

    var $productTable = $("#productTable");
    var $addRowSample = $('tr.add-row.sample', $productTable);
    var getAddRow = function(productID) {

        if (productID != undefined) {
            var $row = $('table.transfer-table > tbody > tr', $productTable).filter(function() {
                return $(this).data("id") == productID
            });
            return $row;
        }

        return $('tr.add-row:not(.sample)', $productTable);
    };
    if ($productTable.length) {
        var locationsArr = _.values(LOCATION);
    }

    var locationID = $('#locationOutID').val();
    if (locationID) {
        if (!getAddRow().length) {
            $($addRowSample.clone()).removeClass('sample').appendTo($('tbody', $productTable));
        } else {
// reset row values
            var $row = getAddRow();
            $row.remove();
            $($addRowSample.clone()).removeClass('sample').appendTo($('tbody', $productTable));
        }

        locationOut = locationID;
        setProductTypeahead();
        $productTable.show();
    }

// TODO location cannot be changed after adding an item
    $('#locationOut').typeahead({source: locationsArr, updater: function(selection) {
            var key = Object.keys(LOCATION).filter(function(key) {
                return LOCATION[key] === selection;
            })[0];
            // add new blank row
            if (!getAddRow().length) {
                $($addRowSample.clone()).removeClass('sample').appendTo($('tbody', $productTable));
            } else {
// reset row values
                var $row = getAddRow();
                $row.remove();
                $($addRowSample.clone()).removeClass('sample').appendTo($('tbody', $productTable));
            }

            locationOut = key;
            eb.ajax({
                type: 'POST',
                url: BASE_URL + '/productAPI/getActiveProductsFromLocation',
                data: {locationID: key},
                success: function(respond) {
                    locationProducts = respond.data;
                    setProductTypeahead(locationProducts);
                }
            });
            $productTable.show();
            return selection;
        }});
    function setProductTypeahead() {

        var $currentRow = getAddRow();
        var documentType = 'transfer';
        loadDropDownFromDatabase('/productAPI/search-location-products-for-dropdown/serial-search', locationOut, 0, '#productCode', $currentRow, '', documentType);
        
        $("#productCode", $currentRow).selectpicker('refresh');
        $("#productCode", $currentRow).on('change', function() {
            if ($(this).val() != 0 && selectedSerialPID != $(this).val()) {
                selectedSerialPID = $(this).val();
                selectedPID = $(this).val();
                var itemMapper = $('#productCode').data('extra');
                if (itemMapper) {
                    selectedSerialID = itemMapper[selectedPID].srl_id ? itemMapper[selectedPID].srl_id : undefined;
                    selectedPID = itemMapper[selectedPID].pid;
                }

                eb.ajax({
                    type: 'POST',
                    url: BASE_URL + '/productAPI/get-location-product-details',
                    data: {productID: selectedPID, locationID: locationID},
                    success: function(respond) {
                        var currentElem = new Array();
                        locationProducts[selectedPID] = respond.data;
                        selectProductForTransfer(selectedPID);
                    }
                });
            }
        });
    }
    $.each(LOCATION, function(index, value) {
        $('#locationIn').append($("<option value='" + index + "'>" + value + "</option>"));
    });
    $('#locationIn').selectpicker('refresh');
    $('#locationIn').on('change', function() {
        var key = $(this).val();
        if (locationOut == key) {
            key = 0;
            p_notification(false, eb.getMessage('ERR_TRANSFER_SELECT_LOCAT'));
            $('#locationIn').val('').selectpicker('render');
            return null;
        }
        locationIn = key;
        enableProductInLocation = {};
    });

    function selectProductForTransfer(selectedProduct) {
        // check if product is already selected
        var $currentRow = getAddRow();
        if (transferProducts[selectedProduct] != undefined) {
            p_notification(false, eb.getMessage('ERR_DELI_ALREADY_SELECT_PROD'));
            $currentRow.remove();
            var $newRow = $($addRowSample.clone()).removeClass('sample').appendTo($('tbody', $productTable));
            $('div.bootstrap-select', $newRow).remove();
            setProductTypeahead();
            $newRow.find("#productCode").focus();
            return;
        }
        var productType = locationProducts[selectedProduct].pT;
        var productCode = locationProducts[selectedProduct].pC;
        var productName = locationProducts[selectedProduct].pN;
        var availableQuantity = (locationProducts[selectedProduct].LPQ == null) ? 0 : locationProducts[selectedProduct].LPQ;
        ClearRowFields($currentRow);
        $("input[name='availableQuantity']", $currentRow).parent().addClass('input-group');
        $("input[name='transferQuantity']", $currentRow).parent().addClass('input-group');
        $("input[name='unitPrice']", $currentRow).parent().addClass('input-group');
        $("input[name='availableQuantity']", $currentRow).siblings('.uomqty,.uom-select').remove();
        $("input[name='availableQuantity']", $currentRow).show();
        $("input[name='transferQuantity']", $currentRow).siblings('.uomqty,.uom-select').remove();
        $("input[name='transferQuantity']", $currentRow).show();
        $("#productCode", $currentRow).data('PC', productCode);
        $("#productCode", $currentRow).data('PT', productType);
        $("#productCode", $currentRow).data('PN', productName);
        $("input[name='availableQuantity']", $currentRow).val(availableQuantity).addUom(locationProducts[selectedProduct].uom);
        $("input[name='transferQuantity']", $currentRow).addUom(locationProducts[selectedProduct].uom);
        $('.uomqty').attr("id", "itemQuantity");
        $("input[name='unitPrice']", $currentRow).val('0.00');
        $("input[name='unitPrice']", $currentRow).attr("style", "text-align:right");
        $currentRow.data('id', selectedProduct);
       
        addBatchProduct(selectedProduct);
        addSerialProduct(selectedProduct);
        addSerialBatchProduct(selectedProduct);
        // check if any batch / serial products exist
        if ($.isEmptyObject(locationProducts[selectedProduct].batch) &&
                $.isEmptyObject(locationProducts[selectedProduct].serial) &&
                $.isEmptyObject(locationProducts[selectedProduct].batchSerial)) {
            $currentRow.removeClass('subproducts');
            $('.edit-modal', $currentRow).parent().addClass('hidden');
            $("td[colspan]", $currentRow).attr('colspan', 2);
            $("input[name='transferQuantity']", $currentRow).prop('readonly', false).focus();
        } else {
            $currentRow.addClass('subproducts');
            $('.edit-modal', $currentRow).parent().removeClass('hidden');
            $("td[colspan]", $currentRow).attr('colspan', 1);
            $("input[name='transferQuantity']", $currentRow).prop('readonly', true);

            if (selectedSerialID) {
                batchModalValidate(selectedProduct);
            } else {
                $("button.edit-modal", $currentRow).trigger('click');
            }
        }

        $currentRow.attr('id', 'product' + selectedProduct);
    }

    function getCurrentProductData($thisRow) {
        $("input.uomqty", $thisRow).change();
        var thisVals = {
            productID: $thisRow.data('id'),
            productCode: $("#productCode", $thisRow).data('PC'),
            productName: $("#productCode", $thisRow).data('PN'),
            productType: $("#productCode", $thisRow).data('PT'),
            availableQuantity: {
                qty: $("input[name='availableQuantity']", $thisRow).val(),
            },
            transferQuantity: {
                qty: $("input[name='transferQuantity']", $thisRow).val(),
            },
            selectedUomId: $thisRow.find('#transferQty').siblings('.uom-select').children('button').find('.selected').data('uomID')
//            unitPrice: $("input[name='unitPrice']", $thisRow).val()
        };
        thisVals.availableQuantity.qtyByBase = (thisVals.availableQuantity.qty);
        return thisVals;
    }

    function ClearRowFields($currentRow) {

        $("input[name='transferQuantity']", $currentRow).prop('readonly', false);
        $("input[name='unitPrice']", $currentRow).prop('disabled', false);
    }

    $productTable.on('click', 'button.add, button.save', function(e) {
        e.preventDefault();
        var $thisRow = $(this).parents('tr');
        var thisVals = getCurrentProductData($thisRow);
        if ((thisVals.productCode) == undefined && (thisVals.productName) == undefined) {
            p_notification(false, eb.getMessage('ERR_DELI_SELECT_PROD'));
            $("#productCode", $thisRow).focus();
            return false;
        }

        if ((thisVals.productCode.trim()) == '') {
            p_notification(false, eb.getMessage('ERR_TRANSFER_PRODCODE'));
            $("#productCode", $thisRow).focus();
            return false;
        }

        if ((thisVals.productName.trim()) == '') {
            p_notification(false, eb.getMessage('ERR_QUOT_PRNAME_EMPTY'));
            $("#productCode", $thisRow).focus();
            return false;
        }

        if (thisVals.productType != 2 && (isNaN(parseFloat(thisVals.transferQuantity.qty)) || parseFloat(thisVals.transferQuantity.qty) <= 0)) {
            p_notification(false, eb.getMessage('ERR_TRANSFER_VALQUAN'));
            $("input[name='transferQuantity']", $thisRow).focus();
            return false;
        }

        thisVals.transferQuantity.qtyByBase = (thisVals.transferQuantity.qty);
        if (thisVals.productType != 2 && (parseFloat(thisVals.transferQuantity.qtyByBase) > parseFloat(thisVals.availableQuantity.qtyByBase))) {
            p_notification(false, eb.getMessage('ERR_TRANSFER_QUAN'));
            $("input[name='transferQuantity']", $thisRow).focus().select();
            return false;
        }

//        if ((thisVals.unitPrice.trim()) == '' || (thisVals.unitPrice.trim()) <= 0) {
//            p_notification(false, eb.getMessage('ERR_TRANSFER_VALIDUPRICE'));
//            $("input[name='unitPrice']", $thisRow).focus().select();
//            return false;
//        }

// if add button is clicked
        if ($(this).hasClass('add')) {
            var $currentRow = getAddRow();
            $currentRow
                    .removeClass('add-row')
                    .find("#productCode, input[name='transferQuantity']").prop('readonly', true).end()
                    .find("input[name='unitPrice']").prop('disabled', true).end()
                    .find("button.delete").removeClass('disabled');
            $currentRow.find("input[name='transferQuantity']").change();
            $currentRow.find("#productCode").prop('disabled', true).selectpicker('render');
//            accounting.formatMoney($currentRow.find("input[name='unitPrice']").val());

            var $newRow = $($addRowSample.clone()).removeClass('sample').appendTo($('tbody', $productTable));
            $('div.bootstrap-select', $newRow).remove();
            selectedSerialPID = null;
            $newRow.find("#productCode").focus();
            setProductTypeahead();
        } else { // if save button is clicked
            $thisRow.removeClass('edit-row')
            $thisRow.find("input[name='transferQuantity']").prop('readonly', true).change();
            $thisRow.find("input[name='unitPrice']").prop('disabled', true).change();
        }

// if batch product modal is available for this product
        if ($thisRow.hasClass('subproducts')) {
            $('.edit-modal', $currentRow).parent().addClass('hidden');
            $("td[colspan]", $currentRow).attr('colspan', 2);
        }

        transferProducts[thisVals.productID] = thisVals;
        $('#locationOut').prop('readonly', true);
    });
    $productTable.on('click', 'button.edit', function(e) {

        var $thisRow = $(this).parents('tr');
        $thisRow.addClass('edit-row');
        if ($thisRow.hasClass('subproducts')) {
            $('.edit-modal', $thisRow).parent().removeClass('hidden');
            $("td[colspan]", $thisRow).attr('colspan', 1);
        } else {
            $("input[name='transferQuantity']", $thisRow).prop('readonly', false).change().focus();
            $("input[name='unitPrice']", $thisRow).prop('disabled', false).change();
        }
    });
    $productTable.on('click', 'button.delete', function(e) {

        var $thisRow = $(this).parents('tr');
        var productID = $thisRow.data('id');

        delete transferProducts[productID];
        delete transferSubProducts[productID];
        delete locationProducts[productID];

        $thisRow.remove();
    });
    function batchModalValidate(productID) {
        var productID = productID;
        var $thisParentRow = getAddRow(productID);
        var thisVals = getCurrentProductData($thisParentRow);
        var $batchTable = $("#add-product-batch .batch-table tbody");
        var qtyTotal = 0;
        var subProducts = {};
        $("input[name='transferQuantity'], input[name='transferQuantityCheck']:checked", $batchTable).each(function() {

            var $thisSubRow = $(this).parents('tr');
            var thisTransferQuantity = $(this).val();
            if ((thisTransferQuantity).trim() != "" && isNaN(parseFloat(thisTransferQuantity))) {
                p_notification(false, eb.getMessage('ERR_TRANSFER_VALQUAN'));
                $(this).focus();
                return qtyTotal = false;
            }

            thisTransferQuantity = (isNaN(parseFloat(thisTransferQuantity))) ? 0 : parseFloat(thisTransferQuantity);
            // check if trasnfer qty is greater than available qty
            var thisAvailableQuantity = $("input[name='availableQuantity']", $thisSubRow).val();
            thisAvailableQuantity = (isNaN(parseFloat(thisAvailableQuantity))) ? 0 : parseFloat(thisAvailableQuantity);
            var thisAvailableQuantityByBase = thisAvailableQuantity;
            var thisTransferQuantityByBase = thisTransferQuantity;

            if (selectedSerialID != null) {
                thisTransferQuantity = 1;
            }

            if (thisTransferQuantityByBase > thisAvailableQuantityByBase) {
                p_notification(false, eb.getMessage('ERR_TRANSFER_QUAN'));
                $(this).focus();
                return qtyTotal = false;
            }
            qtyTotal = qtyTotal + (thisTransferQuantity);
            // if a product transfer is present, prepare array to be sent to backend

            if ((thisTransferQuantity) > 0 || selectedSerialID != null) {
                var thisSubProduct = {};
                var mapKey = '';
                if ($(".batchCode", $thisSubRow).data('id')) {
                    thisSubProduct.batchID = $(".batchCode", $thisSubRow).data('id');
                    mapKey = mapKey + 'B' + thisSubProduct.batchID;
                } else if (selectedSerialID != null && !$.isEmptyObject(locationProducts[productID].batchSerial)) {
                    if (locationProducts[productID].batchSerial[selectedSerialID].PBID != null) {
                        thisSubProduct.batchID = locationProducts[productID].batchSerial[selectedSerialID].PBID;
                        mapKey = mapKey + 'B' + thisSubProduct.batchID;
                    }
                }

                if ($(".serialID", $thisSubRow).data('id')) {
                    thisSubProduct.serialID = $(".serialID", $thisSubRow).data('id');
                    mapKey = mapKey + 'S' + thisSubProduct.serialID;
                }
                else if (selectedSerialID != null && !subProducts[mapKey]) {
                    thisSubProduct.serialID = selectedSerialID;
                    mapKey = mapKey + 'S' + thisSubProduct.serialID;
                    selectedSerialID = null;
                }

                thisSubProduct.qtyByBase = thisTransferQuantity;
                subProducts[mapKey] = thisSubProduct;
            }
        });
        // to break out form $.each and exit function
        if (qtyTotal === false)
            return false;
        // ideally, since the individual batch/serial quantity is checked to be below the available qty,
        // the below condition should never become true
        if (qtyTotal > thisVals.availableQuantity.qtyByBase) {
            p_notification(false, eb.getMessage('ERR_TRANSFER_TOTALQUAN'));
            return false;
        }

        // if close modal, without add any sub product
        if(!$.isEmptyObject(subProducts)){
            transferSubProducts[productID] = subProducts;
        }
        
        var $transferQ = $("input[name='transferQuantity']", $thisParentRow);
        $transferQ.val(qtyTotal).change();
        return true;
    }


    $('#search_key').on('keyup', function() {
        $('.serialID').each(function(id, row) {
            var txt = $(row).text();
            var search_key = $('#search_key').val();

            if (txt && txt.match(new RegExp('(' + search_key + ')', 'gi'))) {
                $(this).parent('tr').removeClass('hide');
            } else if (search_key != "") {
                $(this).parent('tr').addClass('hide');
            } else if (search_key == "") {
                $(this).parent('tr').removeClass('hide');
            }
        });
    });

    $productTable.on('click', 'button.edit-modal', function(e) {

        var $thisRow = $(this).parents('tr');
        var productID = $thisRow.data('id');
        // populate data into modal ifi different product 'view subroducts' button is clicked

        if ($('#add-product-batch').data('id') != productID) {
            
            // remove existing rows (might be from other product)
            $("#batch_data tr:not(.hidden.batch_row)").remove();
            addBatchProduct(productID);
            addSerialProduct(productID);
            addSerialBatchProduct(productID);
        }

        $('#search_key').val('').keyup();
        $('#add-product-batch').data('id', productID).modal('show');
        $('#add-product-batch').unbind('hide.bs.modal');

        //check this product serail or batch
        if (!$.isEmptyObject(locationProducts[productID].serial)) {
            $('#add-product-batch .serial-auto-select').removeClass('hidden');
            $('#numberOfRow').val('');
        } else {
            $('#add-product-batch .serial-auto-select').addClass('hidden');
        }

        $('#batch-save').on('click', function(e) {
            e.preventDefault();
            // validate batch / serial products before closing modal
            if (!batchModalValidate(e)) {
                return false;
            } else {
                $('#add-product-batch').modal('hide');
            }
        });

        $('#add-product-batch').on('hide.bs.modal', function(e) {
            // validate batch / serial products before closing modal
            if (!batchModalValidate(productID)) {
                e.stopPropagation();
                return false;
            }
        });
    });
    $('#product-batch-modal').on('submit', function(e) {
        $('#add-product-batch').modal('hide');
        return false;
    });
    $('#select_serials').on('click',function(e) {
        var numberOfRow = $('#numberOfRow').val();
        if (numberOfRow != '' || numberOfRow != 0) {
            $('#batch_data > tr').each(function(key, value){
                if (numberOfRow != 0) {
                    $("input[name='transferQuantityCheck']", value).prop('checked', true);
                    numberOfRow--;
                } else {
                    $("input[name='transferQuantityCheck']", value).prop('checked', false);
                }
            });
        }    
        
    });

    $('#back').on('click', function() {
        $('#date').val('');
        $('#description').val('');
        $('#locationIn').val('');
        $('#locationIn').selectpicker('refresh');
        $('#transfer-table > tbody > tr').each(function() {
            if (!$(this).hasClass('add-row')) {
                $(this).remove();
            }
        });
        locationIn = null;
        batchProducts = {};
        batchSerialProducts = {};
        serialProducts = {};
        transferProducts = {};
        transferSubProducts = {};
        $('#locationOut').prop('readonly', false);
    });

    $('#create-transfer-form').on('submit', function(e) {
        if ($("tr.edit-row", $productTable).length > 0) {
            p_notification(false, eb.getMessage('ERR_TRANSFER_SAVEPROD'));
            return false;
        } else {
            saveTransfer();
        }
        e.preventDefault();
    });

    loadDropDownFromDatabase('/transferAPI/search-transfer-for-dropdown', "", 0, '#searchTransfer');
    $('#searchTransfer').trigger('change');
    $('#searchTransfer').on('change', function(e) {
        e.preventDefault();
        if ($(this).val() > 0 && selectedTransferID != $(this).val()) {
            selectedTransferID = $(this).val();
            var param = {transferID: selectedTransferID}
            getViewAndLoad('/transferAPI/get-transfer-by-transferID', 'transfer-list', param)
        }
    });
    $('#transferSearchClear').on('click', function() {
        getViewAndLoad('/transferAPI/get-transfer-by-transferID/' + getCurrPage(), 'transfer-list', {});
        $('#searchTransfer').val('').trigger('change');
        selectedTransferID = null;
    });

    $("form.transfer-search").submit(function(e) {
        var form_data = {
            keyword: $("#transfer-search-keyword", this).val()
        };
        eb.ajax({
            url: BASE_URL + '/transferAPI/search',
            method: 'post',
            data: form_data,
            dataType: 'json',
            success: function(data) {
                if (data.status == true) {
                    $("#transfer-list").html(data.html);
                } else {

                }
            }
        });
        e.stopPropagation();
        return false;
    });
    $("form.transfer-search button.reset").click(function(e) {
        $("#transfer-search-keyword").val('');
        $("form.transfer-search").submit();
    });
    function saveTransfer(forceLocationAdd) {

        forceLocationAdd = (forceLocationAdd == undefined) ? false : true;
        var formData = {
            transferCode: $('#transferCode').val(),
            locationOut: $('#locationOut').val(),
            locationIn: $('#locationIn').val(),
            date: $('#date').val(),
            description: $('#description').val()
        };
        $('#save').attr('disabled', true);
        if (validateTransferForm(formData)) {
            eb.ajax({
                type: 'POST',
                url: BASE_URL + '/transferAPI/saveTransferDetails',
                data: {
                    transferCode: formData.transferCode,
                    locationOutID: locationOut,
                    locationInID: locationIn,
                    products: transferProducts,
                    subProducts: transferSubProducts,
                    date: formData.date,
                    description: formData.description,
                    forceLocationAdd: enableProductInLocation,
                    locationRefID: $('#locationRefID').val()
                },
                success: function(respond) {
                    if (respond.status == true) {
                        var fileInput = document.getElementById('documentFiles');
                        if(fileInput.files.length > 0) {
                            var form_data = false;
                            if (window.FormData) {
                                form_data = new FormData();
                            }

                            form_data.append("documentID", respond.data.transferID);
                            form_data.append("documentTypeID", 15);
                            
                            for (var i = 0; i < fileInput.files.length; i++) {
                                form_data.append("files[]", fileInput.files[i]);
                            }

                            eb.ajax({
                                url: BASE_URL + '/store-files',
                                type: 'POST',
                                processData: false,
                                contentType: false,
                                data: form_data,
                                success: function(res) {
                                    console.log(res);
                                }
                            });
                        }

                        setTimeout(function(){ 
                            window.location = (BASE_URL + '/transfer/list');
                        }, 3000);
                    } else {
                        if (respond.data != undefined && respond.data.NOT_INITIALIZED != undefined) {
                            bootbox.alert(respond.msg);
                        } else {
                            p_notification(false, respond.msg);
                        }
                    }
                }
            });
        }
    }

    function validateTransferForm(formData) {
        var selectedid = "#" + document.activeElement.id;
//        var tableId = $(selectedid).closest('table').attr("id");
        if (formData.transferCode == null || formData.transferCode == "") {
            p_notification(false, eb.getMessage('ERR_TRANSFER_TRANCODE'));
            $('#save').attr('disabled', false);
            return false;
        } else if (formData.locationOut == null || formData.locationOut == "") {
            p_notification(false, eb.getMessage('ERR_TRANSFER_TRANLOCAT_FROM'));
             $('#save').attr('disabled', false);
            $('#locationOut').focus();
            return false;
        } else if (formData.locationIn == null || formData.locationIn == "") {
            p_notification(false, eb.getMessage('ERR_TRANSFER_TRANLOCAT_TO'));
            $('#locationIn').focus();
             $('#save').attr('disabled', false);
            return false;
        } else if (formData.locationIn == formData.locationOut) {
            p_notification(false, eb.getMessage('ERR_TRANSFER_CANNOT_TRANS'));
            $('#locationIn').focus();
             $('#save').attr('disabled', false);
            return false;
        } else if (formData.date == null || formData.date == "") {
            p_notification(false, eb.getMessage('ERR_TRANSFER_TRANSDATE'));
            $('#date').focus();
             $('#save').attr('disabled', false);
            return false;
        } else if (jQuery.isEmptyObject(transferProducts)) {
            p_notification(false, eb.getMessage('ERR_TRANSFER_ADDPROD'));
             $('#save').attr('disabled', false);
            return false;
        }

        return true;
    }

    $('table#transfer-table>tbody').on('keypress', 'input#itemQuantity, input#unitPrice, input#input-block-level, #transferQty,#availableQty ', function(e) {

        if (e.which == 13) {
            $('button.add', $(this).parents('tr.add-row')).trigger('click');
            e.preventDefault();
        }

    });
    /**
     * Inside Batch / Serial Product modal
     */

    function addBatchProduct(selectedProduct) {

        if ((!$.isEmptyObject(locationProducts[selectedProduct].batch))) {

            batchProduct = locationProducts[selectedProduct].batch;
            for (var i in batchProduct) {
                if ((!$.isEmptyObject(locationProducts[selectedProduct].productIDs))) {
                    if (locationProducts[selectedProduct].productIDs[i]) {
                        continue;
                    }
                }

                if (batchProduct[i].PBQ <= 0) {
                    continue;
                }

                var batchID = batchProduct[i].PBID;
                var $clonedRow = $($('.batch_row').clone());
                $clonedRow
                        .data('id', i)
                        .removeAttr('id')
                        .removeClass('hidden batch_row')
                        .addClass('remove_row');
                $(".batchCode", $clonedRow).html(batchProduct[i].PBC).data('id', batchProduct[i].PBID);
                $("input[name='availableQuantity']", $clonedRow).val(batchProduct[i].PBQ).addUom(locationProducts[selectedProduct].uom);
                $("input[name='transferQuantity']", $clonedRow).addUom(locationProducts[selectedProduct].uom);
                $("input[name='transferQuantityCheck']", $clonedRow).remove();
                // if value was entered previously, populate field
                if (transferSubProducts[selectedProduct] != undefined && transferSubProducts[selectedProduct]['B' + batchID] != undefined) {
                    $("input[name='transferQuantity']", $clonedRow).val(transferSubProducts[selectedProduct]['B' + batchID].qtyByBase);
                }
                $clonedRow.insertBefore('.batch_row');
            }
        }
    }

    function addSerialProduct(selectedProduct) {

        if ((!$.isEmptyObject(locationProducts[selectedProduct].serial))) {
            productSerial = locationProducts[selectedProduct].serial;

            var subProList = transferSubProducts;

            var list_order = new Array();
            for (var i in subProList) {
                var s_id = _.pluck(subProList[i], 'serialID');
                s_id = s_id[0];
                if (subProList[i] != undefined && subProList[i]['S' + s_id] &&  productSerial[subProList[i]['S' + s_id]['serialID']]) {
                    list_order.push(subProList[i]['S' + s_id]['serialID']);
                }
            }

            for (var i in productSerial) {
                if (!_.where(list_order, i).length) {
                    list_order.push(i);
                } else {
                    continue;
                }
            }
            $("#batch_data tr:not(.hidden)").remove();
            for (var id in list_order) {
                var i = list_order[id];

                var serialID = productSerial[i].PSID;
                var serialQty = (productSerial[i].PSS == 1) ? 0 : 1;
                var $clonedRow = $($('.batch_row').clone());
                $clonedRow
                        .data('id', i)
                        .removeAttr('id')
                        .removeClass('hidden batch_row')
                        .addClass('remove_row');
                $(".serialID", $clonedRow)
                        .html(productSerial[i].PSC)
                        .data('id', productSerial[i].PSID);
                $("input[name='availableQuantity']", $clonedRow).val(serialQty).addUom(locationProducts[selectedProduct].uom);
                $("input[name='transferQuantity']", $clonedRow).parent().remove();

                if (transferSubProducts[selectedProduct] != undefined && transferSubProducts[selectedProduct]['S' + serialID] != undefined) {
                    $("input[name='transferQuantityCheck']", $clonedRow).attr('checked', (transferSubProducts[selectedProduct]['S' + serialID].qtyByBase == 1));
                }

                $clonedRow.insertBefore('.batch_row');
            }
        }
    }

    function addSerialBatchProduct(selectedProduct) {

        if ((!$.isEmptyObject(locationProducts[selectedProduct].batchSerial))) {
            productBatchSerial = locationProducts[selectedProduct].batchSerial;
            for (var i in productBatchSerial) {

                var serialID = productBatchSerial[i].PSID;
                var batchID = productBatchSerial[i].PBID;
                var serialQty = (productBatchSerial[i].PSS == 1) ? 0 : 1;
                var $clonedRow = $($('.batch_row').clone());
                $clonedRow
                        .data('id', i)
                        .removeAttr('id')
                        .removeClass('hidden batch_row')
                        .addClass('remove_row');
                $(".serialID", $clonedRow)
                        .html(productBatchSerial[i].PSC)
                        .data('id', productBatchSerial[i].PSID);
                $(".batchCode", $clonedRow)
                        .html(productBatchSerial[i].PBC)
                        .data('id', productBatchSerial[i].PBID);
                $("input[name='availableQuantity']", $clonedRow).val(serialQty).addUom(locationProducts[selectedProduct].uom);
                $("input[name='transferQuantity']", $clonedRow).parent().remove();
                if (transferSubProducts[selectedProduct] != undefined && transferSubProducts[selectedProduct]['B' + batchID + 'S' + serialID] != undefined) {
                    $("input[name='transferQuantityCheck']", $clonedRow).attr('checked', (transferSubProducts[selectedProduct]['B' + batchID + 'S' + serialID].qtyByBase == 1));
                }

                $clonedRow.insertBefore('.batch_row');
            }
        }
    }

    var nowTemp = new Date();
    var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);
    var checkin = $('#date').datepicker({onRender: function(date) {
            return date.valueOf() > now.valueOf() ? ((U_DATE_R_OVERRIDE)?'':'disabled') : '';
        },
        format: $("#date").attr("data-dateformat"),
    }).on('changeDate', function(ev) {
        checkin.hide();
    }).data('datepicker');
    /**
     * View sub products for Transfer
     */

    var $transferViewTable = $('table.transfer-view-table');
//    $('button.subproducts-modal').on('click', function(e) {
//        var $thisRow = $(this).parents('tr');
//
//        var $modalUomBoxes = $(".uom-select", $($(this).data('target')));
//        $modalUomBoxes.replaceWith($(".uom-select", $thisRow).clone());
//
//        $modalUomBoxes = $(".uom-select", $($(this).data('target')));
//        $modalUomBoxes.each(function() {
//            var $thisQtyInput = $(this).siblings("input[type='text']");
//            var thisId = $thisQtyInput.data('id');
//
//            $thisQtyInput.removeData('conversion');
//
//            $("input[type='radio']", $(this)).each(function() {
//                var $thisRadio = $(this);
//                var thisNewId = thisId + '_' + $thisRadio.attr('id');
//                $thisRadio.attr('id', thisNewId);
//                $thisRadio.attr('name', thisId);
//                $thisRadio.siblings('label').attr('for', thisNewId);
//            });
//        });
//
//        initializeUoms($($(this).data('target')));
    //    });

    $('input.modal-close').on('click', function(e) {
        $('.modal').modal('hide');
    });

    function initializeUoms($container) {

        $('.uom-select', $container).each(function() {
            var currentProductID = $(this).attr('id');
            $('.productUom', $(this)).addUom(productsUom[currentProductID]['uoms']);
        });
        // adding uom to batch product modal
        $('.batch-serial-modal').each(function() {
            var currentProductID = $(this).attr('id').split("-")[2];
            $('.uom-select', $(this)).each(function() {
                $('.productUom', $(this)).addUom(productsUom[currentProductID]['uoms']);
            });
        });
        // when changing the uom type
//        $('.uom-select', $container).each(function() {
//            var $thisUomContainer = $(this);
//
//            $thisUomContainer.on('change', "input[type='radio']", function() {
//
//                // show abbr in the dropdown button
//                var abbr = $(this).data('abbr');
//                $('button span.selected', $thisUomContainer).text(abbr);
//
//                var $thisQtyInput = $thisUomContainer.siblings("input[type='text']");
//                var thisQtyVal = $thisQtyInput.val();
//                var thisQtyConversion = $thisQtyInput.data('conversion');
//
//                // if a conversion is not set before, it is assumed to be the base
//                thisQtyConversion = (thisQtyConversion > 0) ? thisQtyConversion : 1;
//
//                var newQtyConversion = $(this).val();
//                var newQtyVal = (thisQtyVal * thisQtyConversion) / newQtyConversion;
//
//                $thisQtyInput.data('conversion', newQtyConversion).data('abbr', abbr);
//
//                // if value is 0 and the field is not readonly, then remove leading zero
//                $thisQtyInput.val(newQtyVal);
//
//            });
//
//            $("input[type='radio'][value='1.0']", $thisUomContainer).prop('checked', true).trigger('change');
//
        //        });
    }

    initializeUoms($transferViewTable);
    //mobile view large search
    $(window).bind('ready resize', function() {
        if ($(window).width() < 480) {
            $('.transfer_search_div').removeClass('input-group');
            $('.transfer_search_div span').removeClass('input-group-btn');
        } else {
            $('.transfer_search_div').addClass('input-group');
            $('.transfer_search_div span').addClass('input-group-btn');
        }
    });
    //back_btn
    $('.tra_back_btn').on('click', function() {
        window.history.go(-1);
    });

    $('#transfer-list').on('click', '.doc_attachments', function() {
        setDataToAttachmentViewModal($(this).attr('data-transfer-related-id'));
        $('#viewAttachmentModal').modal('show');
    });

});

function setDataToAttachmentViewModal(documentID) {
    $('#doc-attach-table tbody tr').remove();
    $('#doc-attach-table tfoot div').remove();
    $('#doc-attach-table tbody div').remove();

    eb.ajax({
        type: 'POST',
        url: BASE_URL + '/get-document-related-attachement',
        data: {
            documentID: documentID,
            documentTypeID: 15
        },
        success: function(respond) {
            if (respond.status == true) {
                $('#doc-attach-table thead tr').removeClass('hidden');
                $.each(respond.data, function(index, value) {
                    tableBody = "<tr><td><a href="+value.link+">"+value.docName+"</a></td></tr>";
                    $('#doc-attach-table tbody').append(tableBody);
                });
            } else {
                $('#doc-attach-table thead tr').addClass('hidden');
                var noDataFooter = "<div class= ' col-lg-6 text-right'><label><b>No Data</b></label>";
                $('#doc-attach-table tbody').append(noDataFooter);
            }
        }
    });
}

