/**
 * @author Prathap Weerasinghe <prathap@thinkcube.net>
 * This page contains functions related to notification service
 */

{
    var d = new Date();
    var yy = d.getFullYear();
    var mm = (d.getMonth() + 1 < 10) ? d.getMonth() + 1 : d.getMonth() + 1;
    var dd = (d.getDate() < 10) ? "0" + d.getDate() : d.getDate();
    var date = yy + "-" + mm + "-" + dd;
    var main_div;
    function notification(msg, title, type) {
        this.msg = msg;
        this.title = title;
        this.type = type;
        this.que = new Array();
        this.add_notification = addToQue;
        this.showNotifications = showNotifications;
        if (msg) {
            this.showMessage = showMessage();
        }
        function message(title, body, type, link) {
            this.title = title;
            this.body = body;
            this.type = type;
            this.link = link;
        }
        function addToQue(title, body, type, link) {
            var msg = new message(title, body, type, link);
            this.que.push(msg);
        }
        function showMessage() {
            switch (type) {
                case "error":
                    cls = "alert-danger";
                    break;
                default:
                case "info":
                    cls = "alert-info";
                    break;
                case "warning":
                    cls = "alert-warning";
                    cls = "alert-danger";
                    break;
                case "success":
                    cls = "alert-success";
                    break;
            }
            div = "<div class='disabl_when_print msg_alert " + cls + "'>" +
                    "<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>Ok</button>";
            if (title) {
                div += "<strong>" + title + "&nbsp;:&nbsp;</strong>";
            }
            div += this.msg;
            div += "</div>";
            if (null == main_div) {
                main_div = document.createElement("div");
                main_div.className = "message";
            } else {
                $(main_div).show();
            }
            $(main_div).append(div);
            $(main_div).delay(5000).fadeOut(function() {
                $(this).html("");
            });
            $("body").prepend(main_div);
        }

        function showNotifications() {

            for (var i in this.que) {
                switch (this.que[i].type) {
                    case "over_due_purchase_inv":
                        cls = "fa fa-file-o";
                        break;
                    case "over_due_inv":
                        cls = "fa fa-file-o";
//                        cls = "fa fa-flag-o";
                        break;
                    case "over_due_reorder_level":
                        cls = "fa fa-arrow-down";
                        break;
                    case "over_due_inventory_level":
                        cls = "fa fa-arrow-down error";
//                        cls = "fa fa-minus-circle";
                        break;
                    case "warning":
                        cls = "alert-warning";
                        cls = "alert-danger";
                        break;
                    case "success":
                        cls = "alert-success";
                        break;
                    default:
                        cls = "alert-info";
                }
                var link = this.que[i].link == undefined ? '#' : this.que[i].link;
                noti_message = "<li><a class=" + this.que[i].type + " href='" + link + "'>" +
                        "<p class='heading'><i class='" + cls + "'></i>" + this.que[i].title + "</p>" +
                        "<div class='notifi-body'><span class='light_color'>" + this.que[i].body + "</span>" +
                        "</div></a></li>";
                $("#notification_body").prepend(noti_message);
            }

            if (this.que.length > 0) {
                if (this.que.length < 10) {
                    $("#notifi_badge_div").append("<span class='badge'>0" + this.que.length + "</span>");
                } else {
                    $("#notifi_badge_div").append("<span class='badge'>" + this.que.length + "</span>");
                }
            } else {
                message_error = "<li><a href='#'>" +
                        "<p class='heading'> No messages</p>" +
                        "<span class='light_color'>No messges to display</span>" +
                        "</a></li>";
                $("#notification_body").prepend(message_error);
            }
        }
    }
}
$(window).bind("load", function() {
    eb.ajax({
        type: "POST",
        async: false,
        url: '/notification/getTodayNotification',
        data: {date: date},
        success: function(response) {
            var a = new notification();
            if (response != "false") {
                if (response.invoice) {
                    for (var i in response.invoice) {
                        var res = response.invoice[i];
                        switch (res.Type) {
                            case "Invoice":
                                a.add_notification("A scheduled Invoice!", "Invoice named \"" + res.name + "\" is scheduled to issue today.", "reminder", BASE_URL + "/invoice/index/rec/" + res.id);
                                break;
                            case "SalesOrder":
                                a.add_notification("A scheduled Sales Order!", "Sales Order named \"" + res.name + "\" is scheduled to issue today.", "reminder", BASE_URL + "/salesOrders/index/rec/" + res.id);
                                break;
                            case "Overdue":
                                a.add_notification("An overdue Invoice!", "Invoice \"" + res.salesInvoiceCode + "\" issued to \"" + res.customerName + "\" has reached the due date.", "over_due_inv", "/invoice/invoiceView/" + res.salesInvoiceID);
                                break;

                        }
                    }
                }
                if (response.purchaseInvocie) {
                    for (var i in response.purchaseInvocie) {
                        var res = response.purchaseInvocie[i];
                        switch (res.Type) {
                            case "Overdue":
                                a.add_notification("An overdue Purchase Invoice!", "Purchase Invoice \"" + res.purchaseInvoiceCode + "\" issued to \"" + res.supplierName + "\" has reached the due date.", "over_due_purchase_inv", "/pi/view/" + res.purchaseInvoiceID);
                                break;
                        }
                    }
                }
                if (response.reorderLevel) {
                    for (var i in response.reorderLevel) {
                        var res = response.reorderLevel[i];
                        switch (res.Type) {
                            case "Overdue":
                                a.add_notification("Inventory stock levels low!", "Item \"" + res.productName + "\" has reached the re-order level.", "over_due_reorder_level");
                                break;
                        }
                    }
                }
                if (response.inventoryLevel) {
                    for (var i in response.inventoryLevel) {
                        var res = response.inventoryLevel[i];
                        switch (res.Type) {
                            case "Overdue":
                                a.add_notification("Minimum stock level reached!", "Item \"" + res.productName + "\" has reached the minimum inventory level.", "over_due_inventory_level");
                                break;
                        }
                    }
                }
            }
            a.showNotifications();
        }
    });


});