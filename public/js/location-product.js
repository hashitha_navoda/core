/**
 * @author ASHAN <ashan@thinkcube.com>
 * This file contains form validation
 */

var allProducts = {};

$(document).ready(function () {

    function LocationProduct() {
        this.minInvLevel = 0;
        this.reorderLevel = 0;
        this.sellingPrice = 0;
        this.discountPercentage = 0;
        this.discountValue = 0;
    }

    $('#locationTable').hide();
    $('.addProductLocationDetails').hide();
    var productLocation = BASE_URL + '/productAPI/getProducts';
    $('#locations').attr('data-live-search', 'true').selectpicker();
    $('#locations').change(function () {

        if ($(this).val() == 0) {
            $('#locationTable').hide();
            return false;
        }

        $('#locationTable').show();
        $('#tbl_data').empty();
        eb.ajax({
            url: productLocation,
            type: 'POST',
            data: {
                locationID: $('#locations').val()
            },
            dataType: 'json',
            async: false,
            success: function (res) {
                if (res.status) {
                    for (var j in res.data) {
                        var thisProduct = res.data[j];

                        var locationProductDiscountValue;
                        var locationProductDiscountPercentage;
                        var productDiscountValue;
                        var productDiscountPercentage;

                        if (thisProduct.locationDiscountPercentage == null) {
                            locationProductDiscountPercentage = 0;
                        } else {
                            locationProductDiscountPercentage = thisProduct.locationDiscountPercentage;
                        }
                        if (thisProduct.locationDiscountValue == null) {
                            locationProductDiscountValue = 0;
                        } else {
                            locationProductDiscountValue = thisProduct.locationDiscountValue;
                        }
                        if (thisProduct.productDiscountValue == null) {
                            productDiscountValue = 0;
                        } else {
                            productDiscountValue = thisProduct.productDiscountValue;
                        }
                        if (thisProduct.productDiscountPercentage == null) {
                            productDiscountPercentage = 0;
                        } else {
                            productDiscountPercentage = thisProduct.productDiscountPercentage;
                        }
                        var $td = '';
                        if (thisProduct.productDiscountEligible == 1) {
                            $td = "<td><div class='input-group'><input type='text' name='discountPercentage' value=" + locationProductDiscountPercentage + " class='form-control'><span class='input-group-addon'>%</span></div></td>" +
                                    "<td><div class='input-group'><input type='text' name='discountValue' value=" + parseFloat(locationProductDiscountValue).toFixed(2) + " class='form-control'><span class='input-group-addon'>LKR</span></div></td>";
                        } else {
                            $td = "<td class='text-center'>-</td>" +
                                    "<td class='text-center'>-</td>";
                        }

                        var productCode = (thisProduct.productCode !== null) ? thisProduct.productCode : '-';
                        var productName = (thisProduct.productName !== null) ? thisProduct.productName : '-';

                        if (thisProduct.productCode !== null || thisProduct.productName !== null) {
                            var $tr = $("<tr data-id=\"" + thisProduct.locationProductID + "\">" +
                                    "<td>" + productCode + "</td>" +
                                    "<td>" + productName + "</td>" +
                                    "<td><input type='text' name='minInvLevel' value=" + parseFloat(thisProduct.locationProductMinInventoryLevel).toFixed(2) + " class='form-control'></td>" +
                                    "<td><input type='text' name='reorderLevel' value=" + parseFloat(thisProduct.locationProductReOrderLevel).toFixed(2) + " class='form-control'></td>" +
                                    "<td><input type='text' name='sellingPrice' value=" + parseFloat(thisProduct.defaultSellingPrice).toFixed(2) + " class='form-control'></td>" +
                                    $td +
                                    "</tr>");
                            $('#tbl_data').append($tr);
                        }

                        if (thisProduct.productDiscountValue == null || thisProduct.locationDiscountValue == null) {
                            $("[name='discountValue']", $tr).prop('readonly', 1);
                        } 
                        if (thisProduct.productDiscountPercentage == null || thisProduct.locationDiscountPercentage == null) {
                            $("[name='discountPercentage']", $tr).prop('readonly', 1);
                        }

                        $("[name='discountPercentage']", $tr).data('max', productDiscountPercentage);
                        $("[name='discountValue']", $tr).data('max', parseFloat(productDiscountValue).toFixed(2));
                    }
                } else {
                    p_notification(res.status, res.msg);
                }
                $('.editProductLocationDetails').hide();
            }
        });
    });

    var $locationProductForm = $("form[name='location-product-mass']");

    function validateLocationProductDetails() {

        var success = true;
        var $productRows = $("table.location-product-table tbody tr", $locationProductForm);

        $("[name='minInvLevel'],  [name='reorderLevel'], [name='sellingPrice'], [name='discountPercentage'], [name='discountValue']", $productRows).each(function () {
            var thisVal = $(this).val();
            if (thisVal.trim() != "" && (isNaN(parseFloat(thisVal)) || parseFloat(thisVal) < 0)) {
                p_notification(false, eb.getMessage('ERR_LOCATPROD_FIELD'));
                success = false;
                $(this).focus();
                return false;
            }

        });

        // check if location wise discount is greater than product wise discount
        if (success) {
            $("[name='discountPercentage'], [name='discountValue']", $productRows).filter(':not([readonly])').each(function () {
                var thisVal = $(this).val();
                var thisName = $(this).attr('name');
                var maxVal = $(this).data('max');
                var sellingPriceVal = $("[name='sellingPrice']", $(this).parents('tr')).val();
                if (parseFloat(maxVal) != 0) {
                    if (parseFloat(thisVal) > parseFloat(maxVal)) {
                        p_notification(false, "Maximum Discount " + thisName.replace('discount', '') + " allowed for this item is " + maxVal);//MESSAGE['']);
                        success = false;
                        $(this).focus();
                        return false;
                    }
                } else if (thisName == "discountPercentage" && parseFloat(maxVal) == 0) {
                    if (parseFloat(thisVal) > 100 || parseFloat(thisVal) < 0) {
                        p_notification(false, "Discount " + thisName.replace('discount', '') + " range allowed for this item is 0 - 100%");//MESSAGE['']);
                        success = false;
                        $(this).focus();
                        return false;
                    }
                } else if (thisName == "discountValue" && parseFloat(maxVal) == 0) {
                    if (parseFloat(thisVal) > parseFloat(sellingPriceVal) || parseFloat(thisVal) < 0) {
                        p_notification(false, "Discount " + thisName.replace('discount', '') + " range allowed for this item is 0 - " + sellingPriceVal);//MESSAGE['']);
                        success = false;
                        $(this).focus();
                        return false;
                    }
                }

            });
        }

        // check if min. inventory level is greater than the reorder level
        if (success) {
            $("[name='minInvLevel']", $productRows).each(function () {
                var thisVal = $(this).val();
                var $thisReOrderLevel = $("[name='reorderLevel']", $(this).parents('tr'));

                if (parseFloat(thisVal) > parseFloat($thisReOrderLevel.val())) {
                    p_notification(false, eb.getMessage('ERR_LOCATPROD_REORDERLEVEL'));
                    success = false;
                    $thisReOrderLevel.focus();
                    return false;
                }

            });
        }

        // check if discount value greater than selling price
        // only if selling price is set (greater than 0)
        if (success) {
            $("[name='discountValue']:not([readonly])", $productRows).each(function () {
                var thisVal = $(this).val();
                var sellingPriceVal = $("[name='sellingPrice']", $(this).parents('tr')).val();


                if (parseFloat(sellingPriceVal) > 0 && parseFloat(thisVal) > parseFloat(sellingPriceVal)) {
                    p_notification(false, eb.getMessage('ERR_LOCATPROD_ITEM_DISVAL'));
                    success = false;
                    $(this).focus();
                    return false;
                }

            });
        }

        return success;
    }

    var addAllProduct = BASE_URL + '/productAPI/addProductLocationDetails';

    $locationProductForm.on('submit', function (e) {

        // validate all inputs
        // error message is returned from the function
        if (!validateLocationProductDetails()) {
            e.stopPropagation();
            return false;
        }

        $("table.location-product-table tbody tr", $locationProductForm).each(function () {
            var productID = $(this).data('id');
            allProducts[productID] = new LocationProduct();
            allProducts[productID].minInvLevel = parseFloat($("[name='minInvLevel']", $(this)).val());
            allProducts[productID].reorderLevel = parseFloat($("[name='reorderLevel']", $(this)).val());
            allProducts[productID].sellingPrice = parseFloat($("[name='sellingPrice']", $(this)).val());
            allProducts[productID].discountPercentage = (isNaN(parseFloat($("[name='discountPercentage']", $(this)).val()))) ? null : parseFloat($("[name='discountPercentage']", $(this)).val());
            allProducts[productID].discountValue = (isNaN(parseFloat($("[name='discountValue']", $(this)).val()))) ? null : parseFloat($("[name='discountValue']", $(this)).val());
        });

        eb.ajax({
            url: addAllProduct,
            type: 'POST',
            data: {
                locationID: $('#locations').val(),
                productData: allProducts
            },
            dataType: 'json',
            async: false,
            success: function (data) {
                if (data.status == true) {
                    p_notification(true, data.msg);
                } else {
                    p_notification(false, data.msg);
                }
            }
        });

        e.stopPropagation();
        return false;
    });

//    $('#save').on('click', function() {
//        var Product_data = {};
//        for (var i in allProduct) {
//            if (allProduct[i].quantity != 0 || allProduct[i].reorderLevel != 0 || allProduct[i].sellingPrice != 0 || allProduct[i].discountPercentage != 0 || allProduct[i].discountValue != 0) {
//                Product_data[i] = JSON.stringify(allProduct[i]);
//                alert(allProduct[i].quantity);
//            }
//        }
//        eb.ajax({
//            url: addAllProduct,
//            type: 'POST',
//            data: {
//                productID: $('#productID').val(),
//                productData: JSON.stringify(Product_data)
//            },
//            dataType: 'json',
//            async: false,
//            success: function(res) {
//                if (res[0] = "true") {
//                    notification($('#productID').val() + " this product detaisls successfuly added to all location", "Success", "success");
//                    window.location.assign(BASE_URL + '/product/index');
//                } else {
//                    notification("failed to add " + productID + " this product detaisls to " + locationID + " this loaction", "Error", "warning");
//                }
//            }
//        });
//    });

});

