/**
 * @author ASHAN <ashan@thinkcube.com>
 * This file contains form validation
 */

var EDIT_MODE = false;
var ignoreBudgetLimitFlag = false;

$(document).ready(function() {
    $('.selectpicker').selectpicker();
    $('#deMinInvLevel').addUom(productUoms);
    $('#deMaxInvLevel').addUom(productUoms);
    $('#deReOrderLevel').addUom(productUoms);
    $('#deOpeningQuantity').addUom(productUoms);

    $("table.location-product-table tbody tr", $locationProductForm).each(function() {
        $("[name='openingLPQ']", $(this)).addUom(productUoms);
        $("[name='minInvLevel']", $(this)).addUom(productUoms);
        $("[name='maxInvLevel']", $(this)).addUom(productUoms);
        $("[name='reorderLevel']", $(this)).addUom(productUoms);
        $("[name='itemPrice']", $(this)).addUomPrice(productUoms);
        $("[name='sellingPrice']", $(this)).addUomPrice(productUoms);
    });
    var $locationProductForm = $("form[name='location-product']");

    function validateLocationProductDetails() {

        var success = true;
        var $productRows = $("table.location-product-table tbody tr", $locationProductForm);

        $("[name='minInvLevel'],[name='maxInvLevel'],  [name='reorderLevel'], [name='sellingPrice'], [name='discountPercentage'], [name='discountValue'], [name='openingLPQ']", $productRows).each(function() {
            var thisVal = $(this).val();
            if (thisVal.trim() != "" && (isNaN(parseFloat(thisVal)) || parseFloat(thisVal) < 0)) {
                p_notification(false, eb.getMessage('ERR_LOCATPROD_FIELD'));
                success = false;
                $(this).focus();
                return false;
            }

        });

        // check if location wise discount is greater than product wise discount
        if (success) {
            $("[name='discountPercentage'], [name='discountValue']", $productRows).filter(':not([readonly])').each(function() {
                var thisVal = $(this).val();
                var thisName = $(this).attr('name');
                var maxVal = $("[name='" + thisName + "Max']").val();
                var sellingPriceVal = $("[name='sellingPrice']", $(this).parents('tr')).val();
                if (parseFloat(maxVal) != 0) {
                    if (parseFloat(thisVal) > parseFloat(maxVal)) {
                        p_notification(false, "Maximum Discount " + thisName.replace('discount', '') + " allowed for this item is " + maxVal);//MESSAGE['']);
                        success = false;
                        $(this).focus();
                        return false;
                    }
                } else if (thisName == "discountPercentage" && parseFloat(maxVal) == 0) {
                    if (parseFloat(thisVal) > 100 || parseFloat(thisVal) < 0) {
                        p_notification(false, "Discount " + thisName.replace('discount', '') + " range allowed for this item is 0 - 100%");//MESSAGE['']);
                        success = false;
                        $(this).focus();
                        return false;
                    }
                } else if (thisName == "discountValue" && parseFloat(maxVal) == 0) {
                    if (parseFloat(thisVal) > parseFloat(sellingPriceVal) || parseFloat(thisVal) < 0) {
                        p_notification(false, "Discount " + thisName.replace('discount', '') + " range allowed for this item is 0 - " + sellingPriceVal);//MESSAGE['']);
                        success = false;
                        $(this).focus();
                        return false;
                    }
                }

            });
        }

        // check if min. inventory level is greater than the reorder level
        if (success) {
            $("[name='minInvLevel']", $productRows).each(function() {
                var thisVal = $(this).val();
                var $thisReOrderLevel = $("[name='reorderLevel']", $(this).parents('tr'));

                if (parseFloat(thisVal) > parseFloat($thisReOrderLevel.val())) {
                    p_notification(false, eb.getMessage('ERR_LOCATPROD_REORDERLEVEL'));
                    success = false;
                    $thisReOrderLevel.focus();
                    return false;
                }

            });
        }

        // check if discount value greater than selling price
        // only if selling price is set (greater than 0)
        if (success) {
            $("[name='discountValue']:not([readonly])", $productRows).each(function() {
                var thisVal = $(this).val();
                var sellingPriceVal = $("[name='sellingPrice']", $(this).parents('tr')).val();

                if (parseFloat(sellingPriceVal) > 0 && parseFloat(thisVal) > parseFloat(sellingPriceVal)) {
                    p_notification(false, eb.getMessage('ERR_LOCATPROD_ITEM_DISVAL'));
                    success = false;
                    $(this).focus();
                    return false;
                }

            });
        }

        return success;
    }

    var addAllProduct = BASE_URL + '/productAPI/addLocationAllProductDetails';

    $locationProductForm.on('submit', function(e) {
        // validate all inputs
        // error message is returned from the function
        if (!validateLocationProductDetails()) {
            e.stopPropagation();
            return false;
        }

        var openingLPQ = {};
        var minInvLevel = {};
        var maxInvLevel = {};
        var reorderLevel = {};
        var sellingPrice = {};
        var sellingPriceUom = {};
        var itemPrice = {};
        var discountPercentage = {};
        var discountValue = {};

        $("table.location-product-table tbody tr", $locationProductForm).each(function() {
            var locationID = $(this).data('locationid');
            openingLPQ[locationID] = $("[name='openingLPQ']", $(this)).val();
            minInvLevel[locationID] = $("[name='minInvLevel']", $(this)).val();
            maxInvLevel[locationID] = $("[name='maxInvLevel']", $(this)).val();
            reorderLevel[locationID] = $("[name='reorderLevel']", $(this)).val();
            sellingPrice[locationID] = $("[name='sellingPrice']", $(this)).val();
            itemPrice[locationID] = $("[name='itemPrice']", $(this)).val();
            sellingPriceUom[locationID] = $("[name='sellingPriceUom']", $(this)).val();
            discountPercentage[locationID] = $("[name='discountPercentage']", $(this)).val();
            discountValue[locationID] = $("[name='discountValue']", $(this)).val();
        });
        var uomConFactorArray;
        setUomConversionFactor(sellingPriceUom, $('#productID').val(), UomCallBack);
        function UomCallBack(result) {
            uomConFactorArray = result;
            return uomConFactorArray;
        }
        var productLocationData = {
            productID: $('#productID').val(),
            openingLPQ: openingLPQ,
            minInvLevel: minInvLevel,
            maxInvLevel: maxInvLevel,
            reorderLevel: reorderLevel,
            sellingPrice: sellingPrice,
            itemPrice: itemPrice,
            sellingPriceUom: sellingPriceUom,
            discountPercentage: discountPercentage,
            discountValue: discountValue,
            uomConversionFactor: uomConFactorArray,
            ignoreBudgetLimit: ignoreBudgetLimitFlag
        };

        if (EDIT_MODE) {
            productLocationData.editMode = true;
        }

        addProductLocation(productLocationData);

        e.stopPropagation();
        return false;
    });


    function addProductLocation(productLocationData)
    {
        eb.ajax({
            url: addAllProduct,
            type: 'POST',
            data: productLocationData,
            dataType: 'json',
            async: false,
            success: function(data) {
                if (data.status == true) {
                    if (!EDIT_MODE) {
                        location.assign(BASE_URL + '/product');
                    } else {
                        window.setTimeout(function() {
                            window.location.assign(BASE_URL + '/product/list');
                        }, 1000);
                        p_notification(true, data.msg);
                    }
                } else {
                    if (data.data == "NotifyBudgetLimit") {
                        bootbox.confirm(data.msg+' ,Are you sure you want to continue ?', function(result) {
                            if (result == true) {
                                ignoreBudgetLimitFlag = true;
                                productLocationData.ignoreBudgetLimit = true;
                                addProductLocation(productLocationData);
                            } else {
                                setTimeout(function(){ 
                                    location.reload();
                                }, 3000);
                            }
                        });
                    } else {
                        p_notification(false, data.msg);
                    }
                }
            }
        });
    }


    $('.locationDefaultValues').on('click', function() {
//        $(this).attr('disabled', true);
        $(this).parents('tr').find('.minInvLevel').val($('#deMinInvLevel').val());
        $(this).parents('tr').find('.maxInvLevel').val($('#deMaxInvLevel').val());
        $(this).parents('tr').find('.reorderLevel').val($('#deReOrderLevel').val());
        $(this).parents('tr').find('.sellingPrice').val($('#deSellingPrice').val());
        $(this).parents('tr').find('.uomPrice').val($('#productDefaultPurchasePrice').val());
        $(this).parents('tr').find('.itemPrice').val($('#productDefaultPurchasePrice').val());
        if ($('#productDiscountPercentage').val() != "") {
            $(this).parents('tr').find('.discountPercentage').val($('#deDiscPrecentage').val());
        }
        if ($('#productDiscountValue').val() != "") {
            $(this).parents('tr').find('.discountValue').val($('#deDiscValue').val());
        }
        $(this).parents('tr').find('.minInvLevel').change();
        $(this).parents('tr').find('.maxInvLevel').change();
        $(this).parents('tr').find('.reorderLevel').change();
    });

    $('#allLocationsDefaultValues').on('click', function() {
//        if ($(this).is(':checked')) {
        bootbox.confirm('Are you sure you want to set default value for all locations', function(result) {
            if (result === true) {
//                $('#allLocationsDefaultValues').attr('disabled', true);
//                $('.locationDefaultValues').attr('checked', true);
//                $('.locationDefaultValues').attr('disabled', true);
                $('.minInvLevel').val($('#deMinInvLevel').val());
                $('.maxInvLevel').val($('#deMaxInvLevel').val());
                $('.reorderLevel').val($('#deReOrderLevel').val());
                $('.sellingPrice').val($('#deSellingPrice').val());
                $('.sellingPriceUom').val($('#productDefaultsellingPriceUom').val());
                $('.sellingPriceUom').selectpicker('refresh');
                $('.discountPercentage').val($('#deDiscPrecentage').val());
                $('.discountValue').val($('#deDiscValue').val());
//                    $('.openingLPQ:enabled').val($('#deOpeningQuantity').val());
//                    $('.openingLPQ').change();
                $('.minInvLevel').change();
                $('.maxInvLevel').change();
                $('.reorderLevel').change();
                $('.sellingPrice').change();
//                    $('.discountValue').change();
            }
        });
//        }
    });

    $('#back').on('click', function() {
        window.setTimeout(function() {
            window.location.assign(BASE_URL + '/product/edit/' + $('#productID').val());
        }, 1000);
    });
    //use to find UomConversion Factor
    function setUomConversionFactor(uomValue, productID, callback) {
        var data = {
            uomValue: uomValue,
            productID: productID
        }
        eb.ajax({
            url: BASE_URL + '/productAPI/getUomConversionValue',
            type: 'POST',
            data: data,
            dataType: 'json',
            async: false,
            success: function(data) {
                if (data.status == true) {
                    conversionFactor = data.data;
                    if (callback) {
                        callback(conversionFactor);
                    }
                }
            }
        });
    }


    //use to find UomConversion Factor
//    function setUomConversionFactor(uomValue, productID, callback) {
//        var data = {
//            uomValue: uomValue,
//            productID: productID
//        }
//        eb.ajax({
//            url: BASE_URL + '/productAPI/getUomConversionValue',
//            type: 'POST',
//            data: data,
//            dataType: 'json',
//            async: false,
//            success: function(data) {
//                if (data.status == true) {
//                    conversionFactor = data.data;
//                    console.log(conversionFactor);
//                    if (callback) {
//                        callback(conversionFactor);
//                    }
//                }
//            }
//        });
//    }
});

