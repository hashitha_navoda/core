
$(document).ready(function() {
var currencySymbol;
     $(document.body).on('click', '#notification_bodyy a.over_due_inv, a.over_due_purchase_inv', function (e) {
        e.preventDefault();

        showInvoicePreview(BASE_URL + $(this).attr("href"));
        return false;

    });

    howInvoicePreview = function (url) {
        var path = "/invoice-api/send-email";
        documentPreview(url, iframe, path, function ($preview) {
            $("button.invoice-history-close", $preview).on("click", function () {
                $("#inv_history", $preview).modal("hide");
            });

            if ($("#deliveryNoteSave").length && $('#currentLocation').length) { // mamke sure we are in Invoice form
                $preview.on('hidden.bs.modal', function () {

                    // since this event is being called when any modal is closed, even
                    // when the email modal is closed, this even gets triggered
                    // so check whether the main modal is visible
                    if (!$preview.is(':visible')) {
                        window.location.reload();
                    }
                });
            }
        });
    }
    
    var $welcomeWindow = $('#welcomeWindow');
    checkIsUserLogInFirstTime();
    function checkIsUserLogInFirstTime() {
        var url = BASE_URL + '/api/settings/wizard/checkIsUserLogInFirstTime';
        eb.ajax({
            type: 'POST',
            url: url,
            data: {},
            success: function(respond) {
                if (respond.status == true) {
                    $welcomeWindow.modal('show');
                    updateUserLogged();
                }
            }
        });
    }

    function updateUserLogged() {
        var url = BASE_URL + '/api/settings/wizard/updateUserLogged';
        eb.ajax({
            type: 'POST',
            url: url,
            data: {},
            success: function(respond) {
                if (respond.status == true) {
                    return true;
                }
            }
        });
    }

    $('#invoiceBtn').on('click', function(e) {
        window.location.href = BASE_URL + "/invoice/create";
    });

    $('#inventoryBtn').on('click', function(e) {
        window.location.href = BASE_URL + '/product';
    });
    
    currentPeriod = 'thisWeek';
    // getWidgetData('thisWeek');

    getTotalSalesData('thisWeek');

    function getTotalSalesData(period){
        eb.ajax({
            type: 'POST',
            url: BASE_URL + '/dashboardAPI/getTotalSalesData',
            data: {period: period},
            success: function(data) {
                currencySymbol = data.companyCurrencySymbol;
                $('#total-sales').each(function () {
                    $(this).prop('Counter',0).animate({
                        Counter: data.totalSales
                    }, {
                        duration: 500,
                        easing: 'swing',
                        step: function (now) {
                            $(this).text(currencySymbol+' '+accounting.formatMoney(now));
                        }
                    });
                });

                if (data.totalSalesProfitLoss > 0) {
                    $('#total-sales-profit').removeClass('hidden');
                    $('#total-sales-loss').addClass('hidden');
                    $('#total-sales-profit').html('+'+data.totalSalesProfitLoss+'%<span><i class="glyphicon glyphicon-arrow-up"></i></span>');
                } else {
                    $('#total-sales-profit').addClass('hidden');
                    $('#total-sales-loss').removeClass('hidden');
                    var loss = data.totalSalesProfitLoss * -1;
                    $('#total-sales-loss').html('+'+loss+'%<span><i class="glyphicon glyphicon-arrow-down"></i></span>');
                }

                // $.each(data.bestMovingItems, function(index, val) {
                //     $(".bestMovingItems").append("<div class='row item-row'><div class='col-lg-1 table-text'>"+(parseFloat(index)+1)+"</div><div class='col-lg-9 table-text p-text' style='width: 100%;'>"+val.productName+"</div><div class='col-lg-2 table-text text-right'>"+val.movingCount+"</div></div>");
                // });

                switch (period) {
                    case 'thisYear':
                        $('#last-period-total-sales').text('Compared to ('+currencySymbol+' '+accounting.formatMoney(data.totalSalesLastYear)+' last year)');
                        break;
                    case 'thisMonth':
                        $('#last-period-total-sales').text('Compared to ('+currencySymbol+' '+accounting.formatMoney(data.totalSalesLastYear)+' last month)');
                        break;
                    case 'thisWeek':
                        $('#last-period-total-sales').text('Compared to ('+currencySymbol+' '+accounting.formatMoney(data.totalSalesLastYear)+' last week)');
                        break;
                    case 'thisDay':
                        $('#last-period-total-sales').text('Compared to ('+currencySymbol+' '+accounting.formatMoney(data.totalSalesLastYear)+' last day)');
                        break;
                    default:
                        // statements_def
                        break;
                }
                
            }
        }).then(function () {
            getTotalStockData(period);
        });
    }


    function getTotalStockData(period){
        eb.ajax({
            type: 'POST',
            url: BASE_URL + '/dashboardAPI/getTotalStockData',
            data: {period: period},
            success: function(data) {
                currencySymbol = data.companyCurrencySymbol;
                $('#gross-profitt').each(function () {
                    $(this).prop('Counter',0).animate({
                        Counter: data.totalStockValue
                    }, {
                        duration: 1000,
                        easing: 'swing',
                        step: function (now) {
                            $(this).text(currencySymbol+' '+accounting.formatMoney(now));
                        }
                    });
                });  
            }
        }).then(function () {
            getChequeData(period);
        });
    }

    function getChequeData(period){
        eb.ajax({
            type: 'POST',
            url: BASE_URL + '/dashboardAPI/getChequeData',
            data: {period: period},
            success: function(data) {
                currencySymbol = data.companyCurrencySymbol;
                $('#gross-profit').each(function () {
                    $(this).prop('Counter',0).animate({
                        Counter: data.recivedChequeTotal
                    }, {
                        duration: 1000,
                        easing: 'swing',
                        step: function (now) {
                            $(this).text(currencySymbol+' '+accounting.formatMoney(now));
                        }
                    });
                });
                $('#total-payments').each(function () {
                    $(this).prop('Counter',0).animate({
                        Counter: data.settledInvoiceChequesTotal
                    }, {
                        duration: 1000,
                        easing: 'swing',
                        step: function (now) {
                            $(this).text(currencySymbol+' '+accounting.formatMoney(now));
                        }
                    });
                });

                if (data.recivedChequeProfitLoss > 0) {
                    $('#total-gross-profit').removeClass('hidden');
                    $('#total-gross-loss').addClass('hidden');
                    $('#total-gross-profit').html('+'+data.recivedChequeProfitLoss+'%<span><i class="glyphicon glyphicon-arrow-up"></i></span>');
                } else {
                    $('#total-gross-profit').addClass('hidden');
                    $('#total-gross-loss').removeClass('hidden');
                    var loss = data.recivedChequeProfitLoss * -1;
                    $('#total-gross-loss').html('+'+loss+'%<span><i class="glyphicon glyphicon-arrow-down"></i></span>');
                }

                if (data.settledChequeProfitLoss > 0) {
                    $('#total-payment-profit').removeClass('hidden');
                    $('#total-payment-loss').addClass('hidden');
                    $('#total-payment-profit').html('+'+data.settledChequeProfitLoss+'%<span><i class="glyphicon glyphicon-arrow-up"></i></span>');
                } else {
                    $('#total-payment-profit').addClass('hidden');
                    $('#total-payment-loss').removeClass('hidden');
                    var loss = data.settledChequeProfitLoss * -1;
                    $('#total-payment-loss').html('+'+loss+'%<span><i class="glyphicon glyphicon-arrow-down"></i></span>');
                }

                switch (period) {
                    case 'thisYear':
                        $('#last-period-total-gross').text('Compared to ('+currencySymbol+' '+accounting.formatMoney(data.lastPeriodRecivedChequeTotal)+' last year)');
                        $('#last-period-total-payment').text('Compared to ('+currencySymbol+' '+accounting.formatMoney(data.lastPeriodSettledInvoiceChequesTotal)+' last year)');
                        break;
                    case 'thisMonth':
                        $('#last-period-total-gross').text('Compared to ('+currencySymbol+' '+accounting.formatMoney(data.lastPeriodRecivedChequeTotal)+' last month)');
                        $('#last-period-total-payment').text('Compared to ('+currencySymbol+' '+accounting.formatMoney(data.lastPeriodSettledInvoiceChequesTotal)+' last month)');
                        break;
                    case 'thisWeek':
                        $('#last-period-total-gross').text('Compared to ('+currencySymbol+' '+accounting.formatMoney(data.lastPeriodRecivedChequeTotal)+' last week)');
                        $('#last-period-total-payment').text('Compared to ('+currencySymbol+' '+accounting.formatMoney(data.lastPeriodSettledInvoiceChequesTotal)+' last week)');
                        break;
                    case 'thisDay':
                        $('#last-period-total-gross').text('Compared to ('+currencySymbol+' '+accounting.formatMoney(data.lastPeriodRecivedChequeTotal)+' last day)');
                        $('#last-period-total-payment').text('Compared to ('+currencySymbol+' '+accounting.formatMoney(data.lastPeriodSettledInvoiceChequesTotal)+' last day)');
                        break;
                    default:
                        // statements_def
                        break;
                }
                
            }
        });
    }


    function getWidgetData(period){
        eb.ajax({
            type: 'POST',
            url: BASE_URL + '/dashboardAPI/getDashboardData',
            data: {period: period},
            success: function(data) {
                $('#total-sales').each(function () {
                    $(this).prop('Counter',0).animate({
                        Counter: data.totalSales
                    }, {
                        duration: 500,
                        easing: 'swing',
                        step: function (now) {
                            $(this).text('LKR '+accounting.formatMoney(now));
                        }
                    });
                });
                $('#gross-profitt').each(function () {
                    $(this).prop('Counter',0).animate({
                        Counter: data.totalStockValue
                    }, {
                        duration: 1000,
                        easing: 'swing',
                        step: function (now) {
                            $(this).text('LKR '+accounting.formatMoney(now));
                        }
                    });
                });
                $('#gross-profit').each(function () {
                    $(this).prop('Counter',0).animate({
                        Counter: data.recivedChequeTotal
                    }, {
                        duration: 1000,
                        easing: 'swing',
                        step: function (now) {
                            $(this).text('LKR '+accounting.formatMoney(now));
                        }
                    });
                });
                $('#total-payments').each(function () {
                    $(this).prop('Counter',0).animate({
                        Counter: data.settledInvoiceChequesTotal
                    }, {
                        duration: 1000,
                        easing: 'swing',
                        step: function (now) {
                            $(this).text('LKR '+accounting.formatMoney(now));
                        }
                    });
                });

                if (data.recivedChequeProfitLoss > 0) {
                    $('#total-gross-profit').removeClass('hidden');
                    $('#total-gross-loss').addClass('hidden');
                    $('#total-gross-profit').html('+'+data.recivedChequeProfitLoss+'%<span><i class="glyphicon glyphicon-arrow-up"></i></span>');
                } else {
                    $('#total-gross-profit').addClass('hidden');
                    $('#total-gross-loss').removeClass('hidden');
                    var loss = data.recivedChequeProfitLoss * -1;
                    $('#total-gross-loss').html('+'+loss+'%<span><i class="glyphicon glyphicon-arrow-down"></i></span>');
                }

                if (data.settledChequeProfitLoss > 0) {
                    $('#total-payment-profit').removeClass('hidden');
                    $('#total-payment-loss').addClass('hidden');
                    $('#total-payment-profit').html('+'+data.settledChequeProfitLoss+'%<span><i class="glyphicon glyphicon-arrow-up"></i></span>');
                } else {
                    $('#total-payment-profit').addClass('hidden');
                    $('#total-payment-loss').removeClass('hidden');
                    var loss = data.settledChequeProfitLoss * -1;
                    $('#total-payment-loss').html('+'+loss+'%<span><i class="glyphicon glyphicon-arrow-down"></i></span>');
                }

                if (data.totalSalesProfitLoss > 0) {
                    $('#total-sales-profit').removeClass('hidden');
                    $('#total-sales-loss').addClass('hidden');
                    $('#total-sales-profit').html('+'+data.totalSalesProfitLoss+'%<span><i class="glyphicon glyphicon-arrow-up"></i></span>');
                } else {
                    $('#total-sales-profit').addClass('hidden');
                    $('#total-sales-loss').removeClass('hidden');
                    var loss = data.totalSalesProfitLoss * -1;
                    $('#total-sales-loss').html('+'+loss+'%<span><i class="glyphicon glyphicon-arrow-down"></i></span>');
                }

                $.each(data.bestMovingItems, function(index, val) {
                    $(".bestMovingItems").append("<div class='row item-row'><div class='col-lg-1 table-text'>"+(parseFloat(index)+1)+"</div><div class='col-lg-9 table-text p-text' style='width: 100%;'>"+val.productName+"</div><div class='col-lg-2 table-text text-right'>"+val.movingCount+"</div></div>");
                });

                switch (period) {
                    case 'thisYear':
                        $('#last-period-total-sales').text('Compared to (LKR '+accounting.formatMoney(data.totalSalesLastYear)+' last year)');
                        $('#last-period-total-gross').text('Compared to (LKR '+accounting.formatMoney(data.lastPeriodRecivedChequeTotal)+' last year)');
                        $('#last-period-total-payment').text('Compared to (LKR '+accounting.formatMoney(data.lastPeriodSettledInvoiceChequesTotal)+' last year)');
                        break;
                    case 'thisMonth':
                        $('#last-period-total-sales').text('Compared to (LKR '+accounting.formatMoney(data.totalSalesLastYear)+' last month)');
                        $('#last-period-total-gross').text('Compared to (LKR '+accounting.formatMoney(data.lastPeriodRecivedChequeTotal)+' last month)');
                        $('#last-period-total-payment').text('Compared to (LKR '+accounting.formatMoney(data.lastPeriodSettledInvoiceChequesTotal)+' last month)');
                        break;
                    case 'thisWeek':
                        $('#last-period-total-sales').text('Compared to (LKR '+accounting.formatMoney(data.totalSalesLastYear)+' last week)');
                        $('#last-period-total-gross').text('Compared to (LKR '+accounting.formatMoney(data.lastPeriodRecivedChequeTotal)+' last week)');
                        $('#last-period-total-payment').text('Compared to (LKR '+accounting.formatMoney(data.lastPeriodSettledInvoiceChequesTotal)+' last week)');
                        break;
                    case 'thisDay':
                        $('#last-period-total-sales').text('Compared to (LKR '+accounting.formatMoney(data.totalSalesLastYear)+' last day)');
                        $('#last-period-total-gross').text('Compared to (LKR '+accounting.formatMoney(data.lastPeriodRecivedChequeTotal)+' last day)');
                        $('#last-period-total-payment').text('Compared to (LKR '+accounting.formatMoney(data.lastPeriodSettledInvoiceChequesTotal)+' last day)');
                        break;
                    default:
                        // statements_def
                        break;
                }
                
            }
        });
    }

    $('.period').on('change', function(event) {
        event.preventDefault();
        currentPeriod = $(this).val();
        // getWidgetData($(this).val());
        getTotalSalesData($(this).val());
    });
});