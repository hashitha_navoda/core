/**
 * @author Damith Thamara <damith@thinkcube.com>
 * This javascript contains Purchase Returns related functions
 */
var selectedGRN;
var locationProducts = {};
var directReturnNoteType = false;
var selectedSupplier;
var selectedId;
var currentProducts = {};
var subProductsForCheck = {};
var checkSubProduct = {};
var discountEditedManually = [];
var deliverSubProducts = {};
var deliverProducts = {};
var hideDuplicate = false;
var ignoreBudgetLimitFlag = false;
var subProductsForCheck = {};
var SubProductsQty = {};
var dimensionData = {};
$(document).ready(function() {
    var priceListItems = [];
    var defaultSellingPriceData = [];
    var rowincrementID = 1;
    var retireveLocation;
    var grnData;
    var grnProducts;
    var totalTaxList = {};
    var currentReturnItemCode;
    var currentItemReturnQty;
    var currentItemreturnAll = false;
    var returnProducts = {};
    var batchProducts = {};
    var serialProducts = {};
    var selectedGrnID;
    var locationID = $('#idOfLocation').val();
    var $productTable = $("#deliveryNoteProductTable");
    var $addRowSample = $('tr.add-row.sample.hidden', $productTable);
    var dimensionArray = {};
    var dimensionTypeID = null;

    function returnProduct(lPID, pID, pCode, pN, pQ, pRQ, pD, pUP, pTotal, pTax, bProducts, sProducts, productType, thisItemGrnProID) {
        this.locationPID = lPID;
        this.pID = pID;
        this.pCode = pCode;
        this.pName = pN;
        this.pQuantity = pQ;
        this.pReturnQuantity = pRQ;
        this.pDiscount = pD;
        this.pUnitPrice = pUP;
        this.pTotal = pTotal;
        this.pTax = pTax;
        this.bProducts = bProducts;
        this.sProducts = sProducts;
        this.productType = productType;
        this.thisItemGrnProID = thisItemGrnProID;
    }

    function batchProduct(bID, bCode, bQty, bRQty) {
        this.bID = bID;
        this.bCode = bCode;
        this.bQty = bQty;
        this.bRQty = bRQty;
    }

    function serialProduct(sID, sCode, sBCode, sRQty, sGrnProId) {
        this.sID = sID;
        this.sCode = sCode;
        this.sBCode = sBCode;
        this.sRQty = sRQty;
        this.sGrnProId = sGrnProId;
    }

    var getAddRow = function(proIncID) {
        if (!(proIncID === undefined || proIncID == '')) {
            var $row = $('table#deliveryNoteProductTable > tbody#add-new-item-row-for-dPr > tr').filter(function() {
                return $(this).data("proIncID") == proIncID;
            });
            return $row;
        }

        return $('tr.add-row:not(.sample)', $productTable);
    };
    if (!getAddRow().length) {
        $($addRowSample.clone()).removeClass('sample hidden').appendTo($('#add-new-item-row-for-dPr'));
    }

    loadDropDownFromDatabase('/api/grn/search-open-grn-for-dropdown', "", 0, '#startByCode');
    loadDropDownFromDatabase('/supplierAPI/search-suppliers-for-dropdown', "", 0, '#directSupplier');
    $('#startByCode').selectpicker('refresh');

    $('#startByCode').on('change', function() {
        if ($(this).val() > 0 && selectedGRN != $(this).val()) {
            selectedGRN = $(this).val();
            $('#startByCode').prop('disabled', true);
            loadGRN(selectedGRN);
        }
    });

    $('#directSupplier').selectpicker('refresh');
    $('#directSupplier').on('change', function() {
        if ($(this).val() > 0 && $(this).val() != selectedSupplier) {
            selectedSupplier = $(this).val();
            eb.ajax({
                type: 'POST',
                url: BASE_URL + '/api/pr/getPrNoForLocation',
                data: {locationID: locationID},
                success: function(respond) {
                    if (respond.status == false) {
                        p_notification(false, respond.msg);
                        $('#prNo').val('');
                    }
                    else if (respond.status == true) {
                        $('#prNo').val(respond.data.refNo);
                        $('#locRefID').val(respond.data.locRefID);
                    }
                },
                async: false
            });
        }
        else if($(this).val() == 0){
            selectedSupplier = '';
        }
    });

    $('#dimensionView').on('click', function(e) {
        clearDimensionModal();
        var prNo = $('#prNo').val();

        if (!$.isEmptyObject(dimensionData)) {
            $.each(dimensionData[prNo], function(index, val) {
                dimensionArray[index] = val;                
            });
            $.each(dimensionArray, function(index, value) {
                var newTrID = 'tr_' + value.dimensionTypeId;
                var clonedRow = $($('#dimensionPreSample').clone()).attr('id', newTrID).addClass('addedDimensions');
                $("input[name='dimensionType']", clonedRow).val(value.dimensionTypeTxt);
                $("input[name='dimensionValue']", clonedRow).val(value.dimensionValueTxt);
                clonedRow.removeClass('hidden');
                clonedRow.insertBefore('#dimensionPreSample');
                clearAddedNewDimensionRow();
            });
        }
        $('#addDimensionModal').modal('show');
    });

    // dimension modal
    function clearDimensionModal()
    {
        $("#addDimensionModal #dimensionTable tbody .addedDimensions").remove();
        dimensionArray = {};
    }


    $('.dimenisonchange').on('change', function(e) {
        dimensionTypeID = this.value;
        getDimensionValueListByDimensionType(dimensionTypeID, function() {
        });
    });



    function getDimensionValueListByDimensionType(dimensionTypeID, callback) {
        switch (dimensionTypeID) {
            case "job":
                $('.dimensionVal').addClass('hidden');
                $('.dimensionProjectNo').addClass('hidden');
                $('.dimensionJobNo').removeClass('hidden');
                break;
            case "project":
                $('.dimensionVal').addClass('hidden');
                $('.dimensionProjectNo').removeClass('hidden');
                $('.dimensionJobNo').addClass('hidden');
                break;
           default:
                $('.dimensionVal').removeClass('hidden');
                $('.dimensionProjectNo').addClass('hidden');
                $('.dimensionJobNo').addClass('hidden');
                $.ajax({
                    type: 'POST',
                    url: BASE_URL + '/dimension-api/getRelatedDimensionValues',
                    data: {dimensionId: dimensionTypeID},
                    success: function(respond) {
                        //empty options
                        $('#dimensionVal').empty();
                        $('#dimensionVal').append($("<option>", {value: '', html: 'Select dimension value'}));
                        $(respond.data).each(function(index, dimValue) {
                            $.each(dimValue, function(ind, val) {
                                $('#dimensionVal').append($("<option>", {value: ind, html: val.value}));
                            });
                        });
                        callback();
                    }
                });
                break;
        }
    }

    $('#add_dimension').on('click', function(e) {
        e.preventDefault();
        //if plus button is clicked then disabled the button
        //if button is clicked more than one, then it does not continue
        if ($(this).is(':disabled')) {
            return false;
        } else {
            $(this).attr('disabled', true);
            var $thisRow = $(this).parents('tr');
            if ($('.dimenisonchange').val() == 0 || $('.dimenisonchange').val() == '' || $('.dimenisonchange').val() == null) {
                p_notification(false, eb.getMessage('ERR_DIMENSION_SELCET'));
                $(this).attr('disabled', false);
            } else {
                
                if ($('.dimenisonchange').val() != 'job' && $('.dimenisonchange').val() != 'project') {
                    if ($('.dimensionVal').val() == 0 || $('.dimensionVal').val() == '' || $('.dimensionVal').val() == null) {
                        p_notification(false, eb.getMessage('ERR_DIMENSION_VALUE_SELCET'));
                        $(this).attr('disabled', false);
                        return;
                    }
                }

                if ($('.dimenisonchange').val() == 'project') {
                    if ($('.dimensionProjectNo').val() == 0 || $('.dimensionProjectNo').val() == '' || $('.dimensionProjectNo').val() == null) {
                        p_notification(false, eb.getMessage('ERR_DIMENSION_VALUE_SELCET'));
                        $(this).attr('disabled', false);
                        return;
                    }
                }

                if ($('.dimenisonchange').val() == 'job') {
                    if ($('.dimensionJobNo').val() == 0 || $('.dimensionJobNo').val() == '' || $('.dimensionJobNo').val() == null) {
                        p_notification(false, eb.getMessage('ERR_DIMENSION_VALUE_SELCET'));
                        $(this).attr('disabled', false);
                        return;
                    }
                }

                if (dimensionArray[dimensionTypeID]) {
                    p_notification(false, eb.getMessage('ERR_DIMENSION_ALREADY_INSERTED'));
                    $('.dimenisonchange').val('');
                    $('.dimenisonchange').selectpicker('render');
                    $('.dimenisonchange').trigger('change');
                } else {
                    addNewDimensionRow($(this).parents('tr'));
                }
                $(this).attr('disabled', false);
            }
        }
    });

    function addNewDimensionRow(thisRow) {

        var dimensionTypeId = $('.dimenisonchange', thisRow).val();
        var dimensionTypeTxt = $('.dimenisonchange', thisRow).find("option:selected").text();

        switch (dimensionTypeID) {
            case "job":
                var dimensionValueTxt = $('.dimensionJobNo', thisRow).find("option:selected").text();
                var dimensionValueId = $('.dimensionJobNo', thisRow).val();
                break;
            case "project":
                var dimensionValueTxt = $('.dimensionProjectNo', thisRow).find("option:selected").text();
                var dimensionValueId = $('.dimensionProjectNo', thisRow).val();
                break;
           default:
                var dimensionValueTxt = $('.dimensionVal', thisRow).find("option:selected").text();
                var dimensionValueId = $('.dimensionVal', thisRow).val();
                break;
        }
       
        var dimensionDuplicateCheck = false;
        if (!$.isEmptyObject(dimensionArray)) {
            $.each(dimensionArray, function(index, value) {
                if (value.dimensionTypeId == dimensionTypeId) {
                    dimensionDuplicateCheck = true;
                }
            });   
        }
       
        if (dimensionDuplicateCheck) {
            p_notification(false, eb.getMessage("ERR_DIMENSION_ALREADY_INSERTED"));
            return false;
        }

        var newTrID = 'tr_' + dimensionTypeId;
        var clonedRow = $($('#dimensionPreSample').clone()).attr('id', newTrID).addClass('addedDimensions');
        $("input[name='dimensionType']", clonedRow).val(dimensionTypeTxt);
        $("input[name='dimensionValue']", clonedRow).val(dimensionValueTxt);
        clonedRow.removeClass('hidden');
        clonedRow.insertBefore('#dimensionPreSample');
        dimensionArray[dimensionTypeId] = new dimension(dimensionTypeId, dimensionValueId,dimensionTypeTxt , dimensionValueTxt);
        clearAddedNewDimensionRow();
    }


    function dimension(dimensionTypeId, dimensionValueId, dimensionTypeTxt, dimensionValueTxt) {
        this.dimensionTypeId = dimensionTypeId;
        this.dimensionValueId = dimensionValueId;
        this.dimensionTypeTxt = dimensionTypeTxt;
        this.dimensionValueTxt = dimensionValueTxt;
    }

    function clearAddedNewDimensionRow() {
        $('.dimenisonchange').val('');
        $('.dimenisonchange').selectpicker('render');
        $('.dimenisonchange').trigger('change');
    }

    $('.addDimensions').on('click', function(e) {
        e.preventDefault();

        // validate products before closing modal
        if (!dimensionModalValidate(e)) {
            e.stopPropagation();
            return false;
        } else {
            $('#addDimensionModal').modal('hide');
        }
    });

    function dimensionModalValidate(e) {
        var prNo = $('#prNo').val();
        if ($.isEmptyObject(dimensionArray)) {
            dimensionData = {};
            return true;   
        } else {
            var dimensionSingleData = {};
            $.each(dimensionArray, function(index, val) {
                dimensionSingleData[index] = val;
            });
            dimensionData[prNo] = dimensionSingleData;
            return true;
        }
    }

    $("#addDimensionModal #dimensionTable").on('click', '.deleteDimensionType', function(e) {
        e.preventDefault();
        var deleteDTrID = $(this).closest('tr').attr('id');
        var deleteDID = deleteDTrID.split('tr_')[1].trim();
        bootbox.confirm('Are you sure you want to remove this dimension ?', function(result) {
            if (result == true) {
                delete dimensionArray[deleteDID];
                $('#' + deleteDTrID).remove();
            }
        });
    });

    loadDropDownFromDatabase('/job-api/searchJobsForDropdown', "", 0, '.dimensionJobNo');
    $('.dimensionJobNo').selectpicker('refresh');
    $('.dimensionJobNo').on('change', function() {
        var dimensionValue = $(this).val();
    });

    loadDropDownFromDatabase('/api/project/searchProjectsForDropdown', "", 0, '.dimensionProjectNo');
    $('.dimensionProjectNo').selectpicker('refresh');
    $('.dimensionProjectNo').on('change', function() {
        var dimensionValue = $(this).val();
    });

    $('#refreshStartBy').on('click', function() {
        $('#supplier').val('');
        $('#receivedDate').val('');
        $('#retrieveLocation').val('');
        $('#supplierReference').val('');
        $('#prNo').val('');
        $('#comment').val('');
        $('#startByCode').val('');
        $('#startByCode').prop('disabled', false);
        $('#startByCode').selectpicker('render');
        clearModalWindow();
        returnProducts = {};
        batchProducts = {};
        serialProducts = {};
        $('.returnedProducts').remove();
        $('.tempGrnPro').remove();
        setTotalTax();
        setPrTotalCost();
    });

    $('#directReturn').on('click', function(){
        if($(this).is(':checked')){
            $('#productAddOverlay').removeClass('overlay');
            hideDuplicate = false;
            setProductTypeahead(hideDuplicate);
            locationProducts = {};
            clearProductDataForNewReturn();
            $('#startByCode').attr('disabled',true);
            $('.direct-return').removeClass('hidden');
            $('.normal-return').addClass('hidden');
            $('.supplierReference').addClass('hidden');
            $('.direct-location').removeClass('hidden');
            $('.normal-location').addClass('hidden');
            $('#item_code', $addRowSample).selectpicker();
            $('.directSupplier').removeClass('hidden');
            $('.normalSupplier').addClass('hidden');
            directReturnNoteType = true;
            $('.grnDate').addClass('hidden');
            var nowTemp = new Date();
            var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);
            var checkin = $('#returnDate').datepicker({onRender: function(date) {
                    return (date.valueOf() > now.valueOf() ? ((U_DATE_R_OVERRIDE) ? '' : 'disabled') : '');
                },
                //format: 'yyyy-mm-dd'
            }).on('changeDate', function(ev) {
                checkin.hide();
            }).data('datepicker');
            checkin.setValue(now);
            $('#dimension_div').removeClass('hidden');
        } else {
            $('#dimension_div').addClass('hidden');
            $('#startByCode').attr('disabled',false);
            $('.direct-return').addClass('hidden');
            $('.normal-return').removeClass('hidden');
            $('.directSupplier').addClass('hidden');
            $('.normalSupplier').removeClass('hidden');
            $('.direct-location').addClass('hidden');
            $('.normal-location').removeClass('hidden');
            directReturnNoteType = false;
            $('.grnDate').removeClass('hidden');
            $('.supplierReference').removeClass('hidden');
        }
    });

    $('#add-new-item-row').on('click', '.delete', function() {
        $(this).parents('tr').remove();
    });

    $('#prCancel', '.btnPanel').on('click', function() {
        window.location.reload();
    });

    $('#showTax').on('click', function() {
        if (this.checked) {
            if (!jQuery.isEmptyObject(returnProducts)) {
                $('#totalTaxShow').removeClass('hidden');
            } else {
                p_notification(false, eb.getMessage('ERR_GRN_PROD_TAX'));
            }

        } else {
            $('#totalTaxShow').addClass('hidden');
        }

    });

    $('#directTaxShow').on('click', function() {
        if (this.checked) {
            if (!jQuery.isEmptyObject(deliverProducts)) {
                $('#directTotalTaxShow').removeClass('hidden');
            } else {
                p_notification(false, eb.getMessage('ERR_GRN_PROD_TAX'));
            }

        } else {
            $('#directTotalTaxShow').addClass('hidden');
        }

    });

    $('#returnProductTable').on('click', '.deleteRetProducts', function() {

        var deleteProduct = $(this).parents('tr');
        bootbox.confirm('Are you sure you want to remove this product ?', function(result) {
            if (result == true) {
                delete(returnProducts[deleteProduct.attr('id')]);
                deleteProduct.remove();
                setTotalTax();
                setPrTotalCost();
            }
        });
    });
    $('#returnProductTable').on('click', '.returnViewSubProducts', function() {
        clearModalWindow();
        var thisItemID = $(this).parents('tr').attr('id');
        var thisItemDetails = returnProducts[thisItemID];

        var avselectedUomAbbr = grnProducts[thisItemID].selectedAvUomID;
        var selectedUomAbbr = grnProducts[thisItemID].selectedRtUomID;

        var avconversion = locationProducts[grnProducts[thisItemID].gPID].uom[avselectedUomAbbr].uC;
        var rtconversion = locationProducts[grnProducts[thisItemID].gPID].uom[selectedUomAbbr].uC;

        $('#viewReturnsSubProductsModal').modal('show');
        if (!jQuery.isEmptyObject(thisItemDetails.bProducts) && !jQuery.isEmptyObject(thisItemDetails.sProducts)) {
            for (var i in thisItemDetails.sProducts) {
                var clonedBPRow = $('#defaultRow').clone().attr('id', 'sub_' + i).removeClass('hidden').addClass('tempSubProductRow');
                $(".batchCode", clonedBPRow).html(thisItemDetails.sProducts[i].sBCode);
                $(".serialID", clonedBPRow).html(thisItemDetails.sProducts[i].sCode);
                $("input[name='availableQuantity']", clonedBPRow).val('1');
                $("input[name='returnQuantity']", clonedBPRow).remove();
                $("input[name='returnQuantityCheck']", clonedBPRow).attr('disabled', true);
                if (typeof thisItemDetails.sProducts[i].sRQty != 'undefined' && thisItemDetails.sProducts[i].sRQty == "1") {
                    $("input[name='returnQuantityCheck']", clonedBPRow).prop('checked', true);
                }
                clonedBPRow.insertBefore('#defaultRow');
            }
            $('#subProductsSave').addClass('hidden');
        } else if (!jQuery.isEmptyObject(thisItemDetails.bProducts)) {
            for (var i in thisItemDetails.bProducts) {
                var clonedBPRow = $('#defaultRow').clone().attr('id', 'sub_' + i).removeClass('hidden').addClass('tempSubProductRow');
                $(".batchCode", clonedBPRow).html(thisItemDetails.bProducts[i].bCode);
                $("input[name='availableQuantity']", clonedBPRow).val(thisItemDetails.bProducts[i].bQty).addUom(locationProducts[grnProducts[thisItemID].gPID].uom);
                $("input[name='returnQuantityCheck']", clonedBPRow).remove();
                $("input[name='returnQuantity']", clonedBPRow).attr('disabled', true);
                if (typeof thisItemDetails.bProducts[i].bRQty != 'undefined') {
                    $("input[name='returnQuantity']", clonedBPRow).val(thisItemDetails.bProducts[i].bRQty).addUom(locationProducts[grnProducts[thisItemID].gPID].uom);
                }
                clonedBPRow.insertBefore('#defaultRow');
            }
            $('#subProductsSave').addClass('hidden');
        } else {
            for (var i in thisItemDetails.sProducts) {
                var clonedBPRow = $('#defaultRow').clone().attr('id', 'sub_' + i).removeClass('hidden').addClass('tempSubProductRow');
                $(".batchCode", clonedBPRow).html(thisItemDetails.sProducts[i].sBCode);
                $(".serialID", clonedBPRow).html(thisItemDetails.sProducts[i].sCode);
                $("input[name='availableQuantity']", clonedBPRow).val('1');
                $("input[name='returnQuantity']", clonedBPRow).remove();
                $("input[name='returnQuantityCheck']", clonedBPRow).attr('disabled', true);
                if (typeof thisItemDetails.sProducts[i].sRQty != 'undefined' && thisItemDetails.sProducts[i].sRQty == "1") {
                    $("input[name='returnQuantityCheck']", clonedBPRow).prop('checked', true);
                }
                clonedBPRow.insertBefore('#defaultRow');
            }
            $('#subProductsSave').addClass('hidden');
        }
    });

    $('#viewReturnsSubProductsModal').on('keyup', 'input[name=returnQuantity]', function() {
        if (toFloat($(this).val()) > toFloat($(this).parents("tr").find("input[name=availableQuantity]").val())) {
            p_notification(false, eb.getMessage('ERR_PR_LESSQUAL'));
            $(this).val('');
        } else if ($(this).val() < 0 || isNaN($(this).val())) {
            p_notification(false, eb.getMessage('ERR_PR_PROPQUAL'));
            $(this).val('');
        }
    });


    $('#add-new-item-row').on('click', '.viewSubProducts', function() {
        clearModalWindow();
        var thisItemID = $(this).parents('tr').find("input[name=itemCode]").data('PII');
        var pID = $(this).parents('tr').attr("id").split('_');
        currentReturnItemCode = thisItemID;
        var thisItemPurchasedQty = $(this).parents('tr').find(".purchasedQty").val();
        currentItemreturnAll = false;
        if (currentItemReturnQty == thisItemPurchasedQty) {
            currentItemreturnAll = true;
        }
        currentItemReturnQty = $(this).parents('tr').find(".returnQty").val();
        var thisItemDetails = grnProducts[thisItemID];
        $('#viewReturnsSubProductsModal').modal('show');
        if (!jQuery.isEmptyObject(thisItemDetails.bP) && !jQuery.isEmptyObject(thisItemDetails.sP)) {
            for (var i in thisItemDetails.sP) {
                var clonedBPRow = $('#defaultRow').clone().attr('id', 'sub_' + i).removeClass('hidden').addClass('tempSubProductRow');
                $(".batchCode", clonedBPRow).html(thisItemDetails.sP[i].sBC);
                $(".serialID", clonedBPRow).html(thisItemDetails.sP[i].sC);
                $("input[name='availableQuantity']", clonedBPRow).val('1');
                $("input[name='returnQuantity']", clonedBPRow).remove();
                if (currentItemreturnAll == true) {
                    $("input[name='returnQuantityCheck']", clonedBPRow).prop('checked', true);
                    $("input[name='returnQuantityCheck']", clonedBPRow).attr('disabled', true);
                }
                if (typeof thisItemDetails.sP[i].sRQ != 'undefined' && thisItemDetails.sP[i].sRQ == "1") {
                    $("input[name='returnQuantityCheck']", clonedBPRow).prop('checked', true);
                }
                clonedBPRow.insertBefore('#defaultRow');
            }
        } else if (!jQuery.isEmptyObject(thisItemDetails.bP)) {
            for (var i in thisItemDetails.bP) {
                var availableQty = thisItemDetails.bP[i].bQ;
                var clonedBPRow = $('#defaultRow').clone().attr('id', 'sub_' + i).removeClass('hidden').addClass('tempSubProductRow');
                $(".batchCode", clonedBPRow).html(thisItemDetails.bP[i].bC);
                $("input[name='availableQuantity']", clonedBPRow).val((parseFloat(availableQty).toFixed(2))).addUom(locationProducts[pID[1]].uom);
                $("input[name='returnQuantityCheck']", clonedBPRow).remove();
                if (currentItemreturnAll == true) {
                    $("input[name='returnQuantity']", clonedBPRow).val(thisItemDetails.bP[i].bQ).addUom(locationProducts[pID[1]].uom);
                    $("input[name='returnQuantity']", clonedBPRow).attr('disabled', true);
                }
                if (typeof thisItemDetails.bP[i].bRQ != 'undefined') {
                    $("input[name='returnQuantity']", clonedBPRow).val(thisItemDetails.bP[i].bRQ).addUom(locationProducts[pID[1]].uom);
                }

                 $("input[name='returnQuantity']", clonedBPRow).addUom(locationProducts[pID[1]].uom);

                clonedBPRow.insertBefore('#defaultRow');
            }
        } else {
            for (var i in thisItemDetails.sP) {
                var clonedBPRow = $('#defaultRow').clone().attr('id', 'sub_' + i).removeClass('hidden').addClass('tempSubProductRow');
                $(".batchCode", clonedBPRow).html(thisItemDetails.sP[i].sBC);
                $(".serialID", clonedBPRow).html(thisItemDetails.sP[i].sC);
                $("input[name='availableQuantity']", clonedBPRow).val('1');
                $("input[name='returnQuantity']", clonedBPRow).remove();
                if (currentItemreturnAll == true) {
                    $("input[name='returnQuantityCheck']", clonedBPRow).prop('checked', true);
                    $("input[name='returnQuantityCheck']", clonedBPRow).attr('disabled', true);
                }
                if (typeof thisItemDetails.sP[i].sRQ != 'undefined' && thisItemDetails.sP[i].sRQ == "1") {
                    $("input[name='returnQuantityCheck']", clonedBPRow).prop('checked', true);
                }
                clonedBPRow.insertBefore('#defaultRow');
            }
        }
    });

    $('#viewReturnsSubProductsModal').on('click', '#subProductsSave', function() {
        if (currentItemreturnAll == true) {
            $('#viewReturnsSubProductsModal').modal('hide');
        } else {
            var totalReturnQty = 0;
            var thisItemDetails = grnProducts[currentReturnItemCode];
            if (!jQuery.isEmptyObject(thisItemDetails.bP) && !jQuery.isEmptyObject(thisItemDetails.sP)) {
                $('.tempSubProductRow').each(function() {
                    if ($(this).find($("input[name='returnQuantityCheck']")).is(":checked")) {
                        totalReturnQty++;
                    }
                });
                if (totalReturnQty > currentItemReturnQty) {
                    p_notification(false, eb.getMessage('ERR_PR_CHECKQUAL'));
                    return false;
                } else if (totalReturnQty < currentItemReturnQty) {
                    p_notification(false, eb.getMessage('ERR_PR_RESUBPRO'));
                    return false;
                } else {
                    for (var i in thisItemDetails.bP) {
                        thisItemDetails.bP[i]['bRQ'] = '0';
                    }
                    $('.tempSubProductRow').each(function() {
                        var subProID = (this.id).split("_")[1];
                        if ($(this).find($("input[name='returnQuantityCheck']")).is(":checked")) {
                            thisItemDetails.sP[subProID]['sRQ'] = "1";
                            var thisItemOwnBatch = thisItemDetails.sP[subProID].sBC;
                            for (var j in thisItemDetails.bP) {
                                if (thisItemDetails.bP[j].bC == thisItemOwnBatch) {
                                    if (typeof thisItemDetails.bP[j]['bRQ'] != "undefined") {
                                        thisItemDetails.bP[j]['bRQ'] = toFloat(thisItemDetails.bP[j]['bRQ']) + 1;
                                    } else {
                                        thisItemDetails.bP[j]['bRQ'] = '1';
                                    }
                                }
                            }
                        } else {
                            thisItemDetails.sP[subProID]['sRQ'] = "0";
                        }
                    });
                    thisItemDetails['itemsReturned'] = true;
                    $('#viewReturnsSubProductsModal').modal('hide');
                }
            } else if (!jQuery.isEmptyObject(thisItemDetails.bP)) {
                $('.tempSubProductRow').each(function() {
                    totalReturnQty += toFloat($(this).find($("input[name='returnQuantity']")).val());
                });
                if (totalReturnQty > currentItemReturnQty) {
                    p_notification(false, eb.getMessage('ERR_PR_CHECKQUAL'));
                    return false;
                } else if (totalReturnQty < currentItemReturnQty) {
                    p_notification(false, eb.getMessage('ERR_PR_RESUBPRO'));
                    return false;
                } else {
                    $('.tempSubProductRow').each(function() {
                        var subProID = (this.id).split("_")[1];
                        thisItemDetails.bP[subProID]['bRQ'] = $(this).find($("input[name='returnQuantity']")).val();
                    });
                    thisItemDetails['itemsReturned'] = true;
                    $('#viewReturnsSubProductsModal').modal('hide');
                }
            } else {
                $('.tempSubProductRow').each(function() {
                    if ($(this).find($("input[name='returnQuantityCheck']")).is(":checked")) {
                        totalReturnQty++;
                    }
                });
                if (totalReturnQty > currentItemReturnQty) {
                    p_notification(false, eb.getMessage('ERR_PR_CHECKQUAL'));
                    return false;
                } else if (totalReturnQty < currentItemReturnQty) {
                    p_notification(false, eb.getMessage('ERR_PR_RESUBPRO'));
                    return false;
                } else {
                    $('.tempSubProductRow').each(function() {
                        var subProID = (this.id).split("_")[1];
                        if ($(this).find($("input[name='returnQuantityCheck']")).is(":checked")) {
                            thisItemDetails.sP[subProID]['sRQ'] = "1";
                        } else {
                            thisItemDetails.sP[subProID]['sRQ'] = "0";
                        }
                    });
                    thisItemDetails['itemsReturned'] = true;
                    $('#viewReturnsSubProductsModal').modal('hide');
                }
            }
        }
    });

    $('#add-new-item-row').on('focusout', '.returnQty,.uomqty', function() {
        // var thisItemID = $(this).parents('tr').find("input[name=itemCode]").data('PC');
        var thisItemID = $(this).parents('tr').find("input[name=itemCode]").data('PII');
        var thisItemDetails = grnProducts[thisItemID];
        var baseqty = $(this).parents('tr').find('.returnQty').val();
        if (baseqty > toFloat(thisItemDetails.gPQ)) {
            p_notification(false, eb.getMessage('ERR_PR_RETQUANPURCHASE'));
            $(this).val(grnProducts[thisItemID].gPQ);
        } else if (isNaN(baseqty)) {
            p_notification(false, eb.getMessage('ERR_PR_PROPQUAL'));
            $(this).val('');
            $(this).parents('tr').find('.itemTotal').html('0.00');
            $(this).parents('tr').find('.taxUl').html('');
        } else if (baseqty == '0') {
            p_notification(false, eb.getMessage('ERR_PR_RETQUAN'));
            //p_notification(false, "Return quantity can't be 0");
            $(this).parents('tr').find('.itemTotal').html('0.00');
            $(this).val('');
            $(this).parents('tr').find('.taxUl').html('');
        } else {
            if (baseqty == '') {
                var newItemTotal = (toFloat(thisItemDetails.gPT) / toFloat(thisItemDetails.gPQ)) * toFloat(baseqty);
                $(this).parents('tr').find('.itemTotal').html(newItemTotal.toFixed(2));
            } else {
                var newItemTotal = (toFloat(thisItemDetails.gPT) / toFloat(thisItemDetails.gPQ)) * toFloat(baseqty);
                $(this).parents('tr').find('.itemTotal').html(newItemTotal.toFixed(2));
                var emptyTax = false;
                if (jQuery.isEmptyObject(thisItemDetails.pT)) {
                    emptyTax = true;
                }
                var gProTaxHtml = '';
                for (var k in thisItemDetails.pT) {
                    var gProductTax = thisItemDetails.pT[k];
                    var newitemTaxAmount = (toFloat(gProductTax.pTA) / toFloat(thisItemDetails.gPQ)) * toFloat(baseqty);
                    gProTaxHtml += "<li>&nbsp;&nbsp;<label>&nbsp;&nbsp;" + gProductTax.pTN + "(" + gProductTax.pTP + "%)&nbsp;&nbsp;-&nbsp;" + newitemTaxAmount.toFixed(2) + "</label></li>";
                }
                if (emptyTax) {
                    $(this).parents('tr').find('.taxUl').html('');
                } else {
                    $(this).parents('tr').find('.taxUl').html(gProTaxHtml);
                }
            }
        }
    });

    $('#add-new-item-row').on('click', '.addItem', function() {
        batchProducts = {};
        serialProducts = {};
        // var thisItemID = $(this).parents('tr').find("input[name=itemCode]").data('PC');
        var thisItemID = $(this).parents('tr').find("input[name=itemCode]").data('PII');
        var thisItemGrnProID =$(this).parents('tr').find("input[name=itemCode]").data('GRNPID');
        var thisItemReturnQty = $(this).parents('tr').find(".returnQty").val();
        var thisItemPurchasedQty = $(this).parents('tr').find(".purchasedQty").val();
        var thisItemTotal = $(this).parents('tr').find(".itemTotal").html();
        var thisItemUom = $(this).parents('tr').find(".returnQty").siblings('.uom-select').find('.selected').html();
        var uomReturnQty = $(this).parents('tr').find(".returnQty").siblings('.uomqty').val();
        var uomPurchasedQty = $(this).parents('tr').find(".purchasedQty").siblings('.uomqty').val();
        var uomPrice = $(this).parents('tr').find('.uomPrice').val();
        var thisItemTaxDiv = $(this).parents('tr').find(".taxDiv").html();
        var productType = $(this).parents('tr').find('input[name="itemCode"]').data('PT');

        grnProducts[thisItemID].selectedAvUomID = $(this).parents('tr').find(".purchasedQty").siblings('.uom-select').find('.selected').data('uomID');
        grnProducts[thisItemID].selectedRtUomID = $(this).parents('tr').find(".returnQty").siblings('.uom-select').find('.selected').data('uomID');
        if (thisItemPurchasedQty == thisItemReturnQty) {
            for (var i in grnProducts[thisItemID].bP) {
                var thisBatchItem = grnProducts[thisItemID].bP[i];
                batchProducts[thisBatchItem.bC] = new batchProduct(i, thisBatchItem.bC, thisBatchItem.bQ, thisBatchItem.bQ);
            }
            for (var j in grnProducts[thisItemID].sP) {
                var thisSerialItem = grnProducts[thisItemID].sP[j];
                serialProducts[thisSerialItem.sC] = new serialProduct(j, thisSerialItem.sC, thisSerialItem.sBC, '1',thisSerialItem.sGrnProductID);
            }

            for (var k in grnProducts[thisItemID].pT) {
                var thisTax = grnProducts[thisItemID].pT[k];
                thisTax.pTA = toFloat((thisTax.pTA / thisItemPurchasedQty * thisItemReturnQty)).toFixed(2);
            }
            var rItem = grnProducts[thisItemID];
            returnProducts[thisItemID] = new returnProduct(rItem.lPID, rItem.gPID, rItem.gPC, rItem.gPN, rItem.gPQ, thisItemReturnQty, rItem.gPD, rItem.gPP, thisItemTotal, rItem.pT, batchProducts, serialProducts, productType, thisItemGrnProID);

            var noSubProductsFlag = false;
            if (jQuery.isEmptyObject(grnProducts[thisItemID].bP) && jQuery.isEmptyObject(grnProducts[thisItemID].sP)) {
                noSubProductsFlag = true;
            }
            setAddedProductRow(thisItemID, uomPurchasedQty, uomReturnQty, thisItemUom, thisItemTaxDiv, thisItemTotal, noSubProductsFlag, uomPrice);
            setTotalTax();
            setPrTotalCost();
            $(this).parents('tr').remove();
        } else {
            if (typeof grnProducts[thisItemID].itemsReturned == "undefined" && (!jQuery.isEmptyObject(grnProducts[thisItemID].bP) || !jQuery.isEmptyObject(grnProducts[thisItemID].sP))) {
                $(this).parents('tr').find(".viewSubProducts").trigger('click');
            } else {
                for (var i in grnProducts[thisItemID].bP) {
                    var thisBatchItem = grnProducts[thisItemID].bP[i];
                    if (thisBatchItem.bRQ != "0" && thisBatchItem.bRQ != "") {
                        batchProducts[thisBatchItem.bC] = new batchProduct(i, thisBatchItem.bC, thisBatchItem.bQ, thisBatchItem.bRQ);
                    }
                }
                for (var j in grnProducts[thisItemID].sP) {
                    var thisSerialItem = grnProducts[thisItemID].sP[j];
                    if (thisSerialItem.sRQ != "0") {
                        serialProducts[thisSerialItem.sC] = new serialProduct(j, thisSerialItem.sC, thisSerialItem.sBC, thisSerialItem.sRQ, thisSerialItem.sGrnProductID);
                    }
                }

                for (var k in grnProducts[thisItemID].pT) {
                    var thisTax = grnProducts[thisItemID].pT[k];
                    thisTax.pTA = toFloat((thisTax.pTA / thisItemPurchasedQty * thisItemReturnQty)).toFixed(2);
                }
                var rItem = grnProducts[thisItemID];
                returnProducts[thisItemID] = new returnProduct(rItem.lPID, rItem.gPID, rItem.gPC, rItem.gPN, rItem.gPQ, thisItemReturnQty, rItem.gPD, rItem.gPP, thisItemTotal, rItem.pT, batchProducts, serialProducts, productType, thisItemGrnProID);

                var noSubProductsFlag = false;
                if (jQuery.isEmptyObject(grnProducts[thisItemID].bP) && jQuery.isEmptyObject(grnProducts[thisItemID].sP)) {
                    noSubProductsFlag = true;
                }

                setAddedProductRow(thisItemID, uomPurchasedQty, uomReturnQty, thisItemUom, thisItemTaxDiv, thisItemTotal, noSubProductsFlag, uomPrice);
                setTotalTax();
                setPrTotalCost();
                $(this).parents('tr').remove();
            }
        }
    });

    $('#prForm').submit(function(e) {
        e.preventDefault();
        if (directReturnNoteType) {
            saveDirectReturn();
        } else {
            saveReturn();
        }       
    });

    function saveReturn()
    {
        if ($('#startByCode').val() === '') {
            p_notification(false, eb.getMessage('ERR_PR_SELECTGRNCODE'));
            return false;
        }

        if (jQuery.isEmptyObject(returnProducts)) {
            p_notification(false, eb.getMessage('ERR_PR_ADDPRORETURN'));
            return false;
        } else if ($('.tempGrnPro').length > 0) {
            p_notification(false, eb.getMessage('ERR_PR_REMUNWANT'));
            return false;
        } else if ($('#returnDate').val() == '') {
            p_notification(false, eb.getMessage('ERR_PR_RETDATE'));
            return false;
        } else {
            $('#prSave').prop('disabled', true);
            var prFT = accounting.unformat($('#finaltotal').html());
            var showTax = 0;
            if ($('#showTax').is(':checked')) {
                showTax = 1;
            }
            var prPostData = {
                sID: grnData.gSID,
                gID: selectedGrnID,
                gCode : $('#startByCode').find("option:selected").text(),
                prC: $('#prNo').val(),
                rD: $('#receivedDate').val(),
                prD: $('#returnDate').val(),
                prSR: $('#supplierReference').val(),
                prL: grnData.gRLID,
                cm: $('#comment').val(),
                prRP: returnProducts,
                fT: prFT,
                sT: showTax,
                lRID: $('#locRefID').val(),
                dimensionData: dimensionData,
                ignoreBudgetLimit: ignoreBudgetLimitFlag
            };
            eb.ajax({
                type: 'POST',
                url: BASE_URL + '/api/pr/savePurchaseReturn',
                data: prPostData,
                success: function(respond) {
                    if (respond.status == true) {
                        p_notification(respond.status, respond.msg);
                        var fileInput = document.getElementById('documentFiles');
                        if(fileInput.files.length > 0) {
                            var form_data = false;
                            if (window.FormData) {
                                form_data = new FormData();
                            }
                            form_data.append("documentID", respond.data.prID);
                            form_data.append("documentTypeID", 11);
                            
                            for (var i = 0; i < fileInput.files.length; i++) {
                                form_data.append("files[]", fileInput.files[i]);
                            }

                            eb.ajax({
                                url: BASE_URL + '/store-files',
                                type: 'POST',
                                processData: false,
                                contentType: false,
                                data: form_data,
                                success: function(res) {
                                }
                            });
                        }

                        documentPreview(BASE_URL + '/pr/preview/' + respond.data.prID, 'documentpreview', "/api/pr/send-pr-email", function($preview) {
                            $preview.on('hidden.bs.modal', function() {
                                if (!$("#preview:visible").length) {
                                    window.location.reload();
                                }
                            });
                        });
                    } else {
                        if (respond.data == "NotifyBudgetLimit") {
                            bootbox.confirm(respond.msg+' ,Are you sure you want to save ?', function(result) {
                                if (result == true) {
                                    ignoreBudgetLimitFlag = true;
                                    saveReturn();
                                } else {
                                    setTimeout(function(){ 
                                        location.reload();
                                    }, 3000);
                                }
                            });
                        } else {
                            $('#prSave').prop('disabled', false);
                            p_notification(respond.status, respond.msg);
                        }
                    }
                },
                async: false
            });
        }
    }

    function saveDirectReturn()
    {
        if (jQuery.isEmptyObject(deliverProducts)) {
            p_notification(false, eb.getMessage('ERR_PR_ADDPRORETURN'));
            return false;
        } else if ($('#directSupplier').val().trim() === '' || $('#directSupplier').val() == null || $('#directSupplier').val() == 0) {
            p_notification(false, eb.getMessage('ERR_PR_SUPPLIER_EMPTY'));
            return false;
        } else if ($('#returnDate').val() == '') {
            p_notification(false, eb.getMessage('ERR_PR_RETDATE'));
            return false;
        } else {
            $('#prSave').prop('disabled', true);
            var prFT = accounting.unformat($('#directFinaltotal').html());
            var showTax = 0;
            if ($('#directTaxShow').is(':checked')) {
                showTax = 1;
            }
           
            var prPostData = {
                sID: $('#directSupplier').val(),
                prC: $('#prNo').val(),
                prD: $('#returnDate').val(),
                prL: locationID,
                cm: $('#comment').val(),
                prRP: deliverProducts,
                prRPSub: deliverSubProducts,
                fT: prFT,
                sT: showTax,
                lRID: $('#locRefID').val(),
                dimensionData: dimensionData,
                ignoreBudgetLimit : ignoreBudgetLimitFlag
            };
            eb.ajax({
                type: 'POST',
                url: BASE_URL + '/api/pr/saveDirectPurchaseReturn',
                data: prPostData,
                success: function(respond) {
                    if (respond.status == true) {
                        p_notification(respond.status, respond.msg);
                        var fileInput = document.getElementById('documentFiles');
                        if(fileInput.files.length > 0) {
                            var form_data = false;
                            if (window.FormData) {
                                form_data = new FormData();
                            }
                            form_data.append("documentID", respond.data.prID);
                            form_data.append("documentTypeID", 11);
                            
                            for (var i = 0; i < fileInput.files.length; i++) {
                                form_data.append("files[]", fileInput.files[i]);
                            }

                            eb.ajax({
                                url: BASE_URL + '/store-files',
                                type: 'POST',
                                processData: false,
                                contentType: false,
                                data: form_data,
                                success: function(res) {
                                }
                            });
                        }
                        documentPreview(BASE_URL + '/pr/preview/' + respond.data.prID, 'documentpreview', "/api/pr/send-pr-email", function($preview) {
                            $preview.on('hidden.bs.modal', function() {
                                if (!$("#preview:visible").length) {
                                    window.location.reload();
                                }
                            });
                        });
                    } else {
                        if (respond.data == "NotifyBudgetLimit") {
                            bootbox.confirm(respond.msg+' ,Are you sure you want to save ?', function(result) {
                                if (result == true) {
                                    ignoreBudgetLimitFlag = true;
                                    saveDirectReturn();
                                } else {
                                    setTimeout(function(){ 
                                        location.reload();
                                    }, 3000);
                                }
                            });
                        } else {
                            $('#prSave').prop('disabled', false);
                            p_notification(respond.status, respond.msg);
                        }
                    }
                },
                async: false
            });
        }
    }

    $('table#returnProductTable>tbody').on('keypress', 'input.returnQty, input#unitPrice, input#deliveryNoteDiscount, #deliverQuanity,#availableQuantity ', function(e) {

        if (e.which == 13) {
            $('button.addItem ', $(this).parents('tr')).trigger('click');
            e.preventDefault();
        }

    });

    function setTotalTax() {
        totalTaxList = {};
        for (var k in returnProducts) {
            if (!jQuery.isEmptyObject(returnProducts[k].pTax)) {
                for (var l in returnProducts[k].pTax) {
                    if (totalTaxList.hasOwnProperty(l)) {
                        var curTA = totalTaxList[l].tA;
                        var newTA = (toFloat(curTA) + toFloat(returnProducts[k].pTax[l].pTA));
                        totalTaxList[l].tA = newTA.toFixed(2);
                    } else {
                        totalTaxList[l] = {tN: returnProducts[k].pTax[l].pTN, tP: returnProducts[k].pTax[l].pTP, tA: returnProducts[k].pTax[l].pTA};
                    }
                }
            }
        }
        var totalTaxHtml = "";
        for (var t in totalTaxList) {
            totalTaxHtml += totalTaxList[t].tN + "(" + totalTaxList[t].tP + "%)&nbsp" + accounting.formatMoney(totalTaxList[t].tA) + "<br>";
        }
        $('#totalTaxShow').html(totalTaxHtml);
    }

    function setPrTotalCost() {
        var prTotal = 0;
        for (var i in returnProducts) {
            prTotal += toFloat(returnProducts[i].pTotal);
        }
        $('#finaltotal').html(accounting.formatMoney(prTotal));
    }

    function setAddedProductRow(thisItemID, thisItemPurchasedQty, thisItemReturnQty, thisItemUom, thisItemTaxDiv, thisItemTotal, noSubProductsFlag, uomPrice) {
        var clonedRetRow = $('#preSetSample').clone().attr('id', thisItemID).removeClass('hidden').addClass('returnedProducts');
        $('#code', clonedRetRow).html(grnProducts[thisItemID].gPC + ' - ' + grnProducts[thisItemID].gPN);
        $('#code', clonedRetRow).attr('id', '');
        $('#purQty', clonedRetRow).html(thisItemPurchasedQty + " " + thisItemUom);
        $('#purQty', clonedRetRow).attr('id', '');
        $('#retQty', clonedRetRow).html(thisItemReturnQty + " " + thisItemUom);
        $('#retQty', clonedRetRow).attr('id', '');
        $('#unit', clonedRetRow).html(uomPrice);
        $('#unit', clonedRetRow).attr('id', '');
        $('#disc', clonedRetRow).html(accounting.formatMoney(grnProducts[thisItemID].gPD));
        $('#disc', clonedRetRow).attr('id', '');
        $('#tax', clonedRetRow).html(thisItemTaxDiv);
        $('#tax', clonedRetRow).attr('id', '');
        $('#ttl', clonedRetRow).html(accounting.formatMoney(thisItemTotal));
        $('#ttl', clonedRetRow).attr('id', '');
        if (noSubProductsFlag) {
            $('.returnViewSubProducts', clonedRetRow).addClass('hidden');

        }
        clonedRetRow.insertBefore('#preSetSample');
    }

    function clearModalWindow() {
        $('.tempSubProductRow').remove();
        batchProducts = {};
        serialProducts = {};
        $('#subProductsSave').removeClass('hidden');
    }

    function setGrnProducts(grnProducts) {
        for (var i in grnProducts) {
            var indexKey = i;
            var gProduct = grnProducts[i];
            var baseUom;
            var baseDecimal;
            for (var j in gProduct.pUom) {
                if (gProduct.pUom[j].uC == 1) {
                    baseUom = gProduct.pUom[j].uA;
                    baseDecimal = gProduct.pUom[j].uDP;
                }
            }
            var gProTaxHtml = "";
            var emptyTax = false;
            if (jQuery.isEmptyObject(gProduct.pT)) {
                emptyTax = true;
            }
            for (var k in gProduct.pT) {
                var gProductTax = gProduct.pT[k];
                gProTaxHtml += "<li>&nbsp;&nbsp;<label>&nbsp;&nbsp;" + gProductTax.pTN + "(" + gProductTax.pTP + "%)&nbsp;&nbsp;-&nbsp;" + gProductTax.pTA + "</label></li>";
            }
            var clonedAddNew = $($('#addNewItemTr').clone()).attr('id', 'grnPro_' + gProduct.gPID).addClass('tempGrnPro').removeClass('hidden');
            $('#itemCode', clonedAddNew).val(gProduct.gPC + ' - ' + gProduct.gPN);
            $('#itemCode', clonedAddNew).data('PT', gProduct.productType);
            $('#itemCode', clonedAddNew).data('PC', gProduct.gPC);
            $('#itemCode', clonedAddNew).data('PN', gProduct.gPN);
            // PII - product index id
            $('#itemCode', clonedAddNew).data('PII', indexKey);
            $('#itemCode', clonedAddNew).data('GRNPID', gProduct.grnProductID);
            $('#itemCode', clonedAddNew).prop('disabled', true);
            $('#itemCode', clonedAddNew).attr('id', '');
            if (gProduct.gPQ == null) {
                gProduct.gPQ = 0;
            }
            var qty = parseFloat(gProduct.gPQ).toFixed(baseDecimal);
            $('#purchasedQuantity', clonedAddNew).val(qty);
            $('#purchasedQuantity', clonedAddNew).prop('disabled', true);
            $('#purchasedQuantity', clonedAddNew).addUom(gProduct.pUom);
            $('#purchasedQuantity', clonedAddNew).attr('id', '');
            $('.returnQty', clonedAddNew).val(gProduct.gPQ).addUom(gProduct.pUom);
            $('.taxUl', clonedAddNew).html(gProTaxHtml);
            if (emptyTax) {
                $('#tax', clonedAddNew).html('');
            }
            $('#tax', clonedAddNew).attr('id', '');
            $('#addNewUom', clonedAddNew).attr('id', '').prop('disabled', true);
            $('#addUomUl', clonedAddNew).attr('id', '');
            $('#unitPrice', clonedAddNew).val(gProduct.gPP);
            $('#unitPrice', clonedAddNew).prop('disabled', true);
            $('#unitPrice', clonedAddNew).attr('id', '').addUomPrice(gProduct.pUom);

            var baseUom;
            for (var j in locationProducts[gProduct.gPID].uom) {
                if (locationProducts[gProduct.gPID].uom[j].pUBase == 1) {
                    baseUom = locationProducts[gProduct.gPID].uom[j].uomID;
                }
            }

            $('#prDiscount', clonedAddNew).prop('disabled', true);
            if (locationProducts[gProduct.gPID].pPDV) {
                $('#prDiscount',clonedAddNew).val(gProduct.gPD).addUomPrice(locationProducts[gProduct.gPID].uom, baseUom);
                $('#prDiscount',clonedAddNew).siblings('.uom-price-select').find('.uom_price_drop_down').attr('disabled', true);
                $(".sign", clonedAddNew).text('Rs');
                $('#prDiscount', clonedAddNew).attr('id', '');
            }
            if (locationProducts[gProduct.gPID].pPDP) {
                $('#prDiscount',clonedAddNew).val(gProduct.gPD);
                $(".sign", clonedAddNew).text('%');
                $('#prDiscount', clonedAddNew).attr('id', '');
            }

            if (locationProducts[gProduct.gPID].pPDP == null && locationProducts[gProduct.gPID].pPDV == null) {
                $('#prDiscount',clonedAddNew).attr('readonly', true);
                $('#prDiscount', clonedAddNew).attr('id', '');
            }

            if (gProduct.productType == 2 && gProduct.gPQ == 0) {
                gProduct.gPT = gProduct.gPP;
            }
            $('#addNewTotal', clonedAddNew).html(gProduct.gPT);
            $('#addNewTotal', clonedAddNew).attr('id', '');
            if (jQuery.isEmptyObject(gProduct.bP) && jQuery.isEmptyObject(gProduct.sP)) {
                $('.viewSubProducts', clonedAddNew).remove();
            }
            clonedAddNew.insertBefore('#addNewItemTr');
        }
    }

    function loadGRN(grnID) {
        eb.ajax({
            type: 'POST',
            url: BASE_URL + '/api/grn/getGrnDetailsForPr',
            data: {grnID: grnID},
            success: function(respond) {
                if (respond.data.inactiveItemFlag) {
                    p_notification(false, respond.data.errorMsg);
                    setTimeout(function(){ 
                        window.location.assign(BASE_URL + "/pr/create")
                    }, 3000);
                    return false;
                }
                if (respond.msg != '') {
                    p_notification(false, respond.msg);
                }
                grnData = respond.data.grnData;
                selectedGrnID = grnID;
                $('#supplier').val(grnData.gSN);

                //start : make delivery date to standrd format for the validation
                var receiveDate = grnData.gD;
                var params;
                var format;
                var dateformat = $('#receivedDate').data("date-format");
                var slash = receiveDate.indexOf("/");
                var dot = receiveDate.indexOf(".");
                var dash = receiveDate.indexOf("-");

                if (dot > 0) {
                    params = receiveDate.split(".");
                    format = dateformat.split(".");
                }
                if (slash > 0) {
                    params = receiveDate.split("/");
                    format = dateformat.split("/");
                }
                if (dash > 0) {
                    params = receiveDate.split("-");
                    format = dateformat.split("-");
                }

                var joindate;
                if (format[0] == 'yyyy') {
                    joindate = new Date(params[0], params[1] - 1, params[2]);
                }
                else if (format[0] == 'dd') {
                    joindate = new Date(params[2], params[1] - 1, params[0]);
                }
                else if (format[0] == 'mm') {
                    joindate = new Date(params[2], params[0] - 1, params[1]);
                }
                //date formation end

                $('#receivedDate').val(grnData.gD);
                var nowTemp = new Date();
                var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);
                var gDate = new Date(Date.parse(joindate));
                var newgDate = new Date(gDate.setDate(gDate.getDate()));
                if ($('#returnDate').data('datepicker')) {
                    $('#returnDate').removeData('datepicker');
                }
                var returnDate = $('#returnDate').datepicker({
                    onRender: function(date) {
                        return ((date.valueOf() < newgDate.valueOf()) ? ((U_DATE_R_OVERRIDE)?'':'disabled') : '' || (date.valueOf() > now.valueOf()) ? ((U_DATE_R_OVERRIDE)?'':'disabled') : '');
                    }
                }).on('changeDate', function(ev) {
                    returnDate.hide();
                }).data('datepicker');

                $('#retrieveLocation').val(grnData.gRL);
                $('#supplierReference').val(grnData.gSR);
                $('#comment').val(grnData.gC);
                retireveLocation = grnData.gRLID;
                setLocationProducts(grnData.gProducts, retireveLocation);
                setGrnProducts(grnData.gProducts);
                grnProducts = grnData.gProducts;

            },
            async: false
        });
        eb.ajax({
            type: 'POST',
            url: BASE_URL + '/api/pr/getPrNoForLocation',
            data: {locationID: retireveLocation},
            success: function(respond) {
                if (respond.status == false) {
                    p_notification(false, respond.msg);
                    $('#prNo').val('');
                }
                else if (respond.status == true) {
                    $('#prNo').val(respond.data.refNo);
                    $('#locRefID').val(respond.data.locRefID);
                }
            },
            async: false
        });
    }

    function setLocationProducts(grnProducts, grnLocID) {

        for (var i in grnProducts) {
            var gProduct = grnProducts[i];
            eb.ajax({
                type: 'POST',
                url: BASE_URL + '/productAPI/get-location-product-details',
                data: {productID: gProduct.gPID, locationID: grnLocID},
                success: function(respond) {
                    var currentElem = new Array();
                    currentElem[gProduct.gPID] = respond.data;
                    locationProducts = $.extend(locationProducts, currentElem);
                },
                async: false
            });
        }
        
    }

    function clearProductDataForNewReturn() {
        var $currentRow = getAddRow();
        $currentRow.removeClass('hidden');
        deliverProducts = {};
        deliverSubProducts = {};
        productsTax = {};
        productsTotal = {};
        $('#comment').val('');
        $('#subtotal').html('0.00');
        $('#directFinaltotal').html('0.00');
    }

    function setProductTypeahead(hideDuplicate) {

        var $currentRow = getAddRow();
        $('#item_code', $currentRow).selectpicker();
        if (hideDuplicate) {
            $(".btn-group.bootstrap-select.form-control.show-tick:not(:first)", $currentRow).remove();
        }
        var documentType = 'invoice';
        loadDropDownFromDatabase('/productAPI/search-location-products-for-dropdown/serial-search', locationID, 1, '#item_code', $currentRow, '', documentType);
        
        $('#item_code', $currentRow).on('change', function() {
            if ($(this).val() != 'add' && $(this).val() != 0 && selectedId != $(this).val()) {
                selectedId = $(this).val();
                var itemMapper = $('#item_code').data('extra');
                var pid = itemMapper[selectedId].pid;

                eb.ajax({
                    type: 'POST',
                    url: BASE_URL + '/productAPI/get-location-product-details',
                    data: {productID: pid, locationID: locationID},
                    success: function(respond) {
                        selectProductForTransfer(respond.data);
                        $('#itemUnitPrice', $currentRow).trigger('blur');
                    }
                });
            }
        });
    }

    function selectProductForTransfer(selectedlocationProduct) {

        var sameItemFlag = false;
        $.each(deliverProducts , function(i, delPro){
            // check if product is already selected
            if (delPro.productID == selectedlocationProduct.pID) {
                p_notification(false, eb.getMessage('ERR_DELI_ALREADY_SELECT_PROD'));
                sameItemFlag = true;
            }
        });

        if (sameItemFlag == false) {
            var $currentRow = getAddRow();
            clearProductRow($currentRow);

            currentProducts[selectedlocationProduct.pID] = selectedlocationProduct;
            var productID = selectedlocationProduct.pID;
            var productCode = selectedlocationProduct.pC;
            var productType = selectedlocationProduct.pT;
            var productName = selectedlocationProduct.pN;
            var giftCard = selectedlocationProduct.giftCard;
            var availableQuantity = (selectedlocationProduct.LPQ == null) ? 0 : selectedlocationProduct.LPQ;
            var defaultSellingPrice = selectedlocationProduct.dPP;
            if (selectedlocationProduct.pPDP != null) {
                var productDiscountPrecentage = parseFloat(selectedlocationProduct.pPDP).toFixed(2);
            } else {
                var productDiscountPrecentage = 0;
            }
            
            if (selectedlocationProduct.pPDV != null) {
                var productDiscountValue = parseFloat(selectedlocationProduct.pPDV).toFixed(2);
            } else {
                var productDiscountValue = 0;
            }

            if (priceListItems[productID] !== undefined && parseFloat(priceListItems[productID].itemPrice) > 0) {
                defaultSellingPrice = parseFloat(priceListItems[productID].itemPrice);
                var itemDiscountType = priceListItems[productID].itemDiscountType;
                productDiscountValue = (itemDiscountType == 1) ? priceListItems[productID].itemDiscount : 0;
                productDiscountPrecentage = (itemDiscountType == 2) ? priceListItems[productID].itemDiscount : 0;
            }
            defaultSellingPriceData[productID] = defaultSellingPrice;

            $currentRow.data('id', selectedlocationProduct.pID);
            $currentRow.data('icid', rowincrementID);
            $currentRow.data('locationproductid', selectedlocationProduct.lPID);
            $currentRow.data('proIncID', rowincrementID);
            $currentRow.data('stockupdate', true);
            $('#availableQuantity', $currentRow).parent().addClass('input-group');
            $('#deliverQuanity', $currentRow).parent().addClass('input-group');
            $("input[name='discount']", $currentRow).prop('readonly', false);
            $("#item_code", $currentRow).data('PT', productType);
            $("#item_code", $currentRow).data('PN', productName);
            $("#item_code", $currentRow).data('PC', productCode);
            $("#item_code", $currentRow).data('GiftCard', giftCard);
            $("input[name='availableQuantity']", $currentRow).val(availableQuantity).prop('readonly', true);
            $("input[name='unitPrice']", $currentRow).val(parseFloat(defaultSellingPrice)).addUomPrice(selectedlocationProduct.uom);

            var baseUom;
            for (var j in selectedlocationProduct.uom) {
                if (selectedlocationProduct.uom[j].pUBase == 1) {
                    baseUom = selectedlocationProduct.uom[j].uomID;
                }
            }

            if (selectedlocationProduct.dPEL == 1 || (priceListItems[productID] != undefined && parseFloat(priceListItems[productID].itemPrice) > 0)) {
                if (selectedlocationProduct.pPDV != null) {
                    productDiscountValue = parseFloat(productDiscountValue).toFixed(2);
                    $("input[name='discount']", $currentRow).val(productDiscountValue).addUomPrice(selectedlocationProduct.uom, baseUom);
                    $("input[name='discount']", $currentRow).siblings('.uom-price-select').find('.uom_price_drop_down').attr('disabled', true);
                    $("input[name='discount']", $currentRow).addClass('value');
                    if ($("input[name='discount']", $currentRow).hasClass('precentage')) {
                        $("input[name='discount']", $currentRow).removeClass('precentage');
                    }
                    $(".sign", $currentRow).text('Rs');
                } else if (selectedlocationProduct.pPDP != null){
                    productDiscountPrecentage = parseFloat(productDiscountPrecentage).toFixed(2);
                    $("input[name='discount']", $currentRow).val(productDiscountPrecentage);
                    $("input[name='discount']", $currentRow).addClass('precentage');
                    if ($("input[name='discount']", $currentRow).hasClass('value')) {
                        $("input[name='discount']", $currentRow).removeClass('value');
                    }
                    $(".sign", $currentRow).text('%');
                }
            } else {
                $("input[name='discount']", $currentRow).prop('readonly', true);
            }
            $("input[name='deliverQuanity']", $currentRow).prop('readonly', false);
            
            // add uom list
            if (productType != 2) {
                $("input[name='availableQuantity'], input[name='deliverQuanity']", $currentRow).addUom(selectedlocationProduct.uom);
            } else {
                $("input[name='deliverQuanity']", $currentRow).addUom(selectedlocationProduct.uom);
                $("input[name='availableQuantity']", $currentRow).css("display", "none");
            }
            $('.uomqty').attr("id", "itemQuantity");
            $('#unitPrice',$currentRow).siblings('.uomPrice').attr("id", "itemUnitPrice");
            var deleted = 0;
            
            // clear old rows
            $("#batch_data tr:not(.hidden)").remove();
            addBatchProduct(productID, productCode, deleted);
            addSerialProduct(productID, productCode, deleted, []);
            addSerialBatchProduct(productID, productCode, deleted);

            //check this product serail or batch
            if (!$.isEmptyObject(selectedlocationProduct.serial)) {
                $('#product-batch-modal .serial-auto-select').removeClass('hidden');
                $('#numberOfRow').val('');
            } else {
                $('#product-batch-modal .serial-auto-select').addClass('hidden');
            }


            var rowincID = rowincrementID;
            $("tr", '#batch_data').each(function() {
                var $thisSubRow = $(this);
                for (var i in subProductsForCheck[productID]) {
                    if (subProductsForCheck[productID][i].serialID !== undefined) {
                        if ($(".serialID", $thisSubRow).data('id') == subProductsForCheck[productID][i].serialID) {
                            $("input[name='deliverQuantityCheck']", $thisSubRow).prop('checked', true);
                            $("input[name='warranty']", $thisSubRow).val(subProductsForCheck[productID][i].warranty);
                            $("input[name='deliverQuantityCheck']", $thisSubRow).attr('disabled', true);
                        }

                    } else if (subProductsForCheck[productID][i].batchID !== undefined && (rowincID == subProductsForCheck[productID][i].incrementID) ) {
                        if ($(".batchCode", $thisSubRow).data('id') == subProductsForCheck[productID][i].batchID) {
                            $(this).find("input[name='deliverQuantity']").val(subProductsForCheck[productID][i].qtyByBase).change();
                        }
                    }
                }
            });

            //check this product serail or batch
            if (!$.isEmptyObject(selectedlocationProduct.serial)) {
                $('#add-product-batch .serial-auto-select').removeClass('hidden');
                $('#numberOfRow').val('');
            } else {
                $('#add-product-batch .serial-auto-select').addClass('hidden');
            }

            // check if any batch / serial products exist
            if ($.isEmptyObject(selectedlocationProduct.batch) &&
                    $.isEmptyObject(selectedlocationProduct.serial) &&
                    $.isEmptyObject(selectedlocationProduct.batchSerial)) {
                $currentRow.removeClass('subproducts');
                $('.edit-modal', $currentRow).parent().addClass('hidden');
                $("td[colspan]", $currentRow).attr('colspan', 2);
                $("input[name='deliverQuantity']", $currentRow).prop('readonly', false).focus();
            } else {
                $currentRow.addClass('subproducts');
                $('.edit-modal', $currentRow).parent().removeClass('hidden');
                $("td[colspan]", $currentRow).attr('colspan', 1);
                $("input[name='deliverQuantity']", $currentRow).prop('readonly', true);
                $('#addDirectReturnProductsModal').data('icid', rowincrementID);
                $('#addDirectReturnProductsModal').data('proIncID', rowincrementID);
                $('#addDirectReturnProductsModal').modal('show');
            }

            setTaxListForProduct(selectedlocationProduct.pID, $currentRow);
            $currentRow.attr('id', 'product' + selectedlocationProduct.pID);
        }
    }

    function clearProductRow($currentRow) {
        $("input[name='availableQuantity']", $currentRow).val('').siblings('.uomqty,.uom-select').remove().end().show();
        $("input[name='deliverQuanity']", $currentRow).val('').siblings('.uomqty,.uom-select').remove().end().show();
        $("input[name='unitPrice']", $currentRow).val('').siblings('.uomPrice,.uom-price-select').remove().end().show();
        $("input[name='discount']", $currentRow).val('').siblings('.uomPrice,.uom-price-select').remove().end().show();
        $(".tempLi", $currentRow).remove('');
        $("#taxApplied", $currentRow).removeClass('glyphicon-check');
        $("#taxApplied", $currentRow).addClass('glyphicon-unchecked');
        $(".uomList", $currentRow).remove('');
    }

    function addBatchProduct(productID, productCode, deleted) {

        if ((!$.isEmptyObject(currentProducts[productID].batch))) {
            $('.warranty').removeClass('hidden');
            $('.expDate').removeClass('hidden');
            batchProduct = currentProducts[productID].batch;
            for (var i in batchProduct) {
                if (deleted != 1 && batchProduct[i].PBQ != 0) {
                    if ((!$.isEmptyObject(currentProducts[productID].productIDs))) {
                        if (currentProducts[productID].productIDs[i]) {
                            continue;
                        }
                    }

                    var batchKey = productCode + "-" + batchProduct[i].PBID;
                    var $clonedRow = $($('.batch_row').clone());
                    $clonedRow
                            .data('id', i)
                            .removeAttr('id')
                            .removeClass('hidden batch_row')
                            .addClass('remove_row');
                    $(".batchCode", $clonedRow).html(batchProduct[i].PBC).data('id', batchProduct[i].PBID);
                    $("input[name='availableQuantity']", $clonedRow).val(batchProduct[i].PBQ);
                    $("input[name='deliverQuantityCheck']", $clonedRow).remove();
                    $("input[name='warranty']", $clonedRow).prop('readonly', true);
                    $("input[name='warranty']", $clonedRow).val(batchProduct[i].PBWoD);
                    $("input[name='expireDate']", $clonedRow).val(batchProduct[i].PBExpD);
                    $("input[name='btPrice']", $clonedRow).val(parseFloat(batchProduct[i].PBUP).toFixed(2));
                    if (priceListItems[productID] != undefined && parseFloat(priceListItems[productID].itemPrice) > 0) {
                        $("input[name='btPrice']", $clonedRow).val(parseFloat(priceListItems[productID].itemPrice).toFixed(2));
                    }
                    if (batchProducts[batchKey]) {
                        $("input[name='deliverQuantity']", $clonedRow).val(batchProducts[batchKey].Qty);
                    }
                    if(!(batchProduct[i].PBExpD == null || batchProduct[i].PBExpD == '0000-00-00')){
                        var expireCheck = checkEpireData(batchProduct[i].PBExpD);
                        if(expireCheck){
                            $("input[name='deliverQuantity']", $clonedRow).attr('disabled',true);
                        }
                    }

                    $clonedRow.insertBefore('.batch_row');
                    $("input[name='availableQuantity'],input[name='deliverQuantity']", $clonedRow).addUom(currentProducts[productID].uom);
                } else {
                    batchProducts[batchKey] = undefined;
                }
            }
        }
    }

    function addSerialProduct(productID, productCode, deleted) {
        if ((!$.isEmptyObject(currentProducts[productID].serial))) {
            productSerial = currentProducts[productID].serial;

            var subProList = deliverSubProducts;
            var list_order = new Array();
            for (var i in subProList) {
                if (subProList[i] !== undefined && productSerial[subProList[i][0]['serialID']]) {
                    list_order.push(subProList[i][0]['serialID']);
                }
            }

            for (var i in productSerial) {
                if (!_.where(list_order, i).length) {
                    list_order.push(i);
                } else {
                    continue;
                }
            }

            for (var id in list_order) {
                var i = list_order[id];
                if (deleted != 1) {
                    var serialKey = productCode + "-" + productSerial[i].PSID;
                    var serialQty = (productSerial[i].PSS == 1) ? 0 : 1;
                    var $clonedRow = $($('.batch_row').clone());
                    $clonedRow
                            .data('id', i)
                            .removeAttr('id')
                            .removeClass('hidden batch_row')
                            .addClass('remove_row');
                    $(".serialID", $clonedRow)
                            .html(productSerial[i].PSC)
                            .data('id', productSerial[i].PSID);
                    $("input[name='availableQuantity']", $clonedRow).val(serialQty);
                    $("input[name='deliverQuantity']", $clonedRow).parent().remove();
                    if (currentProducts[productID].giftCard == 0) {
                        $(".warranty").removeClass('hidden');
                        $(".warrant").removeClass('hidden');
                        $(".expDate").removeClass('hidden');
                        let wPeriod = "";
                        if(productSerial[i].PSWoT==='4'){
                            wPeriod = "Years"
                        }else if(productSerial[i].PSWoT==='3'){
                            wPeriod = "Months"
                        }else if(productSerial[i].PSWoT==='2'){
                            wPeriod = "Weeks"
                        }else{
                            wPeriod = "Days"
                        }
                        $("input[name='warrantyType']",$clonedRow).val(wPeriod).attr('disabled',true);
                        $("input[name='warranty']", $clonedRow).val(productSerial[i].PSWoD);
                        $("input[name='expireDate']", $clonedRow).val(productSerial[i].PSExpD);
                        if(!(productSerial[i].PSExpD == null || productSerial[i].PSExpD == '0000-00-00')){
                            var expireCheck = checkEpireData(productSerial[i].PSExpD);
                            if(expireCheck){
                                $("input[name='deliverQuantityCheck']", $clonedRow).attr('disabled',true);
                            }
                        }
                    }

                    if (serialProducts[serialKey]) {
                        if (serialProducts[serialKey].Qty == 1) {
                            $("input[name='deliverQuantityCheck']", $clonedRow).attr('checked', true);
                        }
                    }

                    $clonedRow.insertBefore('.batch_row');
                    $("input[name='availableQuantity'],input[name='deliverQuantity']", $clonedRow).addUom(currentProducts[productID].uom);
                } else {
                    serialProducts[serialKey] = undefined;
                }
            }
        }
    }

    function addSerialBatchProduct(productID, productCode, deleted) {

        if ((!$.isEmptyObject(currentProducts[productID].batchSerial))) {
            productBatchSerial = currentProducts[productID].batchSerial;
            for (var i in productBatchSerial) {
                if (deleted != 1) {
                    var serialKey = productCode + "-" + productBatchSerial[i].PBID;
                    var serialQty = (productBatchSerial[i].PSS == 1) ? 0 : 1;
                    var $clonedRow = $($('.batch_row').clone());
                    $clonedRow
                            .data('id', i)
                            .removeAttr('id')
                            .removeClass('hidden batch_row')
                            .addClass('remove_row');
                    $(".serialID", $clonedRow)
                            .html(productBatchSerial[i].PSC)
                            .data('id', productBatchSerial[i].PSID);
                    $(".batchCode", $clonedRow)
                            .html(productBatchSerial[i].PBC)
                            .data('id', productBatchSerial[i].PBID);
                    $("input[name='availableQuantity']", $clonedRow).val(serialQty);
                    $("input[name='deliverQuantity']", $clonedRow).parent().remove();
                    $("input[name='btPrice']", $clonedRow).val(parseFloat(productBatchSerial[i].PBUP).toFixed(2));
                    if (priceListItems[productID] != undefined && parseFloat(priceListItems[productID].itemPrice) > 0) {
                        $("input[name='btPrice']", $clonedRow).val(parseFloat(priceListItems[productID].itemPrice).toFixed(2));
                    }
                    if (currentProducts[productID].giftCard == 0) {
                        $(".warranty").removeClass('hidden');
                        $(".expDate").removeClass('hidden');
                        $("input[name='warranty']", $clonedRow).val(productBatchSerial[i].PBSWoD);
                        $("input[name='expireDate']", $clonedRow).val(productBatchSerial[i].PBSExpD);
                        if(!(productBatchSerial[i].PBSExpD == null || productBatchSerial[i].PBSExpD == '0000-00-00')){
                            var expireCheck = checkEpireData(productBatchSerial[i].PBSExpD);
                            if(expireCheck){
                                $("input[name='deliverQuantityCheck']", $clonedRow).attr('disabled',true);
                            }
                        }
                    }
                    if (batchProducts[serialKey]) {
                        if (batchProducts[serialKey].Qty == 1) {
                            $("input[name='deliveryQuantityCheck']", $clonedRow).attr('checked', true);
                        }
                    }

                    $clonedRow.insertBefore('.batch_row');
                    $("input[name='availableQuantity']", "input[name='deliverQuantity']", $clonedRow).addUom(currentProducts[productID].uom);
                } else {
                    batchProducts[serialKey] = undefined;
                }
            }
        }
    }

    function setTaxListForProduct(productID, $currentRow) {

        if ((!jQuery.isEmptyObject(currentProducts[productID].tax))) {
            productTax = currentProducts[productID].tax;
            $currentRow.find('#taxApplied').removeClass('glyphicon glyphicon-unchecked');
            $currentRow.find('#taxApplied').addClass('glyphicon glyphicon-check');
            $currentRow.find('.tempLi').remove();
            $currentRow.find('#addNewTax').attr('disabled', false);
            for (var i in productTax) {
                var clonedLi = $($currentRow.find('#sampleLi').clone()).removeClass('hidden').attr('id', 'li_' + i).addClass("tempLi");
                clonedLi.children(".taxChecks").attr('id', productID + '_' + i).prop('checked', true).addClass('addNewTaxCheck');
                if (productTax[i].tS == 0) {
                    clonedLi.children(".taxChecks").attr('disabled', 'disabled');
                    clonedLi.children(".taxName").addClass('crossText');
                }
                clonedLi.children(".taxName").html('&nbsp&nbsp' + productTax[i].tN + '&nbsp&nbsp' + productTax[i].tP + '%').attr('for', productID + '_' + i);
                clonedLi.insertBefore($currentRow.find('#sampleLi'));
            }
        } else {
            $('#taxApplied').addClass('glyphicon glyphicon-unchecked');
            $currentRow.find('#addNewTax').attr('disabled', true);
        }
    }

    $productTable.on('focusout', '#deliverQuanity,#unitPrice,#prDiscount,.uomqty,.uomPrice', function() {
        var $thisRow = $(this).parents('tr');
        var productID = $thisRow.data('id');
        calculateItemTotal($thisRow, productID);
    });

    function calculateItemTotal(thisRow, productID) {
        var thisRow = thisRow;
        var productID = productID;
        var checkedTaxes = Array();
        thisRow.find('input:checkbox.addNewTaxCheck').each(function() {
            if (this.checked) {
                var cliTID = this.id;
                var tTaxID = cliTID.split('_')[1];
                checkedTaxes.push(tTaxID);
            }
        });
        var productType = thisRow.find('#item_code').data('PT');
        var tmpItemQuentity = thisRow.find('#deliverQuanity').val();
        if (productType == 2 && tmpItemQuentity == 0) {
            tmpItemQuentity = 1;
        }
        var tmpItemTotal = (toFloat(thisRow.find('#unitPrice').val()) * toFloat(tmpItemQuentity));
        var tmpItemCost = tmpItemTotal;
 
        if (currentProducts[productID].dPEL == 1 && (currentProducts[productID].pPDV != null || currentProducts[productID].pPDP != null)) {
            //current product dsiscount (May changed due to insertion of promotion but there's a validation below)
            var currentDiscount = 0;
            if (thisRow.hasClass('add-row') || (discountEditedManually[thisRow.data('icid')] !== undefined && discountEditedManually[thisRow.data('icid')] == true) || thisRow.hasClass('copied')) {
                currentDiscount = thisRow.find('#prDiscount').val();
                discountEditedManually[thisRow.data('icid')] = false;
            } else {
                if (currentProducts[productID].pPDV != 0) {
                    currentDiscount = currentProducts[productID].pPDV;
                } else {
                    currentDiscount = currentProducts[productID].pPDP;
                }
            }
            if (currentProducts[productID].pPDV != null) {
                thisRow.find('#prDiscount').val(currentDiscount);
                thisRow.find('#prDiscount').addClass('value');
                if (thisRow.find('#prDiscount').hasClass('precentage')) {
                    thisRow.find('#prDiscount').removeClass('precentage');
                }
                thisRow.find('.sign').text('Rs');
            } 

            if (currentProducts[productID].pPDP != null){
                thisRow.find('#prDiscount').val(currentDiscount);
                thisRow.find('#prDiscount').addClass('precentage');
                if (thisRow.find('#prDiscount').hasClass('value')) {
                    thisRow.find('#prDiscount').removeClass('value');
                }
                thisRow.find('.sign').text('%');
            } 
        } else {
            thisRow.find('#prDiscount').prop('readonly', true);
        }

        if (thisRow.find('#prDiscount').hasClass('value')) {
            var tmpPDiscountvalue = isNaN(thisRow.find('#prDiscount').val()) ? 0 : thisRow.find('#prDiscount').val();
            if (tmpPDiscountvalue > 0) {
                var validatedDiscount = validateDiscount(productID, 'val', tmpPDiscountvalue, thisRow.find('#unitPrice').val());
                thisRow.find('#prDiscount').val(validatedDiscount.toFixed(2));
                tmpItemCost -= tmpItemQuentity * validatedDiscount;
            }
        } else {
            var tmpPDiscount = isNaN(thisRow.find('#prDiscount').val()) ? 0 : thisRow.find('#prDiscount').val();
            if (tmpPDiscount > 0) {
                var validatedDiscount = validateDiscount(productID, 'per', tmpPDiscount, tmpItemQuentity);
                thisRow.find('#prDiscount').val(validatedDiscount.toFixed(2));
                tmpItemCost -= (tmpItemTotal * toFloat(validatedDiscount) / toFloat(100));
            }
        }
        
        if (checkedTaxes) {
            currentItemTaxResults = calculateItemCustomTax(tmpItemCost, checkedTaxes);
            currentTaxAmount = (currentItemTaxResults == null) ? null : currentItemTaxResults.tTA;
        } else {
            currentTaxAmount = 0;
            currentItemTaxResults = '';
        }
        var itemCost = toFloat(tmpItemCost) + toFloat(currentTaxAmount);
        productsTotal[productID] = itemCost;
        productsTax[productID] = currentItemTaxResults;
        
        thisRow.find('#addNewTotal').html(accounting.formatMoney(itemCost));
        if (productsTax[productID] != null) {
            if (jQuery.isEmptyObject(productsTax[productID].tL)) {
                thisRow.find('#taxApplied').addClass('glyphicon glyphicon-unchecked');
            } else {
                thisRow.find('#taxApplied').removeClass('glyphicon glyphicon-unchecked');
                thisRow.find('#taxApplied').addClass('glyphicon glyphicon-check');
            }
        }
    }

    function validateDiscount(productID, discountType, currentDiscount, unitPrice) {
        var defaultProductData = currentProducts[productID];
        var newDiscount = 0;
        if (defaultProductData.dPEL == 1) {
            if (discountType == 'val') {
                if (toFloat(defaultProductData.pPDV) !=0 && toFloat(currentDiscount) > toFloat(defaultProductData.pPDV)) {
                    if (promotionProducts[productID] || (priceListItems[productID] && parseFloat(priceListItems[productID].itemPrice) > 0)) {
                        newDiscount = toFloat(currentDiscount);
                    } else {
                        newDiscount = toFloat(defaultProductData.pPDV);
                        p_notification(false, eb.getMessage('ERR_INVO_DISCOUNT_VALUE'));
                    }
                } else if (toFloat(defaultProductData.pPDV) ==0) {
                    if (toFloat(currentDiscount) > toFloat(unitPrice)) {
                        newDiscount = toFloat(unitPrice);
                        p_notification(false, eb.getMessage('ERR_INVO_DISCOUNT_VALUE_UNITE_P'));
                    } else {
                        newDiscount = toFloat(currentDiscount);    
                    }
                } else {
                    newDiscount = toFloat(currentDiscount);
                }

            } else {
                if (toFloat(defaultProductData.pPDP) != 0 && toFloat(currentDiscount) > toFloat(defaultProductData.pPDP)) {
                    if (promotionProducts[productID] || (priceListItems[productID] && parseFloat(priceListItems[productID].itemPrice) > 0)) {
                        newDiscount = toFloat(currentDiscount);
                    } else {
                        newDiscount = toFloat(defaultProductData.pPDP);
                        p_notification(false, eb.getMessage('ERR_INVO_DISC_PERCENTAGE'));
                    }
                } else {
                    newDiscount = toFloat(currentDiscount);
                }
            }
        } else {
            if (priceListItems[productID] && parseFloat(priceListItems[productID].itemPrice) > 0) {
                newDiscount = toFloat(currentDiscount);
            } else {
                p_notification(false, eb.getMessage('ERR_INVO_DISABLE_DISC'));
                newDiscount = 0.00;
            }
        }
        return newDiscount;
    }

    $productTable.on('click', '#selectAll', function() {
        $(this).parents('#addTaxUl').find('input:checkbox.addNewTaxCheck').each(function() {
            if (!this.checked) {
                this.checked = true;
            }
        });
        $(this).parents('tr').find("#unitPrice").trigger(jQuery.Event("focusout"));
    });

    $productTable.on('click', '#deselectAll', function() {
        $(this).parents('#addTaxUl').find('input:checkbox.addNewTaxCheck').each(function() {
            if (this.checked) {
                this.checked = false;
            }
        });
        $(this).parents('tr').find("#unitPrice").trigger(jQuery.Event("focusout"));
    });

    $productTable.on('click', 'ul.dropdown-menu', function(e) {
        e.stopPropagation();
    });

    $productTable.on('change', '.taxChecks.addNewTaxCheck', function() {
        $(this).parents('tr').find("#unitPrice").trigger(jQuery.Event("focusout"));
    });


    $productTable.on('click', 'button.add, button.save', function(e) {
        e.preventDefault();
        var $thisRow = $(this).parents('tr');
        var thisVals = getCurrentProductData($thisRow);

        $("input[name='availableQuantity']", $thisRow).prop('readonly', true);
        if ((thisVals.productCode) === undefined && (thisVals.productName) === undefined) {
            p_notification(false, eb.getMessage('ERR_DELI_SELECT_PROD'));
            $("#item_code", $thisRow).focus();
            return false;
        }

        if ((thisVals.productPrice.trim()) < 0) {
            p_notification(false, eb.getMessage('ERR_INVO_SET_PROD'));
            $("input[name='unitPrice']", $thisRow).focus();
            return false;
        }
        if (thisVals.productType != 2 && (isNaN(parseFloat(thisVals.deliverQuantity.qty)) || parseFloat(thisVals.deliverQuantity.qty) <= 0)) {
            p_notification(false, eb.getMessage('ERR_DELI_ENTER_QUAN'));
            $("input[name='deliverQuantity']", $thisRow).focus();
            return false;
        }

        if (thisVals.productType != 2 && thisVals.stockUpdate && parseFloat(thisVals.deliverQuantity.qty) > parseFloat(thisVals.availableQuantity.qty)) {
            p_notification(false, eb.getMessage('ERR_DPR_QUAN'));
            $("input[name='deliverQuantity']", $thisRow).focus();
            if (checkSubProduct[thisVals.productIncID] === undefined) {
                return false;
            }
        }

        if (thisVals.productDiscount) {
            if (isNaN(parseFloat(thisVals.productDiscount)) || parseFloat(thisVals.productDiscount) < 0) {
                p_notification(false, eb.getMessage('ERR_DELI_VALID_DISCOUNT'));
                $("input[name='discount']", $thisRow).focus();
                return false;
            }
        }

        if (thisVals.productTotal < 0) {
            if (isNaN(parseFloat(thisVals.productTotal)) || parseFloat(thisVals.productTotal) <= 0) {
                p_notification(false, eb.getMessage('ERR_INVO_PROD_TOTAL'));
                $("input[name='unitPrice']", $thisRow).focus();
                return false;
            }
        }

        if ($thisRow.hasClass('subproducts')) {
            var seletedProdutQty = 0;
            if (!$.isEmptyObject(deliverSubProducts[thisVals.productIncID])) {
                $.each(deliverSubProducts[thisVals.productIncID] , function(i, currSubProduct){
                    seletedProdutQty += currSubProduct.qtyByBase
                });
            }
            if ($.isEmptyObject(deliverSubProducts[thisVals.productIncID])) {
                p_notification(false, eb.getMessage('ERR_DELI_SUB_PRODUCT_CHECK', thisVals.productCode));
                $('.edit-modal', $thisRow).focus();
                return false;
            }else if(seletedProdutQty != thisVals.deliverQuantity.qty){
                p_notification(false, eb.getMessage('ERR_DELI_SUB_PRODUCT_CHECK_WITH_PQTY', thisVals.productCode));
                $('.edit-modal', $thisRow).focus();
                return false;
            }
        }

        // if add button is clicked
        if ($(this).hasClass('add')) {
            if(!$thisRow.hasClass('copied')){
                $thisRow.data('icid', rowincrementID);
            }
            var $currentRow = $thisRow;
            var $flag = false;
            if ($currentRow.hasClass('add-row')) {
                $flag = true;
            }

            $currentRow
                    .removeClass('edit-row')
                    .find("#item_code, input[name='deliverQuanity'],input[name='availableQuantity'],input[name='unitPrice'],input[name='discount'], input[name='uomqty']").prop('readonly', true).end()
                    .find('td').find('#addNewTax').attr('disabled', true)
                    .find("button.delete").removeClass('disabled');
            $currentRow.find("input[name='deliverQuanity']").change();
            $currentRow.find("input[name='unitPrice']").change();
            $currentRow.find("input[name='discount']").change();
            $currentRow.find('#deliveryNoteDiscount').trigger('focusout');
            $currentRow.removeClass('add-row');
            discountEditedManually[$thisRow.data('icid')] = true;
            $currentRow.find("#item_code").prop('disabled', true).selectpicker('render');
            if ($flag) {
                var $newRow = $($addRowSample.clone()).removeClass('sample hidden').appendTo($('#add-new-item-row-for-dPr', $productTable));
                hideDuplicate = true;
                setProductTypeahead(hideDuplicate);
                $newRow.find("#item_code").focus();
            }
        } 

        $(this, $currentRow).parent().find('.add').addClass('hidden');
        $(this, $currentRow).parent().find('.save').addClass('hidden');
        $(this, $currentRow).parent().parent().find('.delete').removeClass('disabled');
        // if batch product modal is available for this product
        if ($thisRow.hasClass('subproducts')) {
            $('.edit-modal', $currentRow).parent().addClass('hidden');
            $("td[colspan]", $currentRow).attr('colspan', 2);
        }

        //append product increment ID to product ID
        if ($(this).hasClass('save')) {
            var saveProductID = $thisRow.data('id') + '_' + $thisRow.data('proIncID');
            deliverProducts[saveProductID] = thisVals;
        } else {
            var increID = rowincrementID;
            if($thisRow.data('proIncID')!== undefined){
                increID = $thisRow.data('proIncID')
            }
            thisVals = getCurrentProductData($thisRow);
            deliverProducts[thisVals.productID + '_' + increID] = thisVals;
            if(!$thisRow.hasClass('copied')){
                $thisRow.data('proIncID', increID);
                rowincrementID++;
            }
            $thisRow.removeClass('copied');
        }
        setTotalTaxForDirectReturn();
        setDirectPrTotalCost();
        selectedId = null;
    });

    function setDirectPrTotalCost() {
        var subTotal = 0;
        for (var i in deliverProducts) {
            subTotal += toFloat(deliverProducts[i].productTotal);
        }
        if (subTotal < 0) {
            subTotal = 0;
        }
        
        $('#subtotal').html(accounting.formatMoney(subTotal));
        var finalTotal = subTotal;

        $('#directFinaltotal').html(accounting.formatMoney(finalTotal));
        return  finalTotal;
    }

    function setTotalTaxForDirectReturn() {
        totalTaxList = {};
        if (TAXSTATUS) {
            for (var k in deliverProducts) {
                for (var l in deliverProducts[k].pTax.tL) {
                    if (totalTaxList.hasOwnProperty(l)) {
                        var curTA = totalTaxList[l].tA;
                        var newTA = (toFloat(curTA) + toFloat(deliverProducts[k].pTax.tL[l].tA));
                        totalTaxList[l].tA = newTA.toFixed(2);
                    } else {
                        totalTaxList[l] = {tN: deliverProducts[k].pTax.tL[l].tN, tP: deliverProducts[k].pTax.tL[l].tP, tA: deliverProducts[k].pTax.tL[l].tA, susTax: deliverProducts[k].pTax.tL[l].susTax};
                    }
                }
            }

            var totalTaxHtml = "";
            for (var t in totalTaxList) {
                totalTaxHtml += totalTaxList[t].tN + "(" + totalTaxList[t].tP + "%)&nbsp" + accounting.formatMoney(totalTaxList[t].tA) + "<br>";
            }
            $('#directTotalTaxShow').html(totalTaxHtml);
        }
    }

    function getCurrentProductData($thisRow) {

        var productDesc = ($('.itemDescText', $thisRow).val() != '') ? $('.itemDescText', $thisRow).val() : '';
        var discountType = '';
        if ($("input[name='discount']", $thisRow).hasClass('value')) {
            discountType = 'value';
        } else if ($("input[name='discount']", $thisRow).hasClass('precentage')) {
            discountType = 'precentage';
        }

        var selected_serials = $thisRow.find('.edit-modal').data('selectedSids');

        var pid = rowincrementID;
        if ($thisRow.data('proIncID') !== undefined) {
            pid = $thisRow.data('proIncID');
        }
        var thisVals = {
            productID: $thisRow.data('id'),
            documentTypeID: (typeof $thisRow.data('documenttypeid') != 'undefined') ? $thisRow.data('documenttypeid') : null,
            documentID: (typeof $thisRow.data('prodocuid') != 'undefined') ? $thisRow.data('prodocuid') : null,
            locationID: (typeof $thisRow.data('locationid') != 'undefined') ? $thisRow.data('locationid') : $('#idOfLocation').val(),
            locationProductID: (typeof $thisRow.data('locationproductid') != 'undefined') ? $thisRow.data('locationproductid') : null,
            productIncID: $thisRow.data('id') + '_' + pid,
            productCode: $("#item_code", $thisRow).data('PC'),
            productName: $("#item_code", $thisRow).data('PN'),
            productPrice: $("input[name='unitPrice']", $thisRow).val(),
            productDiscount: $("input[name='discount']", $thisRow).val(),
            productDiscountType: discountType,
            productTotal: productsTotal[$thisRow.data('id')],
            pTax: productsTax[$thisRow.data('id')],
            productType: $("#item_code", $thisRow).data('PT'),
            productDescription: productDesc,
            stockUpdate: $thisRow.data('stockupdate'),
            giftCard: $("#item_code", $thisRow).data('GiftCard'),
            availableQuantity: {
                qty: $("input[name='availableQuantity']", $thisRow).val(),
            },
            deliverQuantity: {
                qty: $("input[name='deliverQuanity']", $thisRow).val(),
            },
            selected_serials: selected_serials,
            selectedUomId: $thisRow.find('#deliverQuanity').siblings('.uom-select').children('button').find('.selected').data('uomID')
        };
        return thisVals;
    }

    $productTable.on('click', 'button.delete', function(e) {
        e.preventDefault();
        if (!$(this).hasClass('disabled')) {
            var $thisRow = $(this).parents('tr');
            var productID = $thisRow.data('id') + '_' + $thisRow.data('proIncID');

            var serial = $thisRow.find('button.edit-modal').data('selectedSids');

            if (serial) {
                for (var i in deliverSubProducts[productID]) {
                    if (serial.indexOf(deliverSubProducts[productID][i].serialID) != -1 || deliverSubProducts[productID][0].batchID != undefined ) {
                        delete deliverSubProducts[productID][i];
                        delete subProductsForCheck[$thisRow.data('id')][i];
                    }
                }
                delete deliverSubProducts[productID];
                delete deliverProducts[productID];
                checkSubProduct[productID] = undefined;
            } else {
                delete deliverProducts[productID];
                checkSubProduct[productID] = undefined;
            }

            setDirectPrTotalCost();
            setTotalTaxForDirectReturn();
            $thisRow.remove();
        }

    });

    $('#batch-save').on('click', function(e) {
        e.preventDefault();
        // validate batch / serial products before closing modal
        if (!batchModalValidate(e)) {
            e.stopPropagation();
            return false;
        } else {
            $('#addDirectReturnProductsModal').modal('hide');
        }
    });

    $('.close').on('click', function() {
        $('#addDirectReturnProductsModal').modal('hide');
    });

    function batchModalValidate(e) {

        var productID = $('#addDirectReturnProductsModal').data('id');
        var incid = $('#addDirectReturnProductsModal').data('icid');
        var proInID = $('#addDirectReturnProductsModal').data('proIncID');
        $('#addDirectReturnProductsModal').data('id', '');
        var $thisParentRow = getAddRow(proInID);
        var thisVals = getCurrentProductData($thisParentRow);
        var $batchTable = $("#addDirectReturnProductsModal .batch-table tbody");
        var qtyTotal = 0;
        var subProducts = [];
        var oldSerials = $('#addDirectReturnProductsModal').data('selectedSIDs');
        var thisSelection = [];
        var deliveryUnitPrice;

        if ($thisParentRow.length > 1) {
            for (var row = 0; $thisParentRow.length > row; row++) {
                if (_.isEqual($('button.edit-modal', $thisParentRow[row]).data('selectedSids'), oldSerials)) {
                    var realParent = $thisParentRow[row];
                }
            }
            $thisParentRow = realParent;
        }


        $("input[name='deliverQuantity'], input[name='deliverQuantityCheck']:checked", $batchTable).each(function() {

            var $thisSubRow = $(this).parents('tr');
            var thisDeliverQuantityByBase = $(this).val();
            if ((thisDeliverQuantityByBase).trim() != "" && isNaN(parseFloat(thisDeliverQuantityByBase))) {
                p_notification(false, eb.getMessage('ERR_INVO_PROD_QUAN'));
                $(this).focus();
                return qtyTotal = false;
            }

            thisDeliverQuantityByBase = (isNaN(parseFloat(thisDeliverQuantityByBase))) ? 0 : parseFloat(thisDeliverQuantityByBase);
            // check if trasnfer qty is greater than available qty
            var thisAvailableQuantityByBase = $("input[name='availableQuantity']", $thisSubRow).val();
            thisAvailableQuantityByBase = (isNaN(parseFloat(thisAvailableQuantityByBase))) ? 0 : parseFloat(thisAvailableQuantityByBase);
            if (thisDeliverQuantityByBase > thisAvailableQuantityByBase) {
                p_notification(false, eb.getMessage('ERR_DPR_QUAN'));
                return qtyTotal = false;
                $(this).focus().select();
            }

             if(thisDeliverQuantityByBase != 0){
                deliveryUnitPrice = $("input[name='btPrice']", $thisSubRow).val();
            }

            var warranty = $thisSubRow.find('#warranty').val();
            if (isNaN(warranty)) {
                p_notification(false, eb.getMessage('ERR_INVO_WARRANTY_PERIOD_SHBE_NUM'));
                $thisSubRow.find('#warranty').focus().select();
                return qtyTotal = false;
            }

            if (thisDeliverQuantityByBase > 0) {
                var thisSubProduct = {};
                if ($(".batchCode", $thisSubRow).data('id')) {
                    thisSubProduct.batchID = $(".batchCode", $thisSubRow).data('id');
                }
                if ($(".serialID", $thisSubRow).data('id')) {
                    thisSubProduct.serialID = $(".serialID", $thisSubRow).data('id');
                }

                thisSubProduct.qtyByBase = thisDeliverQuantityByBase;
                thisSubProduct.incrementID = incid;
                thisSubProduct.warranty = $thisSubRow.find('#warranty').val();


                if ($(this).attr('disabled') != 'disabled') {
                    if(thisSubProduct.serialID!==undefined){
                        thisSelection.push(thisSubProduct.serialID);
                    }
                    subProducts.push($.extend({}, thisSubProduct));
                }
            }else{
                if(deliverSubProducts[thisVals.productIncID] !== undefined){
                    var obj = _.findWhere(deliverSubProducts[thisVals.productIncID], {batchID: $(".batchCode", $thisSubRow).data('id')});
                    if(obj!==undefined){
                        deliverSubProducts[thisVals.productIncID].splice(deliverSubProducts[thisVals.productIncID].indexOf(obj), 1);
                    }

                    if(subProductsForCheck[thisVals.productID] !== undefined){
                        var obj1 = _.findWhere(subProductsForCheck[thisVals.productID], {batchID: $(".batchCode", $thisSubRow).data('id'), incrementID: incid});
                        if(obj1!==undefined){
                            subProductsForCheck[thisVals.productID].splice(subProductsForCheck[thisVals.productID].indexOf(obj1), 1);

                        }
                    }
                }
            }

            //set total product quantity
            qtyTotal = qtyTotal + thisDeliverQuantityByBase;
            for (var old_sr in oldSerials) {
                if (deliverSubProducts[thisVals.productIncID]) {
                    var obj = _.findWhere(deliverSubProducts[thisVals.productIncID], {serialID: oldSerials[old_sr]});
                    deliverSubProducts[thisVals.productIncID].splice(deliverSubProducts[thisVals.productIncID].indexOf(obj), 1);
                }
                if (subProductsForCheck[thisVals.productID] && !(subProductsForCheck[thisVals.productID].serialID == '' || subProductsForCheck[thisVals.productID].serialID === undefined)) {
                    var obj1 = _.findWhere(subProductsForCheck[thisVals.productID], {serialID: oldSerials[old_sr], incrementID: incid });
                    subProductsForCheck[thisVals.productID].splice(subProductsForCheck[thisVals.productID].indexOf(obj1), 1);
                }
            }
        });
        $("input[name='deliverQuanity']", $thisParentRow).val(qtyTotal).change();

        // use to set unit price using batch price
        if(subProducts.length == 1 && subProducts[0].batchID != null && subProducts[0].serialID == null && deliveryUnitPrice != 0){
            $("input[name='unitPrice']", $thisParentRow).val(deliveryUnitPrice).change();
        } else if(subProducts[0].batchID != null && subProducts[0].serialID != null){
            var tempBatchIDs = [];
            var sameBatchFlag = true;
            $.each(subProducts, function(key, value){
                tempBatchIDs[key] = value.batchID;
            });
            var tmpBtID;
            $.each(tempBatchIDs, function(key, value){

                if(key == 0){
                    tmpBtID = value;
                }
                if(tmpBtID != value){
                    sameBatchFlag = false;
                }
            });
            if(sameBatchFlag){
                $("input[name='unitPrice']", $thisParentRow).val(deliveryUnitPrice).change();
            } else {
                $("input[name='unitPrice']", $thisParentRow).val(defaultSellingPriceData[thisVals.productID]).change();
            }

        } else {
            $("input[name='unitPrice']", $thisParentRow).val(defaultSellingPriceData[thisVals.productID]).change();
        }




        if (checkSubProduct[thisVals.productIncID] !== undefined) {
            if (qtyTotal != $("input[name='deliverQuanity']", $thisParentRow).val()) {
                p_notification(false, eb.getMessage('ERR_DELI_TOTAL_SUBQUAN'));
                return qtyTotal = false;
            } else {
                checkSubProduct[thisVals.productIncID] = 1;
            }
        }

        if (qtyTotal === false) {
            return false;
        }

        if (thisVals.stockUpdate == true) {
            if (parseInt(qtyTotal) > parseInt(thisVals.availableQuantity.qty)) {
                p_notification(false, eb.getMessage('ERR_PR_TOTAL_QUAN'));
                return false;
            }
        }

        for (var i in subProducts) {
            if (deliverSubProducts[thisVals.productIncID] === undefined) {
                deliverSubProducts[thisVals.productIncID] = new Array();
            }
            if (subProductsForCheck[thisVals.productID] === undefined) {
                subProductsForCheck[thisVals.productID] = new Array();
            }
             //obj is for get invoiceSubProducts object related to sub  Product Data
            //obj1 is for get subProductsForCheck object related to sub  Product Data
            if (subProducts[i].serialID == '' || subProducts[i].serialID === undefined) {
                var obj = _.findWhere(deliverSubProducts[thisVals.productIncID], {batchID: subProducts[i].batchID, incrementID: subProducts[i].incrementID});
                var obj1 = _.findWhere(subProductsForCheck[thisVals.productID], {batchID: subProducts[i].batchID, incrementID: subProducts[i].incrementID});
            } else {
                var obj = _.findWhere(deliverSubProducts[thisVals.productIncID], {serialID: subProducts[i].serialID, incrementID: subProducts[i].incrementID});
                var obj1 = _.findWhere(subProductsForCheck[thisVals.productID], {serialID: subProducts[i].serialID, incrementID: subProducts[i].incrementID});

            }
            if (obj === undefined) {
                deliverSubProducts[thisVals.productIncID].push(subProducts[i]);
            } else {
                deliverSubProducts[thisVals.productIncID][deliverSubProducts[thisVals.productIncID].indexOf(obj)].qtyByBase = subProducts[i].qtyByBase;
            }
            if (obj1 === undefined) {
                subProductsForCheck[thisVals.productID].push(subProducts[i]);
            } else {
                subProductsForCheck[thisVals.productID][subProductsForCheck[thisVals.productID].indexOf(obj1)].qtyByBase = subProducts[i].qtyByBase;
            }
        }

        SubProductsQty[thisVals.productIncID] = qtyTotal;
        var $transferQ = $("input[name='deliverQuanity']", $thisParentRow).prop('readonly', true);
        if (checkSubProduct[thisVals.productIncID] === undefined) {
            if (!$.isEmptyObject(subProducts[0]) && subProducts[0].serialID) {
                $transferQ.val(thisSelection.length).change();
            } else {
                $transferQ.val(qtyTotal).change();
            }
        }
        $('button.edit-modal', $thisParentRow).data('selectedSids', thisSelection);
        $("#unitPrice", $thisParentRow).trigger(jQuery.Event("focusout"));
        $("#itemUnitPrice", $thisParentRow).trigger(jQuery.Event("focusout"));
        return true;
    }

    $productTable.on('click', 'button.edit-modal', function(e) {
        var productID = $(this).parents('tr').data('id');
        var productincID = productID + "_" + $(this).parents('tr').data('proIncID');
        var rowincID = $(this).parents('tr').data('icid');
        var stockUpdate = $(this).parents('tr').data('stockupdate');
        var productCode = $(this).parents('tr').find('#item_code').val();
        var selectedSIDs = $(this).data('selectedSids');

        $("#batch_data tr:not(.hidden)").remove();
        $('#addDirectReturnProductsModal').data('icid', rowincID);
        $('#addDirectReturnProductsModal').data('proIncID', rowincID);
        addBatchProduct(productID, productCode, 0);
        addSerialProduct(productID, productCode, 0);
        addSerialBatchProduct(productID, productCode, 0);

        //check this product serail or batch
        if (!$.isEmptyObject(currentProducts[productID].serial)) {
            $('#add-product-batch .serial-auto-select').removeClass('hidden');
            $('#numberOfRow').val('');
        } else {
            $('#add-product-batch .serial-auto-select').addClass('hidden');
        }
        $("tr", '#batch_data').each(function() {
            var $thisSubRow = $(this);
            for (var i in subProductsForCheck[productID]) {
                if (subProductsForCheck[productID][i].serialID !== undefined) {
                    if ($(".serialID", $thisSubRow).data('id') == subProductsForCheck[productID][i].serialID) {
                        $("input[name='deliverQuantityCheck']", $thisSubRow).prop('checked', true);
                        $("input[name='warranty']", $thisSubRow).val(subProductsForCheck[productID][i].warranty);

                        if (selectedSIDs && selectedSIDs.indexOf(subProductsForCheck[productID][i].serialID) == -1) {
                            $("input[name='deliverQuantityCheck']", $thisSubRow).attr('disabled', true);
                        }
                    }

                } else if (subProductsForCheck[productID][i].batchID !== undefined  && (rowincID == subProductsForCheck[productID][i].incrementID)) {
                    if ($(".batchCode", $thisSubRow).data('id') == subProductsForCheck[productID][i].batchID) {
                        $(this).find("input[name='deliverQuantity']").val(subProductsForCheck[productID][i].qtyByBase).change();
                    }
                }
            }
        });

        if (!$.isEmptyObject(deliverSubProducts[productincID])) {
            $('#addDirectReturnProductsModal').data('id', $(this).parents('tr').data('id'));
        } 

        $('#addDirectReturnProductsModal').data('selectedSIDs', selectedSIDs);
        $('#addDirectReturnProductsModal').modal('show');
        $('#search_key').val('').keyup();
        $('#addDirectReturnProductsModal').unbind('hide.bs.modal');
    });

    function checkEpireData(ExpireDate){
        var today = new Date();
        var exDate = new Date(ExpireDate);

        var ctoday = Date.UTC(today.getFullYear(), today.getMonth()+1, today.getDate());
        var cexDate = Date.UTC(exDate.getFullYear(), exDate.getMonth()+1, exDate.getDate());

        var flag = true;
        if(cexDate >= ctoday){
            flag = false;
        }
        return flag;
    }
});