var selectedPrID;
var selectedSupplierID;

$(document).ready(function() {

    $('#prSearchBySupplier').selectpicker('hide');
    $('#prSearchSelector').change(function() {
        var selecter = $(this).val();
        if (selecter === 'searchBySupplier') {
            $('#prSearchByPR').selectpicker('hide');
            $('#prSearchBySupplier').selectpicker('show');
        } else {
            $('#prSearchBySupplier').selectpicker('hide');
            $('#prSearchByPR').selectpicker('show');
        }
    });

    loadDropDownFromDatabase('/api/pr/search-purchase-returns-for-dropdown-byPR-code', "", 0, '#prSearchByPR');
    $('#prSearchByPR').change(function() {
        if ($(this).val() > 0 && selectedPrID != $(this).val()) {
            selectedPrID = $(this).val();
            var param = {
            	PRID: $(this).val(),
            	PRCode: $(this).find('option:selected').text(),
            };
            getViewAndLoad('/api/pr/getPurchaseReturnsForView', 'prList', param);
        }
    });

    loadDropDownFromDatabase('/supplierAPI/search-suppliers-for-dropdown', "", 0, '#prSearchBySupplier');
    $('#prSearchBySupplier').change(function() {
        if ($(this).val() > 0 && selectedSupplierID != $(this).val()) {
            selectedSupplierID = $(this).val();
            var param = {
            	supplierID: $(this).val(),
            	supplierCode: $(this).find('option:selected').text(),
            };
            getViewAndLoad('/api/pr/getPurchaseReturnsForView', 'prList', param);
        }
    });

    $('#prSearchClear').on('click', function() {
        getViewAndLoad('/api/pr/getPurchaseReturnsForView', 'prList', {});
        $('#prSearchSelector').selectpicker('val', 'searchByPR');
        $('#prSearchBySupplier').selectpicker('hide');
        $('#prSearchByPR').selectpicker('show');
        $('#prSearchByPR,#prSearchBySupplier').val('').trigger('change');
        selectedPrID = selectedSupplierID = null;
    });

    //responsive issues
    $(window).bind('resize ready', function() {
        ($(window).width() <= 1200) ? $('#filter-button').addClass('margin_top_low') : $('#filter-button').removeClass('margin_top_low');
    });

    $(window).bind('resize ready', function() {
        ($(window).width() <= 480) ? $('#filter-button').addClass('col-xs-12') : $('#filter-button').removeClass('col-xs-12');
    });


     $('#prList').on('click', '.pr_related_docs', function() {
        setDataToHistoryModal($(this).attr('data-pr-related-id'));
        $('#addDocHistoryModal').modal('show');
    });

     $('#prList').on('click', '.pr_related_in_grns', function() {
        setDataToInGrnModal($(this).attr('data-pr-related-id'));
        $('#retunItemInGrnViewModal').modal('show');
    });

    $('#prList').on('click', '.doc_attachments', function() {
        setDataToAttachmentViewModal($(this).attr('data-pr-related-id'));
        $('#viewAttachmentModal').modal('show');
    });


    $('#addDocHistoryModal #doc-history-table').on('click', '.document_view', function() {
        var documentId = $(this).parents('tr').attr('data-docid');
        var documentType = $(this).parents('tr').attr('data-doctype');
        $('#DocumentViewModal').modal('show');
        getPrintPreviewByDocumentTypeAndDocumentID(documentId, documentType);
    });

    $('#retunItemInGrnViewModal #grn-in-table').on('click', '.document_view', function() {
        var documentId = $(this).parents('tr').attr('data-docid');
        var documentType = $(this).parents('tr').attr('data-doctype');
        $('#DocumentViewModal').modal('show');
        getPrintPreviewByDocumentTypeAndDocumentID(documentId, documentType);
    });
});

function setDataToAttachmentViewModal(documentID) {
    $('#doc-attach-table tbody tr').remove();
    $('#doc-attach-table tfoot div').remove();
    $('#doc-attach-table tbody div').remove();

    eb.ajax({
        type: 'POST',
        url: BASE_URL + '/get-document-related-attachement',
        data: {
            documentID: documentID,
            documentTypeID: 11
        },
        success: function(respond) {
            if (respond.status == true) {
                $('#doc-attach-table thead tr').removeClass('hidden');
                $.each(respond.data, function(index, value) {
                    tableBody = "<tr><td><a href="+value.link+">"+value.docName+"</a></td></tr>";
                    $('#doc-attach-table tbody').append(tableBody);
                });
            } else {
                $('#doc-attach-table thead tr').addClass('hidden');
                var noDataFooter = "<div class= ' col-lg-6 text-right'><label><b>No Data</b></label>";
                $('#doc-attach-table tbody').append(noDataFooter);
            }
        }
    });
}

function setDataToHistoryModal(purchaseReturnID) {
    $('#doc-history-table tbody tr').remove();
    $('#doc-history-table tfoot div').remove();
    $('#doc-history-table tbody div').remove();

    eb.ajax({
        type: 'POST',
        url: BASE_URL + '/api/pr/getAllRelatedDocumentDetailsByPrId',
        data: {purchaseReturnID: purchaseReturnID},
        success: function(respond) {
            if (respond.status == true) {
                $('#doc-history-table thead tr').removeClass('hidden');
                $.each(respond.data, function(index, value) {
                    if (value != null) {
                        $.each(value, function(index2, value2) {
                            tableBody = "<tr data-docId=" + value2['documentID'] + " data-doctype=" + value2['type'] + "><td class='' >" + value2['type'] + "</td><td>" + value2['code'] + "</td><td>" + value2['amount'] + "</td><td>" + value2['issuedDate'] + "</td><td class='text-center'><span class='glyphicon glyphicon-eye-open openlist link_color document_view ' style='cursor:pointer'></span></td></tr>";
                            $('#doc-history-table tbody').append(tableBody);
                        });
                    }
                });
                var footer = " <div class='col-lg-12 col-md-12 col-sm-12 text-right'><button type='button' class='btn btn-primary col-lg-12 col-md-12 col-sm-12' data-dismiss='modal' aril-hidden='true'>Back</button></div>";
                $('#doc-history-table tfoot').append(footer);
            } else {
                $('#doc-history-table thead tr').addClass('hidden');
                var noDataFooter = "<div class= ' col-lg-9 text-right'><label><b>No Data</b></label>";
                $('#doc-history-table tbody').append(noDataFooter);
            }
        }
    });
}


function setDataToInGrnModal(purchaseReturnID) {
    $('#grn-in-table tbody tr').remove();
    $('#grn-in-table tfoot div').remove();
    $('#grn-in-table tbody div').remove();

    eb.ajax({
        type: 'POST',
        url: BASE_URL + '/api/pr/getAllRelatedDocumentInDetailsForPrByPrId',
        data: {purchaseReturnID: purchaseReturnID},
        success: function(respond) {
            if (respond.status == true) {
                $('#grn-in-table thead tr').removeClass('hidden');
                $.each(respond.data, function(index, value) {
                    if (value != null) {
                        $.each(value, function(index2, value2) {
                            tableBody = "<tr data-docId=" + value2['documentID'] + " data-doctype=" + value2['type'] + "><td class='' >" + value2['type'] + "</td><td>" + value2['code'] + "</td><td>" + value2['amount'] + "</td><td>" + value2['issuedDate'] + "</td><td class='text-center'><span class='glyphicon glyphicon-eye-open openlist link_color document_view ' style='cursor:pointer'></span></td></tr>";
                            $('#grn-in-table tbody').append(tableBody);
                        });
                    }
                });
                var footer = " <div class='col-lg-12 col-md-12 col-sm-12 text-right'><button type='button' class='btn btn-primary col-lg-12 col-md-12 col-sm-12' data-dismiss='modal' aril-hidden='true'>Back</button></div>";
                $('#grn-in-table tfoot').append(footer);
            } else {
                $('#grn-in-table thead tr').addClass('hidden');
                var noDataFooter = "<div class= ' col-lg-12 text-center'><label><b>No Data</b></label>";
                $('#grn-in-table tbody').append(noDataFooter);
            }
        }
    });
}

function getPrintPreviewByDocumentTypeAndDocumentID(documentID, documentType) {
    var $iframe = $('#related-document-view');
    $iframe.ready(function() {
        $iframe.contents().find("body div").remove();
    });
    var URL;
    if (documentType == 'PurchaseOrder') {
        URL = BASE_URL + '/po/document/' + documentID;
    } else if (documentType == 'Grn') {
        URL = BASE_URL + '/grn/document/' + documentID;
    }
    else if (documentType == 'PurchaseInvoice') {
        URL = BASE_URL + '/pi/document/' + documentID;
    }
    else if (documentType == 'PurchaseReturn') {
        URL = BASE_URL + '/pr/document/' + documentID;
    }
    else if (documentType == 'DebitNote') {
        URL = BASE_URL + '/debit-note/document/' + documentID;
    }
    else if (documentType == 'SupplierPayment') {
        URL = BASE_URL + '/supplierPayments/document/' + documentID;
    }
    else if (documentType == 'DebitNotePayment') {
        URL = BASE_URL + '/debit-note-payments/document/' + documentID;
    }

    eb.ajax({
        type: 'POST',
        url: URL,
        success: function(respond) {
            var division = "<div></div>";
            $iframe.ready(function() {
                $iframe.contents().find("body").append(division);
            });
            $iframe.ready(function() {
                $iframe.contents().find("body div").append(respond);
            });
        }
    });

}