/**
 * @author SANDUN <sandun@thinkcube.com>
 * This file contain what wizard related functions
 */
var currentStep = 0;
var wizard = true;
$(document).ready(function() {
    addDisableClass();
    function addDisableClass() {
        var url = BASE_URL + '/api/settings/wizard/checkCompany';
        eb.ajax({
            type: 'POST',
            url: url,
            data: {},
            success: function(respond) {
                currentStep = respond.data ? respond.data : 0;
                if (currentStep == 0) {
                    $("li:nth-child(2),li:nth-child(3),li:nth-child(4)").addClass('disable');
                }else if (currentStep == 1) {
                    $("li:nth-child(3),li:nth-child(4)").addClass('disable');
                } else if (currentStep == 2) {
                    $("li:nth-child(4)").addClass('disable');
                }

                $(".side_bar ul li.disable a").click(function(e) {
                    e.preventDefault();
                    return false;
                });
            }
        });
    }
//company related
    $('#create-company-button').hide();
    $('#profile_pic').hide();
    //location related
    $('.no_top_margin').hide();
    $('.location_create').hide();
    $('.location-header').remove();
    $('.custom_hr').remove();
    $('.location_create').remove();
    $('#location-header').remove();
    $('#location-header div').removeClass('margin_top');
    $('#location-list').removeClass('margin_top');
    $('#list-locations').hide();
    $('.paginationHolder').hide();
    $('#wizardLocationList td:nth-child(8),th:nth-child(8),td:nth-child(7),th:nth-child(7),td:nth-child(6),th:nth-child(6),td:nth-child(5),th:nth-child(5)').hide();
    //uom related
    $('.uom-search-head').remove();
    //category related
    $('.category').remove();
    $('#add-locaion').attr("id", "wizardLocationForm");
    $('#create-company-button').attr("id", "wizardSubmit");
    $('form#create-company-form').on('submit', function(e) {
        e.preventDefault();
        companySubmit(e);
    });
    $('.footer').on('click', '.next_btn , .skip_btn', function(e) {
        e.preventDefault();
        var formID = $('form').attr('id') ? $('form').attr('id') : '';
        var divID = $('.finish-set-up').attr('id') ? $('.finish-set-up').attr('id') : '';
        var accountsID = $('.accounts').attr('id') ? $('.accounts').attr('id') : '';
        if (formID == 'create-company-form') {
            companySubmit(e);
        } else if (divID == 'finishSetup') {
            updateCompanyWizardState(4, function() {
                window.location.href = BASE_URL + "/";
            });
        }
    });

    function updateUseAccoutingFlag(){
        var url = BASE_URL + '/api/settings/wizard/updateUseAccountingFalg';
        var flag = $("input[name='use_accounting']:checked").val();
        var costingMethod = $("input[name='costingMethod']:checked").val();
        eb.ajax({
            type: 'POST',
            url: url,
            data: {flag: flag,costingMethod:costingMethod},
            success: function(respond) {
                p_notification(respond.status, respond.msg);
                if (respond.status && flag == 1) {
                    updateGlAccFromWizard(accountsCallBack);                 
                }else{
                    updateCompanyWizardState(2, function() {
                        window.location.href = BASE_URL + "/wizard/tax";
                    });
                }
            }
        });
    }

    function accountsCallBack(){
        updateCompanyWizardState(2, function() {
            window.location.href = BASE_URL + "/wizard/tax";
        });
    }

    function updateCompanyWizardState(state, callback) {
        var url = BASE_URL + '/api/settings/wizard/updateWizardState';
        var status = false;
        eb.ajax({
            type: 'POST',
            url: url,
            data: {state: state},
            success: function(respond) {
                if (respond.status) {
                    status = true;
                    callback();
                }
            }
        });
        return status;
    }

    $('form#wizardLocationForm').on('submit', function(e) {
        e.preventDefault();
        var locationID = $('#locationID').val();
        var locationCode = $('#locationCode').val();
        var locationName = $('#locationName').val();
        var locationEmail = $('#locationEmail').val();
        var locationTelephone = $('#locationTelephone').val();
        var locationFax = $('#locationFax').val();
        var locationAddressLine1 = $('#locationAddressLine1').val();
        var locationAddressLine2 = $('#locationAddressLine2').val();
        var locationAddressLine3 = $('#locationAddressLine3').val();
        var locationAddressLine4 = $('#locationAddressLine4').val();
        var locationStatus = $('#locationStatus').val();
        var param = {
            locationID: locationID,
            locationCode: locationCode,
            locationName: locationName,
            locationEmail: locationEmail,
            locationTelephone: locationTelephone,
            locationFax: locationFax,
            locationAddressLine1: locationAddressLine1,
            locationAddressLine2: locationAddressLine2,
            locationAddressLine3: locationAddressLine3,
            locationAddressLine4: locationAddressLine4,
            locationStatus: locationStatus
        };
        if (validate_location_form() && !check_location(locationID, locationCode)) {
            if (locationID === "") {
                var url = BASE_URL + '/settings/location/addLocationWizard';
                eb.ajax({
                    type: 'POST',
                    url: url,
                    data: param,
                    success: function(result)
                    {
                        if (result.status == true) {
                            window.location.reload();
                        }
                    }
                });
            } else {
                var url = BASE_URL + '/settings/location/updateLocation';
                eb.ajax({
                    type: 'POST',
                    url: url,
                    data: param,
                    success: function(result)
                    {
                        p_notification(result.status, result.msg);
                    }
                });
            }
        }

    });
    function companySubmit(e) {
        var url2 = BASE_URL + '/companyAPI/index';
        var url4 = BASE_URL + '/companyAPI/preview';
        var param = $('#create-company-form').serialize();
        var valid = new Array(
                    $('#companyName').val(),
                    $('#companyAddress').val(),
                    $('#postalCode').val(),
                    $('#telephoneNumber').val(),
                    $('#email').val(),
                    $('#brNumber').val(),
                    $('#website').val(),
                    $('#country').val()
                );        
        if (validateCompanyForm(valid)) {
            var submitForm = eb.post(url2, param);
            submitForm.done(function(data) { // for update company details
                console.log(data);
                if (data.status == true) {
                    input = document.getElementById('image_file');
                    var file;
                    file = input.files[0];
                    if (file) {
                        formdata = false;
                        if (window.FormData) {
                            formdata = new FormData();
                        }
                        e.preventDefault();
                        if ($('#w').val() == '' || $('#w').val() == null) {
                            $('#w').val('200');
                        }
                        if ($('#h').val() == '' || $('#h').val() == null) {
                            $('#h').val('200');
                        }
                        if ($('#x1').val() == '' || $('#x1').val() == null) {
                            $('#x1').val('0');
                        }
                        if ($('#y1').val() == '' || $('#y1').val() == null) {
                            $('#y1').val('0');
                        }
                        if ($('#x2').val() == '' || $('#x2').val() == null) {
                            $('#x2').val('200');
                        }
                        if ($('#y2').val() == '' || $('#y2').val() == null) {
                            $('#y2').val('200');
                        }
                        formdata.append("images", file);
                        formdata.append("w", $('#w').val());
                        formdata.append("h", $('#h').val());
                        formdata.append("x1", $('#x1').val());
                        formdata.append("y1", $('#y1').val());
                        formdata.append("x2", $('#x2').val());
                        formdata.append("y2", $('#y2').val());
                        formdata.append("companyID", data.data);
                        $('#w').val('');
                        $('#h').val('');
                        $('#x1').val('');
                        $('#y1').val('');
                        $('#x2').val('');
                        $('#y2').val('');
                        
                        eb.ajax({ // for upload company image
                            url: url4,
                            type: 'POST',
                            processData: false,
                            contentType: false,
                            data: formdata,
                            success: function(res) {
                                if (res) {
                                    updateCompanyWizardState(3, function() { // for finsh setup
                                        window.location.href = BASE_URL + "/wizard/finish-setup";
                                    });
                                }
                            }
                        });
                    } else {
                        updateCompanyWizardState(3, function() { // for finsh setup
                            window.location.href = BASE_URL + "/wizard/finish-setup";
                        });
                    }
                } else if (data.status == false) {
                    p_notification(false, 'You can not go next step without saving company details.');
                }
            });
        }
    }

    function validateCompanyForm(valid) {
        var cn = valid[0];
        var ca = valid[1];
        var cp = valid[2];
        var ct = valid[3];
        var ce = valid[4];
        var cbr = valid[5];
        var country = valid[7];
        var atpos = ce.indexOf("@");
        var dotpos = ce.lastIndexOf(".");
        var regex = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
        if (cn == null || cn == "") {
            p_notification(false, 'Please enter the Company Name.');
            return false;
        } else if (ca == null || ca == "") {
            p_notification(false, 'Please enter the Company Address.');
            return false;
        } else if (isNaN(cp)) {
            p_notification(false, 'Postal code must be numeric.');
            return false;
        } else if (cp.length > 10) {
            p_notification(false, 'Postal code cannot be more than 12 characters.');
            return false;
        } else if (ct == null || ct == "") {
            p_notification(false, 'Please enter the Company Telephone Number.');
            return false;
        } else if (isNaN(ct)) {
            p_notification(false, 'Telephone Number should be numeric.');
            return false;
        } else if (ct.length < 10 || ct.length > 13) {
            p_notification(false, 'Telephone number should be between 10 to 13 digits.');
            return false;
        } else if (ce == null || ce == "") {
            p_notification(false, 'Please enter a email address.');
            return false;
        } else if (ce != "" && !regex.test(ce)) {
            p_notification(false, 'Please enter a valid email address.');
            return false;
        } else if (!country) {
            p_notification(false, 'Please Select a country.');
            return false;
        } else {
            return true;
        }
    }

//    $('#image-submit').on('click', function(e) {
//        e.preventDefault();
//        input = document.getElementById('image_file');
//        var file = input.files[0];
//        if (file) {
//            formdata = false;
//            if (window.FormData) {
//                formdata = new FormData();
//            }
//            e.preventDefault();
//            formdata = eb.getImageDimension("images", file, formdata, '200', '200', '0', '0', '200', '200');
//            eb.ajax({
//                url: BASE_URL + '/image-preview',
//                type: 'POST',
//                processData: false,
//                contentType: false,
//                data: formdata,
//                success: function(data) {
//                    //$('#thumbs').html('<img src="' + data + '" />');
//                    $('#thumbs').attr('src', data);
//                    return data;
//                }
//            });
//            $('#imageUploaderModal').modal('hide');
//        }
//    });
//mobile menu btn
    $('.mobile_menu').on('click', function() {
        $('.side_bar_wrapper').slideToggle();
    });
    var side_bar_wrap = $('.side_bar_wrapper');
    $(window).on('resize ready', function() {
        if ($(window).width() > 768) {
            if (!$(side_bar_wrap).is(':visible')) {
                $(side_bar_wrap).show();
            }
        } else {
            $(side_bar_wrap).hide();
        }

//next button mobile view
        if ($(window).width() < 480) {
            $('.next_btn, .skip_btn').css({
                width: '100%',
                margin: 0
            });
            $('.skip_btn').css('margin-bottom', '10px');
        } else {
            $('.next_btn, .skip_btn').css({
                width: 'auto'
            });
            $('.skip_btn').css('margin-bottom', '0');
        }
    });

    $("input[name='use_accounting']").on('click',function(){
        if($(this).val() == 1){
            $('.showGlAccounts').removeClass('hidden');
        }else {
            $('.showGlAccounts').addClass('hidden');
        }
    });
});
