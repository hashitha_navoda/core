function goBack() {
    window.history.go(-1);
}

$(document).ready(function(){

    // Load reference view
    $("#doc_ref").on('click', '.ref-doc', function(){
        var link = BASE_URL;
        var docTypUrl = [];
        docTypUrl["SalesInvoice"] = "/invoice";
        docTypUrl["SalesQuotation"] = "/quotation";
        docTypUrl["SalesOrder"] = "/salesOrders";
        docTypUrl["DeliveryNote"] = "/delivery-note";
        docTypUrl["SalesReturns"] = "/return";
        docTypUrl["CreditNote"] = "/credit-note";
        docTypUrl["CustomerPayment"] = "/customerPayments";
        docTypUrl["POSPrintout"] = "/invoice";
        docTypUrl["PurchaseOrder"] = "/po";
        docTypUrl["GoodsReceivedNote"] = "/grn";
        docTypUrl["PurchaseReturn"] = "/pr";
        docTypUrl["PaymentVoucher(PurchaseInvoice)"] = "/pi";
        docTypUrl["DebitNote"] = "/debit-note";
        docTypUrl["SupplierPayment"] = "/supplierPayments";
        docTypUrl["InventoryTransfer"] = "/transfer";
        docTypUrl["InventoryAdjustment"] = "/inventory-adjustment";
        docTypUrl["POSPrintout"] = "/invoice";
        docTypUrl["CreditNotePayment"] = "/credit-note-payments";
        docTypUrl["DebitNotePayment"] = "/debit-note-payments";
        docTypUrl["Job"] = "/job";
        docTypUrl["ExpensePaymentVoucher"] = "/expense-purchase-invoice";
        docTypUrl["DispatchNote"] = "/dispatch-note";

        var docType = $(this).data('doctype').replace(/\s/g,'');

        if(docTypUrl[docType]){
            link = BASE_URL + docTypUrl[docType] + "/document/" + $(this).data('docid');
        }else {
            //
        }

        $('#doc_title').text($(this).data('doctype'));
        $('#doc_view').attr('src', link);
        $('#doc_ref_view').modal('show');
    });

    // Close reference document view modal
    $(".close_doc_ref_view").on('click', function(){
        $('#doc_ref_view').modal('hide');
    });

    $(document).on('show.bs.modal', '.modal', function () {
        var zIndex = 1040 + (10 * $('.modal:visible').length);
        $(this).css('z-index', zIndex);
    });
});
