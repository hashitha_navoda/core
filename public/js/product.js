/**
 * @author ASHAN <ashan@thinkcube.com>
 * This file contains form validation
 */
var proupdatemass = new Array();
var productID = '';
var imageID = 0;
var suplliers = new Array();
var loc = new Array();
var flag = 0;
var TaxDiscountflag = 0;
var ImageAndControllers = 0;
var EDIT_MODE = false;
var editCategoryID = '';
var itemAttrValue = [];
var itemAttrTmpryValue = [];
var itemAttrValueEditedFlag = false;
var inventoryTypeChangeFlag = false;
var uomTriggedFlag = false;
var conversionVal = 1;
var currentPath = window.location.pathname;

$(document).ready(function() {

	loadDropDownFromDatabase('/api/discount-schemes/search-discount-scheme-for-dropdown', "", "", '#discountSchemeID');
    loadDropDownFromDatabase('/accounts-api/search-active-accounts-for-dropdown', "", "", '#productSalesAccountID');
	loadDropDownFromDatabase('/accounts-api/search-active-accounts-for-dropdown', "", "", '#productInventoryAccountID');
	loadDropDownFromDatabase('/accounts-api/search-active-accounts-for-dropdown', "", "", '#productCOGSAccountID');
	loadDropDownFromDatabase('/accounts-api/search-active-accounts-for-dropdown', "", "", '#productAdjusmentAccountID');
    loadDropDownFromDatabase('/accounts-api/search-active-accounts-for-dropdown', "", "", '#productDeperciationAccountID');
    var itemBarcodeTbody = $('#itemBarcodeTbody');
    var itemBarCodesList = [];

    var expiryAlartArray = new Array();

    $('#productHandelingFixedAssets').on('click', function(event) {
        if ($(this).is(':checked') && $('#serialProduct').is(':checked')) {
            $('#depreciation_ac_div').removeClass('hidden');
        } else {
            $('#depreciation_ac_div').addClass('hidden');
        }
    });

    $('#serialProduct').on('click', function(event) {
        if ($(this).is(':checked') && $('#productHandelingFixedAssets').is(':checked')) {
            $('#depreciation_ac_div').removeClass('hidden');
        } else {
            $('#depreciation_ac_div').addClass('hidden');
        }
    });

    updateStatusOfDefalutQuantity();
    $('#productTypeID').on('change', function() {
        updateStatusOfDefalutQuantity();
        if ($('#startingQuantity').length) {
            if ($(this).val() == 1) {
                $('#startingQuantity').attr('disabled', false);
            } else {
                $('#startingQuantity').attr('disabled', true);
            }
        }
        if ($(this).val() == 2) {
            $('.add-uom').attr('disabled', true);
            var allUom = getAllUoms();
            if (Object.keys(allUom).length > 1) {
                p_notification(false, eb.getMessage('ERROR_UOM_SET'));
                $(this).val(1);
                if ($('.add-uom').hasClass('edit_mode')) {
                    $('.add-uom').attr('disabled', true);
                } else {
                    $('.add-uom').attr('disabled', false);
                }
            }
        } else {
            $('.add-uom').attr('disabled', false);
        }
    });
    if ($("input[name='editmode']").length) {
        EDIT_MODE = true;
        editCategoryID = $("[name='categoryParentID']").val();

    }

    if (EDIT_MODE) {
        if ($('#serialProduct').is(':checked') && $('#productHandelingFixedAssets').is(':checked')) {
            $('#depreciation_ac_div').removeClass('hidden');
        } else {
            $('#depreciation_ac_div').addClass('hidden');
        }
    }

    
    //get existing expire alert details
    if (EDIT_MODE) {
        eb.ajax({
            url: '/productAPI/getExpireAlertDetals',
            method: 'post',
            data: {productID: $("[name='productID']").val()},
            dataType: 'json',
            success: function(data) {
                if (data.status == true) {
                    expiryAlartArray = data.data;
                }
            }
        });
                    
    }

    //get existing expire alert details
    if (EDIT_MODE) {
        eb.ajax({
            url: '/productAPI/getBarCodes',
            method: 'post',
            data: {productID: $("[name='productID']").val()},
            dataType: 'json',
            success: function(data) {
                itemBarCodesList = (data['data']) ? data['data'] :[];
                refreshItemBarcodeList();
            }
        });
                    
    }

    if (EDIT_MODE) {
        var discountSchemeID = $('#discountSchemeID').data('id');
        if(discountSchemeID!=''){
            var discountSchemeName = $('#discountSchemeID').data('value');
            $('#discountSchemeID').append("<option value='"+discountSchemeID+"'>"+discountSchemeName+"</option>")
            $('#discountSchemeID').val(discountSchemeID).selectpicker('refresh');
        }

        $('#item_type_change_note').removeClass('hide');
    } else {
        $('#item_type_change_note').addClass('hide');
    }
    var productSalesAccountID = $('#productSalesAccountID').data('id');
    if(productSalesAccountID!=''){
    	var productSalesAccountName = $('#productSalesAccountID').data('value');
    	$('#productSalesAccountID').append("<option value='"+productSalesAccountID+"'>"+productSalesAccountName+"</option>")
    	$('#productSalesAccountID').val(productSalesAccountID).selectpicker('refresh');
    }

    var productInventoryAccountID = $('#productInventoryAccountID').data('id');
    if(productInventoryAccountID!=''){
    	var productInventoryAccountName = $('#productInventoryAccountID').data('value');
    	$('#productInventoryAccountID').append("<option value='"+productInventoryAccountID+"'>"+productInventoryAccountName+"</option>")
    	$('#productInventoryAccountID').val(productInventoryAccountID).selectpicker('refresh');
    }

    var productCOGSAccountID = $('#productCOGSAccountID').data('id');
    if(productCOGSAccountID!=''){
    	var productCOGSAccountName = $('#productCOGSAccountID').data('value');
    	$('#productCOGSAccountID').append("<option value='"+productCOGSAccountID+"'>"+productCOGSAccountName+"</option>")
    	$('#productCOGSAccountID').val(productCOGSAccountID).selectpicker('refresh');
    }

    var productAdjusmentAccountID = $('#productAdjusmentAccountID').data('id');
    if(productAdjusmentAccountID!=''){
    	var productAdjusmentAccountName = $('#productAdjusmentAccountID').data('value');
    	$('#productAdjusmentAccountID').append("<option value='"+productAdjusmentAccountID+"'>"+productAdjusmentAccountName+"</option>")
    	$('#productAdjusmentAccountID').val(productAdjusmentAccountID).selectpicker('refresh');
    }

    var productDeperciationAccountID = $('#productDeperciationAccountID').data('id');
    if(productDeperciationAccountID!=''){
        var productDeperciationAccountName = $('#productDeperciationAccountID').data('value');
        $('#productDeperciationAccountID').append("<option value='"+productDeperciationAccountID+"'>"+productDeperciationAccountName+"</option>")
        $('#productDeperciationAccountID').val(productDeperciationAccountID).selectpicker('refresh');
    }

    var $createProductForm = $("#create-product-form");
    var $createCategoryForm = $("#create-category-form");
    $("[name='productGlobalProduct']").on('change', function() {
        if ($("#locationItem").prop('checked')) {
            $('#itemLocations').show();
        } else {
            $('#itemLocations').hide();
        }
    }).trigger('change');

    $("#showTaxDiscount").on('click', function() {
        $("#showTaxDiscount").toggleClass('expanded');
        $("#taxDiscount").slideToggle();
    });

    $("#showImageAndController").on('click', function() {
        $("#showImageAndController").toggleClass('expanded');
        $("#imageAndController").slideToggle();
    });

     $("#showItemDefaultAccount").on('click', function() {
        $("#showItemDefaultAccount").toggleClass('expanded');
        $("#defaultAccounts").slideToggle();
    });

    $("#productTaxEligible").on('change', function() {
        if (this.checked) {
            $("#showtax").slideDown();
        } else {
            $("#showtax").slideUp();
            $(".taxes").attr('checked', false);
        }
    }).trigger('change');
    $("#productDiscountEligible").on('change', function() {
        if (this.checked) {
            $("#showdiscount").slideDown();
        } else {
            $("#showdiscount").slideUp();
            $("#productDiscount").val('');
            $("#discountSchemeEligible").prop("checked", false).trigger('change');
        }
    }).trigger('change');

    $("#discountSchemeEligible").on('change', function() {
        if (this.checked) {
            $("#showdiscountScheme").slideDown();
            $("#showdiscountManual").slideUp();
        } else {
            $("#showdiscountScheme").slideUp();
            $("#showdiscountManual").slideDown();
            $("#productDiscount").val('');
        }
    }).trigger('change');

    $("#productPurchaseDiscountEligible").on('change', function() {
        if (this.checked) {
            $("#showPurchaseDiscount").slideDown();
        } else {
            $("#showPurchaseDiscount").slideUp();

        }
    }).trigger('change');

    $("#autoGenarate").on('click', function() {
        if (this.checked) {
            $("#Prefix").fadeIn();
        } else {
            $("#Prefix").fadeOut();
        }
    });
    $createProductForm.on('reset', function(e) {
        setTimeout(function() {
            $createProductForm.find("select, input[type='radio'], input[type='checkbox']").trigger('change');
        }, 1);
    });
    $createProductForm.on('submit', function(e) {
        e.preventDefault();
        if ($('.add-uom').hasClass('edit_mode') && uomAddCount > 1) {
            var msg = eb.getMessage('UOM_EDIT_VARIFIC');
            bootbox.confirm(msg, function(result) {
                if (result == true) {
                    saveAndUpdate();
                }
            });
        } else {
            saveAndUpdate();
        }
        function saveAndUpdate() {
            e.preventDefault();
            // get selected locations
            var locations = $("input.locationCheck:checked").map(function() {
                return $(this).data('locationid');
            }).get();
            // get selected taxes
            var taxes = $("input.taxes:checked").map(function() {
                return $(this).data('taxid');
            }).get();
            // get selected handeling types
            var handeling = $("input.handeling:checked").map(function() {
                return this.id;
            }).get();
            var flag = '';
            if ($('#startingQuantity').length) {
                flag = 'invoice';
            } else {
                flag = 'product';
            }
            var productDefaultOpeningQuantity = "";
            if ($("[name='productDefaultOpeningQuantity']", $createProductForm).length) {
                productDefaultOpeningQuantity = $("[name='productDefaultOpeningQuantity']", $createProductForm).val();
            }
            var formData = {
                productCode: $("[name='productCode']", $createProductForm).val(),
                productBarcode: $("[name='productBarcode']", $createProductForm).val(),
                productName: $("[name='productName']", $createProductForm).val(),
                handeling: handeling,
                productDescription: $("[name='productDescription']", $createProductForm).val(),
                categoryID: $("[name='categoryParentID']").val(),
                productTypeID: $("[name='productTypeID']", $createProductForm).val(),
                productGlobalProduct: $("[name='productGlobalProduct']:checked", $createProductForm).val(),
                locations: locations,
                productTaxEligible: $("[name='productTaxEligible']", $createProductForm).is(':checked'),
                taxes: taxes,
                discountEligible: $("[name='productDiscountEligible']", $createProductForm).is(':checked'),
                useDiscountSchema: $("[name='discountSchemeEligible']", $createProductForm).is(':checked'),
                productDiscountPercentage: $("[name='productDiscountPercentage']", $createProductForm).val(),
                productDiscountValue: $("[name='productDiscountValue']", $createProductForm).val(),
                purchaseDiscountEligible: $("[name='productPurchaseDiscountEligible']", $createProductForm).is(':checked'),
                productPurchaseDiscountPercentage: $("[name='productPurchaseDiscountPercentage']", $createProductForm).val(),
                productPurchaseDiscountValue: $("[name='productPurchaseDiscountValue']", $createProductForm).val(),
                productImageURL: imageID,
                batchProduct: $("[name='batchProduct']", $createProductForm).is(':checked'),
                serialProduct: $("[name='serialProduct']", $createProductForm).is(':checked'),
                hsCode: $("[name='hsCode']").val(),
                customDutyValue: $("[name='customDutyValue']", $createProductForm).val(),
                customDutyPercentage: $("[name='customDutyPercentage']", $createProductForm).val(),
                productState: $("[name='productState']", $createProductForm).val(),
                productDefaultSellingPrice: (uomTriggedFlag) ? $("[name='productDefaultSellingPrice']", $createProductForm).val() : ($("[name='productDefaultSellingPrice']", $createProductForm).val() / getAllUoms()[getUomsDispaly()].uomConversion),
                productDefaultMinimumInventoryLevel: $("[name='productDefaultMinimumInventoryLevel']", $createProductForm).val().replace(/,/g, ''),
                productDefaultMaximunInventoryLevel: $("[name='productDefaultMaximunInventoryLevel']", $createProductForm).val().replace(/,/g, ''),
                productDefaultReorderLevel: $("[name='productDefaultReorderLevel']", $createProductForm).val().replace(/,/g, ''),
                flag: flag,
                productDefaultOpeningQuantity: productDefaultOpeningQuantity,
                uomDisplay: getUomsDispaly(),
                productDefaultPurchasePrice: (uomTriggedFlag) ? $("[name='productDefaultPurchasePrice']", $createProductForm).val() : ($("[name='productDefaultPurchasePrice']", $createProductForm).val() / getAllUoms()[getUomsDispaly()].uomConversion),
                rackID: $("[name='rackId']", $createProductForm).val(),
                itemDetail: $("[name='itemDetail']", $createProductForm).val(),
                productSalesAccountID: $("#productSalesAccountID", $createProductForm).val(),
                productInventoryAccountID: $("#productInventoryAccountID", $createProductForm).val(),
                productCOGSAccountID: $("#productCOGSAccountID", $createProductForm).val(),
                productAdjusmentAccountID: $("#productAdjusmentAccountID", $createProductForm).val(),
                productDeperciationAccountID: $("#productDeperciationAccountID", $createProductForm).val(),
                expiryAlertArray: expiryAlartArray,
                itemAttrValue: itemAttrValue,
                discountSchemeID: ($("#discountSchemeID", $createProductForm).val()) ? $("#discountSchemeID", $createProductForm).val() : null,
            };
          
            if (validateProductForm(formData)) {

                if (inventoryTypeChangeFlag) {
                    $('#change_inventory_type').modal('show');
                    $('#change-inventory-type-button').on('click',function(e){
                        e.preventDefault();
                        var valid = new Array(
                                        $('#username').val(),
                                        $('#password').val(),
                                    );
                        if(validateParams(valid)){
                            eb.ajax({
                                type: 'POST',
                                url: BASE_URL + '/productAPI/getAdminPrivilegeForInventoryTypeChange',
                                data: {
                                    username: $("#username").val(),
                                    password: $('#password').val()
                                },
                                success: function(respond) {
                                    if(respond.status){
                                        saveAndUpdateProduct(formData);
                                    } else {
                                        p_notification(respond.status, respond.msg);
                                    }
                                },
                                async: false
                            });
                        }
                    });
                } else {
                    saveAndUpdateProduct(formData);
                }
                
            }
        }

        function saveAndUpdateProduct (formData) {
            formData['uom'] = getAllUoms();
            formData['displayUoms'] = getUomsDispaly();
            formData['suppliers'] = getSelectedSuppliers();
            var action = (EDIT_MODE) ? 'save-product' : 'add-product';
            if (EDIT_MODE) {
                formData['productID'] = $("[name='productID']").val();
            }
            formData['itemBarCodes'] = itemBarCodesList;
            var barCodeString = "";
            $.each(itemBarCodesList, function(key, valueObj) {
                if (key == 0) {
                    barCodeString = valueObj['itemBarCode'];
                } else {
                    barCodeString = barCodeString +','+valueObj['itemBarCode'];
                }
            });
            
            formData['productBarcode'] = barCodeString;

            eb.ajax({
                url: '/productAPI/' + action,
                method: 'post',
                data: formData,
                dataType: 'json',
                success: function(data) {
                    if (data.status == true) {
                        if (!$('.addNewItem').length) {
                            input = document.getElementById('image_file');
                            var file;
                            file = input.files[0];
                            if (file) {
                                imagedata = false;
                                if (window.FormData) {
                                    imagedata = new FormData();
                                }
                                e.preventDefault();
                                imagedata.append("ID", data.data.productID);
                                imagedata = eb.getImageDimension("images", file, imagedata, '200', '200', '0', '0', '200', '200');
                                eb.ajax({
                                    url: BASE_URL + '/productAPI/preview',
                                    type: 'POST',
                                    processData: false,
                                    contentType: false,
                                    data: imagedata,
                                    success: function(res) {
                                        if (res) {
                                            if (!EDIT_MODE) {
                                                window.location = (BASE_URL + '/product/location-product-details/' + data.data.productID);
                                            } else {
                                                // redirecting to the same page because location product rows need to update
                                                // eg: updating product discounts should update locationwise values too
                                                // TODO - use ajax to load the view without refreshing the page
                                                window.location = (BASE_URL + '/product/edit/' + data.data.productID);
                                            }
                                            imageID = res[0];
                                        }
                                    }
                                });
                            } else {
                                if (!EDIT_MODE) {
                                    window.location = (BASE_URL + '/product/location-product-details/' + data.data.productID);
                                } else {
                                    // redirecting to the same page because location product rows need to update
                                    // eg: updating product discounts should update locationwise values too
                                    // TODO - use ajax to load the view without refreshing the page
                                    window.location = (BASE_URL + '/product/edit/' + data.data.productID);
                                }
                            }
                        } else {
                            var startingQuantity = $('#startingQuantity').val();
                            var openingPrice = $('#productDefaultPurchasePrice').val();
                            var productType = $('#productTypeID').val();
                            var productID = data.data.productID;
                            if (startingQuantity != '' && productType == 1) {
                                var items = {};
                                var locationProductData = data.data.locationProductData;

                                var itemUom = _.find(formData.uom, function(val, key) {
                                    if (val.displayUom == 1 || val.displayUom) {
                                        val['uomID'] = key;
                                        return val;
                                    }
                                });
                                var uomID = itemUom.uomID;
                                var locationProductID = locationProductData.locationProductID;
                                var uomConversionRate = $("#qty").siblings('.uom-select').find('.selected').data('uc');
                                uomConversionRate = uomConversionRate != 0 ? uomConversionRate : itemUom.uomConversion;

                                items[productID] = new positiveAdjustmentProduct(locationProductID, productID, formData.productCode, formData.productName, startingQuantity, openingPrice, formData.uom, uomID, '0', {}, {}, productType, uomConversionRate);

                                var goods_issue_tbl = {
                                    goods_issue_date: $('#adjusmentDate').val(),
                                    goods_issue_reason: "opening balnce from invoice",
                                    goods_issue_type_id: 2,
                                    locRefID: 'No'
                                };
                                eb.ajax({
                                    type: 'POST',
                                    url: BASE_URL + '/api/inventory-adjustment/update-positive-adjustment',
                                    async: false,
                                    data: {items: items, goods_issue_tbl_data: goods_issue_tbl, locationID: locationOut, flag: 'invoice'},
                                });
                            }
                            eb.ajax({
                                type: 'POST',
                                url: BASE_URL + '/productAPI/getActiveProductsFromLocation',
                                data: {locationID: locationOut},
                                success: function(respond) {
                                    var currentElem = new Array();
                                    $.each(respond.data, function(key, valueObj) {
                                        if (valueObj.sales) {
                                            currentElem[key] = valueObj;
                                        }
                                    });
                                    locationProducts = {};
                                    locationProducts = $.extend({}, currentElem);

                                    var srtArr = currentPath.split("/");

                                    if (srtArr[1] != "quotation") { 
                                        $('#itemCode', productRow).append($("<option value='pid_" + productID + "'>" + formData.productCode + '-' + formData.productName + "</option>"));
                                        $('#itemCode', productRow).selectpicker('refresh');
                                        $('#itemCode', productRow).val('pid_'+productID);
                                        $('#itemCode', productRow).selectpicker('refresh');
                                        
                                        var itemmapper = (typeof $('#itemCode').data('extra') !== 'undefined') ? $('#itemCode').data('extra') : {} ;
                                        itemmapper['pid_'+productID] = {'pid':productID};
                                        
                                        $('#itemCode').data('extra',itemmapper);
                                        $('#itemCode', productRow).trigger('change');
                                    } else {
                                        $('#item_code').append($("<option value='" + productID + "'>" + formData.productCode + '-' + formData.productName + "</option>"));
                                        $('#item_code').selectpicker('refresh');
                                        $('#item_code').val(productID);
                                        $('#item_code').selectpicker('refresh');
                                        
                                        var itemmapper = (typeof $('#item_code').data('extra') !== 'undefined') ? $('#item_code').data('extra') : {} ;
                                        itemmapper['pid_'+productID] = {'pid':productID};
                                        
                                        $('#item_code').data('extra',itemmapper);
                                        $('#item_code').trigger('change');
                                    }

                                    
                                    
                                    clearProductScreenForInvocie();
                                    
                                    $('#createProduct').modal('hide');
                                    p_notification(data.status, data.msg);
                                }
                            });
                        }
                    } else {
                        p_notification(false, data.msg);
                    }
                }
            });
        }
    });

///////////////////////////////////////////
    $('#item-attr-add-item').on('click', function(e){
        e.preventDefault();
        itemAttrTmpryValue = itemAttrValue;
        $('#item-attr-add-product-table tbody tr.clonedRow').remove();
        if($(this).hasClass('edited')){
            if(itemAttrValueEditedFlag == true){
                $.each(itemAttrValue, function(key, value){
                    if(value != undefined){
                        $('#item-attr-add-product-table tbody tr.sample-data-set').clone()
                            .removeClass('hidden sample-data-set')
                            .addClass('clonedRow').attr('id','add_'+key)
                            .appendTo('#item-attr-add-product-table tbody');
                        $('#add_'+key).children('td').find('.select-item-attr-add-pro').val(key).attr('disabled',true);
                        $('#add_'+key).find('.add-values').addClass('hidden');
                        $('#add_'+key).find('.delete-values').removeClass('hidden');
                        $('#add_'+key).find('.edit-values').removeClass('hidden');
                        $('#add_'+key).attr('data-itemid', key);
                    }
                });
                $('#item-attr-add-product-table tbody tr.sample-data-set').clone()
                    .removeClass('hidden sample-data-set')
                        .addClass('clonedRow')
                            .appendTo('#item-attr-add-product-table tbody');
            } else {
                var proID =$("[name='productID']").val();
                eb.ajax({
                type: 'POST',
                data: {id: proID},
                url: BASE_URL + '/api/settings/item-attribute/get-item-attributes-by-product-id',
                success: function(respond) {
                        if(respond.status){
                            $.each(respond.data, function(key, value){
                                $('#item-attr-add-product-table tbody tr.sample-data-set').clone()
                                    .removeClass('hidden sample-data-set')
                                    .addClass('clonedRow').attr('id','edit_'+key)
                                    .appendTo('#item-attr-add-product-table tbody');
                                $('#edit_'+key).children('td').find('.select-item-attr-add-pro').val(key).attr('disabled',true);
                                $('#edit_'+key).find('.add-values').addClass('hidden');
                                $('#edit_'+key).find('.delete-values').removeClass('hidden');
                                $('#edit_'+key).find('.edit-values').removeClass('hidden');
                                $('#edit_'+key).attr('data-itemid', key);
                                var itemAttValueTmp = [];
                                $.each(value, function(subKey, subValue){
                                    itemAttValueTmp[subKey] = subValue;
                                });
                                itemAttrTmpryValue[key] = itemAttValueTmp;
                                
                            });
                        }
                        itemAttrValueEditedFlag = true;
                        $('#item-attr-add-product-table tbody tr.sample-data-set').clone()
                            .removeClass('hidden sample-data-set')
                                .addClass('clonedRow')
                                    .appendTo('#item-attr-add-product-table tbody');
                       
                    }

                });
            }
           
        } else {
            if(itemAttrValue.length > 0){
                $.each(itemAttrValue, function(key, value){
                    if(value != undefined){
                        $('#item-attr-add-product-table tbody tr.sample-data-set').clone()
                            .removeClass('hidden sample-data-set')
                            .addClass('clonedRow').attr('id','add_'+key)
                            .appendTo('#item-attr-add-product-table tbody');
                        $('#add_'+key).children('td').find('.select-item-attr-add-pro').val(key).attr('disabled',true);
                        $('#add_'+key).find('.add-values').addClass('hidden');
                        $('#add_'+key).find('.delete-values').removeClass('hidden');
                        $('#add_'+key).find('.edit-values').removeClass('hidden');
                        $('#add_'+key).attr('data-itemid', key);
                    }
                });
            }
            $('#item-attr-add-product-table tbody tr.sample-data-set').clone()
                .removeClass('hidden sample-data-set')
                    .addClass('clonedRow')
                        .appendTo('#item-attr-add-product-table tbody');
        
            
            
        }
        $('#itemAttrViewModal').modal('show');
        

    });
   
    $('#item-attr-add-product-table').on('click', '.add-values', function(e){
        e.preventDefault();
        var attrRow = $(this).parents('tr');
        var thisItemID = $('.select-item-attr-add-pro', attrRow).val();
        if(itemAttrValue[thisItemID] != undefined){
            p_notification(false, eb.getMessage('ERR_SAME_ITEM_ATTR'));
            return false;
        } else {
            $('.clonedRow', $(this).parents()).attr('data-itemid', thisItemID);
            eb.ajax({
            type: 'POST',
            data: {id: thisItemID},
            url: BASE_URL + '/api/settings/item-attribute/view-sub-item',
            success: function(respond) {
                    if(respond.status){
                        $('#item-attr-add-product-value-table tbody tr').remove();
                        $.each(respond.data, function(key, value){
                            var newRow = "<tr><td class='descript clearfix' id='"+value['itemAttributeValueID']+"'><input class='form-control' type='text' disabled \n\
                            value='"+ value['itemAttributeValueDescription'] +"'></td><td></td>\n\
                            <td><input type='checkbox' class='value-check-box'> </td>";
                        $('#item-attr-add-product-value-table tbody').append(newRow);
                        });
                    $('#itemAttrValueViewModal').modal('show');
                    $('#item-att-value-adding-modal .add-sub-item-attr').attr('data-attrid',thisItemID);
                    }
                }
            });
    
        } 
        
        
    });
    $('#itemAttrValueViewModal').on('click','.add-sub-item-attr', function(e){
        e.preventDefault();
        var subItemAttr = [];
        var itemInFlag = false;
        var itemAttID = $(this).attr('data-attrid');
        $.each($(this).parents().find('#item-attr-add-product-value-table tbody tr'), function(key, value){
            if($(value).find('.value-check-box').is(':checked')){
                subItemAttr[key] = $(value).find('.descript').attr('id');
                itemInFlag = true;
            } 
            itemAttrTmpryValue[itemAttID] = subItemAttr;
            
        });
        if(!itemInFlag){
            p_notification(false, eb.getMessage('ERR_ITEM_ATTR_VALUE_NULL'));
            return false;
        } else {
            $('#itemAttrValueViewModal').modal('hide');
            var selectedRow = $("[data-itemid='"+itemAttID+"']");
            $('.select-item-attr-add-pro', selectedRow).attr('disabled',true);
            $('.add-values', selectedRow).addClass('hidden');
            $('.delete-values', selectedRow).removeClass('hidden');
            $('.edit-values',selectedRow).removeClass('hidden');

            if(!$(this).hasClass('editMode')){
                $('#item-attr-add-product-table tbody tr.sample-data-set').clone()
                .removeClass('hidden sample-data-set').addClass('clonedRow')
                    .appendTo('#item-attr-add-product-table tbody');
            }
            
            itemInFlag = false;
        }
    }); 

    $('#itemAttrViewModal').on('click','.close-item-attr-modal', function(e){
        e.preventDefault();
        itemAttrTmpryValue = [];
        // itemAttrValueEditedFlag = false;
    });
    
    $('#itemAttrViewModal').on('click','.save-item-attr-modal', function(e){
        e.preventDefault();
        if(itemAttrTmpryValue == '' && itemAttrValue == ''){
            p_notification(false,eb.getMessage('ERR_NO_ITEM_ATTR_SELECT'));
            return false;
        } else {
            itemAttrValue = itemAttrTmpryValue;
            $('#itemAttrViewModal').modal('hide');
            itemAttrTmpryValue = [];
        }
                
    });

    $('#itemAttrViewModal').on('click', '.delete-values', function(e){
        e.preventDefault();
        var itemAttrID = $(this).parents('tr').attr('data-itemid');
        itemAttrTmpryValue.splice(itemAttrID,1);
        $(this).parents('tr').remove();
    });

    $('#itemAttrViewModal').on('click','.edit-values', function(e){
        var itemAttrbID = $(this).parents('tr').attr('data-itemid');
        eb.ajax({
            type: 'POST',
            data: {id: itemAttrbID},
            url: BASE_URL + '/api/settings/item-attribute/view-sub-item',
            success: function(respond) {
                    if(respond.status){
                        $('#item-attr-add-product-value-table tbody tr').remove();
                        $.each(respond.data, function(key, value){
                            if(jQuery.inArray(value['itemAttributeValueID'], itemAttrTmpryValue[itemAttrbID]) !== -1){
                               var newRow = "<tr><td class='descript clearfix' id='"+value['itemAttributeValueID']+"'><input class='form-control' type='text' disabled \n\
                                    value='"+ value['itemAttributeValueDescription'] +"'></td><td></td>\n\
                                    <td><input type='checkbox' class='value-check-box' checked> </td>"; 
                            } else {
                                var newRow = "<tr><td class='descript clearfix' id='"+value['itemAttributeValueID']+"'><input class='form-control' type='text' disabled \n\
                                    value='"+ value['itemAttributeValueDescription'] +"'></td><td></td>\n\
                                    <td><input type='checkbox' class='value-check-box'> </td>";
                            }
                           
                        $('#item-attr-add-product-value-table tbody').append(newRow);
                        });
                    $('#itemAttrValueViewModal').modal('show');
                    $('#item-att-value-adding-modal .add-sub-item-attr').attr('data-attrid',itemAttrbID).addClass('editMode');
                    }
                    
                }

            });
    });


//////////////////////////////////////////
    function  clearProductScreenForInvocie() {
        $('#productCode').val('');
        $('#productName').val('');
        $('#startingQuantity').val('');
        $('#startingQuantity').attr('disabled', false);
        $('#productBarcode').val('');
        $('#productTypeID').val(1);
        $('#productHandelingPurchaseProduct').attr('checked', false);
        $('#productHandelingSalesProduct').attr('checked', false);
        $('#productHandelingConsumables').attr('checked', false);
        $('#productHandelingFixedAssets').attr('checked', false);
        $('#productDescription').val('');
        $("#showtax").slideUp();
        $("#showdiscount").slideUp();
        $('.uom-list tbody tr').each(function() {
            if (!($(this).hasClass('base') || $(this).hasClass('sample'))) {
                $(this).remove();
            }
        });
        $('#productDefaultSellingPrice').val('');
        $('#productDefaultMinimumInventoryLevel').val('');
        $('#productDefaultMaximunInventoryLevel').val('');
        $('#productDefaultReorderLevel').val('');
        $('#productTaxEligible').attr('checked', false);
        $('.tax-table').find('.taxes').attr('checked', false);
        $('#productDiscountEligible').attr('checked', false);
        $('#productDiscountPercentage').val('');
        $('#productDiscountValue').attr('');
    }

    function positiveAdjustmentProduct(lPID, pID, pCode, pN, pQ, pUP, pUom, uomID, pTotal, bProducts, sProducts, productType, uomConversionRate) {
        this.locationPID = lPID;
        this.pID = pID;
        this.pCode = pCode;
        this.pName = pN;
        this.pQuantity = pQ;
        this.pUnitPrice = pUP;
        this.pUom = pUom;
        this.uomID = uomID;
        this.pTotal = pTotal;
        this.bProducts = bProducts;
        this.sProducts = sProducts;
        this.productType = productType;
        this.uomConversionRate = uomConversionRate;
    }

    function validateProductForm(formData) {
        var success = true;
        if (formData.productCode == null || formData.productCode == "") {
            p_notification(false, eb.getMessage('ERR_PRODUCT_ITEMCODE'));
            $("[name='productCode']", $createProductForm).focus();
            success = false;
        } else if (formData.productName == null || formData.productName == "") {
            p_notification(false, eb.getMessage('ERR_PRODUCT_ITEMNAME'));
            $("[name='productName']", $createProductForm).focus();
            success = false;
        } else if ((formData.handeling).length == 0) {
            p_notification(false, eb.getMessage('ERR_PRODUCT_HANDLINGTYPE'));
            success = false;
        } else if (CATEGORIES_PATH_LIST[formData.categoryID] == undefined) {
            p_notification(false, eb.getMessage('ERR_PRODUCT_VALIDCATG'));
            $("[name='categoryParentIDHelper']", $createProductForm).focus();
            return false;
        } else if (!$("input:radio[name=productGlobalProduct]").is(':checked')) {
            p_notification(false, eb.getMessage('ERR_PRODUCT_LOCATRADIO'));
            success = false;
        } else if (formData.productGlobalProduct == 0 && (formData.locations).length == 0) {
            p_notification(false, eb.getMessage('ERR_PRODUCT_ONELOCAT'));
            success = false;
        } else if (formData.productTaxEligible == 1 && (formData.taxes).length == 0) {
            p_notification(false, eb.getMessage('ERR_PRODUCT_TAXELIGIBLE'));
            success = false;
            //temporary comment that validation...
//        } else if (formData.discountEligible == 1 && (formData.productDiscountPercentage.trim() == '') && (formData.productDiscountValue.trim() == '')) {
//            p_notification(false, eb.getMessage('ERR_PRODUCT_DISVALUE'));
//            $("[name='productDiscountPercentage']", $createProductForm).focus();
//            success = false;
        } else if (formData.discountEligible == 1 && (formData.productDiscountPercentage.trim() != '') && (formData.productDiscountValue.trim() != '')) {
            p_notification(false, eb.getMessage('ERR_PRODUCT_VAL_OR_PERCENT'));
            $("[name='productDiscountPercentage']", $createProductForm).focus();
            success = false;
        }else if (formData.discountEligible == 1 && formData.useDiscountSchema == 0 && (formData.productDiscountPercentage.trim() == '') && (formData.productDiscountValue.trim() == '')) {
            p_notification(false, eb.getMessage('ERR_PRODUCT_VAL_OR_PERCENT_EMPTY'));
            $("[name='productDiscountPercentage']", $createProductForm).focus();
            success = false;
        } else if (formData.discountEligible == 1 && formData.useDiscountSchema == 1) {
            if (formData.discountSchemeID == null || formData.discountSchemeID == 0 || formData.discountSchemeID == '') {
                p_notification(false, eb.getMessage('ERR_PRODUCT_DIS_SCHEM_EMPTY'));
                success = false;
            }

        } else if(formData.purchaseDiscountEligible == 1 && (formData.productPurchaseDiscountPercentage.trim() != '') && (formData.productPurchaseDiscountValue.trim() != '')) {
            p_notification(false, eb.getMessage('ERR_PRODUCT_VAL_OR_PERCENT_PURCHASE'));
            $("[name='productPurchaseDiscountPercentage']", $createProductForm).focus();
            success = false;
        }  else if(formData.purchaseDiscountEligible == 1 && (formData.productPurchaseDiscountPercentage.trim() == '') && (formData.productPurchaseDiscountValue.trim() == '')) {
            p_notification(false, eb.getMessage('ERR_PRODUCT_VAL_OR_PERCENT_EMPTY_PURCHASE'));
            $("[name='productPurchaseDiscountPercentage']", $createProductForm).focus();
            success = false;
        } else if (formData.discountEligible == 1 && (formData.productDiscountPercentage != "") && ((isNaN(parseFloat(formData.productDiscountPercentage))))) {
            p_notification(false, eb.getMessage('ERR_PRODUCT_VALID_DISCPERCENT'));
            $("[name='productDiscountPercentage']", $createProductForm).focus();
            success = false;
        } else if (formData.purchaseDiscountEligible == 1 && (formData.productPurchaseDiscountPercentage != "") && ((isNaN(parseFloat(formData.productPurchaseDiscountPercentage))))) {
            p_notification(false, eb.getMessage('ERR_PRODUCT_VALID_DISCPERCENT_PURCHASE'));
            $("[name='productPurchaseDiscountPercentage']", $createProductForm).focus();
            success = false;
        } else if (formData.discountEligible == 1 && (formData.productDiscountPercentage != "") && (parseFloat(formData.productDiscountPercentage) > 100)) {
            p_notification(false, eb.getMessage('ERR_PRODUCT_DISCPERCENT_LIMIT'));
            $("[name='productDiscountPercentage']", $createProductForm).focus();
            success = false;
        } else if(formData.purchaseDiscountEligible == 1 && (formData.productPurchaseDiscountPercentage != "") && (parseFloat(formData.productPurchaseDiscountPercentage) > 100)) {
            p_notification(false, eb.getMessage('ERR_PRODUCT_DISCPERCENT_LIMIT_PURCHASE'));
            $("[name='productPurchaseDiscountPercentage']", $createProductForm).focus();
            success = false; 
        } else if (formData.discountEligible == 1 && (formData.productDiscountValue != "") && ((isNaN(parseFloat(formData.productDiscountValue))))) {
            p_notification(false, eb.getMessage('ERR_PRODUCT_VALID_DISCVAL'));
            $("[name='productDiscountValue']", $createProductForm).focus();
            success = false;
        } else if(formData.purchaseDiscountEligible == 1 && (formData.productPurchaseDiscountValue != "") && ((isNaN(parseFloat(formData.productPurchaseDiscountValue))))) {
            p_notification(false, eb.getMessage('ERR_PRODUCT_VALID_DISCVAL_PURCHASE'));
            $("[name='productDiscountValue']", $createProductForm).focus();
            success = false;
        } else if (formData.productDefaultSellingPrice != '' && isNaN(formData.productDefaultSellingPrice)) {
            p_notification(false, eb.getMessage('ERR_PRODUCT_VALID_PRODDEFUSELLPRI'));
            $("[name='productDefaultSellingPrice']", $createProductForm).focus();
            success = false;
        } else if (formData.productDefaultOpeningQuantity != '' && (isNaN(formData.productDefaultOpeningQuantity) || formData.productDefaultOpeningQuantity < 0)) {
            p_notification(false, eb.getMessage('ERR_PRODUCT_VALID_PRODDEFOPENQTY'));
            $("[name='productDefaultOpeningQuantity']", $createProductForm).focus();
            success = false;
        } else if (formData.productDefaultMinimumInventoryLevel != '' && isNaN(formData.productDefaultMinimumInventoryLevel)) {
            p_notification(false, eb.getMessage('ERR_PRODUCT_VALID_PRODDEFUMININVELEVEL'));
            $("[name='productDefaultMinimumInventoryLevel']", $createProductForm).focus();
            success = false;
        } else if (formData.productDefaultMaximunInventoryLevel != '' && isNaN(formData.productDefaultMaximunInventoryLevel)) {
            p_notification(false, eb.getMessage('ERR_PRODUCT_VALID_PRODDEFUMAXINVELEVEL'));
            $("[name='productDefaultMaximunInventoryLevel']", $createProductForm).focus();
            success = false;
        } else if (formData.productDefaultReorderLevel != '' && isNaN(formData.productDefaultReorderLevel)) {
            p_notification(false, eb.getMessage('ERR_PRODUCT_VALID_PRODDEFUREORDERLEV'));
            $("[name='productDefaultReorderLevel']", $createProductForm).focus();
            success = false;
        } else if (formData.productDefaultMinimumInventoryLevel != '' && (formData.productDefaultReorderLevel == '' || parseFloat(formData.productDefaultReorderLevel) < parseFloat(formData.productDefaultMinimumInventoryLevel))) {
            p_notification(false, eb.getMessage('ERR_PRODUCT_VALID_IL_CANT_BE_MOTHAN_RL'));
            $("[name='productDefaultMinimumInventoryLevel']", $createProductForm).focus();
            success = false;
        }  else if (formData.productDefaultMaximunInventoryLevel != '' && (formData.productDefaultMinimumInventoryLevel == '' || parseFloat(formData.productDefaultMaximunInventoryLevel) < parseFloat(formData.productDefaultMinimumInventoryLevel))) {
            p_notification(false, eb.getMessage('ERR_PRODUCT_VALID_IL_CANT_BE_MOTHAN_MIL'));
            $("[name='productDefaultMaximunInventoryLevel']", $createProductForm).focus();
            success = false;
        } else if ((formData.customDutyPercentage != null || formData.customDutyPercentage != "") && isNaN(formData.customDutyPercentage)) {
            p_notification(false, eb.getMessage('ERR_PRODUCT_CUSDUTY'));
            $("[name='customDutyPercentage']", $createProductForm).focus();
            success = false;
        } else if ((formData.customDutyPercentage.trim() != '') && (formData.customDutyValue.trim() != '')) {
            p_notification(false, eb.getMessage('ERR_PRODUCT_CUSDUTYVAL_PERCENT'));
            $("[name='customDutyPercentage']", $createProductForm).focus();
            success = false;
        } else if ((formData.customDutyPercentage != null || formData.customDutyPercentage != "") && parseFloat(formData.customDutyPercentage) > 100) {
            p_notification(false, eb.getMessage('ERR_PRODUCT_CUSDUTY_PERCENT'));
            $("[name='customDutyPercentage']", $createProductForm).focus();
            success = false;
        } else if ((formData.customDutyValue != null || formData.customDutyValue != "") && isNaN(formData.customDutyValue)) {
            p_notification(false, eb.getMessage('ERR_PRODUCT_VALID_CUSDUTY'));
            $("[name='customDutyValue']", $createProductForm).focus();
            success = false;
        } else if (!validateUomList()) {
// errors will be displayed from the 'validateUomList' method
            success = false;
        } else if (!formData.uomDisplay) {
            p_notification(false, eb.getMessage('ERR_PRODUCT_DISPLAY_UOM'));
            success = false;
        }
        else if ($('#startingQuantity').length) {
            if (formData.productTypeID == 1) {
                if (isNaN($('#startingQuantity').val()) || $('#startingQuantity').val() < 0) {
                    p_notification(false, eb.getMessage('ERR_PRODUCT_VALID_OB'));
                    $("#startingQuantity", $createProductForm).focus();
                    success = false;
                }
            }
        } else if (EDIT_MODE) {
            var pID = $("[name='productID']").val();
            eb.ajax({
                type: 'POST',
                url: BASE_URL + '/productAPI/getProductTransactions',
                data: {productID: pID},
                success: function(respond) {
                    if (respond.status) {
                        $.each(respond.data, function(key, value) {
                            if ((value.itemInID != null || value.itemOutID != null) && value.productTypeID == "1") {
                                if (formData.productTypeID != value.productTypeID) {
                                    p_notification(false, eb.getMessage('ERROR_PRODUCT_TYPE_SAVE'));
                                    success = false;                           
                                }
                            } 
                        });

                        if (respond.data[0].productTypeID == "2" && respond.data[0].productTypeID != formData.productTypeID) {
                            p_notification(false, eb.getMessage('ERROR_PRODUCT_TYPE_SAVE_NON_INV'));
                            success = false;
                        }

                        if (respond.data[0].productTypeID != formData.productTypeID) {
                            inventoryTypeChangeFlag = true;
                        }
                    }
                },
                async: false
            });
        }
        return success;
    }
    $("#potentialSuppliers").attr('data-live-search', 'true').selectpicker();
    var potentialSuppliersDropdown = $("#potentialSuppliers");
    var $supplierList = $("table.suppliers-list", $createProductForm);
    var $supplierListRow = $("tbody tr.sample", $supplierList);
    potentialSuppliersDropdown.change(function() {
        var supplierID = potentialSuppliersDropdown.val();
        var supplierName = $("option:selected", potentialSuppliersDropdown).text();
        var currSuppliers = getSelectedSuppliers();
        if (currSuppliers.indexOf(supplierID) < 0) {
            if (supplierID != '') {
                var $newRow = $supplierListRow.clone().removeClass('hidden sample');
                $newRow.find('td:first').text(supplierName);
                $newRow.data('supplierid', supplierID);
                $newRow.appendTo($supplierList.find('tbody'));
                updateSupplierListTableHeight();
            }
        } else {
            p_notification(false, eb.getMessage('ERR_PRODUCT_SUPPLIER'));
            return false;
        }
    });
    $supplierList.on('click', '.delete', function(e) {
        $(this).parents('tr').remove();
        updateSupplierListTableHeight();
        e.stopPropagation();
        return false;
    });
    function getSelectedSuppliers() {
        var $allSupplierTrs = $("tr:not(.sample)", $supplierList);
        var supplierList = [];
        $allSupplierTrs.each(function() {
            supplierList.push($(this).data('supplierid'));
        });
        return supplierList;
    }
    $("#itemBarcodeBtn").on('click', function(e){
        $('#barCodeAddModal').modal('show');
    });

    function refreshItemBarcodeList(){
        var model = $('#barCodeAddModal');
        var tbody = $('#itemBarcodeTbody', model);
        $('tr.saved', tbody).remove();
        $.each(itemBarCodesList, function(index, value) {
            var $newTr = $($('tr.subSample', tbody).clone()).appendTo(tbody);
            $('#bar-code', $newTr).val(value.itemBarCode);
            $('#barcodeId', $newTr).val(value.itemBarCodeID);
            $newTr.removeClass('hidden subSample');
            $newTr.addClass('saved').attr('id', 'saved');
        });
    }

    $('#addBarCode', itemBarcodeTbody).on('click', function(e) {
        var tr = $(this).parents('tr');
        var name = $('#name', tr).val();
        if (name == null || name == '') {
            p_notification(false, eb.getMessage('ERR_INV_BAR_CODE'));
            return false;
        } else if (itemBarCodesList.some(el => el.itemBarCode === name)) {
            p_notification(false, eb.getMessage('ERR_DUP_BAR_CODE'));
            return false;
        } else if (name.length > 25){
            p_notification(false, eb.getMessage('ERR_BARCODE_LENGTH'));
            return false;
        } else if(!name.match(/^[0-9A-Za-z\-_\.\% ]*$/)) {
            p_notification(false, eb.getMessage('ERR_INVALID_BARCODE'));
            return false;
        } else {
            itemBarCodesList.push({'itemBarCodeID':'new', 'itemBarCode':name});
            $('#name', tr).val('');
            refreshItemBarcodeList();
        }
    });

    itemBarcodeTbody.on('click', '.delete', function(e) {
        var tr = $(this).parents('tr');
        var id = $('#barcodeId', tr).val();
        var name = $('#bar-code', tr).val();
        itemBarCodesList = $.grep(itemBarCodesList, function(data, index) {
           return data.itemBarCode != name;
        })
        
        refreshItemBarcodeList();
    });

    $('#btnAddItemBarCodes').on('click', function(e) {
        $('#barCodeAddModal').modal('hide');
    });

    function updateSupplierListTableHeight() {
        if ($("tr:not(.sample)", $supplierList).length) {
            $(".suppliers-list-cont").show();
        } else {
            $(".suppliers-list-cont").hide();
        }
    }
    updateSupplierListTableHeight();
    $("form.product-search").submit(function(e) {
        var keyword = $('#productSearch').val();
        var fixedAsset = false;
        if (window.location.pathname == '/fixed-asset-management/listFixedAsset') {
            fixedAsset = true;
        }
        var param = {searchProductString: keyword, fixedAssetFlag : fixedAsset};
        getViewAndLoad('/productAPI/getProductsForSearch', 'productList', param);
        e.stopPropagation();
        return false;
    });
    $("form.product-search button.reset").click(function(e) {
        $("#productSearch").val('');
        $("form.product-search").submit();
    });
    if ($createProductForm.length) {
        initializeCategoryTypeahead($createProductForm);
        initializeCategoryTypeahead($createCategoryForm);
        createCategoryFormSubmitCallback = function(data) {
            initializeCategoryTypeahead($createProductForm, data.data.paths, data.data.categories);
            initializeCategoryTypeahead($createCategoryForm, data.data.paths, data.data.categories);
            $("[name='categoryParentID']", $createProductForm).val(data.data.categoryID).change();
            $("[name='categoryParentIDHelper']", $createProductForm).val(CATEGORIES_PATH_LIST[data.data.categoryID]);
            $('.modal').modal('hide');
        }
    }
//edit category name was not populate in edit product view therfor add this to show category
    if ($("input[name='editmode']").length) {
        $("[name='categoryParentID']").val(editCategoryID);
        $("[name='categoryParentID']").selectpicker('refresh');
    } else {
        var defaultCategory = $("[name='categoryParentID']").attr('data-id');
        $("[name='categoryParentID']").val(defaultCategory);
        $("[name='categoryParentID']").selectpicker('refresh');
        var defaultUomID = $('#defaultUomID').val();
        $("[name='uomID[]']").val(defaultUomID);
    }

    $('#image-submit').on('click', function(e) {
        input = document.getElementById('image_file');
        var file;
        file = input.files[0];
        if (file) {
            imagedata = false;
            if (window.FormData) {
                imagedata = new FormData();
            }
            e.preventDefault();
            imagedata = eb.getImageDimension("images", file, imagedata, '200', '200', '0', '0', '200', '200');
            eb.ajax({
                url: BASE_URL + '/image-preview',
                type: 'POST',
                processData: false,
                contentType: false,
                data: imagedata,
                success: function(data) {
                    $('#thumbs').attr('src', data);
                    $('#imageUploaderModal').modal('hide');
                    $('#image_file').data('flag', true);
                    return data;
                }
            });
        }
    });
    //    $('#showProductList').on('click', function() {
////        var param = {};
////        getViewAndLoad('/supplierAPI/getSupplierListView', 'supplierList', param);
//        $('#createProductForm').toggleClass('hidden');
//        $('#productList').toggleClass('hidden');
//    });
    $("#product_more").hide();
    $("#moredetailsProdct").on('click', function() {
        $("#product_more").slideDown();
        $('#moredetailsProdct').hide();
    });
    var expire_alert_counter = 0;
    var tempExpDateAlert = [];
    var initialExpDateModal = true;
    $('#expiry_alert_btn').on('click', function(e) {
        e.preventDefault();
        $('#expAlertBtn').modal('show');
        tempExpDateAlert = [];
        if (EDIT_MODE && initialExpDateModal && expiryAlartArray.length > 1) {
            for (var i = expiryAlartArray.length - 1; i >= 0; i--) {
                expire_alert_counter++;
                if (expiryAlartArray[i] != 0) {
                    var className = 'clonedDiv_'+expire_alert_counter;
                    var clonedClass = $('.sample_data_set').clone().appendTo('#expire_data_div')
                        .attr('class', className +' clonedDiv');
                    $(clonedClass).children().find('.number_of_expiry_days').val(expiryAlartArray[i]).attr('disabled', true);
                    $(clonedClass).children().find('.add_unit').addClass('hidden');
                    $(clonedClass).children().find('.delete_unit').removeClass('hidden');
                }

            }    
        } 
        
        expire_alert_counter++;
        var className = 'clonedDiv_'+expire_alert_counter;
        if (initialExpDateModal) {
            $('.sample_data_set').clone().appendTo('#expire_data_div').attr('class', className +' clonedDiv');
            initialExpDateModal = false;
        }
    });

    $('#expire_data_div ').on('click','.btnExpiryDyAdd', function() {
        var expiryDays = $(this).parent('div').parent('div').find('.number_of_expiry_days ').val();
        var indexOftmpy = tempExpDateAlert.indexOf(expiryDays);
        var indexOfRealy = expiryAlartArray.indexOf(expiryDays);
        if (expiryDays == null || expiryDays == '') {
            p_notification(false, eb.getMessage('ERROR_EXPIRY_VALUE_NULL'));
            return false;
        }
        else if (isNaN(expiryDays)) {
            p_notification(false, eb.getMessage('ERROR_EXPIRY_DATA_TYPE'));
            return false;
        } else if (indexOftmpy != -1 || indexOfRealy != -1) {
            p_notification(false, eb.getMessage('ERROR_SAME_NUMBER'));
            return false;
        } else {
            $(this).parent('div').addClass('hidden');
            $(this).parents().find('.delete_unit').removeClass('hidden');
            $(this).parent('div').parent('div').find('.number_of_expiry_days ').attr('disabled', true);
            tempExpDateAlert.push(expiryDays); 
            expire_alert_counter++;
            var newClassName = 'clonedDiv_'+expire_alert_counter;
            $('.sample_data_set').clone().appendTo('#expire_data_div').attr('class', newClassName +' clonedDiv');
            $('.'+newClassName).find('.delete_unit').addClass('hidden');
        }
    });
    $('#expiry_data_set').on('click', '#expiry_date_set_key', function() {
        if (expiryAlartArray.length > 0) {
            Array.prototype.push.apply(expiryAlartArray, tempExpDateAlert);
        } else {
            expiryAlartArray = tempExpDateAlert;    
        }
        $('#expAlertBtn').modal('hide');
    });
    $('#expire_data_div ').on('click','.btnExpiryDyDel', function() {
        var removedValue = $(this).parent('div').parent('div').find('.number_of_expiry_days').val();
        var indexOftmp = tempExpDateAlert.indexOf(removedValue);
        var indexOfReal = expiryAlartArray.indexOf(removedValue);
        if (indexOftmp != -1) {
            tempExpDateAlert.splice(indexOftmp, 1);
        } else {
            expiryAlartArray.splice(indexOfReal, 1);
        }
        $(this).parent('div').parent('div').parent('div').parent('div').remove();

    });

    $('.expiryAlertClose').on('click', function() {
        $('#expire_data_div .clonedDiv').remove();
    });

});
function deleteProduct(productID, productName) {
    bootbox.confirm("Are you sure you want to delete this Product '" + productName + "' ?", function(result) {
        if (result == true) {
            eb.ajax({
                type: 'POST',
                url: BASE_URL + '/productAPI/deleteProduct/' + getCurrPage(),
                data: {productID: productID, fixedAssetFlag : false},
                success: function(respond) {
                    p_notification(respond.status, respond.msg);
                    if (respond.status == true) {
                        $('#productList').html(respond.html);
                    }
                }
            });
        }
    });
}

function deleteProductFixedAsset(productID, productName) {
    bootbox.confirm("Are you sure you want to delete this Product '" + productName + "' ?", function(result) {
        if (result == true) {
            eb.ajax({
                type: 'POST',
                url: BASE_URL + '/productAPI/deleteProduct/' + getCurrPage(),
                data: {productID: productID, fixedAssetFlag : true},
                success: function(respond) {
                    p_notification(respond.status, respond.msg);
                    if (respond.status == true) {
                        $('#productList').html(respond.html);
                    }
                }
            });
        }
    });
}

function productStatusChange(thisElem, productID, productName) {
    var state = !$('.glyphicon', thisElem).hasClass('glyphicon-check');
    var state_msg = (state) ? 'activate' : 'deactivate';
    bootbox.confirm("Are you sure you want to " + state_msg + " this item '" + productName + "'?", function(result) {
        if (result === true) {
            eb.ajax({
                type: 'POST',
                url: BASE_URL + '/productAPI/change-state/' + getCurrPage(),
                data: {productID: productID, productState: +state,fixedAssetFlag : false},
                success: function(data) {
                    if (data.status == true) {
                        $("#productList").html(data.html);
                        p_notification(true, data.msg);
                    } else {
                        p_notification(false, data.msg);
                    }
                }
            });
        }
    });
}

function productStatusChangeFixedAsset(thisElem, productID, productName) {
    var state = !$('.glyphicon', thisElem).hasClass('glyphicon-check');
    var state_msg = (state) ? 'activate' : 'deactivate';
    bootbox.confirm("Are you sure you want to " + state_msg + " this item '" + productName + "'?", function(result) {
        if (result === true) {
            eb.ajax({
                type: 'POST',
                url: BASE_URL + '/productAPI/change-state/' + getCurrPage(),
                data: {productID: productID, productState: +state, fixedAssetFlag : true},
                success: function(data) {
                    if (data.status == true) {
                        $("#productList").html(data.html);
                        p_notification(true, data.msg);
                    } else {
                        p_notification(false, data.msg);
                    }
                }
            });
        }
    });
}

// UOM related functions
var uomAddCount = 1;
var validateUomList;
var getAllUoms;
var getUomsDispaly;
$(function() {
    var $listArea = $("table.uom-list tbody");
    $("button.add-uom").click(function(e) {
        uomAddCount++;
        var $placeholderItem = $("tr.sample", $listArea);
        $listArea.append($placeholderItem.clone().removeClass('sample'));
    });
    $listArea.on('change', "[name='productBaseUom']", function() {
        var $thisParent = $(this).parents('tr');
        // TODO confirm before resetting conversions

        $("[name='productUomConversion[]']", $listArea).val('').prop('readonly', false);
        $("[name='productUomConversion[]']", $thisParent).val(1).prop('readonly', true);
        $("a.delete", $listArea).removeClass('hidden');
        $("a.delete", $thisParent).addClass('hidden');
    });
    $listArea.on('click', "tr:not(.base) [name='productBaseUom']", function(e) {
        var $thisItem = $(this);
        var $allUomTrs = $("tr:not(.sample, .base)", $listArea);
        var conversion_factors_empty = true;
        $("[name='productUomConversion[]']", $allUomTrs).each(function() {
            var conversionValue = $(this).val();
            if (!(isNaN(parseFloat(conversionValue)) || parseFloat(conversionValue) == 0)) {
                conversion_factors_empty = false;
                return false;
            }
        });
        if (!conversion_factors_empty) {
            // TODO check if values are present in input boxes
            bootbox.confirm('Changing the base Unit of Measure will reset all the conversion factors. Are you sure you want to continue?', function(result) {
                if (result) {
                    $thisItem.prop('checked', true).change();
                }
            });
            e.stopPropagation();
            return false;
        }
    });
    $listArea.on('click', "a.delete", function(e) {
        if ($(this).parents('tr').find("[name='productDisplayUom']").is(':checked')) {
            p_notification(false, eb.getMessage('ERR_PRODUCT_UOM_DELETION'));
            return false;
        }
        $(this).parents('tr').remove();
        e.stopPropagation();
        return false;
    });
    $listArea.on('change', "[name='productBaseUom']", function(e) {
        var $thisParent = $(this).parents('tr');
        $("[name='productUomConversion[]']", $listArea).val('').prop('readonly', false);
        $("[name='productUomConversion[]']", $thisParent).val(1).prop('readonly', true);
        $("tr", $listArea).removeClass('base');
        $thisParent.addClass('base');
    });
    validateUomList = function() {

        var $allUomTrs = $("tr:not(.sample)", $listArea);
        var uomList = {};
        var success = true;
        // validate if same uom is added multiple times
        $("[name='uomID[]']", $allUomTrs).each(function() {
            var $thisParent = $(this).parents('tr');
            var uomID = $(this).val();
            var conversionValue = $thisParent.find("[name='productUomConversion[]']").val();
            // conversion cannot be empty or less than 1
            if (uomID == '') {
                p_notification(false, eb.getMessage('ERR_PRODUCT_UOM_BASE'));
                success = false;
            } else if ((isNaN(parseFloat(conversionValue)) || parseFloat(conversionValue) == 0 || parseFloat(conversionValue) < 1) && success == true) {
                p_notification(false, eb.getMessage('ERR_PRODUCT_UOM'));
                $thisParent.find("[name='productUomConversion[]']").focus();
                success = false;
            }

            if (uomID != null) {
                uomList[uomID] = conversionValue;
            }
        });
        // if UOM list is empty
        if ($.isEmptyObject(uomList)) {
            p_notification(false, eb.getMessage('ERR_PRODUCT_ATLEASTONE_UOM'));
            success = false;
        } else if ($allUomTrs.length != Object.keys(uomList).length) {
            p_notification(false, eb.getMessage('ERR_PRODUCT_ADD_UOM'));
            success = false;
        }

        return success;
    }

    getAllUoms = function() {
        var $allUomTrs = $("tr:not(.sample)", $listArea);
        var uomList = {};
        $allUomTrs.each(function() {
            var uomID = $("[name='uomID[]']", $(this)).val();
            var uomConversion = ($("[name='productUomConversion[]']", $(this)).val()) ? $("[name='productUomConversion[]']", $(this)).val() : 1;
            var baseUom = $("[name='productBaseUom']", $(this)).is(':checked');
            var displayUom = $("[name='productDisplayUom']", $(this)).is(':checked');
            uomList[uomID] = {uomConversion: uomConversion, baseUom: baseUom, displayUom: displayUom};

        });
        return uomList;
    }
    getUomsDispaly = function() {
        var $allUomTrs = $("tr:not(.sample)", $listArea);
        var uomDisplayValue = false;
        $allUomTrs.each(function() {
            var uomID = $("[name='uomID[]']", $(this)).val();
            if ($(this).find('input[name="productDisplayUom"]').is(':checked')) {
                uomDisplayValue = uomID;
            }
        });
        return uomDisplayValue;
    }

    if (EDIT_MODE) {
        $('.product-edit-container .toggle_purpose').click(function() {
            var $main = $(this).parents('.product-edit-container');
            if (!$main.hasClass('expanded')) {
                $('.product-edit-container').removeClass('expanded')
                $main.addClass('expanded')
            } else {
                $main.removeClass('expanded')
            }
        });
    }

    $('#productTypeID').on('change', function() {
        var thisValue = $(this).val();
        if (!EDIT_MODE) {
            if (thisValue == 2) {
                $('.b-s-checkbox input').attr('checked', false);
                $('.b-s-checkbox input').prop('disabled', true);
                $('#productDefaultOpeningQuantity').attr('disabled', false);
            } else {
                $('.b-s-checkbox input').prop('disabled', false);
                $('#productDefaultOpeningQuantity').attr('disabled', true);
            }
        }
    });

    $('.product-default-selling-price').on('click', '#productDefaultSellingPrice', function() {
        setUomToSellingPrice();
        $(this).focus().trigger('change');
    });

    $('.product-default-purchase-price').on('click', '#productDefaultPurchasePrice', function() {
        setUomToSellingPrice();
        $(this).focus().trigger('change');
    });

    $('.startingQuantity').on('click', function() {
        setUomToSellingPrice();
        $(this).focus().trigger('change');
    });
    $('.uom-list').on('change', function() {
        setUomToSellingPrice();
    });
    $('.uom-list tbody').on('click', 'tr a.delete', function() {
        setUomToSellingPrice();
    });

    setUomToSellingPrice = function() {
        var uoms = getAllUoms();
        var displayUomValue = getUomsDispaly();
        var uomSizeArray = Object.keys(uoms);
        var uom = new Array();
        var uCFlag = false;
        for (index = 0; index < uomSizeArray.length; index++) {
            i = uomSizeArray[index];
            var displayUom = (uoms[uomSizeArray[index]].displayUom == true) ? 1 : 0;
            if (displayUom == 1) {
                conversionVal = uoms[uomSizeArray[index]].uomConversion;
            }
            var uomData = {
                uA: UOMLISTBYID[uomSizeArray[index]]['uomAbbr'],
                uomID: UOMLISTBYID[uomSizeArray[index]]['uomID'],
                uN: UOMLISTBYID[uomSizeArray[index]]['uomName'],
                uC: uoms[uomSizeArray[index]].uomConversion,
                uDP: UOMLISTBYID[uomSizeArray[index]]['uomDecimalPlace'],
                pUDisplay: displayUom
            };
            if (uoms[uomSizeArray[index]].uomConversion === '1') {
                uCFlag = true;
            }
            uom[index] = uomData;
        }

        if (uCFlag) {
            $('#productDefaultSellingPrice').val($('#productDefaultSellingPrice').val() / conversionVal);
            $('#productDefaultPurchasePrice').val($('#productDefaultPurchasePrice').val() / conversionVal);
            
            uomTriggedFlag  = true;
            
            $('#productDefaultSellingPrice').parent().addClass('input-group');
            $('#productDefaultSellingPrice').siblings('.uomPrice').remove();
            $('#productDefaultSellingPrice').addUomPrice(uom);
            $('#productDefaultPurchasePrice').parent().addClass('input-group');
            $('#productDefaultPurchasePrice').siblings('.uomPrice').remove();
            $('#productDefaultPurchasePrice').addUomPrice(uom);
            if ($('#startingQuantity').length) {
                $('#startingQuantity').parent('div').addClass('input-group');
                $('#startingQuantity').siblings('.uomqty').remove();
                $('#startingQuantity').addUom(uom);
            }
        }
    }

});
function updateStatusOfDefalutQuantity() {
    if ($('#productTypeID').val() == 2) {
        $('#productDefaultMinimumInventoryLevel').attr('disabled', true);
        $('#productDefaultMaximunInventoryLevel').attr('disabled', true);
        $('#productDefaultReorderLevel').attr('disabled', true);
    } else {
        $('#productDefaultMinimumInventoryLevel').attr('disabled', false);
        $('#productDefaultMaximunInventoryLevel').attr('disabled', false);
        $('#productDefaultReorderLevel').attr('disabled', false);
    }
}

function validateParams(valid)
{
    var user = valid[0];
    var pass = valid[1];
    if (user == null || user == "") {
        p_notification(false, eb.getMessage('ERR_VIEWPAY_UNAME'));
    } else if (pass == null || pass == "") {
        p_notification(false, eb.getMessage('ERR_VIEWPAY_PWD'));
    } else {
        return true;
    }
}