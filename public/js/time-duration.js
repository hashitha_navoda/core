//this plugin for convert text field to time duration boxes
//the text field time value contain in minutes.
(function($) {
    messageArray = new Object();
    messageArray.ERR_DAYS = "Days should be numeric, integer and positive.";
    messageArray.ERR_HOURS = "Hours should be numeric, integer,positive and cannot be more than or equal to 24.";
    messageArray.ERR_MINUTES = "Minutes should be numeric, integer,positive and cannot be more than or equal to 60.";

    $.fn.addDuration = function() {
        $(this).each(function() {
            var $field = $(this);
            $field.hide();

            var $newFields = $("<table class='job_table table table-condensed'>\n\
                                    <thead>\n\
                                        <th>Days</th>\n\
                                        <th>Hours</th>\n\
                                        <th>Min</th>\n\
                                    </thead>\n\
                                    <tr>\n\
                                        <td><input type='text' class='days form-control' placeholder='Days'></td>\n\
                                        <td><input type='text' class='hours form-control' placeholder='Hours'></td>\n\
                                        <td><input type='text' class='minutes form-control' placeholder='Minutes'></td>\n\
                                    </tr>\n\
                                </table>");
            $field.after($newFields);

            $field.parent('div').find('.days').on('change', function() {
                var days = $(this).val();
                var hours = $field.parent('div').find('.hours').val();
                var minutes = $field.parent('div').find('.minutes').val();
                if (isNaN(days) || days < 0 || days % 1 != 0) {
                    p_notification(false, messageArray.ERR_DAYS);
                    $(this).val('0');
                    days = 0;
                }
                var result = convertToMinutes(days, hours, minutes);
                $field.val(result);
                $field.trigger('click');
            });

            $field.parent('div').find('.hours').on('change', function() {
                var days = $field.parent('div').find('.days').val();
                var hours = $(this).val();
                var minutes = $field.parent('div').find('.minutes').val();
                if (isNaN(hours) || hours > 23 || hours < 0 || hours % 1 != 0) {
                    p_notification(false, messageArray.ERR_HOURS);
                    $(this).val('0');
                    hours = 0;
                }
                var result = convertToMinutes(days, hours, minutes);
                $field.val(result);
                $field.trigger('click');
            });

            $field.parent('div').find('.minutes').on('change', function() {
                var days = $field.parent('div').find('.days').val();
                var hours = $field.parent('div').find('.hours').val();
                var minutes = $(this).val();
                if (isNaN(minutes) || minutes > 60 || minutes < 0 || minutes % 1 != 0) {
                    p_notification(false, messageArray.ERR_MINUTES);
                    $(this).val('0');
                    minutes = 0;
                }
                var result = convertToMinutes(days, hours, minutes);
                $field.val(result);
                $field.trigger('click');
            });

            $field.change(function() {
                var durationTime = $(this).val();
                var less = 0;
                var days = parseInt(durationTime / (60 * 24));
                less = durationTime % (60 * 24);
                var hours = parseInt(less / 60);
                less = less % 60;
                var minutes = less;

                $field.parent('div').find('.days').val(days);
                $field.parent('div').find('.hours').val(hours);
                $field.parent('div').find('.minutes').val(minutes);

            });

            if ($field.val()) {
                $field.trigger('change');
            }
        });

        function convertToMinutes(days, hours, minutes) {
            var result = (days * 24 * 60) + (hours * 60) + (minutes * 1);
            return result;
        }
    };

}(jQuery));