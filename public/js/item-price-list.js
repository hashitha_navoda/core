/**
 * @author ASHAN <ashan@thinkcube.com>
 * This file contains item price list js fuction
 */

var allProducts = {};

$(document).ready(function() {

    var editLocations = [];
    var priceListId = 0;

    function itemPriceList() {
        this.productID = 0;
        this.productPrice = 0;
    }

    $('#locations').attr('data-live-search', 'true').selectpicker();
    $('#locations').change(function() {
        if ($(this).val() == 0) {
            $('#itemPriceList').addClass('hidden');
            return false;
        }
        $('#tbl_data').empty();
        callProducts($('#locations').val());

    });

    function callProducts(locationIDs) {
        $('#itemPriceList').removeClass('hidden');
        var productLocation = BASE_URL + '/productAPI/getProductsByLocationIds';
        eb.ajax({
            url: productLocation,
            type: 'POST',
            data: {
                locationID: locationIDs
            },
            dataType: 'json',
            async: false,
            success: function(res) {
                if (res.status) {
                    for (var j in res.data) {
                        var thisProduct = res.data[j];
                        var productCode = (thisProduct.productCode !== null) ? thisProduct.productCode : '-';
                        var productName = (thisProduct.productName !== null) ? thisProduct.productName : '-';

                        if (thisProduct.productCode !== null || thisProduct.productName !== null) {
                            var $tr = $("<tr data-id=\'" + thisProduct.productID + "'"
                                    + "data-code = '"+ productCode + "'"
                                    + "data-name = '"+ productName + "'" + ">" +
                                    "<td class='productCode'>" + productCode + "</td>" +
                                    "<td class='productName'>" + productName + "</td>" +
                                    "<td><input type='text' name='itemPrice' value='0.00' class='form-control'></td>\n\
                                    <td><select name='itemDiscountType' class='form-control'><option value='2'>Percentage</option><option value='1'>Value</option></select></td>\n\
                                    <td><input type='text' name='itemDiscount' value='0.00' class='form-control'></td>" +
                                    "</tr>");
                            $('#tbl_data').append($tr);
                        }
                    }
                } else {
                    p_notification(res.status, res.msg);
                }
            }
        });
    }

    var $locationProductForm = $("form[name='item-price-list-form']");

    function validateItemPriceListDetails() {
        var success = true;
        if ($("[name='productGlobalProduct']:checked").val() == 0 && $('#locations').val() == null) {
            p_notification(false, eb.getMessage('ERR_ITEM_PRICE_LIST_LOCATION'));
            success = false;
            $('#itemPriceListName').focus();
            return false;
        }

        if (success && $('#itemPriceListName').val() == '') {
            p_notification(false, eb.getMessage('ERR_ITEM_PRICE_LIST_NAME'));
            success = false;
            $('#itemPriceListName').focus();
            return false;
        }

        if (success) {
            var count = 0;
            $('#tbl_data tr').each(function() {
                var itemPrice = parseFloat($(this).find("[name='itemPrice']").val());
                var itemDiscountType = parseFloat($(this).find("[name='itemDiscountType']").val());
                var itemDiscount = parseFloat($(this).find("[name='itemDiscount']").val());
                var regEx = /^[+]?([0-9]+(?:[\.][0-9]*)?|\.[0-9]+)$/;

                if (!regEx.test(itemPrice)) {
                    p_notification(false, eb.getMessage('ERR_ITEM_PRICE_LIST_LOCATPROD_FIELD'));
                    $(this).find("[name='itemPrice']").focus();
                    success = false;
                    return false;
                } else if (!regEx.test(itemDiscount)) {
                    p_notification(false, eb.getMessage('ERR_ITEM_PRICE_LIST_DISCOUNT_FIELD'));
                    $(this).find("[name='itemDiscount']").focus();
                    success = false;
                    return false;
                }
                else if (itemDiscountType == '1' && itemPrice < itemDiscount) {
                    p_notification(false, eb.getMessage('ERR_ITEM_PRICE_LIST_DISCOUNT_VALUE'));
                    $(this).find("[name='itemDiscount']").focus();
                    success = false;
                    return false;
                } else if (itemDiscountType == '2' && itemDiscount > parseFloat(100)) {
                    p_notification(false, eb.getMessage('ERR_ITEM_PRICE_LIST_DISCOUNT_PRECENTAGE'));
                    $(this).find("[name='itemDiscount']").focus();
                    success = false;
                    return false;
                } else {
                    count++;
                }
            });
            if (count == 0) {
                p_notification(false, eb.getMessage('ERR_ITEM_PRICE_LIST_ENTER_ONE_PRICE'));
                success = false;
            }
        }
        return success;
    }

    $locationProductForm.on('submit', function(e) {
        e.preventDefault();
        var submit = $('#save').val();

        // validate all inputs
        // error message is returned from the function
        if (!validateItemPriceListDetails()) {
            e.stopPropagation();
            return false;
        }

        $("table.item-price-list-table tbody tr", $locationProductForm).each(function() {
            var productID = $(this).data('id');
            allProducts[productID] = new itemPriceList();
            allProducts[productID].productID = productID;
            allProducts[productID].productPrice = parseFloat($("[name='itemPrice']", $(this)).val());
            allProducts[productID].productDiscountType = $("[name='itemDiscountType']", $(this)).val();
            allProducts[productID].productDiscount = parseFloat($("[name='itemDiscount']", $(this)).val());
        });

        var locations = [];
        var priceListGlobal = 0;
        var addAllProduct = BASE_URL + '/productAPI/saveItemPriceList';
        if (submit == "Save") {
            if ($("[name='productGlobalProduct']:checked").val() == 1) {
                $('#locations').find('option').each(function() {
                    if ($(this).val() > 0) {
                        locations.push($(this).val());
                    }
                });
                priceListGlobal = 1;
            } else {
                locations = $('#locations').val();
            }
        } else {
            locations = editLocations;
            addAllProduct = BASE_URL + '/productAPI/updateItemPriceList';
        }
        eb.ajax({
            url: addAllProduct,
            type: 'POST',
            data: {
                locationIDs: locations,
                productData: JSON.stringify(allProducts),
                priceListName: $('#itemPriceListName').val(),
                priceListGlobal: priceListGlobal,
                priceListId: priceListId
            },
            dataType: 'json',
            async: false,
            success: function(data) {
                p_notification(data.status, data.msg);
                if (data.status) {
                    window.location.reload();
                }
            }
        });

        e.stopPropagation();
        return false;
    });

    $("[name='productGlobalProduct']").on('change', function(e) {
        e.preventDefault();
        var loc = [];
        $('#locations').find('option').each(function() {
            if ($(this).val() > 0) {
                loc.push($(this).val());
            }
        });
        $('.locationDiv').show();
        $('#tbl_data').empty();
        $('#itemPriceList').addClass('hidden');
        if ($(this).val() == 1) {
            $('#itemPriceList').removeClass('hidden');
            callProducts(loc);
            $('.locationDiv').hide();
        }
    });

    $('.editPriceList', '#price_list_data').on('click', function() {
        priceListId = $(this).parents('tr').data('id');
        $('#itemPriceList').removeClass('hidden');
        var URL = BASE_URL + '/productAPI/getPriceListDetailsByPriceListId';
        eb.ajax({
            url: URL,
            type: 'POST',
            data: {
                priceListId: priceListId
            },
            dataType: 'json',
            async: false,
            success: function(res) {
                if (res.status) {
                    var priceListGlobal = 0;
                    var priceListName;

                    priceListLocationData = res.data['priceListLocationsData'];
                    for (var i in priceListLocationData) {
                        var thisProduct = priceListLocationData[i];
                        priceListGlobal = thisProduct.priceListGlobal;
                        priceListName = thisProduct.priceListName;
                        editLocations.push(thisProduct.locationId);
                    }

                    if (priceListGlobal == 1) {
                        $('#globalItem').attr('checked', true);
                        $('.locationDiv').hide();
                    } else {
                        $('#locationItem').attr('checked', true);
                        $('#locations').val(editLocations).attr('disabled', true).selectpicker('refresh');
                    }
                    $("[name='productGlobalProduct']").attr('disabled', true);
                    $("#itemPriceListName").val(priceListName).attr('disabled', true);
                    $('#save').attr('value', 'Update');
                    $('#tbl_data > tr').remove();

                    priceListItemsData = res.data['priceListItemsData'];
                    for (var j in priceListItemsData) {
                        var thisProduct = priceListItemsData[j];
                        var productCode = (thisProduct.productCode != undefined) ? thisProduct.productCode : '-';
                        var productName = (thisProduct.productName != undefined) ? thisProduct.productName : '-';
                        var productId = (thisProduct.productId != undefined) ? thisProduct.productId : thisProduct.productID;
                        var productDiscountType = (thisProduct.priceListItemsDiscountType != undefined) ? thisProduct.priceListItemsDiscountType : '-';
                        var productDiscount = (thisProduct.priceListItemsDiscount != undefined) ? thisProduct.priceListItemsDiscount : '0.00';
                        var productPrice = (thisProduct.priceListItemsPrice != undefined) ? thisProduct.priceListItemsPrice : '0.00';
                        if (thisProduct.productCode !== null || thisProduct.productName !== null) {
                            var element = "<select name='itemDiscountType' class='form-control'><option value='2' selected>Percentage</option><option value='1'>Value</option></select>";;
                            if (productDiscountType == '1') {
                                element = "<select name='itemDiscountType' class='form-control'><option value='2' >Percentage</option><option value='1' selected>Value</option></select>";
                            }                           
                            
                            var $tr = $("<tr data-id=\'" + productId + "'"
                                    + "data-code = '"+ productCode + "'"
                                    + "data-name = '"+ productName + "'" + ">" +
                                    "<td class='productCode'>" + productCode + "</td>" +
                                    "<td class='productName'>" + productName + "</td>" +
                                    "<td><input type='text' name='itemPrice' value='" + accounting.formatMoney(productPrice, null, getDecimalPlaces(productPrice), '') + "' class='form-control'></td>\n\
                                    <td> "+ element +" </td>\n\
                                    <td><input type='text' name='itemDiscount' value='"+parseFloat(productDiscount).toFixed(2)+"' class='form-control'></td>" +
                                    "</tr>");
                            $('#tbl_data').append($tr);
                        }
                    }
                } else {
                    p_notification(res.status, res.msg);
                }
            }
        });
    });

    $('#cancel').on('click', function() {
        clearePriceList();
    });

    function clearePriceList() {
        $('.locationDiv').show();
        $('#locationItem').attr('checked', true);
        $('#locations').val('').attr('disabled', true).selectpicker('refresh');
        $("[name='productGlobalProduct']").attr('disabled', false);
        $("#itemPriceListName").val('').attr('disabled', false);
        $('#save').attr('value', 'Save');
        $('#tbl_data').empty();
        $('#itemPriceList').addClass('hidden');
        editLocations = [];
        priceListId = 0;
    }

    $('.inacProductList,.acProductList').on('click', function() {
        var state = ($(this).hasClass('inacProductList')) ? 0 : 1;
        var msg = ($(this).hasClass('inacProductList')) ? "inactive" : "active";
        var prlid = $(this).parents('tr').data('id');
        var inactiveProductList = BASE_URL + '/productAPI/activeInactivePriceList';
        bootbox.confirm("Are you sure you want to " + msg + " this price list?", function(result) {
            if (result === true) {
                eb.ajax({
                    url: inactiveProductList,
                    type: 'POST',
                    data: {
                        priceListId: prlid,
                        priceListState: state,
                    },
                    dataType: 'json',
                    async: false,
                    success: function(data) {
                        p_notification(data.status, data.msg);
                        if (data.status) {
                            window.location.reload();
                        }
                    }
                });
            }
        });
    });

    $('.deleteProductList').on('click', function() {
        var prlid = $(this).parents('tr').data('id');
        var deleteProductList = BASE_URL + '/productAPI/deletePriceList';
        bootbox.confirm("Are you sure you want to delete this price list?", function(result) {
            if (result === true) {
                eb.ajax({
                    url: deleteProductList,
                    type: 'POST',
                    data: {
                        priceListId: prlid,
                    },
                    dataType: 'json',
                    async: false,
                    success: function(data) {
                        p_notification(data.status, data.msg);
                        if (data.status) {
                            window.location.reload();
                        }
                    }
                });
            }
        });
    });

    $('#item-search').on('keyup paste', function(e) {
        if (e.type == 'paste') {
            setTimeout(function () {
                var string = $('#item-search').val().toString().toLowerCase();
                itemSearch(string);
            }, 100);
        } else {
            var string = $(this).val().toString().toLowerCase();
            itemSearch(string);
        }
    });

    $('#search-clr-btn').on('click', function () {
        $('#item-search').val('');
        $('#item-search').trigger('keyup');
    });

    function itemSearch(string) {
        var recordFlag = false;

        $('.no-result-tr').remove(); //remove no result row
        $('#tbl_data tr').each(function (index, element){
            var itemCode = $(this).data('code').toString().toLowerCase();
            var itemName = $(this).data('name').toString().toLowerCase();

            if (itemCode.search(string) < 0 && itemName.search(string) < 0) {
                $(this).addClass('hidden');
            } else {
                $(this).removeClass('hidden');
                recordFlag = true;
            }
        });
        if(recordFlag === false) {//for add no result row
            $('#tbl_data').append("<tr class='not_found no-result-tr'><td colspan='3'>No results were found.</td></tr>");
        }
    }
});

