/*  @author Yashora Jayamanne <yashora@thinkcube.com>
 * common js file to preview document
 */

var documentPreview;

documentPreview = function(url, iframe, path, additional) {

    //$('.selectpicker').selectpicker();
    var preview = eb.post(url);
    preview.done(function(resp) {
        $('.selectpicker').selectpicker();
        var $preview = $("#preview");

        $(".modal-dialog", $preview).html(resp);
        $('#preview').modal({
            backdrop: 'static',
            keyboard: false
        })
        $("#preview").modal('show');
        setTimeout(function() {
            $preview.scrollTop(0);
        }, 200);

        $("#templateID").on("change", function() {
            var $iframe = $('#' + iframe, $preview);
            $iframe.attr('src', $iframe.data('origsrc') + '/' + $(this).val());
        });

        $("#document_print", $preview).on("click", function() {
            printIframe(iframe);
        });

        $("#document_email", $preview).on("click", function () {
            $("#email-modal").modal("show");
        });

        $("#document_sms", $preview).on("click", function () {
            $("#sms-modal").modal("show");
        });

        $("#send", $preview).off('click').on("click", function() {
            // prevent doubleclick
            if ($(this).hasClass('disabled')) {
                return false;
            }
            var state = validateEmail($("#email_to", $preview).val(), $("#email-body", $preview).html());
            if (state) {
                var param = {
                    documentID: $(this).data("docid"), //$(this).data("so_id"),
                    to_email: $("#email_to", $preview).val(),
                    subject: $("#email_sub", $preview).val(),
                    body: $(".email-body", $preview).html(),
                    templateID: $('#templateID').val()
                };

                var mail = eb.post(path, param); ///<-
                mail.done(function(rep) {
                    p_notification(rep.status, rep.msg);

                    $("#email-modal button.btn", $preview).removeClass('disabled')
                    $("#email-modal", $preview).modal("hide");
                });
                $("#email-modal button.btn", $preview).addClass('disabled')
            }
        });

        $("#sendSms", $preview).off("click").on("click", function () {
            // prevent doubleclick
            if ($(this).hasClass("disabled")) {
            return false;
            }
            var regex = /<br\s*[\/]?>/gi;
            var formData = {
                telephoneNumber: $("#sms_to", $preview).val(),
                smsBody: $("#sms_body", $preview).html().replace(regex, "\n"),
            };  
            eb.ajax({
                type: "POST",
                url: BASE_URL + "/invoice-api/sendSms",
                data: formData,
                success: function (respond) {
                    if (respond.status) {
                        p_notification(true, respond.msg);
                        $("#sms-modal", $preview).modal("hide");
                    } else {
                        p_notification(false, respond.msg);
                        $("#sms-modal", $preview).modal("hide");
                    }
                },
            });
        });

		$('#journalEntry', $preview).on('click',function(){
			$("#journal-entry-modal", $preview).modal("show");
		});

        $("button.email-modal-close", $preview).on("click", function() {
            $("#email-modal", $preview).modal("hide");
        });

        $("button.sms-modal-close", $preview).on("click", function () {
          $("#sms-modal", $preview).modal("hide");
        });

        $("#cancel", $preview).on("click", function() {
            $preview.modal('hide');
        });

        if (additional !== undefined) {
            var result = additional($preview);
        }

    });
}