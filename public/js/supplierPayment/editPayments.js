
/**
 * @author ASHAN <ashan@thinkcube.com>
 * This file contains form validation
 */


$(document).ready(function() {
    var methods = [];
    var addpayment = BASE_URL + '/supplierPaymentsAPI/editPaymentMethods';

    $('.payMethod').each(function() {
        var bankID = '';
        var accountID = '';
        var bankdiv = '';
        var methodId = $(this).data('method');
        if (methodId == 5) {
            bankID = $(this).find('.bankID').val();
            accountID = $(this).find('.selectAccountID').val();
            bankdiv = $(this).find('#bankTransferGroup');
            if (bankID) {
                $(this).find('.bankID').attr('disabled', true);
                $('.main_div').addClass('Ajaxloading');
                //
            } else {
                bankdiv.find('.accountID').attr('disabled', true);
            }
        } else if (methodId == 2) {
            bankID = $(this).find('.chbankID').val();
            accountID = $(this).find('.selectCheckAccountID').val();
            bankdiv = $(this).find('#checkGroup');
            if (bankID) {
                $(this).find('.chbankID').attr('disabled', true);
                $('.main_div').addClass('Ajaxloading');
                //
            } else {
                bankdiv.find('.checkAccountID').attr('disabled', true);
            }
        }
        if (bankID) {
            eb.ajax({
                type: 'post',
                url: BASE_URL + '/account-api/bankAccountListForDropdown',
                data: {
                    bankId: bankID,
                },
                dataType: "json",
                async: false,
                success: function(data) {
                    if (data['status'] == true) {
                        if ($.isEmptyObject(data['data'].list)) {
                            p_notification(false, eb.getMessage('ERR_DOESNT_HAVE_ANY_ACC_FOR_THIS_BANK', bankName));
                            if (methodId == 5) {
                                $(this).find('.bankID').val('');
                                bankdiv.find('.accountID').attr('disabled', true);
                            } else if (methodId == 2) {
                                $(this).find('.chbankID').val('');
                                bankdiv.find('.checkAccountID').attr('disabled', true);
                            }
                        } else {
                            $.each(data['data'].list, function(key, value) {
                                var option = "<option value=" + value.value + ">" + value.text + "</option>";
                                if (methodId == 5) {
                                    bankdiv.find('.accountID').attr('disabled', false);
                                    bankdiv.find('.accountID').append(option);
                                } else if (methodId == 2) {
                                    bankdiv.find('.checkAccountID').attr('disabled', false);
                                    bankdiv.find('.checkAccountID').append(option);
                                }
                            });
                            if (accountID != '') {
                                if (methodId == 5) {
                                    bankdiv.find('.accountID').val(accountID).attr('disabled', true);
                                } else if (methodId == 2) {
                                    bankdiv.find('.checkAccountID').val(accountID).attr('disabled', true);
                                }
                            }
                        }
                    }
                    $('.main_div').removeClass('Ajaxloading');
                }
                ,
            });
        }
    });

    $('#editPayment').on('submit', function(e) {
        e.preventDefault();
        if (validate_form()) {
            eb.ajax({
                type: 'post',
                url: addpayment,
                data: {
                    paymentMethods: methods,
                    paymentId: $('#paymentID').val(),
                    paymentComment: $('#paymentComment').val(),
                },
                dataType: "json",
                success: function(data) {
                    if (data.status == true) {
                        p_notification(data.status, data.msg);
                        window.location.assign(BASE_URL + '/supplierPayments/view');
                    }
                },
            });
        }

    });

    function validate_form() {

        var checked = true;
        methods = [];
        $('.payMethod').each(function() {
            var methodID = $(this).data('method');
            var methodTypeID = $(this).data('methodid');
            var paidAmount = $('.paidAmount', this).val();
            var checquenumber = $('.checquenumber', this).val();
            var chbankID = $('.chbankID', this).val();
            var checkAccountID = $('.checkAccountID', this).val();
            var reciptnumber = $('.reciptnumber', this).val();
            var cardnumber = $('.cardnumber', this).val();
            var bankID = $('.bankID', this).val();
            var accountID = $('.accountID', this).val();
            var supplierBank = $('.supplierBank', this).val();
            var supplierAccountNumber = $('.supplierAccountNumber', this).val();
            var giftCardID = $('.giftCardID', this).val();

            methods.push({
                methodID: methodID,
                methodTypeID: methodTypeID,
                paidAmount: paidAmount,
                checkNumber: checquenumber,
                checkBankId: chbankID,
                checkAccountId: checkAccountID,
                reciptnumber: reciptnumber,
                cardnumber: cardnumber,
                bankID: bankID,
                accountID: accountID,
                supplierBank: supplierBank,
                supplierAccountNumber: supplierAccountNumber,
                giftCardID: giftCardID,
            });
            if (methodID == 2) {
                if (checquenumber != "" && checquenumber.length > 8) {
                    p_notification(false, eb.getMessage('ERR_PAY_CHEQUE_INVALID'));
                    $('.checquenumber', this).focus();
                    checked = false;
                    return false;
                } else if (checquenumber != "" && isNaN(checquenumber)) {
                    p_notification(false, eb.getMessage('ERR_ADVPAY_CHEQUE_NUMR'));
                    $('.checquenumber', this).focus();
                    checked = false;
                    return false;
                }
            } else if (methodID == 3) {
                if (reciptnumber != "" && reciptnumber.length > 12) {
                    p_notification(false, eb.getMessage('ERR_PAY_RECIPTNO_VALIDITY'));
                    $('.reciptnumber', this).focus();
                    checked = false;
                    return false;
                } else if (cardnumber != "" && isNaN(cardnumber)) {
                    p_notification(false, eb.getMessage('ERR_ADVPAY_CRDTCARD_NUMR'));
                    $('.cardnumber', this).focus();
                    checked = false;
                    return false;
                }
            } else if (methodID == 5) {
                if (supplierAccountNumber != '' && isNaN(supplierAccountNumber)) {
                    p_notification(false, eb.getMessage('ERR_PAY_METHOD_CUST_ACCUNT'));
                    $('.supplierAccountNumber', this).focus();
                    checked = false;
                    return false;
                }
            } else if (methodID == 6) {
                // if (giftCardID == null || giftCardID == "") {
                //     p_notification(false, eb.getMessage('ERR_PAY_METHOD_GIFT_CARD_SH_SELECT'));
                //     $('.giftCardID', this).focus();
                //     checked = false;
                //     return false;
                // }
            }
        });
        if (checked) {
            return true;
        } else {
            return false;
        }
    }

    $(document).on('change', '.bankID', function() {
        var bankdiv = $(this).parents('#bankTransferGroup');
        var bankID = $(this).val();
        var bank = $(this);
        var bankName = $("option:selected", $(this)).text();
        bankdiv.find('.accountID').find('option').each(function() {
            if ($(this).val()) {
                $(this).remove();
            }
        });
        if (bankID) {
            $('.main_div').addClass('Ajaxloading');
            eb.ajax({
                type: 'post',
                url: BASE_URL + '/account-api/bankAccountListForDropdown',
                data: {
                    bankId: bankID,
                },
                dataType: "json",
                async: false,
                success: function(data) {
                    if (data['status'] == true) {
                        if ($.isEmptyObject(data['data'].list)) {
                            p_notification(false, eb.getMessage('ERR_DOESNT_HAVE_ANY_ACC_FOR_THIS_BANK', bankName));
                            $(bank).val('');
                            bankdiv.find('.accountID').attr('disabled', true);
                        } else {
                            $.each(data['data'].list, function(key, value) {
                                var option = "<option value=" + value.value + ">" + value.text + "</option>";
                                bankdiv.find('.accountID').attr('disabled', false);
                                bankdiv.find('.accountID').append(option);
                            });
                        }
                    }
                    $('.main_div').removeClass('Ajaxloading');
                },
            });
        } else {
            bankdiv.find('.accountID').attr('disabled', true);
        }
    });

    $(document).on('change', '.chbankID', function() {
        var bankdiv = $(this).parents('#checkGroup');
        var bankID = $(this).val();
        var bank = $(this);
        var bankName = $("option:selected", $(this)).text();
        bankdiv.find('.checkAccountID').find('option').each(function() {
            if ($(this).val()) {
                $(this).remove();
            }
        });
        if (bankID) {
            $('.main_div').addClass('Ajaxloading');
            eb.ajax({
                type: 'post',
                url: BASE_URL + '/account-api/bankAccountListForDropdown',
                data: {
                    bankId: bankID,
                },
                dataType: "json",
                async: false,
                success: function(data) {
                    if (data['status'] == true) {
                        if ($.isEmptyObject(data['data'].list)) {
                            p_notification(false, eb.getMessage('ERR_DOESNT_HAVE_ANY_ACC_FOR_THIS_BANK', bankName));
                            $(bank).val('');
                            bankdiv.find('.checkAccountID').attr('disabled', true);
                        } else {
                            $.each(data['data'].list, function(key, value) {
                                var option = "<option value=" + value.value + ">" + value.text + "</option>";
                                bankdiv.find('.checkAccountID').attr('disabled', false);
                                bankdiv.find('.checkAccountID').append(option);
                            });
                        }
                    }
                    $('.main_div').removeClass('Ajaxloading');
                },
            });
        } else {
            bankdiv.find('.checkAccountID').attr('disabled', true);
        }
    });

});
