var tmptotal;
var paymentID;
var supplierID;

var loadPaymentsPreview;
var ignoreBudgetLimitFlag = false; 

$(document).ready(function () {

    if (!$('#filter-button').length) {
        return false;
    }

    var whichPayment = $('#whichPayment').val();
    $('.deli_row').hide();
    $('#deli_form_group').hide();
    $('#tax_td').hide();
    $('#pay-sup-search').selectpicker('hide');
    $('#view-div').hide();
    $('#supdiv').hide();
    $('#back-pay-cust').hide();
    $('#button-print').hide();
    $('#button-email').hide();
    $('#button-print').on('click', function () {
        printdiv('view-div');
    });
    $('#back-pay-cust').on('click', function () {
        $('#view-div').hide();
        $('#supdiv').show();
        $('#back-pay-cust').hide();
        $('#button-print').hide();
    });

    $('#pay-search-select').on('change', function () {
        if ($(this).val() == 'Payments No') {
            $('#pay-sup-search').selectpicker('hide');
            $('#pay-search').selectpicker('show');
            $('#pay-sup-search').val('');
            $('#pay-sup-search').selectpicker('render');
            $('#view-div').hide();
            $('#supdiv').hide();
            $('#back-pay-cust').hide();
            $('#button-print').hide();
            $('#paymentsview').show();
            $('#button-email').hide();
        } else if ($(this).val() == 'Supplier Name') {
            $('#pay-search').selectpicker('hide');
            $('#pay-sup-search').selectpicker('show');
            $('#pay-search').val('');
            $('#pay-search').selectpicker('render');
            $('#view-div').hide();
            $('#supdiv').hide();
            $('#back-pay-cust').hide();
            $('#button-print').hide();
            $('#paymentsview').show();
            $('#button-email').hide();
        }
    });

    loadDropDownFromDatabase('/supplierPaymentsAPI/search-supplier-payments-dropdown', "", whichPayment, '#pay-search');
    $('#pay-search').selectpicker('refresh');

    $('#pay-search').on('change', function () {
        if ($(this).val() > 0 && $(this).val() != paymentID) {
            paymentID = $(this).val();
            $('#view-div').hide();
            $('#paymentsview').hide();
            $('#button-email').hide();
            $('#pay-cust-view').html('');
            var getpaybycusturl = '/supplierPaymentsAPI/RetrivePayments';
            var requestquobycust = eb.post(getpaybycusturl, {
                PaymentID: paymentID,
                PaymentCode : $(this).find("option:selected").text(),
            });
            requestquobycust.done(function (retdata) {
                $('#suppPaymentsview').html(retdata);
            });
        }
    });

    loadDropDownFromDatabase('/supplierAPI/search-suppliers-for-dropdown', "", 0, '#pay-sup-search');
    $('#pay-sup-search').selectpicker('refresh');

    $('#pay-sup-search').on('change', function () {
        if ($(this).val() > 0 && $(this).val() != supplierID) {
            supplierID = $(this).val();
            $('#supdiv').hide();
            $('#view-div').hide();
            $('#paymentsview').hide();
            $('#button-email').hide();
            $('#pay-cust-view').html('');
            var getpaybycusturl = '/supplierPaymentsAPI/RetriveSupplierPayments';
            var requestquobycust = eb.post(getpaybycusturl, {
                supplierID: supplierID,
                WhichPayment: whichPayment,
                supplierCode : $(this).find("option:selected").text(),
            });
            requestquobycust.done(function (retdata) {
                $('#suppPaymentsview').html(retdata);
            });
        }
    });

    $('#clear-search').on('click', function () {
        var requestquobycust = eb.post('/supplierPayments/getSupplierPaymentsList', {'whichPayment': whichPayment});
        requestquobycust.done(function (retdata) {
            if (retdata.status == false) {
                p_notification(false, retdata.msg);
            } else {
                $('#suppPaymentsview').html(retdata);
                $('#from-date,#to-date').val('');
                if ($('#pay-search-select').val() == 'Payments No') {
                    $("#pay-search").val('').trigger('change');
                    $('#pay-search').empty().selectpicker('refresh')
                } else {
                    $("#pay-sup-search").val('').trigger('change');
                    $('#pay-sup-search').empty().selectpicker('refresh');
                }
                supplierID = '';
                paymentID = '';
            }

        });
    });

    function printdiv(divID) {
        var DocumentContainer = document.getElementById(divID);
        var WindowObject = window.open('', 'PrintWindow', 'width=750,height=650,top=50,left=50,toolbars=no,scrollbars=yes,status=no,resizable=yes');

        WindowObject.document.writeln('<!DOCTYPE html>');
        WindowObject.document.writeln('<html><head><title></title>');
        WindowObject.document.writeln('<link rel="stylesheet" type="text/css" href="/assets/bootstrap/css/bootstrap.min.css">');
        WindowObject.document.writeln('</head><body>');
        WindowObject.document.writeln(DocumentContainer.innerHTML);
        WindowObject.document.writeln('</body></html>');

        WindowObject.document.close();
        WindowObject.focus();
        WindowObject.print();
        WindowObject.close();

    }

    $('#filter-button').on('click', function () {
        if ($('#from-date').val() == '' || $('#to-date').val() == '') {
            p_notification(false, eb.getMessage('ERR_VIEWPAY_FILLDATA'));
        } else {
            var paymentfilter = BASE_URL + '/supplierPaymentsAPI/getPaymentsByDatefilter';
            var filterrequest = eb.post(paymentfilter, {
                'fromdate': $('#from-date').val(),
                'todate': $('#to-date').val(),
                'supplierID': supplierID,
                'whichPayment': whichPayment
            });
            filterrequest.done(function (retdata) {
                if (retdata.msg == 'nopayment') {
                    p_notification(false, eb.getMessage('ERR_VIEWPAY_NORANGE'));
                    $('#supdiv').html('');
                } //else {
                $('.deli_row').hide();
                $('#paymentsview').hide();
                $('#deli_form_group').hide();
                $('#tax_td').hide();
                $('#view-div').hide();
                $('#back-pay-cust').hide();
                $('#button-print').hide();
                $('#button-email').hide();
                $('#suppPaymentsview').html(retdata);
                //  }
            });
        }
    });

    $(document).on('click', '.del', function () {
        var input = $(this).attr('id');
        wrongPayment(input);
    });

    $(document).on('click', '.doc_attachments', function () {
        setDataToAttachmentViewModal($(this).attr('data-pay-related-id'));
        $('#viewAttachmentModal').modal('show');
    });

    function validate_data(valid) {
        var user = valid[0];
        var pass = valid[1];
        if (user == null || user == "") {
            p_notification(false, eb.getMessage('ERR_VIEWPAY_UNAME'));
        } else if (pass == null || pass == "") {
            p_notification(false, eb.getMessage('ERR_VIEWPAY_PWD'));
        } else {
            return true;
        }
    }

    function wrongPayment(payno) {

        $('#delete-payment-button').off('click').on('click', function (e) {
            e.preventDefault();
            deletePayment(payno);
        });
    }

    $(document).on('click', '.pay_related_docs_in_search', function() {
        setDataToHistoryModal($(this).attr('data-pay-related-id'));
        $('#addDocHistoryModal').modal('show');
    });

    function setDataToHistoryModal(payID) {
        $('#doc-history-table tbody tr').remove();
        $('#doc-history-table tfoot div').remove();
        $('#doc-history-table tbody div').remove();

        eb.ajax({
            type: 'POST',
            url: BASE_URL + '/supplierPaymentsAPI/getAllRelatedDocumentDetailsByPaymentID',
            data: {payID: payID},
            success: function(respond) {
                if (respond.status == true) {
                    $('#doc-history-table thead tr').removeClass('hidden');
                    $.each(respond.data, function(index, value) {
                        if (value != null) {
                            $.each(value, function(index2, value2) {
                                tableBody = "<tr data-docId=" + value2['documentID'] + " data-doctype=" + value2['type'] + "><td class='' >" + value2['type'] + "</td><td>" + value2['code'] + "</td><td>" + value2['amount'] + "</td><td>" + value2['issuedDate'] + "</td><td class='text-center'><span class='glyphicon glyphicon-eye-open openlist link_color document_view ' style='cursor:pointer'></span></td></tr>";
                                $('#doc-history-table tbody').append(tableBody);
                            });
                        }
                    });
                    var footer = " <div class='col-lg-12 col-md-12 col-sm-12 text-right'><button type='button' class='btn btn-primary col-lg-12 col-md-12 col-sm-12' data-dismiss='modal' aril-hidden='true'>Back</button></div>";
                    $('#doc-history-table tfoot').append(footer);
                } else {
                    $('#doc-history-table thead tr').addClass('hidden');
                    var noDataFooter = "<div class= ' col-lg-9 text-right'><label><b>No Data</b></label>";
                    $('#doc-history-table tbody').append(noDataFooter);
                }
            }
        });
    }

    $('#doc-history-table').on('click', '.document_view', function() {
        var documentId = $(this).parents('tr').attr('data-docid');
        var documentType = $(this).parents('tr').attr('data-doctype');
        $('#DocumentViewModal').modal('show');
        getPrintPreviewByDocumentTypeAndDocumentID(documentId, documentType);

    });

    function getPrintPreviewByDocumentTypeAndDocumentID(documentID, documentType) {
        var $iframe = $('#related-document-view');
        $iframe.ready(function() {
            $iframe.contents().find("body div").remove();
        });
        var URL;
        if (documentType == 'PurchaseOrder') {
            URL = BASE_URL + '/po/document/' + documentID;
        } else if (documentType == 'Grn') {
            URL = BASE_URL + '/grn/document/' + documentID;
        }
        else if (documentType == 'PurchaseInvoice') {
            URL = BASE_URL + '/pi/document/' + documentID;
        }
        else if (documentType == 'PurchaseReturn') {
            URL = BASE_URL + '/pr/document/' + documentID;
        }
        else if (documentType == 'DebitNote') {
            URL = BASE_URL + '/debit-note/document/' + documentID;
        }
        else if (documentType == 'SupplierPayment') {
            URL = BASE_URL + '/supplierPayments/document/' + documentID;
        }
        else if (documentType == 'DebitNotePayment') {
            URL = BASE_URL + '/debit-note-payments/document/' + documentID;
        } else if (documentType == 'PaymentVoucher') {
            URL = BASE_URL + '/expense-purchase-invoice/document/' + documentID;
        }

        eb.ajax({
            type: 'POST',
            url: URL,
            success: function(respond) {
                var division = "<div></div>";
                $iframe.ready(function() {
                    $iframe.contents().find("body").append(division);
                });
                $iframe.ready(function() {
                    $iframe.contents().find("body div").append(respond);
                });
            }
        });

    }


    function deletePayment(payno)
    {
        var valid = new Array(
                $('#username').val(),
                $('#password').val()
                );
        if (validate_data(valid)) {
//                $('#button-email').show();
//                $("#form_rwo").html('');
            var username = $("#username").val();
            var password = $('#password').val();
            var comment = $('#comment').val();
//                $('.main_div').addClass('Ajaxloading');
            var getpayurl = BASE_URL + '/supplierPaymentsAPI/deletePayment';
            var getpayrequest = eb.post(getpayurl, {
                payno: payno,
                username: username,
                password: password,
                outgoingPaymentCancelMessage: comment,
                ignoreBudgetLimit : ignoreBudgetLimitFlag
            });
            getpayrequest.done(function (data) {
                if (data[0] == true) {
                    p_notification(true, eb.getMessage('SUCC_VIEWPAY_DELETE_PAY'));
                    setTimeout(function () {
                        window.location.reload();
                    }, 1000);
                } else if (data[0] == 'chengeCredit') {
                    p_notification(false, eb.getMessage('ERR_VIEWPAY_SUPADVPAY'));
                } else if (data[0] == 'admin') {
                    p_notification(false, eb.getMessage('ERR_VIEWPAY_USER_PRIVILEGE'));
                } else if (data[0] == 'user' || data[0] == 'pass') {
                    p_notification(false, eb.getMessage('ERR_VIEWPAY_INVALID_USRNAME_PWD'));
                } else if (data[0] == false) {
                    if (data.length == 3) {
                        if (data[1] == "NotifyBudgetLimit") {
                            bootbox.confirm(data[2]+' ,Are you sure you want to cancel ?', function(result) {
                                if (result == true) {
                                    ignoreBudgetLimitFlag = true;
                                    deletePayment(payno);
                                } else {
                                    setTimeout(function(){ 
                                        location.reload();
                                    }, 3000);
                                }
                            });
                        } else {
                            p_notification(false, data[2]);
                        }
                    } else {
                        p_notification(false, eb.getMessage('ERR_VIEWPAY_USER_AUTH'));
                    }
                } else if (data[0] == 'PAYMENTSTATUSERR') {
                    p_notification(false, data[1]);
                }
            });
        }
    }


    function setDataToAttachmentViewModal(documentID) {
        $('#doc-attach-table tbody tr').remove();
        $('#doc-attach-table tfoot div').remove();
        $('#doc-attach-table tbody div').remove();

        eb.ajax({
            type: 'POST',
            url: BASE_URL + '/get-document-related-attachement',
            data: {
                documentID: documentID,
                documentTypeID: 14
            },
            success: function(respond) {
                if (respond.status == true) {
                    $('#doc-attach-table thead tr').removeClass('hidden');
                    $.each(respond.data, function(index, value) {
                        tableBody = "<tr><td><a href="+value.link+">"+value.docName+"</a></td></tr>";
                        $('#doc-attach-table tbody').append(tableBody);
                    });
                } else {
                    $('#doc-attach-table thead tr').addClass('hidden');
                    var noDataFooter = "<div class= ' col-lg-6 text-right'><label><b>No Data</b></label>";
                    $('#doc-attach-table tbody').append(noDataFooter);
                }
            }
        });
    }

///////DatePicker\\\\\\\
    var nowTemp = new Date();
    var frmdate;
    var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);

    var checkin = $('#from-date').datepicker().on('changeDate', function (ev) {
        checkin.hide();
        frmdate = ev.date;
        $('#to-date')[0].focus();
    }).data('datepicker');
    var checkout = $('#to-date').datepicker({
        onRender: function (date) {
            return date.valueOf() < frmdate ? 'disabled' : '';
        }
    }).on('changeDate', function (ev) {
        checkout.hide();
    }).data('datepicker');
/////EndOFDatePicker\\\\\\\\\
});