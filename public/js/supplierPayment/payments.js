
/**
 * @author ASHAN <ashan@thinkcube.com>
 * This file contains form validation
 */
var locationID = '';
var idInvoice = '';
var supIDPay = '';
var currentlySelectedInvoiceID;
var currentlySelectedPVID;
var $selection = 'Supplier Name';
var isSupplierNameEmpty = true;
var isPINoEmpty = true;
var isPVNoEmpty = true;
var ignoreBudgetLimitFlag = false;
var dimensionData = {};
var methods = [];
var customCurrencyRate = 1;

$(document).ready(function () {

    loadDropDownFromDatabase('/accounts-api/search-active-accounts-for-dropdown', "", "", '#cashGlAccountID');
    loadDropDownFromDatabase('/accounts-api/search-active-accounts-for-dropdown', "", "", '#chequeGlAccountID');
    loadDropDownFromDatabase('/accounts-api/search-active-accounts-for-dropdown', "", "", '#cardGlAccountID');
    loadDropDownFromDatabase('/accounts-api/search-active-accounts-for-dropdown', "", "", '#bankTransferGlAccountID');
    loadDropDownFromDatabase('/accounts-api/search-active-accounts-for-dropdown', "", "", '#lcGlAccountID');
    loadDropDownFromDatabase('/accounts-api/search-active-accounts-for-dropdown', "", "", '#ttGlAccountID');


    var dimensionArray = {};
    var dimensionTypeID = null;
    var paymentMethodIncrement = 1;
    invoiceLeftToPay = new Array();

    $('#invoiceId').hide();
    $('#purchaseVoucherId').hide();
    $(".postdated-cheque-date").hide();

    var nowTemp = new Date();
    var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);
    var postdatedDate = $('.postdated-cheque-date').datepicker({
        format: 'yyyy-mm-dd',
        orientation: "top",
        onRender: function (date) {
            return date.valueOf() < now.valueOf() ? ((U_DATE_R_OVERRIDE) ? '' : 'disabled') : '';
        }
    }).on('changeDate', function (e) {
        postdatedDate.hide();
    }).data('datepicker');

    var day = nowTemp.getDate() < 10 ? '0' + nowTemp.getDate() : nowTemp.getDate();
    var month = (nowTemp.getMonth() + 1) < 10 ? '0' + (nowTemp.getMonth() + 1) : (nowTemp.getMonth() + 1);
    $("#currentdate").val(eb.getDateForDocumentEdit('#currentdate', day, month, nowTemp.getFullYear()));
     if(!U_DATE_R_OVERRIDE){
        $('#currentdate').attr('disabled',true);
    } else {
        $('#currentdate').attr('disabled',false);
    }
    var test = $('#currentdate').datepicker({
        format: 'yyyy-mm-dd',
        onRender: function(date) {
            return date.valueOf() < now.valueOf() ? ((U_DATE_R_OVERRIDE)?'':'disabled') : '';
        }
    }).on('changeDate', function(e) {
        test.hide();
    }).data('datepicker');

    $('#dimensionView').on('click', function(e) {
        clearDimensionModal();
        var paymentID = $('#paymentID').val();

        if (!$.isEmptyObject(dimensionData)) {
            $.each(dimensionData[paymentID], function(index, val) {
                dimensionArray[index] = val;                
            });
            $.each(dimensionArray, function(index, value) {
                var newTrID = 'tr_' + value.dimensionTypeId;
                var clonedRow = $($('#dimensionPreSample').clone()).attr('id', newTrID).addClass('addedDimensions');
                $("input[name='dimensionType']", clonedRow).val(value.dimensionTypeTxt);
                $("input[name='dimensionValue']", clonedRow).val(value.dimensionValueTxt);
                clonedRow.removeClass('hidden');
                clonedRow.insertBefore('#dimensionPreSample');
                clearAddedNewDimensionRow();
            });
        }
        $('#addDimensionModal').modal('show');
    });

    // dimension modal
    function clearDimensionModal()
    {
        $("#addDimensionModal #dimensionTable tbody .addedDimensions").remove();
        dimensionArray = {};
    }


    $('.dimenisonchange').on('change', function(e) {
        dimensionTypeID = this.value;
        getDimensionValueListByDimensionType(dimensionTypeID, function() {
        });
    });



    function getDimensionValueListByDimensionType(dimensionTypeID, callback) {
        switch (dimensionTypeID) {
            case "job":
                $('.dimensionVal').addClass('hidden');
                $('.dimensionProjectNo').addClass('hidden');
                $('.dimensionJobNo').removeClass('hidden');
                break;
            case "project":
                $('.dimensionVal').addClass('hidden');
                $('.dimensionProjectNo').removeClass('hidden');
                $('.dimensionJobNo').addClass('hidden');
                break;
           default:
                $('.dimensionVal').removeClass('hidden');
                $('.dimensionProjectNo').addClass('hidden');
                $('.dimensionJobNo').addClass('hidden');
                $.ajax({
                    type: 'POST',
                    url: BASE_URL + '/dimension-api/getRelatedDimensionValues',
                    data: {dimensionId: dimensionTypeID},
                    success: function(respond) {
                        //empty options
                        $('#dimensionVal').empty();
                        $('#dimensionVal').append($("<option>", {value: '', html: 'Select dimension value'}));
                        $(respond.data).each(function(index, dimValue) {
                            $.each(dimValue, function(ind, val) {
                                $('#dimensionVal').append($("<option>", {value: ind, html: val.value}));
                            });
                        });
                        callback();
                    }
                });
                break;
        }
    }

    $('#add_dimension').on('click', function(e) {
        e.preventDefault();
        //if plus button is clicked then disabled the button
        //if button is clicked more than one, then it does not continue
        if ($(this).is(':disabled')) {
            return false;
        } else {
            $(this).attr('disabled', true);
            var $thisRow = $(this).parents('tr');
            if ($('.dimenisonchange').val() == 0 || $('.dimenisonchange').val() == '' || $('.dimenisonchange').val() == null) {
                p_notification(false, eb.getMessage('ERR_DIMENSION_SELCET'));
                $(this).attr('disabled', false);
            } else {
                
                if ($('.dimenisonchange').val() != 'job' && $('.dimenisonchange').val() != 'project') {
                    if ($('.dimensionVal').val() == 0 || $('.dimensionVal').val() == '' || $('.dimensionVal').val() == null) {
                        p_notification(false, eb.getMessage('ERR_DIMENSION_VALUE_SELCET'));
                        $(this).attr('disabled', false);
                        return;
                    }
                }

                if ($('.dimenisonchange').val() == 'project') {
                    if ($('.dimensionProjectNo').val() == 0 || $('.dimensionProjectNo').val() == '' || $('.dimensionProjectNo').val() == null) {
                        p_notification(false, eb.getMessage('ERR_DIMENSION_VALUE_SELCET'));
                        $(this).attr('disabled', false);
                        return;
                    }
                }

                if ($('.dimenisonchange').val() == 'job') {
                    if ($('.dimensionJobNo').val() == 0 || $('.dimensionJobNo').val() == '' || $('.dimensionJobNo').val() == null) {
                        p_notification(false, eb.getMessage('ERR_DIMENSION_VALUE_SELCET'));
                        $(this).attr('disabled', false);
                        return;
                    }
                }

                if (dimensionArray[dimensionTypeID]) {
                    p_notification(false, eb.getMessage('ERR_DIMENSION_ALREADY_INSERTED'));
                    $('.dimenisonchange').val('');
                    $('.dimenisonchange').selectpicker('render');
                    $('.dimenisonchange').trigger('change');
                } else {
                    addNewDimensionRow($(this).parents('tr'));
                }
                $(this).attr('disabled', false);
            }
        }
    });

    function addNewDimensionRow(thisRow) {

        var dimensionTypeId = $('.dimenisonchange', thisRow).val();
        var dimensionTypeTxt = $('.dimenisonchange', thisRow).find("option:selected").text();

        switch (dimensionTypeID) {
            case "job":
                var dimensionValueTxt = $('.dimensionJobNo', thisRow).find("option:selected").text();
                var dimensionValueId = $('.dimensionJobNo', thisRow).val();
                break;
            case "project":
                var dimensionValueTxt = $('.dimensionProjectNo', thisRow).find("option:selected").text();
                var dimensionValueId = $('.dimensionProjectNo', thisRow).val();
                break;
           default:
                var dimensionValueTxt = $('.dimensionVal', thisRow).find("option:selected").text();
                var dimensionValueId = $('.dimensionVal', thisRow).val();
                break;
        }
       
        var dimensionDuplicateCheck = false;
        if (!$.isEmptyObject(dimensionArray)) {
            $.each(dimensionArray, function(index, value) {
                if (value.dimensionTypeId == dimensionTypeId) {
                    dimensionDuplicateCheck = true;
                }
            });   
        }
       
        if (dimensionDuplicateCheck) {
            p_notification(false, eb.getMessage("ERR_DIMENSION_ALREADY_INSERTED"));
            return false;
        }

        var newTrID = 'tr_' + dimensionTypeId;
        var clonedRow = $($('#dimensionPreSample').clone()).attr('id', newTrID).addClass('addedDimensions');
        $("input[name='dimensionType']", clonedRow).val(dimensionTypeTxt);
        $("input[name='dimensionValue']", clonedRow).val(dimensionValueTxt);
        clonedRow.removeClass('hidden');
        clonedRow.insertBefore('#dimensionPreSample');
        dimensionArray[dimensionTypeId] = new dimension(dimensionTypeId, dimensionValueId,dimensionTypeTxt , dimensionValueTxt);
        clearAddedNewDimensionRow();
    }


    function dimension(dimensionTypeId, dimensionValueId, dimensionTypeTxt, dimensionValueTxt) {
        this.dimensionTypeId = dimensionTypeId;
        this.dimensionValueId = dimensionValueId;
        this.dimensionTypeTxt = dimensionTypeTxt;
        this.dimensionValueTxt = dimensionValueTxt;
    }

    function clearAddedNewDimensionRow() {
        $('.dimenisonchange').val('');
        $('.dimenisonchange').selectpicker('render');
        $('.dimenisonchange').trigger('change');
    }

    $('.addDimensions').on('click', function(e) {
        e.preventDefault();

        // validate products before closing modal
        if (!dimensionModalValidate(e)) {
            e.stopPropagation();
            return false;
        } else {
            $('#addDimensionModal').modal('hide');
        }
    });

    function dimensionModalValidate(e) {
        var paymentID = $('#paymentID').val();
        if ($.isEmptyObject(dimensionArray)) {
            dimensionData = {};
            return true;   
        } else {
            var dimensionSingleData = {};
            $.each(dimensionArray, function(index, val) {
                dimensionSingleData[index] = val;
            });
            dimensionData[paymentID] = dimensionSingleData;
            return true;
        }
    }

    $("#addDimensionModal #dimensionTable").on('click', '.deleteDimensionType', function(e) {
        e.preventDefault();
        var deleteDTrID = $(this).closest('tr').attr('id');
        var deleteDID = deleteDTrID.split('tr_')[1].trim();
        bootbox.confirm('Are you sure you want to remove this dimension ?', function(result) {
            if (result == true) {
                delete dimensionArray[deleteDID];
                $('#' + deleteDTrID).remove();
            }
        });
    });

    loadDropDownFromDatabase('/job-api/searchJobsForDropdown', "", 0, '.dimensionJobNo');
    $('.dimensionJobNo').selectpicker('refresh');
    $('.dimensionJobNo').on('change', function() {
        var dimensionValue = $(this).val();
    });

    loadDropDownFromDatabase('/api/project/searchProjectsForDropdown', "", 0, '.dimensionProjectNo');
    $('.dimensionProjectNo').selectpicker('refresh');
    $('.dimensionProjectNo').on('change', function() {
        var dimensionValue = $(this).val();
    });

    $(document).on('change', '.postdated-cheque', function() {
        if ($(this).is(":checked")) {
            $(this).parents('.payMethod').find(".postdated-cheque-date").show();
        } else {
            $(this).parents('.payMethod').find(".postdated-cheque-date").hide();
        }
    });

    $('#invoiceSearchType').on('change', function (e) {

        $selection = $('#invoiceSearchType').val();
        $('#invoiceId').hide();
        $('#supplierName').hide();
        $('#purchaseVoucherId').hide();
        currentlySelectedInvoiceID = '';
        currentlySelectedPVID = '';
        supIDPay = '';
        if ($selection == 'Supplier Name') {
            $('#supplierName').show();
            $('.dimensionDiv').removeClass('hidden');
        } else if ($selection == 'PI ID') {
            $('#invoiceId').show();
            $('.dimensionDiv').addClass('hidden');
        } else if ($selection == 'PV ID') {
            $('.dimensionDiv').addClass('hidden');
            $('#purchaseVoucherId').show();
        }
    });


    $('#reset-supplier-invoiceId-button').on('click', function () {
        location.reload();
    });

    locationID = $('#idOfLocation').val();

    $.each(LOCATION, function (index, value) {
        $('#locationID').append($("<option value='" + index + "'>" + value + "</option>"));
    });

    $('#locationID').selectpicker('refresh');

    $('#locationID').on('change', function () {
        locationID = $(this).val();
        getPaymentNumber(locationID);
    });

    function getPaymentNumber(key) {
        eb.ajax({
            type: 'POST',
            url: BASE_URL + '/customerPaymentsAPI/getPaymentNoForLocation',
            data: {locationID: key},
            success: function (respond) {
                $('#paymentID').val(respond.data['refNo']);
            }
        });
    }

    $("#advancePayment").hide();
    $("#paymentbill").hide();
    $("#crnote").hide();
    $("#creditGroup").hide();
    $("#loyaltyCardGroup").hide();
    $("#checkGroup").hide();
    // $("#cashGroup").hide();
    $("#bankTransferGroup").hide();
    $("#giftCardGroup").hide();
    $("#lcGroup").hide();
    $("#ttGroup").hide();


    $('#advance').on('click', function (e) {
        e.preventDefault();
        window.location.assign(BASE_URL + '/supplierPayments/advancePayment');
    });


    var creditnotevalue = 0;
    var paymentTotal = 0.00;
    var oldDiscountAmount = 0.00;
    var creditSupplierID = 0;
    var supplierID = 0;
    var pvAccountID = 0;
    var currentcredit = 0;
    var newbalance = 0;
    var supplierbalance = 0;
    var suppliercredit = 0;
    var allamount = 0;
    var ccid = 0;
    var chid = 0;
    var cashid = 0;
    var loycardid = 0;
    var giftcardid = 0;
    var btra = 0;
    var lc = 0;
    var tt = 0;
    var onchangeval = 0;
    var value = 0;

    var addpayment = BASE_URL + '/supplierPaymentsAPI/addAllPayment';
    var url = BASE_URL + '/supplierPaymentsAPI/check-credit';
    var sup_change = false;
    var flag = 0;

    loadDropDownFromDatabase('/supplierPaymentsAPI/searchPV-for-supplier-payments-dropdown', "", 0, '#InvoiceID');
    $('#InvoiceID').trigger('change');
    $('#InvoiceID').on('change', function () {
        if ($(this).val() > 0 && $(this).val() != currentlySelectedInvoiceID) {
            currentlySelectedInvoiceID = $(this).val();
            idInvoice = $(this).val();
            isPINoEmpty = false;
            changeEvent(idInvoice, "Invoice");
        }else if($(this).val() == 0){
            isPINoEmpty = true;
        }else if($(this).val() == currentlySelectedInvoiceID){
            isPINoEmpty = false;
        }
    });

    loadDropDownFromDatabase('/supplierPaymentsAPI/searchPaymentVoucherForSupplierPaymentsDropdown', "", 0, '#paymentVoucherID');
    $('#paymentVoucherID').trigger('change');
    $('#paymentVoucherID').on('change', function () {
        if ($(this).val() > 0 && $(this).val() != currentlySelectedPVID) {
            currentlySelectedPVID = $(this).val();
            isPVNoEmpty = false;
            changeEvent(currentlySelectedPVID, "PaymentVoucher");
        }else if($(this).val() == 0){
            isPVNoEmpty = true;
        }else if($(this).val() == currentlySelectedPVID){
            isPVNoEmpty = false;
        }
    });

    loadDropDownFromDatabase('/supplierAPI/search-suppliers-for-dropdown', "", 0, '#supplierID');
    $('#supplierID').selectpicker('refresh');

    $('#supplierID').on('change', function () {
        if ($(this).val() > 0 && $(this).val() != supIDPay) {
            supIDPay = $(this).val();
            isSupplierNameEmpty = false;
            changeEvent(supIDPay, "Supplier");
        }else if($(this).val() == 0){
            isSupplierNameEmpty = true;
        }else if($(this).val() == supIDPay){
            isSupplierNameEmpty = false;
        }
    });

    arr = new Array();
    curcre = new Array();
    curbal = new Array();
    paid = new Array();
    total = new Array();
    laloc = new Array();
    ncurcre = new Array();
    ncurbal = new Array();
    allocation = new Array();
    nowall = new Array();
    nowall1 = new Array();
    credit = new Array();
    currentcreditAmount = new Array();
    fixedCreditAmount = new Array();
    invoice = new Array();
    function Invoice() {
        this.credit = "null";
        this.creditallocation = 0;
        this.payment = null;
        this.allocation = 0;
        this.id = 0;
        this.paymentID = 0;
    }

    function changeEvent(key, type) {
        clearPaymentScreen();
        if (onchangeval != key) {
            onchangeval = key;
            flag++;
            $('#paymentbill').addClass('margin_top margin_bottom');
            $("#crnote").hide();
            if (type == 'Supplier') {
                $('#InvoiceID').val('');
                value = "supplie=" + key;
                supplierID = key;
            } else if (type == 'Invoice') {
                $('#currentCredit').val('0.00');
                value = "invoice=" + key;
                if ($('#InvoiceID').val() != '') {
                    var inv = BASE_URL + '/supplierPaymentsAPI/checkInvoice';
                    eb.ajax({
                        type: 'post',
                        url: inv,
                        data: {invoiceID: key},
                        success: function (invoicedata) {
                            if (invoicedata.status == true) {
                                if (invoicedata.data.status == 4 || invoicedata.data.status == 5) {
                                    p_notification(false, eb.getMessage('ERR_PAYMENT_PAIDFULL'));
                                } else {
                                    supplierID = invoicedata.data.purchaseInvoiceSupplierID;
                                }
                            } else {
//                                p_notification(false, "Wrong Invoice ID.");
                            }
                        },
                        async: false
                    });
                }
            } else if (type == 'PaymentVoucher') {
                $('#currentCredit').val('0.00');
                value = "pVocher=" + key;
                if ($('#paymentVoucherID').val() != '') {
                    var pVUrl = BASE_URL + '/expense-purchase-invoice-api/checkPaymentVoucher';
                    eb.ajax({
                        type: 'post',
                        url: pVUrl,
                        data: {pVID: key},
                        success: function (pVdata) {
                            if (pVdata.status == true) {
                                if (pVdata.data.paymentVoucherStatus == 4 || pVdata.data.paymentVoucherStatus == 5) {
                                    p_notification(false, eb.getMessage('ERR_PAYMENT_PAIDFULL'));
                                } else if (pVdata.data.paymentVoucherStatus == 7) {
                                    p_notification(false, eb.getMessage('ERR_PAYMENT_NOT_APPOV'));
                                } else {
                                    supplierID = pVdata.data.paymentVoucherSupplierID;
                                    pvAccountID = pVdata.data.paymentVoucherAccountID;
                                }
                            } else {
                            }
                        },
                        async: false
                    });
                }
            }

            $("#paymentbill").show();
            if (sup_change) {
                var s = document.getElementById('paymentbill');
                s.value = '';
            }
            sup_change = true;
            $('#appendtable').html('');
            eb.ajax({
                type: 'post',
                url: BASE_URL + '/supplierPaymentsAPI/invoice?id=' + value,
                success: function (data) {
                    $('#appendtable').html(data);
                },
                async: false
            });

            var amount;
            var discount;
            var totalamount;
            $('#discountAmount').keyup(function () {
                if ($('#amount').val()) {
                    if (isNaN($('#discountAmount').val())) {
                        $('#discountAmount').val('');
                        p_notification(false, eb.getMessage('ERR_PAY_DISCOUNT_NUMR'));
                        $('#totalpayment').html(numberWithCommas(parseFloat($('#amount').val().replace(/,/g, '')).toFixed(2)));
                    } else {
                        amount = $('#amount').val().replace(/,/g, '');
                        if (parseFloat($('#discountAmount').val()) >= parseFloat(amount)) {
                            p_notification(false, eb.getMessage('ERR_PAY_DISCOUNT_AMOUNT'));
                            $('#discountAmount').val('');
                            $('#totalpayment').html(numberWithCommas(parseFloat(amount).toFixed(2)));
                        } else {
                            discount = $('#discountAmount').val().replace(/,/g, '');
                            totalamount = amount - discount;
                            $('#totalpayment').html(numberWithCommas(parseFloat(totalamount).toFixed(2)));
                        }
                    }
                } else {
                    p_notification(false, eb.getMessage('ERR_PAYMENT_AMOUNT'));
                    $('#discountAmount').val('');
                }
            }).attr('autocomplete', 'off');

            eb.ajax({
                type: 'post',
                url: url,
                data: {supplierID: supplierID},
                dataType: "json",
                async: false,
                success: function (values) {
                    if (values.data != null) {
                        if (values.data[0] != null) {
                            suppliercredit = values.data[0];
                            $('#currentCredit').val(numberWithCommas(parseFloat(values.data[0]).toFixed(2)));
                        } else {
                            $('#currentCredit').val("0.00");
                        }

                        if (values.data[3] != null) {
                            supplierbalance = values.data[3];
                            $('#currentBalance').val(numberWithCommas(parseFloat(values.data[3]).toFixed(2)));
                        } else {
                            $('#currentBalance').val("0.00");
                        }
                        if (values.data[1] !== null) {
                            $('#supplierID').val(supplierID);
                            $('#supplierID').selectpicker('refresh');
                            $('#displaySupplier').val(values.data[1]);
                        } else {
                            p_notification(false, eb.getMessage('ERR_PAYMENT_NO_DATABASE'));
                            $('#supplierID').val('');
                        }
                        if (values.data[2] && values.data[2] !== null) {
                            $('#paymentTerm').val(values.data[2]);
                        }
                        creditnotevalue = 0;
                        creditSupplierID = 0;
                        currentcredit = 0;
                        newbalance = 0;
                    }
                }
            });
            $('#creditNote').val('');
            $('#creditNotecheck').prop('checked', false);
        }
    }
    ;

    $(document).on('click', '.chk', function () {
        if ($(this).is(":checked")) {
            id = $(this).parents('tr').attr('id');
            $('#' + id + ' .zzz #view').attr('readonly', false);
            curcre[id] = parseFloat($('#currentCredit').val().replace(/,/g, ''));
            curbal[id] = parseFloat($('#currentBalance').val().replace(/,/g, ''));
            if (nowall[id]) {
                if (nowall[id] == 0) {
                    if (curcre[id] > 0) {
                        paid[id] = $('#' + id + " .xxx").html().replace(/,/g, '');
                        total[id] = $('#' + id + " .yyy").html().replace(/,/g, '');
                        laloc[id] = total[id] - paid[id];
                        if (laloc[id] > curcre[id]) {
                            ncurcre[id] = 0;
                            ncurbal[id] = parseFloat(curbal[id]) - parseFloat(curcre[id]);
                            allocation[id] = curcre[id];
                            fixedCreditAmount[id] = curcre[id];
                        } else {
                            ncurcre[id] = parseFloat(curcre[id]) - parseFloat(laloc[id]);
                            ncurbal[id] = parseFloat(curbal[id]) - parseFloat(laloc[id]);
                            allocation[id] = laloc[id];
                            fixedCreditAmount[id] = laloc[id];
                        }
                        $('#' + id + ' .zzz #view').val(numberWithCommas(allocation[id].toFixed(2)));
                    } else {
                        ncurcre[id] = curcre[id];
                        ncurbal[id] = curbal[id];
                        allocation[id] = 0;
                        p_notification(false, eb.getMessage('ERR_PAYMENT_NOCREDIT'));
                        $('#' + id + ' .chk').prop('checked', false);
                    }
                } else {
                    if (curcre[id] > 0) {
                        laloc[id] = $('#' + id + ' .ttt').html().replace(/,/g, '');
                        if (parseFloat(laloc[id]) >= nowall[id] || (parseFloat(laloc[id]) > 0 && parseFloat(curcre[id]) > 0)) {
                            aa = parseFloat(laloc[id]);
                            if (curcre[id] >= aa) {
                                ncurcre[id] = parseFloat(curcre[id]) - aa;
                                ncurbal[id] = parseFloat(curbal[id]) - aa;
                                allocation[id] = aa;
                            } else {
                                ncurcre[id] = 0;
                                ncurbal[id] = parseFloat(curbal[id]) - parseFloat(curcre[id]);
                                allocation[id] = curcre[id];
                            }
                            $('#' + id + ' .zzz #view').val(numberWithCommas(allocation[id].toFixed(2)));
                        } else {
                            allocation[id] = 0;
                            ncurcre[id] = parseFloat(curcre[id]);
                            ncurbal[id] = curbal[id];
                            p_notification(false, eb.getMessage('ERR_PAYMENT_FULLPAYMENT'));
                            $('#' + id + ' .chk').prop('checked', false);
                            $('#' + id + ' .zzz #view').attr('readonly', true);
                        }
                    } else {
                        ncurcre[id] = curcre[id];
                        ncurbal[id] = curbal[id];
                        allocation[id] = 0;
                        p_notification(false, eb.getMessage('ERR_PAYMENT_NOCREDIT'));
                        $('#' + id + ' .chk').prop('checked', false);
                    }
                }
            } else {
                paid[id] = $('#' + id + " .xxx").html().replace(/,/g, '');
                total[id] = $('#' + id + " .yyy").html().replace(/,/g, '');
                laloc[id] = total[id] - paid[id];
                if (curcre[id] > 0) {
                    if (laloc[id] > curcre[id]) {
                        ncurcre[id] = 0;
                        ncurbal[id] = parseFloat(curbal[id]) - parseFloat(curcre[id]);
                        allocation[id] = curcre[id];
                        // fixedCreditAmount[id] = curcre[id];
                        curcre[id] = 0;
                    } else {
                        ncurcre[id] = parseFloat(curcre[id]) - parseFloat(laloc[id]);
                        ncurbal[id] = parseFloat(curbal[id]) - parseFloat(laloc[id]);
                        allocation[id] = laloc[id];
                        fixedCreditAmount[id] = laloc[id];
                    }
                    $('#' + id + ' .zzz #view').val(numberWithCommas(parseFloat(allocation[id]).toFixed(2)));
                } else {
                    ncurcre[id] = curcre[id];
                    ncurbal[id] = curbal[id];
                    allocation[id] = 0;
                    p_notification(false, eb.getMessage('ERR_PAYMENT_NOCREDIT'));
                    $('#' + id + ' .chk').prop('checked', false);
                    if (allocation[id]) {
                        $('#' + id + ' .zzz #view').val(numberWithCommas(parseFloat(allocation[id]).toFixed(2)));
                    } else {
                        $('#' + id + ' .zzz #view').val(numberWithCommas(parseFloat(0).toFixed(2)));
                        delete arr[id];
                    }
                }
            }
            if (arr[id]) {
                if (arr[id].allocation == 0 || arr[id].allocation == 0.00) {
                    $('#' + id + ' .ttt').html(numberWithCommas(parseFloat(laloc[id] - allocation[id]).toFixed(2)));
                } else {
                    if (arr[id].allocation >= laloc[id]) {
                        $('#' + id + ' .ttt').html('0.00');
                    } else {
                        $('#' + id + ' .ttt').html(numberWithCommas(parseFloat(laloc[id] - allocation[id]).toFixed(2)));
                    }
                }
            } else {
                if (allocation[id]) {
                    $('#' + id + ' .ttt').html(numberWithCommas(parseFloat(laloc[id] - allocation[id]).toFixed(2)));
                } else {
                    $('#' + id + ' .ttt').html(numberWithCommas(parseFloat(laloc[id]).toFixed(2)));
                }
            }
            $('#currentCredit').val(numberWithCommas(parseFloat(ncurcre[id]).toFixed(2)));
            $('#currentBalance').val(numberWithCommas(parseFloat(ncurbal[id]).toFixed(2)));

            id = id.toString();
            if (arr[id]) {
                credit[id] = $(this).val();
                arr[id].credit = credit[id];
                arr[id].creditallocation = allocation[id];
            } else {
                arr[id] = new Invoice();
                credit[id] = $(this).val();
                arr[id].id = id;
                arr[id].creditallocation = allocation[id];
                arr[id].credit = credit[id];
                arr[id].paymentID = $('#paymentID').val().replace(/\s/g, "");
            }
            if (allocation[id]) {
                allamount += allocation[id];
            } else {
                allamount += 0;
            }
        } else {
            id = $(this).parents('tr').attr('id');
            $('#' + id + ' .zzz #view').attr('readonly', true);
            curcre[id] = parseFloat($('#currentCredit').val().replace(/,/g, ''));
            curbal[id] = parseFloat($('#currentBalance').val().replace(/,/g, ''));
            var lal = $('#' + id + " .ttt").html().replace(/,/g, '');
            var totalRow = $('#' + id + " .yyy").html().replace(/,/g, '');
            var settledRow = $('#' + id + " .xxx").html().replace(/,/g, '');
            laloc[id] = totalRow - settledRow;
            if (arr[id].allocation == 0) {
                $('#' + id + ' .ttt').html(numberWithCommas(parseFloat(laloc[id]).toFixed(2)));
                arr[id].paymentID = 0;
            } else {
                if (parseFloat(arr[id].allocation) >= (parseFloat(totalRow) - parseFloat(settledRow))) {
                    $('#' + id + ' .ttt').html('0.00');
                } else {
                    $('#' + id + ' .ttt').html(numberWithCommas(parseFloat(allocation[id]).toFixed(2)));
                } 
            }
            if (parseFloat(lal) != 0) {
                var newLeftAmount = parseFloat(lal) + parseFloat(arr[id].creditallocation);
                laloc[id] = parseFloat(laloc[id]) + parseFloat(arr[id].creditallocation);
                $('#' + id + ' .ttt').html(numberWithCommas(newLeftAmount.toFixed(2)));
            }

            if (curcre[id] >= 0) {
                if (allocation[id] != null) {
                    ncurcre[id] = parseFloat(curcre[id]) + parseFloat(allocation[id]);
                    if (nowall[id]) {
                        if (nowall[id] != 0) {
                            if (parseFloat(nowall[id]) >= parseFloat(laloc[id])) {
                                ncurbal[id] = parseFloat(ncurbal[id]);
                            } else {
                                ncurbal[id] = parseFloat(curbal[id]) + parseFloat(allocation[id]);
                            }
                        } else {
                            ncurbal[id] = parseFloat(curbal[id]) + parseFloat(allocation[id]);
                        }
                    } else {
                        ncurbal[id] = parseFloat(curbal[id]) + parseFloat(allocation[id]);
                    }
                } else {
                    ncurcre[id] = parseFloat(curcre[id]);
                    ncurbal[id] = parseFloat(curbal[id]);
                }
            } else {
                ncurcre[id] = parseFloat(curcre[id]) + parseFloat(allocation[id]);
                ncurbal[id] = parseFloat(curbal[id]) + parseFloat(allocation[id]);
            }
            credit[id] = null;
            arr[id].credit = null;
            arr[id].creditallocation = 0;
            allamount = allamount - allocation[id];
            allocation[id] = 0;
            $('#currentCredit').val(numberWithCommas(parseFloat(ncurcre[id]).toFixed(2)));
            $('#currentBalance').val(numberWithCommas(parseFloat(ncurbal[id]).toFixed(2)));
            $('#' + id + ' .zzz #view').val('0.00');
            if (arr[id].allocation == 0) {
                $('#' + id + " .met #paymentMethod").val(0);
                delete arr[id];
            }
        }
        $('#amount').val(numberWithCommas(parseFloat(allamount).toFixed(2)));
        if ($('#discountAmount').val().replace(/,/g, '') == '') {
            $('#totalpayment').html(numberWithCommas(parseFloat(allamount).toFixed(2)));
        } else {
            if (allamount > $('#discountAmount').val()) {
                $('#totalpayment').html(numberWithCommas(parseFloat(allamount - $('#discountAmount').val().replace(/,/g, '')).toFixed(2)));
            } else {
                p_notification(false, eb.getMessage('ERR_PAYMENT_MOREDISC'));
                $('#totalpayment').html($('#amount').val());
                $('#discountAmount').val('');
            }
        }
    });

     $(document).on('change', '#view', function () {
        id = $(this).parents('tr').attr('id');
        if (fixedCreditAmount[id] != undefined) {
            curcre[id] = parseFloat($('#currentCredit').val().replace(/,/g, '')) + parseFloat(fixedCreditAmount[id]);
        } else {
            curcre[id] = parseFloat($('#currentCredit').val().replace(/,/g, ''));
        }
        curbal[id] = parseFloat($('#currentBalance').val().replace(/,/g, ''));
       
        paid[id] = $('#' + id + " .xxx").html().replace(/,/g, '');
        total[id] = $('#' + id + " .yyy").html().replace(/,/g, '');
        if (nowall[id] == undefined) {
            laloc[id] = total[id] - paid[id];
        } else {
            laloc[id] = total[id] - paid[id] - nowall[id];
        }
        var rawAmount = $('#' + id + ' .zzz #view').val();
        var cashAmount = $('#' + id + ' .all #thisallocation').val().replace(/,/g, '');
        if (cashAmount != "") {
            laloc[id] = total[id] - paid[id] - cashAmount;
        }
        if (rawAmount > laloc[id]) {
            allamount = allamount - laloc[id];    
            allocation[id] = laloc[id];
            $('#' + id + ' .zzz #view').val(numberWithCommas(parseFloat(laloc[id]).toFixed(2)));
        } else {
            if (rawAmount >  curcre[id]) {
                // $('#' + id + ' .zzz #view').val(numberWithCommas(parseFloat(fixedCreditAmount[id]).toFixed(2)));
                $('#' + id + ' .zzz #view').val(numberWithCommas(parseFloat(rawAmount).toFixed(2)));
                // fixedCreditAmount[id] = rawAmount;
                if (allocation[id] != undefined) {
                    allamount = allamount - parseFloat(allocation[id]);
                } else {
                    allamount = allamount;
                }
            } else {
                rawAmount = rawAmount;
                allamount = allamount - parseFloat(laloc[id]);
            }
            arr[id].creditallocation = rawAmount;
            ncurcre[id] = curcre[id] + parseFloat(allocation[id]) - parseFloat(rawAmount);
            if (allocation[id] != undefined) {
                ncurbal[id] = curbal[id] + allocation[id] - rawAmount;
            } else {
                ncurbal[id] = curbal[id] - rawAmount;
            }
            $('#currentCredit').val(numberWithCommas(parseFloat(ncurcre[id]).toFixed(2)))
            $('#currentBalance').val(numberWithCommas(parseFloat(ncurbal[id]).toFixed(2)));
            allocation[id] = parseFloat(rawAmount);
            $('#' + id + ' .ttt').html(numberWithCommas(parseFloat(laloc[id] - rawAmount).toFixed(2)));
        }

        id = id.toString();
        if (arr[id]) {
            arr[id].creditallocation = allocation[id];
        } else {
            arr[id] = new Invoice();
            credit[id] = $(this).val();
            arr[id].id = id;
            arr[id].creditallocation = allocation[id];
            arr[id].credit = credit[id];
            arr[id].paymentID = $('#paymentID').val().replace(/\s/g, "");
        }

        if (allocation[id]) {
            allamount += allocation[id];
        } else {
            allamount += 0;
        }

        $('#amount').val(numberWithCommas(parseFloat(allamount).toFixed(2)));
        if ($('#discountAmount').val().replace(/,/g, '') == '') {
            $('#totalpayment').html(numberWithCommas(parseFloat(allamount).toFixed(2)));
        } else {
            if (allamount > $('#discountAmount').val()) {
                $('#totalpayment').html(numberWithCommas(parseFloat(allamount - $('#discountAmount').val().replace(/,/g, '')).toFixed(2)));
            } else {
                p_notification(false, eb.getMessage('ERR_PAYMENT_MOREDISC'));
                $('#totalpayment').html($('#amount').val());
                $('#discountAmount').val('');
            }
        }
        $('#' + id + ' .zzz #view').attr('readonly', true);
    });

    $(document).on('change', '.sel', function () {
        id = (this.parentNode).parentNode.getAttribute('id');
        id = id.toString();
        value = $(this).val();
        if (value == "null" || value == 0) {
            //            p_notification(false, "Please select valid payment method for " + id + " invoice.");
            if (arr[id]) {
                arr[id].payment = null;
            }
        } else {
            if (arr[id]) {
                arr[id].payment = $(this).val();
            } else {
                arr[id] = new Invoice();
                arr[id].id = id;
                arr[id].payment = $(this).val();
                arr[id].paymentID = $('#paymentID').val().replace(/\s/g, "");
            }
        }
        ccid = 0;
        chid = 0;
        cashid = 0;
        loycardid = 0;
        btra = 0;
        giftcardid = 0;
        lc = 0;
        tt = 0;
        for (var i in arr) {
            if (arr[i].payment == '3') {
                ccid += 1;
            } else if (arr[i].payment == '2') {
                chid += 1;
            } else if (arr[i].payment == '5') {
                btra += 1;
            } else if (arr[i].payment == '7') {
                lc += 1;
            } else if (arr[i].payment == '8') {
                tt += 1;
            } else if(arr[i].payment == '1'){
                cashid += 1;
            } else if(arr[i].payment == '4'){
            	loycardid +=1;
            } else if(arr[i].payment == '6'){
            	giftcardid +=1;
            }
        }

        if (cashid != 0) {
            $("#cashGroup").show();
            loadDropDownFromDatabase('/accounts-api/search-active-accounts-for-dropdown', "", "", '#cashGlAccountID');
        } else {
            $("#cashGroup").hide();
        }

        if (ccid != 0) {
            $("#creditGroup").show();
            loadDropDownFromDatabase('/accounts-api/search-active-accounts-for-dropdown', "", "", '#cardGlAccountID');
        } else {
            $("#creditGroup").hide();
        }

        if (chid != 0) {
            $("#checkGroup").show();
            loadDropDownFromDatabase('/accounts-api/search-active-accounts-for-dropdown', "", "", '#chequeGlAccountID');
        } else {
            $("#checkGroup").hide();
        }

        if (loycardid != 0) {
            $("#loyaltyCardGroup").show();
            loadDropDownFromDatabase('/accounts-api/search-active-accounts-for-dropdown', "", "", '#loyaltyCardGlAccountID');
        } else {
            $("#loyaltyCardGroup").hide();
        }

        if (btra != 0) {
            $("#bankTransferGroup").show();
            loadDropDownFromDatabase('/accounts-api/search-active-accounts-for-dropdown', "", "", '#bankTransferGlAccountID');
        } else {
            $("#bankTransferGroup").hide();
        }

        if (giftcardid != 0) {
            $("#giftCardGroup").show();
            loadDropDownFromDatabase('/accounts-api/search-active-accounts-for-dropdown', "", "", '#giftCardGlAccountID');
        } else {
            $("#giftCardGroup").hide();
        }

        if (lc != 0) {
            $("#lcGroup").show();
            loadDropDownFromDatabase('/accounts-api/search-active-accounts-for-dropdown', "", "", '#lCGlAccountID');
        } else {
            $("#lcGroup").hide();
        }

        if (tt != 0) {
            $("#ttGroup").show();
            loadDropDownFromDatabase('/accounts-api/search-active-accounts-for-dropdown', "", "", '#tTGlAccountID');
        } else {
            $("#ttGroup").hide();
        }
    });


    $(document).on('click', '.edi', function() {
        id = (this.parentNode).parentNode.getAttribute('id');
        if (!invoiceLeftToPay[id]) {
            var newlaloc = $('#' + id + " .ttt").html().replace(/,/g, '');
            $(this).val(newlaloc);
            $('#' + id + " .ttt").html('0.00');
            invoiceLeftToPay[id] = newlaloc;
            setPaymentTotal();
        }
    });

    $(document).on('change', '.edi', function() {
        var amount = parseFloat($(this).val());
        id = (this.parentNode).parentNode.getAttribute('id');
        if (amount < 0 || isNaN(amount)) {
            p_notification(false, eb.getMessage('ERR_PAY_AMOUNT_SHOUDBE_NUMBER'));
            amount = 0.00;
        } else if (amount > parseFloat(invoiceLeftToPay[id])) {
            p_notification(false, eb.getMessage('ERR_PAY_TINVPAI_AM_CANT_BE_MORE_THAN_LEFTPAID'));
            amount = 0.00;
        }
        $(this).val(amount);
        var newlaloc = numberWithCommas(parseFloat(invoiceLeftToPay[id] - amount));
        $('#' + id + " .ttt").html(newlaloc);
        if (amount == 0.00) {
            delete invoiceLeftToPay[id];
        }
        setPaymentTotal();
    });

    function clearPaymentScreen() {
        $('#amount').val('0.00');
        $('#totalpayment').text('0.00');
        creditnotevalue = 0;
        creditSupplierID = 0;
        supplierID = 0;
        pvAccountID = 0;
        currentcredit = 0;
        newbalance = 0;
        supplierbalance = 0;
        suppliercredit = 0;
        allamount = 0;
        ccid = 0;
        chid = 0;
        loycardid = 0;
        cashid = 0;
        giftcardid = 0;
        onchangeval = 0;
        value = 0;
        sup_change = false;
        flag = 0;
        arr = new Array();
        curcre = new Array();
        curbal = new Array();
        paid = new Array();
        total = new Array();
        laloc = new Array();
        ncurcre = new Array();
        ncurbal = new Array();
        allocation = new Array();
        nowall = new Array();
        nowall1 = new Array();
        credit = new Array();
        invoice = new Array();
    }

    function validate_form(valid) {
        var cus = valid[0];
        var payt = valid[1];
        var date = valid[2];
        var dis = valid[3];
        var amount = valid[4];
        var item = valid[5];
        var bankID = $('#bankID').val();
        var accountID = $('.accountID').val();
        var useAccounting = $('#useAccounting').val();
        var supplierAccountNumber = $('#suplierAccountNumber').val();
        if ($('#supplierID').val() == '' && $('#InvoiceID').val() == '' && $('#paymentVoucherID').val() == '') {
            p_notification(false, eb.getMessage('ERR_PAVMENT_SUPNAME&CODE'));
            return false;
        }

        if(isSupplierNameEmpty && $selection =='Supplier Name'){
            p_notification(false, eb.getMessage('ERR_PAVMENT_SUPNAME&CODE'));
            return false;
        }else if(isPINoEmpty && $selection =='PI ID'){
            p_notification(false, eb.getMessage('ERR_PAVMENT_SUPNAME&CODE'));
            return false;
        }else if(isPVNoEmpty && $selection =='PV ID'){
            p_notification(false, eb.getMessage('ERR_PAVMENT_SUPNAME&CODE'));
            return false;
        } else if ($('#restToPaid').val().replace(/,/g, '') > 0) {
            p_notification(false, eb.getMessage('ERR_PAY_TOTAL_SHOULD_BE_PAY'));
            return false;
        }


        if (valid[10] == '-') {
            p_notification(false, eb.getMessage('ERR_PAY_REFNUM'));
        } else if (date == null || date == "") {
            p_notification(false, eb.getMessage('ERR_ADVPAY_DATE'));
        } else if (isNaN(amount)) {
            p_notification(false, eb.getMessage('ERR_PAYMENT_AMOUNTNUM'));
        } else if (amount < 0) {
            p_notification(false, eb.getMessage('ERR_PAYMENT_AMOUNTNEG'));
        } else if (isNaN(dis)) {
            p_notification(false, eb.getMessage('ERR_PAY_DISCOUNT_NUMR'));
        } else if (dis < 0) {
            p_notification(false, eb.getMessage('ERR_PAY_DISCOUNT_NEG'));
        } else {

            var checked = true;
            methods = [];
            var creditAmount = parseFloat($('#creditAmount').val().replace(/,/g, ''));
            $('.payMethod').each(function() {
                var methodID = $('.paymentMethod option:selected', this).val();
                var paidAmount = $('.paidAmount', this).val();
                var checquenumber = $('.checquenumber', this).val();
                var chequeReference = $('.chequeReference', this).val();
                var bank = $('.bank', this).val();
                var bankTransferReferenceNumber = $('#bankTransferReferenceNumber', this).val();
                var reciptnumber = $('.reciptnumber', this).val();
                var cardnumber = $('.cardnumber', this).val();
                var bankID = $('#supplierBank').val();
                var accountID = $('.suplierAccountNumber', this).val();
                var customerBank = $('.supplierBank', this).val();
                var customerAccountNumber = $('.supplierAccountNumber', this).val();
                var giftCardID = $('.giftCardID', this).val();
                var lcPaymentReference = $('.lcPaymentReference', this).text();
                var ttPaymentReference = $('.ttPaymentReference', this).text();
                var giftCardReferenceNumber = $('#giftCardReferenceNumber').val();
                var postdatedStatus = 0;
                var postdatedDate = null;
                var cashAccountID = $(this).find('#cashGlAccountID').val();
                var chequeAccountID = $(this).find('#chequeGlAccountID').val();
                var creditAccountID = $(this).find('#cardGlAccountID').val();
                var bankTransferAccountID = $(this).find('#bankTransferGlAccountID').val();
                var lcAccountID = $(this).find('#lCGlAccountID').val();
                var ttAccountID = $(this).find('#tTGlAccountID').val();
                var loyaltyCardGlAccountID = $(this).find('#loyaltyCardGlAccountID').val();
                var chequeBankID = $(this).find('.chbankID').val();
                var chequeBankAccID = $(this).find('.checkAccountID').val();
                var bankTransferBankID = $(this).find('.bankID').val();
                var bankTransferBankAccountID = $(this).find('.accountID').val();
                //check whether postdated cheque
                if ($('.postdated-cheque', this).is(":checked")) {
                    postdatedStatus = 1;
                    postdatedDate = $('.postdated-cheque-date', this).val();
                }

                if (parseFloat(paidAmount) > 0) {
                    methods.push({
                        methodID: methodID,
                        paidAmount: paidAmount,
                        checkNumber: checquenumber,
                        bank: bank,
                        reciptnumber: reciptnumber,
                        cardnumber: cardnumber,
                        bankID: bankID,
                        bankTransferReferenceNumber: bankTransferReferenceNumber,
                        accountID: accountID,
                        supplierBank: customerBank,
                        supplierAccountNumber: customerAccountNumber,
                        giftCardID: giftCardID,
                        giftCardReferenceNumber: giftCardReferenceNumber,
                        postdatedStatus: postdatedStatus,
                        postdatedDate: postdatedDate,
                        lcPaymentReference: lcPaymentReference,
                        ttPaymentReference: ttPaymentReference,
                        cashAccountID : cashAccountID,
                        chequeAccountID : chequeAccountID,
                        creditAccountID : creditAccountID,
                        loyaltyCardGlAccountID : loyaltyCardGlAccountID,
                        bankTransferAccountID : bankTransferAccountID,
                        lcAccountID : lcAccountID,
                        ttAccountID : ttAccountID,
                        chequeReference: chequeReference,
                        chequeBankID: chequeBankID,
                        chequeBankAccID : chequeBankAccID,
                        bankTransferBankID: bankTransferBankID,
                        bankTransferBankAccountID: bankTransferBankAccountID

                    });
                }


                if (!paidAmount && !creditAmount) {
                    p_notification(false, eb.getMessage('ERR_PAY_PAID_AM_CAN_BE_EMTY'));
                    $('.paidAmount', this).focus();
                    checked = false;
                    return false;
                } else if (methodID == '') {
                    p_notification(false, eb.getMessage('ERR_PAY_PAID_METHOD_SHBE_SELECT'));
                    $('.advancepaymentMethod', this).focus();
                    checked = false;
                    return false;
                } else if (methodID == 2) {
//                    if (checquenumber == null || checquenumber == "") {
//                        p_notification(false, eb.getMessage('ERR_ADVPAY_CHEQUE'));
//                        $('.checquenumber', this).focus();
//                        checked = false;
//                        return false;
//                    } else

//                    if (checquenumber != "" && checquenumber.length > 8) {
//                        p_notification(false, eb.getMessage('ERR_PAY_CHEQUE_INVALID'));
//                        $('.checquenumber', this).focus();
//                        checked = false;
//                        return false;
//                    } else if (checquenumber != "" && isNaN(checquenumber)) {
//                        p_notification(false, eb.getMessage('ERR_ADVPAY_CHEQUE_NUMR'));
//                        $('.checquenumber', this).focus();
//                        checked = false;
//                        return false;
//                    }
//                    else if (bank == null || bank == "") {
//                        p_notification(false, eb.getMessage('ERR_ADVPAY_BANK_NAME'));
//                        $('.bank', this).focus();
//                        checked = false;
//                        return false;
//                    }

                } else if (methodID == 3) {
//                    if (reciptnumber == null || reciptnumber == "") {
//                        p_notification(false, eb.getMessage('ERR_ADVPAY_RECNO'));
//                        $('.reciptnumber', this).focus();
//                        checked = false;
//                        return false;
//                    } else
                    if (reciptnumber != "" && reciptnumber.length > 12) {
                        p_notification(false, eb.getMessage('ERR_PAY_RECIPTNO_VALIDITY'));
                        $('.reciptnumber', this).focus();
                        checked = false;
                        return false;
                    } 
                } else if (methodID == 5) {
                    // if ((chequeBankID != null || chequeBankID != "") && (chequeBankID != null || chequeBankID != "")) {
                    //    p_notification(false, eb.getMessage('ERR_PAY_METHOD_BANK_ID'));
                    //    $('.bankID', this).focus();
                    //    checked = false;
                    //    return false;
                    // }
                   // } else if (accountID == null || accountID == "") {
//                        p_notification(false, eb.getMessage('ERR_PAY_METHOD_ACCOUNT_ID'));
//                        $('.accountID', this).focus();
//                        checked = false;
//                        return false;
//                    } else
                    if (customerAccountNumber != '' && isNaN(customerAccountNumber)) {
                        p_notification(false, eb.getMessage('ERR_PAY_METHOD_CUST_ACCUNT'));
                        $('.customerAccountNumber', this).focus();
                        checked = false;
                        return false;
                    }
                } else if (methodID == 6) {
                    if (giftCardID == null || giftCardID == "") {
                        p_notification(false, eb.getMessage('ERR_PAY_METHOD_GIFT_CARD_SH_SELECT'));
                        $('.giftCardID', this).focus();
                        checked = false;
                        return false;
                    }
                } else if (methodID == 4) {
                    var credit = parseFloat($('#creditAmount').val().replace(/,/g, ''));
                    credit = isNaN(credit) ? 0 : credit;
                    var amount = parseFloat($('#amount').val().replace(/,/g, ''));
                    var totalBill = amount - credit;
                    totalBill = totalBill * customCurrencyRate;
                    var pAmount = paidAmount * customCurrencyRate;

                    if (!loyalty.isValid(pAmount, totalBill)) {
                        p_notification(false, eb.getMessage('ERR_LOYALTY_REDEEM_EXCEED'));
                        checked = false;
                        return false;
                    }
                    // loyalty.calculateLoyaltyPoints(0, totalBill);
                    return true;
                }

                if (methodID != 4) {
                    var credit = parseFloat($('#creditAmount').val().replace(/,/g, ''));
                    credit = isNaN(credit) ? 0 : credit;
                    var amount = parseFloat($('#amount').val().replace(/,/g, ''));

                    var totalBill = amount - credit;
                    totalBill = totalBill * customCurrencyRate;
                    // loyalty.calculateLoyaltyPoints(0, totalBill);
                }
            });
            if (checked) {
                return true;
            } else {
                return false;
            }
        }
    }

    function printdiv(divID) {
        var DocumentContainer = document.getElementById(divID);
        var WindowObject = window.open('', 'PrintWindow', 'width=750,height=650,top=50,left=50,toolbars=no,scrollbars=yes,status=no,resizable=yes');

        WindowObject.document.writeln('<!DOCTYPE html>');
        WindowObject.document.writeln('<html><head><title></title>');
        WindowObject.document.writeln('<link rel="stylesheet" type="text/css" href="/assets/bootstrap/css/bootstrap.min.css">');
        WindowObject.document.writeln('</head><body>');
        WindowObject.document.writeln(DocumentContainer.innerHTML);
        WindowObject.document.writeln('</body></html>');

        WindowObject.document.close();
        WindowObject.focus();
        WindowObject.print();
        WindowObject.close();
    }


    $('#addandprint,#supplier-payments-form').on('submit', function (e) {
        e.preventDefault();
        saveSupplierPayment();
    });

    function saveSupplierPayment()
    {
        $("#addandprint").attr("disabled", "disabled");
        //        $("#addPayment").attr("disabled", "disabled");
        var rmCreditBalance = false;
        if($("#remove-credit-balance").prop('checked') == true){
            rmCreditBalance = true;
        } else {
            rmCreditBalance = false;
        }

        var valid = new Array(
                supplierID,
                $('#paymentTerm').val(),
                $('#currentdate').val(),
                $('#discountAmount').val().replace(/,/g, ''),
                $('#amount').val().replace(/,/g, ''),
                arr,
                $('#reciptnumber').val(),
                $('#cardnumber').val(),
                $('#checquenumber').val(),
                $('#bank').val(),
                $('#paymentID').val().replace(/\s/g, ""));
        var Inv_data = {};
        // var chequeBankAmount = 0.00;
        // for (var i in arr) {
        //     if (arr[i].allocation != 0 || arr[i].creditallocation != 0) {
        //         Inv_data[i] = JSON.stringify(arr[i]);
        //         if (arr[i].payment == '2') {
        //             chequeBankAmount = +chequeBankAmount + +arr[i].allocation;
        //         }
        //     }
        // }

        var Inv_data = {};
        $('#paymentbill').find('tbody > tr').each(function() {
            var amount = $(this).find('.edi').val();
            if (amount > 0) {
                var invoiceID = this.id;
                Inv_data[invoiceID] = amount;

            }
        });

        // console.log(Inv_data);
        // return;

        // var Inv_data = {};
        // $('#paymentbill').find('tbody > tr').each(function() {
        //     var amount = $(this).find('.edi').val().replace(/,/g, '');
        //     if (amount > 0) {
        //         var invoiceID = this.id;
        //         Inv_data[invoiceID] = parseFloat(amount);

        //     }
        // });


        if (validate_form(valid)) {
            // console.log(methods);
            // return;
            $('.main_div').addClass('Ajaxloading');
            eb.ajax({
                type: 'post',
                url: addpayment,
                data: {
                    supplierID: supplierID,
                    pvAccountID: pvAccountID,
                    supplierCredit: suppliercredit,
                    supplierBalance: supplierbalance,
                    creditNote: $('#creditNote').val(),
                    invData: JSON.stringify(Inv_data),
                    paymentCode: $('#paymentID').val().replace(/\s/g, ""),
                    // date: $('#currentdate').html().replace(/\s/g, ""),
                    date: $('#currentdate').val(),
                    paymentTerm: $('#paymentTerm').val(),
                    amount: $('#amount').val().replace(/,/g, ''),
                    discount: $('#discountAmount').val().replace(/,/g, ''),
                    memo: $('#memo').val(),
                    paymentType: 'invoice',
                    creditAmount: $('#creditAmount').val(),
                    locationID: locationID,
                    PaymentMethodDetails : JSON.stringify(methods),
                    rmCreditBalance: rmCreditBalance,
                    dimensionData: JSON.stringify(dimensionData),
                    ignoreBudgetLimit: ignoreBudgetLimitFlag
                },
                dataType: "json",
                async: false,
                success: function (data) {
                    if (data['state'] == true) {
                        var fileInput = document.getElementById('documentFiles');
                        if(fileInput.files.length > 0) {
                            var form_data = false;
                            if (window.FormData) {
                                form_data = new FormData();
                            }
                            form_data.append("documentID", data['id']);
                            form_data.append("documentTypeID", 14);
                            
                            for (var i = 0; i < fileInput.files.length; i++) {
                                form_data.append("files[]", fileInput.files[i]);
                            }

                            eb.ajax({
                                url: BASE_URL + '/store-files',
                                type: 'POST',
                                processData: false,
                                contentType: false,
                                data: form_data,
                                success: function(res) {
                                }
                            });
                        }

                        p_notification(true, eb.getMessage('SUCC_PAYMENT_ADDPAY', data['value']));

                        documentPreview(BASE_URL + '/supplierPayments/viewReceipt/' + data['id'], 'documentpreview', "/supplierPaymentsAPI/sendSupplierPaymentEmail", function ($preview) {
                            $preview.on('hidden.bs.modal', function () {
                                if (!$("#preview:visible").length) {
                                    window.location.assign(BASE_URL + '/supplierPayments/create/');
                                }
                            });
                        });
                    } else if (data['state'] == false) {
                        if (data['data'] == "NotifyBudgetLimit") {
                            bootbox.confirm(data['msg']+' ,Are you sure you want to save this payment ?', function(result) {
                                if (result == true) {
                                    ignoreBudgetLimitFlag = true;
                                    saveSupplierPayment();
                                } else {
                                    setTimeout(function(){ 
                                        location.reload();
                                    }, 3000);
                                }
                            });
                        } else {
                            p_notification(false, data['msg']);
                            $('.main_div').removeClass('Ajaxloading');
                        }
                    } else if (!data['status']) {
                        p_notification(false, data['msg']);
                        $('.main_div').removeClass('Ajaxloading');

                    }
                },
            });
        } else {
//            $("#addandprint").attr("disabled", false);
            $("#addPayment").attr("disabled", false);
        }
    }

    $('#creditNotecheck').on("click", function () {
        getCreditNoteBySupplierID(supplierID);
        if ($('#creditNotecheck').is(":checked")) {
            $("#crnote").show();
            if ($('#supplierID').val() != '') {
                var abc = $('#creditNote').typeahead();
                abc.data('typeahead').source = credit_note_list;
            } else {
                p_notification(false, eb.getMessage('ERR_PAYMENT_SELECT_CUS'));
                $('#creditNotecheck').prop('checked', false);
            }
        } else {
            $('#creditNote').val('');
            $('#creditNotecheck').prop('checked', false);
            $("#crnote").hide();
            if (creditnotevalue) {
                var uncheckvalue = parseFloat($('#currentCredit').val().replace(/,/g, ''));
                $('#currentCredit').val(numberWithCommas(parseFloat(uncheckvalue - parseFloat(creditnotevalue)).toFixed(2)));
                creditnotevalue = 0;
                creditSupplierID = 0;
                currentcredit = 0;
                newbalance = 0;
            }
        }
        ;
    });
//todo after creating the debit note
    $('#creditNote').on('change', function () {
        var crnoteno = $('#creditNote').val();
        var crnoteurl = BASE_URL + '/customerPaymentsAPI/getCreditNoteByID';
        var request = eb.post(crnoteurl, {
            creditNote: crnoteno});
        request.done(function (values) {
            if (values[0] != null) {
                if (values[0].state != "close") {
                    if (values[0].customerID == supplierID) {
                        if (creditnotevalue) {
                            currentcredit = parseFloat($('#currentCredit').val().replace(/,/g, ''));
                            $('#currentCredit').val(numberWithCommas(parseFloat(currentcredit - parseFloat(creditnotevalue)).toFixed(2)));
                            creditnotevalue = parseFloat(values[0].totalAmount);
                            creditSupplierID = values[0].customerID;
                        } else {
                            creditnotevalue = parseFloat(values[0].totalAmount);
                            creditSupplierID = values[0].customerID;
                            currentcredit = parseFloat($('#currentCredit').val().replace(/,/g, '')).toFixed(2);
                            newbalance = 0;
                        }
                        if (creditSupplierID == null) {
                            $('#currentCredit').val(parseFloat(0).toFixed(2));
                            newbalance = parseFloat(creditnotevalue);
                            $('#currentCredit').val(numberWithCommas(parseFloat(newbalance).toFixed(2)));
                        } else {
                            currentcredit = parseFloat($('#currentCredit').val().replace(/,/g, ''));
                            newbalance = parseFloat(currentcredit + creditnotevalue);
                            $('#currentCredit').val(numberWithCommas(parseFloat(newbalance).toFixed(2)));
                        }
                    } else {
                        currentcredit = parseFloat($('#currentCredit').val().replace(/,/g, ''));
                        $('#currentCredit').val(numberWithCommas(parseFloat(currentcredit - parseFloat(creditnotevalue)).toFixed(2)));
                        creditnotevalue = parseFloat(0).toFixed(2);
                        p_notification(false, eb.getMessage('ERR_PAYMENT_CUST_CREDIT'));
                        //p_notification(false, "Enter customer related credit note.");
                    }
                } else {
                    //p_notification(false, " " + values[0].creditNoteID + " this Credit Note Is close.");
                    p_notification(false, eb.getMessage('ERR_PAYMENT_CREDIT_CLOSE', " ", values[0].creditNoteID));
                    currentcredit = parseFloat($('#currentCredit').val().replace(/,/g, ''));
                    $('#currentCredit').val(numberWithCommas(parseFloat(currentcredit - creditnotevalue).toFixed(2)));
                    creditnotevalue = 0;
                }
            } else {
                currentcredit = parseFloat($('#currentCredit').val().replace(/,/g, ''));
                $('#currentCredit').val(numberWithCommas(parseFloat(currentcredit - parseFloat(creditnotevalue)).toFixed(2)));
                creditnotevalue = parseFloat(0).toFixed(2);
                //                p_notification(false, "Please enter valid credit note.");
            }

        });
    });

    $(document).on('change', '.paymentMethod', function() {
        $(".paymentMethod option[value='1']").attr('disabled', false);
        var paymentMethods = $(this).val();
        var $payMethod = $(this).parents('.payMethod');
        $("#cashGroup", $payMethod).hide();
        $("#paidAmount", $payMethod).attr('disabled', false);
        $("#creditGroup", $payMethod).hide();
        $("#checkGroup", $payMethod).hide();
        $("#bankTransferGroup", $payMethod).hide();
        $("#giftCardGroup", $payMethod).hide();
        $("#postdatedChequeGroup", $payMethod).hide();
        $("#lcGroup", $payMethod).hide();
        $("#ttGroup", $payMethod).hide();

        if(paymentMethods == 1){
            $("#cashGroup", $payMethod).show();
              $(".paymentMethod option[value='1']").attr('disabled', 'disabled');
        }else if (paymentMethods == 3) {
            $("#creditGroup", $payMethod).show();
        } else if (paymentMethods == 2) {
            $(this).parents('.payMethod').find("#creditGroup").hide();
            $(this).parents('.payMethod').find("#bankTransferGroup").hide();
            //for postdated cheque
            $(this).parents('.payMethod').find("#postdatedChequeGroup").show();
            $(this).parents('.payMethod').find("#checkGroup").show();

        } else if (paymentMethods == 4) {

            if (loyalty.isAvailable()) {
                $(this).parents('.payMethod').find("#creditGroup").hide();
                $(this).parents('.payMethod').find("#checkGroup").hide();
                $(this).parents('.payMethod').find("#bankTransferGroup").hide();
//                $(this).parents('.payMethod').find("#loyalty_details").show();
            } else {
                p_notification(false, eb.getMessage('ERR_DOESNT_HAVE_LOYALTY_CARD'));
                $('.paymentMethod').val('1').change();
            }



        } else if (paymentMethods == 5) {
            $("#bankTransferGroup", $payMethod).show();
        } else if (paymentMethods == 6) {
            $("#giftCardGroup", $payMethod).show();
            $("#paidAmount", $payMethod).val('').attr('disabled', true);
            $("#giftCardID", $payMethod).val('');
        } else if (paymentMethods == 7) {
            $("#lcGroup", $payMethod).show();
        } else if (paymentMethods == 8) {
            $("#ttGroup", $payMethod).show();
        }
    });

    $(document).on('change', '.checkAccountID', function() {
        var glAcc = $("option:selected", $(this)).attr('data-glaccount');
        var bankdiv = $(this).parents('#checkGroup');

        if (glAcc) {
            $('.main_div').addClass('Ajaxloading');
            eb.ajax({
                type: 'post',
                url: BASE_URL + '/accounts-api/getFinanceAccountsByAccountsID',
                data: {
                    financeAccountsID: glAcc
                },
                dataType: "json",
                async: false,
                success: function(data) {
                    if (data['status'] == true) {
                        var fName= data.data.financeAccounts.financeAccountsCode+'_'+data.data.financeAccounts.financeAccountsName

                        bankdiv.find('#chequeGlAccountID').append("<option value='"+glAcc+"'>"+fName+"</option>")
                        bankdiv.find('#chequeGlAccountID').val(glAcc).selectpicker('refresh');
                        
                    }
                    $('.main_div').removeClass('Ajaxloading');
                },
            });
        } else {
            bankdiv.find('.accountID').attr('disabled', true);
        }
    });


    $(document).on('change', '.accountID', function() {
        var glAcc = $("option:selected", $(this)).attr('data-glaccount');
        var bankdiv = $(this).parents('#bankTransferGroup');

        if (glAcc) {
            $('.main_div').addClass('Ajaxloading');
            eb.ajax({
                type: 'post',
                url: BASE_URL + '/accounts-api/getFinanceAccountsByAccountsID',
                data: {
                    financeAccountsID: glAcc
                },
                dataType: "json",
                async: false,
                success: function(data) {
                    if (data['status'] == true) {
                        var fName= data.data.financeAccounts.financeAccountsCode+'_'+data.data.financeAccounts.financeAccountsName

                        bankdiv.find('#bankTransferGlAccountID').append("<option value='"+glAcc+"'>"+fName+"</option>")
                        bankdiv.find('#bankTransferGlAccountID').val(glAcc).selectpicker('refresh');
                        
                    }
                    $('.main_div').removeClass('Ajaxloading');
                },
            });
        } else {
            bankdiv.find('.accountID').attr('disabled', true);
        }
    });



    $(document).on('change', '.bankID', function() {
        var bankdiv = $(this).parents('#bankTransferGroup');
        var bankID = $(this).val();
        var bank = $(this);
        var bankName = $("option:selected", $(this)).text();
        bankdiv.find('.accountID').find('option').each(function() {
            if ($(this).val()) {
                $(this).remove();
            }
        });
        if (bankID) {
            $('.main_div').addClass('Ajaxloading');
            eb.ajax({
                type: 'post',
                url: BASE_URL + '/account-api/bankAccountListForDropdown',
                data: {
                    bankId: bankID,
                    withGLAccount: true
                },
                dataType: "json",
                async: false,
                success: function(data) {
                    if (data['status'] == true) {
                        if ($.isEmptyObject(data['data'].list)) {
                            p_notification(false, eb.getMessage('ERR_DOESNT_HAVE_ANY_ACC_FOR_THIS_BANK', bankName));
                            $(bank).val('');
                            bankdiv.find('.accountID').attr('disabled', true);
                        } else {
                            $.each(data['data'].list, function(key, value) {
                                var option = "<option data-glaccount="+value.glAccountID+" value=" + value.value + ">" + value.text + "</option>";
                                bankdiv.find('.accountID').attr('disabled', false);
                                bankdiv.find('.accountID').append(option);
                            });
                        }
                    }
                    $('.main_div').removeClass('Ajaxloading');
                },
            });
        } else {
            bankdiv.find('.accountID').attr('disabled', true);
        }
    });

    $(document).on('change', '.giftCardID', function() {
        var giftCardID = $(this).val();
        var expireDate = GIFTCARDLIST[giftCardID].giftCardExpireDate;
        var value = GIFTCARDLIST[giftCardID].giftCardValue;
        var flag = 0;
        $('.giftCardID').each(function() {
            if (giftCardID == $(this).val()) {
                flag++;
            }
        });
        if (flag > 1) {
            p_notification(false, eb.getMessage('ERR_PAY_METHOD_GIFR_CARD_ALREDY_SELECT'));
            $(this).val('');
            value = 0.00;
        } else if (expireDate) {
            var exdate = new Date(expireDate);
            var today = new Date();
            if (exdate < today) {
                p_notification(false, eb.getMessage('ERR_PAY_METHOD_GIFR_CARD_EXPIRED'));
                $(this).val('');
                value = 0.00;
            }
        }
        $(this).parents('.payMethod').find(".paidAmount").val(value).trigger('change');
    });

    $('#paymentMethodAdd').on('click', function() {
        paymentMethodIncrement++;
        var NewPaymentMethodDiv = $('#payMethod_1').clone();
        NewPaymentMethodDiv.attr('id', 'payMethod_' + paymentMethodIncrement);
        var newID = 'payMethod_' + paymentMethodIncrement;

        $('.pamentMethodDelete', NewPaymentMethodDiv).removeClass('hidden');
        $('#creditGroup', NewPaymentMethodDiv).hide();
        $('#checkGroup', NewPaymentMethodDiv).hide();
        $('#bankTransferGroup', NewPaymentMethodDiv).hide();
        $('#giftCardGroup', NewPaymentMethodDiv).hide();
        $('#lcGroup', NewPaymentMethodDiv).hide();
        $('#ttGroup', NewPaymentMethodDiv).hide();
        $('#paidAmount', NewPaymentMethodDiv).val('').attr('disabled', false);
        $('#cashGroup', NewPaymentMethodDiv).show();

        var flag = 0;
        $(".paymentMethod", ".addPaymentMethod").each(function() {
            if ($(this).val() == 1)
                flag = 1;
        });
        if (flag == 1) {
            $('#cashGroup', NewPaymentMethodDiv).hide();
            $(".paymentMethod option[value='1']", NewPaymentMethodDiv).attr('disabled', 'disabled');
            $(".paymentMethod", NewPaymentMethodDiv).val(2);
            $('#checkGroup', NewPaymentMethodDiv).show();
        } else {
            $(".paymentMethod option[value='1']", '.addPaymentMethod').attr('disabled', 'disabled');
        }
        $('.addPaymentMethod').append(NewPaymentMethodDiv);
        //set selectpickers to the clonedPayment Methods
        $('#'+newID).find('#cashGlAccountID').removeData('AjaxBootstrapSelect');
        $('#'+newID).find('#cashGlAccountID').removeData('selectpicker');
        $('#'+newID).find('#cashGlAccountID').siblings('.bootstrap-select').remove();
        $('#'+newID).find('#cashGlAccountID').siblings('.bootstrap-select').find('.dropdown-menu').remove();
        loadDropDownFromDatabase('/accounts-api/search-active-accounts-for-dropdown', "", "", $('#'+newID).find('#cashGlAccountID'));

        $('#'+newID).find('#chequeGlAccountID').removeData('AjaxBootstrapSelect');
        $('#'+newID).find('#chequeGlAccountID').removeData('selectpicker');
        $('#'+newID).find('#chequeGlAccountID').siblings('.bootstrap-select').remove();
        $('#'+newID).find('#chequeGlAccountID').siblings('.bootstrap-select').find('.dropdown-menu').remove();
        loadDropDownFromDatabase('/accounts-api/search-active-accounts-for-dropdown', "", "", $('#'+newID).find('#chequeGlAccountID'));

        $('#'+newID).find('#creditGlAccountID').removeData('AjaxBootstrapSelect');
        $('#'+newID).find('#creditGlAccountID').removeData('selectpicker');
        $('#'+newID).find('#creditGlAccountID').siblings('.bootstrap-select').remove();
        $('#'+newID).find('#creditGlAccountID').siblings('.bootstrap-select').find('.dropdown-menu').remove();
        loadDropDownFromDatabase('/accounts-api/search-active-accounts-for-dropdown', "", "", $('#'+newID).find('#creditGlAccountID'));

        $('#'+newID).find('#bankTransferGlAccountID').removeData('AjaxBootstrapSelect');
        $('#'+newID).find('#bankTransferGlAccountID').removeData('selectpicker');
        $('#'+newID).find('#bankTransferGlAccountID').siblings('.bootstrap-select').remove();
        $('#'+newID).find('#bankTransferGlAccountID').siblings('.bootstrap-select').find('.dropdown-menu').remove();
        loadDropDownFromDatabase('/accounts-api/search-active-accounts-for-dropdown', "", "", $('#'+newID).find('#bankTransferGlAccountID'));

        $('#'+newID).find('#lcGlAccountID').removeData('AjaxBootstrapSelect');
        $('#'+newID).find('#lcGlAccountID').removeData('selectpicker');
        $('#'+newID).find('#lcGlAccountID').siblings('.bootstrap-select').remove();
        $('#'+newID).find('#lcGlAccountID').siblings('.bootstrap-select').find('.dropdown-menu').remove();
        loadDropDownFromDatabase('/accounts-api/search-active-accounts-for-dropdown', "", "", $('#'+newID).find('#lcGlAccountID'));

        $('#'+newID).find('#ttGlAccountID').removeData('AjaxBootstrapSelect');
        $('#'+newID).find('#ttGlAccountID').removeData('selectpicker');
        $('#'+newID).find('#ttGlAccountID').siblings('.bootstrap-select').remove();
        $('#'+newID).find('#ttGlAccountID').siblings('.bootstrap-select').find('.dropdown-menu').remove();
        loadDropDownFromDatabase('/accounts-api/search-active-accounts-for-dropdown', "", "", $('#'+newID).find('#ttGlAccountID'));


        //for postdated cheque
        $("#postdatedChequeGroup", NewPaymentMethodDiv).hide();
        $(".postdated-cheque-date", NewPaymentMethodDiv).hide();
        NewPaymentMethodDiv.find('.postdated-cheque').attr('checked', false);
        postdatedDate = $('.postdated-cheque-date').datepicker({
            format: 'yyyy-mm-dd',
            onRender: function(date) {
                return date.valueOf() < now.valueOf() ? ((U_DATE_R_OVERRIDE)?'':'disabled') : '';
            }
        }).on('changeDate', function(e) {
            postdatedDate.hide();
        }).data('datepicker');

        //addjust alignment
        $('.payMethod')
                .not('#payMethod_1')
                .addClass('panel panel-default');

        $('.payMethod .form-horizontal:first-child')
                .not('#payMethod_1 .form-horizontal:first-child')
                .addClass('panel-body');

    });

    $(document).on('click', '.pamentMethodDelete', function() {
        $(this).parents('.payMethod').remove();
        setCreditAmountAndRestToPaid();
    });

    $('.addPaymentMethod').on('keypress', 'input#paidAmount', function(e) {
        if (e.which == 13) {
            e.preventDefault();
        }
    });

    $('#creditAmount').on('change', function() {
        var creditAmount = $(this).val();
        if (suppliercredit == 0.00) {
            p_notification(false, eb.getMessage('ERR_PAY_CUST_NOCRDT_BAL'));
            $(this).val(0.00);
        } else if (isNaN(creditAmount) || creditAmount < 0) {
            p_notification(false, eb.getMessage('ERR_PAY_CREDIT_AMOUNT_SHOUDBE_NUMBER'));
            $(this).val(0.00);
        } else {
            var totalPaidAmount = parseFloat(getTotalPaidAmount());
            var creditAmountCanPay = paymentTotal - totalPaidAmount;
            if (creditAmountCanPay < creditAmount) {
                p_notification(false, eb.getMessage('ERR_PAY_CAMOUNT_AM_CANT_BE_MORE_THAN_RESTPAID'));
                $(this).val(0.00);
            }
        }
        setCreditAmountAndRestToPaid();

    });


     $('#paidAmount').on('change', function() {
        var paidAmount = $(this).val();
        var creditAmount = $('#creditAmount').val();
        if (creditAmount > 0) {
            var totalPaidAmount = parseFloat(getTotalPaidAmount());
            var restToPaid = paymentTotal - totalPaidAmount - creditAmount;
            if (restToPaid < 0) {
                p_notification('info', eb.getMessage('INFO_PAY_RESTPAID_CANT_BE_MORE_THAN_CAMOUNT'));
                var restToPaidValue = Math.abs(restToPaid);
                var newCreditAmount = creditAmount - restToPaidValue;
                if (newCreditAmount < 0) {
                    newCreditAmount = 0;
                }
                $('#creditAmount').val(newCreditAmount)
            }
        }
        setCreditAmountAndRestToPaid();
    });

    $(document).on('change', '.paidAmount', function() {
        var paidAmount = $(this).val();
        if (isNaN(paidAmount) || paidAmount < 0) {
            p_notification(false, eb.getMessage('ERR_PAY_PAID_AMOUNT_SHOUDBE_NUMBER'));
            $(this).val(0.00);
        } else {
            var creditAmount = $('#creditAmount').val();
            var restToPaidTotal = paymentTotal - creditAmount;
            var totalPaidAmount = parseFloat(getTotalPaidAmount());
            if (restToPaidTotal < totalPaidAmount) {
                restBalance = totalPaidAmount - restToPaidTotal;
            }
        }
        setCreditAmountAndRestToPaid();

    });


    $('#discountAmount').on('change', function() {
        var discount = $('#discountAmount').val();
        if (discount < 0 || isNaN(discount)) {
            p_notification(false, eb.getMessage('ERR_PAY_DISCOUNT_AMOUNT_SHOUDBE_NUMBER'));
            $(this).val(0.00);
        } else {
            if (paymentTotal <= discount) {
                p_notification(false, eb.getMessage('ERR_PAY_DISCOUNT_AMOUNT'));
                $(this).val(0.00);
            } else {
                var creditAmount = parseFloat(($('#creditAmount').val() != '') ? $('#creditAmount').val() : 0);
                var totalPaidAmount = parseFloat(getTotalPaidAmount());
                var discountCanGive = parseFloat(paymentTotal) - totalPaidAmount - creditAmount + oldDiscountAmount;
                console.log(discountCanGive);
                if (discountCanGive < discount) {
                    p_notification(false, eb.getMessage('ERR_PAY_DISC_AM_CANT_BE_MORE_THAN_RESTPAID'));
                    $(this).val(0.00);
                }
            }
        }
        setPaymentTotal();
    });


    function setPaymentTotal() {
        var totalOfPayment = 0.00;

        $('#paymentbill').find('tbody > tr').each(function() {
            if ($(this).find('.edi').val()) {
                totalOfPayment += parseFloat($(this).find('.edi').val().replace(/,/g, ''));
            }
        });
        paymentTotal = totalOfPayment;
        oldDiscountAmount = parseFloat(($('#discountAmount').val() != '') ? $('#discountAmount').val() : 0);
        if (oldDiscountAmount != 0 && paymentTotal != 0.00) {
            paymentTotal = paymentTotal - oldDiscountAmount;
        }
        $('#amount').val(numberWithCommas(totalOfPayment.toFixed(2)));
        $('#restToPaid').val(numberWithCommas(paymentTotal.toFixed(2)));
        $('#totalpayment').html(numberWithCommas(paymentTotal.toFixed(2)));

        var customerCurrentBalance = supplierbalance - totalOfPayment;
        if ($('#customCurrencyId').val() == '') {
            $('#currentBalance').val(numberWithCommas(customerCurrentBalance.toFixed(2)));
        }

        setCreditAmountAndRestToPaid();

    }




    function getTotalPaidAmount() {
        var totalPaidAmount = 0.00;
        $('.paidAmount').each(function() {
            totalPaidAmount += ($(this).val()) ? parseFloat($(this).val()) : 0.00;
        });
        return totalPaidAmount;
    }


    function setCreditAmountAndRestToPaid() {
        var creditAmount = parseFloat(($('#creditAmount').val() != '') ? $('#creditAmount').val() : 0);
        var customerCreditAmount = 0.00;
        var paidAmount = 0.00;
        if (creditAmount > paymentTotal) {
            $('#creditAmount').val(paymentTotal);
            creditAmount = paymentTotal;
        }

        if ((creditAmount * customCurrencyRate) > parseFloat(suppliercredit)) {
            p_notification(false, eb.getMessage('ERR_PAY_CUST_CREDIT_CNT_MO_THAN_AMOUNT'));
            $('#creditAmount').val(0.00);
            creditAmount = 0.00;
            customerCreditAmount = suppliercredit;
        } else {
            customerCreditAmount = (suppliercredit - creditAmount).toFixed(2);
        }

//get the total of paid amount form many payment methods
        var totalPaidAmount = getTotalPaidAmount();
        console.log(totalPaidAmount);

        paidAmount = paymentTotal - creditAmount - totalPaidAmount;
            $('#currentCredit').val(numberWithCommas(customerCreditAmount));
        // if ($('#customCurrencyId').val() == '') {
        // }
        $('#restToPaid').val(numberWithCommas(paidAmount.toFixed(2)));
    }




    function numberWithCommas(x) {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }

    var inv_id = $('#InvoiceID').attr('value');

    if (inv_id != "") {
        idInvoice = $('#idInvoice').val();
        $("#invoiceSearchType").val('PI ID').change();
        $('#InvoiceID').trigger("change");
        $("#InvoiceID").attr("disabled", "disabled");
        $("#supplierID").attr("disabled", "disabled");
        $(document).on("click", "#cancel", function () {
            window.history.back();
        });
    }

    var pv_id = $('#paymentVoucherID').attr('value');

    if (pv_id != "") {
        currentlySelectedPVID = $('#pvIdInvoice').val();
        $("#invoiceSearchType").val('PV ID').change();
        $('#paymentVoucherID').trigger("change");
        $("#paymentVoucherID").attr("disabled", "disabled");
        $("#supplierID").attr("disabled", "disabled");
        $(document).on("click", "#cancel", function () {
            window.history.back();
        });
    }

    var cust_id = $('#hsup').val();
    if (cust_id != "") {
        changeEvent(cust_id, "Customer");
        $("#InvoiceID").attr("disabled", "disabled");
        $("#supplierID").attr("disabled", "disabled");
        $(document).on("click", "#cancel", function () {
            window.history.back();
        });
    }

    $(document).on('change', '.chbankID', function() {
        var bankdiv = $(this).parents('#checkGroup');
        var bankID = $(this).val();
        var bank = $(this);
        var bankName = $("option:selected", $(this)).text();
        bankdiv.find('.checkAccountID').find('option').each(function() {
            if ($(this).val()) {
                $(this).remove();
            }
        });
        if (bankID) {
            $('.main_div').addClass('Ajaxloading');
            eb.ajax({
                type: 'post',
                url: BASE_URL + '/account-api/bankAccountListForDropdown',
                data: {
                    bankId: bankID,
                    withGLAccount: true
                },
                dataType: "json",
                async: false,
                success: function(data) {
                    if (data['status'] == true) {
                        if ($.isEmptyObject(data['data'].list)) {
                            p_notification(false, eb.getMessage('ERR_DOESNT_HAVE_ANY_ACC_FOR_THIS_BANK', bankName));
                            $(bank).val('');
                            bankdiv.find('.checkAccountID').attr('disabled', true);
                        } else {
                            $.each(data['data'].list, function(key, value) {
                                var option = "<option data-glaccount="+value.glAccountID+" value=" + value.value + ">" + value.text + "</option>";
                                bankdiv.find('.checkAccountID').attr('disabled', false);
                                bankdiv.find('.checkAccountID').append(option);
                            });
                        }
                    }
                    $('.main_div').removeClass('Ajaxloading');
                },
            });
        } else {
            bankdiv.find('.checkAccountID').attr('disabled', true);
        }
    });

}, jQuery.ajaxSettings.traditional = true);
