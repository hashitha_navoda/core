$(document).ready(function() {
    var functionality = 0;
    var usability = 0;
    var navigation = 0;
    var loadingTime = 0;
    $("#functionality").bind('rated', function(event, value) {
        functionality = value;
    });
    $("#usability").bind('rated', function(event, value) {
        usability = value;
    });
    $("#navigation").bind('rated', function(event, value) {
        navigation = value;
    });
    $("#loadingTime").bind('rated', function(event, value) {
        loadingTime = value;
    });
    $('#submitFeedback').on('click', function(e) {
        e.preventDefault();
        data = {
            satisfied: $('input:radio[name=satisfied]:checked').val(),
            functionality: functionality,
            usability: usability,
            navigation: navigation,
            loadingTime: loadingTime,
            recommend: $('input:radio[name=recommend]:checked').val(),
            suggestions: $('#suggestions').val()
        }
        var feedbackUrl = BASE_URL + '/userAPI/sendUserFeedback';
        $('#userFeedback').addClass('Ajaxloading');
        eb.ajax({
            type: 'POST',
            url: feedbackUrl,
            data: data,
            success: function(res) {
                if (res.data == true) {
                    p_notification(res.status, res.msg);
                    $(".rateit-reset").trigger(jQuery.Event("click"))
                    $('#suggestions').val('');
                    $('#userFeedback').modal('hide');
                }
                $('#userFeedback').removeClass('Ajaxloading');
            }});
    });

});