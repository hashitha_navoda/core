$(document).ready(function() {

// Search Sales Person
    var searchurl = BASE_URL + '/salesPerson/searchSalesPerson';
    $('form.sales-person-search-form').submit(function(e) {
        e.preventDefault();
        var searchrequest = eb.post(searchurl, {keyword: $('#searchSalesPerson').val()});
        searchrequest.done(function(searchdata) {
            if (searchdata.status) {
                $('#salesPerson-list').html(searchdata.html);
            } else {
                p_notification(searchdata.status, eb.getMessage('ERR_SALEP_NORETRIEVE'));
            }
        });
        return false;
    });
    $('form.sales-person-search-form button.reset').on('click', function() {
        $('#searchSalesPerson').val('');
        $('form.sales-person-search-form').submit();
    });


    $('#reset-sales-person-button').on('click', function(e1) {
        e1.preventDefault();
//        $("#create-user-form").get(0).reset();
        $('#salesPersonSortName').val('');
        $('#salesPersonSortName').focus();
        $('#salesPersonTitle').val('');
        $('#salesPersonTitle').selectpicker('refresh')
        $('#salesPersonFirstName').val('');
        $('#salesPersonLastName').val('');
        $('#salesPersonEmail').val('');
        $('#salesPersonTelephoneNumber').val('');
        $('#salesPersonAddress').val('');
    });


    $('#salesPerson-list').on('click', '.changeStatus', function () {
        var salesPersonID = $(this).attr('id');
        var $statusSpan = $(this).find("span");
        var changeStatusTo = ($statusSpan.attr('class') == 'glyphicon glyphicon-check') ? 'inactive' : 'active';
        eb.ajax({
            type: 'POST',
            url: BASE_URL + '/salesPerson/changeSalesPersonStatus',
            data: {salesPersonID: salesPersonID, changeStatusTo: changeStatusTo},
            success: function (respond) {
                if (respond.status) {
                    if (changeStatusTo == 'inactive') {
                        $statusSpan.removeClass('glyphicon glyphicon-check');
                        $statusSpan.addClass('glyphicon glyphicon-unchecked');
                    } else {
                        $statusSpan.removeClass('glyphicon glyphicon-unchecked');
                        $statusSpan.addClass('glyphicon glyphicon-check');
                    }
                }
            }
        });
    });

    $('#list-sales-person-button').on('click', function(e) {
        e.preventDefault();
        window.open(BASE_URL + '/salesPerson', "_self");
    });

    $('#update-sales-person-button').on('click', function(e) {
        e.preventDefault();
        if (validateInputSalesPersonData() === true) {
            var param = {
                salesPersonID: $('#salesPersonID').val(),
                salesPersonSortName: $('#salesPersonSortName').val(),
                salesPersonTitle: $('#salesPersonTitle').val(),
                salesPersonFirstName: $('#salesPersonFirstName').val(),
                salesPersonLastName: $('#salesPersonLastName').val(),
                salesPersonEmail: $('#salesPersonEmail').val(),
                salesPersonTelephoneNumber: $.trim($('#salesPersonTelephoneNumber').val()),
                salesPersonAddress: $('#salesPersonAddress').val(),
                activeUserId: $('#salesPersonActiveUser').val(),
            };
            var editurl = BASE_URL + '/salesPerson/edit-save';
            eb.ajax({
                type: 'POST',
                url: editurl,
                data: param,
                success: function(res) {
                    if (res.status) {
                        window.open(BASE_URL + '/salesPerson/index', "_self");
                    } else {
                        p_notification(res.status, res.msg);
                    }
                },
            });
        }
    });
    $('#create-sales-person-button').on('click', function(e1) {
        e1.preventDefault();
        if (validateInputSalesPersonData() === true) {

            var param = {
                salesPersonSortName: $('#salesPersonSortName').val(),
                salesPersonTitle: $('#salesPersonTitle').val(),
                salesPersonFirstName: $('#salesPersonFirstName').val(),
                salesPersonLastName: $('#salesPersonLastName').val(),
                salesPersonEmail: $('#salesPersonEmail').val(),
                salesPersonTelephoneNumber: $.trim($('#salesPersonTelephoneNumber').val()),
                salesPersonAddress: $('#salesPersonAddress').val(),
                activeUserId: $('#salesPersonActiveUser').val(),
            };
            var saveurl = BASE_URL + '/salesPerson/save';
            eb.ajax({
                type: 'POST',
                url: saveurl,
                data: param,
                success: function(res) {
                    if (res.status) {
                        window.open(BASE_URL + '/salesPerson/index', "_self");
                    } else {
                        p_notification(res.status, res.msg);
                    }
                },
            });
        }
    });
    $('#salesPerson-list').on('click', 'a', '#salesPerson-list', function() {
        var salesPersonID = $(this).contents().context.id;
        if (salesPersonID.split('-')[1] === 'delete') {
            var deleteurl = BASE_URL + '/salesPerson/delete/' + salesPersonID.split('-')[2];
            bootbox.confirm("Are you sure you want to delete this Sales Person?", function(result) {
                if (result === true) {
                    getViewAndLoad(deleteurl, 'salesPerson-list', {}, function(res) {
                        p_notification(res.status, res.msg);
                    });
                }
            });
            return false;
        }
    });
});
function validateInputSalesPersonData() {
    var sName = $('#salesPersonSortName').val();
    var fName = $('#salesPersonFirstName').val();
    var lName = $('#salesPersonLastName').val();
    var email = $('#salesPersonEmail').val();
    var tp = $.trim($('#salesPersonTelephoneNumber').val());
    var isEmail = true;
    if ($.trim(email) === "") {
        isEmail = false;
    }
    var isTp = true;
    if (tp === "") {
        isTp = false;
    }

    var atpos = email.indexOf("@");
    var dotpos = email.lastIndexOf(".");
    if (sName === "") {
        document.getElementById('salesPersonSortName').focus();
        p_notification(false, eb.getMessage('ERR_SALEP_SHORTN_ENTER'));
    } else if (fName === null || $.trim(fName) === "") {
        document.getElementById('salesPersonFirstName').focus();
        p_notification(false, eb.getMessage('ERR_SALEP_FNAME_EMPTY'));
    } else if (lName === null || $.trim(lName) === "") {
        document.getElementById('salesPersonLastName').focus();
        p_notification(false, eb.getMessage('ERR_SALEP_LNAME_EMPTY'));
    } else if (isEmail &&
            (atpos < 1 || dotpos < atpos + 2 || dotpos + 2 >= email.length)) {
        document.getElementById('salesPersonEmail').focus();
        p_notification(false, eb.getMessage('ERR_SALEP_EMAIL_VALID'));
    } else if (isNaN(tp)) {
        document.getElementById('salesPersonTelephoneNumber').focus();
        p_notification(false, eb.getMessage('ERR_WIZARD_TELENUM'));
    } else if (isTp && tp.length < 10 || tp.length > 13) {
        document.getElementById('salesPersonTelephoneNumber').focus();
        p_notification(false, eb.getMessage('ERR_WIZARD_PHONENO_LIMIT'));
    } else {
        return true;
    }
}

