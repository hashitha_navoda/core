$(document).ready(function() {

    $("#accordion h4 input[type='checkbox']").click(function() {
        $(this).parents('.panel').find("input[type='checkbox']").prop('checked', $(this).prop('checked'));
    });

    $("#accordionModules h4 input[type='checkbox']").click(function() {
        $(this).parents('.modulePanel').find("input[type='checkbox']").prop('checked', $(this).prop('checked'));
    });

    $('.roleSave').on('click', function() {
        var roleName = this.id;
        var updateCheckedFeatureList = Array();
        $($(this).parents('.panel').find("input[type='checkbox'].featureCheckList")).each(function() {
            if (this.checked) {
                updateCheckedFeatureList.push(this.id);
            }
        });

        if (jQuery.isEmptyObject(updateCheckedFeatureList)) {
            //p_notification(false, "You didn't select any feature");
        } else {
            showLoader();
            eb.ajax({
                type: 'POST',
                url: BASE_URL + '/userAPI/changeRolePermission',
                data: {roleName: roleName, featureList: updateCheckedFeatureList},
                success: function(respond) {
                    hideLoader();
                    if (respond.data == true) {
                        window.location.reload();
                    }
                },
                async: false
            });
        }
    });

    $('.roleDelete').on('click', function() {
        var roleName = this.id;
        eb.ajax({
            type: 'POST',
            url: BASE_URL + '/userAPI/roleDelete',
            data: {roleName: roleName},
            success: function(respond) {
                if (respond.data == true) {
                    window.location.reload();
                } else {
                    p_notification(respond.status, respond.msg);
                }
            },
            async: false
        });
    });

    $('#addRole').on('click', function() {
        if ($('#roleName').val() == '') {
            p_notification(false, eb.getMessage('ERR_UPERM_ROLE'));
        } else {
            var roleName = $('#roleName').val();
            var checkedFeatureList = Array();
            $('.featureCheckList').each(function() {
                if (this.checked) {
                    checkedFeatureList.push(this.id);
                }
            });

            if (jQuery.isEmptyObject(checkedFeatureList)) {
                p_notification(false, eb.getMessage('ERR_UPERM_NOFEATURE'));
            } else {
                showLoader();
                eb.ajax({
                    type: 'POST',
                    url: BASE_URL + '/userAPI/addRole',
                    data: {roleName: roleName, featureList: checkedFeatureList},
                    success: function(respond) {
                        hideLoader();
                        if (respond.data == false) {
                            p_notification(respond.status, respond.msg);
                        } else {
                            window.location.reload();
                        }
                    },
                    async: false
                });
            }
        }
    });
    //responsive layout issue fix
    $(window).bind('resize ready', function() {
        if ($(window).width() <= 480) {
            $('.role_name').removeClass('col-xs-8').addClass('col-xs-12 remove_right_padding').css('margin-bottom', '12px');
            $('#addRole').removeClass('col-xs-4').addClass('col-xs-12');
            $('#addRoleForm').removeClass('margin_top');
        } else {
            $('.role_name').addClass('col-xs-8').removeClass('col-xs-12 remove_right_padding').css('margin-bottom', '0px');
            $('#addRole').addClass('col-xs-4').removeClass('col-xs-12');
            $('#addRoleForm').addClass('margin_top');
        }
    });
});