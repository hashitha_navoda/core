$(document).ready(function () {
    $(function () {
        updateUserLicenseUI(noOfEmptyLicense, noOfAssignedUser);

        var initialize_draggable = function () {
            $('#licenses li').draggable({
                revert: "invalid",
                start: function () {
                    $(this).addClass('drag-size');
                },
                stop: function () {
                    $(this).removeClass('drag-size');
                },
            });

            if ($('.licence-position.ui-droppable').length) {
                $('.licence-position.ui-droppable').droppable('destroy');
            }

            $('.licence_wrapper:has(span)').removeClass('hide', function () {
                $(this).parents('.licence-position').find('.deleteUserLicese').removeClass('hide');
            });


            $('.licence-position:not(:has(span))').droppable({
                activeClass: "ui-state-default",
                hoverClass: "ui-state-hover",
                accept: ":not(.ui-sortable-helper)",
                drop: function (event, ui) {
                    var currentUserID = $(this).attr('id');
                    var currentLicenseID = ui.draggable.find('span').attr('data-id');
                    var $license = ui.draggable.find('span').clone();
                    var $licensePosition = $(this);

                    $license.appendTo($licensePosition.find('.licence_wrapper'));
                    ui.draggable.remove();
                    $licensePosition.find('.licence_wrapper').removeClass('hide');
                    $licensePosition.find('.deleteUserLicese').removeClass('hide');
                    initialize_draggable();

                    var deleteURL = BASE_URL + '/user/updateUserLicense';
                    eb.ajax({
                        type: 'POST',
                        url: deleteURL,
                        data: {
                            userID: currentUserID,
                            licenseID: currentLicenseID
                        },
                        success: function (res) {
                            if (res.status) {
                                $('#noOfEmptyLicense').html(res.data['noOfEmptyLicense']);
                                $('#noOfAssignedUser').html(res.data['noOfAssignedUser']);
                                updateUserLicenseUI(res.data['noOfEmptyLicense'], res.data['noOfAssignedUser']);
                                initialize_draggable();
                            } else {
                                location.reload();
                            }
                            p_notification(res.status, res.msg);
                        },
                    });
                }
            });
            return false;
        };

        initialize_draggable();

        $('.deleteUserLicese').on('click', function (e) {
            e.preventDefault();
            updateUserLicenseUI(1);
            var $current_user = $(this).parents('.licence-position').find('.licence_wrapper');
            var $deleteUserLicense = $(this);
            var currentUserID = $current_user.parents('.licence-position').attr('id');

            $deleteUserLicense.parents('.licence-position').find('.licence_wrapper').addClass('hide');
            $deleteUserLicense.addClass('hide');

            var $label = $('<li>').append($current_user.find('span'));
            $('#licenses ol').append($label);
            $('#licenses ol li:last').append('<i class="fa fa-arrows"></i>');
            $current_user.find('span').remove();
//            initialize_draggable();

            var deleteURL = BASE_URL + '/user/removeUserLicense';
            eb.ajax({
                type: 'POST',
                url: deleteURL,
                data: {userID: currentUserID},
                success: function (res) {
                    p_notification(res.status, res.msg);
                    if (res.status) {
                        $('#noOfEmptyLicense').html(res.data['noOfEmptyLicense']);
                        $('#noOfAssignedUser').html(res.data['noOfAssignedUser']);
                        updateUserLicenseUI(res.data['noOfEmptyLicense'], res.data['noOfAssignedUser']);
                        initialize_draggable();
                    } else {
                        setTimeout("location.reload()", 1000);

                    }
                },
            });
        });

        $('#smartUpdateUserLicense').on('click', function () {
            var smartupdateURl = BASE_URL + '/user/smartUpdateUserLicense';
            eb.ajax({
                type: 'POST',
                url: smartupdateURl,
                data: {},
                success: function (res) {
                    p_notification(res.status, res.msg);
                    location.reload();
                },
            });
        });

        $('#smartDeleteUserLicense').on('click', function () {
        	bootbox.confirm("Are you sure you want to remove all assigned licenses?", function(result) {
        		if (result === true) {
        			var smartupdateURl = BASE_URL + '/user/smartRemoveUserLicense';
        			eb.ajax({
        				type: 'POST',
        				url: smartupdateURl,
        				data: {},
        				success: function (res) {
        					p_notification(res.status, res.msg);
        					location.reload();
        				},
        			});
        		}
        	});
        });

    });
});

function updateUserLicenseUI(noOfEmptyLicense, noOfAssignedUser) {
    if (noOfEmptyLicense == 0) {
        if ($('#note').hasClass('hide')) {
            $('#note').removeClass('hide');
        }
        if ($('#smartDeleteUserLicense').hasClass('disabled')) {
            $('#smartDeleteUserLicense').removeClass('disabled')
        }
        if (!$('#smartUpdateUserLicense').hasClass('disabled')) {
            $('#smartUpdateUserLicense').addClass('disabled')
        }
    } else if (typeof noOfAssignedUser !== 'undefined' && noOfAssignedUser == '1') {
        if (!$('#smartDeleteUserLicense').hasClass('disabled')) {
            $('#smartDeleteUserLicense').addClass('disabled')
        }
    }
    else {
        if (!$('#note').hasClass('hide')) {
            $('#note').addClass('hide');
        }
        if ($('#smartUpdateUserLicense').hasClass('disabled')) {
            $('#smartUpdateUserLicense').removeClass('disabled')
        }
        if ($('#smartDeleteUserLicense').hasClass('disabled')) {
            $('#smartDeleteUserLicense').removeClass('disabled')
        }
    }
}


