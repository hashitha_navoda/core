$(document).ready(function() {
    var userCreateViewLoaded = false;
    $('#passwordChange').hide();

    $('form.user-search-form').submit(function(e) {
        e.preventDefault();
        var param = {searchKey: $('#searchuser').val()};
        eb.ajax({
            type: 'POST',
            url: BASE_URL + '/userAPI/getuserfromsearch',
            data: param,
            success: function(respond) {
                $('#user-list').html(respond.html);
            }
        });
    });

    $('form.user-search-form button.reset').on('click', function() {
        $('#searchuser').val('');
        $('form.user-search-form').submit();
    });

    $('#updateclose').on('click', function() {
        window.history.back();
    });

    $('#userUpdateReset').on('click', function() {
        window.location.reload();
    });

    if ($("#userID").val()) {
        $('#userTypeID').prop("disabled", true);
    }


    if ($('#userTypeID').val() == 2) {
        $("#role-div").addClass("hidden");
        $("#licen-div").addClass("hidden");
    } else {
        // $('#userTypeID').prop("disabled", true);
        $("#role-div").removeClass("hidden");
        $("#licen-div").removeClass("hidden");
    }

    var userupdateurl = BASE_URL + '/userAPI/userUpdate';
    $(document).on("click", "#userupdatebutton", function(e) {
        e.preventDefault();
        var u;
        if ($('#passwordChange').is(':visible') || $('#password').val() || $('#passwordC').val()) {
            u = new Array(
                    $('#firstName').val(),
                    $('#lastName').val(),
                    $('#email1').val(),
                    $('#password').val(),
                    $('#passwordC').val()
                    );
        } else {
            u = new Array(
                    $('#username').val(),
                    $('#email1').val()
                    );
        }
        if (validateuserinput(u)) {
            var param = {
                userID: $('#userID').val(),
                userFirstName: $('#firstName').val(),
                userLastName: $('#lastName').val(),
                userEmail1: $('#email1').val(),
                roleID: $('#roleID').val(),
                userTypeID: $('#userTypeID').val(),
                licenseID: $('#licenseID').val(),
                currentPassword: $('#currentPassword').val(),
                userPassword: $('#password').val(),
                passwordC: $('#passwordC').val()
            };

            eb.ajax({
                type: 'POST',
                url: userupdateurl,
                data: param,
                success: function(result) {
                    if (result.data == true) {
                        var url1 = BASE_URL + '/user';
                        window.location.assign(url1);
                    } else {
                        p_notification(result.status, result.msg);
                    }
                },
            });
        }
    });



    $('#userTypeID').on('change', function(e1) {
        if ($('#userTypeID').val() == 2) {
            $("#role-div").addClass("hidden");
        } else {
            $("#role-div").removeClass("hidden");
        }
    });

    $('#create-user-form').on('submit', function(e1) {
        e1.preventDefault();

        if (validateUserName() === false) {
            if (validateuserinputUserCreate() === true) {
                $('#user-create').addClass('hidden');
                if (userCreateViewLoaded == false) {
                    getViewAndLoad('/userAPI/getUserLocationAllocateView', 'user-create-location');
                    userCreateViewLoaded = true;
                } else {
                    $('#user-create-location').removeClass('hidden');
                }
            }
        }
    });

    $('#user-create-location').on('click', '#save-user-location', createUserLocations);
    $('#save-user-location-form').on('submit', createUserLocations);

    function createUserLocations(e2) {
        e2.preventDefault();
        var param1 = {
            userUsername: $('#userUsername').val(),
            userFirstName: $('#userFirstName').val(),
            userLastName: $('#userLastName').val(),
            userEmail1: $('#userEmail1').val(),
            userTypeID: $("#userTypeID").val(),
            roleID: $('#roleID').val(),
            licenseID: $('#licenseID').val(),
            userPassword: $('#userPassword').val(),
            userPasswordC: $('#userPasswordC').val()
        };
        var param2 = new Array;
        var validatelocation = false;
        var inputElements = document.getElementsByTagName('input');
        for (var i = 0; inputElements[i]; ++i) {
            if (inputElements[i].className === "locationCheckbox" && inputElements[i].checked) {
                param2.push(inputElements[i].value);
            }
        }
        var defautLocation = $('input[name=defaultLocation]:checked').val();
        if (typeof defautLocation != 'undefined') {
            var param3 = defautLocation.split('_')[1];
        }
        if (param3) {
            eb.ajax({
                type: 'post',
                url: BASE_URL + '/userAPI/checkDefaultLocation',
                data: {
                    locationID: param3,
                },
                success: function(data) {
                    if (data.status == true) {
                        validatelocation = true;
                    }
                },
                async: false
            });
        }
        if (param2.length === 0) {
            p_notification(false, eb.getMessage('ERR_USER_LOC'));
        } else if (defautLocation === undefined) {
            p_notification(false, eb.getMessage('ERR_USER_DEFLOC'));
        } else if (!$('#' + param3).prop('checked')) {
            p_notification(false, eb.getMessage('ERR_USER_CHECK_DEF_LOC'));
        }
        else if (validatelocation) {
            p_notification(false, eb.getMessage('ERR_USER_INACT_DEF_LOC'));
        }
        else {
            var param3 = defautLocation.split('_')[1];
            param1.defautLocation = param3;

            var param = {user: param1, location: param2};

            var url2 = BASE_URL + '/user/add';

            eb.ajax({
                type: 'POST',
                url: url2,
                data: param,
                success: function(res) {
                    if (res.status) {
                        var url2 = BASE_URL + '/user/Index';
                        window.location.assign(url2);
                    } else {
                        p_notification(res.status, res.msg);
                    }
                },
                async: false
            });
        }
    }

    $('#user-location-reset').on('click', function() {
        $('.locationCheckbox').prop('checked', false);
        $('.defaultLocation').prop('checked', false);
        $('.defaultLocation').prop('disabled', true);
    });


    $('#user-create-location').on('click', '#list-user-button', function() {
        $('#user-create-location').addClass('hidden')
        $('#user-create').removeClass('hidden');
    });

    $('#list-user-button').on('click', function() {
        window.history.back();
    });

    $('#user-create').on('click', '#userCreateBack', function(e) {
        e.preventDefault();
        window.history.back();
    });

    $('#user-list').on('click', '#reset-user-button', function() {
        $("#create-user-form").get(0).reset();
    });

    $('#user-list').on('click', '#reset-user-location', function() {
        $("#save-user-location-form").get(0).reset();
    });

    $(document).on('click', 'a', "#user-list", function() {
        var userID = $(this).contents().context.id;
        if (userID.split('-')[1] === 'delete') {
            var url = BASE_URL + '/user/delete/' + userID.split('-')[2];
            bootbox.confirm("Are you sure you want to delete this user?", function(result) {
                if (result === true) {
                    getViewAndLoad(url, 'user-list', {}, function(res) {
                        p_notification(res.status, res.msg);
                    });
                }
            });
            return false;

        } else if (userID.split('-')[1] === 'status') {
            var userID = userID.split('-')[2];
            var $statusIcon = $(this).contents();
            var msg = '';
            if ($statusIcon.hasClass('fa-check-square-o')) {
                msg = "Are you sure you want to deactivate this user?";
            } else {
                msg = "Are you sure you want to activate this user?";
            }
            bootbox.confirm(msg, function(result) {
                if (result === true) {
                    eb.ajax({
                        type: 'POST',
                        url: BASE_URL + '/userAPI/changeUserStatus',
                        data: {
                            userID: userID,
                        },
                        success: function(result) {
                            if (result.status === true) {
                                if (result.data === 'checked') {
                                    $statusIcon.removeClass('fa-square-o');
                                    $statusIcon.addClass('fa-check-square-o');
                                } else if (result.data === 'unChecked') {
                                    $statusIcon.removeClass('fa-check-square-o');
                                    $statusIcon.addClass('fa-square-o');
                                } else {
                                    var portalURL = result.data;
                                    bootbox.confirm("Are going to buy new licence?", function(result) {
                                        if (result === true) {
                                            window.open(portalURL);
                                        }
                                    });
                                    return false;
                                }
                            }
                            p_notification(result.status, result.msg);
                        },
                    });
                }
            });
            return false;
        }
    });
    $(document).on('click', '#passwordChangeActivate', function() {
        $('#passwordChange').slideDown();
        $('#psssword_icon').removeClass("glyphicon-plus-sign");
        $('#psssword_icon').addClass("glyphicon-minus-sign");
        this.id = "passwordChangeDeactivate";
    });
    $(document).on('click', '#passwordChangeDeactivate', function() {
        $('#passwordChange').slideUp();
        $('#psssword_icon').removeClass("glyphicon-minus-sign");
        $('#psssword_icon').addClass("glyphicon-plus-sign");
        this.id = "passwordChangeActivate";
    });

    $('#update-user-button').on('click', function(e) {
        e.preventDefault();
        var u;
        if ($('#passwordChange').is(':visible') || $('#password').val() || $('#passwordC').val()) {
            u = new Array($('#email1').val(),
                    $('#currentPassword').val(),
                    $('#password').val(),
                    $('#passwordC').val()
                    );
        } else {
            u = new Array($('#username').val(),
                    $('#email1').val()
                    );
        }

        if (validateUpdateUser(u)) {

            var url1 = BASE_URL + '/userAPI/userUpdate';

            var param = $('#user-profile-edit-form').serialize();
            param = param +'&userTypeID='+$("#userTypeID").val();
            var submitForm = eb.post(url1, param);
            submitForm.done(function(data) {
                if (data.status === true) {
                    window.location.reload();
                }
                else {
                    p_notification(false, data.msg);
                }
            });
        }
    });

    //responsive issue fix
    $(window).bind('ready resize', function() {
        if ($(window).width() < 480) {
            $('#update-user-button, #userupdatebutton').addClass('col-xs-12').css('margin-bottom', '8px');
            $('#updateclose').addClass('col-xs-12');
        } else {
            $('#update-user-button, #userupdatebutton').removeClass('col-xs-12').css('margin-bottom', '0px');
            $('#updateclose').removeClass('col-xs-12');
        }
    });

    $(document).on('click', '.locationCheckbox', function() {
        if ($(this).prop('checked')) {
            $('#default_' + this.id).prop('disabled', false);
        } else {
            $('#default_' + this.id).prop('disabled', true);
            $('#default_' + this.id).prop('checked', false);
        }
    });

    $(document).on('click', '#update-user-location', function(e) {
        e.preventDefault();
        var userID = $('#updateLocationUser').val();
        var param2 = new Array;
        var inputElements = document.getElementsByTagName('input');
        for (var i = 0; inputElements[i]; ++i) {
            if (inputElements[i].className === "locationCheckbox" && inputElements[i].checked) {
                param2.push(inputElements[i].value);
            }
        }

        var defautLocation = $('input[name=defaultLocation]:checked').val();
        if (typeof defautLocation != 'undefined') {
            var param3 = defautLocation.split('_')[1];
        }
        if (param2.length === 0) {
            p_notification(false, eb.getMessage('ERR_USER_LOC'));
        } else if (defautLocation === undefined) {
            p_notification(false, eb.getMessage('ERR_USER_DEFLOC'));
        } else if (!$('#' + param3).prop('checked')) {
            p_notification(false, eb.getMessage('ERR_USER_CHECK_DEF_LOC'));
        } else {
            var param1 = {
                userID: userID,
                defautLocation: param3
            };

            var param = {user: param1, location: param2};

            var url2 = BASE_URL + '/user/updateLocations';
            getViewAndLoad(url2, 'user-list', param, function(res) {
                $('#user-header').show();
                p_notification(res.status, res.msg);

                if (res.status == true) {
                    window.setTimeout(function() {
                        var defaultUrl = BASE_URL + '/user/';
                        window.location.assign(defaultUrl);
                    }, 3000);
                }
            });
        }
    });

});
function validateuserinput() {
    var firstName = $('#firstName').val();
    var lastName = $('#lastName').val();
    var email = $('#email1').val();
    var currentPass = $('#currentPassword').val();
    var pass = $('#password').val();
    var passC = $('#passwordC').val();
    if (!$('#passwordChange').is(':visible') && $('#password').val() === $('#passwordC').val()) {
        pass = passC = currentPass = "pwd";
    }

    if (typeof currentPass === "undefined") {
        currentPass = "pwd";
    }
    
    var atpos = email.indexOf("@");
    var dotpos = email.lastIndexOf(".");
    var passFormat = /^(?=.*[A-Z])(?=.*[a-z])(?=.*\d).{8,20}$/;

    if (firstName === null || firstName === "") {
        $('#firstName').focus();
        p_notification(false, eb.getMessage('ERR_USER_FNAME_EMPTY'));
    } else if (lastName === null || lastName === "") {
        $('#lastName').focus();
        p_notification(false, eb.getMessage('ERR_USER_LNAME_EMPTY'));
    } else if (email === null || email === "") {
        $('#email1').focus();
        p_notification(false, eb.getMessage('ERR_USER_EMAIL_EMPTY'));
    } else if (atpos < 1 || dotpos < atpos + 2 || dotpos + 2 >= email.length) {
        $('#email1').focus();
        p_notification(false, eb.getMessage('ERR_USER_EMAIL_VALID'));
    } else if (currentPass === null || currentPass === "") {
        $('#currentPassword').focus();
        p_notification(false, eb.getMessage('ERR_USER_CURR_PASS'));
    } else if (pass === null || pass === "") {
        $('#password').focus();
        p_notification(false, eb.getMessage('ERR_USER_ENTER_PASS'));
    } else if (pass != "pwd" && !pass.match(passFormat)) {
        $('#password').focus();
        p_notification(false, eb.getMessage('ERR_USER_PASS_FORMAT'));
    } else if (passC === null || passC === "") {
        $('#passwordC').focus();
        p_notification(false, eb.getMessage('ERR_USER_PASS'));
    } else if (pass !== passC) {
        $('#passwordC').focus();
        p_notification(false, eb.getMessage('ERR_USER_PASS_MATCH'));
    }
    else {
        return true;
    }

}


function validateUpdateUser() {
    var firstName = $('#firstName').val();
    var lastName = $('#lastName').val();
    var email = $('#email1').val();
    var currentPass = $('#currentPassword').val();
    var pass = $('#password').val();
    var passC = $('#passwordC').val();
    if (!$('#passwordChange').is(':visible') && $('#password').val() === $('#passwordC').val()) {
        pass = passC = currentPass = "pwd";
    }

    if (typeof currentPass === "undefined") {
        currentPass = "pwd";
    }

    var atpos = email.indexOf("@");
    var dotpos = email.lastIndexOf(".");
    if (firstName === null || firstName === "") {
        p_notification(false, eb.getMessage('ERR_USER_FNAME_EMPTY'));
    } else if (lastName === null || lastName === "") {
        p_notification(false, eb.getMessage('ERR_USER_LNAME_EMPTY'));
    } else if (email === null || email === "") {
        p_notification(false, eb.getMessage('ERR_USER_EMAIL_EMPTY'));
    } else if (atpos < 1 || dotpos < atpos + 2 || dotpos + 2 >= email.length) {
        p_notification(false, eb.getMessage('ERR_USER_EMAIL_VALID'));
    } else if (currentPass === null || currentPass === "") {
        p_notification(false, eb.getMessage('ERR_USER_CURR_PASS'));
    } else if (pass === null || pass === "") {
        p_notification(false, eb.getMessage('ERR_USER_ENTER_PASS'));
    } else if (passC === null || passC === "") {
        p_notification(false, eb.getMessage('ERR_USER_PASS'));
    } else if (pass !== passC) {
        p_notification(false, eb.getMessage('ERR_USER_PASS_MATCH'));
    }
    else {
        return true;
    }

}

function validateuserinputUserCreate() {
    var email = $('#userEmail1').val();
    var pass = $('#userPassword').val();
    var passC = $('#userPasswordC').val();
    var uName = $('#userUsername').val();
    var fName = $('#userFirstName').val();
    var lName = $('#userLastName').val();

    var atpos = email.indexOf("@");
    var dotpos = email.lastIndexOf(".");

    if (uName === "") {
        $('#userUsername').focus();
        p_notification(false, eb.getMessage('ERR_USER_UNAME'));
    } else if (uName.length < 5) {
        $('#userUsername').focus();
        p_notification(false, eb.getMessage('ERR_USER_UNAME_LIMIT'));
    } else if (fName === null || fName === "") {
        $('#userFirstName').focus();
        p_notification(false, eb.getMessage('ERR_SALEP_FNAME_EMPTY'));
    } else if (lName === null || lName === "") {
        $('#userLastName').focus();
        p_notification(false, eb.getMessage('ERR_SALEP_LNAME_EMPTY'));
    } else if (email === null || email === "") {
        $('#userEmail1').focus();
        p_notification(false, eb.getMessage('ERR_USER_EFIELD_EMPTY'));
    } else if (atpos < 1 || dotpos < atpos + 2 || dotpos + 2 >= email.length) {
        $('#userEmail1').focus();
        p_notification(false, eb.getMessage('ERR_SALEP_EMAIL_VALID'));
    } else if (pass === null || pass === "") {
        $('#userPassword').focus();
        p_notification(false, eb.getMessage('ERR_USER_ENTER_PASS'));
    } else if (pass.length < 6) {
        $('#userPassword').focus();
        p_notification(false, eb.getMessage('ERR_USER_PASS_LIMIT'));
    } else if (passC === null || passC === "") {
        $('#userPasswordC').focus();
        p_notification(false, eb.getMessage('ERR_USER_PASS'));
    } else if (pass !== passC) {
        $('#userPasswordC').focus();
        p_notification(false, eb.getMessage('ERR_USER_PASS_MATCH'));
    } else {
        return true;
    }
}

function validateUserName() {

    var url1 = BASE_URL + '/userAPI/checkUserByName';
    var uname = $('#userUsername').val();
    eb.ajax({
        type: 'POST',
        url: url1,
        data: {username: uname},
        success: function(data) {
            $('#create-user-button').show();
            if (data.status === true) {
                var msg = "User " + uname + " exists";
                $('#userUsername').focus();
                p_notification(false, msg);
                result = true;
            } else {
                result = false;
            }
        },
        async: false
    });
    return result;
}
