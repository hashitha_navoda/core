/**
 * @author sharmilan <sharmilan@thinkube.com>
 *
 * This js for auto loading notification to core page
 */

var tid = setInterval(notification, 10 * 60 * 1000);



jQuery(document).ready(function() {
    jQuery("abbr.timeago").timeago();
    $('#notificationFilter').on('change', function() {
        var noti_type = $(this).val();
        var url = '/notification/display-all-notifications';
        window.open(url + '/1/' + noti_type, "_self");
    });

    $('#notiList').on('click', '.closer', function(e) {
        e.preventDefault();
        var notificID = $(this).data('id');
        document.getElementById('notification_' + notificID).remove();
        eb.ajax({
            type: 'POST',
            url: BASE_URL + '/api/notification/updateShowState',
            data: {notificID: notificID},
            success: function(respond) {
            }
        });
    });
    $('#displayAllNotificationsList').on('click', '.closer', function(e) {
        e.preventDefault();
        var notificID = $(this).data('id');
        document.getElementById('notification_' + notificID).remove();
        eb.ajax({
            type: 'POST',
            url: BASE_URL + '/api/notification/updateShowState',
            data: {notificID: notificID},
            success: function(respond) {
                if (respond.status) {
//                    p_notification(respond.status, respond.msg); // if you wont to show success message then un commit this line
                    location.reload();
                }
            }
        });
    });

});

function notification() {
    $.ajax({
        type: "POST",
        async: false,
        url: '/api/notification/getNotification',
        success: function(response) {
            $('#notiList').html(response);
        }
    });
}



