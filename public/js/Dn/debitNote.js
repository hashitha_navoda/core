var PVID;
var debitNoteProducts = {};
var batchProducts = {};
var productsTax = {};
var productsTotal = {};
var debitNoteSubProducts = {};
var checkSubProduct = {};
var SubProductsQty = {};
var supplierID = '';
var productBeingEdited = [];
var directDebitNoteType = false;
var selectedSupplier;
var selectedId;
var currentProducts = {};
var subProductsForCheck = {};
var discountEditedManually = [];
var deliverSubProducts = {};
var deliverProducts = {};
var hideDuplicate = false;
var ignoreBudgetLimitFlag = false;
var subProductsForCheck = {};
var SubProductsQty = {};
var serialProducts = {};
var dimensionData = {};
$(document).ready(function() {
    var priceListItems = [];
    var defaultSellingPriceData = [];
    var rowincrementID = 1;
    var totalTaxList = {};
    var locationProducts = '';
    locationID = $('#idOfLocation').val();
    var $directProductTable = $("#deliveryNoteProductTable");
    var $addDirectRowSample = $('tr.add-direct-debit-row.sample.hidden', $directProductTable);

    var currentItemTaxResults;
    var $productTable = $("#productTable");
    var $addRowSample = $('tr.add-row.sample.hidden', $productTable);
    var dimensionArray = {};
    var dimensionTypeID = null;

    var getAddRow = function(productID) {

        if (productID != undefined) {

            var $row = $('table.debitNoteProductTable > tbody > tr', $productTable).filter(function() {
                return $(this).data("id") == productID;
            });

            return $row;
        }
        return $('tr.add-row:not(.sample)', $productTable);
    };
    if (!getAddRow().length) {
        $($addRowSample.clone()).removeClass('sample hidden').appendTo($('#add-new-item-row'));
    }

    var getAddRowForDirectDebit = function(proIncID) {
        if (!(proIncID === undefined || proIncID == '')) {
            var $row = $('table#deliveryNoteProductTable > tbody#add-new-item-row-for-dPr > tr').filter(function() {
                return $(this).data("proIncID") == proIncID;
            });
            return $row;
        }

        return $('tr.add-direct-debit-row:not(.sample)', $directProductTable);
    };
    if (!getAddRowForDirectDebit().length) {
        $($addDirectRowSample.clone()).removeClass('sample hidden').appendTo($('#add-new-item-row-for-dPr'));
    }

    loadDropDownFromDatabase('/supplierAPI/search-suppliers-for-dropdown', "", 0, '#directSupplier');
    loadDropDownFromDatabase('/api/pi/search-location-wisePV-for-dropdown', "", 0, '#PVNo');
    $('#PVNo').selectpicker('refresh');

    $('#PVNo').on('change', function() {
        if ($(this).val() > 0 && PVID != $(this).val()) {
            PVID = $(this).val();
            getPVDetails(PVID);
        }
    });

    $('#dimensionView').on('click', function(e) {
        clearDimensionModal();
        var debitNoteNo = $('#debitNoteNo').val();

        if (!$.isEmptyObject(dimensionData)) {
            $.each(dimensionData[debitNoteNo], function(index, val) {
                dimensionArray[index] = val;                
            });
            $.each(dimensionArray, function(index, value) {
                var newTrID = 'tr_' + value.dimensionTypeId;
                var clonedRow = $($('#dimensionPreSample').clone()).attr('id', newTrID).addClass('addedDimensions');
                $("input[name='dimensionType']", clonedRow).val(value.dimensionTypeTxt);
                $("input[name='dimensionValue']", clonedRow).val(value.dimensionValueTxt);
                clonedRow.removeClass('hidden');
                clonedRow.insertBefore('#dimensionPreSample');
                clearAddedNewDimensionRow();
            });
        }
        $('#addDimensionModal').modal('show');
    });

    // dimension modal
    function clearDimensionModal()
    {
        $("#addDimensionModal #dimensionTable tbody .addedDimensions").remove();
        dimensionArray = {};
    }


    $('.dimenisonchange').on('change', function(e) {
        dimensionTypeID = this.value;
        getDimensionValueListByDimensionType(dimensionTypeID, function() {
        });
    });



    function getDimensionValueListByDimensionType(dimensionTypeID, callback) {
        switch (dimensionTypeID) {
            case "job":
                $('.dimensionVal').addClass('hidden');
                $('.dimensionProjectNo').addClass('hidden');
                $('.dimensionJobNo').removeClass('hidden');
                break;
            case "project":
                $('.dimensionVal').addClass('hidden');
                $('.dimensionProjectNo').removeClass('hidden');
                $('.dimensionJobNo').addClass('hidden');
                break;
           default:
                $('.dimensionVal').removeClass('hidden');
                $('.dimensionProjectNo').addClass('hidden');
                $('.dimensionJobNo').addClass('hidden');
                $.ajax({
                    type: 'POST',
                    url: BASE_URL + '/dimension-api/getRelatedDimensionValues',
                    data: {dimensionId: dimensionTypeID},
                    success: function(respond) {
                        //empty options
                        $('#dimensionVal').empty();
                        $('#dimensionVal').append($("<option>", {value: '', html: 'Select dimension value'}));
                        $(respond.data).each(function(index, dimValue) {
                            $.each(dimValue, function(ind, val) {
                                $('#dimensionVal').append($("<option>", {value: ind, html: val.value}));
                            });
                        });
                        callback();
                    }
                });
                break;
        }
    }

    $('#add_dimension').on('click', function(e) {
        e.preventDefault();
        //if plus button is clicked then disabled the button
        //if button is clicked more than one, then it does not continue
        if ($(this).is(':disabled')) {
            return false;
        } else {
            $(this).attr('disabled', true);
            var $thisRow = $(this).parents('tr');
            if ($('.dimenisonchange').val() == 0 || $('.dimenisonchange').val() == '' || $('.dimenisonchange').val() == null) {
                p_notification(false, eb.getMessage('ERR_DIMENSION_SELCET'));
                $(this).attr('disabled', false);
            } else {

                if ($('.dimenisonchange').val() != 'job' && $('.dimenisonchange').val() != 'project') {
                    if ($('.dimensionVal').val() == 0 || $('.dimensionVal').val() == '' || $('.dimensionVal').val() == null) {
                        p_notification(false, eb.getMessage('ERR_DIMENSION_VALUE_SELCET'));
                        $(this).attr('disabled', false);
                        return;
                    }
                }

                if ($('.dimenisonchange').val() == 'project') {
                    if ($('.dimensionProjectNo').val() == 0 || $('.dimensionProjectNo').val() == '' || $('.dimensionProjectNo').val() == null) {
                        p_notification(false, eb.getMessage('ERR_DIMENSION_VALUE_SELCET'));
                        $(this).attr('disabled', false);
                        return;
                    }
                }

                if ($('.dimenisonchange').val() == 'job') {
                    if ($('.dimensionJobNo').val() == 0 || $('.dimensionJobNo').val() == '' || $('.dimensionJobNo').val() == null) {
                        p_notification(false, eb.getMessage('ERR_DIMENSION_VALUE_SELCET'));
                        $(this).attr('disabled', false);
                        return;
                    }
                }

                if (dimensionArray[dimensionTypeID]) {
                    p_notification(false, eb.getMessage('ERR_DIMENSION_ALREADY_INSERTED'));
                    $('.dimenisonchange').val('');
                    $('.dimenisonchange').selectpicker('render');
                    $('.dimenisonchange').trigger('change');
                } else {
                    addNewDimensionRow($(this).parents('tr'));
                }
                $(this).attr('disabled', false);
            }
        }
    });

    function addNewDimensionRow(thisRow) {

        var dimensionTypeId = $('.dimenisonchange', thisRow).val();
        var dimensionTypeTxt = $('.dimenisonchange', thisRow).find("option:selected").text();

        switch (dimensionTypeID) {
            case "job":
                var dimensionValueTxt = $('.dimensionJobNo', thisRow).find("option:selected").text();
                var dimensionValueId = $('.dimensionJobNo', thisRow).val();
                break;
            case "project":
                var dimensionValueTxt = $('.dimensionProjectNo', thisRow).find("option:selected").text();
                var dimensionValueId = $('.dimensionProjectNo', thisRow).val();
                break;
           default:
                var dimensionValueTxt = $('.dimensionVal', thisRow).find("option:selected").text();
                var dimensionValueId = $('.dimensionVal', thisRow).val();
                break;
        }
       
        var dimensionDuplicateCheck = false;
        if (!$.isEmptyObject(dimensionArray)) {
            $.each(dimensionArray, function(index, value) {
                if (value.dimensionTypeId == dimensionTypeId) {
                    dimensionDuplicateCheck = true;
                }
            });   
        }
       
        if (dimensionDuplicateCheck) {
            p_notification(false, eb.getMessage("ERR_DIMENSION_ALREADY_INSERTED"));
            return false;
        }

        var newTrID = 'tr_' + dimensionTypeId;
        var clonedRow = $($('#dimensionPreSample').clone()).attr('id', newTrID).addClass('addedDimensions');
        $("input[name='dimensionType']", clonedRow).val(dimensionTypeTxt);
        $("input[name='dimensionValue']", clonedRow).val(dimensionValueTxt);
        clonedRow.removeClass('hidden');
        clonedRow.insertBefore('#dimensionPreSample');
        dimensionArray[dimensionTypeId] = new dimension(dimensionTypeId, dimensionValueId,dimensionTypeTxt , dimensionValueTxt);
        clearAddedNewDimensionRow();
    }


    function dimension(dimensionTypeId, dimensionValueId, dimensionTypeTxt, dimensionValueTxt) {
        this.dimensionTypeId = dimensionTypeId;
        this.dimensionValueId = dimensionValueId;
        this.dimensionTypeTxt = dimensionTypeTxt;
        this.dimensionValueTxt = dimensionValueTxt;
    }

    function clearAddedNewDimensionRow() {
        $('.dimenisonchange').val('');
        $('.dimenisonchange').selectpicker('render');
        $('.dimenisonchange').trigger('change');
    }

    $('.addDimensions').on('click', function(e) {
        e.preventDefault();

        // validate products before closing modal
        if (!dimensionModalValidate(e)) {
            e.stopPropagation();
            return false;
        } else {
            $('#addDimensionModal').modal('hide');
        }
    });

    function dimensionModalValidate(e) {
        var debitNoteNo = $('#debitNoteNo').val();
        if ($.isEmptyObject(dimensionArray)) {
            dimensionData = {};
            return true;   
        } else {
            var dimensionSingleData = {};
            $.each(dimensionArray, function(index, val) {
                dimensionSingleData[index] = val;
            });
            dimensionData[debitNoteNo] = dimensionSingleData;
            return true;
        }
    }

    $("#addDimensionModal #dimensionTable").on('click', '.deleteDimensionType', function(e) {
        e.preventDefault();
        var deleteDTrID = $(this).closest('tr').attr('id');
        var deleteDID = deleteDTrID.split('tr_')[1].trim();
        bootbox.confirm('Are you sure you want to remove this dimension ?', function(result) {
            if (result == true) {
                delete dimensionArray[deleteDID];
                $('#' + deleteDTrID).remove();
            }
        });
    });

    loadDropDownFromDatabase('/job-api/searchJobsForDropdown', "", 0, '.dimensionJobNo');
    $('.dimensionJobNo').selectpicker('refresh');
    $('.dimensionJobNo').on('change', function() {
        var dimensionValue = $(this).val();
    });

    loadDropDownFromDatabase('/api/project/searchProjectsForDropdown', "", 0, '.dimensionProjectNo');
    $('.dimensionProjectNo').selectpicker('refresh');
    $('.dimensionProjectNo').on('change', function() {
        var dimensionValue = $(this).val();
    });

    $('#directDebit').on('click', function(){
        if($(this).is(':checked')){
            $('#productsAddOverlay').removeClass('overlay');
            hideDuplicate = false;
            setProductTypeahead(hideDuplicate);
            locationProducts = {};
            clearProductDataForNewDirectDebitNote();
            $('#PVNo').attr('disabled',true);
            $('.direct-debit').removeClass('hidden');
            $('.normal-debit').addClass('hidden');
            $('#addDebitNoteProductsModal').addClass('hidden');
            $('#item_code', $addDirectRowSample).selectpicker();
            $('.directSupplier').removeClass('hidden');
            $('#dimension_div').removeClass('hidden');
            $('.normalSupplier').addClass('hidden');
            directDebitNoteType = true;
        } else {
            $('#dimension_div').addClass('hidden');
            $('#addDebitNoteProductsModal').removeClass('hidden');
            $('#PVNo').attr('disabled',false);
            $('.direct-debit').addClass('hidden');
            $('.normal-debit').removeClass('hidden');
            $('.directSupplier').addClass('hidden');
            $('.normalSupplier').removeClass('hidden');
            directDebitNoteType = false;
        }
    });

    // this will get all pv related details
    function getPVDetails(PVID) {
        eb.ajax({
            type: 'POST',
            url: BASE_URL + '/api/pi/getPurchaseVoucherDetails',
            data: {
                PVID: PVID,
                locationID: locationID
            },
            success: function(respond) {
                clearProductDataForNewDebitNote();
                if (respond.status) {
                    if (respond.data.inactiveItemFlag) {
                        p_notification(false, respond.data.errorMsg);
                        setTimeout(function(){ 
                            window.location.assign(BASE_URL + "/debit-note")
                        }, 3000);
                        return false;
                    }
                    supplierID = respond.data.purchaseVoucher.purchaseInvoiceSupplierID;
                    $('#supplier').val(respond.data.purchaseVoucher.supplierCode + '-' + respond.data.purchaseVoucher.supplierName).attr('disabled', true);
                    if (respond.data.purchaseVoucher.paymentTermID) {
                        $('#paymentTerm').val(respond.data.purchaseVoucher.paymentTermID);
                    } else {
                        $('#paymentTerm').val(1);
                    }
                    $('#comment').val(respond.data.purchaseVoucher.purchaseInvoiceComment);
                    Products = respond.data.productDetails;
                    locationProducts = respond.data.pVProduct;
                    selectProductForDebitNote(locationProducts);
                } else {
                    PVID = '';
                    p_notification(respond.status, respond.msg);
                    $('#PVNo').val('');
                }
            }
        });
    }


    function clearProductDataForNewDebitNote() {
        var $currentRow = getAddRow();
        $currentRow.removeClass('hidden');
        $('#add-new-item-row', $productTable).children('tr:not(.add-row)').remove('tr');
        debitNoteSubProducts = {};
        debitNoteProducts = {};
        checkSubProduct = {};
        batchProducts = {};
        productsTax = {};
        productsTotal = {};
//        $('#deliveryNoteAddress').val('');
        $('#comment').val('');
        $('#subtotal').html('0.00');
        $('#finaltotal').html('0.00');

    }

    function selectProductForDebitNote(locationProducts) {
        var checkProduct = 0;
        $.each(locationProducts, function(selectedProduct, value) {
            var productCode = value.productCode;
            var productName = value.productName;
            var availableQuantity = value.pVProductQuantity;
            var pVproductID = value.pVProductID;
            var defaultSellingPrice = value.pVProductPrice;
            var productDiscountType = value.pVProductDiscountType;
            var productDiscount = value.pVProductDiscount;
            var productType = value.productType;
            var purchaseInvoiceProductDocumentTypeID = value.purchaseInvoiceProductDocumentTypeID;
            var purchaseInvoiceProductDocumentID = value.purchaseInvoiceProductDocumentID;
            var flag = false;
            if (productType == 2) {
                flag = true;
            }
            if (productType != 2 && availableQuantity != 0) {
                flag = true;
            }

            if (flag) {
                checkProduct = 1;
                var $currentRow = getAddRow();
                clearProductRow($currentRow);
                $("input[name='itemCode']", $currentRow).val(productCode + ' - ' + productName).prop('readonly', true);
                $("input[name='itemCode']", $currentRow).data('PT', productType);
                $("input[name='itemCode']", $currentRow).data('PC', productCode);
                $("input[name='itemCode']", $currentRow).data('PN', productName);
                $("input[name='itemCode']", $currentRow).data('PDocumentTypeID', purchaseInvoiceProductDocumentTypeID);
                $("input[name='itemCode']", $currentRow).data('PDocumentID', purchaseInvoiceProductDocumentID);
                $("input[name='debitNoteQuanity']", $currentRow).parent().addClass('input-group');
                $("input[name='invoicedQuantity']", $currentRow).parent().addClass('input-group');
                $("input[name='debitNoteQuanity']", $currentRow).val(availableQuantity).prop('readonly', true).addUom(Products[selectedProduct].uom);
                $("input[name='invoicedQuantity']", $currentRow).val(availableQuantity).prop('readonly', true).addUom(Products[selectedProduct].uom);
                $("input[name='unitPrice']", $currentRow).val(defaultSellingPrice).addUomPrice(Products[selectedProduct].uom);
                
                var baseUom;
                for (var j in Products[selectedProduct].uom) {
                    if (Products[selectedProduct].uom[j].pUBase == 1) {
                        baseUom = Products[selectedProduct].uom[j].uomID;
                    }
                }

                if (productDiscountType == 'value') {
                     $("input[name='discount']", $currentRow).val(productDiscount).prop('readonly', true).addUomPrice(Products[selectedProduct].uom, baseUom);
                     $("input[name='discount']", $currentRow).siblings('.uom-price-select').find('.uom_price_drop_down').attr('disabled', true);
                    $("input[name='discount']", $currentRow).addClass('value');
                } else if (productDiscountType == 'precentage') {
                    $("input[name='discount']", $currentRow).val(productDiscount).prop('readonly', true);
                    $("input[name='discount']", $currentRow).addClass('precentage');
                    $(".sign", $currentRow).text('%');
                } 

                $currentRow.data('id', selectedProduct);
                $currentRow.data('pId', value.productID);
                $currentRow.data('dPID', pVproductID);
                $currentRow.parents().find('.delete').removeClass('disabled');

                // clear old rows
                $("#batch_data tr:not(.hidden)").remove();

                // check if any batch / serial products exist
                if ($.isEmptyObject(value.subProduct)) {
                    $currentRow.removeClass('subproducts');
                    $('.edit-modal', $currentRow).parent().addClass('hidden');
                    $("td[colspan]", $currentRow).attr('colspan', 2);
                    $("input[name='debitNoteQuanity']", $currentRow).prop('readonly', false).change().focus();
                } else {
                    $currentRow.addClass('subproducts');
                    $('.edit-modal', $currentRow).parent().removeClass('hidden');
                    $("td[colspan]", $currentRow).attr('colspan', 1);
                    $("input[name='debitNoteQuanity']", $currentRow).prop('readonly', true).change();
                }
                $currentRow.addClass('edit-row');
                setTaxListForProduct(selectedProduct, $currentRow);
                $currentRow.attr('id', 'product' + selectedProduct);
                $('#debitNoteQuanity,.uomqty', $currentRow).trigger(jQuery.Event("focusout"));
                $currentRow
                        .removeClass('add-row');
                $($addRowSample.clone()).removeClass('sample hidden').appendTo($('#add-new-item-row', $productTable));
            }
        });
        if (checkProduct == 0) {
            p_notification(false, eb.getMessage('ERR_DEBIT_NOTE_PV_PRD_RETURN'));
        }
        var $currentRow = getAddRow();
        $currentRow.addClass('hidden');
        return;

    }

    $productTable.on('click', 'button.edit-modal', function(e) {

        var productID = $(this).parents('tr').data('id');
        var productCode = $(this).parents('tr').find('#itemCode').data('PC');
        $("#batch_data tr:not(.hidden)").remove();
        $('#addDebitNoteProductsModal').modal('show').data('id', productID);
        addSubProduct(productID, productCode, 0);
        $("tr", '#batch_data').each(function() {
            var $thisSubRow = $(this);
            for (var i in debitNoteSubProducts[productID]) {
                if (debitNoteSubProducts[productID][i].serialID != '') {
                    if ($(".serialID", $thisSubRow).data('id') == debitNoteSubProducts[productID][i].serialID) {
                        $("input[name='debitNoteProductSubQuantityCheck']", $thisSubRow).prop('checked', true);
                    }
                } else if (debitNoteSubProducts[productID][i].batchID != '') {
                    if ($(".batchCode", $thisSubRow).data('id') == debitNoteSubProducts[productID][i].batchID) {
                        $(this).find("input[name='debitNoteProductSubQuantity']").val(debitNoteSubProducts[productID][i].qtyByBase).change();
                    }
                }
            }
        });

        $('#addDebitNoteProductsModal').data('id', $(this).parents('tr').data('id')).modal('show');
        $('#addDebitNoteProductsModal').unbind('hide.bs.modal');
    });

    $productTable.on('click', 'button.add, button.save', function(e) {
        var $thisRow = $(this).parents('tr');

        var thisVals = getCurrentProductData($thisRow);
        $("input[name='invoicedQuantity']", $thisRow).prop('readonly', true);
        if ((thisVals.productCode.trim()) == '' || (thisVals.productName.trim()) == '') {
            p_notification(false, eb.getMessage('ERR_DEBIT_SELECT_PROD'));
            $("input[name='itemCode']", $thisRow).focus();
            return false;
        }

        if (thisVals.productType != 2 && (isNaN(parseFloat(thisVals.debitNoteQuantity.qty)) || parseFloat(thisVals.debitNoteQuantity.qty) <= 0)) {
            p_notification(false, eb.getMessage('ERR_DEBIT_VALQUANTITY'));
            $("input[name='debitNoteQuanity']", $thisRow).focus();
            return false;
        }
        if (parseFloat(thisVals.debitNoteQuantity.qty) > parseFloat(thisVals.availableQuantity.qty)) {
            p_notification(false, eb.getMessage('ERR_DEBIT_QUANTITY'));
            $("input[name='debitNoteQuanity']", $thisRow).focus();
            if (checkSubProduct[thisVals.productID] == undefined) {
                return false;
            }
        }

        if (thisVals.productDiscount) {
            if (isNaN(parseFloat(thisVals.productDiscount)) || parseFloat(thisVals.productDiscount) < 0) {
                p_notification(false, eb.getMessage('ERR_DEBIT_VALID_DISCOUNT'));
                $("input[name='discount']", $thisRow).focus();
                return false;
            }
        }

        if ($thisRow.hasClass('subproducts')) {
            if (SubProductsQty[thisVals.productID] != '' && (parseFloat(SubProductsQty[thisVals.productID]) != parseFloat(thisVals.debitNoteQuantity.qty))) {
                p_notification(false, eb.getMessage('ERR_DEBIT_SUB_PRODUCT_CHECK', thisVals.productCode));
                $('.edit-modal', $thisRow).focus();
                return false;
            }
        }

        // if add button is clicked

        if(productBeingEdited.indexOf($(this).parents('tr').data('id')) >= 0){
            productBeingEdited.splice(productBeingEdited.indexOf($(this).parents('tr').data('id')),1);
        }
        $thisRow.removeClass('edit-row');
        if ($(this).hasClass('add')) {
            $thisRow
                    .removeClass('add-row')
                    .find("input[name='itemCode'], input[name='itemName'], input[name='invoicedQuantity'],input[name='debitNoteQuanity'],input[name='unitPrice'],input[name='discount']").prop('readonly', true).end()
                    .find('td').find('#addNewTax').attr('disabled', true);
        } else if ($(this).hasClass('save')) {
            $thisRow
                    .find("input[name='itemCode'], input[name='itemName'], input[name='invoicedQuantity'],input[name='debitNoteQuanity'],input[name='unitPrice'],input[name='discount']").prop('readonly', true).end()
                    .find('td').find('#addNewTax').attr('disabled', true);
        } else { // if save button is clicked
            $thisRow.find("input[name='invoicedQuantity']").prop('readonly', true);
        }

        // if batch product modal is available for this product
        $(this, $thisRow).parent().find('.edit').removeClass('hidden');
        $(this, $thisRow).parent().find('.add').addClass('hidden');
        $(this, $thisRow).parent().find('.save').addClass('hidden');
//        $(this, $thisRow).parents().find('.delete').removeClass('disabled');
        if ($thisRow.hasClass('subproducts')) {
            $('.edit-modal', $thisRow).parent().addClass('hidden');
            $("td[colspan]", $thisRow).attr('colspan', 2);
        } else {
            $("#debitNoteQuanity", $thisRow).attr('disabled', true).change();
        }
        debitNoteProducts[thisVals.productID] = thisVals;
        setDebitNoteTotalCost();
    });

    $productTable.on('click', 'button.edit', function(e) {

        var $thisRow = $(this).parents('tr');
        productBeingEdited.push($(this).parents('tr').data('id'));
        $thisRow.addClass('edit-row')
                .find('td').find('#addNewTax').attr('disabled', false);
        $(this, $thisRow).parent().find('.edit').addClass('hidden');
        $(this, $thisRow).parent().find('.save').removeClass('hidden');
        if ($thisRow.hasClass('subproducts')) {
            $('.edit-modal', $thisRow).parent().removeClass('hidden');
            $("td[colspan]", $thisRow).attr('colspan', 1);
        } else {
            $("#debitNoteQuanity", $thisRow).attr('disabled', false).prop('readonly', false).change().focus();
        }
    });

    $productTable.on('click', 'button.delete', function(e) {
        if(productBeingEdited.indexOf($(this).parents('tr').data('id')) >= 0){
            productBeingEdited.splice(productBeingEdited.indexOf($(this).parents('tr').data('id')),1);
        }
        var $thisRow = $(this).parents('tr');
        var deletePID = $thisRow.data('id');
        delete debitNoteProducts[deletePID];
        setDebitNoteTotalCost();
        $thisRow.remove();
    });

    function addSubProduct(selectedProduct, productCode, deleted) {
        if ((!$.isEmptyObject(locationProducts[selectedProduct].subProduct))) {

            batchProduct = locationProducts[selectedProduct].subProduct;
            batch = Products[selectedProduct].batch;
            batchSerial = Products[selectedProduct].batchSerial;
            productBatchSerial = Products[selectedProduct].batchSerial;
            productSerial = Products[selectedProduct].serial;
            for (var i in batchProduct) {
                if (batchProduct[i].pVSubProductQuantity != 0) {
                    if (deleted != 1 && batchProduct[i].productBatchID != null && batchProduct[i].productSerialID != null) {
                        var serialKey = productCode + "-" + batchProduct[i].productSerialID;
                        var serialQty = (batchProduct[i].pVSubProductQuantity == 1) ? 1 : 0;

                        var $clonedRow = $($('.batch_row').clone());
                        $clonedRow
                                .data('id', batchProduct[i].productSerialID)
                                .removeAttr('id')
                                .removeClass('hidden batch_row')
                                .addClass('remove_row');
                        $(".serialID", $clonedRow)
                                .html(batchSerial[batchProduct[i].productSerialID].PSC)
                                .data('id', batchProduct[i].productSerialID)
                                .data('subDocumentID', batchProduct[i].pvProductDocumentID);
                        $(".batchCode", $clonedRow)
                                .html(batchSerial[batchProduct[i].productSerialID].PBC)
                                .data('id', batchProduct[i].productBatchID);
                        $("input[name='availableProductSubQuantity']", $clonedRow).val(serialQty).addUom(Products[selectedProduct].uom);
                        if (serialQty == 0) {
                            $("input[name='debitNoteProductSubQuantityCheck']", $clonedRow).prop('disabled', true);
                        }
                        $("input[name='debitNoteProductSubQuantity']", $clonedRow).parent().remove();

                        if (batchProducts[serialKey]) {
                            if (batchProducts[serialKey].Qty == 1) {
                                $("input[name='debitNoteProductSubQuantityCheck']", $clonedRow).attr('checked', true);
                            }
                        }

                        $clonedRow.insertBefore('.batch_row');

                    } else if (deleted != 1 && batchProduct[i].productBatchID != null) {
                        if ((!$.isEmptyObject(Products[selectedProduct].productIDs))) {
                            if (Products[selectedProduct].productIDs[batchProduct[i].productBatchID]) {
                                continue;
                            }
                        }
                        var batchKey = productCode + "-" + batchProduct[i].productBatchID;

                        var $clonedRow = $($('.batch_row').clone());
                        $clonedRow
                                .data('id', batchProduct[i].productBatchID)
                                .removeAttr('id')
                                .removeClass('hidden batch_row')
                                .addClass('remove_row');
                        $(".batchCode", $clonedRow).html(batch[batchProduct[i].productBatchID].PBC).data('id', batchProduct[i].productBatchID);
                        $("input[name='availableProductSubQuantity']", $clonedRow).val(batchProduct[i].pVSubProductQuantity).addUom(Products[selectedProduct].uom);
                        if (batchProduct[i].pVSubProductQuantity == 0) {
                            $("input[name='debitNoteProductSubQuantity']", $clonedRow).prop('disabled', true);
                        }
                        $("input[name='debitNoteProductSubQuantityCheck']", $clonedRow).remove();
                        if (batchProducts[batchKey]) {
                            $("input[name='debitNoteProductSubQuantity']", $clonedRow).val(batchProducts[batchKey].Qty);
                        }
                        $("input[name='debitNoteProductSubQuantity']", $clonedRow).addUom(Products[selectedProduct].uom);
                        $("input[name='debitNoteProductSubQuantity']", $clonedRow).data('subDocumentID', batchProduct[i].pvProductDocumentID);
                        $clonedRow.insertBefore('.batch_row');

                    } else if (deleted != 1 && batchProduct[i].productSerialID != null) {
                        var serialKey = productCode + "-" + batchProduct[i].productSerialID;
                        var serialQty = (batchProduct[i].pVSubProductQuantity == 1) ? 1 : 0;

                        var $clonedRow = $($('.batch_row').clone());
                        $clonedRow
                                .data('id', batchProduct[i].productSerialID)
                                .removeAttr('id')
                                .removeClass('hidden batch_row')
                                .addClass('remove_row');
                        $(".serialID", $clonedRow)
                                .html(batchProduct[i].productSerialCode)
                                .data('id', batchProduct[i].productSerialID)
                                .data('subDocumentID', batchProduct[i].pvProductDocumentID);
                        $("input[name='availableProductSubQuantity']", $clonedRow).val(serialQty).addUom(Products[selectedProduct].uom);
                        if (serialQty = 0) {
                            $("input[name='debitNoteProductSubQuantityCheck']", $clonedRow).prop('disabled', true);
                        }
                        $("input[name='debitNoteProductSubQuantity']", $clonedRow).parent().remove();

                        if (batchProducts[serialKey]) {
                            if (batchProducts[serialKey].Qty == 1) {
                                $("input[name='debitNoteProductSubQuantityCheck']", $clonedRow).attr('checked', true);
                            }
                        }

                        $clonedRow.insertBefore('.batch_row');

                    } else {
                        batchProducts[batchKey] = undefined;
                    }
                }
            }
        }
    }

    function setTaxListForProduct(productID, $currentRow, ItemTaxes) {
        if ((!jQuery.isEmptyObject(locationProducts[productID].tax))) {
            productTax = locationProducts[productID].tax;
            $currentRow.find('#taxApplied').removeClass('glyphicon glyphicon-unchecked');
            $currentRow.find('#taxApplied').addClass('glyphicon glyphicon-check');
            $currentRow.find('.tempLi').remove();
            for (var i in productTax) {
                var taxAmount = 0.00;
                if (!jQuery.isEmptyObject(ItemTaxes)) {
                    taxAmount = ItemTaxes[i].tA;
                }
                var clonedLi = $($currentRow.find('#sampleLi').clone()).removeClass('hidden').attr('id', 'li_' + i).addClass("tempLi");
                clonedLi.children(".taxChecks").attr('id', productID + '_' + i).prop('checked', true).prop('disabled', true).addClass('addNewTaxCheck');
                clonedLi.children(".taxName").html('&nbsp&nbsp' + productTax[i].pVTaxName + '&nbsp&nbsp' + productTax[i].pVTaxPrecentage + '% , ' + taxAmount + ' RS').attr('for', productID + '_' + i);
                clonedLi.insertBefore($currentRow.find('#sampleLi'));
            }
        } else {
            $('#taxApplied').addClass('glyphicon glyphicon-unchecked');
            $currentRow.find('#addNewTax').attr('disabled', 'disabled');
        }

    }

    function clearProductRow($currentRow) {
        $("input[name='itemCode']", $currentRow).val('');
        $("input[name='invoicedQuantity']", $currentRow).val('');
        $("input[name='debitNoteQuanity']", $currentRow).val('');
        $("input[name='unitPrice']", $currentRow).val('');
        $("input[name='discount']", $currentRow).val('');
        $(".tempLi", $currentRow).remove('');
        $("#taxApplied", $currentRow).removeClass('glyphicon-check');
        $("#taxApplied", $currentRow).addClass('glyphicon-unchecked');
        $(".selected", $currentRow).text('');
        $(".uomList", $currentRow).remove('');
    }

    $('#batch-save').on('click', function(e) {
        e.preventDefault();
        // validate batch / serial products before closing modal
        if (!batchModalValidate(e)) {
            e.stopPropagation();
            return false;
        } else {
            $('#addDebitNoteProductsModal').modal('hide');
        }
    });

    function batchModalValidate(e) {

        var productID = $('#addDebitNoteProductsModal').data('id');
        var $thisParentRow = getAddRow(productID);
        var thisVals = getCurrentProductData($thisParentRow);
        var $batchTable = $("#addDebitNoteProductsModal .batch-table tbody");
        var qtyTotal = 0;
        var subProducts = [];

        $("input[name='debitNoteProductSubQuantity'], input[name='debitNoteProductSubQuantityCheck']:checked", $batchTable).each(function() {

        	var $thisSubRow = $(this).parents('tr');
        	if($thisSubRow.hasClass('remove_row')){

        		var thisDebitNoteQuantity = $(this).val();
        		if ((thisDebitNoteQuantity).trim() != "" && isNaN(parseFloat(thisDebitNoteQuantity))) {
        			p_notification(false, eb.getMessage('ERR_DEBIT_VALQUANTITY'));
        			$(this).focus();
        			return qtyTotal = false;
        		}

        		thisDebitNoteQuantity = (isNaN(parseFloat(thisDebitNoteQuantity))) ? 0 : parseFloat(thisDebitNoteQuantity);

            	// if a conversion is not set, it is assumed to be the base (eg: serial checkbox doesnt have multiple UOMs)

            	// check if trasnfer qty is greater than available qty
            	var thisAvailableQuantity = $("input[name='availableProductSubQuantity']", $thisSubRow).val();
            	thisAvailableQuantity = (isNaN(parseFloat(thisAvailableQuantity))) ? 0 : parseFloat(thisAvailableQuantity);
            	var thisAvailableQuantityByBase = thisAvailableQuantity;

            	var thisDebitNoteQuantityByBase = thisDebitNoteQuantity;

            	if (thisDebitNoteQuantityByBase > thisAvailableQuantityByBase) {
            		p_notification(false, eb.getMessage('ERR_DEBIT_QUANTITY'));
            		$(this).focus();

            		return qtyTotal = false;
            	}

            	qtyTotal = qtyTotal + (thisDebitNoteQuantity);

            	// if a product transfer is present, prepare array to be sent to backend

            	if ((thisDebitNoteQuantity) > 0) {

            		var thisSubProduct = {};
            		thisSubProduct.batchID = '';
            		thisSubProduct.serialID = '';

            		if ($(".batchCode", $thisSubRow).data('id')) {
                        thisSubProduct.batchID = $(".batchCode", $thisSubRow).data('id');
                        thisSubProduct.subDocumentTypeID = $("input[name='debitNoteProductSubQuantity']", $thisSubRow).data('subDocumentID');
                    }

                    if ($(".serialID", $thisSubRow).data('id')) {
                        thisSubProduct.serialID = $(".serialID", $thisSubRow).data('id');
            			thisSubProduct.subDocumentTypeID = $(".serialID", $thisSubRow).data('subDocumentID');
            		}

            		thisSubProduct.qtyByBase = thisDebitNoteQuantity;

            		subProducts.push(thisSubProduct);
            	}
        	}
        });

        if (checkSubProduct[thisVals.productID] != undefined) {
            if (qtyTotal != $("input[name='debitNoteQuanity']", $thisParentRow).val()) {
                p_notification(false, eb.getMessage('ERR_DEBIT_TOTAL_SUBQUAN'));
                return qtyTotal = false;
            } else {
                checkSubProduct[thisVals.productID] = 1;
            }
        }

        // to break out form $.each and exit function
        if (qtyTotal === false)
            return false;

        // ideally, since the individual batch/serial quantity is checked to be below the available qty,
        // the below condition should never become true
        if (qtyTotal > thisVals.availableQuantity.qty) {
            p_notification(false, eb.getMessage('ERR_DEBIT_TOTAL_INVQUAN'));
            return false;
        }

        debitNoteSubProducts[thisVals.productID] = subProducts;
        SubProductsQty[thisVals.productID] = qtyTotal;
        var $debitNoteQty = $("input[name='debitNoteQuanity']", $thisParentRow).prop('readonly', true);
        if (checkSubProduct[thisVals.productID] == undefined) {
            $debitNoteQty.val(qtyTotal).change();
        }
        $('#debitNoteQuanity,.uomqty', $thisParentRow).trigger(jQuery.Event("focusout"));

        return true;
    }

    function getCurrentProductData($thisRow) {
        var discountType = '';
        if ($("input[name='discount']", $thisRow).hasClass('value')) {
            discountType = 'value';
        } else if ($("input[name='discount']", $thisRow).hasClass('precentage')) {
            discountType = 'precentage';
        }
        var pVPID = $thisRow.data('dPID');
        var availableqty = $("input[name='invoicedQuantity']", $thisRow).val();
        if (isNaN(availableqty)) {
            availableqty = 0;
        }
        var debitNoteqty = $("input[name='debitNoteQuanity']", $thisRow).val();
        if (debitNoteqty == '') {
            debitNoteqty = 0;
        }
        var thisVals = {
            productID: $thisRow.data('id'),
            productIID: $thisRow.data('pId'),
            purchaseVoucherProductID: pVPID,
            productCode: $("input[name='itemCode']", $thisRow).data('PC'),
            productName: $("input[name='itemCode']", $thisRow).data('PN'),
            productPrice: $("input[name='unitPrice']", $thisRow).val(),
            productDiscount: $("input[name='discount']", $thisRow).val(),
            productDiscountType: discountType,
            productTotal: productsTotal[$thisRow.data('id')],
            pTax: productsTax[$thisRow.data('id')],
            productType: $("input[name='itemCode']", $thisRow).data('PT'),
            documentTypeID: $("input[name='itemCode']", $thisRow).data('PDocumentTypeID'),
            documentID: $("input[name='itemCode']", $thisRow).data('PDocumentID'),
            availableQuantity: {
                qty: availableqty,
            },
            debitNoteQuantity: {
                qty: debitNoteqty,
            }
        };

        return thisVals;
    }



    $productTable.on('focusout', '#debitNoteQuanity,.uomqty,.uomPrice', function() {
        var $thisRow = $(this).parents('tr');
        var productID = $thisRow.data('id');
        var checkedTaxes = Array();
        $thisRow.find('input:checkbox.addNewTaxCheck').each(function() {
            if (this.checked) {
                var cliTID = this.id;
                var tTaxRowID = cliTID.split('_');
                if (tTaxRowID.length == 4) {
                   var tTaxID =  tTaxRowID[3];
                } else {
                   var tTaxID =  tTaxRowID[2];
                }
                checkedTaxes.push(tTaxID);
            }
        });
        var productType = $(this).parents('tr').find('#itemCode').data('PT');
        var debitNoteQty = $(this).parents('tr').find('#debitNoteQuanity').val();
        if (productType == 2 && debitNoteQty == 0) {
            debitNoteQty = 1;
        }
        var tmpItemTotal = (toFloat($(this).parents('tr').find('#unitPrice').val()) * toFloat(debitNoteQty));
        var tmpItemCost = tmpItemTotal;
        var tmpItemQuentity = debitNoteQty;

        if ($(this).parents('tr').find('#discount').hasClass('value')) {
            var tmpPDiscountvalue = isNaN($(this).parents('tr').find('#discount').val()) ? 0 : $(this).parents('tr').find('#discount').val();
            if (tmpPDiscountvalue > 0) {
                tmpItemCost -= tmpPDiscountvalue * tmpItemQuentity;
            }
        } else {
            var tmpPDiscount = isNaN($(this).parents('tr').find('#discount').val()) ? 0 : $(this).parents('tr').find('#discount').val();
            if (tmpPDiscount > 0) {
                tmpItemCost -= (tmpItemTotal * toFloat(tmpPDiscount) / toFloat(100));
            }
        }
        if (checkedTaxes) {
            currentItemTaxResults = calculateItemCustomTax(tmpItemCost, checkedTaxes);
            currentTaxAmount = (currentItemTaxResults == null) ? null : currentItemTaxResults.tTA;
        } else {
            currentTaxAmount = 0;
            currentItemTaxResults = '';
        }

        var itemCost = toFloat(tmpItemCost) + toFloat(currentTaxAmount);
        productsTotal[productID] = itemCost;
        productsTax[productID] = currentItemTaxResults;
        $(this).parents('tr').find('#addNewTotal').html(accounting.formatMoney(itemCost));
        if (productsTax[productID] != null) {
            if (jQuery.isEmptyObject(productsTax[productID].tL)) {
                $(this).parents('tr').find('#taxApplied').addClass('glyphicon glyphicon-unchecked');
            } else {
                $(this).parents('tr').find('#taxApplied').removeClass('glyphicon glyphicon-unchecked');
                $(this).parents('tr').find('#taxApplied').addClass('glyphicon glyphicon-check');
            }
            setTaxListForProduct(productID, $thisRow, currentItemTaxResults.tL);
        }
    });

    function setDebitNoteTotalCost() {
        var grnTotal = 0;
        for (var i in debitNoteProducts) {
            grnTotal += toFloat(debitNoteProducts[i].productTotal);
        }
        $('#subtotal').html(accounting.formatMoney(grnTotal));
        if ($('#deliveryChargeEnable').is(':checked')) {
            var deliveryAmount = $('#deliveryCharge').val();
            grnTotal += toFloat(deliveryAmount);
        }
        $('#finaltotal').html(accounting.formatMoney(grnTotal));
    }

    $('#debitNoteSaveButton').on('click', function(e) {
        e.preventDefault();
        if (directDebitNoteType) {
            if (jQuery.isEmptyObject(deliverProducts)) {
                p_notification(false, eb.getMessage('ERR_DEBIT_ADD_PROD'));
                return false;
            } 
            saveDirectDebitNoteData();
        } else {
            if ($('#PVNo').val() == "" || $('#PVNo').val() == "0") {
                p_notification(false, eb.getMessage('ERR_DEBIT_ADD_PV_TO_DN'));
                return false;
            } else if (jQuery.isEmptyObject(debitNoteProducts)) {
                p_notification(false, eb.getMessage('ERR_DEBIT_ADD_PROD'));
                return false;
            } else if(productBeingEdited.length != 0){
                p_notification(false, eb.getMessage('ERR_DEBIT_SAVE_PROD'));
                return false;
            }
            saveDebitNoteData();
        }
        return false;
    });

    $('#debitNoteCancelButton').on('click', function() {
        window.location.reload();
    });

    function saveDebitNoteData() {
        for (var i in checkSubProduct) {
            var $currentRow = getAddRow(i);
            if (!$($currentRow).find('.edit-modal').parent('td').hasClass('hidden')) {
                if (checkSubProduct[i] == 0) {
                    p_notification(false, eb.getMessage('ERR_DEBIT_ADD_SUBPROD', debitNoteProducts[i].productCode));
                    return;
                }
            }
        }
        var formData = {
            debitNoteCode: $('#debitNoteNo').val(),
            location: $('#currentLocation').val(),
            date: $('#debitNoteDate').val(),
            supplier: $('#supplier').val(),
            supplierID: supplierID
        };
        if (validateDebitNoteForm(formData)) {
            $('#debitNoteSaveButton').prop('disabled', true);
            eb.ajax({
                type: 'POST',
                url: BASE_URL + '/debit-note-api/saveDebitNoteDetails',
                data: {
                    debitNoteCode: formData.debitNoteCode,
                    locationID: locationID,
                    products: debitNoteProducts,
                    subProducts: debitNoteSubProducts,
                    date: formData.date,
                    supplierID: supplierID,
                    supplierName: $('#supplier').val(),
                    debitNoteTotalPrice: $('#finaltotal').text().replace(/,/g, ''),
                    debitNoteComment: $('#comment').val(),
                    paymentTermID: $('#paymentTerm').val(),
                    pVID: PVID,
                    locationProduct: locationProducts,
                    dimensionData: dimensionData,
                    ignoreBudgetLimit: ignoreBudgetLimitFlag,
                },
                success: function(respond) {
                    if (respond.status) {
                        p_notification(respond.status, respond.msg);
                        var fileInput = document.getElementById('documentFiles');
                        if(fileInput.files.length > 0) {
                            var form_data = false;
                            if (window.FormData) {
                                form_data = new FormData();
                            }
                            form_data.append("documentID", respond.data.debitNoteID);
                            form_data.append("documentTypeID", 13);
                            
                            for (var i = 0; i < fileInput.files.length; i++) {
                                form_data.append("files[]", fileInput.files[i]);
                            }

                            eb.ajax({
                                url: BASE_URL + '/store-files',
                                type: 'POST',
                                processData: false,
                                contentType: false,
                                data: form_data,
                                success: function(res) {
                                }
                            });
                        }

                        documentPreview(BASE_URL + '/debit-note/viewDebitNoteReceipt/' + respond.data.debitNoteID, 'documentpreview', "/debit-note-api/send-email", function($preview) {
                            $preview.on('hidden.bs.modal', function() {
                                if (!$("#preview:visible").length) {
                                    window.location.reload();
                                }
                            });
                        });
                    } else {
                        if (respond.data == "NotifyBudgetLimit") {
                            bootbox.confirm(respond.msg+' ,Are you sure you want to save ?', function(result) {
                                if (result == true) {
                                    ignoreBudgetLimitFlag = true;
                                    saveDebitNoteData();
                                } else {
                                    setTimeout(function(){ 
                                        location.reload();
                                    }, 3000);
                                }
                            });
                        } else {
                            $('#debitNoteSaveButton').prop('disabled', false);
                            p_notification(respond.status, respond.msg);
                        }
                    }
                },
                error: function (error) {
                    $('#debitNoteSaveButton').prop('disabled', false);
                }
            });
        }
    }

    function saveDirectDebitNoteData() {
        var formData = {
            debitNoteCode: $('#debitNoteNo').val(),
            location: $('#currentLocation').val(),
            date: $('#debitNoteDate').val(),
            supplier: $('#directSupplier').val(),
            supplierID: $('#directSupplier').val()
        };
        if (validateDebitNoteForm(formData)) {
            $('#debitNoteSaveButton').prop('disabled', true);
            eb.ajax({
                type: 'POST',
                url: BASE_URL + '/debit-note-api/saveDirectDebitNoteDetails',
                data: {
                    debitNoteCode: formData.debitNoteCode,
                    locationID: locationID,
                    products: deliverProducts,
                    subProducts: deliverSubProducts,
                    date: formData.date,
                    supplierID: $('#directSupplier').val(),
                    debitNoteTotalPrice: $('#directFinaltotal').text().replace(/,/g, ''),
                    debitNoteComment: $('#comment').val(),
                    paymentTermID: $('#paymentTerm').val(),
                    dimensionData: dimensionData,
                    ignoreBudgetLimit: ignoreBudgetLimitFlag
                },
                success: function(respond) {
                    if (respond.status) {
                        p_notification(respond.status, respond.msg);
                        var fileInput = document.getElementById('documentFiles');
                        if(fileInput.files.length > 0) {
                            var form_data = false;
                            if (window.FormData) {
                                form_data = new FormData();
                            }
                            form_data.append("documentID", respond.data.debitNoteID);
                            form_data.append("documentTypeID", 13);
                            
                            for (var i = 0; i < fileInput.files.length; i++) {
                                form_data.append("files[]", fileInput.files[i]);
                            }

                            eb.ajax({
                                url: BASE_URL + '/store-files',
                                type: 'POST',
                                processData: false,
                                contentType: false,
                                data: form_data,
                                success: function(res) {
                                }
                            });
                        }
                        documentPreview(BASE_URL + '/debit-note/viewDebitNoteReceipt/' + respond.data.debitNoteID, 'documentpreview', "/debit-note-api/send-email", function($preview) {
                            $preview.on('hidden.bs.modal', function() {
                                if (!$("#preview:visible").length) {
                                    window.location.reload();
                                }
                            });
                        });
                    } else {
                        if (respond.data == "NotifyBudgetLimit") {
                            bootbox.confirm(respond.msg+' ,Are you sure you want to save ?', function(result) {
                                if (result == true) {
                                    ignoreBudgetLimitFlag = true;
                                    saveDirectDebitNoteData();
                                } else {
                                    setTimeout(function(){ 
                                        location.reload();
                                    }, 3000);
                                }
                            });
                        } else {
                            $('#debitNoteSaveButton').prop('disabled', false);
                            p_notification(respond.status, respond.msg);
                        }
                    }
                },
                error: function (error) {
                    $('#debitNoteSaveButton').prop('disabled', false);
                }
            });
        }
    }

    function validateDebitNoteForm(formData) {

        if (formData.location == null || formData.location == "") {
            p_notification(false, eb.getMessage('ERR_DEBIT_SELECT_LOCAT'));
            $('#currentLocation').focus();
            return false;
        } else if (formData.debitNoteCode == null || formData.debitNoteCode == "") {
            p_notification(false, eb.getMessage('ERR_DEBIT_NUM_BLANK'));
            return false;
        } else if (formData.date == null || formData.date == "") {
            p_notification(false, eb.getMessage('ERR_DEBIT_DATE_BLANK'));
            $('#debitNoteDate').focus();
            return false;
        } else if (!isDate(formData.date)) {
            p_notification(false, eb.getMessage('ERR_DEBIT_DATE_FORMAT'));
            $('#debitNoteDate').focus();
            return false;
        } else if (formData.supplier == null || formData.supplier == "") {
            p_notification(false, eb.getMessage('ERR_DEBIT_SELECT_SUP'));
            $('#supplier').focus();
            return false;
        } else if (formData.supplierID == null || formData.supplierID == "") {
            p_notification(false, eb.getMessage('ERR_DEBIT_INVALID_SUP'));
            $('#supplier').focus();
            return false;
        } else if (jQuery.isEmptyObject(debitNoteProducts) && directDebitNoteType == false) {
            p_notification(false, eb.getMessage('ERR_DEBIT_ADD_ONEPROD'));
            return false;
        }

        return true;
    }

    function isDate(txtDate)
    {
        var currVal = txtDate;
        if (currVal == '')
            return false;

        //Declare Regex
        var rxDatePattern = /^(\d{4})(\/|-)(\d{1,2})(\/|-)(\d{1,2})$/;
        var dtArray = currVal.match(rxDatePattern); // is format OK?

        if (dtArray == null)
            return false;

        //Checks for mm/dd/yyyy format.
        dtMonth = dtArray[3];
        dtDay = dtArray[5];
        dtYear = dtArray[1];
        if (dtMonth < 1 || dtMonth > 12)
            return false;
        else if (dtDay < 1 || dtDay > 31)
            return false;
        else if ((dtMonth == 4 || dtMonth == 6 || dtMonth == 9 || dtMonth == 11) && dtDay == 31)
            return false;
        else if (dtMonth == 2)
        {
            var isleap = (dtYear % 4 == 0 && (dtYear % 100 != 0 || dtYear % 400 == 0));
            if (dtDay > 29 || (dtDay == 29 && !isleap))
                return false;
        }
        return true;
    }

    var nowTemp = new Date();
    var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);

    var checkin = $('#debitNoteDate').datepicker({onRender: function(date) {
            return date.valueOf() < now.valueOf() ? ((U_DATE_R_OVERRIDE)?'':'disabled') : '';
        },
        format: 'yyyy-mm-dd'
    }).on('changeDate', function(ev) {
        checkin.hide();
    }).data('datepicker');


     function clearProductDataForNewDirectDebitNote() {
        var $currentRow = getAddRowForDirectDebit();
        $currentRow.removeClass('hidden');
        deliverProducts = {};
        deliverSubProducts = {};
        productsTax = {};
        productsTotal = {};
        $('#comment').val('');
        $('#directSubtotal').html('0.00');
        $('#directFinaltotal').html('0.00');
    }

    function setProductTypeahead(hideDuplicate) {

        var $currentRow = getAddRowForDirectDebit();
        $('#item_code', $currentRow).selectpicker();
        if (hideDuplicate) {
            $(".btn-group.bootstrap-select.form-control.show-tick:not(:first)", $currentRow).remove();
        }
        var documentType = 'invoice';
        loadDropDownFromDatabase('/productAPI/search-location-products-for-dropdown/serial-search', locationID, 1, '#item_code', $currentRow, '', documentType);
        
        $('#item_code', $currentRow).on('change', function() {
            if ($(this).val() != 'add' && $(this).val() != 0 && selectedId != $(this).val()) {
                selectedId = $(this).val();
                var itemMapper = $('#item_code').data('extra');
                var pid = itemMapper[selectedId].pid;

                eb.ajax({
                    type: 'POST',
                    url: BASE_URL + '/productAPI/get-location-product-details',
                    data: {productID: pid, locationID: locationID},
                    success: function(respond) {
                        selectProductForTransfer(respond.data);
                        $('#itemUnitPrice', $currentRow).trigger('blur');
                    }
                });
            }
        });
    }

    function selectProductForTransfer(selectedlocationProduct) {

        var sameItemFlag = false;
        $.each(deliverProducts , function(i, delPro){
            // check if product is already selected
            if (delPro.productID == selectedlocationProduct.pID) {
                p_notification(false, eb.getMessage('ERR_DELI_ALREADY_SELECT_PROD'));
                sameItemFlag = true;
            }
        });

        if (sameItemFlag == false) {
            var $currentRow = getAddRowForDirectDebit();
            clearProductRowForDirectDebit($currentRow);

            currentProducts[selectedlocationProduct.pID] = selectedlocationProduct;
            var productID = selectedlocationProduct.pID;
            var productCode = selectedlocationProduct.pC;
            var productType = selectedlocationProduct.pT;
            var productName = selectedlocationProduct.pN;
            var giftCard = selectedlocationProduct.giftCard;
            var availableQuantity = (selectedlocationProduct.LPQ == null) ? 0 : selectedlocationProduct.LPQ;
            var defaultSellingPrice = selectedlocationProduct.dPP;
            if (selectedlocationProduct.pPDP != null) {
                var productDiscountPrecentage = parseFloat(selectedlocationProduct.pPDP).toFixed(2);
            } else {
                var productDiscountPrecentage = 0;
            }
            
            if (selectedlocationProduct.pPDV != null) {
                var productDiscountValue = parseFloat(selectedlocationProduct.pPDV).toFixed(2);
            } else {
                var productDiscountValue = 0;
            }

            if (priceListItems[productID] !== undefined && parseFloat(priceListItems[productID].itemPrice) > 0) {
                defaultSellingPrice = parseFloat(priceListItems[productID].itemPrice);
                var itemDiscountType = priceListItems[productID].itemDiscountType;
                productDiscountValue = (itemDiscountType == 1) ? priceListItems[productID].itemDiscount : 0;
                productDiscountPrecentage = (itemDiscountType == 2) ? priceListItems[productID].itemDiscount : 0;
            }
            defaultSellingPriceData[productID] = defaultSellingPrice;

            $currentRow.data('id', selectedlocationProduct.pID);
            $currentRow.data('icid', rowincrementID);
            $currentRow.data('locationproductid', selectedlocationProduct.lPID);
            $currentRow.data('proIncID', rowincrementID);
            $currentRow.data('stockupdate', true);
            $('#availableQuantity', $currentRow).parent().addClass('input-group');
            $('#deliverQuanity', $currentRow).parent().addClass('input-group');
            $("input[name='discount']", $currentRow).prop('readonly', false);
            $("#item_code", $currentRow).data('PT', productType);
            $("#item_code", $currentRow).data('PN', productName);
            $("#item_code", $currentRow).data('PC', productCode);
            $("#item_code", $currentRow).data('GiftCard', giftCard);
            $("input[name='availableQuantity']", $currentRow).val(availableQuantity).prop('readonly', true);
            $("input[name='unitPrice']", $currentRow).val(parseFloat(defaultSellingPrice)).addUomPrice(selectedlocationProduct.uom);

            var baseUom;
            for (var j in selectedlocationProduct.uom) {
                if (selectedlocationProduct.uom[j].pUBase == 1) {
                    baseUom = selectedlocationProduct.uom[j].uomID;
                }
            }

            if (selectedlocationProduct.dPEL == 1 || (priceListItems[productID] != undefined && parseFloat(priceListItems[productID].itemPrice) > 0)) {
                if (selectedlocationProduct.pPDV != null) {
                    productDiscountValue = parseFloat(productDiscountValue).toFixed(2);
                    $("input[name='debitDiscount']", $currentRow).val(productDiscountValue).addUomPrice(selectedlocationProduct.uom, baseUom);
                    $("input[name='debitDiscount']", $currentRow).siblings('.uom-price-select').find('.uom_price_drop_down').attr('disabled', true);
                    $("input[name='debitDiscount']", $currentRow).addClass('value');
                    if ($("input[name='debitDiscount']", $currentRow).hasClass('precentage')) {
                        $("input[name='debitDiscount']", $currentRow).removeClass('precentage');
                    }
                    $(".sign", $currentRow).text('Rs');
                } else if (selectedlocationProduct.pPDP != null){
                    productDiscountPrecentage = parseFloat(productDiscountPrecentage).toFixed(2);
                    $("input[name='debitDiscount']", $currentRow).val(productDiscountPrecentage);
                    $("input[name='debitDiscount']", $currentRow).addClass('precentage');
                    if ($("input[name='debitDiscount']", $currentRow).hasClass('value')) {
                        $("input[name='debitDiscount']", $currentRow).removeClass('value');
                    }
                    $(".sign", $currentRow).text('%');
                }
            } else {
                $("input[name='debitDiscount']", $currentRow).prop('readonly', true);
            }
            $("input[name='deliverQuanity']", $currentRow).prop('readonly', false);
            
            // add uom list
            if (productType != 2) {
                $("input[name='availableQuantity'], input[name='deliverQuanity']", $currentRow).addUom(selectedlocationProduct.uom);
            } else {
                $("input[name='deliverQuanity']", $currentRow).addUom(selectedlocationProduct.uom);
                $("input[name='availableQuantity']", $currentRow).css("display", "none");
            }
            $('.uomqty').attr("id", "itemQuantity");
            $('#unitPrice',$currentRow).siblings('.uomPrice').attr("id", "itemUnitPrice");
            var deleted = 0;
            
            // clear old rows
            $("#batch_data tr:not(.hidden)").remove();
            addBatchProduct(productID, productCode, deleted);
            addSerialProduct(productID, productCode, deleted, []);
            addSerialBatchProduct(productID, productCode, deleted);

            //check this product serail or batch
            if (!$.isEmptyObject(selectedlocationProduct.serial)) {
                $('#product-batch-modal .serial-auto-select').removeClass('hidden');
                $('#numberOfRow').val('');
            } else {
                $('#product-batch-modal .serial-auto-select').addClass('hidden');
            }


            var rowincID = rowincrementID;
            $("tr", '#batch_data').each(function() {
                var $thisSubRow = $(this);
                for (var i in subProductsForCheck[productID]) {
                    if (subProductsForCheck[productID][i].serialID !== undefined) {
                        if ($(".serialID", $thisSubRow).data('id') == subProductsForCheck[productID][i].serialID) {
                            $("input[name='deliverQuantityCheck']", $thisSubRow).prop('checked', true);
                            $("input[name='warranty']", $thisSubRow).val(subProductsForCheck[productID][i].warranty);
                            $("input[name='deliverQuantityCheck']", $thisSubRow).attr('disabled', true);
                        }

                    } else if (subProductsForCheck[productID][i].batchID !== undefined && (rowincID == subProductsForCheck[productID][i].incrementID) ) {
                        if ($(".batchCode", $thisSubRow).data('id') == subProductsForCheck[productID][i].batchID) {
                            $(this).find("input[name='deliverQuantity']").val(subProductsForCheck[productID][i].qtyByBase).change();
                        }
                    }
                }
            });

            //check this product serail or batch
            if (!$.isEmptyObject(selectedlocationProduct.serial)) {
                $('#add-product-batch .serial-auto-select').removeClass('hidden');
                $('#numberOfRow').val('');
            } else {
                $('#add-product-batch .serial-auto-select').addClass('hidden');
            }

            // check if any batch / serial products exist
            if ($.isEmptyObject(selectedlocationProduct.batch) &&
                    $.isEmptyObject(selectedlocationProduct.serial) &&
                    $.isEmptyObject(selectedlocationProduct.batchSerial)) {
                $currentRow.removeClass('subproducts');
                $('.edit-modal', $currentRow).parent().addClass('hidden');
                $("td[colspan]", $currentRow).attr('colspan', 2);
                $("input[name='deliverQuantity']", $currentRow).prop('readonly', false).focus();
            } else {
                $currentRow.addClass('subproducts');
                $('.edit-modal', $currentRow).parent().removeClass('hidden');
                $("td[colspan]", $currentRow).attr('colspan', 1);
                $("input[name='deliverQuantity']", $currentRow).prop('readonly', true);
                $('#addDirectDebitProductsModal').data('icid', rowincrementID);
                $('#addDirectDebitProductsModal').data('proIncID', rowincrementID);
                $('#addDirectDebitProductsModal').modal('show');
            }

            setTaxListForProductDirectDebit(selectedlocationProduct.pID, $currentRow);
            $currentRow.attr('id', 'product' + selectedlocationProduct.pID);
        }
    }

    function clearProductRowForDirectDebit($currentRow) {
        $("input[name='availableQuantity']", $currentRow).val('').siblings('.uomqty,.uom-select').remove().end().show();
        $("input[name='deliverQuanity']", $currentRow).val('').siblings('.uomqty,.uom-select').remove().end().show();
        $("input[name='unitPrice']", $currentRow).val('').siblings('.uomPrice,.uom-price-select').remove().end().show();
        $("input[name='debitDiscount']", $currentRow).val('').siblings('.uomPrice,.uom-price-select').remove().end().show();
        $(".tempLi", $currentRow).remove('');
        $("#taxApplied", $currentRow).removeClass('glyphicon-check');
        $("#taxApplied", $currentRow).addClass('glyphicon-unchecked');
        $(".uomList", $currentRow).remove('');
    }

    function addBatchProduct(productID, productCode, deleted) {

        if ((!$.isEmptyObject(currentProducts[productID].batch))) {
            $('.warranty').removeClass('hidden');
            $('.expDate').removeClass('hidden');
            batchProduct = currentProducts[productID].batch;
            for (var i in batchProduct) {
                if (deleted != 1 && batchProduct[i].PBQ != 0) {
                    if ((!$.isEmptyObject(currentProducts[productID].productIDs))) {
                        if (currentProducts[productID].productIDs[i]) {
                            continue;
                        }
                    }

                    var batchKey = productCode + "-" + batchProduct[i].PBID;
                    var $clonedRow = $($('.direct_batch_row').clone());
                    $clonedRow
                            .data('id', i)
                            .removeAttr('id')
                            .removeClass('hidden direct_batch_row')
                            .addClass('remove_row');
                    $(".batchCode", $clonedRow).html(batchProduct[i].PBC).data('id', batchProduct[i].PBID);
                    $("input[name='availableQuantity']", $clonedRow).val(batchProduct[i].PBQ);
                    $("input[name='deliverQuantityCheck']", $clonedRow).remove();
                    $("input[name='warranty']", $clonedRow).prop('readonly', true);
                    $("input[name='warranty']", $clonedRow).val(batchProduct[i].PBWoD);
                    $("input[name='expireDate']", $clonedRow).val(batchProduct[i].PBExpD);
                    $("input[name='btPrice']", $clonedRow).val(parseFloat(batchProduct[i].PBUP).toFixed(2));
                    if (priceListItems[productID] != undefined && parseFloat(priceListItems[productID].itemPrice) > 0) {
                        $("input[name='btPrice']", $clonedRow).val(parseFloat(priceListItems[productID].itemPrice).toFixed(2));
                    }
                    if (batchProducts[batchKey]) {
                        $("input[name='deliverQuantity']", $clonedRow).val(batchProducts[batchKey].Qty);
                    }
                    if(!(batchProduct[i].PBExpD == null || batchProduct[i].PBExpD == '0000-00-00')){
                        var expireCheck = checkEpireData(batchProduct[i].PBExpD);
                        if(expireCheck){
                            $("input[name='deliverQuantity']", $clonedRow).attr('disabled',true);
                        }
                    }

                    $clonedRow.insertBefore('.direct_batch_row');
                    $("input[name='availableQuantity'],input[name='deliverQuantity']", $clonedRow).addUom(currentProducts[productID].uom);
                } else {
                    batchProducts[batchKey] = undefined;
                }
            }
        }
    }

    function addSerialProduct(productID, productCode, deleted) {
        if ((!$.isEmptyObject(currentProducts[productID].serial))) {
            productSerial = currentProducts[productID].serial;

            var subProList = deliverSubProducts;
            var list_order = new Array();
            for (var i in subProList) {
                if (subProList[i] !== undefined && productSerial[subProList[i][0]['serialID']]) {
                    list_order.push(subProList[i][0]['serialID']);
                }
            }

            for (var i in productSerial) {
                if (!_.where(list_order, i).length) {
                    list_order.push(i);
                } else {
                    continue;
                }
            }

            for (var id in list_order) {
                var i = list_order[id];
                if (deleted != 1) {
                    var serialKey = productCode + "-" + productSerial[i].PSID;
                    var serialQty = (productSerial[i].PSS == 1) ? 0 : 1;
                    var $clonedRow = $($('.direct_batch_row').clone());
                    $clonedRow
                            .data('id', i)
                            .removeAttr('id')
                            .removeClass('hidden direct_batch_row')
                            .addClass('remove_row');
                    $(".serialID", $clonedRow)
                            .html(productSerial[i].PSC)
                            .data('id', productSerial[i].PSID);
                    $("input[name='availableQuantity']", $clonedRow).val(serialQty);
                    $("input[name='deliverQuantity']", $clonedRow).parent().remove();
                    if (currentProducts[productID].giftCard == 0) {
                        $(".warranty").removeClass('hidden');
                        $(".warrant").removeClass('hidden');
                        $(".expDate").removeClass('hidden');
                        let wPeriod = "";
                        if(productSerial[i].PSWoT==='4'){
                            wPeriod = "Years"
                        }else if(productSerial[i].PSWoT==='3'){
                            wPeriod = "Months"
                        }else if(productSerial[i].PSWoT==='2'){
                            wPeriod = "Weeks"
                        }else{
                            wPeriod = "Days"
                        }
                        $("input[name='warrantyType']",$clonedRow).val(wPeriod).attr('disabled',true);
                        $("input[name='warranty']", $clonedRow).val(productSerial[i].PSWoD);
                        $("input[name='expireDate']", $clonedRow).val(productSerial[i].PSExpD);
                        if(!(productSerial[i].PSExpD == null || productSerial[i].PSExpD == '0000-00-00')){
                            var expireCheck = checkEpireData(productSerial[i].PSExpD);
                            if(expireCheck){
                                $("input[name='deliverQuantityCheck']", $clonedRow).attr('disabled',true);
                            }
                        }
                    }

                    if (serialProducts[serialKey]) {
                        if (serialProducts[serialKey].Qty == 1) {
                            $("input[name='deliverQuantityCheck']", $clonedRow).attr('checked', true);
                        }
                    }

                    $clonedRow.insertBefore('.direct_batch_row');
                    $("input[name='availableQuantity'],input[name='deliverQuantity']", $clonedRow).addUom(currentProducts[productID].uom);
                } else {
                    serialProducts[serialKey] = undefined;
                }
            }
        }
    }

    function addSerialBatchProduct(productID, productCode, deleted) {

        if ((!$.isEmptyObject(currentProducts[productID].batchSerial))) {
            productBatchSerial = currentProducts[productID].batchSerial;
            for (var i in productBatchSerial) {
                if (deleted != 1) {
                    var serialKey = productCode + "-" + productBatchSerial[i].PBID;
                    var serialQty = (productBatchSerial[i].PSS == 1) ? 0 : 1;
                    var $clonedRow = $($('.direct_batch_row').clone());
                    $clonedRow
                            .data('id', i)
                            .removeAttr('id')
                            .removeClass('hidden direct_batch_row')
                            .addClass('remove_row');
                    $(".serialID", $clonedRow)
                            .html(productBatchSerial[i].PSC)
                            .data('id', productBatchSerial[i].PSID);
                    $(".batchCode", $clonedRow)
                            .html(productBatchSerial[i].PBC)
                            .data('id', productBatchSerial[i].PBID);
                    $("input[name='availableQuantity']", $clonedRow).val(serialQty);
                    $("input[name='deliverQuantity']", $clonedRow).parent().remove();
                    $("input[name='btPrice']", $clonedRow).val(parseFloat(productBatchSerial[i].PBUP).toFixed(2));
                    if (priceListItems[productID] != undefined && parseFloat(priceListItems[productID].itemPrice) > 0) {
                        $("input[name='btPrice']", $clonedRow).val(parseFloat(priceListItems[productID].itemPrice).toFixed(2));
                    }
                    if (currentProducts[productID].giftCard == 0) {
                        $(".warranty").removeClass('hidden');
                        $(".expDate").removeClass('hidden');
                        $("input[name='warranty']", $clonedRow).val(productBatchSerial[i].PBSWoD);
                        $("input[name='expireDate']", $clonedRow).val(productBatchSerial[i].PBSExpD);
                        if(!(productBatchSerial[i].PBSExpD == null || productBatchSerial[i].PBSExpD == '0000-00-00')){
                            var expireCheck = checkEpireData(productBatchSerial[i].PBSExpD);
                            if(expireCheck){
                                $("input[name='deliverQuantityCheck']", $clonedRow).attr('disabled',true);
                            }
                        }
                    }
                    if (batchProducts[serialKey]) {
                        if (batchProducts[serialKey].Qty == 1) {
                            $("input[name='deliveryQuantityCheck']", $clonedRow).attr('checked', true);
                        }
                    }

                    $clonedRow.insertBefore('.direct_batch_row');
                    $("input[name='availableQuantity']", "input[name='deliverQuantity']", $clonedRow).addUom(currentProducts[productID].uom);
                } else {
                    batchProducts[serialKey] = undefined;
                }
            }
        }
    }

    function setTaxListForProductDirectDebit(productID, $currentRow) {

        if ((!jQuery.isEmptyObject(currentProducts[productID].tax))) {
            productTax = currentProducts[productID].tax;
            $currentRow.find('#taxApplied').removeClass('glyphicon glyphicon-unchecked');
            $currentRow.find('#taxApplied').addClass('glyphicon glyphicon-check');
            $currentRow.find('.tempLi').remove();
            $currentRow.find('#addNewTax').attr('disabled', false);
            for (var i in productTax) {
                var clonedLi = $($currentRow.find('#sampleLi').clone()).removeClass('hidden').attr('id', 'li_' + i).addClass("tempLi");
                clonedLi.children(".taxChecks").attr('id', productID + '_' + i).prop('checked', true).addClass('addNewTaxCheck');
                if (productTax[i].tS == 0) {
                    clonedLi.children(".taxChecks").attr('disabled', 'disabled');
                    clonedLi.children(".taxName").addClass('crossText');
                }
                clonedLi.children(".taxName").html('&nbsp&nbsp' + productTax[i].tN + '&nbsp&nbsp' + productTax[i].tP + '%').attr('for', productID + '_' + i);
                clonedLi.insertBefore($currentRow.find('#sampleLi'));
            }
        } else {
            $('#taxApplied').addClass('glyphicon glyphicon-unchecked');
            $currentRow.find('#addNewTax').attr('disabled', true);
        }
    }

    $directProductTable.on('focusout', '#deliverQuanity,#unitPrice,#debitDiscount,.uomqty,.uomPrice', function() {
        var $thisRow = $(this).parents('tr');
        var productID = $thisRow.data('id');
        calculateItemTotal($thisRow, productID);
    });

    function calculateItemTotal(thisRow, productID) {
        var thisRow = thisRow;
        var productID = productID;
        var checkedTaxes = Array();
        thisRow.find('input:checkbox.addNewTaxCheck').each(function() {
            if (this.checked) {
                var cliTID = this.id;
                var tTaxID = cliTID.split('_')[1];
                checkedTaxes.push(tTaxID);
            }
        });
        var productType = thisRow.find('#item_code').data('PT');
        var tmpItemQuentity = thisRow.find('#deliverQuanity').val();
        if (productType == 2 && tmpItemQuentity == 0) {
            tmpItemQuentity = 1;
        }
        var tmpItemTotal = (toFloat(thisRow.find('#unitPrice').val()) * toFloat(tmpItemQuentity));
        var tmpItemCost = tmpItemTotal;
 
        if (currentProducts[productID].dPEL == 1 && (currentProducts[productID].pPDV != null || currentProducts[productID].pPDP != null)) {
            //current product dsiscount (May changed due to insertion of promotion but there's a validation below)
            var currentDiscount = 0;
            if (thisRow.hasClass('add-direct-debit-row') || (discountEditedManually[thisRow.data('icid')] !== undefined && discountEditedManually[thisRow.data('icid')] == true) || thisRow.hasClass('copied')) {
                currentDiscount = thisRow.find('#debitDiscount').val();
                discountEditedManually[thisRow.data('icid')] = false;
            } else {
                if (currentProducts[productID].pPDV != 0) {
                    currentDiscount = currentProducts[productID].pPDV;
                } else {
                    currentDiscount = currentProducts[productID].pPDP;
                }
            }
            if (currentProducts[productID].pPDV != null) {
                thisRow.find('#debitDiscount').val(currentDiscount);
                thisRow.find('#debitDiscount').addClass('value');
                if (thisRow.find('#debitDiscount').hasClass('precentage')) {
                    thisRow.find('#debitDiscount').removeClass('precentage');
                }
                thisRow.find('.sign').text('Rs');
            } 

            if (currentProducts[productID].pPDP != null){
                thisRow.find('#debitDiscount').val(currentDiscount);
                thisRow.find('#debitDiscount').addClass('precentage');
                if (thisRow.find('#debitDiscount').hasClass('value')) {
                    thisRow.find('#debitDiscount').removeClass('value');
                }
                thisRow.find('.sign').text('%');
            } 
        } else {
            thisRow.find('#debitDiscount').prop('readonly', true);
        }

        if (thisRow.find('#debitDiscount').hasClass('value')) {
            var tmpPDiscountvalue = isNaN(thisRow.find('#debitDiscount').val()) ? 0 : thisRow.find('#debitDiscount').val();
            if (tmpPDiscountvalue > 0) {
                var validatedDiscount = validateDiscount(productID, 'val', tmpPDiscountvalue, thisRow.find('#unitPrice').val());
                thisRow.find('#debitDiscount').val(validatedDiscount.toFixed(2));
                tmpItemCost -= tmpItemQuentity * validatedDiscount;
            }
        } else {
            var tmpPDiscount = isNaN(thisRow.find('#debitDiscount').val()) ? 0 : thisRow.find('#debitDiscount').val();
            if (tmpPDiscount > 0) {
                var validatedDiscount = validateDiscount(productID, 'per', tmpPDiscount, tmpItemQuentity);
                thisRow.find('#debitDiscount').val(validatedDiscount.toFixed(2));
                tmpItemCost -= (tmpItemTotal * toFloat(validatedDiscount) / toFloat(100));
            }
        }
        
        if (checkedTaxes) {
            currentItemTaxResults = calculateItemCustomTax(tmpItemCost, checkedTaxes);
            currentTaxAmount = (currentItemTaxResults == null) ? null : currentItemTaxResults.tTA;
        } else {
            currentTaxAmount = 0;
            currentItemTaxResults = '';
        }
        var itemCost = toFloat(tmpItemCost) + toFloat(currentTaxAmount);
        productsTotal[productID] = itemCost;
        productsTax[productID] = currentItemTaxResults;
        
        thisRow.find('#addNewTotal').html(accounting.formatMoney(itemCost));
        if (productsTax[productID] != null) {
            if (jQuery.isEmptyObject(productsTax[productID].tL)) {
                thisRow.find('#taxApplied').addClass('glyphicon glyphicon-unchecked');
            } else {
                thisRow.find('#taxApplied').removeClass('glyphicon glyphicon-unchecked');
                thisRow.find('#taxApplied').addClass('glyphicon glyphicon-check');
            }
        }
    }

    function validateDiscount(productID, discountType, currentDiscount, unitPrice) {
        var defaultProductData = currentProducts[productID];
        var newDiscount = 0;
        if (defaultProductData.dPEL == 1) {
            if (discountType == 'val') {
                if (toFloat(defaultProductData.pPDV) !=0 && toFloat(currentDiscount) > toFloat(defaultProductData.pPDV)) {
                    if (priceListItems[productID] && parseFloat(priceListItems[productID].itemPrice) > 0) {
                        newDiscount = toFloat(currentDiscount);
                    } else {
                        newDiscount = toFloat(defaultProductData.pPDV);
                        p_notification(false, eb.getMessage('ERR_INVO_DISCOUNT_VALUE'));
                    }
                } else if (toFloat(defaultProductData.pPDV) ==0) {
                    if (toFloat(currentDiscount) > toFloat(unitPrice)) {
                        newDiscount = toFloat(unitPrice);
                        p_notification(false, eb.getMessage('ERR_INVO_DISCOUNT_VALUE_UNITE_P'));
                    } else {
                        newDiscount = toFloat(currentDiscount);    
                    }
                } else {
                    newDiscount = toFloat(currentDiscount);
                }

            } else {
                if (toFloat(defaultProductData.pPDP) != 0 && toFloat(currentDiscount) > toFloat(defaultProductData.pPDP)) {
                    if (priceListItems[productID] && parseFloat(priceListItems[productID].itemPrice) > 0) {
                        newDiscount = toFloat(currentDiscount);
                    } else {
                        newDiscount = toFloat(defaultProductData.pPDP);
                        p_notification(false, eb.getMessage('ERR_INVO_DISC_PERCENTAGE'));
                    }
                } else {
                    newDiscount = toFloat(currentDiscount);
                }
            }
        } else {
            if (priceListItems[productID] && parseFloat(priceListItems[productID].itemPrice) > 0) {
                newDiscount = toFloat(currentDiscount);
            } else {
                p_notification(false, eb.getMessage('ERR_INVO_DISABLE_DISC'));
                newDiscount = 0.00;
            }
        }
        return newDiscount;
    }

    $directProductTable.on('click', '#selectAll', function() {
        $(this).parents('#addTaxUl').find('input:checkbox.addNewTaxCheck').each(function() {
            if (!this.checked) {
                this.checked = true;
            }
        });
        $(this).parents('tr').find("#unitPrice").trigger(jQuery.Event("focusout"));
    });

    $directProductTable.on('click', '#deselectAll', function() {
        $(this).parents('#addTaxUl').find('input:checkbox.addNewTaxCheck').each(function() {
            if (this.checked) {
                this.checked = false;
            }
        });
        $(this).parents('tr').find("#unitPrice").trigger(jQuery.Event("focusout"));
    });

    $directProductTable.on('click', 'ul.dropdown-menu', function(e) {
        e.stopPropagation();
    });

    $directProductTable.on('change', '.taxChecks.addNewTaxCheck', function() {
        $(this).parents('tr').find("#unitPrice").trigger(jQuery.Event("focusout"));
    });


    $directProductTable.on('click', 'button.addDirectDebit, button.saveDirectDebit', function(e) {
        e.preventDefault();
        var $thisRow = $(this).parents('tr');
        var thisVals = getCurrentProductDataForDirectDebit($thisRow);

        $("input[name='availableQuantity']", $thisRow).prop('readonly', true);
        if ((thisVals.productCode) === undefined && (thisVals.productName) === undefined) {
            p_notification(false, eb.getMessage('ERR_DELI_SELECT_PROD'));
            $("#item_code", $thisRow).focus();
            return false;
        }

        if ((thisVals.productPrice.trim()) < 0) {
            p_notification(false, eb.getMessage('ERR_INVO_SET_PROD'));
            $("input[name='unitPrice']", $thisRow).focus();
            return false;
        }
        if (thisVals.productType != 2 && (isNaN(parseFloat(thisVals.deliverQuantity.qty)) || parseFloat(thisVals.deliverQuantity.qty) <= 0)) {
            p_notification(false, eb.getMessage('ERR_DELI_ENTER_QUAN'));
            $("input[name='deliverQuantity']", $thisRow).focus();
            return false;
        }

        if (thisVals.productType != 2 && thisVals.stockUpdate && parseFloat(thisVals.deliverQuantity.qty) > parseFloat(thisVals.availableQuantity.qty)) {
            p_notification(false, eb.getMessage('ERR_DPR_QUAN'));
            $("input[name='deliverQuantity']", $thisRow).focus();
            if (checkSubProduct[thisVals.productIncID] === undefined) {
                return false;
            }
        }

        if (thisVals.productDiscount) {
            if (isNaN(parseFloat(thisVals.productDiscount)) || parseFloat(thisVals.productDiscount) < 0) {
                p_notification(false, eb.getMessage('ERR_DELI_VALID_DISCOUNT'));
                $("input[name='debitDiscount']", $thisRow).focus();
                return false;
            }
        }

        if (thisVals.productTotal < 0) {
            if (isNaN(parseFloat(thisVals.productTotal)) || parseFloat(thisVals.productTotal) <= 0) {
                p_notification(false, eb.getMessage('ERR_INVO_PROD_TOTAL'));
                $("input[name='unitPrice']", $thisRow).focus();
                return false;
            }
        }

        if ($thisRow.hasClass('subproducts')) {
            var seletedProdutQty = 0;
            if (!$.isEmptyObject(deliverSubProducts[thisVals.productIncID])) {
                $.each(deliverSubProducts[thisVals.productIncID] , function(i, currSubProduct){
                    seletedProdutQty += currSubProduct.qtyByBase
                });
            }
            if ($.isEmptyObject(deliverSubProducts[thisVals.productIncID])) {
                p_notification(false, eb.getMessage('ERR_DELI_SUB_PRODUCT_CHECK', thisVals.productCode));
                $('.edit-modal', $thisRow).focus();
                return false;
            }else if(seletedProdutQty != thisVals.deliverQuantity.qty){
                p_notification(false, eb.getMessage('ERR_DELI_SUB_PRODUCT_CHECK_WITH_PQTY', thisVals.productCode));
                $('.edit-modal', $thisRow).focus();
                return false;
            }
        }

        // if add button is clicked
        if ($(this).hasClass('addDirectDebit')) {
            if(!$thisRow.hasClass('copied')){
                $thisRow.data('icid', rowincrementID);
            }
            var $currentRow = $thisRow;
            var $flag = false;
            if ($currentRow.hasClass('add-direct-debit-row')) {
                $flag = true;
            }

            $currentRow
                    .removeClass('edit-row')
                    .find("#item_code, input[name='deliverQuanity'],input[name='availableQuantity'],input[name='unitPrice'],input[name='debitDiscount'], input[name='uomqty']").prop('readonly', true).end()
                    .find('td').find('#addNewTax').attr('disabled', true)
                    .find("button.delete").removeClass('disabled');
            $currentRow.find("input[name='deliverQuanity']").change();
            $currentRow.find("input[name='unitPrice']").change();
            $currentRow.find("input[name='debitDiscount']").change();
            $currentRow.find('#deliveryNoteDiscount').trigger('focusout');
            $currentRow.removeClass('add-direct-debit-row');
            discountEditedManually[$thisRow.data('icid')] = true;
            $currentRow.find("#item_code").prop('disabled', true).selectpicker('render');
            if ($flag) {
                var $newRow = $($addDirectRowSample.clone()).removeClass('sample hidden').appendTo($('#add-new-item-row-for-dPr', $directProductTable));
                hideDuplicate = true;
                setProductTypeahead(hideDuplicate);
                $newRow.find("#item_code").focus();
            }
        } 

        $(this, $currentRow).parent().find('.addDirectDebit').addClass('hidden');
        $(this, $currentRow).parent().find('.saveDirectDebit').addClass('hidden');
        $(this, $currentRow).parent().parent().find('.delete').removeClass('disabled');
        // if batch product modal is available for this product
        if ($thisRow.hasClass('subproducts')) {
            $('.edit-modal', $currentRow).parent().addClass('hidden');
            $("td[colspan]", $currentRow).attr('colspan', 2);
        }

        //append product increment ID to product ID
        if ($(this).hasClass('saveDirectDebit')) {
            var saveProductID = $thisRow.data('id') + '_' + $thisRow.data('proIncID');
            deliverProducts[saveProductID] = thisVals;
        } else {
            var increID = rowincrementID;
            if($thisRow.data('proIncID')!== undefined){
                increID = $thisRow.data('proIncID')
            }
            thisVals = getCurrentProductDataForDirectDebit($thisRow);
            deliverProducts[thisVals.productID + '_' + increID] = thisVals;
            if(!$thisRow.hasClass('copied')){
                $thisRow.data('proIncID', increID);
                rowincrementID++;
            }
            $thisRow.removeClass('copied');
        }
        setTotalTaxForDirectDebit();
        setDirectDebitTotalCost();
        selectedId = null;
    });

    function setDirectDebitTotalCost() {
        var subTotal = 0;
        for (var i in deliverProducts) {
            subTotal += toFloat(deliverProducts[i].productTotal);
        }
        if (subTotal < 0) {
            subTotal = 0;
        }
        
        $('#directSubtotal').html(accounting.formatMoney(subTotal));
        var finalTotal = subTotal;

        $('#directFinaltotal').html(accounting.formatMoney(finalTotal));
        return  finalTotal;
    }

    function setTotalTaxForDirectDebit() {
        totalTaxList = {};
        if (TAXSTATUS) {
            for (var k in deliverProducts) {
                for (var l in deliverProducts[k].pTax.tL) {
                    if (totalTaxList.hasOwnProperty(l)) {
                        var curTA = totalTaxList[l].tA;
                        var newTA = (toFloat(curTA) + toFloat(deliverProducts[k].pTax.tL[l].tA));
                        totalTaxList[l].tA = newTA.toFixed(2);
                    } else {
                        totalTaxList[l] = {tN: deliverProducts[k].pTax.tL[l].tN, tP: deliverProducts[k].pTax.tL[l].tP, tA: deliverProducts[k].pTax.tL[l].tA, susTax: deliverProducts[k].pTax.tL[l].susTax};
                    }
                }
            }

            var totalTaxHtml = "";
            for (var t in totalTaxList) {
                totalTaxHtml += totalTaxList[t].tN + "(" + totalTaxList[t].tP + "%)&nbsp" + accounting.formatMoney(totalTaxList[t].tA) + "<br>";
            }
            $('#directTotalTaxShow').html(totalTaxHtml);
        }
    }

    function getCurrentProductDataForDirectDebit($thisRow) {

        var productDesc = ($('.itemDescText', $thisRow).val() != '') ? $('.itemDescText', $thisRow).val() : '';
        var discountType = '';
        if ($("input[name='debitDiscount']", $thisRow).hasClass('value')) {
            discountType = 'value';
        } else if ($("input[name='debitDiscount']", $thisRow).hasClass('precentage')) {
            discountType = 'precentage';
        }

        var selected_serials = $thisRow.find('.edit-modal').data('selectedSids');

        var pid = rowincrementID;
        if ($thisRow.data('proIncID') !== undefined) {
            pid = $thisRow.data('proIncID');
        }
        var thisVals = {
            productID: $thisRow.data('id'),
            documentTypeID: (typeof $thisRow.data('documenttypeid') != 'undefined') ? $thisRow.data('documenttypeid') : null,
            documentID: (typeof $thisRow.data('prodocuid') != 'undefined') ? $thisRow.data('prodocuid') : null,
            locationID: (typeof $thisRow.data('locationid') != 'undefined') ? $thisRow.data('locationid') : $('#idOfLocation').val(),
            locationProductID: (typeof $thisRow.data('locationproductid') != 'undefined') ? $thisRow.data('locationproductid') : null,
            productIncID: $thisRow.data('id') + '_' + pid,
            productCode: $("#item_code", $thisRow).data('PC'),
            productName: $("#item_code", $thisRow).data('PN'),
            productPrice: $("input[name='unitPrice']", $thisRow).val(),
            productDiscount: $("input[name='debitDiscount']", $thisRow).val(),
            productDiscountType: discountType,
            productTotal: productsTotal[$thisRow.data('id')],
            pTax: productsTax[$thisRow.data('id')],
            productType: $("#item_code", $thisRow).data('PT'),
            productDescription: productDesc,
            stockUpdate: $thisRow.data('stockupdate'),
            giftCard: $("#item_code", $thisRow).data('GiftCard'),
            availableQuantity: {
                qty: $("input[name='availableQuantity']", $thisRow).val(),
            },
            deliverQuantity: {
                qty: $("input[name='deliverQuanity']", $thisRow).val(),
            },
            selected_serials: selected_serials,
            selectedUomId: $thisRow.find('#deliverQuanity').siblings('.uom-select').children('button').find('.selected').data('uomID')
        };
        return thisVals;
    }

    $directProductTable.on('click', 'button.delete', function(e) {
        e.preventDefault();
        if (!$(this).hasClass('disabled')) {
            var $thisRow = $(this).parents('tr');
            var productID = $thisRow.data('id') + '_' + $thisRow.data('proIncID');

            var serial = $thisRow.find('button.edit-modal').data('selectedSids');

            if (serial) {
                for (var i in deliverSubProducts[productID]) {
                    if (serial.indexOf(deliverSubProducts[productID][i].serialID) != -1 || deliverSubProducts[productID][0].batchID != undefined ) {
                        delete deliverSubProducts[productID][i];
                        delete subProductsForCheck[$thisRow.data('id')][i];
                    }
                }
                delete deliverSubProducts[productID];
                delete deliverProducts[productID];
                checkSubProduct[productID] = undefined;
            } else {
                delete deliverProducts[productID];
                checkSubProduct[productID] = undefined;
            }

            setDirectDebitTotalCost();
            setTotalTaxForDirectDebit();
            $thisRow.remove();
        }

    });

    $('#direct-batch-save').on('click', function(e) {
        e.preventDefault();
        // validate batch / serial products before closing modal
        if (!batchSerialModalValidate(e)) {
            e.stopPropagation();
            return false;
        } else {
            $('#addDirectDebitProductsModal').modal('hide');
        }
    });


    $('.close').on('click', function() {
        $('#addDirectDebitProductsModal').modal('hide');
    });

    function batchSerialModalValidate(e) {

        var productID = $('#addDirectDebitProductsModal').data('id');
        var incid = $('#addDirectDebitProductsModal').data('icid');
        var proInID = $('#addDirectDebitProductsModal').data('proIncID');
        $('#addDirectDebitProductsModal').data('id', '');
        var $thisParentRow = getAddRowForDirectDebit(proInID);
        var thisVals = getCurrentProductDataForDirectDebit($thisParentRow);
        var $batchTable = $("#addDirectDebitProductsModal .batch-table tbody");
        var qtyTotal = 0;
        var subProducts = [];
        var oldSerials = $('#addDirectDebitProductsModal').data('selectedSIDs');
        var thisSelection = [];
        var deliveryUnitPrice;

        if ($thisParentRow.length > 1) {
            for (var row = 0; $thisParentRow.length > row; row++) {
                if (_.isEqual($('button.edit-modal', $thisParentRow[row]).data('selectedSids'), oldSerials)) {
                    var realParent = $thisParentRow[row];
                }
            }
            $thisParentRow = realParent;
        }


        $("input[name='deliverQuantity'], input[name='deliverQuantityCheck']:checked", $batchTable).each(function() {

            var $thisSubRow = $(this).parents('tr');
            var thisDeliverQuantityByBase = $(this).val();
            if ((thisDeliverQuantityByBase).trim() != "" && isNaN(parseFloat(thisDeliverQuantityByBase))) {
                p_notification(false, eb.getMessage('ERR_INVO_PROD_QUAN'));
                $(this).focus();
                return qtyTotal = false;
            }

            thisDeliverQuantityByBase = (isNaN(parseFloat(thisDeliverQuantityByBase))) ? 0 : parseFloat(thisDeliverQuantityByBase);
            // check if trasnfer qty is greater than available qty
            var thisAvailableQuantityByBase = $("input[name='availableQuantity']", $thisSubRow).val();
            thisAvailableQuantityByBase = (isNaN(parseFloat(thisAvailableQuantityByBase))) ? 0 : parseFloat(thisAvailableQuantityByBase);
            if (thisDeliverQuantityByBase > thisAvailableQuantityByBase) {
                p_notification(false, eb.getMessage('ERR_DPR_QUAN'));
                return qtyTotal = false;
                $(this).focus().select();
            }

             if(thisDeliverQuantityByBase != 0){
                deliveryUnitPrice = $("input[name='btPrice']", $thisSubRow).val();
            }

            var warranty = $thisSubRow.find('#warranty').val();
            if (isNaN(warranty)) {
                p_notification(false, eb.getMessage('ERR_INVO_WARRANTY_PERIOD_SHBE_NUM'));
                $thisSubRow.find('#warranty').focus().select();
                return qtyTotal = false;
            }

            if (thisDeliverQuantityByBase > 0) {
                var thisSubProduct = {};
                if ($(".batchCode", $thisSubRow).data('id')) {
                    thisSubProduct.batchID = $(".batchCode", $thisSubRow).data('id');
                }
                if ($(".serialID", $thisSubRow).data('id')) {
                    thisSubProduct.serialID = $(".serialID", $thisSubRow).data('id');
                }

                thisSubProduct.qtyByBase = thisDeliverQuantityByBase;
                thisSubProduct.incrementID = incid;
                thisSubProduct.warranty = $thisSubRow.find('#warranty').val();


                if ($(this).attr('disabled') != 'disabled') {
                    if(thisSubProduct.serialID!==undefined){
                        thisSelection.push(thisSubProduct.serialID);
                    }
                    subProducts.push($.extend({}, thisSubProduct));
                }
            }else{
                if(deliverSubProducts[thisVals.productIncID] !== undefined){
                    var obj = _.findWhere(deliverSubProducts[thisVals.productIncID], {batchID: $(".batchCode", $thisSubRow).data('id')});
                    if(obj!==undefined){
                        deliverSubProducts[thisVals.productIncID].splice(deliverSubProducts[thisVals.productIncID].indexOf(obj), 1);
                    }

                    if(subProductsForCheck[thisVals.productID] !== undefined){
                        var obj1 = _.findWhere(subProductsForCheck[thisVals.productID], {batchID: $(".batchCode", $thisSubRow).data('id'), incrementID: incid});
                        if(obj1!==undefined){
                            subProductsForCheck[thisVals.productID].splice(subProductsForCheck[thisVals.productID].indexOf(obj1), 1);

                        }
                    }
                }
            }

            //set total product quantity
            qtyTotal = qtyTotal + thisDeliverQuantityByBase;
            for (var old_sr in oldSerials) {
                if (deliverSubProducts[thisVals.productIncID]) {
                    var obj = _.findWhere(deliverSubProducts[thisVals.productIncID], {serialID: oldSerials[old_sr]});
                    deliverSubProducts[thisVals.productIncID].splice(deliverSubProducts[thisVals.productIncID].indexOf(obj), 1);
                }
                if (subProductsForCheck[thisVals.productID] && !(subProductsForCheck[thisVals.productID].serialID == '' || subProductsForCheck[thisVals.productID].serialID === undefined)) {
                    var obj1 = _.findWhere(subProductsForCheck[thisVals.productID], {serialID: oldSerials[old_sr], incrementID: incid });
                    subProductsForCheck[thisVals.productID].splice(subProductsForCheck[thisVals.productID].indexOf(obj1), 1);
                }
            }
        });
        $("input[name='deliverQuanity']", $thisParentRow).val(qtyTotal).change();

        // use to set unit price using batch price
        if(subProducts.length == 1 && subProducts[0].batchID != null && subProducts[0].serialID == null && deliveryUnitPrice != 0){
            $("input[name='unitPrice']", $thisParentRow).val(deliveryUnitPrice).change();
        } else if(subProducts[0].batchID != null && subProducts[0].serialID != null){
            var tempBatchIDs = [];
            var sameBatchFlag = true;
            $.each(subProducts, function(key, value){
                tempBatchIDs[key] = value.batchID;
            });
            var tmpBtID;
            $.each(tempBatchIDs, function(key, value){

                if(key == 0){
                    tmpBtID = value;
                }
                if(tmpBtID != value){
                    sameBatchFlag = false;
                }
            });
            if(sameBatchFlag){
                $("input[name='unitPrice']", $thisParentRow).val(deliveryUnitPrice).change();
            } else {
                $("input[name='unitPrice']", $thisParentRow).val(defaultSellingPriceData[thisVals.productID]).change();
            }

        } else {
            $("input[name='unitPrice']", $thisParentRow).val(defaultSellingPriceData[thisVals.productID]).change();
        }




        if (checkSubProduct[thisVals.productIncID] !== undefined) {
            if (qtyTotal != $("input[name='deliverQuanity']", $thisParentRow).val()) {
                p_notification(false, eb.getMessage('ERR_DELI_TOTAL_SUBQUAN'));
                return qtyTotal = false;
            } else {
                checkSubProduct[thisVals.productIncID] = 1;
            }
        }

        if (qtyTotal === false) {
            return false;
        }

        if (thisVals.stockUpdate == true) {
            if (parseInt(qtyTotal) > parseInt(thisVals.availableQuantity.qty)) {
                p_notification(false, eb.getMessage('ERR_PR_TOTAL_QUAN'));
                return false;
            }
        }

        for (var i in subProducts) {
            if (deliverSubProducts[thisVals.productIncID] === undefined) {
                deliverSubProducts[thisVals.productIncID] = new Array();
            }
            if (subProductsForCheck[thisVals.productID] === undefined) {
                subProductsForCheck[thisVals.productID] = new Array();
            }
             //obj is for get invoiceSubProducts object related to sub  Product Data
            //obj1 is for get subProductsForCheck object related to sub  Product Data
            if (subProducts[i].serialID == '' || subProducts[i].serialID === undefined) {
                var obj = _.findWhere(deliverSubProducts[thisVals.productIncID], {batchID: subProducts[i].batchID, incrementID: subProducts[i].incrementID});
                var obj1 = _.findWhere(subProductsForCheck[thisVals.productID], {batchID: subProducts[i].batchID, incrementID: subProducts[i].incrementID});
            } else {
                var obj = _.findWhere(deliverSubProducts[thisVals.productIncID], {serialID: subProducts[i].serialID, incrementID: subProducts[i].incrementID});
                var obj1 = _.findWhere(subProductsForCheck[thisVals.productID], {serialID: subProducts[i].serialID, incrementID: subProducts[i].incrementID});

            }
            if (obj === undefined) {
                deliverSubProducts[thisVals.productIncID].push(subProducts[i]);
            } else {
                deliverSubProducts[thisVals.productIncID][deliverSubProducts[thisVals.productIncID].indexOf(obj)].qtyByBase = subProducts[i].qtyByBase;
            }
            if (obj1 === undefined) {
                subProductsForCheck[thisVals.productID].push(subProducts[i]);
            } else {
                subProductsForCheck[thisVals.productID][subProductsForCheck[thisVals.productID].indexOf(obj1)].qtyByBase = subProducts[i].qtyByBase;
            }
        }

        SubProductsQty[thisVals.productIncID] = qtyTotal;
        var $transferQ = $("input[name='deliverQuanity']", $thisParentRow).prop('readonly', true);
        if (checkSubProduct[thisVals.productIncID] === undefined) {
            if (!$.isEmptyObject(subProducts[0]) && subProducts[0].serialID) {
                $transferQ.val(thisSelection.length).change();
            } else {
                $transferQ.val(qtyTotal).change();
            }
        }
        $('button.edit-modal', $thisParentRow).data('selectedSids', thisSelection);
        $("#unitPrice", $thisParentRow).trigger(jQuery.Event("focusout"));
        $("#itemUnitPrice", $thisParentRow).trigger(jQuery.Event("focusout"));
        return true;
    }

    $directProductTable.on('click', 'button.edit-modal', function(e) {
        var productID = $(this).parents('tr').data('id');
        var productincID = productID + "_" + $(this).parents('tr').data('proIncID');
        var rowincID = $(this).parents('tr').data('icid');
        var stockUpdate = $(this).parents('tr').data('stockupdate');
        var productCode = $(this).parents('tr').find('#item_code').val();
        var selectedSIDs = $(this).data('selectedSids');

        $("#batch_data tr:not(.hidden)").remove();
        $('#addDirectDebitProductsModal').data('icid', rowincID);
        $('#addDirectDebitProductsModal').data('proIncID', rowincID);
        addBatchProduct(productID, productCode, 0);
        addSerialProduct(productID, productCode, 0);
        addSerialBatchProduct(productID, productCode, 0);

        //check this product serail or batch
        if (!$.isEmptyObject(currentProducts[productID].serial)) {
            $('#add-product-batch .serial-auto-select').removeClass('hidden');
            $('#numberOfRow').val('');
        } else {
            $('#add-product-batch .serial-auto-select').addClass('hidden');
        }
        $("tr", '#batch_data').each(function() {
            var $thisSubRow = $(this);
            for (var i in subProductsForCheck[productID]) {
                if (subProductsForCheck[productID][i].serialID !== undefined) {
                    if ($(".serialID", $thisSubRow).data('id') == subProductsForCheck[productID][i].serialID) {
                        $("input[name='deliverQuantityCheck']", $thisSubRow).prop('checked', true);
                        $("input[name='warranty']", $thisSubRow).val(subProductsForCheck[productID][i].warranty);

                        if (selectedSIDs && selectedSIDs.indexOf(subProductsForCheck[productID][i].serialID) == -1) {
                            $("input[name='deliverQuantityCheck']", $thisSubRow).attr('disabled', true);
                        }
                    }

                } else if (subProductsForCheck[productID][i].batchID !== undefined  && (rowincID == subProductsForCheck[productID][i].incrementID)) {
                    if ($(".batchCode", $thisSubRow).data('id') == subProductsForCheck[productID][i].batchID) {
                        $(this).find("input[name='deliverQuantity']").val(subProductsForCheck[productID][i].qtyByBase).change();
                    }
                }
            }
        });

        if (!$.isEmptyObject(deliverSubProducts[productincID])) {
            $('#addDirectDebitProductsModal').data('id', $(this).parents('tr').data('id'));
        } 

        $('#addDirectDebitProductsModal').data('selectedSIDs', selectedSIDs);
        $('#addDirectDebitProductsModal').modal('show');
        $('#search_key').val('').keyup();
        $('#addDirectDebitProductsModal').unbind('hide.bs.modal');
    });

    function checkEpireData(ExpireDate){
        var today = new Date();
        var exDate = new Date(ExpireDate);

        var ctoday = Date.UTC(today.getFullYear(), today.getMonth()+1, today.getDate());
        var cexDate = Date.UTC(exDate.getFullYear(), exDate.getMonth()+1, exDate.getDate());

        var flag = true;
        if(cexDate >= ctoday){
            flag = false;
        }
        return flag;
    }

});



