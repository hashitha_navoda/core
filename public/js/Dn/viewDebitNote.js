var tmptotal;
var debitNoteID;
var supplierID;


$(document).ready(function () {

    $('#debit-note-search-select').val('Debit Note No');
    $('#debit-note-sup-search').selectpicker('hide');
    $('#debit-note-search-select').on('change', function () {
        if ($(this).val() == 'Debit Note No') {
            $('#debit-note-sup-search').selectpicker('hide');
            $('#debit-note-search').selectpicker('show');
            $('#debit-note-sup-search').val('');
            $('#debit-note-sup-search').selectpicker('render');
            $('#debit-note-list').show();
            selectedsearch = 'Invoice No';
            supplierID = '';
        } else if ($(this).val() == 'Supplier Name') {
            $('#debit-note-search').selectpicker('hide');
            $('#debit-note-sup-search').selectpicker('show');
            $('#debit-note-search').val('');
            $('#debit-note-search').selectpicker('render');
            selectedsearch = 'Supplier Name';
            debitNoteID = '';
        }
    });

    loadDropDownFromDatabase('/supplierAPI/search-suppliers-for-dropdown', "", 0, '#debit-note-sup-search');
    $('#debit-note-sup-search').selectpicker('refresh');

    $('#debit-note-sup-search').on('change', function () {
        if ($(this).val() > 0 && supplierID != $(this).val()) {
            supplierID = $(this).val();
            var getreturnsbycusturl = '/debit-note-api/retriveSupplierDebitNote';
            var requestinvbycust = eb.post(getreturnsbycusturl, {
                supplierID: supplierID,
                supplierCode: $(this).find('option:selected').text(),
            });
            requestinvbycust.done(function (retdata) {
                if (retdata == "noDebitNote") {
                    p_notification(false, eb.getMessage('ERR_VIEWDEBNOTE_NO_DEBNSUP'));
                } else {
                    $('#debit-note-list').html(retdata);
                }
            });
        }
    });

    loadDropDownFromDatabase('/debit-note-api/search-debit-note-for-dropdown', "", 0, '#debit-note-search');
    $('#debit-note-search').selectpicker('refresh');

    $('#debit-note-search').on('change', function () {
        if ($(this).val() > 0 && debitNoteID != $(this).val()) {
            debitNoteID = $(this).val();
            var searchurl = BASE_URL + '/debit-note-api/retriveDebitNoteFromDebitNoteID';
            var searchrequest = eb.post(searchurl, {
            	DebitNoteID: debitNoteID,
            	DebitNoteCode: $(this).find('option:selected').text(),
            });
            searchrequest.done(function (searchdata) {
                $('#debit-note-list').html(searchdata);
            });
        }
    });

    $('#filter-button').on('click', function () {
        if ($('#from-date').val() == '' || $('#to-date').val() == '') {
            p_notification(false, eb.getMessage('ERR_VIEWDEBNOTE_FILLDATA'));
        } else {
            var debitNoteFilter = BASE_URL + '/debit-note-api/retriveDebitNoteByDatefilter';
            var filterrequest = eb.post(debitNoteFilter, {
                'fromdate': $('#from-date').val(),
                'todate': $('#to-date').val(),
                'supplierID': supplierID
            });
            filterrequest.done(function (retdata) {
                if (retdata.msg == 'noReturns') {
                    p_notification(false, eb.getMessage('ERR_VIEWDEBNOTE_NO_DEBDATE'));
                } else {
                    $('#debit-note-list').html(retdata);
                }
            });
        }
    });

    $('#clear-search').on('click', function () {
        var request = eb.post('/debit-note/get-debit-note-list', {});
        request.done(function (retdata) {
            $('#debit-note-list').html(retdata);
            $('#debit-note-search-select').selectpicker('val', 'Debit Note No');
            $('#debit-note-sup-search').selectpicker('hide');
            $('#debit-note-search').selectpicker('show');            
            $('#debit-note-sup-search,#debit-note-search').val('').trigger('change');
            $('#debit-note-sup-search,#debit-note-search').selectpicker('render');
            $('#from-date,#to-date').val('');
            supplierID = debitNoteID = null;
        });
    });

    ///////DatePicker\\\\\\\
    var nowTemp = new Date();
    var frmdate;
    var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);

    var checkin = $('#from-date').datepicker().on('changeDate', function (ev) {
        checkin.hide();
        frmdate = ev.date;
        $('#to-date')[0].focus();
    }).data('datepicker');
    var checkout = $('#to-date').datepicker({
        onRender: function (date) {
            return date.valueOf() < frmdate ? 'disabled' : '';
        }
    }).on('changeDate', function (ev) {
        checkout.hide();
    }).data('datepicker');
/////EndOFDatePicker\\\\\\\\\

    //responsive issue
    $(window).bind('resize ready', function () {
        ($(window).width() <= 1200) ? $('#filter-button').parent('.btn-group').addClass('margin_top_low') : $('#filter-button').parent('.btn-group').removeClass('margin_top_low');
    });

//    $(window).bind('resize ready', function () {
//        ($(window).width() <= 480) ? $('#filter-button').addClass('col-xs-12') : $('#filter-button').removeClass('col-xs-12');
//    });

    $('#debit-note-list').on('click', '.dn_related_docs', function() {
        setDataToHistoryModal($(this).attr('data-dn-related-id'));
        $('#addDocHistoryModal').modal('show');
    });

    $('#debit-note-list').on('click', '.doc_attachments', function() {
        setDataToAttachmentViewModal($(this).attr('data-dn-related-id'));
        $('#viewAttachmentModal').modal('show');
    });

    $('#doc-history-table').on('click', '.document_view', function() {
        var documentId = $(this).parents('tr').attr('data-docid');
        var documentType = $(this).parents('tr').attr('data-doctype');
        $('#DocumentViewModal').modal('show');
        getPrintPreviewByDocumentTypeAndDocumentID(documentId, documentType);
    });

});

function setDataToAttachmentViewModal(documentID) {
    $('#doc-attach-table tbody tr').remove();
    $('#doc-attach-table tfoot div').remove();
    $('#doc-attach-table tbody div').remove();

    eb.ajax({
        type: 'POST',
        url: BASE_URL + '/get-document-related-attachement',
        data: {
            documentID: documentID,
            documentTypeID: 13
        },
        success: function(respond) {
            if (respond.status == true) {
                $('#doc-attach-table thead tr').removeClass('hidden');
                $.each(respond.data, function(index, value) {
                    tableBody = "<tr><td><a href="+value.link+">"+value.docName+"</a></td></tr>";
                    $('#doc-attach-table tbody').append(tableBody);
                });
            } else {
                $('#doc-attach-table thead tr').addClass('hidden');
                var noDataFooter = "<div class= ' col-lg-6 text-right'><label><b>No Data</b></label>";
                $('#doc-attach-table tbody').append(noDataFooter);
            }
        }
    });
}

function setDataToHistoryModal(debitNoteID) {
    $('#doc-history-table tbody tr').remove();
    $('#doc-history-table tfoot div').remove();
    $('#doc-history-table tbody div').remove();

    eb.ajax({
        type: 'POST',
        url: BASE_URL + '/debit-note-api/getAllRelatedDocumentDetailsByDebitNoteId',
        data: {debitNoteID: debitNoteID},
        success: function(respond) {
            if (respond.status == true) {
                $('#doc-history-table thead tr').removeClass('hidden');
                $.each(respond.data, function(index, value) {
                    if (value != null) {
                        $.each(value, function(index2, value2) {
                            tableBody = "<tr data-docId=" + value2['documentID'] + " data-doctype=" + value2['type'] + "><td class='' >" + value2['type'] + "</td><td>" + value2['code'] + "</td><td>" + value2['amount'] + "</td><td>" + value2['issuedDate'] + "</td><td class='text-center'><span class='glyphicon glyphicon-eye-open openlist link_color document_view ' style='cursor:pointer'></span></td></tr>";
                            $('#doc-history-table tbody').append(tableBody);
                        });
                    }
                });
                var footer = " <div class='col-lg-12 col-md-12 col-sm-12 text-right'><button type='button' class='btn btn-primary col-lg-12 col-md-12 col-sm-12' data-dismiss='modal' aril-hidden='true'>Back</button></div>";
                $('#doc-history-table tfoot').append(footer);
            } else {
                $('#doc-history-table thead tr').addClass('hidden');
                var noDataFooter = "<div class= ' col-lg-9 text-right'><label><b>No Data</b></label>";
                $('#doc-history-table tbody').append(noDataFooter);
            }
        }
    });
}

function getPrintPreviewByDocumentTypeAndDocumentID(documentID, documentType) {
    var $iframe = $('#related-document-view');
    $iframe.ready(function() {
        $iframe.contents().find("body div").remove();
    });
    var URL;
    if (documentType == 'PurchaseOrder') {
        URL = BASE_URL + '/po/document/' + documentID;
    } else if (documentType == 'Grn') {
        URL = BASE_URL + '/grn/document/' + documentID;
    }
    else if (documentType == 'PurchaseInvoice') {
        URL = BASE_URL + '/pi/document/' + documentID;
    }
    else if (documentType == 'PurchaseReturn') {
        URL = BASE_URL + '/pr/document/' + documentID;
    }
    else if (documentType == 'DebitNote') {
        URL = BASE_URL + '/debit-note/document/' + documentID;
    }
    else if (documentType == 'SupplierPayment') {
        URL = BASE_URL + '/supplierPayments/document/' + documentID;
    }
    else if (documentType == 'DebitNotePayment') {
        URL = BASE_URL + '/debit-note-payments/document/' + documentID;
    }

    eb.ajax({
        type: 'POST',
        url: URL,
        success: function(respond) {
            var division = "<div></div>";
            $iframe.ready(function() {
                $iframe.contents().find("body").append(division);
            });
            $iframe.ready(function() {
                $iframe.contents().find("body div").append(respond);
            });
        }
    });

}