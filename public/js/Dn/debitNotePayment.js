var supplierOldDebitBalance = 0;
var supplierNewDebitBalance = 0;
var ignoreBudgetLimitFlag = false;
debitAmounts = new Array();
var dimensionData = {};

$(document).ready(function() {

var debitNotePaymentsSearch = 'Debit Note';
var supId;
var debNoteId;
var dimensionArray = {};
var dimensionTypeID = null;

    function debitNotes() {
        this.debitNoteID = '';
        this.payAmount = '';
        this.settledAmount = '';
        this.paymentMethod = '';
    }

    $('#supplierID').selectpicker('hide');
    $('#cashGroup').hide();
    $('#checkGroup').hide();
    $('#creditGroup').hide();
    $('#loyaltyCardGroup').hide();
    $('#bankTransferGroup').hide();
    $('#giftCardGroup').hide();
    $('#lcGroup').hide();
    $('#ttGroup').hide();

    $('#debitNotePaymentsSearch').on('change', function() {
        if ($(this).val() == "Debit Note") {
            $('#debitNoteID').selectpicker('show');
            $('#supplierID').selectpicker('hide');
            debitNotePaymentsSearch = 'Debit Note';
            $('#dimension_div').addClass('hidden');
        } else {
            $('#supplierID').selectpicker('show');
            $('#debitNoteID').selectpicker('hide');
            debitNotePaymentsSearch = 'Supplier Name';
            $('#dimension_div').removeClass('hidden');
        }
    });

    loadDropDownFromDatabase('/supplierAPI/search-suppliers-for-dropdown', "", 0, '#supplierID');
    $('#supplierID').selectpicker('refresh');
    $('#supplierID').on('change', function() {
        if ($(this).val() > 0 && supplierID != $(this).val()) {
            supplierID = $(this).val();
            supplierCode = $(this).find('option:selected').text();
            supId = $(this).val();
            getDebitNoteDetails('supplier', supplierID,supplierCode);
        } else if($(this).val() > 0){
            supId = $(this).val();
        } else if($(this).val() == 0 && supplierID != null){
            supId = '';
        }
    });

    $('#dimensionView').on('click', function(e) {
        clearDimensionModal();
        var debitNotePaymentID = $('#debitNotePaymentID').val();

        if (!$.isEmptyObject(dimensionData)) {
            $.each(dimensionData[debitNotePaymentID], function(index, val) {
                dimensionArray[index] = val;                
            });
            $.each(dimensionArray, function(index, value) {
                var newTrID = 'tr_' + value.dimensionTypeId;
                var clonedRow = $($('#dimensionPreSample').clone()).attr('id', newTrID).addClass('addedDimensions');
                $("input[name='dimensionType']", clonedRow).val(value.dimensionTypeTxt);
                $("input[name='dimensionValue']", clonedRow).val(value.dimensionValueTxt);
                clonedRow.removeClass('hidden');
                clonedRow.insertBefore('#dimensionPreSample');
                clearAddedNewDimensionRow();
            });
        }
        $('#addDimensionModal').modal('show');
    });

    // dimension modal
    function clearDimensionModal()
    {
        $("#addDimensionModal #dimensionTable tbody .addedDimensions").remove();
        dimensionArray = {};
    }


    $('.dimenisonchange').on('change', function(e) {
        dimensionTypeID = this.value;
        getDimensionValueListByDimensionType(dimensionTypeID, function() {
        });
    });



    function getDimensionValueListByDimensionType(dimensionTypeID, callback) {
        switch (dimensionTypeID) {
            case "job":
                $('.dimensionVal').addClass('hidden');
                $('.dimensionProjectNo').addClass('hidden');
                $('.dimensionJobNo').removeClass('hidden');
                break;
            case "project":
                $('.dimensionVal').addClass('hidden');
                $('.dimensionProjectNo').removeClass('hidden');
                $('.dimensionJobNo').addClass('hidden');
                break;
           default:
                $('.dimensionVal').removeClass('hidden');
                $('.dimensionProjectNo').addClass('hidden');
                $('.dimensionJobNo').addClass('hidden');
                $.ajax({
                    type: 'POST',
                    url: BASE_URL + '/dimension-api/getRelatedDimensionValues',
                    data: {dimensionId: dimensionTypeID},
                    success: function(respond) {
                        //empty options
                        $('#dimensionVal').empty();
                        $('#dimensionVal').append($("<option>", {value: '', html: 'Select dimension value'}));
                        $(respond.data).each(function(index, dimValue) {
                            $.each(dimValue, function(ind, val) {
                                $('#dimensionVal').append($("<option>", {value: ind, html: val.value}));
                            });
                        });
                        callback();
                    }
                });
                break;
        }
    }

    $('#add_dimension').on('click', function(e) {
        e.preventDefault();
        //if plus button is clicked then disabled the button
        //if button is clicked more than one, then it does not continue
        if ($(this).is(':disabled')) {
            return false;
        } else {
            $(this).attr('disabled', true);
            var $thisRow = $(this).parents('tr');
            if ($('.dimenisonchange').val() == 0 || $('.dimenisonchange').val() == '' || $('.dimenisonchange').val() == null) {
                p_notification(false, eb.getMessage('ERR_DIMENSION_SELCET'));
                $(this).attr('disabled', false);
            } else {
                if (dimensionArray[dimensionTypeID]) {
                    p_notification(false, eb.getMessage('ERR_DIMENSION_ALREADY_INSERTED'));
                    $('.dimenisonchange').val('');
                    $('.dimenisonchange').selectpicker('render');
                    $('.dimenisonchange').trigger('change');
                } else {
                    addNewDimensionRow($(this).parents('tr'));
                }
                $(this).attr('disabled', false);
            }
        }
    });

    function addNewDimensionRow(thisRow) {

        var dimensionTypeId = $('.dimenisonchange', thisRow).val();
        var dimensionTypeTxt = $('.dimenisonchange', thisRow).find("option:selected").text();

        switch (dimensionTypeID) {
            case "job":
                var dimensionValueTxt = $('.dimensionJobNo', thisRow).find("option:selected").text();
                var dimensionValueId = $('.dimensionJobNo', thisRow).val();
                break;
            case "project":
                var dimensionValueTxt = $('.dimensionProjectNo', thisRow).find("option:selected").text();
                var dimensionValueId = $('.dimensionProjectNo', thisRow).val();
                break;
           default:
                var dimensionValueTxt = $('.dimensionVal', thisRow).find("option:selected").text();
                var dimensionValueId = $('.dimensionVal', thisRow).val();
                break;
        }
       
        var dimensionDuplicateCheck = false;
        if (!$.isEmptyObject(dimensionArray)) {
            $.each(dimensionArray, function(index, value) {
                if (value.dimensionTypeId == dimensionTypeId) {
                    dimensionDuplicateCheck = true;
                }
            });   
        }
       
        if (dimensionDuplicateCheck) {
            p_notification(false, eb.getMessage("ERR_DIMENSION_ALREADY_INSERTED"));
            return false;
        }

        var newTrID = 'tr_' + dimensionTypeId;
        var clonedRow = $($('#dimensionPreSample').clone()).attr('id', newTrID).addClass('addedDimensions');
        $("input[name='dimensionType']", clonedRow).val(dimensionTypeTxt);
        $("input[name='dimensionValue']", clonedRow).val(dimensionValueTxt);
        clonedRow.removeClass('hidden');
        clonedRow.insertBefore('#dimensionPreSample');
        dimensionArray[dimensionTypeId] = new dimension(dimensionTypeId, dimensionValueId,dimensionTypeTxt , dimensionValueTxt);
        clearAddedNewDimensionRow();
    }


    function dimension(dimensionTypeId, dimensionValueId, dimensionTypeTxt, dimensionValueTxt) {
        this.dimensionTypeId = dimensionTypeId;
        this.dimensionValueId = dimensionValueId;
        this.dimensionTypeTxt = dimensionTypeTxt;
        this.dimensionValueTxt = dimensionValueTxt;
    }

    function clearAddedNewDimensionRow() {
        $('.dimenisonchange').val('');
        $('.dimenisonchange').selectpicker('render');
        $('.dimenisonchange').trigger('change');
    }

    $('.addDimensions').on('click', function(e) {
        e.preventDefault();

        // validate products before closing modal
        if (!dimensionModalValidate(e)) {
            e.stopPropagation();
            return false;
        } else {
            $('#addDimensionModal').modal('hide');
        }
    });

    function dimensionModalValidate(e) {
        var debitNotePaymentID = $('#debitNotePaymentID').val();
        if ($.isEmptyObject(dimensionArray)) {
            dimensionData = {};
            return true;   
        } else {
            var dimensionSingleData = {};
            $.each(dimensionArray, function(index, val) {
                dimensionSingleData[index] = val;
            });
            dimensionData[debitNotePaymentID] = dimensionSingleData;
            return true;
        }
    }

    $("#addDimensionModal #dimensionTable").on('click', '.deleteDimensionType', function(e) {
        e.preventDefault();
        var deleteDTrID = $(this).closest('tr').attr('id');
        var deleteDID = deleteDTrID.split('tr_')[1].trim();
        bootbox.confirm('Are you sure you want to remove this dimension ?', function(result) {
            if (result == true) {
                delete dimensionArray[deleteDID];
                $('#' + deleteDTrID).remove();
            }
        });
    });

    loadDropDownFromDatabase('/job-api/searchJobsForDropdown', "", 0, '.dimensionJobNo');
    $('.dimensionJobNo').selectpicker('refresh');
    $('.dimensionJobNo').on('change', function() {
        var dimensionValue = $(this).val();
    });

    loadDropDownFromDatabase('/api/project/searchProjectsForDropdown', "", 0, '.dimensionProjectNo');
    $('.dimensionProjectNo').selectpicker('refresh');
    $('.dimensionProjectNo').on('change', function() {
        var dimensionValue = $(this).val();
    });

    loadDropDownFromDatabase('/debit-note-api/search-active-debit-note-for-dropdown', "", 0, '#debitNoteID');
    $('#debitNoteID').selectpicker('refresh');

    $('#debitNoteID').on('change', function() {
        if ($(this).val() > 0 && debitNoteID != $(this).val()) {
            debitNoteID = $(this).val();
            debitNoteCode = $(this).find('option:selected').text();
            debNoteId = $(this).val();
            getDebitNoteDetails('debitNote', debitNoteID, debitNoteCode);
        } else if($(this).val() > 0){
            debNoteId = $(this).val();
        } else if($(this).val() == 0 && debitNoteID != null){
            debNoteId = '';
        }
    });


    $(document).on('click', '.edi', function() {
        if ($(this).val() == '') {
            var dNoteID = $(this).parents('tr').attr('id');
            var leftToPay = accounting.unformat($(this).parent().parent().find('.leftToPay').text());
            supplierNewDebitBalance = supplierNewDebitBalance - accounting.unformat(leftToPay);
            if (supplierNewDebitBalance >= 0) {
                debitAmounts[dNoteID] = leftToPay;
                $(this).val(accounting.formatMoney(leftToPay));
                $('#currentCredit').val(supplierNewDebitBalance);
                var amount = accounting.unformat($('#amount').val());
                amount = amount + leftToPay;
                var discount = $('#discountAmount').val();
                if (isNaN(discount) || discount < 0) {
                    $('#discountAmount').val('');
                    discount = 0.00;
                    p_notification(false, eb.getMessage('ERR_DNDISCAMOUNT_SHOUD_POSITIVE_AND_NUMERIC'));
                }
                else if (discount > amount) {
                    $('#discountAmount').val('');
                    discount = 0.00;
                    p_notification(false, eb.getMessage('ERR_DNDISCAMOUNT_CNTBE_MO_THAN_TAMOUNT'));
                }
                amount = amount - discount;
                $('#amount').val(accounting.formatMoney(amount));
                $('#totalpayment').text(accounting.formatMoney(amount));
            } else {
                supplierNewDebitBalance = supplierNewDebitBalance + accounting.unformat(leftToPay);
                p_notification(false, eb.getMessage('ERR_SUPCDBAL_NOT_ENOUGH'));
                $(this).val('');
                debitAmounts[dNoteID] = 0;
            }
        }
    });

    $(document).on('keyup', '.edi', function() {
        var dNoteID = $(this).parents('tr').attr('id');
        var leftToPay = accounting.unformat($(this).parent().parent().find('.leftToPay').text());
        var givenAmount = $(this).val();
        var amount = 0;
        if (givenAmount == "") {
            givenAmount = 0;
        }
        if (givenAmount < 0) {
            givenAmount = 0.00;
            p_notification(false, eb.getMessage('ERR_DNPAY_CANT_BE_NEGATIVE'));
            $(this).val('');
        }
        if (givenAmount <= leftToPay) {
            supplierNewDebitBalance = supplierNewDebitBalance + parseFloat(debitAmounts[dNoteID]) - givenAmount;
            if (supplierNewDebitBalance >= 0) {
                amount = accounting.unformat($('#amount').val());
                amount = amount + parseFloat(givenAmount) - parseFloat(debitAmounts[dNoteID]);
                debitAmounts[dNoteID] = givenAmount;
            } else {
                supplierNewDebitBalance = supplierNewDebitBalance + parseFloat(givenAmount);
                amount = accounting.unformat($('#amount').val());
                amount = amount - parseFloat(debitAmounts[dNoteID]);
                p_notification(false, eb.getMessage('ERR_SUPCDBAL_NOT_ENOUGH'));
                debitAmounts[dNoteID] = 0.00;
                $(this).val('');
            }
        } else {
            supplierNewDebitBalance = supplierNewDebitBalance + parseFloat(debitAmounts[dNoteID]);
            amount = accounting.unformat($('#amount').val());
            amount = amount - parseFloat(debitAmounts[dNoteID]);
            debitAmounts[dNoteID] = 0.00;
            $(this).val('');
            if (isNaN(givenAmount)) {
                p_notification(false, eb.getMessage('ERR_DNPAYAMOUNT_NUMERIC'));
            } else {
                p_notification(false, eb.getMessage('ERR_DNPAYAMOUNT_CANT_BE_MORE_CNAMOUNT'));
            }
        }
        var discount = $('#discountAmount').val();
        if (isNaN(discount) || discount < 0) {
            $('#discountAmount').val('');
            discount = 0.00;
            p_notification(false, eb.getMessage('ERR_DNDISCAMOUNT_SHOUD_POSITIVE_AND_NUMERIC'));
        }
        else if (discount > amount) {
            $('#discountAmount').val('');
            discount = 0.00;
            p_notification(false, eb.getMessage('ERR_DNDISCAMOUNT_CNTBE_MO_THAN_TAMOUNT'));
        }
        amount = amount - discount;
        $('#currentCredit').val(supplierNewDebitBalance);
        $('#amount').val(accounting.formatMoney(amount));
        $('#totalpayment').text(accounting.formatMoney(amount));
    });

    $('#discountAmount').on('keyup', function() {
        var discount = $(this).val();
        var tamount = accounting.unformat($('#amount').val());
        if (isNaN(discount) || discount < 0) {
            $(this).val('');
            p_notification(false, eb.getMessage('ERR_DNDISCAMOUNT_SHOUD_POSITIVE_AND_NUMERIC'));
        } else {
            if (discount > tamount) {
                $(this).val('');
                p_notification(false, eb.getMessage('ERR_DNDISCAMOUNT_CNTBE_MO_THAN_TAMOUNT'));
            } else {
                $('#totalpayment').text(accounting.formatMoney(tamount - discount));
            }
        }
    });

    $(document).on('change', '.sel', function() {
        var checkCash = 0;
        var checkCheque = 0;
        var checkCredit = 0;
        var checkLoyalty = 0;
        var checkBankTransfer = 0;
        var checkGiftCard = 0;
        var checkLc = 0;
        var checkTt = 0;

        $('#cashGroup').hide();
    	$('#checkGroup').hide();
    	$('#creditGroup').hide();
    	$('#loyaltyCardGroup').hide();
    	$('#bankTransferGroup').hide();
    	$('#giftCardGroup').hide();
    	$('#lcGroup').hide();
    	$('#ttGroup').hide();

        $('#debitNotesDetails').find('tbody tr').each(function() {
            var paymethod = $(this).find('#paymentMethod').val();
            if(paymethod == 1){
            	checkCash = 1;
            } else if (paymethod == 2) {
                checkCheque = 1;
            } else if (paymethod == 3) {
                checkCredit = 1;
            } else if(paymethod == 4){
            	checkLoyalty = 1;
            } else if(paymethod == 5){
            	checkBankTransfer = 1;
            } else if(paymethod == 6){
            	checkGiftCard =1;
            } else if(paymethod == 7){
            	checkLc = 1;
            } else if(paymethod == 8){
            	checkTt = 1;
            }
        });

        if (checkCash == 1 ) {
        	 loadDropDownFromDatabase('/accounts-api/search-active-accounts-for-dropdown', "", "", '#cashGlAccountID');
            $("#cashGroup").show();
        }

        if (checkCheque == 1 ) {
        	 loadDropDownFromDatabase('/accounts-api/search-active-accounts-for-dropdown', "", "", '#chequeGlAccountID');
            $("#checkGroup").show();
        }

        if (checkCredit == 1) {
        	 loadDropDownFromDatabase('/accounts-api/search-active-accounts-for-dropdown', "", "", '#cardGlAccountID');
            $("#creditGroup").show();
        }

        if (checkLoyalty == 1) {
        	 loadDropDownFromDatabase('/accounts-api/search-active-accounts-for-dropdown', "", "", '#loyaltyCardGlAccountID');
            $("#loyaltyCardGroup").show();
        }

        if (checkBankTransfer == 1) {
        	 loadDropDownFromDatabase('/accounts-api/search-active-accounts-for-dropdown', "", "", '#bankTransferGlAccountID');
            $("#bankTransferGroup").show();
        }

        if(checkGiftCard == 1){
        	 loadDropDownFromDatabase('/accounts-api/search-active-accounts-for-dropdown', "", "", '#giftCardGlAccountID');
        	$('#giftCardGroup').show();
        }

        if(checkLc == 1){
        	 loadDropDownFromDatabase('/accounts-api/search-active-accounts-for-dropdown', "", "", '#lCGlAccountID');
        	$('#lcGroup').show();
        }

        if(checkTt == 1){
        	 loadDropDownFromDatabase('/accounts-api/search-active-accounts-for-dropdown', "", "", '#tTGlAccountID');
        	$('#ttGroup').show();
        }

    });

    $('#reset-debit-note-button').on('click', function() {
        $("#supplierID").val('').selectpicker('render');
        $("#debitNoteID").val('').selectpicker('render');
        $("#appendtable").children().remove();
        $("#currentBalance").val('');
        $("#currentCredit").val('');
        $("#discountAmount").val('');
        $("#amount").val('');
        $("#memo").val('');
        $("#totalpayment").text('0.00');
        supplierID = '';
        debitNoteID = '';
    });

    $('#debitNotePayments').on('click', function() {
        saveDebitNotePayment();
    });

    function saveDebitNotePayment()
    {
        var paymentUrl = BASE_URL + '/debit-note-payments-api/addDebitNotePayment';
        debitNotePayment = {};
        var useAccounting = $('#useAccounting').val();
        var flag = true;
        var checkCash = 0;
        var checkCheque = 0;
        var checkCredit = 0;
        var checkLoyalty = 0;
        var checkBankTransfer = 0;
        var checkGiftCard = 0;
        var checkLc = 0;
        var checkTt = 0;
        $('#debitNotesDetails').find('tbody tr').each(function() {
            var debitNoteID = this.id;
            if ($(this).find('.edi').val() != '') {
                if ($(this).find('#paymentMethod').val() != 0) {
                    debitNotePayment[debitNoteID] = new debitNotes();
                    debitNotePayment[debitNoteID].debitNoteID = debitNoteID;
                    debitNotePayment[debitNoteID].payAmount = accounting.unformat($(this).find('.payamount').text());
                    debitNotePayment[debitNoteID].settledAmount = accounting.unformat($(this).find('.edi').val());
                    debitNotePayment[debitNoteID].paymentMethod = $(this).find('#paymentMethod').val();
                    if($(this).find('#paymentMethod').val() == 1){
                        checkCash = 1;
                    } else if ($(this).find('#paymentMethod').val() == 2) {
                        checkCheque = 1;
                    } else if ($(this).find('#paymentMethod').val() == 3) {
                        checkCredit = 1;
                    } else if($(this).find('#paymentMethod').val() == 4){
                        checkLoyalty = 1;
                    } else if($(this).find('#paymentMethod').val() == 5){
                        checkBankTransfer = 1;
                    } else if($(this).find('#paymentMethod').val() == 6){
                        checkGiftCard = 1;
                    } else if($(this).find('#paymentMethod').val() == 7){
                        checkLc = 1;
                    } else if($(this).find('#paymentMethod').val() == 8){
                        checkTt = 1;
                    }
                } else {
                    p_notification(false, eb.getMessage('ERR_DN_PLS_SELECT_PAYMETHOD', $(this).find('.code').text()));
                    flag = false;
                }
            }
        });
        if(debitNotePaymentsSearch == 'Debit Note' && (debNoteId == '' || debNoteId == null)){
            p_notification(false, eb.getMessage('ERR_DEBIT_SELECT_DCODE'));
            flag = false;
        } else if(debitNotePaymentsSearch == 'Supplier Name' && (supId == '' || supId == null)){
            p_notification(false, eb.getMessage('ERR_DEBIT_SELECT_SUPP'));
            flag = false;
        } else if (flag && jQuery.isEmptyObject(debitNotePayment)) {
            p_notification(false, eb.getMessage('ERR_PLS_SET_ONE_DN_FOR_PAYMENT'));
            flag = false;
        }

        if(checkCash == 1 && useAccounting == 1 && ($('#cashGlAccountID').val() == null || $('#cashGlAccountID').val() == 0)){
            p_notification(false, eb.getMessage('ERR_PUR_PAY_PLEASE_SELECT_CASH_ACCOUNT_ID'));
            flag = false;
        }

        if (checkCheque) {
            var checNumber = $('#checquenumber').val();
            var bank = $('#bank').val();
            if(useAccounting == 1 && ($('#chequeGlAccountID').val() == null || $('#chequeGlAccountID').val() == 0)){
                p_notification(false, eb.getMessage('ERR_PUR_PAY_PLEASE_SELECT_CHEQUE_ACCOUNT_ID'));
                flag = false;
            }else if (checNumber == null || checNumber == "") {
                p_notification(false, eb.getMessage('ERR_DNPAY_CHEQUE'));
                flag = false;
            } else if (checNumber.length > 8) {
                p_notification(false, eb.getMessage('ERR_DNPAY_CHEQUE_INVALID'));
                flag = false;
            } else if (isNaN(checNumber)) {
                p_notification(false, eb.getMessage('ERR_DNPAY_CHEQUE_NUMR'));
                flag = false;
            } else if (bank == null || bank == "") {
                p_notification(false, eb.getMessage('ERR_DNPAY_BANK_NAME'));
                flag = false;
            }
        }
        if (checkCredit) {
            var reciptNumber = $('#reciptnumber').val();
            var cardNumber = $('#cardnumber').val();
            if(useAccounting == 1 && ($('#cardGlAccountID').val() == null || $('#cardGlAccountID').val() == 0)){
                p_notification(false, eb.getMessage('ERR_PUR_PAY_PLEASE_SELECT_CARD_ACCOUNT_ID'));
                flag = false;
            } else if (reciptNumber == null || reciptNumber == "") {
                p_notification(false, eb.getMessage('ERR_DNPAY_RECNO'));
                flag = false;
            } else if (reciptNumber.length > 12) {
                p_notification(false, eb.getMessage('ERR_DNPAY_RECIPTNO_VALIDITY'));
                flag = false;
            } else if (isNaN(cardNumber)) {
                p_notification(false, eb.getMessage('ERR_DNPAY_CRDTCARD_NUMR'));
                flag = false;
            }
        }

        if(checkLoyalty == 1 && useAccounting == 1 && ($('#loyaltyCardGlAccountID').val() == null || $('#loyaltyCardGlAccountID').val() == 0)){
            p_notification(false, eb.getMessage('ERR_PUR_PAY_PLEASE_SELECT_LOYALTY_CARD_ACCOUNT_ID'));
            flag = false;
        }

        if(checkBankTransfer == 1 && useAccounting == 1 && ($('#bankTransferGlAccountID').val() == null || $('#bankTransferGlAccountID').val() == 0)){
             p_notification(false, eb.getMessage('ERR_PUR_PAY_PLEASE_SELECT_BANK_TRANSFER_ACCOUNT_ID'));
            flag = false;
        }

        if(checkGiftCard == 1 && useAccounting == 1 && ($('#giftCardGlAccountID').val() == null || $('#giftCardGlAccountID').val() == 0)){
             p_notification(false, eb.getMessage('ERR_PUR_PAY_PLEASE_SELECT_GIFT_CARD_ACCOUNT_ID'));
            flag = false;
        }

        if(checkLc == 1 && useAccounting == 1 && ($('#lCGlAccountID').val() == null || $('#lCGlAccountID').val() == 0)){
            p_notification(false, eb.getMessage('ERR_PUR_PAY_PLEASE_SELECT_LC_ACCOUNT_ID'));
            flag = false;
        }

        if(checkTt == 1 && useAccounting == 1 && ($('#tTGlAccountID').val() == null || $('#tTGlAccountID').val() == 0)){
            p_notification(false, eb.getMessage('ERR_PUR_PAY_PLEASE_SELECT_TT_ACCOUNT_ID'));
            flag = false;
        }

        if (flag && $('#amount').val().replace(/,/g, '') <= 0) {
            p_notification(false, eb.getMessage('ERR_PAY_FILL_AMOUNT'));
            return false;
        }
        if (flag) {
            var methods = new Array();

            if(checkCash != 0){
                methods.push({
                    'paymentMethodID':1,
                    'paymentMethodFinanceAccountID':$('#cashGlAccountID').val(),
                })
            }

            if (checkCheque != 0) {
                methods.push({
                    'paymentMethodID':'2',
                    'paymentMethodFinanceAccountID':$('#chequeGlAccountID').val(),
                    'paymentMethodReferenceNumber' :$('#checquenumber').val(),
                    'paymentMethodBank' :$('#bank').val(),
                });
            }

            if (checkCredit != 0) {
                methods.push({
                    'paymentMethodID':'3',
                    'paymentMethodFinanceAccountID':$('#cardGlAccountID').val(),
                    'paymentMethodReferenceNumber' :$('#reciptnumber').val(),
                    'paymentMethodCardID' :$('#cardnumber').val(),
                });
            }

            if (checkLoyalty != 0) {
                methods.push({
                    'paymentMethodID':'4',
                    'paymentMethodFinanceAccountID':$('#loyaltyCardGlAccountID').val(),
                });
            }

            if (checkBankTransfer != 0) {
                methods.push({
                    'paymentMethodID':'5',
                    'paymentMethodFinanceAccountID': $('#bankTransferGlAccountID').val(),
                });
            }

            if(checkGiftCard != 0){
                methods.push({
                    'paymentMethodID':'6',
                    'paymentMethodFinanceAccountID':$('#giftCardGlAccountID').val(),
                });
            }

            if (checkLc != 0) {
                methods.push({
                    'paymentMethodID':'7',
                    'paymentMethodFinanceAccountID':$('#lCGlAccountID').val(),
                });
            }

            if (checkTt != 0) {
                methods.push({
                    'paymentMethodID':'8',
                    'paymentMethodFinanceAccountID':$('#tTGlAccountID').val(),
                });
            }

            $('.main_div').addClass('Ajaxloading');
            eb.ajax({
                type: 'post',
                url: paymentUrl,
                data: {
                    supplierID: supplierID,
                    supplierCredit: supplierNewDebitBalance,
                    debitNotePaymentsData: debitNotePayment,
                    debitNotepaymentCode: $('#debitNotePaymentID').val().replace(/\s/g, ""),
                    date: $('#currentdate').html().replace(/\s/g, ""),
                    paymentTerm: $('#paymentTerm').val(),
                    amount: $('#amount').val().replace(/,/g, ''),
                    discount: $('#discountAmount').val().replace(/,/g, ''),
                    memo: $('#memo').val(),
                    locationID: $('#locationID').val(),
                    dimensionData: dimensionData,
                    ignoreBudgetLimit: ignoreBudgetLimitFlag,
                    PaymentMethodDetails : JSON.stringify(methods)
                },
                dataType: "json",
                async: false,
                success: function(data) {
                    if (data.status == true) {
                        var fileInput = document.getElementById('documentFiles');
                        if(fileInput.files.length > 0) {
                            var form_data = false;
                            if (window.FormData) {
                                form_data = new FormData();
                            }
                            form_data.append("documentID", data.data.debitNotePaymentID);
                            form_data.append("documentTypeID", 18);
                            
                            for (var i = 0; i < fileInput.files.length; i++) {
                                form_data.append("files[]", fileInput.files[i]);
                            }

                            eb.ajax({
                                url: BASE_URL + '/store-files',
                                type: 'POST',
                                processData: false,
                                contentType: false,
                                data: form_data,
                                success: function(res) {
                                }
                            });
                        }

                        if (checkCash != 0 || checkCredit != 0 || checkCheque != 0 || checkLoyalty != 0 || checkBankTransfer != 0 || checkGiftCard != 0 || checkLc != 0 || checkTt != 0) {
                            var addpayurl = BASE_URL + '/debit-note-payments-api/addDebitNotePaymentMethodNumbers';
                            var param = {
                                debitNotePaymentID: data.data.debitNotePaymentID,
                                debitNotePaymentMethodDetails: JSON.stringify(methods)
                            };
                            var addcreditcardandchecque = eb.post(addpayurl, param);
                            addcreditcardandchecque.done(function(value) {
                                p_notification(true, eb.getMessage('SUCC_DNPAY_ADDPAY', data.data.debitNotePaymentCode));
                                documentPreview(BASE_URL + '/debit-note-payments/viewDebitNotePaymentReceipt/' + data.data.debitNotePaymentID, 'documentpreview', "/debit-note-payments-api/send-email", function($preview) {
                                    $preview.on('hidden.bs.modal', function() {
                                        if (!$("#preview:visible").length) {
                                            window.location.reload();
                                        }
                                    });
                                });
                            });
                        } else {
                            p_notification(true, eb.getMessage('SUCC_DNPAY_ADDPAY', data.data.debitNotePaymentCode));
                            documentPreview(BASE_URL + '/debit-note-payments/viewDebitNotePaymentReceipt/' + data.data.debitNotePaymentID, 'documentpreview', "/debit-note-payments-api/send-email", function($preview) {
                                $preview.on('hidden.bs.modal', function() {
                                    if (!$("#preview:visible").length) {
                                        window.location.reload();
                                    }
                                });
                            });
                        }
                    } else if (data['state'] == false) {
                        p_notification(false, data['msg']);
                        $('.main_div').removeClass('Ajaxloading');
                    } else if (data.status == false) {
                        if (data.data == "NotifyBudgetLimit") {
                            bootbox.confirm(data.msg+' ,Are you sure you want to save ?', function(result) {
                                if (result == true) {
                                    ignoreBudgetLimitFlag = true;
                                    saveDebitNotePayment();
                                } else {
                                    setTimeout(function(){ 
                                        location.reload();
                                    }, 3000);
                                }
                            });
                        } else {
                            p_notification(false, data.msg);
                        }
                    }
                },
            });
        }
    }



    function getDebitNoteDetails(type, id, code) {
        $("#amount").val('');
        $("#totalpayment").text('0.00');
        var getDetailsUrl;
        var data;
        if (type == 'supplier') {
            getDetailsUrl = BASE_URL + '/debit-note-payments-api/getSupplierRelatedDebitNotes';
            data = {
                supplierID: id,
                supplierCode: code,
            };
        } else if (type == 'debitNote') {
            getDetailsUrl = BASE_URL + '/debit-note-payments-api/getDebitNoteDetailsByDebitNoteID';
            data = {
                debitNoteID: id,
                debitNoteCode: code,
            };
        }
        eb.ajax({
            type: 'post',
            url: getDetailsUrl,
            data: data,
            success: function(value) {
                if (value.status) {
                    if (type == 'supplier') {
                        if (value.data.eligibleDebitNoteCount == 0) {
                            p_notification(false, "Debit Notes of this supplier are fully paid");
                        }
                    }
                    $('#appendtable').html(value.html);
                    $('#currentBalance').val(accounting.formatMoney(value.data.supplier.supplierOutstandingBalance));
                    $('#currentCredit').val(accounting.formatMoney(value.data.supplier.supplierCreditBalance));
                    supplierID = value.data.supplier.supplierID;
                    supplierNewDebitBalance = supplierOldDebitBalance = value.data.supplier.supplierCreditBalance;
                } else {
                    p_notification(false, value.msg);
                }
            },
            async: false
        });
    }

}
);