var ignoreBudgetLimitFlag = false;
$(document).ready(function() {
    var supplierID = '';
    var deleteDebitNotePaymentID = '';
    var debitNotePaymentID = '';
    
    $('#dn-pay-sup-search').selectpicker('hide');
    $('#dn-pay-search-select').on('change', function() {
        if ($(this).val() == 'Payments No') {
            $('#dn-pay-search').selectpicker('show');
            $('#dn-pay-sup-search').selectpicker('hide');
        } else {
            $('#dn-pay-sup-search').removeClass('hidden')
            $('#dn-pay-sup-search').selectpicker('show');
            $('#dn-pay-search').selectpicker('hide');
        }
        $('#dn-pay-search').selectpicker('refresh');
        $('#dn-pay-sup-search').selectpicker('refresh');
    });

    loadDropDownFromDatabase('/supplierAPI/search-suppliers-for-dropdown', "", 0, '#dn-pay-sup-search');
    $('#dn-pay-sup-search').selectpicker('refresh');

    $('#dn-pay-sup-search').on('change', function() {
        if ($(this).val() > 0 && supplierID != $(this).val()) {
            supplierID = $(this).val();
            supplierCode = $(this).find('option:selected').text();
            var getSupDNPUrl = '/debit-note-payments-api/retriveSupplierDebitNotePayments';
            var requestSupDNP = eb.post(getSupDNPUrl, {
                supplierID: supplierID,
                supplierCode: supplierCode
            });
            requestSupDNP.done(function(retdata) {
                if (retdata == "noDebitNotePayments") {
                    p_notification(false, eb.getMessage('ERR_VIEWDEBNOTEPAY_NO_DNPAYSUP'));
                } else {
                    $('#debit-note-payment-list-view').html(retdata);
                }
            });
        }
    });

    loadDropDownFromDatabase('/debit-note-payments-api/search-debit-note-payment-for-dropdown', "", 0, '#dn-pay-search');
    $('#dn-pay-search').selectpicker('refresh');

    $('#dn-pay-search').on('change', function() {
        if ($(this).val() > 0 && debitNotePaymentID != $(this).val()) {
            debitNotePaymentID = $(this).val();
            debitNotePaymentCode = $(this).find('option:selected').text();
            var getDNPByDNIDUrl = '/debit-note-payments-api/retriveDebitNotePaymentByDebitNotePaymentID';
            var requestDNPIDDNP = eb.post(getDNPByDNIDUrl, {
                debitNotePaymentID: debitNotePaymentID,
                debitNotePaymentCode: debitNotePaymentCode
            });
            requestDNPIDDNP.done(function(retdata) {
                if (retdata == "notFindDebitNotePayment") {
                    p_notification(false, eb.getMessage('ERR_VIEWDEBNOTEPAY_NO_DNPAYNOTFOUND'));
                } else {
                    $('#debit-note-payment-list-view').html(retdata);
                }
            });
        }
    });

    $('#clear-search').on('click', function() {
        var request = eb.post('/debit-note-payments/get-debit-note-payment-list', {});
        request.done(function(retdata) {
            $('#debit-note-payment-list-view').html(retdata);            
            $('#dn-pay-search-select').selectpicker('val', 'Payments No');
            $('#dn-pay-search').selectpicker('show');
            $('#dn-pay-sup-search').selectpicker('hide');            
            $('#dn-pay-search,#dn-pay-sup-search').val('').trigger('change');
            $('#dn-pay-search,#dn-pay-sup-search').selectpicker('render');
            $('#from-date,#to-date').val('');
            supplierID = debitNotePaymentID = null;
        });
    });

    $('#filter-button').on('click', function() {
        if ($('#from-date').val() == '' || $('#to-date').val() == '') {
            p_notification(false, eb.getMessage('ERR_VIEWDEBNOTEPAY_FILLDATA'));
        } else {
            var DNPFilterUrl = BASE_URL + '/debit-note-payments-api/retriveDebitNotePaymentsByDatefilter';
            var filterrequest = eb.post(DNPFilterUrl, {
                'fromdate': $('#from-date').val(),
                'todate': $('#to-date').val(),
                'supplierID': supplierID
            });
            filterrequest.done(function(retdata) {
                if (retdata == "notFindDebitNotePayment") {
                    p_notification(false, eb.getMessage('ERR_VIEWDEBNOTEPAY_NO_DNPAYFOUND_BY_DATE'));
                } else {
                    $('#debit-note-payment-list').html(retdata);
                }
            });
        }
    });

    $(document).on('click', ' .del', function() {
        deleteDebitNotePaymentID = $(this).attr('id');

    });

    $(document).on('click', ' .doc_attachments', function() {
        setDataToAttachmentViewModal($(this).attr('data-dnpay-related-id'));
        $('#viewAttachmentModal').modal('show');
    });

    $('#delete-dn-payment-button').off('click').on('click', function(e) {
        e.preventDefault();
        deleteDebitNotePayment();
    });

    $('#doc-history-table').on('click', '.document_view', function() {
        var documentId = $(this).parents('tr').attr('data-docid');
        var documentType = $(this).parents('tr').attr('data-doctype');
        $('#DocumentViewModal').modal('show');
        getPrintPreviewByDocumentTypeAndDocumentID(documentId, documentType);
    });

    $('#debit-note-payment-list-view').on('click', '.dn_payment_related_docs', function() {
        setDataToHistoryModal($(this).attr('data-dn-payment-related-id'));
        $('#addDocHistoryModal').modal('show');
    });

    function setDataToHistoryModal(debitNotePaymentID) {
        $('#doc-history-table tbody tr').remove();
        $('#doc-history-table tfoot div').remove();
        $('#doc-history-table tbody div').remove();

        eb.ajax({
            type: 'POST',
            url: BASE_URL + '/debit-note-payments-api/getAllRelatedDocumentDetailsByDebitNotePaymentId',
            data: {debitNotePaymentID: debitNotePaymentID},
            success: function(respond) {
                if (respond.status == true) {
                    $('#doc-history-table thead tr').removeClass('hidden');
                    $.each(respond.data, function(index, value) {
                        if (value != null) {
                            $.each(value, function(index2, value2) {
                                tableBody = "<tr data-docId=" + value2['documentID'] + " data-doctype=" + value2['type'] + "><td class='' >" + value2['type'] + "</td><td>" + value2['code'] + "</td><td>" + value2['amount'] + "</td><td>" + value2['issuedDate'] + "</td><td class='text-center'><span class='glyphicon glyphicon-eye-open openlist link_color document_view ' style='cursor:pointer'></span></td></tr>";
                                $('#doc-history-table tbody').append(tableBody);
                            });
                        }
                    });
                    var footer = " <div class='col-lg-12 col-md-12 col-sm-12 text-right'><button type='button' class='btn btn-primary col-lg-12 col-md-12 col-sm-12' data-dismiss='modal' aril-hidden='true'>Back</button></div>";
                    $('#doc-history-table tfoot').append(footer);
                } else {
                    $('#doc-history-table thead tr').addClass('hidden');
                    var noDataFooter = "<div class= ' col-lg-9 text-right'><label><b>No Data</b></label>";
                    $('#doc-history-table tbody').append(noDataFooter);
                }
            }
        });
    }

    function getPrintPreviewByDocumentTypeAndDocumentID(documentID, documentType) {
        var $iframe = $('#related-document-view');
        $iframe.ready(function() {
            $iframe.contents().find("body div").remove();
        });
        var URL;
        if (documentType == 'PurchaseOrder') {
            URL = BASE_URL + '/po/document/' + documentID;
        } else if (documentType == 'Grn') {
            URL = BASE_URL + '/grn/document/' + documentID;
        }
        else if (documentType == 'PurchaseInvoice') {
            URL = BASE_URL + '/pi/document/' + documentID;
        }
        else if (documentType == 'PurchaseReturn') {
            URL = BASE_URL + '/pr/document/' + documentID;
        }
        else if (documentType == 'DebitNote') {
            URL = BASE_URL + '/debit-note/document/' + documentID;
        }
        else if (documentType == 'SupplierPayment') {
            URL = BASE_URL + '/supplierPayments/document/' + documentID;
        }
        else if (documentType == 'DebitNotePayment') {
            URL = BASE_URL + '/debit-note-payments/document/' + documentID;
        }

        eb.ajax({
            type: 'POST',
            url: URL,
            success: function(respond) {
                var division = "<div></div>";
                $iframe.ready(function() {
                    $iframe.contents().find("body").append(division);
                });
                $iframe.ready(function() {
                    $iframe.contents().find("body div").append(respond);
                });
            }
        });

    }

    function deleteDebitNotePayment()
    {
        var valid = new Array(
                $('#username').val(),
                $('#password').val()
                );
        if (validate_data(valid)) {
            var username = $("#username").val();
            var password = $('#password').val();
            var comment = $('#comment').val();
//                $('.main_div').addClass('Ajaxloading');
            var getpayurl = BASE_URL + '/debit-note-payments-api/deleteDebitNotePayment';
            var getpayrequest = eb.post(getpayurl, {
                debitNotePaymentID: deleteDebitNotePaymentID,
                username: username,
                password: password,
                debitNotePaymentCancelMessage: comment,
                ignoreBudgetLimit: ignoreBudgetLimitFlag,
            });
            getpayrequest.done(function(data) {
                if (data.status) {
                    $('#delete_dn_payment').modal('hide');
                    p_notification(true, eb.getMessage('ERR_VIEWDEBNOTEPAY_PAY_DELETE'));
                    setTimeout(function() {
                        window.location.reload();
                    }, 1000);
                } else if (data.data == "NotifyBudgetLimit") {
                    bootbox.confirm(data.msg+' ,Are you sure you want to save this invoice ?', function(result) {
                        if (result == true) {
                            ignoreBudgetLimitFlag = true;
                            deleteDebitNotePayment();
                        } else {
                            setTimeout(function(){ 
                                location.reload();
                            }, 3000);
                        }
                    });
                } else if (data.data == 'admin') {
                    p_notification(false, eb.getMessage('ERR_VIEWDEBNOTEPAY_USER_PRIVILEGE'));
                } else if (data.data == 'user' || data.data == 'pass') {
                    p_notification(false, eb.getMessage('ERR_VIEWDEBNOTEPAY_INVALID_USRNAME_PWD'));
                } else if (data.data == false) {
                    p_notification(false, eb.getMessage('ERR_VIEWDEBNOTEPAY_USER_AUTH'));
                } 
            });
        }
    }


    function setDataToAttachmentViewModal(documentID) {
        $('#doc-attach-table tbody tr').remove();
        $('#doc-attach-table tfoot div').remove();
        $('#doc-attach-table tbody div').remove();

        eb.ajax({
            type: 'POST',
            url: BASE_URL + '/get-document-related-attachement',
            data: {
                documentID: documentID,
                documentTypeID: 18
            },
            success: function(respond) {
                if (respond.status == true) {
                    $('#doc-attach-table thead tr').removeClass('hidden');
                    $.each(respond.data, function(index, value) {
                        tableBody = "<tr><td><a href="+value.link+">"+value.docName+"</a></td></tr>";
                        $('#doc-attach-table tbody').append(tableBody);
                    });
                } else {
                    $('#doc-attach-table thead tr').addClass('hidden');
                    var noDataFooter = "<div class= ' col-lg-6 text-right'><label><b>No Data</b></label>";
                    $('#doc-attach-table tbody').append(noDataFooter);
                }
            }
        });
    }

    function validate_data(valid) {
        var user = valid[0];
        var pass = valid[1];
        if (user == null || user == "") {
            p_notification(false, eb.getMessage('ERR_VIEWCRNOTEPAY_UNAME'));
        } else if (pass == null || pass == "") {
            p_notification(false, eb.getMessage('ERR_VIEWCRNOTEPAY_PWD'));
        } else {
            return true;
        }
    }


///////DatePicker\\\\\\\
    var nowTemp = new Date();
    var frmdate;
    var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);

    var checkin = $('#from-date').datepicker().on('changeDate', function(ev) {
        checkin.hide();
        frmdate = ev.date;
        $('#to-date')[0].focus();
    }).data('datepicker');
    var checkout = $('#to-date').datepicker({
        onRender: function(date) {
            return date.valueOf() < frmdate ? 'disabled' : '';
        }
    }).on('changeDate', function(ev) {
        checkout.hide();
    }).data('datepicker');
/////EndOFDatePicker\\\\\\\\\
});