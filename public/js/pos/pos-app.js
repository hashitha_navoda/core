/* global angular, currency */

var PRODUCTS_DETAILS = [];

var app = angular.module('PosApp', ['LocalStorageModule', 'ngSanitize']);

app.controller('PosController', ['$scope', '$http', '$sce', 'localStorageService', function($scope, $http, $sce, localStorageService) {

        var products = [];
        var local = localStorageService.isSupported ? localStorageService : undefined;
        if (local) {
            $scope.online = true;
            var unSaved = {};
        }

        var Customer = {id: 0};

        $scope.currency = currency;
        $scope.previewUrl = BASE_URL + "/invoice/document/template/pos" + postpl;
        $scope.previewInvOnlyUrl = BASE_URL + "/invoice/document/template/pos-inv-only" + invtpl;
        $scope.previewPayOnlyUrl = BASE_URL + "/invoice/document/template/pos-pay-only" + paytpl;
        $scope.previewPOSCNUrl = BASE_URL + "/credit-note/document/template/posCN" + cntpl;
        $scope.login = isLogin;
        $scope.loginPanel = !isLogin;
        $scope.closingBalance = closeBalance;
        $scope.holds = new Array();
        $scope.order = new Array();
        $scope.orderNumber = 0;
        $scope.batchData = new Array();
        $scope.order[0] = {
            inv: {},
            pay: {},
            customer: {id: 0},
            sub_product: {},
            loyalty: {},
            gift_card: new Array(),
            promotion: {},
            priceListSelect: {},
            orderNumber: $scope.orderNumber
        };
        $scope.inv = $scope.order[0].inv;
        $scope.pay = $scope.order[0].pay;
        $scope.customer = $scope.order[0].customer;
        $scope.sub_product = $scope.order[0].sub_product;
        $scope.promotion = $scope.order[0].promotion;
        $scope.priceListSelectID = null;
        $scope.inv.totalTax = 0;
        $scope.priceListItems = {};
        $scope.inv.discountSelect = 'pre';
        $scope.inv.discount = '0.00';
        $scope.inv.totalBeforeDiscount = 0;
        $scope.inv.realDiscount = null;
        $scope.inv.comment = null;
        $scope.selectedProduct = null;
        $scope.creditNoteFlag = false;
        $scope.creditNotePaymentFlag = false;
        $scope.docType = 'pos';
        $scope.creditNoteItemQty = 0;
        $scope.payedCreditNoteID = null;
        $scope.creditNoteID = null;
        $scope.CNTemplate = false;
        $scope.togglePosModal = true;
        $scope.cNDiscDisable = false;
        $scope.finDiscount = 0;
        $scope.CNPromotion = [];
        $scope.placeholder = 'Search Items';
        $scope.toggleTypeID = 'item_search_bar';
        $scope.showPromo = false;
        $scope.selectInvoFromSearch = true;
        $scope.customers = _.map(CUSTOMERS_NAMES, function(name, id){ return {id: id, name: name}});
        $scope.loyalty = {card_no : null};
        $scope.newCust = {};
        $scope.dis_on_eligible = dis_on_eligible;
        $scope.inclusive_tax_on_eligible = inclusive_tax_on_eligible;
        $scope.override_price = override_price;
        $scope.alow_price_list = alow_price_list;
        $scope.alow_edit_price_list_price = alow_edit_price_list_price;
        $scope.override_discount = override_discount;
        $scope.canOverride = false;
        $scope.getAuthToPricelist = false;
        $scope.canUsePriceList = false;
        $scope.canEditPrice = false;
        $scope.overrideTempSetting = {temp: true};
        $scope.priceListTempSetting = {temp: true};
        $scope.priceListEditPriceTempSetting = {temp: true};
        $scope.skipPrinting = false;
        $scope.auth = { username: "", pass: "", temp: true};
        $scope.liveUpdate = false;
        $scope.serial = new Array();
        $scope.invoiceList = new Array();
        $scope.priceList = {};
        var posPrintoutImageConversionStatus = false;

        if ($scope.inclusive_tax_on_eligible) {
            $('#priceLabel').addClass('hidden');
            $('#inclusiveTaxPriceLabel').removeClass('hidden');
            $('#totalLabel').addClass('hidden');
            $('#inclusiveTaxTotalLabel').removeClass('hidden');
        } else {
            $('#priceLabel').removeClass('hidden');
            $('#inclusiveTaxPriceLabel').addClass('hidden');
            $('#totalLabel').removeClass('hidden');
            $('#inclusiveTaxTotalLabel').addClass('hidden');
        }

        function wsConnect() {
            var protocol = location.protocol == 'http:' ? 'ws://' : 'wss://';
            var chanel = BASE_URL.replace(/(http.*:\/\/)([A-z0-9]*)(.*)/, "$2");
            var domain = BASE_URL.replace(/https?:\/\//i, "");
            
            var conn = new ab.Session(protocol + domain + ":" + WSPORT,
                function() {
                    $scope.liveUpdate = true
                    conn.subscribe(chanel, function(topic, data) {
                        local.set('ts_', data.ts_);
                        $scope.updateInventory(data.products);
                    });
                },
                function() {
                    $scope.liveUpdate = false
                    wsReconnect();
                    console.warn('WebSocket connection closed');
                },
                {'skipSubprotocolCheck': true}
            );
        }
        wsConnect();

        var wsConnectionTimer = null;
        function wsReconnect() {
            if ($scope.liveUpdate) {
                clearInterval(wsConnectionTimer);
            }else {
                wsConnect();
                return;
            }
            if (!wsConnectionTimer) {
                wsConnectionTimer = setInterval(wsReconnect, 2000);
            }
        }

        getProducts = function() {
            var ts_ = local.get('ts_');
            PRODUCTS_DETAILS = local.get('products_details');
            const params = PRODUCTS_DETAILS == null ? {} : {'timestamp': ts_};

            $http.post('/pos-api/get-products-details', params)
                .success(function(res){
                    if (res.status) {
                        if (PRODUCTS_DETAILS && ts_) {
                            for(var pid in res.data.products_details.products){
                                PRODUCTS_DETAILS[pid] = res.data.products_details.products[pid];
                            }
                        }else {
                            PRODUCTS_DETAILS = res.data.products_details.products
                        }
                        posPrintoutImageConversionStatus = res.data.products_details.posPrintoutImageConversionStatus;
                        prepareProducts();
                        local.set('products_details', PRODUCTS_DETAILS);
                    }
                });
        }
        getProducts();

        $scope.updateInventory = function(products) {
            for(var pid in products){
                if (locID == products[pid].locID) {
                    var idx = $scope.products.indexOf(_.findWhere($scope.products, {pID: pid}));
                    // if item has deleted
                    if (products[pid].action && products[pid].action == 'deleted') {
                        delete $scope.products[idx];
                        delete PRODUCTS_DETAILS[pid];
                        continue;
                    }
                    // If new item is created
                    if(idx == -1){
                        $scope.products.push({
                            pID: products[pid].pID,
                            pC: products[pid].pC,
                            pN: products[pid].pN,
                            LPQ: products[pid].LPQ,
                            uom: products[pid].uom, 
                            barcodes: products[pid].barcodes 
                        });
                    }else {
                        // If existinf item has been updated
                        $scope.products[idx].pC = products[pid].pC;
                        $scope.products[idx].pN = products[pid].pN;
                        $scope.products[idx].LPQ = products[pid].LPQ;
                        $scope.products[idx].uom = products[pid].uom;
                        $scope.products[idx].barcodes = products[pid].barcodes;
                    }
                    PRODUCTS_DETAILS[pid] = products[pid];
                }
            }
            local.set('products_details', PRODUCTS_DETAILS);
        };

        function prepareProducts() {
            // $scope.subItemBatchQty = '0';
            angular.forEach(PRODUCTS_DETAILS, function(value, key) {
                var pro = {
                    pID: value.pID,
                    pC: value.pC,
                    pN: value.pN,
                    LPQ: value.LPQ,
                    uom: value.uom,
                }
                this.push(pro);
            }, products);
            for(var i in PRODUCTS_DETAILS){
                if(Object.keys(PRODUCTS_DETAILS[i].serial).length > 0){
                    for(var si in PRODUCTS_DETAILS[i].serial){
                        $scope.serial.push({id:i, serial: PRODUCTS_DETAILS[i].serial[si].PSC});
                    }
                }
            }
        }

        $scope.inv.cart = new Array();
        $scope.ready = true;
        $scope.products = products;
        $scope.sys = {error: false};
        if (requireRef) {
            $scope.sys = {
                error: true,
            };
            $scope.loginPanel = false;
        }
        $scope.gift_cards_list = GIFTCARDLIST;
        
        $scope.serial = new Array();

        $scope.invoiceList = new Array();

        $scope.adminAuthenticate = function(){

            if(!$scope.auth.username){
                p_notification(false, "Please enter a valid username.");
                return false;
            }
            if(!$scope.auth.pass){
                p_notification(false, "Please enter password.");
                return false;
            }

            $http.post('/pos-api/get-user-permission', $scope.auth)
                .success(function(res){
                    if ($scope.getAuthToPricelist) {

                        if(res.status){
                            $scope.canUsePriceList = true;
                            $scope.canEditPrice = true;
                        }else{    
                            $scope.canUsePriceList = false;
                            $scope.canEditPrice = false;
                        }
                        $scope.priceListTempSetting.temp = $scope.auth.temp;

                        $scope.auth = { username: "", pass: "", temp: true};
                        p_notification(res.status, res.msg);
                        closeAuthorisationModal();
                    } else {
                        if(res.status){
                            $scope.canOverride = true;
                        }else{    
                            $scope.canOverride = false;
                        }
                        $scope.overrideTempSetting.temp = $scope.auth.temp;

                        $scope.auth = { username: "", pass: "", temp: true};
                        p_notification(res.status, res.msg);
                        closeAuthorisationModal();
                    }
                });
        }

        // default price override setting
        $scope.changePriceSetting = function(){
            data = {
                setting: 'OVERRIDE_ITEM_PRICE',
                value: {override_item_price: ($scope.override_price == 'allow' ? true : false) }
            };
            if(!$scope.overrideTempSetting.temp){
                $http.post('/pos-api/change-settings', data)
                    .success(function(res){
                        p_notification(res.status, res.msg);
                    });
            }
        }

        // price list asign setting
        $scope.changePriceListSetting = function(){

            if ($scope.alow_price_list == 'deny') {
                $scope.removePriceList(); 
            }

            data = {
                setting: 'ALLOW_PRICE_LIST',
                value: {allow_price_list_pos: ($scope.alow_price_list == 'allow' ? true : false), allow_edit_price_list_price: ($scope.alow_edit_price_list_price == 'allow' ? true : false)}
            };
            if(!$scope.priceListTempSetting.temp){
                $http.post('/pos-api/change-settings', data)
                    .success(function(res){
                        p_notification(res.status, res.msg);
                    });
            }
        } 

        $scope.getAuthForPriceList = function(){
            $scope.getAuthToPricelist = true;
        }

        $scope.getAuthForOverrides = function(){
            $scope.getAuthToPricelist = false;
        }


        // default discount override setting
        $scope.changeDiscountSetting = function(){
            data = {
                setting: 'OVERRIDE_ITEM_DISCOUNT',
                value: {override_item_discount: ($scope.override_discount == 'allow' ? true : false) }
            };

            if(!$scope.overrideTempSetting.temp){
                $http.post('/pos-api/change-settings', data)
                    .success(function(res){
                        p_notification(res.status, res.msg);
                    });
            }
        }

        // change invoice discount apllication method
        $scope.changeInvoiceDiscountSetting = function(){
            data = {
                setting: 'INV_DISCOUNT',
                value: {discount_only_eligible_items: $scope.dis_on_eligible}
            };

            $http.post('/pos-api/change-settings', data)
                .success(function(res){
                    p_notification(res.status, res.msg);
                });
        }

        // change invoice tax apllication method
        $scope.changeInvoiceTaxSetting = function(){
            data = {
                setting: 'INCLUSIVE_TAX',
                value: {inclusive_tax: $scope.inclusive_tax_on_eligible}
            };

            $http.post('/pos-api/change-settings', data)
                .success(function(res){
                    p_notification(res.status, res.msg);
                    if ($scope.inclusive_tax_on_eligible) {
                        $('#priceLabel').addClass('hidden');
                        $('#inclusiveTaxPriceLabel').removeClass('hidden');
                        $('#totalLabel').addClass('hidden');
                        $('#inclusiveTaxTotalLabel').removeClass('hidden');
                    } else {
                        $('#priceLabel').removeClass('hidden');
                        $('#inclusiveTaxPriceLabel').addClass('hidden');
                        $('#totalLabel').removeClass('hidden');
                        $('#inclusiveTaxTotalLabel').addClass('hidden');
                    }
                });
        }

        // change template
        $scope.changeTemplate = function(type, tplId){
            switch(type){
                case "8" : // POS Printout
                    $scope.previewUrl = BASE_URL + "/invoice/document/template/pos/" + tplId;
                    break;
                case "24" : // POS Credit Note
                    $scope.previewPOSCNUrl = BASE_URL + "/credit-note/document/template/posCN/"; + tplId;
                    break;
                case "25" : // POS (invoice only)
                    $scope.previewInvOnlyUrl = BASE_URL + "/invoice/document/template/pos-inv-only/" + tplId;
                    break;
                case "26" : // POS (pay only)
                    $scope.previewPayOnlyUrl = BASE_URL + "/invoice/document/template/pos-pay-only/" + tplId;
                    break;
            }
            $data = {
                documentTypeID: type,
                templateID: tplId
            };
            $http.post('/pos-api/change-location-template', $data)
                .success(function(res){
                    p_notification(res.status, res.msg);
                });
        }

        // get custoemr by id
        $scope.getCustomer = function(id, withLoyaltyCard){
            var url = BASE_URL + "/customerAPI/getcustomerByID";
            var data = {customerID: id};
            $http.post(url, data)
                .success(function(res){
                    if(!res.status){
                        $scope.customer.id = 0;
                        return;
                    }
                    var cust = res.data.customerData;
                    $scope.customer.id = cust.customerID;
                    $scope.customer.name = (cust.customerTitle ? cust.customerTitle : '') + " " + cust.customerName;
                    $scope.customer.credit = cust.customerCurrentCredit;
                    $scope.customer.balance = cust.customerCurrentBalance;
                    $scope.customer.customerLoyaltyCode = (cust.customerLoyaltyCode) ? cust.customerLoyaltyCode : false;

                    if(!cust.customerLoyaltyCode){
                        $scope.removeLoyaltyCard();
                    }else if(withLoyaltyCard && cust.customerLoyaltyCode){
                        $scope.loyalty.card_no = cust.customerLoyaltyCode;
                        // false - do not get customer
                        $scope.applyLoyaltyCard(false);
                    }

                });
        }

        // create new customer and assign as invoice customer
        $scope.createCustomer = function(){
            if($scope.validateNewCustomer()){
            $http.post(BASE_URL + "/customerAPI/add", $scope.newCust)
                .success(function(res) {
                    if(res.status){
                        var customerName = $scope.newCust.customerCode + "-" + $scope.newCust.customerName;
                        $scope.customers.push({id:res.data.customerID, name: customerName});
                        $scope.getCustomer(res.data.customerID, true);
                        p_notification(true, res.msg);
                        closeCustomerModal();
                    }else{
                        p_notification(false, res.msg);
                    }
                });

            }
        }

        // Return current customer's ID
        $scope.getCustomerID = function(){
            return $scope.customer.id;
        }

        // Return current customer's credit amount
        $scope.getCustomerCredit = function(){
            return $scope.customer.credit;
        }

        // Return current customer's balance amount
        $scope.getCustomerBalance = function(){
            return $scope.customer.balance;
        }

        // Return current customer's name
        $scope.getCustomerName = function(){
            return $scope.customer.name;
        }

        // Remove current customer's name
        $scope.removeCustomer = function(){
            $scope.customer.id = 0;
            $scope.customer.name = "";
            $scope.customer.credit = "";
            $scope.customer.balance = "";
            $scope.removeLoyaltyCard();
        }


        $scope.prepareCustomerForm = function(){
            $scope.newCust = {};
            $scope.newCust.customerProfile = {unsavedProfile: {customerProfileName: "Default"}};
            $scope.newCust.customerPrimaryProfileID = 'unsavedProfile';
            $scope.newCust.customerPaymentTerm = 1;
            $scope.newCust.customerReceviableAccountID = CUSTOMER_ACCOUNTS.customerReceviable;
            $scope.newCust.customerSalesAccountID = CUSTOMER_ACCOUNTS.customerSales;
            $scope.newCust.customerSalesDiscountAccountID = CUSTOMER_ACCOUNTS.customerSalesDiscount;
            $scope.newCust.customerAdvancePaymentAccountID = CUSTOMER_ACCOUNTS.customerAdvancePayment;

            $http.get(BASE_URL + "/customerAPI/get-next-customer-code")
                .success(function(res){
                    $scope.newCust.customerCode = res.data.refNo;
                })
        }

        $scope.getLoyaltyCodes = function(){
            var url = BASE_URL + "loyalty/getAvailableLoyaltyCodes";
            var data = {loyalty_type: $scope.newCust.customerLoyaltyNo};

            $http.post(url, data)
                .success(function(res){
                    if (res.status) {
                        $scope.loyaltyCodes = res.data;
                        if (!Object.keys(res.data).length) {
                            $scope.newCust.customerLoyaltyCode = "";
                        }
                    }
                });
        }

        $scope.validateNewCustomer = function(){
            var emailRegex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;

            if(!$scope.newCust || !$scope.newCust.customerName){
                p_notification(false, "Please enter customer name, it is mandatory.");
                return false;
            }

            if(!$scope.newCust.customerCode){
                p_notification(false, "Please enter a customer code, it is mandatory.");
                return false;
            }

            //
            // if(!$scope.newCust.customerTelephoneNumber){
            //     p_notification(false, "Please enter telephone number, it is mandatory.");
            //     return false;
            // }
            if($scope.newCust.customerTelephoneNumber){
                if(isNaN($scope.newCust.customerTelephoneNumber) || $scope.newCust.customerTelephoneNumber.length < 9 || $scope.newCust.customerTelephoneNumber.length > 15){
                    p_notification(false, "Phone number should be numeric and between 9 and 15 digits.");
                    return false;
                }
            }

            if($scope.newCust.customerEmail && !emailRegex.test($scope.newCust.customerEmail)){
                p_notification(false, "Please enter a valid email address.");
                return false;
            }

            if($scope.newCust.customerCreditLimit && isNaN($scope.newCust.customerCreditLimit)){
                p_notification(false, "Credit limit should be numeric.");
                return false;
            }else if($scope.newCust.customerCreditLimit && $scope.newCust.customerCreditLimit < 0){
                p_notification(false, "Credit limit cannot be negative.");
                return false;
            }

            if($scope.newCust.customerDiscount && (isNaN($scope.newCust.customerDiscount) || $scope.newCust.customerDiscount < 0 || $scope.newCust.customerDiscount > 100)){
                p_notification(false, "Discount must be positive, numeric and less than or equal to 100%");
                return false;
            }

            if($scope.newCust.customerLoyaltyNo && !$scope.newCust.customerLoyaltyCode){
                p_notification(false, "Please select loyalty code for the customer.");
                return false;
            }

            return true;
        }

        // Default configs
        $scope.config = {
            pos_modes: [
                {id: "nor", name: "Invoice & Pay"},
                {id: "pay", name: "Pay Only"},
                {id: "inv", name: "Invoice Only"}
            ],
            active_pos_mode: {id: "nor", name: "Normal"}
        };
        $scope.active_pos_mode = {id: "nor", name: "Normal"};
        $scope.previous_pos_mode = {};

        /**
         * Retrive requested configuration value
         * @param  {string} param Config name. If there are multiple levels use period (.) to seperate. Eg."pos_modes.pay"
         * @return {Sting | number | Object | Null}
         */
        $scope.getConfig = function(param){

            // var paramAr = param.split('.');
            // var cnf = $scope.config;

            // for (var i in paramAr) {
            //     cnf = cnf[paramAr[i]];
            // };

            // return cnf;
            return $scope.config[param];
        }


        // /**
        //  * Set configurations
        //  * @param  {string} param Config name. If there are multiple levels use period (.) to seperate. Eg."pos_modes.pay"
        //  * @param  {String | Number | Object} value Configuration value
        //  * @return {bool}       [description]
        //  */
        // $scope.setConfig = function(param, value){

        //             console.log($scope.config);
        //     var paramAr = param.split('.');
        //     var cnf = $scope.config;

        //     for (var i in paramAr) {

        //         if((parseInt(i) + 1) == paramAr.length){
        //             cnf[paramAr[i]] = value;
        //             console.log($scope.config);
        //             return true;
        //         }

        //         if(!cnf){
        //             return false;
        //         }
        //     };
        //     return false;
        // }


        $scope.setConfig = function(cnf, val){
            $scope.config["active_pos_mode"] = val;
            $scope.active_pos_mode = val;

            local.set('config', $scope.config);

            return true;
        };

        $scope.initConfig = function(){
            var lc = local.get('config');
            if(lc){
                updateConfig(lc, $scope.config)
            }else{
                local.set('config', $scope.config)
            }
        }
        $scope.initConfig();

        function updateConfig (localConf, deflConf){
            for (var i in deflConf){
                if(typeof(localConf[i]) == "object" && typeof(deflConf[i]) == "object"){
                    updateConfig(localConf[i], deflConf[i]);
                }else if(typeof(localConf[i]) != "object" && typeof(deflConf[i]) != "object"){
                    if(!localConf[i]){
                    localConf[i] = deflConf[i];
                }

                deflConf[i] = localConf[i]
                }else{
                    localConf[i] = deflConf[i];
                }
            }

            for (var i in localConf){
                if(typeof(localConf[i]) == "object" && typeof(deflConf[i]) == "object"){
                    updateConfig(localConf[i], deflConf[i]);
                }

                if(localConf[i] && !deflConf[i]){
                    delete localConf[i];
                }
            }
        };

        var invUpdater = false;
        $scope.changeMode = function(mode){
            if($scope.inv.cart.length){
                p_notification(false, "Please hold or cancel current invoice to continue.");
                return false;
            }

            $scope.config.active_pos_mode = mode;
            $scope.setConfig('active_pos_mode', mode);

            if(mode.id == 'pay'){
                $scope.updateInvoice()
                invUpdater = setInterval(function() {
                    $scope.updateInvoice();
                }, 5000);

                $('#priceLabel').removeClass('hidden');
                $('#inclusiveTaxPriceLabel').addClass('hidden');
                $('#totalLabel').removeClass('hidden');
                $('#inclusiveTaxTotalLabel').addClass('hidden');
            }else{
                if(invUpdater){
                    clearInterval(invUpdater);
                    invUpdater = false;
                }

                if ($scope.inclusive_tax_on_eligible) {
                    $('#priceLabel').addClass('hidden');
                    $('#inclusiveTaxPriceLabel').removeClass('hidden');
                    $('#totalLabel').addClass('hidden');
                    $('#inclusiveTaxTotalLabel').removeClass('hidden');
                } else {
                    $('#priceLabel').removeClass('hidden');
                    $('#inclusiveTaxPriceLabel').addClass('hidden');
                    $('#totalLabel').removeClass('hidden');
                    $('#inclusiveTaxTotalLabel').addClass('hidden');
                }
            }
        }

        /**
         * Get unpaid invoices list to show in "pay only" mode
         */
        $scope.updateInvoice = function(){
            var invCount = $scope.invoiceList.length;
            var lastInvID = 0;

            if(invCount){
                for (var i = 0; i < $scope.invoiceList.length; i++) {
                    lastInvID = parseInt(lastInvID) < parseInt($scope.invoiceList[i].id) ? $scope.invoiceList[i].id : lastInvID;
                };
            }


            $http.post(BASE_URL + "/pos-api/get-unpaid-pos-invoices", {lastID: lastInvID})
                .success(function(data) {
                    if(data.status){
                        if(invCount){
                            for(var inv in data.data){
                                $scope.invoiceList.unshift(data.data[inv]);
                            }
                        }else{
                            $scope.invoiceList = data.data;
                            selectFirstInvoice();
                        }
                    }
                });
        }

        $scope.getInvoiceDetails = function(invId){
            $http.post(BASE_URL + "/pos-api/get-invoice-products", {invoiceID: invId})
                .success(function(res){
                    if(!res.status){
                        // Show error message: "Couldn't retrieve invoice"
                        p_notification(false, res.msg);
                        return false;
                    }

                    $scope.inv.discountSelect = 'pre';
                    $scope.inv.discount = 0.00;

                    var invDetails = _.findWhere($scope.invoiceList, {id: invId})
                    if(invDetails.invDiscType == "presentage" && invDetails.invDiscVal != null){
                        $scope.inv.discountSelect = 'pre';
                        $scope.inv.discount = parseFloat(invDetails.invDiscRate);
                    }else if(invDetails.invDiscVal != null){
                        $scope.inv.discountSelect = 'val';
                        $scope.inv.discount = parseFloat(invDetails.invDiscVal).toFixed(2);
                    }

                    $scope.inv.promotionDiscount = invDetails.invPromoDisc;
                    $scope.promotion.totalDiscount = invDetails.invPromoDisc;
                    $scope.inv.totalTax = 0;
                    $scope.inv.cart = new Array();

                    $scope.inv.invoiceId = invDetails.id;
                    $scope.inv.invoiceCode = invDetails.code;

                    // Set customer
                    if(invDetails.cust_id){
                        $scope.getCustomer(invDetails.cust_id);
                    }

                    for(var proId in res.data){

                        var pro = res.data[proId];

                        var sign = "";
                        if(pro.salesInvoiceProductDiscount > 0 && pro.salesInvoiceProductDiscountType == 'precentage'){
                            var sign = "%";
                        }else if(pro.salesInvoiceProductDiscount > 0){
                            var sign = currency;
                        }

                        $scope.inv.cart.push({
                            pID: pro.productID,
                            pC: pro.productCode,
                            pN: pro.productName,
                            pT: pro.productType,
                            qty: parseFloat(pro.salesInvoiceProductQuantity),
                            pUom: pro.uomAbbr,
                            pUomUc: pro.productUomConversion, //productUom.uC,
                            pR: parseFloat(pro.salesInvoiceProductPrice), // productUom.uC,
                            disc: discount.amount, // Calculated discount value for the product (price * discount)
                            dType: pro.salesInvoiceProductDiscountType, // Discount Type
                            dAmo: pro.salesInvoiceProductDiscount, // Dedicated discount for each product
                            sign: sign,
                            pTotal: pro.salesInvoiceProductTotal
                            // tax: taxDetails,
                            // giftCard: PRODUCTS_DETAILS[productID].giftCard,
                            // uomOptions: uomSelectOptions
                        });

                        if ($scope.inclusive_tax_on_eligible == true && $scope.togglePosModal == true) {
                            if (pro.salesInvoiceProductDiscount == 0 || pro.salesInvoiceProductDiscount == null) {
                                var inclusiveTaxTotalPrice = parseFloat(pro.salesInvoiceProductQuantity) * parseFloat(pro.inclusiveTaxSalesInvoiceProductPrice);
                                var realTotalWithoutTax = parseFloat(pro.salesInvoiceProductQuantity) * parseFloat(pro.salesInvoiceProductPrice);
                                var newTotalTax = parseFloat(inclusiveTaxTotalPrice) - parseFloat(realTotalWithoutTax);
                                $scope.inv.totalTax += newTotalTax ? parseFloat(newTotalTax) : 0;
                            } else {
                                $scope.inv.totalTax += pro.taxTotal ? parseFloat(pro.taxTotal) : 0;
                            }
                        } else {
                            $scope.inv.totalTax += pro.taxTotal ? parseFloat(pro.taxTotal) : 0;
                        }
                    }


                });

        }
        $scope.changeMode($scope.config.active_pos_mode);

        /**
         * Add products for relavent order.
         * @param {int} productID
         */
        $scope.addToCart = function(productID, selectedUom) {
            var uom = new Array(),
                taxes = new Array(),
                taxDetails = new Array(),
                discount = 0,
                itemCost = 0;

                // Set quantity and unit price on credit note mode
                if($scope.creditNoteFlag == true){
                    var qty = $scope.creditNoteItemQty;
                    var realUnitPrice = PRODUCTS_DETAILS[productID].pR;
                    PRODUCTS_DETAILS[productID].pR = $scope.creditNoteItemPrice;
                    PRODUCTS_DETAILS[productID].incPR = $scope.creditNoteInclusiveTaxItemPrice;
                    $scope.creditNoteItemPrice = 0;
                    $scope.creditNoteInclusiveTaxItemPrice = 0;
                }
                else{
                   var qty = $scope.selectedProduct.qty !== undefined ? $scope.selectedProduct.qty : 1;
               }
            angular.forEach(PRODUCTS_DETAILS[productID].uom, function(val, key) {
                this.push({uA: val.uA, id: key, uC: val.uC, disp: val.disp});
            }, uom);

            if (locationTax == true) {
                angular.forEach(PRODUCTS_DETAILS[productID].tax, function(val, key) {
                    this.push(key);
                }, taxes);
            }

            discount = getItemDiscount(productID, PRODUCTS_DETAILS[productID].pR, null);
            itemCost = (PRODUCTS_DETAILS[productID].pR - discount.amount) * 1;

            // if invoice discount has given as a percentage, that percentage should be deducted form item price before calculate tax
            var invDisc = 0;
            if($scope.inv.discountSelect == "pre"){
                invDisc= itemCost*$scope.inv.discount/100;
            }

            taxDetails = calculateItemCustomTax(itemCost-invDisc, taxes);
            if (!taxDetails) {
                taxDetails = {tTA: "", tL: {}};
            }
            taxDetails.tTA = parseFloat(taxDetails.tTA);

            if (uom[0] === undefined) {
                uom[0] = {uA: "", id: ""};
            } else {
                for (var i in uom) {
                    if (uom[i].uC == 1) {
                        baseUom = uom[i];
                    }
                    if (uom[i].disp == 1) {
                        displayUom = uom[i];
                    }
                }
            }
            productUom = displayUom != undefined ? displayUom : baseUom;

            if(selectedUom){
                productUom = _.findWhere(uom, {id: selectedUom});
            }


            var uomSelectOptions = {value: '', options: []};
            angular.forEach(PRODUCTS_DETAILS[productID].uom, function(val, key) {
                this.push({id: key, uA: val.uA});
            }, uomSelectOptions.options);

            uomSelectOptions.value = _.where(uomSelectOptions.options, {id: productUom.id})[0];

            if ($scope.sub_product[productID] != undefined) {
                if ($scope.sub_product[productID].type == "b") {
                    
                    if ($scope.priceListSelectID != null) {
                        if(!$scope.priceListItems[productID]) {
                            // var pPrice = PRODUCTS_DETAILS[productID].pR * productUom.uC;    
                            var totalBPrice = 0;
                            $.each($scope.sub_product[productID].products, function(index, val) {
                                if (val[0] != undefined) {
                                    var subBatchQty = val[0].val;
                                    var batchPrice = val.PBPRICE;
                                    totalBPrice += parseFloat(subBatchQty) * parseFloat(batchPrice); 
                                }
                            });

                            if (totalBPrice == 0) {
                                var pPrice = PRODUCTS_DETAILS[productID].pR * productUom.uC;
                            } else {
                                var pPrice = ((totalBPrice / qty) * productUom.uC).toFixed(2);
                            }

                        } else {

                            var pPrice = $scope.priceListItems[productID]['itemPrice'] * productUom.uC;  
                        }

                    } else {
                        var totalBPrice = 0;
                        $.each($scope.sub_product[productID].products, function(index, val) {
                            if (val[0] != undefined) {
                                var subBatchQty = val[0].val;
                                var batchPrice = val.PBPRICE;
                                totalBPrice += parseFloat(subBatchQty) * parseFloat(batchPrice); 
                            }
                        });

                        if (totalBPrice == 0) {
                            var pPrice = PRODUCTS_DETAILS[productID].pR * productUom.uC;
                        } else {
                            var pPrice = ((totalBPrice / qty) * productUom.uC).toFixed(2);
                        }
                    }

                } else {
                    // var pPrice = PRODUCTS_DETAILS[productID].pR * productUom.uC;
                    if ($scope.priceListSelectID != null) {
                        if(!$scope.priceListItems[productID]) {

                            var pPrice = PRODUCTS_DETAILS[productID].pR * productUom.uC;    
                        } else {

                            var pPrice = $scope.priceListItems[productID]['itemPrice'] * productUom.uC;  
                        }

                    } else {

                        var pPrice = PRODUCTS_DETAILS[productID].pR * productUom.uC;    
                    }
                }
            } else {

                if ($scope.priceListSelectID != null) {
                    if(!$scope.priceListItems[productID]) {

                        var pPrice = PRODUCTS_DETAILS[productID].pR * productUom.uC;    
                    } else {

                        var pPrice = $scope.priceListItems[productID]['itemPrice'] * productUom.uC;  
                    }

                } else {

                    var pPrice = PRODUCTS_DETAILS[productID].pR * productUom.uC;    
                }

            }

            $scope.inv.cart.push({
                pID: productID,
                pC: PRODUCTS_DETAILS[productID].pC,
                pN: PRODUCTS_DETAILS[productID].pN,
                pT: PRODUCTS_DETAILS[productID].pT,
                dE: PRODUCTS_DETAILS[productID].dE,
                qty: qty,
                pUom: productUom.uA,
                pUomUc: productUom.uC,
                pR: pPrice,
                disc: discount.amount, // Applyed discount value for the product
                dType: discount.type, // Discount Type
                dAmo: discount.val, //
                sign: discount.sign,
                tax: taxDetails,
                giftCard: PRODUCTS_DETAILS[productID].giftCard,
                uomOptions: uomSelectOptions,
                lPID : PRODUCTS_DETAILS[productID].lPID,
                incPrInCreditNote : PRODUCTS_DETAILS[productID].incPR,
            });
            if($scope.creditNoteFlag !== true){
                focusNext();
            }else{
                PRODUCTS_DETAILS[productID].pR = realUnitPrice;
            }

           

        };

        /**
         * When select differant UOM from options, change unit price of selected products to invoice.
         * @param {int} cartIndex Index id of the list of selected items in invoice.
         */
        $scope.setUom = function(cartIndex) {
            var pID = $scope.inv.cart[cartIndex].pID;
            var selectedUom = $scope.inv.cart[cartIndex].uomOptions.value.id;
            var originalPrice = $scope.inv.cart[cartIndex].pR / $scope.inv.cart[cartIndex].pUomUc;
            var uom = PRODUCTS_DETAILS[pID].uom[selectedUom];
            var discount = getItemDiscount(pID, originalPrice, cartIndex);

            $scope.inv.cart[cartIndex].pR = originalPrice * uom.uC;
            $scope.inv.cart[cartIndex].pUom = uom.uA;
            $scope.inv.cart[cartIndex].pUomUc = uom.uC;
            $scope.inv.cart[cartIndex].disc = discount.amount;
            $scope.inv.cart[cartIndex].dType = discount.type;
            $scope.inv.cart[cartIndex].dAmo = discount.val;
            $scope.inv.cart[cartIndex].sign = discount.sign;

            /*
             If the product hsas batch items in subproducts array with quantity (sItem.val),
             it should be changed to corospondig value of selected UOM.
             */
            if($scope.sub_product[pID]){
                angular.forEach($scope.sub_product[pID].products, function(sItem, sId){
                    if(sItem.PBC){
                            var dispUOM = _.findWhere(PRODUCTS_DETAILS[pID].uom, {disp:"1"});
                            var newVal = (sItem.val / dispUOM.uC) * uom.uC;

                            sItem.val = newVal;
                    }
                });
            }
            $scope.validate('qty',cartIndex);
            focusNext();
        }

        /**
         * Convert given quantity (in base UOM) in to displaying uom
         * @param  {float} qty     Quantity
         * @param  {[type]} uomList List of UOMs
         * @return {string}         "25 Kg"
         */
        $scope.getQtyByDisplayUOM = function(qty, uomList){
            qty = parseFloat(qty);
            var uom = _.findWhere(uomList, {disp:"1"});
            var dispQTY = (qty / uom.uC).toFixed(uom.uDP);

            return dispQTY + ' ' + uom.uA;
        }

        /**
         * Calculate price of the product according to display UOM
         * @param  {int} productID   product ID of the product
         * @return {float}
         */
        $scope.getDisplayPrice = function(productID) {
            var uom = new Array();

            angular.forEach(PRODUCTS_DETAILS[productID].uom, function(val, key) {
                this.push({uA: val.uA, id: key, uC: val.uC, disp: val.disp});
            }, uom);

            if (uom[0] === undefined) {
                uom[0] = {uA: "", id: ""};
            } else {
                for (var i in uom) {
                    if (uom[i].uC == 1) {
                        baseUom = uom[i];
                    }
                    if (uom[i].disp == 1) {
                        displayUom = uom[i];
                    }
                }
            }
            productUom = displayUom != undefined ? displayUom : baseUom;
            return PRODUCTS_DETAILS[productID].pR * productUom.uC;
        }

        /**
         * Set total quantity of selected subproducts to main product.
         */
        $scope.setQuantity = function(subProductID) {
            var totalQty = 0;
            var subProduct = $scope.sub_product[$scope.selectedProduct.pID];
            var displayUom = _.findWhere($scope.selectedProduct.uom, {disp: "1"});
            var curOdr = $scope.orderNumber;

            // "obj" use for a single sub pruduct which contains in "subProduct.products" object.
            angular.forEach(subProduct.products, function(obj, key) {
                if (obj.val == undefined && (obj[curOdr] == undefined)) {
                    return;
                }

                if(subProduct.type == 'b'){
                    var val = parseFloat(obj[curOdr].val);
                }else{
                    var val = parseFloat(obj.val);
                }

                if (subProduct.type !== 'b' || (subProduct.type === 'b' && val * displayUom.uC <= obj.PBQ)) {

                    // When found matching serial product to subProductID set its order number as current order's number.
                    if(obj.PSID && obj.PSID == subProductID){
                        subProduct.products[obj.PSID].orderNo = curOdr;
                    }

                    if(subProduct.type == "b" || ( subProduct.products[obj.PSID] && subProduct.products[obj.PSID].orderNo == curOdr)){
                        totalQty += val;
                    }
                } else {
                    subProduct.products[obj.PBID].val = 0;
                    if(subProduct.products[obj.PSID]){
                            subProduct.products[obj.PSID].orderNo = -1;
                        }
                }
            });
            var isexists = _.where($scope.inv.cart, {pID: $scope.selectedProduct.pID});
            if (isexists.length > 0) {
                isexists[0].qty = totalQty;

                if (subProduct != undefined) {
                    if (subProduct.type == "b") {
                        var totalBPrice = 0;
                        $.each(subProduct.products, function(index, val) {
                            if (val[0] != undefined) {
                                var subBatchQty = val[0].val;
                                var batchPrice = val.PBPRICE;
                                totalBPrice += parseFloat(subBatchQty) * parseFloat(batchPrice); 
                            }
                        });

                        if (totalBPrice == 0) {
                            var pPrice = ($scope.selectedProduct.pR).toFixed(2);
                        } else {
                            var pPrice = (totalBPrice / totalQty).toFixed(2);
                        }
                        isexists[0].pR = pPrice;
                    } 
                } 
            }
            $scope.selectedProduct.qty = totalQty;
        };

        $scope.setSubProducts = function(productID) {
            $scope.sub_product[productID] = {};
            if (PRODUCTS_DETAILS[productID].sP === '1' && PRODUCTS_DETAILS[productID].bP === '1') {
                $scope.sub_product[productID].type = 'bs';
                $scope.sub_product[productID].products = PRODUCTS_DETAILS[productID].batchSerial;
                $scope.selectedProduct.type = 'bs';
                $scope.selectedProduct.id = 'bs';
            }
            else if (PRODUCTS_DETAILS[productID].sP === '1' && PRODUCTS_DETAILS[productID].bP !== '1') {
                $scope.sub_product[productID].type = 's';
                $scope.sub_product[productID].products = PRODUCTS_DETAILS[productID].serial;
                $scope.selectedProduct.type = 's';
            }
            else if (PRODUCTS_DETAILS[productID].sP !== '1' && PRODUCTS_DETAILS[productID].bP === '1') {
                $scope.sub_product[productID].type = 'b';
                $scope.sub_product[productID].products = PRODUCTS_DETAILS[productID].batch;
                $scope.selectedProduct.type = 'b';
            }

            if (PRODUCTS_DETAILS[productID].auto_add_id) {
                $scope.sub_product[productID].products[PRODUCTS_DETAILS[productID].auto_add_id].val = 1;
                $scope.setQuantity();
                $scope.saveSubProduct(productID);
            } else {
                show_selector_panel();
            }
        };

        $scope.saveSubProduct = function(productID) {
            $scope.selector_key = null;
            var valid = false;
            if (Object.keys($scope.sub_product[productID].products).length) {
                angular.forEach($scope.sub_product[productID].products, function(obj, key) {

                    if(obj.PBID != null && obj[$scope.orderNumber] != undefined && obj[$scope.orderNumber].val > 0){
                        valid = true;
                        return;
                    }
                    else if (obj.val != undefined && obj.val > 0) {
                        if(obj.orderNo == $scope.orderNumber){
                            valid = true;
                            return;
                        }
                    }
                });

                if (valid) {
                    var isexists = _.where($scope.inv.cart, {pID: productID});
                    if (isexists.length === 0) {
                        $scope.addToCart(productID);
                    }
                    closeItemSelector();
                } else {
                    p_notification(false, 'Select at least one item.');
                }
            } else {
                closeItemSelector();
            }
        };

        $scope.selectProduct = function(productID) {
            $scope.key = null;
            $scope.selectedProduct = PRODUCTS_DETAILS[productID];
            if (PRODUCTS_DETAILS[productID].sP === '1' || PRODUCTS_DETAILS[productID].bP === '1') {
                $scope.setSubProducts(productID);
            } else {
                var isexists = _.where($scope.inv.cart, {pID: productID});
                if (isexists.length > 0) {
                    isexists[0].qty++;
                    //Hilight productRow when already added product qty incrementd
                    var selectedProductRaw = $('.details_tbody > tr#pid_' + productID);
                    selectedProductRaw.addClass('animateBackground');
                    setTimeout(function() {
                        /*make sure remove previously added class after the 1s.
                         *adding same class again does not animate the ele*/
                        selectedProductRaw.removeClass('animateBackground');
                    }, 1000);
                } else {
                    $scope.addToCart(productID);
                }
            }

            if(!$scope.customer.customerLoyaltyCode){
                $scope.removeLoyaltyCard();
            }else if($scope.customer.customerLoyaltyCode){
                $scope.loyalty.card_no = $scope.customer.customerLoyaltyCode;
                // false - do not get customer
                $scope.applyLoyaltyCard(false);
            }
            hideItemDetails();
        };
        /**
         * Calculate product cost, tax and discount
         * @param {type} i for product id
         * @returns {$scope.inv.cart.qty|Number|discount.amount|$scope.inv.cart.pR}
         */
        $scope.getItemTotal = function(i) {

            if ($scope.inv.cart[i] !== undefined) {

                if ($scope.inclusive_tax_on_eligible == true && $scope.togglePosModal == true) {
                    var itemCost = $scope.getItemTotalForInclusiveTax(i);
                    return itemCost;
                } else {
                    if (!PRODUCTS_DETAILS[$scope.inv.cart[i].pID]) {
                        // var errorMsg = $scope.inv.cart[i].pC + " - " + $scope.inv.cart[i].pN + " has been removed from inventory.";
                        // p_notification(false, errorMsg );
                        return;
                    }

                    var taxes = new Array(),
                            taxDetails = new Array(),
                            discount = 0,
                            itemCost = 0;
                    angular.forEach($scope.inv.cart[i].tax.tL, function(val, key) {
                        this.push(key);
                    }, taxes);

                    promoDiscount = $scope.applyPromotion($scope.inv.cart[i].pID, $scope.inv.cart[i].pR, $scope.inv.cart[i].qty);


                    if (promoDiscount) {
                        discount = {val: promoDiscount, type: "value", amount: promoDiscount, sign: $scope.currency};
                        $scope.inv.cart[i].disc = discount.amount; // Applyed discount value for the product
                        $scope.inv.cart[i].dType = discount.type; // Discount Type
                        // $scope.inv.cart[i].dAmo = discount.val / $scope.inv.cart[i].qty; //
                        $scope.inv.cart[i].sign = discount.sign;
                    } else {
                        discount = getItemDiscount($scope.inv.cart[i].pID, $scope.inv.cart[i].pR, i);
                        discount.amount = discount.amount * $scope.inv.cart[i].qty;
                        $scope.inv.cart[i].disc = discount.amount; // Applyed discount value for the product
                        $scope.inv.cart[i].dType = discount.type; // Discount Type
                        // $scope.inv.cart[i].dAmo = discount.val; //
                        $scope.inv.cart[i].sign = discount.sign;
                    }

                    itemCost = ($scope.inv.cart[i].pR * $scope.inv.cart[i].qty) - discount.amount;

                    // if invoice discount has given as a percentage, that percentage should be deducted form item price before calculate tax
                    var invDisc = 0;
                    if($scope.inv.discountSelect == "pre"){
                        invDisc = itemCost*$scope.inv.discount/100;
                    }

                    taxDetails = calculateItemCustomTax(itemCost - invDisc, taxes);
                    if (!taxDetails) {
                        taxDetails = {tTA: "0", tL: {}};
                    }
                    taxAmount = (taxDetails.tTA === undefined) ? 0 : taxDetails.tTA;
                    taxDetails.tTA = isNaN(parseFloat(taxDetails.tTA)) ? 0 : taxDetails.tTA;
                    $scope.inv.cart[i].tax = taxDetails;
                    return itemCost;
                }
            }
        };
        /**
         * Calculate product cost, tax and discount for inclusive tax
         * @param {type} i for product id
         * @returns {$scope.inv.cart.qty|Number|discount.amount|$scope.inv.cart.pR}
         */
        $scope.getItemTotalForInclusiveTax = function(i) {

            if ($scope.inv.cart[i] !== undefined) {
                if (!PRODUCTS_DETAILS[$scope.inv.cart[i].pID]) {
                    // var errorMsg = $scope.inv.cart[i].pC + " - " + $scope.inv.cart[i].pN + " has been removed from inventory.";
                    // p_notification(false, errorMsg );
                    return;
                }

                var taxes = new Array(),
                        taxDetails = new Array(),
                        discount = 0,
                        itemCost = 0;
                angular.forEach($scope.inv.cart[i].tax.tL, function(val, key) {
                    this.push(key);
                }, taxes);


                var realUnitPrice = calculateUnitePriceByInclusiveTax($scope.inv.cart[i].pR, taxes);
                $scope.inv.cart[i].inclusiveTaxpR = realUnitPrice;
                promoDiscount = $scope.applyPromotion($scope.inv.cart[i].pID, realUnitPrice, $scope.inv.cart[i].qty);

                if (promoDiscount) {
                    discount = {val: promoDiscount, type: "value", amount: promoDiscount, sign: $scope.currency};
                    $scope.inv.cart[i].disc = discount.amount; // Applyed discount value for the product
                    $scope.inv.cart[i].dType = discount.type; // Discount Type
                    // $scope.inv.cart[i].dAmo = discount.val / $scope.inv.cart[i].qty; //
                    $scope.inv.cart[i].sign = discount.sign;
                } else {
                    discount = getItemDiscount($scope.inv.cart[i].pID, realUnitPrice, i);
                    discount.amount = discount.amount * $scope.inv.cart[i].qty;
                    $scope.inv.cart[i].disc = discount.amount; // Applyed discount value for the product
                    $scope.inv.cart[i].dType = discount.type; // Discount Type
                    // $scope.inv.cart[i].dAmo = discount.val; //
                    $scope.inv.cart[i].sign = discount.sign;
                }

                itemCost = (realUnitPrice * $scope.inv.cart[i].qty) - discount.amount;

                // if invoice discount has given as a percentage, that percentage should be deducted form item price before calculate tax
                var invDisc = 0;
                if($scope.inv.discountSelect == "pre"){
                    invDisc = itemCost*$scope.inv.discount/100;
                }

                taxDetails = calculateItemCustomTax(itemCost - invDisc, taxes);
                if (!taxDetails) {
                    taxDetails = {tTA: "0", tL: {}};
                }
                taxAmount = (taxDetails.tTA === undefined) ? 0 : taxDetails.tTA;
                taxDetails.tTA = isNaN(parseFloat(taxDetails.tTA)) ? 0 : taxDetails.tTA;
                $scope.inv.cart[i].tax = taxDetails;
                return itemCost;
            }
        };
        /**
         * Calculate total tax amount for an order
         * @returns {Number|@var;totalTax}
         */

        $scope.getTotalTax = function() {
            var totalTax = 0;
            var totalPriceWithTax = 0;
            var totalPriceWithOutTax = 0;
            angular.forEach($scope.inv.cart, function(val, key) {
                var itemTax = isNaN($scope.inv.cart[key].tax.tTA) ? 0 : $scope.inv.cart[key].tax.tTA;
                totalTax += parseFloat(itemTax);

                if ($scope.togglePosModal == true) {
                    if ($scope.inv.cart[key].disc == 0) {
                        totalPriceWithTax += parseFloat($scope.inv.cart[key].qty) * parseFloat($scope.inv.cart[key].pR);
                    } else {
                        var temp = (parseFloat($scope.inv.cart[key].pTotal) + parseFloat(itemTax));
                        totalPriceWithTax += parseFloat(temp);
                    }
                } else {
                    if ($scope.inv.cart[key].disc == 0) {
                        totalPriceWithTax += parseFloat($scope.inv.cart[key].qty) * parseFloat($scope.inv.cart[key].incPrInCreditNote);
                    } else {
                        var temp = (parseFloat($scope.inv.cart[key].pTotal) + parseFloat(itemTax));
                        totalPriceWithTax += parseFloat(temp);
                    }
                }
                totalPriceWithOutTax += parseFloat($scope.inv.cart[key].pTotal);
            }, totalTax);
           
            totalTax = (isNaN(totalTax)) ? 0 : totalTax;
            if ($scope.inclusive_tax_on_eligible == true) {
                return totalPriceWithTax - totalPriceWithOutTax.toFixed(2);
            } else {
                return totalTax;
            }



        };

        $scope.getSubTotal = function() {
            var total = 0;
            angular.forEach($scope.inv.cart, function(val, key) {
                total += parseFloat($scope.inv.cart[key].pTotal ? $scope.inv.cart[key].pTotal : 0);
            }, total);
            //add new variable call totalBeforeDiscount in to the inv array
            $scope.inv.totalBeforeDiscount=total+$scope.inv.totalTax;
            return total;
        };

        $scope.getTotalPayment = function(){
            var cash = parseFloat($scope.pay.cash ? $scope.pay.cash : 0);
            var cardPayment = parseFloat($scope.pay.cardPayment ? $scope.pay.cardPayment : 0);
            var loyaltyPayment = parseFloat($scope.pay.redeem_value ? $scope.pay.redeem_value : 0);
            var giftCard = $scope.getGiftCardTotal();
            var creditPayment = parseFloat($scope.pay.creditNotePayment ? $scope.pay.creditNotePayment : 0);

            return total_paymet = cash + cardPayment + loyaltyPayment + giftCard +creditPayment;
        }

        $scope.getBalance = function () {
            var promoDiscount = parseFloat($scope.promotion.totalDiscount ? $scope.promotion.totalDiscount : 0);
            var finTotal = parseFloat($scope.inv.finTotal ? $scope.inv.finTotal : 0);

            return ($scope.getTotalPayment() + promoDiscount - finTotal).toFixed(2);
        };

        $scope.validatePayment = function() {

            if ($scope.getBalance() < 0) {
                p_notification(false, "Payment is not sufficient.");
                return false;
            }
            if ($scope.pay.cardPayment > 0) {
                if ($scope.pay.cardRef && isNaN($scope.pay.cardRef)){
                    p_notification(false, "Card referance number should be numeric.");
                    return false;
                }
                if ($scope.pay.cardNo && isNaN($scope.pay.cardNo)) {
                    p_notification(false, "Card number should be numeric.");
                    return false;
                }
                // if (!$scope.pay.cardRef){
                //     p_notification(false, "Card referance cannot be empty.");
                //     return false;
                // }
                // if (!$scope.pay.cardNo) {
                //     p_notification(false, "Card number cannot be empty.");
                //     return false;
                // }
            }
            if ($scope.pay.redeem_value > 0) {
                if ($scope.loyalty.current_points >= $scope.pay.redeem_value / $scope.loyalty.redeem) {
                    $scope.loyalty.redeemed_points = $scope.pay.redeem_value / $scope.loyalty.redeem;
                } else {
                    p_notification(false, "Maximum redemption value has exceed.");
                    return false;
                }
            }
            if ($scope.gift_card != undefined) {
                $scope.pay.giftCards = $scope.getAppliedGiftCards();
            }
            return true;
        };


        $scope.doPayment = function() {
            if ($scope.config.active_pos_mode.id != 'inv' && (!$scope.validatePayment()) || !$scope.ready) {
                return false;
            }

            $scope.inv.discountOnlyOnEligible = $scope.dis_on_eligible;
            $scope.inv.inclusiveTax = $scope.inclusive_tax_on_eligible;

            $scope.ready = false;
            var orderData = {
                invoice: $scope.inv,
                payment: $scope.pay,
                cust_id: $scope.getCustomerID(),
                cust_credit: $scope.getCustomerCredit(),
                cust_balance: $scope.getCustomerBalance(),
                subProduct: $scope.getSubProductDetails(),
                loyaltyData: $scope.loyalty,
                giftCard: $scope.gift_card,
                giftCardDetails: $scope.getAppliedGiftCards(),
                promotion: {
                    'promoID': $scope.promotion.promoID,
                    'itemList': $scope.promotion.itemList,
                    'totalDiscount': $scope.promotion.totalDiscount
                },
                totalPayment: $scope.getTotalPayment(),
                posMode: $scope.config.active_pos_mode.id,
                payedCreditNoteID : $scope.payedCreditNoteID,

            };
            $http.post(BASE_URL + "/pos-api/create", orderData)
                    .success(function(data) {
                        if (!data.status) {
                            p_notification(false, data.msg);
                            $scope.ready = true;
                            hidePaymentModal();

                            return;
                        }
                        if ($scope.config.active_pos_mode.id != "pay") {
                            $scope.updateItems();
                        }
                        $scope.updateGiftCards(orderData.giftCard);

                        $scope.closingBalance = data.data.clBal;

                        orderData.dateFormat = dateFormat;
                        orderData.timeFormat = timeFormat;
                        orderData.invCode = data.data.invCode;
                        orderData.customerName = $scope.getCustomerName();

                        if ($scope.holds.indexOf($scope.orderNumber) > -1) {
                            $scope.cancelOrder();
                        } else {
                            newOrder();
                        }

                        if (orderData.loyaltyData) {
                            orderData.earnedLoyaltyPoints = orderData.loyaltyData.earned_points;
                            orderData.totalLoyaltyPoints = parseFloat(orderData.loyaltyData.current_points) + parseFloat(orderData.loyaltyData.earned_points) - parseFloat(orderData.loyaltyData.redeemed_points);
                        }

                        // delete paid invoice
                        if ($scope.config.active_pos_mode.id == "pay") {
                            var inv = _.findWhere($scope.invoiceList, {id: data.data.invID});
                            var indx = $scope.invoiceList.indexOf(inv);

                            $scope.invoiceList.splice(indx, 1);
                        }
                        if($scope.config.active_pos_mode.id == 'nor'){
                            document.getElementById('printPreview').contentWindow.updatePreview(orderData);
                            printIframe("printPreview", posPrintoutImageConversionStatus);
                        }else if($scope.config.active_pos_mode.id == 'inv'){
                            document.getElementById('printInvOnlyPreview').contentWindow.updatePreview(orderData);
                            printIframe("printInvOnlyPreview", posPrintoutImageConversionStatus);
                        }else if($scope.config.active_pos_mode.id == 'pay' && !$scope.skipPrinting){
                            document.getElementById('printPayOnlyPreview').contentWindow.updatePreview(orderData);
                            printIframe("printPayOnlyPreview", posPrintoutImageConversionStatus);
                        }

                        // revoke temporaly granted permission
                        if($scope.overrideTempSetting.temp){
                            if (alow_price_list == 'deny') {
                                $scope.removePriceList();
                            }
                            $scope.override_price = override_price;
                            $scope.override_discount = override_discount;
                        }
                        $scope.overrideTempSetting.temp = false;
                        $scope.canOverride = false;


                        // revoke temporaly granted permission for use price list
                        if($scope.priceListTempSetting.temp){
                            $scope.alow_price_list = alow_price_list;
                            $scope.alow_edit_price_list_price = alow_edit_price_list_price;
                        }
                        $scope.priceListTempSetting.temp = false;
                        $scope.canUsePriceList = false;
                        $scope.canEditPrice = false;

                        hidePaymentModal();
                        focusNext(".item_search_bar");

                        $scope.ready = true;
                    })
                    .error(function() {

                        var tempCode = $scope.getTemporyReferance();
                        orderData.tempCode = tempCode;

                        // If invoice saved in local storage send current date, time and user with related order
                        orderData.issueDateTime = new Date();
                        orderData.userID = userID;

                        if (local) {
                            if ($scope.config.active_pos_mode.id != "pay") {
                                $scope.updateItems();
                            }
                            $scope.updateGiftCards(orderData.giftCard);
                            $scope.online = false;
                            unSaved[Date.now()] = orderData;
                            $scope.updateLocalStorage();
                            newOrder();
                            hidePaymentModal();
                            $scope.syncOrders();
                            $scope.ready = true;
                        }

                        orderData.invCode = tempCode;
                        orderData.dateFormat = dateFormat;
                        orderData.timeFormat = timeFormat;
                        orderData.customerName = $scope.getCustomerName();

                        if (orderData.loyaltyData) {
                            orderData.earnedLoyaltyPoints = orderData.loyaltyData.earned_points;
                            orderData.totalLoyaltyPoints = parseFloat(orderData.loyaltyData.current_points) + parseFloat(orderData.loyaltyData.earned_points) - parseFloat(orderData.loyaltyData.redeemed_points);
                        }

                        // delete paid invoice
                        if ($scope.config.active_pos_mode.id == "pay") {
                            var inv = _.findWhere($scope.invoiceList, {id: data.data.invID});
                            var indx = $scope.invoiceList.indexOf(inv);

                            $scope.invoiceList.splice(indx, 1);
                        }

                        // revoke temporaly granted permission
                        if($scope.overrideTempSetting.temp){
                            $scope.override_price = override_price;
                            $scope.override_discount = override_discount;
                        }
                        $scope.overrideTempSetting.temp = false;
                        $scope.canOverride = false;

                        if($scope.config.active_pos_mode.id == 'nor'){
                            document.getElementById('printPreview').contentWindow.updatePreview(orderData);
                            printIframe("printPreview", posPrintoutImageConversionStatus);
                        }else if($scope.config.active_pos_mode.id == 'inv'){
                            document.getElementById('printInvOnlyPreview').contentWindow.updatePreview(orderData);
                            printIframe("printInvOnlyPreview", posPrintoutImageConversionStatus);
                        }else if($scope.config.active_pos_mode.id == 'pay' && !$scope.skipPrinting){
                            document.getElementById('printPayOnlyPreview').contentWindow.updatePreview(orderData);
                            printIframe("printPayOnlyPreview", posPrintoutImageConversionStatus);
                        }
                    });
        };
        /**
         * load Invoices to Create Credit Note
         */
        $scope.loadInvoice = function(data)
        {

            $http.post(BASE_URL + "/pos-api/searchDataForPOSCreditNote",{data:data})
                .success(function(respond)
                {
                    $scope.creditNotesItem = respond.data;
                });

            $scope.docType = 'CN';
        }
        /**
        * load selected invoice and related item details to credit note.
        */
        $scope.selectInvoice = function(ID)
        {
            if($scope.online){
                $scope.selectedInvoiceID = ID;
                addInv();
            }
            else{
                p_notification(false,'you are offline. cannot create credit Note.');
            }

        }
        /**
         * Load Credit Note that related to given Invoice ID
         */
        $scope.createCN = function()
        {

            if(!$scope.online){
                p_notification(false,'You are offlne. cannot create credit Note');

                return false;
            }

            $http.post(BASE_URL + "/pos-api/getDataForPOSCreditNote",{data:$scope.selectedInvoiceID})
                .success(function(respond){
                    if(!respond.status){
                        p_notification(false, 'All the items of this invoice have already returned.');
                        return false;
                    }
                    $scope.cNinvoice = _.findWhere($scope.creditNotesItem, {"salesInvoiceID": $scope.selectedInvoiceID});
                    if(respond.data.promotionDetails['0']){
                        $scope.promData = respond.data.promotionDetails['0'];
                        $scope.remPromoDiscVal = respond.data.remainingPromotionDiscValue;
                            if(respond.data.promotionDetails['0'].promotionType == '2' && respond.data.promotionDetails['0'].invoicePromotionDiscountType == '2'){
                                $scope.showPromo = true;
                                $scope.CNPromotionType = 'pre';
                                $scope.CNPromotion.CNPromotionAmount = respond.data.promotionDetails['0'].invoicePromotionDiscountAmount;
                            } else if(respond.data.promotionDetails['0'].promotionType == '2' && respond.data.promotionDetails['0'].invoicePromotionDiscountType == '1'){
                                $scope.showPromo = true;
                                $scope.CNPromotionType = 'val'
                                $scope.CNPromotion.CNPromotionAmount = respond.data.promotionDetails['0'].invoicePromotionDiscountAmount;
                                $scope.tempCNPromoAmount = respond.data.promotionDetails['0'].invoicePromotionDiscountAmount;
                                    if(respond.data.remainingPromotionDiscValue !== null){
                                            $scope.CNPromotion.CNPromotionAmount = respond.data.remainingPromotionDiscValue;
                                            $scope.tempCNPromoAmount = respond.data.remainingPromotionDiscValue;
                                    }

                            }
                    }

                    $scope.creditNoteFlag = true;
                    $scope.creditNoteDetails = respond.data.item;
                    $scope.inv.discount = respond.data.totalDiscAmo;
                    $scope.inv.realDiscount = respond.data.totalDiscAmo;
                    if(respond.data.totalDiscType == 'presentage'){
                        $scope.inv.discountSelect = 'pre';
                        $scope.cNDiscDisable = true;
                    }
                    else{
                        $scope.inv.discountSelect = 'val';
                        if(respond.data.remainingDiscValue !== null)
                        {
                            $scope.inv.discount = respond.data.remainingDiscValue;
                            $scope.inv.existingDiscValue = respond.data.remainingDiscValue;
                        }

                    }
                    $scope.finDiscount = $scope.inv.discount;

                    angular.forEach($scope.creditNoteDetails,function(value,key){
                        var proID = value.pID;
                        var statment = value.pN +' Product is not assign to this location.';
                        var dataSet = respond.data.item[proID];
                        if(PRODUCTS_DETAILS[proID] == null){
                            p_notification(false,statment);
                        }
                        else{
                            if(dataSet.proDiscType == 'value') {

                                PRODUCTS_DETAILS[proID].dP = null;
                                PRODUCTS_DETAILS[proID].dV = dataSet.proDiscount;
                                PRODUCTS_DETAILS[proID].dE = '1';

                            }else if(dataSet.proDiscType == 'precentage') {

                                PRODUCTS_DETAILS[proID].dV = null;
                                PRODUCTS_DETAILS[proID].dE = '1';
                                PRODUCTS_DETAILS[proID].dP = dataSet.proDiscount;
                            }
                            $scope.creditNoteItemQty = value.qty;
                            $scope.creditNoteItemPrice = value.pR;
                            $scope.creditNoteInclusiveTaxItemPrice = value.inclusiveTaxpR;
                            $scope.addToCart(proID, dataSet.selectedUom);
                        }
                    });

                    $scope.cartLength = $scope.inv.cart.length;
                    if($scope.cartLength == 1){
                        $scope.tempQty = $scope.inv.cart[0].qty;
                    }
                    $scope.invTotal = respond.data.invTotalValue;
                    $scope.key = '';
                    $scope.selectInvoFromSearch = false;

            });
         }

         /**
         * Use to load sub product in to the POS credit Note
         */
        $scope.setBatchSerialSubProducts = function(productID, qty, index)
        {
            if($scope.docType == 'CN'){
                $scope.CnSelectedProduct = "[" + PRODUCTS_DETAILS[productID].pC + " - " + PRODUCTS_DETAILS[productID].pN +"]";
                $scope.cartIndex = index;
                if($scope.creditNoteDetails[productID].proType == 's' || $scope.creditNoteDetails[productID].proType == 'bS'){
                    $scope.creditNoteSubProducts = $scope.creditNoteDetails[productID].products;
                    $scope.creditNoteProductType = $scope.creditNoteDetails[productID].proType;
                    $scope.productIDForCN = productID;
                    showCreditNoteSerialModal('show');

                }else if($scope.creditNoteDetails[productID].proType == 'b'){
                    $scope.creditNoteSubProducts = $scope.creditNoteDetails[productID].products;
                    $scope.creditNoteProductType = $scope.creditNoteDetails[productID].proType;
                    $scope.productIDForCN = productID;
                    showCreditNoteSerialModal('show');
                }
                else {
                    if($scope.creditNoteDetails[productID].qty < qty) {
                        p_notification(false, 'Please add low value');
                        $scope.inv.cart[index].qty = $scope.creditNoteDetails[productID].qty;
                    }
                    if($scope.creditNoteDetails[productID].qty == qty && $scope.cartLength == 1 && $scope.inv.discountSelect == 'val'){
                        $scope.inv.discount = $scope.finDiscount;
                    }
                    if($scope.creditNoteDetails[productID].qty == qty && $scope.cartLength == 1 && $scope.showPromo == true && $scope.CNPromotionType == 'val'){
                        $scope.CNPromotion.CNPromotionAmount = $scope.tempCNPromoAmount;
                    }

                }
            }
        }
        //set batch quantity to sub item
        $scope.setBatchQty = function(list,subItemBatchQty)
        {
            $scope.batchData = new Array();
            if(list.tempBatchQty < subItemBatchQty || subItemBatchQty < 0){
                angular.forEach($scope.creditNoteSubProducts,function(value,key){
                    if(list.proBatchID == value.proBatchID){
                        value.returnQty = '';
                    }
                })
                p_notification(false,"Please add valid product quantities.");

            }else{
                if(subItemBatchQty != ''){
                    $scope.batchData[list.proBatchID] = subItemBatchQty;
                }else{
                    $scope.batchData[list.proBatchID] = 0;
                }

            }
        }

        //delete batch-serial items in POS credit note
        $scope.deleteSubProduct = function(cartIndex){
            ($scope.inv.cart).splice(cartIndex, 1);
            showCreditNoteSerialModal('hide');
        }
        // calculate batch-serial item remaining quantity and add to item array
        $scope.saveCrditNoteSubProduct = function(productID)
        {
            var batchSerialItemQty = 0;
            var batchItemQty = 0;
            angular.forEach($scope.creditNoteSubProducts,function(value,key){
                if(value.selected != 0){
                    batchSerialItemQty++;
                }
            });

            angular.forEach($scope.creditNoteDetails[productID].products,function(value,key){
                if($scope.batchData[value.proBatchID]){
                    value.batchQty = $scope.batchData[value.proBatchID];
                    batchItemQty += parseFloat(value.batchQty);
                }else{
                    value.batchQty = 0;
                }

            });
            angular.forEach($scope.inv.cart,function(value,key){
                if(value.pID == productID && $scope.creditNoteDetails[productID].proType !== 'b'){
                    value.qty = batchSerialItemQty;
                }else if(value.pID == productID && $scope.creditNoteDetails[productID].proType == 'b'){
                    value.qty = batchItemQty;
                }
            });

                showCreditNoteSerialModal('hide');


        }
        //validate promotion discount for Credit note
        $scope.$watch("CNPromotion.CNPromotionAmount", function(value){
            if($scope.tempCNPromoAmount < value && $scope.CNPromotionType == 'val'){
                p_notification(false, 'credit note promotion discount can not exceed the inovice promotion discount value.');
                $scope.CNPromotion.CNPromotionAmount = $scope.tempCNPromoAmount;
                return false;
            }else if($scope.cartLength == 1 && $scope.inv.cart[0] && $scope.tempQty == $scope.inv.cart[0].qty && $scope.CNPromotion.CNPromotionAmount != $scope.tempCNPromoAmount && $scope.showPromo && $scope.CNPromotionType == 'val'){
                p_notification(false, 'Promotion discount can not change.');
                $scope.CNPromotion.CNPromotionAmount = $scope.tempCNPromoAmount;
            }
            else{
                $scope.CNPromotion.CNPromotionAmount = value;

            }
        });

        /**
         * use to save credit note
         */
        $scope.savePOSCreditNote = function(dataSet)
        {
            //print recipt
            document.getElementById('printPOSCNPreview').contentWindow.updatePreview(dataSet);
            printIframe("printPOSCNPreview", posPrintoutImageConversionStatus);
            newOrder();
            $scope.selectInvoFromSearch = true;
            $scope.showPromo = false;
            $scope.CNPromotion.CNPromotionAmount = 0;
            $scope.inv.discount = 0;
            hidePaymentModal();

        }

        $scope.saveAndGetReciptInPosCreditNote = function(type)
        {
           $scope.storeCreditNoteDetails(type, $scope.savePOSCreditNote);
        }
        $scope.savePayAndGetReciptInPosCreditNote = function(value)
        {
            $scope.storeCreditNoteDetails('saveAndPay',$scope.doCreditNotePayment);
        }

        $scope.loadCreditNotePayModal = function()
        {
            creditNotePayment();
        }

        /**
         * save credit note payments
         */

        $scope.doCreditNotePayment = function(value)
        {

            var data = {
                genaralData : $scope.inv,
                productDetails : $scope.creditNoteDetails,
                creditNoteTotal : $scope.inv.finTotal,
                type : 'saveAndPay',
                creditNoteID : $scope.creditNoteID,
                cusID : $scope.getCustomerID()
            }

            if($scope.online){
                $http.post(BASE_URL + "/pos-api/updateClosingBalance", data)
                    .success(function(respond){
                if(respond.status){
                    $scope.closingBalance = respond.data.clblance;
                    data.CNCode = respond.data.code;
                    data.userName = respond.data.user;
                    data.totalInvPromDiscount = $scope.getPromotionDiscountForCN();
                    data.totalInvDiscount = $scope.getInvoiceDiscount();
                    creditNotePayment('close');
                    document.getElementById('printPOSCNPreview').contentWindow.updatePreview(data);
                    printIframe("printPOSCNPreview", posPrintoutImageConversionStatus);
                    newOrder();
                    $scope.selectInvoFromSearch = true;
                    $scope.showPromo = false;
                    $scope.CNPromotion.CNPromotionAmount = 0;
                    $scope.inv.discount = 0;
                } else {
                    // show normal POS credit note print recipt
                    data.type = 'save';
                    creditNotePayment('close');
                    p_notification(false, respond.msg);
                    document.getElementById('printPOSCNPreview').contentWindow.updatePreview(data);
                    printIframe("printPOSCNPreview", posPrintoutImageConversionStatus);
                    newOrder();
                    $scope.selectInvoFromSearch = true;
                    $scope.showPromo = false;
                    $scope.CNPromotion.CNPromotionAmount = 0;
                    $scope.inv.discount = 0;
                }
                });
            } else {
                    p_notification(false,'you are offline.cannot create creditNote payments');
            }

        }

        $scope.storeCreditNoteDetails = function (type,callback)
        {

            var data = {
                genaralData : $scope.inv,
                productDetails : $scope.creditNoteDetails,
                invID : $scope.selectedInvoiceID,
                type : type,
                promotionValue : $scope.CNPromotion.CNPromotionAmount,
                promoData : $scope.promData,
                promoType : $scope.CNPromotionType,
                promoRemainingValue : $scope.remPromoDiscVal,
            };

            if($scope.inv.subTotal == 0){
                p_notification(false,'please add a valid quantity.');
            }else {
                if($scope.online){
                    $http.post(BASE_URL + "/pos-api/savePOSCreditNote",data)
                        .success(function(res){
                        if(res.status == false) {
                            p_notification(false, res.msg);
                            return false;
                        }
                        else {
                            data.dateFormat = dateFormat;
                            data.timeFormat = timeFormat;
                            data.CNCode = res.data.creditNoteCode;
                            data.userName = res.data.user;
                            $scope.CNTemplate = true;
                            $scope.creditNoteID = res.data.creditNoteID;
                            data.totalInvPromDiscount = $scope.getPromotionDiscountForCN();
                            data.totalInvDiscount = $scope.getInvoiceDiscount();
                            if(callback){
                                callback(data);
                            }
                        }

                    });
                } else {
                    p_notification(false,'you are offline.cannot create credit note');
                }
            }

        }

        /**
         * load POS credit note details for payments
         */

        $scope.getCreditNoteForPayment = function()
        {
           if($scope.online){
            var data  = {
                cusID : $scope.getCustomerID()
            };
            $http.post(BASE_URL + '/pos-api/getCreditNoteDetails', data)
                .success(function(respond){
                    if(respond.status){
                        $scope.creditNotePaymentFlag = true;
                        $scope.creditNotePaymentDetails = respond.data;
                        showCreditNotePaymentModal();
                    }
                    else
                    {
                        p_notification(false,'No Credit Note exist.');
                    }

                });
            }else{
                p_notification(false,'you are offline.cannot create credit notes');
            }
        }
        /**
        *validate credit note total in POS payment process
        */

        $scope.selectCNPayment = function(creditNoteID)
        {
            $scope.pay.creditNotePayment = 0;
            // if(parseFloat($scope.creditNotePaymentDetails[creditNoteID].creditNoteTotal) > parseFloat($scope.inv.finTotal)){
            //     p_notification(false, 'CreditNote Amount Can not be more than Total amount');
            //     showCreditNotePaymentModal();
            //     return false;
            // }
                hideCreditNotePaymentModal();
                $scope.pay.creditNotePayment = $scope.creditNotePaymentDetails[creditNoteID].creditNoteTotal;
                $scope.payedCreditNoteID = creditNoteID;
                $scope.pay.cash = 0;
        }


        /*Toggle ui view*/
        //todo: refactor using angular watch method
        $scope.toggleUiView = function(e) {
            if (e.currentTarget.checked) { //we are in pos view
                if ($scope.online) {
                    if ($scope.inv.cart.length != 0) {
                        //stop checkbox being checked
                        e.preventDefault();
                        e.stopPropagation();
                        p_notification(false,'Please hold or delete existing invoice');
                        return;
                    } else { //Display cn-view
                        // Before active credit note type, save current pos mode and
                        // change pos mode to "Invoice & Pay" mode.
                        var posMode = $scope.getConfig('pos_modes');
                        $scope.previous_pos_mode = $scope.active_pos_mode;
                        $scope.changeMode(posMode[0]);

                        $scope.togglePosModal = false;
                        $scope.placeholder = 'Search Invoice';
                        $scope.toggleTypeID = 'invoice_search_bar'
                        $scope.loadInvoice();

                        $('#priceLabel').removeClass('hidden');
                        $('#inclusiveTaxPriceLabel').addClass('hidden');
                        $('#totalLabel').removeClass('hidden');
                        $('#inclusiveTaxTotalLabel').addClass('hidden');
                    }
                } else {
                    e.preventDefault();
                    e.stopPropagation();
                    p_notification(false,'You are now offline. cannot create credit Note');
                }
            } else { //we are in cn-view
                if ($scope.inv.cart.length != 0) {
                    //stop checkbox being checked
                    e.preventDefault();
                    e.stopPropagation();
                    p_notification(false,'Please delete or complete existing Credit Note');
                    return;
                } else { // display the pos view
                    // When deactivating Credit Note type, Change pos mode to previouse mode.
                    $scope.changeMode($scope.previous_pos_mode);
                    $scope.creditNoteFlag = false;
                    $scope.key = "";
                    $scope.togglePosModal = true;
                    $scope.cNDiscDisable = false;
                    $scope.placeholder = 'Search Items';
                    $scope.toggleTypeID = 'item_search_bar'
                    $scope.docType = 'pos';

                    if ($scope.inclusive_tax_on_eligible) {
                        $('#priceLabel').addClass('hidden');
                        $('#inclusiveTaxPriceLabel').removeClass('hidden');
                        $('#totalLabel').addClass('hidden');
                        $('#inclusiveTaxTotalLabel').removeClass('hidden');
                    } else {
                        $('#priceLabel').removeClass('hidden');
                        $('#inclusiveTaxPriceLabel').addClass('hidden');
                        $('#totalLabel').removeClass('hidden');
                        $('#inclusiveTaxTotalLabel').addClass('hidden');
                    }
                }
            }
        }

        $scope.checkExistItem = function()
        {
            if($scope.docType = 'CN' && $scope.inv.cart.length != 0){
                p_notification(false,'Please Delete or complete existing Credit Note');
                return false;
                $scope.key = '';
            }
        }

        // validate invoice total discount
        $scope.$watch('inv.discount',function(val){
            if($scope.docType == 'CN'){
                    if(!$scope.cNDiscDisable && val > $scope.finDiscount){
                        p_notification(false, 'Credit Note discount can not be exceeded the invoice discount.');
                        $scope.inv.discount = $scope.finDiscount;
                    }
                    else if($scope.cartLength == 1 && $scope.inv.cart[0] && $scope.tempQty == $scope.inv.cart[0].qty && $scope.inv.discount != $scope.finDiscount && !$scope.selectInvoFromSearch){
                        p_notification(false, 'Can not change discount amount.');
                        $scope.inv.discount = $scope.finDiscount;

                    }
            }

        });

        $scope.getTemporyReferance = function() {
            var tStamp = Date.now() / 1000 | 0;
            return "OPOS" + userID + "_" + tStamp.toString();
        };

        /**
         * Check browser time against server time and gives warning message to user to change date and time
         */
        $scope.checkServerDateTime = function(){
            var data = {timeStamp: Date.now() / 1000 | 0};

            $http.post(BASE_URL + "/pos-api/check-date-time", data)
                .success(function(res){
                    if(!res.status){
                        p_notification(false, res.msg);
                    }
                });
        }
        $scope.checkServerDateTime();

        // Apply loyalty card for the current invoice
        $scope.applyLoyaltyCard = function(updateCustomer) {
            if ($scope.loyalty.card_no || $scope.loyalty.phone_no) {
                var data = {
                    loyaltyCardNo: $scope.loyalty.card_no,
                    phoneNo: $scope.loyalty.phone_no
                };
                $http.post(BASE_URL + "/pos-api/getCustomerLoyaltyCard", data)
                    .success(function(res) {
                        if (!res.status) {
                            p_notification(false, res.msg);
                        } else {
                            $scope.loyalty.id = res.data['lid'];
                            $scope.loyalty.name = res.data['lname'];
                            $scope.loyalty.code = res.data['lcode'];
                            $scope.loyalty.earning = res.data['ladd'];
                            $scope.loyalty.redeem = res.data['lredeem'];
                            $scope.loyalty.min_active_lvl = res.data['lactive_min'];
                            $scope.loyalty.max_active_lvl = res.data['lactive_max'];
                            $scope.loyalty.current_points = res.data['cust_lpoints'];
                            $scope.loyalty.earned_points = 0;
                            $scope.loyalty.redeemed_points = 0;
                            $scope.loyalty.cust_loyalty_id = res.data['cust_lylt_id'];
                            $scope.loyalty.cust_name = res.data['cust_name'];

                            if(updateCustomer){
                                $scope.getCustomer(res.data['cust_id'], false);
                            }

                            hideLoyaltyModal();
                            $scope.calculateLoyaltyPoints();
                        }
                    });
            }
        };

        // Remove loyalty card
        $scope.removeLoyaltyCard = function(){
            delete $scope.loyalty;
            $scope.loyalty = {card_no : null};
        }

        $scope.calculateLoyaltyPoints = function() {
            var total = $scope.inv.finTotal;
            var min_lvl = $scope.loyalty.min_active_lvl;
            var max_lvl = $scope.loyalty.max_active_lvl;
            var earning = $scope.loyalty.earning;
            if (min_lvl == null || total >= min_lvl) {
                if (max_lvl != null && total > max_lvl) {
                    $scope.loyalty.earned_points = max_lvl / earning;
                } else {
                    $scope.loyalty.earned_points = total / earning;
                }
            } else {
                $scope.loyalty.earned_points = 0;
            }
        };

        // Get details of the selected promotion
        $scope.getPromotions = function(promoid) {
            $http.post(BASE_URL + "/api/promotions/getPromotion", {promotionID: promoid})
                    .success(function(res) {
                        if (!res.status) {
                            p_notification(false, res.msg);
                        } else {
                            $scope.promotion = res.data;
//                            p_notification(false, res.msg);
                        }
                    });
        };


        // Get details of the selected promotion
        $scope.getPriceList = function(priceListId) {
            $scope.cancelOrder();
            $scope.priceListSelectID = priceListId;

            $http.post(BASE_URL + "/currencySymbol/getPriceListWithDiscount", {priceListId: priceListId})
                    .success(function(res) {
                        if (!res.status) {
                            p_notification(false, res.msg);
                        } else {
                            console.log(res.data);
                            $scope.priceListItems = res.data;
//                            p_notification(false, res.msg);
                        }
                    });
        };

        $scope.applyGiftCard = function() {
            var selected_cards = _.where(GIFTCARDLIST, {selected: "1"});

            $scope.gift_card = _.map(selected_cards, function(val, key) {
                return val.giftCardId;
            });

            hideGiftCardModal();
        };

        $scope.getAppliedGiftCards = function() {
            var cards = [];

            if ($scope.gift_card != undefined) {
                $scope.gift_card.forEach(function(id) {
                    cards.push({
                        giftCardID: id,
                        giftCardCode: $scope.gift_cards_list[id].giftCardCode,
                        paidAmount: $scope.gift_cards_list[id].giftCardValue
                    });
                });
            }

            return cards;
        };

        $scope.getGiftCardTotal = function() {
            var total = 0;

            if ($scope.gift_card != undefined) {
                $scope.gift_card.forEach(function(id) {
                    total = parseFloat(total) + parseFloat($scope.gift_cards_list[id].giftCardValue);
                });
            }

            return total;
        };

        $scope.validateGiftCard = function(id) {
            var fd = new Date();
            var d = fd.getDate();
            var m = fd.getMonth() + 1;
            var y = fd.getFullYear();

            if (d < 10) {
                d = '0' + d;
            }
            if (m < 10) {
                m = '0' + m;
            }

            var date = new Date(y, m, d);
            if (id) {
                var exp_date = new Date($scope.gift_cards_list[id].giftCardExpireDate);
            } else {
                return false;
            }

            if (date.getTime() <= exp_date.getTime())
                return true;
            else
                return false;
        };

        $scope.updateGiftCards = function(giftCards) {
            if (giftCards && giftCards.length) {
                for (var i in giftCards) {
                    delete GIFTCARDLIST[giftCards[i]];
                    delete $scope.gift_cards_list[giftCards[i]];
                }
            }
        }

        /**
         * Apply promotion for current invoice
         * promoType 1 - Item based
         * promoType 2 - Invoice based
         */
        $scope.applyPromotion = function(pID, price, qty) {
            var discount = 0;
            var promo = $scope.promotion;
            var total = parseFloat($scope.inv.finTotal);
            if (!$scope.promotion) {
                return discount;
            }

            if  ($scope.promotion.promoType == 1) {
                for (var j = 0; j < $scope.promotion.combinations.length; ++j) {
                    if ($scope.promotion.combinations[j]['relatedProductIds'].includes(pID)){
                        if ($scope.promotion.combinations[j]['conditionType'] == "AND") {
                            var trueRules = 0;
                            for (var k = 0; k < $scope.promotion.combinations[j]['ruleSet'].length; ++k) {
                                for (var l = 0; l < $scope.inv.cart.length; ++l) {
                                    if ($scope.promotion.combinations[j]['ruleSet'][k]['productID'] == $scope.inv.cart[l]['pID']) {
                                        var cartQty = Number($scope.inv.cart[l]['qty']);
                                        var ruleQty = Number($scope.promotion.combinations[j]['ruleSet'][k]['quantity']);

                                        switch ($scope.promotion.combinations[j]['ruleSet'][k]['rule']) {
                                            case "equal":
                                              if (cartQty == ruleQty) {
                                                  trueRules = trueRules + 1;
                                              }
                                              break;
                                            case "greater than or equal":
                                              if (cartQty >= ruleQty) {
                                                  trueRules = trueRules + 1;
                                              }
                                              break;
                                            case "less than or equal":
                                              if (cartQty <= ruleQty) {
                                                  trueRules = trueRules + 1;
                                              }
                                              break;
                                            case "greater than":
                                              if (cartQty > ruleQty) {
                                                  trueRules = trueRules + 1;
                                              }
                                              break;
                                            case "less than":
                                              if (cartQty < ruleQty) {
                                                  trueRules = trueRules + 1;
                                              }
                                              break;
                                            case "not equal":
                                              if (cartQty != ruleQty) {
                                                  trueRules = trueRules + 1;
                                              }
                                              break;
                                           
                                            default:
                                              // code...
                                              break;
                                        }
                                    
                                    } 
                                }
                            }

                            if (trueRules == $scope.promotion.combinations[j]['ruleSet'].length) {
                                // discount type is value
                                if ($scope.promotion.combinations[j]['discountType'] == 1) {
                                    var tempDis = qty * $scope.promotion.combinations[j]['discountAmount'];

                                    discount = discount + tempDis;

                                }

                                if ($scope.promotion.combinations[j]['discountType'] == 2) {
                                    var tempDis = ((price * $scope.promotion.combinations[j]['discountAmount']) / 100 ) *  qty;

                                    discount = discount + tempDis;

                                }
                            }
                        } else if ($scope.promotion.combinations[j]['conditionType'] == "OR") {
                            var isAllow = false;
                            for (var m = 0; m < $scope.promotion.combinations[j]['ruleSet'].length; ++m) {
                                if (pID == $scope.promotion.combinations[j]['ruleSet'][m]['productID']) {
                                    let cartQty = Number(qty);
                                    let ruleQty = Number($scope.promotion.combinations[j]['ruleSet'][m]['quantity']);

                                    switch ($scope.promotion.combinations[j]['ruleSet'][m]['rule']) {
                                        case "equal":
                                          if (cartQty == ruleQty) {
                                            isAllow = true;
                                          }
                                          break;
                                        case "greater than or equal":
                                          if (cartQty >= ruleQty) {
                                            isAllow = true;
                                          }
                                          break;
                                        case "less than or equal":
                                          if (cartQty <= ruleQty) {
                                            isAllow = true;
                                          }
                                          break;
                                        case "greater than":
                                          if (cartQty > ruleQty) {
                                            isAllow = true;
                                          }
                                          break;
                                        case "less than":
                                          if (cartQty < ruleQty) {
                                            isAllow = true;
                                          }
                                          break;
                                        case "not equal":
                                          if (cartQty != ruleQty) {
                                            isAllow = true;
                                          }
                                          break;
                                       
                                        default:
                                          // code...
                                          break;
                                    }
                                }
                            }
                            if (isAllow) {
                              //discount type is value
                                if ($scope.promotion.combinations[j]['discountType'] == 1) {
                                    var tempDis = qty * $scope.promotion.combinations[j]['discountAmount'];

                                    discount = discount + tempDis;

                                }

                                if ($scope.promotion.combinations[j]['discountType'] == 2) {
                                    var tempDis = ((price * $scope.promotion.combinations[j]['discountAmount']) / 100 ) *  qty;

                                    discount = discount + tempDis;

                                }
                            }                        

                        }
                    }

                }
                return discount;
            } else if ($scope.promotion.promoType == 3) { 
                for (var j = 0; j < $scope.promotion.combinations.length; ++j) {
                    if ($scope.promotion.combinations[j]['relatedProductIds'].includes(pID)){
                        if ($scope.promotion.combinations[j]['conditionType'] == "AND") {
                            var isAllow = false;
                            var qtyRuleCount = 0;
                            var trueCount = 0;
                            for (var k = 0; k < $scope.promotion.combinations[j]['ruleSet'].length; ++k) {
                                if ($scope.promotion.combinations[j]['ruleSet'][k]['attributeID'] == 0 || $scope.promotion.combinations[j]['ruleSet'][k]['attributeID'] == null) {
                                    qtyRuleCount = qtyRuleCount +1;
                                    let cartQty = Number(qty);
                                    let ruleQty = Number($scope.promotion.combinations[j]['ruleSet'][k]['quantity']);

                                    switch ($scope.promotion.combinations[j]['ruleSet'][k]['rule']) {
                                        case "equal":
                                          if (cartQty == ruleQty) {
                                              trueCount = trueCount + 1;
                                          }
                                          break;
                                        case "greater than or equal":
                                          if (cartQty >= ruleQty) {
                                              trueCount = trueCount + 1;
                                          }
                                          break;
                                        case "less than or equal":
                                          if (cartQty <= ruleQty) {
                                              trueCount = trueCount + 1;
                                          }
                                          break;
                                        case "greater than":
                                          if (cartQty > ruleQty) {
                                              trueCount = trueCount + 1;
                                          }
                                          break;
                                        case "less than":
                                          if (cartQty < ruleQty) {
                                              trueCount = trueCount + 1;
                                          }
                                          break;
                                        case "not equal":
                                          if (cartQty != ruleQty) {
                                              trueCount = trueCount + 1;
                                          }
                                          break;
                                       
                                        default:
                                          // code...
                                          break;
                                    }

                                }
                            }

                            if (qtyRuleCount == trueCount) {
                              isAllow = true;
                            } else {
                              isAllow = false;
                            }

                            if (isAllow) {
                                //discount type is value
                                if ($scope.promotion.combinations[j]['discountType'] == 1) {
                                  var tempDis = qty *  $scope.promotion.combinations[j]['discountAmount'];

                                  discount = discount + tempDis;

                                }

                                if ($scope.promotion.combinations[j]['discountType'] == 2) {
                                  var tempDis = ((price * $scope.promotion.combinations[j]['discountAmount']) / 100 ) * qty;

                                  discount = discount + tempDis;

                                }
                            }


                        }
                        else if ($scope.promotion.combinations[j]['conditionType'] == "OR") {
                            //discount type is value
                            if ($scope.promotion.combinations[j]['discountType'] == 1) {
                              var tempDis = qty * $scope.promotion.combinations[j]['discountAmount'];

                              discount = discount + tempDis;

                            }

                            if ($scope.promotion.combinations[j]['discountType'] == 2) {
                              var tempDis = ((price * $scope.promotion.combinations[j]['discountAmount']) / 100 ) *  qty;

                              discount = discount + tempDis;

                            }
                        }

                    }

                }
                return discount;
            }

            // promotion type is invoce based and discount in value
            if (promo['promoType'] == 2 && promo['discountType'] == 1) {
                if (total >= promo['minValue']) {
                    discount = promo['discountAmount'];
                }
                $scope.promotion.totalDiscount = discount;

            }
            // promotion type is invoce based and discount in precentage
            if (promo['promoType'] == 2 && promo['discountType'] == 2) {
                if (total >= promo['minValue'] && total <= promo['maxValue']) {
                    discount = total * promo['discountAmount'] / 100;
                } else if (total > promo['maxValue']) {
                    discount = promo['maxValue'] * promo['discountAmount'] / 100;
                }
                $scope.promotion.totalDiscount = discount;
            }      
        };

        $scope.getPromotionDiscount = function() {
            if ($scope.promotion) {
                return $scope.promotion.totalDiscount;
            }
            else
                return 0;
        };

        //calculate invoice total discount value for credit note
        $scope.getInvoiceDiscount = function() {
            var disc = parseFloat($scope.inv.discount);
            if (isNaN(disc)) {
                disc = 0;
            }

            if ($scope.inv.discountSelect == 'pre') {
                if($scope.dis_on_eligible){
                    var total = 0;
                    for (var i in $scope.inv.cart) {
                        if($scope.inv.cart[i].dE == "1"){
                            total += parseFloat($scope.inv.cart[i].pTotal);
                        }
                    }
                    disc = total * disc / 100;
                }else{
                    disc = ($scope.inv.subTotal) * disc / 100;
                }
            }
            $scope.inv.discountAmount = disc;
            return disc;
        }

        //calculate promotion discount for credit notes
        $scope.getPromotionDiscountForCN =function()
        {

            var efectedValue = parseFloat($scope.inv.subTotal + $scope.inv.totalTax + $scope.getInvoiceDiscount());

            if (isNaN(efectedValue)) {
                return 0;
            }
            if($scope.CNPromotionType == 'pre'){
                var promDiscValue = efectedValue * ($scope.CNPromotion.CNPromotionAmount/100);
                return parseFloat(promDiscValue);
            }
            else if($scope.CNPromotionType == 'val'){
                return parseFloat($scope.CNPromotion.CNPromotionAmount);
            }else{
                return 0;
            }



        }

        $scope.setPromotionItemList = function(itemName, discount) {

            var has_item = _.find($scope.promotion.itemList, function(item) {
                if (item.p_name == itemName) {
                    item.discount = discount;
                    return true;
                }
            });

            if (!has_item) {
                $scope.promotion.itemList.push({p_name: itemName, discount: discount});
            }

//            if ($scope.promotion.itemList.length == 0) {
//                $scope.promotion.itemList.push({p_name: itemName, discount: discount});
//            } else {
//                $scope.promotion.itemList.forEach(function(item, i) {
//                    if (item.p_name == itemName) {
//                        $scope.promotion.itemList[i].discount = discount;
//                    } else {
//                        $scope.promotion.itemList.push({p_name: itemName, discount: discount});
//                    }
//                });
//            }
        };


        $scope.removePromotions = function() {
            $scope.promotion = {};
        };

        $scope.removePriceList = function() {
            $scope.cancelOrder();
            $scope.priceListSelectID = null;
            $scope.priceListItems = {};
        };

        // Save currently enterd invoice data and make ready the POS for a new order
        $scope.holdOrder = function() {
            if($scope.docType == 'CN'){
                $scope.doCreditNotePayment($scope.inv.finTotal);
            }else{
                if ($scope.inv.cart.length > 0) {
                        if ($scope.holds.length > 0) {
                            if ($scope.holds.indexOf($scope.orderNumber) == -1) {
                                $scope.holds.push($scope.orderNumber);
                                newOrder();
                            } else {
                                $scope.retrieveOrder("Current order.");
                            }
                        } else {
                            $scope.holds.push($scope.orderNumber);
                            newOrder();
                        }
                focusNext(".item_search_bar");
                }
            }



        };

        /**
         *  - Save unfinished order details as 'Current order.' in holds list.
         *  - Retrieve selected order, passed through the parameter.
         *  - If passed no order numer, previous order.
         *
         * @param {type} retrieveOrder
         * @returns {undefined}
         */
        $scope.retrieveOrder = function(retrieveOrder) {
         //call save and pay credit note function when user click  F6
         if($scope.docType == 'CN'){
            $scope.savePOSCreditNote('saveAndPay');
         }else{
               if ($scope.holds.length > 0) {
                var curOdr = $scope.order[$scope.order.length - 1].orderNumber;
                var displayOrder = $scope.orderNumber;
                // if retrieveOrder equals to null then retrieve previos order in hold list
                if (retrieveOrder === null) {
                    var id = $scope.holds.indexOf(displayOrder);
                    if (id !== -1 && id - 1 > -1) {
                        retrieveOrder = $scope.holds[id - 1];
                    } else {
                        if (displayOrder === curOdr) {
                            retrieveOrder = $scope.holds[$scope.holds.length - 1];
                        } else {
                            retrieveOrder = curOdr;
                        }
                    }
                }

                else if (retrieveOrder == "Current order.") {
                    retrieveOrder = curOdr;
                }

                if (retrieveOrder !== curOdr && $scope.holds.indexOf("Current order.") === -1) {
                    $scope.holds.push("Current order.");
                }
                else if (retrieveOrder === curOdr && $scope.holds.indexOf("Current order.") !== -1) {
                    indx = $scope.holds.indexOf("Current order.");
                    $scope.holds.splice(indx, 1);
                }

                $scope.orderNumber = retrieveOrder;
                id = idOfOrder(retrieveOrder);
                $scope.inv = $scope.order[id].inv;
                $scope.pay = $scope.order[id].pay;

                // for(var indx in $scope.inv.cart){
                //     $scope.setQuantity($scope.inv.cart[indx].pID);
                // }

                $scope.sub_product = $scope.order[id].sub_product;
            }
         }
        };

        /**
         * Clear all the data related to currently editing order.
         * @returns {undefined}
         */
        $scope.cancelOrder = function() {

            // Reset subproducts
            for(var i in $scope.inv.cart){
                var pid = $scope.inv.cart[i].pID;

                if((PRODUCTS_DETAILS[pid].bP != 0 || PRODUCTS_DETAILS[pid].sP != 0) && $scope.togglePosModal && $scope.config.active_pos_mode.id != "pay"){
                    if($scope.sub_product[pid])
                        $scope.removeSubProducts(pid);
                }
            }

            if ($scope.holds.indexOf($scope.orderNumber) !== -1) {
                indx = $scope.holds.indexOf($scope.orderNumber);
                $scope.holds.splice(indx, 1);
                $scope.retrieveOrder($scope.order[$scope.order.length - 1].orderNumber);
            } else {

                $scope.inv.cart = new Array();
                $scope.selectInvoFromSearch = true;
                $scope.inv.totalTax = 0;
                $scope.inv.discount = 0;
                focusNext(".item_search_bar");
            }
            delete $scope.inv.invoiceId;
            delete $scope.inv.invoiceCode;
            delete $scope.cNinvoice;

            $scope.showPromo = false;
            $scope.CNPromotion.CNPromotionAmount = 0;
        };
        /**
         * Remove selected sub products for given product id.
         * @param  {int} productID Product id
         */
        $scope.removeSubProducts = function(productID){
            if(!$scope.togglePosModal){
                return;
            }

            for(var indx in $scope.sub_product[productID].products){
                var item = $scope.sub_product[productID].products[indx];
                if(item.PBC != null && item[$scope.orderNumber] && item[$scope.orderNumber].val){
                        $scope.sub_product[productID].products[indx][$scope.orderNumber].val = null ;
                }else{
                    if(item.val && item.orderNo == $scope.orderNumber ){
                        $scope.sub_product[productID].products[indx].val = null ;
                    }
                }
            }
        }

        /**
         * Prepare subproducts array to post
         * @return {array}
         */
        $scope.getSubProductDetails = function() {
            var sp = {};
            angular.forEach($scope.sub_product, function(obj, pid) {
                sp[pid] = {};
                angular.forEach(obj.products, function(val, key) {
                    var qty = 0;
                    var uom = _.findWhere(PRODUCTS_DETAILS[pid].uom, {disp: "1"});
                    var warranty = val.PSW ? val.PSW : val.PBW;
                    var expireDate = val.PSED ? val.PSED : val.PBED;

                    if(val.PBID && val[$scope.orderNumber]){
                        qty = val[$scope.orderNumber].val;
                    }else if(val.orderNo == $scope.orderNumber){
                        qty = val.val;
                    }

                    if (qty > 0) {
                        sp[pid][key] = {
                            batchID: val.PBID,
                            serialID: val.PSID,
                            serialCode: val.PSC,
                            qtyByBase: qty*uom.uC,
                            warranty: warranty,
                            expireDate: expireDate
                        };
                    }
                });
            });
            return sp;
        };

        /**
         * After invoice is raised
         *  - delete invoiced serial items (PRODUCTS_DETAILS[key].sP)
         *  - delete invoiced serial batch items
         *  - Update batch quantity (PRODUCTS_DETAILS[key].bP)
         *  - Update available quantity
         */
        $scope.updateItems = function() {

            // Do not update items if web-socket connection is on
            if($scope.liveUpdate && $scope.online){
                return;
            }

            angular.forEach($scope.sub_product, function(val, key) {
                if (PRODUCTS_DETAILS[key].sP == "1") {

                    // a = sub product
                    // a.val = selected quantity of the subproduct
                    _.each(val.products, function(a) {
                        if (a.val > 0 && a.orderNo == $scope.orderNumber) {
                            delete PRODUCTS_DETAILS[key].serial[a.PSID];
                        }

                        if(a.PBC != null && a.PSID != null && a.val > 0 && a.orderNo == $scope.orderNumber){
                                delete PRODUCTS_DETAILS[key].batchSerial[a.PSID];
                        }
                    });
                }
                if (PRODUCTS_DETAILS[key].bP == "1") {
                    _.each(val.products, function(a) {
                        var qty = a[$scope.orderNumber] ? a[$scope.orderNumber].val : 0;


                        if (qty > 0 && PRODUCTS_DETAILS[key].batch[a.PBID] != undefined) {

                            var item = _.findWhere($scope.inv.cart, {pID: key});

                            var uom = _.findWhere(PRODUCTS_DETAILS[key].uom, {disp: "1"});
                            PRODUCTS_DETAILS[key].batch[a.PBID].PBQ -= qty * item.pUomUc;
                            delete PRODUCTS_DETAILS[key].batch[a.PBID].val;
                        }
                    });
                }
            });

            angular.forEach($scope.inv.cart, function(i){
                PRODUCTS_DETAILS[i.pID].LPQ -= parseFloat(i.qty*i.pUomUc);
                for(var pro in $scope.products){
                    if($scope.products[pro].pID == i.pID){
                        $scope.products[pro].LPQ -= parseFloat(i.qty*i.pUomUc);
                    }
                }
            });
        };

        $scope.showLoginPanel = function() {
            $scope.loginPanel = true;
            append_login_again();
        };

        $scope.authenticate = function(action, url) {
            var data = {
                openingBalance: $scope.openingBalance,
                closingBalance: $scope.closingBalance,
                action: action
            };
            if (action == 'logout') {
                local.set('products_details', null);
                local.set('ts_', null);
                local.set('holdNumber', null);
                local.set('orderDetails', null);
                local.set('orderNumber', null);
            }

            $http.post(BASE_URL + "/pos-api/authenticate", data)
                    .success(function(res) {
                        if (res.status) {
                            if (res.data.logout) {
                                window.location.href = url;
                            } else {
                                remove_login_modal();
                                $scope.login = true;
                                $scope.closingBalance = res.data.ob;
                                focusNext();
                            }
                        } else {
                            p_notification(false, res.msg);
                        }
                    });
            return false;
        };

        var barcodeInputTimer;
        $scope.formData = {};

        $scope.getIdOfBarcode = function(val) {
            $scope.barcode = val;
            clearTimeout(barcodeInputTimer);
            // fix for below scenario
            // change event gets triggers everytime a char is entered
            // when scanning code through device, if barcode has 16 chars = function gets called 16 times
            barcodeInputTimer = setTimeout(function() {

                $scope.$apply(function() {
                    $scope.addToCartFromBarcode();
                    $scope.barcode = null;
                });
            }, 100);
        };

        $scope.addToCartFromBarcode = function() {
            if ($scope.barcode == null) {
                return false;
            }

            // var item = _.where(PRODUCTS_DETAILS, {pBC: $scope.barcode});
            var item = _.filter(PRODUCTS_DETAILS, function(product){
                var dat = _.where(product.barcodes, {barCode: $scope.barcode});
                if (dat.length > 0) {
                    return dat;
                }
            });

            if (item.length > 0) {
                $scope.selectProduct(item[0].pID);
                $scope.barcode = null;
            }

            var serialItem = _.where($scope.serial, {serial: $scope.barcode})
            if(serialItem.length > 0){
                $scope.selectProduct(serialItem[0].id);
                $scope.barcode = null;
            }
        };

        $scope.validateOrder = function() {
            //call to credit note save function when user click F7
            if($scope.docType == 'CN'){
                $scope.savePOSCreditNote('save');
            }
            else{
                $scope.key = null;
                $scope.pay.cash = null;
                $scope.pay.cardPayment = null;
                $scope.pay.redeem_value = null;
                if ($scope.inv.discount == '') {
                    $scope.inv.discount = '0.00';
                }
                if ($scope.inv.cart.length > 0) {
                    var state = true;
                    for (var i in $scope.inv.cart) {

                        if ($scope.inv.cart[i].qty <= 0) {
                            focusNext(".qty:eq(" + i + ")");
                            p_notification(false, "Please enter the quantity.");
                            state = false;
                            break;
                        }
                        if ($scope.inv.cart[i].pR <= 0) {
                            focusNext(".unitPrice:eq(" + i + ")");
                            p_notification(false, "Please enter the unit price.");
                            state = false;
                            break;
                        }
                        else if($scope.override_price == 'deny'){
                            var price = parseFloat($scope.inv.cart[i].pR);
                            var selectedUomUc = parseFloat($scope.inv.cart[i].pUomUc);
                            var displayUomUc = parseFloat(_.findWhere(PRODUCTS_DETAILS[$scope.inv.cart[i].pID].uom, {disp:"1"}).uC);

                            var actualPrice = price / selectedUomUc * displayUomUc;
                            if ($scope.inclusive_tax_on_eligible == false) {
                                if(actualPrice < $scope.getDisplayPrice($scope.inv.cart[i].pID)){
                                    focusNext(".unitPrice:eq(" + i + ")");
                                    p_notification(false, "Unit Price cannot be less than default price.");
                                    state = false;
                                    break;
                                }
                            }                     
                        }
                        if (($scope.inv.cart[i].sign == "%" && $scope.inv.cart[i].dAmo > 100) || ($scope.inv.cart[i].sign != "%" && parseFloat($scope.inv.cart[i].dAmo) > parseFloat($scope.inv.cart[i].pR))) {
                            focusNext(".unitDiscount:eq(" + i + ")");
                            p_notification(false, "Discount value cannot be more than unit price.");
                            state = false;
                            break;
                        }
                        

                        if (!$scope.isValidSubproducts()){
                            p_notification(false, "No enough subproducts.");
                            state = false;
                            break;                            
                        }
                    }
                    if($scope.inv.finTotal <= 0 && state){
                        p_notification(false, "Total value must be possitive.");
                        state = false;
                    }

                    if (state) {
                        if ($scope.config.active_pos_mode.id != 'inv') {
                            if($scope.config.active_pos_mode.id == 'pay'){
                                $scope.validatePaymentInstance($scope.inv.invoiceId);
                            }
                            else{
                                showModal();
                            }

                        }else{
                            $scope.doPayment();
                        }
                    }
                } else {
                    p_notification(false, "Please enter at least one Item.");
                }
            }

        };

        $scope.isValidSubproducts = function(){
            var status = true;

            for(var pid in $scope.sub_product) {
                for(var indx in $scope.sub_product[pid].products) {
                    var itm = $scope.sub_product[pid].products[indx];
                    var uom = _.findWhere(PRODUCTS_DETAILS[pid].uom, {disp: "1"});

                    if(itm.PBID && itm[$scope.orderNumber]){
                        if (itm[$scope.orderNumber].val * uom.uC > itm.PBQ) {
                            return false;
                        }
                    }
                }
            }
            return status;
        }

        $scope.loadFromLocalData = function() {
            // If browser support local storage,
            // On page reload check if there any data saved on local storage about :
            //   - Un-synced order details.
            //   - Hold order detils.
            //
            // If there are un-synced orders
            //   - check internet connection and start syncing.
            //
            // If tere are hold orders, update the holdOders array
            //

        };

        $scope.isOnline = function(){
           $scope.online = navigator.onLine;
            return $scope.online;
        }

        /**
         * this function will save,
         *  - unsaved order details
         *  - currently holded orders
         *  - and current order number
         * on local storage.
         */
        $scope.updateLocalStorage = function() {
            local.set('orders', unSaved);
            local.set('orderNumber', $scope.orderNumber);
            local.set('holds', $scope.holds);
        };

        var checker = false;
        var bulkSent = false;
        $scope.syncOrders = function() {
            if ($scope.online && (checker === false)) {
                checker = setInterval(function() {
                    $http.get(BASE_URL + '/pos-api/create')
                            .success(function() {
                                clearInterval(checker);
                                checker = false;
                                $scope.online = true;
                                $scope.syncOrders();
                            })
                            .error(function(){
                                $scope.online = false;
                            });
                }, 5000);
            }
            else if ($scope.online) {
                var bulk = local.get('orders');
                if (!bulk || bulkSent) {
                    return false;
                }
                bulkSent = true;
                $http.post(BASE_URL + '/pos-api/create/bulk', bulk)
                        .success(function(res) {
                            for (var i in res.data) {
                                delete unSaved[i];
                            }
                            $scope.updateLocalStorage();
                            bulkSent = false;
                        })
                        .error(function(res, status) {
                            // if(status == 200){
                            // }
                                $scope.online = false;
                                bulkSent = false;
                                $scope.syncOrders();
                        });
            }
        };


        window.addEventListener("focus", function(event) {
            $scope.syncOrders();
        });
        $scope.syncOrders();

        $scope.validate = function(element, index) {
            number = function(val) {
                if (isNaN(val))
                    return false;
                else
                    return true;
            };
            switch (element) {
                case 'price':
                    $scope.inv.cart[index].pR = number($scope.inv.cart[index].pR) ? $scope.inv.cart[index].pR : '0.00';
                    break;
                case 'item_discount':
                    $scope.inv.cart[index].dAmo = number($scope.inv.cart[index].dAmo) ? $scope.inv.cart[index].dAmo : '0.00';
                    $scope.inv.cart[index].dAmo = $scope.inv.cart[index].dAmo > 0 ? $scope.inv.cart[index].dAmo : '0.00';
                    validateDiscount(index);
                    break;
                case 'qty':
                    if ($scope.inv.cart[index].qty < 0) {
                        $scope.inv.cart[index].qty = 1;
                    }
                    $scope.inv.cart[index].qty = number($scope.inv.cart[index].qty) ? $scope.inv.cart[index].qty : 1;

                    var decimalPlaces = PRODUCTS_DETAILS[$scope.inv.cart[index].pID].uom[$scope.inv.cart[index].uomOptions.value.id].uDP;
                    if ($scope.countDecimals($scope.inv.cart[index].qty) > decimalPlaces) {
                        $scope.inv.cart[index].qty = parseFloat($scope.inv.cart[index].qty).toFixed(decimalPlaces);
                    }
                    break;
                case 'discount':
                    validateDisPerc();
                    $scope.inv.discount = number($scope.inv.discount) ? $scope.inv.discount : '0.00';
                    break;
                case 'openingBalance':
                    $scope.openingBalance = number($scope.openingBalance) ? $scope.openingBalance : null;
                    break;
                case 'cash':
                    $scope.pay.cash = number($scope.pay.cash) ? $scope.pay.cash : null;
                    break;
                case 'cardPayment':
                    $scope.pay.cardPayment = number($scope.pay.cardPayment) ? $scope.pay.cardPayment : null;
                    break;
                case 'loyaltyRedeem':
                    $scope.pay.redeem_value = number($scope.pay.redeem_value) ? $scope.pay.redeem_value : 0.00;
                    break;
                case 'creditNote':
                    $scope.pay.creditNotePayment = number($scope.pay.creditNotePayment) ? $scope.pay.creditNotePayment : 0.00;
            }

            if(!$scope.customer.customerLoyaltyCode){
                $scope.removeLoyaltyCard();
            }else if($scope.customer.customerLoyaltyCode){
                $scope.loyalty.card_no = $scope.customer.customerLoyaltyCode;
                // false - do not get customer
                $scope.applyLoyaltyCard(false);
            }
        };

        $scope.countDecimals = function (value) { 
            if ((value % 1) != 0) 
                return value.toString().split(".")[1].length;  
            return 0;
        };

        $scope.isHold = function(productID, serialID){
            var inArray = false;

            var curOdr = $scope.orderNumber;

            for(var i in $scope.holds){
                if(!$scope.order[i].sub_product[productID]){
                    return inArray;
                }

                var serialProduct = $scope.order[i].sub_product[productID].products[serialID];
                if(serialProduct.val && serialProduct.val == 1){

                    if(curOdr == serialProduct.orderNo){
                        continue;
                    }

                    inArray = true;
                    break;
                }

            }

            return inArray;
        }

        /**
         * Delete item form order list (cart)
         * @param  {int} id Index of selected item in inv.cart array
         */
        $scope.deleteItem = function(id) {
            if(PRODUCTS_DETAILS[$scope.inv.cart[id].pID].bP != 0 || PRODUCTS_DETAILS[$scope.inv.cart[id].pID].sP != 0){
                $scope.removeSubProducts($scope.inv.cart[id].pID);
            }

            $scope.$apply(function() {
                ($scope.inv.cart).splice(id, 1);
            });
            if($scope.inv.cart.length == 0){
                $scope.selectInvoFromSearch = true;
            }
            focusNext('.item_search_bar');
            focusNext();
        };

        $scope.deleteProduct = function(index){
            var msg = 'Are you sure you want to delete this item ?';
            var msgHeader = 'Item delete confirmation';

            deleteProductConfermation(msg, msgHeader, index);
        }

        /**
        * validate payments when user try to pay
        **/

        $scope.validatePaymentInstance =function(invoiceID)
        {
            $http.post(BASE_URL + '/pos-api/validate-unpayed-invoice-save',{invoiceID : invoiceID})
                .success(function(respond){
                    if(!respond.status){
                        p_notification(false,respond.msg);
                    }
                    else{
                        showModal();
                    }
                });
        }

        $scope.showItemDetailsBox = function(id){
            showItemDetails(id);
        }

         /**
         *
         * @param {String} productID
         * @param {Float} price
         * @returns {_L4.getItemDiscount.dis}
         */
        function getItemDiscount(productID, price, cartIndex) {
            var dis = {val: "", type: "", amount: "", sign: ""};
            var uom = new Array();
            var productUom = {};


            if (cartIndex !== null && $scope.inv.cart[cartIndex].uomOptions !== undefined) {
                var selectedUom = $scope.inv.cart[cartIndex].uomOptions.value.id;
                productUom = PRODUCTS_DETAILS[productID].uom[selectedUom];
            } else {

                angular.forEach(PRODUCTS_DETAILS[productID].uom, function(val, key) {
                    this.push({uA: val.uA, id: key, uC: val.uC, disp: val.disp});
                }, uom);

                if (uom[0] === undefined) {
                    uom[0] = {uA: "", id: ""};
                } else {
                    for (var i in uom) {
                        if (uom[i].uC == 1) {
                            baseUom = uom[i];
                        }
                        if (uom[i].disp == 1) {
                            displayUom = uom[i];
                        }
                    }
                }

                productUom = displayUom != undefined ? displayUom : baseUom;
            }

            if (PRODUCTS_DETAILS[productID].dE != null) {
                if (PRODUCTS_DETAILS[productID].dP != null) {
                    var dAmount = $scope.inv.cart[cartIndex] ? $scope.inv.cart[cartIndex].dAmo : PRODUCTS_DETAILS[productID].dP;
                    dis.type = "precentage";
                    dis.val =  dAmount;
                    dis.amount = ((dAmount * price) / 100);
                    dis.sign = "%";
                }
                else if (PRODUCTS_DETAILS[productID].dV != null) {
                    var dAmount = $scope.inv.cart[cartIndex] ? $scope.inv.cart[cartIndex].dAmo : PRODUCTS_DETAILS[productID].dV;
                    dis.type = "value";
                    dis.val = dAmount * 1;
                    dis.amount = dAmount * 1;
                    dis.sign = $scope.currency;
                }
            }
            return dis;
        }

        function newOrder() {
            $scope.orderNumber++;
            $scope.order.push({
                inv: {},
                pay: {},
                customer: {id: 0},
                sub_product: {},
                loyalty: {},
                gift_card: new Array(),
                promotion: {},
                orderNumber: $scope.orderNumber
            });

            var curOdr = $scope.order.length - 1;
            $scope.inv = $scope.order[curOdr].inv;
            $scope.pay = $scope.order[curOdr].pay;
            $scope.customer = $scope.order[curOdr].customer;
            $scope.loyalty = $scope.order[curOdr].loyalty;
            $scope.gift_card = $scope.order[curOdr].gift_card;
            $scope.promotion = $scope.order[curOdr].promotion;
            $scope.sub_product = $scope.order[curOdr].sub_product;
            $scope.inv.cart = new Array();
            $scope.inv.totalTax = 0;
            $scope.inv.discount = 0;
            $scope.inv.discountSelect = 'pre';
            //            $scope.pay.payType = $scope.payTypes[0];
        }


        function idOfOrder(order) {
            for (var key in $scope.order) {
                if ($scope.order[key].orderNumber === order)
                    return key;
            }
        }
        //this function use to validate discount percentage and discount value.
        function validateDisPerc()
        {
            if ($scope.inv.discountSelect == 'pre' && $scope.inv.discount > 100) {
                //use to validate discount percentage
                p_notification(false, 'Discount percentage cannot be more than 100%.');
                $scope.inv.discount = '0.00';
                return false;
            } else if ($scope.inv.discountSelect == 'val'&& $scope.inv.discount>($scope.inv.totalBeforeDiscount)) {
                 //use to validate discount fixed value
                p_notification(false,'Discount value cannot be more than total value.');
                $scope.inv.discount = '0.00';
                return false;
            }
            else if($scope.inv.discount < 0){
                p_notification(false,'Discount value cannot be minus value.');
                $scope.inv.discount = '0.00';
                return false;
            }

            return true;
        }

        //this function use to validate the item discount
        function validateDiscount(index)
        {
            var productID = $scope.inv.cart[index].pID;
            var defaultProductData = PRODUCTS_DETAILS[productID];
            if (defaultProductData.dE == 1) {
                if ($scope.inv.cart[index].sign == '%') {
                    if (toFloat(defaultProductData.dP) != 0 && toFloat($scope.inv.cart[index].dAmo) > toFloat(defaultProductData.dP)) {
                        p_notification(false, 'Discount percentage can not more than default dicount');
                        $scope.inv.cart[index].dAmo = defaultProductData.dP;
                        return false;
                    } else if (toFloat(defaultProductData.dP) == 0) {
                        if (toFloat($scope.inv.cart[index].dAmo) > 100) {
                            p_notification(false, 'Discount percentage cannot be more than 100%');
                            $scope.inv.cart[index].dAmo = 100;
                            return false;
                        }
                    }
                } else {
                    if (toFloat(defaultProductData.dV) !=0 && toFloat($scope.inv.cart[index].dAmo) > toFloat(defaultProductData.dV)) {
                        p_notification(false, 'Discount value cannot more than default discount value');
                        $scope.inv.cart[index].dAmo = defaultProductData.dV;
                        return false;
                    } else if (toFloat(defaultProductData.dV) == 0) {
                        if (toFloat($scope.inv.cart[index].dAmo) > toFloat($scope.inv.cart[index].pR)) {
                            p_notification(false, 'Discount value cannot more than unit price');
                            $scope.inv.cart[index].dAmo = $scope.inv.cart[index].pR;
                            return false;
                        }
                    }
                }
            } 
            return true;
        }

        $scope.prepareProductForm = function(){
            $scope.newItem = {};
            $scope.newItem.productSalesAccountID = CUSTOMER_ACCOUNTS.productSales;
            $scope.newItem.productInventoryAccountID = CUSTOMER_ACCOUNTS.productInventory;
            $scope.newItem.productCOGSAccountID = CUSTOMER_ACCOUNTS.productCOGSA;
            $scope.newItem.productAdjusmentAccountID = CUSTOMER_ACCOUNTS.productAdjusment;
            var defaultUomID = $('#defaultUomID').val();
            $("[name='uomID[]']").val(defaultUomID);
            $("#categoryParentID").val(1);
        }

    }]);

app.filter('search', function() {
    return function(text, phrase, $scope) {
        if (phrase !== undefined && phrase !== null) {
            phrase = phrase.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');
        }else{
            phrase = "";
        }

        var output = _.filter(text, function(item) {

            item.dispName = item.pC;
            if (item.auto_add_id) {
                delete item.auto_add_id;
            }

            var pc = item.pC.toLowerCase();
            var pn = item.pN.toLowerCase();
            var ph = phrase.toLowerCase();

            if (pc.match(new RegExp('(' + ph + ')', 'gi')) ||
                    pn.match(new RegExp('(' + ph + ')', 'gi'))) {

                var pn_pos = pn.indexOf(ph);
                item.sarch_occurance = pn_pos;

                if (pn_pos === -1) {
                    var pc_pos = pc.indexOf(ph);
                    item.sarch_occurance = pc_pos;
                }

                if(item.sarch_occurance !== -1){
                    item.sarch_occurance += 1;
                    if(pc == ph || pn == ph){
                        item.sarch_occurance = 0;
                    }
                }
                return true;
            }

            if (PRODUCTS_DETAILS[item.pID]) {
                item.serial = PRODUCTS_DETAILS[item.pID].serial;
                for (var srl in item.serial) {
                    if (item.serial[srl].PSC && item.serial[srl].PSC.match(new RegExp('(' + ph + ')', 'gi'))) {
                        item.dispName = item.serial[srl].PSC;
                        item.auto_add_id = srl;

                        return true;
                    }
                }
            }
        });
        return output;
    };
});
app.filter('search_sub_product', function() {
    return function(text, phrase) {
        return _.filter(text, function(item) {
            if (!phrase) {
                return item;
            }
            else {
                if ((item.PBC !== null && item.PBC.match(new RegExp('(' + phrase + ')', 'gi'))) || (item.PSID !== null && item.PSC.match(new RegExp('(' + phrase + ')', 'gi')))) {
                    return true;
                }
            }
        });
    };
});
app.filter('search_gift_card', function() {
    return function(text, phrase) {
        return _.filter(text, function(card) {
            if (!phrase) {
                return card;
            }
            else {
                if ((card.giftCardCode !== null && card.giftCardCode.match(new RegExp('(' + phrase + ')', 'gi')))) {
                    return true;
                }
            }
        });
    };
});
app.filter('search_credit_note', function() {
    return function(text, search_key) {
        return _.filter(text, function(cn) {
            if (!search_key) {
                return cn;
            }
            else {
                if ((cn.creditNoteCode !== null && cn.creditNoteCode.match(new RegExp('(' + search_key + ')', 'gi')))) {
                    return true;
                }
            }
        });
    };
});

app.filter('highlight', function() {
    return function(html, key) {
        return html.replace(new RegExp('(' + key + ')', 'gi'), '<b class="highlighted">$1</b>');
    };
});
app.directive('selectrow', function() {
    return function(scope, element, attr) {
        $('input', element).blur(function() {
            element.removeClass('s');
        });
        $('input', element).focus(function() {
            element.addClass('s');
        });
    };
});
app.directive('hotkey', function() {
    return function(scope, element, attr) {

        $(document).on('keydown', function(e) {
            if (e.ctrlKey) {
                switch (e.keyCode) {
                    // case 68:
                    //     e.preventDefault();
                    //     $('#discount').focus().select(); // ctrl+d
                    //     break;
                    case 83:
                        e.preventDefault();
                        $('.item_search_bar').focus().select(); // ctrl+s
                        break;
                    case 66:
                        e.preventDefault();
                        $('#barCode').focus().select(); // ctrl+b
                        break;
                    // default:
                        // e.preventDefault();
                }
            }
            else {
                switch (e.keyCode) {
                    case 113:   // F2 - Payment
                        e.preventDefault();
                        if ($('#paymentModal:visible').length) {
                            $('#print_receipt').click();
                        }
                        break;
                    case 114:   // F3 - Cancel
                        e.preventDefault();
                        scope.$apply(scope.cancelOrder());
                        break;
                    case 115:   // F4 - Hold
                        e.preventDefault();
                        scope.$apply(scope.holdOrder());
                        break;
                    case 116:   // F5 - Open loyalty card modal\Disale refreshing
                        e.preventDefault();
                        if ($('#paymentModal:visible').length) {
                            $('#add_loyalty_card').click();
                        }
                        break;
                    case 117:   // F6 - Retrieve
                        e.preventDefault();
                        scope.$apply(scope.retrieveOrder(null));
                        break;
                    case 118:   // F7 - pay
                        e.preventDefault();
                        scope.$apply(scope.validateOrder());
                        break;
                    case 119:   // F8 - Promotions
                        e.preventDefault();
                        $('#promotion_list_btn').click();
                        //                        scope.$apply(scope.validateOrder());
                        break;
                    case 120:   // F9 - Gift Cards
                        e.preventDefault();
                        if ($('#paymentModal:visible').length) {
                            $('#add_gift_card').click();
                        }
                        //                        scope.$apply(scope.validateOrder());
                        break;
                    case 121:   // F10 - Invoice Discount
                        e.preventDefault();
                        $('#discount').focus().select();
                        break;
                    case 122:   // F11 - Toggle Discount
                        e.preventDefault();
                        if ($("#discount_select").val() == "val") {
                            $("#discount_select").val("pre").change();
                        } else {
                            $("#discount_select").val("val").change();
                        }
                        $('#discount').focus().select();

                        break;
                    case 9:   // Tab
                        e.preventDefault();
                        focusNext();
                        break;
                   case 13:   // F10 - Invoice Discount
                        e.preventDefault();
                        $('.item_search_bar').focus().select(); //enter button
                        break;

                }
            }
        });
        $('#loyaltyModal').on('keypress', function(e) {
            if (e.keyCode == 13) {
                $('#apply_card').click();
            }
        });
        $("#posLogout").on('click', function() {
            scope.$apply(scope.showLoginPanel());
        });
        
        var barcode = [];
        var barcodeMinLength = 1;
        var barcodeTimer;
        var barcodeTimerLastInp;
        $(function() {
            // $(document).on('keydown', function(e) {
            $(document).on('keydown', function(e) {
                var reg = /^[a-z0-9\-]+$/i;
                if (e.keyCode == 189 || e.keyCode == 109) {
                    e.keyCode = 45;
                }

                var enteredChar = String.fromCharCode(e.keyCode);
                if (reg.test(enteredChar)) {
                    barcode.push(enteredChar);
                    barcodeTimerLastInp = $(e.target);
                    clearTimeout(barcodeTimer);
                    barcodeTimer = setTimeout(function() {
                        if (barcode.length > barcodeMinLength) {
                            var scope = angular.element(document.getElementById('barCode')).scope();
                            scope.$apply(function() {
                                // scope.barcode = barcode.join('');
                                // scope.addToCartFromBarcode();
                                var val = barcode.join('');
                                scope.getIdOfBarcode(val);
                            });
                            if (barcodeTimerLastInp.is('input')) {
                                barcodeTimerLastInp.val('').change();
                            }
                        }

                        // clear vars
                        barcode = [];
                        barcodeTimerLastInp = null;
                    }, 50);
                }
            });
        });
        $('.location').on('click', function(e) {
            e.preventDefault();
            scope.$apply(scope.authenticate('logout', $(this).attr('href')));
        });
    };
});
app.directive('location', function() {
    return function(scope, element, attr) {
    };
});

var closeAuthorisationModal = function(){
    $("#auth_modal").modal('hide');
}

var showItemDetails = function(proId){

    if(!PRODUCTS_DETAILS[proId]){
        return;
    }

    if (!$('#itemDetails:visible').length) {
        $('#itemDetails').removeClass('hide');
    }
    $('#item_name').text(PRODUCTS_DETAILS[proId].pC + " - " + PRODUCTS_DETAILS[proId].pN);
    $('#rack_id').text(PRODUCTS_DETAILS[proId].rackID ? PRODUCTS_DETAILS[proId].rackID : 'No rack id');
    $('#detail').text(PRODUCTS_DETAILS[proId].dtl ? PRODUCTS_DETAILS[proId].dtl : 'No information');
}

var hideItemDetails = function(){
    $('#itemDetails').addClass('hide');
}

var closeCustomerModal = function(){
    $("#customerCreate").modal('hide');
}

var selectFirstInvoice = function(){
    setTimeout(function(){
        $('#inv-list tbody tr:first').addClass("s");
    },0);
    focusNext("#invoice_selector");
}

var showCreditNote = function()
{
    $("#creditNote").modal('show');
}
var showCreditNotePaymentModal = function()

{
    $('#creditNoteListModal').modal('show');
}
var hideCreditNotePaymentModal = function()
{
    $('#creditNoteListModal').modal('hide');
}

var showCreditNoteSerialModal = function(value)
{
    if(value == 'show'){
    $('#credit_note_product_selector').modal('show');
    }
    else{
     $('#credit_note_product_selector').modal('hide');
    }
}

var creditNotePayment = function(value)
{
    if(value == 'close'){
        $('#creditNotePaymentModal').modal('hide');
    }else{
        $('#creditNotePaymentModal').modal('show');
     }
}

var showModal = function() {
    $("#paymentModal").modal('show');
    $('#paymentModal').on('shown.bs.modal show.bs.modal', function(e) {
        focusNext('#cash');
    });
    $('#paymentModal').on('hidden.bs.modal show.bs.modal', function(e) {
        focusNext('.item_search_bar');
    });
};
var hidePaymentModal = function() {
    $("#paymentModal").modal('hide');
};
var hideLoyaltyModal = function() {
    $("#loyaltyModal").modal('hide');
};
var hideGiftCardModal = function() {
    $("#giftModal").modal('hide');
};
var show_selector_panel = function() {
    $('#product_selector').modal('show');
    $('#product_selector').on('shown.bs.modal', function() {
        $('#product_selector table tbody tr').removeClass('s');
        $('#product_selector table tbody tr:first').addClass('s');
        $('#product_selector table tbody tr:first').find('input:first').select();
    });
};
var closeItemSelector = function() {
    $("#product_selector").modal('hide');
};
function focusNext(ele) {
    if (ele) {
        $(ele).focus().select();
    } else {
        var focus = $(':focus').attr('id');
        switch (focus) {
            case 'item_search_bar':
                setTimeout(function() {
                    if ($('.qty:last').attr('disabled') === 'disabled') {
                        $('.unitPrice:last').focus().select();
                    } else {
                        $('.qty:last').focus().select();
                    }
                }, 5);
                break;
            case 'product_selector':
                var qty_focus = setTimeout(function() {
                    $(".unitPrice").focus().select();
                    clearTimeout(qty_focus);
                }, 500);
                break;
            case 'qty':
                $slec = $('#itemList').find('.s');
                if ($slec.length > 0) {
                    $slec = $('#itemList').find('.s .uom').focus();
                } else {
                    $(".uom").focus();
                }
                break;
            case 'uom':
                $slec = $('#itemList').find('.s');
                if ($slec.length > 0) {
                    $slec = $('#itemList').find('.s .unitPrice').focus().select();
                } else {
                    $(".unitPrice").focus().select();
                }
                break;
            case 'unitPrice':
                $slec = $('#itemList').find('.s');
                if ($slec.length > 0) {
                    $slec = $('#itemList').find('.s .unitDiscount').focus().select();
                } else {
                    $(".unitDiscount").focus().select();
                }
                break;
            case 'unitDiscount':
                $('.item_search_bar').focus();
                $('#itemList').find('.s').removeClass('s');
                break;
            default:
                var target = $(':focus');
                if (target.parent(".popup").length) {
                    if (target.next(':input').attr('name') !== undefined) {
                        target.next(':input').focus();
                    } else {
                        target.closest(".popup").children(':input').first().focus();
                    }
                }
                if (target.closest(".modal").length) {
                    var mdl = target.closest(".modal").attr('id');
                    var mdl_inputs = $('#' + mdl + ' input:visible, #' + mdl + ' select:visible');
                    var nextInput = mdl_inputs.get(mdl_inputs.index(target) + 1);
                    if (nextInput) {
                        nextInput.focus();
                    } else {
                        mdl_inputs.get(0).focus();
                    }
                }
                break;
        }
    }
}


// Prepare requet as post
app.config(function($httpProvider) {
    $httpProvider.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded';
    $httpProvider.defaults.transformRequest = function(data) {
        if (data === undefined) {
            return data;
        }
        return $.param(data);
    };
});


$(function() {
    $('#product_selector, .item_search_bar, #itemList').on('keydown', function(e) {
        var options = {};

        if ($(this).attr('id') === 'item_search_bar') {
            $table = $('#suggestions-list');
            options.enter = clickSelectedItem;
            options.default = selectDefault;
            $cnvalue = false;

            setTimeout(function(){
                var proId = $table.find('.s').attr('id');
                if(!$(e.target).val()){
                    $('#itemDetails').addClass('hide');
                }else{
                    showItemDetails(proId);
                }
            }, 5);
            // options.tabspace = focusNext();
        }
        else if($(this).attr('id') === 'invoice_selector'){
            $table = $('#inv-list');
            options.enter = clickSelectedItem;
            options.default = selectDefault;
            $cnvalue = false;
        }
        else if ($(this).attr('id') === 'product_selector') {
            $table = $('#item-selector-tbl');
            options.space = selectItem;
            $cnvalue = false;
            options.enter = function() {
                $('#save_sub_product').click();
            };
            options.move = function($table) {
                $table.find('.s input:first').focus().select();
            };
            options.tabspace = function() {
                $('#selector_search').focus().select();
            };
        }
        else if ($(this).attr('id') === 'itemList') {
            $table = $('#itemList');
            options.move = function($table) {
                if ($table.find('.s input:first').attr('disabled') === 'disabled') {
                    $table.find('.s .unitPrice').focus().select();
                } else {
                    $table.find('.s input:first').focus().select();
                }
            };
            options.delete = deleteFromItemList;
            options.f2 = editSubProducts;
            $cnvalue = false;
        }
        else if($(this).attr('id') === 'invoice_search_bar'){
            $table = $('#suggestions-list');
            options.default = selectDefault;
            options.enter = clickSelectedItem;
            $cnvalue = true;
            $('#itemDetails').addClass('hide');
            addInv;
        }

        switch (e.keyCode) {
            case 38: // Up key
                moveUpOnList($table);
                if (options.move)
                    options.move($table, e);
                e.preventDefault();
                break;
            case 40: // Down key
                moveDownOnList($table);
                if (options.move)
                    options.move($table, e);
                e.preventDefault();
                break;
            case 13: // Enter key
                if (options.enter)
                    options.enter($table);
                e.preventDefault();
                break;
            case 32: // Space
                if (options.space)
                    options.space($table, e);
                break;
            case 46: // Delete
                if (options.delete)
                    options.delete($table, e);
                e.preventDefault();
                break;
            case 9: // Tab
                if (options.tabspace)
                    options.tabspace($table);
                break;
            case 113: // F2
                if (options.f2)
                    options.f2($table, e);
                break;
            default: // Other than spesific key
                if (options.default)
                    options.default($table);
                break;
        }
    });

    function moveDownOnList($table) {
        if($cnvalue){
            var $lines = $table.find('tbody tr.cNSerchList');
        }else{
            var $lines = $table.find('tbody tr');
        }
        var $currLine = $lines.filter('.s');
        if (!$currLine.length) {

            $lines.first().addClass('s');
        } else {
            if ($currLine.next('tr').length) {
                $lines.removeClass('s');
                $currLine.next('tr').addClass('s');
            } else { //focus to the first tr
                $lines.removeClass('s'); //remove current selection
                $lines.first().addClass('s');
            }
        }
    }

    function moveUpOnList($table) {
        if($cnvalue){
            var $lines = $table.find('tbody tr.cNSerchList');
        }else{
            var $lines = $table.find('tbody tr');
        }
        var $currLine = $lines.filter('.s');
        if (!$currLine.length) {
            $lines.last().addClass('s');
        } else {
            if ($currLine.prev('tr').length) {
                $lines.removeClass('s');
                $currLine.prev('tr').addClass('s');
            } else {
                $lines.removeClass('s'); //remove current selection
                $lines.last().addClass('s');
            }
        }
    }

    var clickSelectedItem = function($table) {
        var $lines = $table.find('tbody tr');
        $lines.filter('.s').click();
        $lines.removeClass('s');
        if($cnvalue){
            addInv();
        }

    };

    var selectItem = function($table, event) {
//        if (event !== undefined)
//            event.preventDefault();

        var $lines = $table.find('tbody tr');
        var $elem = $lines.filter('.s').find('input');
        if ($elem.attr('type') === 'checkbox') {
        } else {
            $elem.focus();
        }
    };

    var deleteFromItemList = function($table, event) {

        var index = $table.find('.s').attr('data-index');

        if(!index){
            var node = event.target;
            index = $(node).closest("tr").attr('data-index');
        }

        var msg = 'Are you sure you want to delete this item ?';
        var msgHeader = 'Item delete confirmation';

        deleteProductConfermation(msg, msgHeader, index);
    };

    var selectDefault = function($table){
        $table.find('tbody tr').removeClass("s");
        setTimeout(function(){
            $table.find('tbody tr:visible:first').addClass("s");
        }, 0);
    }

    var editSubProducts = function($table, event){
        var id = $(event.target).closest('tr').attr('id');
        var pid = id.split("_")[1]
        if(PRODUCTS_DETAILS[pid].sP === '1' || PRODUCTS_DETAILS[pid].bP === '1')
            angular.element('#PosController').scope().selectProduct(pid);
    }

    $('#product_selector').on('hidden.bs.modal', function() {
        focusNext(".item_search_bar");
    });

    // Loyalty card modal show
    $('#add_loyalty_card').on('click', function() {
        if ($('#paymentModal:visible').length) {
            $('#loyaltyModal').modal('show');
        }
    });
    $('#loyaltyModal').on('shown.bs.modal', function() {
        focusNext("input[name=card_no]");
    });
    $('#loyaltyModal').on('hidden.bs.modal', function() {
        $('#cash').focus();
    });
    // Gift card modal show
    $('#add_gift_card').on('click', function() {
        if ($('#paymentModal:visible').length) {
            $('#giftModal').modal('show');
        }
    });
    $('#giftModal').on('shown.bs.modal', function() {
        focusNext("input[name=gift_card_id]");
    });
    $('#giftModal').on('hidden.bs.modal', function() {
        $('#cash').focus();
    });

});
// Set selected mode activate
var setPosMode = function(mode){
    if(angular.element(document.getElementById('PosController')).scope().setConfig("active_pos_mode", mode)){
        $('#settings .setting-item i').addClass('hidden');
        $('#settings #' + mode + ' i').removeClass('hidden');
    }
};

var deleteProductConfermation = function (msg, msgHeader, index){

    $('.confirmation').confirm(msg, msgHeader, index, function(id) {
        angular.element('#PosController').scope().deleteItem(id);
    });
}

var addInv = function() {
    var msg = 'Are you sure you want to create this credit note ?';
    var msgHeader = 'Credit Note confirmation';
    $('.confirmation').confirm(msg, msgHeader, null,
    function(id) {
        angular.element('#PosController').scope().createCN();
    });
    $('#search').val('');
};

$(function(){
    $("#moredetails").on('click', function(){
        if($("#customer_more").hasClass('hide')){
            $("#customer_more").removeClass('hide');
            $("#moredetails > span").removeClass('glyphicon-circle-arrow-down').addClass('glyphicon-circle-arrow-up');
        }else{
            $("#customer_more").addClass('hide');
            $("#moredetails > span").removeClass('glyphicon-circle-arrow-up').addClass('glyphicon-circle-arrow-down');
        }
    });
});

var uomAddCount = 1;
var validateUomList;
var getAllUoms;
var getUomsDispaly;
var imageID = 0;
var expiryAlartArray = new Array();
var itemAttrValue = [];
$(function(){
    $("#moredetailsProdct").on('click', function() {
        if($("#product_more").hasClass('hide')){
            $("#product_more").removeClass('hide');
            $("#moredetailsProdct > span").removeClass('glyphicon-circle-arrow-down').addClass('glyphicon-circle-arrow-up');
        }else{
            $("#product_more").addClass('hide');
            $("#moredetailsProdct > span").removeClass('glyphicon-circle-arrow-up').addClass('glyphicon-circle-arrow-down');
        }
    });

    updateStatusOfDefalutQuantity();
    $('#productTypeID').on('change', function() {
        updateStatusOfDefalutQuantity();
        if ($('#startingQuantity').length) {
            if ($(this).val() == 1) {
                $('#startingQuantity').attr('disabled', false);
            } else {
                $('#startingQuantity').attr('disabled', true);
            }
        }
        if ($(this).val() == 2) {
            $('.add-uom').attr('disabled', true);
            var allUom = getAllUoms();
            if (Object.keys(allUom).length > 1) {
                p_notification(false, 'Non-inventory item allow only one Uom type.Please remove additional uom types.');
                $(this).val(1);
                if ($('.add-uom').hasClass('edit_mode')) {
                    $('.add-uom').attr('disabled', true);
                } else {
                    $('.add-uom').attr('disabled', false);
                }
            }
        } else {
            $('.add-uom').attr('disabled', false);
        }
    });

    $('#productSalesAccountID').val(CUSTOMER_ACCOUNTS.productSales);

    // UOM related functions
    var $listArea = $("table.uom-list tbody");
    $("button.add-uom").click(function(e) {
        uomAddCount++;
        var $placeholderItem = $("tr.sample", $listArea);
        $listArea.append($placeholderItem.clone().removeClass('sample'));
    });
    $listArea.on('change', "[name='productBaseUom']", function() {
        var $thisParent = $(this).parents('tr');
        // TODO confirm before resetting conversions

        $("[name='productUomConversion[]']", $listArea).val('').prop('readonly', false);
        $("[name='productUomConversion[]']", $thisParent).val(1).prop('readonly', true);
        $("a.delete", $listArea).removeClass('hidden');
        $("a.delete", $thisParent).addClass('hidden');
    });
    $listArea.on('click', "tr:not(.base) [name='productBaseUom']", function(e) {
        var $thisItem = $(this);
        var $allUomTrs = $("tr:not(.sample, .base)", $listArea);
        var conversion_factors_empty = true;
        $("[name='productUomConversion[]']", $allUomTrs).each(function() {
            var conversionValue = $(this).val();
            if (!(isNaN(parseFloat(conversionValue)) || parseFloat(conversionValue) == 0)) {
                conversion_factors_empty = false;
                return false;
            }
        });
        if (!conversion_factors_empty) {
            // TODO check if values are present in input boxes
            bootbox.confirm('Changing the base Unit of Measure will reset all the conversion factors. Are you sure you want to continue?', function(result) {
                if (result) {
                    $thisItem.prop('checked', true).change();
                }
            });
            e.stopPropagation();
            return false;
        }
    });
    $listArea.on('click', "a.delete", function(e) {
        if ($(this).parents('tr').find("[name='productDisplayUom']").is(':checked')) {
            p_notification(false, 'Before delete this uom, please select another uom type as display uom.');
            return false;
        }
        $(this).parents('tr').remove();
        e.stopPropagation();
        return false;
    });
    $listArea.on('change', "[name='productBaseUom']", function(e) {
        var $thisParent = $(this).parents('tr');
        $("[name='productUomConversion[]']", $listArea).val('').prop('readonly', false);
        $("[name='productUomConversion[]']", $thisParent).val(1).prop('readonly', true);
        $("tr", $listArea).removeClass('base');
        $thisParent.addClass('base');
    });
    validateUomList = function() {

        var $allUomTrs = $("tr:not(.sample)", $listArea);
        var uomList = {};
        var success = true;
        // validate if same uom is added multiple times
        $("[name='uomID[]']", $allUomTrs).each(function() {
            var $thisParent = $(this).parents('tr');
            var uomID = $(this).val();
            var conversionValue = $thisParent.find("[name='productUomConversion[]']").val();
            // conversion cannot be empty or less than 1
            if (uomID == '') {
                p_notification(false, 'Please Select uom');
                success = false;
            } else if ((isNaN(parseFloat(conversionValue)) || parseFloat(conversionValue) == 0 || parseFloat(conversionValue) < 1) && success == true) {
                p_notification(false, 'Enter a valid value for Conversion Value of Unit of Measure.');
                $thisParent.find("[name='productUomConversion[]']").focus();
                success = false;
            }

            if (uomID != null) {
                uomList[uomID] = conversionValue;
            }
        });
        // if UOM list is empty
        if ($.isEmptyObject(uomList)) {
            p_notification(false, 'Please select at least one  Unit of Measurement for the item.');
            success = false;
        } else if ($allUomTrs.length != Object.keys(uomList).length) {
            p_notification(false, 'You cannot add the same Unit of Measure multiple times for a single item.');
            success = false;
        }

        return success;
    }

    getAllUoms = function() {
        var $allUomTrs = $("tr:not(.sample)", $listArea);
        var uomList = {};
        $allUomTrs.each(function() {
            var uomID = $("[name='uomID[]']", $(this)).val();
            var uomConversion = ($("[name='productUomConversion[]']", $(this)).val()) ? $("[name='productUomConversion[]']", $(this)).val() : 1;
            var baseUom = $("[name='productBaseUom']", $(this)).is(':checked');
            var displayUom = $("[name='productDisplayUom']", $(this)).is(':checked');
            uomList[uomID] = {uomConversion: uomConversion, baseUom: baseUom, displayUom: displayUom};

        });
        return uomList;
    }
    getUomsDispaly = function() {
        var $allUomTrs = $("tr:not(.sample)", $listArea);
        var uomDisplayValue = false;
        $allUomTrs.each(function() {
            var uomID = $("[name='uomID[]']", $(this)).val();
            if ($(this).find('input[name="productDisplayUom"]').is(':checked')) {
                uomDisplayValue = uomID;
            }
        });
        return uomDisplayValue;
    }

    $('.product-default-selling-price').on('click', '#productDefaultSellingPrice', function() {
        setUomToSellingPrice();
        $(this).focus().trigger('change');
    });

    $('.product-default-purchase-price').on('click', '#productDefaultPurchasePrice', function() {
        setUomToSellingPrice();
        $(this).focus().trigger('change');
    });

    $('.startingQuantity').on('click', function() {
        setUomToSellingPrice();
        $(this).focus().trigger('change');
    });
    $('.uom-list').on('change', function() {
        setUomToSellingPrice();
    });
    $('.uom-list tbody').on('click', 'tr a.delete', function() {
        setUomToSellingPrice();
    });

    setUomToSellingPrice = function() {
        var uoms = getAllUoms();

        var displayUomValue = getUomsDispaly();
        var uomSizeArray = Object.keys(uoms);
        var uom = new Array();
        var uCFlag = false;
        for (index = 0; index < uomSizeArray.length; index++) {
            i = uomSizeArray[index];
            var displayUom = (uoms[uomSizeArray[index]].displayUom == true) ? 1 : 0;
            var uomData = {
                uA: UOMLISTBYID[uomSizeArray[index]]['uomAbbr'],
                uomID: UOMLISTBYID[uomSizeArray[index]]['uomID'],
                uN: UOMLISTBYID[uomSizeArray[index]]['uomName'],
                uC: uoms[uomSizeArray[index]].uomConversion,
                uDP: UOMLISTBYID[uomSizeArray[index]]['uomDecimalPlace'],
                pUDisplay: displayUom
            };
            if (uoms[uomSizeArray[index]].uomConversion === '1') {
                uCFlag = true;
            }
            uom[index] = uomData;
        }
        if (uCFlag) {
            $('#productDefaultSellingPrice').parent().addClass('input-group');
            $('#productDefaultSellingPrice').siblings('.uomPrice').remove();
            $('#productDefaultSellingPrice').addUomPrice(uom);
            $('#productDefaultPurchasePrice').parent().addClass('input-group');
            $('#productDefaultPurchasePrice').siblings('.uomPrice').remove();
            $('#productDefaultPurchasePrice').addUomPrice(uom);
            if ($('#startingQuantity').length) {
                $('#startingQuantity').parent('div').addClass('input-group');
                $('#startingQuantity').siblings('.uomqty').remove();
                $('#startingQuantity').addUom(uom);
            }
        }
    }

    var $createProductForm = $("#create-product-form");
    var $createCategoryForm = $("#create-category-form");
    $("[name='productGlobalProduct']").on('change', function() {
        if ($("#locationItem").prop('checked')) {
            $('#itemLocations').show();
        } else {
            $('#itemLocations').hide();
        }
    }).trigger('change');

     $("#productTaxEligible").on('change', function() {
        if (this.checked) {
            $("#showtax").slideDown();
        } else {
            $("#showtax").slideUp();
            $(".taxes").attr('checked', false);
        }
    }).trigger('change');
    $("#productDiscountEligible").on('change', function() {
        if (this.checked) {
            $("#showdiscount").slideDown();
        } else {
            $("#showdiscount").slideUp();
            $("#productDiscount").val('');
        }
    }).trigger('change');

     $createProductForm.on('submit', function(e) {
        e.preventDefault();
        saveAndUpdate();
        function saveAndUpdate() {
            e.preventDefault();
            // get selected locations
            var locations = $("input.locationCheck:checked").map(function() {
                return $(this).data('locationid');
            }).get();
            // get selected taxes
            var taxes = $("input.taxes:checked").map(function() {
                return $(this).data('taxid');
            }).get();
            // get selected handeling types
            var handeling = $("input.handeling:checked").map(function() {
                return this.id;
            }).get();
            var flag = '';
            if ($('#startingQuantity').length) {
                flag = 'invoice';
            } else {
                flag = 'product';
            }
            var productDefaultOpeningQuantity = "";
            if ($("[name='productDefaultOpeningQuantity']", $createProductForm).length) {
                productDefaultOpeningQuantity = $("[name='productDefaultOpeningQuantity']", $createProductForm).val();
            }
            var formData = {
                productCode: $("[name='productCode']", $createProductForm).val(),
                productBarcode: $("[name='productBarcode']", $createProductForm).val(),
                productName: $("[name='productName']", $createProductForm).val(),
                handeling: handeling,
                productDescription: $("[name='productDescription']", $createProductForm).val(),
                categoryID: $("[name='categoryParentID']").val(),
                productTypeID: $("[name='productTypeID']", $createProductForm).val(),
                productGlobalProduct: $("[name='productGlobalProduct']:checked", $createProductForm).val(),
                locations: locations,
                productTaxEligible: $("[name='productTaxEligible']", $createProductForm).is(':checked'),
                taxes: taxes,
                discountEligible: $("[name='productDiscountEligible']", $createProductForm).is(':checked'),
                productDiscountPercentage: $("[name='productDiscountPercentage']", $createProductForm).val(),
                productDiscountValue: $("[name='productDiscountValue']", $createProductForm).val(),
                purchaseDiscountEligible: $("[name='productPurchaseDiscountEligible']", $createProductForm).is(':checked'),
                productPurchaseDiscountPercentage: $("[name='productPurchaseDiscountPercentage']", $createProductForm).val(),
                productPurchaseDiscountValue: $("[name='productPurchaseDiscountValue']", $createProductForm).val(),
                productImageURL: imageID,
                batchProduct: $("[name='batchProduct']", $createProductForm).is(':checked'),
                serialProduct: $("[name='serialProduct']", $createProductForm).is(':checked'),
                hsCode: $("[name='hsCode']").val(),
                customDutyValue: $("[name='customDutyValue']", $createProductForm).val(),
                customDutyPercentage: $("[name='customDutyPercentage']", $createProductForm).val(),
                productState: $("[name='productState']", $createProductForm).val(),
                productDefaultSellingPrice: $("[name='productDefaultSellingPrice']", $createProductForm).val(),
                productDefaultMinimumInventoryLevel: $("[name='productDefaultMinimumInventoryLevel']", $createProductForm).val().replace(/,/g, ''),
                productDefaultReorderLevel: $("[name='productDefaultReorderLevel']", $createProductForm).val().replace(/,/g, ''),
                flag: flag,
                productDefaultOpeningQuantity: productDefaultOpeningQuantity,
                uomDisplay: getUomsDispaly(),
                productDefaultPurchasePrice: $("[name='productDefaultPurchasePrice']", $createProductForm).val(),
                rackID: $("[name='rackId']", $createProductForm).val(),
                itemDetail: $("[name='itemDetail']", $createProductForm).val(),
                productSalesAccountID: $("#productSalesAccountID", $createProductForm).val(),
                productInventoryAccountID: $("#productInventoryAccountID", $createProductForm).val(),
                productCOGSAccountID: $("#productCOGSAccountID", $createProductForm).val(),
                productAdjusmentAccountID: $("#productAdjusmentAccountID", $createProductForm).val(),
                expiryAlertArray: expiryAlartArray,
                itemAttrValue: itemAttrValue,
            };
            if (validateProductForm(formData)) {
                formData['uom'] = getAllUoms();
                formData['displayUoms'] = getUomsDispaly();
                formData['suppliers'] = [];

                eb.ajax({
                    url: '/productAPI/add-product',
                    method: 'post',
                    data: formData,
                    dataType: 'json',
                    success: function(data) {
                        if (data.status == true) {
                            var startingQuantity = $('#startingQuantity').val();
                            var openingPrice = $('#productDefaultPurchasePrice').val();
                            var productType = $('#productTypeID').val();
                            var productID = data.data.productID;
                            if (startingQuantity != '' && productType == 1) {
                                var items = {};
                                var locationProductData = data.data.locationProductData;

                                var itemUom = _.find(formData.uom, function(val, key) {
                                    if (val.displayUom == 1 || val.displayUom) {
                                        val['uomID'] = key;
                                        return val;
                                    }
                                });
                                var uomID = itemUom.uomID;
                                var locationProductID = locationProductData.locationProductID;
                                var uomConversionRate = $("#qty").siblings('.uom-select').find('.selected').data('uc');
                                uomConversionRate = uomConversionRate != 0 ? uomConversionRate : itemUom.uomConversion;

                                items[productID] = new positiveAdjustmentProduct(locationProductID, productID, formData.productCode, formData.productName, startingQuantity, openingPrice, formData.uom, uomID, '0', {}, {}, productType, uomConversionRate);

                                var goods_issue_tbl = {
                                    goods_issue_date: $('#adjusmentDate').val(),
                                    goods_issue_reason: "opening balnce from invoice",
                                    goods_issue_type_id: 2,
                                    locRefID: 'No'
                                };
                                eb.ajax({
                                    type: 'POST',
                                    url: BASE_URL + '/api/inventory-adjustment/update-positive-adjustment',
                                    async: false,
                                    data: {items: items, goods_issue_tbl_data: goods_issue_tbl, locationID: data.data.locationProductData.locationID, flag: 'invoice'},
                                });
                            }
                            p_notification(data.status, data.msg);
                            var chkProductUpdated = setInterval(function(){ 
                                if (PRODUCTS_DETAILS[productID] != undefined) {
                                    angular.element('#PosController').scope().selectProduct(productID);
                                    clearProductScreenForInvocie();
                                    $('#createProduct').modal('hide');
                                    clearInterval(chkProductUpdated);
                                } 
                            }, 500);
                        } else {
                            p_notification(false, data.msg);
                        }
                    }
                });
            }
        }
    });
});

function updateStatusOfDefalutQuantity() {
    if ($('#productTypeID').val() == 2) {
        $('#productDefaultMinimumInventoryLevel').attr('disabled', true);
        $('#productDefaultReorderLevel').attr('disabled', true);
    } else {
        $('#productDefaultMinimumInventoryLevel').attr('disabled', false);
        $('#productDefaultReorderLevel').attr('disabled', false);
    }
}

function  clearProductScreenForInvocie() {
    $('#productCode').val('');
    $('#productName').val('');
    $('#startingQuantity').val('');
    $('.uomqty').val('');
    $('#startingQuantity').attr('disabled', false);
    $('#productBarcode').val('');
    $('#productTypeID').val(1);
    $('#productHandelingPurchaseProduct').attr('checked', false);
    $('#productHandelingSalesProduct').attr('checked', false);
    $('#productHandelingConsumables').attr('checked', false);
    $('#productHandelingFixedAssets').attr('checked', false);
    $('#productDescription').val('');
    $("#showtax").slideUp();
    $("#showdiscount").slideUp();
    $('.uom-list tbody tr').each(function() {
        if (!($(this).hasClass('base') || $(this).hasClass('sample'))) {
            $(this).remove();
        }
    });
    $('#productDefaultSellingPrice').val('');
    $('#productDefaultMinimumInventoryLevel').val('');
    $('#productDefaultReorderLevel').val('');
    $('#productTaxEligible').attr('checked', false);
    $('.tax-table').find('.taxes').attr('checked', false);
    $('#productDiscountEligible').attr('checked', false);
    $('#productDiscountPercentage').val('');
    $('#productDiscountValue').attr('');
}

function positiveAdjustmentProduct(lPID, pID, pCode, pN, pQ, pUP, pUom, uomID, pTotal, bProducts, sProducts, productType, uomConversionRate) {
    this.locationPID = lPID;
    this.pID = pID;
    this.pCode = pCode;
    this.pName = pN;
    this.pQuantity = pQ;
    this.pUnitPrice = pUP;
    this.pUom = pUom;
    this.uomID = uomID;
    this.pTotal = pTotal;
    this.bProducts = bProducts;
    this.sProducts = sProducts;
    this.productType = productType;
    this.uomConversionRate = uomConversionRate;
}

function validateProductForm(formData) {
    var success = true;
    if (formData.productCode == null || formData.productCode == "") {
        p_notification(false, 'Please enter an Item Code.');
        $("[name='productCode']", $createProductForm).focus();
        success = false;
    } else if (formData.productName == null || formData.productName == "") {
        p_notification(false, 'Please enter an Item Name.');
        $("[name='productName']", $createProductForm).focus();
        success = false;
    } else if ((formData.handeling).length == 0) {
        p_notification(false, 'Please select at least one Item Handling Type.');
        success = false;
    } else if (CATEGORIES_PATH_LIST[formData.categoryID] == undefined) {
        p_notification(false, 'Please select an Item Category');
        $("[name='categoryParentIDHelper']", $createProductForm).focus();
        return false;
    } else if (!$("input:radio[name=productGlobalProduct]").is(':checked')) {
        p_notification(false, 'Please enter Item Location as Global or Location.');
        success = false;
    } else if (formData.productGlobalProduct == 0 && (formData.locations).length == 0) {
        p_notification(false, 'You must select at least one location for a item.');
        success = false;
    } else if (formData.productTaxEligible == 1 && (formData.taxes).length == 0) {
        p_notification(false, 'You must select at least one tax for a tax eligible item.');
        success = false;
    } else if (formData.discountEligible == 1 && (formData.productDiscountPercentage.trim() != '') && (formData.productDiscountValue.trim() != '')) {
        p_notification(false, 'You can only enter either a Discount Value or Percentage, not both.');
        $("[name='productDiscountPercentage']", $createProductForm).focus();
        success = false;
    }else if (formData.discountEligible == 1 && (formData.productDiscountPercentage.trim() == '') && (formData.productDiscountValue.trim() == '')) {
        p_notification(false, 'You should enter either a Discount Value or Percentage, cannot keep empty');
        $("[name='productDiscountPercentage']", $createProductForm).focus();
        success = false;
    } else if(formData.purchaseDiscountEligible == 1 && (formData.productPurchaseDiscountPercentage.trim() != '') && (formData.productPurchaseDiscountValue.trim() != '')) {
        p_notification(false, 'You can only enter either a Value or Percentage for purchase Discount, not both.');
        $("[name='productPurchaseDiscountPercentage']", $createProductForm).focus();
        success = false;
    }  else if(formData.purchaseDiscountEligible == 1 && (formData.productPurchaseDiscountPercentage.trim() == '') && (formData.productPurchaseDiscountValue.trim() == '')) {
        p_notification(false, 'You can only enter either a Value or Percentage for purchase Discount, cannot keep empty.');
        $("[name='productPurchaseDiscountPercentage']", $createProductForm).focus();
        success = false;
    } else if (formData.discountEligible == 1 && (formData.productDiscountPercentage != "") && ((isNaN(parseFloat(formData.productDiscountPercentage))))) {
        p_notification(false, 'Please enter a valid value for item discount percentage.');
        $("[name='productDiscountPercentage']", $createProductForm).focus();
        success = false;
    } else if (formData.purchaseDiscountEligible == 1 && (formData.productPurchaseDiscountPercentage != "") && ((isNaN(parseFloat(formData.productPurchaseDiscountPercentage))))) {
        p_notification(false, 'Please enter a valid value for item purchase discount percentage.');
        $("[name='productPurchaseDiscountPercentage']", $createProductForm).focus();
        success = false;
    } else if (formData.discountEligible == 1 && (formData.productDiscountPercentage != "") && (parseFloat(formData.productDiscountPercentage) > 100)) {
        p_notification(false,'Item Discount Percentage cannot be over 100%');
        $("[name='productDiscountPercentage']", $createProductForm).focus();
        success = false;
    } else if(formData.purchaseDiscountEligible == 1 && (formData.productPurchaseDiscountPercentage != "") && (parseFloat(formData.productPurchaseDiscountPercentage) > 100)) {
        p_notification(false, 'Item purchase discount Percentage cannot be over 100%.');
        $("[name='productPurchaseDiscountPercentage']", $createProductForm).focus();
        success = false; 
    } else if (formData.discountEligible == 1 && (formData.productDiscountValue != "") && ((isNaN(parseFloat(formData.productDiscountValue))))) {
        p_notification(false, 'Please enter a valid value for item discount value.');
        $("[name='productDiscountValue']", $createProductForm).focus();
        success = false;
    } else if (formData.productDefaultSellingPrice != '' && isNaN(formData.productDefaultSellingPrice)) {
        p_notification(false, 'Product Default Selling price should be numeric.');
        $("[name='productDefaultSellingPrice']", $createProductForm).focus();
        success = false;
    } else if (formData.productDefaultOpeningQuantity != '' && (isNaN(formData.productDefaultOpeningQuantity) || formData.productDefaultOpeningQuantity < 0)) {
        p_notification(false, 'Product Default Opening quantity should be numeric.');
        $("[name='productDefaultOpeningQuantity']", $createProductForm).focus();
        success = false;
    } else if (formData.productDefaultMinimumInventoryLevel != '' && isNaN(formData.productDefaultMinimumInventoryLevel)) {
        p_notification(false, 'Product Default Min.Inventory level should be numeric.');
        $("[name='productDefaultMinimumInventoryLevel']", $createProductForm).focus();
        success = false;
    } else if (formData.productDefaultReorderLevel != '' && isNaN(formData.productDefaultReorderLevel)) {
        p_notification(false, 'Product Default Reorder level should be numeric.');
        $("[name='productDefaultReorderLevel']", $createProductForm).focus();
        success = false;
    } else if (formData.productDefaultMinimumInventoryLevel != '' && (formData.productDefaultReorderLevel == '' || parseFloat(formData.productDefaultReorderLevel) < parseFloat(formData.productDefaultMinimumInventoryLevel))) {
        p_notification(false, 'Product Default  Min.Inventory level cannot be more than Product Default Reorder level.');
        $("[name='productDefaultMinimumInventoryLevel']", $createProductForm).focus();
        success = false;
    } else if ((formData.customDutyPercentage != null || formData.customDutyPercentage != "") && isNaN(formData.customDutyPercentage)) {
        p_notification(false, 'Please enter a valid value for Custom Duty Percentage.');
        $("[name='customDutyPercentage']", $createProductForm).focus();
        success = false;
    } else if ((formData.customDutyPercentage.trim() != '') && (formData.customDutyValue.trim() != '')) {
        p_notification(false, 'You can only enter either a Custom Duty Value or Percentage, not both.');
        $("[name='customDutyPercentage']", $createProductForm).focus();
        success = false;
    } else if ((formData.customDutyPercentage != null || formData.customDutyPercentage != "") && parseFloat(formData.customDutyPercentage) > 100) {
        p_notification(false, 'Custom Duty Percentage cannot be over 100%');
        $("[name='customDutyPercentage']", $createProductForm).focus();
        success = false;
    } else if ((formData.customDutyValue != null || formData.customDutyValue != "") && isNaN(formData.customDutyValue)) {
        p_notification(false, 'Please enter a valid value for Custom Duty Value.');
        $("[name='customDutyValue']", $createProductForm).focus();
        success = false;
    } else if (!validateUomList()) {
// errors will be displayed from the 'validateUomList' method
        success = false;
    } else if (!formData.uomDisplay) {
        p_notification(false, 'Please select display uom type.');
        success = false;
    }
    else if ($('#startingQuantity').length) {
        if (formData.productTypeID == 1) {
            if (isNaN($('#startingQuantity').val()) || $('#startingQuantity').val() < 0) {
                p_notification(false, 'Please enter a valid opening balance for product.');
                $("#startingQuantity", $createProductForm).focus();
                success = false;
            }
        }
    } 
    return success;
}

(function($) {
    $.fn.confirm = function(msg, msgHead, id, callBack) {

        $('#confirmation-msg', $cnf).text(msg);
        $('#confirmation-msg-head',$cnf).text(msgHead);
        var $cnf = $(this);
        $('.delete_conf_wrapper').fadeIn(500).show();

        $cnf.animate({'top': '25%', opacity: 1}, 500);
        $('#yes-confirmation', $cnf).focus();
        $cnf.bind('keydown', function(e) {
            switch (e.keyCode) {
                case 37:
                    $cnf.find('#no-confirmation').focus();
                    break;
                case 39:
                    $cnf.find('#yes-confirmation').focus();
                    break;
            }
        });
        $('#yes-confirmation', $cnf).unbind('click').bind('click', function() {
            closeConfirm($cnf);
            callBack(id);
        });
        $('#no-confirmation', $cnf).unbind('click').bind('click', function() {
            closeConfirm($cnf);
        });
        closeConfirm = function($cnf) {
            if(type = 'delete'){
                $('.delete_conf_wrapper').fadeOut();
            }else{
                $('.credit_note_conf_wrapper').fadeOut();
            }
            $cnf.animate({'top': '-100%', opacity: 0}, 500);
            $cnf = undefined;
        };
    };
}(jQuery));

(function($) {

    $.fn.addUomPrice = function(uoms) {
        $(this).each(function() {

            var $price = $(this);
            var name = $price.attr('name') + new Date().getUTCMilliseconds();

            // remove existing dropdown
            $price.siblings('.uom-price-select').remove();

            // create new dropdown
            var $dropdown = $('<div class="input-group-btn uom-price-select"></div>');
            var $btn = $('<button data-toggle="dropdown" class="btn btn-default dropdown-toggle uom_price_drop_down" type="button"><span class="selected"></span><em>&nbsp;</em><span class="fa fa-caret-down"></span></button>');
            var $addContainer = $('<ul class="text-left dropdown-menu pull-right"></ul>');
            $btn.appendTo($dropdown);
            $btn.after($addContainer);
            $price.after($dropdown);

            // show original price input
            $price.hide();

            // create new input for uom price
            var $uomPrice = $('<input>').addClass('form-control text-right uomPrice reduce_le_ri_padding');
            $price.before($uomPrice);

            if (uoms !== undefined) {
                $btn.find('li').remove();
                $.each(uoms, function(i) {
                    var $option = $('<li class="col-lg-12"></li>');
                    var $lidiv = $('<div class="radio_button"></div>');
                    var $radiobutton = $('<input type="radio" value="1">').attr('name', name + '_uomtype').attr('id', name + '_uomtype' + i).css('margin-right', '4px');
                    var $label = $('<label></label>').attr('for', name + '_uomtype' + i);
                    $label.text(this.uN + ' (' + this.uA + ')');
                    $option.data('id', this.uomID)
                            .data('state', this.us)
                            .data('abbr', this.uA)
                            .data('conversion', this.uC);
                    $option.appendTo($addContainer);
                    $lidiv.appendTo($option);
                    $radiobutton.appendTo($lidiv);
                    $radiobutton.after($label);
                    if (this.pUDisplay == 1) {
                        $radiobutton.attr('checked', true);
                        $btn.find('.selected').text(this.uA)
                                .data('uc', this.uC)
                                .data('uomID', this.uomID);

                        var basePrice = $price.val();
                        var conversionRate = this.uC;
                        var price = basePrice * conversionRate;

                        $uomPrice.val(price);
                    }
                });
            }



            $dropdown.find('input[type="radio"]').on('change', function() {
                var basePrice = $price.val();
                var conversionRate = $(this).parents('li').data('conversion');
                $(this).parents('ul').siblings('button').find('.selected').text($(this).parents('li').data('abbr'))
                        .data('uc', conversionRate)
                        .data('uomID', $(this).parents('li').data('id'));

                var price = basePrice * conversionRate;

                $uomPrice.val(price);
            });

            $uomPrice.change(function() {
                var price = $uomPrice.val();
                var conversionRate = $dropdown.find('.selected').data('uc');
                if (!($.isNumeric(price)) || price < 0) {
                    $uomPrice.val('');
                    price = 0.00;
                }
                var basePrice = price / conversionRate;
                $price.val(basePrice).trigger('change', 'keyup', 'focusout');
            });

            $price.change(function() {
                var basePrice = $price.val();
                var conversionRate = $dropdown.find('.selected').data('uc');
                var price = basePrice * conversionRate;

                price = parseFloat(price).toFixed(getDecimalPlaces(price));

                $uomPrice.val(price);
                if ($price.prop('readonly') == true || $price.prop('disabled') == true) {
                    $uomPrice.attr('disabled', true);
                } else {
                    $uomPrice.attr('disabled', false);
                    $uomPrice.focus().select();
                }
            });

            $price.trigger('change');

        });

        return $(this);
    };

}(jQuery));

(function($) {

    // TODO - write function to get current UOM ID. Check where this is used and
    // see if it is necessary

    $.fn.addUom = function(uoms) {
        isPriceUom = typeof isPriceUom !== 'undefined' ? isPriceUom : false;
        $(this).each(function() {

            var $qty = $(this);
            var name = $qty.attr('name') + new Date().getUTCMilliseconds();

            // remove existing dropdown
            $qty.siblings('.uom-select').remove();

            // create new dropdown
            var $dropdown = $('<div class="input-group-btn uom-select"></div>');
            var $btn = $('<button data-toggle="dropdown" class="btn btn-default dropdown-toggle" type="button"><span class="selected"></span><em>&nbsp;</em><span class="fa fa-caret-down"></span></button>');
            var $addContainer = $('<ul class="text-left dropdown-menu pull-right"></ul>');
            $btn.appendTo($dropdown);
            $btn.after($addContainer);
            $qty.after($dropdown);

            // show original qty input
            $qty.hide();

            // create new input for uom qty
            var $uomQty = $('<input>').addClass('form-control text-right uomqty reduce_le_ri_padding');
            $qty.before($uomQty);

            if (uoms !== undefined) {
                $btn.find('li').remove();
                $.each(uoms, function(i) {
                    var $option = $('<li class="col-lg-12"></li>');
                    var $lidiv = $('<div class="radio_button"></div>');
                    var $radiobutton = $('<input type="radio" value="1">').attr('name', name + '_uomtype').attr('id', name + '_uomtype' + i).css('margin-right', '4px');
                    var $label = $('<label></label>').attr('for', name + '_uomtype' + i);
                    $label.text(this.uN + ' (' + this.uA + ')');
                    $option.data('id', this.uomID)
                            .data('state', this.us)
                            .data('abbr', this.uA)
                            .data('decimalPlaces', this.uDP)
                            .data('conversion', this.uC);
                    $option.appendTo($addContainer);
                    $lidiv.appendTo($option);
                    $radiobutton.appendTo($lidiv);
                    $radiobutton.after($label);
                    if (this.pUDisplay == 1) {
                        $radiobutton.attr('checked', true);
                        $btn.find('.selected').text(this.uA)
                                .data('uc', this.uC)
                                .data('decimalPlaces', this.uDP)
                                .data('uomID', this.uomID);

                        var base = $qty.val();
                        var conversionRate = this.uC;
                        var decimalPlaces = this.uDP;
                        var Qty = base / conversionRate;
    
                        if (decimalPlaces == 0) {
                            Qty = Math.floor(parseFloat(Qty));
                        } else {
                            Qty = parseFloat(Qty).toFixed(decimalPlaces);
                        }
                        $uomQty.val(Qty);
                    }
                });
            }

            $dropdown.find('input[type="radio"]').on('change', function() {
                var base = $qty.val();
                var conversionRate = $(this).parents('li').data('conversion');
                var decimalPlaces = $(this).parents('li').data('decimalPlaces');
                $(this).parents('ul').siblings('button').find('.selected').text($(this).parents('li').data('abbr'))
                        .data('uc', conversionRate)
                        .data('decimalPlaces', decimalPlaces)
                        .data('uomID', $(this).parents('li').data('id'));

                var qty = base / conversionRate;

                if (decimalPlaces == 0) {
                    qty = Math.floor(parseFloat(qty));
                } else {
                    qty = parseFloat(qty).toFixed(decimalPlaces);
                }
                $uomQty.val(qty);
            });

            $uomQty.change(function() {
                var qtyVal = $uomQty.val();
                var conversionRate = $dropdown.find('.selected').data('uc');
                var decimalPlace = $dropdown.find('.selected').data('decimalPlaces');
                if (!($.isNumeric(qtyVal)) || qtyVal < 0) {
                    $uomQty.val('');
                    qtyVal = 0.00;
                }
                var baseQty = qtyVal * conversionRate;
                if (decimalPlace == 0) {
                    baseQty = Math.floor(parseFloat(baseQty));
                } else {
                    baseQty = parseFloat(baseQty).toFixed(decimalPlace);
                }
                $qty.val(baseQty).trigger('change', 'keyup', 'focusout');
            });

            $qty.change(function() {
                var base = $qty.val();
                var conversionRate = $dropdown.find('.selected').data('uc');
                var decimalPlaces = $dropdown.find('.selected').data('decimalPlaces');
                var fromQty = base / conversionRate;
    
                if (decimalPlaces == 0) {
                    fromQty = Math.floor(parseFloat(fromQty));
                } else {
                    fromQty = parseFloat(fromQty).toFixed(decimalPlaces);
                }
                $uomQty.val(fromQty);
                if ($qty.prop('readonly') == true || $qty.prop('disabled') == true) {
                    $uomQty.attr('disabled', true);
                } else {
                    $uomQty.attr('disabled', false);
                    $uomQty.focus().select();
                }
            });

            $qty.trigger('change');

        });

        return $(this);
    };

}(jQuery));