$(document).ready(function () {

    //Focus to default item entering mode
    mode = 's';
    selectMode();

    //window fixed height
    var fixedContentHeight = 0;
    $(".navbar-default, .second_row, .bar_code_section, .details_table_heading, .amount_details").each(function () {
        fixedContentHeight += parseInt($(this).height());
    });

    var product_list_height = 0;
    $(".navbar-default, .second_row").each(function () {
        product_list_height += parseInt($(this).height());
    });

    $(window).resize(function () {
        var windowHeight = $(window).height();
        var login_wrapper = $('.login_outer_wrapper').height();
        var login_height = $('.pos_login_wrapper').height();

        $('.stretchable').css('height', windowHeight - (fixedContentHeight + 55));
        $('.product_items').css('height', windowHeight - (product_list_height));
        $('.pos_login_wrapper').animate({
            'opacity': 1,
            'margin-top': ((login_wrapper - login_height) / 2) - 40
        }, 500, function () {
            $('#openingBalance').focus();
        });

        //scroll bar append
        var stretchable_height = $('.stretchable').height();
        $('.details_tbody').css("height", (stretchable_height - 10) + 'px');
        $('.details_tbody').css("maxHeight", (stretchable_height - 10) + 'px');

    }).trigger('resize');

    //onfocus_barcode
    $('.bar_code .form-control').focus(function () {
        $('.bar_code .input-group-addon').css('border-color', '#3276b1');
    }).focusout(function () {
        $('.bar_code .input-group-addon').css('border-color', '#666666');
    });

//    //retrive list item_slide_toggle
//    $('#retrive_list_btn').on('click', function(event) {
//        if ($('ul.retrive_list li').length < 1) {
//            var text = "Order List Empty";
//            $('ul.retrive_list').append('<li class="empty">' + text + '</li>');
//        } else {
//            $('ul.retrive_list li.empty').remove();
//        }
//        $('.retrive_list').slideToggle();
//        event.stopPropagation();
//    });

    // POS drop down
    // $('.pos_drop_down button').on('click', function (event) {
    $('.main-buttons').on('click', '.pos_drop_down button', function (event) {
        var li = $(this).siblings('ul').find('li');

        var pos_dd_name = $(this).parent('.pos_drop_down').attr('data-pos_dd_name')

        if (li.length < 1) {
            var text = pos_dd_name + " List Empty";
            $(this).siblings('ul').append('<li class="empty">' + text + '</li>');
        } else {
            $(this).siblings('ul li.empty').remove();
        }
        $(this).siblings('ul').slideToggle();
        event.stopPropagation();
    });

    //hide the retrive list item out side click,(if list item visible) and remove the message li if it is exist
    $('body').on('click', function (e) {
        if (!$('.retrive_list').is(':visible')) {
            return;
        }
        
        if ($('ul.retrive_list li').length >= 1) {
            if ($('.retrive_list li').hasClass('empty')) {
                $('ul.retrive_list li.empty').remove();
            }
        }
        if(!$(e.target).hasClass('drop-down-search')){
            $('.retrive_list').hide();
        }
    });

    //payment_modal select payment method
    $('#payment_method').change(function () {
        $('.card, .cash').hide(0);

        if ($('#payment_method').val() === 0) {
            $('.cash').show(0).find('input:first').focus().select();
        } else {
            $('.card').show(0).find('input:first').focus().select();
        }
    });

    //log_out close window btn
    $('.log_out_close_btn').on('click', function () {
        remove_login_modal();
    });

    //hide the search list item after search input field losses the focus
    $('#search').on('focusout', function () {
        if ($('.search_result').is(':visible')) {
//            $('.search_result').hide();
        }
    });

//    $("#printPreview").on("load", function() {
//        if (($(this).attr('src')).trim() != '') {
//            printIframe("printPreview");
//        }
//    });

    // Stop auto-fill for currency type input boxes.
    $(".currency").attr('autocomplete', 'off');


    $('#loyaltyModal').on('shown.bs.modal', function () {
        //still not implemented
    });

    //pos-view / credit-note-view - switch state
    // $('#toggle-view').click(function() {
    //     $('.view-state').toggleClass('current-state-selected');
    // });

});//dom_ready_event


function stopTab(e) {
    var evt = e || window.event;
    if (evt.keyCode === 9) {
        return false;
    }
}

//focus func()
function selectMode() {
    if (mode === 's') {
        $('#search').focus();
    }
    else {
        $('#barCode').focus();
    }
}

// remove the login_modal
function remove_login_modal() {
    $('.pos_login_wrapper').animate({
        'margin-top': 0,
        'opacity': 0
    }, 500, function () {
        $('.login_outer_wrapper').animate({
            'opacity': 0,
            'z-index': -99999
        });
    });
}

// append login again
function append_login_again() {
    var login_wrapper = $('.login_outer_wrapper').height();
    var login_height = $('.pos_login_wrapper').height();

    $('.login_outer_wrapper').animate({
        'z-index': 99999
    }, 100, function () {
        $('.login_outer_wrapper').animate({
            'opacity': 1
        });
        $('.pos_login_wrapper').animate({
            'margin-top': ((login_wrapper - login_height) / 2) - 40,
            'opacity': 1
        });
    });
}
