
//this check the pos opening balance isNumeric
$(document).ready(function() {
    $('#openingBalance').on('input', function() {
        var $openningBalance = $(this);
        var value = $openningBalance.val();

        if (!$.isNumeric(value)) {
            $openningBalance.val('');
            p_notification(false, eb.getMessage('ERR_POSLOG_BALANCENUM'));
        }
    });
});