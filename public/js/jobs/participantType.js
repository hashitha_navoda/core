$(document).ready(function() {

    var departmentId   = null;
    var departmentName = null;
    var departmentCode = null;
    var departmentStatus = null;
    
    $('#btnAdd').on('click',function(){
        var departmentCode = $('#departmentCode').val();
        var departmentName = $('#departmentName').val();
        
        if(departmentCode.trim() && departmentName.trim()){
            eb.ajax({
                type: 'POST',
                url: BASE_URL + '/department-api/createDepartment',
                data: {
                    departmentCode : departmentCode,
                    departmentName : departmentName,
                    tournamentFlag : true
                },
                success: function(respond) {
                    p_notification(respond.status, respond.msg);                
                    if(respond.status){
                        $('#departmentCode').val('');
                        $('#departmentName').val('');
                        window.setTimeout(function() {
                                location.reload();
                        }, 1000);                    
                    }                
                }
            });
        } else {
            p_notification(false, eb.getMessage('ERR_DEPARTMENT_REQUIRED'));
        }
    });

    //for cancel btn
    $('#btnCancel').on('click',function (){
        $('input[type=text]').val('');        
        departmentRegisterView();        
    });

    $('#department-list').on('click','.department-action',function (e){    
        var action = $(this).data('action');
        departmentId   = $(this).closest('tr').data('department-id');
        departmentName = $(this).closest('tr').data('department-name');
        departmentCode = $(this).closest('tr').data('department-code');
        departmentStatus = $(this).closest('tr').data('department-status');

        switch(action) {
            case 'edit':
                e.preventDefault();
                departmentUpdateView();
                //set selected department details
                $('#departmentCode').val(departmentCode);
                $('#departmentName').val(departmentName);
                break;
            case 'delete':
                deleteDepartment(departmentId);
                break;

            default:
                console.error('Invalid action.');
                break;
        }
    });

    //this function for switch to department register view
    function departmentRegisterView(){
       $('.updateTitle').addClass('hidden');
       $('.addTitle').removeClass('hidden');
       $('.updateDiv').addClass('hidden');
       $('.addDiv').removeClass('hidden');
    }

    //this function for switch to department update view
    function departmentUpdateView(){
       $('.addTitle').addClass('hidden');
       $('.updateTitle').removeClass('hidden');
       $('.addDiv').addClass('hidden');
       $('.updateDiv').removeClass('hidden');
    }

    $('#btnUpdate').on('click',function(){
        var departmentCode = $('#departmentCode').val();
        var departmentName = $('#departmentName').val();
        
        if(departmentCode.trim() && departmentName.trim()){
            eb.ajax({
                type: 'POST',
                url: BASE_URL + '/department-api/update',
                data: {
                    departmentCode : departmentCode,
                    departmentName : departmentName,
                    departmentID: departmentId,
                    tournamentFlag : true
                },
                success: function(respond) {
                    p_notification(respond.status, respond.msg);                
                    if(respond.status){
                        $('#departmentCode').val('');
                        $('#departmentName').val('');
                        window.setTimeout(function() {
                                location.reload();
                        }, 1000);                  
                    }                
                }
            });
        } else {
            p_notification(false, eb.getMessage('ERR_DEPARTMENT_REQUIRED'));
        }
    });

    //for search
    $('#searchDept').on('click', function() {
        var key = $('#departmentSearch').val();

        if (key.trim()) {
            $.ajax({
                type: 'POST',
                url: BASE_URL + '/department-api/search',
                data: {
                    searchKey : key
                },
                success: function(data) {
                    if (data.status == true) {
                        if (data.status) {
                            $("#department-list").html(data.html);
                        } else {
                            p_notification(data.status, data.msg);
                        }
                    }            
                }
            });
        } else {
            p_notification(false, eb.getMessage('ERR_DEPT_SEARCH_KEY'));
        }
    });

    //for rest btn
    $('#btnReset').on('click',function (){
        $('input[type=text]').val('');
    });  

    //for search cancel btn
    $('#searchCancel').on('click',function (){
        $('#departmentSearch').val('');
        fetchAll();
    });

    //this function for fetach all the departments
    function fetchAll(){
        $.ajax({
            type: 'POST',
            data: {
                searchKey: ''
            },
            url: BASE_URL + '/department-api/search',
            success: function(respond) {
                $('#department-list').html(respond.html);            
            }
        });
    }

    function deleteDepartment(departmentId) {
        bootbox.confirm('Are you sure you want to delete this participant type?', function($result) {
            if ($result == true) {
                eb.ajax({
                    url: BASE_URL + '/department-api/deleteDepartment',
                    method: 'post',
                    data: {
                        departmentID: departmentId,
                        tournamentFlag : true
                    },
                    dataType: 'json',
                    success: function(data) {
                        if (data.status == true) {
                            p_notification(data.status, data.msg);
                            window.location.reload();
                        } else {
                            p_notification(data.status, data.msg);
                        }
                    }
                });
            }
        });
    }

});
