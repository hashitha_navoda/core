$(document).ready(function() {

    var teamId   = null;
    var teamName = null;
    var teamCode = null;
    var teamEmployees = null;
    

    $('#btnAdd').on('click',function(){
        teamCode = $('#teamCode').val();
        teamName = $('#teamName').val();
        teamEmployees = $('#teamEmployee').val();

        if(teamCode.trim() && teamName.trim() && teamEmployees != null){      
            if(jQuery.inArray("0", teamEmployees) !== -1) {
                p_notification(false, eb.getMessage('ERR_TEAM_REQUIRED'));
                return;
            }

            eb.ajax({
                type: 'POST',
                url: BASE_URL + '/team-api/createTeam',
                data: {
                    teamCode : teamCode,
                    teamName : teamName,
                    teamEmployees : teamEmployees
                },
                success: function(respond) {
                    p_notification(respond.status, respond.msg);                
                    if(respond.status){
                        $('#teamCode').val('');
                        $('#teamName').val('');
                        $('#teamEmployee').val('');
                        window.setTimeout(function() {
                                location.reload();
                        }, 1000);                    
                    }                
                }
            });
        } else {
            p_notification(false, eb.getMessage('ERR_TEAM_REQUIRED'));
        }
    });

    //for cancel btn
    $('#btnCancel').on('click',function (){
        $('input[type=text]').val('');
        $('select[name=teamEmp]').val('');
        $('.selectpicker').selectpicker('refresh');      
        teamRegisterView();        
    });

    $('#team-list').on('click','.team-action',function (e){    
        var action = $(this).data('action');
        teamId   = $(this).closest('tr').data('team-id');
        teamName = $(this).closest('tr').data('team-name');
        teamCode = $(this).closest('tr').data('team-code');
        status = $(this).closest('tr').data('team-status');

        switch(action) {
            case 'edit':
                e.preventDefault();
                teamUpdateView();

                //set selected team details
                $('#teamCode').val(teamCode);
                $('#teamName').val(teamName);
                getTeamEmployees(teamId);
                break;

            case 'status':
                var msg;
                var status;
                var currentDiv = $(this).contents();
                var flag = true;
                if ($(this).children().hasClass('fa-check-square-o')) {
                    msg = 'Are you sure you want to Deactivate this team';
                    status = '0';
                }
                else if ($(this).children().hasClass('fa-square-o')) {
                    msg = 'Are you sure you want to Activate this team';
                    status = '1';
                }
                activeInactive(teamId, status, currentDiv, msg, flag);
                break;
            case 'view':
                getTeamEmployees(teamId, 'loadModal');
                break;
            case 'delete':
                deleteTeam(teamId);
                break;
            default:
                console.error('Invalid action.');
                break;
        }
    });

    //this function for switch to team register view
    function teamRegisterView(){
       $('.updateTitle').addClass('hidden');
       $('.addTitle').removeClass('hidden');
       $('.updateDiv').addClass('hidden');
       $('.addDiv').removeClass('hidden');
    }

    //this function for switch to team update view
    function teamUpdateView(){
       $('.addTitle').addClass('hidden');
       $('.updateTitle').removeClass('hidden');
       $('.addDiv').addClass('hidden');
       $('.updateDiv').removeClass('hidden');
    }

    /**
     *
     *use to change active state
     */
    function activeInactive(type, status, currentDiv, msg, flag) {
        bootbox.confirm(msg, function($result) {
            if ($result == true) {
                eb.ajax({
                    url: BASE_URL + '/team-api/changeStatusID',
                    method: 'post',
                    dataType: 'json',
                    data: {
                        'teamID': type,
                        'status': status,
                    },
                    success: function(data) {
                        if (data.status) {
                            p_notification(data.status, data.msg);
                            if (currentDiv.hasClass('fa-check-square-o') && flag) {
                                currentDiv.removeClass('fa-check-square-o');
                                currentDiv.addClass('fa-square-o');
                            }
                            else {
                                currentDiv.removeClass('fa-square-o');
                                currentDiv.addClass('fa-check-square-o');
                            }
                        } else {
                            p_notification(data.status, data.msg);
                        }
                    }
                });
            }
        });
    }

    $('#btnUpdate').on('click',function(){
        teamCode = $('#teamCode').val();
        teamName = $('#teamName').val();
        teamEmployees = $('#teamEmployee').val();
        
        if(teamCode.trim() && teamName.trim() && teamEmployees != null){
            
            if(jQuery.inArray("0", teamEmployees) !== -1) {
                p_notification(false, eb.getMessage('ERR_EMP_REQUIRED'));
                return;
            }

            eb.ajax({
                type: 'POST',
                url: BASE_URL + '/team-api/update',
                data: {
                    teamCode : teamCode,
                    teamName : teamName,
                    teamEmployees : teamEmployees,
                    teamID : teamId
                },
                success: function(respond) {
                    p_notification(respond.status, respond.msg);                
                    if(respond.status){
                        $('#departmentCode').val('');
                        $('#departmentName').val('');
                        window.setTimeout(function() {
                                location.reload();
                        }, 1000);                  
                    }                
                }
            });
        } else {
            p_notification(false, eb.getMessage('ERR_TEAM_REQUIRED'));
        }
    });

    //for search
    $('#searchTm').on('click', function() {
        var key = $('#teamSearch').val();

        if (key.trim()) {
            getTeamSearchList(key);
        } else {
            p_notification(false, eb.getMessage('ERR_TEAM_SEARCH_KEY'));
        }
        
    });

    //for rest btn
    $('#btnReset').on('click',function (){
        $('input[type=text]').val('');
        $('select[name=teamEmp]').val('');
        $('.selectpicker').selectpicker('refresh');
    });  

    //for search cancel btn
    $('#searchCancel').on('click',function (){
        $('#teamSearch').val('');
        var key = $('#teamSearch').val();
        getTeamSearchList(key);
    });

    function getTeamEmployees(id, state = null) {
        var employeeIds = [];            
        $.ajax({
            type: 'GET',
            data: {
                teamID: id
            },
            url: BASE_URL + '/team-api/getTeamEmployees',
            success: function(respond) {
                if (state == null) {
                    for (var i = 0; i < respond.data.length; i++) {
                       employeeIds.push(parseInt(respond.data[i]['employeeID']));
                    }
                    $('select[name=teamEmp]').selectpicker('val', employeeIds);
                }
                else {
                    $('.employeeViewModalBody').html(respond.html);
                    $('#employeeViewModal').modal('show');
                }
                
            }
        });
    }

    function getTeamSearchList(key) {
        $.ajax({
            type: 'POST',
            url: BASE_URL + '/team-api/search',
            data: {
                searchKey : key
            },
            success: function(data) {
                if (data.status == true) {
                    if (data.status) {
                        $("#team-list").html(data.html);
                    } else {
                        p_notification(data.status, data.msg);
                    }
                }            
            }
        });
    }

    function deleteTeam(teamId) {
        bootbox.confirm('Are you sure you want to delete this Team?', function($result) {
            if ($result == true) {
                eb.ajax({
                    url: BASE_URL + '/team-api/deleteTeamByTeamID',
                    method: 'post',
                    data: {
                        teamID: teamId,
                    },
                    dataType: 'json',
                    success: function(data) {
                        if (data.status == true) {
                            p_notification(data.status, data.msg);
                            window.location.reload();
                        } else {
                            p_notification(data.status, data.msg);
                        }
                    }
                });
            }
        });
    }
});
