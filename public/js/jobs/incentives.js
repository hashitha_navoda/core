var vehicleTypeID = 0;
var taskID = 0;
var dirtyFlag = false;
var serviceList = [];
var serviceListID = 0;

$(document).ready(function() {

    $('td.sub-task').find("table").hide();
    $("#serviceListTbody").click('.showSubTask', function (e) {
        event.stopPropagation();
        var currentSpan = $(e.target).closest('span');
        var selectedTaskId = $(e.target).closest('tr').attr('id');

        if (currentSpan.hasClass('fa-square-o')) {
            //getRelatedSubTasks(selectedTaskId);
            currentSpan.removeClass('fa-square-o');
            currentSpan.addClass('fa-check-square-o');
            $(e.target).closest("tr").next().find("table").slideToggle();
        } else {
            currentSpan.removeClass('fa-check-square-o');
            currentSpan.addClass('fa-square-o');
            $(e.target).closest("tr").next().find("table").slideUp();
        }
        return false;
    })
    var $serviceList = $('#serviceList');
    var $serviceListTbody = $('#serviceListTbody', $serviceList);
    var $subTaskRow = $('tr.sub-task-row', $serviceListTbody);
    var $serviceListSampleRow = $('tr.sample', $serviceListTbody).add($subTaskRow);

    loadDropDownFromDatabase('/api/resources/searchVehicleTypesForDropdown', "", 0, '#vehicleType');
    $('#vehicleType').on('change', function() {
        if (!($(this).val() == null || $(this).val() == 0) && vehicleTypeID != $(this).val()){
            if (dirtyFlag) {
                p_notification(false, eb.getMessage('ERR_JOB_CRD_CHG_WITHOUT_SAVE'));
                $(this).val(0);
                $(this).selectpicker('refresh');
            } else {
                vehicleTypeID = $(this).val();
                dirtyFlag = false;
                requestServiceList(vehicleTypeID);
            }
        }

        if ($(this).val() != 0) {
            serviceId = $(this).val();
            //getRelatedSubTasks($(this).val());
        }
    });

    $serviceListTbody.on('click', '.add', function(e) {
        $('#save', $(this).parents('tr')).trigger('click');
    });

    $serviceListTbody.on('click', '.edit', function(e) {
        e.preventDefault();
        var $tr = $(this).parents('tr');
        console.log($tr);
        $('#incentiveRate', $tr).attr('disabled', false);
        $('#incentiveFor', $tr).attr('disabled', false);
        $('#incentiveFor', $tr).selectpicker('refresh');
        $('#edit', $tr).addClass('hidden');
        $('#save', $tr).removeClass('hidden');
        dirtyFlag = true;
    });

    $serviceListTbody.on('click', '.save', function(e) {
        var $tr = $(this).parents('tr');
        var data = {
            taskCardTaskID: $tr.attr('id'),
            taskID: $('#serviceId', $tr).val(),
            taskText: $('#serviceName', $tr).val(),
            taskCardTaskIncentiveValue: parseFloat($('#incentiveRate', $tr).val()),
            taskCardTaskIncentiveType: $('#incentiveFor', $tr).val(),
        };

        var format = accounting.formatMoney($('#incentiveRate', $tr).val());
        $('#incentiveRate', $tr).val(format);

        if (serviceRowValidation(data)) {
            index = serviceList.findIndex((obj => obj.taskCardTaskID == data.taskCardTaskID));
            serviceList[index] = data;
            $('#incentiveRate', $tr).attr('disabled', true);
            $('#incentiveFor', $tr).attr('disabled', true);
            $('#incentiveFor', $tr).selectpicker('refresh');
            $('#edit', $tr).removeClass('hidden');
            $('#save', $tr).addClass('hidden');
            $('#add', $tr).addClass('hidden');
            dirtyFlag = true;
        }
    });

    $serviceListTbody.on('click', '.delete', function(e) {
        var $tr = $(this).parents('tr');
        var taskID = $tr.attr('id');
        serviceList = $.grep(serviceList, function(data, index) {
            $('#serviceId', $tr).val();
            return data.taskCardTaskID != taskID;
        });
        dirtyFlag = true;
        refreshServiceList();
    });

    $serviceList.on('click', '#saveAllService', function(e) {
        e.preventDefault();
        if($('.edit', $serviceListTbody).hasClass('hidden')) {
            p_notification(false, eb.getMessage('ERR_UN_SVD_SER_ROW'));
        } else {
            eb.ajax({
                type: 'POST',
                url: BASE_URL + '/api/incentives/saveServiceIncentives',
                data: {
                    jobCardId: serviceListID,
                    serviceList: serviceList,
                },
                success: function(respond) {
                    p_notification(respond.status, respond.msg);
                    if (respond.status) {
                        dirtyFlag = false;
                    }
                }
            });
        }
    });

    function requestServiceList(vehicleTypeID) {
        eb.ajax({
            type: 'POST',
            url: BASE_URL + '/api/incentives/getServiceList',
            data: {
                vehicleTypeID: vehicleTypeID
            },
            success: function(respond) {
                if (respond.status) {
                    serviceList = respond.data;
                    serviceListID = vehicleTypeID;
                    refreshServiceList();
                } else {
                    p_notification(respond.status, respond.msg);
                }
            }
        });
    }

    // THIS FUNCTION CAN BE USED TO LOAD THE SUB TASKS. CHECK LINE 16
    /*function getRelatedSubTasks(Id) {
        eb.ajax({
            type: 'POST', url: BASE_URL + '/serviceSetup-api/getSubTaskByTaskId',
            data: { taskID: Id },
            success: function (respond) {
                if (respond.status == true) {
                    $('#addServiceRateModal #viewServiceSubTasks .viewedSubTasks').remove();

                    for (var i = 0; i < respond.data.length; i++) {
                        var newTrID = 'trs_' + respond.data[i]['subTaskID'];
                        var clonedRow = $($('#subTskRow').clone()).attr('id', newTrID).addClass('viewedSubTasks').data('sub-task-id', respond.data[i]['subTaskID']);
                        $("#subTskCode", clonedRow).val(respond.data[i]['code']).attr('disabled', 'disabled');
                        $("#subTskRate", clonedRow).val(respond.data[i]['rate']);
                        clonedRow.insertBefore('#subTskRow');
                    }

                }
            }
        });
    }*/

    function refreshServiceList() {
        $('#saveAllService', $serviceList).addClass('hidden');
        $('.serviceRow', $serviceListTbody).remove();
        if (Object.keys(serviceList).length > 0) {
            $.each(serviceList, function(index, value) {
                var $newRow = $($serviceListSampleRow.clone()).appendTo($serviceListTbody).attr('id', value.taskCardTaskID);
                $('#serviceCode', $newRow).val(value.taskCode);
                $('#serviceName', $newRow).val(value.taskName);
                $('#serviceId', $newRow).val(value.taskID);
                $('#jobCode', $newRow).val(value.jobCardCode);
                if (value.taskCardTaskIncentiveValue == null) {
                    $('#incentiveRate', $newRow).val('');
                    $('#edit', $newRow).trigger('click');
                    // $('#incentiveRate', $newRow).attr('disabled', false);
                    // $('#incentiveFor', $newRow).attr('disabled', false);
                    // $('#incentiveFor', $newRow).selectpicker('refresh');
                    $('#save', $newRow).addClass('hidden');
                    $('#add', $newRow).removeClass('hidden');
                    dirtyFlag = true;
                } else {
                    $('#incentiveRate', $newRow).val(accounting.formatMoney(parseFloat(value.taskCardTaskIncentiveValue)));
                }

                $('#incentiveFor', $newRow).val(value.taskCardTaskIncentiveType);
                $('#incentiveFor', $newRow).selectpicker('refresh');
                $newRow.removeClass('sample hidden');
                $newRow.addClass('serviceRow');
            });
            $('#saveAllService', $serviceList).removeClass('hidden');
        } else {
            var $newRow = $($serviceListSampleRow.clone()).appendTo($serviceListTbody);
            $newRow.html('<td colspan="5">No results were found.</td>');
            $newRow.removeClass('sample hidden');
            $newRow.addClass('not_found serviceRow');
        }
    }

    function serviceRowValidation(data) {
        if (data.taskCardTaskID == null || data.taskCardTaskID == '') {
            p_notification(false, eb.getMessage('ERR_INV_INC_TCTID_USR_INPUT'));
            return false;
        }
        if (data.taskID == null || data.taskID == '' || data.taskID == '0' || data.taskID == 0 || data.taskText == null || data.taskText == '') {
            p_notification(false, eb.getMessage('ERR_INV_INC_TID_USR_INPUT'));
            return false;
        }
        if (data.taskCardTaskIncentiveValue == null || data.taskCardTaskIncentiveValue == '' || !$.isNumeric(data.taskCardTaskIncentiveValue) || data.taskCardTaskIncentiveValue < 0) {
            p_notification(false, eb.getMessage('ERR_INV_INC_RATE_USR_INPUT'));
            return false;
        }
        if (data.taskCardTaskIncentiveType == null || data.taskCardTaskIncentiveType == '' || data.taskCardTaskIncentiveType == '0' || data.taskCardTaskIncentiveType == 0) {
            p_notification(false, eb.getMessage('ERR_INV_INC_TYP_USR_INPUT'));
            return false;
        }
        return true;
    }
});
