$(function() {

    var jobID = null;
    var jobTaskID = null;
    var deleteAttmpJobId = null;
    var selectRestartServices = {};

    loadDropDownFromDatabase('/job-card-setup-api/searchJobCardsForDropdownForDashboard', "", 0, '#jobCard');
    $('#jobCard').selectpicker('refresh');
    loadDropDownFromDatabase('/task_setup_api/searchTasksForDropdown', "", '', '#serviceType');
    $('#serviceType').selectpicker('refresh');

    var startdateFormat = $('#serviceStartingDate').data('date-format');
    $('#serviceStartingDate').datetimepicker({
        format: startdateFormat + ' hh:ii'
    }).on('change', function(ev) {
        $('#serviceStartingDate').datetimepicker('hide');
        setDates(true);
    });

    var enddateFormat = $('#serviceEndingDate').data('date-format');
    $('#serviceEndingDate').datetimepicker({
        format: enddateFormat + ' hh:ii',
    }).on('change', function(ev) {
        $('#serviceEndingDate').datetimepicker('hide');
        if ($('#serviceStartingDate').val()) {
            setDates(true);
        } else {
            p_notification(false, eb.getMessage('ERR_START_DATE_MISSING'));
            $('#serviceStartingDate').focus();
            $('#serviceEndingDate').val('');
            return false;
        }
    });

    function setDates(falg) {
        var startingTime = $('#serviceStartingDate').datetimepicker('getDate');
        var endingTime = $('#serviceEndingDate').datetimepicker('getDate');
        if ($('#serviceStartingDate').val() && $('#serviceEndingDate').val()) {
            if (startingTime.getTime() > endingTime.getTime()) {
                p_notification(false, eb.getMessage('ERR_JOB_ST_DATE_CANT_BE_MORE_THAN_ED_DATE'));
                $('#serviceStartingDate').focus();
                return false;
            }
        }
    }

    $('#searchJob').on('click', function(e) {
        e.preventDefault();
        var formData = {
            jobSearchKey: $('#job-search-keyword').val(),
            status : $('#jbStatus').val()
        };

        if (formData.jobSearchKey != '') {
            searchValue(formData);
        } else {
            p_notification(false, eb.getMessage('ERR_JBTYPE_SEARCH_KEY'));
        }
    });

    $('#resetJob').on('click', function(e) {
        e.preventDefault();
        $('#job-search-keyword').val('');
        $('#serviceStartingDate').val('');
        $('#serviceEndingDate').val('');
        var formData = {
            status : $('#jbStatus').val()
        };
        clearJobCardSearchDropdown();
        clearServiceSearchDropdown();
        searchValue(formData);
    });

    function searchValue(formData) {
        eb.ajax({
            url: BASE_URL + '/service-job-api/getJobBySearchKey',
            method: 'post',
            data: formData,
            dataType: 'json',
            success: function(data) {
                if (data.status) {
                    $("#search-job-list").html(data.html);
                } else {
                    p_notification(data.status, data.msg);
                }
            }
        });
    }
    
    $( "#jobCard, #serviceType, #serviceEndingDate").change(function() {
        var formData = {
            jobSearchKey: $('#job-search-keyword').val(),
            jobCard:($('#jobCard').val() != 0 && $('#jobCard').val() != null) ? $('#jobCard').val() : null,
            serviceType: ($('#serviceType').val() != 0 && $('#serviceType').val() != null) ? $('#serviceType').val() : null,
            toDate: ($('#serviceEndingDate').val()) ? $('#serviceEndingDate').val() : null,
            fromDate: ($('#serviceStartingDate').val()) ? $('#serviceStartingDate').val() : null,
            status : $('#jbStatus').val()
        }

        searchValue(formData);
    });


    $('#search-job-list').on('click','.serviceView', function(){
        jobID = $(this).closest('tr').data('jobid');
        jobSupervisorName= $(this).closest('tr').data('job-supervisor');
        $('#jobSupervisorName').text(jobSupervisorName);

        getJobRelatedServices(jobID);
    });

    $('#search-job-list').on('click','.restart_view', function(){
        selectRestartServices = {};

        jobID = $(this).closest('tr').data('jobid');
        getJobRelatedServices(jobID, true);
        
    });

    $('#serviceListModal').on('click','.select-restart-service', function(){

            var currentDiv = $(this).contents();
            var selectedJobTaskId = $(this).closest('tr').attr('id');
            if ($('#'+selectedJobTaskId +' #icon').hasClass('fa-check-square-o')) {
                $('#'+selectedJobTaskId +' #icon').removeClass('fa-check-square-o');
                $('#'+selectedJobTaskId +' #icon').addClass('fa-square-o');
                delete selectRestartServices[selectedJobTaskId];
            }
            else {
                $('#'+selectedJobTaskId +' #icon').removeClass('fa-square-o');
                $('#'+selectedJobTaskId +' #icon').addClass('fa-check-square-o');
                selectRestartServices[selectedJobTaskId] = selectedJobTaskId;
            }
    });


    function getJobRelatedServices(jobId, restartView = false) {
        eb.ajax({
            type: 'POST', url: BASE_URL + '/service-job-api/getJobServices', 
            data: {jobId: jobID}, 
            success: function(respond) {
                if (respond.status == true) {
                    $("#service-list").html(respond.html);

                    if (restartView) {
                        $('#serviceListModal, #viewServiceBody').addClass('hidden');
                        $('#serviceModalInner').addClass('service-restart-modal');
                        $('#serviceListModal, #viewServiceHeader').addClass('hidden');
                        $('#serviceListModal, #serviceRestartBody').removeClass('hidden');
                        $('#serviceListModal, #serviceRestartHeader').removeClass('hidden');
                        $('#serviceListModal, #btnRestart').removeClass('hidden');
                        
                    } else {
                        $('#serviceListModal #btnRestart').addClass('hidden');
                        $('#serviceModalInner').removeClass('service-restart-modal');
                    }
                    $('#serviceListModal').modal('toggle');
                } 
            }
        });

    }

    function clearJobCardSearchDropdown() {
        $('#jobCard')
            .empty()
            .val('')
            .append($("<option></option>")
                    .attr("value", "")
                    .text('Filter by job card'))
            .selectpicker('refresh')
            .trigger('change');
    }

    function clearServiceSearchDropdown() {
        $('#serviceType')
            .empty()
            .val('')
            .append($("<option></option>")
                    .attr("value", "")
                    .text('Filter by service'))
            .selectpicker('refresh')
            .trigger('change');
    }

    $('#btnViewServiceClose').on('click', function(){
        $('#serviceListModal').modal('toggle');
    });

    $('.delete-job').on('click', function(e) {
        e.preventDefault();

        deleteAttmpJobId = $(this).parents('tr').data('jobid');
        $("#myModal").modal('show');

    });

    $('#normal-job-cancel').on('click', function(e) {
        $("#myModal").modal('hide');
        canceljob(false);

    });

    $('#job-cancel-with-item-return').on('click', function(e) {
        $("#myModal").modal('hide');
        canceljob(true);
    });

    $('#back-to-dashboard').on('click', function(e) {
        $("#myModal").modal('hide');
    });


    function canceljob(returnItems) {
      eb.ajax({
          url: BASE_URL + '/service-job-api/cancelJob',
          method: 'post',
          data: {
              jobID: deleteAttmpJobId,
              returnDeliveryNote : returnItems
          },
          dataType: 'json',
          success: function(data) {
              if (data.status == true) {
                  p_notification(data.status, data.msg);
                  window.location.reload();
              } else {
                  p_notification(data.status, data.msg);
              }
          }
      });

    }



    $('#serviceListModal #service-list').on('click','.employee-view', function() {
        jobTaskID = $(this).closest('tr').data('job-task-id');
        getJobTaskRelatedEmployees();

    }); 

    function getJobTaskRelatedEmployees() {
        eb.ajax({
            type: 'POST', url: BASE_URL + '/service-job-dashboard-api/getJobTaskRelatedEmployees', 
            data: {
                jobId: jobID,
                jobTaskID: jobTaskID
            }, 
            success: function(respond) {
                if (respond.status == true) {

                    $('#addEmployeeModal .addedEmployees').remove();
                    for (var i = 0; i < respond.data.length; i++) {
                        var newTrID = 'tr_' + respond.data[i]['employeeID'];
                        var clonedRow = $($('#jobEmpRow').clone()).attr('id', newTrID).addClass('addedEmployees');
                        $("#EmpName", clonedRow).text(respond.data[i]['employeeFirstName']);
                        $("#EmpCode", clonedRow).text(respond.data[i]['employeeCode']);
                        clonedRow.removeClass('hidden');
                        clonedRow.insertBefore('#jobEmpRow');
                    }

                    if (respond.data.length > 0) {
                        $('#addEmployeeModal').modal('toggle');
                    } else {
                        p_notification(false, eb.getMessage('ERR_NO_ANY_EMP_ASSIGN_SRVCE'));
                        return false;
                    }

                } 

            }
        });
    }

    $('#serviceListModal').on('click','#btnRestart', function() {

        var serviceArray = Object.keys(selectRestartServices);
        eb.ajax({
            type: 'POST', url: BASE_URL + '/service-job-api/restartJobRelatedServices', 
            data: {
                jobId: jobID,
                serviceArray: serviceArray
            }, 
            success: function(respond) {
                p_notification(respond.status, respond.msg);
                if (respond.status == true) {
                    $('#serviceListModal').modal('toggle');
                    var formData = {
                        status : $('#jbStatus').val()
                    };
                    searchValue(formData);
                } 

            }
        });

    }); 

});