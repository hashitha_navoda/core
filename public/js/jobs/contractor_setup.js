$(document).ready(function() {
	//Contractor save
    $("#contractorForm").submit(function(e){
        var contractorForm = $('#contractorForm');
        var editMode = contractorForm.find('#editMode').val();
        var contractorID = contractorForm.find('#editMode').attr('contractorID');
        var entityid = contractorForm.find('#editMode').attr('entityid');
        e.preventDefault();
        var formData = {
            'contractorFirstName' : $('#firstName').val(),
            'contractorSecondName' : $('#lastName').val(),
            'contractorTP' : $('#MobileNo').val(),
            'IsPermanentFlag' : $('#CharactorType').prop('checked'),
        };

        if (editMode === 'false') {
            var url = BASE_URL + '/api/contractor/create';
        } else {
            var url = BASE_URL + '/api/contractor/edit-contractor/' + contractorID + '/' + entityid;
        }
        if(contractorSetupValidatiion(formData)) {
            $.ajax({
                type: 'POST',
                url: url,
                data: formData,
                success: function(data) {
                    if (data.status == true) {
                        p_notification(data.status, data.msg);
                        if (data.status) {
                            window.setTimeout(function() {
                                location.reload();
                            }, 1000);
                        }
                    }            
                }
            });
        }
    });

    //Contractor edit in list
    $('#contractor-list').on('click', 'a.edit', function(e) {
        e.preventDefault();
        var tr = $(this).parents('tr');
        contractorID = tr.attr('contractorID');
        entityid = tr.attr('entityid');
        var firstName = tr.find('#firstName').text();
        var secondName = tr.find('#lastName').text();
        var contractorTP = tr.find('#contractorTP').text();
        var contractorType = tr.find('#isPermanentFlag').text();
        
        if(contractorType === "Temporary"){
            contractorType = false;
        }else{
            contractorType = true;
        }
        
        var contractorForm = $('#contractorForm');
        contractorForm.find('#addTitle').addClass('hidden');
        contractorForm.find('#updateTitle').removeClass('hidden');
        contractorForm.find('#btnAddNewContractor').text('Update');
        contractorForm.find('#editMode').val('true');
        contractorForm.find('#editMode').attr('contractorID', contractorID);
        contractorForm.find('#editMode').attr('entityid', entityid);
        contractorForm.find('#firstName').val(firstName);
        contractorForm.find('#lastName').val(secondName);
        contractorForm.find('#MobileNo').val(contractorTP);
        contractorForm.find('#CharactorType').prop('checked', contractorType);
    });

    //contractor delete in list
    $('#contractor-list').on('click', 'a.delete', function(e) {
        e.preventDefault();
        
        var tr = $(this).parents('tr');
        contractorID = tr.attr('contractorID');
        entityid = tr.attr('entityid');
        
        bootbox.confirm('Are you sure you want to delete this Contractor ?', function(result) {

            if (result === true) {
                eb.ajax({
                    url: BASE_URL + '/api/contractor/delete-contractor',
                    method: 'post',
                    data: {
                        contractorID: contractorID,
                        entityid: entityid,
                    },
                    dataType: 'json',
                    success: function(data) {
                        p_notification(data.status, data.msg);
                        if (data.status) {
                            tr.remove();
                        } 
                    }
                });
            }
        });
    });

    //for search contractor by name
    $('.contractor-search-form').submit(function(e){
        e.preventDefault();
        var key = $('#contractorSearch').val();
        $.ajax({
            type: 'POST',
            url: BASE_URL + '/api/contractor/search',
            data: {
                searchKey : key
            },
            success: function(data) {
                if (data.status == true) {
                    if (data.status) {
                        $("#contractor-list").html(data.html);
                    } else {
                        p_notification(data.status, data.msg);
                    }
                }            
            }
        });
    });

    //for search cancel btn
    $('#searchCancel').on('click',function (){
        $('#contractorSearch').val('');
        fetchAll();
    });

    //this function for fetach all the contractors
    function fetchAll(){
        $.ajax({
            type: 'POST',
            data: {
                searchKey: ''
            },
            url: BASE_URL + '/api/contractor/search',
            success: function(respond) {
                $('#contractor-list').html(respond.html);            
            }
        });
    }

    //contractor status change in list
    $('#contractor-list').on('click', 'a.status', function(e) {
        e.preventDefault();
        var tr = $(this).parents('tr');
        contractorID = tr.attr('contractorID');
        entityid = tr.attr('entityid');
        contractorStatus = tr.attr('contractorStatus');
        bootbox.confirm('Are you sure you want to change the status of this Contractor ?', function(result) {
            if (result === true) {
                eb.ajax({
                    url: BASE_URL + '/api/contractor/change-status-contractor',
                    method: 'post',
                    data: {
                        contractorID: contractorID,
                        contractorStatus: contractorStatus,
                        entityid: entityid,
                    },
                    dataType: 'json',
                    success: function(data) {
                        p_notification(data.status, data.msg);
                        window.setTimeout(function() {
                            location.reload();
                        }, 1000);
                    }
                });
            }
        });
    });

    //contractor-setup validation
	function contractorSetupValidatiion(data) {
        var mobileNo = /^(\+\d{1,3}[- ]?)?\d{10}$/;
		if (data.contractorFirstName == '') {
			p_notification(false, eb.getMessage('ERR_CONTRACTOR_FIRST_NAME'));
            return false;
		} else if (data.contractorSecondName == '') {
			p_notification(false, eb.getMessage('ERR_CONTRACTOR_SECOND_NAME'));
        	return false;
		} else if (data.contractorTP == '') {
			p_notification(false, eb.getMessage('ERR_CONTRACTOR_ASSIGN_MOBILE_NO'));
        	return false;
		} else if (!(data.contractorTP.match(mobileNo))) {
            p_notification(false, eb.getMessage('ERR_CONTRACTOR_ASSIGN_MOBILE_NO_CHARACTERS'));
        } else {
			return true;
		}
	}
});