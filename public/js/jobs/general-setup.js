var generalSetupWizard = false;
var admins = {};
var storeKeeper = {};
var userData = {};
$(document).ready(function(){
    //get active user
    eb.ajax({
        type: 'POST',
        url: BASE_URL + '/userAPI/getAllUserData',
        data: {},
        success: function(respond) {
            userData = respond.data;
        },
        async: false
    });

    if ($('#wizardStatus').val() == 1) {
        generalSetupWizard = true;
    }

    eb.ajax({
        url: BASE_URL + '/api/general_setup/get-general-setting',
        method: 'get',
        dataType: 'json',
        success: function(respond) {
            setGeneralSetting(respond.data);
        }
    });

    $('#contractorAndProject').hide();
    $('#showMaintainContractorAndProjects').on('click', function() {
        $("#showMaintainContractorAndProjects").toggleClass('expanded');
        $("#contractorAndProject").slideToggle();
        $('#resourceTypes').hide();
        $('#userRoles').hide();
    });

    $('#resourceTypes').hide();
    $('#showResourceTypes').on('click', function() {
        $("#showResourceTypes").toggleClass('expanded');
        $("#resourceTypes").slideToggle();
        $('#contractorAndProject').hide();
        $('#userRoles').hide();
    });

    $('#userRoles').hide();
    $('#showUserRoles').on('click', function() {
        $("#showUserRoles").toggleClass('expanded');
        $("#userRoles").slideToggle();
        $('#resourceTypes').hide();
        $('#contractorAndProject').hide();
    });

    $("form#genaralSettingForm").submit(function(e){
        e.preventDefault();
        var formData = getGeneralSetting();
        eb.ajax({
            url: BASE_URL + '/api/general_setup/save-general-setting',
            method: 'post',
            data: formData,
            dataType: 'json',
            success: function(data) {
                p_notification(data.status, data.msg);
                if (data.status) {
                    if (generalSetupWizard) {
                        window.location.assign(BASE_URL + "/jobs")
                    } else {
                        window.setTimeout(function() {
                            location.reload();
                        }, 1000);
                    }
                } else {
                    $('#genaralSettingForm').focus();
                }
            }
        });
    });

    function getGeneralSetting() {
        return {
            maintain_project : $('#maintainProjectFlag').prop('checked'),
            raw_materials : $('#rawMaterialsFlag').prop('checked'),
            finished_goods : $('#finishedGoodsFlag').prop('checked'),
            fixed_assets : $('#fixedAssetsFlag').prop('checked'),
            vehicles : $('#vehiclesFlag').prop('checked'),
            generalSetupWizard : generalSetupWizard,
            assign_for_jobs : $('#assignForJobsFlag').prop('checked'),
            assign_for_tasks : $('#assignForTasksFlag').prop('checked'),
            jobAdmins : admins,
            jobStoreKeepers : storeKeeper
        };
    }

    function setGeneralSetting(data) {
        $('#maintainProjectFlag').prop('checked', data.jobGeneralSettings['maintainProject']);
        $('#rawMaterialsFlag').prop('checked', data.jobGeneralSettings['rawMaterials']);
        $('#finishedGoodsFlag').prop('checked', data.jobGeneralSettings['finishedGoods']);
        $('#fixedAssetsFlag').prop('checked', data.jobGeneralSettings['fixedAssets']);
        $('#vehiclesFlag').prop('checked', data.jobGeneralSettings['vehicles']);
        $('#assignForJobsFlag').prop('checked', data.jobGeneralSettings['contractorForJob']);
        $('#assignForTasksFlag').prop('checked', data.jobGeneralSettings['contractorForTask']);

        $.each(data.adminData, function(index, val) {
            $('.admin-table').removeClass('hidden');
            setJobAdminList(val.userID);             
        });

        $.each(data.storeKeeperData, function(index, val) {
            $('.store-keeper-table').removeClass('hidden');
            setJobStoreKeeperList(val.userID);             
        });

    }

    $('#adminID').selectpicker('render').selectpicker('refresh');
    $('#adminID').trigger('change');
    var adminKey;
    $('#adminID').on('change', function() {
        if ($(this).val() > 0 && $(this).val() != adminKey) {
            adminKey = $(this).val();
            if (!admins[adminKey]) {
                $('.admin-table').removeClass('hidden');
                setJobAdminList(adminKey);
            } else {
                $('#adminID').val('select');
                $('#adminID').selectpicker('refresh');
                p_notification(false, eb.getMessage('ERR_ACT_OWNR_NAME_ALREDY_EXIST'));
                $('#adminID').focus();
            }
        }
    });

    function setJobAdminList(adminID) {
        var newTrID = 'tr_' + adminID;
        var clonedRow = $($('#adminPreSetSample').clone()).attr('id', newTrID).addClass('addedAdmin');
        $("#adminName", clonedRow).text(userData[adminID]);
        clonedRow.removeClass('hidden');
        clonedRow.insertBefore('#adminPreSetSample');
        $('#adminID').focus();
        admins[adminID] = new adminDetails(adminID, userData[adminID]);
        $('#adminID').selectpicker('refresh');
        $('#adminID').focus();
    }

    function adminDetails(adminID, adminFullName) {
        this.adminID = adminID;
        this.adminFullName = adminFullName;
    }

    $('#storeKeeperID').selectpicker('render').selectpicker('refresh');
    $('#storeKeeperID').trigger('change');
    var storeKeeperKey;
    $('#storeKeeperID').on('change', function() {
        if ($(this).val() > 0 && $(this).val() != storeKeeperKey) {
            storeKeeperKey = $(this).val();
            if (!storeKeeper[storeKeeperKey]) {
                $('.store-keeper-table').removeClass('hidden');
                setJobStoreKeeperList(storeKeeperKey);
            } else {
                $('#storeKeeperID').val('select');
                $('#storeKeeperID').selectpicker('refresh');
                p_notification(false, eb.getMessage('ERR_ACT_OWNR_NAME_ALREDY_EXIST'));
                $('#storeKeeperID').focus();
            }
        }
    });

    function setJobStoreKeeperList(storeKeeperID) {
        var newTrID = 'tr_' + storeKeeperID;
        var clonedRow = $($('#storeKeeperPreSetSample').clone()).attr('id', newTrID).addClass('addedStoreKeeper');
        $("#storeKeeperName", clonedRow).text(userData[storeKeeperID]);
        clonedRow.removeClass('hidden');
        clonedRow.insertBefore('#storeKeeperPreSetSample');
        $('#storeKeeperID').focus();
        storeKeeper[storeKeeperID] = new storeKeeperDetails(storeKeeperID, userData[storeKeeperID]);
        $('#storeKeeperID').selectpicker('refresh');
        $('#storeKeeperID').focus();
    }

    function storeKeeperDetails(storeKeeperID, storekeeperFullName) {
        this.storeKeeperID = storeKeeperID;
        this.storekeeperFullName = storekeeperFullName;
    }


    $(document).on('click', '.admin-delete', function() {
        var deleteAdminTrID = $(this).closest('tr').attr('id');
        var deleteAdminID = deleteAdminTrID.split('tr_')[1].trim();
        delete admins[deleteAdminID];
        $('.admin-table #' + deleteAdminTrID).remove();

        if (_.values(admins).length == 0) {
            $('.admin-table').addClass('hidden');
        }
    });

    $(document).on('click', '.store-keeper-delete', function() {
        var deleteSKTrID = $(this).closest('tr').attr('id');
        var deleteSkID = deleteSKTrID.split('tr_')[1].trim();
        delete storeKeeper[deleteSkID];
        $('.store-keeper-table #' + deleteSKTrID).remove();

        if (_.values(storeKeeper).length == 0) {
            $('.store-keeper-table').addClass('hidden');
        }
    });
});