var selectedParticipantID = null;
var selectedParticipantTypeID = null;
var selectedParticipantSubTypeID = null;
var activeParticipants = {};
var activeParticipantTypes = {};
var activeParticipantSubTypes = {};
var participants = {};
var teamParticipants = {};
$(document).ready(function() {
    var teamId   = null;
    var teamName = null;
    var teamCode = null;
    var teamEmployees = {};
    
    $.ajax({
        type: 'POST',
        url: BASE_URL + '/participant-api/getParticipants',
        data: {},
        success: function(respond) {
            activeParticipants = respond.data.participantData;       
            activeParticipantTypes = respond.data.participantTypeData;       
            activeParticipantSubTypes = respond.data.participantSubTypeData;       
        },
        async: false
    });

    $('#partiType').on('change', function(event) {
        event.preventDefault();
        if ($('#partiType').val() != null || $('#partiType').val() != 0) {
            eb.ajax({
                type: 'POST',
                url: BASE_URL + '/participant-api/getDepartmentWiseEmployees',
                data: {
                    participantTypeID : $('#partiType').val(),
                    participantSubTypeID : $('#partiSubType').val()
                },
                success: function(respond) {
                    if ($('#partiType').val() != 0 && $('#partiSubType').val() != 0) {
                        $("#teamEmployee").empty();
                        $("#teamEmployee").
                                    append($("<option></option>").
                                            attr("value", 0).
                                            text('Select Participants'));
                        $.each(respond.data, function(index, val) {
                            $("#teamEmployee").
                                    append($("<option></option>").
                                            attr("value", index).
                                            text(val));
                            $('#teamEmployee').selectpicker('refresh');
                        });     
                    } else {
                        $("#teamEmployee").empty();
                        $("#teamEmployee").
                                    append($("<option></option>").
                                            attr("value", 0).
                                            text('Select Participants'));
                        
                    } 
                }
            });
            if ($('#partiType').val() != 0 && $('#partiSubType').val() != 0) {
                $('#teamEmployee').attr('disabled',false);
                $('.selectpicker').selectpicker('refresh');
            } else {
                $('#teamEmployee').attr('disabled',true);
                $('.selectpicker').selectpicker('refresh');
            }
        }
    });

    $('#partiSubType').on('change', function(event) {
        event.preventDefault();
        if ($('#partiSubType').val() != null || $('#partiSubType').val() != 0) {
            eb.ajax({
                type: 'POST',
                url: BASE_URL + '/participant-api/getDepartmentWiseEmployees',
                data: {
                    participantTypeID : $('#partiType').val(),
                    participantSubTypeID : $('#partiSubType').val()
                },
                success: function(respond) {
                    if ($('#partiType').val() != 0 && $('#partiSubType').val() != 0) {
                        $("#teamEmployee").empty();
                        $("#teamEmployee").
                                    append($("<option></option>").
                                            attr("value", 0).
                                            text('Select Participants'));
                        $.each(respond.data, function(index, val) {
                            $("#teamEmployee").
                                    append($("<option></option>").
                                            attr("value", index).
                                            text(val));
                            $('#teamEmployee').selectpicker('refresh');
                        });
                    } else {
                        $("#teamEmployee").empty();
                        $("#teamEmployee").
                                    append($("<option></option>").
                                            attr("value", 0).
                                            text('Select Participants'));
                        
                    }        
                }
            });
            if ($('#partiType').val() != 0 && $('#partiSubType').val() != 0) {
                $('#teamEmployee').attr('disabled',false);
                $('.selectpicker').selectpicker('refresh');
            } else {
                $('#teamEmployee').attr('disabled',true);
                $('.selectpicker').selectpicker('refresh');
            }
        }
    });


    var participantKey;
    $('#teamEmployee').on('change', function(event) {
        event.preventDefault();
        if ($(this).val() > 0) {
            participantKey = $(this).val();
            selectedParticipantID = $(this).val();
            selectedParticipantTypeID = $('#partiType').val();
            selectedParticipantSubTpeID = $('#partiSubType').val();
            $('#partiType').val('0');
            $('#partiType').selectpicker('refresh');

            $('#partiSubType').val('0');
            $('#partiSubType').selectpicker('refresh');

            $('#teamEmployee').val('0');
            $('#teamEmployee').attr('disabled',true);
            $('#teamEmployee').selectpicker('refresh');

            var duplicateCheck = false;

            $.each(participants, function(index, val) {
                if (index == selectedParticipantTypeID +'-' + selectedParticipantSubTpeID + '-' + selectedParticipantID) {
                    duplicateCheck = true;                        
                }
            });

            if (!duplicateCheck) {
                selectParticipants(selectedParticipantID, selectedParticipantTypeID, selectedParticipantSubTpeID);
            } else {
                p_notification(false, eb.getMessage('ERR_PART_DUPLICATE'));
            }
        }
    });

    function selectParticipants(selectedParticipantID, selectedParticipantTypeID, selectedParticipantSubTpeID) {
        $('.participant-table').removeClass('hidden');
        setParticipantList(selectedParticipantID, selectedParticipantTypeID, selectedParticipantSubTpeID);
       
    }

    function setParticipantList(selectedParticipantID, selectedParticipantTypeID, selectedParticipantSubTpeID) {
        var newTrID = 'tr_' + selectedParticipantTypeID +'-' + selectedParticipantSubTpeID + '-' + selectedParticipantID;
        var clonedRow = $($('#participantPreSetSample').clone()).attr('id', newTrID).addClass('addedParticipant');
        $("#participantTypeName", clonedRow).text(activeParticipantTypes[selectedParticipantTypeID]);
        $("#participantSubTypeName", clonedRow).text(activeParticipantSubTypes[selectedParticipantSubTpeID]);
        $("#participantName", clonedRow).text(activeParticipants[selectedParticipantID]);
        clonedRow.removeClass('hidden');
        clonedRow.insertBefore('#participantPreSetSample');
        participants[selectedParticipantTypeID +'-' + selectedParticipantSubTpeID + '-' + selectedParticipantID] = new participant(selectedParticipantTypeID,selectedParticipantSubTpeID,selectedParticipantID);
    }

    function participant(participantTypeID, participantSubTypeID, participantID) {
        this.participantTypeID = participantTypeID;
        this.participantSubTypeID = participantSubTypeID;
        this.participantID = participantID;
    }

    $('#addParti').on('click', function(e) {
        e.preventDefault();

        // validate products before closing modal
        if (!participantModalValidate(e)) {
            e.stopPropagation();
            return false;
        } else {
            $('#addParticipantModal').modal('hide');
        }
    });

    function participantModalValidate(e) {
        var tempParticipants = {};
        if (!$.isEmptyObject(participants)) {
            $.each(participants, function(index, value) {
                tempParticipants[index] = value;
            });
        }

        if ($.isEmptyObject(tempParticipants)) {
            teamParticipants = {};
            return true;   
        } else {
            teamParticipants = tempParticipants;
            return true;
        }
    }



    $('#addParticipants').on('click',function(){
        participants = {};
        $("#addParticipantModal #partTable tbody .addedParticipant").remove();
        if ($.isEmptyObject(teamParticipants)) {
            $('.participant-table').addClass('hidden');
        } else {
            $.each(teamParticipants, function(index, val) {
                participants[index] = val;
                setParticipantList(val.participantID, val.participantTypeID, val.participantSubTypeID);
            });
            $('.participant-table').removeClass('hidden');
        }
        $("#addParticipantModal").modal('toggle');
    });

    $(document).on('click', '.participant-delete', function() {
        var deleteTrID = $(this).closest('tr').attr('id');
        var deletePartID = deleteTrID.split('tr_')[1].trim();
        delete participants[deletePartID];
        $('#' + deleteTrID).remove();

        if (_.values(participants).length == 0) {
            $('.participant-table').addClass('hidden');
        }
    });

    $('#btnAdd').on('click',function(){
        teamCode = $('#teamCode').val();
        teamName = $('#teamName').val();
        teamEmployees = teamParticipants;

        if(teamCode.trim() && teamName.trim() && teamEmployees != {}){      
            if(jQuery.inArray("0", teamEmployees) !== -1) {
                p_notification(false, eb.getMessage('ERR_TEAM_REQUIRED'));
                return;
            }

            eb.ajax({
                type: 'POST',
                url: BASE_URL + '/tournament-team-api/createTeam',
                data: {
                    teamCode : teamCode,
                    teamName : teamName,
                    teamEmployees : teamEmployees
                },
                success: function(respond) {
                    p_notification(respond.status, respond.msg);                
                    if(respond.status){
                        $('#teamCode').val('');
                        $('#teamName').val('');
                        $('#teamEmployee').val('');
                        window.setTimeout(function() {
                                location.reload();
                        }, 1000);                    
                    }                
                }
            });
        } else {
            p_notification(false, eb.getMessage('ERR_TEAM_REQUIRED'));
        }
    });

    //for cancel btn
    $('#btnCancel').on('click',function (){
        $('input[type=text]').val('');
        $('select[name=teamEmp]').val('');
        $('.selectpicker').selectpicker('refresh');      
        teamRegisterView();        
    });

    $('#team-list').on('click','.team-action',function (e){    
        var action = $(this).data('action');
        teamId   = $(this).closest('tr').data('team-id');
        teamName = $(this).closest('tr').data('team-name');
        teamCode = $(this).closest('tr').data('team-code');
        status = $(this).closest('tr').data('team-status');

        switch(action) {
            case 'edit':
                e.preventDefault();
                teamUpdateView();

                //set selected team details
                $('#teamCode').val(teamCode);
                $('#teamName').val(teamName);
                getTeamEmployees(teamId);
                break;

            case 'status':
                var msg;
                var status;
                var currentDiv = $(this).contents();
                var flag = true;
                if ($(this).children().hasClass('fa-check-square-o')) {
                    msg = 'Are you sure you want to Deactivate this team';
                    status = '0';
                }
                else if ($(this).children().hasClass('fa-square-o')) {
                    msg = 'Are you sure you want to Activate this team';
                    status = '1';
                }
                activeInactive(teamId, status, currentDiv, msg, flag);
                break;
            case 'view':
                getTeamEmployees(teamId, 'loadModal');
                break;
            case 'delete':
                deleteTeam(teamId);
                break;
            default:
                console.error('Invalid action.');
                break;
        }
    });

    //this function for switch to team register view
    function teamRegisterView(){
       $('.updateTitle').addClass('hidden');
       $('.addTitle').removeClass('hidden');
       $('.updateDiv').addClass('hidden');
       $('.addDiv').removeClass('hidden');
    }

    //this function for switch to team update view
    function teamUpdateView(){
       $('.addTitle').addClass('hidden');
       $('.updateTitle').removeClass('hidden');
       $('.addDiv').addClass('hidden');
       $('.updateDiv').removeClass('hidden');
    }

    /**
     *
     *use to change active state
     */
    function activeInactive(type, status, currentDiv, msg, flag) {
        bootbox.confirm(msg, function($result) {
            if ($result == true) {
                eb.ajax({
                    url: BASE_URL + '/tournament-team-api/changeStatusID',
                    method: 'post',
                    dataType: 'json',
                    data: {
                        'teamID': type,
                        'status': status,
                    },
                    success: function(data) {
                        if (data.status) {
                            p_notification(data.status, data.msg);
                            if (currentDiv.hasClass('fa-check-square-o') && flag) {
                                currentDiv.removeClass('fa-check-square-o');
                                currentDiv.addClass('fa-square-o');
                            }
                            else {
                                currentDiv.removeClass('fa-square-o');
                                currentDiv.addClass('fa-check-square-o');
                            }
                        } else {
                            p_notification(data.status, data.msg);
                        }
                    }
                });
            }
        });
    }

    $('#btnUpdate').on('click',function(){
        teamCode = $('#teamCode').val();
        teamName = $('#teamName').val();
        teamEmployees = teamParticipants;
        
        if(teamCode.trim() && teamName.trim() && teamEmployees != {}){
            
            if($.isEmptyObject(teamEmployees)) {
                p_notification(false, eb.getMessage('ERR_EMP_REQUIRED'));
                return;
            }

            eb.ajax({
                type: 'POST',
                url: BASE_URL + '/tournament-team-api/update',
                data: {
                    teamCode : teamCode,
                    teamName : teamName,
                    teamEmployees : teamEmployees,
                    teamID : teamId
                },
                success: function(respond) {
                    p_notification(respond.status, respond.msg);                
                    if(respond.status){
                        $('#departmentCode').val('');
                        $('#departmentName').val('');
                        window.setTimeout(function() {
                                location.reload();
                        }, 1000);                  
                    }                
                }
            });
        } else {
            p_notification(false, eb.getMessage('ERR_TEAM_REQUIRED'));
        }
    });

    //for search
    $('#searchTm').on('click', function() {
        var key = $('#teamSearch').val();

        if (key.trim()) {
            getTeamSearchList(key);
        } else {
            p_notification(false, eb.getMessage('ERR_TEAM_SEARCH_KEY'));
        }
        
    });

    //for rest btn
    $('#btnReset').on('click',function (){
        $('input[type=text]').val('');
        $('select[name=teamEmp]').val('');
        $('.selectpicker').selectpicker('refresh');
    });  

    //for search cancel btn
    $('#searchCancel').on('click',function (){
        $('#teamSearch').val('');
        var key = $('#teamSearch').val();
        getTeamSearchList(key);
    });

    function getTeamEmployees(id, state = null) {
        var employeeIds = [];            
        $.ajax({
            type: 'GET',
            data: {
                teamID: id
            },
            url: BASE_URL + '/tournament-team-api/getTeamPartcipants',
            success: function(respond) {
                if (state == null) {
                    $.each(respond.data, function(index, val) {
                        teamParticipants[val.participantTypeID +'-' + val.participantSubTypeID + '-' + val.employeeID] = new participant(val.participantTypeID,val.participantSubTypeID,val.employeeID);
                    });
                }
                else {
                    $('.employeeViewModalBody').html(respond.html);
                    $('#employeeViewModal').modal('show');
                }
                
            }
        });
    }

    function getTeamSearchList(key) {
        $.ajax({
            type: 'POST',
            url: BASE_URL + '/tournament-team-api/searchParticipant',
            data: {
                searchKey : key
            },
            success: function(data) {
                if (data.status == true) {
                    if (data.status) {
                        $("#team-list").html(data.html);
                    } else {
                        p_notification(data.status, data.msg);
                    }
                }            
            }
        });
    }

    function deleteTeam(teamId) {
        bootbox.confirm('Are you sure you want to delete this Team?', function($result) {
            if ($result == true) {
                eb.ajax({
                    url: BASE_URL + '/tournament-team-api/deleteTeamByTeamID',
                    method: 'post',
                    data: {
                        teamID: teamId,
                    },
                    dataType: 'json',
                    success: function(data) {
                        if (data.status == true) {
                            p_notification(data.status, data.msg);
                            window.location.reload();
                        } else {
                            p_notification(data.status, data.msg);
                        }
                    }
                });
            }
        });
    }
});
