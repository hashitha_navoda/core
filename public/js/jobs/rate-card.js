$(document).ready(function() {

    var serviceId   = null;
    var serviceNameCode = null;
    var serviceRate = null;
    var serviceUOM = null;
    var serviceUOMText = null;
    var departmentStationCode = null;
    var departmentStationName = null;
    var departmentStations = {};
    var mainDepartmentStations = {};
    var alldepartmentStations = {};
    var serviceRates = {};
    var mainServiceRates = {};
    var subTaskRates = {};
    var editSubTaskRate = null;
    var rateCardId = null;
 
    loadDropDownFromDatabase('/task_setup_api/searchTasksForDropdown', "", 1, '#serviceCode', '', '', false);
    $('#serviceCode').on('change', function() {

        if ( $(this).val() in serviceRates && $(this).val() != 0) {
            p_notification(false, eb.getMessage('ERR_SRVC_DUPLICATE'));
            clearServiceCode();
            return;
        }

        if ($(this).val() != 0) {
            serviceId = $(this).val();
            getRelatedSubTasks($(this).val());
        }
    });


    
    $('#btnAdd').on('click',function(){
        var rateCardCode = $('#rateCardCode').val();
        var rateCardName = $('#rateCardName').val();

        var formData = {
            rateCardCode: rateCardCode,
            rateCardName: rateCardName
        }
    
        if(validateFormData(formData)){
            eb.ajax({
                type: 'POST',
                url: BASE_URL + '/rate-card-api/createRateCard',
                data: {
                    rateCardCode : rateCardCode,
                    rateCardName : rateCardName,
                    mainServiceRates: mainServiceRates
                    
                },
                success: function(respond) {
                    p_notification(respond.status, respond.msg);                
                    if(respond.status){
                        $('#departmentCode').val('');
                        $('#departmentName').val('');
                        window.setTimeout(function() {
                                location.reload();
                        }, 1000);                    
                    }                
                }
            });
        } 
    });

    //for cancel btn
    $('#btnCancel, #btnViewCancel').on('click',function (){
        $('input[type=text]').val('');  
        $('#servicesRates').text('Add rates for services');      
        rateCardRegisterView();        
    });

    $('#rate-card-list').on('click','.rateCard-action',function (e){    
        var action = $(this).data('action');
        rateCardId   = $(this).closest('tr').data('rate-card-id');
        rateCardName = $(this).closest('tr').data('rate-card-name');
        rateCardCode = $(this).closest('tr').data('rate-card-code');
        rateCardStatus = $(this).closest('tr').data('rate-card-status');

        switch(action) {
            case 'edit':
                e.preventDefault();
                rateCardUpdateView();
                //set selected department details
                $('#rateCardCode').val(rateCardCode);
                $('#rateCardName').val(rateCardName);
                getRelatedServiceRates(rateCardId);
                break;
            case 'delete':
                deleteRateCard(rateCardId);
                break;
            case 'status':
                var msg;
                var status;
                var currentDiv = $(this).contents();
                var flag = true;
                if ($(this).children().hasClass('fa-check-square-o')) {
                    msg = 'Are you sure you want to Deactivate this rate card';
                    status = '0';
                }
                else if ($(this).children().hasClass('fa-square-o')) {
                    msg = 'Are you sure you want to Activate this rate card';
                    status = '1';
                }
                activeInactive(rateCardId, status, currentDiv, msg, flag);
                break;
            case 'view':
                e.preventDefault();
                rateCardView();
                //set selected department details
                $('#rateCardCode').val(rateCardCode).attr('disabled', 'disabled');
                $('#rateCardName').val(rateCardName).attr('disabled', 'disabled');
                getRelatedServiceRates(rateCardId);
                $('#addServiceRateModal #addNewServiceRateRow').addClass('hidden');
                $('#addServiceRateModal .deleteAddedServiceRates').addClass('hidden');
                $('#addServiceRateModal #addServiceRates').addClass('hidden');
                $('#addServiceRateModal #viewServiceRates').removeClass('hidden');
                $('#btnViewRateCardServicesClose').removeClass('hidden');
                $('#servicesRates').text('View rates for services');
                break;

            default:
                console.error('Invalid action.');
                break;
        }
    });

    //this function for switch to rate card register view
    function rateCardRegisterView(){
        $('.updateTitle').addClass('hidden');
        $('.addTitle').removeClass('hidden');
        $('.updateDiv').addClass('hidden');
        $('.addDiv').removeClass('hidden');
        $('.viewTitle').addClass('hidden');
        $('.viewDiv').addClass('hidden');
        $('#rateCardCode').attr('disabled', false);
        $('#rateCardName').attr('disabled', false);

        $('#addServiceRateModal #addServiceRateBody .addedServiceRate').remove();
        $('#addServiceRateModal #addNewServiceRateRow').removeClass('hidden');
        $('#addServiceRateModal .deleteAddedServiceRates').removeClass('hidden');
        $('#addServiceRateModal #addServiceRates').removeClass('hidden');
        $('#addServiceRateModal #viewServiceRates').addClass('hidden');
        mainDepartmentStations = {};
        departmentStations = {};
    }

    //this function for switch to rate card update view
    function rateCardUpdateView(){
        $('.addTitle').addClass('hidden');
        $('.updateTitle').removeClass('hidden');
        $('.addDiv').addClass('hidden');
        $('.viewTitle').addClass('hidden');
        $('.viewDiv').addClass('hidden');
        $('.updateDiv').removeClass('hidden');
        $('#rateCardCode').attr('disabled', false);
        $('#rateCardName').attr('disabled', false);
        $('#addServiceRateModal #addServiceRateBody .addedServiceRate').remove();
        $('#addServiceRateModal #addNewServiceRateRow').removeClass('hidden');
        $('#addServiceRateModal .deleteAddedServiceRates').removeClass('hidden');
        $('#addServiceRateModal #addServiceRates').removeClass('hidden');
        $('#addServiceRateModal #viewServiceRates').addClass('hidden');
    }

    //this function for switch to rate card update view
    function rateCardView(){
       $('.addTitle').addClass('hidden');
       $('.updateTitle').addClass('hidden');
       $('.viewTitle').removeClass('hidden');
       $('.addDiv').addClass('hidden');
       $('.updateDiv').addClass('hidden');
       $('.viewDiv').removeClass('hidden');
    }

    $('#btnUpdate').on('click',function(){
        var rateCardCode = $('#rateCardCode').val();
        var rateCardName = $('#rateCardName').val();

        var formData = {
            rateCardCode: rateCardCode,
            rateCardName: rateCardName
        }

        if(validateFormData(formData)){
            eb.ajax({
                type: 'POST',
                url: BASE_URL + '/rate-card-api/updateRateCard',
                data: {
                    rateCardID: rateCardId,
                    rateCardCode : rateCardCode,
                    rateCardName : rateCardName,
                    mainServiceRates: mainServiceRates
                    
                },
                success: function(respond) {
                    p_notification(respond.status, respond.msg);                
                    if(respond.status){
                        $('#departmentCode').val('');
                        $('#departmentName').val('');
                        window.setTimeout(function() {
                                location.reload();
                        }, 1000);                    
                    }                
                }
            });
        } 
    });

    //for search
    $('#searchRteCrd').on('click', function() {
        var key = $('#RteCrdSearch').val();

        searchRateCard(key);
    });


    function searchRateCard(key) {  
        $.ajax({
            type: 'POST',
            url: BASE_URL + '/rate-card-api/search',
            data: {
                searchKey : key
            },
            success: function(data) {
                if (data.status == true) {
                    if (data.status) {
                        $("#rate-card-list").html(data.html);
                    } else {
                        p_notification(data.status, data.msg);
                    }
                }            
            }
        });  
    }

    //for rest btn
    $('#btnReset').on('click',function (){
        $('input[type=text]').val('');
        $('#addServiceRateModal #addServiceRateBody .addedServiceRate').remove();
        serviceRates = {};
        mainServiceRates = {};
        subTaskRates = {};
    });  

    //for search cancel btn
    $('#searchCancel').on('click',function (){
        $('#rateCardSearch').val('');
        searchRateCard();
    });

    
    function deleteRateCard(rateCardId) {
        bootbox.confirm('Are you sure you want to delete this rate card?', function($result) {
            if ($result == true) {
                eb.ajax({
                    url: BASE_URL + '/rate-card-api/deleteRateCard',
                    method: 'post',
                    data: {
                        rateCardID: rateCardId
                    },
                    dataType: 'json',
                    success: function(data) {
                        if (data.status == true) {
                            p_notification(data.status, data.msg);
                            window.location.reload();
                        } else {
                            p_notification(data.status, data.msg);
                        }
                    }
                });
            }
        });
    }

    $('#servicesRates').on('click', function() {
        reomoveUnAddedServiceRates();
    });

     $('#btnSrvcClose').on('click',function(){    
        reomoveUnAddedServiceRates();
    });

    //add new rate card service rate
    $('#add_dep_station').on('click', function(e) {
        e.preventDefault();

        //if plus button is clicked then disabled the button
        //if button is clicked more than one, then it does not continue

        if ($(this).is(':disabled')) {
            return false;
        } else {
            $(this).attr('disabled', true);
            var $thisRow = $(this).parents('tr');
            if (serviceId == null) {
                p_notification(false, eb.getMessage('PLS_SELECT_AT_LEAST_ONE_SRVC'));
                clearServiceCode();
                $(this).attr('disabled', false);
                return false;                
            } else if (!$('#serviceRateVal').val().trim()) {
                p_notification(false, eb.getMessage('ERR_REQUIRED_SRVC_RTE'));
                $(this).attr('disabled', false);
                return false; 
            }else if (isNaN(parseFloat($('#serviceRateVal').val()))) {
                p_notification(false, eb.getMessage('ERR_REQUIRED_NUM_FOR_SRVC_RTE'));
                $(this).attr('disabled', false);
                return false; 
            }else if ($('#uom').val() == "" || $('#uom').val() == null || $('#uom').val() == 0) {
                p_notification(false, eb.getMessage('ERR_REQUIRED_UOM'));
                $(this).attr('disabled', false);
                return false; 
            } else {
                serviceNameCode = $('#serviceCode option:selected').html()
                serviceRate = $('#serviceRateVal').val();
                serviceUOM = $('#uom').val();
                serviceUOMText = $('#uom option:selected').html();
                addNewServiceRateRow(false);
                $(this).attr('disabled', false);
            }
        }
    });

    function addNewServiceRateRow(autofill) {
        
        var newTrID = 'tr_'+serviceId;
        var clonedRow = $($('#preSample').clone()).attr('id', newTrID).addClass('addedServiceRate');
        $("input[name='addSreviceCodeName']", clonedRow).val(serviceNameCode);
        $("input[name='addServiceRate']", clonedRow).val(accounting.formatMoney(serviceRate));
        $("input[name='addUom']", clonedRow).val(serviceUOMText);
        clonedRow.removeClass('hidden');
        clonedRow.insertBefore('#preSample');
        serviceRates[serviceId] = {
            rate: accounting.unformat(serviceRate),
            uom: serviceUOM,
            subTaskRates: subTaskRates,

        }
        clearAddedNewRow();
    }

    function clearAddedNewRow() {
        clearServiceCode();
        $('#serviceRateVal').val('');
        $('#uom').val(0);
        $('#departmentStationCode').val('');
        $('.viewedSubTasks').addClass('hidden');
        $('#subTskHeader').addClass('hidden');
        $('#subTskHeader').addClass('hidden');
        $('#addServiceRateModal #subTaskRate .serviceSubTaskEnableIcon').removeClass('fa-check-square-o');
        $('#addServiceRateModal #subTaskRate .serviceSubTaskEnableIcon').addClass('fa-square-o');
        $('#serviceRateVal').val('').attr('disabled', false);
        subTaskRates = {};

    }

    $('#btnAddSrvcRates').on('click',function(){
        if ($('#serviceCode').val() > 0) {
            p_notification(false, eb.getMessage('ERR_ADD_DEPT_STAT'));
            return;
        }

        addServicesRatesToMainObject();
        $("#addServiceRateModal").modal('toggle');

    });

    //service Rates footer add button
    function addServicesRatesToMainObject()
    {
        mainServiceRates = {};
        for (var key in serviceRates) {
            if (serviceRates.hasOwnProperty(key)) {
                mainServiceRates[key] = serviceRates[key];
            }
        }

    }

    function reomoveUnAddedServiceRates() {
        var temp1 = [];
        for (var key in mainServiceRates) {
            if (mainServiceRates.hasOwnProperty(key)) {
                temp1.push(key);
            }
        }
        
        var temp2 = [];
        for (var key in serviceRates) {
            if (serviceRates.hasOwnProperty(key)) {
                temp2.push(key);
            }
        }
        for (var i = 0; i < temp2.length; i++) {    
            if ($.inArray(temp2[i], temp1) == -1) {
                delete serviceRates[temp2[i]];
                $('#' + 'tr_'+temp2[i]).remove();
            }           
        }
        addServicesRatesToMainObject();
        $("#addServiceRateModal").modal('toggle');
    }

    $("#addServiceRateModal").on('click','.deleteAddedServiceRates', function(e) {
        e.preventDefault();
        var deleteTempServiceRteID = $(this).closest('tr').attr('id');
        var deleteServiceRteID = deleteTempServiceRteID.split('tr_')[1].trim();

        bootbox.confirm('Are you sure you want to remove this Service Rate ?', function(result) {
            if (result == true) {
                delete serviceRates[deleteServiceRteID];
                $('#' + deleteTempServiceRteID).remove();
            }
        });
    });
    $("#addServiceRateModal").on('click','.viewSubTaskRate', function(e) {
        var tempRowID = $(this).closest('tr').attr('id');
        var rowID = tempRowID.split('tr_')[1].trim();

        if (Object.keys(serviceRates[rowID].subTaskRates).length == 0) {
            p_notification(false, eb.getMessage('ERR_NO_SUB_TSK_RET_ASSIGN'));
            return;
        } 

        $('#subTaskRatesViewModal #viewServiceSubTaskRates .viewedSubTaskRates').remove();

        for (var key in serviceRates[rowID].subTaskRates) {
            var newTrID = 'trs_'+key;
            var clonedRow = $($('#subTskRteViewRow').clone()).attr('id', newTrID).addClass('viewedSubTaskRates').data('sub-task-id', key);
            $("#addedSubTskCode", clonedRow).val(serviceRates[rowID].subTaskRates[key].name).attr('disabled', 'disabled');
            $("#addedSubTskRate", clonedRow).val(accounting.formatMoney(serviceRates[rowID].subTaskRates[key].rate)).attr('disabled', 'disabled');
            clonedRow.removeClass('hidden');
            clonedRow.insertBefore('#subTskRteViewRow');
            $('#subTskRteViewRow').addClass('hidden');
        }
        
        $("#subTaskRatesViewModal").modal('toggle');
    
    });

    function getRelatedServiceRates(Id) {
        eb.ajax({
            type: 'POST', url: BASE_URL + '/rate-card-api/getRelatedServiceRates', 
            data: {rateCardID: Id}, 
            success: function(respond) {
                if (respond.status == true) {
                    if (Id == null) {
                        alldepartmentStations = respond.data;
                    } else {
                        $('#addServiceRateModal #formRow .addedServiceRate').remove();

                        for (var val in respond.data) {
                            respond.data[val].rate = accounting.unformat(respond.data[val].rate);
                        }

                        mainServiceRates = respond.data;

                        for (var key in respond.data) { 

                            serviceId = key;
                            serviceNameCode = respond.data[key].nameCode;
                            serviceRate = accounting.formatMoney(respond.data[key].rate);
                            subTaskRates = respond.data[key].subTaskRates;
                            serviceUOMText  = respond.data[key].uomText;
                            serviceUOM = respond.data[key].uom;
                            addNewServiceRateRow(true);
                        }
                    }


                } 
            }
        });
    }

    /**
     *
     *use to change active state
     */
    function activeInactive(type, status, currentDiv, msg, flag) {
        bootbox.confirm(msg, function($result) {
            if ($result == true) {
                eb.ajax({
                    url: BASE_URL + '/rate-card-api/changeStatusID',
                    method: 'post',
                    dataType: 'json',
                    data: {
                        'rateCardID': type,
                        'status': status,
                    },
                    success: function(data) {
                        if (data.status) {
                            p_notification(data.status, data.msg);
                            if (currentDiv.hasClass('fa-check-square-o') && flag) {
                                currentDiv.removeClass('fa-check-square-o');
                                currentDiv.addClass('fa-square-o');
                            }
                            else {
                                currentDiv.removeClass('fa-square-o');
                                currentDiv.addClass('fa-check-square-o');
                            }
                        } else {
                            p_notification(data.status, data.msg);
                        }
                    }
                });
            }
        });
    }

    $('#addServiceRateModal #subTaskRate').on('click', function(){

        var currentDiv = $(this).contents();
        if (currentDiv.hasClass('fa-check-square-o')) {
            $('.viewedSubTasks').addClass('hidden');
            $('#subTskHeader').addClass('hidden');
            currentDiv.removeClass('fa-check-square-o');
            currentDiv.addClass('fa-square-o');
            $('#serviceRateVal').val('').attr('disabled', false);
            $('#viewServiceSubTasks .viewedSubTasks #subTskRate').val('');
            $('#viewServiceSubTasks .viewedSubTasks .save').addClass('hidden');
            $('#viewServiceSubTasks .viewedSubTasks .edit').addClass('hidden');
            $('#viewServiceSubTasks .viewedSubTasks .add').removeClass('hidden');
        }
        else {
            if ($('#serviceCode').val() == 0) {    
                p_notification(false, eb.getMessage('PLS_SELECT_AT_LEAST_ONE_SRVC'));
                clearServiceCode();
                return false;
            }
            $('.viewedSubTasks').removeClass('hidden');
            $('#subTskHeader').removeClass('hidden');
            $('#serviceRateVal').val('').attr('disabled', 'disabled');
            currentDiv.removeClass('fa-square-o');
            currentDiv.addClass('fa-check-square-o');
        }
    });

    function clearServiceCode($currentRow) {
        $('#serviceCode')
                .empty()
                .val('')
                .append($("<option></option>")
                        .attr("value", "0")
                        .text('Select a Service'))
                .selectpicker('refresh')
                .trigger('change');
    }

    function getRelatedSubTasks(Id) {
        eb.ajax({
            type: 'POST', url: BASE_URL + '/serviceSetup-api/getSubTaskByTaskId', 
            data: {taskID: Id}, 
            success: function(respond) {
                if (respond.status == true) {
                    $('#addServiceRateModal #viewServiceSubTasks .viewedSubTasks').remove();

                    for (var i = 0; i < respond.data.length; i++) {
                        var newTrID = 'trs_'+respond.data[i]['subTaskID'];
                        var clonedRow = $($('#subTskRow').clone()).attr('id', newTrID).addClass('viewedSubTasks').data('sub-task-id', respond.data[i]['subTaskID']);
                        $("#subTskCode", clonedRow).val(respond.data[i]['code']).attr('disabled', 'disabled');
                        $("#subTskRate", clonedRow).val(respond.data[i]['rate']);
                        clonedRow.insertBefore('#subTskRow');
                    }

                } 
            }
        });
    }

    $('#viewServiceSubTasks').on('click', '.add', function(){

        var rowId = $(this).closest('tr').attr('id');
        var subTskId = $(this).closest('tr').data('sub-task-id');
        if ($('#'+rowId+' #subTskRate').val() == '') {
            p_notification(false, eb.getMessage('PLS_ADD_SUB_CRD_RTE'));
            return false;
        }
        var rateVal = $('#'+rowId+' #subTskRate').val();
        $('#'+rowId+' #subTskRate').val(accounting.formatMoney(rateVal));

        var serviceRateVal = (accounting.unformat($('#serviceRateVal').val()) == '') ? 0 : accounting.unformat($('#serviceRateVal').val());
        var newVal = parseFloat(serviceRateVal) + parseFloat(rateVal);
        $('#serviceRateVal').val(accounting.formatMoney(newVal));
        var subTskCode = $('#'+rowId+' #subTskCode').val();
        subTaskRates[subTskId] = {
            name: subTskCode,
            rate: rateVal
        };
        $('#'+rowId+' #subTskRate').attr('disabled', 'disabled');


        $(this).addClass('hidden');
        $('#'+rowId+' .edit').removeClass('hidden');
    });

    $('#viewServiceSubTasks').on('click', '.edit', function(){

        var rowId = $(this).closest('tr').attr('id');

        editSubTaskRate = $('#'+rowId+' #subTskRate').val();
        $(this).addClass('hidden');
        $('#'+rowId+' .save').removeClass('hidden');
        $('#'+rowId+' #subTskRate').attr('disabled', false);

    });

    $('#viewServiceSubTasks').on('click', '.save', function(){

        var subTskId = $(this).closest('tr').data('sub-task-id');
        var rowId = $(this).closest('tr').attr('id');
        var substractedServiceRate = parseFloat($('#serviceRateVal').val()) - parseFloat(editSubTaskRate);
        $(this).addClass('hidden');
        $('#'+rowId+' .edit').removeClass('hidden');
        var rateVal = $('#'+rowId+' #subTskRate').val();
        var serviceRateVal = (substractedServiceRate == '') ? 0 : substractedServiceRate;
        var newVal = parseFloat(serviceRateVal) + parseFloat(rateVal);
        $('#serviceRateVal').val(newVal);
        subTaskRates[subTskId].rate = rateVal;
        $('#'+rowId+' #subTskRate').attr('disabled', 'disabled');

    });

    function validateFormData(formData) {
        if (formData.rateCardCode == "") {
            p_notification(false, eb.getMessage('ERR_RTE_CRD_CODE_CNT_BE_NULL'));
            $('#rateCardCode').focus();
            return false;
        } else if (formData.rateCardName == '') {
            p_notification(false, eb.getMessage('ERR_RTE_CRD_NAME_CNT_BE_NULL'));
            $('#rateCardName').focus();
            return false;
        }
        return true;
    }

    $('#serviceRateVal').focusout(function(){
        accounting.unformat($('#serviceRateVal').val());
        var formattedVal =  accounting.formatMoney($('#serviceRateVal').val());
        $('#serviceRateVal').val(formattedVal);
    });

});
