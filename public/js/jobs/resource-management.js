var projectId = 0;
var jobId = 0;
var jobMaterialRequest = [];

$(document).ready(function() {

    $('#JobCode_byProject').selectpicker('hide');
	$('#JobCode_search').selectpicker('show');

    //DatePicker
    var nowTemp = new Date();
    var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);

    var dueDate = $('#asAtDate').datepicker({onRender: function(date) {
            return date.valueOf() > now.valueOf() ? ((U_DATE_R_OVERRIDE) ? '' : 'disabled') : '';
        },
    }).on('changeDate', function(ev) {
        dueDate.hide();
    }).data('datepicker');

	loadDropDownFromDatabase('/api/project/search-projects-for-dropdown', '', 0, '#projectCode', '', '', false);
    $('#projectCode').selectpicker('refresh');

    loadDropDownFromDatabase('/job-api/search-jobs-for-dropdown', '', 1, '#JobCode_search', '', '', false);
    $('#JobCode_search').selectpicker('refresh');

    $('#projectCode').on('change', function() {
        if ($(this).val() != projectId) {
            projectId = $(this).val();
            jobId = 0;
            $('#JobCode_byProject, #JobCode_search').selectpicker('val', 0);
            $('#JobCode_byProject, #JobCode_search').selectpicker('refresh').selectpicker('render');
            if (projectId > 0) {
                $('#JobCode_byProject').selectpicker('show');
                $('#JobCode_search').selectpicker('hide');
                loadJobPicker(projectId);
            } else {
                $('#JobCode_byProject').selectpicker('hide');
                $('#JobCode_search').selectpicker('show');
            }
        }
    });

    $('#JobCode_byProject, #JobCode_search').on('change', function() {
        if ($(this).val() > 0 && ($(this).val() != jobId)) {
            jobId = $(this).val();
        }
    });

    $('#makeMaterialsRequest').on('click', function() {
        var reqMateProducts = [];
        $('tr.reqMaterial').each(function(){
            var jobProductId = $(this).attr('id');
            var reqMateralsQty = $('#reqMateralsQty', $(this)).val();
            if(reqMateralsQty != '' && reqMateralsQty != 0) {
                var tempReqMate = {
                    'jobProductId' : jobProductId,
                    'requestedQuantity' : reqMateralsQty,
                };
                reqMateProducts.push(tempReqMate);
            }
        });
        if($.isEmptyObject(reqMateProducts)) {
            p_notification(false, eb.getMessage('ERR_REQ_MATE_FORM'));
            return false;
        }
        var reqMateData = {
            'jobId' : jobId,
            'materialRequestDate' : $('#asAtDate').val(),
            'reqMateProducts' : reqMateProducts,
        };
        eb.ajax({
            url: BASE_URL + '/material-requisition-api/save',
            method: 'post',
            data: reqMateData,
            dataType: 'json',
            success: function(data) {
                p_notification(data.status, data.msg);
                if(data.status) {
                    updateReqQty(data.data.updatedProducts);
                    setjobMaterialRequest(data.data.jobMaterialRequest);
                    $('#requestMaterialsModal').modal('hide');
                }
            }
        });
    });

    $('#btnManage').on('click', function() {
    	formData = {
    		'projectId' : projectId ? projectId : null,
    		'jobId' : jobId ? jobId : null,
    	};
    	if (validationManageForm(formData)) {

            manageResource();
    	}
    });

    function manageResource() {

        eb.ajax({
            url: BASE_URL + '/api/resourceManagement/load-job-products',
            method: 'post',
            data: {
                jobId: jobId,
            },
            dataType: 'json',
            success: function(data) {
                if(data.status) {
                    $('#default-job-list').addClass('hidden');
                    $('.jobTaskRow').remove();
                    $('.jobProRow').remove();
                    flag1 = setJobProducts(data.data.availableJobProductList);
                    flag2 = setJobUnProducts(data.data.unavailableJobProductList);
                    if (flag1 != false) {
                        $('#tablink_1').addClass('active');
                        $('#tab_1').addClass('active in');
                        $('#tablink_2').removeClass('active');
                        $('#tab_2').removeClass('active');
                        $('#tab_2').removeClass('active in');
                        $('#productPanel_footer').removeClass('hidden');
                    } else if (flag2 != false) {
                        $('#tablink_2').addClass('active');
                        $('#tab_2').addClass('active in');
                        $('#tablink_1').removeClass('active');
                        $('#tab_1').removeClass('active');
                        $('#tab_1').removeClass('active in');
                        $('#productPanel_footer').removeClass('hidden');
                    } else {
                        p_notification(false, eb.getMessage('ERR_EMPTY_PRO_LIST'));
                        $('#productPanel_footer').addClass('hidden');
                    }
                    setjobMaterialRequest(data.data.jobMaterialRequest);
                } else {
                    p_notification(data.status, data.msg);
                }
            }
        });
    }

    $('#default-job-list').on('click', '.manageRawBtn', function(){
        jobId = $(this).closest('tr').data('jobId');
        manageResource();
    });

    $('#default-job-list').on('click','.viewContractors',function (e){
        $("#viewJobContractorModal #jobContractorTable tbody .addedcontractors").remove();
        var tempJobID = $(this).closest('tr').data('jobId');
        eb.ajax({
            url: BASE_URL + '/job-api/getJobContractorsByJobID',
            method: 'post',
            data: {jobID:tempJobID},
            dataType: 'json',
            success: function(respond) {
                if (respond.status) {
                    $.each(respond.data, function(index, val) {
                        var newTrID = 'tr_' + val.jobSupervisorID;
                        var clonedRow = $($('#contractorRow').clone()).attr('id', newTrID).addClass('addedcontractors');
                        $("#contractorName", clonedRow).text(val.contractorFirstName+" "+val.contractorSecondName);
                        clonedRow.removeClass('hidden');
                        clonedRow.insertBefore('#contractorRow');
                    });
                    $('#viewJobContractorModal').modal('show');
                }
            }
        });  
    });

    $('#default-job-list').on('click','.job-supervisor-view',function (e){ 
        $("#viewJobSupervisorModal #jobSupervisorTable tbody .addedSupervisors").remove();
        var tempJobID = $(this).closest('tr').data('jobId');  
        eb.ajax({
            url: BASE_URL + '/job-api/getJobSupervisorsByJobID',
            method: 'post',
            data: {jobID:tempJobID},
            dataType: 'json',
            success: function(respond) {
                if (respond.status) {
                    $.each(respond.data, function(index, val) {
                        var newTrID = 'tr_' + val.jobSupervisorID;
                        var clonedRow = $($('#preSample').clone()).attr('id', newTrID).addClass('addedSupervisors');
                        $("#supervisorName", clonedRow).text(val.employeeFirstName+" "+val.employeeSecondName);
                        clonedRow.removeClass('hidden');
                        clonedRow.insertBefore('#preSample');
                    });
                    $('#viewJobSupervisorModal').modal('show');
                }
            }
        });  
    });

    $('#productPanel').on('focusout', '#usedQty', function() {
        if(!validationProductRow($(this).parents('tr'))) {
            $(this).focus();
        }
    });

    $('#requestRawMaterials').on('click', function() {
        var requestRawMaterialsData = new Array();
        $('.requestCheck').each(function(){
            if (this.checked == true) {
                requestRawMaterialsData.push({
                    'prId' : $(this).parents('tr').attr('id'),
                    'prCode' : $('#itemCode', $(this).parents('tr')).val(),
                    'prUom' : $('#uom', $(this).parents('tr')).val(),
                });
            }
        });
        if (requestRawMaterialsData.length > 0) {
            var tbody = $('#materialsRequestTable');
            $('.reqMaterial', tbody).remove();
            if (!$.isEmptyObject(requestRawMaterialsData)) {
                $.each(requestRawMaterialsData, function(index, value){
                    var $newPrRow = $('.sample', tbody).clone().appendTo(tbody);
                    $newPrRow.removeClass('hidden');
                    $newPrRow.removeClass('sample');
                    $newPrRow.addClass('reqMaterial');
                    $newPrRow.attr('id', value['prId']);
                    $('#reqMateralsCode', $newPrRow).val(value['prCode']);
                    $('#reqMateralsUom', $newPrRow).val(value['prUom']);
                });
            }
            var asAtDate = $('#asAtDate').val() ? $('#asAtDate').val() : null
            
            if (!asAtDate) {
                p_notification(false, eb.getMessage('ERR_SEL_RM_DATE'));
                return false;
            }


            $('#requestMaterialsModal').modal('show');
        } else {
            p_notification(false, eb.getMessage('ERR_EMPTY_RM_REQ_MATERIAL'));
        }
    });

    $('#productPanel_1, #productPanel_2, #productPanel_3').on('click', 'button.viewMateReqModal', function(e) {
        var $tr = $(this).parents('tr');
        var $modal = $('#mateReqViewModal');
        var $table = $('#mateReqViewTable', $modal);
        var mateReqList = jobMaterialRequest[$tr.attr('id')];
        $(".mateReqRow", $table).remove();
        if (!$.isEmptyObject(mateReqList)) {
            $.each(mateReqList, function(index, value) {
                var $newMateReqRow = $(".sample", $table).clone().appendTo($table);
                $('#date', $newMateReqRow).html(value.materialRequestDate);
                $('#code', $newMateReqRow).html(value.materialRequestCode);
                $('#qty', $newMateReqRow).html(value.requestedQuantity);
                $('#'+value.materialRequestStatus, $('#state', $newMateReqRow)).removeClass('hidden');
                $newMateReqRow.removeClass('hidden');
                $newMateReqRow.removeClass('sample');
                $newMateReqRow.addClass('mateReqRow');
            });
        } else {
            var $newMateReqRow = $(".sample", $table).clone().appendTo($table);
            $newMateReqRow.html('No Results');
            $newMateReqRow.removeClass('hidden');
            $newMateReqRow.removeClass('sample');
            $newMateReqRow.addClass('mateReqRow');
        }
        $('#jobProductName', $modal).html($('#itemCode', $tr).val());

        $modal.modal('show');
    });

    $('#updateRawMaterials').on('click', function() {
        var updateData = {};
        var errorFlag = true;
        $('.jobProRow').each(function(){
            if (!validationProductRow(this)) { 
                errorFlag = false;
            }
            updateData[$(this).attr('id')] = {
                'jobProductID' : $(this).attr('id'),
                'jobProductUsedQty' : $('#usedQty', this).val(),
            };
        });
        if (errorFlag) {
            eb.ajax({
                url: BASE_URL + '/api/resourceManagement/update',
                method: 'post',
                data: {
                    updateData: updateData,
                },
                dataType: 'json',
                success: function(data) {
                    p_notification(data.status, data.msg);
                    if(data.status) {
                        $('#btnManage').click();
                    }
                }
            });
        }
    });

    function loadJobPicker(projectId) {
        eb.ajax({
            url: BASE_URL + '/job-api/load-jobs-for-dropdown',
            method: 'post',
            data: {
                projectId: projectId,
            },
            dataType: 'json',
            success: function(data) {
            	if(data.status) {
            		setJobSelectPicker(data.data.jobList);
            	} else {
            		p_notification(data.status, data.msg);
            	}
            }
        });
    }

    function setJobSelectPicker(data) {
        $('#JobCode_byProject').html("<option value='0'>" + "Select a Job" + "</option>");
        $('#JobCode_byProject').selectpicker('refresh');
        if (!$.isEmptyObject(data)) {
            $.each(data, function(index, value) {
                $('#JobCode_byProject').append("<option value='" + value.value + "'>" + value.text + "</option>");
            });
            $('#JobCode_byProject').selectpicker('refresh');
        }
    }

    function validationManageForm(data) {
    	if (!data.jobId) {
	        p_notification(false, eb.getMessage('ERR_SEL_RM_JOB'));
    		return false;
    	} else {
    		return true;
    	}
    }

    function validationProductRow($thisRow) {
        var usedQty = parseFloat($('#usedQty', $thisRow).val());
        var totalAvailableQty = parseFloat($('#totalAvailableQty', $thisRow).val());
        var reOrderQty = parseFloat($('#reOrderQty', $thisRow).val());

        if (!$.isNumeric(usedQty) || usedQty < 0) {
            p_notification(false, eb.getMessage('ERR_WRONG_IN_USED_QTY'));
            $('#usedQty', $thisRow).val('0');
            return false;
        }

        var availableQty = parseFloat(totalAvailableQty - usedQty);
        $('#availableQty', $thisRow).val(availableQty);

        if (availableQty < 0) {
            p_notification(false, eb.getMessage('ERR_WRONG_USED_QTY'));
            return false;
        }

        if (availableQty <= reOrderQty) {
            $('#requestCheck', $thisRow).removeClass('hidden');
        } else {
            $('#requestCheck', $thisRow).addClass('hidden');
        }
        return true;
    }

    function setJobProducts(data) {
        $('#productPanel_1').addClass('hidden');
        $('#productPanel_2').addClass('hidden');
        $('#productPanel_3').addClass('hidden');
        $('#productPanel_footer').addClass('hidden');
        if (!$.isEmptyObject(data)) {
            $.each(data, function(typeIndex, taskValue) {
                var productPanel = $('#productPanel_' + typeIndex);
                var tbody = $('#add-new-item-row', productPanel);
                if (!$.isEmptyObject(taskValue)) {
                    $.each(taskValue, function(taskIndex, typeValue) {
                        var $newTaskRow = $(".sample", productPanel).clone().appendTo(tbody);
                        $newTaskRow.attr('id', 'tsk_' + taskIndex);
                        $newTaskRow.removeClass('sample');
                        $newTaskRow.removeClass('add-row');
                        $newTaskRow.removeClass('hidden');
                        $newTaskRow.addClass('jobTaskRow');
                        if (!$.isEmptyObject(typeValue)) {
                            $.each(typeValue, function(index, value) {
                                var $newRow = $(".sample", productPanel).clone().appendTo(tbody);
                                $newRow.attr('id', index);
                                if (value.taskCode == null || value.taskName == null) {
                                    var panelName = $('h4.panel-title', productPanel).html();
                                    $newTaskRow.html('<td colspan="8">Non Task ' + panelName + '</td>');
                                } else {
                                    $newTaskRow.html('<td colspan="8">' + value.taskCode + ' - ' + value.taskName + '</td>');
                                }
                                $('#itemCode', $newRow).val(value.productCode + ' - ' + value.productName);
                                $('#uom', $newRow).val(value.uomName + ' (' + value.uomAbbr + ')');
                                $('#requiredQty', $newRow).val(value.jobProductAllocatedQty ? value.jobProductAllocatedQty : 0);
                                $('#reOrderQty', $newRow).val(value.jobProductReOrderLevel ? value.jobProductReOrderLevel : 0);
                                $('#usedQty', $newRow).val(value.jobProductUsedQty ? value.jobProductUsedQty : 0);
                                var disAvailableQty = parseFloat($('#totalAvailableQty', $newRow).val()) - parseFloat($('#usedQty', $newRow).val());
                                $('#totalAvailableQty', $newRow).val(value.jobProductIssuedQty ? value.jobProductIssuedQty : 0);
                                $('#availableQty', $newRow).val(disAvailableQty ? disAvailableQty : 0);
                                $newRow.removeClass('sample');
                                $newRow.removeClass('hidden');
                                $newRow.addClass('jobProRow');
                                validationProductRow($newRow);
                            });
                        }
                    });
                productPanel.removeClass('hidden');
                }
            });
            $('#tablink_1').removeClass('hidden');
            $('#tab_1').removeClass('hidden');
        } else {
            $('#tablink_1').addClass('hidden');
            $('#tab_1').addClass('hidden');
            return false;
        }
    }

    function setJobUnProducts(data) {
        $('#unProductPanel_1').addClass('hidden');
        $('#unProductPanel_2').addClass('hidden');
        $('#unProductPanel_3').addClass('hidden');
        $('#productPanel_footer').addClass('hidden');
        if (!$.isEmptyObject(data)) {
            $.each(data, function(typeIndex, taskValue) {
                var productPanel = $('#unProductPanel_' + typeIndex);
                var tbody = $('#add-new-item-row', productPanel);
                if (!$.isEmptyObject(taskValue)) {
                    $.each(taskValue, function(taskIndex, typeValue) {
                        var $newTaskRow = $(".sample", productPanel).clone().appendTo(tbody);
                        $newTaskRow.attr('id', 'tsk_' + taskIndex);
                        $newTaskRow.removeClass('sample');
                        $newTaskRow.removeClass('add-row');
                        $newTaskRow.removeClass('hidden');
                        $newTaskRow.addClass('jobTaskRow');
                        if (!$.isEmptyObject(typeValue)) {
                            $.each(typeValue, function(index, value) {
                                var $newRow = $(".sample", productPanel).clone().appendTo(tbody);
                                $newRow.attr('id', index);
                                if (value.taskCode == null || value.taskName == null) {
                                    var panelName = $('h4.panel-title', productPanel).html();
                                    $newTaskRow.html('<td colspan="8">Non Task ' + panelName + '</td>');
                                } else {
                                    $newTaskRow.html('<td colspan="8">' + value.taskCode + ' - ' + value.taskName + '</td>');
                                }
                                $('#itemCode', $newRow).val(value.productCode + ' - ' + value.productName);
                                $('#uom', $newRow).val(value.uomName + ' (' + value.uomAbbr + ')');
                                $('#requiredQty', $newRow).val(value.jobProductAllocatedQty ? value.jobProductAllocatedQty : 0);
                                $('#availableQty', $newRow).val(value.jobProductIssuedQty ? value.jobProductIssuedQty : 0);
                                $('#reOrderQty', $newRow).val(value.jobProductReOrderLevel ? value.jobProductReOrderLevel : 0);
                                $('#usedQty', $newRow).val(value.jobProductUsedQty ? value.jobProductUsedQty : 0);
                                var totalAvailableQty = parseFloat($('#availableQty', $newRow).val()) + parseFloat($('#usedQty', $newRow).val());
                                $('#totalAvailableQty', $newRow).val(totalAvailableQty ? totalAvailableQty : 0);
                                $newRow.removeClass('sample');
                                $newRow.removeClass('hidden');
                                $newRow.addClass('jobProRow');
                                validationProductRow($newRow);
                            });
                        }
                    });
                    productPanel.removeClass('hidden');
                }
            });
            $('#tablink_2').removeClass('hidden');
            $('#tab_2').removeClass('hidden');
        } else {
            $('#tablink_2').addClass('hidden');
            $('#tab_2').addClass('hidden');
            return false;
        }
    }

    function setjobMaterialRequest(data) {
        jobMaterialRequest = data;
    }

    function updateReqQty(data) {
        $.each(data, function(index, value) {
            var tr = $('.jobProRow#'+index);
            $('#requiredQty', tr).val(value['jobProductAllocatedQty']);
            $('#requestCheck', tr).attr('checked', false);
        });
    }

});

