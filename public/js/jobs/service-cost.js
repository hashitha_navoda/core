var expenseTypeID = null;
var savedJobTaskDetailsObject = {};
var savedJobCardDetailsObject = {};
var savedJobProductDetails = {};
var savedOtherJobCostDetails = {};
var rateCardID = null;
var jobId = null;
var jobServices = {};
var activeProductData = {};
var serviceMaterials = {};
$(document).ready(function() {
    var locationID = $('#navBarSelectedLocation').attr('data-locationid');
    //get active product list
    $.ajax({
        type: 'POST',
        url: BASE_URL + '/productAPI/getActiveProductsFromLocation',
        data: {locationID: locationID},
        success: function(respond) {
            activeProductData = respond.data;
        },
        async: false
    });

    loadDropDownFromDatabase('/productAPI/search-location-products-for-dropdown', '', 0, '#tradeMaterialID', '', '', "jobTradingGoods");
    $('#tradeMaterialID').trigger('change');
    var tempProductKey;
    $('#tradeMaterialID').on('change', function() {
        if ($(this).val() > 0 && ($(this).val() != tempProductKey)) {
            tempProductKey = $(this).val();
            $('#tradeMaterialID').
                    append($("<option></option>").
                            attr("value", 'select').
                            text("Select Item"));
            $('#tradeMaterialID').val('select');
            $('#tradeMaterialID').selectpicker('refresh');

            var itemDuplicateCheck = false;

            $.each(serviceMaterials, function(index, val) {
                if (val.jobMaterialProID == tempProductKey) {
                    itemDuplicateCheck = true;                        
                }
            });

            if (!itemDuplicateCheck) {
                selectProducts(tempProductKey);
            } else {
                p_notification(false, eb.getMessage('ERR_GRN_INSERT_PROD'));
                $('#fixedAssetsID').focus();
            }
        } 
    });


    function selectProducts(selectedProductID) {

        if (!$.isEmptyObject(activeProductData[selectedProductID])) {
            $('.trade-product-table ').removeClass('hidden');
            setTradingProductList(selectedProductID);
            $("input[name='issQuantity']").focus();
        }
    }

    function setTradingProductList(tradingGoodProID) {
        var newTrID = 'tr_' + tradingGoodProID;
        var clonedRow = $($('#tradeProductPreSetSample').clone()).attr('id', newTrID).addClass('addedTradeProducts');
        $("#tradeProdctName", clonedRow).val(activeProductData[tradingGoodProID].pN);
        $("#tradeProdctCode", clonedRow).text(activeProductData[tradingGoodProID].pC);
        $("input[name='avaliableQuantity']", clonedRow).val(activeProductData[tradingGoodProID].LPQ);
        $("input[name='issQuantity']", clonedRow).val(1).addUom(activeProductData[tradingGoodProID].uom);
        $("input[name='issuedUnitPrice']", clonedRow).val(activeProductData[tradingGoodProID].dSP).addUomPrice(activeProductData[tradingGoodProID].uom);
        $("input[name='issTotal']", clonedRow).val(accounting.formatMoney(activeProductData[tradingGoodProID].dSP * 1));
        clonedRow.removeClass('hidden');
        clonedRow.insertBefore('#tradeProductPreSetSample');
        clonedRow.addClass('productToAdd');
        $('#tradeMaterialID').attr('disabled', true);
        var des = $(clonedRow).find('.itemDescText').val();
        serviceMaterials[tradingGoodProID] = new jobMaterial(tradingGoodProID,1,activeProductData[tradingGoodProID].lPID, null, activeProductData[tradingGoodProID].dSP, des);
    }

    $(document).on('click', '.addItemDescription', function() {
        if ($('i', $(this)).hasClass('fa-comment-o')) {
            $('i', $(this)).removeClass('fa-comment-o').addClass('fa-comment');
            $(this).parents('tr').addClass('comment');
            $(this).parent().parent().siblings('.itemDescText').removeClass('hidden').focus();
        } else {
            $('i', $(this)).removeClass('fa-comment').addClass('fa-comment-o');
            $(this).parents('tr').removeClass('comment');
            $(this).parent().parent().siblings('.itemDescText').addClass('hidden');
        }
    });

    function loadJobTempTradeMaterials(value)
    {
        var newTrID = 'tr_' + value.productID;
        var clonedRow = $($('#tradeProductPreSetSample').clone()).attr('id', newTrID).addClass('addedTradeProducts');
        $("#tradeProdctCode", clonedRow).text(value.productCode);
        $("#tradeProdctName", clonedRow).val(value.productName);
        $("input[name='avaliableQuantity']", clonedRow).val(activeProductData[value.productID].LPQ);
        $("input[name='issQuantity']", clonedRow).val(value.issueQty).addUom(activeProductData[value.productID].uom);
        $("input[name='issuedUnitPrice']", clonedRow).val(activeProductData[value.productID].dSP).addUomPrice(activeProductData[value.productID].uom);
        $("input[name='issTotal']", clonedRow).val(accounting.formatMoney(activeProductData[value.productID].dSP * value.issueQty));
        $(clonedRow).find('.itemDescText').val(value.itemDes).attr('disabled', false);
        clonedRow.removeClass('hidden');
        clonedRow.insertBefore('#tradeProductPreSetSample');
        clonedRow.addClass('productToAdd');
        $(".trade-goods-save", clonedRow).addClass('hidden');
        // $(".trade-goods-edit", clonedRow).removeClass('hidden');
        var des = $(clonedRow).find('.itemDescText').val();

        serviceMaterials[value.productID] = new jobMaterial(value.productID, value.issueQty, activeProductData[value.productID].lPID, value.productUomID, activeProductData[value.productID].dSP, des);
    }


    $('#tradeMaterialTable').on('focusout', '.uomqty,.uomPrice', function(e){
        e.preventDefault();
        var $thisRow = $(this).parents('tr');
        calculateMaterialRowCost($thisRow);
    });

    function calculateMaterialRowCost(thisRow) {
        var thisRow = thisRow;
        var itemQuentity = thisRow.find('#issQuantity').val();
        var itemCost = (toFloat(thisRow.find('#issuedUnitPrice').val()) * toFloat(itemQuentity));
        thisRow.find('#issTotal').val(accounting.formatMoney(itemCost));
    }

    $(document).on('click', '.trade-goods-plus , .trade-goods-save', function() {

        var avaliableQuantity = $(this).closest('tr').find("input[name='avaliableQuantity']").val();
        var unitPrice = $(this).closest('tr').find("input[name='issuedUnitPrice']").val();
        var itemDes = $(this).closest('tr').find("#itemDescText").val();
        var issueQuantity = $(this).closest('tr').find("input[name='issQuantity']").val();
        var selectedUomID = $(this).closest('tr').find("input[name='issQuantity']").siblings('.uom-select').children('button').find('.selected').data('uomID');

        if (parseFloat(issueQuantity) <= 0 || issueQuantity == "") {
            p_notification(false, eb.getMessage('ERR_VLID_ISSUEQTY'));
            $(this).closest('tr').find("input[name='issQuantity']").select();    
            return false;
        }

        if (parseFloat(avaliableQuantity) < parseFloat(issueQuantity)) {
            p_notification(false, eb.getMessage('ERR_VLID_ISSQTY_VS_AVALIBLE_QTY'));
            $(this).closest('tr').find("input[name='issQuantity']").select();    
            return false;
        }
        
        var tradingProTrID = $(this).closest('tr').attr('id');
        var tradingProID = tradingProTrID.split('tr_')[1].trim();
        if (serviceMaterials[tradingProID]) {
            serviceMaterials[tradingProID].issueQty = issueQuantity;
            serviceMaterials[tradingProID].unitPrice = unitPrice;
            serviceMaterials[tradingProID].jobMaterialSelectedUomID = selectedUomID;
            serviceMaterials[tradingProID].itemDes = itemDes;
            $("#tradeMaterialID").attr('disabled', false);
            $(this).closest('tr').find("input.uomqty").attr('disabled', true);
            $(this).closest('tr').find("input.uomPrice").attr('disabled', true);
            $(this).addClass('hidden');
            $(this).closest('tr').find(".trade-goods-edit").removeClass('hidden');
            $(this).closest('tr').find('.itemDescText').addClass('hidden').attr('disabled', true);

            if (!$(this).closest('tr').find('.addItemDescription i').hasClass('fa-comment-o')) {
              $(this).closest('tr').find('.addItemDescription i').removeClass('fa-comment').addClass('fa-comment-o');
            }

            $(this).closest('tr').removeClass('comment');
        }
        $(this).closest('tr').removeClass('productToAdd');
        $(this).closest('tr').removeClass('productToSave');
    });


    $(document).on('click', '.trade-goods-edit', function() {
        $(this).closest('tr').find("input.uomqty").attr('disabled', false);
        $(this).closest('tr').find("input.uomPrice").attr('disabled', false);
        $(this).addClass('hidden');
        $(this).closest('tr').find(".trade-goods-plus").addClass('hidden');
        $(this).closest('tr').find(".trade-goods-save").removeClass('hidden');
        $(this).closest('tr').addClass('productToSave');
        $(this).closest('tr').find('.itemDescText').attr('disabled', false);
    });

    $(document).on('click', '.trade-goods-delete', function() {

        var deleteTradingProTrID = $(this).closest('tr').attr('id');
        var deleteTradingProID = deleteTradingProTrID.split('tr_')[1].trim();
        delete serviceMaterials[deleteTradingProID];
        $('#' + deleteTradingProTrID).remove();
        $("#tradeMaterialID").attr('disabled', false);
    });

    function jobMaterial(jobMaterialProID, issueQty, jobMaterialTaskLocationProductID = null, jobMaterialSelectedUomID = null, unitPrice, itemDes = null) {
        this.jobMaterialProID = jobMaterialProID;
        this.issueQty = issueQty;
        this.jobMaterialTaskLocationProductID = jobMaterialTaskLocationProductID;
        this.jobMaterialSelectedUomID = jobMaterialSelectedUomID;
        this.unitPrice = unitPrice;
        this.itemDes = itemDes;
    }

	function jobTask(jobTaskID, jobTaskQty, jobTaskUnitCost, jobTaskUnitRate) {
        this.jobTaskID = jobTaskID;
        this.jobTaskQty = jobTaskQty;
        this.jobTaskUnitCost = jobTaskUnitCost;
        this.jobTaskUnitRate = jobTaskUnitRate;
    }


    function jobCard(jobCardID, jobCardQty, jobCardUnitCost, jobCardUnitRate) {
        this.jobCardID = jobCardID;
        this.jobCardQty = jobCardQty;
        this.jobCardUnitCost = jobCardUnitCost;
        this.jobCardUnitRate = jobCardUnitRate;
    }

    function jobTaskProduct(jobTaskID, jobProductID, jobProductQty, jobProductUnitPrice, locationProductID) {
        this.jobTaskID = jobTaskID;
        this.jobProductID = jobProductID;
        this.jobProductQty = jobProductQty;
        this.jobProductUnitPrice = jobProductUnitPrice;
        this.locationProductID = locationProductID;
    }

    function jobOtherCost(jobID, costID, unitCost, units) {
        this.jobID = jobID;
        this.costID = costID;
        this.unitCost = unitCost;
        this.units = units;
    }

    loadDropDownFromDatabase('/productAPI/searchNonInventoryActiveProductsForDropdown', "", false, '#costType');
    $('#costType').selectpicker('refresh');
    $('#costType').on('change', function() {
        if ($(this).val() != 0 || $(this).val() != expenseTypeID) {
            expenseTypeID = $(this).val();
        }
    });


    loadDropDownFromDatabase('/rate-card-api/search-rate-cards-for-dropdown', "", 0, '#rateCardID');
    $('#rateCardID').on('change', function() {
        if(!($(this).val() == null || $(this).val() == 0) && rateCardID != $(this).val()){
			rateCardID = $(this).val();
			getRateCardDetails(rateCardID);
        }
    });


    $('#add-other-cost-row').on('click', '.add-cost',function(){
        var costType = $(this).parents('tr').find('#costType').val();
        var costTypeName = $(this).parents('tr').find('#costType option:selected').text();
        var unitCost = $(this).parents('tr').find('#other-cost-unit-price').val();
        var qty = $(this).parents('tr').find('#other-cost-unit').val();
        var tbody = $('#add-other-cost-row');
        
        if (costType == '' || costType == 0) {
            p_notification(false, eb.getMessage('ERR_SELECT_COST_TYPE'));
        } else if (isNaN(unitCost)) {
            p_notification(false, eb.getMessage('ERR_WRONG_DATA_TYPE'));
            $(this).parents('tr').find('#other-cost-unit-price').val(accounting.formatMoney(0));
        } else if (isNaN(qty)) {
            p_notification(false, eb.getMessage('ERR_WRONG_DATA_TYPE'));
            $(this).parents('tr').find('#other-cost-unit').val(accounting.formatMoney(0));
        } else if (parseFloat(unitCost) < 0) {
            p_notification(false, eb.getMessage('ERR_NEGATIVE_VALUE'));
            $(this).parents('tr').find('#other-cost-unit-price').val(accounting.formatMoney(0));
        } else if (parseFloat(qty) < 0) {
            p_notification(false, eb.getMessage('ERR_NEGATIVE_VALUE'));
            $(this).parents('tr').find('#other-cost-unit').val(accounting.formatMoney(0));
        } else if (savedOtherJobCostDetails[costType] != undefined) {
            p_notification(false, eb.getMessage('ERR_SAME_VALUE'));
            $(this).parents('tr').find('#other-cost-unit').val(accounting.formatMoney(0));
        } else {
            var newRow = $(".add-other-cost-original").clone().attr('data-id',costType).addClass('addedOtherCost').appendTo(tbody);
            $('#selectedType', newRow).val(costTypeName).attr('disabled', true);
            $('.selected-cost', newRow).removeClass('hidden');
            $('.remove-normal-cost', newRow).removeClass('hidden');
            $('.select-cost', newRow).addClass('hidden');
            $('.add-normal-cost', newRow).addClass('hidden');
            $('#other-cost-unit-price', newRow).val(accounting.formatMoney(unitCost)).attr('disabled', true);
            $('#other-cost-unit', newRow).val(accounting.formatMoney(qty)).attr('disabled', true);
            $('#other-cost-total', newRow).val(accounting.formatMoney(parseFloat(unitCost)*parseFloat(qty))).attr('disabled', true);
            savedOtherJobCostDetails[costType] = new jobOtherCost($('#jobId').val(),costType, unitCost,qty);
            newRow.removeClass('add-other-cost-original');
        
            // clear original columns
            $(this).parents('tr').find('#costType').val('');
            $(this).parents('tr').find('#other-cost-unit-price').val('');
            $(this).parents('tr').find('#other-cost-unit').val('');
            $(this).parents('tr').find('#other-cost-total').val('');
            $(this).parents('tr').find('#costType').val(0).trigger('change').empty().selectpicker('refresh');
        }

    });

    $('#add-other-cost-row').on('click', '.remove-cost', function() {
        var costType = $(this).parents('tr').find('#costType').val();
        delete(savedOtherJobCostDetails[costType]);
        $(this).parents('tr').remove();
    });


    $('#add-other-cost-row').on('change', '#other-cost-unit-price, #other-cost-unit', function() {
        var selectedValue = $(this).val();
        if (isNaN(selectedValue) || parseFloat(selectedValue) < 0) {
            p_notification(false, eb.getMessage('ERR_WRONG_DATA_TYPE'));
            $(this).val(accounting.formatMoney(0));
            selectedValue = 0;
        }
        var otherCostUnitPrice = 0;
        var otherCostUnitQty = 0;
        otherCostUnitPrice = $(this).parents('tr').find('#other-cost-unit-price').val();
        otherCostUnitQty = $(this).parents('tr').find('#other-cost-unit').val();

        var OtherCostTotal = parseFloat(otherCostUnitPrice) * parseFloat(otherCostUnitQty);
        $(this).parents('tr').find('#other-cost-total').val(accounting.formatMoney(OtherCostTotal));

    });

	eb.ajax({
        type: 'POST',
        url: BASE_URL + '/api/serviceCost/getJobDetailsByJobId',
        data: {jobID: $('#jobId').val()},
        success: function(respond) {
            var jobData = respond.data;
            jobServices = jobData.jobServices;
            var jobTradeMaterials = jobData.jobTradeMaterials;

            if (jobData['useJobCardServices'] == true) {
                setJobCardRates(jobData['jobCardRateDetails']);
            }

            $.each(jobServices, function(jobProKey, jobProData) {
               loadJobServices(jobProData);	
            });

            if (Object.keys(savedJobTaskDetailsObject) == 0) {
                $("#serviceTable").addClass('hidden');
            } 

            $.each(jobTradeMaterials, function(jobProKey, jobProData) {
               loadJobTradeMaterials(jobProData);	
            });

            $.each(jobData.tempMaterials, function(jobProKey, jobProData) {
                $('.trade-product-table ').removeClass('hidden');
                loadJobTempTradeMaterials(jobProData);   
            });

            
        },
        async: false
    });

    $(document).on('click', '.job-card-edit', function() {
        // $(this).closest('tr').find("input#jobCardQuantity").attr('disabled', false);
        $(this).closest('tr').find("input#jobCardRate").attr('disabled', false);
        $(this).addClass('hidden');
        $(this).closest('tr').find(".job-card-save").removeClass('hidden');
        $(this).closest('tr').addClass('productToSave');
    });


    function setJobCardRates($data) {
        $('#jobCardDataRow').attr('data-job-card-id', $data['jobCardID']);
        $("#jobCardTable #jobCardDataRow #jobCardCode").text($data['taskCardCode']);
        $("#jobCardTable #jobCardDataRow #jobCardName").text($data['taskCardName']);
        $("#jobCardTable #jobCardDataRow #jobCardQty #jobCardQuantity").val('1.00');
        $("#jobCardTable #jobCardDataRow #jobCardServiceRate #jobCardRate").val(accounting.formatMoney($data['serviceRate']));
        $("#jobCardTable #jobCardDataRow #jobCardServiceRateTotal #jobCardTotal").val(accounting.formatMoney($data['serviceRate']));
        if ($("#jobCardTable").hasClass('hidden')) {
            $("#jobCardTable").removeClass('hidden');
        }


        var qty = $("#jobCardTable #jobCardDataRow #jobCardQty #jobCardQuantity").val();
        var cost = $("#jobCardTable #jobCardDataRow #jobCardServiceRate #jobCardTotal").val();
        var rate = $("#jobCardTable #jobCardDataRow #jobCardServiceRateTotal #jobCardRate").val();



        savedJobCardDetailsObject[$data['jobCardID']] = new jobCard($data['jobCardID'], qty, $data['serviceRate'], $data['serviceRate']);
    }

    $('.job-card-table').on('focusout', '#jobCardQuantity,#jobCardRate', function(e){
        e.preventDefault();
        var $thisRow = $(this).parents('tr');
        var itemQuentity = $thisRow.find('#jobCardQuantity').val();
        var itemCost = (toFloat($thisRow.find('#jobCardRate').val().replace(/,/g, '')) * toFloat(itemQuentity));
        $thisRow.find('#jobCardTotal').val(accounting.formatMoney(itemCost));
    });

    $(document).on('click', '.job-card-save', function() {

        var quantity = $(this).closest('tr').find("input[name='jobCardQuantity']").val();
        var unitRate = $(this).closest('tr').find("input[name='jobCardRate']").val();
        
        // var serviceTrID = $(this).closest('tr').attr('id');
        var jobCardID = $(this).closest('tr').attr('data-job-card-id');
        if (savedJobCardDetailsObject[jobCardID]) {
            savedJobCardDetailsObject[jobCardID].jobCardQty = quantity;
            savedJobCardDetailsObject[jobCardID].jobCardUnitCost = parseFloat(unitRate.replace(/,/g, ''));
            savedJobCardDetailsObject[jobCardID].jobCardUnitRate = parseFloat(unitRate.replace(/,/g, ''));
            $(this).closest('tr').find("input#jobCardQuantity").attr('disabled', true);
            $(this).closest('tr').find("input#jobCardRate").attr('disabled', true);
            $(this).addClass('hidden');
            $(this).closest('tr').find(".job-card-edit").removeClass('hidden');
        }
    });


    function loadJobServices(value)
    {

        if (value.jobTaskTaskCardID == null) {
            var newTrID = 'tr_' + value.jobServiceID;
            var clonedRow = $($('#servicePreSetSample').clone()).attr('id', newTrID).addClass('addedServices');
            $("#serviceCode", clonedRow).text(value.serviceCode);
            $("#serviceName", clonedRow).text(value.serviceName);
            $("input[name='quantity']", clonedRow).val('1.00');
            $("input[name='serviceRate']", clonedRow).val(accounting.formatMoney(value.serviceRate));
            $("input[name='serviceTotal']", clonedRow).val(accounting.formatMoney(value.serviceRate));
            clonedRow.removeClass('hidden');
            clonedRow.insertBefore('#servicePreSetSample');

            savedJobTaskDetailsObject[value.jobServiceID] = new jobTask(value.jobServiceID,1,value.serviceRate,value.serviceRate);

            $(".service-save", clonedRow).addClass('hidden');
            $(".service-edit", clonedRow).removeClass('hidden');
        }
    }

	function loadJobTradeMaterials(value)
    {
    	if (value.jobProductMaterialTypeID == 2) {
	        var newTrID = 'tr_' + value.jobProductID;
	        var clonedRow = $($('#materialsPreSetSample').clone()).attr('id', newTrID).addClass('addedTradeMaterials');
	        $("#itemCode", clonedRow).text(value.productCode);
	        $("#itemName", clonedRow).text(value.productName);
            var jobProductIssuedQty = (isNaN(value.jobProductIssuedQty)) ? 0 : value.jobProductIssuedQty;
	        $("input[name='quantity']", clonedRow).val(parseFloat(jobProductIssuedQty).toFixed(2));
	        $("input[name='itemUnitPrice']", clonedRow).val(parseFloat(value.jobProductUnitPrice).toFixed(2));
	        var productTotal = parseFloat(jobProductIssuedQty) * parseFloat(value.jobProductUnitPrice);
	        $("input[name='itemTotal']", clonedRow).val(productTotal.toFixed(2));
	        clonedRow.removeClass('hidden');
	        clonedRow.insertBefore('#materialsPreSetSample');
    		
	        $(".item-save", clonedRow).addClass('hidden');
	        $(".item-edit", clonedRow).removeClass('hidden');
    	}

        savedJobProductDetails[value.jobProductID] = new jobTaskProduct(value.jobProductTaskID, value.jobProductID, value.jobProductIssuedQty, value.jobProductUnitPrice, value.locationProductID);

    }

    $(document).on('click', '.view-service-materials', function() {
        var jobServiceTrID = $(this).closest('tr').attr('id');
        var jobServiceID = jobServiceTrID.split('tr_')[1].trim();

        $("#viewServiceMaterialModal #serviceMaterialTable tbody .addedServiceMaterials").remove();
        $.each(jobServices[jobServiceID].serviceProducts, function(index, val) {
            var newTrID = 'tr_' + index;
            var clonedRow = $($('#preSample1').clone()).attr('id', newTrID).addClass('addedServiceMaterials');
            $("#productName", clonedRow).text(val.productCode+" - "+val.productName);
            $("#qty", clonedRow).text(val.jobProductIssuedQty);
            clonedRow.removeClass('hidden');
            clonedRow.insertBefore('#preSample1');
        });

        if (Object.keys(jobServices[jobServiceID].serviceProducts).length > 0) {
        	$('#viewServiceMaterialModal').modal('show');
        } else {
            p_notification(false, eb.getMessage('NO_ANY_MATERIALS_HAVE'));
            return;
        }
    });

	$(document).on('click', '.view-service-sub-tasks', function() {
        var jobServiceTrID = $(this).closest('tr').attr('id');
        var jobServiceID = jobServiceTrID.split('tr_')[1].trim();

        $("#viewServiceSubTaskModal #serviceSubTaskTable tbody .addedServiceSubTasks").remove();
        $.each(jobServices[jobServiceID].serviceSubTasks, function(index, val) {
            var newTrID = 'tr_' + index;
            var clonedRow = $($('#preSample2').clone()).attr('id', newTrID).addClass('addedServiceSubTasks');
            $("#subTaskCode", clonedRow).text(val.subTaskCode);
            $("#subTaskName", clonedRow).text(val.subTaskName);
            clonedRow.removeClass('hidden');
            clonedRow.insertBefore('#preSample2');
        });

        if (Object.keys(jobServices[jobServiceID].serviceSubTasks).length > 0) {
        	$('#viewServiceSubTaskModal').modal('show');
        } else {
            p_notification(false, eb.getMessage('NO_ANY_SUB_TASK_HAVE'));
            return;
        }
    });

    $(document).on('click', '.service-edit', function() {
        $(this).closest('tr').find("input#quantity").attr('disabled', false);
        $(this).closest('tr').find("input#serviceRate").attr('disabled', false);
        $(this).addClass('hidden');
        $(this).closest('tr').find(".service-save").removeClass('hidden');
        $(this).closest('tr').addClass('productToSave');
    });

    $('.service-table').on('focusout', '#quantity,#serviceRate', function(e){
        e.preventDefault();
        var $thisRow = $(this).parents('tr');
        var itemQuentity = $thisRow.find('#quantity').val();
        var itemCost = (toFloat($thisRow.find('#serviceRate').val().replace(/,/g, '')) * toFloat(itemQuentity));
        $thisRow.find('#serviceTotal').val(accounting.formatMoney(itemCost));
    });

    $(document).on('click', '.service-save', function() {

        var quantity = $(this).closest('tr').find("input[name='quantity']").val();
        var unitRate = $(this).closest('tr').find("input[name='serviceRate']").val();
        
        var serviceTrID = $(this).closest('tr').attr('id');
        var serviceID = serviceTrID.split('tr_')[1].trim();
        if (savedJobTaskDetailsObject[serviceID]) {
            savedJobTaskDetailsObject[serviceID].jobTaskQty = toFloat(quantity.replace(",", ""));
            savedJobTaskDetailsObject[serviceID].jobTaskUnitCost = toFloat(unitRate.replace(",", ""));
            savedJobTaskDetailsObject[serviceID].jobTaskUnitRate = toFloat(unitRate.replace(",", ""));
            $(this).closest('tr').find("input#quantity").attr('disabled', true);
            $(this).closest('tr').find("input#serviceRate").attr('disabled', true);
            $(this).addClass('hidden');
            $(this).closest('tr').find(".service-edit").removeClass('hidden');
        }
    });

    $(document).on('click', '.item-edit', function() {
        $(this).closest('tr').find("input#itemUnitPrice").attr('disabled', false);
        $(this).addClass('hidden');
        $(this).closest('tr').find(".item-save").removeClass('hidden');
        $(this).closest('tr').addClass('productToSave');
    });

    $('.materials-table').on('focusout', '#itemUnitPrice', function(e){
        e.preventDefault();
        var $thisRow = $(this).parents('tr');
        var itemQuentity = $thisRow.find('#quantity').val();
        var itemCost = (toFloat($thisRow.find('#itemUnitPrice').val()) * toFloat(itemQuentity));
        $thisRow.find('#itemTotal').val(accounting.formatMoney(itemCost));
    });

    $(document).on('click', '.item-save', function() {

        var unitRate = $(this).closest('tr').find("input[name='itemUnitPrice']").val();
        
        var materialProTrID = $(this).closest('tr').attr('id');
        var materialProID = materialProTrID.split('tr_')[1].trim();
        if (savedJobProductDetails[materialProID]) {
            savedJobProductDetails[materialProID].jobProductUnitPrice = unitRate;
            $(this).closest('tr').find("input#itemUnitPrice").attr('disabled', true);
            $(this).addClass('hidden');
            $(this).closest('tr').find(".item-edit").removeClass('hidden');
        }
    });

    function getRateCardDetails(rateCardID) 
    {
    	eb.ajax({
	        type: 'POST',
	        url: BASE_URL + '/rate-card-api/getRelatedServiceRates',
	        data: {rateCardID: rateCardID},
	        success: function(respond) {
	            $.each(jobServices, function(index, val) {
	            	$.each(respond.data, function(index2, val2) {
	            		if (val.serviceID == index2) {
	            			if ($.isEmptyObject(val2.subTaskRates)) {
	            				if (savedJobTaskDetailsObject[index]) {
	            					savedJobTaskDetailsObject[index].jobTaskUnitCost = parseFloat(val2.rate);
	            					savedJobTaskDetailsObject[index].jobTaskUnitRate = parseFloat(val2.rate);
	            					$('#tr_'+index).find("input#serviceRate").val(accounting.formatMoney(val2.rate));
	            					$("input#serviceRate", $('#tr_'+index).closest('tr')).trigger(jQuery.Event("focusout"));
	            				}
	            			} else {
	            				var subTaskServiceRate = 0;
	            				$.each(val2.subTaskRates, function(index3, val3) {
	            					$.each(val.serviceSubTasks, function(index4, val4) {
	            						 if (index3 == val4.subTaskID) {
	            						 	subTaskServiceRate += parseFloat(val3.rate);
	            						 }
	            					});
	            				});
	            				if (savedJobTaskDetailsObject[index]) {
	            					savedJobTaskDetailsObject[index].jobTaskUnitCost = parseFloat(subTaskServiceRate);
	            					savedJobTaskDetailsObject[index].jobTaskUnitRate = parseFloat(subTaskServiceRate);
	            					$('#tr_'+index).find("input#serviceRate").val(accounting.formatMoney(subTaskServiceRate));
	            					$("input#serviceRate", $('#tr_'+index).closest('tr')).trigger(jQuery.Event("focusout"));
	            				}
	            			}
	            		}
	            	});
	            });
	        },
	        async: false
	    });
    }

    $('#save-cost').on('click', function() {
     	formData = {
            'jobCost' : savedOtherJobCostDetails,
            'jobProduct' : savedJobProductDetails,
            'jobTask' : savedJobTaskDetailsObject,
            'jobCard' : savedJobCardDetailsObject,
            'jobID' : $('#jobId').val(),
            'otherMaterials': serviceMaterials,
            'invoiceComment' : $('#invoiceComment').val()
        };
        if (validationCostSave(formData)) {
            eb.ajax({
                url: BASE_URL + '/api/serviceCost/save',
                method: 'post',
                data: formData,
                dataType: 'json',
                success: function(respond) {
                    p_notification(respond.status,respond.msg);
                    if (respond.status) {
                        showInvoicePreview(BASE_URL + '/invoice/invoiceView/' + respond.data.salesInvoiceID);
                        window.setTimeout(function() {
                            window.location.assign(BASE_URL + "/invoice/view");
                        }, 5000);
                    } 
                    
                }
            });
        }
    });

    function validationCostSave(data)
    {
        if ((!Array.isArray(data.jobCost) || !data.jobCost.length) 
                && (!Array.isArray(data.jobProduct) || !data.jobProduct.length)
                    && (Object.keys(data.jobTask).length == 0) && (Object.keys(data.jobCard).length == 0)) {
            p_notification(false, eb.getMessage('NO DATA_IN_ARRAY'));
            $('#save-cost').attr("disabled", false);
            return false; 

        } else {
            $('#save-cost').attr("disabled", true);
            return true;
        }
    }

    $('.jEmployees').on('click', function(event) {
        event.preventDefault();
        $("#viewServiceEmployeeModal #serviceEmployeesTable tbody .addedEmployees").remove();
        eb.ajax({
            url: BASE_URL + '/api/serviceCost/getJobEmployeeByJobId',
            method: 'post',
            data: {jobID:$(this).attr('id')},
            dataType: 'json',
            success: function(respond) {
                if (respond.status) {
                    $.each(respond.data, function(index, val) {
                        var newTrID = 'tr_' + val.jobTaskID;
                        var clonedRow = $($('#preSample').clone()).attr('id', newTrID).addClass('addedEmployees');
                        if (val.employeeCode != null) {
                            $("#employeeCode", clonedRow).text(val.employeeCode);
                            $("#employeeName", clonedRow).text(val.employeeName);
                        } else {
                            $("#employeeName", clonedRow).text("Default Employee");
                        }
                        clonedRow.removeClass('hidden');
                        clonedRow.insertBefore('#preSample');
                    });
                }
            }
        });  
        $('#viewServiceEmployeeModal').modal('show');
    })
});