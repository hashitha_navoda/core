var jobVehicleStatus = {};
var boxType = null;
$(document).ready(function() {
    var locationID = $('#navBarSelectedLocation').attr('data-locationid');
    $('.statusChangeBox').popover({
        html: true,
        placement: 'bottom',
        trigger:'click',
        content: $("#secret").html()
    }).on('click', function(event) {
        event.preventDefault();
        boxType = $(this).data('box-type');
    });

    var isMyPopoverVisible = false;//assuming popovers are hidden by default  

    $(".statusChangeBox").on('shown.bs.popover',function(){
    isMyPopoverVisible = true;
    });

    $(".statusChangeBox").on('hidden.bs.popover',function(){
    isMyPopoverVisible = false;
    });

    $(".status-type-container").on("click", ".statusOption", function(e) {
      e.preventDefault();
      if (isMyPopoverVisible == false) {
        return false;
      }
      var toggleClassName;
      var statusOption = $(this).children('p')[0].innerText;
      switch (boxType) {
        case 'air-filter':
            toggleClassName = 'airFliterDiv';
            break;
        case 'engine-oil':
            toggleClassName = 'engineOildiv';
            break;
        case 'wiper-fluid':
            toggleClassName = 'wiperFluidDiv';
            break;
        case 'wiper-blades':
            toggleClassName = 'wiperBladesDiv';
            break;
        case 'tyres':
            toggleClassName = 'tyresDiv';
            break;
        case 'check-belts':
            toggleClassName = 'checkBeltsDiv';
            break;
        case 'coolant':
            toggleClassName = 'coolantDiv';
            break;
        case 'brake-fluid':
            toggleClassName = 'brakeFluidDiv';
            break;
        case 'steering-fluid':
            toggleClassName = 'steeringFluidDiv';
            break;
        case 'transmission-fluid':
            toggleClassName = 'transmissionFluidDiv';
            break;
        case 'battery':
            toggleClassName = 'batteryDiv';
            break;
        case 'oil-filter':
            toggleClassName = 'oilFliterDiv';
            break;
        case 'gear-box-oil':
            toggleClassName = 'gearBoxOilDiv';
            break;
        case 'differential-oil':
            toggleClassName = 'differentialOilDiv';
            break;
        case 'ac-cabin-filter':
            toggleClassName = 'acCabinFilterDiv';
            break;
        case 'fuel-filter':
            toggleClassName = 'fuelFilterDiv';
            break;
        case 'radiator-coolant-level':
            toggleClassName = 'radiatorCoolantLevelDiv';
            break;
        case 'power-steering-oil':
            toggleClassName = 'powerSteeringOilDiv';
            break;
        case 'radiatorHoses':
            toggleClassName = 'radiatorHosesDiv';
            break;
        case 'driveBelts':
            toggleClassName = 'driveBeltsDiv';
            break;
        case 'engineMount':
            toggleClassName = 'engineMountDiv';
            break;
        case 'shockMounts':
            toggleClassName = 'shockMountsDiv';
            break;
        case 'silencerMounts':
            toggleClassName = 'silencerMountsDiv';
            break;
        case 'axleBoots':
            toggleClassName = 'axleBootsDiv';
            break;
        case 'headLamps':
            toggleClassName = 'headLampsDiv';
            break;
        case 'signalLamps':
            toggleClassName = 'signalLampsDiv';
            break;
        case 'parkingLamps':
            toggleClassName = 'parkingLampsDiv';
            break;
        case 'fogLamps':
            toggleClassName = 'fogLampsDiv';
            break;
        case 'tailLamp':
            toggleClassName = 'tailLampDiv';
            break;
        case 'roomLamp':
            toggleClassName = 'roomLampDiv';
            break;
        case 'wScreenWasherFluid':
            toggleClassName = 'wScreenWasherFluidDiv';
            break;
        case 'rackBoots':
            toggleClassName = 'rackBootsDiv';
            break;
        case 'brakePadsLiners':
            toggleClassName = 'brakePadsLinersDiv';
            break;
        case 'tyreWareUneven':
            toggleClassName = 'tyreWareUnevenDiv';
            break;
        case 'wheelNutsStuds':
            toggleClassName = 'wheelNutsStudsDiv';
            break;
        default:
            // statements_def
            break;
      }

      $("."+toggleClassName + ' .status-type').removeClass('hidden');
      $("."+toggleClassName).removeClass('printDiv');
      $("."+toggleClassName + ' .status-type').text(statusOption);
      $("."+toggleClassName + ' .status-type').siblings('img').removeClass('hideDiv');
      $("."+toggleClassName + ' .status-type').removeClass('before-selecteding-type');
      $("."+toggleClassName + ' .status-type').siblings('hr').removeClass('default-status-line');
      $("."+toggleClassName + ' .status-type').siblings('hr').addClass('status-line');
      $("."+toggleClassName + ' .status-type').siblings('hr').removeClass('default-transmission-fluid-status-line');
      $("."+toggleClassName + ' .status-type').siblings('hr').addClass('transmission-fluid-status-line');
      setVehicleStatus(boxType, statusOption);
      $("."+toggleClassName).popover('toggle');
    });

  
    var jobVehicleID = $('.jobVehicleID').val();
    if (jobVehicleStatus[jobVehicleID] == undefined) {
        jobVehicleStatus[jobVehicleID] = new vehicleStatus();
    }

    jobVehicleStatus[jobVehicleID].airFilter = $('.airFilter').val();
    jobVehicleStatus[jobVehicleID].engineOil = $('.engineOil').val();
    jobVehicleStatus[jobVehicleID].wiperFluid = $('.wiperFluid').val();
    jobVehicleStatus[jobVehicleID].wiperBlades = $('.wiperBlades').val();
    jobVehicleStatus[jobVehicleID].tyres = $('.tyres').val();
    jobVehicleStatus[jobVehicleID].checkBelts = $('.checkBelts').val();
    jobVehicleStatus[jobVehicleID].coolant = $('.coolant').val();
    jobVehicleStatus[jobVehicleID].brakeFluid = $('.brakeFluid').val();
    jobVehicleStatus[jobVehicleID].steeringFluid = $('.steeringFluid').val();
    jobVehicleStatus[jobVehicleID].transmissionFluid = $('.transmissionFluid').val();
    jobVehicleStatus[jobVehicleID].battery = $('.battery').val();
    jobVehicleStatus[jobVehicleID].oilFilter = $('.oilFliter').val();;
    jobVehicleStatus[jobVehicleID].gearBoxOil = $('.gearBoxOil').val();;
    jobVehicleStatus[jobVehicleID].differentialOil = $('.differentialOil').val();;
    jobVehicleStatus[jobVehicleID].acCabinFilter = $('.acCabinFilter').val();;
    jobVehicleStatus[jobVehicleID].fuelFilter = $('.fuelFilter').val();;
    jobVehicleStatus[jobVehicleID].radiatorCoolantLevel = $('.radiatorCoolantLevel').val();;
    jobVehicleStatus[jobVehicleID].powerSteeringOil = $('.powerSteeringOil').val();;
    jobVehicleStatus[jobVehicleID].radiatorHoses = $('.radiatorHoses').val();;
    jobVehicleStatus[jobVehicleID].driveBelts = $('.driveBelts').val();;
    jobVehicleStatus[jobVehicleID].engineMount = $('.engineMount').val();;
    jobVehicleStatus[jobVehicleID].shockMounts = $('.shockMounts').val();;
    jobVehicleStatus[jobVehicleID].silencerMounts = $('.silencerMounts').val();;
    jobVehicleStatus[jobVehicleID].axleBoots = $('.axleBoots').val();;
    jobVehicleStatus[jobVehicleID].headLamps = $('.headLamps').val();;
    jobVehicleStatus[jobVehicleID].signalLamps = $('.signalLamps').val();;
    jobVehicleStatus[jobVehicleID].parkingLamps = $('.parkingLamps').val();;
    jobVehicleStatus[jobVehicleID].fogLamps = $('.fogLamps').val();;
    jobVehicleStatus[jobVehicleID].tailLamp = $('.tailLamp').val();;
    jobVehicleStatus[jobVehicleID].roomLamp = $('.roomLamp').val();;
    jobVehicleStatus[jobVehicleID].wScreenWasherFluid = $('.wScreenWasherFluid').val();;
    jobVehicleStatus[jobVehicleID].rackBoots = $('.rackBoots').val();;
    jobVehicleStatus[jobVehicleID].brakePadsLiners = $('.brakePadsLiners').val();;
    jobVehicleStatus[jobVehicleID].tyreWareUneven = $('.tyreWareUneven').val();;
    jobVehicleStatus[jobVehicleID].wheelNutsStuds = $('.wheelNutsStuds').val();;

  function setVehicleStatus(item, status) {
    var jobVehicleID = $('.jobVehicleID').val();

    if (jobVehicleStatus[jobVehicleID] == undefined) {
        jobVehicleStatus[jobVehicleID] = new vehicleStatus();
    }

    switch (item) {
        case 'air-filter':
            jobVehicleStatus[jobVehicleID].airFilter = status;
            break;
        case 'engine-oil':
            jobVehicleStatus[jobVehicleID].engineOil = status;
            break;
        case 'wiper-fluid':
            jobVehicleStatus[jobVehicleID].wiperFluid = status;
            break;
        case 'wiper-blades':
            jobVehicleStatus[jobVehicleID].wiperBlades = status;
            break;
        case 'tyres':
            jobVehicleStatus[jobVehicleID].tyres = status;
            break;
        case 'check-belts':
            jobVehicleStatus[jobVehicleID].checkBelts = status;
            break;
        case 'coolant':
            jobVehicleStatus[jobVehicleID].coolant = status;
            break;
        case 'brake-fluid':
            jobVehicleStatus[jobVehicleID].brakeFluid = status;
            break;
        case 'steering-fluid':
            jobVehicleStatus[jobVehicleID].steeringFluid = status;
            break;
        case 'transmission-fluid':
            jobVehicleStatus[jobVehicleID].transmissionFluid = status;
            break;
        case 'battery':
            jobVehicleStatus[jobVehicleID].battery = status;
            break;
        case 'oil-filter':
            jobVehicleStatus[jobVehicleID].oilFilter = status;
            break;
        case 'gear-box-oil':
            jobVehicleStatus[jobVehicleID].gearBoxOil = status;
            break;
        case 'differential-oil':
            jobVehicleStatus[jobVehicleID].differentialOil = status;
            break;
        case 'ac-cabin-filter':
            jobVehicleStatus[jobVehicleID].acCabinFilter = status;
            break;
        case 'fuel-filter':
            jobVehicleStatus[jobVehicleID].fuelFilter = status;
            break;
        case 'radiator-coolant-level':
            jobVehicleStatus[jobVehicleID].radiatorCoolantLevel = status;
            break;
        case 'power-steering-oil':
            jobVehicleStatus[jobVehicleID].powerSteeringOil = status;
            break;
        case 'radiatorHoses':
            jobVehicleStatus[jobVehicleID].radiatorHoses = status;
            break;
        case 'driveBelts':
            jobVehicleStatus[jobVehicleID].driveBelts = status;
            break;
        case 'engineMount':
            jobVehicleStatus[jobVehicleID].engineMount = status;
            break;
        case 'shockMounts':
            jobVehicleStatus[jobVehicleID].shockMounts = status;
            break;
        case 'silencerMounts':
            jobVehicleStatus[jobVehicleID].silencerMounts = status;
            break;
        case 'axleBoots':
            jobVehicleStatus[jobVehicleID].axleBoots = status;
            break;
        case 'headLamps':
            jobVehicleStatus[jobVehicleID].headLamps = status;
            break;
        case 'signalLamps':
            jobVehicleStatus[jobVehicleID].signalLamps = status;
            break;
        case 'parkingLamps':
            jobVehicleStatus[jobVehicleID].parkingLamps = status;
            break;
        case 'fogLamps':
            jobVehicleStatus[jobVehicleID].fogLamps = status;
            break;
        case 'tailLamp':
            jobVehicleStatus[jobVehicleID].tailLamp = status;
            break;
        case 'roomLamp':
            jobVehicleStatus[jobVehicleID].roomLamp = status;
            break;
        case 'wScreenWasherFluid':
            jobVehicleStatus[jobVehicleID].wScreenWasherFluid = status;
            break;
        case 'rackBoots':
            jobVehicleStatus[jobVehicleID].rackBoots = status;
            break;
        case 'brakePadsLiners':
            jobVehicleStatus[jobVehicleID].brakePadsLiners = status;
            break;
        case 'tyreWareUneven':
            jobVehicleStatus[jobVehicleID].tyreWareUneven = status;
            break;
        case 'wheelNutsStuds':
            jobVehicleStatus[jobVehicleID].wheelNutsStuds = status;
            break;
        default:
            // statements_def
            break;
    }
  }

    $('.save-button-div').on('click', function(event) {
        event.preventDefault();
        var jobVehicleID = $('.jobVehicleID').val();
        var jobID = $('.jobID').val();

        if (jobVehicleStatus[jobVehicleID] == undefined) {
            jobVehicleStatus[jobVehicleID] = new vehicleStatus();
        }

        jobVehicleStatus[jobVehicleID].frontLeftTyrePressure = $('.frontLeftTyrePressure').val();
        jobVehicleStatus[jobVehicleID].rearLeftTyrePressure = $('.rearLeftTyrePressure').val();
        jobVehicleStatus[jobVehicleID].frontRightTyrePressure = $('.frontRightTyrePressure').val();
        jobVehicleStatus[jobVehicleID].rearRightTyrePressure = $('.rearRightTyrePressure').val();
        jobVehicleStatus[jobVehicleID].odoMeter = $('.odoMeter').val();
        jobVehicleStatus[jobVehicleID].brakePadsFrontfactor = $('.brakePadsFrontfactor').val();
        jobVehicleStatus[jobVehicleID].brakePadsRearfactor = $('.brakePadsRearfactor').val();
        jobVehicleStatus[jobVehicleID].brakePadsChangeFluid = $('.brakePadsChangeFluid').val();

        eb.ajax({
            type: 'POST', url: BASE_URL + '/service-job-dashboard-api/saveVehicleStatus', 
            data: {
                jobVehicleID: jobVehicleID,
                jobID: jobID,
                vehicleStatusData: jobVehicleStatus[jobVehicleID]
            }, 
            success: function(respond) {
                p_notification(respond.status, respond.msg);
                if (respond.status == true) {
                    getJobVehicleRelatedStatus(jobID);
                    // window.location.reload();
                } 
            }
        });
    });

    function vehicleStatus(airFilter, engineOil, wiperFluid, wiperBlades, tyres, checkBelts, coolant, brakeFluid, steeringFluid, transmissionFluid, battery, frontLeftTyrePressure, rearLeftTyrePressure, frontRightTyrePressure, rearRightTyrePressure, odoMeter, brakePadsFrontfactor, brakePadsRearfactor, brakePadsChangeFluid, oilFilter, gearBoxOil,
      differentialOil, acCabinFilter, fuelFilter, radiatorCoolantLevel, powerSteeringOil, radiatorHoses, driveBelts, engineMount, shockMounts, silencerMounts, 
      axleBoots, headLamps,signalLamps, parkingLamps, fogLamps, tailLamp, roomLamp, wScreenWasherFluid, rackBoots, brakePadsLiners,  tyreWareUneven, wheelNutsStuds) {
        this.airFilter = airFilter;
        this.engineOil = engineOil;
        this.wiperFluid = wiperFluid;
        this.wiperBlades = wiperBlades;
        this.tyres = tyres;
        this.checkBelts = checkBelts;
        this.coolant = coolant;
        this.brakeFluid = brakeFluid;
        this.steeringFluid = steeringFluid;
        this.transmissionFluid = transmissionFluid;
        this.battery = battery;
        this.frontLeftTyrePressure = frontLeftTyrePressure;
        this.rearLeftTyrePressure = rearLeftTyrePressure;
        this.frontRightTyrePressure = frontRightTyrePressure;
        this.rearRightTyrePressure = rearRightTyrePressure;
        this.odoMeter = odoMeter;
        this.brakePadsFrontfactor = brakePadsFrontfactor;
        this.brakePadsRearfactor = brakePadsRearfactor;
        this.brakePadsChangeFluid = brakePadsChangeFluid;
        this.oilFilter = oilFilter
        this.gearBoxOil = gearBoxOil
        this.differentialOil = differentialOil
        this.acCabinFilter = acCabinFilter
        this.fuelFilter = fuelFilter
        this.radiatorCoolantLevel = radiatorCoolantLevel
        this.powerSteeringOil = powerSteeringOil
        this.radiatorHoses = radiatorHoses
        this.driveBelts = driveBelts
        this.engineMount = engineMount
        this.shockMounts = shockMounts
        this.silencerMounts = silencerMounts
        this.axleBoots = axleBoots
        this.headLamps = headLamps
        this.signalLamps = signalLamps
        this.parkingLamps = parkingLamps
        this.fogLamps = fogLamps
        this.tailLamp = tailLamp
        this.roomLamp = roomLamp
        this.wScreenWasherFluid = wScreenWasherFluid
        this.rackBoots = rackBoots
        this.brakePadsLiners = brakePadsLiners
        this.tyreWareUneven = tyreWareUneven
        this.wheelNutsStuds = wheelNutsStuds
    }

    $('.remarks-button-div').on('click', function(event) {
        event.preventDefault();
        $('.remarksDiv').removeClass('hidden');
        $('.statusDiv').addClass('hidden');
    });

    $('.back-button-div').on('click', function(event) {
        event.preventDefault();
        $('.statusDiv').removeClass('hidden');
        $('.remarksDiv').addClass('hidden');
    });  
});  