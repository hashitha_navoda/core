var jobCardId = 0;
var vehicleTypeId = 0;
var serviceList = [];
var serviceMaterialsList = [];
var tradeMaterialsList = [];

$(document).ready(function() {

    var $serviceList = $('#serviceList');
    var $serviceListTbody = $('#serviceListTbody', $serviceList);
    var $serviceListSampleRow = $('tr.sample', $serviceListTbody);

    var $addMaterialsModal = $('#addMaterialsModal');
    var $serviceMaterialsTbody = $('#serviceMaterials', $addMaterialsModal);
    var $serviceMaterialsSampleRow = $('.sample', $serviceMaterialsTbody);
    var $tradeMaterialsTbody = $('#tradeMaterials', $addMaterialsModal);
    var $tradeMaterialsSampleRow = $('.sample', $tradeMaterialsTbody);
    
    var $viewMaterialsModal = $('#viewMaterialsModal');
    var $serviceMaterialsViewTbody = $('#serviceMaterials', $viewMaterialsModal);
    var $serviceMaterialsViewSampleRow = $('.sample', $serviceMaterialsViewTbody);
    var $tradeMaterialsViewTbody = $('#tradeMaterials', $viewMaterialsModal);
    var $tradeMaterialsViewSampleRow = $('.sample', $tradeMaterialsViewTbody);

    var locationID = $('#navBarSelectedLocation').attr('data-locationid');
    loadDropDownFromDatabase('/productAPI/search-location-products-for-dropdown', locationID, 0, '.locationProductID', '', '', 'taskCard');
    $('.locationProductID').on('change', function() {
        $tr = $(this).parents('tr');
        var selectLocationProductID = $('#selectLocationProductID', $tr).val();
        if(!($(this).val() == null || $(this).val() == 0) && selectLocationProductID != $(this).val()){
            eb.ajax({
                type: 'POST',
                url: BASE_URL + '/productAPI/get-location-product-details',
                data: {
                    productID: $(this).val(),
                    locationID: locationID
                },
                success: function(respond) {
                    if (respond.data) {
                        $('#selectLocationProductID', $tr).val(respond.data.lPID);
                        $('#selectLocationProductName', $tr).val(respond.data.pN);
                        $('#uom option:not(:first)', $tr).remove();
                        $.each(respond.data.uom, function(index, value) {
                            $('#uom', $tr).append('<option value="' + value.uomID + '">' + value.uN + '(' + value.uA + ')</option>');
                        });
                        $('#uom', $tr).prop('disabled', false).val(0).selectpicker('refresh');
                    }
                }
            });
        }
    });


    //loadDropDownFromDatabase('/task_card_api/search-task-cards-for-dropdown', "", 0, '#jobCardNumbers','', '', 'standerd-materials');
    loadDropDownFromDatabase('/api/resources/searchVehicleTypesForDropdown', "", 0, '#vehicleTypes', '', '', 'standerd-materials');
    $('#vehicleTypes').on('change', function() {
        if (!($(this).val() == null || $(this).val() == 0) && vehicleTypeId != $(this).val()){
            vehicleTypeId = $(this).val();
            requestServiceList(vehicleTypeId);
        }
    });

	$serviceListTbody.on('click', 'a.view', function(e) {
    	var $tr = $(this).parents('tr');
    	var jobCardServiceId = $tr.attr('id');
        $('#jobCardServiceId', $viewMaterialsModal).val(jobCardServiceId);
    	requestServiceProductList(jobCardServiceId);
    });

    $('.userInput').on('click', '#add', function(e) {
    	var $tr = $(this).parents('tr');
        var locationProductID = $('#selectLocationProductID', $tr).val();
    	var taskCardTaskProductName = $('#selectLocationProductName', $tr).val();
    	var taskCardTaskProductType = $tr.attr('taskCardTaskProductType');
    	var taskCardTaskProductQty = $('#qty', $tr).val();
    	var taskCardTaskProductUomID = $('#uom', $tr).val();
    	var taskCardTaskProductUomText = $('#uom', $tr).find("option:selected").text();
        var taskCardTaskProductAllUom = [];
        $('#uom option', $tr).each(function(){
            if($(this).val() > 0) {
                taskCardTaskProductAllUom.push({
                    uomID : $(this).val(),
                    uomText : $(this).html(),
                });
            }
        });

        var floatNumber = /^\d*\.?\d*$/;
    	if (locationProductID == 0 || locationProductID == '0' || locationProductID == null || locationProductID == '' || taskCardTaskProductName == null || taskCardTaskProductName == '') {
            $('#serviceMaterialsProductCode', $tr).focus();
            p_notification(false, eb.getMessage('ERR_VALI_ADD_STD_PROID'));
            return false;
    	} else if (serviceMaterialsList.some(el => el.locationProductID === locationProductID) || tradeMaterialsList.some(el => el.locationProductID === locationProductID)) {
            p_notification(false, eb.getMessage('ERR_VALI_ADD_STD_PROID_DUP'));
            return false;
    	} else if (taskCardTaskProductType == null || taskCardTaskProductType == '') {
			p_notification(false, eb.getMessage('ERR_VALI_ADD_STD_PROTYPE'));
            return false;
    	} else if (taskCardTaskProductQty == null || taskCardTaskProductQty == '' || !(taskCardTaskProductQty.match(floatNumber))) {
    		$('#qty', $tr).focus();
    		p_notification(false, eb.getMessage('ERR_VALI_ADD_STD_PROQTY'));
            return false;
    	} else if (taskCardTaskProductUomID == 0 || taskCardTaskProductUomID == '0' || taskCardTaskProductUomID == null || taskCardTaskProductUomID == '' || taskCardTaskProductUomText == null || taskCardTaskProductUomText == '') {
    		$('#uom', $tr).focus();
    		p_notification(false, eb.getMessage('ERR_VALI_ADD_STD_PROUOM'));
    		return false;
    	} else if (taskCardTaskProductAllUom.length < 1) {
            $('#uom', $tr).focus();
            p_notification(false, eb.getMessage('ERR_VALI_ADD_STD_PROUOM'));
            return false;
        }

    	var material = {
    		taskCardTaskProductID : 'new',
    		locationProductID : locationProductID,
    		taskCardTaskProductName : taskCardTaskProductName,
    		taskCardTaskProductType : taskCardTaskProductType,
    		taskCardTaskProductQty : taskCardTaskProductQty,
    		taskCardTaskProductUomID : taskCardTaskProductUomID,
            taskCardTaskProductAllUom : taskCardTaskProductAllUom,
    		taskCardTaskProductUomText : taskCardTaskProductUomText,
    	};

    	if (taskCardTaskProductType == 1) {
			serviceMaterialsList.push(material);
    	} else if (taskCardTaskProductType == 2) {
			tradeMaterialsList.push(material);
    	}

        clearInputFields($tr);
    	refreshServiceProductList();
    });

    $serviceMaterialsViewTbody.on('click', '#delete', function(e) {
        var $tr = $(this).parents('tr');
        var locationProductID = $('#locationProductID', $tr).val();

        serviceMaterialsList = $.grep(serviceMaterialsList, function(data, index) {
           return data.locationProductID != locationProductID;
        });
        refreshServiceProductList();
    });

    $tradeMaterialsViewTbody.on('click', '#delete', function(e) {
        var $tr = $(this).parents('tr');
        var locationProductID = $('#locationProductID', $tr).val();

        tradeMaterialsList = $.grep(tradeMaterialsList, function(data, index) {
           return data.locationProductID != locationProductID;
        });
        refreshServiceProductList();
    });

    $($serviceMaterialsViewTbody).on('click', '#edit', function(e) {
        var $tr = $(this).parents('tr');
        $('#edit', $tr).addClass('hidden');
        $('#save', $tr).removeClass('hidden');
        $('#qty', $tr).attr('disabled', false);
        $('#uom', $tr).attr('disabled', false);
        $('#uom', $tr).selectpicker('refresh');
    });

    $($tradeMaterialsViewTbody).on('click', '#edit', function(e) {
        var $tr = $(this).parents('tr');
        $('#edit', $tr).addClass('hidden');
        $('#save', $tr).removeClass('hidden');
        $('#qty', $tr).attr('disabled', false);
        $('#uom', $tr).attr('disabled', false);
        $('#uom', $tr).selectpicker('refresh');
    });

    $serviceMaterialsViewTbody.on('click', '#save', function(e) {
        var $tr = $(this).parents('tr');
        var locationProductID = $('#locationProductID', $tr).val();
        var qty = $('#qty', $tr).val();
        var uom = $('#uom', $tr).val();
        if(materialValidationForView(qty, uom)) {
            index = serviceMaterialsList.findIndex((obj => obj.locationProductID == locationProductID));
            serviceMaterialsList[index].taskCardTaskProductQty = qty;
            serviceMaterialsList[index].taskCardTaskProductUomID = uom;
            refreshServiceProductList();
            $('#edit', $tr).removeClass('hidden');
            $('#save', $tr).addClass('hidden');
        }

    });

    $tradeMaterialsViewTbody.on('click', '#save', function(e) {
        var $tr = $(this).parents('tr');
        var locationProductID = $('#locationProductID', $tr).val();
        var qty = $('#qty', $tr).val();
        var uom = $('#uom', $tr).val();
        if(materialValidationForView(qty, uom)) {
            index = tradeMaterialsList.findIndex((obj => obj.locationProductID == locationProductID));
            tradeMaterialsList[index].taskCardTaskProductQty = qty;
            tradeMaterialsList[index].taskCardTaskProductUomID = uom;
            refreshServiceProductList();
            $('#edit', $tr).removeClass('hidden');
            $('#save', $tr).addClass('hidden');
        }
    });

    $viewMaterialsModal.on('click', '#saveViewMaterialsModal', function(e) {
        if ($('.edit', $viewMaterialsModal).hasClass('hidden')) {
            p_notification(false, eb.getMessage('ERR_SAV_TR_JOB_CRD_SRV_MATE'));
        } else {
            var jobCardServiceId = $('#jobCardServiceId', $viewMaterialsModal).val();
            var materialsList = $.merge(serviceMaterialsList, tradeMaterialsList);
            eb.ajax({
                type: 'POST',
                url: BASE_URL + '/api/resources/editMaterials',
                data: {
                    jobCardServiceId: jobCardServiceId,
                    vehicleTypeID: $('#vehicleTypes').val(),
                    materialsList: materialsList
                },
                success: function(respond) {
                    p_notification(respond.status, respond.msg);
                    if (respond.status) {
                        $viewMaterialsModal.modal('hide');
                    }
                }
            });
        }
    });

    function requestServiceList(vehicleTypeId) {
    	eb.ajax({
    	    type: 'POST',
    	    url: BASE_URL + '/api/resources/getServiceList',
    	    data: {
                vehicleTypeID: vehicleTypeId
    	    },
    	    success: function(respond) {
    	        if (respond.status) {
                    if (respond.data != null) {
    	        	  serviceList = respond.data;
                    } else {
                        serviceList = [];
                    }
    	        	refreshServiceList();
    	        } else {
                    serviceList = [];
                    refreshServiceList();
		            p_notification(respond.status, respond.msg);
    	        }
    	    }
    	});
    }

    function refreshServiceList() {
    	var $newRow;
    	$('.serviceRow', $serviceListTbody).remove();
    	if (Object.keys(serviceList).length > 0) {
            $.each(serviceList, function(index, value) {
                $newRow = $($serviceListSampleRow.clone()).appendTo($serviceListTbody).attr('id', value.serviceID);
                $('#code', $newRow).html(value.taskCode);
                $('#name', $newRow).html(value.taskName);
                $newRow.removeClass('sample hidden');
                $newRow.addClass('serviceRow');
            });
        } else {
            $newRow = $($serviceListSampleRow.clone()).appendTo($serviceListTbody);
            $newRow.html('<td colspan="3">No results were found.</td>');
            $newRow.removeClass('sample hidden');
            $newRow.addClass('not_found serviceRow');
        }
    }

    function requestServiceProductList(serviceID) {
        eb.ajax({
    	    type: 'POST',
    	    url: BASE_URL + '/api/resources/getServiceProductListWithProductUom',
    	    data: {
    	        serviceID: serviceID,
                vehicleTypeID: $('#vehicleTypes').val(),
    	    },
    	    success: function(respond) {
    	        if (respond.status) {
    	        	if(respond.data['serviceMaterialsList'] != null) {
	    	        	serviceMaterialsList = respond.data['serviceMaterialsList'];    	        		
    	        	} else {
    	        		serviceMaterialsList = [];
    	        	}
    	        	if(respond.data['tradeMaterialsList'] != null) {
	    	        	tradeMaterialsList = respond.data['tradeMaterialsList'];
    	        	} else {
						tradeMaterialsList = [];
    	        	}
    	        	refreshServiceProductList();
    	        } else {
		            p_notification(respond.status, respond.msg);
    	        }
    	    }
    	});
    }

    function refreshServiceProductList() {
	    $('.serviceMaterialRow', $serviceMaterialsViewTbody).remove();
        $('.tradeMaterialRow', $tradeMaterialsViewTbody).remove();
        if (serviceMaterialsList != null && serviceMaterialsList.length > 0) {
            $.each(serviceMaterialsList, function(index, value) {
                $newRow = $($serviceMaterialsViewSampleRow.clone())
                    .appendTo($serviceMaterialsViewTbody)
                    .attr('id', value.taskCardTaskProductID);
                $('#code', $newRow).val(value.taskCardTaskProductName);
                $('#locationProductID', $newRow).val(value.locationProductID);
                $('#qty', $newRow).val(parseFloat(value.taskCardTaskProductQty));
                if (value.taskCardTaskProductAllUom != null && value.taskCardTaskProductAllUom.length > 0) {
                    $.each(value.taskCardTaskProductAllUom, function(i, v) {
                        $('#uom', $newRow).append('<option value="' + v.uomID + '">' + v.uomText + '</option>');
                    });
                }
                $('#uom', $newRow).selectpicker('render');
                $('#uom', $newRow).val(value.taskCardTaskProductUomID);
                $('#uom', $newRow).selectpicker('refresh');
                $newRow.removeClass('sample hidden');
                $newRow.addClass('serviceMaterialRow');
            });
        }
        if (tradeMaterialsList != null && tradeMaterialsList.length > 0) {
            $.each(tradeMaterialsList, function(index, value) {
                $newRow = $($tradeMaterialsViewSampleRow.clone())
                    .appendTo($tradeMaterialsViewTbody)
                    .attr('id', value.taskCardTaskProductID);
                $('#locationProductID', $newRow).val(value.locationProductID);
                $('#code', $newRow).val(value.taskCardTaskProductName);
                $('#qty', $newRow).val(parseFloat(value.taskCardTaskProductQty));
                if (value.taskCardTaskProductAllUom != null && value.taskCardTaskProductAllUom.length > 0) {
                    $.each(value.taskCardTaskProductAllUom, function(i, v) {
                        $('#uom', $newRow).append('<option value="' + v.uomID + '">' + v.uomText + '</option>');
                    });
                }
                $('#uom', $newRow).selectpicker('render');
                $('#uom', $newRow).val(value.taskCardTaskProductUomID);
                $('#uom', $newRow).selectpicker('refresh');
                $newRow.removeClass('sample hidden');
                $newRow.addClass('tradeMaterialRow');
            });
        }
		$viewMaterialsModal.modal('show');
    }

    function clearInputFields(tr) {
        if (tr != null) {
            $tr = tr;
        } else {
            $tr = $('.userInput');
        }
        $('#locationProductID', $tr).val(0);
        $('#locationProductID', $tr).selectpicker('render');
        $('#selectLocationProductID', $tr).val('');
        $('#selectLocationProductName', $tr).val('');
        $('#qty', $tr).val('');
        $('#uom', $tr).val(0);
        $('#uom', $tr).prop('disabled', true);
        $('#uom', $tr).selectpicker('render');
    }

    function materialValidationForView(qty, uom) {
        var floatNumber = /^\d*\.?\d*$/;
        if (qty == null || qty == '' || !(qty.match(floatNumber))) {
            p_notification(false, eb.getMessage('ERR_VALI_ADD_STD_PROQTY'));
            return false;
        } else if (uom == null || uom == '' || uom == '0' || uom == 0) {
            p_notification(false, eb.getMessage('ERR_VALI_ADD_STD_PROUOM'));
            return false;
        }
        return true;
    }
});