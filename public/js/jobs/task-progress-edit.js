$(document).ready(function() {

    var jobTaskProgressValue = null;
    var jobTaskUsedQuantity = null;
    var jobTaskID = null;
    var jobID = null;
    var projectID = null;
    var originalUsedQty = null;


    if (localStorage.getItem("job_list_search_params") != null) {
        $('#searchBackBtn').removeClass('hidden');
        $('#normalBackBtn').addClass('hidden');
        var jobListSearchParams = localStorage.getItem('job_list_search_params');
        var params = JSON.parse(jobListSearchParams);
        params.backAction = true;
        localStorage.setItem("job_list_search_params", JSON.stringify(params));
    } else {
        $('#normalBackBtn').removeClass('hidden');
        $('#searchBackBtn').addClass('hidden');
    }

    $('#normalBackBtn').on('click', function(){
         window.history.go(-1);
    });

    $('#viewSupervisors').on('click', function(){
        $("#viewJobSupervisorModal #jobSupervisorTable tbody .addedSupervisors").remove();
        var tempJobID = $('#jobId').val();
        eb.ajax({
            url: BASE_URL + '/job-api/getJobSupervisorsByJobID',
            method: 'post',
            data: {jobID:tempJobID},
            dataType: 'json',
            success: function(respond) {
                if (respond.status) { 
                    $.each(respond.data, function(index, val) {
                        var newTrID = 'tr_' + val.jobSupervisorID;
                        var clonedRow = $($('#preSample').clone()).attr('id', newTrID).addClass('addedSupervisors');
                        $("#supervisorName", clonedRow).text(val.employeeFirstName+" "+val.employeeSecondName);
                        clonedRow.removeClass('hidden');
                        clonedRow.insertBefore('#preSample');
                    });
                }
            }
        });  
        $('#viewJobSupervisorModal').modal('show');
    });

    $('#viewContractors').on('click', function(){
        $("#viewJobContractorModal #jobContractorTable tbody .addedcontractors").remove();
        var tempJobID = $('#jobId').val();
        eb.ajax({
            url: BASE_URL + '/job-api/getJobContractorsByJobID',
            method: 'post',
            data: {jobID:tempJobID},
            dataType: 'json',
            success: function(respond) {
                if (respond.status) {
                    $.each(respond.data, function(index, val) {
                        var newTrID = 'tr_' + val.jobSupervisorID;
                        var clonedRow = $($('#contractorRow').clone()).attr('id', newTrID).addClass('addedcontractors');
                        $("#contractorName", clonedRow).text(val.contractorFirstName+" "+val.contractorSecondName);
                        clonedRow.removeClass('hidden');
                        clonedRow.insertBefore('#contractorRow');
                    });
                }
            }
        });  
        $('#viewJobContractorModal').modal('show');
    });

    $('#job-task-list').on('click', '.startTask', function(){
        var jobTaskID = $(this).closest('tr').data('job-task-id');
        var jobID = $(this).closest('tr').data('job-id');
        var projectID = $(this).closest('tr').data('project-id');

        var actionType = 'start';
        updateTaskAction(actionType, jobTaskID, jobID, projectID);

    });

    $('#job-task-list').on('click', '.resumeTask', function(){
        var jobTaskID = $(this).closest('tr').data('job-task-id');
        var jobID = $(this).closest('tr').data('job-id');
        var projectID = $(this).closest('tr').data('project-id');

        var actionType = 'resume';
        updateTaskAction(actionType, jobTaskID, jobID, projectID);

    });

    function updateTaskAction(action, jobTaskID, jobID, projectID) {
        
        eb.ajax({
            type: 'POST',
            url: BASE_URL + '/progress-api/start-task',
            dataType: 'json',
            data: {
                jobTaskID : jobTaskID,
                jobID: jobID,
                action: action,
                projectID: projectID,
                status: 8
            },
            success: function(respond) {
                if (respond.status) {
                    window.location.reload();
                } else {
                    p_notification(respond.status, respond.msg);
                }        

            }
        });
    }

    $('#job-task-list').on('click', '.updateTaskProgress', function(){

        var tempJobTaskID = $(this).closest('tr').data('job-task-id');
        var tempJobID = $(this).closest('tr').data('job-id');
        var tempProjectID = $(this).closest('tr').data('project-id');
        var jobTaskStatus = $(this).closest('tr').data('job-task-status');

        eb.ajax({
            type: 'POST',
            url: BASE_URL + '/progress-api/get-task-details-by-id',
            data: {
                jobTaskID : tempJobTaskID,
            },
            success: function(respond) {
                if (respond.status) {
                    $('.taskProgressUpdateModalBody').html(respond.html);
                    var prgressVal = respond.data['jobTaskProgress'];
                    var usedQty = respond.data['jobTaskUsedQty'];
                    originalUsedQty = respond.data['jobTaskUsedQty'];
                    jobTaskID = tempJobTaskID;
                    jobID = tempJobID;
                    projectID = tempProjectID;
                    jobTaskProgressValue = prgressVal;
                    jobTaskUsedQuantity = usedQty;

                    var pVal = (prgressVal % 1 == 0) ? prgressVal : prgressVal.toFixed(2);
                    var temp = pVal+'%';
                    $('#progressVal').text(temp);
                    $('#taskProgressBar').css('width', pVal+'%').attr('aria-valuenow', pVal);
                    $('#taskProgress').val(pVal);
                    $('#taskProgressUpdateModal').modal('show');
                } else {
                    p_notification(respond.status, respond.msg);
                }        

            },
            async : false
        });

        if (jobTaskStatus == 9) {
            $('#taskProgressUpdateModal .taskProgressUpdateModalBody #holdTask').prop('disabled', true);
        }
    });

    $('#taskProgressUpdateModal').keyup('#usedQty',function() {
        
        var usedQuantity = $('#usedQty').val();
        var estQuantity = $('#jobTaskEstQty').val();
        $('#taskProgress').val(jobTaskProgressValue);

        if ($('#usedQty').val() == '') {
            usedQuantity = 0;
        }

        if (isNaN(Number(usedQuantity)) || Number(usedQuantity) > Number(estQuantity)) {
            $('#usedQty').val(jobTaskUsedQuantity)
            usedQuantity = jobTaskUsedQuantity;
        }

        var progressValue = (usedQuantity * 100) / estQuantity; 
        jobTaskProgressValue = progressValue;

        var pVal = (progressValue % 1 == 0) ? progressValue : progressValue.toFixed(2);
        var temp = pVal+'%';
        $('#progressVal').text(temp);
        $('#taskProgressBar').css('width', pVal+'%').attr('aria-valuenow', pVal);
        $('#taskProgress').val(pVal);

    });


    $('#taskProgressUpdateModal').on('click','#updateJobTaskProgress',function() {
        var tempJobTaskID = jobTaskID;
        var taskPrgress = jobTaskProgressValue;
        var usedQuantity = $('#usedQty').val();
        var tempJobID = jobID;
        var tempProjectID = projectID;

        eb.ajax({
            type: 'POST',
            url: BASE_URL + '/progress-api/update-job-task-progress',
            data: {
                jobTaskID : tempJobTaskID,
                jobTaskProgress : taskPrgress,
                jobID: tempJobID,
                projectID: tempProjectID,
                usedQuantity: usedQuantity
            },
            success: function(respond) {
                p_notification(respond.status, respond.msg);
                if (respond.status) {
                    $("#taskProgressUpdateModal").modal('toggle');
                    window.setTimeout(function() {
                        location.reload();
                    }, 1000);
                }
            }
        });

    });

    $('#taskProgressUpdateModal').on('click','#holdTask',function() {
        var tempJobTaskID = jobTaskID;
        var tempJobID = jobID;
        var tempProjectID = projectID;
        eb.ajax({
            type: 'POST',
            url: BASE_URL + '/progress-api/hold-task',
            dataType: 'json',
            data: {
                jobTaskID : tempJobTaskID,
                jobID: tempJobID,
                projectID: tempProjectID,
                status: 17
            },
            success: function(respond) {
                if (respond.status) {
                    window.location.reload();
                } else {
                    p_notification(respond.status, respond.msg);
                }        

            }
        });

    });

    $('#taskProgressUpdateModal').on('click','#closeProgressModal',function() {
        $("#taskProgressUpdateModal").modal('toggle');
    });

    $('#job-task-list').on('click','.endTask',function() {
        var tempJobTaskID = $(this).closest('tr').data('job-task-id');
        var tempJobID = $(this).closest('tr').data('job-id');
        var tempProjectID = $(this).closest('tr').data('project-id');
        eb.ajax({
            type: 'POST',
            url: BASE_URL + '/progress-api/end-task',
            dataType: 'json',
            data: {
                jobTaskID : tempJobTaskID,
                jobID: tempJobID,
                projectID: tempProjectID,
                status: 18
            },
            success: function(respond) {
                p_notification(respond.status, respond.msg);
                if (respond.status) {
                    window.setTimeout(function() {
                        location.reload();
                    }, 1000);
                }        
            }
        });

    });

    $('#btnBack').on('click', function(){
         window.history.go(-1);
    });

});
