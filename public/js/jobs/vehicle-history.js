var serviceVehicles = [];
var jobID = null;
var jobTaskID = null;
$(document).ready(function() {
  $("#byVehcleTabTitle").click(function(){
    $('#vehicleContentDiv').removeClass('hideDiv');
    $('#itemContentDiv').addClass('hideDiv');
    $('#job-preview-div').removeClass('hidden');
    $('#job-preview-div-by-item').addClass('hidden');
  });

  $("#byItemTabTitle").click(function(){
    $('#vehicleContentDiv').addClass('hideDiv');
    $('#itemContentDiv').removeClass('hideDiv');
    $('#job-preview-div').addClass('hidden');
    $('#job-preview-div-by-item').removeClass('hidden');
  });


  $('.vehicleRegNo').select2({
        tags: true,
        createTag: function (params) {
            var term = $.trim(params.term);
            if (term === '') {
              return null;
            } else {
                var splitDetails = term.match(/([A-Za-z]+)([0-9]+)/);
                if (splitDetails != null) {
                    var newTerm = splitDetails[1]+"-"+splitDetails[2];
                } else {
                    var newTerm = term;
                }
            }

            return {
              id: newTerm.toUpperCase(),
              text: newTerm.toUpperCase(),
              newTag: true // add additional parameters
            }
        },
        tokenSeparators: [',', ' '],
        minimumInputLength: 1,
        minimumResultsForSearch: 10,
        ajax: {
            url: BASE_URL + '/service-job-api/searchVehiclesForDropdown',
            dataType: "json",
            type: "POST",
            data: function (params) {

                var queryParameters = {
                    searchKey: params.term,
                    customerID: $('.customerID').val()
                }
                return queryParameters;
            },
            processResults: function (data) {
                return {
                    results: $.map(data.data.list, function (item) {
                        return {
                            text: item.text,
                            id: item.value
                        }
                    })
                };
            }
        },
        placeholder: 'Ex: AAA-4444',
    });

    $('.vehicleRegNo').on('select2:select', function (e) { 
        var data = e.params.data;
        getVehicleDetails(data.text);
        vehicleClick = true;
    });


    $('.vehicleRegNo2').select2({
        tags: true,
        createTag: function (params) {
            var term = $.trim(params.term);
            if (term === '') {
              return null;
            } else {
                var splitDetails = term.match(/([A-Za-z]+)([0-9]+)/);
                if (splitDetails != null) {
                    var newTerm = splitDetails[1]+"-"+splitDetails[2];
                } else {
                    var newTerm = term;
                }
            }

            return {
              id: newTerm.toUpperCase(),
              text: newTerm.toUpperCase(),
              newTag: true // add additional parameters
            }
        },
        tokenSeparators: [',', ' '],
        minimumInputLength: 1,
        minimumResultsForSearch: 10,
        ajax: {
            url: BASE_URL + '/service-job-api/searchVehiclesForDropdown',
            dataType: "json",
            type: "POST",
            data: function (params) {

                var queryParameters = {
                    searchKey: params.term,
                    customerID: $('.customerID').val()
                }
                return queryParameters;
            },
            processResults: function (data) {
                return {
                    results: $.map(data.data.list, function (item) {
                        return {
                            text: item.text,
                            id: item.value
                        }
                    })
                };
            }
        },
        placeholder: 'Ex: AAA-4444',
    });

    $('.vehicleRegNo2').on('select2:select', function (e) { 
        var data = e.params.data;
        getVehicleDetails(data.text, 'byItem');
        // vehicleClick = true;
    });
    // $.ajax({
    //     type: 'POST',
    //     url: BASE_URL +'/service-job-api/getAllVehical',
    //     data:{},
    //     success: function(respond) {
    //         serviceVehicles = respond.data.list;
    //         autocomplete(document.getElementById("vehicleRegNo"), serviceVehicles, "byVehicle");
    //         autocomplete(document.getElementById("vehicleRegNo2"), serviceVehicles, "byItem");
    //     }
    // });

      var options = {
        fill: {
          type:'solid',
          opacity: [0.35, 1],
        },

        chart: {
          height: 130,
          type: 'area',
          zoom: {
            enabled: false
          },
          toolbar: {
            show: false
          }
        },
        dataLabels: {
          enabled: false
        },
        stroke: {
          curve: 'straight'
        },
        series: [{
          name: "STOCK ABC",
          data: [0, 5000, 10100, 14900, 21000]
        }],
        labels: ["13 Jan 2017","14 April 2017","15 Jul 2017","13 Nov 2017","14 Feb 2018"],
        xaxis: {
          type: 'datetime',
        },
        yaxis: {
          title: {
            text: 'km'
          }
        },
        legend: {
            horizontalAlign: 'left'
        }
      }

      var chart = new ApexCharts(
          document.querySelector("#runningChart"),
          options
      );

    chart.render();

    // function autocomplete(inp, arr, detailType) {
    //     var currentFocus;
    //     inp.addEventListener("focusin", function(e) {
    //         e.preventDefault();
    //         var a, b, i, val = this.value;
    //         closeAllLists();
    //         currentFocus = -1;
    //         a = document.createElement("DIV");
    //         a.setAttribute("id", this.id + "autocomplete-list");
    //         a.setAttribute("class", "autocomplete-items");
    //         this.parentNode.appendChild(a);
    //         if (arr != null) {
    //             for (i = 0; i < arr.length; i++) {
    //                 if (arr[i]['value'].substr(0, val.length).toUpperCase() == val.toUpperCase()) {
    //                     b = document.createElement("DIV");
    //                     b.innerHTML = "<strong>" + arr[i]['value'].substr(0, val.length) + "</strong>";
    //                     b.innerHTML += arr[i]['value'].substr(val.length);
    //                     b.innerHTML += "<input type='hidden' value='" + arr[i]['value'] + "'>";
    //                     b.addEventListener("click", function(e) {
    //                         inp.value = this.getElementsByTagName("input")[0].value;
    //                         if (detailType == "byVehicle") {
    //                             getVehicleDetails(this.getElementsByTagName("input")[0].value);
    //                         } else {
    //                             getVehicleDetails(this.getElementsByTagName("input")[0].value, 'byItem');
    //                         }
    //                         closeAllLists();
    //                     });
    //                     // a.appendChild(b);
    //                 }
    //             }
    //         } 
    //     });
    //     inp.addEventListener("input", function(e) {
    //         var a, b, i, val = this.value;
    //          closeAllLists();
    //         if (!val) { return false;}
    //         currentFocus = -1;
    //         a = document.createElement("DIV");
    //         a.setAttribute("id", this.id + "autocomplete-list");
    //         a.setAttribute("class", "autocomplete-items");
    //         this.parentNode.appendChild(a);
    //         if ((arr != null) && (arr.length !== 0)) {
    //             for (i = 0; i < arr.length; i++) {
    //                 if (arr[i]['value'] != null) {
    //                     if (arr[i]['value'].substr(0, val.length).toUpperCase() == val.toUpperCase()) {
    //                         b = document.createElement("DIV");
    //                         b.innerHTML = "<strong>" + arr[i]['value'].substr(0, val.length) + "</strong>";
    //                         b.innerHTML += arr[i]['value'].substr(val.length);
    //                         b.innerHTML += "<input type='hidden' data-key='"+ arr[i]['key'] +"' value='" + arr[i]['value'] + "'>";
    //                         b.addEventListener("click", function(e) {
    //                             inp.value = this.getElementsByTagName("input")[0].value;
    //                             if (detailType == "byVehicle") {
    //                                 getVehicleDetails(this.getElementsByTagName("input")[0].value);
    //                             } else {
    //                                 getVehicleDetails(this.getElementsByTagName("input")[0].value, 'byItem');
    //                             }
    //                             closeAllLists();
    //                         });
    //                         a.appendChild(b);
    //                     }
    //                 }
    //             }
    //         } 
    //     });
    //     inp.addEventListener("keydown", function(e) {
    //         var x = document.getElementById(this.id + "autocomplete-list");
    //         if (x) x = x.getElementsByTagName("div");
    //         if (e.keyCode == 40) {
    //             currentFocus++;
    //             addActive(x);
    //         } else if (e.keyCode == 38) { //up
    //             currentFocus--;
    //             addActive(x);
    //         } else if (e.keyCode == 13) {
    //             e.preventDefault();
    //             if (currentFocus > -1) {
    //                 if (x) x[currentFocus].click();
    //             }
    //           }
    //     });
          
    //     function addActive(x) {
    //         if (!x) return false;
    //         removeActive(x);
    //         if (currentFocus >= x.length) currentFocus = 0;
    //         if (currentFocus < 0) currentFocus = (x.length - 1);
    //         x[currentFocus].classList.add("autocomplete-active");

    //         var target = document.querySelector('.autocomplete-active');
    //         target.parentNode.scrollTop = target.offsetTop;
    //     }
          
    //     function removeActive(x) {
    //         for (var i = 0; i < x.length; i++) {
    //           x[i].classList.remove("autocomplete-active");
    //         }
    //     }
          
    //     function closeAllLists(elmnt) {
    //         var x = document.getElementsByClassName("autocomplete-items");
    //         for (var i = 0; i < x.length; i++) {
    //             if (elmnt != x[i] && elmnt != inp) {
    //                 x[i].parentNode.removeChild(x[i]);
    //             }
    //         }
    //     }

    //     document.addEventListener("click", function (e) {
    //         closeAllLists(e.target);
    //     });
    // }    


    function getVehicleDetails(vehicleRegNo, type = null) {
        $.ajax({
            type: 'POST', url: BASE_URL + '/service-job-api/getJobsByVehicleRegNo',
            data: {vehicleRegNo: vehicleRegNo},
            success: function(respond) {
                if (respond.status == true) {
                    if (type == "byItem") {
                        eb.ajax({
                            type: 'POST', url: BASE_URL + '/service-job-api/getJobSideViewByJobId',
                            data: {jobId: respond.data[0].jobId},
                            success: function(res) {
                                $('.vehicleRegistrationNum').text(res.data.serviceVehicleRegNo);
                                $('.taskCardName').text(res.data.taskCardName);
                                $('.customerName').text(res.data.customerName);
                                $('.createdTimeStamp').text(res.data.createdTimeStamp);
                                $('.vehicleModelName').text(((res.data.vehicleModelName != null) ? res.data.vehicleModelName : "") +" "+((res.data.vehicleSubModelName != null) ? res.data.vehicleSubModelName : ""));
                                $('.jobpreviewByItem').removeClass('hidden');
                            }
                        });
                    } else {
                        $('#jobList').html(respond.html);
                        jobID = respond.data[0].jobId;
                        setDataForJobPreview(respond.data[0].jobId);
                    }
                }
            }
        });
    }

    function setDataForJobPreview(jobId)
    {
        eb.ajax({
            type: 'POST', url: BASE_URL + '/service-job-api/getJobSideViewByJobId',
            data: {jobId: jobId},
            success: function(respond) {
                if (respond.status == true) {
                    $('#job-preview-div').html(respond.html);
                }
            }
        });   
    }

    $('.remarks-button-div').on('click', function(event) {
        event.preventDefault();
        $('#vehicleContentDiv').addClass('hidden');
        $('#remarkButton').addClass('hidden');
        $('#vehicle-remarks').removeClass('hidden');
        $('#backButton').removeClass('hidden');
    });

    $('.service-button-div').on('click', function(event) {
        event.preventDefault();
        $('#vehicleContentDiv').addClass('hidden');
        $('#remarkButton').addClass('hidden');
        // $('#vehicle-remarks').removeClass('hidden');
        $('#job-service-div').removeClass('hidden');
        $('#backButton').removeClass('hidden');
    });

    $('.back-button-div').on('click', function(event) {
        event.preventDefault();
        $('#vehicleContentDiv').removeClass('hidden');
        $('#remarkButton').removeClass('hidden');
        $('#vehicle-remarks').addClass('hidden');
        $('#job-service-div').addClass('hidden');
        $('#backButton').addClass('hidden');
    });

    $('.jobTile').on('click', function(event) {
        event.preventDefault();
         $('.list-group li').removeClass('selected');
        $(this).addClass('selected');
        setDataForJobPreview($(this).attr('data-job-id'));
    });

    $('.clickable').on('click', function(event) {
        event.preventDefault();
        eb.ajax({
            type: 'POST', url: BASE_URL + '/service-job-api/getJobItemChangeSideView',
            data: {item: $(this).attr('data-box-type'), vehicleRegNo: $('#vehicleRegNo2').val()},
            success: function(respond) {
                if (respond.status == true) {
                    $('#jobListByItem').html(respond.html);
                }
            }
        });   
    });

    $('#jobServiceTable').on('click','.employee-view', function() {
        jobTaskID = $(this).closest('tr').data('job-task-id');
        getJobTaskRelatedEmployees();

    }); 

    function getJobTaskRelatedEmployees() {
        eb.ajax({
            type: 'POST', url: BASE_URL + '/service-job-dashboard-api/getJobTaskRelatedEmployees', 
            data: {
                jobId: jobID,
                jobTaskID: jobTaskID
            }, 
            success: function(respond) {
                if (respond.status == true) {

                    $('#addEmployeeModal .addedEmployees').remove();
                    for (var i = 0; i < respond.data.length; i++) {
                        var newTrID = 'tr_' + respond.data[i]['employeeID'];
                        var clonedRow = $($('#jobEmpRow').clone()).attr('id', newTrID).addClass('addedEmployees');
                        $("#EmpName", clonedRow).text(respond.data[i]['employeeFirstName']);
                        $("#EmpCode", clonedRow).text(respond.data[i]['employeeCode']);
                        clonedRow.removeClass('hidden');
                        clonedRow.insertBefore('#jobEmpRow');
                    }

                    if (respond.data.length > 0) {
                        $('#addEmployeeModal').modal('show');
                    } else {
                        p_notification(false, eb.getMessage('ERR_NO_ANY_EMP_ASSIGN_SRVCE'));
                        return false;
                    }

                } 

            }
        });
    }
});