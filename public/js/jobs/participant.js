var uploadedAttachments = {};
var deletedAttachmentIds = [];
var deletedAttachmentIdArray = [];

$(document).ready(function() {
    $('#viewUploadedFiles').on('click', function(e) {
        $('#doc-attach-table tbody tr').remove();
        $('#doc-attach-table tfoot div').remove();
        $('#doc-attach-table tbody div').remove();

        if (!$.isEmptyObject(uploadedAttachments)) {
            $('#doc-attach-table thead tr').removeClass('hidden');
            $.each(uploadedAttachments, function(index, value) {
                if (!deletedAttachmentIds.includes(value['documentAttachemntMapID'])) {
                    tableBody = "<tr id="+value['documentAttachemntMapID']+" data-attachId=" + value['documentAttachemntMapID'] + "><td><a href="+value.docLink+">"+value.documentRealName+"</a></td><td class='text-center'><span class='glyphicon glyphicon-trash delete_attachment' style='cursor:pointer'></span></td></tr>";
                    $('#doc-attach-table tbody').append(tableBody);
                }
            });
        } else {
            $('#doc-attach-table thead tr').addClass('hidden');
            var noDataFooter = "<div class= ' col-lg-6 text-right'><label><b>No Data</b></label>";
            $('#doc-attach-table tbody').append(noDataFooter);
        }
        $('#attach_view_footer').removeClass('hidden');
        $('#viewAttachmentModal').modal('show');
    });

    $('#doc-attach-table').on('click', '.delete_attachment', function() {
        var attachementID = $(this).parents('tr').attr('data-attachid');
        bootbox.confirm('Are you sure you want to remove this attachemnet?', function(result) {
            if (result == true) {
                $('#' + attachementID).remove();
                deletedAttachmentIdArray.push(attachementID);
            }
        });
    });

    $('#viewAttachmentModal').on('click', '#saveAttachmentEdit', function(event) {
        event.preventDefault();

        $.each(deletedAttachmentIdArray, function(index, val) {
            deletedAttachmentIds.push(val);
        });

        $('#viewAttachmentModal').modal('hide');
    });

    var nowTemp = new Date();
    var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);
    var nowTemp = new Date();
    var now1 = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);
    var myDate = new Date();
    var prettyDate = myDate.getFullYear() + '-' + (myDate.getMonth() + 1) + '-' + (myDate.getDate() < 10 ? '0' : '') + myDate.getDate();
    var day = myDate.getDate() < 10 ? '0' + myDate.getDate() : myDate.getDate();
    var month = (myDate.getMonth() + 1) < 10 ? '0' + (myDate.getMonth() + 1) : (myDate.getMonth() + 1);
    $("#employeeDOB").val(eb.getDateForDocumentEdit('#employeeDOB', day, month, myDate.getFullYear()));
    var checkin = $('#employeeDOB').datepicker({onRender: function(date) {
            return date.valueOf() < now.valueOf() ? ((U_DATE_R_OVERRIDE) ? '' : 'disabled') : '';
        },
        //  format: 'yyyy-mm-dd',
    }).on('changeDate', function(ev) {
        checkin.hide();
    }).data('datepicker');

    var employeeId   = null;
    var employeeFirstName = null;
    var employeeSecondName = null;
    var employeeAddress = null;
    var employeeTP = null;
    var employeeEmail = null;
    var employeeCode = null;
    var employeeStatus = null;
    var employeeParticipantType = null;
    var employeeIDNo = null;
    var employeeDOB = null;
    var employeeGender = null;
    var employeeParticipantSubType = null;
    var entityID = null;
    

    
    $('#btnAdd').on('click',function(){
        employeeCode = $('#employeeCode').val();
        employeeFirstName = $('#employeeFirstName').val();
        employeeSecondName = $('#employeeLastName').val();
        employeeTP = $('#employeeMobileNumber').val();
        employeeAddress = $('#employeeAddress').val();
        employeeIDNo = $('#employeeIdentityReference').val();
        employeeEmail = $('#employeeEmail').val();
        employeeParticipantType = $('#employeeDepartment').val();
        employeeParticipantSubType = $('#employeeDesignation').val();
        employeeDOB = $('#employeeDOB').val();
        employeeGender = $('input[name=gender]:checked').val();


        var formData = {
            employeeCode: employeeCode,
            employeeFirstName: employeeFirstName,
            employeeSecondName: employeeSecondName, 
            employeeAddress: employeeAddress, 
            employeeTP: employeeTP, 
            employeeEmail: employeeEmail,
            employeeIDNo: employeeIDNo,
            employeeParticipantType: employeeParticipantType,
            employeeParticipantSubType: employeeParticipantSubType,
            employeeDOB: employeeDOB,
            employeeGender: employeeGender
        }

        if (validateFormData(formData)) { 
            
            if (employeeTP.trim()) {
                var phoneno = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;  
                if (!(employeeTP.match(phoneno)))  { 
                    p_notification(false, eb.getMessage('ERR_TP_NUM'));
                    return;
                }
            }

            if (employeeEmail.trim()) {
                var emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

                if (!(emailRegex.test(employeeEmail))) {
                    p_notification(false, eb.getMessage('ERR_EMAIL_NOT_VALID'));
                    return;
                }
            }

            eb.ajax({
                type: 'POST',
                url: BASE_URL + '/participant-api/createParticipant',
                data: formData,
                success: function(respond) {
                    p_notification(respond.status, respond.msg);                
                    if(respond.status){
                        var fileInput = document.getElementById('documentFiles');
                        if(fileInput.files.length > 0) {
                            var form_data = false;
                            if (window.FormData) {
                                form_data = new FormData();
                            }
                            form_data.append("documentID", respond.data);
                            form_data.append("documentTypeID", 35);
                            
                            for (var i = 0; i < fileInput.files.length; i++) {
                                form_data.append("files[]", fileInput.files[i]);
                            }

                            eb.ajax({
                                url: BASE_URL + '/store-files',
                                type: 'POST',
                                processData: false,
                                contentType: false,
                                data: form_data,
                                success: function(res) {
                                }
                            });
                        }

                        $('#employeeCode').val('');
                        $('#employeeFirstName').val('');
                        $('#employeeLastName').val('');
                        $('#employeeAddress').val('');
                        $('#employeeMobileNumber').val('');
                        $('#employeeEmail').val('');
                        $('#employeeIdentityReference').val('');
                        $('#employeeDepartment').val('');
                        $('#employeeDesignation').val('');
                        window.setTimeout(function() {
                                location.reload();
                        }, 1000);                    
                    }                
                }
            });
        }
    });

    //for cancel btn
    $('#btnCancel').on('click',function (){
        $('input[type=text]').val('');
        $('input[type=email]').val('');  
        $('select[name=employeeDept]').val('');
        $('select[name=employeeDesig]').val('');
        $('.selectpicker').selectpicker('refresh');      
        employeeRegisterView();        
    });

    $('#employee-list').on('click','.employee-action',function (e){    
        var action = $(this).data('action');
        employeeId   = $(this).closest('tr').data('employee-id');
        employeeFirstName = $(this).closest('tr').data('employee-first-name');
        employeeSecondName = $(this).closest('tr').data('employee-last-name');
        employeeAddress = $(this).closest('tr').data('employee-address');
        employeeTP = $(this).closest('tr').data('employee-employee-tp');
        employeeEmail = $(this).closest('tr').data('employee-email');
        employeeCode = $(this).closest('tr').data('employee-code');
        entityID = $(this).closest('tr').data('entity-id');
        employeeDepartmentID = $(this).closest('tr').data('employee-department-id');
        employeeIDNo = $(this).closest('tr').data('employee-id-no');
        employeeDOB = $(this).closest('tr').data('employee-dob');
        employeeGender = $(this).closest('tr').data('employee-gender');
        status = $(this).closest('tr').data('employee-status');

        switch(action) {
            case 'edit':
                e.preventDefault();
                employeeUpdateView();

                //set selected department details
                $('#employeeCode').val(employeeCode);
                $('#employeeFirstName').val(employeeFirstName);
                $('#employeeLastName').val(employeeSecondName);
                $('#employeeAddress').val(employeeAddress);
                $('#employeeMobileNumber').val(employeeTP);
                $('#employeeEmail').val(employeeEmail);
                $('#employeeIdentityReference').val(employeeIDNo);
                $('#employeeDOB').val(employeeDOB);
                $('.createPoDiv').addClass('hidden');
                $('.updatePoDiv').removeClass('hidden');
                $('.updatePoDiv_view').removeClass('hidden');

                if (employeeGender == 1) {
                    $('input[name="gender"][value="male"]').prop("checked", true);
                } else {
                    $('input[name="gender"][value="female"]').prop("checked", true);
                }
                getEmployeeDesignations(employeeId);
                getPartcipantTypes(employeeId);
                getUploadedAttachemnts(employeeId);
                $('select[name=employeeDept]').val(employeeDepartmentID);
                $('.selectpicker').selectpicker('refresh');
                break;

            case 'status':
                var msg;
                var status;
                departmentId   = $(this).closest('tr').data('department-id');
                var currentDiv = $(this).contents();
                var flag = true;
                if ($(this).children().hasClass('fa-check-square-o')) {
                    msg = 'Are you sure you want to Deactivate this Participant';
                    status = '0';
                }
                else if ($(this).children().hasClass('fa-square-o')) {
                    msg = 'Are you sure you want to Activate this Participant';
                    status = '1';
                }
                activeInactive(employeeId, status, currentDiv, msg, flag);
                break;
            case 'view':
                getEmployeeDesignations(employeeId, 'loadModal');
                break;
            case 'participant-type':
                getPartcipantTypes(employeeId, 'loadModal');
                break;
            case 'delete':
                deleteEmployee(employeeId);
                break;
            default:
                console.error('Invalid action.');
                break;
        }
    });

    //this function for switch to employee register view
    function employeeRegisterView(){
       $('.updateTitle').addClass('hidden');
       $('.addTitle').removeClass('hidden');
       $('.updateDiv').addClass('hidden');
       $('.addDiv').removeClass('hidden');
    }

    //this function for switch to employee update view
    function employeeUpdateView(){
       $('.addTitle').addClass('hidden');
       $('.updateTitle').removeClass('hidden');
       $('.addDiv').addClass('hidden');
       $('.updateDiv').removeClass('hidden');
    }

    /**
     *
     *use to change active state
     */
    function activeInactive(type, status, currentDiv, msg, flag) {
        bootbox.confirm(msg, function($result) {
            if ($result == true) {
                eb.ajax({
                    url: BASE_URL + '/participant-api/changeStatusID',
                    method: 'post',
                    dataType: 'json',
                    data: {
                        'employeeID': type,
                        'status': status,
                    },
                    success: function(data) {
                        if (data.status) {
                            p_notification(data.status, data.msg);
                            if (currentDiv.hasClass('fa-check-square-o') && flag) {
                                currentDiv.removeClass('fa-check-square-o');
                                currentDiv.addClass('fa-square-o');
                            }
                            else {
                                currentDiv.removeClass('fa-square-o');
                                currentDiv.addClass('fa-check-square-o');
                            }
                        } else {
                            p_notification(data.status, data.msg);
                        }
                    }
                });
            }
        });
    }

    $('#btnUpdate').on('click',function(){
        employeeCode = $('#employeeCode').val();
        employeeFirstName = $('#employeeFirstName').val();
        employeeSecondName = $('#employeeLastName').val();
        employeeTP = $('#employeeMobileNumber').val();
        employeeAddress = $('#employeeAddress').val();
        employeeIDNo = $('#employeeIdentityReference').val();
        employeeEmail = $('#employeeEmail').val();
        employeeParticipantType = $('#employeeDepartment').val();
        employeeParticipantSubType = $('#employeeDesignation').val();
        employeeDOB = $('#employeeDOB').val();
        employeeGender = $('input[name=gender]:checked').val();


        var formData = {
            employeeCode: employeeCode,
            employeeFirstName: employeeFirstName,
            employeeSecondName: employeeSecondName, 
            employeeAddress: employeeAddress, 
            employeeTP: employeeTP, 
            employeeEmail: employeeEmail,
            employeeIDNo: employeeIDNo,
            employeeParticipantType: employeeParticipantType,
            employeeParticipantSubType: employeeParticipantSubType,
            employeeDOB: employeeDOB,
            employeeGender: employeeGender,
            employeeID: employeeId,
            entityID: entityID
        }

        var existingAttachemnts = {};
        var deletedAttachments = {};
        $.each(uploadedAttachments, function(index, val) {
            if (!deletedAttachmentIds.includes(val['documentAttachemntMapID'])) {
                existingAttachemnts[index] = val;
            } else {
                deletedAttachments[index] = val;
            }
        });

        
        if(validateFormData(formData)){
            
            if (employeeTP.trim()) {
                var phoneno = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;  
                if (!(employeeTP.match(phoneno)))  { 
                    p_notification(false, eb.getMessage('ERR_TP_NUM'));
                    return;
                }
            }

            if (employeeEmail.trim()) {
                var emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

                if (!(emailRegex.test(employeeEmail))) {
                    p_notification(false, eb.getMessage('ERR_EMAIL_NOT_VALID'));
                    return;
                }
            }

            eb.ajax({
                type: 'POST',
                url: BASE_URL + '/participant-api/update',
                data: formData,
                success: function(respond) {
                    p_notification(respond.status, respond.msg);                
                    if(respond.status){
                        var fileInput = document.getElementById('editDocumentFiles');
                        var form_data = false;
                        if (window.FormData) {
                            form_data = new FormData();
                        }
                        form_data.append("documentID", respond.data);
                        form_data.append("documentTypeID", 35);
                        form_data.append("updateFlag", true);
                        form_data.append("editedDocumentID", employeeId);
                        form_data.append("documentCode", formData.employeeCode);
                        form_data.append("deletedAttachmentIds", deletedAttachmentIds);
                        
                        if(fileInput.files.length > 0) {
                            for (var i = 0; i < fileInput.files.length; i++) {
                                form_data.append("files[]", fileInput.files[i]);
                            }
                        }

                        eb.ajax({
                            url: BASE_URL + '/store-files',
                            type: 'POST',
                            processData: false,
                            contentType: false,
                            data: form_data,
                            success: function(res) {
                            }
                        });

                        $('#departmentCode').val('');
                        $('#departmentName').val('');
                        window.setTimeout(function() {
                                location.reload();
                        }, 1000);                  
                    }                
                }
            });
        } 
    });

    //for search
    $('#searchEmp').on('click', function() {
        var key = $('#employeeSearch').val();
        var searchDepartmentID = ($('#employeeDepartmentSearch').val() != 0 && $('#employeeDepartmentSearch').val() != null) ? $('#employeeDepartmentSearch').val() : null;
        var searchDesignationID = ($('#employeeDesignationSearch').val() != 0 && $('#employeeDesignationSearch').val() != null) ? $('#employeeDesignationSearch').val() : null;
        getSearchResults(key, searchDepartmentID, searchDesignationID);
        
    });

    $( "#employeeDepartmentSearch, #employeeDesignationSearch").change(function() {
        var key = $('#employeeSearch').val();
        var searchDepartmentID = ($('#employeeDepartmentSearch').val() != 0 && $('#employeeDepartmentSearch').val() != null) ? $('#employeeDepartmentSearch').val() : null;
        var searchDesignationID = ($('#employeeDesignationSearch').val() != 0 && $('#employeeDesignationSearch').val() != null) ? $('#employeeDesignationSearch').val() : null;
        getSearchResults(key, searchDepartmentID, searchDesignationID);
    });

    //for rest btn
    $('#btnReset').on('click',function (){
        $('input[type=text]').val('');
        $('input[type=email]').val('');  
        $('select[name=employeeDept]').val('');
        $('select[name=employeeDesig]').val('');
        $('.selectpicker').selectpicker('refresh');
    });  

    //for search cancel btn
    $('#searchCancel').on('click',function (){
        $('#employeeSearch').val('');
        $('select[name=employeeDeptSearch]').val('');
        $('select[name=employeeDesigSearch]').val('');
        $('.selectpicker').selectpicker('refresh');  
        getSearchResults();
    });

    function getEmployeeDesignations(id, state = null) {
        var designationIds = [];            
        $.ajax({
            type: 'GET',
            data: {
                employeeID: id
            },
            url: BASE_URL + '/participant-api/getEmployeeDesignation',
            success: function(respond) {
                if (state == null) {
                    for (var i = 0; i < respond.data.length; i++) {
                       designationIds.push(parseInt(respond.data[i]['designationID']));
                    }
                    $('select[name=employeeDesig]').selectpicker('val', designationIds);
                }
                else {
                    $('.designationViewModalBody').html(respond.html);
                    $('#designationViewModal').modal('show');
                }
                
            }
        });
    }

   function getPartcipantTypes(id, state = null) {
        var participantTypeIds = [];            
        $.ajax({
            type: 'GET',
            data: {
                employeeID: id
            },
            url: BASE_URL + '/participant-api/getPartcipantTypes',
            success: function(respond) {
                if (state == null) {
                    for (var i = 0; i < respond.data.length; i++) {
                       participantTypeIds.push(parseInt(respond.data[i]['departmentID']));
                    }
                    $('select[name=employeeDept]').selectpicker('val', participantTypeIds);
                }
                else {
                    $('.participantTypeModalBody').html(respond.html);
                    $('#participantTypeModal').modal('show');
                }
                
            }
        });
    }

    function getUploadedAttachemnts(id) {
        var participantTypeIds = [];            
        $.ajax({
            type: 'GET',
            data: {
                employeeID: id
            },
            url: BASE_URL + '/participant-api/getUploadedAttachemnts',
            success: function(respond) {
                if (!$.isEmptyObject(respond.data)) {
                    uploadedAttachments = respond.data;
                }

            }
        });
    }

    function getSearchResults(key = null, searchDepartmentID = null, searchDesignationID = null ) {
        $.ajax({
            type: 'POST',
            url: BASE_URL + '/participant-api/search',
            data: {
                searchKey : key,
                searchDepartmentID : searchDepartmentID,
                searchDesignationID : searchDesignationID

            },
            success: function(data) {
                if (data.status == true) {
                    if (data.status) {
                        $("#employee-list").html(data.html);
                    } else {
                        p_notification(data.status, data.msg);
                    }
                }            
            }
        });
    }

    function deleteEmployee(employeeId) {
        bootbox.confirm('Are you sure you want to delete this Participant?', function($result) {
            if ($result == true) {
                eb.ajax({
                    url: BASE_URL + '/participant-api/deleteEmployee',
                    method: 'post',
                    data: {
                        employeeID: employeeId,
                        entityID: entityID
                    },
                    dataType: 'json',
                    success: function(data) {
                        if (data.status == true) {
                            p_notification(data.status, data.msg);
                            window.location.reload();
                        } else {
                            p_notification(data.status, data.msg);
                        }
                    }
                });
            }
        });
    }

    function validateFormData(formData)
    {
        if (formData.employeeCode == '') {
            p_notification(false, eb.getMessage('ERR_PART_CODE_CNT_BE_NULL'));
            return false;
        }
        else if (formData.employeeIDNo == '') {
            p_notification(false, eb.getMessage('ERR_PART_IDNO_CNT_BE_NULL'));
            return false;
        } 
        else if (formData.employeeParticipantType == null) {
            p_notification(false, eb.getMessage('ERR_PART_TYPE_CNT_BE_NULL'));
            return false;
        }
        else if(formData.employeeParticipantSubType == null) {
            p_notification(false, eb.getMessage('ERR_PART_SUB_TYPE_CNT_BE_NULL'));
            return false;
        }
        return true;
    }

    $('#employee-list').on('click', '.doc_attachments', function() {
        setDataToAttachmentViewModal($(this).attr('data-par-related-id'));
        $('#viewAttachmentModal').modal('show');
    });

    function setDataToAttachmentViewModal(documentID) {
        $('#doc-attach-table tbody tr').remove();
        $('#doc-attach-table tfoot div').remove();
        $('#doc-attach-table tbody div').remove();

        eb.ajax({
            type: 'POST',
            url: BASE_URL + '/get-document-related-attachement',
            data: {
                documentID: documentID,
                documentTypeID: 35
            },
            success: function(respond) {
                if (respond.status == true) {
                    $('#doc-attach-table thead tr').removeClass('hidden');
                    $.each(respond.data, function(index, value) {
                        tableBody = "<tr><td><a href="+value.link+">"+value.docName+"</a></td></tr>";
                        $('#doc-attach-table tbody').append(tableBody);
                    });
                } else {
                    $('#doc-attach-table thead tr').addClass('hidden');
                    var noDataFooter = "<div class= ' col-lg-6 text-right'><label><b>No Data</b></label>";
                    $('#doc-attach-table tbody').append(noDataFooter);
                }
            }
        });
    }
});
