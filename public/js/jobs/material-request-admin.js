$(document).ready(function() {

    var fromDate = $('#fromDate').datepicker({}).on('changeDate', function(ev) {
        if (ev.date.valueOf() < toDate.date.valueOf()) {
            var newDate = new Date(ev.date)
            newDate.setDate(newDate.getDate() + 1);
            toDate.setValue(newDate);
        }
        fromDate.hide();
        $('#toDate')[0].focus();
    }).data('datepicker');

    var toDate = $('#toDate').datepicker({
        onRender: function(date) {
            return date.valueOf() <= fromDate.date.valueOf() ? 'disabled' : '';
        }
    }).on('changeDate', function(ev) {
        toDate.hide();
    }).data('datepicker');

    $('#requests-list').on('click', '.approve-request', function() {
        var reqId = $(this).attr('data-reqid');
        var viewBtn = $('#requests-list table tbody tr#' + reqId + " td button.view-request");
        approveRequest(reqId, $(this), viewBtn);
    });

    $('.material-modal .modal-footer .approve-request').on('click', function() {
        var reqId = $(this).attr('data-reqid');
        var listBtn = $('#requests-list table tbody tr#' + reqId + " td button.approve-request");
        var viewBtn = $('#requests-list table tbody tr#' + reqId + " td button.view-request");
        approveRequest(reqId, $(this), viewBtn, listBtn);
    });

    $('#requests-list').on('click', '.view-request', function() {
        var reqId = $(this).attr('data-reqid');
        var status = $(this).attr('data-status');
        viewRequestProducts(reqId, status);

    });

    $('#search-form').on('submit', function(e) {
        e.preventDefault();

        var searchKey = $("#searchKey").val();
        var fromDate = $('input[name="fromDate"]').val();
        var toDate = $('input[name="toDate"]').val();
        var fixDate = fromDate ? (toDate ? null : toDate) : toDate;

        if (!searchKey && !fromDate && !toDate) {
            p_notification(false, eb.getMessage('ERR_SRCH_FIELDS'));
            return
        }
        searchRequest(searchKey, fromDate, toDate, fixDate);
    });

    function searchRequest(searchKey, fromDate, toDate, fixDate) {
        eb.ajax({
            type: 'POST',
            url: BASE_URL + '/material-requisition/search',
            data: {
                searchKey: searchKey,
                fromDate: fromDate,
                toDate: toDate,
                fixDate: fixDate
            },
            success: function (respond) {
                $('#requests-list').html(respond.html);
            }
        });
    }

    $('#cancelSearch').on('click', function(event) {
        event.preventDefault();

        var searchKey = $("#searchKey").val();
        var fromDate = $('input[name="fromDate"]').val();
        var toDate = $('input[name="toDate"]').val();
        var fixDate = fromDate ? (toDate ? null : toDate) : toDate;

        if (searchKey != "") {
            searchRequest('', fromDate, toDate, fixDate);
        }
        $("#searchKey").val('');
    });
});


    loadPreview = function(url) {
        var preview = eb.post(url);
        var path = "/material-requisition-api/sendMaterialRequisitionEmail";
        
        preview.done(function(view_phtml) {
            var iframe = 'documentpreview';
            var $preview = $("#preview");

            $(".modal-dialog", $preview).html(view_phtml);
            $preview.modal('show');
            setTimeout(function() {
                $preview.scrollTop(0);
            }, 200);

            $("#templateID").on("change", function() {
            var $iframe = $('#' + iframe, $preview);
            $iframe.attr('src', $iframe.data('origsrc') + '/' + $(this).val());
        });

        $("#document_print", $preview).on("click", function() {
            printIframe(iframe);
        });

        $("#document_email", $preview).on("click", function() {
            $("#email-modal").modal("show");
        });

       $("#send", $preview).off('click').on("click", function() {
            // prevent doubleclick
            if ($(this).hasClass('disabled')) {
                return false;
            }
            var state = validateEmail($("#email_to", $preview).val(), $("#email-body", $preview).html());
            if (state) {
                var param = {
                    documentID: $(this).data("docid"), //$(this).data("so_id"),
                    to_email: $("#email_to", $preview).val(),
                    subject: $("#email_sub", $preview).val(),
                    body: $(".email-body", $preview).html(),
                    templateID: $('#templateID').val()
                };

                var mail = eb.post(path, param); ///<-
                mail.done(function(rep) {
                    p_notification(rep.status, rep.msg);

                    $("#email-modal button.btn", $preview).removeClass('disabled')
                    $("#email-modal", $preview).modal("hide");
                });
                $("#email-modal button.btn", $preview).addClass('disabled')
            }
        });

            $("button.email-modal-close", $preview).on("click", function() {
                $("#email-modal", $preview).modal("hide");
            });

            $("#cancel", $preview).on("click", function() {
                $preview.modal('hide');
            });

            if ($("#grnSave").length) {
                $preview.on('hidden.bs.modal', function() {

                    // since this event is being called when any modal is closed, even
                    // when the email modal is closed, this even gets triggered
                    // so check whether the main modal is visible
                    if (!$("#grnPreview:visible").length) {
                        window.location.reload();
                    }
                });
            }
        });
    }


    $(document).on("click", ".preview", function(e) {
        e.preventDefault();
        loadPreview(BASE_URL + $(this).attr("data-href"));
    });


var approveRequest = function(reqId, $approveBtn, $viewBtn, $listBtn = null) {
    eb.ajax({
        type: 'POST',
        url: BASE_URL + '/material-requisition-api/update-request',
        data: {
            'requestId': reqId
        },
        success: function(respond) {
            p_notification(respond.status, respond.msg);
            if(respond.status){
                $approveBtn.removeClass('btn-success approve-request');
                $approveBtn.addClass('btn-default disabled');
                $viewBtn.attr('data-status', 16);

                if ($listBtn) {
                    $listBtn.removeClass('btn-success approve-request');
                    $listBtn.addClass('btn-default disabled');
                }
            }
        }
    });

}

var viewRequestProducts = function (reqId, status) {
    eb.ajax({
        type: 'POST',
        url: BASE_URL + '/material-requisition-api/get-requested-products',
        data: {
            'requestId': reqId
        },
        success: function(respond) {
            loadMaterialDetailsModal(respond['data'], reqId, status);
        }
    });
}

var loadMaterialDetailsModal = function (data, reqId, status) {

    var modal = $('.material-modal');
    var tbody = $('<tbody></tbody>')
    var approveBtn = modal.find('.approve-request');
    var columns = [
        'productCode',
        'productName',
        'requestedQuantity',
        'locationProductQuantity'
    ];


    for (var i = 0; i < data.length; i++) {
        var row = $('<tr></tr>');

        for (var j = 0; j < columns.length; j++) {
            var rowData = getColData(columns[j], data[i][columns[j]]);
            row.append(rowData);
        }

        tbody.append(row);
    }

    if (status == 8) {  // if pending
        if (!approveBtn.hasClass('btn-success approve-request')) {
            approveBtn.addClass('btn-success approve-request');
        }
        approveBtn.attr('data-reqid', reqId);
        approveBtn.show();
    } else {
        approveBtn.hide();
    }

    modal.find('table tbody').replaceWith(tbody);
    modal.modal('show');
}


var getColData = function (colName, data) {
    var cssClass = {
        "jobProductRequestedQty": 'approved-qty',
        "locationProductQuantity": 'available-qty',
        "jobProductIssuedQty": 'issued-qty'
    };

    if (cssClass[colName]) {
        return $('<td class="' + cssClass[colName] +'" ></td>').text(data)
    }
    return $('<td></td>').text(data)
}
