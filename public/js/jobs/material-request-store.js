var productUomData = {};
$(document).ready(function() {

    var fromDate = $('#fromDate').datepicker({}).on('changeDate', function(ev) {
        if (ev.date.valueOf() < toDate.date.valueOf()) {
            var newDate = new Date(ev.date)
            newDate.setDate(newDate.getDate() + 1);
            toDate.setValue(newDate);
        }
        fromDate.hide();
        $('#toDate')[0].focus();
    }).data('datepicker');

    var toDate = $('#toDate').datepicker({
        onRender: function(date) {
            return date.valueOf() <= fromDate.date.valueOf() ? 'disabled' : '';
        }
    }).on('changeDate', function(ev) {
        toDate.hide();
    }).data('datepicker');


    $('#requests-list').on('click', '.view-request', function() {
        var jobId = $(this).attr('data-jobid');
        var reqId = $(this).attr('data-reqid');
        var status = $(this).attr('data-status');
        viewRequestProducts(jobId, reqId, status);
    });

    $('table.material-table').on('focusout', 'input.uomqty', function() {
        if($(this).siblings('.issue-qty').val() != '') {
            errors =  !isValid($(this).siblings('.issue-qty'));
        }
    });

    $('.material-modal .modal-footer .issue-items').on('click', function() {
        var $productElements = $('.material-table input.issue-qty');
        var proList = [];

        $productElements.each(function () {
            if ($(this).val() && $(this).val() > 0)
                proList.push({
                    jobProductId: $(this).attr('name'),
                    issueQty: $(this).val()
                });
        });

        if (!errors)
            issueMaterials(proList, $(this).attr('data-jobid'), $(this).attr('data-reqid'));
    });


    $('#search-form').on('submit', function(e) {
        e.preventDefault();

        var searchKey = $("#searchKey").val();
        var fromDate = $('input[name="fromDate"]').val();
        var toDate = $('input[name="toDate"]').val();
        var fixDate = fromDate ? (toDate ? null : toDate) : toDate;

        if (!searchKey && !fromDate && !toDate) {
            p_notification(false, eb.getMessage('ERR_SRCH_FIELDS'));
            return
        }
        searchRequest(searchKey, fromDate, toDate, fixDate);
    });



    function searchRequest(searchKey, fromDate, toDate, fixDate) {
        eb.ajax({
            type: 'POST',
            url: BASE_URL + '/material-requisition/search',
            data: {
                searchKey: searchKey,
                fromDate: fromDate,
                toDate: toDate,
                fixDate: fixDate
            },
            success: function (respond) {
                $('#requests-list').html(respond.html);
            }
        });
    }


    $('#cancelSearch').on('click', function(event) {
        event.preventDefault();

        var searchKey = $("#searchKey").val();
        var fromDate = $('input[name="fromDate"]').val();
        var toDate = $('input[name="toDate"]').val();
        var fixDate = fromDate ? (toDate ? null : toDate) : toDate;

        if (searchKey != "") {
            searchRequest('', fromDate, toDate, fixDate);
        }
        $("#searchKey").val('');
    });
});

var errors = false;

var issueMaterials = function (products, jobId, reqId) {
    eb.ajax({
        type: 'POST',
        url: BASE_URL + '/material-requisition-api/issue-material',
        data: {
            jobId: jobId,
            materials: products,
            reqId: reqId
        },
        success: function (respond) {
            p_notification(respond['status'], respond['msg']);
            $('.material-modal').modal('hide');
        }
    });
}


var viewRequestProducts = function (jobId, reqId, status) {
    eb.ajax({
        type: 'POST',
        url: BASE_URL + '/material-requisition-api/get-requested-products',
        data: {
            'requestId': reqId
        },
        success: function(respond) {
            loadMaterialDetailsModal(respond['data'], status, jobId, reqId);
        }
    });
}

var loadMaterialDetailsModal = function (data, status, jobId, reqId) {
        
    var modal = $('.material-modal');
    var tbody = $('<tbody></tbody>');
    var issueBtn = modal.find('.issue-items');
    var columns = [
        'productCode',
        'productName',
        'requestedQuantity',
        'locationProductQuantity',
        'jobProductIssuedQty'
    ];

    for (var i = 0; i < data.length; i++) {
        var row = $('<tr data-requestedquantity="'+data[i].requestedQuantity+'" data-locationproductquantity="'+data[i].locationProductQuantity+'" data-jobproductissuedqty="'+data[i].jobProductIssuedQty+'"></tr>');

        for (var j = 0; j < columns.length; j++) {
            var rowData = getColData(columns[j], data[i][columns[j]], data[i].productID);
            row.append(rowData);
        }
        productUomData[data[i].productID] = data[i].locationProductDetails.uom;
        row.append(getQtyInput(data[i]));
        tbody.append(row);
    }

    if (status == 16) {  // if approved
        issueBtn.attr('data-jobid', jobId).attr('data-reqid', reqId).show();
    } else {
        issueBtn.hide();
    }

    modal.find('table tbody').replaceWith(tbody);

    $('.material-modal table tr input').each(function(id, ele) {
        $(ele).addUom(productUomData[$(ele).attr('id')]);
    });
    modal.modal('show');
}

var getColData = function (colName, data, productID) {
    var cssClass = {
        "requestedQuantity": 'approved-qty',
        "locationProductQuantity": 'available-qty',
        "jobProductIssuedQty": 'issued-qty'
    };

    if (cssClass[colName]) {
        return $('<td class="' + cssClass[colName] +'" ></td>').html(
            '<div class="input-group"><input class="input-group form-control" id="'+productID+'" disabled value="'+data+'"></div>'
        );
    }
    return $('<td></td>').text(data)
}

var getQtyInput = function (data) {
    return $('<td></td>').html(
            '<div class="input-group"><input class="form-control issue-qty" id="'+data['productID']+'" name="' +
            data['jobProductId'] + '"></div>'
        );
}

var isValid = function (inputEle) {
    var newIssueQty = parseFloat($(inputEle).val());
    var availableQty = parseFloat($(inputEle).closest('tr').attr('data-locationproductquantity'));
    var approvedQty = parseFloat($(inputEle).closest('tr').attr('data-requestedQuantity'));
    var issuedQty = parseFloat($(inputEle).closest('tr').attr('data-jobproductissuedqty'));

    if (isNaN(newIssueQty)) {
        $(inputEle).focus();
        p_notification(false, eb.getMessage('ERR_INVALID_INPUT'));
        return false;
    }

    if (newIssueQty > availableQty) {
        $(inputEle).focus();
        p_notification(false, eb.getMessage('ERR_ISSUE_QTY'));
        return false;
    }

    if ((newIssueQty + issuedQty) > approvedQty) {
        $(inputEle).focus();
        p_notification(false, eb.getMessage('ERR_APPROVE_QTY'));
        return false;
    }

    return true;
}
