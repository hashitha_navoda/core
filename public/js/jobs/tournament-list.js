$(document).ready(function() {

    $('#projectList').on('click','.project-manager-view-action',function (){
        var projectId = $(this).closest('tr').data('project-id');            
        $.ajax({
            type: 'GET',
            data: {
                projectId: projectId
            },
            url: BASE_URL + '/api/tournament/getProjectManagers',
            success: function(respond) {
                $('.employeeViewModalBody').html(respond.html);
                $('#employeeViewModal').modal('show');
                
            }
        });
    });

    //for search
    $('#searchProjectBtn').on('click', function() {
        var key = $('#projectSearch').val();
        var projectMgtEmpID = ($('#projectManagerSearch').val() != 0 && $('#projectManagerSearch').val() != null) ? $('#projectManagerSearch').val() : null;
        var projectTypeID = ($('#projectTypeSearch').val() != 0 && $('#projectTypeSearch').val() != null) ? $('#projectTypeSearch').val() : null;
        var projectStatusID = ($('#projectStatusSearch').val() != 0 && $('#projectStatusSearch').val() != null) ? $('#projectStatusSearch').val() : null;
        getSearchResults(key, projectMgtEmpID, projectTypeID, projectStatusID);
        
    });

    $('#resetSearch').on('click', function() {
        $('#projectSearch').val('');
        $('#projectManagerSearch').val(0).selectpicker('refresh').trigger('change');
        $('#projectTypeSearch').val(0).selectpicker('refresh').trigger('change');
        $('#projectStatusSearch').val(0).selectpicker('refresh').trigger('change');
        var key = null;
        var projectMgtEmpID = null;
        var projectTypeID = null;
        var projectStatusID = null;
        getSearchResults(key, projectMgtEmpID, projectTypeID, projectStatusID);
        
    });

    $( "#projectManagerSearch, #projectTypeSearch, #projectStatusSearch").change(function() {
        var key = $('#projectSearch').val();
        var projectMgtEmpID = ($('#projectManagerSearch').val() != 0 && $('#projectManagerSearch').val() != null) ? $('#projectManagerSearch').val() : null;
        var projectTypeID = ($('#projectTypeSearch').val() != 0 && $('#projectTypeSearch').val() != null) ? $('#projectTypeSearch').val() : null;
        var projectStatusID = ($('#projectStatusSearch').val() != 0 && $('#projectStatusSearch').val() != null) ? $('#projectStatusSearch').val() : null;
        getSearchResults(key, projectMgtEmpID, projectTypeID, projectStatusID);
    });


    function getSearchResults(key = null, projectMgtEmpID = null, projectTypeID = null, projectStatusID = null) {
        
        $.ajax({
            type: 'POST',
            url: BASE_URL + '/api/tournament/search',
            data: {
                searchKey : key,
                projectMgtEmpID : projectMgtEmpID,
                projectTypeID : projectTypeID,
                projectStatusID : projectStatusID

            },
            success: function(data) {
                if (data.status == true) {
                    if (data.status) {
                        $("#projectList").html(data.html);
                    } else {
                        p_notification(data.status, data.msg);
                    }
                }            
            }
        });
    }

    $('#projectList').on('click','.delete',function (){
        var projectId = $(this).closest('tr').data('project-id');   
        var entityID = $(this).closest('tr').data('entityid');   
        bootbox.confirm('Are you sure you want to delete this Tournament', function($result) {
            if ($result == true) {
                $.ajax({
                    type: 'POST',
                    data: {
                        projectId: projectId,
                        entityID: entityID
                    },
                    url: BASE_URL + '/api/tournament/delete',
                    success: function(respond) {
                        p_notification(respond.status, respond.msg)
                        if (respond.status) {
                            window.location.reload();
                        }
                        
                    }
                });
            }
        });
    });
});
