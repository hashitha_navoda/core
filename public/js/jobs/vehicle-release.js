$(function() {
    $('#job-search').on('submit', function(e) {
        e.preventDefault();
        var formData = {
            jobSearchKey: $('#job-search-keyword').val(),
        };
        if (formData.jobSearchKey != '') {
            searchValue(formData);
        } else {
            p_notification(false, eb.getMessage('ERR_JBTYPE_SEARCH_KEY'));
        }
    });

    $('#job-search').on('click', '.reset', function(e) {
        e.preventDefault();
        searchValue('');
        $('#job-search-keyword').val('');
    });

    function searchValue(formData) {
        eb.ajax({
            url: BASE_URL + '/api/vehicleRelease/getJobBySearchKeyForVehicleRelease',
            method: 'post',
            data: formData,
            dataType: 'json',
            success: function(data) {
                if (data.status) {
                    $("#job-list").html(data.html);
                } else {
                    p_notification(data.status, data.msg);
                }
            }
        });
    }

    $('.outVehicle').on('click', function(event) {
        event.preventDefault();
        var jobID = $(this).parents('tr').data('jobid');
        var formData = {
            outType: 'singleJob',
            jobID: jobID
        };
        bootbox.confirm('Are you sure you want to out this vehicle ?', function(result) {
            if (result == true) {
                vehicleOutAndClose(formData);
            }
        });
    });

    $('.outAllVehicle').on('click', function(event) {
        event.preventDefault();
        var formData = {
            outType: 'bulk',
            jobID: null
        };
        bootbox.confirm('Are you sure you want to all vehicles ?', function(result) {
            if (result == true) {
                vehicleOutAndClose(formData);
            }
        });
    });

    function vehicleOutAndClose(formData)
    {
        eb.ajax({
            type: 'POST',
            url: BASE_URL + '/api/vehicleRelease/vehicleOut',
            data: formData,
            success: function(respond) {
                p_notification(respond.status, respond.msg);
                window.setTimeout(function() {
                    location.reload();
                }, 2000);
            },
            async: false
        });
    }
});