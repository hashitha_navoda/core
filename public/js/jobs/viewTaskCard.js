$(function() {
    $('#task-search').on('submit', function(e) {
        e.preventDefault();
        var formData = {
            taskSearchKey: $('#task-search-keyword').val(),
        };
        if (formData.taskSearchKey != '') {
            searchValue(formData);
        } else {
            p_notification(false, eb.getMessage('ERR_JBTYPE_SEARCH_KEY'));
        }
    });

    $('#task-search').on('click', '.reset', function(e) {
        e.preventDefault();
        searchValue('');
        $('#task-search-keyword').val('');
    });

    function searchValue(formData) {
        eb.ajax({
            url: BASE_URL + '/task_card_api/getTaskCardBySearchKey',
            method: 'post',
            data: formData,
            dataType: 'json',
            success: function(data) {
                if (data.status) {
                    $("#task-card-list").html(data.html);
                } else {
                    p_notification(data.status, data.msg);
                }
            }
        });
    }
    
    $('#task-card-list').on('click', 'a.delete', function(e) {
        e.preventDefault();

        var taskCardID = $(this).parents('tr').data('taskcardid');
        var taskCardCode = $(this).parents('tr').find('.tCode').text();
        var flag = false;
        bootbox.confirm('Are you sure you want to delete this Task Card?', function(result) {
            if (result === true) {
                eb.ajax({
                    url: BASE_URL + '/task_card_api/deleteTaskCardByTaskCardID',
                    method: 'post',
                    data: {
                        taskCardID: taskCardID,
                        taskCardCode: taskCardCode,
                    },
                    dataType: 'json',
                    success: function(data) {
                        if (data.status == true) {
                            p_notification(data.status, data.msg);
                            window.location.reload();
                        } else {
                            p_notification(data.status, data.msg);
                        }
                    }
                });
            }
        });

    });
});