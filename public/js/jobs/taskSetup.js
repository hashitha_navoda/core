var taskID = '';
var departmentID = '';
var productID = null;
var editMode = false;
$(function() {

    loadDropDownFromDatabase('/department-api/searchDepartmentsForDropdown', "", 1, '#department', false, false, false, true);
    $('#department').selectpicker('refresh');
    $('#department').on('change', function() {
        departmentID = $(this).val();
    });

    loadDropDownFromDatabase('/productAPI/searchNonInventoryActiveProductsForDropdown', "", false, '#addItem');
    $('#addItem').selectpicker('refresh');
    $('#addItem').on('change', function() {
        if ($(this).val() != 0 || $(this).val() != productID) {
            productID = $(this).val();
        }
    });

    $('#create-task-form').on('submit', function(e) {
        e.preventDefault();
        var formData = {
            taskCode: $('#taskCode').val(),
            taskName: $('#taskName').val(),
            editMode: editMode,
            taskID: taskID,
            departmentID: $('#department').val(),
            departmentID: departmentID,
            productID: productID
        };

        if (validateFormData(formData)) {
            eb.ajax({
                url: BASE_URL + '/task_setup_api/add',
                method: 'post',
                data: formData,
                dataType: 'json',
                success: function(data) {
                    p_notification(data.status, data.msg);
                    if (data.status) {
                        window.setTimeout(function() {
                            location.reload();
                        }, 1000);
                    } else {
                        if (data.data == "TERR") {
                            $('#taskCode').focus();
                        }
                    }
                }
            });
        }
    });

    $('#task-search').on('submit', function(e) {
        e.preventDefault();
        var formData = {
            taskSearchKey: $('#task-search-keyword').val(),
        };
        if (formData.taskSearchKey != '') {
            searchValue(formData);
        } else {
            p_notification(false, eb.getMessage('ERR_JBTYPE_SEARCH_KEY'));
        }
    });

    $('#task-search').on('click', '.reset', function(e) {
        e.preventDefault();
        searchValue('');
        $(this).parents('tr').find('#addItem').val(0).trigger('change').empty().selectpicker('refresh');
        $('#task-search-keyword').val('');
    });

    $('#task-list').on('click', 'a.edit', function(e) {
        e.preventDefault();
        taskID = $(this).parents('tr').data('taskid');
        $('#taskCode').val($(this).parents('tr').find('.tCode').text()).attr('disabled', true);
        $('#taskName').val($(this).parents('tr').find('.tName').text());
        $('#department').val($(this).parents('tr').data('departmentid'));
        $('#department').selectpicker('refresh');
        departmentID = $('#department').val();
        $("#addItem").empty().
            append($("<option></option>").
                     attr("value", $(this).parents('tr').data('productid')).
                    text($(this).parents('tr').find('.tProduct').text()));
        $('#addItem').val($(this).parents('tr').data('productid'));
        $('#addItem').prop('disabled', true);
        $('#addItem').selectpicker('render');
        productID = $('#addItem').val();
       
        $('.updateTitle').removeClass('hidden');
        $('.addTitle').addClass('hidden');
        $('.updateDiv').removeClass('hidden');
        $('.addDiv').addClass('hidden');
        editMode = true;
    });

    $('.cancel').on('click', function(e) {
        e.preventDefault();
        taskID = '';
        $('#taskCode').val('').attr('disabled', false);
        $('#taskName').val('');
        $('.updateTitle').addClass('hidden');
        $('.addTitle').removeClass('hidden');
        $('.updateDiv').addClass('hidden');
        $('.addDiv').removeClass('hidden');
        editMode = false;
    });

    $('#task-list').on('click', 'a.delete', function(e) {
        e.preventDefault();
        $('#taskCode').val('').attr('disabled', false);
        $('#taskName').val('');
        $('.updateTitle').addClass('hidden');
        $('.addTitle').removeClass('hidden');
        $('.updateDiv').addClass('hidden');
        $('.addDiv').removeClass('hidden');
        editMode = false;

        var taskID = $(this).parents('tr').data('taskid');
        var currentDiv = $(this).parents('tr').find('a.status').contents();
        var taskCode = $(this).parents('tr').find('.tCode').text();
        var flag = false;
        bootbox.confirm('Are you sure you want to delete this Task?', function(result) {
            if (result === true) {
                eb.ajax({
                    url: BASE_URL + '/task_setup_api/deleteTaskByTaskID',
                    method: 'post',
                    data: {
                        taskID: taskID,
                        taskCode: taskCode,
                    },
                    dataType: 'json',
                    success: function(data) {
                        if (data.status == true) {
                            p_notification(data.status, data.msg);
                            window.location.reload();
                        } else {
                            p_notification(data.status, data.msg);
                        }
                    }
                });
            }
        });

    });

    function validateFormData(formData) {
        if (formData.taskCode == "") {
            p_notification(false, eb.getMessage('ERR_TSKSTP_CODE_CNT_BE_NULL'));
            $('#taskCode').focus();
            return false;
        } else if (formData.taskName == '') {
            p_notification(false, eb.getMessage('ERR_TSKSTP_NAME_CNT_BE_NULL'));
            $('#taskName').focus();
            return false;
        } else if (formData.departmentID == '') {
            p_notification(false, eb.getMessage('ERR_TSKSTP_DPT_CNT_BE_NULL'));
            $('#department').focus();
            return false;
        } else if (formData.productID == null || formData.productID == 0) {
            p_notification(false, eb.getMessage('ERR_TSKSTP_PRO_CNT_BE_NULL'));
            $('#addItem').focus();
            return false;
        }
        return true;
    }
    function searchValue(formData) {
        eb.ajax({
            url: BASE_URL + '/task_setup_api/getTaskBySearchKey',
            method: 'post',
            data: formData,
            dataType: 'json',
            success: function(data) {
                if (data.status) {
                    $("#task-list").html(data.html);
                } else {
                    p_notification(data.status, data.msg);
                }
            }
        });
    }

});