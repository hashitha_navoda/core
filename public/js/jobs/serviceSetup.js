var serviceID = '';
var departmentID = '';
var productID = null;
var editMode = false;
var subTaskCode = null;
var subTaskName = null;
var subTasks = {};
var allsubTasks = {};
var mainSubTasks = {};
$(function() {

    getRelatedSubTasks(null);

    loadDropDownFromDatabase('/department-api/searchDepartmentsForDropdown', "", 1, '#department', false, false, false, true);
    $('#department').selectpicker('refresh');
    $('#department').on('change', function() {
        departmentID = $(this).val();
    });

    loadDropDownFromDatabase('/productAPI/searchNonInventoryActiveProductsForDropdown', "", false, '#addItem');
    $('#addItem').selectpicker('refresh');
    $('#addItem').on('change', function() {
        if ($(this).val() != 0 || $(this).val() != productID) {
            productID = $(this).val();
        }
    });

    $('#add, #updateType').on('click', function(e) {
        
        e.preventDefault();


        var vehicalTypeIDs = [];
        $.each($('#vehicalTypeDiv')[0].children, function(index, val) {
            $.each(val.children, function(ind, valOfInp) {
                $.each(valOfInp.children, function(indexOfChec, valOfCheck) {
                    if (valOfCheck.type == "checkbox" && valOfCheck.checked == true) {
                        vehicalTypeIDs.push(val.id);
                    }
                });
            });
        });

        var formData = {
            taskCode: $('#serviceCode').val(),
            taskName: $('#serviceName').val(),
            editMode: editMode,
            taskID: serviceID,
            departmentID: $('#department').val(),
            productID: productID,
            subTasks: mainSubTasks,
            vehicalTypeIDs: vehicalTypeIDs
        };

        if (validateFormData(formData)) {
            eb.ajax({
                url: BASE_URL + '/serviceSetup-api/add',
                method: 'post',
                data: formData,
                dataType: 'json',
                success: function(data) {
                    p_notification(data.status, data.msg);
                    if (data.status) {
                        window.setTimeout(function() {
                            location.reload();
                        }, 1000);
                    } else {
                        if (data.data == "TERR") {
                            $('#taskCode').focus();
                        }
                    }
                }
            });
        }
    });

    $('#task-search').on('submit', function(e) {
        e.preventDefault();
        var formData = {
            taskSearchKey: $('#service-search-keyword').val(),
        };

        if (formData.taskSearchKey != '') {
            searchValue(formData);
        } else {
            p_notification(false, eb.getMessage('ERR_SERVICE_STP_SEARCH_KEY'));
        }
    });

    $('#task-search').on('click', '.reset', function(e) {
        e.preventDefault();
        searchValue('');
        $(this).parents('tr').find('#addItem').val(0).trigger('change').empty().selectpicker('refresh');
        $('#service-search-keyword').val('');
    });

    $('#service-list').on('click', 'a.edit', function(e) {

        e.preventDefault();
        serviceID = $(this).parents('tr').data('serviceid');
        $('#serviceCode').val($(this).parents('tr').find('.sCode').text()).attr('disabled', true);
        $('#serviceName').val($(this).parents('tr').find('.sName').text());
        $('#department').val($(this).parents('tr').data('departmentid'));
        $('#department').selectpicker('refresh');
        departmentID = $('#department').val();
        $("#addItem").empty().
            append($("<option></option>").
                     attr("value", $(this).parents('tr').data('productid')).
                    text($(this).parents('tr').find('.sProduct').text()));
        $('#addItem').val($(this).parents('tr').data('productid'));
        $('#addItem').prop('disabled', true);
        $('#addItem').selectpicker('render');
        productID = $('#addItem').val();
       
        $('.updateTitle').removeClass('hidden');
        $('.addTitle').addClass('hidden');
        $('.update').removeClass('hidden');
        $('.add').addClass('hidden');
        editMode = true;
        getRelatedSubTasks(serviceID, true);

        $('#addVehicalType').addClass('hidden');
        $('#viewVehicalType').removeClass('hidden');
        $('#vehicalTypeDiv').removeClass('hidden');

    });

    $('#cancelService, #reset').on('click', function(e) {
        e.preventDefault();
        cancelResetView();
    });

    function cancelResetView() {
        serviceID = '';
        $('#serviceCode').val('').attr('disabled', false);
        $('#serviceName').val('');
        $('.updateTitle').addClass('hidden');
        $('.addTitle').removeClass('hidden');
        $('.update').addClass('hidden');
        $('.add').removeClass('hidden');
        editMode = false;
        $('select[name=serviceDept]').val('');
        $('select[name=addItem]').val('');
        $('#addItem').prop('disabled', false);
        $("#addItem").empty().
            append($("<option></option>").
                    attr("value", '').
                    text('Select a non inventory item'));
        $('.selectpicker').selectpicker('refresh');
        $('#addSubTaskModal #addSubTaskBody .addedSubTask').remove();
    }

    $('#service-list').on('click', 'a.delete', function(e) {
        e.preventDefault();
        cancelResetView();

        var serviceID = $(this).parents('tr').data('serviceid');
        var currentDiv = $(this).parents('tr').find('a.status').contents();
        var serviceCode = $(this).parents('tr').find('.sCode').text();
        var flag = false;
        bootbox.confirm('Are you sure you want to delete this Service?', function(result) {
            if (result === true) {
                eb.ajax({
                    url: BASE_URL + '/serviceSetup-api/deleteServiceByServiceID',
                    method: 'post',
                    data: {
                        taskID: serviceID,
                        taskCode: serviceCode,
                    },
                    dataType: 'json',
                    success: function(data) {
                        if (data.status == true) {
                            p_notification(data.status, data.msg);
                            window.location.reload();
                        } else {
                            p_notification(data.status, data.msg);
                        }
                    }
                });
            }
        });

    });

    function validateFormData(formData) {
        if (formData.taskCode == "") {
            p_notification(false, eb.getMessage('ERR_SRVCSTP_CODE_CNT_BE_NULL'));
            $('#serviceCode').focus();
            return false;
        } else if (formData.taskName == '') {
            p_notification(false, eb.getMessage('ERR_SRVCSTP_NAME_CNT_BE_NULL'));
            $('#serviceName').focus();
            return false;
        } else if (formData.departmentID == '' || formData.departmentID == 0) {
            p_notification(false, eb.getMessage('ERR_SRVCSTP_DPT_CNT_BE_NULL'));
            $('#department').focus();
            return false;
        } else if (formData.productID == null || formData.productID == 0) {
            p_notification(false, eb.getMessage('ERR_SRVCSTP_PRO_CNT_BE_NULL'));
            $('#addItem').focus();
            return false;
        }
        return true;
    }
    function searchValue(formData) {
        eb.ajax({
            url: BASE_URL + '/serviceSetup-api/getServicesBySearchKey',
            method: 'post',
            data: formData,
            dataType: 'json',
            success: function(data) {
                if (data.status) {
                    $("#service-list").html(data.html);
                } else {
                    p_notification(data.status, data.msg);
                }
            }
        });
    }

    $('#serviceSubTasks').on('click', function() {
        reomoveUnAddedSubTasks();
    });

     $('#btnSubTaskClose').on('click',function(){    
        reomoveUnAddedSubTasks();
    });

    //add new project manager
    $('#add_sub_task').on('click', function(e) {
        e.preventDefault();

        //if plus button is clicked then disabled the button
        //if button is clicked more than one, then it does not continue

        if ($(this).is(':disabled')) {
            return false;
        } else {
            $(this).attr('disabled', true);
            var $thisRow = $(this).parents('tr');
            if (!$('#subTaskCode').val().trim()) {
                // $('#addNewProjectManagerRow').val('');
                p_notification(false, eb.getMessage('ERR_REQUIRED_SUB_TSK_CODE'));
                $(this).attr('disabled', false);
            } else if (!$('#subTaskName').val().trim()) {
                // $('#addNewProjectManagerRow').val('');
                p_notification(false, eb.getMessage('ERR_REQUIRED_SUB_TSK_NAME'));
                $(this).attr('disabled', false);
            } else {
                subTaskCode = $('#subTaskCode').val();
                subTaskName = $('#subTaskName').val();
                addNewSubTaskRow(false);
                $(this).attr('disabled', false);
                
            }
        }
    });

    function addNewSubTaskRow(autofill) {


        if ($('#mode').val() != 'view') {
            if (autofill == false) {
                if (subTaskCode in subTasks ) {
                    p_notification(false, eb.getMessage('ERR_SUB_TSK_DUPLICATE'));
                    return;
                }
                if (subTaskCode in allsubTasks) {
                    p_notification(false, eb.getMessage('ERR_SUB_TSK_SYS_DUPLICATE'));
                    return;
                }
            }

            var tempSubTaskCode = subTaskCode.replace(/\s/g, '');
            var newTrID = 'tr_'+tempSubTaskCode;
            var clonedRow = $($('#preSample').clone()).attr('id', newTrID).addClass('addedSubTask');
            $("input[name='addSubTaskName']", clonedRow).val(subTaskName);
            $("input[name='addSubTaskCode']", clonedRow).val(subTaskCode);
            clonedRow.data('sub-task-code', subTaskCode);
            clonedRow.removeClass('hidden');
            clonedRow.insertBefore('#preSample');
            subTasks[subTaskCode] = subTaskName;
            if (autofill) {
                $('#addSubTaskModal #addSubTaskBody #formRow '+'#'+newTrID+' #colnedDeleteSubTask').addClass('disabled');
            }

            clearAddedNewRow();
        } 
    }

    function clearAddedNewRow() {
        $('#subTaskName').val('');
        $('#subTaskCode').val('');
    }

    $('#btnAddSubTask').on('click',function(){
        if ($('#subTaskCode').val().trim() || $('#subTaskName').val().trim()) {
            p_notification(false, eb.getMessage('ERR_ADD_SELECT_SUB_TASK'));
            return;
        }

        addServiceSubTask();
        $("#addSubTaskModal").modal('toggle');

    });


    function addServiceSubTask()
    {
        mainSubTasks = {};
        for (var key in subTasks) {
            if (subTasks.hasOwnProperty(key)) {
                mainSubTasks[key] = subTasks[key];
            }
        }

    }

    function reomoveUnAddedSubTasks() {
        
        var temp1 = [];
        for (var key in mainSubTasks) {
            if (mainSubTasks.hasOwnProperty(key)) {
                temp1.push(key);
            }
        }
        
        var temp2 = [];
        for (var key in subTasks) {
            if (subTasks.hasOwnProperty(key)) {
                temp2.push(key);
            }
        }
        for (var i = 0; i < temp2.length; i++) {    
            if ($.inArray(temp2[i], temp1) == -1) {
                delete subTasks[temp2[i]];
                $('#' + 'tr_'+temp2[i]).remove();
            }           
        }
        addServiceSubTask();
       
        $("#addSubTaskModal #addSubTaskBody").removeClass('hidden');
        $("#btnAddSubTask").removeClass('hidden');
        $("#btnSubTaskClose").removeClass('hidden');
        $("#btnViewSubTaskClose").addClass('hidden');
        $("#addSubTaskModal #viewSubTaskBody").addClass('hidden');
        $("#addSubTaskModal").modal('toggle');
    }

    $("#addSubTaskModal").on('click','.deleteSubTsk', function(e) {
        e.preventDefault();
        var deleteSubTaskTempID = $(this).closest('tr').attr('id');
        var deleteSubTaskID = $(this).closest('tr').data('sub-task-code');

        delete subTasks[deleteSubTaskID];
        $('#' + deleteSubTaskTempID).remove();

    });

    function getRelatedSubTasks(Id, vehiclTypeFlag = false) {
        eb.ajax({
            type: 'POST', url: BASE_URL + '/serviceSetup-api/getRelatedSubTasks', 
            data: {taskID: Id, vehiclTypeFlag: vehiclTypeFlag}, 
            success: function(respond) {
                if (respond.status == true) {
                    if (Id == null) {
                        if (vehiclTypeFlag) {
                            allsubTasks = respond.data.subTasks;
                        } else {
                            allsubTasks = respond.data;
                        }
                    } else {
                        if (vehiclTypeFlag) {
                            $('#subTaskAddTable #formRow .addedSubTask').remove();
                            mainSubTasks = respond.data.subTasks;

                            for (var key in respond.data.subTasks) { 
                                subTaskCode = key;
                                subTaskName = respond.data.subTasks[key];
                                addNewSubTaskRow(true);
                            }

                            if (respond.data.vehicleTypes.length > 0) {
                                $.each($('#vehicalTypeDiv')[0].children, function(index, val) {
                                    $.each(val.children, function(ind, valOfInp) {
                                        $.each(valOfInp.children, function(indexOfChec, valOfCheck) {
                                            if (valOfCheck.type == "checkbox") {
                                                $.each(respond.data.vehicleTypes, function(inde, valex) {
                                                    if (valex == val.id) {
                                                        valOfCheck.checked = true;
                                                    }
                                                });
                                            }
                                        });
                                    });
                                });
                            }

                        } else {
                            $('#subTaskAddTable #formRow .addedSubTask').remove();
                            mainSubTasks = respond.data;

                            for (var key in respond.data) { 
                                subTaskCode = key;
                                subTaskName = respond.data[key];
                                addNewSubTaskRow(true);
                            }
                        }
                    }


                } 
            }
        });
    }

    function viewRelatedSubTasks(Id) {
        eb.ajax({
            type: 'POST', url: BASE_URL + '/serviceSetup-api/getSubTaskByTaskId', 
            data: {taskID: Id}, 
            success: function(respond) {
                if (respond.status == true) {
                    $("#addSubTaskModal #addSubTaskBody").addClass('hidden');
                    $("#btnAddSubTask").addClass('hidden');
                    $("#btnSubTaskClose").addClass('hidden');
                    $("#btnViewSubTaskClose").removeClass('hidden');
                    $("#addSubTaskModal #viewSubTaskBody").removeClass('hidden');
                    $("#addSubTaskModal").modal('toggle');

                    $('#addSubTaskModal #viewSubTaskBody .viewedSubTasks').remove();

                    for (var i = 0; i < respond.data.length; i++) {
                        
                        var newTrID = 'trs_'+respond.data[i]['code'];
                        var clonedRow = $($('#subTskRow').clone()).attr('id', newTrID).addClass('viewedSubTasks').data('sub-task-id', respond.data[i]['subTaskID']);
                        $("#subTskCode", clonedRow).text(respond.data[i]['code']);
                        $("#subTskName", clonedRow).text(respond.data[i]['name']);
                        clonedRow.removeClass('hidden');
                        clonedRow.insertBefore('#subTskRow');
                        if (respond.data[i]['status'] == '1') {
                            $('#addSubTaskModal #viewSubTaskBody #viewNewSubTaskRow '+'#'+newTrID+' .subTskStatusIcon').addClass('fa-check-square-o');
                        } else {
                            $('#addSubTaskModal #viewSubTaskBody #viewNewSubTaskRow '+'#'+newTrID+' .subTskStatusIcon').addClass('fa-square-o');
                        }
                    }    
                } 
            }
        });
    }

    $('#subTaskViewTable').on('click', '.subTskStatus', function(){
        var subTaskTempCode = $(this).closest('tr').attr('id');
        var subTaskCode = subTaskTempCode.split('trs_')[1].trim();
        if ($('#addSubTaskModal #viewSubTaskBody #viewNewSubTaskRow '+'#'+subTaskTempCode+' .subTskStatusIcon').hasClass('fa-check-square-o')) {
            msg = 'Are you sure you want to Deactivate this sub task';
            status = '0';
        }
        else if ($('#addSubTaskModal #viewSubTaskBody #viewNewSubTaskRow '+'#'+subTaskTempCode+' .subTskStatusIcon').hasClass('fa-square-o')) {
            msg = 'Are you sure you want to Activate this sub task';
            status = '1';
        }

        var subTaskId = $(this).closest('tr').data('sub-task-id');

        activeStatus(subTaskCode, msg, status, subTaskId);
    });


    $('#subTaskViewTable').on('click', '.deleteSubTask', function(){
        var subTaskTempCode = $(this).closest('tr').attr('id');
        var subTaskCode = subTaskTempCode.split('trs_')[1].trim();

        var subTaskId = $(this).closest('tr').data('sub-task-id');
        
        deleteSubTask(subTaskId, subTaskCode);
    });


    function activeStatus(relateSubTaskCode, msg, status, subTaskId) {
        var tempCodeId = 'trs_'+relateSubTaskCode;
        var currentDiv = $('#addSubTaskModal #viewSubTaskBody #viewNewSubTaskRow '+'#'+tempCodeId+' .subTskStatusIcon');
        bootbox.confirm(msg, function($result) {
            if ($result == true) {
                eb.ajax({
                    url: BASE_URL + '/serviceSetup-api/changeSubTaskStatus',
                    method: 'post',
                    dataType: 'json',
                    data: {
                        'subTaskId': subTaskId,
                        'subTaskStatus': status,
                    },
                    success: function(data) {
                        if (data.status) {
                            p_notification(data.status, data.msg);
                            if (currentDiv.hasClass('fa-check-square-o')) {
                                currentDiv.removeClass('fa-check-square-o');
                                currentDiv.addClass('fa-square-o');
                            }
                            else {
                                currentDiv.removeClass('fa-square-o');
                                currentDiv.addClass('fa-check-square-o');
                            }
                        } else {
                            p_notification(data.status, data.msg);
                        }
                    }
                });
            }
        });
    }

    function deleteSubTask(subTaskId, subTaskCode) {
        var tempCodeId = 'trs_'+subTaskCode;
        var currentDiv = $('#addSubTaskModal #viewSubTaskBody #viewNewSubTaskRow '+'#'+tempCodeId);
        bootbox.confirm('Are you sure you want to delete this Sub Task ?', function($result) {
            if ($result == true) {
                eb.ajax({
                    url: BASE_URL + '/serviceSetup-api/deleteSubTask',
                    method: 'post',
                    dataType: 'json',
                    data: {
                        'subTaskId': subTaskId,
                    },
                    success: function(data) {
                        if (data.status) {
                            p_notification(data.status, data.msg);
                            currentDiv.remove();
                        } else {
                            p_notification(data.status, data.msg);
                        }
                    }
                });
            }
        });
    }

    /**
     *
     *use to change active state
     */
    function activeInactive(type, status, currentDiv, msg, flag) {
        bootbox.confirm(msg, function($result) {
            if ($result == true) {
                eb.ajax({
                    url: BASE_URL + '/serviceSetup-api/changeStatusID',
                    method: 'post',
                    dataType: 'json',
                    data: {
                        'taskID': type,
                        'status': status,
                    },
                    success: function(data) {
                        if (data.status) {
                            p_notification(data.status, data.msg);
                            if (currentDiv.hasClass('fa-check-square-o') && flag) {
                                currentDiv.removeClass('fa-check-square-o');
                                currentDiv.addClass('fa-square-o');
                            }
                            else {
                                currentDiv.removeClass('fa-square-o');
                                currentDiv.addClass('fa-check-square-o');
                            }
                        } else {
                            p_notification(data.status, data.msg);
                        }
                    }
                });
            }
        });
    }

    $('#service-list').on('click', '.view-subTask', function(e) {
        cancelResetView();
        serviceID = $(this).parents('tr').data('serviceid');
        viewRelatedSubTasks(serviceID);
    });

    $('#service-list').on('click', '.serviceStat', function(e) {
        cancelResetView();
        var msg;
        var status;
        serviceID   = $(this).closest('tr').data('serviceid');
        var currentDiv = $(this).contents();
        var flag = true;

        if ($(this).children().hasClass('fa-check-square-o')) {
            msg = 'Are you sure you want to Deactivate this Service';
            status = '0';
        }
        else if ($(this).children().hasClass('fa-square-o')) {
            msg = 'Are you sure you want to Activate this Service';
            status = '1';
        }
        activeInactive(serviceID, status, currentDiv, msg, flag);
    });

    $('#addVehicalType').on('click', function(event) {
        event.preventDefault();
        $(this).addClass('hidden');
        $('#viewVehicalType').removeClass('hidden');
        $('#vehicalTypeDiv').removeClass('hidden');
    });
});