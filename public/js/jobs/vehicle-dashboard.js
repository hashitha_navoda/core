var jobData = {};
var deleteAttmpJobId = null;
var jobGlobalID = null;
var activeProductData = {};
var serviceMaterials = {};
var incrementOffset = 6;
var pendingOffset = 6;
var completedOffset = 6;
var inprogressOffset = 6;
var pendingCount = 6;
var completedCount = 6;
var selectedTab = "overall";
$(document).ready(function() {
    var locationID = $('#navBarSelectedLocation').attr('data-locationid');
    var pendingCount = parseFloat($("#pendingJobCount").attr('data-count'));
    var inprogressCount = parseFloat($("#inprogressJobCount").attr('data-count'));
    var completedCount = parseFloat($("#completedJobCount").attr('data-count'));
    //get active product list
    eb.ajax({
        type: 'POST',
        url: BASE_URL + '/productAPI/getActiveProductsFromLocation',
        data: {locationID: locationID},
        success: function(respond) {
            activeProductData = respond.data;
        },
        async: false
    });

    $('.newJob').on('click', function(event) {
      event.preventDefault();
        window.location.assign(BASE_URL + "/service-job");
    });

    $('.listView').on('click', function(event) {
      event.preventDefault();
        window.location.assign(BASE_URL + "/service-job-dashboard/list");
    });

    var jobTaskID = null;
  eb.ajax({
      type: 'POST',
      url: BASE_URL +'/service-job-dashboard-api/getJobsForDashboard',
      data:{},
      success: function(respond) {
          jobData = respond.data.data;
          if (respond.data.data.pendingJobCount > 0) {

            $('.jobPreviewDiv').removeClass('hidden');
            var jData = jobData.pendingJobList[0];
            jobGlobalID = jData.jobId;
            $('.editJob').attr('href', '/service-job/'+jobGlobalID);
            $('.deleteJob').attr('jobid', jobGlobalID);
            getJobRelatedServices(jData.jobId);
            getJobMaterials(jData.jobId);
            $('.jobPreviewSvg svg defs linearGradient .stop1').attr('stop-color', '#fabe50');
            $('.jobPreviewSvg svg defs linearGradient .stop2').attr('stop-color', '#f8701e');
            $('.vehicleRegNo').html(jData.serviceVehicleRegNo);
            $('.jobCardName').html(jData.taskCardName);
            $('.jobCustomerName').html(jData.customerName);
            $('.jobStartedTime').html(jData.createdTimeStamp);
            var vehicleModelName = (jData.vehicleModelName == null) ? "" : jData.vehicleModelName;
            var vehicleSubModelName = (jData.vehicleSubModelName == null) ? "" : jData.vehicleSubModelName;
            $('.vehicleModal').html(vehicleModelName+" "+vehicleSubModelName);
            $('.save-button-material-divv').removeClass('hidden');
            $('.addMaterialRow').removeClass('hidden');
            $('.addedTradeProducts').remove();
            $('.trade-product-table ').addClass('hidden');
          } else if (respond.data.data.inprogressJobCount > 0) {

            $(".pendingTab").removeClass('active');
            $("#pendingJobsTab").removeClass('in active');
            $("#inprogressJobsTab").addClass('in active');
            $(".inprogressTab").addClass('active');
            $('.inprogressJobsTab').trigger('click');


            $('.jobPreviewDiv').removeClass('hidden');
            var jData = jobData.inProgressJobList[0];
            jobGlobalID = jData.jobId;
            $('.editJob').attr('href', '/service-job/'+jobGlobalID);
            $('.deleteJob').attr('jobid', jobGlobalID);
            $('.editJob').removeClass('hidden');
            $('.deleteJob').removeClass('hidden');
            getJobRelatedServices(jData.jobId);
            getJobMaterials(jData.jobId);
            $('.jobPreviewSvg svg defs linearGradient .stop1').attr('stop-color', '#4e30b6');
            $('.jobPreviewSvg svg defs linearGradient .stop2').attr('stop-color', '#bd67d4');
            $('.vehicleRegNo').html(jData.serviceVehicleRegNo);
            $('.jobCardName').html(jData.taskCardName);
            $('.jobCustomerName').html(jData.customerName);
            $('.jobStartedTime').html(jData.createdTimeStamp);
            var vehicleModelName = (jData.vehicleModelName == null) ? "" : jData.vehicleModelName;
            var vehicleSubModelName = (jData.vehicleSubModelName == null) ? "" : jData.vehicleSubModelName;
            $('.vehicleModal').html(vehicleModelName+" "+vehicleSubModelName);
            $('.save-button-material-divv').removeClass('hidden');
            $('.addMaterialRow').removeClass('hidden');
            $('.addedTradeProducts').remove();
            $('.trade-product-table ').addClass('hidden');
          } else if (respond.data.data.completedJobCount > 0) {
            $('.jobPreviewDiv').removeClass('hidden');
            var jData = respond.data.data.completedJobList[0];
            jobGlobalID = jData.jobId;
            $('.editJob').attr('href', '/service-job/'+jobGlobalID);
            $('.deleteJob').attr('jobid', jobGlobalID);
            if (jData.jobStatus == 20) {
              $('.editJob').addClass('hidden');
              $('.deleteJob').addClass('hidden');
            } else {
              $('.editJob').removeClass('hidden');
              $('.deleteJob').removeClass('hidden');
            }
            getJobRelatedServices(jData.jobId);
            getJobMaterials(jData.jobId);
            $('.jobPreviewSvg svg defs linearGradient .stop1').attr('stop-color', '#b4ec51');
            $('.jobPreviewSvg svg defs linearGradient .stop2').attr('stop-color', '#429321');
            $('.vehicleRegNo').html(jData.serviceVehicleRegNo);
            $('.jobCardName').html(jData.taskCardName);
            $('.jobCustomerName').html(jData.customerName);
            $('.jobStartedTime').html(jData.createdTimeStamp);
            var vehicleModelName = (jData.vehicleModelName == null) ? "" : jData.vehicleModelName;
            var vehicleSubModelName = (jData.vehicleSubModelName == null) ? "" : jData.vehicleSubModelName;
            $('.vehicleModal').html(vehicleModelName+" "+vehicleSubModelName);
            $('.save-button-material-divv').addClass('hidden');
            $('.addMaterialRow').addClass('hidden');
            $('.addedTradeProducts').remove();
            $('.trade-product-table ').addClass('hidden');
          }
      }
  });

  // $("#costTab").on('click', function(event){
  //   console.log(jobGlobalID);
  //   // getJobEstimatedCostDetails();
  // });

// $(".job-tabs").scroll(function() {
$(document).on('wheel mousedown keydown','.job-tabs', function(e){
// document.getElementById("jobTab").addEventListener('scroll', function(e){
  console.log(selectedTab);
  if (selectedTab == "pending") {
    if($(".job-tabs").scrollTop() + $(".job-tabs").height() >= $(".job-tabs")[0].scrollHeight) {
      if (pendingCount > pendingOffset) {
        eb.ajax({
            type: 'POST',
            url: BASE_URL +'/service-job-dashboard-api/getPendingJobsForDashboard',
            data:{offset:pendingOffset},
            success: function(respond) {
                pendingOffset += 6;
                jobData.pendingJobList = jobData.pendingJobList.concat(respond.data.data.pendingJobList);
                $.each(respond.data.data.pendingJobList,function(key, value){
                    addPendingJobs(key, value);
                });
            }
        });
      }
    }
  } else if (selectedTab == "inprogress") {
    if($(".job-tabs").scrollTop() + $(".job-tabs").height() >= $(".job-tabs")[0].scrollHeight) {
      if (inprogressCount > inprogressOffset) {
        eb.ajax({
            type: 'POST',
            url: BASE_URL +'/service-job-dashboard-api/getInprogressJobsForDashboard',
            data:{offset:inprogressOffset},
            success: function(respond) {
                inprogressOffset += 6;
                // jobData = respond.data.data;
                jobData.inProgressJobList = jobData.inProgressJobList.concat(respond.data.data.inProgressJobList);
                $.each(respond.data.data.inProgressJobList,function(key, value){
                    addInprogressJobs(key, value);
                });
            }
        });
      }
    }
  } else if (selectedTab == "completed") {
    if($(".job-tabs").scrollTop() + $(".job-tabs").height() >= $(".job-tabs")[0].scrollHeight) {
      if (completedCount > completedOffset) {
        eb.ajax({
            type: 'POST',
            url: BASE_URL +'/service-job-dashboard-api/getCompletedJobsForDashboard',
            data:{offset:completedOffset},
            success: function(respond) {
                completedOffset += 6;
                // jobData = respond.data.data;
                jobData.completedJobList = jobData.completedJobList.concat(respond.data.data.completedJobList);
                $.each(respond.data.data.completedJobList,function(key, value){
                    addCompletedJobs(key, value);
                });
            }
        });
      }
    }
  }

});

  function addPendingJobs(key, value) {
    var $cloneRow = $($('.pending-sample-div').clone()).removeClass('pending-sample-div hidden');
    $cloneRow.addClass('sub-stat pendingjobs jobTile');

    $cloneRow.find('#timesStamp').text(value['createdTimeStamp'].split(" ")[1]);
    $cloneRow.find('#regNo').text(value['serviceVehicleRegNo']);
    $cloneRow.find('#kms').text(value['jobVehicleKMs']+' KM');
    $cloneRow.find('#tp').text(value['customerTelephoneNumber']);
    $cloneRow.find('#vm').text(value['vehicleModelName']);
    $cloneRow.find('#sm').text(value['vehicleSubModelName']);
    $cloneRow.attr('data-id', value['jobId']);
    $( "#pendingJobsTab" ).append($cloneRow);

  }

  function addInprogressJobs(key, value) {
    var $cloneRow = $($('.inprogress-sample-div').clone()).removeClass('inprogress-sample-div hidden');
    $cloneRow.addClass('sub-stat inProgressJob jobTile');

    $cloneRow.find('#timesStamp-inp').text(value['createdTimeStamp'].split(" ")[1]);
    $cloneRow.find('#regNo-inp').text(value['serviceVehicleRegNo']);
    $cloneRow.find('#kms-inp').text(value['jobVehicleKMs']+' KM');
    $cloneRow.find('#tp-inp').text(value['customerTelephoneNumber']);
    $cloneRow.find('#vm-inp').text(value['vehicleModelName']);
    $cloneRow.find('#sm-inp').text(value['vehicleSubModelName']);
    $cloneRow.attr('data-id', value['jobId']);
    $( "#inprogressJobsTab" ).append($cloneRow);

  }

  function addCompletedJobs(key, value) {
    var $cloneRow = $($('.completed-sample-div').clone()).removeClass('completed-sample-div hidden');
    $cloneRow.addClass('sub-stat completedJobs jobTile');

    $cloneRow.find('#timesStamp-com').text(value['createdTimeStamp'].split(" ")[1]);
    $cloneRow.find('#regNo-com').text(value['serviceVehicleRegNo']);
    $cloneRow.find('#kms-com').text(value['jobVehicleKMs']+' KM');
    $cloneRow.find('#tp-com').text(value['customerTelephoneNumber']);
    $cloneRow.find('#vm-com').text(value['vehicleModelName']);
    $cloneRow.find('#sm-com').text(value['vehicleSubModelName']);
    $cloneRow.attr('data-id', value['jobId']);
    $( "#finishedJobsTab" ).append($cloneRow);

  }


  function getJobEstimatedCostDetails(jobid) {
    eb.ajax({
        type: 'POST', url: BASE_URL + '/service-job-dashboard-api/getJobEstimatedCostDetails', 
        data: {jobId: jobid}, 
        success: function(respond) {
          // console.log(respond.data['data']['nonJobCardServices']);

          if (respond.data['data']['nonJobCardServices'].length > 0) {
            $('.service-table').removeClass('hidden');
            setServiceCostList(respond.data['data']['nonJobCardServices']);
          } else {
            $('.service-table').addClass('hidden');
            
          }

          if (respond.data['data']['jobCardRateDetails'].length > 0) {
              $('.job-card-table').removeClass('hidden');
              setJobCardCostList(respond.data['data']['jobCardRateDetails']);
          } else {
              $('.job-card-table').addClass('hidden');
          }

          if (respond.data['data']['tradeMaterials'].length > 0) {
              $('.trade-material-table').removeClass('hidden');
              setTradeMaterialList(respond.data['data']['tradeMaterials']);
          } else {
              $('.trade-material-table').addClass('hidden');
          }

          //calculate Est Total
          var total = parseFloat(respond.data['data']['jobCardRateTotal']) + parseFloat(respond.data['data']['nonJobCardServicesTotal']) + parseFloat(respond.data['data']['tradeMaterialTotal']);  
          $('#estTotal').text('Estimated Cost: '+total.toFixed(2));

        }
    });
  }


  function setServiceCostList(data) {
      $(".addedNonJobCardService").remove();
      for (var i = 0; i < data.length; i++) {
        var newTrID = 'tr_' + data[i]['jobServiceID'];
        var clonedRow = $($('#nonJobCardService').clone()).attr('id', newTrID).addClass('addedNonJobCardService');
        $("#serviceCode", clonedRow).text(data[i]['serviceCode']);
        $("#serviceName", clonedRow).text(data[i]['serviceName']);
        $("#serviceRate", clonedRow).text(data[i]['serviceRate']);
        clonedRow.removeClass('hidden');
        clonedRow.insertBefore('#nonJobCardService');
      }
  }

  function setJobCardCostList(data) {
    $(".addedJobCard").remove();
      for (var i = 0; i < data.length; i++) {
        // console.log(data[i]);
        var newTrID = 'tr_' + data[i]['jobCardID'];
        var clonedRow = $($('#jobCardRate').clone()).attr('id', newTrID).addClass('addedJobCard');
        $("#jobCardCode", clonedRow).text(data[i]['taskCardCode']);
        $("#jobCardName", clonedRow).text(data[i]['taskCardName']);
        $("#jobCardServiceRate", clonedRow).text(parseFloat(data[i]['serviceRate']).toFixed(2));
        clonedRow.removeClass('hidden');
        clonedRow.insertBefore('#jobCardRate');
      }
  }

  function setTradeMaterialList(data) {
    $(".addedTradeMaterial").remove();
      for (var i = 0; i < data.length; i++) {
        var newTrID = 'tr_' + data[i]['jobTempMaterialID'];
        var clonedRow = $($('#tradeMaterealRow').clone()).attr('id', newTrID).addClass('addedTradeMaterial');
        $("#itemNameAndCode", clonedRow).text(data[i]['productCode']+'-'+data[i]['productName']);
        $("#itemQty", clonedRow).text(parseFloat(data[i]['issueQty']).toFixed(2));
        $("#itemPrice", clonedRow).text(parseFloat(data[i]['defaultSellingPrice']).toFixed(2));
        $("#totalPrice", clonedRow).text(parseFloat((data[i]['defaultSellingPrice']) * parseFloat(data[i]['issueQty'])).toFixed(2));
        clonedRow.removeClass('hidden');
        clonedRow.insertBefore('#tradeMaterealRow');
      }
  }

  $(document).on('click', '#side-list .pendingJobsTab',function(event) {
      selectedTab = "pending";
      if (jobData.pendingJobCount > 0) {
          $('.jobPreviewDiv').removeClass('hidden');
          var jData = jobData.pendingJobList[0];
          jobGlobalID = jData.jobId;
          $('.editJob').attr('href', '/service-job/'+jobGlobalID);
          $('.deleteJob').attr('jobid', jobGlobalID);
          $('.editJob').removeClass('hidden');
          $('.deleteJob').removeClass('hidden');
          if (!$('.vehicle-service-tab-full-div').hasClass('hidden')) {
            $('.vehicle-service-tab-full-div').addClass('hidden');
          }

          if ($('.loadin-div').hasClass('hidden')) {
            $('.loadin-div').removeClass('hidden');
          }
          getJobRelatedServices(jData.jobId);
          getJobMaterials(jData.jobId);
          $('.jobPreviewSvg svg defs linearGradient .stop1').attr('stop-color', '#fabe50');
          $('.jobPreviewSvg svg defs linearGradient .stop2').attr('stop-color', '#f8701e');
          $('.vehicleRegNo').html(jData.serviceVehicleRegNo);
          $('.jobCardName').html(jData.taskCardName);
          $('.jobCustomerName').html(jData.customerName);
          $('.jobStartedTime').html(jData.createdTimeStamp);
          var vehicleModelName = (jData.vehicleModelName == null) ? "" : jData.vehicleModelName;
          var vehicleSubModelName = (jData.vehicleSubModelName == null) ? "" : jData.vehicleSubModelName;
          $('.vehicleModal').html(vehicleModelName+" "+vehicleSubModelName);
          $('.save-button-material-divv').removeClass('hidden');
          $('.addMaterialRow').removeClass('hidden');
          $('.addedTradeProducts').remove();
          $('.trade-product-table ').addClass('hidden');
      } else {
          $('.jobPreviewDiv').addClass('hidden');
      }
  });

  $(document).on('click', '#side-list .overallJobsTab',function(event) {
      selectedTab = "overall";
      if (jobData.completedJobCount > 0) {
        $('.jobPreviewDiv').removeClass('hidden');
        var jData = jobData.completedJobList[0];
        jobGlobalID = jData.jobId;
        $('.editJob').attr('href', '/service-job/'+jobGlobalID);
        $('.deleteJob').attr('jobid', jobGlobalID);
        if (jData.jobStatus == 20) {
          $('.editJob').addClass('hidden');
          $('.deleteJob').addClass('hidden');
        } else {
          $('.editJob').removeClass('hidden');
          $('.deleteJob').removeClass('hidden');
        }
        getJobRelatedServices(jData.jobId);
        getJobMaterials(jData.jobId);
        $('.jobPreviewSvg svg defs linearGradient .stop1').attr('stop-color', '#b4ec51');
        $('.jobPreviewSvg svg defs linearGradient .stop2').attr('stop-color', '#429321');
        $('.vehicleRegNo').html(jData.serviceVehicleRegNo);
        $('.jobCardName').html(jData.taskCardName);
        $('.jobCustomerName').html(jData.customerName);
        $('.jobStartedTime').html(jData.createdTimeStamp);
        $('.save-button-material-divv').addClass('hidden');
        $('.addMaterialRow').addClass('hidden');
        $('.addedTradeProducts').remove();
        $('.trade-product-table ').addClass('hidden');
        var vehicleModelName = (jData.vehicleModelName == null) ? "" : jData.vehicleModelName;
            var vehicleSubModelName = (jData.vehicleSubModelName == null) ? "" : jData.vehicleSubModelName;
            $('.vehicleModal').html(vehicleModelName+" "+vehicleSubModelName);
      } else if (jobData.inprogressJobCount > 0) {
        $('.jobPreviewDiv').removeClass('hidden');
        var jData = jobData.inProgressJobList[0];
        jobGlobalID = jData.jobId;
        $('.editJob').attr('href', '/service-job/'+jobGlobalID);
        $('.deleteJob').attr('jobid', jobGlobalID);
        $('.editJob').removeClass('hidden');
        $('.deleteJob').removeClass('hidden');
        getJobRelatedServices(jData.jobId);
        getJobMaterials(jData.jobId);
        $('.jobPreviewSvg svg defs linearGradient .stop1').attr('stop-color', '#4e30b6');
        $('.jobPreviewSvg svg defs linearGradient .stop2').attr('stop-color', '#bd67d4');
        $('.vehicleRegNo').html(jData.serviceVehicleRegNo);
        $('.jobCardName').html(jData.taskCardName);
        $('.jobCustomerName').html(jData.customerName);
        $('.jobStartedTime').html(jData.createdTimeStamp);
        $('.save-button-material-divv').removeClass('hidden');
        $('.addMaterialRow').removeClass('hidden');
        $('.addedTradeProducts').remove();
        $('.trade-product-table ').addClass('hidden');
        var vehicleModelName = (jData.vehicleModelName == null) ? "" : jData.vehicleModelName;
            var vehicleSubModelName = (jData.vehicleSubModelName == null) ? "" : jData.vehicleSubModelName;
            $('.vehicleModal').html(vehicleModelName+" "+vehicleSubModelName);
      } else if (jobData.pendingJobCount > 0) {
        $('.jobPreviewDiv').removeClass('hidden');
        var jData = jobData.pendingJobList[0];
        jobGlobalID = jData.jobId;
        $('.editJob').attr('href', '/service-job/'+jobGlobalID);
        $('.deleteJob').attr('jobid', jobGlobalID);
        $('.editJob').removeClass('hidden');
        $('.deleteJob').removeClass('hidden');
        getJobRelatedServices(jData.jobId);
        getJobMaterials(jData.jobId);
        $('.jobPreviewSvg svg defs linearGradient .stop1').attr('stop-color', '#fabe50');
        $('.jobPreviewSvg svg defs linearGradient .stop2').attr('stop-color', '#f8701e');
        $('.vehicleRegNo').html(jData.serviceVehicleRegNo);
        $('.jobCardName').html(jData.taskCardName);
        $('.jobCustomerName').html(jData.customerName);
        $('.jobStartedTime').html(jData.createdTimeStamp);
        $('.save-button-material-divv').removeClass('hidden');
        $('.addMaterialRow').removeClass('hidden');
        $('.addedTradeProducts').remove();
        $('.trade-product-table ').addClass('hidden');
        var vehicleModelName = (jData.vehicleModelName == null) ? "" : jData.vehicleModelName;
            var vehicleSubModelName = (jData.vehicleSubModelName == null) ? "" : jData.vehicleSubModelName;
            $('.vehicleModal').html(vehicleModelName+" "+vehicleSubModelName);
      } else {
          $('.jobPreviewDiv').addClass('hidden');
      }
  });

  $(document).on('click','#side-list .inprogressJobsTab',function(event) {
      selectedTab = "inprogress";
      if (jobData.inprogressJobCount > 0) {
          $('.jobPreviewDiv').removeClass('hidden');
          var jData = jobData.inProgressJobList[0];
          jobGlobalID = jData.jobId;
          $('.editJob').attr('href', '/service-job/'+jobGlobalID);
          $('.deleteJob').attr('jobid', jobGlobalID);
          $('.editJob').removeClass('hidden');
          $('.deleteJob').removeClass('hidden');
          if (!$('.vehicle-service-tab-full-div').hasClass('hidden')) {
            $('.vehicle-service-tab-full-div').addClass('hidden');
          }

          if ($('.loadin-div').hasClass('hidden')) {
            $('.loadin-div').removeClass('hidden');
          }
          getJobRelatedServices(jData.jobId);
          getJobMaterials(jData.jobId);
          $('.jobPreviewSvg svg defs linearGradient .stop1').attr('stop-color', '#4e30b6');
          $('.jobPreviewSvg svg defs linearGradient .stop2').attr('stop-color', '#bd67d4');
          $('.vehicleRegNo').html(jData.serviceVehicleRegNo);
          $('.jobCardName').html(jData.taskCardName);
          $('.jobCustomerName').html(jData.customerName);
          $('.jobStartedTime').html(jData.createdTimeStamp);
          $('.save-button-material-divv').removeClass('hidden');
          $('.addMaterialRow').removeClass('hidden');
          $('.addedTradeProducts').remove();
          $('.trade-product-table ').addClass('hidden');
          var vehicleModelName = (jData.vehicleModelName == null) ? "" : jData.vehicleModelName;
            var vehicleSubModelName = (jData.vehicleSubModelName == null) ? "" : jData.vehicleSubModelName;
            $('.vehicleModal').html(vehicleModelName+" "+vehicleSubModelName);
      } else {
          $('.jobPreviewDiv').addClass('hidden');
      }
  });

  $(document).on('click','#side-list .completedJobsTab', function(event) {
      selectedTab = "completed";
      if (jobData.completedJobCount > 0) {
          $('.jobPreviewDiv').removeClass('hidden');
          var jData = jobData.completedJobList[0];
          jobGlobalID = jData.jobId;
          $('.editJob').attr('href', '/service-job/'+jobGlobalID);
          $('.deleteJob').attr('jobid', jobGlobalID);
          if (jData.jobStatus == 20) {
            $('.editJob').addClass('hidden');
            $('.deleteJob').addClass('hidden');
          } else {
            $('.editJob').removeClass('hidden');
            $('.deleteJob').removeClass('hidden');
          }
          if (!$('.vehicle-service-tab-full-div').hasClass('hidden')) {
            $('.vehicle-service-tab-full-div').addClass('hidden');
          }

          if ($('.loadin-div').hasClass('hidden')) {
            $('.loadin-div').removeClass('hidden');
          }
          getJobRelatedServices(jData.jobId);
          getJobMaterials(jData.jobId);
          $('.jobPreviewSvg svg defs linearGradient .stop1').attr('stop-color', '#b4ec51');
          $('.jobPreviewSvg svg defs linearGradient .stop2').attr('stop-color', '#429321');
          $('.vehicleRegNo').html(jData.serviceVehicleRegNo);
          $('.jobCardName').html(jData.taskCardName);
          $('.jobCustomerName').html(jData.customerName);
          $('.jobStartedTime').html(jData.createdTimeStamp);
          $('.save-button-material-divv').addClass('hidden');
          $('.addMaterialRow').addClass('hidden');
          $('.addedTradeProducts').remove();
          $('.trade-product-table ').addClass('hidden');
          var vehicleModelName = (jData.vehicleModelName == null) ? "" : jData.vehicleModelName;
            var vehicleSubModelName = (jData.vehicleSubModelName == null) ? "" : jData.vehicleSubModelName;
            $('.vehicleModal').html(vehicleModelName+" "+vehicleSubModelName);
      } else {
          $('.jobPreviewDiv').addClass('hidden');
      }
  });

    $(document).on('click', '.jobTile', function (event) {
    // $('.jobTile').on('click', function(event) {
        event.preventDefault();
        jobGlobalID = $(this).attr('data-id');
        if ($(this).hasClass('completedJobs')) {
            $.each(jobData.completedJobList, function(index, jData) {
                if (jData.jobId == jobGlobalID) {
                    $('.editJob').attr('href', '/service-job/'+jobGlobalID);
                    $('.deleteJob').attr('jobid', jobGlobalID);
                    if (jData.jobStatus == 20) {
                      $('.editJob').addClass('hidden');
                      $('.deleteJob').addClass('hidden');
                    } else {
                      $('.editJob').removeClass('hidden');
                      $('.deleteJob').removeClass('hidden');
                    }

                    if (!$('.vehicle-service-tab-full-div').hasClass('hidden')) {
                      $('.vehicle-service-tab-full-div').addClass('hidden');
                    }

                    if ($('.loadin-div').hasClass('hidden')) {
                      $('.loadin-div').removeClass('hidden');
                    }
                    getJobRelatedServices(jData.jobId);
                    getJobMaterials(jData.jobId);
                    $('.jobPreviewSvg svg defs linearGradient .stop1').attr('stop-color', '#b4ec51');
                    $('.jobPreviewSvg svg defs linearGradient .stop2').attr('stop-color', '#429321');
                    $('.vehicleRegNo').html(jData.serviceVehicleRegNo);
                    $('.jobCardName').html(jData.taskCardName);
                    $('.jobCustomerName').html(jData.customerName);
                    $('.jobStartedTime').html(jData.createdTimeStamp);
                    var vehicleModelName = (jData.vehicleModelName == null) ? "" : jData.vehicleModelName;
                    var vehicleSubModelName = (jData.vehicleSubModelName == null) ? "" : jData.vehicleSubModelName;
                    $('.vehicleModal').html(vehicleModelName+" "+vehicleSubModelName);
                    $('.save-button-material-divv').addClass('hidden');
                    $('.addMaterialRow').addClass('hidden');
                    $('.addedTradeProducts').remove();
                    $('.trade-product-table ').addClass('hidden');                  
                }
            });
        } else if ($(this).hasClass('inProgressJob')) {
            $.each(jobData.inProgressJobList, function(index, jData) {
                if (jData.jobId == jobGlobalID) {

                    if (!$('.vehicle-service-tab-full-div').hasClass('hidden')) {
                      $('.vehicle-service-tab-full-div').addClass('hidden');
                    }

                    if ($('.loadin-div').hasClass('hidden')) {
                      $('.loadin-div').removeClass('hidden');
                    }
                    getJobRelatedServices(jData.jobId);
                    getJobMaterials(jData.jobId);
                    $('.editJob').attr('href', '/service-job/'+jobGlobalID);
                    $('.deleteJob').attr('jobid', jobGlobalID);
                    $('.editJob').removeClass('hidden');
                    $('.deleteJob').removeClass('hidden');
                    $('.jobPreviewSvg svg defs linearGradient .stop1').attr('stop-color', '#4e30b6');
                    $('.jobPreviewSvg svg defs linearGradient .stop2').attr('stop-color', '#bd67d4');
                    $('.vehicleRegNo').html(jData.serviceVehicleRegNo);
                    $('.jobCardName').html(jData.taskCardName);
                    $('.jobCustomerName').html(jData.customerName);
                    $('.jobStartedTime').html(jData.createdTimeStamp);
                    var vehicleModelName = (jData.vehicleModelName == null) ? "" : jData.vehicleModelName;
                    var vehicleSubModelName = (jData.vehicleSubModelName == null) ? "" : jData.vehicleSubModelName;
                    $('.vehicleModal').html(vehicleModelName+" "+vehicleSubModelName);
                    $('.save-button-material-divv').removeClass('hidden');
                    $('.addMaterialRow').removeClass('hidden');
                    $('.addedTradeProducts').remove();
                    $('.trade-product-table ').addClass('hidden');                    
                }
            });
        } else if ($(this).hasClass('pendingjobs')) {
            $.each(jobData.pendingJobList, function(index, jData) {
                if (jData.jobId == jobGlobalID) {

                    if (!$('.vehicle-service-tab-full-div').hasClass('hidden')) {
                      $('.vehicle-service-tab-full-div').addClass('hidden');
                    }

                    if ($('.loadin-div').hasClass('hidden')) {
                      $('.loadin-div').removeClass('hidden');
                    }
                    getJobRelatedServices(jData.jobId);
                    getJobMaterials(jData.jobId);
                    $('.editJob').attr('href', '/service-job/'+jobGlobalID);
                    $('.deleteJob').attr('jobid', jobGlobalID);
                    $('.editJob').removeClass('hidden');
                    $('.deleteJob').removeClass('hidden');
                    $('.jobPreviewSvg svg defs linearGradient .stop1').attr('stop-color', '#fabe50');
                    $('.jobPreviewSvg svg defs linearGradient .stop2').attr('stop-color', '#f8701e');
                    $('.vehicleRegNo').html(jData.serviceVehicleRegNo);
                    $('.jobCardName').html(jData.taskCardName);
                    $('.jobCustomerName').html(jData.customerName);
                    $('.jobStartedTime').html(jData.createdTimeStamp);
                    var vehicleModelName = (jData.vehicleModelName == null) ? "" : jData.vehicleModelName;
                    var vehicleSubModelName = (jData.vehicleSubModelName == null) ? "" : jData.vehicleSubModelName;
                    $('.vehicleModal').html(vehicleModelName+" "+vehicleSubModelName);  
                    $('.save-button-material-divv').removeClass('hidden');
                    $('.addMaterialRow').removeClass('hidden');
                    $('.addedTradeProducts').remove();
                    $('.trade-product-table ').addClass('hidden');                
                }
            });
        }
    });


  function getJobRelatedServices(jobId, updateList = false) {
    eb.ajax({
        type: 'POST', url: BASE_URL + '/service-job-dashboard-api/getJobRelatedServices', 
        data: {jobId: jobId}, 
        success: function(respond) {
            if (respond.status == true) {
                var serviceList = respond.data;
                $("#service-list").html(respond.html);
                $.each(serviceList, function(key, service) {
                    var clonedElement = $($('.jobStationSelect').clone()).attr('id', service.jobTaskID+"_stat").addClass('addedStation');

                    $("select[name='stationSelect']", clonedElement).empty();
                    $("select[name='stationSelect']", clonedElement)
                             .append($("<option></option>")
                                        .attr("value",0)
                                        .text("Select a station"));
                    $("select[name='stationSelect']", clonedElement).selectpicker('refresh');

                    $.each(service.departmentStationList, function(index, value) {
                        $("select[name='stationSelect']", clonedElement)
                             .append($("<option></option>")
                                        .attr("value",value.departmentStationId)
                                        .text(value.departmentStationName));

                    });
    
                    $("select[name='stationSelect']", clonedElement).selectpicker('refresh');
                    clonedElement.removeClass('hidden');
                    $('#'+ service.jobTaskID +' .jobStation').append(clonedElement);

                    clonedElement[0].children[2].remove();

                    if (clonedElement.length > 1) {
                      for (var i = 1; i < clonedElement.length; i++) {
                        clonedElement[i].remove();
                      }
                    }

                    var clonedElementemp = $($('.jobEmployeeSelect').clone()).attr('id', service.jobTaskID+"_emp").addClass('addedEmployee');

                    $("select[name='employeeSelect']", clonedElementemp).empty();
                    $("select[name='employeeSelect']", clonedElementemp).selectpicker('refresh');

                    $.each(service.departmentEmployeeList, function(index, val) {
                        $("select[name='employeeSelect']", clonedElementemp)
                             .append($("<option></option>")
                                        .attr("value",val.employeeId)
                                        .text(val.employeeCode+" "+((val.employeeFirstName != null) ? val.employeeFirstName: "")));

                    });
    
                    $("select[name='employeeSelect']", clonedElementemp).selectpicker('refresh');
                    clonedElementemp.removeClass('hidden');
                    $('#'+ service.jobTaskID +' .jobEmployee').append(clonedElementemp);

                    clonedElementemp[0].children[2].remove();

                    if (clonedElementemp.length > 1) {
                      for (var i = 1; i < clonedElementemp.length; i++) {
                        clonedElementemp[i].remove();
                      }
                    }
                });

                getJobEstimatedCostDetails(jobId);
                getJobVehicleRelatedStatus(jobId);
                getJobVehicleRelatedRemarks($('.jobVehicleID').val());
            } 
        }
    });
  }

    function getJobMaterials(jobId)
    {
        eb.ajax({
            type: 'POST', url: BASE_URL + '/service-job-dashboard-api/getJobRelatedMaterials', 
            data: {jobId: jobId}, 
            success: function(respond) {
                if (respond.status == true) {
                    $.each(respond.data, function(index, val) {
                        loadtradeMaterials(val);
                    });
                }
            }
        });
    }

    function loadtradeMaterials(value)
    {
        var issueQty = value.issueQty;
        $('.trade-product-table').removeClass('hidden');
        var newTrID = 'tr_' + value.productID;
        var clonedRow = $($('#tradeProductPreSetSample').clone()).attr('id', newTrID).addClass('addedTradeProducts');
        $("#tradeProdctNameAndCode", clonedRow).val(activeProductData[value.productID].pN+'-'+activeProductData[value.productID].pC).attr('disabled', true);
        $("input[name='avaliableQuantity']", clonedRow).val(activeProductData[value.productID].LPQ).addUomPrice(activeProductData[value.productID].uom);
        $("input[name='issueQuantity']", clonedRow).val(value.issueQty).addUom(activeProductData[value.productID].uom);
        $(clonedRow).find('.itemDescText').val(value.itemDes).attr('disabled', true);
        clonedRow.removeClass('hidden');
        clonedRow.insertBefore('#tradeProductPreSetSample');
        serviceMaterials[value.productID] = new jobMaterial(value.productID,null, issueQty, 1, activeProductData[value.productID].lPID, value.productUomID, activeProductData[value.productID].dSP,true, value.itemDes);

        $("input.uomqty", clonedRow).attr('disabled', true);
        $(".trade-goods-plus", clonedRow).addClass('hidden');
        $(".trade-goods-edit", clonedRow).addClass('hidden');
        $(".trade-goods-delete", clonedRow).addClass('hidden');
    }

    function getJobVehicleRelatedStatus(jobId) {
        eb.ajax({
            type: 'POST',
            url: BASE_URL +'/service-job-dashboard-api/getJobVehicleRelatedStatus',
            data:{jobID: jobId},
            success: function(respond) {
                $("#vehicle-status-view").html(respond.html);
            },
            async: false
        });
    }


    $(document).on('click', '.addItemDescription', function() {
        if ($('i', $(this)).hasClass('fa-comment-o')) {
            $('i', $(this)).removeClass('fa-comment-o').addClass('fa-comment');
            $(this).parents('tr').addClass('comment');
            $(this).parent().parent().siblings('.itemDescText').removeClass('hidden').focus();
        } else {
            $('i', $(this)).removeClass('fa-comment').addClass('fa-comment-o');
            $(this).parents('tr').removeClass('comment');
            $(this).parent().parent().siblings('.itemDescText').addClass('hidden');
        }
    });

    $('.remarks-button-div').on('click', function(event) {
        event.preventDefault();
        $('.remarksDiv').removeClass('hidden');
        $('.statusDiv').addClass('hidden');
    });

    $('.back-button-div').on('click', function(event) {
        event.preventDefault();
        $('.statusDiv').removeClass('hidden');
        $('.remarksDiv').addClass('hidden');
    });

    $('.save-button-remarks-div').on('click', function(event) {
        event.preventDefault();
        eb.ajax({
            type: 'POST', url: BASE_URL + '/service-job-dashboard-api/saveVehicleRemark', 
            data: {
                jobVehicleID: $('.jobVehicleID').val(),
                remark: $('.remarkText').val()
            }, 
            success: function(respond) {
                if (respond.status == true) {
                    getJobVehicleRelatedRemarks($('.jobVehicleID').val());
                    $('.statusDiv').removeClass('hidden');
                    $('.remarksDiv').addClass('hidden');
                    $('.remarkText').val('');
                } 
            }
        });
    });

    function getJobVehicleRelatedRemarks(jobVehicleID)
    {
        eb.ajax({
            type: 'POST',
            url: BASE_URL +'/service-job-dashboard-api/getJobVehicleRelatedRemarks',
            data:{jobVehicleID: jobVehicleID},
            success: function(respond) {
                $("#vehicle-remarks").html(respond.html);
                if ($('.vehicle-service-tab-full-div').hasClass('hidden')) {
                  $('.vehicle-service-tab-full-div').removeClass('hidden');
                }

                if (!$('.loadin-div').hasClass('hidden')) {
                  $('.loadin-div').addClass('hidden');
                }

            },
            async: false
        });
    }



  $("#service-list ").on('click', '.start_task',function(){
        jobTaskID = $(this).closest('tr').data('job-task-id');
        var serviceJobID = $(this).closest('tr').data('job-id');
     
        var issueMaterialFlag = false;
        if($("#"+jobTaskID+"issueMaterialForTask").is(':checked')) {
            issueMaterialFlag = true;
        } else {
            issueMaterialFlag = false;
        }

        var empArray = $(this).closest("tr").children('.jobEmployee').children('.addedEmployee').children("select[name='employeeSelect']").val();

        if ($(this).closest("tr").children('.jobStation').children('.addedStation').children("select[name='stationSelect']").val() != 0) {
            var departmentStationID = $(this).closest("tr").children('.jobStation').children('.addedStation').children("select[name='stationSelect']").val();
        } else {
            var departmentStationID = null;
        }

        if (empArray == null) {
            bootbox.confirm('Are you sure you want to continue without selecting an employee?', function($result) {
                if ($result == true) {
                    eb.ajax({
                        type: 'POST', url: BASE_URL + '/service-job-dashboard-api/startJobRelatedService', 
                        data: {
                            jobTaskId: jobTaskID,
                            jobId: serviceJobID,
                            issueMaterialFlag : issueMaterialFlag,
                            departmentStationID : departmentStationID,
                            empArray : empArray,
                            isFromDashboard: true
                        }, 
                        success: function(respond) {
                            if (respond.status == true) {
                                if (!$('.vehicle-service-tab-full-div').hasClass('hidden')) {
                                  $('.vehicle-service-tab-full-div').addClass('hidden');
                                }

                                if ($('.loadin-div').hasClass('hidden')) {
                                  $('.loadin-div').removeClass('hidden');
                                }
                                getJobRelatedServices(serviceJobID, true);
                                $('.jobPreviewSvg svg defs linearGradient .stop1').attr('stop-color', '#4e30b6');
                                $('.jobPreviewSvg svg defs linearGradient .stop2').attr('stop-color', '#bd67d4');
                                jobData = respond.data;
                                $("#side-list").html(respond.html);

                                pendingCount = parseInt(respond.data.pendingJobCount);
                                inprogressCount = parseInt(respond.data.inprogressJobCount);
                                inprogressOffset = 6;
                                pendingOffset = 6;
                                completedOffset = 6;
                                completedCount = parseInt(respond.data.completedJobCount);
                                $(".pendingTab").removeClass('active');
                                $("#pendingJobsTab").removeClass('in active');
                                $("#finishedJobsTab").removeClass('in active');
                                $(".completedTab").removeClass('active');
                                $("#inprogressJobsTab").addClass('active in');
                                $(".inprogressTab").addClass('active');
                                $('#side-list .inprogressJobsTab').trigger('click');
                            } else {
                              p_notification(false, respond.msg);
                            }
                        }
                    });
                }
            });
        } else {
            eb.ajax({
              type: 'POST', url: BASE_URL + '/service-job-dashboard-api/startJobRelatedService', 
              data: {
                  jobTaskId: jobTaskID,
                  jobId: serviceJobID,
                  issueMaterialFlag : issueMaterialFlag,
                  departmentStationID : departmentStationID,
                  empArray : empArray,
                  isFromDashboard: true
              }, 
              success: function(respond) {
                  if (respond.status == true) {
                      if (!$('.vehicle-service-tab-full-div').hasClass('hidden')) {
                        $('.vehicle-service-tab-full-div').addClass('hidden');
                      }

                      if ($('.loadin-div').hasClass('hidden')) {
                        $('.loadin-div').removeClass('hidden');
                      }
                      getJobRelatedServices(serviceJobID, true);
                      $('.jobPreviewSvg svg defs linearGradient .stop1').attr('stop-color', '#4e30b6');
                      $('.jobPreviewSvg svg defs linearGradient .stop2').attr('stop-color', '#bd67d4');

                      jobData = respond.data;
                      $("#side-list").html(respond.html);

                      pendingCount = parseInt(respond.data.pendingJobCount);
                      inprogressCount = parseInt(respond.data.inprogressJobCount);
                      inprogressOffset = 6;
                      pendingOffset = 6;
                      completedOffset = 6;
                      completedCount = parseInt(respond.data.completedJobCount);
                      $(".pendingTab").removeClass('active');
                      $("#pendingJobsTab").removeClass('in active');
                      $("#finishedJobsTab").removeClass('in active');
                      $(".completedTab").removeClass('active');
                      $("#inprogressJobsTab").addClass('in active');
                      $(".inprogressTab").addClass('active');
                      $('#side-list .inprogressJobsTab').trigger('click');
                  } else {
                    p_notification(false, respond.msg);
                  }
              }
          });
        }
    });

  $("#service-list ").on('click', '.hold_task',function(){
        jobTaskID = $(this).closest('tr').data('job-task-id');
        var serviceJobID = $(this).closest('tr').data('job-id');
        eb.ajax({
            type: 'POST', url: BASE_URL + '/service-job-dashboard-api/holdJobRelatedService', 
            data: {
                jobTaskId: jobTaskID,
                jobId: serviceJobID
            }, 
            success: function(respond) {
                if (respond.status == true) {
                    // p_notification(respond.status, respond.msg);
                    if (!$('.vehicle-service-tab-full-div').hasClass('hidden')) {
                      $('.vehicle-service-tab-full-div').addClass('hidden');
                    }

                    if ($('.loadin-div').hasClass('hidden')) {
                      $('.loadin-div').removeClass('hidden');
                    }
                    getJobRelatedServices(serviceJobID, true);
                } 
            }
        });
    });

  $("#service-list ").on('click', '.end_task',function(){
        jobTaskID = $(this).closest('tr').data('job-task-id');
        var serviceJobID = $(this).closest('tr').data('job-id');
        eb.ajax({
            type: 'POST', url: BASE_URL + '/service-job-dashboard-api/stopJobRelatedService', 
            data: {
                jobTaskId: jobTaskID,
                jobId: serviceJobID,
                isFromDashboard: true
            }, 
            success: function(respond) {
                if (respond.status == true) {
                    if (!$('.vehicle-service-tab-full-div').hasClass('hidden')) {
                      $('.vehicle-service-tab-full-div').addClass('hidden');
                    }

                    if ($('.loadin-div').hasClass('hidden')) {
                      $('.loadin-div').removeClass('hidden');
                    }
                    getJobRelatedServices(serviceJobID, true);
                    var result = checkAvailabilityToEnableEndAll(respond.data.serviceList);
                    if (result) {
                        $('.jobPreviewSvg svg defs linearGradient .stop1').attr('stop-color', '#b4ec51');
                        $('.jobPreviewSvg svg defs linearGradient .stop2').attr('stop-color', '#429321');
                        jobData = respond.data;
                        $("#side-list").html(respond.html);

                        pendingCount = parseInt(respond.data.pendingJobCount);
                        inprogressCount = parseInt(respond.data.inprogressJobCount);
                        inprogressOffset = 6;
                        pendingOffset = 6;
                        completedOffset = 6;
                        completedCount = parseInt(respond.data.completedJobCount);
                        // selectedTab = "pending";

                        $(".inprogressTab").removeClass('active');
                        $(".pendingTab").removeClass('active');
                        $("#pendingJobsTab").removeClass('in active');
                        $("#inprogressJobsTab").removeClass('in active');
                        $("#finishedJobsTab").addClass('in active');
                        $(".completedTab").addClass('active');
                        $('#side-list .completedJobsTab').trigger('click');
                    }
                }  
            }
        });
    });

  function checkAvailabilityToEnableEndAll(dataSet) {
        
        var pendingNum = 0;
        var inprogressNum = 0;

        for (var i = 0; i < dataSet.length; i++) {
            if (dataSet[i]['jobTaskStatus'] == 3 ) {
                pendingNum += 1;
            }

            if (dataSet[i]['jobTaskStatus'] == 8) {
                inprogressNum += 1;
            }
        }

        if (pendingNum == 0 && inprogressNum == 0) {
            return true;
        }
        return false;
    }

  $("#service-list ").on('click', '.restart_task',function(){
        jobTaskID = $(this).closest('tr').data('job-task-id');
        var serviceJobID = $(this).closest('tr').data('job-id');
        eb.ajax({
            type: 'POST', url: BASE_URL + '/service-job-dashboard-api/restartJobRelatedService', 
            data: {
                jobTaskId: jobTaskID,
                jobId: serviceJobID,
                isFromDashboard: true
            }, 
            success: function(respond) {
                if (respond.status == true) {
                    $('.jobPreviewSvg svg defs linearGradient .stop1').attr('stop-color', '#4e30b6');
                    $('.jobPreviewSvg svg defs linearGradient .stop2').attr('stop-color', '#bd67d4');
                    if (!$('.vehicle-service-tab-full-div').hasClass('hidden')) {
                      $('.vehicle-service-tab-full-div').addClass('hidden');
                    }

                    if ($('.loadin-div').hasClass('hidden')) {
                      $('.loadin-div').removeClass('hidden');
                    }
                    getJobRelatedServices(serviceJobID, true);

                    // selectedTab = "pending";

                    if ($(".completedTab").hasClass('active')) {

                      $("#side-list").html(respond.html);
                      jobData = respond.data;
                      pendingCount = parseInt(respond.data.pendingJobCount);
                      inprogressCount = parseInt(respond.data.inprogressJobCount);
                      inprogressOffset = 6;
                      pendingOffset = 6;
                      completedOffset = 6;
                      completedCount = parseInt(respond.data.completedJobCount);
                      $(".pendingTab").removeClass('active');
                      $("#pendingJobsTab").removeClass('in active');
                      $("#finishedJobsTab").removeClass('in active');
                      $(".completedTab").removeClass('active');
                      $("#inprogressJobsTab").addClass('in active');
                      $(".inprogressTab").addClass('active');
                      $('#side-list .inprogressJobsTab').trigger('click');
                    }
                } 
            }
        });
    });

  // $(".status-type-box").dblclick(function(){
  //   $(this).find('.status-type-img').addClass('hideDiv');
  //   $(this).find(".status-type").removeClass('hideDiv');
  //   $(this).find(".status-type").addClass('before-selecteding-type');
  //   $(this).find('.status-line').addClass('default-status-line');
  //   $(this).find('.transmission-fluid-status-line').addClass('default-transmission-fluid-status-line');
  // });

  // $(".status-type").click(function(){
  //   $(this).siblings('img').removeClass('hideDiv');
  //   $(this).siblings('span').addClass('hideDiv')
  //   $(this).siblings('span').find('.status-type-ok').addClass('hideDiv');
  //   $(this).removeClass('before-selecteding-type');
  //   $(this).siblings('hr').removeClass('default-status-line');
  //   $(this).siblings('hr').addClass('status-line');
  //   $(this).siblings('hr').removeClass('default-transmission-fluid-status-line');
  //   $(this).siblings('hr').addClass('transmission-fluid-status-line');
  // });

  $(".odo-input").keyup(function(){
    if ($.isNumeric($(this).val().slice(-1))) {
      $('#next-service-odo').html((parseInt($(this).val())+5000)+' km');
    } else {
      p_notification(false, 'Enter numbers Only!!');
      $(this).val($(this).val().slice(0, -1));
    }
  });

  var x, i, j, selElmnt, a, b, c;
  /*look for any elements with the class "custom-select":*/
  x = document.getElementsByClassName("custom-select");
  for (i = 0; i < x.length; i++) {
    selElmnt = x[i].getElementsByTagName("select")[0];
    /*for each element, create a new DIV that will act as the selected item:*/
    a = document.createElement("DIV");
    a.setAttribute("class", "select-selected");
    a.innerHTML = selElmnt.options[selElmnt.selectedIndex].innerHTML;
    x[i].appendChild(a);
    /*for each element, create a new DIV that will contain the option list:*/
    b = document.createElement("DIV");
    b.setAttribute("class", "select-items select-hide");
    for (j = 1; j < selElmnt.length; j++) {
      /*for each option in the original select element,
      create a new DIV that will act as an option item:*/
      c = document.createElement("DIV");
      c.innerHTML = selElmnt.options[j].innerHTML;
      c.addEventListener("click", function (e) {
        /*when an item is clicked, update the original select box,
        and the selected item:*/
        var y, i, k, s, h;
        s = this.parentNode.parentNode.getElementsByTagName("select")[0];
        h = this.parentNode.previousSibling;
        for (i = 0; i < s.length; i++) {
          if (s.options[i].innerHTML == this.innerHTML) {
            s.selectedIndex = i;
            h.innerHTML = this.innerHTML;
            y = this.parentNode.getElementsByClassName("same-as-selected");
            for (k = 0; k < y.length; k++) {
              y[k].removeAttribute("class");
            }
            this.setAttribute("class", "same-as-selected");
            break;
          }
        }
        h.click();
      });
      b.appendChild(c);
    }
    x[i].appendChild(b);
    a.addEventListener("click", function (e) {
      /*when the select box is clicked, close any other select boxes,
      and open/close the current select box:*/
      e.stopPropagation();
      closeAllSelect(this);
      this.nextSibling.classList.toggle("select-hide");
      this.classList.toggle("select-arrow-active");
    });
  }
  function closeAllSelect(elmnt) {
    /*a function that will close all select boxes in the document,
    except the current select box:*/
    var x, y, i, arrNo = [];
    x = document.getElementsByClassName("select-items");
    y = document.getElementsByClassName("select-selected");
    for (i = 0; i < y.length; i++) {
      if (elmnt == y[i]) {
        arrNo.push(i)
      } else {
        y[i].classList.remove("select-arrow-active");
      }
    }
    for (i = 0; i < x.length; i++) {
      if (arrNo.indexOf(i)) {
        x[i].classList.add("select-hide");
      }
    }
  }
  /*if the user clicks anywhere outside the select box,
  then close all select boxes:*/
  document.addEventListener("click", closeAllSelect);

   loadDropDownFromDatabase('/productAPI/search-location-products-for-dropdown', '', 0, '#tradeMaterialID', '', '', "jobTradingGoods");
    $('#tradeMaterialID').trigger('change');
    var tempProductKey;
    $('#tradeMaterialID').on('change', function() {
        if ($(this).val() > 0 && ($(this).val() != tempProductKey)) {
            tempProductKey = $(this).val();
            $('#tradeMaterialID').
                    append($("<option></option>").
                            attr("value", 'select').
                            text("Select Item"));
            $('#tradeMaterialID').val('select');
            $('#tradeMaterialID').selectpicker('refresh');
            $(".addedTradeProducts .uomqty").focus();
            var trID = 'tr_'+tempProductKey;
            var itemDuplicateCheck = false;

            $.each(serviceMaterials, function(index, val) {
                if (val.productID == tempProductKey) {
                    itemDuplicateCheck = true;                        
                }
            });

            if (!itemDuplicateCheck) {
                selectProducts(tempProductKey);
            } else {
                p_notification(false, eb.getMessage('ERR_GRN_INSERT_PROD'));
                $('#fixedAssetsID').focus();
            }
        } 
    });

    function selectProducts(selectedProductID) {

        if (!$.isEmptyObject(activeProductData[selectedProductID])) {
            $('.trade-product-table ').removeClass('hidden');
            setTradingProductList(selectedProductID);
            $("input[name='issueQuantity']").focus();
        }
    }

    function setTradingProductList(tradingGoodProID) {
        var newTrID = 'tr_' + tradingGoodProID;
        var clonedRow = $($('#tradeProductPreSetSample').clone()).attr('id', newTrID).addClass('addedTradeProducts');
        $("#tradeProdctNameAndCode", clonedRow).val(activeProductData[tradingGoodProID].pN+'-'+activeProductData[tradingGoodProID].pC).attr('disabled', true);
        $("input[name='avaliableQuantity']", clonedRow).val(activeProductData[tradingGoodProID].LPQ).addUomPrice(activeProductData[tradingGoodProID].uom);
        $("input[name='issueQuantity']", clonedRow).addUom(activeProductData[tradingGoodProID].uom);
        clonedRow.removeClass('hidden');
        clonedRow.insertBefore('#tradeProductPreSetSample');
        clonedRow.addClass('productToAdd');
        $('#tradeMaterialID').attr('disabled', true);
        $('.uomqty').attr("id", "itemQuantity");
        $("#"+newTrID+" #itemQuantity").val("");
        $("#"+newTrID+" #itemQuantity").focus();
        var des = $(clonedRow).find('.itemDescText').val();
        serviceMaterials[tradingGoodProID] = new jobMaterial(tradingGoodProID, null,0,2,activeProductData[tradingGoodProID].lPID, null, activeProductData[tradingGoodProID].dSP, des);
    }

    function jobMaterial(jobMaterialProID, jobProductID = null, issueQty, jobProductMaterialTypeID, jobMaterialTaskLocationProductID = null, jobMaterialSelectedUomID = null, unitPrice, isAdded = false, itemDes = null) {
        this.productID = jobMaterialProID;
        this.jobProductID = jobProductID;
        this.issueQty = issueQty;
        this.materialTypeID = jobProductMaterialTypeID;
        this.LocationProductID = jobMaterialTaskLocationProductID;
        this.selectedUomID = jobMaterialSelectedUomID;
        this.unitPrice = unitPrice;
        this.isAdded = isAdded;
        this.itemDes = itemDes;
    }


    $('.deleteJob').on('click', function(e) {
      e.preventDefault();

      deleteAttmpJobId = $(this).attr('jobid');

      $("#myModal").modal('show');

    });

    $('#normal-job-cancel').on('click', function(e) {
        $("#myModal").modal('hide');
        canceljob(false);

    });

    $('#job-cancel-with-item-return').on('click', function(e) {
        $("#myModal").modal('hide');
        canceljob(true);
    });

    $('#back-to-dashboard').on('click', function(e) {
        $("#myModal").modal('hide');
    });


    function canceljob(returnItems) {
      eb.ajax({
          url: BASE_URL + '/service-job-api/cancelJob',
          method: 'post',
          data: {
              jobID: deleteAttmpJobId,
              returnDeliveryNote : returnItems
          },
          dataType: 'json',
          success: function(data) {
              if (data.status == true) {
                  p_notification(data.status, data.msg);
                  window.location.reload();
              } else {
                  p_notification(data.status, data.msg);
              }
          }
      });

    }


    $(document).on('click', '.trade-goods-plus , .trade-goods-save', function() {

        var avaliableQuantity = $(this).closest('tr').find("input[name='avaliableQuantity']").val();
        var issueQuantity = $(this).closest('tr').find("input[name='issueQuantity']").val();
        var itemDes = $(this).closest('tr').find("#itemDescText").val();
        var selectedUomID = $(this).closest('tr').find("input[name='issueQuantity']").siblings('.uom-select').children('button').find('.selected').data('uomID');

        if (parseFloat(issueQuantity) <= 0 || issueQuantity == "") {
            p_notification(false, eb.getMessage('ERR_VLID_ISSUEQTY'));
            $(this).closest('tr').find("input[name='issueQuantity']").select();    
            return false;
        }

        if (parseFloat(avaliableQuantity) < parseFloat(issueQuantity)) {
            p_notification(false, eb.getMessage('ERR_VLID_ISSQTY_VS_AVALIBLE_QTY'));
            $(this).closest('tr').find("input[name='issueQuantity']").select();    
            return false;
        }
        
        var tradingProTrID = $(this).closest('tr').attr('id');
        var tradingProID = tradingProTrID.split('tr_')[1].trim();
        if (serviceMaterials[tradingProID]) {
            serviceMaterials[tradingProID].issueQty = issueQuantity;
            serviceMaterials[tradingProID].selectedUomID = selectedUomID;
            serviceMaterials[tradingProID].itemDes = itemDes;
            serviceMaterials[tradingProID].isAdded = true;
            $("#tradeMaterialID").attr('disabled', false);
            $(this).closest('tr').find("input.uomqty").attr('disabled', true);
            $(this).closest('tr').find("input.issueQuantity").attr('disabled', true);
            $(this).addClass('hidden');
            $(this).closest('tr').find(".trade-goods-edit").removeClass('hidden');
            $(this).closest('tr').find('.itemDescText').addClass('hidden').attr('disabled', true);

            if (!$(this).closest('tr').find('.addItemDescription i').hasClass('fa-comment-o')) {
              $(this).closest('tr').find('.addItemDescription i').removeClass('fa-comment').addClass('fa-comment-o');
            }

            $(this).closest('tr').removeClass('comment');
        }
        $(this).closest('tr').removeClass('productToAdd');
        $(this).closest('tr').removeClass('productToSave');
    });


    $(document).on('click', '.trade-goods-edit', function() {
        $(this).closest('tr').find("input.uomqty").attr('disabled', false);
        $(this).addClass('hidden');
        $(this).closest('tr').find(".trade-goods-plus").addClass('hidden');
        $(this).closest('tr').find(".trade-goods-save").removeClass('hidden');
        $(this).closest('tr').addClass('productToSave');
        $(this).closest('tr').find('.itemDescText').attr('disabled', false);
        var tradingProTrID = $(this).closest('tr').attr('id');
        var tradingProID = tradingProTrID.split('tr_')[1].trim();
        serviceMaterials[tradingProID].isAdded = false;


    });

    $(document).on('click', '.trade-goods-delete', function() {

        var deleteTradingProTrID = $(this).closest('tr').attr('id');
        var deleteTradingProID = deleteTradingProTrID.split('tr_')[1].trim();
        delete serviceMaterials[deleteTradingProID];
        $('#' + deleteTradingProTrID).remove();
        $("#tradeMaterialID").attr('disabled', false);
    });

    $('#save-material-button').on('click', function(event) {
        event.preventDefault();

        var notAddArray = [];
        $.each(serviceMaterials, function(index, material) {

          if (!material.isAdded) {
            notAddArray.push(material.productID);
          }

        });

        if (notAddArray.length > 0) {
          p_notification(false, "Please add selected material before save.");
          return;
        }

        if ($.isEmptyObject(serviceMaterials)) {
            p_notification(false, "Please add atleast one material.");
            return false;
        }

        eb.ajax({
            type: 'POST', url: BASE_URL + '/service-job-dashboard-api/saveTempJobMaterials', 
            data: {
                serviceMaterials: serviceMaterials,
                jobId: $('.deleteJob').attr('jobid')
            }, 
            success: function(respond) {
                console.log(respond);
                p_notification(respond.status, respond.msg);
                getJobEstimatedCostDetails($('.deleteJob').attr('jobid'));
                return true;
            }
        });
    });

});  