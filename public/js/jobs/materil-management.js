var activeProductData = {};
var serviceMaterials = {};
var selectedServiceMaterial = null;
$(document).ready(function() {
	var locationID = $('#navBarSelectedLocation').attr('data-locationid');

	//get active product list
    eb.ajax({
        type: 'POST',
        url: BASE_URL + '/productAPI/getActiveProductsFromLocation',
        data: {locationID: locationID},
        success: function(respond) {
            activeProductData = respond.data;
        },
        async: false
    });


    eb.ajax({
        type: 'POST',
        url: BASE_URL + '/api/materialManagement/getJobServiceByJobServiceId',
        data: {jobServiceID: $('#jobTaskId').val()},
        success: function(respond) {
            var jobData = respond.data.data;
            var existingServiceResources = jobData.jobProducts;
            $.each(existingServiceResources, function(jobProKey, jobProData) {
                console.log(jobProData);
                if (jobProData.jobProductMaterialTypeID == 1) {
                   loadServiceGoods(jobProData);
                } else if (jobProData.jobProductMaterialTypeID == 2) {
                    loadtradeMaterials(jobProData);
                }
                
            });
        },
        async: false
    });


    loadDropDownFromDatabase('/productAPI/search-location-products-for-dropdown', '', 0, '#tradeMaterialID', '', '', "jobTradingGoods");
    $('#tradeMaterialID').trigger('change');
    var tempProductKey;
    $('#tradeMaterialID').on('change', function() {
        if ($(this).val() > 0 && ($(this).val() != tempProductKey)) {
            tempProductKey = $(this).val();
            $('#tradeMaterialID').
                    append($("<option></option>").
                            attr("value", 'select').
                            text("Select Item"));
            $('#tradeMaterialID').val('select');
            $('#tradeMaterialID').selectpicker('refresh');

            var itemDuplicateCheck = false;

            $.each(serviceMaterials, function(index, val) {
                if (val.jobMaterialProID == tempProductKey) {
                    itemDuplicateCheck = true;                        
                }
            });

            if (!itemDuplicateCheck) {
                selectProducts(tempProductKey);
            } else {
                p_notification(false, eb.getMessage('ERR_GRN_INSERT_PROD'));
                $('#fixedAssetsID').focus();
            }
        } 
    });


    loadDropDownFromDatabase('/productAPI/search-location-products-for-dropdown', locationID, 0, '.locationProductID', '', '', 'taskCard');
    $('.locationProductID').on('change', function() {
        $tr = $(this).parents('tr');
        $("#avaliableQuantity").val('').siblings('.uomqty,.uom-select').remove();
        $("#avaliableQuantity").show();

        var selectLocationProductID = $('#selectLocationProductID', $tr).val();
        if(!($(this).val() == null || $(this).val() == 0) && selectLocationProductID != $(this).val()){
            eb.ajax({
                type: 'POST',
                url: BASE_URL + '/productAPI/get-location-product-details',
                data: {
                    productID: $(this).val(),
                    locationID: locationID
                },
                success: function(respond) {
                    if (respond.data) {
                        selectedServiceMaterial = respond.data;
                        // $('#selectLocationProductID', $tr).val(respond.data.lPID);
                        $("input[name='avaliableQuantity']", $tr).val(respond.data.LPQ).addUom(respond.data.uom);;
                        $("input[name='std-qty']", $tr).val(0);
                        $("input[name='issued-qty']", $tr).val(0);
                        $("input[name='issue-qty']", $tr).val(0);


                        $("#std-qty").attr('disabled', false);
                        $("#issue-qty", $tr).attr('disabled', false);
                        $('#uom option:not(:first)', $tr).remove();
                        $.each(respond.data.uom, function(index, value) {
                            $('#uom', $tr).append('<option value="' + value.uomID + '">' + value.uN + '(' + value.uA + ')</option>');
                        });
                        $('#uom', $tr).prop('disabled', false).val(0).selectpicker('refresh');
                    }
                }
            });
        }
    });

    function selectProducts(selectedProductID) {

        if (!$.isEmptyObject(activeProductData[selectedProductID])) {
            $('.trade-product-table ').removeClass('hidden');
            setTradingProductList(selectedProductID);
            $("input[name='issueQuantity']").focus();
        }
    }

    function setTradingProductList(tradingGoodProID) {
        var newTrID = 'tr_' + tradingGoodProID;
        var clonedRow = $($('#tradeProductPreSetSample').clone()).attr('id', newTrID).addClass('addedTradeProducts');
        $("#tradeProdctNameAndCode", clonedRow).text(activeProductData[tradingGoodProID].pN+'-'+activeProductData[tradingGoodProID].pC);
        $("input[name='avaliableQuantity']", clonedRow).val(activeProductData[tradingGoodProID].LPQ).addUomPrice(activeProductData[tradingGoodProID].uom);
        $("input[name='standardQuantity']", clonedRow).val(0).addUomPrice(activeProductData[tradingGoodProID].uom);
        $("input[name='issuedQuantity']", clonedRow).val(0).addUomPrice(activeProductData[tradingGoodProID].uom);
        $("input[name='issueQuantity']", clonedRow).addUom(activeProductData[tradingGoodProID].uom);
        clonedRow.removeClass('hidden');
        clonedRow.insertBefore('#tradeProductPreSetSample');
        clonedRow.addClass('productToAdd');
        $('#tradeMaterialID').attr('disabled', true);
        serviceMaterials[tradingGoodProID] = new jobMaterial(tradingGoodProID, null,0,2,activeProductData[tradingGoodProID].lPID, null, activeProductData[tradingGoodProID].dSP);
    }


    $(document).on('click', '.trade-goods-plus , .trade-goods-save', function() {

        var avaliableQuantity = $(this).closest('tr').find("input[name='avaliableQuantity']").val();
        var issueQuantity = $(this).closest('tr').find("input[name='issueQuantity']").val();
        var selectedUomID = $(this).closest('tr').find("input[name='issueQuantity']").siblings('.uom-select').children('button').find('.selected').data('uomID');

        if (parseFloat(issueQuantity) <= 0 || issueQuantity == "") {
            p_notification(false, eb.getMessage('ERR_VLID_ISSUEQTY'));
            $(this).closest('tr').find("input[name='issueQuantity']").select();    
            return false;
        }

        if (parseFloat(avaliableQuantity) < parseFloat(issueQuantity)) {
            p_notification(false, eb.getMessage('ERR_VLID_ISSQTY_VS_AVALIBLE_QTY'));
            $(this).closest('tr').find("input[name='issueQuantity']").select();    
            return false;
        }
        
        var tradingProTrID = $(this).closest('tr').attr('id');
        var tradingProID = tradingProTrID.split('tr_')[1].trim();
        if (serviceMaterials[tradingProID]) {
            serviceMaterials[tradingProID].issueQty = issueQuantity;
            serviceMaterials[tradingProID].jobMaterialSelectedUomID = selectedUomID;
            $("#tradeMaterialID").attr('disabled', false);
            $(this).closest('tr').find("input.uomqty").attr('disabled', true);
            $(this).closest('tr').find("input.issueQuantity").attr('disabled', true);
            $(this).addClass('hidden');
            $(this).closest('tr').find(".trade-goods-edit").removeClass('hidden');
        }
        $(this).closest('tr').removeClass('productToAdd');
        $(this).closest('tr').removeClass('productToSave');
    });


    $(document).on('click', '#add-service-mat', function() {
        $tr = $(this).parents('tr');
        var uomID = $("#uom", $tr).val();
        var newServiceMat = {};  

        if (selectedServiceMaterial.pID in serviceMaterials) {
            p_notification(false, eb.getMessage('ERR_ALREADY_ADDED_ITEM'));
            return false;
        }


        if (uomID == 0 || uomID == '') {
            p_notification(false, eb.getMessage('ERR_UOM_SELECT'));
            return false;
        }

        if (parseFloat($("input[name='avaliableQuantity']", $tr).val()) <  $("#issue-qty", $tr).val()) {
            p_notification(false, eb.getMessage('AVA_QTY_NOT_ENOUGH'));
            return false;
        }


        newServiceMat.jobMaterialTaskLocationProductID = selectedServiceMaterial.lPID;
        newServiceMat.jobProductID = 'new';
        newServiceMat.productID = selectedServiceMaterial.pID;
        newServiceMat.jobProductUnitPrice = 0.00000;
        newServiceMat.uomID = uomID;
        newServiceMat.jobProductAllocatedQty = $("#std-qty", $tr).val();
        newServiceMat.jobProductIssuedQty = 0;
        newServiceMat.jobProductReOrderLevel = null;
        newServiceMat.jobProductMaterialTypeID = 1;
        newServiceMat.productCode = selectedServiceMaterial.pC;
        newServiceMat.productName = selectedServiceMaterial.pN;
        newServiceMat.uomAbbr = selectedServiceMaterial.uom[uomID]['uA'];
        newServiceMat.issueQty = $("#issue-qty", $tr).val();
        newServiceMat.productEstimatedCost = 0;

        loadServiceGoods(newServiceMat);

        $('#locationProductID').find('option').remove();
        $('#locationProductID').removeData('AjaxBootstrapSelect');
        $('#locationProductID').removeData('selectpicker');
        $('#locationProductID').siblings('.bootstrap-select').remove();
        $('#locationProductID').siblings('.bootstrap-select').find('.dropdown-menu').remove();
        $('#locationProductID').append('<option value="">Select Service MAterial</option>');
        $('#locationProductID').attr('disabled',false).val('');
        $('#locationProductID').selectpicker('refresh');
        loadDropDownFromDatabase('/productAPI/search-location-products-for-dropdown', locationID, 0, '.locationProductID', '', '', 'taskCard');

        $('#uom', $tr).empty().val('').selectpicker('render');
        $('#std-qty', $tr).empty().val('');
        $('#issue-qty', $tr).empty().val('');
        $('#issued-qty', $tr).empty().val('');
        $("#avaliableQuantity").val('').siblings('.uomqty,.uom-select').remove();
        $("#avaliableQuantity").show();

    });


    $(document).on('click', '.trade-goods-edit', function() {
        $(this).closest('tr').find("input.uomqty").attr('disabled', false);
        $(this).addClass('hidden');
        $(this).closest('tr').find(".trade-goods-plus").addClass('hidden');
        $(this).closest('tr').find(".trade-goods-save").removeClass('hidden');
        $(this).closest('tr').addClass('productToSave');
    });

    $(document).on('click', '.trade-goods-delete', function() {

        var deleteTradingProTrID = $(this).closest('tr').attr('id');
        var deleteTradingProID = deleteTradingProTrID.split('tr_')[1].trim();
        delete serviceMaterials[deleteTradingProID];
        $('#' + deleteTradingProTrID).remove();
        $("#tradeMaterialID").attr('disabled', false);
    });

    function loadtradeMaterials(value)
    {
    	var issueQty = 0;
        $('.trade-product-table').removeClass('hidden');
        var newTrID = 'tr_' + value.productID;
        var clonedRow = $($('#tradeProductPreSetSample').clone()).attr('id', newTrID).addClass('addedTradeProducts');
        $("#tradeProdctNameAndCode", clonedRow).text(activeProductData[value.productID].pN+'-'+activeProductData[value.productID].pC);
        $("input[name='avaliableQuantity']", clonedRow).val(activeProductData[value.productID].LPQ).addUomPrice(activeProductData[value.productID].uom);
        $("input[name='standardQuantity']", clonedRow).val(value.jobProductAllocatedQty).addUomPrice(activeProductData[value.productID].uom);
        if (value.jobProductIssuedQty == null) {
	        if (parseFloat(activeProductData[value.productID].LPQ) >= parseFloat(value.jobProductAllocatedQty)) {
	        	issueQty = value.jobProductAllocatedQty;
	        	$("input[name='issueQuantity']", clonedRow).val(value.jobProductAllocatedQty).addUom(activeProductData[value.productID].uom);
	        } else {
	        	issueQty = activeProductData[value.productID].LPQ;
	        	$("input[name='issueQuantity']", clonedRow).val(activeProductData[value.productID].LPQ).addUom(activeProductData[value.productID].uom);
	        }
        	$("input[name='issuedQuantity']", clonedRow).val(0).addUomPrice(activeProductData[value.productID].uom);
        } else {
        	issueQty = 0;
        	$("input[name='issuedQuantity']", clonedRow).val(value.jobProductIssuedQty).addUomPrice(activeProductData[value.productID].uom);
        	$("input[name='issueQuantity']", clonedRow).val(0).addUom(activeProductData[value.productID].uom);
        }
        clonedRow.removeClass('hidden');
        clonedRow.insertBefore('#tradeProductPreSetSample');

        serviceMaterials[value.productID] = new jobMaterial(value.productID,value.jobProductID, issueQty, value.jobProductMaterialTypeID, null, null, activeProductData[value.productID].dSP);

        $("input.uomqty", clonedRow).attr('disabled', true);
        $(".trade-goods-plus", clonedRow).addClass('hidden');
        $(".trade-goods-edit", clonedRow).removeClass('hidden');
        $(".trade-goods-delete", clonedRow).addClass('hidden');
    }

    function loadServiceGoods(value)
    {
    	var issueQty = 0;
        $('.service-goods-table').removeClass('hidden');
        var newTrID = 'tr_' + value.productID;
        var clonedRow = $($('#serviceGoodsPreSetSample').clone()).attr('id', newTrID).addClass('addedServiceProducts');
        $("#serviceGoodsNameAndCode", clonedRow).text(activeProductData[value.productID].pN+'-'+activeProductData[value.productID].pC);
        $("input[name='avaliableQuantity']", clonedRow).val(activeProductData[value.productID].LPQ).addUomPrice(activeProductData[value.productID].uom);
        $("input[name='standardQuantity']", clonedRow).val(value.jobProductAllocatedQty).addUomPrice(activeProductData[value.productID].uom);
        if (value.jobProductIssuedQty == null) {
	        if (parseFloat(activeProductData[value.productID].LPQ) >= parseFloat(value.jobProductAllocatedQty)) {
	        	// $("input[name='issueQuantity']", clonedRow).val(value.jobProductAllocatedQty).addUom(activeProductData[value.productID].uom);

                if (value.jobProductID == "new") {
                    issueQty = value.issueQty;
                    if  (parseFloat(activeProductData[value.productID].LPQ) >= parseFloat(value.issueQty)) {
                        $("input[name='issueQuantity']", clonedRow).val(value.issueQty).addUom(activeProductData[value.productID].uom);
                    }

                } else {
	        	    issueQty = value.jobProductAllocatedQty;
                    $("input[name='issueQuantity']", clonedRow).val(value.jobProductAllocatedQty).addUom(activeProductData[value.productID].uom);
                }


	        } else {
	        	// $("input[name='issueQuantity']", clonedRow).val(activeProductData[value.productID].LPQ).addUom(activeProductData[value.productID].uom);

                if (value.jobProductID == "new") {
	        	    issueQty = value.issueQty;
                    $("input[name='issueQuantity']", clonedRow).val(value.issueQty).addUom(activeProductData[value.productID].uom);
                } else {
                    issueQty = activeProductData[value.productID].LPQ;
                    $("input[name='issueQuantity']", clonedRow).val(activeProductData[value.productID].LPQ).addUom(activeProductData[value.productID].uom);
                }

	        }
        	$("input[name='issuedQuantity']", clonedRow).val(0).addUomPrice(activeProductData[value.productID].uom);
        } else {
        	issueQty = 0;
        	$("input[name='issuedQuantity']", clonedRow).val(value.jobProductIssuedQty).addUomPrice(activeProductData[value.productID].uom);

            if (value.jobProductID == "new") {
                issueQty = value.issueQty;
                $("input[name='issueQuantity']", clonedRow).val(value.issueQty).addUom(activeProductData[value.productID].uom);
            } else {
               
    	        $("input[name='issueQuantity']", clonedRow).val(0).addUom(activeProductData[value.productID].uom);
            }


        }
        clonedRow.removeClass('hidden');
        clonedRow.insertBefore('#serviceGoodsPreSetSample');

        
        if (value.jobProductID == "new") {
            serviceMaterials[value.productID] = new jobMaterial(value.productID,value.jobProductID, issueQty, value.jobProductMaterialTypeID,value.jobMaterialTaskLocationProductID,value.uomID,activeProductData[value.productID].dSP, value.jobProductAllocatedQty);

        } else {

            serviceMaterials[value.productID] = new jobMaterial(value.productID,value.jobProductID, issueQty, value.jobProductMaterialTypeID,null,null,activeProductData[value.productID].dSP);
        }


        // $("input.uomqty", clonedRow).attr('disabled', true);

        

        $("input.uomqty", clonedRow).attr('disabled', true);
        $(".service-goods-plus", clonedRow).addClass('hidden');
        $(".service-goods-edit", clonedRow).removeClass('hidden');
    }

    function jobMaterial(jobMaterialProID, jobProductID = null, issueQty, jobProductMaterialTypeID, jobMaterialTaskLocationProductID = null, jobMaterialSelectedUomID = null, unitPrice, jobProductAllocatedQty = null) {
        this.jobMaterialProID = jobMaterialProID;
        this.jobProductID = jobProductID;
        this.issueQty = issueQty;
        this.jobProductMaterialTypeID = jobProductMaterialTypeID;
        this.jobMaterialTaskLocationProductID = jobMaterialTaskLocationProductID;
        this.jobMaterialSelectedUomID = jobMaterialSelectedUomID;
        this.unitPrice = unitPrice;
        this.jobProductAllocatedQty = jobProductAllocatedQty;
    }


    $(document).on('click', '.service-goods-save', function() {

        var avaliableQuantity = $(this).closest('tr').find("input[name='avaliableQuantity']").val();
        var issueQuantity = $(this).closest('tr').find("input[name='issueQuantity']").val();
        var selectedUomID = $(this).closest('tr').find("input[name='issueQuantity']").siblings('.uom-select').children('button').find('.selected').data('uomID');

        if (parseFloat(issueQuantity) <= 0 || issueQuantity == "") {
            p_notification(false, eb.getMessage('ERR_VLID_ISSUEQTY'));
            $(this).closest('tr').find("input[name='issueQuantity']").select();    
            return false;
        }

        if (parseFloat(avaliableQuantity) < parseFloat(issueQuantity)) {
            p_notification(false, eb.getMessage('ERR_VLID_ISSQTY_VS_AVALIBLE_QTY'));
            $(this).closest('tr').find("input[name='issueQuantity']").select();    
            return false;
        }
        
        var serviceProTrID = $(this).closest('tr').attr('id');
        var serviceProID = serviceProTrID.split('tr_')[1].trim();
        if (serviceMaterials[serviceProID]) {
            serviceMaterials[serviceProID].issueQty = issueQuantity;
            serviceMaterials[serviceProID].jobMaterialSelectedUomID = selectedUomID;
            $(this).closest('tr').find("input.uomqty").attr('disabled', true);
            $(this).addClass('hidden');
            $(this).closest('tr').find(".service-goods-edit").removeClass('hidden');
        }
    });


    $(document).on('click', '.service-goods-edit', function() {
        $(this).closest('tr').find("input.uomqty").attr('disabled', false);
        $(this).addClass('hidden');
        $(this).closest('tr').find(".service-goods-plus").addClass('hidden');
        $(this).closest('tr').find(".service-goods-save").removeClass('hidden');
    });

    $(document).on('click', '.service-goods-delete', function() {
        var deleteServiceProTrID = $(this).closest('tr').attr('id');
        var deleteServiceProID = deleteServiceProTrID.split('tr_')[1].trim();
        delete serviceMaterials[deleteServiceProID];
        $('#' + deleteServiceProTrID).remove();
    });


    $('#issueTradeMaterials, #issueServiceMaterials, #issueBothMaterials').on('click', function(event) {
    	event.preventDefault();	

    	var saveMaterialType;
    	if ($(this).hasClass('serviceMaterials')) {
    		saveMaterialType = "service";
    	} else if ($(this).hasClass('tradeMaterials')) {
    		saveMaterialType = "trade";
    	} else if ($(this).hasClass('both')) {
            saveMaterialType = "both";
        }

        var formData = {
            serviceMaterials:serviceMaterials,
            jobServiceID:$('#jobTaskId').val(),
            jobId:$('#jobId').val(),
            saveMaterialType:saveMaterialType
        };

    	eb.ajax({
            url: BASE_URL + '/api/materialManagement/updateServiceMaterials',
            method: 'post',
            data: formData,
            dataType: 'json',
            success: function(data) {
                p_notification(data.status, data.msg);
                if (data.status) {
                    window.setTimeout(function() {
                        window.location.assign(BASE_URL + "/materialManagement/index")
                    }, 1000);
                }
            }
        });
    });

    $('.jobEmployees').on('click', function(event) {
        event.preventDefault();
        $("#viewServiceEmployeeModal #serviceEmployeesTable tbody .addedEmployees").remove();
        eb.ajax({
            url: BASE_URL + '/api/materialManagement/getJobEmployeeByJobTaskId',
            method: 'post',
            data: {jobTaskID:$(this).attr('id')},
            dataType: 'json',
            success: function(respond) {
                if (respond.status) {
                    $.each(respond.data, function(index, val) {
                        var newTrID = 'tr_' + val.jobTaskID;
                        var clonedRow = $($('#preSample').clone()).attr('id', newTrID).addClass('addedEmployees');
                        if (val.employeeCode != null) {
                            $("#employeeCode", clonedRow).text(val.employeeCode);
                            $("#employeeName", clonedRow).text(val.employeeName);
                        } else {
                            $("#employeeName", clonedRow).text("Default Employee");
                        }
                        clonedRow.removeClass('hidden');
                        clonedRow.insertBefore('#preSample');
                    });
                }
            }
        });  
        $('#viewServiceEmployeeModal').modal('show');
    });
});