var locationProducts = [];
var selectedProductID;
var taskProductQty;
var selectedProductUom;
var uomQty;
var selectedId;
var taskProducts = {}
var tasks = {};
var taskRowEdit = false;
var uomList = {};
var priceListForCommonData = [];
var priceListForSingleTask = [];

$(document).ready(function() {
    var items = {};
    var $taskTable = $("#taskCardTaskTable");
    var $addRowSample = $('tr.add-row.sample.hidden', $taskTable);
    var proId;
    var locationProductID;
    var locationID = $('#navBarSelectedLocation').attr('data-locationid');
    var rowincrementID = 1;
    
    loadDropDownFromDatabase('/productAPI/search-location-products-for-dropdown', locationID, 0, '#itemCode', '', '', 'taskCard');
    $('#item_code').trigger('change');
    $('#itemCode').children().not('.itemCodeDefaultSelect').remove();
    $('#itemCode').selectpicker('refresh');

    var getAddRow = function(taskIncID) {
        if (!(taskIncID === undefined || taskIncID == '')) {
            var $row = $('table#taskCardTaskTable > tbody#add-new-item-row > tr').filter(function() {
                return $(this).data("taskIncID") == taskIncID;
            });
            return $row;
        }

        return $('tr.add-row:not(.sample)', $taskTable);
    };
    if (!getAddRow().length) {
        $($addRowSample.clone()).removeClass('sample hidden').appendTo($('#add-new-item-row'));
    }

    setTaskTypeahead();

    setUomArray();

    function setTaskTypeahead() {
        var $currentRow = getAddRow();
        $('#taskCode', $currentRow).selectpicker();
        var locationID = locationID;

        $('#uom', $currentRow).selectpicker('render').selectpicker('refresh');
        $('#uom', $currentRow).on('change', function() {
            $("#uom", $currentRow).data('uomIID', $(this).val());
        });

        loadDropDownFromDatabase('/task_setup_api/searchTasksForDropdown', "", 1, '#taskCode', $currentRow, '', false);
        $('#taskCode', $currentRow).on('change', function() {
            if (selectedId == $(this).val()) {
                p_notification(false, eb.getMessage('ERR_SELECTED_TASK_DUPLICATE'));
                $(this).val(0);
                clearTaskCode($currentRow);
                return false;
            }

            if ($(this).val() != 0 && selectedId != $(this).val()) {
                selectedId = $(this).val();
                selectTasksForTaskCard(selectedId);
            } 
        });
    }

    function clearTaskCode($currentRow) {
        $('#taskCode', $currentRow)
                .empty()
                .val('')
                .append($("<option></option>")
                        .attr("value", "0")
                        .text('Select a task'))
                .selectpicker('refresh')
                .trigger('change');
    }

    function setUomArray() {
        var regExp = /\(([^)]+)\)/;
        eb.ajax({
            url: BASE_URL + '/api/settings/uom/searchUomForDropdown',
            method: 'post',
            data: null,
            dataType: 'json',
            success: function(data) {
                if (!$.isEmptyObject(data.data.list)) {
                    $.each(data.data.list, function(index, value) {
                        var matches = regExp.exec(value['text']);
                        uomList[value['value']] = matches[1];
                    });
                }
            }
        });
    }

    function selectTasksForTaskCard(selectedTaskId) {

        var $currentRow = getAddRow();

        var taskDuplicateCheck = false;
        var taskCardTaskID = selectedTaskId;

        if (!$.isEmptyObject(tasks)) {
            $.each(tasks, function(index, value) {
                if (value.taskID == taskCardTaskID) {
                    taskDuplicateCheck = true;
                }
            });
        }

        if (taskDuplicateCheck) {
            p_notification(false, eb.getMessage('ERR_SELECTED_TASK_DUPLICATE'));
        }

        $currentRow.data('id', selectedTaskId);
        $currentRow.data('icid', rowincrementID);
        $currentRow.data('taskIncID', rowincrementID);
        $("#taskCode", $currentRow).data('taskIID', selectedTaskId);
        var rowincID = rowincrementID;

        $currentRow.attr('id', 'task' + selectedTaskId);
    }

    $taskTable.on('click', 'button.add, button.save', function(e) {
        e.preventDefault();
        var $thisRow = $(this).parents('tr');
        var thisVals = getCurrentTaskData($thisRow);

        var taskDuplicateCheck = false;
        if (!$.isEmptyObject(tasks)) {
            $.each(tasks, function(index, value) {
                if (value.taskID == thisVals.taskID) {
                    taskDuplicateCheck = true;
                }
            });
        }

        if ((thisVals.taskID) === undefined) {
            p_notification(false, eb.getMessage('PLS_SELECT_AT_LEAST_ONE_TASK'));
            $("#taskCode", $thisRow).focus();
            return false;
        }

        if ((thisVals.UomId) === undefined || (thisVals.UomId) === '0') {
            p_notification(false, eb.getMessage('PLS_SELECT_UOM'));
            $("#uom", $thisRow).focus();
            return false;
        }

        if (priceListForCommonData[thisVals.taskID] == undefined) {
            p_notification(false, eb.getMessage('PLS_ADD_UNIT_PRICE'));
            return false;
        }

        // if add button is clicked
        if ($(this).hasClass('add')) {
            if (taskDuplicateCheck) {
                p_notification(false, eb.getMessage('ERR_SELECTED_TASK_DUPLICATE'));
                return false;
            }

            $thisRow.data('icid', rowincrementID);
            var $currentRow = $thisRow;
            var $flag = false;
            if ($currentRow.hasClass('add-row')) {
                $flag = true;
            }

            $currentRow
                    .removeClass('edit-row')
                    .find("#taskCode, input[name='defaultRate'],input[name='rate2'],input[name='rate3'],#uom").prop('readonly', true).end()
                    .find("button.delete").removeClass('disabled');
            $currentRow.find("input[name='defaultRate']").change();
            $currentRow.find("input[name='rate2']").change();
            $currentRow.find("input[name='rate3']").change();
            $currentRow.removeClass('add-row');
            $currentRow.find("#taskCode").prop('disabled', true).selectpicker('render');
            $currentRow.find("#uom").prop('disabled', true).selectpicker('render');
            $currentRow.find(".edit-modal").prop('disabled', true);
            $currentRow.find(".add-uprice").prop('disabled', true);
            if ($flag) {
                var $newRow = $($addRowSample.clone()).removeClass('sample hidden').appendTo($('#add-new-item-row', $taskTable));
                setTaskTypeahead();
                $newRow.find("#taskCode").focus();
            }
        } else if ($(this).hasClass('save')) {
            var taskId = $(this).parent().parent().data('id');
            var $currentRow = $thisRow;
            $currentRow
                    .removeClass('edit-row')
                    .find("#taskCode, input[name='defaultRate'],input[name='rate2'],input[name='rate3'],#uom").prop('readonly', true).end()
                    .find("button.delete").removeClass('disabled');
            $currentRow.find("button.edit-modal").attr('disabled', 'disabled');
            $currentRow.find("input[name='defaultRate']").change();
            $currentRow.find("input[name='rate2']").change();
            $currentRow.find("input[name='rate3']").change();
            taskRowEdit = false;
        }

        $(this, $currentRow).parent().find('.edit').removeClass('hidden');
        $(this, $currentRow).parent().find('.add').addClass('hidden');
        $(this, $currentRow).parent().find('.save').addClass('hidden');
        $(this, $currentRow).parent().parent().find('.delete').removeClass('disabled');
        
        //append task increment ID to task ID
        if ($(this).hasClass('save')) {
            var saveTaskID = $thisRow.data('id') + '_' + $thisRow.data('taskIncID');
            tasks[saveTaskID] = thisVals;
        } else {
            var increID = rowincrementID;
            if($thisRow.data('taskIncID')!== undefined){
                increID = $thisRow.data('taskIncID')
            }
            thisVals = getCurrentTaskData($thisRow);
            tasks[thisVals.taskID + '_' + increID] = thisVals;
            rowincrementID++;
        }
        clearProductModal();
    });

    $('#itemCode').on('change', function(e) {
        var $thisRow = $(this).parents('tr');
        if (typeof items[$(this).val()] != 'undefined' && $(this).val() != '') {
            p_notification(false, eb.getMessage("ERR_QUOT_PR_ALREADY_ADD"));
            $('.uomLi').remove();
            $('#uomAb').html('');
            $('#itemCode').val('');
            clearItemCode();
            $('#qty').val('');
            selectedProductUom = '';
            $('#addNewItemRow .uomqty').val('');
            $('#addNewItemRow .uom-select').remove();
            return false;
        } else if ($(this).val() > 0 && selectedProductID != $('#itemCode').val()) {

            $('.uomLi').remove();
            selectedProductID = $(this).val();
            eb.ajax({
                type: 'POST',
                url: BASE_URL + '/productAPI/get-location-product-details',
                data: {productID: selectedProductID, locationID: locationID},
                success: function(respond) {
                    var currentElem = new Array();
                    currentElem[selectedProductID] = respond.data;
                    locationProducts = $.extend(locationProducts, currentElem);

                    var itemCode = $('#itemCode').val();
                    var data = new Array();
                    $("#qty").val('').siblings('.uomqty,.uom-select').remove();
                    $("#qty").parent().addClass('input-group');
                    $("#qty").show();

                    data[0] = locationProducts[selectedProductID];
                    $('#qty').addUom(data[0].uom);
                    $('.uomqty').attr("id", "itemQuantity");
                    $('#qty').parent().addClass('input-group');

                    proId = data[0].pID;
                    locationProductID = data[0].lPID;
                    $('#itemCode').data('PT', data[0].pT);
                    $('#itemCode').data('PN', data[0].pN);
                    $('#itemCode').data('PC', data[0].pC);
                    $('.uomqty').focus();
                }
            });
        }
    });

    

    function clearItemCode() {
        $('#itemCode')
                .empty()
                .val('')
                .append($("<option></option>")
                        .attr("value", "")
                        .text('Select Item Code or Name'))
                .selectpicker('refresh')
                .trigger('change');
    }

    $('#add_item').on('click', function(e) {
        e.preventDefault();
        //if plus button is clicked then disabled the button
        //if button is clicked more than one, then it does not continue
        if ($(this).is(':disabled')) {
            return false;
        } else {
            $(this).attr('disabled', true);
            var $thisRow = $(this).parents('tr');
            $("#qty").siblings('.uomqty').change();
            if ($('#itemCode').val() == '' || $('#itemCode').val() == null) {
                $('#addNewItemRow #itemQuantity').val('');
                p_notification(false, eb.getMessage('ERR_INVENTADJUS_PRODCODE'));
                $(this).attr('disabled', false);
            } else {
                taskProductQty = $('#qty').val();
                uomQty = $(".adding-qty .uomqty", $thisRow).val();
                var productType = $('#itemCode').data('PT');
                selectedProductUom = $("#qty").siblings('.uom-select').children('button').find('.selected').data('uomID');
                if (typeof (selectedProductUom) == 'undefined' || selectedProductUom == '') {
                    p_notification(false, eb.getMessage('ERR_GRN_SELECT_UOM'));
                    $(this).attr('disabled', true);
                } else {
                    if (productType != 2 && ($('#qty').val() == '' || $('#qty').val() == 0)) {
                        p_notification(false, eb.getMessage('ERR_GRN_ENTER_QUANTITY'));
                        $(this).attr('disabled', false);
                        $('.uomqty').focus();
                    } else {
                        if (items[proId]) {
                            p_notification(false, eb.getMessage('ERR_INVENTADJUS_ITEMINSERT'));
                            $('#itemCode').val('');
                            $('#itemCode').selectpicker('render');
                            $("#qty").val('').siblings('.uomqty,.uom-select').remove();
                            $("#qty").show();
                            $('#uomAb').html('');
                        } else {
                            addNewProductRow();
                        }
                                    }
                    $(this).attr('disabled', false);
                }
            }
        }
    });

    function addNewProductRow() {

        var taskID = $('#addProductsModal').data('id');
        var incid = $('#addProductsModal').data('icid');
        var taskIncID = $('#addProductsModal').data('taskIncID');
        var $thisParentRow = getAddRow(taskIncID);
        var thisVals = getCurrentTaskData($thisParentRow);


        var uomConversionRate = $("#qty").siblings('.uom-select').find('.selected').data('uc');
        var proCodeAndName = $('#itemCode option:selected').html();
        var iC = $('#itemCode').val();
        var productType = $('#itemCode').data('PT');
        var iN = $('#itemCode').data('PN');
        var proCode = $('#itemCode').data('PC');
        var selectedUomAbbr = $("#qty").siblings('.uom-select').children('button').find('.selected').data('uomID');
        taskProductQty = $('#qty').val();
        var qty = taskProductQty;
       
        if (productType == 2 && taskProductQty == 0) {
            qty = 1;
        }
        var locationProductID = locationProducts[proId].lPID;
       
        var itemDuplicateCheck = false;
        if (!$.isEmptyObject(items)) {
            $.each(items, function(index, value) {
                if (value.locationPID == locationProductID) {
                    itemDuplicateCheck = true;
                }
            });   
        }
       
        if (itemDuplicateCheck) {
            p_notification(false, eb.getMessage("ERR_QUOT_PR_ALREADY_ADD"));
            return false;
        }

        var newTrID = 'tr_' + proId;
        var clonedRow = $($('#preSample').clone()).attr('id', newTrID).addClass('addedProducts');
        $("input[name='productCode']", clonedRow).val(proCodeAndName);
        $("input[name='qty']", clonedRow).val(taskProductQty).addUom(locationProducts[proId].uom);
        $(".uomqty", clonedRow).prop('disabled', true);
        $('#addNewUom', clonedRow).hide();
        clonedRow.removeClass('hidden');
        clonedRow.insertBefore('#preSample');
        items[proId] = new taskCardTaskProduct(locationProductID, proId, proCode, iN, uomQty, selectedProductUom);
        clearAddedNewRow();
        $('[data-id=itemCode]').trigger('click');
    }

    function taskCardTaskProduct(lPID, pID, pCode, pN, pQ, pU) {
        this.locationPID = lPID;
        this.pID = pID;
        this.pCode = pCode;
        this.pName = pN;
        this.pQuantity = pQ;
        this.pUOM = pU;
    }

    function clearAddedNewRow() {
        $('.uomLi').remove();
        $('#uomAb').html('');
        $('#itemCode').val('');
        clearItemCode();
        $('#qty').val('');
        selectedProductUom = '';
        $('#addNewItemRow .uomqty').val('');
        $('#addNewItemRow .uom-select').remove();
    }

    function clearProductModal()
    {
        selectedProductID = '';
        $("#addProductsModal #taskCardTaskProductTable tbody .addedProducts").remove();
        items = {};
    }


    $taskTable.on('click', 'button.edit-modal', function(e) {
        var $thisRow = $(this).parents('tr');
        clearProductModal();
        var taskID = $(this).parents('tr').data('id');
        if (taskID == undefined) {
            p_notification(false, eb.getMessage('PLS_SELECT_AT_LEAST_ONE_TASK'));
            return false;
        }
        var taskIncID = taskID + "_" + $(this).parents('tr').data('taskIncID');
        var rowincID = $(this).parents('tr').data('icid');

        $('#addProductsModal').data('icid', rowincID);
        $('#addProductsModal').data('taskIncID', rowincID);

        if (!$.isEmptyObject(taskProducts[taskIncID])) {
            $('#addInvoiceProductsModal').data('id', $(this).parents('tr').data('id'));
        }

        if (!$.isEmptyObject(taskProducts[taskIncID])) {
            items = taskProducts[getCurrentTaskData($thisRow)['taskIncID']];
            $.each(items, function(index, value) {
                var newTrID = 'tr_' + value.pID;
                var clonedRow = $($('#preSample').clone()).attr('id', newTrID).addClass('addedProducts');
                $("input[name='productCode']", clonedRow).val(value.pName);
                $("input[name='qty']", clonedRow).val(value.pQuantity);
                $("#addNewUom", clonedRow).html(uomList[value.pUOM]).prop('disabled', true);
                $(".uomqty", clonedRow).prop('disabled', true);
                clonedRow.removeClass('hidden');
                clonedRow.insertBefore('#preSample');
                clearAddedNewRow();
            });

        }
        $('#addProductsModal').modal('show');
    });

    $('#addTCProducts').on('click', function(e) {
        e.preventDefault();
        // validate products before closing modal
        if (!taskProductModalValidate(e)) {
            e.stopPropagation();
            return false;
        } else {
            $('#addProductsModal').modal('hide');
        }
    });

    function getCurrentTaskData($thisRow) {
        var tid = rowincrementID;
        if ($thisRow.data('taskIncID') !== undefined) {
            tid = $thisRow.data('taskIncID');
        }
        var thisVals = {
            taskID: $thisRow.data('id'),
            taskIncID: $thisRow.data('id') + '_' + tid,
            defaultRate: $("input[name='defaultRate']", $thisRow).val(),
            rate2: $("input[name='rate2']", $thisRow).val(),
            rate3: $("input[name='rate3']", $thisRow).val(),
            UomId: $("#uom", $thisRow).data('uomIID')
        };
        return thisVals;
    }

    function taskProductModalValidate(e) {

        var taskID = $('#addProductsModal').data('id');
        var incid = $('#addProductsModal').data('icid');
        var taskIncID = $('#addProductsModal').data('taskIncID');
        $('#addProductsModal').data('id', '');
        var $thisParentRow = getAddRow(taskIncID);
        var thisVals = getCurrentTaskData($thisParentRow);
        var $productTable = $("#addProductsModal #taskCardTaskProductTable tbody");
        var subProducts = [];

        if ($.isEmptyObject(items)) {
            if (taskProducts[thisVals.taskIncID] != undefined) {
                delete taskProducts[thisVals.taskIncID];
            }
            return true;
        } else {
            taskProducts[thisVals.taskIncID] = items;
            return true;
        }

    }

    $("#addProductsModal #taskCardTaskProductTable").on('click', '.deleteTaskCardProducts', function(e) {
        e.preventDefault();
        var deletePTrID = $(this).closest('tr').attr('id');
        var deletePID = deletePTrID.split('tr_')[1].trim();

        bootbox.confirm('Are you sure you want to remove this product ?', function(result) {
            if (result == true) {
                delete items[deletePID];
                selectedProductID = "";
                $('#' + deletePTrID).remove();
            }
        });
    });


    $('#saveTaskCard').on('click',function(e){
        e.preventDefault();

        var tr = $('tr', $('tbody#add-new-item-row')).not('.sample');
        var btnSaveFlag = $('button.save', tr).is(':not(.hidden)');
        if(btnSaveFlag) {
            p_notification(false, eb.getMessage('ERR_TASKS_NOT_SAVE'));
            return false;
        }

        var formData = {
            taskCardCode: $('#taskCardCode').val(),
            taskCardName: $('#taskCardName').val(),
            taskCardTasks: tasks,
            taskCardProducts: taskProducts,
            taskCardTaskPriceList: priceListForCommonData

        };



        var url = typeof(edittaskCardID) != 'undefined' ? BASE_URL + '/task_card_api/updateTaskCard/' + $('#edittaskCardID').val() : BASE_URL + '/task_card_api/saveTaskCard';

        if (validateFormData(formData)) {
            eb.ajax({
                url: url,
                method: 'post',
                data: formData,
                dataType: 'json',
                success: function(data) {
                    p_notification(data.status, data.msg);
                    if (data.status) {
                        window.setTimeout(function() {
                            window.location.assign(BASE_URL + "/task_card/list")
                        }, 1000);
                    } else {
                        if (data.data == "JTCERR") {
                            $('#taskCode').focus();
                        }
                    }
                }
            });
        }
    });

    $('#reset').on('click', function() {
        window.setTimeout(function() {
            location.reload();
        }, 200);
    });


    function validateFormData(formData)
    {
        if (formData.taskCardCode == "") {
            p_notification(false, eb.getMessage('ERR_TSKCRD_CODE_CNT_BE_NULL'));
            $('#taskCardCode').focus();
            return false;
        } else if (formData.taskCardName == '') {
            p_notification(false, eb.getMessage('ERR_TSKCRD_NAME_CNT_BE_NULL'));
            $('#taskCardName').focus();
            return false;
        } else if ($.isEmptyObject(formData.taskCardTasks)) {
            p_notification(false, eb.getMessage('ERR_TSKCRD_TASKS_CNT_BE_NULL'));
            $('#taskCode').focus();
            return false;
        }
        return true;
    }


    $taskTable.on('click', 'button.delete', function(e) {
        e.preventDefault();
        if (!$(this).hasClass('disabled')) {
            var $thisRow = $(this).parents('tr');

            var taskID = $thisRow.data('id') + '_' + $thisRow.data('taskIncID');
            if (taskProducts[taskID] != undefined) {
                delete taskProducts[taskID];
            } 

            delete tasks[taskID];
            $thisRow.remove();
        }
    });

    $taskTable.on('click', 'button.edit', function(e) {
        taskRowEdit = true;
        var $thisRow = $(this).parents('tr');

        $thisRow.addClass('edit-row')
                .find("input[name='defaultRate'],input[name='rate2'],input[name='rate3']").prop('readonly', false).end()
                .find("button.delete").addClass('disabled');
        $thisRow.find("button.edit-modal").removeAttr('disabled');
        $thisRow.find("button.add-uprice").removeAttr('disabled');
        $thisRow.find("input[name='defaultRate']").change();
        $thisRow.find("input[name='rate2']").change();
        $thisRow.find("input[name='rate3']").change();
        $thisRow.find("#uom").prop('disabled', false).selectpicker('render');
        $(this, $thisRow).parent().find('.edit').addClass('hidden');
        $(this, $thisRow).parent().find('.save').removeClass('hidden');
    });

    if ( typeof(edittaskCardID) != 'undefined') {
        eb.ajax({
            url: BASE_URL + '/task_card_api/getTaskCardTaskProduct',
            method: 'post',
            data: {'edittaskCardID' : edittaskCardID},
            dataType: 'json',
            success: function(data) {
                if (data.status) {
                    loadTasks(data.data);
                } else {
                    p_notification(data.status, data.msg);
                }
            }
        });
    }

    function loadTasks(data) {
        if (!$.isEmptyObject(data)) {
            $.each(data, function(index, value) {
                var $thisRow = $('#add-new-item-row').find('tr.add-row').not(".hidden");

                $thisRow.find('#taskCode').append($('<option></option>').val(value.taskID).html(value.taskCode + '-' + value.taskName));
                $thisRow.find('#taskCode').selectpicker('val', value.taskID);
                $thisRow.find('#taskCode').selectpicker('render');
                selectTasksForTaskCard(value.taskID);

                $("input[name='defaultRate']", $thisRow).val(value.taskCardTaskRate1);
                $("input[name='rate2']", $thisRow).val(value.taskCardTaskRate2);
                $("input[name='rate3']", $thisRow).val(value.taskCardTaskRate3);

                $thisRow.find('#uom').selectpicker('val', value.uomID);
                $thisRow.find('#uom').selectpicker('render');
                $("#uom", $thisRow).data('uomIID', value.uomID);

                var $taskIncID = getCurrentTaskData($thisRow)['taskIncID'];
                if (!$.isEmptyObject(value.productList)) {
                    $.each(value.productList, function(index, value) {
                        items[value.productID] = new taskCardTaskProduct(
                            value.locationProductID, value.productID, value.productCode, value.taskCardTaskProductName,
                            value.taskCardTaskProductQty, value.taskCardTaskProductUomID);
                    });
                }

                if (!$.isEmptyObject(value.priceList)) {
                    var tmpRates = [];
                    $.each(value.priceList, function(index, value) {
                        tmpRates.push(value.rates);
                    });
                    priceListForCommonData[value.taskID] = tmpRates;
                }

                $('button.add', $thisRow).trigger("click");
                taskProducts[$taskIncID] = items;
                items = {};
            });
        }
    }

    $taskTable.on('focusout', '#defaultRate,#rate2,#rate3', function() {
        var $thisRow = $(this).parents('tr');

        if (isNaN($thisRow.find('#defaultRate').val()) || parseFloat($thisRow.find('#defaultRate').val()) < 0) {
            $thisRow.find('#defaultRate').val('0.00');
        }

        if (isNaN($thisRow.find('#rate2').val()) || parseFloat($thisRow.find('#rate2').val()) < 0) {
            $thisRow.find('#rate2').val('0.00');
        }

        if (isNaN($thisRow.find('#rate3').val()) || parseFloat($thisRow.find('#rate3').val()) < 0) {
            $thisRow.find('#rate3').val('0.00');
        }

    });

    $taskTable.on('click', 'button.add-uprice', function(e) {
        e.preventDefault();
        var $thisRow = $(this).parents('tr');
        var taskID = $(this).parents('tr').data('id');
        $('#task-pricing-table .newLine').remove();
        if (taskID == undefined) {
            p_notification(false, eb.getMessage('PLS_SELECT_AT_LEAST_ONE_TASK'));
            return false;
        }
        if (priceListForCommonData[taskID] != undefined) {
            $.each(priceListForCommonData[taskID], function(key,value){
                var clonedRow = $('#task-pricing-table .sample-data-set').clone();
                clonedRow.find('.uni-price').val(value);
                clonedRow.find('.add-value').addClass('hidden');
                clonedRow.find('.edit-value').removeClass('hidden');
                clonedRow.find('.delete-value').removeClass('hidden');
                clonedRow.appendTo('#task-pricing-table .item-attr-body').removeClass('hidden sample-data-set').addClass('newLine added');
            });
        }
        
        var clonedNewRow = $('#task-pricing-table .sample-data-set').clone();
        clonedNewRow.appendTo('#task-pricing-table .item-attr-body').removeClass('hidden sample-data-set').addClass('newLine');
        $('#task-pricing-table').attr('data-id', taskID);
        $('#addMultiplePriceModal').modal('show');
    });

    $('#task-pricing-table').on('click', '.add-value', function(e) {
        e.preventDefault();
        var price = $(this).parents('tr').find('.uni-price').val();
        if (isNaN(price) || parseFloat(price) < 0 || price == '') {
            p_notification(false, eb.getMessage('PLS_ADD_VALUE'));
            return false;   
        } else {
            $(this).parent().find('.add-value').addClass('hidden');
            $(this).parents('tr').find('.uni-price').attr('disabled', true);
            $(this).parent().find('.edit-value').removeClass('hidden');
            $(this).parent().find('.delete-value').removeClass('hidden');
            var clonedline = $('#task-pricing-table .sample-data-set').clone();
            clonedline.appendTo('#task-pricing-table .item-attr-body').removeClass('hidden sample-data-set').addClass('newLine');
            $(this).parents('tr').addClass('added');

            
        }
    });

    $('#task-pricing-table').on('click', '.edit-value', function(e) {
        e.preventDefault();
        $(this).parents('tr').find('.uni-price').attr('disabled', false);
        $(this).parents('tr').removeClass('added');
        $(this).parent().find('.save-value').removeClass('hidden');
        $(this).parent().find('.edit-value').addClass('hidden');
        $(this).parent().find('.delete-value').addClass('hidden');
    });

    $('#task-pricing-table').on('click', '.save-value', function(e) {
        e.preventDefault();
        var price = $(this).parents('tr').find('.uni-price').val();
        if (isNaN(price) || parseFloat(price) < 0 || price == '') {
            p_notification(false, eb.getMessage('PLS_ADD_VALUE'));
            return false;   
        } else {
            $(this).parents('tr').find('.uni-price').attr('disabled', true);
            $(this).parents('tr').addClass('added');
            $(this).parent().find('.save-value').addClass('hidden');
            $(this).parent().find('.edit-value').removeClass('hidden');
            $(this).parent().find('.delete-value').removeClass('hidden');
            $(this).parent().find('.add-value').addClass('hidden');
        }
    });

    $('#task-pricing-table').on('click', '.delete-value', function(e) {
        e.preventDefault();
        $(this).parents('tr').remove();
    });

    $('#task-price-modal').on('click', '.save-task-price-modal', function(e) {
        e.preventDefault();
        var tempArray = [];
        $.each($(this).parents().find('.item-attr-body tr.added'), function(key, value) {
                var value = $(this).find('.uni-price').val();
                tempArray.push(value);
        });
        var taskID = $('#task-pricing-table').attr('data-id');
        if (tempArray == null) {
            priceListForCommonData[taskID] = undefined;
        } else {
            priceListForCommonData[taskID] = tempArray;
        }
        $('#addMultiplePriceModal').modal('hide');

    });


});
