$(document).ready(function() {

    var projectID = null;
    var jobText = null;
    var jobID = null;
    var activityID = '';
    var isChanged = false;
    var activityCode = '';
    var jobListSearchParams = null;
    var params = null;
    $('#jobSearchDropDown').selectpicker('show');
    $('#projectRelatedjobSearchDropDown').selectpicker('hide');

    var url = document.URL;
    var splitUrl = url.split("/");
    if (splitUrl[4] == 'list' && $('#paramVal').val() == 'false') {
        localStorage.removeItem('job_list_search_params');
    }

    if (splitUrl[4] == 'list' && localStorage.getItem("job_list_search_params") != null) {
        jobListSearchParams = localStorage.getItem('job_list_search_params');
        params = JSON.parse(jobListSearchParams);

        if (params.backAction == true) {
            jobText = params.jobText;
            projectID = (params.projectID == "") ? null: params.projectID;
            jobID = (params.jobID == "") ? null : params.jobID;
            getProjectDetails(projectID);
            searchJobList();

            if (projectID != 0 && projectID) {
                $('#jobSearchDropDown').selectpicker('hide');
                $('#projectRelatedjobSearchDropDown').selectpicker('show');
                loadprojectRelatedJobsToPicker(projectID);  
            } else {
                $('#jobSearchDropDown').selectpicker('show');
                $('#projectRelatedjobSearchDropDown').selectpicker('hide');
                setjobSelectPickerValue();
                
            }
        }
    }

    if (splitUrl[4] == 'view-requests') {
        if (localStorage.getItem("job_list_search_params") != null) {
            $('#searchBackBtn').removeClass('hidden');
            $('#normalBackBtn').addClass('hidden');
            var jobListSearchParams = localStorage.getItem('job_list_search_params');
            var params = JSON.parse(jobListSearchParams);
            params.backAction = true;
            localStorage.setItem("job_list_search_params", JSON.stringify(params));
        } else {
            $('#searchBackBtn').addClass('hidden');
            $('#normalBackBtn').removeClass('hidden');
        }
    }

    $('#normalBackBtn').on('click', function(){
         window.history.go(-1);
    });

    loadDropDownFromDatabase('/api/project/search-projects-for-dropdown', '', 0, '#projectSearchDropDown', '', '', false);
    $('#projectSearchDropDown').selectpicker('refresh');

    loadDropDownFromDatabase('/job-api/search-jobs-for-dropdown', '', 0, '#jobSearchDropDown', '', '', false);
    $('#jobSearchDropDown').selectpicker('refresh');

    $('#jobSearchDropDown').change(function(){
        jobID = $('#jobSearchDropDown').val();
        jobText = $('select[name=jobSearch] option[value='+jobID+']').text();
    });

    $('#projectRelatedjobSearchDropDown').change(function(){
        jobID = $('#projectRelatedjobSearchDropDown').val();
        jobText = $('select[name=projectjobSearch] option[value='+jobID+']').text();
    });

    $('#projectSearchDropDown').change(function(){

        projectID = $('#projectSearchDropDown').val();

        if (projectID != 0 && projectID) {
            $('#jobSearchDropDown').selectpicker('hide');
            $('#projectRelatedjobSearchDropDown').selectpicker('show');
            loadprojectRelatedJobsToPicker($('#projectSearchDropDown').val());

            isChanged = true;
        } else {
            $('#jobSearchDropDown').selectpicker('show');
            $('#projectRelatedjobSearchDropDown').selectpicker('hide');
            isChanged = false;
        }
        $('#backAction').val('false')
    });

    $('#searchProgressJob').on('click', function(){
        searchJobList();
    });

    function validateSearchOptions () {

        projectID = (projectID == '0' || projectID == null) ? null : projectID;
        jobID = (jobID == '0' || jobID == null) ? null : jobID; 
        if (projectID == null  && jobID == null) {
            p_notification(false, eb.getMessage('ERR_PROGRESS_PROJ_AND_JOB_SEARC_FIELD_EMPTY'));
            return false;
            
        }
        return true;
    }

    $('#progress-job-list, #default-job-list').on('click','.job-supervisor-view',function (e){ 
        $("#viewJobSupervisorModal #jobSupervisorTable tbody .addedSupervisors").remove();
        var tempJobID = $(this).closest('tr').data('job-id');  
        eb.ajax({
            url: BASE_URL + '/job-api/getJobSupervisorsByJobID',
            method: 'post',
            data: {jobID:tempJobID},
            dataType: 'json',
            success: function(respond) {
                if (respond.status) {
                    $.each(respond.data, function(index, val) {
                        var newTrID = 'tr_' + val.jobSupervisorID;
                        var clonedRow = $($('#preSample').clone()).attr('id', newTrID).addClass('addedSupervisors');
                        $("#supervisorName", clonedRow).text(val.employeeFirstName+" "+val.employeeSecondName);
                        clonedRow.removeClass('hidden');
                        clonedRow.insertBefore('#preSample');
                    });
                    $('#viewJobSupervisorModal').modal('show');
                }
            }
        });  
    });

    function loadprojectRelatedJobsToPicker(projectId) {
        eb.ajax({
            url: BASE_URL + '/job-api/load-jobs-for-dropdown',
            method: 'post',
            data: {
                projectId: projectId,
                progressFlag: true
            },
            dataType: 'json',
            success: function(data) {
                if(data.status) {
                    setProjectRelateJobSelectPicker(data.data.jobList);
                } else {
                    p_notification(data.status, data.msg);
                }
            }
        });
    }

    function setProjectRelateJobSelectPicker(data) {
        if (params != null && params.backAction == true) {
            $('#projectRelatedjobSearchDropDown').html("<option value='"+jobID+"'>" + params.jobText + "</option>");
        } else {
            $('#projectRelatedjobSearchDropDown').html("<option value=''>" + "Select a Job" + "</option>");
        }
        $.each(data, function(index, value) {
            if (params != null && params.backAction == true) {
                if (value.value != jobID) {
                    $('#projectRelatedjobSearchDropDown').append("<option value='" + value.value + "'>" + value.text + "</option>");
                }
            } else {
                $('#projectRelatedjobSearchDropDown').append("<option value='" + value.value + "'>" + value.text + "</option>");
            }
        });
        $('#projectRelatedjobSearchDropDown').selectpicker('refresh');

        if (params != null) {
            params.backAction = false;
            localStorage.setItem("job_list_search_params", JSON.stringify(params));    
        }
        
    }

    $('#searchCancelProgressJob').on('click', function() {
        isChanged = false;
        clearProjectDropdown();
        clearProjectRelatedJobDropdown();
        $(".jobsTable").remove();
        $("#default-job-list").removeClass('hidden');
        clearJobDropdown();
        
    });

    function clearProjectDropdown () {
        $('#projectSearchDropDown')
               .empty()
               .val('')
               .append($("<option></option>")
                       .attr("value", "0")
                       .text('Select a Project'))
               .selectpicker('refresh')
               .trigger('change');
    }

    function clearProjectRelatedJobDropdown () {
        $('#projectRelatedjobSearchDropDown')
               .empty()
               .val('')
               .append($("<option></option>")
                       .attr("value", "0")
                       .text('Select a Job'))
               .selectpicker('refresh')
               .trigger('change');
    }

    function clearJobDropdown () {
        $('#jobSearchDropDown')
               .empty()
               .val('')
               .append($("<option></option>")
                       .attr("value", "0")
                       .text('Select a Job'))
               .selectpicker('refresh')
               .trigger('change');
    }


    $('#progress-job-list, #default-job-list').on('click','.viewContractors',function (e){
        $("#viewJobContractorModal #jobContractorTable tbody .addedcontractors").remove();
        var tempJobID = $(this).closest('tr').data('job-id');
        eb.ajax({
            url: BASE_URL + '/job-api/getJobContractorsByJobID',
            method: 'post',
            data: {jobID:tempJobID},
            dataType: 'json',
            success: function(respond) {
                if (respond.status) {
                    $.each(respond.data, function(index, val) {
                        var newTrID = 'tr_' + val.jobSupervisorID;
                        var clonedRow = $($('#contractorRow').clone()).attr('id', newTrID).addClass('addedcontractors');
                        $("#contractorName", clonedRow).text(val.contractorFirstName+" "+val.contractorSecondName);
                        clonedRow.removeClass('hidden');
                        clonedRow.insertBefore('#contractorRow');
                    });
                    $('#viewJobContractorModal').modal('show');
                }
            }
        });  
    });

    $('#viewSupervisors').on('click', function(){
        $("#viewJobSupervisorModal #jobSupervisorTable tbody .addedSupervisors").remove();
        var tempJobID = $('#jobId').val();
        eb.ajax({
            url: BASE_URL + '/job-api/getJobSupervisorsByJobID',
            method: 'post',
            data: {jobID:tempJobID},
            dataType: 'json',
            success: function(respond) {
                if (respond.status) { 
                    $.each(respond.data, function(index, val) {
                        var newTrID = 'tr_' + val.jobSupervisorID;
                        var clonedRow = $($('#preSample').clone()).attr('id', newTrID).addClass('addedSupervisors');
                        $("#supervisorName", clonedRow).text(val.employeeFirstName+" "+val.employeeSecondName);
                        clonedRow.removeClass('hidden');
                        clonedRow.insertBefore('#preSample');
                    });
                    $('#viewJobSupervisorModal').modal('show');
                }
            }
        });  
    });

    $('#viewContractors').on('click', function(){
        $("#viewJobContractorModal #jobContractorTable tbody .addedcontractors").remove();
        var tempJobID = $('#jobId').val();
        eb.ajax({
            url: BASE_URL + '/job-api/getJobContractorsByJobID',
            method: 'post',
            data: {jobID:tempJobID},
            dataType: 'json',
            success: function(respond) {
                if (respond.status) {
                    $.each(respond.data, function(index, val) {
                        var newTrID = 'tr_' + val.jobSupervisorID;
                        var clonedRow = $($('#contractorRow').clone()).attr('id', newTrID).addClass('addedcontractors');
                        $("#contractorName", clonedRow).text(val.contractorFirstName+" "+val.contractorSecondName);
                        clonedRow.removeClass('hidden');
                        clonedRow.insertBefore('#contractorRow');
                    });
                    $('#viewJobContractorModal').modal('show');
                }
            }
        });  
    });


    $('#requestTable').on('click', '.viewProducts', function(){
        $("#viewRequestProductModal #requestProductTable tbody .reqquestedProducts").remove();
        var materialRequestId = $(this).closest('tr').data('material-request-id');
        eb.ajax({
            url: BASE_URL + '/material-requisition-api/get-requested-products',
            method: 'post',
            data: {requestId:materialRequestId},
            dataType: 'json',
            success: function(respond) {
                if (respond.status) {
                    $.each(respond.data, function(index, val) {
                        var newTrID = 'tr_' + val.materialRequestJobProductId;
                        var clonedRow = $($('#productRow').clone()).attr('id', newTrID).addClass('reqquestedProducts');
                        $("#itemCode", clonedRow).text(val.productCode);
                        $("#itemName", clonedRow).text(val.productName);
                        $("#requestQty", clonedRow).text(val.requestedQuantity);
                        $("#issuedQty", clonedRow).text(val.jobProductIssuedQty);
                        clonedRow.removeClass('hidden');
                        clonedRow.insertBefore('#productRow');
                    });
                    $('#viewRequestProductModal').modal('show');
                }
            }
        })

    });

    $('#btnBack').on('click', function(){
         window.history.go(-1);
    });

    function searchJobList() {
        if (validateSearchOptions()) {

            var searchParams = {
                projectID : projectID,
                jobID : (jobID),
                jobText: (jobText != null) ? jobText: 'Select a job',
                backAction: true
            }

            localStorage.setItem("job_list_search_params", JSON.stringify(searchParams));
            eb.ajax({
                type: 'POST',
                url: BASE_URL + '/progress-api/search-by-project-and-job',
                data: {
                    projectID : projectID,
                    jobID : jobID

                },
                success: function(data) {
                    if (data.status == true) {
                        if (data.status) {
                            $('#default-job-list').addClass('hidden');
                            $("#progress-job-list").html(data.html);
                        }
                    }            
                }
            });
        }
    }

    function getProjectDetails(projectID) {
        eb.ajax({
            type: 'POST',
            url: BASE_URL + '/api/project/getProjectDetails',
            data: {
                projectID: projectID
            },
            success: function(respond) {
                if (respond.status) {
                    $('#projectSearchDropDown').empty();
                    $('#projectSearchDropDown').
                    append($("<option></option>").
                            attr("value", "0").
                            text('Select a Project'));
                    $('#projectSearchDropDown').
                    append($("<option selected='selected'></option>").
                            attr("value", projectID).
                            text(respond.data.projectCode + '-' + respond.data.projectName));
                    $('#projectSearchDropDown').selectpicker('refresh');
                }
            }
        });
    }

    function setjobSelectPickerValue () {
        $('#jobSearchDropDown').html("<option value='"+jobID+"'>" + params.jobText + "</option>");
        if (params != null) {
            params.backAction = false;
            localStorage.setItem("job_list_search_params", JSON.stringify(params));    
        }
    }

});
