$(document).ready(function() {

    var employeeId   = null;
    var employeeFirstName = null;
    var employeeSecondName = null;
    var employeeAddress = null;
    var employeeTP = null;
    var employeeEmail = null;
    var employeeCode = null;
    var employeeStatus = null;
    var employeeDepartmentID = null;
    var employeeIDNo = null;
    

    
    $('#btnAdd').on('click',function(){
        employeeCode = $('#employeeCode').val();
        employeeFirstName = $('#employeeFirstName').val();
        employeeSecondName = $('#employeeLastName').val();
        employeeAddress = $('#employeeAddress').val();
        employeeTP = $('#employeeMobileNumber').val();
        employeeEmail = $('#employeeEmail').val();
        employeeIDNo = $('#employeeIdentityReference').val();
        employeeDepartmentID = $('#employeeDepartment').val();
        employeeDesignations = $('#employeeDesignation').val();


        var formData = {
            employeeCode: employeeCode,
            employeeFirstName: employeeFirstName,
            employeeSecondName: employeeSecondName, 
            employeeAddress: employeeAddress, 
            employeeTP: employeeTP, 
            employeeEmail: employeeEmail,
            employeeIDNo: employeeIDNo,
            employeeDepartmentID: employeeDepartmentID,
            employeeDesignations: employeeDesignations
        }

        if (validateFormData(formData)) { 
            
            if (employeeTP.trim()) {
                var phoneno = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;  
                if (!(employeeTP.match(phoneno)))  { 
                    p_notification(false, eb.getMessage('ERR_TP_NUM'));
                    return;
                }
            }

            if (employeeEmail.trim()) {
                var emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

                if (!(emailRegex.test(employeeEmail))) {
                    p_notification(false, eb.getMessage('ERR_EMAIL_NOT_VALID'));
                    return;
                }
            }

            eb.ajax({
                type: 'POST',
                url: BASE_URL + '/employee-api/createEmployee',
                data: {
                    employeeCode : employeeCode,
                    employeeFirstName : employeeFirstName,
                    employeeSecondName : employeeSecondName,
                    employeeAddress : employeeAddress,
                    employeeTP : employeeTP,
                    employeeEmail : employeeEmail,
                    employeeIDNo : employeeIDNo,
                    employeeDepartmentID : employeeDepartmentID,
                    employeeDesignations : employeeDesignations
                },
                success: function(respond) {
                    p_notification(respond.status, respond.msg);                
                    if(respond.status){
                        $('#employeeCode').val('');
                        $('#employeeFirstName').val('');
                        $('#employeeLastName').val('');
                        $('#employeeAddress').val('');
                        $('#employeeMobileNumber').val('');
                        $('#employeeEmail').val('');
                        $('#employeeIdentityReference').val('');
                        $('#employeeDepartment').val('');
                        $('#employeeDesignation').val('');
                        window.setTimeout(function() {
                                location.reload();
                        }, 1000);                    
                    }                
                }
            });
        }
    });

    //for cancel btn
    $('#btnCancel').on('click',function (){
        $('input[type=text]').val('');
        $('input[type=email]').val('');  
        $('select[name=employeeDept]').val('');
        $('select[name=employeeDesig]').val('');
        $('.selectpicker').selectpicker('refresh');      
        employeeRegisterView();        
    });

    $('#employee-list').on('click','.employee-action',function (e){    
        var action = $(this).data('action');
        employeeId   = $(this).closest('tr').data('employee-id');
        employeeFirstName = $(this).closest('tr').data('employee-first-name');
        employeeSecondName = $(this).closest('tr').data('employee-last-name');
        employeeAddress = $(this).closest('tr').data('employee-address');
        employeeTP = $(this).closest('tr').data('employee-employee-tp');
        employeeEmail = $(this).closest('tr').data('employee-email');
        employeeCode = $(this).closest('tr').data('employee-code');
        entityID = $(this).closest('tr').data('entity-id');
        employeeDepartmentID = $(this).closest('tr').data('employee-department-id');
        employeeIDNo = $(this).closest('tr').data('employee-id-no');
        status = $(this).closest('tr').data('employee-status');

        switch(action) {
            case 'edit':
                e.preventDefault();
                employeeUpdateView();

                //set selected department details
                $('#employeeCode').val(employeeCode);
                $('#employeeFirstName').val(employeeFirstName);
                $('#employeeLastName').val(employeeSecondName);
                $('#employeeAddress').val(employeeAddress);
                $('#employeeMobileNumber').val(employeeTP);
                $('#employeeEmail').val(employeeEmail);
                $('#employeeIdentityReference').val(employeeIDNo);
                getEmployeeDesignations(employeeId);
                $('select[name=employeeDept]').val(employeeDepartmentID);
                $('.selectpicker').selectpicker('refresh');
                break;

            case 'status':
                var msg;
                var status;
                departmentId   = $(this).closest('tr').data('department-id');
                var currentDiv = $(this).contents();
                var flag = true;
                if ($(this).children().hasClass('fa-check-square-o')) {
                    msg = 'Are you sure you want to Deactivate this employee';
                    status = '0';
                }
                else if ($(this).children().hasClass('fa-square-o')) {
                    msg = 'Are you sure you want to Activate this employee';
                    status = '1';
                }
                activeInactive(employeeId, status, currentDiv, msg, flag);
                break;
            case 'view':
                getEmployeeDesignations(employeeId, 'loadModal');
                break;
            case 'delete':
                deleteEmployee(employeeId);
                break;
            default:
                console.error('Invalid action.');
                break;
        }
    });

    //this function for switch to employee register view
    function employeeRegisterView(){
       $('.updateTitle').addClass('hidden');
       $('.addTitle').removeClass('hidden');
       $('.updateDiv').addClass('hidden');
       $('.addDiv').removeClass('hidden');
    }

    //this function for switch to employee update view
    function employeeUpdateView(){
       $('.addTitle').addClass('hidden');
       $('.updateTitle').removeClass('hidden');
       $('.addDiv').addClass('hidden');
       $('.updateDiv').removeClass('hidden');
    }

    /**
     *
     *use to change active state
     */
    function activeInactive(type, status, currentDiv, msg, flag) {
        bootbox.confirm(msg, function($result) {
            if ($result == true) {
                eb.ajax({
                    url: BASE_URL + '/employee-api/changeStatusID',
                    method: 'post',
                    dataType: 'json',
                    data: {
                        'employeeID': type,
                        'status': status,
                    },
                    success: function(data) {
                        if (data.status) {
                            p_notification(data.status, data.msg);
                            if (currentDiv.hasClass('fa-check-square-o') && flag) {
                                currentDiv.removeClass('fa-check-square-o');
                                currentDiv.addClass('fa-square-o');
                            }
                            else {
                                currentDiv.removeClass('fa-square-o');
                                currentDiv.addClass('fa-check-square-o');
                            }
                        } else {
                            p_notification(data.status, data.msg);
                        }
                    }
                });
            }
        });
    }

    $('#btnUpdate').on('click',function(){
        employeeCode = $('#employeeCode').val();
        employeeFirstName = $('#employeeFirstName').val();
        employeeSecondName = $('#employeeLastName').val();
        employeeAddress = $('#employeeAddress').val();
        employeeTP = $('#employeeMobileNumber').val();
        employeeEmail = $('#employeeEmail').val();
        employeeIDNo = $('#employeeIdentityReference').val();
        employeeDepartmentID = $('#employeeDepartment').val();
        employeeDesignations = $('#employeeDesignation').val();

        var formData = {
            employeeCode: employeeCode,
            employeeFirstName: employeeFirstName,
            employeeSecondName: employeeSecondName, 
            employeeAddress: employeeAddress, 
            employeeTP: employeeTP, 
            employeeEmail: employeeEmail,
            employeeIDNo: employeeIDNo,
            employeeDepartmentID: employeeDepartmentID,
            employeeDesignations: employeeDesignations
        }


        
        if(validateFormData(formData)){
            
            if (employeeTP.trim()) {
                var phoneno = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;  
                if (!(employeeTP.match(phoneno)))  { 
                    p_notification(false, eb.getMessage('ERR_TP_NUM'));
                    return;
                }
            }

            if (employeeEmail.trim()) {
                var emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

                if (!(emailRegex.test(employeeEmail))) {
                    p_notification(false, eb.getMessage('ERR_EMAIL_NOT_VALID'));
                    return;
                }
            }

            eb.ajax({
                type: 'POST',
                url: BASE_URL + '/employee-api/update',
                data: {
                    employeeCode : employeeCode,
                    employeeFirstName : employeeFirstName,
                    employeeSecondName : employeeSecondName,
                    employeeAddress : employeeAddress,
                    employeeTP : employeeTP,
                    employeeEmail : employeeEmail,
                    employeeIDNo : employeeIDNo,
                    departmentID : employeeDepartmentID,
                    employeeDesignations : employeeDesignations,
                    employeeID : employeeId,
                    entityID: entityID
                },
                success: function(respond) {
                    p_notification(respond.status, respond.msg);                
                    if(respond.status){
                        $('#departmentCode').val('');
                        $('#departmentName').val('');
                        window.setTimeout(function() {
                                location.reload();
                        }, 1000);                  
                    }                
                }
            });
        } 
    });

    //for search
    $('#searchEmp').on('click', function() {
        var key = $('#employeeSearch').val();
        var searchDepartmentID = ($('#employeeDepartmentSearch').val() != 0 && $('#employeeDepartmentSearch').val() != null) ? $('#employeeDepartmentSearch').val() : null;
        var searchDesignationID = ($('#employeeDesignationSearch').val() != 0 && $('#employeeDesignationSearch').val() != null) ? $('#employeeDesignationSearch').val() : null;
        getSearchResults(key, searchDepartmentID, searchDesignationID);
        
    });

    $( "#employeeDepartmentSearch, #employeeDesignationSearch").change(function() {
        var key = $('#employeeSearch').val();
        var searchDepartmentID = ($('#employeeDepartmentSearch').val() != 0 && $('#employeeDepartmentSearch').val() != null) ? $('#employeeDepartmentSearch').val() : null;
        var searchDesignationID = ($('#employeeDesignationSearch').val() != 0 && $('#employeeDesignationSearch').val() != null) ? $('#employeeDesignationSearch').val() : null;
        getSearchResults(key, searchDepartmentID, searchDesignationID);
    });

    //for rest btn
    $('#btnReset').on('click',function (){
        $('input[type=text]').val('');
        $('input[type=email]').val('');  
        $('select[name=employeeDept]').val('');
        $('select[name=employeeDesig]').val('');
        $('.selectpicker').selectpicker('refresh');
    });  

    //for search cancel btn
    $('#searchCancel').on('click',function (){
        $('#employeeSearch').val('');
        $('select[name=employeeDeptSearch]').val('');
        $('select[name=employeeDesigSearch]').val('');
        $('.selectpicker').selectpicker('refresh');  
        getSearchResults();
    });

    function getEmployeeDesignations(id, state = null) {
        var designationIds = [];            
        $.ajax({
            type: 'GET',
            data: {
                employeeID: id
            },
            url: BASE_URL + '/employee-api/getEmployeeDesignation',
            success: function(respond) {
                if (state == null) {
                    for (var i = 0; i < respond.data.length; i++) {
                       designationIds.push(parseInt(respond.data[i]['designationID']));
                    }
                    $('select[name=employeeDesig]').selectpicker('val', designationIds);
                }
                else {
                    $('.designationViewModalBody').html(respond.html);
                    $('#designationViewModal').modal('show');
                }
                
            }
        });
    }

    function getSearchResults(key = null, searchDepartmentID = null, searchDesignationID = null ) {
        $.ajax({
            type: 'POST',
            url: BASE_URL + '/employee-api/search',
            data: {
                searchKey : key,
                searchDepartmentID : searchDepartmentID,
                searchDesignationID : searchDesignationID

            },
            success: function(data) {
                if (data.status == true) {
                    if (data.status) {
                        $("#employee-list").html(data.html);
                    } else {
                        p_notification(data.status, data.msg);
                    }
                }            
            }
        });
    }

    function deleteEmployee(employeeId) {
        bootbox.confirm('Are you sure you want to delete this Employee?', function($result) {
            if ($result == true) {
                eb.ajax({
                    url: BASE_URL + '/employee-api/deleteEmployee',
                    method: 'post',
                    data: {
                        employeeID: employeeId,
                        entityID: entityID
                    },
                    dataType: 'json',
                    success: function(data) {
                        if (data.status == true) {
                            p_notification(data.status, data.msg);
                            window.location.reload();
                        } else {
                            p_notification(data.status, data.msg);
                        }
                    }
                });
            }
        });
    }

    function validateFormData(formData)
    {
        if (formData.employeeCode == '') {
            p_notification(false, eb.getMessage('ERR_EMP_CODE_CNT_BE_NULL'));
            return false;
        }
        else if (formData.employeeIDNo == '') {
            p_notification(false, eb.getMessage('ERR_EMP_IDNO_CNT_BE_NULL'));
            return false;
        } 
        else if (formData.employeeDepartmentID == 0 || formData.employeeDepartmentID == null) {
            p_notification(false, eb.getMessage('ERR_DEPARTMENT_CNT_BE_NULL'));
            return false;
        }
        else if(formData.employeeDesignations == null) {
            p_notification(false, eb.getMessage('ERR_DESIGNATION_CNT_BE_NULL'));
            return false;
        }
        return true;
    }


});
