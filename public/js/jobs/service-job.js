
var selectedProductID;
var locationProducts = [];
var AddCustomerFlag = false;
var addCustomer = false;
var goJob = false;
var jobCardID = null;
var vehicleRegNo = null;
var rightSideList = {};
var leftSideList = {};
var allServices = {};
var jobCardServices = {};
var serviceID = null;
var otherServiceText = null;
var rightSide = [];
var leftSide = [];
var otherServices = {};
var selectedServices = {};
var isServiceSelected = false;
var subTaskArr = [];
var serviceVehicleID = null;
var vehicleTypeID = null;
var fuelTypeID = null;
var vehicleModelID = null;
var vehicleSubModelID = null;
var subTaskVerifyArr = {};
var tempSubTasks = {};
var editMode = false;
var editmodeSelectedServices = {};
var jobCardServiceData = {};
var jobID
var jobStatus;
var serviceVehicles = [];
var mobileNumbersList = [];
var isNewCustomerDetail = false;
var vehicleClick = false;
var customerClick = false;
var customerID = null;
var isUseJobCard = false;
// var mobileNo = null;
$(document).ready(function() {
    var step = 1;
    $('#selectAllJobCard').css( 'cursor', 'pointer' );
    $("#vehicleType").prop("disabled", true);
    $("#fuelTypeID").prop("disabled", true);
    $("#vehicleModel").prop("disabled", true);
    $("#vehicleSubModel").prop("disabled", true);
    $("#jobCardId").prop("disabled", true);
    $("#serviceId").prop("disabled", true);
    $("#employees").prop("disabled", true);
    $('#vehicleModel').selectpicker('show');
    $('#vehicleSubModel').selectpicker('show');
    // $('#typeRelatevehicleModel').selectpicker('hide');
    $('#typeRelatevehicleSubModel').selectpicker('hide');

    $('#jobCardId').select2({
        // tags: true,
        tokenSeparators: [',', ' '],
        minimumInputLength: 1,
        minimumResultsForSearch: 10,
        ajax: {
            url: BASE_URL + '/job-card-setup-api/search-job-cards-for-dropdown',
            dataType: "json",
            type: "POST",
            data: function (params) {

                var queryParameters = {
                    searchKey: params.term,
                    vehicleType: $('#vehicleTypeFoJob').val()
                }
                return queryParameters;
            },
            processResults: function (data) {
                return {
                    results: $.map(data.data.list, function (item) {
                        return {
                            text: item.text,
                            id: item.value
                        }
                    })
                };
            }
        },
        placeholder: 'Select Job Card',
    });

    $('#serviceId').select2({
        // tags: true,
        tokenSeparators: [',', ' '],
        minimumInputLength: 1,
        minimumResultsForSearch: 10,
        ajax: {
            url: BASE_URL + '/task_setup_api/searchTasksForDropdown',
            dataType: "json",
            type: "POST",
            data: function (params) {

                var queryParameters = {
                    searchKey: params.term,
                    vehicleType: $('#vehicleTypeFoJob').val()
                }
                return queryParameters;
            },
            processResults: function (data) {
                return {
                    results: $.map(data.data.list, function (item) {
                        return {
                            text: item.text,
                            id: item.value
                        }
                    })
                };
            }
        },
        placeholder: 'Select Services',
    });

    $("#jobCardId").prop("disabled", true);
    $("#serviceId").prop("disabled", true);

    $('.customerID').select2({
        tags: true,
        createTag: function (params) {
            var term = $.trim(params.term);
            if (term === '') {
              return null;
            }

            return {
              id: term.charAt(0).toUpperCase() + term.slice(1),
              text: term.charAt(0).toUpperCase() + term.slice(1),
              newTag: true // add additional parameters
            }
          },
        minimumInputLength: 1,
        minimumResultsForSearch: 10,
        ajax: {
            url: BASE_URL + '/customerAPI/search-customers-for-dropdown',
            dataType: "json",
            type: "POST",
            data: function (params) {

                var queryParameters = {
                    searchKey: params.term,
                    addFlag: 'Job'
                }
                return queryParameters;
            },
            processResults: function (data) {
                return {
                    results: $.map(data.data.list, function (item) {
                        return {
                            text: item.text,
                            id: item.value
                        }
                    })
                };
            }
        },
        placeholder: 'Select a customer',
    });

    $('.vehicleType').select2({
        tokenSeparators: [',', ' '],
        minimumInputLength: 1,
        minimumResultsForSearch: 10,
        ajax: {
            url: BASE_URL + '/api/resources/searchVehicleTypesForDropdown',
            dataType: "json",
            type: "POST",
            data: function (params) {

                var queryParameters = {
                    searchKey: params.term
                }
                return queryParameters;
            },
            processResults: function (data) {
                return {
                    results: $.map(data.data.list, function (item) {
                        return {
                            text: item.text,
                            id: item.value
                        }
                    })
                };
            }
        },
        placeholder: 'Select a vehicle type',
    });

    $('.vehicleType').on('select2:select', function (e) { 
        var data = e.params.data;
        vehicleTypeID = data.id;
        vehicleModelID = null;
        loadtypeRelatedVehicleModelToPicker(vehicleTypeID);
        $("#jobCardId").prop("disabled", false);
        $("#serviceId").prop("disabled", false);
    });


    $('#jobCardId').on('select2:select', function (e) {
        var data = e.params.data;
        if(!(data.id == null || data.id == 0) && jobCardID != data.id){
            jobCardID = data.id;
            editmodeSelectedServices = [];
            selectedServices = {};
            allServices = {};
            otherServices = {};
            leftSideList = {};
            rightSideList = {};
            getJobCardRelatedServices(jobCardID);
        } 
    });

    $('#serviceId').on('select2:select', function (e) {
        var data = e.params.data;
        if (data.id in allServices && data.id != 0) {
            p_notification(false, eb.getMessage('ERR_SRVC_DUPLICATE'));
            clearServiceDropdown();
            return;
        }

        var otherServiceId = $('#serviceId').val();
        if ($('#serviceId').val() != 0 && $('#serviceId').val() != null) {
            // $("#jobCardId").prop("disabled", true);
            serviceID = $('#serviceId').val();
            otherServiceText = data.text;

            if (editMode) {
                otherServices[serviceID] = {
                    codeName : otherServiceText,
                    jobCard: ''

                };
            } else {
                otherServices[serviceID] = otherServiceText;
            }

            if (rightSide.length == leftSide.length) {
                leftSide.push(serviceID);
            } else if (rightSide.length > leftSide.length) {
                leftSide.push(serviceID);
            } else {
                rightSide.push(serviceID);
            }
            getRelatedSubTasks(serviceID, 'getCount');
        } 
    });

    $('.vehicleRegID').select2({
        tags: true,
        createTag: function (params) {
            var term = $.trim(params.term);
            if (term === '') {
              return null;
            } else {
                var splitDetails = term.match(/([A-Za-z]+)([0-9]+)/);
                if (splitDetails != null) {
                    var newTerm = splitDetails[1]+"-"+splitDetails[2];
                } else {
                    var newTerm = term;
                }
            }

            return {
              id: newTerm.toUpperCase(),
              text: newTerm.toUpperCase(),
              newTag: true // add additional parameters
            }
        },
        tokenSeparators: [',', ' '],
        minimumInputLength: 1,
        minimumResultsForSearch: 10,
        ajax: {
            url: BASE_URL + '/service-job-api/searchVehiclesForDropdown',
            dataType: "json",
            type: "POST",
            data: function (params) {

                var queryParameters = {
                    searchKey: params.term,
                    customerID: $('.customerID').val()
                }
                return queryParameters;
            },
            processResults: function (data) {
                return {
                    results: $.map(data.data.list, function (item) {
                        return {
                            text: item.text,
                            id: item.value
                        }
                    })
                };
            }
        },
        placeholder: 'Ex: AAA-4444',
    });



    $('.customerID').on('select2:select', function (e) { 
        var data = e.params.data;
        customerID = e.params.data;
        customerClick = true;
        if (data.id != data.text) {
            $("#customerTitle").val(0);
            $("#cusTitleDiv").addClass('hidden');
            getVehicleDetails('', 'customer', data.id);
        } else {
            isNewCustomerDetail = true;
            $("#cusTitleDiv").removeClass('hidden');
        }
    });

     $('.vehicleRegID').on('select2:select', function (e) { 
        var data = e.params.data;
        getVehicleDetails(data.text, 'vehicle');
        vehicleClick = true;
    });

    $('#cusNextBtn').on('click', function(event) {
        event.preventDefault();
        if (!editMode) {
            if ($('.vehicleRegID').val() == null) {
                if ($('#vehicleTypeFoJob').val() == 0) {
                    $("#fuelTypeID").prop("disabled", false);
                    $('.selectpicker').selectpicker('refresh');
                    loadVehicleTypeToPicker();
                    $('#typeRelatevehicleModel')
                            .empty()
                    .val('')
                    .append($("<option></option>")
                            .attr("value", 0)
                            .text('Select Vehicle Model'))
                    .selectpicker('refresh')
                    .trigger('change');
                }
            } else {

                if (!editMode) {
                    loadVehicleTypeToPicker();
                }
                $("#fuelTypeID").prop("disabled", true);
                $('.selectpicker').selectpicker('refresh');
                getVehicleDetails($('.vehicleRegID').select2('data')[0].text, 'vehicle');
                editmodeSelectedServices = [];
                selectedServices = {};
                allServices = {};
                otherServices = {};
                leftSideList = {};
                rightSideList = {};
                $('.viewedJobCardServices').remove();
                $('#selectAllIcon').removeClass('fa-check-square-o');
                $('#selectAllIcon').addClass('fa-square-o');
                $('#selectAllJobCard').data('selected', false);
                clearJobCardDropdown();
            } 
        } else {
            if ($('.vehicleRegID').val() == null) {
                p_notification(false, eb.getMessage('ERR_REQ_VEHI_REG_NUM'));
                $('.vehicleRegID').select2("open");
                return false;
            } else {

                if (!editMode) {
                    loadVehicleTypeToPicker();
                }
                getVehicleDetails($('.vehicleRegID').select2('data')[0].text, 'vehicle');
            } 
        }
        // if (vehicleClick == false && $('#mode').val() != 'edit') {
        // }

        step = 2;
        $('.customerView').addClass('hidden');
        $('.vehicleDetailView').removeClass('hidden');
        $('#step01').removeClass('activeDiv');
        $('#step01').addClass('inActiveDiv');
        $('#step02').addClass('activeDiv');
        $('#step02').removeClass('inActiveDiv');
    });

    $('#vehicleClickDiv').on('click', function(event) {
        event.preventDefault();
        if (step == 1) {
            $('#cusNextBtn').trigger('click');
        } else if (step == 3) {
            $('#servicePrevBtn').trigger('click');
        }
    });

    $('#serviceClickDiv').on('click', function(event) {
        event.preventDefault();
        if (step == 1) {
            if ($('.vehicleRegID').val() == null) {
                p_notification(false, eb.getMessage('ERR_REQ_VEHI_REG_NUM'));
                $('.vehicleRegID').select2("open");
                return false;
            } 
            if ($('#vehicleTypeFoJob').val() == "" || $('#vehicleTypeFoJob').val() == 0 || $('#vehicleTypeFoJob').val() == null) {
                p_notification(false, eb.getMessage('ERR_REQ_VEHI_TYPE'));
                return false;
            }
            $('#step01').removeClass('activeDiv');
            $('#step01').addClass('inActiveDiv');
            $('.customerView').addClass('hidden');
            $('#vehicleNextBtn').trigger('click');
        } else if (step == 2) {
            $('#vehicleNextBtn').trigger('click');
        }
    });

     $('#customerClickDiv').on('click', function(event) {
        event.preventDefault();
        if (step == 2) {
            $('#vehiclePrevBtn').trigger('click');
        } else if (step == 3) {
            $('#step03').removeClass('activeDiv');
            $('#step03').addClass('inActiveDiv');
            $('.serviceDetailView').addClass('hidden');
            $('#vehiclePrevBtn').trigger('click');
        }
    });

    $('#vehiclePrevBtn').on('click', function(event) {
        event.preventDefault();
        step = 1;
        $('.customerView').removeClass('hidden');
        $('.vehicleDetailView').addClass('hidden');
        $('#step01').addClass('activeDiv');
        $('#step01').removeClass('inActiveDiv');
        $('#step02').removeClass('activeDiv');
        $('#step02').addClass('inActiveDiv');
    });


    $('#vehicleNextBtn').on('click', function(event) {
        event.preventDefault();
        if ($('#vehicleTypeFoJob').val() == "" || $('#vehicleTypeFoJob').val() == 0 || $('#vehicleTypeFoJob').val() == null) {
            p_notification(false, eb.getMessage('ERR_REQ_VEHI_TYPE'));
            // $('.vehicleType').select2("open");
            return false;
        }

        if ($('#typeRelatevehicleModel').val() == "" || $('#typeRelatevehicleModel').val() == 0 || $('#typeRelatevehicleModel').val() == null) {
            p_notification(false, eb.getMessage('ERR_REQ_VEHI_MODEL'));
            // $('.vehicleType').select2("open");
            return false;
        }
        step = 3;
        $('.vehicleDetailView').addClass('hidden');
        $('.serviceDetailView').removeClass('hidden');
        $('#step02').removeClass('activeDiv');
        $('#step02').addClass('inActiveDiv');
        $('#step03').addClass('activeDiv');
        $('#step03').removeClass('inActiveDiv');

        if (editMode) {
            if (isUseJobCard) {
                if (jobStatus == 3) {
                    $("#jobCardId").prop("disabled", false);
                } else {
                    $("#jobCardId").prop("disabled", true);
                }
            } else {
                $("#serviceId").prop("disabled", false);
                if (jobStatus != 3 && jobStatus != 8 && jobStatus != 9) {
                    $("#serviceId").prop("disabled", true);
                }
            }
        }

    });

    $('#servicePrevBtn').on('click', function(event) {
        event.preventDefault();
        step = 2;
        $('.vehicleDetailView').removeClass('hidden');
        $('.serviceDetailView').addClass('hidden');
        $('#step02').addClass('activeDiv');
        $('#step02').removeClass('inActiveDiv');
        $('#step03').removeClass('activeDiv');
        $('#step03').addClass('inActiveDiv');
    });

    $('#servicePrevBtn2').on('click', function(event) {
        event.preventDefault();
        step = 2;
        $('.vehicleDetailView').removeClass('hidden');
        $('.serviceDetailView').addClass('hidden');
        $('#step02').addClass('activeDiv');
        $('#step02').removeClass('inActiveDiv');
        $('#step03').removeClass('activeDiv');
        $('#step03').addClass('inActiveDiv');
    });

    $('#selectAllJobCard').on('click', function(){
        var selected = $('#selectAllJobCard').data('selected');
        if (selected) {
            $('#selectAllIcon').removeClass('fa-check-square-o');
            $('#selectAllIcon').addClass('fa-square-o');
            $('#selectAllJobCard').data('selected', false);

            $('#serviceList #leftSideList #viewJobCardServicesLeftSide #selectService .jobCardService').addClass('fa-square-o');
            $('#serviceList #leftSideList #viewJobCardServicesLeftSide #selectService .jobCardService').removeClass('fa-check-square-o');

            $('#serviceList #rightSideList #viewJobCardServicesRightSide #selectService .jobCardService').addClass('fa-square-o');
            $('#serviceList #rightSideList #viewJobCardServicesRightSide #selectService .jobCardService').removeClass('fa-check-square-o');

            $.each(jobCardServiceData, function(index, val) {
                if (editMode) {
                    if (selectedServices[index]['isStarted']) {
                        p_notification(false, eb.getMessage('ERR_STRT_SRVC_HANDLE'));
                        return;
                    } 
                }
                isServiceSelected = false;
                if (selectedServices[index]) {
                    delete selectedServices[index];
                }
            });
        } else {
            $('#selectAllIcon').removeClass('fa-square-o');
            $('#selectAllIcon').addClass('fa-check-square-o');
            $('#selectAllJobCard').data('selected', true);

            $('#serviceList #leftSideList #viewJobCardServicesLeftSide #selectService .jobCardService').removeClass('fa-square-o');
            $('#serviceList #leftSideList #viewJobCardServicesLeftSide #selectService .jobCardService').addClass('fa-check-square-o');

            $('#serviceList #rightSideList #viewJobCardServicesRightSide #selectService .jobCardService').removeClass('fa-square-o');
            $('#serviceList #rightSideList #viewJobCardServicesRightSide #selectService .jobCardService').addClass('fa-check-square-o');

            $.each(jobCardServiceData, function(index, val) {
                isServiceSelected = true;
                selectedServices[index] = {
                    code : allServices[index],
                    jobCardID : jobCardID,
                    selectedSubTasks:{}
                }
                // getRelatedSubTasks(index, 'add');
                $.ajax({
                    type: 'POST', url: BASE_URL + '/serviceSetup-api/getSubTaskByTaskId', 
                    data: {
                        taskID: index,
                        mode: 'jobView'
                    },
                    success: function(respond) {
                        if (respond.status == true) {
                            
                            var temp2 = {};
                            for (var i = 0; i < respond.data.length; i++) {
                                temp2[respond.data[i]['subTaskID']] = {
                                    code :  respond.data[i]['code'],
                                    selected: true
                                }
                                selectedServices[index].selectedSubTasks = temp2;
                            }

                            
                        }
                    }
                });
            });
        }
    });

    $('#showProjectJobs').css( 'cursor', 'pointer' );    
    var items = {};
    $.ajax({
        type: 'POST',
        url: BASE_URL +'/service-job-api/getAllVehical',
        data:{
            customerID: customerID
        },
        success: function(respond) {
            serviceVehicles = respond.data.list;
            // autocomplete(document.getElementById("vehicleRegNo"), serviceVehicles);
        }
    });

    $.ajax({
        type: 'POST',
        url: BASE_URL +'/customerAPI/getAllCustomersDetails',
        data:{
            customerID: customerID
        },
        success: function(respond) {
            if (respond.status) {
                // autocomplete(document.getElementById("customer"), respond.data.userNameArr, 'customer');
                autocomplete(document.getElementById("mobileNo"), respond.data.mobileNumbersArr, 'customer', 'customerMob');
                autocomplete(document.getElementById("emailAddress"), respond.data.emailsArr, 'customer', 'customerMob');
            }
        }
    });

     // check whether its create mode edit mode or view mode
    if ($('#mode').val() == 'edit') {

        jobUpdateView();
        jobID = $('#mode').data('job-id');
        editMode = true;

        eb.ajax({
            type: 'POST',
            url: BASE_URL +'/service-job-api/getEditDetails',
            data:{
                jobID: jobID
            },
            success: function(respond) {

                if(respond.status){

                    jobStatus = respond.data['jobStatus'];
                    isUseJobCard = (respond.data['jobTaskTaskCardID'] != null) ? true : false;

                    if (jobStatus == 20 || jobStatus == 21) {
                        $('#update').addClass('hidden');
                    }
                    $('#jobCode').val(respond.data['jobReferenceNumber']);

                    var vehicleData = {
                        id: respond.data['serviceVehicleID'],
                        text: respond.data['serviceVehicleRegNo']
                    };

                    var newOption3 = new Option(vehicleData.text, vehicleData.id, false, false);
                    $('.vehicleRegID').append(newOption3).trigger('change');
                    // $('.vehicleRegID').prop('disabled', true);

                    customerID = respond.data['customerID'];
                    if (customerID != null) {
                        var cusData = {
                            id: respond.data['customerID'],
                            text: respond.data['customerName']+'-'+respond.data['customerCode']
                        };

                        var newOption = new Option(cusData.text, cusData.id, false, false);
                        $('.customerID').append(newOption).trigger('change');
                    }
                    
                    // $('.customerID').prop('disabled', true);

                    // var vehiclTypeData = {
                    //     id: respond.data['vehicleType'],
                    //     text: respond.data['vehicleTypeName']
                    // };

                    // var newOption2 = new Option(vehiclTypeData.text, vehiclTypeData.id, false, false);
                    // $('.vehicleType').append(newOption2).trigger('change');



                    loadVehicleTypeToPicker();
                    setTimeout(function(){ 
                        $('select[name=vehicleTypeFoJob]').selectpicker('val',respond.data['vehicleType']);
                        // loadtypeRelatedVehicleSubModelToPickerForEdit(respond.data['vehicleModel']);
                        vehicleTypeID = respond.data['vehicleType'];
                    }, 1000);




                    $('#mobileNo').val(respond.data['customerTelephoneNumber']);
                    $('#emailAddress').val(respond.data['customerProfileEmail']);
                    $('#address').val(respond.data['customerProfileLocationNo']);
                    $('#customer').attr("disabled", true);
                    $('#mobileNo').attr("disabled", true);
                    $('#address').attr("disabled", true);
                    $('#emailAddress').attr("disabled", true);

                    loadtypeRelatedVehicleModelToPicker(respond.data['vehicleType']);
                    setTimeout(function(){ 
                        $('select[name=typeRelatevehicleModel]').selectpicker('val',respond.data['vehicleModel']);
                        loadtypeRelatedVehicleSubModelToPickerForEdit(respond.data['vehicleModel']);
                        vehicleModelID = respond.data['vehicleModel'];
                    }, 1000);
                    $('#vehicleSubModel').append($("<option value='" + respond.data['vehicleSubModel'] + "'>" + respond.data['vehicleSubModelName']+ "</option>"));
                    $('select[name=vehicleSubModel]').val(respond.data['vehicleSubModel']);
                    $('#enginNo').val(respond.data['serviceVehicleEngineNo']);
                    if (respond.data['jobVehicleKMs'] != null) {
                        $('#kmCovered').val(parseFloat(respond.data['jobVehicleKMs']));
                    }
                    $('#jobSupervisor').val(respond.data['userFirstName']);
                    $('#customerNote').val(respond.data['customerNotes']);
                    $('#fuelTypeID').val(respond.data['fuelTypeID']);
                    $('#vehicleModal').val(respond.data['vehicleSubModelName']);
                    $('#vehicleModalValue').val(respond.data['vehicleSubModel']);
                    $('#kmCovered').attr('disabled', false);
                    $("#enginNo").prop("disabled", false);
                    $("#vehicleType").prop("disabled", false);
                    $("#fuelTypeID").prop("disabled", false);
                    $("#enginNo").prop("disabled", false);
                    $("#employees").prop("disabled", false);

                    if (respond.data['jobTaskTaskCardID'] != null) {
                        var jbData = {
                            id: respond.data['jobTaskTaskCardID'],
                            text: respond.data['taskCardCode']
                        };

                        var newOption4 = new Option(jbData.text, jbData.id, false, false);
                        $('#jobCardId').append(newOption4).trigger('change');
                    }

                            $("#jobCardId").prop("disabled", false);
                            $("#serviceId").prop("disabled", false);

                    // if (respond.data['jobStatus'] != 3) {
                    //     $("#jobCardId").prop("disabled", true);
                    // }
                    if (respond.data['jobTaskTaskCardID'] != null) {
                        if (jobStatus == 3) {
                            $("#jobCardId").prop("disabled", false);
                        } else {
                            $("#jobCardId").prop("disabled", true);
                        }
                    } else {
                        $("#serviceId").prop("disabled", false);
                        if (jobStatus != 3 && jobStatus != 8 && jobStatus != 9) {
                            $("#serviceId").prop("disabled", true);
                        }
                    }
                    $('#jobSupervisor').data('user-id', respond.data['userID']);
                    vehicleTypeID = respond.data['vehicleType'];
                    fuelTypeID = respond.data['fuelTypeID'];
                    vehicleSubModelID = respond.data['vehicleSubModel'];
                    $('#vehicleModalValue').val(respond.data['vehicleSubModel']);
                    $('.selectpicker').selectpicker('refresh');
                    serviceVehicleID = respond.data['serviceVehicleID'];
                    subTaskVerifyArr = respond.data['subTaskVerifyArr'];
                    selectedServices = respond.data['selectedServices'];
                    editmodeSelectedServices = Object.keys(selectedServices);
                    allServices = {};
                    $('#employees').selectpicker('val', respond.data.allEmployees);
                    $('#continue').addClass('hidden');
                    var keys = Object.keys(respond.data['allServices']);

                    for (var i = 0; i < keys.length; i++) {
                        var remainder = i % 2;

                        if (remainder == 0) {
                            leftSide.push(keys[i]);
                        } else {
                            rightSide.push(keys[i]);
                        }

                    }

                    addJobServicesNewRow(respond.data['allServices'], respond.data['jobTaskTaskCardID'], false, jobStatus);                                
                }                
            }
        });
    }

    $("#customer_more").hide();

    $('#addCustomerModal').on('shown.bs.modal', function() {
        $(this).find("input[name='customerName']").focus();
    });

    //customer more details button
    $("#moredetails").click(function() {
        $("#customer_more").slideDown();
        $('#moredetails').hide();
        $('#customerCurrentBalance', '#addcustomerform').attr('disabled', true);
        $('#customerCurrentCredit', '#addcustomerform').attr('disabled', true);
    });

    
    //load customers for drop down
    // loadDropDownFromDatabase('/customerAPI/search-customers-for-dropdown', "", '', '#customer','','','','',true);
    // $('#customer').selectpicker('refresh');
    // $('#customer').on('change', function() {
    //     customerID = $('#customer').val();
    //     $.ajax({
    //         type: 'POST',
    //         url: BASE_URL +'/service-job-api/getAllVehical',
    //         data:{
    //             customerID: customerID
    //         },
    //         success: function(respond) {
    //             serviceVehicles = respond.data.list;
    //             autocomplete(document.getElementById("vehicleRegNo"), serviceVehicles);
    //             $.ajax({
    //                 type: 'POST',
    //                 url: BASE_URL +'/customerAPI/getCustomerDetails',
    //                 data:{
    //                     customerID: customerID
    //                 },
    //                 success: function(respond) {
    //                     if (respond.data) {
    //                         $('#emailAddress').val(respond.data['customerEmail']);
    //                         $('#address').val(respond.data['customerAddress']);
    //                         $('#mobileNo').val(respond.data['customerTelephoneNumber']);
    //                     }
    //                 }
    //             });
    //         }
    //     });

    // });
    //load customers for drop down
    

    $('#fuelTypeID').on('change', function() {
        if(!($(this).val() == null || $(this).val() == 0)){
            fuelTypeID = $('#fuelTypeID').val();
        } else {
            fuelTypeID = null;
        }
    });

    $('#typeRelatevehicleModel').on('change', function() {
        if(!($(this).val() == null || $(this).val() == 0)){
            vehicleModelID = $(this).val();
            vehicleSubModelID = null;
            loadtypeRelatedVehicleSubModelToPicker(vehicleModelID);
            $('#vehicleSubModel').selectpicker('hide');
            $('#typeRelatevehicleSubModel').selectpicker('show');
            clearvehicleSubModeDropdown(); 
        } else {
            vehicleModelID = null;
            $('#vehicleSubModel').selectpicker('show');
            $('#typeRelatevehicleSubModel').selectpicker('hide');
        }
    });

    $('#vehicleTypeFoJob').on('change', function() {
        if(!($(this).val() == null || $(this).val() == 0)){
            $('#typeRelatevehicleModel').prop("disabled", false);
            vehicleTypeID = $(this).val();
            vehicleModelID = null;
            loadtypeRelatedVehicleModelToPicker(vehicleTypeID);
            $("#jobCardId").prop("disabled", false);
            $("#serviceId").prop("disabled", false); 
        } 
    });

    $('#typeRelatevehicleSubModel').on('change', function() {
        if(!($(this).val() == null || $(this).val() == 0)){
            vehicleSubModelID = $(this).val();
        }
    });

    //load customers for drop down
    loadDropDownFromDatabase('/api/resources/searchVehicleModelsForDropdown', "", '', '#vehicleModel');
    $('#vehicleModel').selectpicker('refresh');
    $('#vehicleModel').on('change', function() {
        if(!($(this).val() == null || $(this).val() == 0)){
            vehicleTypeID = $('#vehicleType').val(); 
        }
    });


    // loadDropDownFromDatabase('/task_setup_api/searchTasksForDropdown', "", '', '#serviceId');
    // $('#serviceId').selectpicker('refresh');
    

    function clearServiceDropdown() {
        $('#serviceId').val(null).trigger("change");
    }

    function clearvehicleModeDropdown() {
        $('#vehicleModel')
                .empty()
                .val('')
                .append($("<option></option>")
                        .attr("value", 0)
                        .text('Select Vehicle Make'))
                .selectpicker('refresh')
                .trigger('change');
    }

    function clearvehicleSubModeDropdown() {
        $('#vehicleSubModel')
                .empty()
                .val('')
                .append($("<option></option>")
                        .attr("value", 0)
                        .text('Select Vehicle Model'))
                .selectpicker('refresh')
                .trigger('change');
    }

    function clearvehicleTypeDropdown() {
        $('#vehicleType')
                .empty()
                .val('')
                .append($("<option></option>")
                        .attr("value", 0)
                        .text('Select Vehicle Type'))
                .selectpicker('refresh')
                .trigger('change');
    }

    function clearJobCardDropdown() {
        $('#jobCardId')
                .empty()
                .val('')
                .append($("<option></option>")
                        .attr("value", 0)
                        .text('Select Job Card'))
                .selectpicker('refresh')
                .trigger('change');
    }

    function clearCustomerDropdown() {
        $('#customer')
                .empty()
                .val('')
                .append($("<option></option>")
                        .attr("value", 0)
                        .text('Select Customer'))
                .selectpicker('refresh')
                .trigger('change');
    }


    // $('#vehicleRegNo').on('keyup', function(e) {
    
    //     var regex=/^[a-zA-Z]+$/;
    //     if ($('#vehicleRegNo').val().match(regex))
    //     {
    //         this.value = this.value.toUpperCase();
    //     }
    // });


    /**
     *
     *use to get job card related services for services edit modal 
     */
    function getJobCardRelatedServices(Id) {
        eb.ajax({
            type: 'POST', url: BASE_URL + '/job-card-setup-api/getRelatedServices', 
            data: {
                taskCardID: Id,
                mode: 'jobView'
            }, 
            success: function(respond) {
                if (respond.status == true) {
                    // $("#serviceId").prop("disabled", true);
                    // $('.selectpicker').selectpicker('refresh');
                    $('#selectAllSpan').removeClass('hidden');
                    rightSide = [];
                    leftSide = [];
                    jobCardServices = {};
                    jobCardServiceData = respond.data['services'];
                    if (editMode) {
                        for (var key in respond.data['services']) {
                            jobCardServices[key] = {
                                codeName : respond.data['services'][key],
                                jobCard: Id
                            }
                        }
                    } else {
                        jobCardServices = respond.data['services'];
                    }

                    subTaskVerifyArr = respond.data['subTaskVerifyArray'];

                    var keys = Object.keys(jobCardServices);

                    for (var i = 0; i < keys.length; i++) {
                        var remainder = i % 2;

                        if (remainder == 0) {
                            leftSide.push(keys[i]);
                        } else {
                            rightSide.push(keys[i]);
                        }

                    }
                    rightSideList = {};
                    leftSideList = {};
                    $('.viewedJobCardServices').remove();
                    addJobServicesNewRow(jobCardServices, Id);

                } 
            }
        });
    }

    function addJobServicesNewRow(services, jobCrdID, otherService = false, jobStatus = null) {

        var allServiceKeys = Object.keys(allServices);
        var serviceKeys = Object.keys(services);

        
        for (var i = 0; i < leftSide.length; i++) {

            if (editMode) {
                if (!($.inArray(leftSide[i], serviceKeys) == -1)) {
                    leftSideList[leftSide[i]] = services[leftSide[i]]['codeName']; 
                }
            } else {
               leftSideList[leftSide[i]] = services[leftSide[i]];
            }

        }
      

        for (var j = 0; j < rightSide.length; j++) {
            if (editMode) {
                if (!($.inArray(rightSide[j], serviceKeys) == -1)) {
                    rightSideList[rightSide[j]] = services[rightSide[j]]['codeName']; 
                }
            } else {
               rightSideList[rightSide[j]] = services[rightSide[j]];
            }
        }
        
        var rightSidekeys = Object.keys(rightSideList);
        var leftSidekeys = Object.keys(leftSideList);
        
        if (rightSidekeys.length > 0 || leftSidekeys.length > 0) {
            $('.serviceListRow').removeClass('hidden');
            if (rightSidekeys.length > 0) {
                $('#rightSideList').removeClass('hidden');
            }   
            if (leftSidekeys.length > 0) {
                $('#leftSideList').removeClass('hidden');
            }   
        }

        for (var i = 0; i < rightSidekeys.length; i++) {    
            if ($.inArray(rightSidekeys[i], allServiceKeys) == -1) {
                var newTrID = 'trs_'+rightSidekeys[i];
                var clonedRow = $($('#jobCardServiceRightViewRow').clone()).attr('id', newTrID).addClass('viewedJobCardServices').data('service-id', rightSidekeys[i]);
                $("#jobCardRightServiceNameCode", clonedRow).text(rightSideList[rightSidekeys[i]]);

                if (editMode) {
                    jobCrdID =  services[rightSidekeys[i]]['jobCard'];
                    if (jobStatus != null && jobStatus != '3') {
                        $('.employeeSelectHeaderRight').removeClass('hidden'); 
                        $('.employeeSelectionRight', clonedRow).removeClass('hidden'); 
                    }
                }

                $(clonedRow).data('job-card-id', jobCrdID);
                clonedRow.removeClass('hidden');
                clonedRow.insertBefore('#jobCardServiceRightViewRow');
                allServices[rightSidekeys[i]] = rightSideList[rightSidekeys[i]];


                if (services[rightSidekeys[i]]['jobCard'] != "") {
                    clonedRow.addClass('jobCardRelatedService');
                }

                if (!subTaskVerifyArr[rightSidekeys[i]]) {
                    $('#'+newTrID+' #viewSubTsk').addClass('hidden');
                }

                if (editMode) {
                    if ($.inArray(rightSidekeys[i], editmodeSelectedServices) == -1) {
                        if (otherService) {
                            $('#serviceList #rightSideList #viewJobCardServicesRightSide '+'#'+newTrID+' #selectService #serviceAddIcon').addClass('fa-check-square-o');
                            $('#serviceList #rightSideList #viewJobCardServicesRightSide '+'#'+newTrID+' #selectService #serviceAddIcon').addClass('normalService');

                            selectedServices[rightSidekeys[i]] = {
                                code : allServices[rightSidekeys[i]],
                                jobCardID : null,
                                selectedSubTasks:{}
                            }
                            autoSelectOtherServiceSubTasks(rightSidekeys[i]);

                        } else {
                            $('#serviceList #rightSideList #viewJobCardServicesRightSide '+'#'+newTrID+' #selectService #serviceAddIcon').addClass('fa-square-o');
                            $('#serviceList #rightSideList #viewJobCardServicesRightSide '+'#'+newTrID+' #selectService #serviceAddIcon').addClass('jobCardService');
                        }
                    } else { 
                        if (services[rightSidekeys[i]]['jobCard'] == '') {
                            $('#serviceList #rightSideList #viewJobCardServicesRightSide '+'#'+newTrID+' #selectService #serviceAddIcon').addClass('fa-check-square-o');   
                            $('#serviceList #rightSideList #viewJobCardServicesRightSide '+'#'+newTrID+' #selectService #serviceAddIcon').addClass('normalService');

                            if (selectedServices[rightSidekeys[i]].isStarted) {
                                $('#serviceList #rightSideList #viewJobCardServicesRightSide '+'#'+newTrID+' #selectService #serviceAddIcon').addClass('hidden');
                            }

                        } else {
                            $('#serviceList #rightSideList #viewJobCardServicesRightSide '+'#'+newTrID+' #selectService #serviceAddIcon').addClass('fa-check-square-o');   
                            $('#serviceList #rightSideList #viewJobCardServicesRightSide '+'#'+newTrID+' #selectService #serviceAddIcon').addClass('jobCardService'); 

                            if (selectedServices[rightSidekeys[i]].isStarted) {
                                $('#serviceList #rightSideList #viewJobCardServicesRightSide '+'#'+newTrID+' #selectService #serviceAddIcon').addClass('hidden');
                            }
                        }
                    }

                    if (selectedServices[rightSidekeys[i]] != undefined) {
                        $.each(selectedServices[rightSidekeys[i]].departmentEmployeeList, function(index, val) {
                            $("select[name='employeeSelectRight']", clonedRow)
                                 .append($("<option></option>")
                                            .attr("value",val.employeeId)
                                            .text(val.employeeCode+" "+((val.employeeFirstName != null) ? val.employeeFirstName: "")));

                        });
                        $("select[name='employeeSelectRight']", clonedRow).selectpicker('refresh');
                        clonedRow[0].children[3].children[2].remove();
                        if (selectedServices[rightSidekeys[i]].selectedEmployeeList.length > 0) {
                            $("select[name='employeeSelectRight']", clonedRow).selectpicker('val',selectedServices[rightSidekeys[i]].selectedEmployeeList);
                        }
                    }
                } else {
                    if (otherService) {
                        $('#serviceList #rightSideList #viewJobCardServicesRightSide '+'#'+newTrID+' #selectService #serviceAddIcon').addClass('fa-check-square-o'); 
                        $('#serviceList #rightSideList #viewJobCardServicesRightSide '+'#'+newTrID+' #selectService #serviceAddIcon').addClass('normalService'); 

                        selectedServices[rightSidekeys[i]] = {
                            code : allServices[rightSidekeys[i]],
                            jobCardID : null,
                            selectedSubTasks:{}
                        }
                        autoSelectOtherServiceSubTasks(rightSidekeys[i]);  

                    } else {
                        $('#serviceList #rightSideList #viewJobCardServicesRightSide '+'#'+newTrID+' #selectService #serviceAddIcon').addClass('fa-square-o');
                        $('#serviceList #rightSideList #viewJobCardServicesRightSide '+'#'+newTrID+' #selectService #serviceAddIcon').addClass('jobCardService');
                    }
                }
            }           
        }
    
        for (var i = 0; i < leftSidekeys.length; i++) {    
            if ($.inArray(leftSidekeys[i], allServiceKeys) == -1) {
                var newTrID = 'trs_'+leftSidekeys[i];
                var clonedRow = $($('#jobCardServiceLeftViewRow').clone()).attr('id', newTrID).addClass('viewedJobCardServices').data('service-id', leftSidekeys[i]);
                $("#jobCardLeftServiceNameCode", clonedRow).text(leftSideList[leftSidekeys[i]]);

                if (editMode) {
                    jobCrdID =  services[leftSidekeys[i]]['jobCard'];
                    if (jobStatus != null && jobStatus != '3') {   
                        $('.employeeSelectHeaderLeft').removeClass('hidden'); 
                        $('.employeeSelectionLeft', clonedRow).removeClass('hidden');
                    } 
                }

                $(clonedRow).data('job-card-id', jobCrdID);
                clonedRow.removeClass('hidden');
                clonedRow.insertBefore('#jobCardServiceLeftViewRow');

                allServices[leftSidekeys[i]] = leftSideList[leftSidekeys[i]];

                if (services[leftSidekeys[i]]['jobCard'] != "") {
                    clonedRow.addClass('jobCardRelatedService');
                }

                if (!subTaskVerifyArr[leftSidekeys[i]]) {
                    $('#'+newTrID+' #viewSubTsk').addClass('hidden');
                }

                 if (editMode) {
                    if ($.inArray(leftSidekeys[i], editmodeSelectedServices) == -1) {
                        if (otherService) {
                            $('#serviceList #leftSideList #viewJobCardServicesLeftSide '+'#'+newTrID+' #selectService #serviceAddIcon').addClass('fa-check-square-o');

                            selectedServices[leftSidekeys[i]] = {
                                code : allServices[leftSidekeys[i]],
                                jobCardID : null,
                                selectedSubTasks:{}
                            }
                            autoSelectOtherServiceSubTasks(leftSidekeys[i]);
                        } else {
                            $('#serviceList #leftSideList #viewJobCardServicesLeftSide '+'#'+newTrID+' #selectService #serviceAddIcon').addClass('fa-square-o');
                            $('#serviceList #leftSideList #viewJobCardServicesLeftSide '+'#'+newTrID+' #selectService #serviceAddIcon').addClass('jobCardService');
                        }
                    } else {
                        if (services[leftSidekeys[i]]['jobCard'] == '') {
                            $('#serviceList #leftSideList #viewJobCardServicesLeftSide '+'#'+newTrID+' #selectService #serviceAddIcon').addClass('fa-check-square-o');
                            $('#serviceList #leftSideList #viewJobCardServicesLeftSide '+'#'+newTrID+' #selectService #serviceAddIcon').addClass('normalService');
                            if (selectedServices[leftSidekeys[i]].isStarted) {
                                $('#serviceList #leftSideList #viewJobCardServicesLeftSide '+'#'+newTrID+' #selectService #serviceAddIcon').addClass('hidden');
                            }

                        } else {
                            $('#serviceList #leftSideList #viewJobCardServicesLeftSide '+'#'+newTrID+' #selectService #serviceAddIcon').addClass('fa-check-square-o');
                            $('#serviceList #leftSideList #viewJobCardServicesLeftSide '+'#'+newTrID+' #selectService #serviceAddIcon').addClass('jobCardService');

                            if (selectedServices[leftSidekeys[i]].isStarted) {
                                $('#serviceList #leftSideList #viewJobCardServicesLeftSide '+'#'+newTrID+' #selectService #serviceAddIcon').addClass('hidden');
                            }

                        }
                    }

                    if (selectedServices[leftSidekeys[i]] != undefined) {
                        $.each(selectedServices[leftSidekeys[i]].departmentEmployeeList, function(index, val) {
                            $("select[name='employeeSelectLeft']", clonedRow)
                                 .append($("<option></option>")
                                            .attr("value",val.employeeId)
                                            .text(val.employeeCode+" "+((val.employeeFirstName != null) ? val.employeeFirstName: "")));

                        });
                        $("select[name='employeeSelectLeft']", clonedRow).selectpicker('refresh');
                        if (selectedServices[leftSidekeys[i]].selectedEmployeeList.length > 0) {
                            $("select[name='employeeSelectLeft']", clonedRow).selectpicker('val',selectedServices[leftSidekeys[i]].selectedEmployeeList);
                        }
                        clonedRow[0].children[3].children[2].remove();
                    }
                } else {
                    if (otherService) {
                        $('#serviceList #leftSideList #viewJobCardServicesLeftSide '+'#'+newTrID+' #selectService #serviceAddIcon').addClass('fa-check-square-o');
                        $('#serviceList #leftSideList #viewJobCardServicesLeftSide '+'#'+newTrID+' #selectService #serviceAddIcon').addClass('normalService');

                        selectedServices[leftSidekeys[i]] = {
                            code : allServices[leftSidekeys[i]],
                            jobCardID : null,
                            selectedSubTasks:{}
                        }
                        autoSelectOtherServiceSubTasks(leftSidekeys[i]);

                    } else {
                        $('#serviceList #leftSideList #viewJobCardServicesLeftSide '+'#'+newTrID+' #selectService #serviceAddIcon').addClass('fa-square-o');
                        $('#serviceList #leftSideList #viewJobCardServicesLeftSide '+'#'+newTrID+' #selectService #serviceAddIcon').addClass('jobCardService');
                    }
                }
            }           
        }
    }


    $('#continue').on('click', function(){

        if (!$('#vehicleRegNo').val().trim()) {
            p_notification(false, eb.getMessage('ERR_REQ_VEHI_REG_NUM'));
            return;
        }

        vehicleRegNo = $('#vehicleRegNo').val();

        // var re = new RegExp("^(?:[A-Z]{2,3}|(?!0*-)[0-9]{1,3})-[0-9]{4}$");
        // if (!re.test(vehicleRegNo)) {
        //     p_notification(false, eb.getMessage('ERR_REQ_VALID_VEHI_REG_NUM'));
        //     return;
        // } 
        getVehicleDetails(vehicleRegNo);

    });

    $('#add_other_service').on('click', function(){

        


    });

    $('#viewJobCardServicesLeftSide, #viewJobCardServicesRightSide').on('click','#selectService', function(){

        var selectedId = $(this).closest('tr').data('service-id');
        var jobCardId = $(this).closest('tr').data('job-card-id');
        var currentDiv = $(this).contents();
        var Id = $(this).closest('tr').attr('id');
        var tbodyId = $(this).closest('tbody').attr('id');
       
        if (tbodyId == 'viewJobCardServicesLeftSide') {
            var div = $('#serviceList #leftSideList #viewJobCardServicesLeftSide '+'#'+Id+' #selectService #serviceAddIcon');
        } else {
            var div = $('#serviceList #rightSideList #viewJobCardServicesRightSide '+'#'+Id+' #selectService #serviceAddIcon');
        }

        if (div.hasClass('fa-check-square-o')) {
                if (editMode) {
                    if (selectedServices[selectedId]['isStarted']) {
                        p_notification(false, eb.getMessage('ERR_STRT_SRVC_HANDLE'));
                        return;
                    } 
                }
                isServiceSelected = false;

                div.removeClass('fa-check-square-o');
                div.addClass('fa-square-o');
                delete selectedServices[selectedId];

                $('#selectAllIcon').removeClass('fa-check-square-o');
                $('#selectAllIcon').addClass('fa-square-o');
                $('#selectAllJobCard').data('selected', false);
        } else {

            isServiceSelected = true;
            div.removeClass('fa-square-o');
            div.addClass('fa-check-square-o');
            var selectedId = $(this).closest('tr').data('service-id');
            selectedServices[selectedId] = {
                code : allServices[selectedId],
                jobCardID : jobCardId,
                selectedSubTasks:{}
            }
            getRelatedSubTasks(selectedId, 'add');
        }

    });

    function getRelatedSubTasks(Id, type) {

        eb.ajax({
            type: 'POST', url: BASE_URL + '/serviceSetup-api/getSubTaskByTaskId', 
            data: {
                taskID: Id,
                mode: 'jobView'
            }, 
            success: function(respond) {
                if (respond.status == true) {
                    if (type == 'add') {
                        var temp2 = {};
                        for (var i = 0; i < respond.data.length; i++) {
                            temp2[respond.data[i]['subTaskID']] = {
                                code :  respond.data[i]['code'],
                                selected: true
                            }
                            selectedServices[Id].selectedSubTasks = temp2;
                        }

                    } else if (type == 'getCount') {

                        if (respond.data.length > 0) {
                            subTaskVerifyArr[Id] = true;
                        } else {
                            subTaskVerifyArr[Id] = false;
                        }

                        addJobServicesNewRow(otherServices, null, true);
                        clearServiceDropdown();
                
                    } else {

                        $('#viewSubTaskModal #subTaskBody #viewServiceSubTask .viewedSubTasks').remove();
                        var clonedRowIds = [];
                        for (var i = 0; i < respond.data.length; i++) {
                            var newTrID = 'trs_'+respond.data[i]['subTaskID'];
                            var clonedRow = $($('#subTskRow').clone()).attr('id', newTrID).addClass('viewedSubTasks').data('sub-task-id', respond.data[i]['subTaskID']);
                            $("#addedSubTskCode", clonedRow).text(respond.data[i]['code']+'-'+respond.data[i]['name']);
                            clonedRow.removeClass('hidden');
                            clonedRow.insertBefore('#subTskRow');
                            clonedRowIds.push(newTrID);
                        }

                        for (var i = 0; i < clonedRowIds.length; i++) {
                            if (isServiceSelected) {
                                var subTaskID = clonedRowIds[i].split('trs_')[1].trim();
                                if (selectedServices[serviceID].selectedSubTasks[subTaskID].selected) {
                                    $('#viewSubTaskModal #subTaskBody #viewServiceSubTask '+'#'+clonedRowIds[i]+' .subTskAddIcon').addClass('fa-check-square-o');
                                } else {
                                    $('#viewSubTaskModal #subTaskBody #viewServiceSubTask '+'#'+clonedRowIds[i]+' .subTskAddIcon').addClass('fa-square-o');
                                }       
                                
                            }
                        }
                        $("#viewSubTaskModal").modal('toggle');
                    }
                } 
            },
            async:true
        });
    }

    $('#viewJobCardServicesLeftSide, #viewJobCardServicesRightSide').on('click','#viewSubTsk', function(){
        var selectedId = $(this).closest('tr').data('service-id');
        serviceID = selectedId;
        if ($('#trs_'+selectedId+' #selectService #serviceAddIcon').hasClass('fa-check-square-o')) {
            isServiceSelected = true;
        }
        else {
            isServiceSelected = false; 
        }
        $('#viewSubTaskModal #subTaskBody #viewServiceSubTask #serviceAddIcon').remove();
        getRelatedSubTasks(selectedId, 'view');
        
    
    });

    $('#viewSubTaskModal #viewServiceSubTask').on('click','.viewedSubTasks .selectSubTask', function(){
        var selectedSubTaskId = $(this).closest('tr').data('sub-task-id');
        var currentDiv = $(this).contents();
        if (currentDiv.hasClass('fa-check-square-o')) {
            isServiceSelected = false;
            currentDiv.removeClass('fa-check-square-o');
            currentDiv.addClass('fa-square-o');
            selectedServices[serviceID].selectedSubTasks[selectedSubTaskId].selected = false;
        }
        else {
            isServiceSelected = true;
            currentDiv.removeClass('fa-square-o');
            currentDiv.addClass('fa-check-square-o');
            var selectedSubTaskId = $(this).closest('tr').data('sub-task-id');

            selectedServices[serviceID].selectedSubTasks[selectedSubTaskId].selected = true;
        }

    });


    $('#add').on('click', function(e) {

        if ($('.vehicleRegID').val() == null) {
            p_notification(false, eb.getMessage('ERR_REQ_VEHI_REG_NUM'));
            return false;
        }

        var mainSelectedServices = jQuery.extend(true, {}, selectedServices);
        for (var key in mainSelectedServices) {
            var temp = {};
            if(Object.keys(mainSelectedServices[key].selectedSubTasks).length > 0) {
                for (var val in mainSelectedServices[key].selectedSubTasks) {
                    if (mainSelectedServices[key].selectedSubTasks[val].selected == true) {
                        temp[val] = mainSelectedServices[key].selectedSubTasks[val];
                    }
                }
                mainSelectedServices[key].selectedSubTasks = temp;
            }
        }
        e.preventDefault();
        var formData = {
            vehicleRegNo: $('.vehicleRegID').val(),
            enginNo: $('#enginNo').val(),
            jobCode: $('#jobCode').val(),
            kmCovered: $('#kmCovered').val(),
            customer: customerID,
            vehicleModel: vehicleModelID,
            vehicleSubModelID: $('#vehicleModalValue').val(),
            vehicleSubModelNewName: $('#vehicleModal').val(),
            vehicleType: vehicleTypeID,
            fuelType: fuelTypeID,
            selectedServices : mainSelectedServices,
            serviceVehicleID: serviceVehicleID,
            customerNotes: $('#customerNote').val(),
            supervisorId: $('#jobSupervisor').data('user-id'),
            serviceEmployees: $('#employees').val(),
            mobileNo: $('#mobileNo').val(),
            isNewCustomerDetail : isNewCustomerDetail
        };

        if (isNaN(parseFloat($('.customerID').val())) && $('.customerID').val() != null) {
            formData.isNewCustomerDetail = true;
            isNewCustomerDetail = true;
        }

        if ($.isEmptyObject(formData.selectedServices)) {
            p_notification(false, "Please add atleast one service to job");
            return false;
        }

        if (isNewCustomerDetail) {
            e.preventDefault();
            var addurl = BASE_URL + '/customerAPI/add';

            var cusTitle = ($("#customerTitle").val() == 0) ? null: $("#customerTitle").val();
            var newCustomerData = {
                customerName: $('.customerID').val(),
                customerTelephoneNumber: $('#mobileNo').val(),
                customerEmail: $('#emailAddress').val(),
                customerTitle: cusTitle
            };

            if (customerClick == false) {
                if (parseFloat($('.customerID').val()) == NaN) {
                    newCustomerData.customerName = $('.customerID').val();
                }
            }
            var customerProfileDataArray = {};
            customerProfileDataArray.unsavedProfile = {
                customerProfileName: 'Default',
                customerProfileEmail: newCustomerData.customerEmail,
                customerProfileLocationNo: $('#address').val(),
            };
            var params = {
                customerName: $('.customerID').val(),
                customerCode: $('#customerCode').val(),
                customerTitle: cusTitle,
                customerTelephoneNumber: newCustomerData.customerTelephoneNumber,
                customerCurrency: 0,
                customerPaymentTerm: 1,
                customerPrimaryProfileID: 'unsavedProfile',
                customerProfile: customerProfileDataArray,
                customerReceviableAccountID: $('#receviableAccountID').val(),
                customerSalesAccountID: $('#salesAccountID').val(),
                customerSalesDiscountAccountID: $('#salesDiscountAccountID').val(),
                customerAdvancePaymentAccountID: $('#advancePaymentAccountID').val()
            };

            if (validateNewCustomerInputs(newCustomerData)) {
                var addrequest = eb.post(addurl, params);
                addrequest.done(function(addretdata) {
                    if (!addretdata.status) {
                        p_notification(addretdata.status, addretdata.msg);
                        return false;
                    }
                    if (addretdata.status === true) {
                        customerID = addretdata.data.customerID;
                        formData.customer = addretdata.data.customerID;
                        if (validateFormData(formData)) {
                            eb.ajax({
                                url: BASE_URL + '/service-job-api/saveJob',
                                method: 'post',
                                data: formData,
                                dataType: 'json',
                                success: function(data) {
                                    p_notification(data.status, data.msg);
                                    if (data.status) {
                                        var url = BASE_URL + '/service-job-dashboard';
                                        window.setTimeout(function() {
                                            location.assign(url);
                                        }, 1000);
                                    } else {
                                        if (data.data == "TERR") {
                                            $('#taskCode').focus();
                                        }
                                    }
                                }
                            });
                        }
                    }
                });
            } else {
                return false;
            }
        } else {
            if (validateFormData(formData)) {
                eb.ajax({
                    url: BASE_URL + '/service-job-api/saveJob',
                    method: 'post',
                    data: formData,
                    dataType: 'json',
                    success: function(data) {
                        p_notification(data.status, data.msg);
                        if (data.status) {
                            var url = BASE_URL + '/service-job-dashboard';
                            window.setTimeout(function() {
                                location.assign(url);
                            }, 1000);
                        } else {
                            if (data.data == "TERR") {
                                $('#taskCode').focus();
                            }
                        }
                    }
                });
            }
        }

    });


    $('#update').on('click', function(e) {

        var mainSelectedServices = jQuery.extend(true, {}, selectedServices);
        for (var key in mainSelectedServices) { 
            var temp = {};
            if(Object.keys(mainSelectedServices[key].selectedSubTasks).length > 0) {
                for (var val in mainSelectedServices[key].selectedSubTasks) {
                    if (mainSelectedServices[key].selectedSubTasks[val].selected == true) {
                        temp[val] = mainSelectedServices[key].selectedSubTasks[val];
                    }
                }
                mainSelectedServices[key].selectedSubTasks = temp;
            }
        }

        e.preventDefault();
        var formData = {
            vehicleRegNo: $('.vehicleRegID').select2('data')[0].text,
            enginNo: $('#enginNo').val(),
            jobCode: $('#jobCode').val(),
            kmCovered: $('#kmCovered').val(),
            customer: customerID,
            vehicleModel: vehicleModelID,
            vehicleSubModelID: $('#vehicleModalValue').val(),
            vehicleType: vehicleTypeID,
            fuelType: fuelTypeID,
            selectedServices : mainSelectedServices,
            serviceVehicleID: serviceVehicleID,
            customerNotes: $('#customerNote').val(),
            supervisorId: $('#jobSupervisor').data('user-id'),
            editedJobID: jobID,
            jobStatus: jobStatus,
            serviceEmployees: $('#employees').val()
        };

        $.each(mainSelectedServices, function(index, val) {
            if ($('#trs_'+index+' .viewEmployee .employeeSelectionRight').val() == undefined) {
                mainSelectedServices[index].selectedEmployeeList = $('#trs_'+index+' .viewEmployee .employeeSelectionLeft').val();;
            } else {
                mainSelectedServices[index].selectedEmployeeList = $('#trs_'+index+' .viewEmployee .employeeSelectionRight').val();;
            }
        });

        if (validateFormData(formData)) {
            eb.ajax({
                url: BASE_URL + '/service-job-api/updateJob',
                method: 'post',
                data: formData,
                dataType: 'json',
                success: function(data) {
                    p_notification(data.status, data.msg);
                    if (data.status) {
                        history.back();
                    } else {
                        if (data.data == "TERR") {
                            $('#taskCode').focus();
                        }
                    }
                }
            });
        }
    });

    function validateFormData(formData) {
        if (formData.vehicleRegNo == "") {
            p_notification(false, eb.getMessage('ERR_REQ_VEHI_REG_NUM'));
            $('#vehicleRegNo').focus();
            return false;
        } else if (Object.keys(formData.selectedServices).length == 0) {
            p_notification(false, eb.getMessage('ERR_REQ_JOB_SRVC'));
            return false;
        } else if (formData.kmCovered != "" && isNaN(parseFloat(formData.kmCovered))) {
            p_notification(false, eb.getMessage('ERR_KM_CVRD_VALUE_NUMERIC'));
            return false;
        }
        return true;
    }

    function validateNewCustomerInputs(inputs) {
        var cName = inputs.customerName;
        var cTPNo = inputs.customerTelephoneNumber;
        var cEmail = inputs.customerEmail;
        var emailRegex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        var nameregex = /^(?!\s*$).+/;

        if (cName === null || cName === '') {
            p_notification(false, eb.getMessage('ERR_CUST_NAME'));
            return false;
        } else if (!nameregex.test(cName)) {
            p_notification(false, eb.getMessage('ERR_CUST_PROFILE_VALID_NAME'));
            return false;
        } else if (cTPNo === null || cTPNo === "") {
            p_notification(false, eb.getMessage('ERR_CUST_EMPTY_PRIMARY_TP'));
            return false;
        } else if (!isPhoneValid(cTPNo)) {
            p_notification(false, eb.getMessage('ERR_CUST_TPNUM_VALIDITY'));
            return false;
        } else if (cEmail !== '' && !emailRegex.test(cEmail)) {
            p_notification(false, eb.getMessage('ERR_CUST_PROFILE_VALID_EMAIL'));
            return false;
        } else {
            return true;
        }
    }

    function loadtypeRelatedVehicleModelToPicker(vehicleTypeID) {
        eb.ajax({
            url: BASE_URL + '/api/resources/load-vehicle-model-for-dropdown',
            method: 'post',
            data: {
                vehicleTypeID: vehicleTypeID,
            },
            dataType: 'json',
            success: function(data) {
                if(data.status) {
                    $('#typeRelatevehicleModel')
                        .empty()
                        .val('')
                        .append($("<option></option>")
                                .attr("value", 0)
                                .text('Select Vehicle Model'))
                        .selectpicker('refresh')
                        .trigger('change');
                    typeRelateVehicleModelSelectPicker(data.data.vehicleModelsList);
                } else if (data.status == false) {
                    p_notification(data.status, data.msg);
                }
            }
        });
    }


    function loadVehicleTypeToPicker() {
        eb.ajax({
            url: BASE_URL + '/api/resources/load-vehicle-type-for-dropdown',
            method: 'post',
            data: {
                
            },
            dataType: 'json',
            success: function(data) {
                if(data.status) {
                    $('#vehicleTypeFoJob')
                        .empty()
                        .val('')
                        .append($("<option></option>")
                                .attr("value", 0)
                                .text('Select Vehicle Type'))
                        .selectpicker('refresh')
                        .trigger('change');
                    vehicleTypeSelectPicker(data.data.data.vehicleTypeList);
                } else if (data.status == false) {
                    p_notification(data.status, data.msg);
                }
            }
        });
    }

    function loadtypeRelatedVehicleSubModelToPicker(vehicleModelID) {
        eb.ajax({
            url: BASE_URL + '/api/resources/load-vehicle-model-for-dropdown',
            method: 'post',
            data: {
                vehicleTypeID: null,
                vehicleModelID: vehicleModelID,
            },
            dataType: 'json',
            success: function(data) {
                if(data.status) {
                    $('#typeRelatevehicleSubModel')
                        .empty()
                        .val('')
                        .append($("<option></option>")
                                .attr("value", 0)
                                .text('Select Vehicle Model'))
                        .selectpicker('refresh')
                        .trigger('change');
                    $('#vehicleModal').val("");
                    $('#vehicleModal').attr('disabled', false);
                    autocompleteModal(document.getElementById("vehicleModal"), data.data.vehicleSubModelsList);
                    // typeRelateVehicleSubModelSelectPicker(data.data.vehicleSubModelsList);
                } else if (data.status == false) {
                    p_notification(data.status, data.msg);
                }
            }
        });
    }

    function loadtypeRelatedVehicleSubModelToPickerForEdit(vehicleModelID) {
        $.ajax({
            url: BASE_URL + '/api/resources/load-vehicle-model-for-dropdown',
            method: 'post',
            data: {
                vehicleTypeID: null,
                vehicleModelID: vehicleModelID,
            },
            dataType: 'json',
            success: function(data) {
                if(data.status) {
                    $('#typeRelatevehicleSubModel')
                        .empty()
                        .val('')
                        .append($("<option></option>")
                                .attr("value", 0)
                                .text('Select Vehicle Model'))
                        .selectpicker('refresh')
                        .trigger('change');
                    $('#vehicleModal').attr('disabled', false);
                    autocompleteModal(document.getElementById("vehicleModal"), data.data.vehicleSubModelsList);
                } else if (data.status == false) {
                    p_notification(data.status, data.msg);
                }
            }
        });
    }

    function typeRelateVehicleModelSelectPicker(data) {     
        $.each(data, function(index, value) {
            $('#typeRelatevehicleModel').append("<option value='" + value.value + "'>" + value.text + "</option>");
        });
        $('#typeRelatevehicleModel').selectpicker('refresh');
    }

    function vehicleTypeSelectPicker(data) { 
        $.each(data, function(index, value) {
            $('#vehicleTypeFoJob').append("<option value='" + value.value + "'>" + value.text + "</option>");
        });
        $('#vehicleTypeFoJob').selectpicker('refresh');
    }

    function typeRelateVehicleSubModelSelectPicker(data) {     
        $.each(data, function(index, value) {
            $('#typeRelatevehicleSubModel').append("<option value='" + value.value + "'>" + value.text + "</option>");
        });
        $('#typeRelatevehicleSubModel').selectpicker('refresh');
    }

    $('#reset').on('click', function(){
        window.location.reload();
        /*
        reload page when reset buttion clicked;
        $('#enginNo').val('');
        $('#kmCovered').attr('disabled', true);
        $('#kmCovered').val('');
        $('#vehicleRegNo').val('');
        $("#jobCardId").prop("disabled", true);
        $('.selectpicker').selectpicker('refresh');
        serviceVehicleID = null;
        clearServiceDropdown();
        clearvehicleModeDropdown();
        clearJobCardDropdown();
        clearvehicleTypeDropdown();
        clearCustomerDropdown();
        $('.viewedJobCardServices').remove();
        $('#viewSubTaskModal #subTaskBody #viewServiceSubTask .viewedSubTasks').remove();
        var vehicleRegNo = null;
        rightSideList = {};
        leftSideList = {};
        allServices = {};
        jobCardServices = {};
        serviceID = null;
        otherServiceText = null;
        rightSide = [];
        leftSide = [];
        otherServices = {};
        selectedServices = {};
        isServiceSelected = false;
        subTaskArr = [];
        serviceVehicleID = null;
        vehicleTypeID = null;
        fuelTypeID = null;
        vehicleModelID = null;
        $('#addCustomerBtn').removeAttr('href');
        $("#customer").prop("disabled", true);
        $("#vehicleType").prop("disabled", true);
        $("#fuelTypeID").prop("disabled", true);
        $("#vehicleModel").prop("disabled", true);
        $("#jobCardId").prop("disabled", true);
        $("#serviceId").prop("disabled", true);
        $('#vehicleModel').selectpicker('show');
        $('#typeRelatevehicleModel').selectpicker('hide');
        $('#customerNote').val('');
        $('#enginNo').attr('disabled','disabled'); 

        */

    });

    $('#back').on('click', function(){
        var url = BASE_URL + '/service-job-dashboard';
        window.location.assign(url);
         
    });

    $('#canceljob').on('click', function(){
        var url = BASE_URL + '/service-job';
        window.location.assign(url);
         
    });

    $('#updateBack').on('click', function(){
       
        switch(jobStatus) {
          case '3':
            var url = BASE_URL + '/service-job/pendingList';
            break;
          case '8':
            var url = BASE_URL + '/service-job/inprogressList';
            break;
          case '9':
            var url = BASE_URL + '/service-job/completedList';
            break;
          case '20':
            var url = BASE_URL + '/service-job/completedList';
            break;
          case '21':
            var url = BASE_URL + '/service-job/completedList';
            break;
          default:
        }

        window.location.assign(url);
         
    });

    //this function for switch to project update view
    function jobUpdateView(){
       $('.addDiv').addClass('hidden');
       $('.updateDiv').removeClass('hidden');
    }

    function autoSelectOtherServiceSubTasks(Id) {
        eb.ajax({
            type: 'POST', url: BASE_URL + '/serviceSetup-api/getSubTask', 
            data: {
                taskID: Id,
                mode: 'jobView'
            }, 
            success: function(respond) {
                if (respond.status == true) {
                    
                    var temp2 = {};
                    for (var i = 0; i < respond.data.length; i++) {
                        temp2[respond.data[i]['subTaskID']] = {
                            code :  respond.data[i]['code'],
                            selected: true
                        }
                        selectedServices[Id].selectedSubTasks = temp2;
                    }
                } 
            },
            async:true
        });
    }
});

function getCustomerDetails(custID) {
    eb.ajax({
        type: 'POST', url: BASE_URL + '/invoice-api/getCustomerDetails', data: {customerID: custID}, success: function(respond) {
            if (respond.status == true) {
                $('#customer').
                    append($("<option></option>").
                            attr("value", respond.data.customerID).
                            text(respond.data.customerName + '-' + respond.data.customerCode));
                $('select[name=customer]').val(respond.data['customerID']);
                $('#customer').selectpicker('refresh');
            } 
        }
    });
}
// Todo change function name to genric one
function getVehicleDetails(vehicleRegNo, detailType='vehicle', key=null){
    if (detailType == 'vehicle') {
        eb.ajax({
            type: 'POST', url: BASE_URL + '/service-job-api/getVehicleDetails',
            data: {vehicleRegNo: vehicleRegNo},
            success: function(respond) {
                if (respond.status == true) {
                    if (respond.data) {
                        if (isNewCustomerDetail) {
                            $("#cusTitleDiv").removeClass('hidden');
                        } else {
                            $("#cusTitleDiv").addClass('hidden');
                        }
                        if (respond.data['customerName'] != null && respond.data['customerID'] != null) {
                            var cusText = (respond.data['customerTitle'] != "null" && respond.data['customerTitle'] != null && respond.data['customerTitle'] != "") ? respond.data['customerTitle']+'. '+respond.data['customerName']+'-'+respond.data['customerCode'] : respond.data['customerName']+'-'+respond.data['customerCode'];
                            var cusData = {
                                id: respond.data['customerID'],
                                text: cusText
                            };
                            customerID = respond.data['customerID'];
                            var newOption = new Option(cusData.text, cusData.id, false, false);
                            $('.customerID').append(newOption).trigger('change');
                        } 

                        if (respond.data['vehicleType'] != null) {
                            var vehiclTypeData = {
                                id: respond.data['vehicleType'],
                                text: respond.data['vehicleTypeName']
                            };
                            vehicleTypeID = respond.data['vehicleType'];
                            // var newOption2 = new Option(vehiclTypeData.text, vehiclTypeData.id, false, false);
                            // $('#').append(newOption2).trigger('change');
                            // $('.vehicleType').prop("disabled", true);
                            // $('#vehicleTypeFoJob').append($("<option value='" + respond.data['vehicleType'] + "'>" + respond.data['vehicleTypeName']+ "</option>"));
                            $('select[name=vehicleTypeFoJob]').val(respond.data['vehicleType']);
                            if(editMode) {
                                $('#vehicleTypeFoJob').prop("disabled", true);
                            }
                            // $('#vehicleTypeFoJob').prop("disabled", true);
                        } else {

                            loadVehicleTypeToPicker();
                            $('#vehicleTypeFoJob').prop("disabled", false);
                        }
                        
                        if (respond.data['vehicleModel'] != null) {
                            vehicleModelID = respond.data['vehicleModel'];
                            $('#typeRelatevehicleModel').append($("<option value='" + respond.data['vehicleModel'] + "'>" + respond.data['vehicleModelName']+ "</option>"));
                            $('select[name=typeRelatevehicleModel]').val(respond.data['vehicleModel']);
                            $('#typeRelatevehicleModel').prop("disabled", true);
                        } else {
                            $('#vehicleTypeFoJob').trigger('change');
                            $('#typeRelatevehicleModel').prop("disabled", false);
                        }
                        $('#vehicleSubModel').append($("<option value='" + respond.data['vehicleSubModel'] + "'>" + respond.data['vehicleSubModelName']+ "</option>"));
                        $('select[name=vehicleSubModel]').val(respond.data['vehicleSubModel']);
                        if (respond.data['serviceVehicleEngineNo'] != null) {
                            $('#enginNo').val(respond.data['serviceVehicleEngineNo']);
                        } else {
                            $('#enginNo').prop("disabled", false);
                        }
                        $('#vehicleModal').val(respond.data['vehicleSubModelName']);
                        $('#vehicleModalValue').val(respond.data['vehicleSubModel']);
                        if (respond.data['fuelTypeID'] != null) {
                            fuelTypeID = respond.data['fuelTypeID'];
                            $('#fuelTypeID').val(respond.data['fuelTypeID']);
                        } else {
                            $("#fuelTypeID").prop("disabled", false);
                        }
                        $('#kmCovered').attr('disabled', false);
                        $("#employees").prop("disabled", false);
                        $('.selectpicker').selectpicker('refresh');
                        serviceVehicleID = respond.data['serviceVehicleID'];
                        $("#jobCardId").prop("disabled", false);
                        $("#serviceId").prop("disabled", false);
                        $('#mobileNo').val(respond.data.customerTelephoneNumber);
                        $('#emailAddress').val(respond.data.customerProfileEmail);
                        $('#address').val(respond.data.customerProfileLocationNo);
                    } else {
                        if (isNewCustomerDetail) {
                            // $("#customerTitle").val();
                            $("#cusTitleDiv").removeClass('hidden');
                        }
                        serviceVehicleID = null;
                        $("#customer").prop("disabled", false);
                        $("#vehicleTypeFoJob").prop("disabled", false);
                        $("#fuelTypeID").prop("disabled", false);
                        $("#vehicleModel").prop("disabled", false);
                        $("#vehicleSubModel").prop("disabled", false);
                        $("#employees").prop("disabled", false);
                        $("#enginNo").val("");
                        $("#enginNo").prop("disabled", false);
                        $("#kmCovered").prop("disabled", false);
                        $("#kmCovered").val("");
                        $("#customerNote").prop("disabled", false);
                        $('.selectpicker').selectpicker('refresh');
                        $('#addCustomerBtn').attr('href', '#addCustomerModal');
                    }
                }
            }
        });
    } else {
        var value = vehicleRegNo;
        var data = {};
        data['customerID'] = key;
        customerID = key;
        $.ajax({
            type: 'POST',
            url: BASE_URL +'/customerAPI/getCustomerDetails',
            data:data,
            success: function(respond) {
                if (respond.data) {
                    //$('#customer').append($("<option value='" + respond.data['customerID'] + "'>" + respond.data['customerName']+'-'+respond.data['customerCode']+ "</option>"));
                    $('#emailAddress').val(respond.data['customerEmail']);
                    $('#emailAddress').attr('disabled', true);
                    $('#address').val(respond.data['customerAddress']);
                    $('#address').attr('disabled', true);
                    var cusData = {
                        id: respond.data['customerID'],
                        text: respond.data['customerName']+'-'+respond.data['customerCode']
                    };

                    var newOption = new Option(cusData.text, cusData.id, false, false);
                    $('.customerID').append(newOption).trigger('change');
                    $('#mobileNo').val(respond.data['customerTelephoneNumber']);
                    $('#mobileNo').attr('disabled', true);
                    $('.selectpicker[data-id="customer"]').attr('title', respond.data['customerName']);
                    $('.selectpicker[data-id="customer"]').empty();
                    $('.selectpicker[data-id="customer"]').append("<span class='filter-option pull-left'>"+respond.data['customerName']+'-'+respond.data['customerCode']+'</span>');
                    customerID = respond.data.customerID;
                    $.ajax({
                        type: 'POST',
                        url: BASE_URL +'/service-job-api/getAllVehical',
                        data:{
                            customerID: customerID
                        },
                        success: function(respond) {
                            serviceVehicles = respond.data.list;
                        }
                    });
                }
            }
        });

    }
}

function autocomplete(inp, arr, detailType='vehicle', flag = "addTo") {
    var currentFocus;
    inp.addEventListener("focusin", function(e) {
        e.preventDefault();
        var a, b, i, val = this.value;
        closeAllLists();
        currentFocus = -1;
        a = document.createElement("DIV");
        a.setAttribute("id", this.id + "autocomplete-list");
        a.setAttribute("class", "autocomplete-items");
        this.parentNode.appendChild(a);
        if (arr != null) {
            for (i = 0; i < arr.length; i++) {
                if (arr[i]['value'].substr(0, val.length).toUpperCase() == val.toUpperCase()) {
                    b = document.createElement("DIV");
                    b.innerHTML = "<strong>" + arr[i]['value'].substr(0, val.length) + "</strong>";
                    b.innerHTML += arr[i]['value'].substr(val.length);
                    b.innerHTML += "<input type='hidden' value='" + arr[i]['value'] + "'>";
                    b.addEventListener("click", function(e) {
                        inp.value = this.getElementsByTagName("input")[0].value;
                        getVehicleDetails(this.getElementsByTagName("input")[0].value);
                        closeAllLists();
                    });
                    // a.appendChild(b);
                }
            }
        } else {
            b = document.createElement("DIV");
            b.innerHTML = "<strong>Add to list</strong>";
            b.innerHTML += "<input type='hidden' value='" + val.toUpperCase() + "'>";
            b.addEventListener("click", function(e) {
                inp.value = this.getElementsByTagName("input")[0].value;
                getVehicleDetails(this.getElementsByTagName("input")[0].value);
                closeAllLists();
            });
            // a.appendChild(b);
        }
    });
    inp.addEventListener("input", function(e) {
        var a, b, i, val = this.value;
         closeAllLists();
        if (!val) { return false;}
        currentFocus = -1;
        a = document.createElement("DIV");
        a.setAttribute("id", this.id + "autocomplete-list");
        a.setAttribute("class", "autocomplete-items");
        this.parentNode.appendChild(a);
        if ((arr != null) && (arr.length !== 0)) {
            for (i = 0; i < arr.length; i++) {
                if (arr[i]['value'].substr(0, val.length).toUpperCase() == val.toUpperCase()) {
                    b = document.createElement("DIV");
                    b.innerHTML = "<strong>" + arr[i]['value'].substr(0, val.length) + "</strong>";
                    b.innerHTML += arr[i]['value'].substr(val.length);
                    b.innerHTML += "<input type='hidden' data-key='"+ arr[i]['key'] +"' value='" + arr[i]['value'] + "'>";
                    b.addEventListener("click", function(e) {
                        inp.value = this.getElementsByTagName("input")[0].value;
                        if (detailType == 'customer') {
                            getVehicleDetails(this.getElementsByTagName("input")[0].value, detailType, this.getElementsByTagName("input")[0].dataset.key);
                        } else {
                            getVehicleDetails(this.getElementsByTagName("input")[0].value, detailType);
                        }
                        closeAllLists();
                    });
                    a.appendChild(b);
                } else {
                    if (flag == "addTo") {
                        window.setTimeout(function() {
                            if (a.children.length == 0) {
                                b = document.createElement("DIV");
                                b.innerHTML = "<strong>Add to list</strong>";
                                if (detailType == 'customer') {
                                    b.innerHTML += "<input type='hidden' value='" + val + "'>";
                                } else {
                                    b.innerHTML += "<input type='hidden' value='" + val.toUpperCase() + "'>";
                                }
                                b.addEventListener("click", function(e) {
                                    inp.value = this.getElementsByTagName("input")[0].value;
                                    //getVehicleDetails(this.getElementsByTagName("input")[0].value);
                                    if (detailType == 'vehicle') {
                                        getVehicleDetails(this.getElementsByTagName("input")[0].value);
                                    }
                                    if (detailType != 'vehicle') {
                                        isNewCustomerDetail = true;
                                    }
                                    // clear drop downs for new customer
                                    autocomplete(document.getElementById("customer"), [ {'key':0,'value':''}], 'customer');
                                    autocomplete(document.getElementById("mobileNo"), [ {'key':0,'value':''}], 'customer', 'customerMob');
                                    autocomplete(document.getElementById("emailAddress"), [ {'key':0,'value':''}], 'customer', 'customerMob');
                                    autocomplete(document.getElementById("vehicleRegNo"), [ {'key':0,'value':''}]);
                                    closeAllLists();
                                });
                                a.appendChild(b);
                            }
                        }, 100);
                    }
                } 
            }
        } else {
            if (flag == "addTo") {
                b = document.createElement("DIV");
                b.innerHTML = "<strong>Add to list</strong>";
                if (detailType == 'customer') {
                    b.innerHTML += "<input type='hidden' value='" + val + "'>";
                } else {
                    b.innerHTML += "<input type='hidden' value='" + val.toUpperCase() + "'>";
                }
                b.addEventListener("click", function(e) {
                    inp.value = this.getElementsByTagName("input")[0].value;
                    getVehicleDetails(this.getElementsByTagName("input")[0].value);
                    closeAllLists();
                });
                a.appendChild(b);
            }
        }
    });
    inp.addEventListener("keydown", function(e) {
        var x = document.getElementById(this.id + "autocomplete-list");
        if (x) x = x.getElementsByTagName("div");
        if (e.keyCode == 40) {
            currentFocus++;
            addActive(x);
        } else if (e.keyCode == 38) { //up
            currentFocus--;
            addActive(x);
        } else if (e.keyCode == 13) {
            e.preventDefault();
            if (currentFocus > -1) {
                if (x) x[currentFocus].click();
            }
          }
    });
      
    function addActive(x) {
        if (!x) return false;
        removeActive(x);
        if (currentFocus >= x.length) currentFocus = 0;
        if (currentFocus < 0) currentFocus = (x.length - 1);
        x[currentFocus].classList.add("autocomplete-active");

        var target = document.querySelector('.autocomplete-active');
        target.parentNode.scrollTop = target.offsetTop;
    }
      
    function removeActive(x) {
        for (var i = 0; i < x.length; i++) {
          x[i].classList.remove("autocomplete-active");
        }
    }
      
    function closeAllLists(elmnt) {
        var x = document.getElementsByClassName("autocomplete-items");
        for (var i = 0; i < x.length; i++) {
            if (elmnt != x[i] && elmnt != inp) {
                x[i].parentNode.removeChild(x[i]);
            }
        }
    }

    document.addEventListener("click", function (e) {
        closeAllLists(e.target);
    });
}


function autocompleteModal(inp, arr)
{
    var currentFocus;
    inp.addEventListener("focusin", function(e) {
        var a, b, i, val = this.value;
        closeAllLists();
        currentFocus = -1;
        a = document.createElement("DIV");
        a.setAttribute("id", this.id + "autocomplete-list2");
        a.setAttribute("class", "autocomplete-items2");
        this.parentNode.appendChild(a);
        if (arr != null) {
            for (i = 0; i < arr.length; i++) {
                if (arr[i].text.substr(0, val.length).toUpperCase() == val.toUpperCase()) {
                    b = document.createElement("DIV");
                    b.innerHTML = "<strong>" + arr[i].text.substr(0, val.length) + "</strong>";
                    b.innerHTML += arr[i].text.substr(val.length);
                    b.innerHTML += "<input type='hidden' value='" + arr[i].text + "' data-id='"+arr[i].value+"'>";
                    b.addEventListener("click", function(e) {
                        var modalValue = this.getElementsByTagName("input")[0].dataset.id;
                        inp.value = this.getElementsByTagName("input")[0].value;
                        $('#vehicleModalValue').val(modalValue);
                        closeAllLists();
                    });
                    a.appendChild(b);
                }
            }
        } else {
            b = document.createElement("DIV");
            b.innerHTML = "<strong>Add to list</strong>";
            b.innerHTML += "<input type='hidden' value='" + val + "'>";
            b.addEventListener("click", function(e) {
                inp.value = this.getElementsByTagName("input")[0].value;
                $('#vehicleModalValue').val('new');
                closeAllLists();
            });
            a.appendChild(b);
        }
    });
    inp.addEventListener("input", function(e) {
        var a, b, i, val = this.value;
        closeAllLists();
        if (!val) { return false;}
        currentFocus = -1;
        a = document.createElement("DIV");
        a.setAttribute("id", this.id + "autocomplete-list2");
        a.setAttribute("class", "autocomplete-items2");
        this.parentNode.appendChild(a);
        if (arr != null) {

            for (i = 0; i < arr.length; i++) {
                if (arr[i].text.substr(0, val.length).toUpperCase() == val.toUpperCase()) {
                    b = document.createElement("DIV");
                    b.innerHTML = "<strong>" + arr[i].text.substr(0, val.length) + "</strong>";
                    b.innerHTML += arr[i].text.substr(val.length);
                    b.innerHTML += "<input type='hidden' value='" + arr[i].text + "' data-id='"+arr[i].value+"'>";
                    b.addEventListener("click", function(e) {
                        var modalValue = this.getElementsByTagName("input")[0].dataset.id;
                        inp.value = this.getElementsByTagName("input")[0].value;
                        $('#vehicleModalValue').val(modalValue);
                        closeAllLists();
                    });
                    a.appendChild(b);
                } else {
                    window.setTimeout(function() {
                        if (a.children.length == 0) {
                            b = document.createElement("DIV");
                            b.innerHTML = "<strong>Add to list</strong>";
                            b.innerHTML += "<input type='hidden' value='" + val + "'>";
                            b.addEventListener("click", function(e) {
                                inp.value = this.getElementsByTagName("input")[0].value;
                                $('#vehicleModalValue').val('new');
                                closeAllLists();
                            });
                            a.appendChild(b);
                        }
                    }, 100);
                } 
            }
        } else {
            b = document.createElement("DIV");
            b.innerHTML = "<strong>Add to list</strong>";
            b.innerHTML += "<input type='hidden' value='" + val + "'>";
            b.addEventListener("click", function(e) {
                inp.value = this.getElementsByTagName("input")[0].value;
                $('#vehicleModalValue').val('new');
                closeAllLists();
            });
            a.appendChild(b);
        }
    });
    inp.addEventListener("keydown", function(e) {
        var x = document.getElementById(this.id + "autocomplete-list2");
        if (x) x = x.getElementsByTagName("div");
        if (e.keyCode == 40) {
            currentFocus++;
            addActive(x);
        } else if (e.keyCode == 38) { //up
            currentFocus--;
            addActive(x);
        } else if (e.keyCode == 13) {
            e.preventDefault();
            if (currentFocus > -1) {
                if (x) x[currentFocus].click();
            }
          }
    });
      
    function addActive(x) {
        if (!x) return false;
        removeActive(x);
        if (currentFocus >= x.length) currentFocus = 0;
        if (currentFocus < 0) currentFocus = (x.length - 1);
        x[currentFocus].classList.add("autocomplete-active");

        var target = document.querySelector('.autocomplete-active');
        target.parentNode.scrollTop = target.offsetTop;
    }
      
    function removeActive(x) {
        for (var i = 0; i < x.length; i++) {
          x[i].classList.remove("autocomplete-active");
        }
    }
      
    function closeAllLists(elmnt) {
        var x = document.getElementsByClassName("autocomplete-items2");
        for (var i = 0; i < x.length; i++) {
            if (elmnt != x[i] && elmnt != inp) {
                x[i].parentNode.removeChild(x[i]);
            }
        }
    }

    document.addEventListener("click", function (e) {
        closeAllLists(e.target);
    });

    var isPhoneValid = function(phoneNumber) {
        var isValid = true;
        if (phoneNumber.length != 0) {
            if (isNaN(phoneNumber) || phoneNumber.length < 9 || phoneNumber.length > 15) {
                isValid = false;
            }
        }
        return isValid;
    }
}