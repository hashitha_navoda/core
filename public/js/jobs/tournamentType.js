var projectTypeID = '';
var editMode = false;
$(function() {
    $('#add').on('click', function(e) {
        e.preventDefault();
        var formData = {
            projectTypeCode: $('.projectTypeCode').val(),
            projectTypeName: $('.projectTypeName').val(),
            editMode: editMode,
            projectTypeID: projectTypeID,
            tournamentFlag: true
        };
        if (validateFormData(formData)) {
            eb.ajax({
                url: BASE_URL + '/project_setup_api/add',
                method: 'post',
                data: formData,
                dataType: 'json',
                success: function(data) {
                    p_notification(data.status, data.msg);
                    if (data.status) {
                        window.setTimeout(function() {
                            location.reload();
                        }, 1000);
                    } else {
                        if (data.data == "PTCERR") {
                            $('#projectTypeCode').focus();
                        }
                    }
                }
            });
        }
    });

    $('#updateType').on('click', function(e) {
        e.preventDefault();
        var formData = {
            projectTypeCode: $('.projectTypeCode').val(),
            projectTypeName: $('.projectTypeName').val(),
            editMode: editMode,
            projectTypeID: projectTypeID,
            tournamentFlag: true
        };
        if (validateFormData(formData)) {
            eb.ajax({
                url: BASE_URL + '/project_setup_api/add',
                method: 'post',
                data: formData,
                dataType: 'json',
                success: function(data) {
                    p_notification(data.status, data.msg);
                    if (data.status) {
                        window.setTimeout(function() {
                            location.reload();
                        }, 1000);
                    } else {
                        if (data.data == "PTCERR") {
                            $('#projectTypeCode').focus();
                        }
                    }
                }
            });
        }
    });

    $('#project-type-search').on('submit', function(e) {
        e.preventDefault();
        var formData = {
            projectTypeSearchKey: $('#project-type-search-keyword').val(),
        };
        if (formData.projectTypeSearchKey != '') {
            searchValue(formData);
        } else {
            p_notification(false, eb.getMessage('ERR_PROTYPE_SEARCH_KEY'));
        }
    });

    $('#project-type-search').on('click', '.reset', function(e) {
        e.preventDefault();
        searchValue('');
        $('#project-type-search-keyword').val('');
    });

    $('#project-type-list').on('click', 'a.edit', function(e) {
        e.preventDefault();
        projectTypeID = $(this).parents('tr').data('projecttypeid');
        $('.projectTypeCode').val($(this).parents('tr').find('.PTC').text()).attr('disabled', true);
        $('.projectTypeName').val($(this).parents('tr').find('.PTN').text());
        $('.updateTitle').removeClass('hidden');
        $('.addTitle').addClass('hidden');
        $('.update').removeClass('hidden');
        $('.add').addClass('hidden');
        editMode = true;
    });

    $('#project-type-list').on('click', 'a.status', function() {
        var msg;
        var status;
        var flag = true;
        var projectTypeID = $(this).parents('tr').data('projecttypeid');
        var currentDiv = $(this).contents();
        if ($(this).children().hasClass('fa-check-square-o')) {
            msg = 'Are you sure you want to Deactivate this tournament Type';
            status = '0';
        }
        else if ($(this).children().hasClass('fa-square-o')) {
            msg = 'Are you sure you want to Activate this tournament type';
            status = '1';
        }
        activeInactiveType(projectTypeID, status, currentDiv, msg, flag);
    });

    $('#reset').on('click', function(e) {
        e.preventDefault();
        projectTypeID = '';
        $('.projectTypeCode').val('').attr('disabled', false);
        $('.projectTypeName').val('');
        $('.updateTitle').addClass('hidden');
        $('.addTitle').removeClass('hidden');
        $('.update').addClass('hidden');
        $('.add').removeClass('hidden');
        editMode = false;
    });

    $('#resetUp').on('click', function(e) {
        e.preventDefault();
        projectTypeID = '';
        $('.projectTypeCode').val('').attr('disabled', false);
        $('.projectTypeName').val('');
        $('.updateTitle').addClass('hidden');
        $('.addTitle').removeClass('hidden');
        $('.update').addClass('hidden');
        $('.add').removeClass('hidden');
        editMode = false;
    });

    $('#project-type-list').on('click', 'a.delete', function(e) {
        e.preventDefault();
        $('.projectTypeCode').val('').attr('disabled', false);
        $('.projectTypeName').val('');
        $('.updateTitle').addClass('hidden');
        $('.addTitle').removeClass('hidden');
        $('.update').addClass('hidden');
        $('.add').removeClass('hidden');
        editMode = false;

        var projectTypeID = $(this).parents('tr').data('projecttypeid');
        var currentDiv = $(this).parents('tr').find('a.status').contents();
        var projectTypeCode = $(this).parents('tr').find('.PTC').text();
        var flag = false;
        var msg = 'This tournament type cannot be deleted. Do you want to de-activate this tournament type';
        bootbox.confirm('Are you sure you want to delete this Tournament Type?', function(result) {
            if (result === true) {
                eb.ajax({
                    url: BASE_URL + '/project_setup_api/deleteProjectTypeByProjectTypeID',
                    method: 'post',
                    data: {
                        projectTypeID: projectTypeID,
                        projectTypeCode: projectTypeCode,
                        tournamentFlag: true
                    },
                    dataType: 'json',
                    success: function(data) {
                        if (data.status == true) {
                            p_notification(data.status, data.msg);
                            window.location.reload();
                        }
                        else if (data.status == false && data.data == 'deactive') {
                            //this condition use,when selected type cannot delete
                            activeInactiveType(projectTypeID, '0', currentDiv, msg, flag);
                        }
                        else {
                            p_notification(data.status, data.msg);
                        }
                    }
                });
            }
        });

    });

    function validateFormData(formData) {
        console.log(formData);
        if (formData.projectTypeCode == "") {
            p_notification(false, eb.getMessage('ERR_TOUR_TYPE_CODE_CNT_BE_NULL'));
            $('#projectTypeCode').focus();
            return false;
        } else if (formData.projectTypeName == '') {
            p_notification(false, eb.getMessage('ERR_TOUR_TYPE_NAME_CNT_BE_NULL'));
            $('#projectTypeName').focus();
            return false;
        }
        return true;
    }

    function searchValue(formData) {
        eb.ajax({
            url: BASE_URL + '/project_setup_api/getProjectTypesBySearchKey',
            method: 'post',
            data: formData,
            dataType: 'json',
            success: function(data) {
                if (data.status) {
                    $("#project-type-list").html(data.html);
                } else {
                    p_notification(data.status, data.msg);
                }
            }
        });
    }

    /**
     *
     * use to change active state
     */
    function activeInactiveType(type, status, currentDiv, msg, flag) {
        bootbox.confirm(msg, function($result) {
            if ($result == true) {
                eb.ajax({
                    url: BASE_URL + '/project_setup_api/changeStatusID',
                    method: 'post',
                    dataType: 'json',
                    data: {
                        'projectTypeID': type,
                        'status': status,
                        tournamentFlag: true
                    },
                    success: function(data) {
                        if (data.status) {
                            p_notification(data.status, data.msg);
                            if (currentDiv.hasClass('fa-square-o') && flag) {
                                currentDiv.addClass('fa-check-square-o');
                                currentDiv.removeClass('fa-square-o');
                            }
                            else {
                                currentDiv.addClass('fa-square-o');
                                currentDiv.removeClass('fa-check-square-o');
                            }
                        } else {
                            p_notification(data.status, data.msg);
                        }
                    },
                    sync: 'false'
                });
            }
        });
    }
});