$(document).ready(function() {
    $('.jEmployees').on('click', function(event) {
        event.preventDefault();
        $("#viewServiceEmployeeModal #serviceEmployeesTable tbody .addedEmployees").remove();
        eb.ajax({
            url: BASE_URL + '/api/serviceCost/getJobEmployeeByJobId',
            method: 'post',
            data: {jobID:$(this).attr('id')},
            dataType: 'json',
            success: function(respond) {
                if (respond.status) {
                    $.each(respond.data, function(index, val) {
                        var newTrID = 'tr_' + val.jobTaskID;
                        var clonedRow = $($('#preSample').clone()).attr('id', newTrID).addClass('addedEmployees');
                        if (val.employeeCode != null) {
                            $("#employeeCode", clonedRow).text(val.employeeCode);
                            $("#employeeName", clonedRow).text(val.employeeName);
                        } else {
                            $("#employeeName", clonedRow).text("Default Employee");
                        }
                        clonedRow.removeClass('hidden');
                        clonedRow.insertBefore('#preSample');
                    });
                }
            }
        });  
        $('#viewServiceEmployeeModal').modal('show');
    })
});