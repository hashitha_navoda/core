$(document).ready(function(){
    
    //vehicle add form submission
    $("form#addVehicleForm").submit(function(e){
        e.preventDefault();
        var vehicleForm = $('#addVehicleForm');
        var editMode = vehicleForm.find('#editMode').val();
        var vehicleid = vehicleForm.find('#editMode').attr('vehicleid');
        var entityid = vehicleForm.find('#editMode').attr('entityid');
        var formData = {
            vehicleName: vehicleForm.find('#vehicleName').val(),
            vehicleCode: vehicleForm.find('#vehicleCode').val(),
            vehicleRegistrationNumber: vehicleForm.find('#registrationNo').val(),
            vehicleCost: vehicleForm.find('#costPerKM').val(),
        };

        if (editMode === 'false') {
            var url = BASE_URL + '/api/vehicle/add-vehicle';
        } else {
            var url = BASE_URL + '/api/vehicle/edit-vehicle/' + vehicleid + '/' + entityid;
        }

        if (vehicleFormDataValidation(formData)) {
            eb.ajax({
                url: url,
                method: 'post',
                data: formData,
                dataType: 'json',
                success: function(data) {
                    p_notification(data.status, data.msg);
                    if (data.status) {
                        window.setTimeout(function() {
                            location.reload();
                        }, 1000);
                    } else {
                        $('#registrationNo').focus();
                    }
                }
            });
        }
    });


    //when reset button click in edit mode on vehicle add form
    $('form#addVehicleForm').on('click', 'button#btnResetNewVehicle', function(e) {
        var vehicleForm = $('#addVehicleForm');
        var editMode = vehicleForm.find('#editMode').val();
        if (editMode) {
            vehicleForm.find('#addVehicleTitle').removeClass('hidden');
            vehicleForm.find('#editVehicleTitle').addClass('hidden');
            vehicleForm.find('#btnAddNewVehicle').text('Add New');
            vehicleForm.find('#editMode').val('false');
            vehicleForm.find('#editMode').removeAttr('vehicleID');
            vehicleForm.find('#editMode').removeAttr('entityId');
            vehicleForm.find('#vehicleCode').removeAttr('disabled');
        }
    });

    //vehicle search form submission
    $("form#vehicleSearchForm").submit(function(e) {
        e.preventDefault();
        var keyword = $('#vehicleSearchKey').val();
        var param = {searchKey: keyword};
        getViewAndLoad(BASE_URL + '/api/vehicle/search-vehicles', 'vehiclesListDiv', param);
    });

    //vehicle search form reset
    $("form#vehicleSearchForm").on('reset', function(e) {
        e.preventDefault();
        $("#vehicleSearchKey").val('');
        $("form#vehicleSearchForm").trigger("submit");
    });

    //vehicle delete in list
    $('#vehiclesListBody').on('click', 'button.delete', function(e) {
        e.preventDefault();
        
        var tr = $(this).parents('tr');
        var vehicleID = tr.attr('vehicleid');
        var vehicleStatus = tr.attr('vehiclestatus');
        var entityId = tr.attr('entityid');
        var vehicleCode = tr.find('#vehicleCode').text();
        
        bootbox.confirm('Are you sure you want to delete this vehicle (' + vehicleCode + ') ?', function(result) {
            if (result === true) {
                eb.ajax({
                    url: BASE_URL + '/api/vehicle/delete-vehicle',
                    method: 'post',
                    data: {
                        vehicleID: vehicleID,
                        entityId: entityId,
                    },
                    dataType: 'json',
                    success: function(data) {
                        p_notification(data.status, data.msg);
                        if (data.status) {
                            tr.remove();
                        } else {
                            if (vehicleStatus == 1) {
                                tr.find('button.changeActivation').trigger( "click" );
                            }
                        }
                    }
                });
            }
        });
    });

    //vehicle status change in list
    $('#vehiclesListBody').on('click', 'button.changeActivation', function(e) {
        e.preventDefault();
        
        var tr = $(this).parents('tr');
        var vehicleID = tr.attr('vehicleid');
        var vehicleStatus = tr.attr('vehiclestatus');
        var entityId = tr.attr('entityid');
        var vehicleCode = tr.find('#vehicleCode').text();
        
        bootbox.confirm('Are you sure you want to change the status of this vehicle (' + vehicleCode + ') ?', function(result) {
            if (result === true) {
                eb.ajax({
                    url: BASE_URL + '/api/vehicle/change-status-vehicle',
                    method: 'post',
                    data: {
                        vehicleID: vehicleID,
                        vehicleStatus: vehicleStatus,
                        entityId: entityId,
                    },
                    dataType: 'json',
                    success: function(data) {
                        p_notification(data.status, data.msg);
                        tr.attr('vehiclestatus', data.data.vehicleStatus);
                        if (data.data.vehicleStatus == 1) {
                            tr.find('.changeActivation').html('<span class="changeActivation fa fa-check-square-o custom_font_size"></span>');;
                        } else if (data.data.vehicleStatus == 2) {
                            tr.find('.changeActivation').html('<span class="fa fa-square-o custom_font_size"></span>');;
                        } else {
                            window.setTimeout(function() {
                                location.reload();
                            }, 1000);
                        }
                    }
                });
            }
        });
    });

    //vehicle edit in list
    $('#vehiclesListBody').on('click', 'button.edit', function(e) {
        e.preventDefault();
        
        var tr = $(this).parents('tr');
        var vehicleID = tr.attr('vehicleid');
        var entityId = tr.attr('entityid');
        var crntVehicleCode = tr.find('#vehicleCode').text();
        var crntVehicleName = tr.find('#vehicleName').text();
        var crntRegistrationNo = tr.find('#vehicleNumber').text();
        var crntCostPerKM = tr.find('#costPerKM').text();
        
        var vehicleForm = $('#addVehicleForm');
        $('html, body').animate({ scrollTop: $("#addVehicleForm").offset().top }, 500);

        vehicleForm.find('#addVehicleTitle').addClass('hidden');
        vehicleForm.find('#editVehicleTitle').removeClass('hidden');
        vehicleForm.find('#btnAddNewVehicle').text('Update');
        vehicleForm.find('#editMode').val('true');
        vehicleForm.find('#editMode').attr('vehicleID', vehicleID);
        vehicleForm.find('#editMode').attr('entityId', entityId);
        vehicleForm.find('#vehicleCode').val(crntVehicleCode);
        vehicleForm.find('#vehicleCode').attr('disabled', true);
        vehicleForm.find('#vehicleName').val(crntVehicleName);
        vehicleForm.find('#registrationNo').val(crntRegistrationNo);
        vehicleForm.find('#costPerKM').val(crntCostPerKM);

        
    });

});

//vehicle add/edit form validation
function vehicleFormDataValidation(data) {
    var cost = /^\d*\.?\d*$/;      
    if (data.vehicleCode == '') {
        p_notification(false, eb.getMessage('ERR_VEHICLES_SETUP_CODE'));
        $('#vehicleCode').focus();
        return false;
    } else if (data.vehicleName == '') {
        p_notification(false, eb.getMessage('ERR_VEHICLES_SETUP_NAME'));
        $('#vehicleName').focus();
        return false;
    } else if (data.vehicleRegistrationNumber == '') {
        p_notification(false, eb.getMessage('ERR_VEHICLES_SETUP_REGNO'));
        $('#registrationNo').focus();
        return false;
    } else if (data.vehicleCost == '' || !(data.vehicleCost.match(cost))) {
        p_notification(false, eb.getMessage('ERR_VEHICLES_SETUP_COST'));
        $('#costPerKM').focus();
        return false;   
    } else {
        return true;
    }
}
