$(function() {
    $('#job-search').on('submit', function(e) {
        e.preventDefault();
        var formData = {
            jobSearchKey: $('#job-search-keyword').val(),
        };
        if (formData.jobSearchKey != '') {
            searchValue(formData);
        } else {
            p_notification(false, eb.getMessage('ERR_JBTYPE_SEARCH_KEY'));
        }
    });

    $('#job-search').on('click', '.reset', function(e) {
        e.preventDefault();
        searchValue('');
        $('#job-search-keyword').val('');
    });

    function searchValue(formData) {
        eb.ajax({
            url: BASE_URL + '/event-api/getJobBySearchKey',
            method: 'post',
            data: formData,
            dataType: 'json',
            success: function(data) {
                if (data.status) {
                    $("#search-job-list").html(data.html);
                } else {
                    p_notification(data.status, data.msg);
                }
            }
        });
    }
    
    $( "#projectCode, #jobSupervisorSearch, #jobStatus, #job-search-keyword").change(function() {
        var formData = {
            jobSearchKey: $('#job-search-keyword').val(),
            projectID:($('#projectCode').val() != 0 && $('#projectCode').val() != null) ? $('#projectCode').val() : null,
            jobSupervisorID: ($('#jobSupervisorSearch').val() != 0 && $('#jobSupervisorSearch').val() != null) ? $('#jobSupervisorSearch').val() : null,
            jobStatusID: ($('#jobStatus').val() != 0 && $('#jobStatus').val() != null) ? $('#jobStatus').val() : null
        }
        searchValue(formData);
    });
});