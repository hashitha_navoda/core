$(function() {
    $('#job-search').on('submit', function(e) {
        e.preventDefault();
        var formData = {
            jobSearchKey: $('#job-search-keyword').val(),
        };
        if (formData.jobSearchKey != '') {
            searchValue(formData);
        } else {
            p_notification(false, eb.getMessage('ERR_JBTYPE_SEARCH_KEY'));
        }
    });

    $('#job-search').on('click', '.reset', function(e) {
        e.preventDefault();
        searchValue('');
        $('#job-search-keyword').val('');
    });

    function searchValue(formData) {
        eb.ajax({
            url: BASE_URL + '/job-api/getJobBySearchKey',
            method: 'post',
            data: formData,
            dataType: 'json',
            success: function(data) {
                if (data.status) {
                    $("#job-list").html(data.html);
                } else {
                    p_notification(data.status, data.msg);
                }
            }
        });
    }
    
    $( "#projectCode, #jobSupervisorSearch, #jobStatus, #job-search-keyword").change(function() {
        var formData = {
            jobSearchKey: $('#job-search-keyword').val(),
            projectID:($('#projectCode').val() != 0 && $('#projectCode').val() != null) ? $('#projectCode').val() : null,
            jobSupervisorID: ($('#jobSupervisorSearch').val() != 0 && $('#jobSupervisorSearch').val() != null) ? $('#jobSupervisorSearch').val() : null,
            jobStatusID: ($('#jobStatus').val() != 0 && $('#jobStatus').val() != null) ? $('#jobStatus').val() : null
        }
        searchValue(formData);
    });

    $('.jobSupervisor').on('click', function(event) {
        event.preventDefault();
        $("#viewJobSupervisorModal #jobSupervisorTable tbody .addedSupervisors").remove();
        eb.ajax({
            url: BASE_URL + '/job-api/getJobSupervisorsByJobID',
            method: 'post',
            data: {jobID:$(this).attr('id')},
            dataType: 'json',
            success: function(respond) {
                if (respond.status) {
                    $.each(respond.data, function(index, val) {
                        var newTrID = 'tr_' + val.jobSupervisorID;
                        var clonedRow = $($('#preSample').clone()).attr('id', newTrID).addClass('addedSupervisors');
                        $("#supervisorName", clonedRow).text(val.employeeFirstName+" "+val.employeeSecondName);
                        clonedRow.removeClass('hidden');
                        clonedRow.insertBefore('#preSample');
                    });
                }
            }
        });  
        $('#viewJobSupervisorModal').modal('show');
    });


    $('.delete-job').on('click', function(e) {
        e.preventDefault();

        var jobID = $(this).parents('tr').data('jobid');
        bootbox.confirm('Are you sure you want to delete this Job?', function(result) {
            if (result === true) {
                eb.ajax({
                    url: BASE_URL + '/job-api/delete',
                    method: 'post',
                    data: {
                        jobID: jobID,
                    },
                    dataType: 'json',
                    success: function(data) {
                        if (data.status == true) {
                            p_notification(data.status, data.msg);
                            window.location.reload();
                        } else {
                            p_notification(data.status, data.msg);
                        }
                    }
                });
            }
        });

    });
});