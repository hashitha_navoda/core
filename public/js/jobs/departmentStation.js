$(document).ready(function() {

    var departmentId   = null;
    var departmentName = null;
    var departmentCode = null;
    var departmentStatus = null;
    var departmentStationCode = null;
    var departmentStationName = null;
    var departmentStations = {};
    var mainDepartmentStations = {};
    var alldepartmentStations = {};

    getRelatedDepartmentStations(null);
    
    $('#btnAdd').on('click',function(){
        var departmentCode = $('#departmentCode').val();
        var departmentName = $('#departmentName').val();
    

        if(departmentCode.trim() && departmentName.trim()){
            eb.ajax({
                type: 'POST',
                url: BASE_URL + '/departmentStation-api/createDepartment',
                data: {
                    departmentCode : departmentCode,
                    departmentName : departmentName,
                    departmentStations: mainDepartmentStations
                },
                success: function(respond) {
                    p_notification(respond.status, respond.msg);                
                    if(respond.status){
                        $('#departmentCode').val('');
                        $('#departmentName').val('');
                        window.setTimeout(function() {
                                location.reload();
                        }, 1000);                    
                    }                
                }
            });
        } else {
            p_notification(false, eb.getMessage('ERR_DEPARTMENT_REQUIRED'));
        }
    });

    //for cancel btn
    $('#btnCancel').on('click',function (){
        $('input[type=text]').val('');        
        departmentRegisterView();        
    });

    $('#department-list').on('click','.department-action',function (e){    
        var action = $(this).data('action');
        departmentId   = $(this).closest('tr').data('department-id');
        departmentName = $(this).closest('tr').data('department-name');
        departmentCode = $(this).closest('tr').data('department-code');
        departmentStatus = $(this).closest('tr').data('department-status');

        switch(action) {
            case 'edit':
                e.preventDefault();
                departmentUpdateView();
                //set selected department details
                $('#departmentCode').val(departmentCode);
                $('#departmentName').val(departmentName);
                getRelatedDepartmentStations(departmentId);

                break;
            case 'delete':
                deleteDepartment(departmentId);
                break;
            case 'view-satations':
                viewRelatedDepartmentStations(departmentId)
                break;
            case 'status':
                var msg;
                var status;
                var currentDiv = $(this).contents();
                var flag = true;
                if ($(this).children().hasClass('fa-check-square-o')) {
                    msg = 'Are you sure you want to Deactivate this department';
                    status = '0';
                }
                else if ($(this).children().hasClass('fa-square-o')) {
                    msg = 'Are you sure you want to Activate this department';
                    status = '1';
                }
                activeInactive(departmentId, status, currentDiv, msg, flag);
                break;
            default:
                console.error('Invalid action.');
                break;
        }
    });

    //this function for switch to department register view
    function departmentRegisterView(){
       $('.updateTitle').addClass('hidden');
       $('.addTitle').removeClass('hidden');
       $('.updateDiv').addClass('hidden');
       $('.addDiv').removeClass('hidden');

       $('#addDepartmentServiceModal #addDepartmentStationBody .addedDepartmentStation').remove();
        mainDepartmentStations = {};
        departmentStations = {};
    }

    //this function for switch to department update view
    function departmentUpdateView(){
       $('.addTitle').addClass('hidden');
       $('.updateTitle').removeClass('hidden');
       $('.addDiv').addClass('hidden');
       $('.updateDiv').removeClass('hidden');
    }

    $('#btnUpdate').on('click',function(){
        var departmentCode = $('#departmentCode').val();
        var departmentName = $('#departmentName').val();
        
        if(departmentCode.trim() && departmentName.trim()){
            eb.ajax({
                type: 'POST',
                url: BASE_URL + '/departmentStation-api/update',
                data: {
                    departmentCode : departmentCode,
                    departmentName : departmentName,
                    departmentID: departmentId,
                    departmentStations: mainDepartmentStations
                },
                success: function(respond) {
                    p_notification(respond.status, respond.msg);                
                    if(respond.status){
                        $('#departmentCode').val('');
                        $('#departmentName').val('');
                        window.setTimeout(function() {
                                location.reload();
                        }, 1000);                  
                    }                
                }
            });
        } else {
            p_notification(false, eb.getMessage('ERR_DEPARTMENT_REQUIRED'));
        }
    });

    //for search
    $('#searchDept').on('click', function() {
        var key = $('#departmentSearch').val();

        if (key.trim()) {
            $.ajax({
                type: 'POST',
                url: BASE_URL + '/departmentStation-api/search',
                data: {
                    searchKey : key
                },
                success: function(data) {
                    if (data.status == true) {
                        if (data.status) {
                            $("#department-list").html(data.html);
                        } else {
                            p_notification(data.status, data.msg);
                        }
                    }            
                }
            });
        } else {
            p_notification(false, eb.getMessage('ERR_DEPT_SEARCH_KEY'));
        }
    });

    //for rest btn
    $('#btnReset').on('click',function (){
        $('input[type=text]').val('');
        $('#addDepartmentServiceModal #addDepartmentStationBody .addedDepartmentStation').remove();
        mainDepartmentStations = {};
        departmentStations = {};
    });  

    //for search cancel btn
    $('#searchCancel').on('click',function (){
        $('#departmentSearch').val('');
        fetchAll();
    });

    //this function for fetach all the departments
    function fetchAll(){
        $.ajax({
            type: 'POST',
            data: {
                searchKey: ''
            },
            url: BASE_URL + '/departmentStation-api/search',
            success: function(respond) {
                $('#department-list').html(respond.html);            
            }
        });
    }

    function deleteDepartment(departmentId) {
        bootbox.confirm('Are you sure you want to delete this Department?', function($result) {
            if ($result == true) {
                eb.ajax({
                    url: BASE_URL + '/departmentStation-api/deleteDepartment',
                    method: 'post',
                    data: {
                        departmentID: departmentId
                    },
                    dataType: 'json',
                    success: function(data) {
                        if (data.status == true) {
                            p_notification(data.status, data.msg);
                            window.location.reload();
                        } else {
                            p_notification(data.status, data.msg);
                        }
                    }
                });
            }
        });
    }

    $('#departmentStations').on('click', function() {
        reomoveUnAddedDepartmentStations();
    });

     $('#btnDepStatClose').on('click',function(){    
        reomoveUnAddedDepartmentStations();
    });

    //add new project manager
    $('#add_dep_station').on('click', function(e) {
        e.preventDefault();

        //if plus button is clicked then disabled the button
        //if button is clicked more than one, then it does not continue

        if ($(this).is(':disabled')) {
            return false;
        } else {
            $(this).attr('disabled', true);
            var $thisRow = $(this).parents('tr');
            if (!$('#departmentStationCode').val().trim()) {
                // $('#addNewProjectManagerRow').val('');
                p_notification(false, eb.getMessage('ERR_REQUIRED_DEPT_STAT_CODE'));
                $(this).attr('disabled', false);
            } else if (!$('#departmentStationName').val().trim()) {
                // $('#addNewProjectManagerRow').val('');
                p_notification(false, eb.getMessage('ERR_REQUIRED_DEPT_STAT_NAME'));
                $(this).attr('disabled', false);
            } else {
                departmentStationCode = $('#departmentStationCode').val();
                departmentStationName = $('#departmentStationName').val();
                addNewDeptServiceRow(false);
                $(this).attr('disabled', false);
                
            }
        }
    });

    function addNewDeptServiceRow(autofill) {

        if ($('#mode').val() != 'view') {
            if (autofill == false) {
                if (departmentStationCode in departmentStations ) {
                    p_notification(false, eb.getMessage('ERR_SELECTED_DEP_STAT_DUPLICATE'));
                    return;
                }
                if (departmentStationCode in alldepartmentStations) {
                    p_notification(false, eb.getMessage('ERR_SELECTED_DEP_STAT_DUPLICATE'));
                    return;
                }
            }

            var departmentStationNameCode = departmentStationCode+' - '+departmentStationName;
            var departmentCodeIdTemp = departmentStationCode.replace(/\s/g, '');
            var newTrID = 'tr_'+departmentCodeIdTemp;
            var clonedRow = $($('#preSample').clone()).attr('id', newTrID).addClass('addedDepartmentStation');
            $("input[name='addDepStationName']", clonedRow).val(departmentStationName);
            $("input[name='addDepStationCode']", clonedRow).val(departmentStationCode);
            clonedRow.data('station-code', departmentStationCode);
            clonedRow.removeClass('hidden');
            clonedRow.insertBefore('#preSample');
            departmentStations[departmentStationCode] = departmentStationName;
            clearAddedNewRow();
            if (autofill) {
                $('#addDepartmentServiceModal #addDepartmentStationBody #formRow '+'#'+newTrID+' #deleteClonedStation').addClass('disabled');
            }
        } 
    }

    function clearAddedNewRow() {
        $('#departmentStationName').val('');
        $('#departmentStationCode').val('');
    }

    $('#btnAddDepStation').on('click',function(){
        if ($('#departmentStationCode').val().trim() || $('#departmentStationName').val().trim()) {
            p_notification(false, eb.getMessage('ERR_ADD_DEPT_STAT'));
            return;
        }

        addDepartmentServices();
        $("#addDepartmentServiceModal").modal('toggle');

    });

    //project managers footer add button
    function addDepartmentServices()
    {
        mainDepartmentStations = {};
        for (var key in departmentStations) {
            if (departmentStations.hasOwnProperty(key)) {
                mainDepartmentStations[key] = departmentStations[key];
            }
        }

    }

    function reomoveUnAddedDepartmentStations() {
        if ($('#mode').val() != 'view') {
            var temp1 = [];
            for (var key in mainDepartmentStations) {
                if (mainDepartmentStations.hasOwnProperty(key)) {
                    temp1.push(key);
                }
            }
            
            var temp2 = [];
            for (var key in departmentStations) {
                if (departmentStations.hasOwnProperty(key)) {
                    temp2.push(key);
                }
            }
            for (var i = 0; i < temp2.length; i++) {    
                if ($.inArray(temp2[i], temp1) == -1) {
                    delete departmentStations[temp2[i]];
                    $('#' + 'tr_'+temp2[i]).remove();
                }           
            }
            addDepartmentServices();
        }
        $("#addDepartmentServiceModal #addDepartmentStationBody").removeClass('hidden');
        $("#btnAddDepStation").removeClass('hidden');
        $("#btnDepStatClose").removeClass('hidden');
        $("#btnViewDeptStatClose").addClass('hidden');
        $("#addDepartmentServiceModal #viewDepStationBody").addClass('hidden');
        $("#addDepartmentServiceModal").modal('toggle');
    }

    $("#addDepartmentServiceModal").on('click','.deleteDepartmentStation', function(e) {
        e.preventDefault();

        var deleteDepStatTempID = $(this).closest('tr').attr('id');
        var deleteDepStatID = $(this).closest('tr').data('station-code');
        
        bootbox.confirm('Are you sure you want to remove this Station ?', function(result) {
            if (result == true) {
                delete departmentStations[deleteDepStatID];
                $('#' + deleteDepStatTempID).remove();
            }
        });
    });

    function getRelatedDepartmentStations(Id) {
        eb.ajax({
            type: 'POST', url: BASE_URL + '/department-api/getRelatedDepartmentStations', 
            data: {departmentID: Id}, 
            success: function(respond) {
                if (respond.status == true) {
                    if (Id == null) {
                        alldepartmentStations = respond.data;
                    } else {
                        $('#departmentStationTable #formRow .addedDepartmentStation').remove();
                        mainDepartmentStations = respond.data;
                        for (var key in respond.data) { 
                            departmentStationCode = key;
                            departmentStationName = respond.data[key];
                            addNewDeptServiceRow(true);
                        }
                    }


                } 
            }
        });
    }

    function viewRelatedDepartmentStations(Id) {
        eb.ajax({
            type: 'POST', url: BASE_URL + '/department-api/getStationByDepartmentId', 
            data: {departmentID: Id}, 
            success: function(respond) {
                if (respond.status == true) {
                    $("#addDepartmentServiceModal #addDepartmentStationBody").addClass('hidden');
                    $("#btnAddDepStation").addClass('hidden');
                    $("#btnDepStatClose").addClass('hidden');
                    $("#btnViewDeptStatClose").removeClass('hidden');
                    $("#addDepartmentServiceModal #viewDepStationBody").removeClass('hidden');
                    $("#addDepartmentServiceModal").modal('toggle');

                    $('#addDepartmentServiceModal #viewDepStationBody .viewedDepartmentStat').remove();

                    for (var i = 0; i < respond.data.length; i++) {
                        var departmentStationCodeIdTemp = respond.data[i]['code'].replace(/\s/g, '');
                        var newTrID = 'trs_'+departmentStationCodeIdTemp;
                        var clonedRow = $($('#depStatRow').clone()).attr('id', newTrID).addClass('viewedDepartmentStat');
                        $("#depStatCode", clonedRow).text(respond.data[i]['code']);
                        $("#depStatName", clonedRow).text(respond.data[i]['name']);
                        clonedRow.data('station-code', respond.data[i]['code']);
                        clonedRow.removeClass('hidden');
                        clonedRow.insertBefore('#depStatRow');
                        if (respond.data[i]['status'] == '1') {
                            $('#addDepartmentServiceModal #viewDepStationBody #viewNewDepStationRow '+'#'+newTrID+' .stationStatusIcon').addClass('fa-check-square-o');
                        } else {
                            $('#addDepartmentServiceModal #viewDepStationBody #viewNewDepStationRow '+'#'+newTrID+' .stationStatusIcon').addClass('fa-square-o');
                        }
                    }    
                } 
            }
        });
    }

    $('#depStationViewTable').on('click', '.stationStatus', function(){
        var relatedStationTempCode = $(this).closest('tr').attr('id');
        var relatedStationCode = $(this).closest('tr').data('station-code');
        if ($('#addDepartmentServiceModal #viewDepStationBody #viewNewDepStationRow '+'#'+relatedStationTempCode+' .stationStatusIcon').hasClass('fa-check-square-o')) {
            msg = 'Are you sure you want to Deactivate this station';
            status = '0';
        }
        else if ($('#addDepartmentServiceModal #viewDepStationBody #viewNewDepStationRow '+'#'+relatedStationTempCode+' .stationStatusIcon').hasClass('fa-square-o')) {
            msg = 'Are you sure you want to Activate this station';
            status = '1';
        }
        activeStatus(relatedStationTempCode, relatedStationCode, msg, status);
    });

    function activeStatus(relatedStationTempCode, relatedStationCode, msg, status) {
        var currentDiv = $('#addDepartmentServiceModal #viewDepStationBody #viewNewDepStationRow '+'#'+relatedStationTempCode+' .stationStatusIcon');
        bootbox.confirm(msg, function($result) {
            if ($result == true) {
                eb.ajax({
                    url: BASE_URL + '/departmentStation-api/changeStationStatus',
                    method: 'post',
                    dataType: 'json',
                    data: {
                        'departmentId': departmentId,
                        'stationCode': relatedStationCode,
                        'stationStatus': status,
                    },
                    success: function(data) {
                        console.log(data.msg);
                        if (data.status) {
                            p_notification(data.status, data.msg);
                            if (currentDiv.hasClass('fa-check-square-o')) {
                                currentDiv.removeClass('fa-check-square-o');
                                currentDiv.addClass('fa-square-o');
                            }
                            else {
                                currentDiv.removeClass('fa-square-o');
                                currentDiv.addClass('fa-check-square-o');
                            }
                        } else {
                            p_notification(data.status, data.msg);
                        }
                    }
                });
            }
        });
    }

    $('#depStationViewTable').on('click', '.deleteStation', function(){
        var relatedStationTempCode = $(this).closest('tr').attr('id');
        var relatedStationCode = $(this).closest('tr').data('station-code');
       
        deleteStation(relatedStationTempCode, relatedStationCode);
    });

    function deleteStation(relatedStationTempCode, relatedStationCode) {
        var currentDiv = $('#addDepartmentServiceModal #viewDepStationBody #viewNewDepStationRow '+'#'+relatedStationTempCode);
        bootbox.confirm('Are you sure you want to delete this Station ?', function($result) {
            if ($result == true) {
                eb.ajax({
                    url: BASE_URL + '/departmentStation-api/deleteStation',
                    method: 'post',
                    dataType: 'json',
                    data: {
                        'departmentId': departmentId,
                        'stationCode': relatedStationCode,
                    },
                    success: function(data) {
                        if (data.status) {
                            p_notification(data.status, data.msg);
                            currentDiv.remove();
                        } else {
                            p_notification(data.status, data.msg);
                        }
                    }
                });
            }
        });
    }

    /**
     *
     *use to change active state
     */
    function activeInactive(type, status, currentDiv, msg, flag) {
        bootbox.confirm(msg, function($result) {
            if ($result == true) {
                eb.ajax({
                    url: BASE_URL + '/department-api/changeStatusID',
                    method: 'post',
                    dataType: 'json',
                    data: {
                        'departmentID': type,
                        'status': status,
                    },
                    success: function(data) {
                        if (data.status) {
                            p_notification(data.status, data.msg);
                            if (currentDiv.hasClass('fa-check-square-o') && flag) {
                                currentDiv.removeClass('fa-check-square-o');
                                currentDiv.addClass('fa-square-o');
                            }
                            else {
                                currentDiv.removeClass('fa-square-o');
                                currentDiv.addClass('fa-check-square-o');
                            }
                        } else {
                            p_notification(data.status, data.msg);
                        }
                    }
                });
            }
        });
    }

});
