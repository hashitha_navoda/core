$(document).ready(function() {

    var designationId   = null;
    var entityId = null;
    var designationName = null;
    var designationCode = null;
    var designationStatus = null;
    
    $('#btnAdd').on('click',function(){
        var designationCode = $('#designationCode').val();
        var designationName = $('#designationName').val();
        
        if(designationCode.trim() && designationName.trim()){
            eb.ajax({
                type: 'POST',
                url: BASE_URL + '/designation-api/createDesignation',
                data: {
                    designationCode : designationCode,
                    designationName : designationName
                },
                success: function(respond) {
                    p_notification(respond.status, respond.msg);                
                    if(respond.status){
                        $('#designationCode').val('');
                        $('#designationName').val('');
                        window.setTimeout(function() {
                                location.reload();
                        }, 1000);                    
                    }                
                }
            });
        } else {
            p_notification(false, eb.getMessage('ERR_DESIGNATION_REQUIRED'));
        }
    });

    //for cancel btn
    $('#btnCancel').on('click',function (){
            $('input[type=text]').val('');        
            designationRegisterView();        
    });

    $('#designation-list').on('click','.designation-action',function (e){    

        var action = $(this).data('action');
        designationId   = $(this).closest('tr').data('designation-id');
        entityId = $(this).closest('tr').data('entity-id');
        designationName = $(this).closest('tr').data('designation-name');
        designationCode = $(this).closest('tr').data('designation-code');
        designationStatus = $(this).closest('tr').data('designation-status');

        switch(action) {
            case 'edit':
                e.preventDefault();
                designationUpdateView();
                //set selected designation details
                $('#designationCode').val(designationCode);
                $('#designationName').val(designationName);
                break;

            case 'status':
                var msg;
                var status;
                designationId   = $(this).closest('tr').data('designation-id');
                var currentDiv = $(this).contents();
                var flag = true;
                if ($(this).children().hasClass('fa-check-square-o')) {
                    msg = 'Are you sure you want to Deactivate this designation';
                    status = '0';
                }
                else if ($(this).children().hasClass('fa-square-o')) {
                    msg = 'Are you sure you want to Activate this designation';
                    status = '1';
                }
                activeInactive(designationId, status, currentDiv, msg, flag);
                break;
            case 'delete':
                deleteDesignation(designationId);
                break;
            default:
                console.error('Invalid action.');
                break;
        }
    });


    //this function for switch to designation register view
    function designationRegisterView(){
       $('.updateTitle').addClass('hidden');
       $('.addTitle').removeClass('hidden');
       $('.updateDiv').addClass('hidden');
       $('.addDiv').removeClass('hidden');
    }

    //this function for switch to designation update view
    function designationUpdateView(){
       $('.addTitle').addClass('hidden');
       $('.updateTitle').removeClass('hidden');
       $('.addDiv').addClass('hidden');
       $('.updateDiv').removeClass('hidden');
    }

    /**
     *
     *use to change active state
     */
    function activeInactive(type, status, currentDiv, msg, flag) {
        bootbox.confirm(msg, function($result) {
            if ($result == true) {
                eb.ajax({
                    url: BASE_URL + '/designation-api/changeStatusID',
                    method: 'post',
                    dataType: 'json',
                    data: {
                        'designationID': type,
                        'status': status,
                    },
                    success: function(data) {
                        if (data.status) {
                            p_notification(data.status, data.msg);
                            if (currentDiv.hasClass('fa-check-square-o') && flag) {
                                currentDiv.removeClass('fa-check-square-o');
                                currentDiv.addClass('fa-square-o');
                            }
                            else {
                                currentDiv.removeClass('fa-square-o');
                                currentDiv.addClass('fa-check-square-o');
                            }
                        } else {
                            p_notification(data.status, data.msg);
                        }
                    }
                });
            }
        });
    }

    $('#btnUpdate').on('click',function(){
        var designationCode = $('#designationCode').val();
        var designationName = $('#designationName').val();
        
        if(designationCode.trim() && designationName.trim()){
            eb.ajax({
                type: 'POST',
                url: BASE_URL + '/designation-api/update',
                data: {
                    designationCode : designationCode,
                    designationName : designationName,
                    designationID: designationId,
                    entityID: entityId,
                },
                success: function(respond) {
                    p_notification(respond.status, respond.msg);                
                    if(respond.status){
                        $('#designationCode').val('');
                        $('#designationName').val('');
                        window.setTimeout(function() {
                                location.reload();
                        }, 1000);                  
                    }                
                }
            });
        } else {
            p_notification(false, eb.getMessage('ERR_DESIGNATION_REQUIRED'));
        }
    });

    //for search
    $('#searchDesi').on('click',function() {
        var key = $('#designationSearch').val();

        if (key.trim()) {
            $.ajax({
                type: 'POST',
                url: BASE_URL + '/designation-api/search',
                data: {
                    searchKey : key
                },
                success: function(data) {
                    if (data.status == true) {
                        if (data.status) {
                            $("#designation-list").html(data.html);
                        } else {
                            p_notification(data.status, data.msg);
                        }
                    }            
                }
            });
        } else {
            p_notification(false, eb.getMessage('ERR_NO_KEY_WORD'));
        }
        
    });

    //for rest btn
    $('#btnReset').on('click',function (){
            $('input[type=text]').val('');
    });  

    //for search cancel btn
    $('#searchCancel').on('click',function (){
        $('#designationSearch').val('');
        fetchAll();
    });

    //this function for fetach all the designations
    function fetchAll(){
        $.ajax({
            type: 'POST',
            data: {
                searchKey: ''
            },
            url: BASE_URL + '/designation-api/search',
            success: function(respond) {
                $('#designation-list').html(respond.html);            
            }
        });
    }

    function deleteDesignation(designationId) {
        bootbox.confirm('Are you sure you want to delete this Designation?', function($result) {
            if ($result == true) {
                eb.ajax({
                    url: BASE_URL + '/designation-api/deleteDesignation',
                    method: 'post',
                    data: {
                        designationID: designationId,
                        entityID: entityId
                    },
                    dataType: 'json',
                    success: function(data) {
                        if (data.status == true) {
                            p_notification(data.status, data.msg);
                            window.location.reload();
                        } else {
                            p_notification(data.status, data.msg);
                        }
                    }
                });
            }
        });
    }

});
