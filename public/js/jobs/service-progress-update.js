
$(document).ready(function() {

    var departmentID = null;
    var jobTaskId = null;
    var jobId = null;
    var empArray = [];
    var departmentStationID = null;
    var nextJobTaskID = null;
    var nextDepartmentID = null;
    $('#startTask').addClass('hidden');
    $('.progress-list-header').addClass('hidden');

    $('.department-list-item').css( 'cursor', 'pointer' );
    $('.pending-service-list-item').css( 'cursor', 'pointer' );
    $('.department-station-list').css( 'cursor', 'pointer' );
    $('.department-emp-list-item').css( 'cursor', 'pointer' );
	
    $('.department-list-item').on('click',function(){
        if (!$(this).hasClass('active-dep')) {
            $('#department-list .active-dep').removeClass('active-dep');
            $(this).addClass('active-dep');

            departmentID = $(this).data('department-id');
            jobTaskId = null;
            jobId = null;
            departmentStationID = null;
            empArray = [];
            $('#startTask').removeClass('hidden');
            $('.progress-list-header').removeClass('hidden');
            getDepartmentRelatedProgressUpdateListsDetails();
        } else {
            departmentID = null;
            $(this).removeClass('active-dep');
            getDepartmentRelatedProgressUpdateListsDetails();
            $('#startTask').addClass('hidden');
            $('.progress-list-header').addClass('hidden');
        }
    });

    $('#progressUpdateList').on('click','.pending-service-list-item', function(){

        if (!$(this).hasClass('active-dep')) {
            $('#pendigServiceList .active-dep').removeClass('active-dep');
            $(this).addClass('active-dep');
            jobTaskId = $(this).data('job-task-id');
            jobId = $(this).data('job-id');
        } else {
            jobTaskId = null;
            jobId = null;
            $(this).removeClass('active-dep');
        }
    });

     $('#progressUpdateList').on('click','.department-station-list',function(){

        if (!$(this).hasClass('active-dep')) {
            $('#depStations .active-dep').removeClass('active-dep');
            $(this).addClass('active-dep');
            departmentStationID = $(this).data('department-station-id');
        } else {
            $(this).removeClass('active-dep');
            departmentStationID = null;
        }
    });

    $('#progressUpdateList').on('click','.department-emp-list-item',function(){

        if (!$(this).hasClass('active-dep')) {
            $(this).addClass('active-dep');
            var empId = $(this).data('employee-id');
            empArray.push(empId);

        } else {
            $(this).removeClass('active-dep');
            var empId = $(this).data('employee-id');

            empArray = jQuery.grep(empArray, function(value) {
              return value != empId;
            });
        }
    });

    function getDepartmentRelatedProgressUpdateListsDetails() {
        eb.ajax({
            url: BASE_URL + '/service-progress-update-api/getDepartmentRelatedProgressUpdateListsDetails',
            method: 'post',
            data: {
                departmentID: departmentID
            },
            dataType: 'json',
            success: function(data) {
                if (data.status) {
                    $("#progressUpdateList").html(data.html);
                    $('.department-list-item').css( 'cursor', 'pointer' );
                    $('.pending-service-list-item').css( 'cursor', 'pointer' );
                    $('.department-station-list').css( 'cursor', 'pointer' );
                    $('.department-emp-list-item').css( 'cursor', 'pointer' );
                    setSameHeightForAllWidgetDiv();
                } else {
                    p_notification(data.status, data.msg);
                }
            }
        });
    }

    $('#progressUpdateList').on('click','#startTask', function(){
        if (jobTaskId == null) {
            p_notification(false, eb.getMessage('ERR_REQ_JOB_SRVC_FOR_START'));
            return;
        }
        startService(jobTaskId, jobId, departmentStationID, empArray);

        jobTaskId = null;
        jobId = null;
        departmentStationID = null;
        empArray = [];
    });

    $('#progressUpdateList').on('click','#resumeSrvce', function(){
        if (jobTaskId == null) {
            p_notification(false, eb.getMessage('ERR_REQ_JOB_SRVC_FOR_START'));
            return;
        }

        startService(jobTaskId, jobId, departmentStationID, empArray, changeFlag = true);

        jobTaskId = null;
        jobId = null;
        departmentStationID = null;
        empArray = [];

    });

    $('#progressUpdateList').on('click', '.holdTask', function(){
        var jobTaskId = $(this).closest('.main-service-row').find('.progress-list').data('job-task-id');
        var jobId = $(this).closest('.main-service-row').find('.progress-list').data('job-id');
        
        eb.ajax({
            type: 'POST', url: BASE_URL + '/service-progress-update-api/holdJobRelatedService', 
            data: {
                jobTaskId: jobTaskId,
                jobId: jobId
            }, 
            success: function(respond) {
                p_notification(respond.status, respond.msg);
                if (respond.status == true) {
                    $('#progressUpdateList #mainServiceList #main_row_'+jobTaskId+' .holdTask').addClass('hidden');
                    $('#progressUpdateList #mainServiceList #main_row_'+jobTaskId+' .resumeTask').removeClass('hidden');
                } 
            }
        });
    });

    $('#progressUpdateList').on('click', '.resumeTask', function(){
        jobTaskId = $(this).closest('.main-service-row').find('.progress-list').data('job-task-id');
        jobId = $(this).closest('.main-service-row').find('.progress-list').data('job-id');

        $("#resumeConfirmModal").modal('toggle');
    });

    $('#progressUpdateList').on('click', '.endTask', function(){
        var jobTaskId = $(this).closest('.main-service-row').find('.progress-list').data('job-task-id');
        var jobId = $(this).closest('.main-service-row').find('.progress-list').data('job-id');
        
        eb.ajax({
            type: 'POST', url: BASE_URL + '/service-progress-update-api/stopJobRelatedService', 
            data: {
                jobTaskId: jobTaskId,
                jobId: jobId,
            }, 
            success: function(respond) {
                p_notification(respond.status, respond.msg);
                if (respond.status == true) {
                    getJobRelatedDueServices(jobId);
                }  
            },async:false
        });
    });

    function startService(jobTaskId, jobId, departmentStationID = null, empArray = null, changeFlag = false){
        eb.ajax({
            type: 'POST', url: BASE_URL + '/service-progress-update-api/startJobRelatedService', 
            data: {
                jobTaskId: jobTaskId,
                jobId: jobId,
                departmentStationID: departmentStationID,
                empArray: empArray,
                changeFlag: changeFlag

            }, 
            success: function(respond) {
                p_notification(respond.status, respond.msg);
                if (respond.status == true) {
                   getDepartmentRelatedProgressUpdateListsDetails();
                } 
            }
        });
    }

    $('#change').on('click', function (){
        $("#resumeConfirmModal").modal('toggle');
        getToResumeServiceDetails(jobTaskId);
         
    });

    $('#resumeTaskConfirm').on('click', function (){
        startService(jobTaskId, jobId);
        $("#resumeConfirmModal").modal('toggle');
    });

    function getToResumeServiceDetails() {
        eb.ajax({
            url: BASE_URL + '/service-progress-update-api/getToResumeServiceDetails',
            method: 'post',
            data: {
                jobTaskId: jobTaskId,
                departmentID: departmentID
            },
            dataType: 'json',
            success: function(data) {
                if (data.status) {
                    $("#progressUpdateList").html(data.html);
                    $('.department-list-item').css( 'cursor', 'pointer' );
                    $('.pending-service-list-item').css( 'cursor', 'pointer' );
                    $('.department-station-list').css( 'cursor', 'pointer' );
                    $('.department-emp-list-item').css( 'cursor', 'pointer' );

                    $('#progressUpdateList #serviceList #'+jobTaskId).removeClass('pending_service_color');
                    $('#progressUpdateList #serviceList #'+jobTaskId).addClass('active-dep');
                    $('#progressUpdateList #mainServiceList #main_row_'+jobTaskId+' .holdTask').addClass('disabled');
                    $('#progressUpdateList #mainServiceList #main_row_'+jobTaskId+' .resumeTask').addClass('disabled');

                    $('#progressUpdateList #startTask').addClass('hidden');
                    $('#progressUpdateList #resumeSrvce').removeClass('hidden');

                    $('html, body').animate({
                      scrollTop: 0
                    }, 800);
                    return false;
                } else {
                    p_notification(data.status, data.msg);
                }
            }
        });
    }

    function getJobRelatedDueServices(jobId) {
        eb.ajax({
            type: 'POST', url: BASE_URL + '/service-progress-update-api/getJobRelatedDueServices', 
            data: {jobId: jobId}, 
            success: function(respond) {
                if (respond.status == true) {

                    $("#due-service-list").html(respond.html);
                    $('.due-service-list-item').css( 'cursor', 'pointer' );

                    if (respond.data > 0) { 
                        $('#dueServiceListModal').modal({
                            backdrop: 'static',
                            keyboard: false
                        });
                    }
                    else {
                        getDepartmentRelatedProgressUpdateListsDetails();
                    }
                } 
            },async:false
        });
        
    }

    $('#due-service-list').on('click','.due-service-list-item',function(){
        if (!$(this).hasClass('active-dep')) {
            $('#due-service-list .active-dep').removeClass('active-dep');
            $(this).addClass('active-dep');

            nextDepartmentID = $(this).data('department-id');
            nextJobTaskID = $(this).data('job-task-id');
        } else {
            departmentID = null;
            $(this).removeClass('active-dep');
            nextDepartmentID = null;
            nextJobTaskID = null;
        }
    });

    $('#btnSkip').on('click',function(){
        getDepartmentRelatedProgressUpdateListsDetails();
        $("#dueServiceListModal").modal('toggle');
    });

    $('#btnContinueNextSrvc').on('click',function(){
        if (nextJobTaskID == null) {
            p_notification(false, eb.getMessage('ERR_REQ_NEXT_JOB_SRVC_FOR_START'));
            return;
        }

        $('#department-list .active-dep').removeClass('active-dep');
        $('#department-list #dep_'+nextDepartmentID).addClass('active-dep');

        getListsForAutoSelectProcess();   
    });


    function getListsForAutoSelectProcess() {
        eb.ajax({
            url: BASE_URL + '/service-progress-update-api/getListsForAutoSelectProcess',
            method: 'post',
            data: {
                departmentID: nextDepartmentID,
                jobTaskId: nextJobTaskID
            },
            dataType: 'json',
            success: function(data) {
                if (data.status) {
                    $("#progressUpdateList").html(data.html);
                    $('.department-list-item').css( 'cursor', 'pointer' );
                    $('.pending-service-list-item').css( 'cursor', 'pointer' );
                    $('.department-station-list').css( 'cursor', 'pointer' );
                    $('.department-emp-list-item').css( 'cursor', 'pointer' );
                    $('#progressUpdateList #serviceList #'+nextJobTaskID).removeClass('pending_service_color');
                    $('#progressUpdateList #serviceList #'+nextJobTaskID).addClass('active-dep');

                    departmentID = nextDepartmentID;
                    jobTaskId = nextJobTaskID;

                    $("#dueServiceListModal").modal('toggle');
                    $('html, body').animate({
                      scrollTop: 0
                    }, 800);
                    return false;
                } else {
                    p_notification(data.status, data.msg);
                }
            }
        });
    }

    function setSameHeightForAllWidgetDiv(){
        var maxHeight = 0;
        $('.vehicle_reg_list_panel_body_mod').each(function (index, value) {
            if($(this).outerHeight() > maxHeight) {
                maxHeight = $(this).outerHeight();
            }
        });
        $('.vehicle_reg_list_panel_body_mod').outerHeight(maxHeight);
    }

});
    