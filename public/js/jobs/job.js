var selectedSupervisorID;
var selectedEmployeeID;
var selectedTaskContractorID;
var selectedManagerID;
var jobSupervisors = [];
var jobManagers = [];
var jobSupervisorsData = {};
var locationProducts = {};
var jobManagersData = {};
var tasksTotal = {};
var rawMaterialProducts = {};
var fixedAssetsProducts = {};
var tradingGoodProducts = {};
var taskCardIDs = {};
var selectedId;
var activeProductData = {};
var jobTasks = {};
var tasks = {};
var jobTaskProducts = {};
var taskProducts = {};
var taskEmployees = {}
var taskContractorsData = {}
var taskCardTaskList = {};
var selectedRMProductID = null;
var selectedFAProductID = null;
var selectedTGProductID = null;
var taskRowEdit = false;
var jobEditflag = false;
var fromProjectFlag = false;
var redirectJobForProject = false;
var efectedJobCode;
var efectedJobID;
var projectID = null;
$(document).ready(function() {
    var customerID;
    var jobSupervisorsArray = {};
    var jobManagersArray = {};
    var taskTempItems = {};
    var taskItems = {};
    var employees = {};
    var taskContractors = {};
    var jSupervisorID;
    var $taskTable = $("#taskCardTaskTable");
    var $addRowSample = $('tr.add-row.sample.hidden', $taskTable);
    var rowincrementID = 1;

    var locationID = $('#navBarSelectedLocation').attr('data-locationid');
    //get active product list
    eb.ajax({
        type: 'POST',
        url: BASE_URL + '/productAPI/getActiveProductsFromLocation',
        data: {locationID: locationID},
        success: function(respond) {
            activeProductData = respond.data;
        },
        async: false
    });

    var getAddRow = function(taskIncID) {
        if (!(taskIncID === undefined || taskIncID == '')) {
            var $row = $('table#taskCardTaskTable > tbody#add-new-task-row > tr').filter(function() {
                return $(this).data("taskIncID") == taskIncID;
            });
            return $row;
        }

        return $('tr.add-row:not(.sample)', $taskTable);
    };
    if (!getAddRow().length) {
        $($addRowSample.clone()).removeClass('sample hidden').appendTo($('#add-new-task-row'));
    }


    var $rawProductTable = $("#rawProductTable");
    var $addrawProductSample = $('tr.add-row.sample', $rawProductTable);
    var getProductAddRow = function(productID) {
        if (productID != undefined) {
            var $row = $('table.transfer-table > tbody > tr', $rawProductTable).filter(function() {
                return $(this).data("id") == productID
            });
            return $row;
        }
        return $('tr.add-row:not(.sample)', $rawProductTable);
    };

    // for update operations
    if($('#editMode').val() == '1'){
        var reqJobID = $('#editMode').attr('data-jobid');
        loadDataToJob(reqJobID);
    }

    if (!$.isEmptyObject(jobTasks)) {
        $.each(jobTasks, function(index, value) {
            var size = Object.keys(value).length;
            if (size > 0) {
                var count = size+" tasks added";
                $('#taskCount').text(count);
           }  
        });
    }

     if (!$.isEmptyObject(jobTaskProducts)) {
        $.each(jobTaskProducts, function(index, value) {
            var size = Object.keys(value).length;
            if (size > 0) {
                var count = size+" products added";
                $('#resourceCount').text(count);
           }  
        });
    }

    $('#resetJob').on('click', function(e) {
        e.preventDefault();
        window.location.reload();
    });

    $('#jobValue').on('change',function(){
        if (parseFloat($('#jobValue').val()) < 0) {
            p_notification(false, eb.getMessage("ERR_JOB_VALUE"));
            $('#jobValue').val(accounting.formatMoney(0));
        } else {
            $('#jobValue').val(accounting.formatMoney($(this).val()));
        }
    });

    $('#show_addi').on('click', function() {
        if (this.checked) {
            $('#additional_div').removeClass('hidden');
        }
        else {
            $('#additional_div').addClass('hidden');
        }
    });


    loadDropDownFromDatabase('/employee-api/searchDesignationWiseEmployeesForDropdown', "", 1, '#taskEmployeeCode', '', '', false);
    $('#taskEmployeeCode').children().not('.taskEmployeeCodeDefaultSelect').remove();
    $('#taskEmployeeCode').selectpicker('refresh');


    loadDropDownFromDatabase('/customerAPI/search-customers-for-dropdown', "", 'Invoice', '#customer');
    $('#customer').selectpicker('refresh');
    $('#customer').on('change', function() {
        if ($(this).val() > 0 && customerID != $(this).val()) {
            customerID = $(this).val();
            $('#projectCode').attr('disabled', true);
        }
    });

    loadDropDownFromDatabase('/employee-api/search-job-supervisors-for-dropdown', '', 0, '#jobSupervisorCode', '', '', false);
    $('#jobSupervisorCode').children().not('.jobSupervisorCodeDefaultSelect').remove();
    $('#jobSupervisorCode').selectpicker('refresh');

    loadDropDownFromDatabase('/api/project/search-projects-for-dropdown', '', 0, '#projectCode', '', '', false);
    $('#projectCode').selectpicker('refresh');
    $('#projectCode').on('change', function() {
        resetJobPage();
        if(!($(this).val() == null || $(this).val() == 0)){
            projectID = $(this).val();
            getProjectDetails(projectID);
        }
    });

    var pathName = window.location.pathname.split('/');
    if (pathName[2] != "edit" && pathName.length == 3) {
        projectID = pathName[2];
        fromProjectFlag = true;
        getProjectDetails(projectID);
    }

    function resetJobPage() {
     
    }

    function getProjectDetails(projectID) {
        eb.ajax({
            type: 'POST',
            url: BASE_URL + '/api/project/getProjectDetails',
            data: {
                projectID: projectID
            },
            success: function(respond) {
                if (respond.status) {

                    if (fromProjectFlag) {
                        $('#projectCode').empty();
                        $('#projectCode').
                        append($("<option></option>").
                                attr("value", projectID).
                                text(respond.data.projectCode + '-' + respond.data.projectName));
                        $('#projectCode').selectpicker('refresh');
                        $('#projectCode').attr('disabled', true);
                    }

                    customerID = respond.data.customerID;
                    $('#customer').empty();
                    $('#customer').
                    append($("<option></option>").
                            attr("value", respond.data.customerID).
                            text(respond.data.customerName + '-' + respond.data.customerCode));
                    $('#customer').selectpicker('refresh');
                    $('#customer').attr('disabled', true);
                }
            }
        });
    }

    loadDropDownFromDatabase('/employee-api/search-job-managers-for-dropdown', '', 0, '#jobManagerCode', '', '', false);
    $('#jobManagerCode').children().not('.jobManagerCodeDefaultSelect').remove();
    $('#jobManagerCode').selectpicker('refresh');

    //DatePicker
    var nowTemp = new Date();
    var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);

    var dueDate = $('#dueDate').datepicker({onRender: function(date) {
            return date.valueOf() > now.valueOf() ? ((U_DATE_R_OVERRIDE) ? '' : 'disabled') : '';
        },
    }).on('changeDate', function(ev) {
        dueDate.hide();
    }).data('datepicker');

    var estimatedEndDate = $('#expDate').datepicker({onRender: function(date) {
            return date.valueOf() > now.valueOf() ? ((U_DATE_R_OVERRIDE) ? '' : 'disabled') : '';
        },
    }).on('changeDate', function(ev) {
        estimatedEndDate.hide();
    }).data('datepicker');

    ////////job supervisor modal\\\\\\\\
    $('#addSupervisor').on('click', function(e) {
        clearJobSupervisorModal();
        var jobCode = $('#jobCode').val();

        if (!$.isEmptyObject(jobSupervisorsData)) {
            $.each(jobSupervisorsData[jobCode], function(index, val) {
                jobSupervisorsArray[index] = val;                
            });
            $.each(jobSupervisorsArray, function(index, value) {
                var newTrID = 'tr_' + value.jobSupervisorID;
                var clonedRow = $($('#supervisorPreSample').clone()).attr('id', newTrID).addClass('addedSupervisors');
                $("input[name='supervisorCode']", clonedRow).val(value.jobSupervisorCodeAndName);
                clonedRow.removeClass('hidden');
                clonedRow.insertBefore('#supervisorPreSample');
                clearAddedNewJobSupervisorRow();
            });

        }
        $('#addJobSupervisorsModal').modal('show');
    });

    function clearJobSupervisorCode() {
        $('#jobSupervisorCode')
                .empty()
                .val('')
                .append($("<option></option>")
                        .attr("value", "")
                        .text('Select job supervisor Code or Name'))
                .selectpicker('refresh')
                .trigger('change');
    }

    $('#jobSupervisorCode').on('change', function(e) {
        if (typeof jobSupervisorsArray[$(this).val()] != 'undefined' && $(this).val() != '') {
            p_notification(false, eb.getMessage("ERR_JOB_SUP_ALREADY_ADD"));
            $('#jobSupervisorCode').val('');
            clearJobSupervisorCode();
            return false;
        } else if ($(this).val() > 0 && selectedSupervisorID != $('#jobSupervisorCode').val()) {

            selectedSupervisorID = $(this).val();
            eb.ajax({
                type: 'POST',
                url: BASE_URL + '/employee-api/get-employee-details',
                data: {employeeID: selectedSupervisorID},
                success: function(respond) {
                    var currentElem = new Array();
                    currentElem[selectedSupervisorID] = respond.data;
                    jobSupervisors = $.extend(jobSupervisors, currentElem);

                    var data = new Array();
                    data[0] = jobSupervisors[selectedSupervisorID];

                    jSupervisorID = data[0].employeeID;
                    $('#jobSupervisorCode').data('jSupID', data[0].employeeID);
                    $('#jobSupervisorCode').data('EFN', data[0].employeeFirstName);
                    $('#jobSupervisorCode').data('ELN', data[0].employeeSecondName);
                    $('#jobSupervisorCode').data('EC', data[0].employeeCode);
                }
            });
        }
    });

    $('#add_superviosr').on('click', function(e) {
        e.preventDefault();
        //if plus button is clicked then disabled the button
        //if button is clicked more than one, then it does not continue
        if ($(this).is(':disabled')) {
            return false;
        } else {
            $(this).attr('disabled', true);
            var $thisRow = $(this).parents('tr');

            if ($('#jobSupervisorCode').val() == 0 || $('#jobSupervisorCode').val() == '' || $('#jobSupervisorCode').val() == null) {
                p_notification(false, eb.getMessage('ERR_JOB_SUPERVISOR'));
                $(this).attr('disabled', false);
            } else {
                if (jobSupervisorsArray[jSupervisorID]) {
                    p_notification(false, eb.getMessage('ERR_JOB_SUPINSERT'));
                    $('#jobSupervisorCode').val('');
                    $('#jobSupervisorCode').selectpicker('render');
                } else {
                    addNewJSRow();
                }
                $(this).attr('disabled', false);
            }
        }
    });

    function addNewJSRow() {

        var iC = $('#jobSupervisorCode').val();

        var jobSupervisorID = $('#jobSupervisorCode').data('jSupID');
        var jobSupervisorCodeAndName = $('#jobSupervisorCode').data('EFN') + ' '+ $('#jobSupervisorCode').data('ELN');
       
        var jSupDuplicateCheck = false;
        if (!$.isEmptyObject(jobSupervisorsArray)) {
            $.each(jobSupervisorsArray, function(index, value) {
                if (value.jobSupervisorID == jSupervisorID) {
                    jSupDuplicateCheck = true;
                }
            });   
        }
       
        if (jSupDuplicateCheck) {
            p_notification(false, eb.getMessage("ERR_JOB_SUPINSERT"));
            return false;
        }

        var newTrID = 'tr_' + jSupervisorID;
        var clonedRow = $($('#supervisorPreSample').clone()).attr('id', newTrID).addClass('addedSupervisors');
        $("input[name='supervisorCode']", clonedRow).val(jobSupervisorCodeAndName);
        clonedRow.removeClass('hidden');
        clonedRow.insertBefore('#supervisorPreSample');
        jobSupervisorsArray[jSupervisorID] = new jobSupervisor(jSupervisorID, jobSupervisorCodeAndName);
        clearAddedNewJobSupervisorRow();
    }

    function jobSupervisor(jSupID, jSupCodeName) {
        this.jobSupervisorID = jSupID;
        this.jobSupervisorCodeAndName = jSupCodeName;
    }

    function clearAddedNewJobSupervisorRow() {
        $('#jobSupervisorCode').val('');
        clearJobSupervisorCode();
    }

    function clearJobSupervisorModal()
    {
        $("#addJobSupervisorsModal #supervisorTable tbody .addedSupervisors").remove();
        jobSupervisorsArray = {};
    }

    $('#addjSuperviosrs').on('click', function(e) {
        e.preventDefault();

        if (!isNaN(parseFloat($('#jobSupervisorCode').val())) && $('#jobSupervisorCode').val() != 0) {
            p_notification(false, eb.getMessage('ERR_ADD_SELECT_JOB_SUPV'));
            return;
        }
        // validate products before closing modal
        if (!jobSupervisorModalValidate(e)) {
            e.stopPropagation();
            return false;
        } else {
            $('#addJobSupervisorsModal').modal('hide');
        }
    });

    function jobSupervisorModalValidate(e) {
        var jobCode = $('#jobCode').val();
        if ($.isEmptyObject(jobSupervisorsArray)) {
            jobSupervisorsData = {};
            return true;   
        } else {
            var jobSupervisorSingleData = {};
            $.each(jobSupervisorsArray, function(index, val) {
                jobSupervisorSingleData[index] = val;
            });
            
            jobSupervisorsData[jobCode] = jobSupervisorSingleData;
            return true;
        }
    }

    $("#addJobSupervisorsModal #jobSupervisorTable").on('click', '.deleteSupervisor', function(e) {
        e.preventDefault();
        var deleteSTrID = $(this).closest('tr').attr('id');
        var deleteSID = deleteSTrID.split('tr_')[1].trim();

        bootbox.confirm('Are you sure you want to remove this job supervisor ?', function(result) {
            if (result == true) {
                delete jobSupervisorsArray[deleteSID];
                $('#' + deleteSTrID).remove();
            }
        });
    });


    // Job manager modal
    function clearJobManagerModal()
    {
        $("#addjobManagersModal #managersTable tbody .addedManagers").remove();
        jobManagersArray = {};
    }

    $('#jobManger').on('click', function(e) {
        clearJobManagerModal();
        var jobCode = $('#jobCode').val();

        if (!$.isEmptyObject(jobManagersData)) {
            $.each(jobManagersData[jobCode], function(index, val) {
                jobManagersArray[index] = val;                
            });
            $.each(jobManagersArray, function(index, value) {
                var newTrID = 'tr_' + value.jobManagerID;
                var clonedRow = $($('#managerPreSample').clone()).attr('id', newTrID).addClass('addedManagers');
                $("input[name='managerCode']", clonedRow).val(value.jobManagerCodeAndName);
                clonedRow.removeClass('hidden');
                clonedRow.insertBefore('#managerPreSample');
                clearAddedNewJobManagerRow();
            });

        }
        $('#addjobManagersModal').modal('show');
    });

    function clearJobManagerCode() {
        $('#jobManagerCode')
                .empty()
                .val('')
                .append($("<option></option>")
                        .attr("value", "")
                        .text('Select job manager Code or Name'))
                .selectpicker('refresh')
                .trigger('change');
    }

    $('#jobManagerCode').on('change', function(e) {
        if (typeof jobManagersArray[$(this).val()] != 'undefined' && $(this).val() != '') {
            p_notification(false, eb.getMessage("ERR_JOB_MANINSERT"));
            $('#jobManagerCode').val('');
            clearJobManagerCode();
            return false;
        } else if ($(this).val() > 0 && selectedManagerID != $('#jobManagerCode').val()) {

            selectedManagerID = $(this).val();
            eb.ajax({
                type: 'POST',
                url: BASE_URL + '/employee-api/get-employee-details',
                data: {employeeID: selectedManagerID},
                success: function(respond) {
                    var currentElem = new Array();
                    currentElem[selectedManagerID] = respond.data;
                    jobManagers = $.extend(jobManagers, currentElem);

                    var data = new Array();
                    data[0] = jobManagers[selectedManagerID];

                    jManagerID = data[0].employeeID;
                    $('#jobManagerCode').data('jManID', data[0].employeeID);
                    $('#jobManagerCode').data('EFN', data[0].employeeFirstName);
                    $('#jobManagerCode').data('ELN', data[0].employeeSecondName);
                    $('#jobManagerCode').data('EC', data[0].employeeCode);
                }
            });
        }
    });

    $('#add_manager').on('click', function(e) {
        e.preventDefault();
        //if plus button is clicked then disabled the button
        //if button is clicked more than one, then it does not continue
        if ($(this).is(':disabled')) {
            return false;
        } else {
            $(this).attr('disabled', true);
            var $thisRow = $(this).parents('tr');
            if ($('#jobManagerCode').val() == 0 || $('#jobManagerCode').val() == '' || $('#jobManagerCode').val() == null) {
                p_notification(false, eb.getMessage('ERR_JOB_SUPERVISOR'));
                $(this).attr('disabled', false);
            } else {
                if (jobManagersArray[jManagerID]) {
                    p_notification(false, eb.getMessage('ERR_JOB_MANINSERT'));
                    $('#jobManagerCode').val('');
                    $('#jobManagerCode').selectpicker('render');
                } else {
                    addNewJMRow();
                }
                $(this).attr('disabled', false);
            }
        }
    });

    function addNewJMRow() {

        var iC = $('#jobManagerCode').val();

        var jobManagerID = $('#jobManagerCode').data('jManID');
        var jobManagerCodeAndName = $('#jobManagerCode').data('EFN') + ' '+ $('#jobManagerCode').data('ELN');
       
        var jManDuplicateCheck = false;
        if (!$.isEmptyObject(jobManagersArray)) {
            $.each(jobManagersArray, function(index, value) {
                if (value.jobManagerID == jManagerID) {
                    jManDuplicateCheck = true;
                }
            });   
        }
       
        if (jManDuplicateCheck) {
            p_notification(false, eb.getMessage("ERR_JOB_MANINSERT"));
            return false;
        }

        var newTrID = 'tr_' + jManagerID;
        var clonedRow = $($('#managerPreSample').clone()).attr('id', newTrID).addClass('addedManagers');
        $("input[name='managerCode']", clonedRow).val(jobManagerCodeAndName);
        clonedRow.removeClass('hidden');
        clonedRow.insertBefore('#managerPreSample');
        jobManagersArray[jManagerID] = new jobManager(jManagerID, jobManagerCodeAndName);
        clearAddedNewJobManagerRow();
    }

    function jobManager(jManID, jManCodeName) {
        this.jobManagerID = jManID;
        this.jobManagerCodeAndName = jManCodeName;
    }

    function clearAddedNewJobManagerRow() {
        $('#jobManagerCode').val('');
        clearJobManagerCode();
    }

    $('#addjManagers').on('click', function(e) {
        e.preventDefault();

        if (!isNaN(parseFloat($('#jobManagerCode').val())) && $('#jobManagerCode').val() != 0) {
            p_notification(false, eb.getMessage('ERR_ADD_SELECT_JOB_MGR'));
            return;
        }

        // validate products before closing modal
        if (!jobManagerModalValidate(e)) {
            e.stopPropagation();
            return false;
        } else {
            $('#addjobManagersModal').modal('hide');
        }
    });

    function jobManagerModalValidate(e) {
        var jobCode = $('#jobCode').val();
        if ($.isEmptyObject(jobManagersArray)) {
            jobManagersData = {};
            return true;   
        } else {
            var jobManagersSingleData = {};
            $.each(jobManagersArray, function(index, val) {
                jobManagersSingleData[index] = val;
            });
            jobManagersData[jobCode] = jobManagersSingleData;
            return true;
        }
    }

    $("#addjobManagersModal #jobManagerTable").on('click', '.deleteManager', function(e) {
        e.preventDefault();
        var deleteMTrID = $(this).closest('tr').attr('id');
        var deleteMID = deleteMTrID.split('tr_')[1].trim();

        bootbox.confirm('Are you sure you want to remove this job manager ?', function(result) {
            if (result == true) {
                delete jobManagersArray[deleteMID];
                $('#' + deleteMTrID).remove();
            }
        });
    });

    // Job add tasks modal
    $('#addTasks').on('click', function(e) {
        setTaskTypeahead();
        $('#addTasksModal').modal('show');
    });

    
    function setTaskTypeahead() {
        var $currentRow = getAddRow();
        $('#taskCode', $currentRow).selectpicker();

        $('#uom', $currentRow).selectpicker('render').selectpicker('refresh');
        $('#uom', $currentRow).on('change', function() {
            $("#uom", $currentRow).data('uomIID', $(this).val());
        });

        loadDropDownFromDatabase('/employee-api/searchEmployeesForDropdown', "", 1, '#employees', $currentRow, '', false);

        loadDropDownFromDatabase('/task_setup_api/searchTasksForDropdown', "", 1, '#taskCode', $currentRow, '', false);
        $('#taskCode', $currentRow).on('change', function() {
            if ($(this).val() != 0 && selectedId != $(this).val()) {
                selectedId = $(this).val();
                selectTasksForJob(selectedId);
            }
        });
    }

    function selectTasksForJob(selectedTaskId) {
        var $currentRow = getAddRow();
        var taskCardTaskID = selectedTaskId;
        $currentRow.addClass('taskToAdd');


        $currentRow.data('id', selectedTaskId);
        $currentRow.data('icid', rowincrementID);
        $currentRow.data('taskIncID', rowincrementID);
        $("#taskCode", $currentRow).data('taskIID', selectedTaskId);
        $("#dRate", $currentRow).val('0.00');
        $("#dCost", $currentRow).val('0.00');
        $("#estimatedQty", $currentRow).val('1');


        if ($currentRow.val() != '0' && $currentRow.val() != '') {
            $currentRow.addClass('taskToAdd');
        }

        $currentRow.attr('id', 'task' + selectedTaskId);
    }

    $taskTable.on('focusout', '#dCost,#estimatedQty,#rate', function() {
        var $thisRow = $(this).parents('tr');
        var taskID = $thisRow.data('taskIncID');
        calculateRowEstimatedCost($thisRow,taskID);
    });

    function calculateRowEstimatedCost(thisRow,taskID) {
        var thisRow = thisRow;
        var itemQuentity = thisRow.find('#estimatedQty').val();

        if (isNaN(parseFloat(itemQuentity))) {
            thisRow.find('#estimatedQty').val('1');
            itemQuentity = 1;
        }
      
        if (isNaN(parseFloat(thisRow.find('#dCost').val()))) {
            thisRow.find('#dCost').val('0.00');
        }
        var itemCost = (toFloat(thisRow.find('#dCost').val()) * toFloat(itemQuentity));
        tasksTotal[taskID] = itemCost;
        thisRow.find('#estimatedCost').val(accounting.formatMoney(itemCost));
    }

    function getCurrentTaskData($thisRow) {
        var tid = rowincrementID;
        if ($thisRow.data('taskIncID') !== undefined) {
            tid = $thisRow.data('taskIncID');
        }
        var thisVals = {
            taskID: $thisRow.data('id'),
            taskIncID: $thisRow.data('id') + '_' + tid,
            ratePerUnit1: $("input[name='dRate']", $thisRow).val(),
            ratePerUnit2: $("#rate", $thisRow).val(),
            costPerUnit: $("#dCost", $thisRow).val(),
            UomId: $("#uom", $thisRow).data('uomIID'),
            taskCardID: $("#taskCode", $thisRow).data('taskCardID'),
            estQty: $("input[name='estQty']", $thisRow).val(),
            estimatedCost: $("input[name='estCost']", $thisRow).val(),
            contactor: taskContractorsData[$thisRow.data('id') + '_' + tid],
            employees: taskEmployees[$thisRow.data('id') + '_' + tid]
        };
        return thisVals;
    }

    $taskTable.on('click', 'button.add, button.save', function(e) {

        e.preventDefault();
        var $thisRow = $(this).parents('tr');
        var thisVals = getCurrentTaskData($thisRow);
        if ((thisVals.taskID) === undefined) {
            p_notification(false, eb.getMessage('PLS_SELECT_AT_LEAST_ONE_TASK'));
            $("#taskCode", $thisRow).focus();
            return false;
        }

        if ((thisVals.UomId) === undefined) {
            p_notification(false, eb.getMessage('PLS_SELECT_UOM'));
            $("#uom", $thisRow).focus();
            return false;
        }

        if (parseFloat(thisVals.estQty) <= 0 || (thisVals.estQty) == "") {
            p_notification(false, eb.getMessage('ERR_JOB_TASK_EST_QNTY'));
            $("#estimatedQty", $thisRow).focus();
            return false;
        }

        if (((thisVals.ratePerUnit1) === undefined || parseFloat(thisVals.ratePerUnit1) < 0) && (thisVals.taskCardID) === undefined) {
            p_notification(false, eb.getMessage('ERR_JOB_TASK_RATE'));
            $("#dRate", $thisRow).focus();
            return false;
        }

        if ((thisVals.ratePerUnit2) == "-1" && (thisVals.taskCardID) != undefined) {
            p_notification(false, eb.getMessage('ERR_JOB_TASK_RATE'));
            $("#dRate", $thisRow).focus();
            return false;
        }

        var taskDuplicateCheck = false;
        $.each(tasks, function(index, val) {
            if (val.taskID == thisVals.taskID && val.taskCardID === undefined) {
                taskDuplicateCheck = true;
            }
        });


        // if add button is clicked
        if ($(this).hasClass('add')) {
            if (taskDuplicateCheck) {
                p_notification(false, eb.getMessage('ERR_JOB_TASK'));
                $("#taskCode", $thisRow).focus();
                return false;
            }

            if(!$thisRow.hasClass('copied')){
                $thisRow.data('icid', rowincrementID);
            }
            var $currentRow = $thisRow;
            var $flag = false;
            if ($currentRow.hasClass('add-row')) {
                $flag = true;
            }

            $currentRow
                    .removeClass('edit-row')
                    .find("#taskCode, input[name='dRate'],input[name='estQty'],input[name='estCost'],#rate, #dCost, #uom").prop('readonly', true).end()
                    .find("button.delete").removeClass('disabled');
            $currentRow.find("input[name='dRate']").change();
            $currentRow.find("input[name='estQty']").change();
            $currentRow.find("input[name='estCost']").change();
            $currentRow.removeClass('add-row');
            $currentRow.find("#taskCode").prop('disabled', true).selectpicker('render');
            $currentRow.find("#uom").prop('disabled', true).selectpicker('render');
            $currentRow.find("#contactor").prop('disabled', true).selectpicker('render');
            $currentRow.find("#rate").prop('disabled', true);
            $currentRow.find(".addEmployee").prop('disabled', true);
            $currentRow.find(".addTaskContractor").prop('disabled', true);
            if ($flag) {
                var $newRow = $($addRowSample.clone()).removeClass('sample hidden').appendTo($('#add-new-task-row', $taskTable));
                setTaskTypeahead();
                $newRow.find("#taskCode").focus();
            }
        } else if ($(this).hasClass('save')) {
            var taskId = $(this).parent().parent().data('id');
            var $currentRow = $thisRow;
            $currentRow
                    .removeClass('edit-row')
                    .find("#taskCode, input[name='dRate'],input[name='estQty'],input[name='estCost'],#rate, #dCost, #uom").prop('readonly', true).end()
                    .find("button.delete").removeClass('disabled');
            $currentRow.find("input[name='dRate']").change();
            $currentRow.find("input[name='estQty']").change();
            $currentRow.find("input[name='estCost']").change();
            $currentRow.find("#taskCode").prop('disabled', true).selectpicker('render');
            $currentRow.find("#uom").prop('disabled', true).selectpicker('render');
            $currentRow.find("#contactor").prop('disabled', true).selectpicker('render');
            $currentRow.find("#rate").prop('disabled', true);
            $currentRow.find(".addEmployee").prop('disabled', true);
            $currentRow.find(".addTaskContractor").prop('disabled', true);
            taskRowEdit = false;
        }

        $(this, $currentRow).parent().find('.edit').removeClass('hidden');
        $(this, $currentRow).parent().find('.add').addClass('hidden');
        $(this, $currentRow).parent().find('.save').addClass('hidden');
        $(this, $currentRow).parent().parent().find('.delete').removeClass('disabled');
        $($thisRow).removeClass('taskToAdd');
        $($thisRow).removeClass('taskToSave');

        //append task increment ID to task ID
        if ($(this).hasClass('save')) {
            var saveTaskID = $thisRow.data('id') + '_' + $thisRow.data('taskIncID');
            tasks[saveTaskID] = thisVals;
        } else {
            var increID = rowincrementID;
            if($thisRow.data('taskIncID')!== undefined){
                increID = $thisRow.data('taskIncID')
            }
            thisVals = getCurrentTaskData($thisRow);
            tasks[thisVals.taskID + '_' + increID] = thisVals;

            if (!$.isEmptyObject(taskCardTaskList)) {}
            $.each(taskCardTaskList, function(index, value) {
                if (value.taskID == thisVals.taskID && $.isEmptyObject(value.taskCardProducts) == false && thisVals.taskCardID == value.taskCardID) {
                    $.each(value.taskCardProducts, function(ind, val) {
                        taskTempItems[val.productID+"_"+thisVals.taskID + '_' + increID] = val;                       
                    });                                       
                }    
            });
            if(!$thisRow.hasClass('copied')){
                rowincrementID++;
            }
        }
    });

    $taskTable.on('click', 'button.edit', function(e) {
        taskRowEdit = true;
        var $thisRow = $(this).parents('tr');

        $thisRow.addClass('edit-row')
                .find("input[name='dRate'],input[name='estQty'],#rate,#dCost, #uom, #contactor").prop('readonly', false).end()
                .find("button.delete").addClass('disabled');
        $thisRow.find("button.addEmployee").removeAttr('disabled');
        $thisRow.find("button.addTaskContractor").removeAttr('disabled');
        $thisRow.find("input[name='dRate']").change();
        $thisRow.find("input[name='estQty']").change();
        $thisRow.find("#uom").prop('disabled', false).selectpicker('render');
        $thisRow.find("#rate").prop('disabled', false);
        $thisRow.find("#contactor").prop('disabled', false).selectpicker('render');
        $thisRow.addClass('taskToSave');
        $(this, $thisRow).parent().find('.edit').addClass('hidden');
        $(this, $thisRow).parent().find('.save').removeClass('hidden');
    });

    $taskTable.on('click', 'button.delete', function(e) {
        e.preventDefault();
        if (!$(this).hasClass('disabled')) {
            var $thisRow = $(this).parents('tr');

            var taskID = $thisRow.data('id') + '_' + $thisRow.data('taskIncID');

            $.each(taskTempItems, function(index, val) {
                removeTaskTempProducts(val.productID+"_"+taskID);
            });

            delete tasks[taskID];
            selectedId = '';
            $thisRow.remove();
        }
    });

    function removeTaskTempProducts(productKey)
    {
        if (taskTempItems[productKey] != undefined) {
            delete taskTempItems[productKey];
        } 
    }

    function calculateTotalEstimatedCost()
    {
        var totalEstimatedCost = 0;
        if (!$.isEmptyObject(jobTasks)) {
            $.each(jobTasks, function(index, value) {
                $.each(value, function(ind, val) {
                    totalEstimatedCost += parseFloat(val.estimatedCost.replace(/,/g, ''));
                });    
            });
        }

        if (!$.isEmptyObject(jobTaskProducts)) {
            $.each(jobTaskProducts, function(index, value) {
                $.each(value, function(ind, val) {
                    if (jobEditflag) {
                        totalEstimatedCost += parseFloat(val.jobMaterialProCost);
                    } else {
                        totalEstimatedCost += parseFloat(val.jobMaterialProCost.replace(/,/g, ''));
                    }
                });
            });
        }

        $('#jobEstCost').val(accounting.formatMoney(totalEstimatedCost));
    }

    $('#addTcTasks').on('click', function(e) {
        e.preventDefault();
       
        if ($('#taskCardTaskTable .taskToAdd').length > 0) {
            p_notification(false, eb.getMessage('ERR_ADD_SELECT_TASK'));
            return;
        }

        if ($('#taskCardTaskTable .taskToSave').length > 0) {
            p_notification(false, eb.getMessage('ERR_ADD_SELECT_EDIT_TASK'));
            return;
        }

        // validate products before closing modal
        if (!jobTaskModalValidate(e)) {
            e.stopPropagation();
            return false;
        } else {
            $('#addTasksModal').modal('hide');
            if (!$.isEmptyObject(jobTasks)) {
                $.each(jobTasks, function(index, value) {
                    var size = Object.keys(value).length;
                    if (size > 0) {
                        var count = size+" tasks added";
                        $('#taskCount').text(count);
                   }  
                });
            }
        }
    });

    function jobTaskModalValidate(e) {
        var jobCode = $('#jobCode').val();
        if ($.isEmptyObject(tasks)) {
            jobTasks = {};
            return true;   
        } else {
            jobTasks[jobCode] = tasks;

            if (!$.isEmptyObject(taskTempItems)) {
                taskItems = taskTempItems;
            }

            calculateTotalEstimatedCost();
            return true;
        }
    }

    $('#taskCardNumbers').append('<option value="">Select Task Cards</option>');
    $('#taskCardNumbers').val('').selectpicker('refresh');

    loadDropDownFromDatabase('/task_card_api/search-task-cards-for-dropdown', "", 0, '#taskCardNumbers');
    $('#taskCardNumbers').selectpicker('refresh');
    $('#taskCardNumbers').on('change', function() {
        resetTaskAddPage();
        if(!($(this).val() == null || $(this).val() == 0) && taskCardIDs != $(this).val()){
            taskCardIDs = $(this).val();
            getTaskCardDetails(taskCardIDs);
        }
    });

    function resetTaskAddPage() {
        tasks = {};
        taskProducts = {};
        taskEmployees = {};
        tasksTotal = {};
        taskTempItems = {};
        taskItems = {};
        $('#add-new-task-row').find('tr').each(function() {
            if (!$(this).hasClass('add-row')) {
                $(this).remove();
            }
        });
        var $currentRow = getAddRow();
        $currentRow.removeClass('hidden');
        $('#jobEstCost').val('');
    }

    function getTaskCardDetails(taskCardIDs) {
        eb.ajax({
            type: 'POST',
            url: BASE_URL + '/task_card_api/getTaskCardDetails',
            data: {
                taskCardIDs: taskCardIDs
            },
            success: function(respond) {
                if (respond.status) {
                    taskCardTaskList = respond.data.taskCardTask;
                    locationProducts = respond.data.locationProducts;

                    $.each(respond.data.taskCardTask, function(index, value) {
                        var $currentRow = getAddRow();

                        //product related data append in here
                        $currentRow.data('taskCardID', value.taskCardID);
                        $currentRow.data('id', value.taskID);
                        $currentRow.data('icid',rowincrementID);
                        var rowIncID = rowincrementID;
                        $currentRow.data('taskIncID',rowincrementID);
                        $currentRow.addClass('copied');
                        $currentRow.addClass('taskToAdd');
                        rowincrementID++;
                        $currentRow.attr('id', 'task' + value.taskID);
                        $currentRow.data('stockupdate', false);
                        $("#taskCode", $currentRow).empty();
                        $("#taskCode", $currentRow).
                                append($("<option></option>").
                                        attr("value", value.taskID).
                                        text(value.taskName + '-' + value.taskCode));
                        $("#taskCode", $currentRow).val(value.taskID).prop('disabled', 'disabled');
                        $("#taskCode", $currentRow).data('taskIID', value.taskID);
                        $("#taskCode", $currentRow).data('taskCardID', value.taskCardID);
                        $("#taskCode", $currentRow).data('taskArrayID', index);
                        $("#taskCode", $currentRow).selectpicker('refresh');
                        $(".ratePicker", $currentRow).removeClass('hidden');
                        $(".rateDiv", $currentRow).addClass('hidden');
                        var optionsAsString = "";
                        $.each(respond.data.ratesSet[value.taskCardTaskID], function(key, value2){
                            optionsAsString += "<option value='" + parseFloat(value2) + "'>" + parseFloat(value2) + "</option>";

                        });

                        $('select[name="rate"]',$currentRow).append( optionsAsString );
                        $('#dCost',$currentRow).val('0.00');
                        
                        $("#uom", $currentRow).empty();
                        $("#uom", $currentRow).
                                append($("<option></option>").
                                        attr("value", value.uomID).
                                        text(value.taskCardUomAbbr));
                        $("#uom", $currentRow).val(value.uomID).prop('disabled', 'disabled');
                        $("#uom", $currentRow).data('uomIID', value.uomID);
                        $("#uom", $currentRow).selectpicker('refresh');
                        
                        $("input[name='dCost']", $currentRow).trigger(jQuery.Event("focusout"));

                        $currentRow.removeClass('add-row');
                        $currentRow.find("button.delete").removeClass('disabled');
                        var $newRow = $($addRowSample.clone()).removeClass('sample hidden').appendTo($('#add-new-task-row', $taskTable));
                        $newRow.find("#taskCode").focus();

                    });
                    var $currentRow = getAddRow();
                    setTaskTypeahead();
                }
            }
        });
    }

    // add resources modal
    $('#addResources').on('click', function(e) {
        setResourcesTypeahead();
        $('#addTaskProductsModal').modal('show');
    });

    function setResourcesTypeahead()
    {
        loadDropDownFromDatabase('/productAPI/search-location-products-for-dropdown', '', 0, '#rawMaterialID', '', '', "jobRawMaterials");
        $('#rawMaterialID').trigger('change');
        var rawMaterialKey;
        $('#rawMaterialID').on('change', function() {
            if ($(this).val() > 0 && $(this).val() != rawMaterialKey) {
                rawMaterialKey = $(this).val();
                selectedRMProductID = $(this).val();
                $('#rawMaterialID').
                        append($("<option></option>").
                                attr("value", 'select').
                                text("Select Raw Material"));
                $('#rawMaterialID').val('select');
                $('#rawMaterialID').selectpicker('refresh');

                var itemDuplicateCheck = false;

                $.each(rawMaterialProducts, function(index, val) {
                    if (val.jobMaterialProID == rawMaterialKey) {
                        itemDuplicateCheck = true;                        
                    }
                });

                if (!itemDuplicateCheck) {
                    selectProducts(rawMaterialKey, 'rawMaterial');
                } else {
                    p_notification(false, eb.getMessage('ERR_GRN_INSERT_PROD'));
                    $('#rawMaterialID').focus();
                }
            }
        });

        loadDropDownFromDatabase('/productAPI/search-location-products-for-dropdown', '', 0, '#fixedAssetsID', '', '', "jobFixedAssets");
        $('#fixedAssetsID').trigger('change');
        var fixedAssetsKey;
        $('#fixedAssetsID').on('change', function() {
            if ($(this).val() > 0 && $(this).val() != fixedAssetsKey) {
                fixedAssetsKey = $(this).val();
                selectedFAProductID = $(this).val();
                $('#fixedAssetsID').
                        append($("<option></option>").
                                attr("value", 'select').
                                text("Select Fixed Assets"));
                $('#fixedAssetsID').val('select');
                $('#fixedAssetsID').selectpicker('refresh');

                var itemDuplicateCheck = false;

                $.each(fixedAssetsProducts, function(index, val) {
                    if (val.jobMaterialProID == fixedAssetsKey) {
                        itemDuplicateCheck = true;                        
                    }
                });

                if (!itemDuplicateCheck) {
                    selectProducts(fixedAssetsKey, "fixedAssets");
                } else {
                    p_notification(false, eb.getMessage('ERR_GRN_INSERT_PROD'));
                    $('#fixedAssetsID').focus();
                }
            }
        });

        loadDropDownFromDatabase('/productAPI/search-location-products-for-dropdown', '', 0, '#temporaryProductID', '', '', "jobTradingGoods");
        $('#temporaryProductID').trigger('change');
        var tempProductKey;
        $('#temporaryProductID').on('change', function() {
            if ($(this).val() > 0 && ($(this).val() != tempProductKey)) {
                tempProductKey = $(this).val();
                selectedTGProductID = $(this).val();
                $('#temporaryProductID').
                        append($("<option></option>").
                                attr("value", 'select').
                                text("Select Item"));
                $('#temporaryProductID').val('select');
                $('#temporaryProductID').selectpicker('refresh');

                var itemDuplicateCheck = false;

                $.each(tradingGoodProducts, function(index, val) {
                    if (val.jobMaterialProID == tempProductKey) {
                        itemDuplicateCheck = true;                        
                    }
                });

                if (!itemDuplicateCheck) {
                    selectProducts(tempProductKey, "tradingGoods");
                } else {
                    p_notification(false, eb.getMessage('ERR_GRN_INSERT_PROD'));
                    $('#fixedAssetsID').focus();
                }
            } 
        });

        if (!$.isEmptyObject(taskItems)) {
            clearTaskItemRows();
            $.each(taskItems, function(index, value) {
                if (value.rawMaterials == "1" && JOBSETTINGS['rawMaterials'] == 1) {
                    var rawMaterialKey = value.productID;
                    selectedRMProductID = value.productID;
                    if (!rawMaterialProducts[rawMaterialKey]) {
                        selectProducts(rawMaterialKey, 'rawMaterial',index, value.taskCardTaskProductQty);
                    }
                }

                if (value.fixedAssets == "1" && JOBSETTINGS['fixedAssets'] == 1) {
                    var fixedAssetsKey = value.productID;
                    selectedFAProductID = value.productID;
                    if (!fixedAssetsProducts[fixedAssetsKey]) {
                        selectProducts(fixedAssetsKey, 'fixedAssets',index, value.taskCardTaskProductQty);
                    }
                }

                if (value.tradingGoods == "1" || value.finishedGoods == "1") {
                    var tempProductKey = value.productID;
                    selectedTGProductID = value.productID;
                    if (!tradingGoodProducts[tempProductKey]) {
                        selectProducts(tempProductKey, 'tradingGoods', index, value.taskCardTaskProductQty);
                    }
                }
            });
        }
    }

    function clearTaskItemRows()
    {
        $("#addTaskProductsModal #rawMaterialTable tbody .addedTaskProducts").remove();
        $("#addTaskProductsModal #fixedAssetsTable tbody .addedTaskProducts").remove();
        $("#addTaskProductsModal #tradingGoodsTable tbody .addedTaskProducts").remove();

        $.each(tradingGoodProducts, function(index, val) {
            if (val.jobMaterialTaskID != undefined) {
                delete tradingGoodProducts[index];
            }
        });
        
        $.each(fixedAssetsProducts, function(index, val) {
            if (val.jobMaterialTaskID != undefined) {
                delete fixedAssetsProducts[index];
            }
        });
        
        $.each(rawMaterialProducts, function(index, val) {
            if (val.jobMaterialTaskID != undefined) {
                delete rawMaterialProducts[index];
            }
        });

    }

    function ClearRowFields($currentRow) {
        $("input[name='addQuantity']", $currentRow).prop('readonly', false);
        $("input[name='unitPrice']", $currentRow).prop('disabled', false);
    }

    function selectProducts(selectedProductID, productType, productKey, taskProductQty = null) {
        // check if product is already selected
        var $currentRow = getProductAddRow();
        ClearRowFields($currentRow);
        if (!$.isEmptyObject(activeProductData[selectedProductID])) {
            if (productType == "rawMaterial") {
                $('.raw-product-table').removeClass('hidden');
                setRawMaterialProductList(selectedRMProductID, productKey, taskProductQty);
            } else if (productType == "fixedAssets") {
                $('.fixed-assests-product-table').removeClass('hidden');
                setFixedAssetsProductList(selectedFAProductID, productKey, taskProductQty);
            } else if (productType == "tradingGoods") {
                $('.trading-goods-table').removeClass('hidden');
                setTradingProductList(selectedTGProductID, productKey, taskProductQty);
            }
            $("input[name='addedQuantity']").focus();
        }
    }


    function setRawMaterialProductList(rawMaterialProID, productKey, taskProductQty) {
        if (productKey != undefined) {
            var newTrID = 'tr_' + productKey;
            var clonedRow = $($('#rawProductPreSetSample').clone()).attr('id', newTrID).addClass('addedTaskProducts');
        } else {
            var newTrID = 'tr_' + rawMaterialProID;
            var clonedRow = $($('#rawProductPreSetSample').clone()).attr('id', newTrID).addClass('addedRawMaterialProduct');
        }
        $("#rawProdctNameAndCode", clonedRow).text(activeProductData[rawMaterialProID].pN+'-'+activeProductData[rawMaterialProID].pC);
        
        if (taskProductQty == null) {
            $("input[name='addedQuantity']", clonedRow).addUom(activeProductData[rawMaterialProID].uom);
        } else {
            $("input[name='addedQuantity']", clonedRow).val(parseFloat(taskProductQty)).addUom(activeProductData[rawMaterialProID].uom);
        }
        $("input[name='availableQuantity']", clonedRow).val(activeProductData[rawMaterialProID].LPQ).prop('readonly', true).addUom(activeProductData[rawMaterialProID].uom);
        $("input[name='reOrderQuantity']", clonedRow).addUom(activeProductData[rawMaterialProID].uom);
        $("input[name='unitPrice']", clonedRow).val(parseFloat(activeProductData[rawMaterialProID].dPP).toFixed(2)).addUomPrice(activeProductData[rawMaterialProID].uom);
        clonedRow.removeClass('hidden');
        clonedRow.addClass('productToAdd');
        clonedRow.insertBefore('#rawProductPreSetSample');
        $("#rawMaterialID").attr('disabled', true);
        if (productKey != undefined) {
            var productTaskID = productKey.split('_')[1];
            rawMaterialProducts[productKey] = new jobMaterial(rawMaterialProID, activeProductData[rawMaterialProID].pN+'-'+activeProductData[rawMaterialProID].pC, 0, activeProductData[rawMaterialProID].dSP, 0, 0, productTaskID, activeProductData[rawMaterialProID].lPID, null, null);
        } else {
            rawMaterialProducts[rawMaterialProID] = new jobMaterial(rawMaterialProID, activeProductData[rawMaterialProID].pN+'-'+activeProductData[rawMaterialProID].pC, 0, activeProductData[rawMaterialProID].dSP, 0, 0, null, activeProductData[rawMaterialProID].lPID, null, null);
        }
    }

    function setFixedAssetsProductList(fixedAssetsProID, productKey, taskProductQty) {

        if (productKey != undefined) {
            var newTrID = 'tr_' + productKey;
            var clonedRow = $($('#fixedProductPreSetSample').clone()).attr('id', newTrID).addClass('addedTaskProducts');
        } else {
            var newTrID = 'tr_' + fixedAssetsProID;
            var clonedRow = $($('#fixedProductPreSetSample').clone()).attr('id', newTrID).addClass('addedFixedAssetsProduct');
        }
        $("#fixedProdctNameAndCode", clonedRow).text(activeProductData[fixedAssetsProID].pN+'-'+activeProductData[fixedAssetsProID].pC);
        
        if (taskProductQty == null) {
            $("input[name='addedQuantity']", clonedRow).addUom(activeProductData[fixedAssetsProID].uom);
        } else {
            $("input[name='addedQuantity']", clonedRow).val(parseFloat(taskProductQty)).addUom(activeProductData[fixedAssetsProID].uom);
        }
        $("input[name='availableQuantity']", clonedRow).val(activeProductData[fixedAssetsProID].LPQ).prop('readonly', true).addUom(activeProductData[fixedAssetsProID].uom);
        $("input[name='reOrderQuantity']", clonedRow).addUom(activeProductData[fixedAssetsProID].uom);
        $("input[name='unitPrice']", clonedRow).val(activeProductData[fixedAssetsProID].dPP).addUomPrice(activeProductData[fixedAssetsProID].uom);
        clonedRow.removeClass('hidden');
        clonedRow.addClass('productToAdd');
        clonedRow.insertBefore('#fixedProductPreSetSample');
        $('#fixedAssetsID').attr('disabled', true);
        if (productKey != undefined) {
            var productTaskID = productKey.split('_')[1];
            fixedAssetsProducts[productKey] = new jobMaterial(fixedAssetsProID, activeProductData[fixedAssetsProID].pN+'-'+activeProductData[fixedAssetsProID].pC,0, activeProductData[fixedAssetsProID].dSP, 0, 0, productTaskID,activeProductData[fixedAssetsProID].lPID , null, null);
        } else {
            fixedAssetsProducts[fixedAssetsProID] = new jobMaterial(fixedAssetsProID, activeProductData[fixedAssetsProID].pN+'-'+activeProductData[fixedAssetsProID].pC,0, activeProductData[fixedAssetsProID].dSP, 0, 0, null, activeProductData[fixedAssetsProID].lPID, null, null);
        }
    }

    function setTradingProductList(tradingGoodProID, productKey, taskProductQty) {
        if (productKey != undefined) {
            var newTrID = 'tr_' + productKey;
            var clonedRow = $($('#tradingGoodsPreSetSample').clone()).attr('id', newTrID).addClass('addedTaskProducts');
        } else {
            var newTrID = 'tr_' + tradingGoodProID;
            var clonedRow = $($('#tradingGoodsPreSetSample').clone()).attr('id', newTrID).addClass('addedtradingGoodProduct');
        }
        $("#tradingGoodsNameAndCode", clonedRow).text(activeProductData[tradingGoodProID].pN+'-'+activeProductData[tradingGoodProID].pC);

        if (taskProductQty == null) {
            $("input[name='addedQuantity']", clonedRow).addUom(activeProductData[tradingGoodProID].uom);
        } else {
            $("input[name='addedQuantity']", clonedRow).val(parseFloat(taskProductQty)).addUom(activeProductData[tradingGoodProID].uom);
        }
        $("input[name='availableQuantity']", clonedRow).val(activeProductData[tradingGoodProID].LPQ).prop('readonly', true).addUom(activeProductData[tradingGoodProID].uom);
        $("input[name='reOrderQuantity']", clonedRow).addUom(activeProductData[tradingGoodProID].uom);
        $("input[name='unitPrice']", clonedRow).val(activeProductData[tradingGoodProID].dPP).addUomPrice(activeProductData[tradingGoodProID].uom);
        clonedRow.removeClass('hidden');
        clonedRow.insertBefore('#tradingGoodsPreSetSample');
        clonedRow.addClass('productToAdd');
        $('#temporaryProductID').attr('disabled', true);
        if (productKey != undefined) {
            var productTaskID = productKey.split('_')[1];
            tradingGoodProducts[productKey] = new jobMaterial(tradingGoodProID, activeProductData[tradingGoodProID].pN+'-'+activeProductData[tradingGoodProID].pC,0, activeProductData[tradingGoodProID].dSP, 0, 0, productTaskID ,activeProductData[tradingGoodProID].lPID, null, null);
        } else {
            tradingGoodProducts[tradingGoodProID] = new jobMaterial(tradingGoodProID, activeProductData[tradingGoodProID].pN+'-'+activeProductData[tradingGoodProID].pC,0, activeProductData[tradingGoodProID].dSP, 0, 0, null, activeProductData[tradingGoodProID].lPID, null, null);
        }
    }

    function jobMaterial(jobMaterialProID, jobMaterialProNameCode, jobMaterialProTotalQty, jobMaterialProUnitPrice, jobMaterialProCost, jobMaterialReorderQty ,jobMaterialTaskID, jobMaterialTaskLocationProductID, jobMaterialSelectedUomID, jobMaterialEditedProductID) {
        this.jobMaterialProID = jobMaterialProID;
        this.jobMaterialProNameCode = jobMaterialProNameCode;
        this.jobMaterialProTotalQty = jobMaterialProTotalQty;
        this.jobMaterialProUnitPrice = jobMaterialProUnitPrice;
        this.jobMaterialProCost = jobMaterialProCost;
        this.jobMaterialReorderQty = jobMaterialReorderQty;
        this.jobMaterialTaskID = jobMaterialTaskID;
        this.jobMaterialTaskLocationProductID = jobMaterialTaskLocationProductID;
        this.jobMaterialSelectedUomID = jobMaterialSelectedUomID;
        this.jobMaterialEditedProductID = jobMaterialEditedProductID;
    }

    $(document).on('click', '.raw-product-plus, .raw-product-save', function() {
        var unitPriceValue = $(this).closest('tr').find("input[name='unitPrice']").val();
        var estQty = $(this).closest('tr').find("input[name='addedQuantity']").val();
        var reOrderQty = $(this).closest('tr').find("input[name='reOrderQuantity']").val();
        var estimatedCost = $(this).closest('tr').find("input[name='estimatedCost']").val();
        var selectedUomID = $(this).closest('tr').find("input[name='addedQuantity']").siblings('.uom-select').children('button').find('.selected').data('uomID');

        if (isNaN(parseFloat(unitPriceValue)) || parseFloat(unitPriceValue) < 0) {
            p_notification(false, eb.getMessage('ERR_VLID_UPRICE'));
            $(this).closest('tr').find("input[name='unitPrice']").select();
            return false;
        } 

        if (parseFloat(estQty) <= 0 || estQty == "") {
            p_notification(false, eb.getMessage('ERR_VLID_ESTQTY'));
            $(this).closest('tr').find("input[name='addedQuantity']").select();    
            return false;
        }

        // if (parseFloat(reOrderQty) < 0 || reOrderQty == "") {
        //     p_notification(false, eb.getMessage('ERR_VLID_REODRERQTY'));
        //     $(this).closest('tr').find("input[name='reOrderQuantity']").select();    
        //     return false;
        // }

        // if (parseFloat(estQty) < parseFloat(reOrderQty)) {
        //     p_notification(false, eb.getMessage('ERR_VLID_EST_REODRERQTY'));
        //     $(this).closest('tr').find("input[name='addedQuantity']").select();    
        //     return false;
        // }

        var rawMProTrID = $(this).closest('tr').attr('id');
        var rawMProID = rawMProTrID.split('tr_')[1].trim();
        if (rawMaterialProducts[rawMProID]) {
            delete taskItems[rawMProID];
            delete taskTempItems[rawMProID];
            rawMaterialProducts[rawMProID].jobMaterialProUnitPrice = unitPriceValue;
            rawMaterialProducts[rawMProID].jobMaterialProTotalQty = estQty;
            rawMaterialProducts[rawMProID].jobMaterialProCost = estimatedCost;
            rawMaterialProducts[rawMProID].jobMaterialReorderQty = reOrderQty;
            rawMaterialProducts[rawMProID].jobMaterialSelectedUomID = selectedUomID;
            $("#rawMaterialID").attr('disabled', false);
            $(this).closest('tr').find("input[name='unitPrice']").val(parseFloat(unitPriceValue).toFixed(2));
            $(this).closest('tr').find("input.uomPrice").attr('disabled', true);
            $(this).closest('tr').find("input.uomqty").attr('disabled', true);
            $(this).addClass('hidden');
            $(this).closest('tr').find(".raw-product-edit").removeClass('hidden');
        }
        $(this).closest('tr').removeClass('productToAdd');
        $(this).closest('tr').removeClass('productToSave');
    });

    $(document).on('click', '.raw-product-edit', function() {
        $(this).closest('tr').find("input.uomPrice").attr('disabled', false);
        $(this).closest('tr').find("input.uomqty").attr('disabled', false);
        $(this).closest('tr').find("div.availableQuantity-div input").attr('disabled', true);
        $(this).addClass('hidden');
        $(this).closest('tr').find(".raw-product-plus").addClass('hidden');
        $(this).closest('tr').find(".raw-product-save").removeClass('hidden');
        $(this).closest('tr').addClass('productToSave');
    });

    $(document).on('click', '.raw-product-delete', function() {
        var deleteRawMProTrID = $(this).closest('tr').attr('id');
        var deleteRawMProID = deleteRawMProTrID.split('tr_')[1].trim();
        delete rawMaterialProducts[deleteRawMProID];
        delete taskItems[deleteRawMProID];
        delete taskTempItems[deleteRawMProID];
        $('#' + deleteRawMProTrID).remove();
        $("#rawMaterialID").attr('disabled', false);

        if (_.values(rawMaterialProducts).length == 0) {
            $('.raw-product-table').addClass('hidden');
        }
    });


    $(document).on('click', '.fixed-assests-product-plus , .fixed-assests-product-save', function() {
        var unitPriceValue = $(this).closest('tr').find("input[name='unitPrice']").val();
        var estQty = $(this).closest('tr').find("input[name='addedQuantity']").val();
        var reOrderQty = $(this).closest('tr').find("input[name='reOrderQuantity']").val();
        var estimatedCost = $(this).closest('tr').find("input[name='estimatedCost']").val();
        var selectedUomID = $(this).closest('tr').find("input[name='addedQuantity']").siblings('.uom-select').children('button').find('.selected').data('uomID');

        if (isNaN(parseFloat(unitPriceValue)) || parseFloat(unitPriceValue) < 0) {
            p_notification(false, eb.getMessage('ERR_VLID_UPRICE'));
            $(this).closest('tr').find("input[name='unitPrice']").select();
            return false;
        } 

        if (parseFloat(estQty) <= 0 || estQty == "") {
            p_notification(false, eb.getMessage('ERR_VLID_ESTQTY'));
            $(this).closest('tr').find("input[name='addedQuantity']").select();    
            return false;
        }

        // if (parseFloat(reOrderQty) < 0 || reOrderQty == "") {
        //     p_notification(false, eb.getMessage('ERR_VLID_REODRERQTY'));
        //     $(this).closest('tr').find("input[name='reOrderQuantity']").select();    
        //     return false;
        // }

        // if (parseFloat(estQty) < parseFloat(reOrderQty)) {
        //     p_notification(false, eb.getMessage('ERR_VLID_EST_REODRERQTY'));
        //     $(this).closest('tr').find("input[name='addedQuantity']").select();    
        //     return false;
        // }

        var fixedAProTrID = $(this).closest('tr').attr('id');
        var fixedAProID = fixedAProTrID.split('tr_')[1].trim();
        if (fixedAssetsProducts[fixedAProID]) {
            delete taskItems[fixedAProID];
            delete taskTempItems[fixedAProID];
            fixedAssetsProducts[fixedAProID].jobMaterialProUnitPrice = unitPriceValue;
            fixedAssetsProducts[fixedAProID].jobMaterialProTotalQty = estQty;
            fixedAssetsProducts[fixedAProID].jobMaterialProCost = estimatedCost;
            fixedAssetsProducts[fixedAProID].jobMaterialReorderQty = reOrderQty;
            fixedAssetsProducts[fixedAProID].jobMaterialSelectedUomID = selectedUomID;
            $("#fixedAssetsID").attr('disabled', false);
            $(this).closest('tr').find("input[name='unitPrice']").val(parseFloat(unitPriceValue).toFixed(2));
            $(this).closest('tr').find("input.uomPrice").attr('disabled', true);
            $(this).closest('tr').find("input.uomqty").attr('disabled', true);
            $(this).addClass('hidden');
            $(this).closest('tr').find(".fixed-assests-product-edit").removeClass('hidden');
        }
        $(this).closest('tr').removeClass('productToAdd');
        $(this).closest('tr').removeClass('productToSave');
    });

    $(document).on('click', '.fixed-assests-product-edit', function() {
        $(this).closest('tr').find("input.uomPrice").attr('disabled', false);
        $(this).closest('tr').find("input.uomqty").attr('disabled', false);
        $(this).closest('tr').find("div.availableQuantity-div input").attr('disabled', true);
        $(this).addClass('hidden');
        $(this).closest('tr').find(".fixed-assests-product-plus").addClass('hidden');
        $(this).closest('tr').find(".fixed-assests-product-save").removeClass('hidden');
        $(this).closest('tr').addClass('productToSave');
    });

    $(document).on('click', '.fixed-assests-product-delete', function() {

        var deleteFixedAProTrID = $(this).closest('tr').attr('id');
        var deleteFixedAProID = deleteFixedAProTrID.split('tr_')[1].trim();
        delete fixedAssetsProducts[deleteFixedAProID];
        delete taskItems[deleteFixedAProID];
        delete taskTempItems[deleteFixedAProID];
        $('#' + deleteFixedAProTrID).remove();
        $("#fixedAssetsID").attr('disabled', false);

        if (_.values(fixedAssetsProducts).length == 0) {
            $('.fixed-assests-product-table').addClass('hidden');
        }
    });

    $(document).on('click', '.trading-goods-plus , .trading-goods-save', function() {

        
        var unitPriceValue = $(this).closest('tr').find("input[name='unitPrice']").val();
        var estQty = $(this).closest('tr').find("input[name='addedQuantity']").val();
        var reOrderQty = $(this).closest('tr').find("input[name='reOrderQuantity']").val();
        var estimatedCost = $(this).closest('tr').find("input[name='estimatedCost']").val();
        var selectedUomID = $(this).closest('tr').find("input[name='addedQuantity']").siblings('.uom-select').children('button').find('.selected').data('uomID');

        if (isNaN(parseFloat(unitPriceValue)) || parseFloat(unitPriceValue) < 0) {
            p_notification(false, eb.getMessage('ERR_VLID_UPRICE'));
            $(this).closest('tr').find("input[name='unitPrice']").select();
            return false;
        } 

        if (parseFloat(estQty) <= 0 || estQty == "") {
            p_notification(false, eb.getMessage('ERR_VLID_ESTQTY'));
            $(this).closest('tr').find("input[name='addedQuantity']").select();    
            return false;
        }

        // if (parseFloat(reOrderQty) < 0 || reOrderQty == "") {
        //     p_notification(false, eb.getMessage('ERR_VLID_REODRERQTY'));
        //     $(this).closest('tr').find("input[name='reOrderQuantity']").select();    
        //     return false;
        // }

        // if (parseFloat(estQty) < parseFloat(reOrderQty)) {
        //     p_notification(false, eb.getMessage('ERR_VLID_EST_REODRERQTY'));
        //     $(this).closest('tr').find("input[name='addedQuantity']").select();    
        //     return false;
        // }
        
        var tradingProTrID = $(this).closest('tr').attr('id');
        var tradingProID = tradingProTrID.split('tr_')[1].trim();
        if (tradingGoodProducts[tradingProID]) {
            delete taskItems[tradingProID];
            delete taskTempItems[tradingProID];
            tradingGoodProducts[tradingProID].jobMaterialProUnitPrice = unitPriceValue;
            tradingGoodProducts[tradingProID].jobMaterialProTotalQty = estQty;
            tradingGoodProducts[tradingProID].jobMaterialProCost = estimatedCost;
            tradingGoodProducts[tradingProID].jobMaterialReorderQty = reOrderQty;
            tradingGoodProducts[tradingProID].jobMaterialSelectedUomID = selectedUomID;
            $("#temporaryProductID").attr('disabled', false);
            $(this).closest('tr').find("input[name='unitPrice']").val(parseFloat(unitPriceValue).toFixed(2));
            $(this).closest('tr').find("input.uomPrice").attr('disabled', true);
            $(this).closest('tr').find("input.uomqty").attr('disabled', true);
            $(this).addClass('hidden');
            $(this).closest('tr').find(".trading-goods-edit").removeClass('hidden');
        }
        $(this).closest('tr').removeClass('productToAdd');
        $(this).closest('tr').removeClass('productToSave');
    });


    $(document).on('click', '.trading-goods-edit', function() {
        $(this).closest('tr').find("input.uomPrice").attr('disabled', false);
        $(this).closest('tr').find("input.uomqty").attr('disabled', false);
        $(this).closest('tr').find("div.availableQuantity-div input").attr('disabled', true);
        $(this).addClass('hidden');
        $(this).closest('tr').find(".trading-goods-plus").addClass('hidden');
        $(this).closest('tr').find(".trading-goods-save").removeClass('hidden');
        $(this).closest('tr').addClass('productToSave');
    });

    $(document).on('click', '.trading-goods-delete', function() {

        var deleteTradingProTrID = $(this).closest('tr').attr('id');
        var deleteTradingProID = deleteTradingProTrID.split('tr_')[1].trim();
        delete tradingGoodProducts[deleteTradingProID];
        delete taskItems[deleteTradingProID];
        delete taskTempItems[deleteTradingProID];
        $('#' + deleteTradingProTrID).remove();
        $("#temporaryProductID").attr('disabled', false);

        if (_.values(tradingGoodProducts).length == 0) {
            $('.trading-goods-table').addClass('hidden');
        }
    });

    $('#addTaskProductsModal #rawMaterialTable').on('focusout', '.uomqty,.uomPrice', function(e){
        e.preventDefault();
        var $thisRow = $(this).parents('tr');
        calculateMaterialRowCost($thisRow);
    });
    

    $('#addTaskProductsModal #fixedAssetsTable').on('focusout', '.uomqty,.uomPrice', function(e){
        e.preventDefault();
        var $thisRow = $(this).parents('tr');
        calculateMaterialRowCost($thisRow);
    });
    
    $('#addTaskProductsModal #tradingGoodsTable').on('focusout', '.uomqty,.uomPrice', function(e){
        e.preventDefault();
        var $thisRow = $(this).parents('tr');
        calculateMaterialRowCost($thisRow);
    });
    

    function calculateMaterialRowCost(thisRow) {
        var thisRow = thisRow;
        var itemQuentity = thisRow.find('#addedQuantity').val();
        var itemCost = (toFloat(thisRow.find('#unitPrice').val()) * toFloat(itemQuentity));
        thisRow.find('#estimatedCost').val(accounting.formatMoney(itemCost));
    }

    $('#addTCProducts').on('click', function(e) {
        e.preventDefault();

        if ($('#tradingGoodsTable .productToAdd').length > 0) {
            p_notification(false, eb.getMessage('ERR_ADD_SELECT_ITEM'));
            return;
        }

        if ($('#rawMaterialTable .productToAdd').length > 0) {
            p_notification(false, eb.getMessage('ERR_ADD_SELECT_RAW_MAT'));
            return;
        }

        if ($('#fixedAssetsTable .productToAdd').length > 0) {
            p_notification(false, eb.getMessage('ERR_ADD_SELECT_FIXED_ASSET'));
            return;
        }

        if ($('#tradingGoodsTable .productToSave').length > 0) {
            p_notification(false, eb.getMessage('ERR_ADD_SELECT_EDIT_ITEM'));
            return;
        }

        if ($('#rawMaterialTable .productToSave').length > 0) {
            p_notification(false, eb.getMessage('ERR_ADD_SELECT_EDIT_RAW_MAT'));
            return;
        }

        if ($('#fixedAssetsTable .productToSave').length > 0) {
            p_notification(false, eb.getMessage('ERR_ADD_SELECT_EDIT_FIXED_ASSET'));
            return;
        }


        // validate products before closing modal
        if (!jobProductModalValidate(e)) {
            e.stopPropagation();
            return false;
        } else {
            $('#addTaskProductsModal').modal('hide');
            if (!$.isEmptyObject(jobTaskProducts)) {
                $.each(jobTaskProducts, function(index, value) {
                    var size = Object.keys(value).length;
                    if (size > 0) {
                        var count = size+" products added";
                        $('#resourceCount').text(count);
                   }  
                });
            }
        }
    });

    function jobProductModalValidate(e) {
        var jobCode = $('#jobCode').val();
        var tempProducts = {};
        if (!$.isEmptyObject(rawMaterialProducts)) {
            $.each(rawMaterialProducts, function(index, value) {
                tempProducts[index+'_raw'] = value;
            });
        }

        if (!$.isEmptyObject(fixedAssetsProducts)) {
            $.each(fixedAssetsProducts, function(index, value) {
                tempProducts[index+'_fixA'] = value;
            });
        }

        if (!$.isEmptyObject(tradingGoodProducts)) {
            $.each(tradingGoodProducts, function(index, value) {
                tempProducts[index+'_traG'] = value;
            });
        }

        if ($.isEmptyObject(tempProducts)) {
            jobTaskProducts = {};
            return true;   
        } else {
            jobTaskProducts[jobCode] = tempProducts;
            calculateTotalEstimatedCost();
            return true;
        }
    }


    $taskTable.on('click', 'button.addEmployee', function(e) {
        var $thisRow = $(this).parents('tr');
        clearEmployeeModal();
        var taskID = $(this).parents('tr').data('id');
        if (taskID == undefined) {
            p_notification(false, eb.getMessage('PLS_SELECT_AT_LEAST_ONE_TASK'));
            return false;
        }
        var taskIncID = taskID + "_" + $(this).parents('tr').data('taskIncID');
        var rowincID = $(this).parents('tr').data('icid');
        $('#addTaskEmployeeModal').data('icid', rowincID);
        $('#addTaskEmployeeModal').data('taskIncID', rowincID);

        if (!$.isEmptyObject(taskEmployees[taskIncID])) {
            $('#addTaskEmployeeModal').data('id', $(this).parents('tr').data('id'));
        }

        if (!$.isEmptyObject(taskEmployees[taskIncID])) {
            employees = taskEmployees[getCurrentTaskData($thisRow)['taskIncID']];
            $.each(employees, function(index, value) {
                var newTrID = 'tr_' + value.employeeID;
                var clonedRow = $($('#employeePreSample').clone()).attr('id', newTrID).addClass('addedEmployees');
                $("input[name='employeeCode']", clonedRow).val(value.employeeCodeAndName);
                clonedRow.removeClass('hidden');
                clonedRow.insertBefore('#employeePreSample');
                clearAddedNewEmployeeRow();
            });

        }
        $('#addTaskEmployeeModal').modal('show');
    });


    function clearEmployeeCode() {
        $('#taskEmployeeCode')
                .empty()
                .val('')
                .append($("<option></option>")
                        .attr("value", "")
                        .text('Select Employee Code or Name'))
                .selectpicker('refresh')
                .trigger('change');
    }

    $('#taskEmployeeCode').on('change', function(e) {
        var $thisRow = $(this).parents('tr');
        if (typeof employees[$(this).val()] != 'undefined' && $(this).val() != '') {
            p_notification(false, eb.getMessage("ERR_JOB_EMPINSERT"));
            $('#taskEmployeeCode').val('');
            clearEmployeeCode();
            return false;
        } else if ($(this).val() > 0 && selectedEmployeeID != $('#taskEmployeeCode').val()) {
            selectedEmployeeID = $(this).val();
            $('#taskEmployeeCode').data('empNameCode', $(this).find("option:selected").text());
            $('#taskEmployeeCode').data('id', selectedEmployeeID);
        }
    });


    $('#add_employee').on('click', function(e) {
        e.preventDefault();
        if ($(this).is(':disabled')) {
            return false;
        } else {
            $(this).attr('disabled', true);
            var $thisRow = $(this).parents('tr');
            if ($('#taskEmployeeCode').val() == '' || $('#taskEmployeeCode').val() == null) {
                p_notification(false, eb.getMessage('ERR_JOB_SUPERVISOR'));
                $(this).attr('disabled', false);
            } else {
                if (employees[selectedEmployeeID]) {
                    p_notification(false, eb.getMessage('ERR_JOB_EMPINSERT'));
                    $('#taskEmployeeCode').val('');
                    $('#taskEmployeeCode').selectpicker('render');
                } else {
                    addNewEmployeeRow();
                }
                $(this).attr('disabled', false);
            }
        }
    });

    function addNewEmployeeRow() {

        var taskID = $('#addTaskEmployeeModal').data('id');
        var incid = $('#addTaskEmployeeModal').data('icid');
        var taskIncID = $('#addTaskEmployeeModal').data('taskIncID');

        var $thisParentRow = getAddRow(taskIncID);
        var thisVals = getCurrentTaskData($thisParentRow);
        var employeeCodeAndName = $('#taskEmployeeCode option:selected').html();
        var employeeID = $('#taskEmployeeCode').val();

        var newTrID = 'tr_' + employeeID;
        var clonedRow = $($('#employeePreSample').clone()).attr('id', newTrID).addClass('addedEmployees');
        $("input[name='employeeCode']", clonedRow).val(employeeCodeAndName);
        clonedRow.removeClass('hidden');
        clonedRow.insertBefore('#employeePreSample');
        employees[employeeID] = new taskEmployeesFun(employeeID,employeeCodeAndName);
        clearAddedNewEmployeeRow();
        $('[data-id=taskEmployeeCode]').trigger('click');
    }

    function taskEmployeesFun(employeeID, employeeCodeAndName) {
        this.employeeID = employeeID;
        this.employeeCodeAndName = employeeCodeAndName;
    }

    function clearAddedNewEmployeeRow() {
        $('#taskEmployeeCode').val('');
        clearEmployeeCode();
    }

    function clearEmployeeModal()
    {
        $("#addTaskEmployeeModal #employeeTable tbody .addedEmployees").remove();
        employees = {};
    }


    $('#addTaskEmployees').on('click', function(e) {
        e.preventDefault();

        if ($('#taskEmployeeCode').val() != 0 && $('#taskEmployeeCode').val() != null) {
            p_notification(false, eb.getMessage('ERR_ADD_SELECT_TASK_CONTRACTOR'));
            return;
        }
        
        // validate products before closing modal
        if (!taskEmployeeModalValidate(e)) {
            e.stopPropagation();
            return false;
        } else {
            $('#addTaskEmployeeModal').modal('hide');
        }
    });

    function taskEmployeeModalValidate(e) {

        var taskID = $('#addTaskEmployeeModal').data('id');
        var incid = $('#addTaskEmployeeModal').data('icid');
        var taskIncID = $('#addTaskEmployeeModal').data('taskIncID');
        $('#addTaskEmployeeModal').data('id', '');
        var $thisParentRow = getAddRow(taskIncID);
        var thisVals = getCurrentTaskData($thisParentRow);
        if ($.isEmptyObject(employees)) {
            taskEmployees = {};
            return true;
        } else {
            taskEmployees[thisVals.taskIncID] = employees;
            return true;
        }
    }


    $("#addTaskEmployeeModal #employeeTable").on('click', '.deleteEmployee', function(e) {
        e.preventDefault();
        var deleteEmprID = $(this).closest('tr').attr('id');
        var deleteEmpID = deleteEmprID.split('tr_')[1].trim();

        bootbox.confirm('Are you sure you want to remove this employee ?', function(result) {
            if (result == true) {
                delete employees[deleteEmpID];
                $('#' + deleteEmprID).remove();
            }
        });
    });


    $taskTable.on('click', 'button.addTaskContractor', function(e) {
        var $thisRow = $(this).parents('tr');
        clearTaskContractorModal();
        var taskID = $(this).parents('tr').data('id');
        if (taskID == undefined) {
            p_notification(false, eb.getMessage('PLS_SELECT_AT_LEAST_ONE_TASK'));
            return false;
        }
        var taskIncID = taskID + "_" + $(this).parents('tr').data('taskIncID');
        var rowincID = $(this).parents('tr').data('icid');

        $('#addTaskContractorModal').data('icid', rowincID);
        $('#addTaskContractorModal').data('taskIncID', rowincID);

        if (!$.isEmptyObject(taskContractorsData[taskIncID])) {
            $('#addTaskContractorModal').data('id', $(this).parents('tr').data('id'));
        }

        if (!$.isEmptyObject(taskContractorsData[taskIncID])) {
            taskContractors = taskContractorsData[getCurrentTaskData($thisRow)['taskIncID']];
            $.each(taskContractors, function(index, value) {
                var newTrID = 'tr_' + value.contractorID;
                var clonedRow = $($('#taskContractorPreSample').clone()).attr('id', newTrID).addClass('addedTaskContractors');
                $("input[name='taskContractorCode']", clonedRow).val(value.taskContractorName);
                clonedRow.removeClass('hidden');
                clonedRow.insertBefore('#taskContractorPreSample');
                clearAddedNewTaskContractorRow();
            });

        }
        $('#addTaskContractorModal').modal('show');
    });


    function clearTaskContractorCode() {
        $('#taskContactor')
                .val('')
                .selectpicker('refresh')
                .trigger('change');
    }

    $('#taskContactor').on('change', function(e) {
        var $thisRow = $(this).parents('tr');
        if (typeof taskContractors[$(this).val()] != 'undefined' && $(this).val() != '') {
            p_notification(false, eb.getMessage("ERR_JOB_CONINSERT"));
            $('#taskContactor').val('');
            clearTaskContractorCode();
            return false;
        } else if ($(this).val() > 0 && selectedTaskContractorID != $('#taskContactor').val()) {
            selectedTaskContractorID = $(this).val();
            $('#taskContactor').data('taskConName', $(this).find("option:selected").text());
            $('#taskContactor').data('id', selectedTaskContractorID);
        }
    });


    $('#add_task_contractor').on('click', function(e) {
        e.preventDefault();
        if ($(this).is(':disabled')) {
            return false;
        } else {
            $(this).attr('disabled', true);
            var $thisRow = $(this).parents('tr');
            if ($('#taskContactor').val() == '' || $('#taskContactor').val() == null) {
                p_notification(false, eb.getMessage('ERR_JOB_CONINSERT'));
                $(this).attr('disabled', false);
            } else {
                if (taskContractors[selectedTaskContractorID]) {
                    p_notification(false, eb.getMessage('ERR_JOB_CONINSERT'));
                    $('#taskContactor').val('');
                    $('#taskContactor').selectpicker('render');
                } else {
                    addNewTaskContractorRow();
                }
                $(this).attr('disabled', false);
            }
        }
    });

    function addNewTaskContractorRow() {

        var taskID = $('#addTaskContractorModal').data('id');
        var incid = $('#addTaskContractorModal').data('icid');
        var taskIncID = $('#addTaskContractorModal').data('taskIncID');
        var $thisParentRow = getAddRow(taskIncID);
        var thisVals = getCurrentTaskData($thisParentRow);

        var taskContractorName = $('#taskContactor option:selected').html();
        var contractorID = $('#taskContactor').val();

        var newTrID = 'tr_' + contractorID;
        var clonedRow = $($('#taskContractorPreSample').clone()).attr('id', newTrID).addClass('addedTaskContractors');
        $("input[name='taskContractorCode']", clonedRow).val(taskContractorName);
        clonedRow.removeClass('hidden');
        clonedRow.insertBefore('#taskContractorPreSample');
        taskContractors[contractorID] = new taskContractorFunc(contractorID,taskContractorName);
        clearAddedNewTaskContractorRow();
        $('[data-id=taskContactor]').trigger('click');
    }

    function taskContractorFunc(contractorID, taskContractorName) {
        this.contractorID = contractorID;
        this.taskContractorName = taskContractorName;
    }

    function clearAddedNewTaskContractorRow() {
        $('#taskContactor').val('');
        clearTaskContractorCode();
    }

    function clearTaskContractorModal()
    {
        $("#addTaskContractorModal #taskContractorTable tbody .addedTaskContractors").remove();
        taskContractors = {};
    }


    $('#addTaskContractor').on('click', function(e) {
        e.preventDefault();

        if ($('#taskContactor').val() != 0 && $('#taskContactor').val() != null) {
            p_notification(false, eb.getMessage('ERR_ADD_SELECT_TASK_CONTRACTOR'));
            return;
        }

        // validate products before closing modal
        if (!taskContractorModalValidate(e)) {
            e.stopPropagation();
            return false;
        } else {
            $('#addTaskContractorModal').modal('hide');
        }
    });

    function taskContractorModalValidate(e) {

        var taskID = $('#addTaskContractorModal').data('id');
        var incid = $('#addTaskContractorModal').data('icid');
        var taskIncID = $('#addTaskContractorModal').data('taskIncID');
        $('#addTaskContractorModal').data('id', '');
        var $thisParentRow = getAddRow(taskIncID);
        var thisVals = getCurrentTaskData($thisParentRow);

        if ($.isEmptyObject(taskContractors)) {
            taskContractorsData = {};
            return true;
        } else {
            taskContractorsData[thisVals.taskIncID] = taskContractors;
            return true;
        }
    }


    $("#addTaskContractorModal #taskContractorTable").on('click', '.deleteTaskContractor', function(e) {
        e.preventDefault();
        var deleteTaskContractorID = $(this).closest('tr').attr('id');
        var deleteTskConID = deleteTaskContractorID.split('tr_')[1].trim();

        bootbox.confirm('Are you sure you want to remove this contractor ?', function(result) {
            if (result == true) {
                delete taskContractors[deleteTskConID];
                $('#' + deleteTaskContractorID).remove();
            }
        });
    });

    $('#saveAndAddJob').on('click', function(e) {
        e.preventDefault();
        redirectJobForProject = true;
    });

    $('#saveJob, #saveAndAddJob').on('click', function(event) {
        event.preventDefault();

        if (!$.isEmptyObject(taskItems)) {
            p_notification(false, eb.getMessage("ERR_SOME_TASK_PRODUCTS_TO_ADD"));     
            return false;     
        }

        var formData = {
            customer: $('#customer').val(),
            projectID: $('#projectCode').val(),
            jobType: $('#jobTypeIds').val(),
            jobCode: $('#jobCode').val(),
            jobName: $('#jobName').val(),
            projectID: projectID, 
            contractor: $('#contactorIds').val(),
            dueDate: $('#dueDate').val(),
            jobValue: $('#jobValue').val().replace(/,/g, ''),
            jobEstimatedCost: $('#jobEstCost').val().replace(/,/g, ''),
            jobEstimatedEndDate: $('#expDate').val(),
            jobSupervisors: jobSupervisorsData,
            jobManagers: jobManagersData,
            jobTasks: tasks,
            jobResources: jobTaskProducts,
            jobEditflag: jobEditflag,
            additionalDetail1: $('#additionalDetail1').val(),
            additionalDetail2: $('#additionalDetail2').val(),
        };

        if (validateFormData(formData)) {
            if (jobEditflag) {
                formData.editedJobID = efectedJobID;
            }

            var url = (jobEditflag == true) ? BASE_URL + '/job-api/updateJob' : BASE_URL + '/job-api/saveJob';
            eb.ajax({
                url: url,
                method: 'post',
                data: formData,
                dataType: 'json',
                success: function(data) {
                    p_notification(data.status, data.msg);
                    if (data.status) {
                        if (redirectJobForProject) {
                            if (projectID != null) {
                                var url = BASE_URL + '/jobs/' + projectID;
                            } else {
                                var url = BASE_URL + '/jobs';
                            }
                            window.location.assign(url);
                        } else {
                            window.setTimeout(function() {
                                window.location.assign(BASE_URL + "/jobs/list")
                            }, 1000);
                        }
                    }
                }
            });
        }
    });

    function validateFormData(formData)
    {
        if (formData.customer == "" || formData.customer == 0) {
            p_notification(false, eb.getMessage('ERR_JOB_CUSTOMER'));
            $('#customer').focus();
            return false;
        } else if (formData.jobType == '') {
            p_notification(false, eb.getMessage('ERR_JOB_JOBTYPE'));
            $('#jobTypeIds').focus();
            return false;
        } else if (formData.jobCode == '') {
            p_notification(false, eb.getMessage('ERR_TSKCRD_NAME_CNT_BE_NULL'));
            $('#jobCode').focus();
            return false;
        } else if (formData.jobName == '') {
            p_notification(false, eb.getMessage('ERR_JOB_JOB_NAME'));
            $('#jobName').focus();
            return false;
        }  

        if ((formData.dueDate != "" && formData.jobEstimatedEndDate != "") && (formData.dueDate < formData.jobEstimatedEndDate)) {
            p_notification(false, eb.getMessage('ERR_JOB_DUEDATE'));
            $('#expDate').focus();
            return false;
        } 
        return true;
    }

    function loadDataToJob(jobID) 
    {
        efectedJobID = null;
        eb.ajax({
            type: 'POST',
            url: BASE_URL + '/job-api/get-data-for-update',
            data: {jobID: jobID},
            success: function(respond) {
                jobEditflag = true;
                var jobData = respond.data;
                efectedJobCode = jobData.jobCode;
                efectedJobID = jobID;
                projectID = jobData.projectId;
                var existingJobResources = jobData.jobProducts;
                var existingJobTasks = jobData.jobTasks;
                var existingJobManagers = jobData.jobManagers;
                var existingJobSupervisors = jobData.jobSupervisors;

                $("#customer").empty().
                        append($("<option></option>").
                                attr("value", jobData.jobCustomerID).
                                text(jobData.jobCustomer));
                $('#customer').val(jobData.jobCustomerID);
                $('#customer').prop('disabled', true);
                $('#customer').selectpicker('render');
                if (!$.isEmptyObject(existingJobSupervisors)) {
                    jobSupervisorsData = existingJobSupervisors;
                }

                if (!$.isEmptyObject(existingJobManagers)) {
                    jobManagersData = existingJobManagers;
                }

                if (jobData.jobAdditionalDetail1 != null || jobData.jobAdditionalDetail2 != null) {
                    $('#show_addi').trigger('click');
                    $('#additional_div').removeClass('hidden');
                    $('#additionalDetail1').val(jobData.jobAdditionalDetail1);
                    $('#additionalDetail2').val(jobData.jobAdditionalDetail2);
                }


                $('#addTasksModal #taskCardDiv').addClass('hidden');
                $.each(existingJobTasks, function(index, value) {
                    loadTasksInEditMode(index,value, existingJobResources);
                });

                jobTasks[jobData.jobCode] = tasks;
                if (!$.isEmptyObject(taskItems)) {
                    $.each(taskItems, function(index, value) {
                        var goodIndex = index.split('_')[0] + '_' + index.split('_')[1] +'_' +index.split('_')[2];
                        if (value.jobProductMaterialTypeID == "1") {
                            loadTradingGoodsInEditMode(value, goodIndex);
                        } 

                        if (value.jobProductMaterialTypeID == "2" && JOBSETTINGS['fixedAssets'] == 1) {
                            loadFixedAssetsInEditMode(value,goodIndex);                       
                        }  

                        if (value.jobProductMaterialTypeID == "3" && JOBSETTINGS['rawMaterials'] == 1) {
                            loadRawMaterialsInEditMode(value,goodIndex);
                        }
                    });
                }

                taskItems = {};

                $.each(existingJobResources, function(jobProKey, jobProData) {
                    if (jobProData.jobProductMaterialTypeID == 1 && jobProData.jobProductTaskID == null) {
                       loadTradingGoodsInEditMode(jobProData, null);
                    } else if (jobProData.jobProductMaterialTypeID == 2 && jobProData.jobProductTaskID == null && JOBSETTINGS['fixedAssets'] == 1) {
                        loadFixedAssetsInEditMode(jobProData, null);
                    } else if (jobProData.jobProductMaterialTypeID == 3 && jobProData.jobProductTaskID == null && JOBSETTINGS['rawMaterials'] == 1) {
                        loadRawMaterialsInEditMode(jobProData, null);
                    }
                    
                });
                jobProductModalValidate();
                var $currentRow = getAddRow();
                setTaskTypeahead();
            },
            async: false
        });
    }

    function loadTasksInEditMode(index, value, existingJobResources) 
    {
        var $currentRow = getAddRow();
        $currentRow.data('taskCardID', value.taskCardID);
        $currentRow.data('id', value.taskID);
        $currentRow.data('icid',rowincrementID);
        var rowIncID = rowincrementID;
        $currentRow.data('taskIncID',rowincrementID);
        rowincrementID++;
        $currentRow.attr('id', 'task' + value.taskID);
        $("#taskCode", $currentRow).empty();
        $("#taskCode", $currentRow).
                append($("<option></option>").
                        attr("value", value.taskID).
                        text(value.taskName + '-' + value.taskCode));
        $("#taskCode", $currentRow).val(value.taskID).prop('disabled', 'disabled');
        $("#taskCode", $currentRow).data('taskIID', value.taskID);
        $("#taskCode", $currentRow).data('taskCardID', value.taskCardID);
        $("#taskCode", $currentRow).data('taskArrayID', index);
        $("#taskCode", $currentRow).selectpicker('refresh');
        
        
        $("#uom", $currentRow).empty();
        $("#uom", $currentRow).
                append($("<option></option>").
                        attr("value", value.taskUomID).
                        text(value.taskUomAbbr));
        $("#uom", $currentRow).val(value.taskUomID).prop('disabled', 'disabled');
        $("#uom", $currentRow).data('uomIID', value.taskUomID);
        $("#uom", $currentRow).selectpicker('refresh');
        $("#dRate",$currentRow).val(parseFloat(value.jobTaskUnitRate));
        $("#dCost",$currentRow).val(parseFloat(value.jobTaskUnitCost));
        $("#estimatedQty",$currentRow).val(parseFloat(value.jobTaskEstQty));
        $("#estimatedCost",$currentRow).val(parseFloat(value.taskEstimatedCost));
        $("input[name='dCost']", $currentRow).trigger(jQuery.Event("focusout"));

        if (!$.isEmptyObject(value.jobEmployees)) {
            var thisVals = getCurrentTaskData($currentRow);
            $.each(value.jobEmployees, function(ind, val) {
                employees[val.employeeDesignationID] = new taskEmployeesFun(val.employeeDesignationID,val.employeeName);
            });
            taskEmployees[thisVals.taskIncID] = employees;
        }
        
        if (!$.isEmptyObject(value.taskContractors)) {
            var thisVals = getCurrentTaskData($currentRow);
            $.each(value.taskContractors, function(ind, val) {
                taskContractors[val.contactorIdOfJobTask] = new taskContractorFunc(val.contactorIdOfJobTask,val.contractorName);
            });
            taskContractorsData[thisVals.taskIncID] = taskContractors;
        }
        
        $currentRow.data('icid', rowincrementID);
        var $flag = false;
        if ($currentRow.hasClass('add-row')) {
            $flag = true;
        }

        $currentRow
                .removeClass('edit-row')
                .find("#taskCode, input[name='dRate'],input[name='estQty'],input[name='estCost'],#rate,#dCost, #uom, #contactor").prop('readonly', true).end()
                .find("button.delete").removeClass('disabled');
        $currentRow.find("input[name='dRate']").change();
        $currentRow.find("input[name='estQty']").change();
        $currentRow.find("input[name='estCost']").change();
        $currentRow.removeClass('add-row');
        $currentRow.find("#taskCode").prop('disabled', true).selectpicker('render');
        $currentRow.find("#uom").prop('disabled', true).selectpicker('render');
        $currentRow.find("#contactor").prop('disabled', true).selectpicker('render');
        $currentRow.find("#rate").prop('disabled', true).selectpicker('render');
        $currentRow.find(".addEmployee").prop('disabled', true);
        $currentRow.find(".addTaskContractor").prop('disabled', true);
        if ($flag) {
            var $newRow = $($addRowSample.clone()).removeClass('sample hidden').appendTo($('#add-new-task-row', $taskTable));
            setTaskTypeahead();
            $newRow.find("#taskCode").focus();
        }

        if (value.jobTaskStatus == "3") {
            $currentRow.find('.edit').removeClass('hidden');
            $currentRow.find('.add').addClass('hidden');
            $currentRow.find('.save').addClass('hidden');
            $currentRow.find('.delete').removeClass('disabled');
        } else {
            $currentRow.find('.edit').addClass('hidden');
            $currentRow.find('.add').addClass('hidden');
            $currentRow.find('.save').addClass('hidden');
            $currentRow.find('.delete').addClass('hidden');
        }


        var increID = rowincrementID;
        if($currentRow.data('taskIncID')!== undefined){
            increID = $currentRow.data('taskIncID')
        }
        thisVals = getCurrentTaskData($currentRow);
        tasks[thisVals.taskID + '_' + increID] = thisVals;

        if (!$.isEmptyObject(existingJobResources)) {}
        $.each(existingJobResources, function(key, jobPro) {
            if (jobPro.jobProductTaskID == index && thisVals.taskCardID == jobPro.jobProductTaskCardID) {
                taskItems[jobPro.productID+"_"+thisVals.taskID + '_' + increID + '_' + jobPro.jobProductMaterialTypeID] = jobPro;                       
            }    
        });
        rowincrementID++;

    }


    function loadTradingGoodsInEditMode(value, index = null)
    {
        $('.trading-goods-table').removeClass('hidden');
        if (index != null ) {
            var newTrID = 'tr_' + index;
        } else {
            var newTrID = 'tr_' + value.productID;
        }
        var clonedRow = $($('#tradingGoodsPreSetSample').clone()).attr('id', newTrID).addClass('addedTaskProducts');
        $("#tradingGoodsNameAndCode", clonedRow).text(activeProductData[value.productID].pN+'-'+activeProductData[value.productID].pC);
        $("input[name='addedQuantity']", clonedRow).val(value.jobProductAllocatedQty).addUom(activeProductData[value.productID].uom);
        $("input[name='reOrderQuantity']", clonedRow).val(value.jobProductReOrderLevel).addUom(activeProductData[value.productID].uom);
        $("input[name='availableQuantity']", clonedRow).val(activeProductData[value.productID].LPQ).prop('readonly', true).addUom(activeProductData[value.productID].uom);
        $("input[name='unitPrice']", clonedRow).val(value.jobProductUnitPrice).addUomPrice(activeProductData[value.productID].uom);
        $('#estimatedCost', clonedRow).val(value.productEstimatedCost);
        clonedRow.removeClass('hidden');
        clonedRow.insertBefore('#tradingGoodsPreSetSample');
        $('#temporaryProductID').attr('disabled', true);

        if (index != null) {
            var productTaskID = index.split('_')[1];
            tradingGoodProducts[index] = new jobMaterial(value.productID, activeProductData[value.productID].pN+'-'+activeProductData[value.productID].pC,value.jobProductAllocatedQty, value.jobProductUnitPrice, value.productEstimatedCost, value.jobProductReOrderLevel, productTaskID ,activeProductData[value.productID].lPID, value.uomID, value.jobProductID);
        } else {
            tradingGoodProducts[value.productID] = new jobMaterial(value.productID, activeProductData[value.productID].pN+'-'+activeProductData[value.productID].pC,value.jobProductAllocatedQty, value.jobProductUnitPrice, value.productEstimatedCost, value.jobProductReOrderLevel, null ,activeProductData[value.productID].lPID, value.uomID, value.jobProductID);
        }
        $("#temporaryProductID").attr('disabled', false);
        $("input.uomPrice", clonedRow).attr('disabled', true);
        $("input.uomqty", clonedRow).attr('disabled', true);

        if (value.jobProductStatus == null) {
            $(".trading-goods-plus", clonedRow).addClass('hidden');
            $(".trading-goods-edit", clonedRow).removeClass('hidden');
        } else {
            $(".trading-goods-plus", clonedRow).addClass('hidden');
            $(".trading-goods-delete", clonedRow).addClass('hidden');
            $(".trading-goods-edit", clonedRow).addClass('hidden');
        }
    }

    function loadFixedAssetsInEditMode(value, index = null)
    {
        $('.fixed-assests-product-table').removeClass('hidden');
        if (index != null) {
            var newTrID = 'tr_' + index;
        } else {
            var newTrID = 'tr_' + value.productID;
        }
        var clonedRow = $($('#fixedProductPreSetSample').clone()).attr('id', newTrID).addClass('addedTaskProducts');
        $("#fixedProdctNameAndCode", clonedRow).text(activeProductData[value.productID].pN+'-'+activeProductData[value.productID].pC);
        $("input[name='addedQuantity']", clonedRow).val(value.jobProductAllocatedQty).addUom(activeProductData[value.productID].uom);
        $("input[name='reOrderQuantity']", clonedRow).val(value.jobProductReOrderLevel).addUom(activeProductData[value.productID].uom);
        $("input[name='availableQuantity']", clonedRow).val(activeProductData[value.productID].LPQ).prop('readonly', true).addUom(activeProductData[value.productID].uom);
        $("input[name='unitPrice']", clonedRow).val(value.jobProductUnitPrice).addUomPrice(activeProductData[value.productID].uom);
        $('#estimatedCost', clonedRow).val(value.productEstimatedCost);

        clonedRow.removeClass('hidden');
        clonedRow.insertBefore('#fixedProductPreSetSample');
        $('#fixedAssetsID').attr('disabled', true);

        if (index != null) {
            var productTaskID = index.split('_')[1];
            fixedAssetsProducts[index] = new jobMaterial(value.productID, activeProductData[value.productID].pN+'-'+activeProductData[value.productID].pC,value.jobProductAllocatedQty, value.jobProductUnitPrice, value.productEstimatedCost, value.jobProductReOrderLevel, productTaskID ,activeProductData[value.productID].lPID, value.uomID, value.jobProductID);
        } else {
            fixedAssetsProducts[value.productID] = new jobMaterial(value.productID, activeProductData[value.productID].pN+'-'+activeProductData[value.productID].pC,value.jobProductAllocatedQty, value.jobProductUnitPrice, value.productEstimatedCost, value.jobProductReOrderLevel, null ,activeProductData[value.productID].lPID, value.uomID , value.jobProductID);
        }
        $("#fixedAssetsID").attr('disabled', false);
        $("input.uomPrice", clonedRow).attr('disabled', true);
        $("input.uomqty", clonedRow).attr('disabled', true);
        if (value.jobProductStatus == null) {
            $(".fixed-assests-product-plus", clonedRow).addClass('hidden'); 
            $(".fixed-assests-product-edit", clonedRow).removeClass('hidden'); 
        } else {
            $(".fixed-assests-product-plus", clonedRow).addClass('hidden'); 
            $(".fixed-assests-product-delete", clonedRow).addClass('hidden');
            $(".fixed-assests-product-edit", clonedRow).addClass('hidden'); 
        }
    }

    function loadRawMaterialsInEditMode(value, index = null)
    {
        $('.raw-product-table').removeClass('hidden');
        if (index != null) {
            var newTrID = 'tr_' + index;
        } else {
            var newTrID = 'tr_' + value.productID;
        }
        var clonedRow = $($('#rawProductPreSetSample').clone()).attr('id', newTrID).addClass('addedTaskProducts');
        $("#rawProdctNameAndCode", clonedRow).text(activeProductData[value.productID].pN+'-'+activeProductData[value.productID].pC);
        $("input[name='addedQuantity']", clonedRow).val(value.jobProductAllocatedQty).addUom(activeProductData[value.productID].uom);
        $("input[name='reOrderQuantity']", clonedRow).val(value.jobProductReOrderLevel).addUom(activeProductData[value.productID].uom);
        $("input[name='availableQuantity']", clonedRow).val(activeProductData[value.productID].LPQ).prop('readonly', true).addUom(activeProductData[value.productID].uom);
        $("input[name='unitPrice']", clonedRow).val(value.jobProductUnitPrice).addUomPrice(activeProductData[value.productID].uom);
        $('#estimatedCost', clonedRow).val(value.productEstimatedCost);

        clonedRow.removeClass('hidden');
        clonedRow.insertBefore('#rawProductPreSetSample');
        $('#rawMaterialID').attr('disabled', true);

        if (index != null) {
            var productTaskID = index.split('_')[1];
            rawMaterialProducts[index] = new jobMaterial(value.productID, activeProductData[value.productID].pN+'-'+activeProductData[value.productID].pC,value.jobProductAllocatedQty, value.jobProductUnitPrice, value.productEstimatedCost, value.jobProductReOrderLevel, productTaskID ,activeProductData[value.productID].lPID, value.uomID , value.jobProductID);
        } else {
            rawMaterialProducts[value.productID] = new jobMaterial(value.productID, activeProductData[value.productID].pN+'-'+activeProductData[value.productID].pC,value.jobProductAllocatedQty, value.jobProductUnitPrice, value.productEstimatedCost, value.jobProductReOrderLevel, null ,activeProductData[value.productID].lPID, value.uomID , value.jobProductID);            
        }
        $("#rawMaterialID").attr('disabled', false);
        $("input.uomPrice", clonedRow).attr('disabled', true);
        $("input.uomqty", clonedRow).attr('disabled', true);

        if (value.jobProductStatus == null) {
            $(".raw-product-plus", clonedRow).addClass('hidden');
            $(".raw-product-edit", clonedRow).removeClass('hidden');
        } else {
            $(".raw-product-plus", clonedRow).addClass('hidden');
            $(".raw-product-delete", clonedRow).addClass('hidden');
            $(".raw-product-edit", clonedRow).addClass('hidden');
        }
    }
});
