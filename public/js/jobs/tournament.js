
var selectedProductID;
var locationProducts = [];
var AddCustomerFlag = false;
var addCustomer = false;
var goJob = false;
var selectedParticipantID = null;
var selectedParticipantTypeID = null;
var selectedParticipantSubTypeID = null;
var activeParticipants = {};
var activeParticipantTypes = {};
var activeParticipantSubTypes = {};
var participants = {};
var tournamentOfficials = {};
$(document).ready(function() {
    $('#showProjectJobs').css( 'cursor', 'pointer' );    
    var items = {};
    var projectId   = null;
    var entityID   = null;
    var projectName = null;
    var projectAddress = null;
    var projectCode = null;
    var projectStatus = 3;
    var projectTypeID = null;
    var NoOfJobs = null;
    var startDate = null;
    var tournamentEstimatedCost = null;
    var projectDescription = null;
    var projectEstimatedCost = null;
    var projectManagersList = [];
    var projectSupervisorsList = [];
    var projectMaterialsList = {};
    var customerID = null;
    var teamID = null;
    var projectManagerID = null;
    var projectManagers = {};
    var projectSupervisorID = null;
    var projectSupervisors = {};
    var items = {};
    var locationProductID = null;
    var endDate = null;
    var projectMaterialsDataList = [];
    var deletedJobList = [];
    var nowTemp = new Date();
    var itemID = null;
    var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);
    var nowTemp = new Date();
    var now1 = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);
    var locationID = $('#navBarSelectedLocation').attr('data-locationid');
    var showJobList = true;
    
    var checkin1 = $('#startDate').datepicker({onRender: function(date) {
        return date.valueOf() < now1.valueOf() ? ((U_DATE_R_OVERRIDE) ? '' : 'disabled') : '';
    },
    }).on('changeDate', function(ev) {
        checkin1.hide();
    }).data('datepicker');
    

    var checkin2 = $('#endDate').datepicker({onRender: function(date) {
        return date.valueOf() < now1.valueOf() ? ((U_DATE_R_OVERRIDE) ? '' : 'disabled') : '';
    },
    }).on('changeDate', function(ev) {
        checkin2.hide();
    }).data('datepicker');


    $.ajax({
        type: 'POST',
        url: BASE_URL + '/participant-api/getParticipants',
        data: {},
        success: function(respond) {
            activeParticipants = respond.data.participantData;       
            activeParticipantTypes = respond.data.participantTypeData;       
            activeParticipantSubTypes = respond.data.participantSubTypeData;       
        },
        async: false
    });

    $('#partiType').on('change', function(event) {
        event.preventDefault();
        if ($('#partiType').val() != null || $('#partiType').val() != 0) {
            eb.ajax({
                type: 'POST',
                url: BASE_URL + '/participant-api/getDepartmentWiseEmployees',
                data: {
                    participantTypeID : $('#partiType').val(),
                    participantSubTypeID : $('#partiSubType').val()
                },
                success: function(respond) {
                    if ($('#partiType').val() != 0 && $('#partiSubType').val() != 0) {
                        $("#teamEmployee").empty();
                        $("#teamEmployee").
                                    append($("<option></option>").
                                            attr("value", 0).
                                            text('Select Participants'));
                        $.each(respond.data, function(index, val) {
                            $("#teamEmployee").
                                    append($("<option></option>").
                                            attr("value", index).
                                            text(val));
                            $('#teamEmployee').selectpicker('refresh');
                        });     
                    } else {
                        $("#teamEmployee").empty();
                        $("#teamEmployee").
                                    append($("<option></option>").
                                            attr("value", 0).
                                            text('Select Participants'));
                        
                    } 
                }
            });
            if ($('#partiType').val() != 0 && $('#partiSubType').val() != 0) {
                $('#teamEmployee').attr('disabled',false);
                $('.selectpicker').selectpicker('refresh');
            } else {
                $('#teamEmployee').attr('disabled',true);
                $('.selectpicker').selectpicker('refresh');
            }
        }
    });

    $('#partiSubType').on('change', function(event) {
        event.preventDefault();
        if ($('#partiSubType').val() != null || $('#partiSubType').val() != 0) {
            eb.ajax({
                type: 'POST',
                url: BASE_URL + '/participant-api/getDepartmentWiseEmployees',
                data: {
                    participantTypeID : $('#partiType').val(),
                    participantSubTypeID : $('#partiSubType').val()
                },
                success: function(respond) {
                    if ($('#partiType').val() != 0 && $('#partiSubType').val() != 0) {
                        $("#teamEmployee").empty();
                        $("#teamEmployee").
                                    append($("<option></option>").
                                            attr("value", 0).
                                            text('Select Participants'));
                        $.each(respond.data, function(index, val) {
                            $("#teamEmployee").
                                    append($("<option></option>").
                                            attr("value", index).
                                            text(val));
                            $('#teamEmployee').selectpicker('refresh');
                        });
                    } else {
                        $("#teamEmployee").empty();
                        $("#teamEmployee").
                                    append($("<option></option>").
                                            attr("value", 0).
                                            text('Select Participants'));
                        
                    }        
                }
            });
            if ($('#partiType').val() != 0 && $('#partiSubType').val() != 0) {
                $('#teamEmployee').attr('disabled',false);
                $('.selectpicker').selectpicker('refresh');
            } else {
                $('#teamEmployee').attr('disabled',true);
                $('.selectpicker').selectpicker('refresh');
            }
        }
    });


    var participantKey;
    $('#teamEmployee').on('change', function(event) {
        event.preventDefault();
        if ($(this).val() > 0) {
            participantKey = $(this).val();
            selectedParticipantID = $(this).val();
            selectedParticipantTypeID = $('#partiType').val();
            selectedParticipantSubTpeID = $('#partiSubType').val();
            $('#partiType').val('0');
            $('#partiType').selectpicker('refresh');

            $('#partiSubType').val('0');
            $('#partiSubType').selectpicker('refresh');

            $('#teamEmployee').val('0');
            $('#teamEmployee').attr('disabled',true);
            $('#teamEmployee').selectpicker('refresh');

            var duplicateCheck = false;

            $.each(participants, function(index, val) {
                var checkindex = index.split('-')[0]+'-'+index.split('-')[1]+'-'+index.split('-')[2];
                if (checkindex == selectedParticipantTypeID +'-' + selectedParticipantSubTpeID + '-' + selectedParticipantID) {
                    duplicateCheck = true;                        
                }
            });

            if (!duplicateCheck) {
                selectParticipants(selectedParticipantID, selectedParticipantTypeID, selectedParticipantSubTpeID);
            } else {
                p_notification(false, eb.getMessage('ERR_PART_DUPLICATE'));
            }
        }
    });

    function selectParticipants(selectedParticipantID, selectedParticipantTypeID, selectedParticipantSubTpeID, teamID) {
        $('.participant-table').removeClass('hidden');
        setParticipantList(selectedParticipantID, selectedParticipantTypeID, selectedParticipantSubTpeID, teamID);
       
    }

    function setParticipantList(selectedParticipantID, selectedParticipantTypeID, selectedParticipantSubTpeID, teamID) {
        if (teamID != undefined) {
            var newTrID = 'tr_' + selectedParticipantTypeID +'-' + selectedParticipantSubTpeID + '-' + selectedParticipantID + '-' + teamID;
        } else {
            var newTrID = 'tr_' + selectedParticipantTypeID +'-' + selectedParticipantSubTpeID + '-' + selectedParticipantID;
        }
        var clonedRow = $($('#participantPreSetSample').clone()).attr('id', newTrID).addClass('addedParticipant');
        $("#participantTypeName", clonedRow).text(activeParticipantTypes[selectedParticipantTypeID]);
        $("#participantSubTypeName", clonedRow).text(activeParticipantSubTypes[selectedParticipantSubTpeID]);
        $("#participantName", clonedRow).text(activeParticipants[selectedParticipantID]);
        clonedRow.removeClass('hidden');
        clonedRow.insertBefore('#participantPreSetSample');

        if (teamID != undefined) { 
            participants[selectedParticipantTypeID +'-' + selectedParticipantSubTpeID + '-' + selectedParticipantID + '-' +teamID] = new participant(selectedParticipantTypeID,selectedParticipantSubTpeID,selectedParticipantID, teamID);
        } else {
            participants[selectedParticipantTypeID +'-' + selectedParticipantSubTpeID + '-' + selectedParticipantID] = new participant(selectedParticipantTypeID,selectedParticipantSubTpeID,selectedParticipantID);
        }
    }

    function participant(participantTypeID, participantSubTypeID, participantID, teamID) {
        this.participantTypeID = participantTypeID;
        this.participantSubTypeID = participantSubTypeID;
        this.participantID = participantID;
        this.teamID = teamID;
    }

    $('#addParti').on('click', function(e) {
        e.preventDefault();

        // validate products before closing modal
        if (!participantModalValidate(e)) {
            e.stopPropagation();
            return false;
        } else {
            $('#addParticipantModal').modal('hide');
        }
    });

    function participantModalValidate(e) {
        var tempParticipants = {};
        if (!$.isEmptyObject(participants)) {
            $.each(participants, function(index, value) {
                tempParticipants[index] = value;
            });
        }

        if ($.isEmptyObject(tempParticipants)) {
            tournamentOfficials = {};
            return true;   
        } else {
            tournamentOfficials = tempParticipants;
            return true;
        }
    }



    $('#tournamentOfficial').on('click',function(){
        participants = {};
        $("#addParticipantModal #partTable tbody .addedParticipant").remove();
        if ($.isEmptyObject(tournamentOfficials)) {
            $('.participant-table').addClass('hidden');
        } else {
            $.each(tournamentOfficials, function(index, val) {
                participants[index] = val;
                setParticipantList(val.participantID, val.participantTypeID, val.participantSubTypeID, val.teamID);
            });
            $('.participant-table').removeClass('hidden');
        }
        $("#addParticipantModal").modal('toggle');
    });

    $(document).on('click', '.participant-delete', function() {
        var deleteTrID = $(this).closest('tr').attr('id');
        var deletePartID = deleteTrID.split('tr_')[1].trim();
        delete participants[deletePartID];
        $('#' + deleteTrID).remove();

        if (_.values(participants).length == 0) {
            $('.participant-table').addClass('hidden');
        }
    });

    //load customers for drop down
    loadDropDownFromDatabase('/tournament-team-api/search-teams-for-dropdown', "", '', '#teamID');
    $('#teamID').selectpicker('refresh');
    $('#teamID').on('change', function() {
        teamID = $('#teamID').val();
        if (teamID != 0 || teamID != null) {
            participants = {};
            $("#addParticipantModal #partTable tbody .addedParticipant").remove();
            getTeamParticipants(teamID);            
        }
    });


    function getTeamParticipants(id) {
        var employeeIds = [];            
        $.ajax({
            type: 'GET',
            data: {
                teamID: id
            },
            url: BASE_URL + '/tournament-team-api/getTeamPartcipants',
            success: function(respond) {
                $.each(respond.data, function(index, val) {
                    selectParticipants(val.employeeID,val.participantTypeID,val.participantSubTypeID, val.teamID);
                });
            }
        });
    }

    // check whether its create mode edit mode or view mode
    if ($('#mode').val() == 'edit' || $('#mode').val() == 'view') {

        ($('#mode').val() == 'edit') ? projectUpdateView() : projectViewMode();
        var proID = $('#mode').data('projectid');

        eb.ajax({
            type: 'GET',
            url: BASE_URL + '/api/tournament/getEditDetails/'+proID,
            success: function(respond) {
                if(respond.status){
                    $('#projectCode').val(respond.data['projectCode']);
                    $('#projectName').val(respond.data['projectName']);
                    $('#customer').append($("<option value='" + respond.data['customerID'] + "'>" + respond.data['customerName']+'-'+respond.data['customerCode']+ "</option>"));
                    $('#customer').attr("disabled", true);
                    $('#projectAddress').val(respond.data['projectAddress']);
                    $('#noOfJobs').val(respond.data['noOfJobs']);
                    $('#startDate').val(respond.data['projectStartingAt'].split(" ", 1));
                    $('#projectValue').val(accounting.formatMoney(respond.data['projectEstimatedCost']));
                    $('#description').val(respond.data['projectDescription']);
                    $('#projectEstimatedCost').val(accounting.formatMoney(respond.data['eventEstimatedCost']));
                    $('select[name=proTypes]').val(respond.data['projectType']);
                    $('select[name=customer]').val(respond.data['customerID']);
                    $('#endDate').val(respond.data['estimatedEndDate'].split(" ", 1));
                    $('#addCustomerBtn').removeAttr('href');
                    $('.selectpicker').selectpicker('refresh');
                    entityID = respond.data['entityID'];
                    projectStatus = respond.data['projectStatus'];

                    $.each(respond.data['projectManagersList'], function(index, val) {
                         tournamentOfficials[index] = new participant(val.participantTypeID, val.participantSubTypeID, val.participantID, val.teamID);
                    });

                    //set Selected Project Supervisors
                    for (var key in respond.data['projectSupervisorsList']) {
                        if (respond.data['projectSupervisorsList'].hasOwnProperty(key)) {
                            projectSupervisorID = key;
                            addNewProjectSupervisorRow(respond.data['projectSupervisorsList'][key]);
                        }
                    }
                    if (respond.data['projectMaterialList'].length > 0) {
                        for (var i = 0; i < respond.data['projectMaterialList'].length; i++) {
                            itemID = respond.data['projectMaterialList'][i]['productID'];
                            addNewItemRow(respond.data['projectMaterialList'][i]);
                        }
                    } else {
                        $('#addProjectMaterialsModal, .not_found').removeClass('hidden');
                    }

                    projectManagers = respond.data['projectManagersList'];
                    projectSupervisors = respond.data['projectSupervisorsList'];
                    addProjectManagers();
                    addProjectSupervisors();

                    if ($('#mode').val() == 'view') {
                        $('input[type=text]').attr('disabled', true);
                        $('input[type=number]').attr('disabled', true);
                        $('textarea').attr('disabled', true);
                        $('#projectTypes').attr("disabled", true);
                        $('#startDate').attr("disabled", true);
                        $('#add_project_manager').attr("disabled", true);
                        $('#add_project_supervisor').attr("disabled", true);
                        $('.deleteProjectJobs').attr("disabled", true);
                        $('#addProjectManagerModal, #addPmBody').addClass('hidden');
                        $('#addProjectManagerModal, #viewPmBody').removeClass('hidden');
                        $('#btnAddProjectManager').addClass('hidden');
                        $('#btnPmModalClose').addClass('hidden');
                        $('#btnViewPmClose').removeClass('hidden');
                        $('#addProjectSupervisorModal, #addSupvBody').addClass('hidden');
                        $('#addProjectSupervisorModal, #viewSupvBody').removeClass('hidden');
                        $('#btnAddProjectSupervisor').addClass('hidden');
                        $('#btnSupvModalClose').addClass('hidden');
                        $('#btnViewSupvModalClose').removeClass('hidden');
                        $('.deleteProjectJobs').addClass('hidden');
                        $('.undoDeleteProjectJobs').addClass('hidden');
                    }                                
                }                
            }
        });
    }

    $("#customer_more").hide();

    $('#addCustomerModal').on('shown.bs.modal', function() {
        $(this).find("input[name='customerName']").focus();
    });

    //customer more details button
    $("#moredetails").click(function() {
        $("#customer_more").slideDown();
        $('#moredetails').hide();
        $('#customerCurrentBalance', '#addcustomerform').attr('disabled', true);
        $('#customerCurrentCredit', '#addcustomerform').attr('disabled', true);
    });

    //project more details view
    $('#viewMoreDetails').on('click',function(){
        $('#viewMoreDetails').addClass('hidden');
        $('#hideMoreDetails').removeClass('hidden');
        $('#attachFileDiv').removeClass('hidden');
        $('#estimateEndDateDiv').removeClass('hidden');
    });

    // project hide more details
    $('#hideMoreDetails').on('click',function(){
        $('#hideMoreDetails').addClass('hidden');
        $('#attachFileDiv').addClass('hidden');
        $('#estimateEndDateDiv').addClass('hidden');
        $('#viewMoreDetails').removeClass('hidden');
    });

    //load customers for drop down
    loadDropDownFromDatabase('/customerAPI/search-customers-for-dropdown', "", '', '#customer');
    $('#customer').selectpicker('refresh');
    $('#customer').on('change', function() {
        customerID = $('#customer').val();
    });

    //load project types for drop down
    if ($('#mode').val() == 'create') {
        loadDropDownFromDatabase('/project_setup_api/search-project-types-for-dropdown', "", '', '#projectTypes');
        $('#projectTypes').selectpicker('refresh');
        $('#projectTypes').on('change', function() {
            projectTypeID = $('#projectTypes').val();
        });
    }
    


    //add new project manager
    $('#add_project_manager').on('click', function(e) {
        e.preventDefault();

        //if plus button is clicked then disabled the button
        //if button is clicked more than one, then it does not continue

        if ($(this).is(':disabled')) {
            return false;
        } else {
            $(this).attr('disabled', true);
            var $thisRow = $(this).parents('tr');
            if ($('#projectMgr').val() == 0 || $('#projectMgr').val() == null) {
                $('#addNewProjectManagerRow').val('');
                p_notification(false, eb.getMessage('ERR_REQUIRED_PM'));
                $(this).attr('disabled', false);
            } else {
                projectManagerID = $('#projectMgr').val();
                addNewProjectManagerRow($('#projectMgr option:selected').html());
                $(this).attr('disabled', false);
                
            }
        }
    });

    $('#projectMgr').on('change', function () {
        if ($(this).val() != 0 && $(this).val() != '' && $(this).val() != null) {
            if ($(this).val() in projectManagers) {
                p_notification(false, eb.getMessage('ERR_SELECTED_PM_DUPLICATE'));
                $(this).val('0');
                return false;
            }
        }
    });

    $('#projectSupv').on('change', function () {
        if ($(this).val() != 0 && $(this).val() != '' && $(this).val() != null) {
            if ($(this).val() in projectSupervisors) {
                p_notification(false, eb.getMessage('ERR_SELECTED_SUPV_DUPLICATE'));
                $(this).val('0');
                return false;
            }
        }
    });

    function addNewProjectManagerRow(projectMgrCodeAndName) {

        if ($('#mode').val() != 'view') {
            if (projectManagerID in projectManagers) {
                p_notification(false, eb.getMessage('ERR_SELECTED_PM_DUPLICATE'));
                return;
            }
            var newTrID = 'tr_'+projectManagerID;
            var clonedRow = $($('#preSample').clone()).attr('id', newTrID).addClass('addedProjectManager');
            $("input[name='projectManagerNameCode']", clonedRow).val(projectMgrCodeAndName);
            clonedRow.removeClass('hidden');
            clonedRow.insertBefore('#preSample');
            projectManagers[projectManagerID] = projectMgrCodeAndName;
            clearAddedNewRow();
        } else {
            var newTrID = 'tr_'+projectManagerID;
            var clonedRow = $($('#pmRow').clone()).attr('id', newTrID).addClass('viewedpM');
            $("#pmName", clonedRow).text(projectMgrCodeAndName);
            clonedRow.removeClass('hidden');
            clonedRow.insertBefore('#pmRow');
        }
    }

    function clearAddedNewRow() {
        $('select[name=projectManagersDropdown]').val('');
        $('.selectpicker').selectpicker('refresh');
    }

    $("#addProjectManagerModal").on('click','.deleteProjectManager', function(e) {
        e.preventDefault();

        var deleteProTempMgrID = $(this).closest('tr').attr('id');
        var deleteProMgrID = deleteProTempMgrID.split('tr_')[1].trim();

        bootbox.confirm('Are you sure you want to remove this project manager ?', function(result) {
            if (result == true) {
                delete projectManagers[deleteProMgrID];
                $('#' + deleteProTempMgrID).remove();
            }
        });
    });


    //add new project supervisor
    $('#add_project_supervisor').on('click', function(e) {
        e.preventDefault();

        //if plus button is clicked then disabled the button
        //if button is clicked more than one, then it does not continue

        if ($(this).is(':disabled')) {
            return false;
        } else {
            $(this).attr('disabled', true);
            var $thisRow = $(this).parents('tr');
            if ($('#projectSupv').val() == 0 || $('#projectSupv').val() == null) {
                $('#addNewProjectSupervisorRow').val('');
                p_notification(false, eb.getMessage('ERR_REQUIRED_SUPV'));
                $(this).attr('disabled', false);
            } else {
                projectSupervisorID = $('#projectSupv').val();
                addNewProjectSupervisorRow($('#projectSupv option:selected').html());
                $(this).attr('disabled', false);
                
            }
        }
    });

    function addNewProjectSupervisorRow(projectSupvCodeAndName) {

        if ($('#mode').val() != 'view') {
            if (projectSupervisorID in projectSupervisors) {
                p_notification(false, eb.getMessage('ERR_SELECTED_SUPV_DUPLICATE'));
                return;
            }

            var newSupvID = 'tr_'+projectSupervisorID;
            var clonedRow = $($('#supvSample').clone()).attr('id', newSupvID).addClass('addedProjectSupervisor');
            $("input[name='projectSupervisorNameCode']", clonedRow).val(projectSupvCodeAndName);
            clonedRow.removeClass('hidden');
            clonedRow.insertBefore('#supvSample');
            projectSupervisors[projectSupervisorID] = projectSupvCodeAndName;
            clearAddedNewSupvRow();
        } else {
            var newSupvID = 'tr_'+projectSupervisorID;
            var clonedRow = $($('#supvRow').clone()).attr('id', newSupvID).addClass('viewedSupv');
            $("#supvName", clonedRow).text(projectSupvCodeAndName);
            clonedRow.removeClass('hidden');
            clonedRow.insertBefore('#supvRow');
        }
    }

    function clearAddedNewSupvRow() {
        $('select[name=projectSupervisorsDropdown]').val('');
        $('.selectpicker').selectpicker('refresh');
    }

    $("#addProjectSupervisorModal").on('click','.deleteProjectSupervisor', function(e) {
        e.preventDefault();

        var deleteProTempSupvID = $(this).closest('tr').attr('id');
        var deleteProSupvID = deleteProTempSupvID.split('tr_')[1].trim();

        bootbox.confirm('Are you sure you want to remove this project supervisor ?', function(result) {
            if (result == true) {
                delete projectSupervisors[deleteProSupvID];
                $('#' + deleteProTempSupvID).remove();
            }
        });

    });

    $('#btnAddProjectManager').on('click',function(){
        if ($('#projectMgr').val() != 0 && $('#projectMgr').val() != null) {
            p_notification(false, eb.getMessage('ERR_ADD_SELECT_PROJ_MGR'));
            return;
        }

        addProjectManagers();
        $("#addProjectManagerModal").modal('toggle');

    });

    $('#projectManager').on('click',function(){
        reomoveUnAddedProjectManagers();

    });

    $('#projectSupervisor').on('click',function(){
        reomoveUnAddedProjectSupervisors();
    });

    $('#btnPmModalClose').on('click',function(){    
        reomoveUnAddedProjectManagers();
    });


    //project managers footer add button
    function addProjectManagers()
    {
        projectManagersList = [];
        for (var key in projectManagers) {
            if (projectManagers.hasOwnProperty(key)) {
                projectManagersList.push(parseInt(key));
            }
        }
        $("#projectManagerIcon").attr('class', 'fa');
        if (projectManagersList.length > 0) {

            var managerText = (projectManagersList.length == 1) ? 'manager' : 'managers';
            var msg = projectManagersList.length+ ' project '+ managerText+ ' selected';
            $('#pmCount').text(msg);
        } else {
            $('#pmCount').text('No project manager selected');
        }

    }

    $('#btnAddProjectSupervisor').on('click',function(){

        if ($('#projectSupv').val() != 0 && $('#projectSupv').val() != null) {
            p_notification(false, eb.getMessage('ERR_ADD_SELECT_PROJ_SUPV'));
            return;
        }

        addProjectSupervisors();
        $("#addProjectSupervisorModal").modal('toggle');

    });

    $('#btnSupvModalClose').on('click',function(){
        reomoveUnAddedProjectSupervisors();
    });


    //project supervisors footer add button
    function addProjectSupervisors()
    {
        projectSupervisorsList = [];
        for (var key in projectSupervisors) {
            if (projectSupervisors.hasOwnProperty(key)) {
                projectSupervisorsList.push(parseInt(key));
            }
        }

        $("#projectSupervisorIcon").attr('class', 'fa');
        if (projectSupervisorsList.length > 0) {
            var managerText = (projectSupervisorsList.length == 1) ? 'supervisor' : 'supervisors';
            var msg = projectSupervisorsList.length+ ' project '+ managerText+ ' selected';
            $('#supvCount').text(msg);
            
        } else {
            $('#supvCount').text('No project supervisor selected');
        }

    }

    function reomoveUnAddedProjectManagers() {
        if ($('#mode').val() != 'view') {
            var temp = [];
            for (var key in projectManagers) {
                if (projectManagers.hasOwnProperty(key)) {
                    temp.push(parseInt(key));
                }
            }
            for (var i = 0; i < temp.length; i++) {    
                if ($.inArray(temp[i], projectManagersList) == -1) {
                    delete projectManagers[temp[i]];
                    $('#' + 'tr_'+temp[i]).remove();
                }           
            }
            addProjectManagers();
        }
        $("#addProjectManagerModal").modal('toggle'); 
    }

    function reomoveUnAddedProjectSupervisors() {
        if ($('#mode').val() != 'view') {
            var temp = [];
            for (var key in projectSupervisors) {
                if (projectSupervisors.hasOwnProperty(key)) {
                    temp.push(parseInt(key));
                }
            }
            for (var i = 0; i < temp.length; i++) {    
                if ($.inArray(temp[i], projectSupervisorsList) == -1) {
                    delete projectSupervisors[temp[i]];
                    $('#' + 'tr_'+temp[i]).remove();
                }           
            }
            addProjectSupervisors();
        }
        $("#addProjectSupervisorModal").modal('toggle'); 
    }
    
    function addNewItemRow(item) {
        
        var newItemID = 'tr_'+itemID;
        var clonedRow = $($('#matRow').clone()).attr('id', newItemID).addClass('viewedItems');
        $("#matName", clonedRow).text(item['productName']);
        $("#matCode", clonedRow).text(item['productCode']);
        $("#matQty", clonedRow).text(item['jobProductAllocatedQty']);
        clonedRow.removeClass('hidden');
        clonedRow.insertBefore('#matRow');
    }

    $('#projectMaterials').on('click',function(){
        $("#addProjectMaterialsModal").modal('toggle');
    });

    $('#saveAndCreateJob').on('click', function(e) {
        e.preventDefault();
        goJob = true;
    });

    //save project
    $('#btnAdd, #saveAndCreateJob').on('click',function(){
        projectCode = $('#projectCode').val();
        projectName = $('#projectName').val();
        projectType = $('#projectTypes').val();
        customerID = $('#customer').val();
        projectAddress = $('#projectAddress').val();
        NoOfJobs= $('#noOfJobs').val();
        startDate = $('#startDate').val();
        tournamentEstimatedCost = accounting.unformat($('#projectValue').val());
        projectDescription = $('#description').val();
        projectEstimatedCost = $('#projectEstimatedCost').val();
        endDate = $('#endDate').val();

        var formData = {
            projectCode: projectCode,
            projectName: projectName,
            projectType: projectType,
            startDate: startDate,
            endDate: endDate
        };

        if (tournamentEstimatedCost != "" && isNaN(parseFloat(tournamentEstimatedCost)))
        {
            p_notification(false, eb.getMessage('ERR_PROJECT_VALUE_NUMERIC'));
            return;
        }

        if (parseFloat(tournamentEstimatedCost) < 0)
        {
            p_notification(false, eb.getMessage('ERR_PROJECT_VALUE_MINUS'));
            return;
        }

        if (NoOfJobs != "" && isNaN(parseFloat(NoOfJobs)))
        {
            p_notification(false, eb.getMessage('ERR_NO_OF_JOBS_NUMERIC'));
            return;
        }

        if(validateFormData(formData)){
           
            eb.ajax({
                type: 'POST',
                url: BASE_URL + '/api/tournament/create',
                data: {
                    projectCode : projectCode,
                    projectName : projectName,
                    projectType : projectType,
                    projectAddress : projectAddress,
                    noOfJobs : NoOfJobs,
                    startDate : startDate, 
                    tournamentEstimatedCost : tournamentEstimatedCost,
                    projectDescription : projectDescription,
                    eventEstimatedCost : projectEstimatedCost,
                    endDate : endDate,
                    tournamentOfficials : tournamentOfficials,
                    teamID : teamID
                },
                success: function(respond) {
                    p_notification(respond.status, respond.msg);                
                    if(respond.status){
                        if(respond.status){
                            if (goJob) {
                                var url = BASE_URL + '/event/' + respond.data;
                                window.location.assign(url);
                            } else {
                                window.location.href = BASE_URL + '/tournament/list';                 
                            }
                        }               
                    }                
                }
            });
        } 
    });

    //for cancel btn
    $('#btnCancel').on('click',function (){
        $('input[type=text]').val('');
        $('input[type=email]').val('');  
        $('select[name=employeeDept]').val('');
        $('select[name=employeeDesig]').val('');
        $('.selectpicker').selectpicker('refresh');      
        employeeRegisterView();        
    });

    //this function for switch to project update view
    function projectUpdateView(){
       $('.addTitle').addClass('hidden');
       $('.updateTitle').removeClass('hidden');
       $('.addDiv').addClass('hidden');
       $('.updateDiv').removeClass('hidden');
    }

    //this function for switch to project update view
    function projectViewMode(){
       $('.addTitle').addClass('hidden');
       $('.viewTitle').removeClass('hidden');
       $('.addDiv').addClass('hidden');
       // $('.viewDiv').removeClass('hidden');
    }

    $('#projectValue').focusout(function(){
        accounting.unformat($('#projectValue').val());
        var formattedVal =  accounting.formatMoney($('#projectValue').val());
        $('#projectValue').val(formattedVal);
    });
        
    
    //update project
    $('#btnUpdate').on('click',function(){

        if (deletedJobList.length > 0) {
            var jobStirng = (deletedJobList.length == 1) ? 'job': 'jobs';
            bootbox.confirm('Number of '+ jobStirng + ' ('+ deletedJobList.length +') will be deleted if you update this project. Are you sure you want to continue?', function(result) {
                updateProject(result);
            });

        } else {
            updateProject(true);
        }
         
    });

    $('#showProjectJobs').on('click', function() {
        if (showJobList == true) {
            $('#jobsTable').removeClass('hidden');
            $('#hideJobLabel').removeClass('hidden');
            $('#showJobLabel').addClass('hidden');
            $('#upIcon').removeClass('hidden');
            $('#downIcon').addClass('hidden');
            showJobList = false;
        }
        else {
            $('#jobsTable').addClass('hidden');
            $('#upIcon').addClass('hidden');
            $('#downIcon').removeClass('hidden');
            $('#hideJobLabel').addClass('hidden');
            $('#showJobLabel').removeClass('hidden');
            showJobList = true;
        }
    });

    $('.deleteProjectJobs').on('click', function(){
        var jobID = $(this).closest('tr').data('job-id');

        if (deletedJobList.indexOf(jobID) == -1) {
            deletedJobList.push(jobID);
        }

        $(this).addClass('disabled');
        $(this).closest('tr').find('.undoDeleteProjectJobs').removeClass('disabled');
    });

     $('.undoDeleteProjectJobs').on('click', function(){
        var jobID = $(this).closest('tr').data('job-id');

        $(this).addClass('disabled');
        $(this).closest('tr').find('.deleteProjectJobs').removeClass('disabled');

        var ElementIndex = deletedJobList.indexOf(jobID);

        if (ElementIndex > -1) {
          deletedJobList.splice(ElementIndex, 1);
        }

     });

    function validateFormData(formData)
    {
        if (formData.projectCode == '') {
            p_notification(false, eb.getMessage('ERR_TOURNAMENT_CODE_CNT_BE_BLANK'));
            $('#jobCode').focus();
            return false;
        } else if (formData.projectName == '') {
            p_notification(false, eb.getMessage('ERR_TOURNAMENT_NAME_CNT_BE_BLANK'));
            return false;
        } else if (formData.projectType == '0' || formData.projectType == '' || formData.projectType == null) {
            p_notification(false, eb.getMessage('ERR_TOURNAMENT_TYPE_CNT_BE_BLANK'));
            $('#jobTypeIds').focus();
            return false;
        } else if (formData.startDate && formData.endDate) {
            if (new Date(formData.startDate) > new Date (formData.endDate)) {
                p_notification(false, eb.getMessage('ERR_ST_DATE_EST_END__TOUR_VALIDATE'));
                return false;
            }
            
        } 
        return true;
    }

    $('#btnBack, #editbtnBack').on('click', function(){
         window.history.go(-1);
    });

    function updateProject(result) {
        if ($('#projectValue').val() != "" && isNaN(parseFloat($('#projectValue').val()))) {
            p_notification(false, eb.getMessage('ERR_PROJECT_VALUE_NUMERIC'));
            return;
        }

        if ($('#noOfJobs').val() != "" && isNaN(parseFloat($('#noOfJobs').val()))) {
            p_notification(false, eb.getMessage('ERR_NO_OF_JOBS_NUMERIC'));
            return;
        }

        projectId = $('#mode').data('projectid');
        entityID = entityID;
        projectCode = $('#projectCode').val();
        projectName = $('#projectName').val();
        projectType = $('#projectTypes').val();
        customerID = $('#customer').val();
        projectAddress = $('#projectAddress').val();
        noOfJobs= $('#noOfJobs').val();
        startDate = $('#startDate').val();
        tournamentEstimatedCost = accounting.unformat($('#projectValue').val());
        projectDescription = $('#description').val();
        projectEstimatedCost =  accounting.unformat($('#projectEstimatedCost').val());
        endDate = $('#endDate').val();

        var formData = {
            projectCode: projectCode,
            projectName: projectName,
            projectType: projectType,
            startDate: startDate,
            endDate: endDate
        };


        if(validateFormData(formData) && result){
            eb.ajax({
                type: 'POST',
                url: BASE_URL + '/api/tournament/update',
                data: {
                    projectId : projectId,
                    projectCode : projectCode,
                    projectName : projectName,
                    projectType : projectType,
                    projectAddress : projectAddress,
                    noOfJobs : noOfJobs,
                    startDate : startDate, 
                    tournamentEstimatedCost : tournamentEstimatedCost,
                    projectDescription : projectDescription,
                    eventEstimatedCost : projectEstimatedCost,
                    entityID: entityID,
                    endDate: endDate,
                    projectStatus: projectStatus,
                    tournamentOfficials : tournamentOfficials
                },
                success: function(respond) {
                    p_notification(respond.status, respond.msg);                
                    if(respond.status){
                       window.location.href = BASE_URL + '/tournament/list';                 
                    }                
                }
            });
        }
    }

});

function getCustomerDetails(custID) {
    eb.ajax({
        type: 'POST', url: BASE_URL + '/invoice-api/getCustomerDetails', data: {customerID: custID}, success: function(respond) {
            if (respond.status == true) {
                $('#customer').
                    append($("<option></option>").
                            attr("value", respond.data.customerID).
                            text(respond.data.customerName + '-' + respond.data.customerCode));
                $('select[name=customer]').val(respond.data['customerID']);
                $('#customer').selectpicker('refresh');
            } 
        }
    });
}


