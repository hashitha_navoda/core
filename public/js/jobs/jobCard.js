var jobCardID = '';
var editMode = false;
var allsubTasks = {};
var jobCardServices = {};
var mainJobCardServices = {};
var serviceID = null;
var editModeService = false;
var productID = null;
$(function() {

    loadDropDownFromDatabase('/task_setup_api/searchTasksForDropdown', "", 1, '#serviceID', '', false);
    $('#serviceID').on('change', function() {
        if ( $(this).val() in jobCardServices && $(this).val() != 0) {
            p_notification(false, eb.getMessage('ERR_SUB_TSK_DUPLICATE'));
            clearServiceDropdown();
            return;
        }
    });

    loadDropDownFromDatabase('/productAPI/searchNonInventoryActiveProductsForDropdown', "", false, '#addItem');
    $('#addItem').selectpicker('refresh');
    $('#addItem').on('change', function() {
        if ($(this).val() != 0 || $(this).val() != productID) {
            productID = $(this).val();
        }
    });
   
    $('#add').on('click', function(e) {
        
        e.preventDefault();
        var addedJobCardServices = [];

        for (var key in mainJobCardServices) {
            if (mainJobCardServices.hasOwnProperty(key)) {
                var temp = {
                    taskID: key
                };
                addedJobCardServices.push(temp); 
               
            }
        }

        var vehicalTypeIDs = [];
        $.each($('#vehicalTypeDiv')[0].children, function(index, val) {
            $.each(val.children, function(ind, valOfInp) {
                $.each(valOfInp.children, function(indexOfChec, valOfCheck) {
                    if (valOfCheck.type == "checkbox" && valOfCheck.checked == true) {
                        vehicalTypeIDs.push(val.id);
                    }
                });
            });
        });
        var formData = {
            taskCardCode: $('#jobCardCode').val(),
            taskCardName: $('#jobCardName').val(),
            editMode: editMode,
            taskCardID: jobCardID,
            taskCardTasks: addedJobCardServices,
            vehicalTypeIDs: vehicalTypeIDs,
            productID: productID
        };

        if (validateFormData(formData)) {
            eb.ajax({
                url: BASE_URL + '/job-card-setup-api/saveJobCard',
                method: 'post',
                data: formData,
                dataType: 'json',
                success: function(data) {
                    p_notification(data.status, data.msg);
                    if (data.status) {
                        window.setTimeout(function() {
                            location.reload();
                        }, 1000);
                    } else {
                        if (data.data == "TERR") {
                            $('#taskCode').focus();
                        }
                    }
                }
            });
        }
    });

    $('#updateType').on('click', function(e) {
        
        e.preventDefault();
        var addedJobCardServices = [];

        for (var key in mainJobCardServices) {
            if (mainJobCardServices.hasOwnProperty(key)) {
                var temp = {
                    taskID: key
                };
                addedJobCardServices.push(temp); 
               
            }
        }

        var vehicalTypeIDs = [];
        $.each($('#vehicalTypeDiv')[0].children, function(index, val) {
            $.each(val.children, function(ind, valOfInp) {
                $.each(valOfInp.children, function(indexOfChec, valOfCheck) {
                    if (valOfCheck.type == "checkbox" && valOfCheck.checked == true) {
                        vehicalTypeIDs.push(val.id);
                    }
                });
            });
        });

        var formData = {
            taskCardCode: $('#jobCardCode').val(),
            taskCardName: $('#jobCardName').val(),
            editMode: editMode,
            taskCardID: jobCardID,
            taskCardTasks: addedJobCardServices,
            vehicalTypeIDs: vehicalTypeIDs,
            productID: productID
        };

        if (validateFormData(formData)) {
            eb.ajax({
                url: BASE_URL + '/job-card-setup-api/updateJobCard',
                method: 'post',
                data: formData,
                dataType: 'json',
                success: function(data) {
                    p_notification(data.status, data.msg);
                    if (data.status) {
                        window.setTimeout(function() {
                            location.reload();
                        }, 1000);
                    } else {
                        if (data.data == "TERR") {
                            $('#taskCode').focus();
                        }
                    }
                }
            });
        }
    });

    $('#job-card-search').on('submit', function(e) {
        e.preventDefault();
        var formData = {
            taskSearchKey: $('#job-card-search-keyword').val(),
        };

        if (formData.jobCardSearchKey != '') {
            searchValue(formData);
        } else {
            p_notification(false, eb.getMessage('ERR_SERVICE_STP_SEARCH_KEY'));
        }
    });

    $('#job-card-search').on('click', '.reset', function(e) {
        e.preventDefault();
        searchValue('');
        $('#job-card-search-keyword').val('');
    });

    $('#job-card-list').on('click', 'a.edit', function(e) {

        e.preventDefault();
        jobCardID = $(this).parents('tr').data('job-card-id');

        $('#jobCardCode').val($(this).parents('tr').find('.jcCode').text()).attr('disabled', true);
        $('#jobCardName').val($(this).parents('tr').find('.jcName').text());
    
        $('.updateTitle').removeClass('hidden');
        $('.addTitle').addClass('hidden');
        $('.update').removeClass('hidden');
        $('.add').addClass('hidden');
        $("#addItem").empty().
            append($("<option></option>").
                     attr("value", $(this).parents('tr').data('productid')).
                    text($(this).parents('tr').data('product')));
        $('#addItem').val($(this).parents('tr').data('productid'));
        $('#addItem').prop('disabled', true);
        $('#addItem').selectpicker('render');
        productID = $('#addItem').val();
        editMode = true;

        $('#addVehicalType').addClass('hidden');
        $('#viewVehicalType').removeClass('hidden');
        $('#vehicalTypeDiv').removeClass('hidden');

        getRelatedServices(jobCardID);
        getRelatedVehicleTypes(jobCardID);


    });

    $('#cancelService, #reset').on('click', function(e) {
        e.preventDefault();
        cancelResetView();
    });

    function cancelResetView() {
        jobCardID = '';
        $('#jobCardCode').val('').attr('disabled', false);
        $('#jobCardName').val('');
        $('.updateTitle').addClass('hidden');
        $('.addTitle').removeClass('hidden');
        $('.update').addClass('hidden');
        $('.add').removeClass('hidden');
        editMode = false;
        $('input[type=text]').val('');
        $('#addServiceModal #addServicesBody .addedService').remove();
        jobCardServices = {};
        mainJobCardServices = {};
    }

    $('#job-card-list').on('click', 'a.delete', function(e) {
        e.preventDefault();
        cancelResetView();
        jobCardID = $(this).parents('tr').data('job-card-id');
        jobCardCode = $(this).parents('tr').find('.jcCode').text();
        bootbox.confirm('Are you sure you want to delete this job card?', function(result) {
            if (result === true) {
                eb.ajax({
                    url: BASE_URL + '/job-card-setup-api/deleteJobCardByJobCardID',
                    method: 'post',
                    data: {
                        taskCardID: jobCardID,
                        taskCardCode: jobCardCode
                    },
                    dataType: 'json',
                    success: function(data) {
                        if (data.status == true) {
                            p_notification(data.status, data.msg);
                            window.location.reload();
                        } else {
                            p_notification(data.status, data.msg);
                        }
                    }
                });
            }
        });

    });

    function validateFormData(formData) {
        if (formData.taskCardCode == "") {
            p_notification(false, eb.getMessage('ERR_REQUIRED_JOB_CRD_CODE'));
            $('#serviceCode').focus();
            return false;
        } else if (formData.taskCardName == '') {
            p_notification(false, eb.getMessage('ERR_REQUIRED_JOB_CRD_NAME'));
            $('#serviceName').focus();
            return false;
        } else if (formData.productID == null || formData.productID == 0) {
            p_notification(false, eb.getMessage('ERR_JBCRD_PRO_CNT_BE_NULL'));
            $('#addItem').focus();
            return false;
        }
        return true;
    }
    function searchValue(formData) {
        eb.ajax({
            url: BASE_URL + '/job-card-setup-api/getJobCardBySearchKey',
            method: 'post',
            data: formData,
            dataType: 'json',
            success: function(data) {
                if (data.status) {
                    $("#job-card-list").html(data.html);
                } else {
                    p_notification(data.status, data.msg);
                }
            }
        });
    }

    $('#jobCardServices').on('click', function() {
        reomoveUnAddedServices();
    });

     $('#btnServicesClose').on('click',function(){    
        reomoveUnAddedServices();
    });

    //add new service to job card
    $('#add_service').on('click', function(e) {
        e.preventDefault();

        //if plus button is clicked then disabled the button
        //if button is clicked more than one, then it does not continue

        if ($(this).is(':disabled')) {
            return false;
        } else {
            $(this).attr('disabled', true);
            var $thisRow = $(this).parents('tr');

            if ($('#serviceID').val() == 0 || $('#serviceID').val() == null) {
                $('#addNewServiceRow').val('');
                p_notification(false, eb.getMessage('ERR_REQUIRED_SRVC'));
                $(this).attr('disabled', false);
            } else {
                serviceID = $('#serviceID').val();
                addNewJobCardServiceRow($('#serviceID option:selected').html(), false);
                $(this).attr('disabled', false);
                
            } 
        }
    });

    function addNewJobCardServiceRow(serviceCodeName, autofill) {

        var newTrID = 'tr_'+serviceID;
        var clonedRow = $($('#preSample').clone()).attr('id', newTrID).addClass('addedService');
        $("input[name='addedServiceCodeName']", clonedRow).val(serviceCodeName);
        clonedRow.removeClass('hidden');
        clonedRow.insertBefore('#preSample');
        jobCardServices[serviceID] = serviceCodeName;
        clearServiceDropdown();
        if (autofill) {
            $('#addServiceModal #addServicesBody #formRow '+'#'+newTrID+' #deleteClonedService').addClass('disabled');
        }
        
    }

    function clearServiceDropdown() {
        $('#serviceID')
                .empty()
                .val('')
                .append($("<option></option>")
                        .attr("value", 0)
                        .text('Select Service'))
                .selectpicker('refresh')
                .trigger('change');
    }

    $('#btnAddServices').on('click',function(){
        if ($('#serviceID').val() != 0 && $('#serviceID').val() != null) {
            p_notification(false, eb.getMessage('ERR_ADD_SERVICE'));
            return;
        }

        addServiceToJobCard();
        $("#addServiceModal").modal('toggle');

    });


    function addServiceToJobCard()
    {
        mainJobCardServices = {};
        for (var key in jobCardServices) {
            if (jobCardServices.hasOwnProperty(key)) {
                mainJobCardServices[key] = jobCardServices[key];
            }
        }

    }

    function reomoveUnAddedServices() {
        
        var temp1 = [];
        for (var key in mainJobCardServices) {
            if (mainJobCardServices.hasOwnProperty(key)) {
                temp1.push(key);
            }
        }
        
        var temp2 = [];
        for (var key in jobCardServices) {
            if (jobCardServices.hasOwnProperty(key)) {
                temp2.push(key);
            }
        }
        for (var i = 0; i < temp2.length; i++) {    
            if ($.inArray(temp2[i], temp1) == -1) {
                delete jobCardServices[temp2[i]];
                $('#' + 'tr_'+temp2[i]).remove();
            }           
        }
        addServiceToJobCard();
       
        $("#addServiceModal #addServicesBody").removeClass('hidden');
        $("#btnAddServices").removeClass('hidden');
        $("#btnServicesClose").removeClass('hidden');
        $("#btnViewServicesClose").addClass('hidden');
        $("#addServiceModal #viewServiceBody").addClass('hidden');
        $("#addServiceModal").modal('toggle');
    }

    $("#addServiceModal").on('click','.deleteAddedService', function(e) {
        e.preventDefault();
        var deleteTempServiceID = $(this).closest('tr').attr('id');
        var deleteServiceID = deleteTempServiceID.split('tr_')[1].trim();

        bootbox.confirm('Are you sure you want to remove this Service ?', function(result) {
            if (result == true) {
                delete jobCardServices[deleteServiceID];
                $('#' + deleteTempServiceID).remove();
            }
        });
    });

    /**
     *
     *use to get job card related services for services edit modal 
     */
    function getRelatedServices(Id) {
        eb.ajax({
            type: 'POST', url: BASE_URL + '/job-card-setup-api/getRelatedServices', 
            data: {taskCardID: Id}, 
            success: function(respond) {
                if (respond.status == true) {
                    if (Id == null) {
                        allsubTasks = respond.data['services'];
                    } else {
                        $('#servicesAddTable #formRow .addedService').remove();
                        mainJobCardServices = respond.data['services'];

                        editModeService = true;
                        for (var key in respond.data['services']) { 
                            serviceID = key;
                            var serviceCodeName = respond.data['services'][key];
                            addNewJobCardServiceRow(serviceCodeName, true);
                        }
                    }
                } 
            }
        });
    }

    /**
     *
     *use to get job card related services for services edit modal 
     */
    function getRelatedVehicleTypes(Id) {
        eb.ajax({
            type: 'POST', url: BASE_URL + '/job-card-setup-api/getRelatedVehicleTypes', 
            data: {taskCardID: Id}, 
            success: function(respond) {
                if (respond.status == true) {
                    $.each($('#vehicalTypeDiv')[0].children, function(index, val) {
                        $.each(val.children, function(ind, valOfInp) {
                            $.each(valOfInp.children, function(indexOfChec, valOfCheck) {
                                if (valOfCheck.type == "checkbox") {
                                    valOfCheck.checked = false;
                                }
                            });
                        });
                    });
                    if (respond.data.vehicleTypes.length > 0) {
                        $.each($('#vehicalTypeDiv')[0].children, function(index, val) {
                            $.each(val.children, function(ind, valOfInp) {
                                $.each(valOfInp.children, function(indexOfChec, valOfCheck) {
                                    if (valOfCheck.type == "checkbox") {
                                        $.each(respond.data.vehicleTypes, function(inde, valex) {
                                            if (valex == val.id) {
                                                valOfCheck.checked = true;
                                            }
                                        });
                                    }
                                });
                            });
                        });
                    }
                } 
            }
        });
    }

    /**
     *
     *use to get job card related services for services view modal 
     */
    function viewRelatedServicesDetails(Id) {
        eb.ajax({
            type: 'POST', url: BASE_URL + '/job-card-setup-api/viewRelatedServicesDetails', 
            data: {taskCardID: Id}, 
            success: function(respond) {
                if (respond.status == true) {
                    $("#addServiceModal #addServicesBody").addClass('hidden');
                    $("#btnAddServices").addClass('hidden');
                    $("#btnServicesClose").addClass('hidden');
                    $("#btnViewServicesClose").removeClass('hidden');
                    $("#addServiceModal #viewServiceBody").removeClass('hidden');
                    $("#addServiceModal").modal('toggle');

                    $('#addServiceModal #viewServiceBody .viewedService').remove();

                    for (var i = 0; i < respond.data.length; i++) {
                        
                        var newTrID = 'trs_'+respond.data[i]['taskCardTaskID'];
                        var clonedRow = $($('#jobSrvcRow').clone()).attr('id', newTrID).addClass('viewedService').data('job-card-service-id', respond.data[i]['taskCardTaskID']);
                        $("#srvcCode", clonedRow).text(respond.data[i]['code']);
                        $("#srvcName", clonedRow).text(respond.data[i]['name']);
                        clonedRow.data('service-id', respond.data[i]['taskID']);
                        clonedRow.removeClass('hidden');
                        clonedRow.insertBefore('#jobSrvcRow');
                        if (respond.data[i]['status'] == '1') {
                            $('#addServiceModal #viewServiceBody #viewNewServiceRow '+'#'+newTrID+' .serviceStatusIcon').addClass('fa-check-square-o');
                        } else {
                            $('#addServiceModal #viewServiceBody #viewNewServiceRow '+'#'+newTrID+' .serviceStatusIcon').addClass('fa-square-o');
                        }
                    }    
                } 
            }
        });
    }

    $('#serviceViewTable').on('click', '.serviceStatus', function(){
        var tempTaskCardTaskId = $(this).closest('tr').attr('id');
        var taskCardTaskID = tempTaskCardTaskId.split('trs_')[1].trim();
        if ($('#addServiceModal #viewServiceBody #viewNewServiceRow '+'#'+tempTaskCardTaskId+' .serviceStatusIcon').hasClass('fa-check-square-o')) {
            msg = 'Are you sure you want to Deactivate this service';
            status = '0';
        }
        else if ($('#addServiceModal #viewServiceBody #viewNewServiceRow '+'#'+tempTaskCardTaskId+' .serviceStatusIcon').hasClass('fa-square-o')) {
            msg = 'Are you sure you want to Activate this service';
            status = '1';
        }

        var jobCardServiceId = $(this).closest('tr').data('job-card-service-id');

        activeStatus(taskCardTaskID, msg, status, jobCardServiceId);
    });


    $('#serviceViewTable').on('click', '.deleteService', function(){
        var jobCardServiceId = $(this).closest('tr').data('job-card-service-id');
        var serviceId = $(this).closest('tr').data('service-id');

        deleteJobCardService(jobCardServiceId, serviceId);
    });

    /**
     *
     *use to change status job card service status
     */
    function activeStatus(taskCardTaskID, msg, status, jobCardServiceId) {
        var tempId = 'trs_'+taskCardTaskID;
        var currentDiv = $('#addServiceModal #viewServiceBody #viewNewServiceRow '+'#'+tempId+' .serviceStatusIcon');
        bootbox.confirm(msg, function($result) {
            if ($result == true) {
                eb.ajax({
                    url: BASE_URL + '/job-card-setup-api/changeJobCardServiceStatus',
                    method: 'post',
                    dataType: 'json',
                    data: {
                        'jobCardServiceId': jobCardServiceId,
                        'taskCardTaskStatus': status,
                    },
                    success: function(data) {
                        if (data.status) {
                            p_notification(data.status, data.msg);
                            if (currentDiv.hasClass('fa-check-square-o')) {
                                currentDiv.removeClass('fa-check-square-o');
                                currentDiv.addClass('fa-square-o');
                            }
                            else {
                                currentDiv.removeClass('fa-square-o');
                                currentDiv.addClass('fa-check-square-o');
                            }
                        } else {
                            p_notification(data.status, data.msg);
                        }
                    }
                });
            }
        });
    }

    /**
     *
     *use to delete job card services
     */
    function deleteJobCardService(jobCardServiceId, serviceId) {
        var tempTaskCardTaskID = 'trs_'+jobCardServiceId;
        var currentDiv = $('#addServiceModal #viewServiceBody #viewNewServiceRow '+'#'+tempTaskCardTaskID);
        bootbox.confirm('Are you sure you want to delete this Service ?', function($result) {
            if ($result == true) {
                eb.ajax({
                    url: BASE_URL + '/job-card-setup-api/deleteJobCardService',
                    method: 'post',
                    dataType: 'json',
                    data: {
                        'taskCardTaskID': jobCardServiceId,
                        'jobCardID' : jobCardID,
                        'taskID' : serviceId
                    },
                    success: function(data) {
                        if (data.status) {
                            p_notification(data.status, data.msg);
                            currentDiv.remove();
                        } else {
                            p_notification(data.status, data.msg);
                        }
                    }
                });
            }
        });
    }

    /**
     *
     *use to change status job card
     */
    function activeInactive(type, status, currentDiv, msg, flag) {
        bootbox.confirm(msg, function($result) {
            if ($result == true) {
                eb.ajax({
                    url: BASE_URL + '/job-card-setup-api/changeStatusID',
                    method: 'post',
                    dataType: 'json',
                    data: {
                        'jobCardId': type,
                        'status': status,
                    },
                    success: function(data) {
                        if (data.status) {
                            p_notification(data.status, data.msg);
                            if (currentDiv.hasClass('fa-check-square-o') && flag) {
                                currentDiv.removeClass('fa-check-square-o');
                                currentDiv.addClass('fa-square-o');
                            }
                            else {
                                currentDiv.removeClass('fa-square-o');
                                currentDiv.addClass('fa-check-square-o');
                            }
                        } else {
                            p_notification(data.status, data.msg);
                        }
                    }
                });
            }
        });
    }

    $('#job-card-list').on('click', '.view-services', function(e) {
        cancelResetView();
        jobCardID = $(this).parents('tr').data('job-card-id');
        viewRelatedServicesDetails(jobCardID);
    });

    $('#job-card-list').on('click', '.jobCardStat', function(e) {
        cancelResetView();
        var msg;
        var status;
        jobCardID   = $(this).closest('tr').data('job-card-id');
        var currentDiv = $(this).contents();
        var flag = true;

        if ($(this).children().hasClass('fa-check-square-o')) {
            msg = 'Are you sure you want to Deactivate this job card';
            status = '0';
        }
        else if ($(this).children().hasClass('fa-square-o')) {
            msg = 'Are you sure you want to Activate this job card';
            status = '1';
        }
        activeInactive(jobCardID, status, currentDiv, msg, flag);
    });

    $('#addVehicalType').on('click', function(event) {
        event.preventDefault();
        $(this).addClass('hidden');
        $('#viewVehicalType').removeClass('hidden');
        $('#vehicalTypeDiv').removeClass('hidden');
    });

});