$(function() {
    loadDropDownFromDatabase('/job-card-setup-api/search-job-cards-for-dropdown', "", 0, '#jobCard');
    $('#jobCard').selectpicker('refresh');
    loadDropDownFromDatabase('/task_setup_api/searchTasksForDropdown', "", '', '#serviceType');
    $('#serviceType').selectpicker('refresh');

    var startdateFormat = $('#serviceStartingDate').data('date-format');
    $('#serviceStartingDate').datetimepicker({
        format: startdateFormat + ' hh:ii'
    }).on('change', function(ev) {
        $('#serviceStartingDate').datetimepicker('hide');
        setDates(true);
    });

    var enddateFormat = $('#serviceEndingDate').data('date-format');
    $('#serviceEndingDate').datetimepicker({
        format: enddateFormat + ' hh:ii',
    }).on('change', function(ev) {
        $('#serviceEndingDate').datetimepicker('hide');
        if ($('#serviceStartingDate').val()) {
            setDates(true);
        } else {
            p_notification(false, eb.getMessage('ERR_START_DATE_MISSING'));
            $('#serviceStartingDate').focus();
            $('#serviceEndingDate').val('');
            return false;
        }
    });

    function setDates(falg) {
        var startingTime = $('#serviceStartingDate').datetimepicker('getDate');
        var endingTime = $('#serviceEndingDate').datetimepicker('getDate');
        if ($('#serviceStartingDate').val() && $('#serviceEndingDate').val()) {
            if (startingTime.getTime() > endingTime.getTime()) {
                p_notification(false, eb.getMessage('ERR_JOB_ST_DATE_CANT_BE_MORE_THAN_ED_DATE'));
                $('#serviceStartingDate').focus();
                return false;
            }
        }
    }

    $('#job-search').on('submit', function(e) {
        e.preventDefault();
        var formData = {
            jobSearchKey: $('#job-search-keyword').val(),
        };
        if (formData.jobSearchKey != '') {
            searchValue(formData);
        } else {
            p_notification(false, eb.getMessage('ERR_JBTYPE_SEARCH_KEY'));
        }
    });

    $('#job-search').on('click', '.reset', function(e) {
        e.preventDefault();
        searchValue('');
        $('#job-search-keyword').val('');
    });

    function searchValue(formData) {
        eb.ajax({
            url: BASE_URL + '/api/materialManagement/getJobServiceBySearchKey',
            method: 'post',
            data: formData,
            dataType: 'json',
            success: function(data) {
                if (data.status) {
                    $("#job-list").html(data.html);
                } else {
                    p_notification(data.status, data.msg);
                }
            }
        });
    }
    
    $( "#jobCard, #serviceType, #serviceEndingDate, #job-search-keyword").change(function() {
        var formData = {
            jobSearchKey: $('#job-search-keyword').val(),
            jobCard:($('#jobCard').val() != 0 && $('#jobCard').val() != null) ? $('#jobCard').val() : null,
            serviceType: ($('#serviceType').val() != 0 && $('#serviceType').val() != null) ? $('#serviceType').val() : null,
            serviceEndingDate: ($('#serviceEndingDate').val()) ? $('#serviceEndingDate').val() : null,
            serviceStartingDate: ($('#serviceStartingDate').val()) ? $('#serviceStartingDate').val() : null
        }
        searchValue(formData);
    });

    $('.jobEmployees').on('click', function(event) {
        event.preventDefault();
        $("#viewServiceEmployeeModal #serviceEmployeesTable tbody .addedEmployees").remove();
        eb.ajax({
            url: BASE_URL + '/api/materialManagement/getJobEmployeeByJobTaskId',
            method: 'post',
            data: {jobTaskID:$(this).attr('id')},
            dataType: 'json',
            success: function(respond) {
                if (respond.status) {
                    $.each(respond.data, function(index, val) {
                        var newTrID = 'tr_' + val.jobTaskID;
                        var clonedRow = $($('#preSample').clone()).attr('id', newTrID).addClass('addedEmployees');
                        if (val.employeeCode != null) {
                            $("#employeeCode", clonedRow).text(val.employeeCode);
                            $("#employeeName", clonedRow).text(val.employeeName);
                        } else {
                            $("#employeeName", clonedRow).text("Default Employee");
                        }
                        clonedRow.removeClass('hidden');
                        clonedRow.insertBefore('#preSample');
                    });
                }
            }
        });  
        $('#viewServiceEmployeeModal').modal('show');
    });
});