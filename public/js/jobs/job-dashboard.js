
$(document).ready(function() {
	var jobId = null;
	var employeeID = null;
	var jobTaskEmployees = {};
	var jobTaskID = null;
	var serviceList = {};
	var entityID = null;

	$('.job_dashbord-list').css( 'cursor', 'pointer' );


	loadDropDownFromDatabase('/employee-api/searchDesignationWiseEmployeesForDropdown', "", 1, '#employeeID', '', '', false);
    $('#employeeID').children().not('.taskEmployeeCodeDefaultSelect').remove();
    $('#employeeID').selectpicker('refresh');


	$('.job_dashbord-list').on('click', function(){

		var customer = $(this).closest('li').data('customer-name');
		var vehicleTypeModel = $(this).closest('li').data('vehicle');
		var supervisor = $(this).closest('li').data('supervisor');
		jobId = $(this).closest('li').data('job-id');
		var inDateTime = $(this).closest('li').data('in-date-time');
		var jobStatus = $(this).closest('li').data('job-status');
		entityID = $(this).closest('li').data('entity-id');

		if ($(this).closest('li').hasClass('pendingList')) {
			$('#btnStartAll').removeClass('hidden');
		}

		var style = "";
		var state = "";
		switch (jobStatus) {
            case 3:
                style = "label label-success";
                $('#btnCosting').addClass('hidden');
                state = 'Pending';
                break;
            case 8:
                style = "label label-primary";
                $('#btnCosting').addClass('hidden');
                state = 'In Progress';
                break;
            case 9:
                style = "label label-info";
                $('#btnCosting').removeClass('hidden');
                state = 'Completed';
		        var url = BASE_URL + '/serviceCost/createCost/'+jobId;
		        $("#btnCosting").attr("href", url);
                break;
            case 20:
                style = "label label-invoiced";
                $('#btnCosting').addClass('hidden');
                state = 'Invoice';
                break;
            case 21:
                style = "label label-success";
                $('#btnCosting').addClass('hidden');
                state = 'Paid';
                break;
        }


		$('#jobCustomer').text(customer);
		$('#jobVehicle').text(vehicleTypeModel);
		$('#jobSupervisor').text(supervisor);
		$('#jobInDate').text(inDateTime);
		$('#jobStatus').text(state).addClass(style);

		getJobRelatedServices(jobId);
	});

	function getJobRelatedServices(jobId, updateList = false) {
		eb.ajax({
            type: 'POST', url: BASE_URL + '/service-job-dashboard-api/getJobRelatedServices', 
            data: {jobId: jobId}, 
            success: function(respond) {
                if (respond.status == true) {
                	serviceList = respond.data;

                    $("#service-list").html(respond.html);
                    var result = checkAvailabilityToEnableEndAll(respond.data);

                    if (result) {
                    	$('#btnEndAll').removeClass('hidden');
                    }

                    var pendingTasks = checkAvailabilityToEnableStartAll(respond.data);

                    if (pendingTasks) {
                    	$('#btnStartAll').removeClass('hidden');
                    }

                    if (!updateList) {

                    	setJobCard();
                    	$('#serviceListModal').modal({
			                backdrop: 'static',
			                keyboard: false
			            });
                    }
                } 
            }
        });

	}

	function setJobCard(){
		var jobCardName = null;
		for (var i = 0; i < serviceList.length; i++) {

			if (serviceList[i]['jobTaskTaskCardID'] != null) {
				jobCardName = serviceList[i]['taskCardName'];
			}
		}
		
		$('#jobCard').text(jobCardName);
	}


	function loadJobStatus(jobId) {
		eb.ajax({
            type: 'POST', url: BASE_URL + '/service-job-dashboard-api/getJobStatusByJobId', 
            data: {
            	jobId: jobId
            }, 
            success: function(respond) {
                if (respond.status == true) {
                    var result = checkAvailabilityToEnableEndAll(respond.data);
                    getJobRelatedServices(jobId, true);

                    if (result) {
                    	$('#btnEndAll').removeClass('hidden');
                    }
                } 
            }
        });
	}

	$("#serviceListModal #service-list ").on('click', '.start_task',function(){
		jobTaskID = $(this).closest('tr').data('job-task-id');
        var issueMaterialFlag = false;
        if($("#"+jobTaskID+"issueMaterialForTask").is(':checked')) {
            issueMaterialFlag = true;
        } else {
            issueMaterialFlag = false;
        }

		eb.ajax({
            type: 'POST', url: BASE_URL + '/service-job-dashboard-api/startJobRelatedService', 
            data: {
            	jobTaskId: jobTaskID,
            	jobId: jobId,
                issueMaterialFlag : issueMaterialFlag
            }, 
            success: function(respond) {
                if (respond.status == true) {
                    var result = checkAvailabilityToEnableEndAll(respond.data);
                    getJobRelatedServices(jobId, true);

                    if (result) {
                    	$('#btnEndAll').removeClass('hidden');
                    } else {
                    	$('#btnEndAll').addClass('hidden');
                    }

                    var pendingTasks = checkAvailabilityToEnableStartAll(respond.data);

	                if (pendingTasks) {
	                	$('#btnStartAll').removeClass('hidden');
	                } else {
	                	$('#btnStartAll').addClass('hidden');
	                }
                } 
            }
        });
	});

	$("#serviceListModal #service-list ").on('click', '.hold_task',function(){
		jobTaskID = $(this).closest('tr').data('job-task-id');

		eb.ajax({
            type: 'POST', url: BASE_URL + '/service-job-dashboard-api/holdJobRelatedService', 
            data: {
            	jobTaskId: jobTaskID,
            	jobId: jobId
            }, 
            success: function(respond) {
                if (respond.status == true) {
                    // p_notification(respond.status, respond.msg);
                    getJobRelatedServices(jobId, true);

                    var result = checkAvailabilityToEnableEndAll(respond.data);

	                if (result) {
	                	$('#btnEndAll').removeClass('hidden');
	                } else {
	                	$('#btnEndAll').addClass('hidden');
	                }

	                var pendingTasks = checkAvailabilityToEnableStartAll(respond.data);

	                if (pendingTasks) {
	                	$('#btnStartAll').removeClass('hidden');
	                } else {
	                	$('#btnStartAll').addClass('hidden');
	                }
                } 
            }
        });
	});

	$('#btnViewServiceClose').on('click', function () {
		$('#serviceListModal').modal('toggle');
		window.location.reload();
	});


	$("#serviceListModal #service-list ").on('click', '.end_task',function(){
		jobTaskID = $(this).closest('tr').data('job-task-id');
		
		eb.ajax({
            type: 'POST', url: BASE_URL + '/service-job-dashboard-api/stopJobRelatedService', 
            data: {
            	jobTaskId: jobTaskID,
            	jobId: jobId,
            	entityID: entityID
            }, 
            success: function(respond) {
                if (respond.status == true) {
                    getJobRelatedServices(jobId, true);

                    var result = checkAvailabilityToEnableEndAll(respond.data);

	                if (result) {
	                	$('#btnEndAll').removeClass('hidden');
	                } else {
	                	$('#btnEndAll').addClass('hidden');
	                }

	                var pendingTasks = checkAvailabilityToEnableStartAll(respond.data);

	                if (pendingTasks) {
	                	$('#btnStartAll').removeClass('hidden');
	                } else {
	                	$('#btnStartAll').addClass('hidden');
	                }
                }  
            }
        });
	});

	$('#btnStartAll').on('click', function() {
        var materialIssueTaskIds = [];
        $('#addNewServiceRateRow tr').each(function(){
            if($("#"+$(this).data('jobTaskId')+"issueMaterialForTask").is(':checked')) {
                materialIssueTaskIds.push($(this).data('jobTaskId'));
            } 
        })
		eb.ajax({
            type: 'POST', url: BASE_URL + '/service-job-dashboard-api/startAllJobRelatedService', 
            data: {
            	jobId: jobId,
                materialIssueTaskIds: materialIssueTaskIds
            }, 
            success: function(respond) {
                if (respond.status == true) {
                    // p_notification(respond.status, respond.msg);
                    $('#btnStartAll').addClass('hidden');
                    $('#btnEndAll').removeClass('hidden');
                    getJobRelatedServices(jobId, true);
                } 
            }
        });
	});

	$('#btnEndAll').on('click', function() {
		eb.ajax({
            type: 'POST', url: BASE_URL + '/service-job-dashboard-api/endAllJobRelatedService', 
            data: {
            	jobId: jobId
            }, 
            success: function(respond) {
                if (respond.status == true) {
                    // p_notification(respond.status, respond.msg);
                    $('#btnStartAll').addClass('hidden');
                    $('#btnEndAll').addClass('hidden');
                    getJobRelatedServices(jobId, true);
                } 
            }
        });
	});

	function checkAvailabilityToEnableEndAll(dataSet) {
		
		var pendingNum = 0;
		var inprogressNum = 0;

		for (var i = 0; i < dataSet.length; i++) {
			if (dataSet[i]['jobTaskStatus'] == 3 ) {
				pendingNum += 1;
			}

			if (dataSet[i]['jobTaskStatus'] == 8) {
				inprogressNum += 1;
			}
		}

		if (pendingNum == 0 && inprogressNum > 0) {
			return true;
		}
		return false;
	}

	function checkAvailabilityToEnableStartAll(dataSet) {
		
		var pendingTskNum = 0;

		for (var i = 0; i < dataSet.length; i++) {
			if (dataSet[i]['jobTaskStatus'] == 3 ) {
				pendingTskNum += 1;
			}
		}

		if (pendingTskNum > 0) {
			return true;
		}
		return false;
	}

	$('#serviceListModal #service-list').on('click','.employee-view', function() {
		jobTaskID = $(this).closest('tr').data('job-task-id');

		getJobTaskRelatedEmployees();

	}); 

	function getJobTaskRelatedEmployees() {

		eb.ajax({
            type: 'POST', url: BASE_URL + '/service-job-dashboard-api/getJobTaskRelatedEmployees', 
            data: {
            	jobId: jobId,
            	jobTaskID: jobTaskID
            }, 
            success: function(respond) {
                if (respond.status == true) {

                    $('#addEmployeeModal .addedEmployees').remove();
                	for (var i = 0; i < respond.data.length; i++) {
                		var newTrID = 'tr_' + respond.data[i]['employeeID'];
		                var clonedRow = $($('#jobEmpRow').clone()).attr('id', newTrID).addClass('addedEmployees');
		                $("#EmpName", clonedRow).text(respond.data[i]['employeeFirstName']);
		                $("#EmpCode", clonedRow).text(respond.data[i]['employeeCode']);
		                clonedRow.removeClass('hidden');
		                clonedRow.insertBefore('#jobEmpRow');
                	}

		            if (respond.data.length > 0) {
                        $('#addEmployeeModal').modal('toggle');
                    } else {
                        p_notification(false, eb.getMessage('ERR_NO_ANY_EMP_ASSIGN_SRVCE'));
                        return false;
                    }
                } 

            }
        });
	}


	$("#serviceListModal #service-list ").on('click', '.restart_task',function(){
		jobTaskID = $(this).closest('tr').data('job-task-id');

		eb.ajax({
            type: 'POST', url: BASE_URL + '/service-job-dashboard-api/restartJobRelatedService', 
            data: {
            	jobTaskId: jobTaskID,
            	jobId: jobId
            }, 
            success: function(respond) {
                if (respond.status == true) {
                    getJobRelatedServices(jobId, true);
                } 
            }
        });
	});

});
    