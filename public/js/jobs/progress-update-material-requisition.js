$(document).ready(function() {

	var selectedTasks = [];
	var selectedTaskRelatedItems = {};
	var selectedOtherItems = {};
	var jobRelatedProducts = [];
	var allSelectedItems = [];
	var openCount = 0;

	if (localStorage.getItem("job_list_search_params") != null) {
		$('#searchBackBtn').removeClass('hidden');
		$('#normalBackBtn').addClass('hidden');
        var jobListSearchParams = localStorage.getItem('job_list_search_params');
        var params = JSON.parse(jobListSearchParams);
        params.backAction = true;
        localStorage.setItem("job_list_search_params", JSON.stringify(params));
    } else {
    	$('#searchBackBtn').addClass('hidden');
		$('#normalBackBtn').removeClass('hidden');
    }
    $('#normalBackBtn').on('click', function(){
         window.history.go(-1);
    });

	getJobRelatedProducts();

	$('#sendRequest').addClass('disabled');

	function getJobRelatedProducts ()
	{
		var tempJobID = $('#jobId').val();
		eb.ajax({
            url: BASE_URL + '/progress-api/getJobRelatedProducts',
            method: 'post',
            data: {jobID:tempJobID},
            dataType: 'json',
            success: function(respond) {
                if (respond.status) {
                	jobRelatedProducts = respond.data;

                	for (var i = 0; i < jobRelatedProducts.length; i++) {
                		if (jobRelatedProducts[i]['jobProductStatus'] == '3') {
                			openCount += 1;
                		}
                	}

                	if (openCount == 0) {
                		$('#selectAllSpan').addClass('hidden');
                	}

                }
            }
        });
	}

	$('#selectAllTask').css( 'cursor', 'pointer' );  
	$('#expandAllTask').css( 'cursor', 'pointer' );  
	$('.selectTask').css( 'cursor', 'pointer' );  
	$('.expandTask').css( 'cursor', 'pointer' );  
	$('.expandTask').css( 'cursor', 'pointer' );  
	$('.selectOtherMaterial').css( 'cursor', 'pointer' );  
	
	$('#selectAllTask').on('click', function(){

		var selected = $('#selectAllTask').data('selected');
		if (selected) {
			$('#selectAllIcon').removeClass('fa-check-square-o');
			$('#selectAllIcon').addClass('fa-square-o');
			$('#selectAllTask').data('selected', false);

			$('.taskTable .selectTask').removeClass('fa-check-square-o');
			$('.taskTable .selectTask').addClass('fa-square-o');
			$('.taskTable .selectTask').data('selected', false);

			$('.otherMaterialsTable .selectOtherMaterial').removeClass('fa-check-square-o');
			$('.otherMaterialsTable .selectOtherMaterial').addClass('fa-square-o');
			$('.otherMaterialsTable .selectOtherMaterial').data('selected', false);

			selectedTaskRelatedItems = {};
			selectedOtherItems = {};

		} else {
			$('#selectAllIcon').removeClass('fa-square-o');
			$('#selectAllIcon').addClass('fa-check-square-o');
			$('#selectAllTask').data('selected', true);

			$('.taskTable .selectTask').removeClass('fa-square-o');
			$('.taskTable .selectTask').addClass('fa-check-square-o');
			$('.taskTable .selectTask').data('selected', true);

			$('.otherMaterialsTable .selectOtherMaterial').removeClass('fa-square-o');
			$('.otherMaterialsTable .selectOtherMaterial').addClass('fa-check-square-o');
			$('.otherMaterialsTable .selectOtherMaterial').data('selected', true);

			addAllItemsToRelatedObjects();
		}

	});

	$('#expandAllTask').on('click', function(){

		var expanded = $('#expandAllTask').data('expanded');
		if (expanded) {
			$('#expandAllIcon').removeClass('fa-check-square-o');
			$('#expandAllIcon').addClass('fa-square-o');
			$('#expandAllTask').data('expanded', false);

			$('.subHeaders').addClass(' hidden');
			$('.subRows').addClass('hidden');
			$('.expandTask').data('expanded', false);
		} else {
			$('#expandAllIcon').removeClass('fa-square-o');
			$('#expandAllIcon').addClass('fa-check-square-o');
			$('#expandAllTask').data('expanded', true);

			$('.subHeaders').removeClass('hidden');
			$('.subRows').removeClass('hidden');
			$('.expandTask').data('expanded', true);
		}

	});

	$('.taskTable').on('click', '.selectTask', function(){
	
		var selected = $('#'+$(this).attr('id')).data('selected');
		var jobTaskId = $(this).attr('id').split("_")[1];
		// return;
		if (selected) {
			$('#'+$(this).attr('id')).removeClass('fa-check-square-o');
			$('#'+$(this).attr('id')).addClass('fa-square-o');
			$('#'+$(this).attr('id')).data('selected', false);
			
			removeTaskRealatedItemFromMainObject(jobTaskId);

			$('#selectAllIcon').removeClass('fa-check-square-o');
			$('#selectAllIcon').addClass('fa-square-o');
			$('#selectAllTask').data('selected', false);

		} else {
			$('#'+$(this).attr('id')).removeClass('fa-square-o');
			$('#'+$(this).attr('id')).addClass('fa-check-square-o');
			$('#'+$(this).attr('id')).data('selected', true);

			addTaskRealatedItemToMainObject(jobTaskId);
		}
	});

	$('.taskTable').on('click', '.expandTask', function(){

		var expanded = $('#'+$(this).attr('id')).data('expanded');
		var tempId = $(this).attr('id').split("_")[1];
		var subHeaderClass = 'subHeader_'+tempId;
		var subRowClass = 'subRow_'+tempId;

		if (expanded) {
			$('.'+subHeaderClass).addClass(' hidden');
			$('.'+subRowClass).addClass('hidden');
			$('#'+$(this).attr('id')).data('expanded', false);
		} else {
			
			$('.'+subHeaderClass).removeClass('hidden');
			$('.'+subRowClass).removeClass('hidden');
			$('#'+$(this).attr('id')).data('expanded', true);
			
		}

	});

	$('.otherMaterialsTable').on('click', '.selectOtherMaterial', function(){
	
		var selected = $('#'+$(this).attr('id')).data('selected');
		var jobProductId = $(this).attr('id').split("_")[1];
		
		if (selected) {
			$('#'+$(this).attr('id')).removeClass('fa-check-square-o');
			$('#'+$(this).attr('id')).addClass('fa-square-o');
			$('#'+$(this).attr('id')).data('selected', false);
			delete selectedOtherItems[jobProductId];
			if (jQuery.isEmptyObject(selectedOtherItems) && jQuery.isEmptyObject(selectedTaskRelatedItems)) {
				$('#sendRequest').addClass('disabled');
			}

			$('#selectAllIcon').removeClass('fa-check-square-o');
			$('#selectAllIcon').addClass('fa-square-o');
			$('#selectAllTask').data('selected', false);

		} else {
			$('#'+$(this).attr('id')).removeClass('fa-square-o');
			$('#'+$(this).attr('id')).addClass('fa-check-square-o');
			$('#'+$(this).attr('id')).data('selected', true);
			addOtherItemsToMainObject(jobProductId);
		}
	});

	function addTaskRealatedItemToMainObject (jobTaskId) {	
		for (var i = 0; i < jobRelatedProducts.length; i++) {		
			if (jobRelatedProducts[i]['jobTaskID'] != null) {
				if (jobTaskId == jobRelatedProducts[i]['jobTaskID']) {
					selectedTaskRelatedItems[jobRelatedProducts[i]['jobTaskID']+'_'+jobRelatedProducts[i]['jobProductID']] = jobRelatedProducts[i];

				}
			}
		}
		$('#sendRequest').removeClass('disabled');
	}

	function addOtherItemsToMainObject (jobProductId) {	
		for (var i = 0; i < jobRelatedProducts.length; i++) {		
			if (jobRelatedProducts[i]['jobTaskID'] == null) {
				if (jobProductId == jobRelatedProducts[i]['jobProductID']) {
					selectedOtherItems[jobRelatedProducts[i]['jobProductID']] = jobRelatedProducts[i];

				}
			}
		}
		$('#sendRequest').removeClass('disabled');
	}

	function addAllItemsToRelatedObjects () {	
		for (var i = 0; i < jobRelatedProducts.length; i++) {		
			if (jobRelatedProducts[i]['jobProductStatus'] == '3') {
				if (jobRelatedProducts[i]['jobTaskID'] != null) {	
						selectedTaskRelatedItems[jobRelatedProducts[i]['jobTaskID']+'_'+jobRelatedProducts[i]['jobProductID']] = jobRelatedProducts[i];				
				} else {
					selectedOtherItems[jobRelatedProducts[i]['jobProductID']] = jobRelatedProducts[i];
				}
			}
		}
		$('#sendRequest').removeClass('disabled');
	}

	function removeTaskRealatedItemFromMainObject (jobTaskId) {	
		for (var prop in selectedTaskRelatedItems) {
			if (selectedTaskRelatedItems[prop]['jobTaskID'] == jobTaskId) {
				delete selectedTaskRelatedItems[prop];
			}
		}

		if (jQuery.isEmptyObject(selectedOtherItems) && jQuery.isEmptyObject(selectedTaskRelatedItems)) {
			$('#sendRequest').addClass('disabled');
		}
	}

	function manageMainItemArray () {
		var taskItems = [];	
		var otherItems = [];	
		for (var prop in selectedTaskRelatedItems) {
		   taskItems.push(selectedTaskRelatedItems[prop]);
		}
		for (var key in selectedOtherItems) {
		   otherItems.push(selectedOtherItems[key]);
		}
		allSelectedItems = taskItems.concat(otherItems);
	}


	$('#sendRequest').on('click', function () {
		manageMainItemArray();

		var tempJobID = $('#jobId').val();
		eb.ajax({
            url: BASE_URL + '/progress-api/sendMaterialRequest',
            method: 'post',
            data: {
            	jobID: tempJobID,
            	itemArray: allSelectedItems
            },
            dataType: 'json',
            success: function(respond) {
            	p_notification(respond.status, respond.msg); 
                if (respond.status) {
                	window.setTimeout(function() {
                            location.reload();
                    }, 2000);
                }
            }
        });

	});

	$('#viewSupervisors').on('click', function(){
		$("#viewJobSupervisorModal #jobSupervisorTable tbody .addedSupervisors").remove();
        var tempJobID = $('#jobId').val();
        eb.ajax({
            url: BASE_URL + '/job-api/getJobSupervisorsByJobID',
            method: 'post',
            data: {jobID:tempJobID},
            dataType: 'json',
            success: function(respond) {
                if (respond.status) {
                    $.each(respond.data, function(index, val) {
                        var newTrID = 'tr_' + val.jobSupervisorID;
                        var clonedRow = $($('#preSample').clone()).attr('id', newTrID).addClass('addedSupervisors');
                        $("#supervisorName", clonedRow).text(val.employeeFirstName+" "+val.employeeSecondName);
                        clonedRow.removeClass('hidden');
                        clonedRow.insertBefore('#preSample');
                    });
                }
            }
        });  
        $('#viewJobSupervisorModal').modal('show');
    });

    $('#viewContractors').on('click', function(){
    	$("#viewJobContractorModal #jobContractorTable tbody .addedcontractors").remove();
        var tempJobID = $('#jobId').val();
        eb.ajax({
            url: BASE_URL + '/job-api/getJobContractorsByJobID',
            method: 'post',
            data: {jobID:tempJobID},
            dataType: 'json',
            success: function(respond) {
                if (respond.status) {
                    $.each(respond.data, function(index, val) {
                        var newTrID = 'tr_' + val.jobSupervisorID;
                        var clonedRow = $($('#contractorRow').clone()).attr('id', newTrID).addClass('addedcontractors');
                        $("#contractorName", clonedRow).text(val.contractorFirstName+" "+val.contractorSecondName);
                        clonedRow.removeClass('hidden');
                        clonedRow.insertBefore('#contractorRow');
                    });
                }
            }
        });  
        $('#viewJobContractorModal').modal('show');
    });


});