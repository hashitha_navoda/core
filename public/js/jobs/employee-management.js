
$(document).ready(function() {

    var departmentID = null;
    var jobTaskId = null;
    var jobId = null;
    var empArray = [];
    var departmentStationID = null;
    var nextJobTaskID = null;
    var nextDepartmentID = null;
    var startingTime = null;
    var endingTime = null;
    var nowTemp = new Date();
    var itemID = null;
    var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);
    var nowTemp = new Date();
    var now1 = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);
    var locationID = $('#navBarSelectedLocation').attr('data-locationid');
    var showJobList = true;
    
    $('.department-list-item').css( 'cursor', 'pointer' );
    var checkin1 = $('#serviceStartingDate').datepicker({onRender: function(date) {
        return date.valueOf() < now1.valueOf() ? ((U_DATE_R_OVERRIDE) ? '' : 'disabled') : '';
    },
    }).on('changeDate', function(ev) {
        checkin1.hide();
        if ($('#serviceEndingDate').val()) {
            if ($('#serviceStartingDate').val() > $('#serviceEndingDate').val()) {
                $('#serviceStartingDate').val('');
                $('#serviceStartingDate').focus();
                p_notification(false, eb.getMessage('ERR_START_DATE_GRATE_END_DTE'));    
            }
            getDepartmentRelatedEmployeeListsDetails();
        }
    }).data('datepicker');
    

    var checkin2 = $('#serviceEndingDate').datepicker({onRender: function(date) {
        return date.valueOf() < now1.valueOf() ? ((U_DATE_R_OVERRIDE) ? '' : 'disabled') : '';
    },
    }).on('changeDate', function(ev) {
        checkin2.hide();
        if ($('#serviceStartingDate').val()) {
            if ($('#serviceStartingDate').val() > $('#serviceEndingDate').val()) {
                $('#serviceEndingDate').val('');
                $('#serviceEndingDate').focus();
                p_notification(false, eb.getMessage('ERR_END_DATE_LESS_STRT_DTE'));    
            }
            getDepartmentRelatedEmployeeListsDetails();
        } else {
            p_notification(false, eb.getMessage('ERR_START_DATE_MISSING'));
            $('#serviceStartingDate').focus();
            $('#serviceEndingDate').val('');
            return false;
        }

    }).data('datepicker');

    function setDates(falg) {
        startingTime = $('#serviceStartingDate').datepicker('getDate');
        endingTime = $('#serviceEndingDate').datepicker('getDate');
        if ($('#serviceStartingDate').val() && $('#serviceEndingDate').val()) {
            if (startingTime.getTime() > endingTime.getTime()) {
                p_notification(false, eb.getMessage('ERR_JOB_ST_DATE_CANT_BE_MORE_THAN_ED_DATE'));
                $('#serviceStartingDate').focus();
                return false;
            }
        }
    }

    $('#allDep').addClass('active-dep');
    getDepartmentRelatedEmployeeListsDetails();

    $('.department-list-item').on('click',function(){
        if (!$(this).hasClass('active-dep')) {
            $('#department-list .active-dep').removeClass('active-dep');
            $(this).addClass('active-dep');

            departmentID = $(this).data('department-id');
            jobTaskId = null;
            jobId = null;
            departmentStationID = null;
            empArray = [];
            $('#startTask').removeClass('hidden');
            $('.progress-list-header').removeClass('hidden');
            getDepartmentRelatedEmployeeListsDetails();


        } else {
            departmentID = null;
            getDepartmentRelatedEmployeeListsDetails();
            $('#startTask').addClass('hidden');
            $('.progress-list-header').addClass('hidden');
        }
    });

    function getDepartmentRelatedEmployeeListsDetails() {
        eb.ajax({
            url: BASE_URL + '/employee-management-api/getDepartmentRelatedEmployeeListsDetails',
            method: 'post',
            data: {
                departmentID: departmentID,
                toDate: ($('#serviceEndingDate').val()) ? $('#serviceEndingDate').val() : null,
                fromDate: ($('#serviceStartingDate').val()) ? $('#serviceStartingDate').val() : null
            },
            dataType: 'json',
            success: function(data) {
                if (data.status) {
                    $("#employeePerformanceList").html(data.html);
                } else {
                    p_notification(data.status, data.msg);
                }
            }
        });
    }

    $('#employeePerformanceList').on('click','.viewEmpDetails', function(){
         var empId = $(this).closest('.main-emp-row').data('emp-id');
         getEmployeeJobTaskListsDetails(empId);

    });

    function getEmployeeJobTaskListsDetails(empId, isModalOpened = false) {
        eb.ajax({
            url: BASE_URL + '/employee-management-api/getEmployeeJobTaskListsDetails',
            method: 'post',
            data: {
                employeeId: empId,
                toDate: ($('#serviceEndingDate').val()) ? $('#serviceEndingDate').val() : null,
                fromDate: ($('#serviceStartingDate').val()) ? $('#serviceStartingDate').val() : null,
                departmentID: departmentID
            },
            dataType: 'json',
            success: function(data) {
                if (data.status) {
                    $("#employee-job-task-list").html(data.html);
                    if (!isModalOpened) {
                        $("#employeeJobTaskModal").modal('toggle');
                    }
                } else {
                    p_notification(data.status, data.msg);
                }
            }
        });
    }

    $('#employeeJobTaskModal #employee-job-task-list ').on('click','.editIncentive .edit', function(){
        var empId = $(this).closest('tr').find('.incentiveVal').attr('disabled', false);
        $(this).addClass('hidden');
        var empId = $(this).closest('tr').find('.save').removeClass('hidden');
    });

    $('#employeeJobTaskModal #employee-job-task-list ').on('click','.editIncentive .save', function(){
        var employeeId = $(this).closest('tr').data('employee-id');
        var jobTaskID = $(this).closest('tr').data('job-task-id');
        var incentiveVal = $(this).closest('tr').find('.incentiveVal').val();

        updateIncentiveValue(employeeId, jobTaskID, incentiveVal);

        var empId = $(this).closest('tr').find('.incentiveVal').attr('disabled', false);
        $(this).addClass('hidden');
        var empId = $(this).closest('tr').find('.save').removeClass('hidden');
    });


    function updateIncentiveValue(employeeId, jobTaskID, incentiveVal) {
        eb.ajax({
            url: BASE_URL + '/employee-management-api/updateIncentiveValue',
            method: 'post',
            data: {
                employeeId: employeeId,
                jobTaskID: jobTaskID,
                incentiveVal: incentiveVal 
            },
            dataType: 'json',
            success: function(data) {
                if (data.status) {
                    p_notification(data.status, data.msg);
                    getEmployeeJobTaskListsDetails(employeeId, true);
                } else {
                    p_notification(data.status, data.msg);
                }
            }
        });
    }

});
    