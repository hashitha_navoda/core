$(document).ready(function() {

    var projectId = null;
    var jobId = null;
    var jobTaskDetails = [];
    var jobTaskProductDetails = [];
    var savedJobTaskDetails = [];
    var savedJobTaskDetailsObject = {};
    var savedJobProductDetails = [];
    var savedOtherJobCostDetails = [];

    function jobTask(jobTaskID, jobTaskQty, jobTaskUnitCost, jobTaskUnitRate) {
        this.jobTaskID = jobTaskID;
        this.jobTaskQty = jobTaskQty;
        this.jobTaskUnitCost = jobTaskUnitCost;
        this.jobTaskUnitRate = jobTaskUnitRate;
    }

    function jobTaskProduct(jobTaskID, jobProductID, jobProductQty, jobProductUnitPrice, locationProductID) {
        this.jobTaskID = jobTaskID;
        this.jobProductID = jobProductID;
        this.jobProductQty = jobProductQty;
        this.jobProductUnitPrice = jobProductUnitPrice;
        this.jobProductUnitPrice = jobProductUnitPrice;
        this.locationProductID = locationProductID;
    }

    function jobOtherCost(jobID, costID, unitCost, units) {
        this.jobID = jobID;
        this.costID = costID;
        this.unitCost = unitCost;
        this.units = units;
    }


    loadDropDownFromDatabase('/job-api/search-job-project-for-dropdown', '', '', '#projectJobCode', '', '', false);
    loadDropDownFromDatabase('/api/cost/search-cost-type-for-dropdown', '', '', '#costType', '', '', false);
    $('#projectCode').selectpicker('refresh');
    
    

    $('#projectJobCode').on('change', function() {
        if ($(this).val() > 0 && ($(this).val() != jobId)) {
            jobId = $(this).val();
        }
    });

    $('#btnManage').on('click', function() {
    	formData = {
    		'jobId' : jobId ? jobId : null,
    	};
    	if (validationManageForm(formData)) {
            $('#cost-of-metarials').addClass('hidden');
    		manageCosts();
    	}
    });

    function manageCosts() {
        $('#cost-of-miscellaneous-table .addedOtherCost').remove();
        eb.ajax({
            url: BASE_URL + '/job-api/get-job-task-with-product-details',
            method: 'post',
            data: {
                jobId: jobId,
            },
            dataType: 'json',
            success: function(data) {
                if(data.status) {
                    $('#default-job-list').addClass('hidden');
                    jobTaskDetails = data.data.jobTaskDetails;
                    jobTaskProductDetails = data.data.jobTaskProductDetails;
                    setJobTasks(jobTaskDetails);
                } else {
                    p_notification(data.status, data.msg);
                }
            }
        });
    }

    $('#default-job-list').on('click', '.getDetailsRawBtn', function(){
        jobId = $(this).closest('tr').data('jobId');
        manageCosts();
    });

    $('#add-new-task-row').on('click', '.add-payment',function() {
        $(this).parents('tr').find('#ratePerUnit').attr('disabled', true);
        $(this).parents('tr').find('#costPerUnit').attr('disabled', true);
        $(this).parents('tr').find('.add').addClass('hidden');
        $(this).parents('tr').find('.remove').removeClass('hidden');
        var jobTaskID = $(this).parents('tr').attr('data-id');
        var taskUnitCost = accounting.unformat($(this).parents('tr').find('#costPerUnit').val());
        var taskUnitRate = accounting.unformat($(this).parents('tr').find('#ratePerUnit').val());
        var taskQty = accounting.unformat($(this).parents('tr').find('#quantity').val());
        savedJobTaskDetails[jobTaskID] = new jobTask(jobTaskID,taskQty,taskUnitCost,taskUnitRate);
        savedJobTaskDetailsObject[jobTaskID] = new jobTask(jobTaskID,taskQty,taskUnitCost,taskUnitRate);
        setNormalProducts(jobTaskProductDetails[jobTaskID],jobTaskID); 

    });

    $('#add-new-material-row').on('click', '.add-payment', function() {
        $(this).parents('tr').find('#unitPrice').attr('disabled', true);
        var jbTaskID = $(this).parents('tr').attr('data-taskid');
        if (jbTaskID != 0 ) {
            $(this).parents('tr').find('.edit-item').removeClass('hidden');
        } else {
            $(this).parents('tr').find('.remove-item').removeClass('hidden');
        } 
        $(this).parents('tr').find('.add-item').addClass('hidden');
        var locationProdID = $(this).parents('tr').find('#itemCode').attr('data-id');
        var jbProID = $(this).parents('tr').attr('data-id');
        var jbProQty = accounting.unformat($(this).parents('tr').find('#soldQty').val());
        var jbProUPrice = accounting.unformat($(this).parents('tr').find('#unitPrice').val());
        savedJobProductDetails[jbProID] = new jobTaskProduct(jbTaskID, jbProID, jbProQty, jbProUPrice, locationProdID);
    });

    $('#add-new-material-row').on('click', '.remove-payment,.edit-payment', function() {
        $(this).parents('tr').find('#unitPrice').attr('disabled', false);
        $(this).parents('tr').find('.add-item').removeClass('hidden');
        $(this).parents('tr').find('.remove-item').addClass('hidden');
        $(this).parents('tr').find('.edit-item').addClass('hidden');
        var id = $(this).parents('tr').attr('data-id');
        delete(savedJobProductDetails[id]);
    });



    $('#add-new-task-row').on('click', '.remove', function() {
        var id = $(this).parents('tr').attr('data-id');
        var deleteFlag = true;
        $.each(savedJobProductDetails, function(index, value) {
            if (value != undefined && value.jobTaskID == id) {
                delete(savedJobProductDetails[index]);
                $('#add-new-material-row').find('[data-taskid = "'+id+'"]').remove();;
                
            }
            
        });
        if (deleteFlag) {
            $(this).parents('tr').find('#ratePerUnit').attr('disabled', false);
            $(this).parents('tr').find('.add').removeClass('hidden');
            $(this).parents('tr').find('.remove').addClass('hidden');
            delete(savedJobTaskDetails[id]);
        }
    });

    $('#add-other-cost-row').on('click', '.add-cost',function(){
        var costType = $(this).parents('tr').find('#costType').val();
        var costTypeName = $(this).parents('tr').find('#costType').text();
        var unitCost = $(this).parents('tr').find('#other-cost-unit-price').val();
        var qty = $(this).parents('tr').find('#other-cost-unit').val();
        var tbody = $('#add-other-cost-row');
        
        if (costType == '' || costType == 0) {
            p_notification(false, eb.getMessage('ERR_SELECT_COST_TYPE'));
        } else if (isNaN(unitCost)) {
            p_notification(false, eb.getMessage('ERR_WRONG_DATA_TYPE'));
            $(this).parents('tr').find('#other-cost-unit-price').val(accounting.formatMoney(0));
        } else if (isNaN(qty)) {
            p_notification(false, eb.getMessage('ERR_WRONG_DATA_TYPE'));
            $(this).parents('tr').find('#other-cost-unit').val(accounting.formatMoney(0));
        } else if (parseFloat(unitCost) < 0) {
            p_notification(false, eb.getMessage('ERR_NEGATIVE_VALUE'));
            $(this).parents('tr').find('#other-cost-unit-price').val(accounting.formatMoney(0));
        } else if (parseFloat(qty) < 0) {
            p_notification(false, eb.getMessage('ERR_NEGATIVE_VALUE'));
            $(this).parents('tr').find('#other-cost-unit').val(accounting.formatMoney(0));
        } else if (savedOtherJobCostDetails[costType] != undefined) {
            p_notification(false, eb.getMessage('ERR_SAME_VALUE'));
            $(this).parents('tr').find('#other-cost-unit').val(accounting.formatMoney(0));
        } else {
            var newRow = $(".add-other-cost-original").clone().attr('data-id',costType).addClass('addedOtherCost').appendTo(tbody);
            $('#selectedType', newRow).val(costTypeName).attr('disabled', true);
            $('.selected-cost', newRow).removeClass('hidden');
            $('.remove-normal-cost', newRow).removeClass('hidden');
            $('.select-cost', newRow).addClass('hidden');
            $('.add-normal-cost', newRow).addClass('hidden');
            $('#other-cost-unit-price', newRow).val(accounting.formatMoney(unitCost)).attr('disabled', true);
            $('#other-cost-unit', newRow).val(accounting.formatMoney(qty)).attr('disabled', true);
            $('#other-cost-total', newRow).val(accounting.formatMoney(parseFloat(unitCost)*parseFloat(qty))).attr('disabled', true);
            savedOtherJobCostDetails[costType] = new jobOtherCost(jobId,costType, unitCost,qty);
            newRow.removeClass('add-other-cost-original');
        
            // clear original columns
            $(this).parents('tr').find('#costType').val('');
            $(this).parents('tr').find('#other-cost-unit-price').val('');
            $(this).parents('tr').find('#other-cost-unit').val('');
            $(this).parents('tr').find('#other-cost-total').val('');
            $(this).parents('tr').find('#costType').val(0).trigger('change').empty().selectpicker('refresh');
        }

    });

    $('#add-other-cost-row').on('click', '.remove-cost', function() {
        var costType = $(this).parents('tr').find('#costType').val();
        delete(savedOtherJobCostDetails[costType]);
        $(this).parents('tr').remove();
    });

    $('#save-cost').on('click', function() {

     formData = {
            'jobCost' : savedOtherJobCostDetails,
            'jobProduct' : savedJobProductDetails,
            'jobTask' : savedJobTaskDetailsObject,
            'jobID' : jobId
        };
        if (validationCostSave(formData)) {
            eb.ajax({
                url: BASE_URL + '/api/cost/save',
                method: 'post',
                data: formData,
                dataType: 'json',
                success: function(respond) {
                    p_notification(respond.status,respond.msg);
                    if (respond.status) {
                        showInvoicePreview(BASE_URL + '/invoice/invoiceView/' + respond.data.salesInvoiceID);
                        window.setTimeout(function() {
                            location.reload();
                        }, 5000);
                    } 
                    
                }
            });
        }
    });

    function validationCostSave(data)
    {
        if ((!Array.isArray(data.jobCost) || !data.jobCost.length) 
                && (!Array.isArray(data.jobProduct) || !data.jobProduct.length)
                    && (Object.keys(data.jobTask).length == 0)) {
            p_notification(false, eb.getMessage('NO DATA_IN_ARRAY'));
            return false; 
        } else {
            return true;
        }
    }

    $('#add-new-material-row').on('change', '#unitPrice', function() {
        var unitVal = $(this).val();
        if (isNaN(unitVal)) {
            p_notification(false, eb.getMessage('ERR_WRONG_DATA_TYPE'));
            $(this).val('0');
            unitVal = 0;
        }
        var unitQty = $(this).parents('tr').find('#soldQty').val();
        var total = parseFloat(unitQty) * parseFloat(unitVal);
        $(this).val(accounting.formatMoney(unitVal));
        $(this).parents('tr').find('#totalValue').val(accounting.formatMoney(total));
    });

    $('#add-new-task-row').on('change', '#ratePerUnit', function() {
        var unitPrice = $(this).val();
        if (isNaN(unitPrice)) {
            p_notification(false, eb.getMessage('ERR_WRONG_DATA_TYPE'));
            $(this).val('0');
            unitPrice = 0;
        }
        var unitQty = $(this).parents('tr').find('#quantity').val();
        var total = parseFloat(unitQty) * parseFloat(unitPrice);
        $(this).val(accounting.formatMoney(unitPrice));
        $(this).parents('tr').find('#totalRate').val(accounting.formatMoney(total));
    });

    $('#add-new-task-row').on('change', '#costPerUnit', function() {
        var unitPrice = $(this).val();
        if (isNaN(unitPrice)) {
            p_notification(false, eb.getMessage('ERR_WRONG_DATA_TYPE'));
            $(this).val('0');
            unitPrice = 0;
        }
        var unitQty = $(this).parents('tr').find('#quantity').val();
        var total = parseFloat(unitQty) * parseFloat(unitPrice);
        $(this).val(accounting.formatMoney(unitPrice));
        $(this).parents('tr').find('#totalCost').val(accounting.formatMoney(total));
    });

    $('#add-other-cost-row').on('change', '#other-cost-unit-price, #other-cost-unit', function() {
        var selectedValue = $(this).val();
        if (isNaN(selectedValue) || parseFloat(selectedValue) < 0) {
            p_notification(false, eb.getMessage('ERR_WRONG_DATA_TYPE'));
            $(this).val(accounting.formatMoney(0));
            selectedValue = 0;
        }
        var otherCostUnitPrice = 0;
        var otherCostUnitQty = 0;
        otherCostUnitPrice = $(this).parents('tr').find('#other-cost-unit-price').val();
        otherCostUnitQty = $(this).parents('tr').find('#other-cost-unit').val();

        var OtherCostTotal = parseFloat(otherCostUnitPrice) * parseFloat(otherCostUnitQty);
        $(this).parents('tr').find('#other-cost-total').val(accounting.formatMoney(OtherCostTotal));

    });

    
    function validationManageForm(data) {
    	if (!data.jobId) {
	        p_notification(false, eb.getMessage('ERR_SEL_RM_JOB'));
    		return false;
    	} else {
    		return true;
    	}
    }

    function setJobTasks(data) {
        $('#job-task').removeClass('hidden');
        $("#job-task tbody .addedJobTasks").remove();
        $('#cost-of-miscellaneous').removeClass('hidden');
        $('#productPanel_footer').removeClass('hidden');
        var tbody = $('#add-new-task-row');
        $.each(data, function(index, value) {
            var newRow = $(".add-task-row").clone().removeClass('hidden').attr('data-id',index).addClass('addedJobTasks').appendTo(tbody);
            $('#taskCode', newRow).val(value.taskCode);
            $('#taskName', newRow).val(value.taskName);
            $('#ratePerUnit', newRow).val(value.jobTaskUnitRate ? accounting.formatMoney(value.jobTaskUnitRate) : 0);
            $('#costPerUnit', newRow).val(value.jobTaskUnitCost ? accounting.formatMoney(value.jobTaskUnitCost) : 0);
            $('#uom', newRow).val(value.uomName);
            $('#quantity', newRow).val(value.jobTaskQty ? accounting.formatMoney(value.jobTaskQty) : 0);
            $('#totalCost', newRow).val(accounting.formatMoney(parseFloat(value.jobTaskUnitCost)* parseFloat(value.jobTaskQty)));
            $('#totalRate', newRow).val(accounting.formatMoney(parseFloat(value.jobTaskUnitRate)* parseFloat(value.jobTaskQty)));
            newRow.removeClass('add-task-row');
            newRow.removeClass('sample');
        });
    }

    function setNormalProducts(data,jobTaskID) {
        var productViewFlag = false;
        if (data != undefined) {
            $.each(data, function(index, val) {
                if (index != "") {
                    productViewFlag = true;                
                }
            });
            
        }

        if (productViewFlag) {
            $('#cost-of-metarials').removeClass('hidden');
            var tbody = $('#add-new-material-row');
            $.each(data, function(index, value) {
                var newRow = $(".add-material-row").clone().removeClass('hidden').attr('data-id',index).addClass('addedNormalTask').appendTo(tbody);
                $('#itemCode', newRow).val(value.productCodeName).attr('data-id', value.locationProductID);
                $('#uom', newRow).val(value.uom);
                if (jobTaskID != 0 ){
                    $('#unitPrice', newRow).attr('disabled', true);
                    $('.edit-item', newRow).removeClass('hidden');    
                    $('.remove-item', newRow).addClass('hidden');    
                    $('.add-item', newRow).addClass('hidden');  
                    savedJobProductDetails[value.jobProductID] = new jobTaskProduct(value.jobTaskID, value.jobProductID, value.jobProductRemInvoicedQty, value.jobProductUnitPrice, value.locationProductID);  
                } else {
                    $('#unitPrice', newRow).attr('disabled', false);    
                }
                $('#unitPrice', newRow).val(value.jobProductUnitPrice ? accounting.formatMoney(value.jobProductUnitPrice) : 0);
                $('#soldQty', newRow).val(value.jobProductRemInvoicedQty);
                $('#totalValue', newRow).val(accounting.formatMoney(parseFloat(value.jobProductUnitPrice)* parseFloat(value.jobProductRemInvoicedQty)));
                newRow.attr('data-taskid', jobTaskID);
                newRow.removeClass('add-material-row');
                newRow.removeClass('sample');
            });   
        }
    }

});

