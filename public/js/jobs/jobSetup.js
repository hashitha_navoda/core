var jobTypeID = '';
var editMode = false;
$(function() {
    $('#create-job-type-form').on('submit', function(e) {
        e.preventDefault();
        var formData = {
            jobTypeCode: $('#jobTypeCode').val(),
            jobTypeName: $('#jobTypeName').val(),
            editMode: editMode,
            jobTypeID: jobTypeID,
        };
        if (validateFormData(formData)) {
            eb.ajax({
                url: BASE_URL + '/job_setup_api/add',
                method: 'post',
                data: formData,
                dataType: 'json',
                success: function(data) {
                    p_notification(data.status, data.msg);
                    if (data.status) {
                        window.setTimeout(function() {
                            location.reload();
                        }, 1000);
                    } else {
                        if (data.data == "JTCERR") {
                            $('#jobTypeCode').focus();
                        }
                    }
                }
            });
        }
    });

    $('#job-type-search').on('submit', function(e) {
        e.preventDefault();
        var formData = {
            jobTypeSearchKey: $('#job-type-search-keyword').val(),
        };
        if (formData.jobTypeSearchKey != '') {
            searchValue(formData);
        } else {
            p_notification(false, eb.getMessage('ERR_JBTYPE_SEARCH_KEY'));
        }
    });

    $('#job-type-search').on('click', '.reset', function(e) {
        e.preventDefault();
        searchValue('');
        $('#job-type-search-keyword').val('');
    });

    $('#job-type-list').on('click', 'a.edit', function(e) {
        e.preventDefault();
        jobTypeID = $(this).parents('tr').data('jobtypeid');
        $('#jobTypeCode').val($(this).parents('tr').find('.JTC').text()).attr('disabled', true);
        $('#jobTypeName').val($(this).parents('tr').find('.JTN').text());
        $('.updateTitle').removeClass('hidden');
        $('.addTitle').addClass('hidden');
        $('.updateDiv').removeClass('hidden');
        $('.addDiv').addClass('hidden');
        editMode = true;
    });

    $('#job-type-list').on('click', 'a.status', function() {
        var msg;
        var status;
        var flag = true;
        var jobTypeID = $(this).parents('tr').data('jobtypeid');
        var currentDiv = $(this).contents();
        if ($(this).children().hasClass('fa-check-square-o')) {
            msg = 'Are you sure you want to Deactivate this job Type';
            status = '0';
        }
        else if ($(this).children().hasClass('fa-square-o')) {
            msg = 'Are you sure you want to Activate this job type';
            status = '1';
        }
        activeInactiveType(jobTypeID, status, currentDiv, msg, flag);
    });

    $('.cancel').on('click', function(e) {
        e.preventDefault();
        jobTypeID = '';
        $('#jobTypeCode').val('').attr('disabled', false);
        $('#jobTypeName').val('');
        $('.updateTitle').addClass('hidden');
        $('.addTitle').removeClass('hidden');
        $('.updateDiv').addClass('hidden');
        $('.addDiv').removeClass('hidden');
        editMode = false;
    });

    $('#job-type-list').on('click', 'a.delete', function(e) {
        e.preventDefault();
        $('#jobTypeCode').val('').attr('disabled', false);
        $('#jobTypeName').val('');
        $('.updateTitle').addClass('hidden');
        $('.addTitle').removeClass('hidden');
        $('.updateDiv').addClass('hidden');
        $('.addDiv').removeClass('hidden');
        editMode = false;

        var jobTypeID = $(this).parents('tr').data('jobtypeid');
        var currentDiv = $(this).parents('tr').find('a.status').contents();
        var jobTypeCode = $(this).parents('tr').find('.JTC').text();
        var flag = false;
        var msg = 'This job type cannot be deleted. Do you want to de-activate this job type';
        bootbox.confirm('Are you sure you want to delete this Job Type?', function(result) {
            if (result === true) {
                eb.ajax({
                    url: BASE_URL + '/job_setup_api/deleteJobTypeByJobTypeID',
                    method: 'post',
                    data: {
                        jobTypeID: jobTypeID,
                        jobTypeCode: jobTypeCode,
                    },
                    dataType: 'json',
                    success: function(data) {
                        if (data.status == true) {
                            p_notification(data.status, data.msg);
                            window.location.reload();
                        }
                        else if (data.status == false && data.data == 'deactive') {
                            activeInactiveType(jobTypeID, '0', currentDiv, msg, flag);
                        } else {
                            p_notification(data.status, data.msg);
                        }
                    }
                });
            }
        });

    });

    function validateFormData(formData) {
        if (formData.jobTypeCode == "") {
            p_notification(false, eb.getMessage('ERR_JBTYPE_CODE_CNT_BE_NULL'));
            $('#jobTypeCode').focus();
            return false;
        } else if (formData.jobTypeName == '') {
            p_notification(false, eb.getMessage('ERR_JBTYPE_NAME_CNT_BE_NULL'));
            $('#jobTypeName').focus();
            return false;
        }
        return true;
    }
    function searchValue(formData) {
        eb.ajax({
            url: BASE_URL + '/job_setup_api/getJobTypesBySearchKey',
            method: 'post',
            data: formData,
            dataType: 'json',
            success: function(data) {
                if (data.status) {
                    $("#job-type-list").html(data.html);
                } else {
                    p_notification(data.status, data.msg);
                }
            }
        });
    }

    /**
     *use to change active state
     */
    function activeInactiveType(type, status, currentDiv, msg, flag) {
        bootbox.confirm(msg, function($result) {
            if ($result == true) {
                eb.ajax({
                    url: BASE_URL + '/job_setup_api/changeStatusID',
                    method: 'post',
                    dataType: 'json',
                    data: {
                        'jobTypeID': type,
                        'status': status,
                    },
                    success: function(data) {
                        if (data.status) {
                            p_notification(data.status, data.msg);
                            if (currentDiv.hasClass('fa-square-o') && flag) {
                                currentDiv.addClass('fa-check-square-o');
                                currentDiv.removeClass('fa-square-o');
                            }
                            else {
                                currentDiv.addClass('fa-square-o');
                                currentDiv.removeClass('fa-check-square-o');
                            }
                        } else {
                            p_notification(data.status, data.msg);
                        }
                    }
                });
            }
        });
    }
});