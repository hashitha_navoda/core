var vehicleTypeID = 0;
var taskID = 0;
var dirtyFlag = false;
var serviceList = [];
var serviceListID = 0;

$(document).ready(function() {

    loadDropDownFromDatabase('/api/resources/searchVehicleTypesForDropdown', "", 0, '#vehicleType');
    $('#vehicleType').on('change', function() {
        if (!($(this).val() == null || $(this).val() == 0) && vehicleTypeID != $(this).val()){
            if (dirtyFlag) {
                p_notification(false, eb.getMessage('ERR_JOB_CRD_CHG_WITHOUT_SAVE'));
                $(this).val(0);
                $(this).selectpicker('refresh');
            } else {
                vehicleTypeID = $(this).val();
                dirtyFlag = false;
                requestServiceList(vehicleTypeID);
                requestJobCardList(vehicleTypeID);
            }
        }
    });

    $('.saveAllService').on('click', function(event) {
        event.preventDefault();

        var incentiveData = {};
        $.each($('#serviceListTbody')[0].children, function(index, val) {
            var classList = val.classList.value.split(" ");
            if (classList.includes('serviceRow')) {
                incentiveData[val.id] = {
                        'incentiveRate' : $('.incentiveRate'+val.id).val(),
                        'incentiveFor': $('.incentiveFor'+val.id).val(),
                        'serviceRate':  $('#serviceRates'+val.id).val(),
                        'subTaskRate':{},
                        'subTaskRateEnable': false
                };

                var subTaskRateData = {};
                $.each($('.sub-task-row'+val.id+' tbody')[0].children, function(index, value) {
                    if (value.children[2].children[0].value != "") {
                        subTaskRateData[value.id] = value.children[2].children[0].value;
                    }
                });
                incentiveData[val.id].subTaskRate = subTaskRateData;
                if ($('#subTaskBtn'+val.id+' .serviceSubTaskEnableIcon').hasClass('fa-check-square-o')) {
                    incentiveData[val.id].subTaskRateEnable = true;
                }
            }
        });

        eb.ajax({
            type: 'POST',
            url: BASE_URL + '/api/incentives/saveServiceIncentives',
            data: {
                vehicleTypeID: $('#vehicleType').val(),
                incentiveData: incentiveData,
            },
            success: function(respond) {
                p_notification(respond.status, respond.msg);
                if (respond.status) {
                    dirtyFlag = false;
                }
            }
        });
    });

    function requestServiceList(vehicleTypeID) {
        eb.ajax({
            type: 'POST',
            url: BASE_URL + '/api/incentives/getServiceList',
            data: {
                vehicleTypeID: vehicleTypeID
            },
            success: function(respond) {
                if (respond.status) {
                    serviceList = respond.data;
                    serviceListID = vehicleTypeID;
                    // refreshServiceList();
                    $('#serviceList').html(respond.html);
                } else {
                    p_notification(respond.status, respond.msg);
                }
            }
        });
    }


    function requestJobCardList(vehicleTypeID) {
        eb.ajax({
            type: 'POST',
            url: BASE_URL + '/api/incentives/getJobCardList',
            data: {
                vehicleTypeID: vehicleTypeID
            },
            success: function(respond) {
                if (respond.status) {
                    jobCardList = respond.data;
                    serviceListID = vehicleTypeID;
                    // refreshServiceList();
                    $('#jobCardList').html(respond.html);
                } else {
                    p_notification(respond.status, respond.msg);
                }
            }
        });
    }

    $('td.job-crd-service').find("table").hide();
    $('td.sub-task').find("table").hide();
    $('.showSubTask').on('click', function(event) {
        event.preventDefault();
        var currentSpan = $(event.target).closest('span');
        var selectedTaskId = $(event.target).closest('tr').attr('id');
            console.log(selectedTaskId);
        if (currentSpan.hasClass('fa-square-o')) {
            $('#serviceRates'+selectedTaskId).attr('disabled', true);
            currentSpan.removeClass('fa-square-o');
            currentSpan.addClass('fa-check-square-o');
            $(event.target).closest("tr").next().find("table").slideToggle();
        } else {
            // console.log('vvvvvvvvvvvvvv');
            $('#serviceRates'+selectedTaskId).attr('disabled', false);
            currentSpan.removeClass('fa-check-square-o');
            currentSpan.addClass('fa-square-o');
            $(event.target).closest("tr").next().find("table").slideUp();
        }
        return false;
    })

    $('.subTskRate').on('focusout', function(event) {
        event.preventDefault();
        var serviceID = $(this).attr('data-id');
        calculateServiceRateBySubTasks(serviceID);
    });

    function calculateServiceRateBySubTasks(serviceID) {
        var serviceRate = 0;
        $.each($('.sub-task-row'+serviceID+' tbody')[0].children, function(index, val) {
            if (val.children[2].children[0].value != "") {
                serviceRate += parseFloat(val.children[2].children[0].value);
            }

        });
        $('#serviceRates'+serviceID).val(serviceRate);
    }
});
