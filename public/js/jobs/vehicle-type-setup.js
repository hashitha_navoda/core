var vehicleModelList = [];
var vehicleSubModelList = [];
var linedAddedFlag = false;
var expandId = null;
$(document).ready(function() {
	var $jobVehicleTable = $("#jobVehicleTable");
	var $viewVehicleTable = $("#viewVehicleTable");
	var addVehicleModels = $('#addVehicleModels');
	var addVehicleSubModels = $('#addVehicleSubModels');
	var vehicleModelTbody = $('#vehicleModelTbody');
	var vehicleSubModelTbody = $('#vehicleSubModelTbody');

	$('#addVehicleModelsBtn').on('click', function(e) {
		addVehicleModels.modal('show');
	});

	$jobVehicleTable.on('click', 'button.addVehicalModel', function(e) {
		var tr = $(this).parents('tr');
		if (tr.hasClass('saved')){
			var trID = tr.attr('id');
			var trArr = trID.split("_");
			expandId = trArr[1];
			vehicleSubModelList = vehicleModelList[trArr[1]]['vehicleSubModelList'];
			refreshVehicleSubModelListView();
		} else {
			$("#addVehicleSubModels #vehicleSubTable #jobVehicleSubTable tbody .saved").remove();
			vehicleSubModelList = [];
			expandId = null;
		}
		// if (linedAddedFlag) {
		// 	$("#addVehicleSubModels #vehicleSubTable #jobVehicleSubTable tbody .saved").remove();
  //   		vehicleSubModelList = [];
		// 	linedAddedFlag = false;
		// }
        addVehicleSubModels.modal('show');
    });

	$('#add', vehicleModelTbody).on('click', function(e) {
		var tr = $(this).parents('tr');
		var name = $('#name', tr).val();
		if (name == null || name == '') {
			p_notification(false, eb.getMessage('ERR_INV_VEH_TYP_NAME'));
			return false;
		} else if (vehicleModelList.some(el => el.vehicleModelName === name)) {
			p_notification(false, eb.getMessage('ERR_DUP_VEH_MOD'));
			return false;
		} else {
			vehicleModelList.push({'vehicleModelID':'new', 'vehicleModelName':name,'vehicleSubModelList':vehicleSubModelList});
			$('#name', tr).val('');
			refreshVehicleModelListView();
			linedAddedFlag = true;
		}
	});

	$('#addSub', vehicleSubModelTbody).on('click', function(e) {
		var tr = $(this).parents('tr');
		var name = $('#name', tr).val();
		if (name == null || name == '') {
			p_notification(false, eb.getMessage('ERR_INV_VEH_TYP_NAME'));
			return false;
		} else if (vehicleSubModelList.some(el => el.vehicleSubModelName === name)) {
			p_notification(false, eb.getMessage('ERR_DUP_VEH_MOD'));
			return false;
		} else {
			vehicleSubModelList.push({'vehicleSubModelID':'new', 'vehicleSubModelName':name});
			$('#name', tr).val('');
			refreshVehicleSubModelListView();
		}
	});

	$('#name', $('#userInput', vehicleModelTbody)).keyup(function(e){
	    if(e.keyCode == 13)
	    {
	        $('#add', $('#userInput', vehicleModelTbody)).trigger('click');
	    }
	});

	$('#btnAddVehicleModels', addVehicleModels).on('click', function(e) {
		if(vehicleModelList.length > 0) {
			addVehicleModels.modal('hide');
		} else {
			p_notification(false, eb.getMessage('ERR_EMPTY_VEH_MOD'));
		}
	});

	$('#btnAddVehicleSubModels', addVehicleSubModels).on('click', function(e) {
		if(vehicleSubModelList.length > 0) {
			addVehicleSubModels.modal('hide');
		} else {
			p_notification(false, eb.getMessage('ERR_EMPTY_VEH_MOD'));
		}
	});

	vehicleModelTbody.on('click', '.delete', function(e) {
		var tr = $(this).parents('tr');
		var id = $('#id', tr).val();
		var name = $('#name', tr).val();
	    vehicleModelList = $.grep(vehicleModelList, function(data, index) {
		   return data.vehicleModelName != name;
		})
	    refreshVehicleModelListView();
	});

	vehicleSubModelTbody.on('click', '.delete', function(e) {
		var tr = $(this).parents('tr');
		var id = $('#subId', tr).val();
		var name = $('#subName', tr).val();
	    vehicleSubModelList = $.grep(vehicleSubModelList, function(data, index) {
		   return data.vehicleSubModelName != name;
		})
		
	    if (expandId != null) {
	    	vehicleModelList[expandId]['vehicleSubModelList'] = vehicleSubModelList;
	    }
	    refreshVehicleSubModelListView();
	});

	$('#addVehicleTypeForm').on('submit', function(e){
		e.preventDefault();
		var form = $(this);
		var mode = form.attr('mode');
		var vehicleTypeName = form.find('#vehicleType').val();

		if(vehicleTypeName == '' || vehicleTypeName == null){
			p_notification(false, eb.getMessage('ERR_INV_VEH_TYP_NAME'));
			return false;
		}
		if(vehicleModelList.length <= 0){
			p_notification(false, eb.getMessage('ERR_EMPTY_VEH_MOD'));
			return false;
		}

		if(mode == 'add'){
			var data = {
				'vehicleTypeName' : vehicleTypeName,
				'vehicleModel' : vehicleModelList,
			};
			var url = BASE_URL + '/api/resources/save-vehicle-type';
		} else {
			var vehicleTypeID = form.find('#vehicleTypeID').val();
			var data = {
				'vehicleTypeID' : vehicleTypeID,
				'vehicleTypeName' : vehicleTypeName,
				'vehicleModel' : vehicleModelList,
			};
			var url = BASE_URL + '/api/resources/update-vehicle-type';
		}

		eb.ajax({
		    type: 'POST',
		    url: url,
		    data: data,
		    success: function(respond) {
                p_notification(respond.status, respond.msg);
		    	if (respond.status) {
		    		window.setTimeout(function() {
		    		    window.location.assign(BASE_URL + "/resources/vehicles")
		    		}, 1000);
		    	}
		    },
		    async: false
		});
	});

	$('.reset', $('#addVehicleTypeForm')).on('click', function(){
		var $form = $('#addVehicleTypeForm');
		$form.attr('mode', 'add');
		$('#addFromTitle', $form).removeClass('hidden'); 
		$('#updateFormTitle', $form).addClass('hidden'); 
		$('#addFormBtn', $form).removeClass('hidden'); 
		$('#updateFormBtn', $form).addClass('hidden'); 
		$('#vehicleTypeID', $form).val('null'); 
		$('#vehicleType', $form).val(''); 
		vehicleModelList = [];
		refreshVehicleModelListView();
	});

	$('#vehicle-type-search').on('submit', function(e){
		e.preventDefault();
		var key = $('#vehicle-type-search-keyword').val();
		var formData = {
		    searchKey: key,
		};
		eb.ajax({
            url: BASE_URL + '/api/resources/search-vehicle-types',
            method: 'post',
            data: formData,
            dataType: 'json',
            success: function(data) {
                if (data.status) {
                    $("#vehicle-type-list").html(data.html);
                } else {
                    p_notification(data.status, data.msg);
                }
            }
        });
	});

	$('#searchVehicleTypeReset').on('click', function(e){
		$('#vehicle-type-search-keyword').val('');
		$('#vehicle-type-search').trigger('submit');
	});

	function refreshVehicleModelListView(){
		var model = $('#addVehicleModels');
		var tbody = $('#vehicleModelTbody', model);
		$('tr.saved', tbody).remove();
		$.each(vehicleModelList, function(index, value) {
			var $newTr = $($('tr.sample', tbody).clone()).appendTo(tbody);
			$('#name', $newTr).val(value.vehicleModelName);
			$('#id', $newTr).val(value.vehicleModelID);
			$newTr.removeClass('hidden sample');
			var randNum = Math.floor((Math.random() * 10000) + 1);
			var trId = 'vmodel'+'_'+index;
			$newTr.addClass('saved').attr('id', trId);
		});
		$countView = $('#vehicleModelCount');
		if(vehicleModelList.length > 0){
			$countView.html(' ( ' + vehicleModelList.length + ' vehicle makes added )');
		} else {
			$countView.html(' ( No vehicle makes added )');
		}
	}

	function refreshVehicleSubModelListView(){
		var model = $('#addVehicleSubModels');
		var tbody = $('#vehicleSubModelTbody', model);
		$('tr.saved', tbody).remove();
		$.each(vehicleSubModelList, function(index, value) {
			var $newTr = $($('tr.subSample', tbody).clone()).appendTo(tbody);
			$('#subName', $newTr).val(value.vehicleSubModelName);
			$('#subId', $newTr).val(value.vehicleSubModelID);
			$newTr.removeClass('hidden subSample');
			$newTr.addClass('saved').attr('id', 'saved');
		});
	}

	$vehicleTypeList = $('#vehicle-type-list');
	
	$vehicleTypeList.on('click', '.edit', function(e) {
		e.preventDefault();
		var vehicleTypeID = $(this).parents('tr').attr('id');
		eb.ajax({
		    type: 'POST',
		    url: BASE_URL + '/api/resources/view-vehicle-type',
		    data: {'vehicleTypeID':vehicleTypeID , 'getVehicleSubModel':true},
		    success: function(respond) {
		    	if (respond.status) {
		    		loadEditForm(respond.data);
		    	}
		    },
		    async: false
		});
	});

	$vehicleTypeList.on('click', '.view', function(e) {
		e.preventDefault();
		var vehicleTypeID = $(this).parents('tr').attr('id');
		eb.ajax({
		    type: 'POST',
		    url: BASE_URL + '/api/resources/view-vehicle-type',
		    data: {'vehicleTypeID':vehicleTypeID, 'getVehicleSubModel':false},
		    success: function(respond) {
		    	if (respond.status) {
		    		loadViewModal(respond.data);
		    	}
		    },
		    async: false
		});
	});

	$vehicleTypeList.on('click', '.status', function(e) {
		e.preventDefault();
		var $tr = $(this).parents('tr');
		var vehicleTypeID = $tr.attr('id');
		eb.ajax({
		    type: 'POST',
		    url: BASE_URL + '/api/resources/change-status-vehicle-type',
		    data: {'vehicleTypeID':vehicleTypeID},
		    success: function(respond) {
                p_notification(respond.status, respond.msg);
		    	if (respond.status) {
		    		if (respond.data['vehicleTypeStatus'] == '1') {
			    		$('#statusSpan', $tr).removeClass('fa-square-o').addClass('fa-check-square-o');
		    		} else {
			    		$('#statusSpan', $tr).removeClass('fa-check-square-o').addClass('fa-square-o');
		    		}
		    	}
		    },
		    async: false
		});
	});

	$vehicleTypeList.on('click', '.delete', function(e) {
		e.preventDefault();
		var $tr = $(this).parents('tr');
		var vehicleTypeID = $tr.attr('id');
		eb.ajax({
		    type: 'POST',
		    url: BASE_URL + '/api/resources/delete-vehicle-type',
		    data: {'vehicleTypeID':vehicleTypeID},
		    success: function(respond) {
		        p_notification(respond.status, respond.msg);
		    	if (respond.status) {
		    		$tr.remove();
		    	}      
		    },
		    async: false
		});
	});

	function loadEditForm(data){
		var $form = $('#addVehicleTypeForm');
		$form.attr('mode', 'edit');
		$('#addFromTitle', $form).addClass('hidden'); 
		$('#updateFormTitle', $form).removeClass('hidden'); 
		$('#addFormBtn', $form).addClass('hidden'); 
		$('#updateFormBtn', $form).removeClass('hidden'); 
		$('#vehicleTypeID', $form).val(data.vehicleTypeID); 
		$('#vehicleType', $form).val(data.vehicleTypeName); 
		vehicleModelList = data.vehicleModelData;
		refreshVehicleModelListView();
	}

	function loadViewModal(data){
		var $model = $('#viewVehicleModels');
		var tbody = $('#viewVehicleModelTbody', $model);
		$('tr.saved', tbody).remove();
		$.each(data.vehicleModelData, function(index, value) {
			var $newTr = $($('tr.subSample', tbody).clone()).appendTo(tbody);
			$('#subModelname', $newTr).text(value['vehicleModelName']);
			$('#subId', $newTr).val(value['vehicleModelID']);
			$newTr.removeClass('hidden subSample');
			$newTr.addClass('saved').attr('id', 'saved');
		});
		$model.modal('show');
	}

	$viewVehicleTable.on('click', 'button.viewVehiclSubModel', function(e) {
		var vehiclModelID = $('#subId', $(this).parent()).val();
		eb.ajax({
		    type: 'POST',
		    url: BASE_URL + '/api/resources/view-vehicle-type',
		    data: {'vehicleTypeID':null,'vehiclModelID':vehiclModelID, 'getVehicleSubModel':false},
		    success: function(respond) {
		    	if (respond.status) {
		    		loadViewSubModal(respond.data.vehicleSubModelData);
		    	}
		    },
		    async: false
		});
    });

    function loadViewSubModal(data){
		var $model = $('#viewVehicleSubModels');
		var tbody = $('#viewVehicleSubModelTbody', $model);
		$('tr.saved', tbody).remove();
		$.each(data, function(index, value) {
			var $newTr = $($('tr.subSample', tbody).clone()).appendTo(tbody);
			$('#subModelname', $newTr).text(value['vehicleSubModelName']);
			$newTr.removeClass('hidden subSample');
			$newTr.addClass('saved').attr('id', 'saved');
		});
		$model.modal('show');
	}
});
