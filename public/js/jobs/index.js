$(document).ready(function() {
    $('#business-type').on('click', function(){
        var docType = {
            id: $('#businessType').val(),
        };
        eb.ajax({
            type: 'POST',
            url: BASE_URL + '/service-job-api/initiate-business',
            data: docType,
            success: function(respond) {
                if(respond.status) {
                    if ($('#businessType').val() == 3) {
                        window.location.assign(BASE_URL + "/tournament/list");
                    } else {
                        window.location.assign(BASE_URL + "/service-job-dashboard/index");
                    }
                }
            }
        });
    });

    $('#business-type-next').on('click', function(){
	var docType = {
	    id: $('#businessType').val(),
            satge: 1
	};
	eb.ajax({
            type: 'POST',
            url: BASE_URL + '/api/project/initiate-business',
            data: docType,
            success: function(respond) {
                if(respond.status) {
                    window.location.reload();
                }
            }
        });
    });

    $('#businessType').on('change',function(){
        if ($(this).val() == 1) {
            $('#business-type').addClass('hidden');
            $('#business-type-next').removeClass('hidden');
        } else {
            $('#business-type-next').addClass('hidden');
            $('#business-type').removeClass('hidden');
        }
    });
});
    