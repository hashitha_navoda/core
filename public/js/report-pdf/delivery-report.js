$(document).ready(function() {
    var reportType = 'dailyDeliveryValues';
    var isAllCustomers = false;
    var isAllItems = false;
    var isAllDeliveryNotes = false;
    var isTaxEnabled = false;
    var isSortByCustomer = false;

    ///////DatePicker for two text Filed\\\\\\\
    var checkin = $('#fromDate').datepicker({
    }).on('changeDate', function(ev) {

        if (ev.date.valueOf() < checkout.date.valueOf()) {
            var newDate = new Date(ev.date);
            checkout.setValue(newDate);
        }
        checkin.hide();
        $('#toDate')[0].focus();
    }).data('datepicker');
    var checkout = $('#toDate').datepicker({
        onRender: function(date) {
            return date.valueOf() <= checkin.date.valueOf();
        }
    }).on('changeDate', function(ev) {
        checkout.hide();
    }).data('datepicker');
    $('#user-selection').addClass('hidden');
    $('#dailyDeliveryValues').click(function() {
        reportType = 'dailyDeliveryValues';
        $('#user-selection').addClass('hidden');
        $('#dailyDeliveryReport').addClass('hidden');
        $('#item_div,#customer_div,#delivery_note_div,#tax_div,#location_div').addClass('hidden');
    });
    $('#dailyDeliveryItems').click(function() {
        reportType = 'dailyDeliveryItems';
        $('#user-selection').addClass('hidden');
        $('#dailyDeliveryReport').addClass('hidden');
        $('#item_div,#customer_div,#delivery_note_div,#tax_div,#location_div').addClass('hidden');
    });
    $('#notInvoiceDeliveryItems').click(function() {
        reportType = 'notInvoiceDeliveryItems';
        $('#user-selection').addClass('hidden');
        $('#dailyDeliveryReport').addClass('hidden');
        $('#item_div,#customer_div,#delivery_note_div,#tax_div,#location_div').addClass('hidden');
    });
    $('#invDelinoteVal').click(function() {
        reportType = 'invDelinoteVal';
        $('#user-selection').addClass('hidden');
        $('#dailyDeliveryReport').addClass('hidden');
        $('#item_div,#customer_div,#delivery_note_div,#tax_div,#location_div').addClass('hidden');
    });

    $('#invDelinoteItem').click(function() {
        reportType = 'invDelinoteItem';
        $('#dailyDeliveryReport').addClass('hidden');
        $('#user-selection').addClass('hidden');
        $('#item_div,#customer_div,#delivery_note_div,#tax_div,#location_div').addClass('hidden');
    });
    $('#notInvDelinoteVal').click(function() {
        reportType = 'notInvDelinoteVal';
        $('#dailyDeliveryReport').addClass('hidden');
        $('#user-selection').addClass('hidden');
        $('#item_div,#customer_div,#delivery_note_div,#tax_div,#location_div').addClass('hidden');
    });
    $('#deliveryItemDetail').click(function() {
        reportType = 'deliveryItemDetail';
        $('#dailyDeliveryReport').addClass('hidden');
        $('#item_div,#customer_div,#delivery_note_div,#tax_div,#location_div').removeClass('hidden');
        isAllDeliveryNotes = $('.isAllDeliveryNotes:checked').val();
        isAllItems = $('.isAllItems:checked').val();
        isAllCustomers = $('.isAllCustomers:checked').val();
        isTaxEnabled = $('#taxCheckBox').is(':checked');
        isSortByCustomer = $('#customerSortCheckBox').is(':checked');
        $('#user-selection').addClass('hidden');
    });
    $('#deliveryItemDetailSummary').click(function() {
        reportType = 'deliveryItemDetailSummary';
        $('#dailyDeliveryReport').addClass('hidden');
        $('#item_div,#customer_div,#delivery_note_div,#tax_div,#location_div').removeClass('hidden');
        isAllDeliveryNotes = $('.isAllDeliveryNotes:checked').val();
        isAllItems = $('.isAllItems:checked').val();
        isAllCustomers = $('.isAllCustomers:checked').val();
        isTaxEnabled = $('#taxCheckBox').is(':checked');
        isSortByCustomer = $('#customerSortCheckBox').is(':checked');
        $('#user-selection').addClass('hidden');
    });
    $('#deliveryNoteWiseInvoice').click(function() {
        reportType = 'deliveryNoteWiseInvoice';
        $('#dailyDeliveryReport').addClass('hidden');
        $('.customer-category-div').addClass('hidden');
        $('#item_div,#customer_div,#delivery_note_div,#tax_div,#location_div').addClass('hidden');
        $('#delivery_note_div').removeClass('hidden');
        isAllDeliveryNotes = $('.isAllDeliveryNotes:checked').val();
        isAllItems = $('.isAllItems:checked').val();
        $('#user-selection').addClass('hidden');
    });
    $('#editedDeliveryNote').click(function() {
        reportType = 'editedDeliveryNote';
        $('#dailyDeliveryReport').addClass('hidden');
        $('.customer-category-div').addClass('hidden');
        $('#customer_div').removeClass('hidden');
        isAllCustomers = $('.isAllCustomers:checked').val();
        $('#user-selection').removeClass('hidden');
        $('#item_div,#delivery_note_div,#tax_div,#location_div').addClass('hidden');
    });
    $('#deliveryNoteWiseSalesReturns').click(function() {
        reportType = 'deliveryNoteWiseSalesReturns';
        $('#dailyDeliveryReport').addClass('hidden');
        $('.customer-category-div').addClass('hidden');
        $('#item_div,#customer_div,#delivery_note_div,#tax_div,#location_div').addClass('hidden');
        $('#delivery_note_div').removeClass('hidden');
        isAllDeliveryNotes = $('.isAllDeliveryNotes:checked').val();
        isAllItems = $('.isAllItems:checked').val();
        $('#user-selection').addClass('hidden');
    });

    loadDropDownFromDatabase('/customerAPI/search-customers-for-dropdown', "", "withDeactivatedCustomers", '#customerName', "", true);
    loadDropDownFromDatabase('/productAPI/search-user-active-location-products-for-dropdown', "", 0, '#itemList', "", true);
    loadDropDownFromDatabase('/delivery-note-api/search-delivery-note-for-dropdown', "", 0, '#deliveryNoteNo');

    $('#viewReport').click(function() {
        if (reportType === 'dailyDeliveryValues') {
            var getpdfdataurl = BASE_URL + '/reports/get/daily-delivery/daily-delivery-value-view';
            inputs = new Array(
                    $('#fromDate').val(),
                    $('#toDate').val());
            if (validteinput(inputs)) {
                var getpdfdatarequest = eb.post(getpdfdataurl, {
                    fromDate: $('#fromDate').val(),
                    toDate: $('#toDate').val(),
                    cusCategories : $('#customerCategoryList').val(),
                });
                getpdfdatarequest.done(function(respond) {
                    if (respond.status == true) {
                        $('#dailyDeliveryReport').removeClass('hidden');
                        setContentToReport('dailyDeliveryReport', respond);
                    }
                });
            }
        } else if (reportType === 'dailyDeliveryItems') {
            var getpdfdataurl = BASE_URL + '/reports/get/daily-delivery/daily-delivery-item-view';
            inputs = new Array(
                    $('#fromDate').val(),
                    $('#toDate').val());
            if (validteinput(inputs)) {
                var getpdfdatarequest = eb.post(getpdfdataurl, {
                    fromDate: $('#fromDate').val(),
                    toDate: $('#toDate').val(),
                    cusCategories : $('#customerCategoryList').val(),
                });
                getpdfdatarequest.done(function(respond) {
                    if (respond.status == true) {
                        $('#dailyDeliveryReport').removeClass('hidden');
                        setContentToReport('dailyDeliveryReport', respond);
                    }
                });
            }
        } else if (reportType === 'invDelinoteVal') {
            var getpdfdataurl = BASE_URL + '/reports/get/daily-delivery/invoiced-delivery-note-values-view';
            inputs = new Array(
                    $('#fromDate').val(),
                    $('#toDate').val());

            if (validteinput(inputs)) {
                var getpdfdatarequest = eb.post(getpdfdataurl, {
                    fromDate: $('#fromDate').val(),
                    toDate: $('#toDate').val(),
                });
                getpdfdatarequest.done(function(respond) {
                    if (respond.status == true) {
                        $('#dailyDeliveryReport').removeClass('hidden');
                        setContentToReport('dailyDeliveryReport', respond);
                    }
                });
            }
        } else if (reportType === 'invDelinoteItem') {
            var getpdfdataurl = BASE_URL + '/reports/get/daily-delivery/invoiced-delivery-note-items-view';

            inputs = new Array(
                    $('#fromDate').val(),
                    $('#toDate').val());

            if (validteinput(inputs)) {
                var getpdfdatarequest = eb.post(getpdfdataurl, {
                    fromDate: $('#fromDate').val(),
                    toDate: $('#toDate').val(),
                });
                getpdfdatarequest.done(function(respond) {
                    if (respond.status == true) {
                        $('#dailyDeliveryReport').removeClass('hidden');
                        setContentToReport('dailyDeliveryReport', respond);
                    }
                });
            }
        }
        else if (reportType === 'notInvDelinoteVal') {
            var getpdfdataurl = BASE_URL + '/reports/get/daily-delivery/not-invoiced-delivery-note-values-view';

            inputs = new Array(
                    $('#fromDate').val(),
                    $('#toDate').val());

            if (validteinput(inputs)) {
                var getpdfdatarequest = eb.post(getpdfdataurl, {
                    fromDate: $('#fromDate').val(),
                    toDate: $('#toDate').val(),
                });
                getpdfdatarequest.done(function(respond) {
                    if (respond.status == true) {
                        $('#dailyDeliveryReport').removeClass('hidden');
                        setContentToReport('dailyDeliveryReport', respond);
                    }
                });
            }
        }
        else if (reportType === 'notInvoiceDeliveryItems') {
            var getpdfdataurl = BASE_URL + '/reports/get/daily-delivery/not-invoice-delivery-item-view';
            inputs = new Array(
                    $('#fromDate').val(),
                    $('#toDate').val());
            if (validteinput(inputs)) {
                var getpdfdatarequest = eb.post(getpdfdataurl, {
                    fromDate: $('#fromDate').val(),
                    toDate: $('#toDate').val(),
                });
                getpdfdatarequest.done(function(respond) {
                    if (respond.status == true) {
                        $('#dailyDeliveryReport').removeClass('hidden');
                        setContentToReport('dailyDeliveryReport', respond);
                    }
                });
            }
        } else if (reportType === 'deliveryItemDetail') {
            var url = BASE_URL + '/reports/get/daily-delivery/delivery-note-items-detail-report';
            var input = {};
            input.fromDate = $('#fromDate').val();
            input.toDate = $('#toDate').val();
            input.isAllCustomers = isAllCustomers;
            input.customerIds = $('#customerName').val();
            input.isAllItems = isAllItems;
            input.itemIds = $('#itemList').val();
            input.isAllDeliveryNotes = isAllDeliveryNotes;
            input.deliveryNoteIds = $('#deliveryNoteNo').val();
            input.isTaxEnabled = isTaxEnabled;
            input.isSortByCustomer = isSortByCustomer;
            input.locationIds = $('#branch-locations').val();
                   
            if (input.itemIds == null && input.isAllItems == 0) {
                input.isAllItems = "1";
                isAllItems = true;
                $('.isAllItems').val('').trigger('click');
                $('#itemList').prop('disabled', true);
            }

            if (input.customerIds == null && input.isAllCustomers == 0) {
                input.isAllCustomers = "1";
                isAllCustomers = true;
                $('.isAllCustomers').val('').trigger('click');
                $('#customerName').prop('disabled', true);
            }


            if (input.fromDate == "" && input.toDate == "") {
                input.NodateSelected = true;            
            } else {
                input.NodateSelected = false;
            }
            
            if (deliveryItemReportValidation(input)) {
                var getpdfdatarequest = eb.post(url, input);
                getpdfdatarequest.done(function(respond) {
                    if (respond.status == true) {
                        $('#dailyDeliveryReport').removeClass('hidden');
                        setContentToReport('dailyDeliveryReport', respond);
                    }
                });
            }
        } else if(reportType ==='deliveryItemDetailSummary'){
            var url = BASE_URL + '/reports/get/daily-delivery/delivery-note-items-detail-summary-report';
            var input = {};
            input.fromDate = $('#fromDate').val();
            input.toDate = $('#toDate').val();
            input.isAllCustomers = isAllCustomers;
            input.customerIds = $('#customerName').val();
            input.isAllItems = isAllItems;
            input.itemIds = $('#itemList').val();
            input.isAllDeliveryNotes = isAllDeliveryNotes;
            input.deliveryNoteIds = $('#deliveryNoteNo').val();
            input.isTaxEnabled = isTaxEnabled;
            input.isSortByCustomer = isSortByCustomer;
            input.locationIds = $('#branch-locations').val();           
        
            if (input.itemIds == null && input.isAllItems == 0) {
                input.isAllItems = "1";
                isAllItems = true;
                $('.isAllItems').val('').trigger('click');
                $('#itemList').prop('disabled', true);
            }

            if (input.customerIds == null && input.isAllCustomers == 0) {
                input.isAllCustomers = "1";
                isAllCustomers = true;
                $('.isAllCustomers').val('').trigger('click');
                $('#customerName').prop('disabled', true);
            }

            if (input.fromDate == "" && input.toDate == "") {
                input.NodateSelected = true;            
            } else {
                input.NodateSelected = false;
            }


            if (deliveryItemReportValidation(input)) {
                var getpdfdatarequest = eb.post(url, input);
                getpdfdatarequest.done(function(respond) {
                    if (respond.status == true) {
                        $('#dailyDeliveryReport').removeClass('hidden');
                        setContentToReport('dailyDeliveryReport', respond);
                    }
                });
            }
        } else if(reportType ==='deliveryNoteWiseInvoice'){
            var url = BASE_URL + '/reports/get/daily-delivery/delivery-note-wise-invoice-report';
            var input = {};
            input.fromDate = $('#fromDate').val();
            input.toDate = $('#toDate').val();
            input.isAllDeliveryNotes = isAllDeliveryNotes;
            input.deliveryNoteIds = $('#deliveryNoteNo').val();


            if (input.fromDate == "" && input.toDate == "") {
                input.NodateSelected = true;            
            } else {
                input.NodateSelected = false;
            }
           
            if (deliveryNoteWiseInvoiceReportValidation(input)) {
                var getpdfdatarequest = eb.post(url, input);
                getpdfdatarequest.done(function(respond) {
                    if (respond.status == true) {
                        $('#dailyDeliveryReport').removeClass('hidden');
                        setContentToReport('dailyDeliveryReport', respond);
                    }
                });
            }
        } else if(reportType ==='deliveryNoteWiseSalesReturns'){
            var url = BASE_URL + '/reports/get/daily-delivery/delivery-note-wise-sales-return-report';
            var input = {};
            input.fromDate = $('#fromDate').val();
            input.toDate = $('#toDate').val();
            input.isAllDeliveryNotes = isAllDeliveryNotes;
            input.deliveryNoteIds = $('#deliveryNoteNo').val();


            if (input.fromDate == "" && input.toDate == "") {
                input.NodateSelected = true;            
            } else {
                input.NodateSelected = false;
            }

            if (deliveryNoteWiseInvoiceReportValidation(input)) {
                var getpdfdatarequest = eb.post(url, input);
                getpdfdatarequest.done(function(respond) {
                    if (respond.status == true) {
                        $('#dailyDeliveryReport').removeClass('hidden');
                        setContentToReport('dailyDeliveryReport', respond);
                    }
                });
            }
        } else if(reportType ==='editedDeliveryNote'){
            var url = BASE_URL + '/reports/get/daily-delivery/edited-delivery-note-report';

            var inputs = [
                $('#fromDate').val(),
                $('#toDate').val(),
            ];
            if (!($('#fromDate').val() && $('#toDate').val()) || validteinput(inputs))
            {
                var getpdfdatarequest = eb.post(url, {
                    fromDate: $('#fromDate').val(),
                    toDate: $('#toDate').val(),
                    users: $('#users').val(),
                    isAllCustomers : isAllCustomers,
                    customerIds : $('#customerName').val(),
                });
                getpdfdatarequest.done(function(respond) {
                    if (respond.status) {
                        $('#dailyDeliveryReport').removeClass('hidden');
                        setContentToReport('dailyDeliveryReport', respond);
                    }
                });
            }
        }
    });

    $('#generatePdf').click(function() {
        if (reportType === 'dailyDeliveryValues') {
            var fromDate = $('#fromDate').val();
            var toDate = $('#toDate').val();
            var cusCategories = $('#customerCategoryList').val();
            inputs = new Array(
                    fromDate,
                    toDate
                    );
            if (validteinput(inputs)) {
                var url = BASE_URL + '/reports/get/daily-delivery/generate-daily-delivery-value-pdf';
                eb.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: {fromDate: fromDate, toDate: toDate, cusCategories: cusCategories},
                    success: function(respond) {
                        if (respond.status == true) {
                            window.open(BASE_URL + '/' + respond.data, '_blank');
                        }
                    }
                });
            }
        } else if (reportType === 'dailyDeliveryItems') {
            var fromDate = $('#fromDate').val();
            var toDate = $('#toDate').val();
            var cusCategories = $('#customerCategoryList').val();
            inputs = new Array(
                    fromDate,
                    toDate
                    );
            if (validteinput(inputs)) {
                var url = BASE_URL + '/reports/get/daily-delivery/generate-daily-delivery-item-pdf';
                eb.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: {fromDate: fromDate, toDate: toDate, cusCategories: cusCategories},
                    success: function(respond) {
                        if (respond.status == true) {
                            window.open(BASE_URL + '/' + respond.data, '_blank');
                        }
                    }
                });
            }
        } else if (reportType === 'invDelinoteVal') {
            var fromDate = $('#fromDate').val();
            var toDate = $('#toDate').val();
            inputs = new Array(
                    fromDate,
                    toDate
                    );

            if (validteinput(inputs)) {
                var url = BASE_URL + '/reports/get/daily-delivery/invoiced-delivery-note-values-pdf';
                eb.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: {fromDate: fromDate, toDate: toDate},
                    success: function(respond) {
                        if (respond.status == true) {
                            window.open(BASE_URL + '/' + respond.data, '_blank');
                        }
                    }
                });
                //window.open(BASE_URL + '/reports/deliverynote-report/invoiced-deliverynote-values-pdf');//;?fromDate=" + fromDate + "&toDate=" + toDate, '_blank');
            }
        } else if (reportType === 'invDelinoteItem') {
            var fromDate = $('#fromDate').val();
            var toDate = $('#toDate').val();
            inputs = new Array(
                    fromDate,
                    toDate
                    );

            if (validteinput(inputs)) {
                var url = BASE_URL + '/reports/get/daily-delivery/invoiced-delivery-note-items-pdf';
                eb.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: {fromDate: fromDate, toDate: toDate},
                    success: function(respond) {
                        if (respond.status == true) {
                            window.open(BASE_URL + '/' + respond.data, '_blank');
                        }
                    }
                });
            }
        } else if (reportType === 'notInvDelinoteVal') {
            var fromDate = $('#fromDate').val();
            var toDate = $('#toDate').val();
            inputs = new Array(
                    fromDate,
                    toDate
                    );

            if (validteinput(inputs)) {
                var url = BASE_URL + '/reports/get/daily-delivery/not-invoiced-delivery-note-values-pdf';
                eb.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: {fromDate: fromDate, toDate: toDate},
                    success: function(respond) {
                        if (respond.status == true) {
                            window.open(BASE_URL + '/' + respond.data, '_blank');
                        }
                    }
                });

                // window.open(BASE_URL + "/reports/deliverynote-report/not-invoiced-deliverynote-values-pdf?fromDate=" + fromDate + "&toDate=" + toDate, '_blank');
            }
        }
        else if (reportType === 'notInvoiceDeliveryItems') {
            var fromDate = $('#fromDate').val();
            var toDate = $('#toDate').val();
            inputs = new Array(
                    fromDate,
                    toDate
                    );
            if (validteinput(inputs)) {
                var url = BASE_URL + '/reports/get/daily-delivery/generate-not-invoice-delivery-item-pdf';
                eb.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: {fromDate: fromDate, toDate: toDate},
                    success: function(respond) {
                        if (respond.status == true) {
                            window.open(BASE_URL + '/' + respond.data, '_blank');
                        }
                    }
                });
            }
        } else if (reportType === 'deliveryItemDetail') {
            var url = BASE_URL + '/reports/get/daily-delivery/delivery-note-items-detail-pdf';
            var input = {};
            input.fromDate = $('#fromDate').val();
            input.toDate = $('#toDate').val();
            input.isAllCustomers = isAllCustomers;
            input.customerIds = $('#customerName').val();
            input.isAllItems = isAllItems;
            input.itemIds = $('#itemList').val();
            input.isAllDeliveryNotes = isAllDeliveryNotes;
            input.deliveryNoteIds = $('#deliveryNoteNo').val();
            input.isTaxEnabled = isTaxEnabled;
            input.isSortByCustomer = isSortByCustomer;
            input.locationIds = $('#branch-locations').val();           
        
            if (input.itemIds == null && input.isAllItems == 0) {
                input.isAllItems = "1";
                isAllItems = true;
                $('.isAllItems').val('').trigger('click');
                $('#itemList').prop('disabled', true);
            }

            if (input.customerIds == null && input.isAllCustomers == 0) {
                input.isAllCustomers = "1";
                isAllCustomers = true;
                $('.isAllCustomers').val('').trigger('click');
                $('#customerName').prop('disabled', true);
            }

            if (input.fromDate == "" && input.toDate == "") {
                input.NodateSelected = true;            
            } else {
                input.NodateSelected = false;
            }

            if (deliveryItemReportValidation(input)) {
                eb.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: input,
                    success: function(respond) {
                        if (respond.status == true) {
                            window.open(BASE_URL + '/' + respond.data, '_blank');
                        }
                    }
                });
            }
        } else if (reportType ==='deliveryItemDetailSummary'){
            var url = BASE_URL + '/reports/get/daily-delivery/delivery-note-items-detail-summary-pdf';
            var input = {};
            input.fromDate = $('#fromDate').val();
            input.toDate = $('#toDate').val();
            input.isAllCustomers = isAllCustomers;
            input.customerIds = $('#customerName').val();
            input.isAllItems = isAllItems;
            input.itemIds = $('#itemList').val();
            input.isAllDeliveryNotes = isAllDeliveryNotes;
            input.deliveryNoteIds = $('#deliveryNoteNo').val();
            input.isTaxEnabled = isTaxEnabled;
            input.isSortByCustomer = isSortByCustomer;
            input.locationIds = $('#branch-locations').val();

            if (input.itemIds == null && input.isAllItems == 0) {
                input.isAllItems = "1";
                isAllItems = true;
                $('.isAllItems').val('').trigger('click');
                $('#itemList').prop('disabled', true);
            }

            if (input.customerIds == null && input.isAllCustomers == 0) {
                input.isAllCustomers = "1";
                isAllCustomers = true;
                $('.isAllCustomers').val('').trigger('click');
                $('#customerName').prop('disabled', true);
            }

            if (input.fromDate == "" && input.toDate == "") {
                input.NodateSelected = true;            
            } else {
                input.NodateSelected = false;
            }

            if (deliveryItemReportValidation(input)) {
                eb.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: input,
                    success: function(respond) {
                        if (respond.status == true) {
                            window.open(BASE_URL + '/' + respond.data, '_blank');
                        }
                    }
                });
            }
        } else if(reportType ==='deliveryNoteWiseInvoice'){
            var url = BASE_URL + '/reports/get/daily-delivery/delivery-note-wise-invoice-pdf';
            var input = {};
            input.fromDate = $('#fromDate').val();
            input.toDate = $('#toDate').val();
            input.isAllDeliveryNotes = isAllDeliveryNotes;
            input.deliveryNoteIds = $('#deliveryNoteNo').val();

            if (input.fromDate == "" && input.toDate == "") {
                input.NodateSelected = true;            
            } else {
                input.NodateSelected = false;
            }

            if (deliveryNoteWiseInvoiceReportValidation(input)) {
                eb.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: input,
                    success: function(respond) {
                        if (respond.status == true) {
                            window.open(BASE_URL + '/' + respond.data, '_blank');
                        }
                    }
                });
            }
        } else if(reportType ==='deliveryNoteWiseSalesReturns'){
            var url = BASE_URL + '/reports/get/daily-delivery/delivery-note-wise-sales-return-pdf';
            var input = {};
            input.fromDate = $('#fromDate').val();
            input.toDate = $('#toDate').val();
            input.isAllDeliveryNotes = isAllDeliveryNotes;
            input.deliveryNoteIds = $('#deliveryNoteNo').val();

            if (input.fromDate == "" && input.toDate == "") {
                input.NodateSelected = true;            
            } else {
                input.NodateSelected = false;
            }
            if (deliveryNoteWiseInvoiceReportValidation(input)) {
                eb.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: input,
                    success: function(respond) {
                        if (respond.status == true) {
                            window.open(BASE_URL + '/' + respond.data, '_blank');
                        }
                    }
                });
            }
        } else if(reportType ==='editedDeliveryNote'){
            var url = BASE_URL + '/reports/get/daily-delivery/edited-delivery-note-pdf';
                             
            var inputs = [
                $('#fromDate').val(),
                $('#toDate').val(),
            ];
            if (!($('#fromDate').val() && $('#toDate').val()) || validteinput(inputs))
            {
                var getpdfdatarequest = eb.post(url, {
                    fromDate: $('#fromDate').val(),
                    toDate: $('#toDate').val(),
                    users: $('#users').val(),
                    isAllCustomers : isAllCustomers,
                    customerIds : $('#customerName').val(),
                });
                getpdfdatarequest.done(function(respond) {
                    if (respond.status) {
                        window.open(BASE_URL + '/' + respond.data, '_blank');
                    }
                });
            }
        }
    });
    $('#csvReport').click(function() {
        if (reportType === 'dailyDeliveryValues') {
            var fromDate = $('#fromDate').val();
            var toDate = $('#toDate').val();
            var cusCategories = $('#customerCategoryList').val();
            inputs = new Array(
                    fromDate,
                    toDate
                    );
            if (validteinput(inputs)) {
                var url = BASE_URL + '/reports/get/daily-delivery/generate-daily-delivery-value-sheet';
                eb.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: {fromDate: fromDate, toDate: toDate, cusCategories: cusCategories},
                    success: function(respond) {
                        if (respond.status == true) {
                            window.open(BASE_URL + '/' + respond.data, '_blank');
                        }
                    }
                });
            }
        } else if (reportType === 'dailyDeliveryItems') {
            var fromDate = $('#fromDate').val();
            var toDate = $('#toDate').val();
            var cusCategories = $('#customerCategoryList').val();
            inputs = new Array(
                    fromDate,
                    toDate
                    );
            if (validteinput(inputs)) {
                var url = BASE_URL + '/reports/get/daily-delivery/generate-daily-delivery-item-sheet';
                eb.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: {fromDate: fromDate, toDate: toDate, cusCategories: cusCategories},
                    success: function(respond) {
                        if (respond.status == true) {
                            window.open(BASE_URL + '/' + respond.data, '_blank');
                        }
                    }
                });
            }
        }
        else if (reportType === 'notInvoiceDeliveryItems') {
            var fromDate = $('#fromDate').val();
            var toDate = $('#toDate').val();
            inputs = new Array(
                    fromDate,
                    toDate
                    );
            if (validteinput(inputs)) {
                var url = BASE_URL + '/reports/get/daily-delivery/generate-not-invoice-delivery-item-sheet';
                eb.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: {fromDate: fromDate, toDate: toDate},
                    success: function(respond) {
                        if (respond.status == true) {
                            window.open(BASE_URL + '/' + respond.data, '_blank');
                        }
                    }
                });
            }
        }
        if (reportType === 'invDelinoteVal') {
            var fromDate = $('#fromDate').val();
            var toDate = $('#toDate').val();
            inputs = new Array(
                    fromDate,
                    toDate
                    );

            if (validteinput(inputs)) {
                var url = BASE_URL + '/reports/get/daily-delivery/invoiced-delivery-note-values-sheet';
                eb.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: {fromDate: fromDate, toDate: toDate},
                    success: function(respond) {
                        if (respond.status == true) {
                            window.open(BASE_URL + '/' + respond.data, '_blank');
                        }
                    }
                });
            }
        } else if (reportType === 'invDelinoteItem') {
            var fromDate = $('#fromDate').val();
            var toDate = $('#toDate').val();
            inputs = new Array(
                    fromDate,
                    toDate
                    );

            if (validteinput(inputs)) {
                var url = BASE_URL + '/reports/get/daily-delivery/invoiced-delivery-note-items-sheet';
                eb.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: {fromDate: fromDate, toDate: toDate},
                    success: function(respond) {
                        if (respond.status == true) {
                            window.open(BASE_URL + '/' + respond.data, '_blank');
                        }
                    }
                });
            }
        }
        else if (reportType === 'notInvDelinoteVal') {
            var fromDate = $('#fromDate').val();
            var toDate = $('#toDate').val();
            inputs = new Array(
                    fromDate,
                    toDate
                    );

            if (validteinput(inputs)) {
                var url = BASE_URL + '/reports/get/daily-delivery/not-invoiced-delivery-note-values-sheet';
                eb.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: {fromDate: fromDate, toDate: toDate},
                    success: function(respond) {
                        if (respond.status == true) {
                            window.open(BASE_URL + '/' + respond.data, '_blank');
                        }
                    }
                });
            }
        } else if (reportType === 'deliveryItemDetail') {
            var url = BASE_URL + '/reports/get/daily-delivery/delivery-note-items-detail-csv';
            var input = {};
            input.fromDate = $('#fromDate').val();
            input.toDate = $('#toDate').val();
            input.isAllCustomers = isAllCustomers;
            input.customerIds = $('#customerName').val();
            input.isAllItems = isAllItems;
            input.itemIds = $('#itemList').val();
            input.isAllDeliveryNotes = isAllDeliveryNotes;
            input.deliveryNoteIds = $('#deliveryNoteNo').val();
            input.isTaxEnabled = isTaxEnabled;
            input.isSortByCustomer = isSortByCustomer;
            input.locationIds = $('#branch-locations').val();
        
            if (input.itemIds == null && input.isAllItems == 0) {
                input.isAllItems = "1";
                isAllItems = true;
                $('.isAllItems').val('').trigger('click');
                $('#itemList').prop('disabled', true);
            }

            if (input.customerIds == null && input.isAllCustomers == 0) {
                input.isAllCustomers = "1";
                isAllCustomers = true;
                $('.isAllCustomers').val('').trigger('click');
                $('#customerName').prop('disabled', true);
            }

            if (input.fromDate == "" && input.toDate == "") {
                input.NodateSelected = true;            
            } else {
                input.NodateSelected = false;
            }

            if (deliveryItemReportValidation(input)) {
                eb.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: input,
                    success: function(respond) {
                        if (respond.status == true) {
                            window.open(BASE_URL + '/' + respond.data, '_blank');
                        }
                    }
                });
            }
        } else if (reportType ==='deliveryItemDetailSummary'){
            var url = BASE_URL + '/reports/get/daily-delivery/delivery-note-items-summary-detail-csv';
            var input = {};
            input.fromDate = $('#fromDate').val();
            input.toDate = $('#toDate').val();
            input.isAllCustomers = isAllCustomers;
            input.customerIds = $('#customerName').val();
            input.isAllItems = isAllItems;
            input.itemIds = $('#itemList').val();
            input.isAllDeliveryNotes = isAllDeliveryNotes;
            input.deliveryNoteIds = $('#deliveryNoteNo').val();
            input.isTaxEnabled = isTaxEnabled;
            input.isSortByCustomer = isSortByCustomer;
            input.locationIds = $('#branch-locations').val();
           
        
            if (input.itemIds == null && input.isAllItems == 0) {
                input.isAllItems = "1";
                isAllItems = true;
                $('.isAllItems').val('').trigger('click');
                $('#itemList').prop('disabled', true);
            }

            if (input.customerIds == null && input.isAllCustomers == 0) {
                input.isAllCustomers = "1";
                isAllCustomers = true;
                $('.isAllCustomers').val('').trigger('click');
                $('#customerName').prop('disabled', true);
            }

            if (input.fromDate == "" && input.toDate == "") {
                input.NodateSelected = true;            
            } else {
                input.NodateSelected = false;
            }

            if (deliveryItemReportValidation(input)) {
                eb.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: input,
                    success: function(respond) {
                        if (respond.status == true) {
                            window.open(BASE_URL + '/' + respond.data, '_blank');
                        }
                    }
                });
            }
        } else if(reportType ==='deliveryNoteWiseInvoice'){
            var url = BASE_URL + '/reports/get/daily-delivery/delivery-note-wise-invoice-csv';
            var input = {};
            input.fromDate = $('#fromDate').val();
            input.toDate = $('#toDate').val();
            input.isAllDeliveryNotes = isAllDeliveryNotes;
            input.deliveryNoteIds = $('#deliveryNoteNo').val();

            if (input.fromDate == "" && input.toDate == "") {
                input.NodateSelected = true;            
            } else {
                input.NodateSelected = false;
            }

            if (deliveryNoteWiseInvoiceReportValidation(input)) {
                eb.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: input,
                    success: function(respond) {
                        if (respond.status == true) {
                            window.open(BASE_URL + '/' + respond.data, '_blank');
                        }
                    }
                });
            }
        } else if(reportType ==='deliveryNoteWiseSalesReturns'){
            var url = BASE_URL + '/reports/get/daily-delivery/delivery-note-wise-sales-return-csv';
            var input = {};
            input.fromDate = $('#fromDate').val();
            input.toDate = $('#toDate').val();
            input.isAllDeliveryNotes = isAllDeliveryNotes;
            input.deliveryNoteIds = $('#deliveryNoteNo').val();

            if (input.fromDate == "" && input.toDate == "") {
                input.NodateSelected = true;            
            } else {
                input.NodateSelected = false;
            }
            if (deliveryNoteWiseInvoiceReportValidation(input)) {
                eb.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: input,
                    success: function(respond) {
                        if (respond.status == true) {
                            window.open(BASE_URL + '/' + respond.data, '_blank');
                        }
                    }
                });
            }
        } else if(reportType ==='editedDeliveryNote'){
            var url = BASE_URL + '/reports/get/daily-delivery/edited-delivery-note-sheet';
                             
            var inputs = [
                $('#fromDate').val(),
                $('#toDate').val(),
            ];
            if (!($('#fromDate').val() && $('#toDate').val()) || validteinput(inputs))
            {
                var getpdfdatarequest = eb.post(url, {
                    fromDate: $('#fromDate').val(),
                    toDate: $('#toDate').val(),
                    users: $('#users').val(),
                    isAllCustomers : isAllCustomers,
                    customerIds : $('#customerName').val(),
                });
                getpdfdatarequest.done(function(respond) {
                    if (respond.status) {
                        window.open(BASE_URL + '/' + respond.data, '_blank');
                    }
                });
            }
        }
    });

    $('.isAllCustomers').on('click', function() {
        isAllCustomers = $(this).val();
        if (isAllCustomers == true) {
            $('#customerName').val('').trigger('change');
            $('#customerName').prop('disabled', true);
        } else {
            $('#customerName').prop('disabled', false);
        }
        $('#customerName').selectpicker('refresh');
    });

    $('.isAllItems').on('click', function() {
        isAllItems = $(this).val();
        if (isAllItems == true) {
            $('#itemList').val('').trigger('change');
            $('#itemList').prop('disabled', true);
        } else {
            $('#itemList').prop('disabled', false);
        }
    });

    $('.isAllDeliveryNotes').on('click', function() {
        isAllDeliveryNotes = $(this).val();
        if (isAllDeliveryNotes == true) {
            $('#deliveryNoteNo').val('').trigger('change');
            $('#deliveryNoteNo').prop('disabled', true);
        } else {
            $('#deliveryNoteNo').prop('disabled', false);
        }
    });

    $('#taxCheckBox').click(function(){
        if ($(this).is(':checked')) {
            isTaxEnabled = true;
        } else {
            isTaxEnabled = false;
        }
    });

    $('#customerSortCheckBox').click(function(){
        if ($(this).is(':checked')) {
            isSortByCustomer = true;
        } else {
            isSortByCustomer = false;
        }
    });

});
// date validation
function validteinput(inputs) {
    var fromDate = inputs[0];
    var toDate = inputs[1];
    if (fromDate == null || fromDate == "") {
        p_notification(false, eb.getMessage('ERR_DAISALE_FROMD'));
        document.getElementById("fromDate").focus();
    }
    else if (toDate == null || toDate == "") {
        p_notification(false, eb.getMessage('ERR_DAISALE_TOD'));
        document.getElementById("toDate").focus();
    } else if (fromDate > toDate) {
        p_notification(false, eb.getMessage('ERR_DAISALE_TODFROMD'));
        document.getElementById("toDate").focus();
    } else {
        return true;
    }
}

function getDateForDatePicker(nextDate) {
    today = new Date();
    if (nextDate) {
        var dd = today.getDate()+1;
    } else {
        var dd = today.getDate();
    }

    var mm = today.getMonth()+1; //As January is 0.
    var yyyy = today.getFullYear();

    if(dd<10) dd='0'+dd;
    if(mm<10) mm='0'+mm;
    return  (yyyy+'-'+mm+'-'+dd);
}

function deliveryItemReportValidation(input)
{
    if (!input.NodateSelected && (input.fromDate == null || input.fromDate == "")) {
        p_notification(false, eb.getMessage('ERR_DAISALE_FROMD'));
        document.getElementById("fromDate").focus();
        return false;
    } else if (!input.locationIds) {
        p_notification(false, eb.getMessage('ERR_CUSREPO_SELELOCATION'));
        return false;
    } else if (!input.NodateSelected && (input.toDate == null || input.toDate == "")) {
        p_notification(false, eb.getMessage('ERR_DAISALE_TOD'));
        document.getElementById("toDate").focus();
        return false;
    } else if (!input.NodateSelected && (input.toDate < input.fromDate)) {
        p_notification(false, eb.getMessage('ERR_DAISALE_TODFROMD'));
        document.getElementById("toDate").focus();
        return false;
    } else if (input.isAllDeliveryNotes == false && input.deliveryNoteIds == null) {
        p_notification(false, eb.getMessage('ERR_DELNTE_REPORT_SELECT_DELIVERYNOTE'));
        return false;
    } else if (input.isAllCustomers == false && input.customerIds == null) {
        p_notification(false, eb.getMessage('ERR_CUSREPO_SELECUST'));
        return false;
    } else if (input.isAllItems == false && input.itemIds == null) {
        p_notification(false, eb.getMessage('ERR_DAISALE_ITEMWISETBL_PRODUCT_ERR'));
        return false;
    } else {
        return true;
    }
}

function deliveryNoteWiseInvoiceReportValidation(input)
{
    if (!input.NodateSelected && (input.fromDate == null || input.fromDate == "")) {
        p_notification(false, eb.getMessage('ERR_DAISALE_FROMD'));
        document.getElementById("fromDate").focus();
        return false;
    } else if (!input.NodateSelected && (input.toDate == null || input.toDate == "")) {
        p_notification(false, eb.getMessage('ERR_DAISALE_TOD'));
        document.getElementById("toDate").focus();
        return false;
    } else if (!input.NodateSelected && (input.toDate < input.fromDate)) {
        p_notification(false, eb.getMessage('ERR_DAISALE_TODFROMD'));
        document.getElementById("toDate").focus();
        return false;
    } else if (input.isAllDeliveryNotes == false && input.deliveryNoteIds == null) {
        p_notification(false, eb.getMessage('ERR_DELNTE_REPORT_SELECT_DELIVERYNOTE'));
        return false;
    } else {
        return true;
    }
}

