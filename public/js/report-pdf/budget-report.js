var dimensionTypeID = null;
var dimensionValue = null;
$(document).ready(function() {
    var reportType = 'budgetReport';
    $('#budgetReport').hide();
    $('#trialBalance').click(function(e) {
        reportType = 'budgetReport';
    });

    var accountIDs;
    loadDropDownFromDatabase('/accounts-api/search-active-accounts-for-dropdown', "", "", '#accountID');
    $('#accountID').on('change', function() {
        accountIDs = $(this).val();

    });

    var fiscalPeriodID;
    $('#fiscalPeriods').on('change',function(){
        fiscalPeriodID = $(this).val();
    });

    var accountType;
    $('#acFilter').on('change',function(){
        accountType = $(this).val();
    });

    $('#viewReport').click(function(e) {
        if(reportType == 'budgetReport'){
            if(budgetvalidation()){
                eb.ajax({
                    type: 'POST',
                    url: BASE_URL + '/budget-report-api/budgetReportView',
                    data: {
                        fiscalPeriodID: fiscalPeriodID,
                        accountIDs: accountIDs,
                        dimensionType : dimensionTypeID,
                        dimensionValue : dimensionValue,
                        accountType : accountType
                    },
                    success: function(respond) {
                        if(respond.status){
                            setContentToReport('budgetReport', respond);
                        }else{
                            p_notification(respond.status, respond.msg);
                        }
                    }
                });
            }
        } 
    });

    $('#generatePdf').click(function(e) {
        if(reportType == 'budgetReport'){
            if(budgetvalidation()){
                var params = {
                        fiscalPeriodID: fiscalPeriodID,
                        accountIDs: accountIDs,
                        dimensionType : dimensionTypeID,
                        dimensionValue : dimensionValue,
                        accountType : accountType
                }
          var url = BASE_URL + '/budget-report-api/generate-budget-report-pdf';
            eb.ajax({
                type: 'POST',
                url: url,
                dataType: 'json',
                data: params,
                success: function(respond) {
                    if (respond.status) {
                        window.open(BASE_URL + '/' + respond.data, '_blank');
                    } 
                }
            });
            }
        } 
    });

    $('#csvReport').click(function(e){
        if(reportType == 'budgetReport'){
            if(budgetvalidation()){
                var params = {
                    fiscalPeriodID: fiscalPeriodID,
                    accountIDs: accountIDs,
                    dimensionType : dimensionTypeID,
                    dimensionValue : dimensionValue,
                    accountType : accountType
                }
                var url = BASE_URL + '/budget-report-api/budgetReportSheet';
                eb.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: params,
                    success: function(respond) {
                        if (respond.status) {
                            window.open(BASE_URL + '/' + respond.data, '_blank');
                        }
                    }
                });
            }
        } 
    });

    $('.dimenisonchange').on('change', function(e) {
        dimensionTypeID = this.value;
        getDimensionValueListByDimensionType(dimensionTypeID, function() {
        });
    });

    function getDimensionValueListByDimensionType(dimensionTypeID, callback) {
        switch (dimensionTypeID) {
            case "job":
                $('.dimensionVal').addClass('hidden');
                $('.dimensionProjectNo').addClass('hidden');
                $('.dimensionJobNo').removeClass('hidden');
                break;
            case "project":
                $('.dimensionVal').addClass('hidden');
                $('.dimensionProjectNo').removeClass('hidden');
                $('.dimensionJobNo').addClass('hidden');
                break;
           default:
                $('.dimensionVal').removeClass('hidden');
                $('.dimensionProjectNo').addClass('hidden');
                $('.dimensionJobNo').addClass('hidden');
                $.ajax({
                    type: 'POST',
                    url: BASE_URL + '/dimension-api/getRelatedDimensionValues',
                    data: {dimensionId: dimensionTypeID},
                    success: function(respond) {
                        //empty options
                        $('#dimensionVal').empty();
                        $('#dimensionVal').append($("<option>", {value: '', html: 'Select dimension value'}));
                        $(respond.data).each(function(index, dimValue) {
                            $.each(dimValue, function(ind, val) {
                                $('#dimensionVal').append($("<option>", {value: ind, html: val.value}));
                            });
                        });
                        callback();
                    }
                });
                break;
        }
    }

    loadDropDownFromDatabase('/job-api/searchJobsForDropdown', "", 0, '.dimensionJobNo');
    $('.dimensionJobNo').selectpicker('refresh');
    $('.dimensionJobNo').on('change', function() {
        dimensionValue = $(this).val();
    });

    loadDropDownFromDatabase('/api/project/searchProjectsForDropdown', "", 0, '.dimensionProjectNo');
    $('.dimensionProjectNo').selectpicker('refresh');
    $('.dimensionProjectNo').on('change', function() {
        dimensionValue = $(this).val();
    });

    $('.dimensionVal').on('change', function() {
        dimensionValue = $(this).val();
    });

});

function budgetvalidation()
{
  var fiscalPeriods = $('#fiscalPeriods').val();
  if(fiscalPeriods == 0){
    p_notification(false, eb.getMessage('ERR_BUDGET_FISCAL_PERIOD_EMPTY'));
        $('#fiscalPeriods').focus();
        return false;
  }
  return true;
}
