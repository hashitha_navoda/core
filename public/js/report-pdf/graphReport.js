/*
 * @author SANDUN <sandun@thinkcube.com>
 * Reporting functions
 */
$(document).ready(function() {

    var reportType = 'InvPay';
    var topCusType;

    var checkin = $('#from_date').datepicker({
    }).on('changeDate', function(ev) {
        if (ev.date.valueOf() < checkout.date.valueOf()) {
            var newDate = new Date(ev.date)
            newDate.setDate(newDate.getDate() + 1);
            checkout.setValue(newDate);
        }
        checkin.hide();
        $('#to_date')[0].focus();
    }).data('datepicker');
    var checkout = $('#to_date').datepicker({
        onRender: function(date) {
            return date.valueOf() <= checkin.date.valueOf() ? 'disabled' : '';
        }
    }).on('changeDate', function(ev) {
        checkout.hide();
    }).data('datepicker');

    $('#inv_pay_graph_view').hide();
    $('#top_cus_report_select').hide();
    $('#top_all_cus_graph_div').hide();
    $('#top_ten_cus_graph_div').hide();
    $('#top_twowenty_cus_graph_div').hide();

    $('#btn_invpay,#btn_allCus,#btn_toptenCus,#btn_toptwowentyCus').hide();

    $('#InvPay').click(function(e) {
        reportType = 'InvPay';
        $('#btn_allCus,#btn_toptenCus,#btn_toptwowentyCus').hide();
        $('.hide_this').show();
        $('#top_cus_report_select').hide();
        $('#from_date').show();
        $('#to_date').show();
        $('#from_date').val('');
        $('#to_date').val('');
        $('.frm_date_lbl').show();
        $('.to_date_lbl').show();
        $('.select_graph').addClass('hidden');
        $('.graph_type').addClass('hidden');
        $('#graph_div_cus').hide();

    });

    $('#topCustomers').click(function(e) {
        reportType = 'topCustomers';
        $('#btn_invpay').hide();
        $("#top_cus_report_select").val("Select Graph Type");
        $('#inv_pay_graph_view').hide();
        $('#top_cus_report_select').show();
        $('#from_date').hide();
        $('#to_date').hide();

        $('.graph_type').removeClass('hidden');
        $('.frm_date_lbl').hide();
        $('.to_date_lbl').hide();
        $('.select_graph').removeClass('hidden');
        $('.hide_this').hide();
    });

    $('#top_cus_report_select').on('change', function() {
        topCusType = $(this).val();
    });

    $('#view_graph').click(function(e) {

        if (reportType == 'InvPay') {
            $('#btn_invpay').show();
            $('#inv_pay').html('');
            var getpdfdataurl = BASE_URL + '/graphReportAPI/getMonthlySalesDetailsGraph';
            inputs = new Array(
                    $('#from_date').val(),
                    $('#to_date').val()
                    );
            var from_date = $('#from_date').val();
            var to_date = $('#to_date').val();

            if (validteinput(inputs)) {

                var getpdfdatarequest = eb.post(getpdfdataurl, {
                    from_date: from_date,
                    to_date: to_date
                });
                getpdfdatarequest.done(function(invpaydata) {

                    $('#inv_pay_graph_view').show();
                    var a = Array();

                    $.each(invpaydata.salesGraphData, function(index, value) {
                        $.each(value, function(subindex, subvalue) {
                            var Month = subvalue.Month;
                            var TotalAmount = parseFloat(Math.round((subvalue.TotalAmount) * 100) / 100).toFixed(2);
                            var TotalPayed = parseFloat(Math.round((subvalue.TotalPayed) * 100) / 100).toFixed(2);
                            a.push({x: Month, y: TotalAmount, z: TotalPayed});
                        });
                    });

                    Morris.Bar({
                        element: 'inv_pay',
                        data: a,
                        xkey: 'x',
                        ykeys: ['y', 'z'],
                        labels: ['Total Invoice Amount', 'Total Payment'],
                        parseTime: false,
                        yLabelFormat: function(y) {
                            return 'Rs.' + accounting.formatMoney(y);
                        },
                        barColors: ['#67C69D', '#0BA462'],
                        xLabelAngle: 60
                    });
                });
            }
        }

        else if (reportType == 'topCustomers') {
            if (topCusType == 'All_Customers') {
                $('#btn_allCus').show();
                $('#btn_toptenCus,#btn_toptwowentyCus').hide();
                $('#graph_div_cus').show();
                $('#top_all_cus_graph_div').show();
                $('#top_ten_cus_graph_div').hide();
                $('#top_twowenty_cus_graph_div').hide();
                $("#cus_all").html('');
                $("#cus_ten").html('');
                $("#cus_twowenty").html('');

                var url = BASE_URL + '/graphReportAPI/getAllTopCustomersDetails';

                var getpdfdatarequest = eb.post(url, {
                    cus_type: 1
                });
                getpdfdatarequest.done(function(data) {
                    $('#top_all_cus_graph_div').show();
                    var graphCusAllData = data['allCusData'];

                    var a = Array();
                    for (var i = 0; i < graphCusAllData.length; i++) {
                        var CusName = graphCusAllData[i].CusName;
                        var cusName_split = CusName.split("-");
                        var Sales = parseFloat(Math.round((graphCusAllData[i].Sales) * 100) / 100).toFixed(2);
                        var Tax = parseFloat(Math.round((graphCusAllData[i].Tax) * 100) / 100).toFixed(2);
                        a.push({x: cusName_split[0], y: Sales, z: Tax});
                    }

                    Morris.Line({
                        element: 'cus_all',
                        data: a,
                        xkey: 'x',
                        ykeys: ['y', 'z'],
                        labels: ['Sales', 'Tax'],
                        lineColors: ['#ffa8b0', '#abcdef'],
                        parseTime: false,
                        yLabelFormat: function(y) {
                            return 'Rs.' + accounting.formatMoney(y);
                        },
                        xLabelAngle: 60
                    });
                });

            }
            else if (topCusType == 'Top_10_Customers') {
                $('#btn_toptenCus').show();
                $('#btn_allCus,#btn_toptwowentyCus').hide();
                $('#graph_div_cus').show();
                $('#top_all_cus_graph_div').hide();
                $('#top_twowenty_cus_graph_div').hide();
                $("#cus_all").html('');
                $("#cus_ten").html('');
                $("#cus_twowenty").html('');

                var url = BASE_URL + '/graphReportAPI/getTopTenCustomersDetails';

                var getpdfdatarequest = eb.post(url, {
                    cus_type: 1
                });
                getpdfdatarequest.done(function(data) {
                    $('#top_ten_cus_graph_div').show();
                    var graphCusTenData = data['tenCusData'];

                    var a = Array();
                    for (var i = 0; i < graphCusTenData.length; i++) {
                        var CusName = graphCusTenData[i].CusName;
                        var cusName_split = CusName.split("-");
                        var Sales = parseFloat(Math.round((graphCusTenData[i].Sales) * 100) / 100).toFixed(2);
                        var Tax = parseFloat(Math.round((graphCusTenData[i].Tax) * 100) / 100).toFixed(2);
                        a.push({x: cusName_split[0], y: Sales, z: Tax});
                    }

                    Morris.Line({
                        element: 'cus_ten',
                        data: a,
                        xkey: 'x',
                        ykeys: ['y', 'z'],
                        labels: ['Sales', 'Tax'],
                        lineColors: ['#ffa8b0', '#abcdef'],
                        parseTime: false,
                        yLabelFormat: function(y) {
                            return 'Rs.' + accounting.formatMoney(y);
                        },
                        xLabelAngle: 60
                    });
                });
            }
            else if (topCusType == 'Top_20_Customers') {
                $('#btn_toptwowentyCus').show();
                $('#btn_allCus,#btn_toptenCus').hide();
                $('#graph_div_cus').show();
                $('#top_ten_cus_graph_div').hide();
                $('#top_all_cus_graph_div').hide();
                $("#cus_all").html('');
                $("#cus_ten").html('');
                $("#cus_twowenty").html('');

                var url = BASE_URL + '/graphReportAPI/getTopTwentyCustomersDetails';

                var getpdfdatarequest = eb.post(url, {
                    cus_type: 1
                });
                getpdfdatarequest.done(function(data) {

                    $('#top_twowenty_cus_graph_div').show();
                    var graphCusTwentyData = data['twentyCusData'];

                    var a = Array();
                    for (var i = 0; i < graphCusTwentyData.length; i++) {
                        var CusName = graphCusTwentyData[i].CusName;
                        var cusName_split = CusName.split("-");
                        var Sales = parseFloat(Math.round((graphCusTwentyData[i].Sales) * 100) / 100).toFixed(2);
                        var Tax = parseFloat(Math.round((graphCusTwentyData[i].Tax) * 100) / 100).toFixed(2);
                        a.push({x: cusName_split[0], y: Sales, z: Tax});
                    }

                    Morris.Line({
                        element: 'cus_twowenty',
                        data: a,
                        xkey: 'x',
                        ykeys: ['y', 'z'],
                        labels: ['Sales', 'Tax'],
                        lineColors: ['#ffa8b0', '#abcdef'],
                        parseTime: false,
                        yLabelFormat: function(y) {
                            return 'Rs.' + accounting.formatMoney(y);
                        },
                        xLabelAngle: 70
                    });

                });

            }
            else {
                p_notification(false, eb.getMessage('ERR_GRAPHRE_SELECUST'));

            }
        }
    });

    /* validation function for Reporting Form
     * @author SANDUN <sandun@thinkcube.com>
     * */
    function validteinput(inputs) {
        var from_date = inputs[0];
        var to_date = inputs[1];
        if (from_date == null || from_date == "") {
            p_notification(false, eb.getMessage('ERR_DAISALE_FROMD'));
            document.getElementById("from_date").focus();
        } else if (to_date == null || to_date == "") {
            p_notification(false, eb.getMessage('ERR_DAISALE_TOD'));
            document.getElementById("to_date").focus();
        } else {
            return true;
        }

    }

    //layout issue
    $(window).bind('resize ready', function() {
        if ($(window).width() <= 480) {
            $('.graph_label_set_date, .graph_label_to_date').css('margin-bottom', '15px').removeClass('col-xs-6').addClass('col-xs-12');
            $('#view_graph').removeClass('btn-default').addClass('col-xs-12 btn-primary');
            $('#btn_invpay, #btn_allCus, #btn_toptenCus, #btn_toptwowentyCus').css('margin-bottom', '15px');
        } else {
            $('.graph_label_set_date, .graph_label_to_date').css('margin-bottom', '0px').addClass('col-xs-6').removeClass('col-xs-12');
            $('#view_graph').addClass('btn-default').removeClass('col-xs-12 btn-primary');
            $('#btn_invpay, #btn_allCus, #btn_toptenCus, #btn_toptwowentyCus').css('margin-bottom', '0px');
        }
    });

});