$(document).ready(function(){
    
    reset();
    //initialize datetimepicker
    $('.datetimepicker').datetimepicker({
        format: 'yyyy-mm-dd',
        minView: 4,
        autoclose: true,
    });
    
    //load account list
    $(document).on('change','#bankList',function (){
        var bankId = $(this).val();
        if(bankId){
            getBankAccountListByBankId(bankId);
        } else {
            $('#accountList').empty();
            $('#accountList').append($("<option>", { value: '', html: '----- Select Account -----' }));
        }
    });
    
    $(document).on('click','#reconciliationDetails',function (){
        reset();
    });
    
    //for report btn
    $('.report-btn').on('click',function(){
        
        var btnId = $(this).prop('id');
        var bankId = $('#bankList').val();
        var accountId = $('#accountList').val();
        var fromDate  = $('#startDate').val();
        var toDate  = $('#endDate').val();
        
        if(bankId && !accountId){
            p_notification(false, 'please select account');
            $('#reconciliation-report').hide();
        } else if(fromDate && toDate && (fromDate > toDate)){
            p_notification(false, 'invalid date range');
            $('#reconciliation-report').hide();
        } else if((fromDate && !toDate) || (!fromDate && toDate)){
            p_notification(false, 'invalid date range');
            $('#reconciliation-report').hide();
        } else {
            switch (btnId){
                case 'viewReport' :
                    viewReport(accountId,fromDate,toDate);
                    break;

                case 'generatePdf' :
                    pdfReport(accountId,fromDate,toDate);
                    break;

                case 'csvReport' :
                    csvReport(accountId,fromDate,toDate);
                    break;

                default :
                    console.log('invalid option');
            }
        }
    });
    
    //get bank account list
    function getBankAccountListByBankId(bankId){
        $.ajax({
            type: 'POST',
            url: BASE_URL + '/account-api/bank-account-list-for-dropdown',
            data: {bankId : bankId},
            success: function(respond) {
                console.log(respond.data.list);
                //empty options
                $('#accountList').empty();
                $('#accountList').append($("<option>", { value: '', html: '----- Select Account -----' }));
                $(respond.data.list).each(function(i, v){ 
                    $('#accountList').append($("<option>", { value: v.value, html: v.text }));
                });
            }
        });
    }
    
    var viewReport = function(accountId,fromDate,toDate){
        console.log('view'+accountId);
        eb.ajax({
            type: 'POST',
            url: BASE_URL + '/api/reconciliation-report/generate-account-reconciliation',
            dataType: 'json',
            data: {accountId: accountId, fromDate: fromDate, toDate: toDate},
            success: function(respond) {
                if (respond.status == true) {
                    setContentToReport('reconciliation-report', respond);
                }
            }
        });
    }
    
    var pdfReport = function(accountId,fromDate,toDate){
        console.log('pdf'+accountId);
        eb.ajax({
            type: 'POST',
            url: BASE_URL + '/api/reconciliation-report/generate-account-reconciliation-pdf',
            dataType: 'json',
            data: {accountId: accountId, fromDate: fromDate, toDate: toDate},
            success: function(respond) {
                if (respond.status == true) {
                    window.open(BASE_URL + '/' + respond.data, '_blank');
                }
            }
        });
    }
    
    var csvReport = function(accountId,fromDate,toDate){
        console.log('csv'+accountId);
        eb.ajax({
            type: 'POST',
            url: BASE_URL + '/api/reconciliation-report/generate-account-reconciliation-csv',
            dataType: 'json',
            data: {accountId: accountId, fromDate: fromDate, toDate: toDate},
            success: function(respond) {
                if (respond.status == true) {
                    window.open(BASE_URL + '/' + respond.data, '_blank');
                }
            }
        });
    }
    
    function reset(){
        $('#bankList,#accountList').val("");
        $('#startDate,#endDate').val("");
        $('#reconciliation-report').hide();
    }
    
});


