
$(document).ready(function() {

///////DatePicker for two text Filed\\\\\\\
    var fm, tm;
    var reportType = 'Monthly';
    var locationIds = $('#branch-locations').val();
    var isAllInvoices = 0;
    var isAllCustomers = 0;
    var isAllItems = 0;
    var isAllCategory = false;
    var checkin = $('#fromDate').datepicker({
    }).on('changeDate', function(ev) {
        if (ev.date.valueOf() < checkout.date.valueOf()) {
            var newDate = new Date(ev.date)
            newDate.setDate(newDate.getDate());
            checkout.setValue(newDate);
        }
        checkin.hide();
        $('#toDate')[0].focus();
    }).data('datepicker');
    var checkout = $('#toDate').datepicker({
        onRender: function(date) {
            return date.valueOf() <= checkin.date.valueOf();
        }
    }).on('changeDate', function(ev) {
        checkout.hide();
    }).data('datepicker');
    $("#fromMonth").datepicker({
        format: "yyyy-mm",
        viewMode: "months",
        minViewMode: 'months'
    });
    $("#toMonth").datepicker({
        format: "yyyy-mm",
        viewMode: "months",
        minViewMode: 'months'
    });
    ///////Month Pick for two text Filed\\\\\\\
    var checkinMonth = $('#fromMonth').datepicker({
    }).on('changeDate', function(ev) {
        var newD = new Date(ev.date.getFullYear(), ev.date.getMonth() + 1);
        checkoutMonth.setValue(newD);
        checkinMonth.hide();
        $('#toMonth')[0].focus();
    }).data('datepicker');
    var checkoutMonth = $('#toMonth').datepicker({
    }).on('changeDate', function(ev) {
        checkoutMonth.hide();
    }).data('datepicker');
    /////Pick date/////////////
    $('#date').datepicker();
    //////Pick year////////////
    $("#year").datepicker({
        format: " yyyy",
        viewMode: "years",
        minViewMode: 'years'
    });
    var checkinYear = $('#year').datepicker({
    }).on('changeDate', function(ev) {
        var newD = new Date(ev.date.getYear());
        checkinYear.hide();
    }).data('datepicker');
    initialCondition();

    loadDropDownFromDatabase('/invoice-api/search-location-wise-open-sales-invoices-for-document-dropdown', locationIds, '', '#invoiceName', "", true);
    loadDropDownFromDatabase('/customerAPI/search-customers-for-dropdown', "", "withDeactivatedCustomers", '#customerName', "", true);
    loadDropDownFromDatabase('/productAPI/search-user-active-location-products-for-dropdown', "", 0, '#itemList', "", true);
    loadDropDownFromDatabase('/api/company/category/search-categories-for-dropdown', "", 0, '#categoryList', "", true);

    $('#Monthly').click(function(e) {
        reportType = 'Monthly';
        $("#locationList").val("").selectpicker("refresh");
        $('#tax').attr('checked', false);
        $('#toMonth,#fromMonth').val('');
        //hide
        $('#year,#fromDate,#toDate,#date').hide();
        $('#salesReport').hide();
        $('#salesGraphReport').hide();
        $('.from_year_lbl').addClass('hidden');
        $('.to_date_lbl').addClass('hidden');
        $('.from_date_lbl').addClass('hidden');
        $('.sales-person').addClass('hidden');
        $('#user-selection').addClass('hidden');
        $('#location-selection').removeClass('hidden');
        $('#div_invoice_payment').addClass('hidden');
        $('.customerSelection').addClass('hidden');
        $('#item_div').addClass('hidden');
        $('#cus-category-selection').addClass('hidden');
        $('.status_div').addClass('hidden');
        //show
        $('#viewGraph,#tax,#lblTax').show();
        $('#fromMonth,#toMonth').show();
        $('.to').show();
        $('.to_month_lbl').show();
        $('.from_month_lbl').show();
        $('.tax_chk_lbl').show();
        $('.tax-type').addClass('hidden');
        $('#payment-term-div').removeClass('hidden');
        $('#category_div').addClass('hidden');
    });
    $('#Daily').click(function(e) {
        reportType = 'Daily';
        $("#locationList").val("").selectpicker("refresh");
        $('#tax').attr('checked', false);
        $('#fromDate,#toDate').val('');
        //hidden
        $('.status_div').addClass('hidden');
        $('#fromMonth,#toMonth,#year').hide();
        $('#salesReport').hide();
        $('#salesGraphReport').hide();
        $('#salessummery_report_view').hide();
        $('#paymentsummery_report_view').hide();
        $('.from_month_lbl').hide();
        $('.to_month_lbl').hide();
        $('.from_year_lbl').addClass('hidden');
        $('.sales-person').addClass('hidden');
        $('#user-selection').addClass('hidden');
        $('#location-selection').removeClass('hidden');
        $('#div_invoice_payment').addClass('hidden');
        $('.customerSelection').addClass('hidden');
        $('#item_div').addClass('hidden');
        $('#cus-category-selection').addClass('hidden');
        //show
        $('#fromDate,#toDate,#date').show();
        $('#viewGraph,#tax,#lblTax').show();
        $('.to').show();
        $('.from_date_lbl').removeClass('hidden');
        $('.to_date_lbl').removeClass('hidden');
        $('.tax_chk_lbl').show();
        $('.tax-type').addClass('hidden');
        $('#payment-term-div').removeClass('hidden');
        $('#category_div').addClass('hidden');
    });
    $('#Annual').click(function(e) {
        reportType = 'Annual';
        $("#locationList").val("").selectpicker("refresh");
        $('#tax').attr('checked', false);
        $('#year').val('');
        $('.status_div').addClass('hidden');
        //hide
        $('#fromDate,#toDate,#fromMonth,#toMonth,#date').hide();
        $('#salesReport').hide();
        $('#salesGraphReport').hide();
        $('.to_date_lbl').addClass('hidden');
        $('.from_date_lbl').addClass('hidden');
        $('.from_month_lbl').hide();
        $('.to_month_lbl').hide();
        $('.to').hide();
        $('.sales-person').addClass('hidden');
        $('#user-selection').addClass('hidden');
        $('#location-selection').removeClass('hidden');
        $('#div_invoice_payment').addClass('hidden');
        $('.customerSelection').addClass('hidden');
        $('#item_div').addClass('hidden');
        $('#cus-category-selection').addClass('hidden');
        //show
        $('#viewGraph,#tax,#lblTax,#year').show();
        $('.from_year_lbl').removeClass('hidden');
        $('.tax_chk_lbl').show();
        $('.tax-type').addClass('hidden');
        $('#payment-term-div').removeClass('hidden');
        $('#category_div').addClass('hidden');
    });

    $('#CustomerWiseOpenInvoice').click(function(e) {
        reportType = 'CustomerWiseOpenInvoice';

        //hide
        $('#fromDate,#toDate,#fromMonth,#toMonth,#date,#year,#lblTax').hide();
        $('#salesReport').hide();
        $('#salesGraphReport').hide();
        $('.to_date_lbl').addClass('hidden');
        $('.from_date_lbl').addClass('hidden');
        $('.from_month_lbl').hide();
        $('.status_div').addClass('hidden');
        $('.to_month_lbl').hide();
        $('.to').hide();
        $('.from_year_lbl').addClass('hidden');
        $('.tax_chk_lbl').hide();
        $('.sales-person').addClass('hidden');
        $('#user-selection').addClass('hidden');
        // $('#location-selection').addClass('hidden');
        $('.tax-type').removeClass('hidden');
        $('#payment-term-div').addClass('hidden');
        $('#div_invoice_payment').addClass('hidden');
        $('#item_div').addClass('hidden');
        $('#cus-category-selection').addClass('hidden');
        $('#location-selection').removeClass('hidden');
        $('#category_div').addClass('hidden');
        //show
        $('.customerSelection').removeClass('hidden');
        if (isAllCustomers) {
            $('#customCustomerSelection').trigger('click');
        }
    });

    $('#CustomerWiseOpenInvoiceWithCreditNote').click(function(e) {
        reportType = 'CustomerWiseOpenInvoiceWithCreditNote';

        //hide
        $('#fromDate,#toDate,#fromMonth,#toMonth,#date,#year,#lblTax').hide();
        $('#salesReport').hide();
        $('#salesGraphReport').hide();
        $('.to_date_lbl').addClass('hidden');
        $('.from_date_lbl').addClass('hidden');
        $('.from_month_lbl').hide();
        $('.to_month_lbl').hide();
        $('.status_div').addClass('hidden');
        $('.to').hide();
        $('.from_year_lbl').addClass('hidden');
        $('.tax_chk_lbl').hide();
        $('#user-selection').addClass('hidden');
        $('#location-selection').addClass('hidden');
        $('.tax-type').removeClass('hidden');
        $('#payment-term-div').addClass('hidden');
        $('#div_invoice_payment').addClass('hidden');
        $('#item_div').addClass('hidden');
        $('#cus-category-selection').addClass('hidden');
        $('#category_div').addClass('hidden');
        //show
        $('.customerSelection').removeClass('hidden');
        if (isAllCustomers) {
            $('#customCustomerSelection').trigger('click');
        }
        $('.sales-person').removeClass('hidden');
        $('.sales-person').parent().css("float", "none");
    });

    $('#Summery').click(function(e) {
        reportType = 'Summery';
        //hide
        $('#tax,#lblTax').hide();
        $('#viewGraph').hide();
        $('#fromMonth,#toMonth,#date,#year').hide();
        $('#salesReport').hide();
        $('#salesGraphReport').hide();
        $('.from_month_lbl').hide();
        $('.status_div').addClass('hidden');
        $('.to_month_lbl').hide();
        $('.from_year_lbl').addClass('hidden');
        $('.tax_chk_lbl').hide();
        $('.sales-person').addClass('hidden');
        $('#user-selection').addClass('hidden');
        $('#location-selection').addClass('hidden');
        $('#div_invoice_payment').addClass('hidden');
        $('.customerSelection').addClass('hidden');
        $('#item_div').addClass('hidden');
        $('#cus-category-selection').addClass('hidden');
        $('#category_div').addClass('hidden');
        //show
        $('.to').show();
        $('.to_date_lbl').removeClass('hidden');
        $('.from_date_lbl').removeClass('hidden');
        $('#fromDate,#toDate').show();
        $('#fromDate,#toDate').val('');
        $('.tax-type').addClass('hidden');
        $('#payment-term-div').addClass('hidden');
    });
    $('#salesPersonWiseOustanding').click(function(e) {
        reportType = 'salesPersonWiseOustanding';
        $('#tax').attr('checked', false);
        //hide
        //$('#tax,#lblTax').hide();
        $('#viewGraph').hide();
        $('.status_div').addClass('hidden');
        $('#fromMonth,#toMonth,#date,#year').hide();
        $('#salesReport').hide();
        $('#salesGraphReport').hide();
        $('.from_month_lbl').hide();
        $('.to_month_lbl').hide();
        $('.from_year_lbl').addClass('hidden');
        $('.tax_chk_lbl').hide();
        $('.sales-person').removeClass('hidden');
        $('#user-selection').addClass('hidden');
        $('#location-selection').addClass('hidden');
        $('#div_invoice_payment').addClass('hidden');
        $('.customerSelection').addClass('hidden');
        $('#category_div').addClass('hidden');
        //show
        $('.to').show();
        $('.to_date_lbl').removeClass('hidden');
        $('.tax_chk_lbl').show();
        $('#tax,#lblTax').show();
        $('.from_date_lbl').removeClass('hidden');
        $('#fromDate,#toDate').show();
        $('#fromDate,#toDate').val('');
        $('.tax-type').addClass('hidden');
        $('#payment-term-div').addClass('hidden');
        $('.customerSelection').removeClass('hidden');
        $('#item_div').removeClass('hidden');
        $('#cus-category-selection').removeClass('hidden');
        if (isAllCustomers) {
            $('#customCustomerSelection').trigger('click');
        }
        $('.sales-person').parent().css("float", "");
    });
    $('#issuedInvItem').on('click', function(e) {
        reportType = 'issuedInvItem';
        $('#tax,#lblTax').hide();
        $('#viewGraph').hide();
        $('#fromMonth,#toMonth,#date,#year').hide();
        $('#salesReport').hide();
        $('#salesGraphReport').hide();
        $('.from_month_lbl').hide();
        $('.to_month_lbl').hide();
        $('.from_year_lbl').addClass('hidden');
        $('.tax_chk_lbl').hide();
        $('.sales-person').addClass('hidden');
        $('#user-selection').addClass('hidden');
        $('#div_invoice_payment').addClass('hidden');
        $('.customerSelection').addClass('hidden');
        $('#item_div').addClass('hidden');
        $('.status_div').addClass('hidden');
        $('#cus-category-selection').addClass('hidden');
        //show
        $('.to').show();
        $('#location-selection').removeClass('hidden');
        $('.to_date_lbl').removeClass('hidden');
        $('.from_date_lbl').removeClass('hidden');
        $('#fromDate,#toDate').show();
        $('#fromDate,#toDate').val('');
        $('.tax-type').addClass('hidden');
        $('#payment-term-div').addClass('hidden');
        $('#category_div').removeClass('hidden');
    });
    function hideGraphDiv() {
        $('#salesGraphReport').hide();
        $('#salesGraphReport').html('');
    }

    $('#cancelledInvoice').click(function(e) {
        reportType = 'cancelledInvoice';
        //hide
        $('#tax,#lblTax').hide();
        $('#viewGraph').hide();
        $('#fromMonth,#toMonth,#date,#year').hide();
        $('#salesReport').hide();
        $('.status_div').addClass('hidden');
        $('#salesGraphReport').hide();
        $('.from_month_lbl').hide();
        $('.to_month_lbl').hide();
        $('.from_year_lbl').addClass('hidden');
        $('.tax_chk_lbl').hide();
        $('.sales-person').addClass('hidden');
        $('.to').hide();
        $('.to_date_lbl').addClass('hidden');
        $('.from_date_lbl').addClass('hidden');
        $('#fromDate,#toDate').hide();
        $('#fromDate,#toDate').val('');
        $('#div_invoice_payment').addClass('hidden');
        $('#item_div').addClass('hidden');
        $('#cus-category-selection').addClass('hidden');
        $('.customerSelection').addClass('hidden');
        $('#category_div').addClass('hidden');
        //show
        $('.customerSelection').removeClass('hidden');
        $('#location-selection').removeClass('hidden');
        $('#user-selection').removeClass('hidden');
        $('#fromDate,#toDate,#date').show();
        $('.to').show();
        $('.from_date_lbl').removeClass('hidden');
        $('.to_date_lbl').removeClass('hidden');
        $('.tax-type').addClass('hidden');
        $('#payment-term-div').addClass('hidden');
    });

    $('#editedInvoice').click(function(e) {
        reportType = 'editedInvoice';
        //hide
        $('#tax,#lblTax').hide();
        $('#viewGraph').hide();
        $('#fromMonth,#toMonth,#date,#year').hide();
        $('#salesReport').hide();
        $('#salesGraphReport').hide();
        $('.from_month_lbl').hide();
        $('.status_div').addClass('hidden');
        $('.to_month_lbl').hide();
        $('.from_year_lbl').addClass('hidden');
        $('.tax_chk_lbl').hide();
        $('.sales-person').addClass('hidden');
        $('.to').hide();
        $('.to_date_lbl').addClass('hidden');
        $('.from_date_lbl').addClass('hidden');
        $('#fromDate,#toDate').hide();
        $('#fromDate,#toDate').val('');
        $('#location-selection').addClass('hidden');
        $('#div_invoice_payment').addClass('hidden');
        $('#item_div').addClass('hidden');
        $('#cus-category-selection').addClass('hidden');
        $('.customerSelection').addClass('hidden');
        $('#category_div').addClass('hidden');
        //show
        $('.customerSelection').removeClass('hidden');
        $('#user-selection').removeClass('hidden');
        $('#fromDate,#toDate,#date').show();
        $('.to').show();
        $('.from_date_lbl').removeClass('hidden');
        $('.to_date_lbl').removeClass('hidden');
        $('.tax-type').addClass('hidden');
        $('#payment-term-div').addClass('hidden');
    });

    $('#invoicePayments').click(function() {
        reportType = 'invoicePayments';
        //hide
        $('#tax,#lblTax').hide();
        $('#viewGraph').hide();
        $('#fromMonth,#toMonth,#date,#year').hide();
        $('#salesReport').hide();
        $('#salesGraphReport').hide();
        $('.from_month_lbl').hide();
        $('.to_month_lbl').hide();
        $('.status_div').addClass('hidden');
        $('.from_year_lbl').addClass('hidden');
        $('.tax_chk_lbl').hide();
        $('.sales-person').addClass('hidden');
        $('.to').hide();
        $('.to_date_lbl').addClass('hidden');
        $('.from_date_lbl').addClass('hidden');
        $('#fromDate,#toDate').hide();
        $('#fromDate,#toDate').val('');
        $('#location-selection').addClass('hidden');
        $('#payment-term-div').addClass('hidden');
        $('#user-selection').addClass('hidden');
        $('.tax-type').addClass('hidden');
        $('#div_invoice_payment').addClass('hidden');
        $('#item_div').addClass('hidden');
        $('#cus-category-selection').addClass('hidden');
        $('.customerSelection').addClass('hidden');
        $('#category_div').addClass('hidden');
        //show
        $('.customerSelection').removeClass('hidden');
        $('#div_invoice_payment').removeClass('hidden');
        $('#fromDate,#toDate,#date').show();
        $('.to').show();
        $('.from_date_lbl').removeClass('hidden');
        $('.to_date_lbl').removeClass('hidden');

    });

    $('#invoiceDetails').click(function() {
        reportType = 'invoiceDetails';
        //hide
        $('#tax,#lblTax').hide();
        $('#viewGraph').hide();
        $('#fromMonth,#toMonth,#date,#year').hide();
        $('#salesReport').hide();
        $('.status_div').addClass('hidden');
        $('#salesGraphReport').hide();
        $('.from_month_lbl').hide();
        $('.to_month_lbl').hide();
        $('.from_year_lbl').addClass('hidden');
        $('.tax_chk_lbl').hide();
        $('.sales-person').addClass('hidden');
        $('.to').hide();
        $('.to_date_lbl').addClass('hidden');
        $('.from_date_lbl').addClass('hidden');
        $('#fromDate,#toDate').hide();
        $('#fromDate,#toDate').val('');
        $('#location-selection').addClass('hidden');
        $('#payment-term-div').addClass('hidden');
        $('#user-selection').addClass('hidden');
        $('.tax-type').addClass('hidden');
        $('#div_invoice_payment').addClass('hidden');
        $('#item_div').addClass('hidden');
        $('.customerSelection').addClass('hidden');
        $('#cus-category-selection').addClass('hidden');
        $('#category_div').addClass('hidden');
        //show
        $('#div_invoice_payment').removeClass('hidden');
        $('#fromDate,#toDate,#date').show();
        $('.to').show();
        $('.from_date_lbl').removeClass('hidden');
        $('.to_date_lbl').removeClass('hidden');
        $('.customerSelection').removeClass('hidden');
    });
    
    $('#invoiceDueDateReport').click(function() {
        reportType = 'invoiceDueDateReport';
        //hide
        $('#tax,#lblTax').hide();
        $('#viewGraph').hide();
        $('#fromMonth,#toMonth,#date,#year').hide();
        $('#salesReport').hide();
        $('#salesGraphReport').hide();
        $('.from_month_lbl').hide();
        $('.to_month_lbl').hide();
        $('.from_year_lbl').addClass('hidden');
        $('.tax_chk_lbl').hide();
        $('.sales-person').addClass('hidden');
        $('#user-selection').addClass('hidden');
        $('#div_invoice_payment').addClass('hidden');
        $('#item_div').addClass('hidden');
        $('#cus-category-selection').addClass('hidden');
        $('.tax-type').addClass('hidden');
        $('#payment-term-div').addClass('hidden');
        $('#category_div').addClass('hidden');
        //show
        $('.customerSelection').removeClass('hidden');
        $('.to').show();
        $('#location-selection').removeClass('hidden');
        $('.to_date_lbl').removeClass('hidden');
        $('.from_date_lbl').removeClass('hidden');
        $('#fromDate,#toDate').show();
        $('#fromDate,#toDate').val('');
        $('.status_div').removeClass('hidden');
    });

    $('#invoiceDetailsWithCustomer').click(function() {
        reportType = 'invoiceDetailsWithCustomer';
        //hide
        $('#tax,#lblTax').hide();
        $('#viewGraph').hide();
        $('#fromMonth,#toMonth,#date,#year').hide();
        $('#salesReport').hide();
        $('#salesGraphReport').hide();
        $('.from_month_lbl').hide();
        $('.to_month_lbl').hide();
        $('.from_year_lbl').addClass('hidden');
        $('.tax_chk_lbl').hide();
        $('.sales-person').addClass('hidden');
        $('#user-selection').addClass('hidden');
        $('#div_invoice_payment').addClass('hidden');
        $('#item_div').addClass('hidden');
        $('#cus-category-selection').addClass('hidden');
        $('.tax-type').addClass('hidden');
        $('#payment-term-div').addClass('hidden');
        $('#category_div').addClass('hidden');
        //show
        $('.customerSelection').addClass('hidden');
        $('.to').show();
        $('#location-selection').removeClass('hidden');
        $('.to_date_lbl').removeClass('hidden');
        $('.from_date_lbl').removeClass('hidden');
        $('#fromDate,#toDate').show();
        $('#fromDate,#toDate').val('');
        $('.status_div').addClass('hidden');
    });

    $(document).on('change', '#branch-locations', function() {
        locationIds = $('#branch-locations').val();
        $('#invoiceName').siblings('div.bootstrap-select').remove();
        $('#invoiceName').removeData().empty();
        loadDropDownFromDatabase('/invoice-api/search-location-wise-open-sales-invoices-for-document-dropdown', locationIds, 0, '#invoiceName', "", true);
        $('button[data-id="invoiceName"]').removeClass('disabled');
    });

    $('.isAllInvoices').on('click', function() {
        isAllInvoices = $(this).val();
        if (isAllInvoices == true) {
            $('#invoiceName').val('').trigger('change');
            $('#invoiceName').prop('disabled', true);
        } else {
            $('#invoiceName').prop('disabled', false);
        }
    });

    $('.isAllCustomers').on('click', function() {
        isAllCustomers = $(this).val();
        if (isAllCustomers == true) {
            $('#customerName').val('').trigger('change');
            $('#customerName').prop('disabled', true);
            p_notification('info', eb.getMessage('INFO_CUS_REPORT_ALL_CUS_SELECTION'));
        } else {
            $('#customerName').prop('disabled', false);
        }
        $('#customerName').selectpicker('refresh');
    });

    $('.isAllItems').on('click', function() {
        isAllItems = $(this).val();
        if (isAllItems == true) {
            $('#itemList').val('').trigger('change');
            $('#itemList').prop('disabled', true);
        } else {
            $('#itemList').prop('disabled', false);
        }
    });

    $('#viewReport').click(function(e) {
        if (reportType == 'Monthly') {
            var checkTaxStatus = getCheckedStatus();
            var getpdfdataurl = BASE_URL + '/report/get/invoice/monthly-invoice-view';
            var fromMonth = $('#fromMonth').val();
            var fromMonthSplit = fromMonth.split("-");
            var toMonth = $('#toMonth').val();
            var toMonthSplit = toMonth.split("-");
            var paymentTerm = $('#paymentTermList').val();
            var locationID = $('#locationList').val();

            fromday(fromMonthSplit[1]);
            today(toMonthSplit[1], toMonthSplit[0]);
            if ($('#fromMonth').val() == null || $('#fromMonth').val() == "") {
                p_notification(false, eb.getMessage("ERR_SALEREPORT_FROMMONTH"));
                $('#fromMonth').focus();
                return false;
            }
            else if ($('#toMonth').val() == null || $('#toMonth').val() == "") {
                p_notification(false, eb.getMessage("ERR_SALEREPORT_TOMONTH"));
                $('#toMonth').focus();
                return false;
            } else if ($('#toMonth').val() < $('#fromMonth').val()) {
                p_notification(false, eb.getMessage('ERR_SALEREPORT_DEFF'));
                $('#toMonth').focus();
                return false;
            } else {
                var getpdfdatarequest = eb.post(getpdfdataurl, {
                    fromMonth: fm,
                    toMonth: tm,
                    checkTaxStatus: checkTaxStatus,
                    paymentTerm: paymentTerm,
                    locationID: locationID
                });
                getpdfdatarequest.done(function(respond) {
                    if (respond.status == true) {
                        setContentToReport('salesReport', respond);
                    }
                });
            }
        }
        else if (reportType == 'Daily') {
            $('#btnDaily').hide();
            var checkTaxStatus = getCheckedStatus();
            var getpdfdataurl = BASE_URL + '/report/get/invoice/daily-invoice-view';
            var paymentTerm = $('#paymentTermList').val();
            var locationID = $('#locationList').val();

            inputs = new Array(
                    $('#fromDate').val(),
                    $('#toDate').val());
            if (validteinput(inputs)) {
                var getpdfdatarequest = eb.post(getpdfdataurl, {
                    fromDate: $('#fromDate').val(),
                    toDate: $('#toDate').val(),
                    checkTaxStatus: checkTaxStatus,
                    paymentTerm: paymentTerm,
                    locationID: locationID
                });
                getpdfdatarequest.done(function(respond) {
                    if (respond.status == true) {
                        setContentToReport('salesReport', respond);
                    }
                });
            }

        }
        else if (reportType == 'Annual') {
            $('#btnAnnual').hide();
            var checkTaxStatus = getCheckedStatus();
            var year = $('#year').val();
            var paymentTerm = $('#paymentTermList').val();
            var locationID = $('#locationList').val();

            year = year.replace(/\s+/g, "");
            $.trim(year);
            var getannualdataurl = BASE_URL + '/report/get/invoice/annual-invoice-view';
            inputs = new Array(
                    $('#year').val()
                    );
            if ($('#year').val() == null || $('#year').val() == "") {
                p_notification(false, eb.getMessage('ERR_SALEREPORT_YEAR'));
                $('#year').focus();
                return false;
            } else {
                var getannualdatarequest = eb.post(getannualdataurl, {
                    year: year,
                    checkTaxStatus: checkTaxStatus,
                    paymentTerm: paymentTerm,
                    locationID: locationID
                });
                getannualdatarequest.done(function(respond) {
                    if (respond.status == true) {
                        setContentToReport('salesReport', respond);
                    }
                });
            }

        }
        else if (reportType == 'Summery') {
            var getdataurl = BASE_URL + '/report/get/invoice/summery-invoice-view';
            inputs = new Array(
                    $('#fromDate').val(),
                    $('#toDate').val()
                    );
            if (validteinput(inputs)) {
                var getdatarequest = eb.post(getdataurl, {
                    fromDate: $('#fromDate').val(),
                    toDate: $('#toDate').val()
                });
                getdatarequest.done(function(respond) {
                    if (respond.status == true) {
                        setContentToReport('salesReport', respond);
                    }
                });
            }
        }
        else if (reportType == 'salesPersonWiseOustanding') {
            var checkTaxStatus = getCheckedStatus();
            var input = {
                fromDate : $('#fromDate').val(),
                toDate : $('#toDate').val(),
                salesPersonIds : $("#salesPersons").val(),
                isAllCustomers : isAllCustomers,
                customerIds : $('#customerName').val(),
                isAllItems : isAllItems,
                itemIds : $('#itemList').val(),
                casCategory : $('#cusCategory').val(),
                showTax : checkTaxStatus
            }

            if (salesPersonWiseOustandingFormValidation(input)) {
                eb.ajax({
                    type: 'POST',
                    url: BASE_URL + '/report/get/invoice/view-outstanding-balance',
                    dataType: 'json',
                    data: input,
                    success: function(respond) {
                        if (respond.status == true) {
                            setContentToReport('salesReport', respond);
                        }
                    }
                });
            }
        }
        else if (reportType == 'CustomerWiseOpenInvoice') {

            var getpdfdataurl = BASE_URL + '/report/get/invoice/open-invoice-report';
            var taxType = $("#taxType").val();
            var customerIds = $('#customerName').val();
            var locationID = $('#locationList').val();

            if(isAllCustomers == true || customerIds != null) {
                var getpdfdatarequest = eb.post(getpdfdataurl, {
                    taxType : taxType,
                    isAllCustomers : isAllCustomers,
                    customerIds : customerIds,
                    locationIDs: locationID
                });
                getpdfdatarequest.done(function(respond) {
                    if (respond.status == true) {
                        setContentToReport('salesReport', respond);
                    }
                });
            } else {
                p_notification(false, eb.getMessage('ERR_CUSREPO_SELECUST'));
            }

        } else if (reportType == 'cancelledInvoice') {
            var getpdfdataurl = BASE_URL + '/report/get/invoice/viewCancelledInvoice';
            var locations = $('#locationList').val();

            if (locations == null || locations.length == 0) {
                p_notification(false, eb.getMessage('ERR_PAYVOUCHER_SELELOC'));
                return false;
            }
            var inputs = [
                $('#fromDate').val(),
                $('#toDate').val(),
            ];
            if (!($('#fromDate').val() && $('#toDate').val()) || validteinput(inputs))
            {
                var getpdfdatarequest = eb.post(getpdfdataurl, {
                    fromDate: $('#fromDate').val(),
                    toDate: $('#toDate').val(),
                    users: $('#users').val(),
                    locations: $('#locationList').val(),
                    isAllCustomers : isAllCustomers,
                    customerIds : $('#customerName').val(),
                });
                getpdfdatarequest.done(function(respond) {
                    if (respond.status) {
                        setContentToReport('salesReport', respond);
                    }
                });
            }
        } else if (reportType == 'editedInvoice') {
            var getpdfdataurl = BASE_URL + '/report/get/invoice/viewEditedInvoice';

            var inputs = [
                $('#fromDate').val(),
                $('#toDate').val(),
            ];
            if (!($('#fromDate').val() && $('#toDate').val()) || validteinput(inputs))
            {
                var getpdfdatarequest = eb.post(getpdfdataurl, {
                    fromDate: $('#fromDate').val(),
                    toDate: $('#toDate').val(),
                    users: $('#users').val(),
                    isAllCustomers : isAllCustomers,
                    customerIds : $('#customerName').val(),
                });
                getpdfdatarequest.done(function(respond) {
                    if (respond.status) {
                        setContentToReport('salesReport', respond);
                    }
                });
            }
        } else if (reportType == 'invoicePayments') {
            var url = BASE_URL + '/report/get/invoice/invoice-payment-details-report';
            var input = {
                fromDate: $('#fromDate').val(),
                toDate: $('#toDate').val(),
                isAllCustomers : isAllCustomers,
                customerIds : $('#customerName').val(),
                isAllInvoices: isAllInvoices,
                invoiceIds: $('#invoiceName').val(),
                locationIds: locationIds
            };

            if (paymentDetailsFromValidation(input)) {
                var request = eb.post(url, input);
                request.done(function(respond) {
                    if (respond.status) {
                        setContentToReport('salesReport', respond);
                    }
                });
            }

        } else if (reportType == 'issuedInvItem') {
            var fromDate = $('#fromDate').val();
            var toDate = $('#toDate').val();
            var locIds = $('#locationList').val();
            var isAllCategories = $('input[name="isAllCategories"]:checked').val();
            var categoryIds = $('#categoryList').val();
            if (fromDate == '' || fromDate == null) {
                p_notification(false, eb.getMessage('ERR_DAISALE_FROMD'));
                document.getElementById("fromDate").focus();
            } else if (toDate == '' || toDate == null) {
                p_notification(false, eb.getMessage('ERR_DAISALE_TOD'));
                document.getElementById("toDate").focus();
            } else if (toDate < fromDate) {
                p_notification(false, eb.getMessage('ERR_SALEREPORT_DEFF'));
                return false;
            }
            else {
                var url = BASE_URL + '/report/get/invoice/view-issued-invoice';
                eb.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: {locationIdList: locIds, fromDate: fromDate, toDate: toDate, isAllCategories: isAllCategories, categoryIds: categoryIds},
                    success: function(respond) {
                        if (respond.status == true) {
                            setContentToReport('salesReport', respond);
                        }
                    }
                });
            }
        } else if (reportType == 'CustomerWiseOpenInvoiceWithCreditNote') {
            var url = BASE_URL + '/report/get/invoice/open-invoice-with-credit-note-report';
            var input = {
                taxType : $("#taxType").val(),
                isAllCustomers : isAllCustomers,
                customerIds : $('#customerName').val(),
                salesPersonIds : $("#salesPersons").val()
            }
            if(openInvoiceWithCreditNotesFormValidation(input)){
                eb.ajax({
                    type: 'POST',
                    url: url,
                    data: input,
                    success: function(respond) {
                        if (respond.status == true) {
                            setContentToReport('salesReport', respond);
                        }
                    }
                });
            }
        } else if (reportType == 'invoiceDetails') {
            var url = BASE_URL + '/report/get/invoice/invoice-details-report';
            var input = {
                fromDate: $('#fromDate').val(),
                toDate: $('#toDate').val(),
                isAllCustomers : isAllCustomers,
                customerIds : $('#customerName').val(),
                isAllInvoices: isAllInvoices,
                invoiceIds: $('#invoiceName').val(),
                locationIds: locationIds
            };

            if (paymentDetailsFromValidation(input)) {
                var request = eb.post(url, input);
                request.done(function(respond) {
                    if (respond.status) {
                        setContentToReport('salesReport', respond);
                    }
                });
            }
        }  else if (reportType == 'invoiceDueDateReport') {
            var url = BASE_URL + '/report/get/invoice/invoice-due-date-report';
            var input = {
                fromDate: $('#fromDate').val(),
                toDate: $('#toDate').val(),
                isAllCustomers : isAllCustomers,
                customerIds : $('#customerName').val(),
                locationIds: $('#locationList').val(),
                status:$("#status").val()
            };

            if (InvoiceDueDateReportInputValidation(input)) {
                var request = eb.post(url, input);
                request.done(function(respond) {
                    if (respond.status) {
                        setContentToReport('salesReport', respond);
                    }
                });
            }
        } else if (reportType == 'invoiceDetailsWithCustomer') {
            var url = BASE_URL + '/report/get/invoice/invoice-details-with-customer-report';
            var input = {
                fromDate: $('#fromDate').val(),
                toDate: $('#toDate').val(),
                locationIds: $('#locationList').val()
            };

            if (invoiceDetailsWithCustomerReportInputValidation(input)) {
                var request = eb.post(url, input);
                request.done(function(respond) {
                    if (respond.status) {
                        setContentToReport('salesReport', respond);
                    }
                });
            }
        } else {
            p_notification(false, "Please select a correct report type first");
        }
    });
    /* function for Pdf generate Reporting
     * @author SANDUN <sandun@thinkcube.com>
     * */
    $('#generatePdf').click(function(e) {
        if (reportType == 'Monthly') {
            var checkTaxStatus = getCheckedStatus();
            var fromMonth = $('#fromMonth').val();
            var fromMonthSplit = fromMonth.split("-");
            var toMonth = $('#toMonth').val();
            var toMonthSplit = toMonth.split("-");
            var paymentTerm = $('#paymentTermList').val();
            var locationID = $('#locationList').val();

            fromday(fromMonthSplit[1]);
            today(toMonthSplit[1], toMonthSplit[0]);
            if ($('#fromMonth').val() == null || $('#fromMonth').val() == "") {
                p_notification(false, eb.getMessage("ERR_SALEREPORT_FROMMONTH"));
                $('#fromMonth').focus();
                return false;
            }
            else if ($('#toMonth').val() == null || $('#toMonth').val() == "") {
                p_notification(false, eb.getMessage("ERR_SALEREPORT_TOMONTH"));
                $('#toMonth').focus();
                return false;
            } else if ($('#toMonth').val() < $('#fromMonth').val()) {
                p_notification(false, eb.getMessage('ERR_SALEREPORT_DEFF'));
                $('#toMonth').focus();
                return false;
            } else {
                var url = BASE_URL + "/report/get/invoice/monthly-invoice-pdf";
                eb.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: {
                        fromMonth: fm,
                        toMonth: tm,
                        checkTaxStatus: checkTaxStatus,
                        paymentTerm: paymentTerm,
                        locationID: locationID
                    },
                    success: function(respond) {
                        if (respond.status == true) {
                            window.open(BASE_URL + '/' + respond.data, '_blank');
                        }
                    }
                });
            }
        } else if (reportType == 'Daily') {
            var fromDate = $('#fromDate').val();
            var toDate = $('#toDate').val();
            var checkTaxStatus = getCheckedStatus();
            var paymentTerm = $('#paymentTermList').val();
            var locationID = $('#locationList').val();

            inputs = new Array(fromDate, toDate);
            if (validteinput(inputs)) {
                var url = BASE_URL + "/report/get/invoice/daily-invoice-pdf";
                eb.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: {
                        fromDate: fromDate,
                        toDate: toDate,
                        checkTaxStatus: checkTaxStatus,
                        paymentTerm: paymentTerm,
                        locationID: locationID
                    },
                    success: function(respond) {
                        if (respond.status == true) {
                            window.open(BASE_URL + '/' + respond.data, '_blank');
                        }
                    }
                });
            }
        } else if (reportType == 'Annual') {
            var checkTaxStatus = getCheckedStatus();
            var paymentTerm = $('#paymentTermList').val();
            var locationID = $('#locationList').val();

            if ($('#year').val() == null || $('#year').val() == "") {
                p_notification(false, eb.getMessage('ERR_SALEREPORT_YEAR'));
                $('#year').focus();
                return false;
            } else {
                var year = $("#year").val();
                year = year.replace(/\s+/g, "");
                $.trim(year);
                ;
                var url = BASE_URL + "/report/get/invoice/annual-invoice-pdf";
                eb.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: {
                        year: year,
                        checkTaxStatus: checkTaxStatus,
                        paymentTerm: paymentTerm,
                        locationID: locationID
                    },
                    success: function(respond) {
                        if (respond.status == true) {
                            window.open(BASE_URL + '/' + respond.data, '_blank');
                        }
                    }
                });
            }
        } else if (reportType == 'Summery') {
            inputs = new Array(
                    $('#fromDate').val(),
                    $('#toDate').val()
                    );
            if (validteinput(inputs)) {
                var fromDate = $("#fromDate").val();
                var toDate = $("#toDate").val();
                var url = BASE_URL + "/report/get/invoice/summery-invoice-pdf";
                eb.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: {fromDate: fromDate, toDate: toDate},
                    success: function(respond) {
                        if (respond.status == true) {
                            window.open(BASE_URL + '/' + respond.data, '_blank');
                        }
                    }
                });
            }
        } else if (reportType == 'Payments') {
            var fromDate = $('#fromDate').val();
            var toDate = $('#toDate').val();
            inputs = new Array(fromDate, toDate);
            if (validteinput(inputs)) {
                window.open(BASE_URL + "/report/get/invoice/generate-payments-summery-pdf?fromDate=" + fromDate + "&toDate=" + toDate, '_blank');
            }
        } else if (reportType == 'salesPersonWiseOustanding') {
            var checkTaxStatus = getCheckedStatus();
            var getpdfdataurl = BASE_URL + '/report/get/invoice/generate-outstanding-balance-pdf';
            var input = {
                fromDate : $('#fromDate').val(),
                toDate : $('#toDate').val(),
                salesPersonIds : $("#salesPersons").val(),
                isAllCustomers : isAllCustomers,
                customerIds : $('#customerName').val(),
                isAllItems : isAllItems,
                itemIds : $('#itemList').val(),
                casCategory : $('#cusCategory').val(),
                showTax : checkTaxStatus
            }

            if (salesPersonWiseOustandingFormValidation(input)){
                eb.ajax({
                    type: 'POST',
                    url: getpdfdataurl,
                    dataType: 'json',
                    data: input,
                    success: function(respond) {
                        if (respond.status == true) {
                            window.open(BASE_URL + '/' + respond.data, '_blank');
                        }
                    }
                });
            }
        }
        else if (reportType == 'CustomerWiseOpenInvoice') {
            var url = BASE_URL + "/report/get/invoice/open-invoice-pdf";
            var taxType = $("#taxType").val();
            var customerIds = $('#customerName').val();
            var locationID = $('#locationList').val();

            if(isAllCustomers == true || customerIds != null) {
                eb.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: {
                        taxType : taxType,
                        isAllCustomers : isAllCustomers,
                        customerIds : customerIds,
                        locationIDs : locationID

                    },
                    success: function(respond) {
                        if (respond.status == true) {
                            window.open(BASE_URL + '/' + respond.data, '_blank');
                        }
                    }
                });
            } else {
                p_notification(false, eb.getMessage('ERR_CUSREPO_SELECUST'));
            }
        }
        else if (reportType == 'CustomerWiseOpenInvoice') {
            var url = BASE_URL + "/report/get/invoice/open-invoice-pdf";
            var taxType = $("#taxType").val();
            var locationID = $('#locationList').val();
            eb.ajax({
                type: 'POST',
                url: url,
                dataType: 'json',
                data: {taxType: taxType, locationIDs :locationID},
                success: function(respond) {
                    if (respond.status == true) {
                        window.open(BASE_URL + '/' + respond.data, '_blank');
                    }
                }
            });

        }
        else if (reportType == 'cancelledInvoice') {
            var getpdfdataurl = BASE_URL + '/report/get/invoice/get-cancelled-invoice-pdf';
            var locations = $('#locationList').val();
            if (locations == null || locations.length == 0) {
                p_notification(false, eb.getMessage('ERR_PAYVOUCHER_SELELOC'));
                return false;
            }
            var inputs = [
                $('#fromDate').val(),
                $('#toDate').val(),
            ];
            if (!($('#fromDate').val() && $('#toDate').val()) || validteinput(inputs))
            {
                var getpdfdatarequest = eb.post(getpdfdataurl, {
                    fromDate: $('#fromDate').val(),
                    toDate: $('#toDate').val(),
                    users: $('#users').val(),
                    locations: $('#locationList').val(),
                    isAllCustomers : isAllCustomers,
                    customerIds : $('#customerName').val(),
                });
                getpdfdatarequest.done(function(respond) {
                    if (respond.status) {
                        window.open(BASE_URL + '/' + respond.data, '_blank');
                    }
                });
            }
        }
        else if (reportType == 'editedInvoice') {
            var getpdfdataurl = BASE_URL + '/report/get/invoice/generate-edited-invoice-pdf';
            var inputs = [
                $('#fromDate').val(),
                $('#toDate').val(),
            ];
            if (!($('#fromDate').val() && $('#toDate').val()) || validteinput(inputs))
            {
                var getpdfdatarequest = eb.post(getpdfdataurl, {
                    fromDate: $('#fromDate').val(),
                    toDate: $('#toDate').val(),
                    users: $('#users').val(),
                    isAllCustomers : isAllCustomers,
                    customerIds : $('#customerName').val(),
                });
                getpdfdatarequest.done(function(respond) {
                    if (respond.status) {
                        window.open(BASE_URL + '/' + respond.data, '_blank');
                    }
                });
            }
        } else if (reportType == 'invoicePayments') {
            var url = BASE_URL + '/report/get/invoice/invoice-payment-details-report-pdf';
            var input = {
                fromDate: $('#fromDate').val(),
                toDate: $('#toDate').val(),
                isAllCustomers : isAllCustomers,
                customerIds : $('#customerName').val(),
                isAllInvoices: isAllInvoices,
                invoiceIds: $('#invoiceName').val(),
                locationIds: locationIds
            };

            if (paymentDetailsFromValidation(input)) {
                eb.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: input,
                    success: function(respond) {
                        if (respond.status) {
                            window.open(BASE_URL + '/' + respond.data, '_blank');
                        }
                    }
                });
            }
        } else if (reportType == 'issuedInvItem') {
            var fromDate = $('#fromDate').val();
            var toDate = $('#toDate').val();
            var locIds = $('#locationList').val();
            var isAllCategories = $('input[name="isAllCategories"]:checked').val();
            var categoryIds = $('#categoryList').val();
            if (fromDate == '' || fromDate == null) {
                p_notification(false, eb.getMessage('ERR_DAISALE_FROMD'));
                document.getElementById("fromDate").focus();
            } else if (toDate == '' || toDate == null) {
                p_notification(false, eb.getMessage('ERR_DAISALE_TOD'));
                document.getElementById("toDate").focus();
            } else if (toDate < fromDate) {
                p_notification(false, eb.getMessage('ERR_SALEREPORT_DEFF'));
                return false;
            }
            else {
                var url = BASE_URL + '/report/get/invoice/generate-issued-invoice-item-pdf';
                eb.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: {locationIdList: locIds, fromDate: fromDate, toDate: toDate, isAllCategories: isAllCategories, categoryIds: categoryIds},
                    success: function(respond) {
                        if (respond.status == true) {
                            window.open(BASE_URL + '/' + respond.data, '_blank');
                        }
                    }
                });
            }
        } else if (reportType == 'CustomerWiseOpenInvoiceWithCreditNote') {
            var url = BASE_URL + "/report/get/invoice/open-invoice-with-credit-note-pdf";
            var input = {
                taxType : $("#taxType").val(),
                isAllCustomers : isAllCustomers,
                customerIds : $('#customerName').val(),
                salesPersonIds : $("#salesPersons").val()
            }
            if(openInvoiceWithCreditNotesFormValidation(input)){
                eb.ajax({
                    type: 'POST',
                    url: url,
                    data: input,
                    success: function(respond) {
                        if (respond.status == true) {
                            window.open(BASE_URL + '/' + respond.data, '_blank');
                        }
                    }
                });
            }
        } else if (reportType == 'invoiceDetails') {
            var url = BASE_URL + '/report/get/invoice/invoice-details-pdf';
            var input = {
                fromDate: $('#fromDate').val(),
                toDate: $('#toDate').val(),
                isAllCustomers : isAllCustomers,
                customerIds : $('#customerName').val(),
                isAllInvoices: isAllInvoices,
                invoiceIds: $('#invoiceName').val(),
                locationIds: locationIds
            };
            if (paymentDetailsFromValidation(input)) {
                eb.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: input,
                    success: function(respond) {
                        if (respond.status) {
                            window.open(BASE_URL + '/' + respond.data, '_blank');
                        }
                    }
                });
            }
        } else if (reportType == 'invoiceDueDateReport') {
            var url = BASE_URL + '/report/get/invoice/invoice-due-date-report-pdf';
            var input = {
                fromDate: $('#fromDate').val(),
                toDate: $('#toDate').val(),
                isAllCustomers : isAllCustomers,
                customerIds : $('#customerName').val(),
                locationIds: $('#locationList').val(),
                status:$("#status").val()
            };
            if (InvoiceDueDateReportInputValidation(input)) {
                eb.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: input,
                    success: function(respond) {
                        if (respond.status) {
                            window.open(BASE_URL + '/' + respond.data, '_blank');
                        }
                    }
                });
            }
        } else if (reportType == 'invoiceDetailsWithCustomer') {
            var url = BASE_URL + '/report/get/invoice/invoice-details-with-customer-report-pdf';
            var input = {
                fromDate: $('#fromDate').val(),
                toDate: $('#toDate').val(),
                locationIds: $('#locationList').val()
            };
            if (invoiceDetailsWithCustomerReportInputValidation(input)) {
                eb.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: input,
                    success: function(respond) {
                        if (respond.status) {
                            window.open(BASE_URL + '/' + respond.data, '_blank');
                        }
                    }
                });
            }
        } else {
            p_notification(false, "Please select a report type first");
        }

    });
    /* function for Excel Reporting
     * @author SANDUN <sandun@thinkcube.com>
     * */
    $('#csvReport').on('click', function(e) {
        if (reportType == 'Monthly') {
            var checkTaxStatus = getCheckedStatus();
            var fromMonth = $('#fromMonth').val();
            var fromMonthSplit = fromMonth.split("-");
            var toMonth = $('#toMonth').val();
            var toMonthSplit = toMonth.split("-");
            var paymentTerm = $('#paymentTermList').val();
            var locationID = $('#locationList').val();

            fromday(fromMonthSplit[1]);
            today(toMonthSplit[1], toMonthSplit[0]);
            if ($('#fromMonth').val() == null || $('#fromMonth').val() == "") {
                p_notification(false, eb.getMessage("ERR_SALEREPORT_FROMMONTH"));
                $('#fromMonth').focus();
                return false;
            }
            else if ($('#toMonth').val() == null || $('#toMonth').val() == "") {
                p_notification(false, eb.getMessage("ERR_SALEREPORT_TOMONTH"));
                $('#toMonth').focus();
                return false;
            } else if ($('#toMonth').val() < $('#fromMonth').val()) {
                p_notification(false, eb.getMessage('ERR_SALEREPORT_DEFF'));
                $('#toMonth').focus();
                return false;
            } else {
                var url = BASE_URL + "/report/get/invoice/monthly-invoice-csv";
                eb.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: {
                        fromMonth: fm,
                        toMonth: tm,
                        checkTaxStatus: checkTaxStatus,
                        paymentTerm: paymentTerm,
                        locationID: locationID
                    },
                    success: function(respond) {
                        if (respond.status == true) {
                            window.open(BASE_URL + '/' + respond.data, '_blank');
                        }
                    }
                });
            }
        }
        else if (reportType == 'Daily') {

            var checkTaxStatus = getCheckedStatus();
            var fromDate = $('#fromDate').val();
            var toDate = $('#toDate').val();
            var paymentTerm = $('#paymentTermList').val();
            var locationID = $('#locationList').val();

            inputs = new Array(fromDate, toDate);
            if (validteinput(inputs)) {
                var url = BASE_URL + "/report/get/invoice/daily-invoice-csv";
                eb.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: {
                        fromDate: fromDate,
                        toDate: toDate,
                        checkTaxStatus: checkTaxStatus,
                        paymentTerm: paymentTerm,
                        locationID: locationID
                    },
                    success: function(respond) {
                        if (respond.status == true) {
                            window.open(BASE_URL + '/' + respond.data, '_blank');
                        }
                    }
                });
            }

        }
        else if (reportType == 'Annual') {
            var checkTaxStatus = getCheckedStatus();
            var paymentTerm = $('#paymentTermList').val();
            var locationID = $('#locationList').val();

            if ($('#year').val() == null || $('#year').val() == "") {
                p_notification(false, eb.getMessage('ERR_SALEREPORT_YEAR'));
                $('#year').focus();
                return false;
            } else {
                var year = $("#year").val();
                year = year.replace(/\s+/g, "");
                $.trim(year);
                var url = BASE_URL + "/report/get/invoice/annual-invoice-csv";
                eb.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: {
                        year: year,
                        checkTaxStatus: checkTaxStatus,
                        paymentTerm: paymentTerm,
                        locationID: locationID
                    },
                    success: function(respond) {
                        if (respond.status == true) {
                            window.open(BASE_URL + '/' + respond.data, '_blank');
                        }
                    }
                });
            }

        } else if (reportType == 'Summery') {
            inputs = new Array(
                    $('#fromDate').val(),
                    $('#toDate').val()
                    );
            if (validteinput(inputs)) {
                var fromDate = $("#fromDate").val();
                var toDate = $("#toDate").val();
                var url = BASE_URL + "/report/get/invoice/summery-invoice-csv";
                eb.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: {fromDate: fromDate, toDate: toDate},
                    success: function(respond) {
                        if (respond.status == true) {
                            window.open(BASE_URL + '/' + respond.data, '_blank');
                        }
                    }
                });
            }
        }
        else if (reportType == 'Payments') {
            var fromDate = $('#fromDate').val();
            var toDate = $('#toDate').val();
            inputs = new Array(fromDate, toDate);
            if (validteinput(inputs)) {

                window.open(BASE_URL + "/report/get/invoice/generate-payments-summery-sheet?fromDate=" + fromDate + "&toDate=" + toDate, '_blank');
            }
        } else if (reportType == 'salesPersonWiseOustanding') {
            var checkTaxStatus = getCheckedStatus();
            var getpdfdataurl = BASE_URL + '/report/get/invoice/generate-outstanding-balance-sheet';
            var input = {
                fromDate : $('#fromDate').val(),
                toDate : $('#toDate').val(),
                salesPersonIds : $("#salesPersons").val(),
                isAllCustomers : isAllCustomers,
                customerIds : $('#customerName').val(),
                isAllItems : isAllItems,
                itemIds : $('#itemList').val(),
                casCategory : $('#cusCategory').val(),
                showTax : checkTaxStatus
            }

            if (salesPersonWiseOustandingFormValidation(input)){
                eb.ajax({
                    type: 'POST',
                    url: getpdfdataurl,
                    dataType: 'json',
                    data: input,
                    success: function(respond) {
                        if (respond.status == true) {
                            window.open(BASE_URL + '/' + respond.data, '_blank');
                        }
                    }
                });
            }
        }
        else if (reportType == 'CustomerWiseOpenInvoice') {
            var url = BASE_URL + "/report/get/invoice/open-invoice-csv";
            var taxType = $("#taxType").val();
            var customerIds = $('#customerName').val();
            var locationID = $('#locationList').val();

            if(isAllCustomers == true || customerIds != null) {
                eb.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: {
                        taxType : taxType,
                        isAllCustomers : isAllCustomers,
                        customerIds : customerIds,
                        locationIDs : locationID
                    },
                    success: function(respond) {
                        if (respond.status == true) {
                            window.open(BASE_URL + '/' + respond.data, '_blank');
                        }
                    }
                });
            } else {
                p_notification(false, eb.getMessage('ERR_CUSREPO_SELECUST'));
            }
        } else if (reportType == 'cancelledInvoice') {
            var getsheetdataurl = BASE_URL + '/report/get/invoice/get-cancelled-invoice-sheet';
            var locations = $('#locationList').val();
            if (locations == null || locations.length == 0) {
                p_notification(false, eb.getMessage('ERR_PAYVOUCHER_SELELOC'));
                return false;
            }
            var inputs = [
                $('#fromDate').val(),
                $('#toDate').val(),
            ];
            if (!($('#fromDate').val() && $('#toDate').val()) || validteinput(inputs))
            {
                eb.ajax({
                    type: 'POST',
                    url: getsheetdataurl,
                    data: {
                        fromDate: $('#fromDate').val(),
                        toDate: $('#toDate').val(),
                        users: $('#users').val(),
                        locations: $('#locationList').val(),
                        isAllCustomers : isAllCustomers,
                        customerIds : $('#customerName').val(),

                    },
                    success: function(respond) {
                        if (respond.status) {
                            window.open(BASE_URL + '/' + respond.data, '_blank');
                        }
                    }
                });
            }
        }
        else if (reportType == 'editedInvoice') {
            var getpdfdataurl = BASE_URL + '/report/get/invoice/generate-edited-invoice-sheet';
            var inputs = [
                $('#fromDate').val(),
                $('#toDate').val(),
            ];
            if (!($('#fromDate').val() && $('#toDate').val()) || validteinput(inputs))
            {
                var getpdfdatarequest = eb.post(getpdfdataurl, {
                    fromDate: $('#fromDate').val(),
                    toDate: $('#toDate').val(),
                    users: $('#users').val(),
                    isAllCustomers : isAllCustomers,
                    customerIds : $('#customerName').val(),
                });
                getpdfdatarequest.done(function(respond) {
                    if (respond.status) {
                        window.open(BASE_URL + '/' + respond.data, '_blank');
                    }
                });
            }
        } else if (reportType == 'invoicePayments') {
            var url = BASE_URL + '/report/get/invoice/invoice-payment-details-report-csv';
            var input = {
                fromDate: $('#fromDate').val(),
                toDate: $('#toDate').val(),
                isAllCustomers : isAllCustomers,
                customerIds : $('#customerName').val(),
                isAllInvoices: isAllInvoices,
                invoiceIds: $('#invoiceName').val(),
                locationIds: locationIds
            };

            if (paymentDetailsFromValidation(input)) {
                eb.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: input,
                    success: function(respond) {
                        if (respond.status) {
                            window.open(BASE_URL + '/' + respond.data, '_blank');
                        }
                    }
                });
            }
        } else if (reportType == 'issuedInvItem') {
            var fromDate = $('#fromDate').val();
            var toDate = $('#toDate').val();
            var locIds = $('#locationList').val();
            var isAllCategories = $('input[name="isAllCategories"]:checked').val();
            var categoryIds = $('#categoryList').val();
            if (fromDate == '' || fromDate == null) {
                p_notification(false, eb.getMessage('ERR_DAISALE_FROMD'));
                document.getElementById("fromDate").focus();
            } else if (toDate == '' || toDate == null) {
                p_notification(false, eb.getMessage('ERR_DAISALE_TOD'));
                document.getElementById("toDate").focus();
            } else if (toDate < fromDate) {
                p_notification(false, eb.getMessage('ERR_SALEREPORT_DEFF'));
                return false;
            }
            else {
                var url = BASE_URL + '/report/get/invoice/generate-issued-invoice-item-sheet';
                eb.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: {locationIdList: locIds, fromDate: fromDate, toDate: toDate, isAllCategories: isAllCategories, categoryIds: categoryIds},
                    success: function(respond) {
                        if (respond.status == true) {
                            window.open(BASE_URL + '/' + respond.data, '_blank');
                        }
                    }
                });
            }
        } else if (reportType == 'CustomerWiseOpenInvoiceWithCreditNote') {
            var url = BASE_URL + "/report/get/invoice/open-invoice-with-credit-note-csv";
            var input = {
                taxType : $("#taxType").val(),
                isAllCustomers : isAllCustomers,
                customerIds : $('#customerName').val(),
                salesPersonIds : $("#salesPersons").val()
            }
            if(openInvoiceWithCreditNotesFormValidation(input)){
                eb.ajax({
                    type: 'POST',
                    url: url,
                    data: input,
                    success: function(respond) {
                        if (respond.status == true) {
                            window.open(BASE_URL + '/' + respond.data, '_blank');
                        }
                    }
                });
            }
        } else if (reportType == 'invoiceDetails') {
            var url = BASE_URL + '/report/get/invoice/invoice-payment-details-csv';
            var input = {
                fromDate: $('#fromDate').val(),
                toDate: $('#toDate').val(),
                isAllCustomers : isAllCustomers,
                customerIds : $('#customerName').val(),
                isAllInvoices: isAllInvoices,
                invoiceIds: $('#invoiceName').val(),
                locationIds: locationIds
            };

            if (paymentDetailsFromValidation(input)) {
                eb.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: input,
                    success: function(respond) {
                        if (respond.status) {
                            window.open(BASE_URL + '/' + respond.data, '_blank');
                        }
                    }
                });
            }
        } else if (reportType == 'invoiceDueDateReport') {
            var url = BASE_URL + '/report/get/invoice/invoice-due-date-report-csv';
            var input = {
                fromDate: $('#fromDate').val(),
                toDate: $('#toDate').val(),
                isAllCustomers : isAllCustomers,
                customerIds : $('#customerName').val(),
                locationIds: $('#locationList').val(),
                status:$("#status").val()
            };

            if (InvoiceDueDateReportInputValidation(input)) {
                eb.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: input,
                    success: function(respond) {
                        if (respond.status) {
                            window.open(BASE_URL + '/' + respond.data, '_blank');
                        }
                    }
                });
            }
        }  else if (reportType == 'invoiceDetailsWithCustomer') {
            var url = BASE_URL + '/report/get/invoice/invoice-details-with-customer-report-csv';
            var input = {
                fromDate: $('#fromDate').val(),
                toDate: $('#toDate').val(),
                locationIds: $('#locationList').val()
            };

            if (invoiceDetailsWithCustomerReportInputValidation(input)) {
                eb.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: input,
                    success: function(respond) {
                        if (respond.status) {
                            window.open(BASE_URL + '/' + respond.data, '_blank');
                        }
                    }
                });
            }
        } else {
            p_notification(false, "Please select a correct report type first");
        }

    });
    $('#viewGraph').on('click', function(e) {
        $('#salesReport').hide();
        $('#salesGraphReport').show();
        $('#salesGraphReport').html('');
        if (reportType == 'Monthly') {
            $('#btnMonthly').show();
            var checkTaxStatus = getCheckedStatus();
            var getpdfdataurl = BASE_URL + '/api/sales-report/get-monthly-sales-graph-data';
            var fromMonth = $('#fromMonth').val();
            var fromMonthSplit = fromMonth.split("-");
            var toMonth = $('#toMonth').val();
            var toMonthSplit = toMonth.split("-");
            fromday(fromMonthSplit[1]);
            today(toMonthSplit[1], toMonthSplit[0]);
            if ($('#fromMonth').val() == null || $('#fromMonth').val() == "") {
                p_notification(false, "From Month must be filled out");
                $('#fromMonth').focus();
                return false;
            }
            else if ($('#toMonth').val() == null || $('#toMonth').val() == "") {
                p_notification(false, "To Month must be filled out");
                $('#toMonth').focus();
                return false;
            } else {

                var getpdfdatarequest = eb.post(getpdfdataurl, {
                    fromMonth: fm,
                    toMonth: tm
                });
                getpdfdatarequest.done(function(respond) {
                    var cSymbol = respond.data['cSymbol'];
                    var monthlySalesData = Array();
                    $.each(respond.data['mothlySalesDetails'], function(index, value) {
                        $.each(value, function(subindex, subvalue) {
                            var month = subvalue.Month;
                            var totalAmount = parseFloat(Math.round((subvalue.TotalAmount) * 100) / 100).toFixed(2);
                            totalAmount = totalAmount ? totalAmount : 0.00;
                            var totalTax = parseFloat(Math.round((subvalue.TotalTax) * 100) / 100).toFixed(2);
                            totalTax = totalTax ? totalTax : 0.00;
                            var totalSales = totalAmount - totalTax;
                            monthlySalesData.push({x: month, y: totalSales, z: totalTax});
                        });
                    });
                    if (checkTaxStatus == true) {
                        Morris.Bar({
                            element: 'salesGraphReport',
                            data: monthlySalesData,
                            xkey: 'x',
                            ykeys: ['y', 'z'],
                            labels: ['Sales Excluding Tax', 'Total Tax'],
                            parseTime: false,
                            yLabelFormat: function(y) {
                                return cSymbol + '. ' + accounting.formatMoney(y);
                            },
                            barColors: ['#FF8000', '#663300'],
                            hoverCallback: function(index, options, content) {
                                return(content);
                            },
                            stacked: true,
                            xLabelAngle: 60,
                            hideHover: 'auto'
                        });
                    } else {
                        Morris.Bar({
                            element: 'salesGraphReport',
                            data: monthlySalesData,
                            xkey: 'x',
                            ykeys: ['y'],
                            labels: ['Sales Excluding Tax'],
                            parseTime: false,
                            yLabelFormat: function(y) {
                                return cSymbol + '. ' + accounting.formatMoney(y);
                            },
                            barColors: ['#FF8000'],
                            xLabelAngle: 60,
                            hideHover: 'auto'
                        });
                    }
                });
            }

        }

        else if (reportType == 'Daily') {
            $('#btnDaily').show();
            var checkTaxStatus = getCheckedStatus();
            var getdataurl = BASE_URL + '/api/sales-report/get-daily-sales-graph-data';
            inputs = new Array(
                    $('#fromDate').val(),
                    $('#toDate').val());
            if (validteinput(inputs)) {
                var getdatarequest = eb.post(getdataurl, {
                    fromDate: $('#fromDate').val(),
                    toDate: $('#toDate').val()
                });
                getdatarequest.done(function(respond) {
                    if (respond.status == true) {

                        var dailySalesRespond = respond.data['dailySalesData'];
                        var cSymbol = respond.data['cSymbol'];
                        var start = new Date($('#fromDate').val());
                        var end = new Date($('#toDate').val());
                        var dayArray = Array();
                        while (start <= end) {
                            var date = start.toString();
                            var dateSplit = date.split(" ");
                            monthToNumber(dateSplit[1]);
                            var new_date = (dateSplit[3] + '-' + (beforemonth) + '-' + dateSplit[2]);
                            dayArray.push(new_date);
                            var newDate = start.setDate(start.getDate() + 1);
                            start = new Date(newDate);
                        }

                        var dailySalesData = Array();
                        for (var i = 0; i < dayArray.length; i++) {
                            var amount;
                            var tax;
                            if (dailySalesRespond[dayArray[i]].length == 0) {
                                amount = 0;
                                tax = 0;
                            } else {
                                amount = parseFloat(Math.round((dailySalesRespond[dayArray[i]].salesAmount) * 100) / 100).toFixed(2);
                                amount = amount ? amount : 0.00;
                                tax = parseFloat(Math.round((dailySalesRespond[dayArray[i]].taxAmount) * 100) / 100).toFixed(2);
                                tax = tax ? tax : 0.00;
                            }
                            var totalSales = amount - tax;
                            dailySalesData.push({x: dayArray[i], y: totalSales, z: tax});
                        }

                        if (checkTaxStatus == true) {
                            Morris.Bar({
                                element: 'salesGraphReport',
                                data: dailySalesData,
                                hoverCallback: function(index, options, content) {
                                    return(content);
                                },
                                xkey: 'x',
                                ykeys: ['y', 'z'],
                                stacked: true,
                                labels: ['Sales Excluding Tax', 'Tax'],
                                barColors: ['#FF8000', '#663300'],
                                yLabelFormat: function(y) {
                                    return cSymbol + '. ' + accounting.formatMoney(y);
                                },
                                parseTime: false,
                                hideHover: 'auto',
                                xLabelAngle: 60
                            });
                        } else {
                            Morris.Bar({
                                element: 'salesGraphReport',
                                data: dailySalesData,
                                xkey: 'x',
                                ykeys: ['y'],
                                labels: ['Sales Excluding Tax'],
                                barColors: ['#FF8000'],
                                yLabelFormat: function(y) {
                                    return cSymbol + '. ' + accounting.formatMoney(y);
                                },
                                parseTime: false,
                                hideHover: 'auto',
                                xLabelAngle: 60
                            });
                        }
                    }
                });
            }

        }

        else if (reportType == 'Annual') {

            var checkTaxStatus = getCheckedStatus();
            var year = $('#year').val();
            year = year.replace(/\s+/g, "");
            $.trim(year);
            var getdataurl = BASE_URL + '/api/sales-report/get-annuval-sales-graph-data';
            var inputs = new Array($('#year').val());
            if ($('#year').val() == null || $('#year').val() == "") {
                p_notification(false, "Year must be filled out");
                $('#year').focus();
                return false;
            } else {
                $('#graph_div').show();
                var getdatarequest = eb.post(getdataurl, {
                    year: year
                });
                getdatarequest.done(function(respond) {
                    if (respond.status == true) {
                        var cSymbol = respond.data['cSymbol'];
                        var annualSalesData = respond.data['annualSalesData'];
                        var qa1 = parseFloat(Math.round((annualSalesData[0].Q1Amount) * 100) / 100).toFixed(2);
                        qa1 = qa1 ? qa1 : 0.00;
                        var qa2 = parseFloat(Math.round((annualSalesData[0].Q2Amount) * 100) / 100).toFixed(2);
                        qa2 = qa2 ? qa2 : 0.00;
                        var qa3 = parseFloat(Math.round((annualSalesData[0].Q3Amount) * 100) / 100).toFixed(2);
                        qa3 = qa3 ? qa3 : 0.00;
                        var qa4 = parseFloat(Math.round((annualSalesData[0].Q4Amount) * 100) / 100).toFixed(2);
                        qa4 = qa4 ? qa4 : 0.00;
                        var qt1 = parseFloat(Math.round((annualSalesData[0].Q1Tax) * 100) / 100).toFixed(2);
                        qt1 = qt1 ? qt1 : 0.00;
                        var qt2 = parseFloat(Math.round((annualSalesData[0].Q2Tax) * 100) / 100).toFixed(2);
                        qt2 = qt2 ? qt2 : 0.00;
                        var qt3 = parseFloat(Math.round((annualSalesData[0].Q3Tax) * 100) / 100).toFixed(2);
                        qt3 = qt3 ? qt3 : 0.00;
                        var qt4 = parseFloat(Math.round((annualSalesData[0].Q4Tax) * 100) / 100).toFixed(2);
                        qt4 = qt4 ? qt4 : 0.00;
                        if (checkTaxStatus == true) {
                            Morris.Bar({
                                element: 'salesGraphReport',
                                data: [
                                    {y: 'Q1', a: qa1 - qt1, b: qt1},
                                    {y: 'Q2', a: qa2 - qt2, b: qt2},
                                    {y: 'Q3', a: qa3 - qt3, b: qt3},
                                    {y: 'Q4', a: qa4 - qt4, b: qt4}
                                ],
                                xkey: 'y',
                                ykeys: ['a', 'b'],
                                labels: ['Sales Excluding Tax', 'Total Tax'],
                                barColors: ['#FF8000', '#663300'],
                                yLabelFormat: function(y) {
                                    return cSymbol + '. ' + accounting.formatMoney(y);
                                },
                                hideHover: 'auto'
                            });
                        } else {
                            Morris.Bar({
                                element: 'salesGraphReport',
                                data: [
                                    {y: 'Q1', a: qa1 - qt1},
                                    {y: 'Q2', a: qa2 - qt2},
                                    {y: 'Q3', a: qa3 - qt3},
                                    {y: 'Q4', a: qa4 - qt4}
                                ],
                                xkey: 'y',
                                ykeys: ['a'],
                                labels: ['Sales Excluding Tax'],
                                barColors: ['#FF8000'],
                                yLabelFormat: function(y) {
                                    return cSymbol + '. ' + accounting.formatMoney(y);
                                },
                                hideHover: 'auto'
                            });
                        }
                    }
                }
                );
            }
        }

        else {
            p_notification(false, "Please select a correct report type first");
        }
    });

    var element;
    $($('#salesReport').contents()).on('mouseover', '.invoice-preview', function(e) {
        element = $(this);
        element.css('cursor','pointer');
        element.css("background-color", "#ddd");
    });

    $($('#salesReport').contents()).on('mouseout', '.invoice-preview', function(e) {
        element.css("background-color", "#f6f6f6");
    });

    $($('#salesReport').contents()).on('click', '.invoice-preview', function(e) {
        var url = $(this).data('href');
        showInvoicePreview(BASE_URL + url);
    });

    var fday1 = '-01';
    var tday30 = '-30';
    var tday31 = '-31';
    /* Set day to from_month text field
     * @author SANDUN <sandun@thinkcube.com>
     * */

    function fromday(fromMonthSplit) {
        var fromMonth = $('#fromMonth').val();
        if (fromMonthSplit == '01') {
            fm = fromMonth.concat(fday1);
        }
        else if (fromMonthSplit == '02') {
            fm = fromMonth.concat(fday1);
        }
        else if (fromMonthSplit == '03') {
            fm = fromMonth.concat(fday1);
        }
        else if (fromMonthSplit == '04') {
            fm = fromMonth.concat(fday1);
        }
        else if (fromMonthSplit == '05') {
            fm = fromMonth.concat(fday1);
        }
        else if (fromMonthSplit == '06') {
            fm = fromMonth.concat(fday1);
        }
        else if (fromMonthSplit == '07') {
            fm = fromMonth.concat(fday1);
        }
        else if (fromMonthSplit == '08') {
            fm = fromMonth.concat(fday1);
        }
        else if (fromMonthSplit == '09') {
            fm = fromMonth.concat(fday1);
        }
        else if (fromMonthSplit == '10') {
            fm = fromMonth.concat(fday1);
        }
        else if (fromMonthSplit == '11') {
            fm = fromMonth.concat(fday1);
        }
        else if (fromMonthSplit == '12') {
            fm = fromMonth.concat(fday1);
        }
    }

    /* Set day to to_month text field
     * @author SANDUN <sandun@thinkcube.com>
     * */
    function today(toMonthSplit, year) {
        var toMonth = $('#toMonth').val();
        if (toMonthSplit == '01') {
            tm = toMonth.concat(tday31);
        }
        else if (toMonthSplit == '02') {
            if (leapYear(year)) {
                var tday = '-29';
            } else {
                var tday = '-28';
            }
            tm = toMonth.concat(tday);
        }
        else if (toMonthSplit == '03') {
            tm = toMonth.concat(tday31);
        }
        else if (toMonthSplit == '04') {
            tm = toMonth.concat(tday30);
        }
        else if (toMonthSplit == '05') {
            tm = toMonth.concat(tday31);
        }
        else if (toMonthSplit == '06') {
            tm = toMonth.concat(tday30);
        }
        else if (toMonthSplit == '07') {
            tm = toMonth.concat(tday31);
        }
        else if (toMonthSplit == '08') {
            tm = toMonth.concat(tday31);
        }
        else if (toMonthSplit == '09') {
            tm = toMonth.concat(tday30);
        }
        else if (toMonthSplit == '10') {
            tm = toMonth.concat(tday31);
        }
        else if (toMonthSplit == '11') {
            tm = toMonth.concat(tday30);
        }
        else if (toMonthSplit == '12') {
            tm = toMonth.concat(tday31);
        }

    }

    /* validation function for Reporting Form
     * @author SANDUN <sandun@thinkcube.com>
     * */
    function validteinput(inputs) {
        var fromDate = inputs[0];
        var toDate = inputs[1];
        var salesPersons = inputs[2];
        if (fromDate == null || fromDate == "") {
            p_notification(false, eb.getMessage('ERR_DAISALE_FROMD'));
            document.getElementById("fromDate").focus();
        } else if (toDate == null || toDate == "") {
            p_notification(false, eb.getMessage('ERR_DAISALE_TOD'));
            document.getElementById("toDate").focus();
        } else if (toDate < fromDate) {
            p_notification(false, eb.getMessage('ERR_DAISALE_TODFROMD'));
            document.getElementById("toDate").focus();
        } else if (salesPersons) {
            if ($('#salesPersons').val() == null || $('#salesPersons').val() == "") {
                p_notification(false, eb.getMessage('ERR_INVOICE_REPORT_SELECT_SALESPERSON'));
                return false;
            }
            return true;
        } else {
            return true;
        }
    }

    /*
     * @author SANDUN <sandun@thinkcube.com>
     * return month number using month string name
     */
    function monthToNumber(month) {
        if (month == 'Jan') {
            beforemonth = '01';
        } else if (month == 'Feb') {

            beforemonth = '02';
        } else if (month == 'Mar') {


            beforemonth = '03';
        } else if (month == 'Apr') {
            beforemonth = '04';
        } else if (month == 'May') {
            beforemonth = '05';
        } else if (month == 'Jun') {
            beforemonth = '06';
        } else if (month == 'Jul') {
            beforemonth = '07';
        } else if (month == 'Aug') {
            beforemonth = '08';
        } else if (month == 'Sep') {
            beforemonth = '09';
        } else if (month == 'Oct') {
            beforemonth = 10;
        } else if (month == 'Nov') {
            beforemonth = 11;
        } else if (month == 'Dec') {
            beforemonth = 12;
        }
    }

    function initialCondition() {
        //show
        $('#viewGraph').show();
        $('#fromMonth,#toMonth').show();
        //hide
        $('#tax,#lblTax').show();
        $('#salesReport').hide();
        $('#monthlySalesGraph,#dailySalesGraph,#annualSalesGraph').hide();
        $('#fromDate,#toDate,#date,#year').hide();
    }

    function getCheckedStatus() {
        var checkTaxStatus;
        if (tax.checked == false) {
            checkTaxStatus = false;
        } else {
            checkTaxStatus = true;
        }
        return checkTaxStatus;
    }
    /*
     * @author SANDUN <sandun@thinkcube.com>
     * return boolean
     * */
    function leapYear(year) {
        return ((year % 4 == 0) && (year % 100 != 0)) || (year % 400 == 0);
    }

    //responsive issue fix
    $(window).bind('resize ready', function() {
        if ($(window).width() <= 480) {
            $('.report_from_month').css('margin-bottom', '15px').removeClass('col-xs-4').addClass('col-xs-12');
            $('.set_to_date').css('margin-bottom', '15px').removeClass('col-xs-4').addClass('col-xs-12');
            $('.btn_panel_height_high').removeClass('btn-group');
            $('#view_report, #view_graph, #generate_pdf, #excel_report').removeClass('btn-default').addClass('col-xs-12 btn-primary').css('margin-bottom', '8px');
            $('#btnMonthly, #btnDaily, #btn_annual').css('margin-bottom', '15px');
        } else {
            $('.report_from_month').css('margin-bottom', '0').removeClass('col-xs-12').addClass('col-xs-4');
            $('.set_to_date').css('margin-bottom', '0').removeClass('col-xs-12').addClass('col-xs-4');
            $('.btn_panel_height_high').addClass('btn-group');
            $('#view_report, #view_graph, #generate_pdf, #excel_report').addClass('btn-default').removeClass('col-xs-12 btn-primary').css('margin-bottom', '0px');
            $('#btnMonthly, #btnDaily, #btn_annual').css('margin-bottom', '0');
        }
    });

    function paymentDetailsFromValidation(input) {

        if (input.locationIds == null) {
            p_notification(false, 'Please select atleast one location.');
            return false;
        } else if (input.isAllInvoices == false && input.invoiceIds == null) {
            p_notification(false, 'Please select atleast one invoice.');
            return false;
        } else if ((input.toDate) && (input.fromDate == null || input.fromDate == "")) {
            p_notification(false, 'Please select From Date.');
            document.getElementById("fromDate").focus();
            return false;
        } else if ((input.fromDate) && (input.toDate == null || input.toDate == "")) {
            p_notification(false, 'Please select To Date.');
            document.getElementById("toDate").focus();
            return false;
        } else if ((input.fromDate && input.toDate) && input.fromDate > input.toDate) {
            p_notification(false, 'Please select a valid date range.');
            document.getElementById("toDate").focus();
            return false;
        } else {
            return true;
        }
    }

    function InvoiceDueDateReportInputValidation(input) {

        if (input.locationIds == null) {
            p_notification(false, 'Please select atleast one location.');
            return false;
        } else if ((input.toDate) && (input.fromDate == null || input.fromDate == "")) {
            p_notification(false, 'Please select From Date.');
            document.getElementById("fromDate").focus();
            return false;
        } else if ((input.fromDate) && (input.toDate == null || input.toDate == "")) {
            p_notification(false, 'Please select To Date.');
            document.getElementById("toDate").focus();
            return false;
        } else if ((input.fromDate && input.toDate) && input.fromDate > input.toDate) {
            p_notification(false, 'Please select a valid date range.');
            document.getElementById("toDate").focus();
            return false;
        } else if (input.isAllCustomers == false && input.customerIds == null){
            p_notification(false, eb.getMessage('ERR_CUSREPO_SELECUST'));
            return false;
        } else {
            return true;
        }
    }

    function invoiceDetailsWithCustomerReportInputValidation(input) {

        if (input.locationIds == null) {
            p_notification(false, 'Please select atleast one location.');
            return false;
        } else if ((input.toDate) && (input.fromDate == null || input.fromDate == "")) {
            p_notification(false, 'Please select From Date.');
            document.getElementById("fromDate").focus();
            return false;
        } else if ((input.fromDate) && (input.toDate == null || input.toDate == "")) {
            p_notification(false, 'Please select To Date.');
            document.getElementById("toDate").focus();
            return false;
        } else if ((input.fromDate && input.toDate) && input.fromDate > input.toDate) {
            p_notification(false, 'Please select a valid date range.');
            document.getElementById("toDate").focus();
            return false;
        } else {
            return true;
        }
    }

    function salesPersonWiseOustandingFormValidation(input){
        if (input.fromDate == null || input.fromDate == "") {
            p_notification(false, eb.getMessage('ERR_DAISALE_FROMD'));
            document.getElementById("fromDate").focus();
            return false;
        } else if (input.toDate == null || input.toDate == "") {
            p_notification(false, eb.getMessage('ERR_DAISALE_TOD'));
            document.getElementById("toDate").focus();
            return false;
        } else if (input.toDate < input.fromDate) {
            p_notification(false, eb.getMessage('ERR_DAISALE_TODFROMD'));
            document.getElementById("toDate").focus();
            return false;
        } else if (input.salesPersonIds == null) {
            p_notification(false, eb.getMessage('ERR_INVOICE_REPORT_SELECT_SALESPERSON'));
            return false;
        } else if (input.isAllCustomers == false && input.customerIds == null){
            p_notification(false, eb.getMessage('ERR_CUSREPO_SELECUST'));
            return false;
        } else if (input.isAllItems == false && input.itemIds == null){
            p_notification(false, eb.getMessage('ERR_DAISALE_ITEMWISETBL_PRODUCT_ERR'));
            return false;
        } else {
            return true;
        }
    }

    function openInvoiceWithCreditNotesFormValidation(input){
        if (input.salesPersonIds == null) {
            p_notification(false, eb.getMessage('ERR_INVOICE_REPORT_SELECT_SALESPERSON'));
            return false;
        } else if (input.isAllCustomers == false && input.customerIds == null){
            p_notification(false, eb.getMessage('ERR_CUSREPO_SELECUST'));
            return false;
        } else {
            return true;
        }
    }

});

