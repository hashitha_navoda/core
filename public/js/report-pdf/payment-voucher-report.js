/*
 * @author Yashora <yashora@thinkcube.com>
 */
$(document).ready(function() {
    var reportType = 'payVoucherDetails';
    var locationIds = $('#locationLists').val();
    var lengthOfLocList = $('#locationLists').find('option').length;
    var isAllPIs = false;
    divLoading();
    setSelectPicker();

    loadDropDownFromDatabase('/api/pi/search-allPV-for-dropdown', "", 0, '#piLists', "", true);
    $('#piLists').on('change', function() {
        var thisValue = $(this).val();
        if (thisValue == null) {
            reSetReportContent('paymentVoucherReport');
        }
    });

    var checkin = $('#fromDate').datepicker({
    }).on('changeDate', function(ev) {

        if (ev.date.valueOf() < checkout.date.valueOf()) {
            var newDate = new Date(ev.date);
            checkout.setValue(newDate);
        }
        checkin.hide();
        $('#toDate')[0].focus();
    }).data('datepicker');

    var checkout = $('#toDate').datepicker({
        onRender: function(date) {
            return date.valueOf() <= checkin.date.valueOf();
        }
    }).on('changeDate', function(ev) {
        checkout.hide();
    }).data('datepicker');

    $('#locationLists').on('change', function() {
        var thisValue = $(this).val();
        if (thisValue == null) {
            reSetReportContent('paymentVoucherReport');
        }
    });

    $('#payVoucherDetails').on('click', function() {
        reportType = 'payVoucherDetails';
        locationIds = $('#locationLists').val();
        $('#locationLists').selectpicker('deselectAll');
        $('#location_div').removeClass('hidden');
        $('#pi_div').addClass('hidden');
    });
    $('#piWiseDebitNote').on('click', function() {
        reportType = 'piWiseDebitNote';
        isAllPIs = $('.isAllPI:checked').val();
        $('#location_div').addClass('hidden');
        $('#pi_div').removeClass('hidden');
        $('#locationLists').selectpicker('deselectAll');

    });


    $('#viewReport').on('click', function() {
        locationIds = $('#locationLists').val();
        var fromDate = $('#fromDate').val();
        var toDate = $('#toDate').val();
        inputs = new Array(
                fromDate,
                toDate,
                locationIds
                );
        if (reportType == 'payVoucherDetails') {
            if (validateinput(inputs)) {
                removeMultiSelect(locationIds);
                var url = BASE_URL + '/reports/get/payment-voucher/view-payment-voucher-details';
                eb.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: {locationIds: locationIds, fromDate: fromDate, toDate: toDate},
                    success: function(respond) {
                        if (respond.status == true) {
                            setContentToReport('paymentVoucherReport', respond);
                        }
                    }
                });
            } else if (lengthOfLocList == 0) {
                p_notification(false, eb.getMessage('ERR_NORECORDS_IN_SELECTPICKER'));
            }
        } else if(reportType ==='piWiseDebitNote'){
            var url = BASE_URL + '/reports/get/payment-voucher/pi-wise-debit-note-report';
            var input = {};
            input.fromDate = $('#fromDate').val();
            input.toDate = $('#toDate').val();
            input.isAllPIs = $('.isAllPI:checked').val();
            input.piIds = $('#piLists').val();
            if (piWiseDebitNoteReportValidation(input)) {
                var getpdfdatarequest = eb.post(url, input);
                getpdfdatarequest.done(function(respond) {
                    if (respond.status == true) {
                        setContentToReport('paymentVoucherReport', respond);
                    }
                });
            }
        }
    });

    $('#generatePdf').on('click', function() {
        locationIds = $('#locationLists').val();
        var fromDate = $('#fromDate').val();
        var toDate = $('#toDate').val();
        inputs = new Array(
                fromDate,
                toDate,
                locationIds
                );
        if (reportType == 'payVoucherDetails') {
            if (validateinput(inputs)) {
                removeMultiSelect(locationIds);
                var url = BASE_URL + '/reports/get/payment-voucher/generate-payment-voucher-details-pdf';
                eb.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: {locationIds: locationIds, fromDate: fromDate, toDate: toDate},
                    success: function(respond) {
                        if (respond.status == true) {
                            window.open(BASE_URL + '/' + respond.data, '_blank');
                        }
                    }
                });
            } else if (lengthOfLocList == 0) {
                p_notification(false, eb.getMessage('ERR_NORECORDS_IN_SELECTPICKER'));
            }
        } else if(reportType ==='piWiseDebitNote'){
            var url = BASE_URL + '/reports/get/payment-voucher/pi-wise-debit-note-pdf';
            var input = {};
            input.fromDate = $('#fromDate').val();
            input.toDate = $('#toDate').val();
            input.isAllPIs = $('.isAllPI:checked').val();
            input.piIds = $('#piLists').val();
            if (piWiseDebitNoteReportValidation(input)) {
                var getpdfdatarequest = eb.post(url, input);
                getpdfdatarequest.done(function(respond) {
                    if (respond.status == true) {
                        window.open(BASE_URL + '/' + respond.data, '_blank');
                    }
                });
            }
        }
    });
    $('#csvReport').on('click', function() {
        locationIds = $('#locationLists').val();
        var fromDate = $('#fromDate').val();
        var toDate = $('#toDate').val();
        inputs = new Array(
                fromDate,
                toDate,
                locationIds
                );
        if (reportType == 'payVoucherDetails') {
            if (validateinput(inputs)) {
                removeMultiSelect(locationIds);
                var url = BASE_URL + '/reports/get/payment-voucher/generate-payment-voucher-details-sheet';
                eb.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: {locationIds: locationIds, fromDate: fromDate, toDate: toDate},
                    success: function(respond) {
                        if (respond.status == true) {
                            window.open(BASE_URL + '/' + respond.data, '_blank');
                        }
                    }
                });
            } else if (lengthOfLocList == 0) {
                p_notification(false, eb.getMessage('ERR_NORECORDS_IN_SELECTPICKER'));
            }
        } else if(reportType ==='piWiseDebitNote'){
            var url = BASE_URL + '/reports/get/payment-voucher/pi-wise-debit-note-csv';
            var input = {};
            input.fromDate = $('#fromDate').val();
            input.toDate = $('#toDate').val();
            input.isAllPIs = $('.isAllPI:checked').val();
            input.piIds = $('#piLists').val();
            if (piWiseDebitNoteReportValidation(input)) {
                var getpdfdatarequest = eb.post(url, input);
                getpdfdatarequest.done(function(respond) {
                    if (respond.status == true) {
                        window.open(BASE_URL + '/' + respond.data, '_blank');
                    }
                });
            }
        }
    });

    function divLoading() {
        $('#paymentVoucherReport').hide();
    }

    function hideReportIFrame() {
        $('#paymentVoucherReport').hide();
        $('#paymentVoucherReport').contents().find('body').html('');
    }

});

function validateinput(inputs) {
    var fromDate = inputs[0];
    var toDate = inputs[1];
    var locIds = inputs[2];
    if (locIds == null) {
        p_notification(false, eb.getMessage('ERR_PAYVOUCHER_SELELOC'));
        document.getElementById("locIds").focus();
    }
    else if (fromDate == null || fromDate == "") {
        p_notification(false, eb.getMessage('ERR_DAISALE_FROMD'));
        document.getElementById("fromDate").focus();
    } else if (toDate == null || toDate == "") {
        p_notification(false, eb.getMessage('ERR_DAISALE_TOD'));
        document.getElementById("toDate").focus();
    } else if (fromDate > toDate) {
        p_notification(false, eb.getMessage('ERR_DAISALE_TODFROMD'));
        document.getElementById("toDate").focus();
    } else {
        return true;
    }
}


function piWiseDebitNoteReportValidation(input)
{
    if (input.fromDate == null || input.fromDate == "") {
        p_notification(false, eb.getMessage('ERR_DAISALE_FROMD'));
        document.getElementById("fromDate").focus();
        return false;
    } else if (input.isAllPIs == false && input.piIds == null) {
        p_notification(false, eb.getMessage('ERR_PIREPO_SELEPICODE'));
        return false;
    } else {
        return true;
    }
}


