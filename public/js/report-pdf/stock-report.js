/*
 * @author SANDUN <sandun@thinkcube.com>
 * Stock Reporting functions
 */
var productIds;
var categoryIds;
var locProductIds;
var sbProductIds;
var lengthOfAvailableProducts;
var lengthOfAvailableCategorys;
var lengthOfAvailableSBProducts;
var isAllProducts = false;
var isDocumentDateWise = false;
var isActDateWise = true;
var isROLWise = false;
var isMILWise = true;
var isAllNonInventoryProducts;
var serialIds;
var isAllSerials = false;
var isReachWise = true;
var isAllWise = false;

$(document).ready(function() {
    var reportType = 'itemDetails';
    var checkin = $('#from_date').datepicker({}).on('changeDate', function(ev) {
        if (ev.date.valueOf() < checkout.date.valueOf()) {
            var newDate = new Date(ev.date)
            newDate.setDate(newDate.getDate());
            checkout.setValue(newDate);
        }
        checkin.hide();
        $('#to_date')[0].focus();
    }).data('datepicker');
    var checkout = $('#to_date').datepicker({
        onRender: function(date) {
            return date.valueOf() < checkin.date.valueOf() ? 'disabled' : '';
        }
    }).on('changeDate', function(ev) {
        checkout.hide();
    }).data('datepicker');

     var checkout2 = $('#to_date2').datepicker({

    }).on('changeDate', function(ev) {
        checkout2.hide();
    }).data('datepicker');


    $("#to_date_div2").hide();
    divLoading();
    setSelectPicker();

    loadDropDownFromDatabase('/productAPI/search-user-active-location-products-for-dropdown', "", 0, '#product', "", true);
    $('#product,#category,#locProduct,#sbProduct').on('change', function() {
        var thisValue = $(this).val();
        if (thisValue === null) {
            reSetReportContent('stockReport');
        }
    });
    loadDropDownFromDatabase('/productAPI/search-product-serials-for-dropdown', "", 0, '#serialName', "", true);
    $('#serialName').on('change', function(){
        var serialIds = $(this).val();
        if (serialIds == null) {
            reSetReportContent('stockReport');
        }

    });

    loadDropDownFromDatabase('/productAPI/search-non-inventory-active-products-for-dropdown', "", 0, '#nonInventoryProduct', "", true);
    $('#product,#category,#locProduct,#sbProduct').on('change', function() {
        var thisValue = $(this).val();
        if (thisValue === null) {
            reSetReportContent('stockReport');
        }
    });

    hideStockReportIFrame();
    $('#from_date_div,#to_date_div').show();
    $('#to_date_div2').hide();
    $('.product-content').show();
    $('.category-content').show();
    $('.location-pro-content').show();
    $('.item-wise-expire-content').hide();
    $('.location-content').show();
    if (isAllProducts) {
        $('#customProductSelection').trigger('click');
    }
    $('#product').val('').trigger('change');
    $('.to_date_lbl,.from_date_lbl').addClass('hidden');
    $('.document-radio').addClass('hidden');
    $('#to_date,#from_date').hide();
    $('#div_serial').addClass('hidden');
    $('.non-inventory-products').addClass('hidden');

    $('#itemDetails').on('click', function() {
        reportType = 'itemDetails';
        hideStockReportIFrame();
        $('#from_date_div,#to_date_div').show();
        $('#to_date_div2').hide();
        $('.product-content').show();
        $('.category-content').show();
        $('.location-pro-content').show();
        $('.item-wise-expire-content').hide();
        $('.location-content').show();
        if (isAllProducts) {
            $('#customProductSelection').trigger('click');
        }
        $('#product').val('').trigger('change');
        $('.to_date_lbl,.from_date_lbl').addClass('hidden');
        $('.document-radio').addClass('hidden');
        $('#to_date,#from_date').hide();
        $('#div_serial').addClass('hidden');
        $('.non-inventory-products').addClass('hidden');
        $('.inv-level-radio').addClass('hidden');
        $('.inv-level-reach-radio').addClass('hidden');
    });
    $('#globalItemDetails').on('click', function() {
        reportType = 'globalItemDetails';
        hideStockReportIFrame();
        $('#from_date_div,#to_date_div').show();
        $('#to_date_div2').hide();
        $('.product-content').show();
        $('.category-content').show();
        $('.location-pro-content').hide();
        $('.item-wise-expire-content').hide();
        $('.location-content').hide();
        if (isAllProducts) {
            $('#customProductSelection').trigger('click');
        }
        $('#product').val('').trigger('change');
        $('.to_date_lbl,.from_date_lbl').addClass('hidden');
        $('.document-radio').addClass('hidden');
        $('#to_date,#from_date').hide();
        $('#div_serial').addClass('hidden');
        $('.non-inventory-products').addClass('hidden');
        $('.inv-level-radio').addClass('hidden');
        $('.inv-level-reach-radio').addClass('hidden');
    });
    $('#itemWise').on('click', function() {
        reportType = 'itemWise';
        hideStockReportIFrame();
        $('#from_date_div,#to_date_div').show();
        $('#to_date_div2').hide();
        $('.product-content').show();
        $('.category-content').show();
        $('.location-pro-content').show();
        $('.item-wise-expire-content').hide();
        $('.location-content').hide();
        if (isAllProducts) {
            $('#customProductSelection').trigger('click');
        }
        $('#product').val('').trigger('change');
        $('.to_date_lbl,.from_date_lbl').addClass('hidden');
        $('.document-radio').addClass('hidden');
        $('#to_date,#from_date').hide();
        $('#div_serial').addClass('hidden');
        $('.non-inventory-products').addClass('hidden');
        $('.inv-level-radio').addClass('hidden');
        $('.inv-level-reach-radio').addClass('hidden');
    });
    $('#categoryWise').on('click', function() {
        reportType = 'categoryWise';
        hideStockReportIFrame();
        $('#from_date_div,#to_date_div').show();
        $('#to_date_div2').hide();
        $('.category-content').show();
        $('.product-content').hide();
        $('.location-pro-content').hide();
        $('.item-wise-expire-content').hide();
        $('.location-content').show();
        if (isAllProducts) {
            $('#customProductSelection').trigger('click');
        }
        $('#product').val('').trigger('change');
        $('.to_date_lbl,.from_date_lbl').addClass('hidden');
        $('.document-radio').addClass('hidden');
        $('#to_date,#from_date').hide();
        $('#div_serial').addClass('hidden');
        $('.non-inventory-products').addClass('hidden');
        $('.inv-level-radio').addClass('hidden');
        $('.inv-level-reach-radio').addClass('hidden');
    });
    $('#locationWise').on('click', function() {
        reportType = 'locationWise';
        hideStockReportIFrame();
        $('#from_date_div,#to_date_div').show();
        $('#to_date_div2').hide();
        $('.category-content').hide();
        $('.product-content').show();
        $('.item-wise-expire-content').hide();
        $('.location-pro-content').show();
        $('.location-content').show();
        if (isAllProducts) {
            $('#customProductSelection').trigger('click');
        }
        $('#product').val('').trigger('change');
        $('.to_date_lbl,.from_date_lbl').addClass('hidden');
        $('.document-radio').addClass('hidden');
        $('#to_date,#from_date').hide();
        $('#div_serial').addClass('hidden');
        $('.non-inventory-products').addClass('hidden');
        $('.inv-level-radio').addClass('hidden');
        $('.inv-level-reach-radio').addClass('hidden');
    });
    $('#itemWiseExpire').on('click', function() {
        reportType = 'itemWiseExpire';
        hideStockReportIFrame();
        $('#from_date_div,#to_date_div').show();
        $('#to_date_div2').hide();
        $('.category-content').hide();
        $('.product-content').show();
        $('.location-pro-content').hide();
        $('.location-content').hide();
        if (isAllProducts) {
            $('#customProductSelection').trigger('click');
        }
        $('#product').val('').trigger('change');
        $('.item-wise-expire-content').show();
        $('.to_date_lbl,.from_date_lbl').removeClass('hidden');
        $('.document-radio').addClass('hidden');
        $('#to_date,#from_date').show();
        $('#to_date,#from_date').val('');
        $('#div_serial').addClass('hidden');
        $('.non-inventory-products').addClass('hidden');
        $('.inv-level-radio').addClass('hidden');
        $('.inv-level-reach-radio').addClass('hidden');
    });
    $('#itemWiseMIL').on('click', function() {
        reportType = 'itemWiseMIL';
        hideStockReportIFrame();
        $('#from_date_div,#to_date_div').show();
        $('#to_date_div2').hide();
        $('.product-content').show();
        $('.category-content').hide();
        $('.location-pro-content').show();
        $('.item-wise-expire-content').hide();
        $('.location-content').show();
        if (isAllProducts) {
            $('#customProductSelection').trigger('click');
        }
        $('#product').val('').trigger('change');
        $('.to_date_lbl,.from_date_lbl').addClass('hidden');
        $('.document-radio').addClass('hidden');
        $('#to_date,#from_date').hide();
        $('#div_serial').addClass('hidden');
        $('.non-inventory-products').addClass('hidden');
        $('.inv-level-radio').removeClass('hidden');
        $('.inv-level-reach-radio').removeClass('hidden');
        isMILWise = true;
        isROLWise = false;
        $('.isMILWise').click();
        isReachWise = true;
        isAllWise = false;
        $('.isReachWise').click();
    });
    $('#categoryWiseMIL').on('click', function() {
        reportType = 'categoryWiseMIL';
        hideStockReportIFrame();
        $('#from_date_div,#to_date_div').show();
        $('#to_date_div2').hide();
        $('.product-content').hide();
        $('.category-content').show();
        $('.location-pro-content').hide();
        $('.item-wise-expire-content').hide();
        $('.location-content').hide();
        if (isAllProducts) {
            $('#customProductSelection').trigger('click');
        }
        $('#product').val('').trigger('change');
        $('.to_date_lbl,.from_date_lbl').addClass('hidden');
        $('.document-radio').addClass('hidden');
        $('#to_date,#from_date').hide();
        $('#div_serial').addClass('hidden');
        $('.non-inventory-products').addClass('hidden');
        $('.inv-level-radio').removeClass('hidden');
        $('.inv-level-reach-radio').removeClass('hidden');
        isMILWise = true;
        isROLWise = false;
        $('.isMILWise').click();
        isReachWise = true;
        isAllWise = false;
        $('.isReachWise').click();
    });
    $('#globalStockValue').on('click', function() {
        reportType = 'globalStockValue';
        hideStockReportIFrame();
        $('#from_date_div,#to_date_div').show();
        $('#to_date_div2').hide();
        $('.product-content').show();
        $('.category-content').show();
        $('.location-pro-content').hide();
        $('.item-wise-expire-content').hide();
        $('.location-content').hide();
        if (isAllProducts) {
            $('#customProductSelection').trigger('click');
        }
        $('#product').val('').trigger('change');
        $('.to_date_lbl,.from_date_lbl').addClass('hidden');
        $('.document-radio').addClass('hidden');
        $('#to_date,#from_date').hide();
        $('#div_serial').addClass('hidden');
        $('.non-inventory-products').addClass('hidden');
        $('.inv-level-radio').addClass('hidden');
        $('.inv-level-reach-radio').addClass('hidden');
    });
    $('#globalStockValueWithAvg').on('click', function() {
        reportType = 'globalStockValueWithAvg';
        hideStockReportIFrame();
        $('#from_date_div,#to_date_div').show();
        $('#to_date_div2').hide();
        $('.product-content').show();
        $('.category-content').show();
        $('.location-pro-content').hide();
        $('.item-wise-expire-content').hide();
        $('.location-content').show();
        if (isAllProducts) {
            $('#customProductSelection').trigger('click');
        }
        $('#product').val('').trigger('change');
        $('.to_date_lbl,.from_date_lbl').addClass('hidden');
        $('.document-radio').addClass('hidden');
        $('#to_date,#from_date').hide();
        $('#div_serial').addClass('hidden');
        $('.non-inventory-products').addClass('hidden');
        $('.inv-level-radio').addClass('hidden');
        $('.inv-level-reach-radio').addClass('hidden');
    });
    $('#globalStockValueWithGrnPrice').on('click', function() {
        reportType = 'globalStockValueWithGrnPrice';
        hideStockReportIFrame();
        $('#from_date_div,#to_date_div').show();
        $('#to_date_div2').hide();
        $('.product-content').show();
        $('.category-content').show();
        $('.location-pro-content').hide();
        $('.item-wise-expire-content').hide();
        $('.location-content').show();
        if (isAllProducts) {
            $('#customProductSelection').trigger('click');
        }
        $('#product').val('').trigger('change');
        $('.document-radio').addClass('hidden');
        $('.to_date_lbl,.from_date_lbl').addClass('hidden');
        $('#to_date,#from_date').hide();
        $('#div_serial').addClass('hidden');
        $('.non-inventory-products').addClass('hidden');
        $('.inv-level-radio').addClass('hidden');
        $('.inv-level-reach-radio').addClass('hidden');
    });
    $('#locationWiseStockValue').on('click', function() {
        reportType = 'locationWiseStockValue';
        hideStockReportIFrame();
        $('#from_date_div,#to_date_div').show();
        $('#to_date_div2').hide();
        $('.location-pro-content').show();
        $('.product-content').show();
        $('.category-content').show();
        $('.document-radio').addClass('hidden');
        $('.item-wise-expire-content').hide();
        $('.location-content').show();
        if (isAllProducts) {
            $('#customProductSelection').trigger('click');
        }
        $('#product').val('').trigger('change');
        $('.to_date_lbl,.from_date_lbl').addClass('hidden');
        $('#to_date,#from_date').hide();
        $('#div_serial').addClass('hidden');
        $('.non-inventory-products').addClass('hidden');
        $('.inv-level-radio').addClass('hidden');
        $('.inv-level-reach-radio').addClass('hidden');
    });
    $('#globalItemMoving').on('click', function() {
        reportType = 'globalItemMoving';
        hideStockReportIFrame();
        $('#from_date_div,#to_date_div').show();
        $('#to_date_div2').hide();
        $('.location-pro-content').show();
        $('.product-content').show();
        $('.category-content').hide();
        $('.item-wise-expire-content').hide();
        $('.document-radio').addClass('hidden');
        $('.location-content').show();
        if (isAllProducts) {
            $('#customProductSelection').trigger('click');
        }
        $('#product').val('').trigger('change');
        $('.to_date_lbl,.from_date_lbl').removeClass('hidden');
        $('#to_date,#from_date').show();
        $('#to_date,#from_date').val('');
        $('#div_serial').addClass('hidden');
        $('.non-inventory-products').addClass('hidden');
        $('.inv-level-radio').addClass('hidden');
        $('.inv-level-reach-radio').addClass('hidden');
    });
    $('#globalItemMovingByDocumentDate').on('click', function() {
        reportType = 'globalItemMovingByDocumentDate';
        hideStockReportIFrame();
        $('#from_date_div,#to_date_div').show();
        $('#to_date_div2').hide();
        $('.location-pro-content').show();
        $('.product-content').show();
        $('.category-content').hide();
        $('.item-wise-expire-content').hide();
        $('.document-radio').addClass('hidden');
        $('.location-content').show();
        if (isAllProducts) {
            $('#customProductSelection').trigger('click');
        }
        $('#product').val('').trigger('change');
        $('.to_date_lbl,.from_date_lbl').removeClass('hidden');
        $('#to_date,#from_date').show();
        $('#to_date,#from_date').val('');
        $('#div_serial').addClass('hidden');
        $('.non-inventory-products').addClass('hidden');
        $('.inv-level-radio').addClass('hidden');
        $('.inv-level-reach-radio').addClass('hidden');
    });
    $('#globalItemMovingWithValue').on('click', function() {
        reportType = 'globalItemMovingWithValue';
        hideStockReportIFrame();
        $('#from_date_div,#to_date_div').show();
        $('#to_date_div2').hide();
        $('.location-pro-content').show();
        $('.product-content').show();
        $('.category-content').hide();
        $('.item-wise-expire-content').hide();
        $('.document-radio').addClass('hidden');
        $('.location-content').show();
        if (isAllProducts) {
            $('#customProductSelection').trigger('click');
        }
        $('#product').val('').trigger('change');
        $('.to_date_lbl,.from_date_lbl').removeClass('hidden');
        $('#to_date,#from_date').show();
        $('#to_date,#from_date').val('');
        $('#div_serial').addClass('hidden');
        $('.non-inventory-products').addClass('hidden');
        $('.inv-level-radio').addClass('hidden');
        $('.inv-level-reach-radio').addClass('hidden');
    });
    $('#invoicedNonInventoryItem').on('click', function() {
        reportType = 'invoicedNonInventoryItem';
        hideStockReportIFrame();
        $('#from_date_div,#to_date_div').show();
        $('#to_date_div2').hide();
        $('.location-pro-content').hide();
        $('.product-content').hide();
        $('.document-radio').addClass('hidden');
        $('.category-content').hide();
        $('.item-wise-expire-content').hide();
        $('.location-content').hide();
        $('.non-inventory-products').removeClass('hidden');
        if (isAllNonInventoryProducts) {
            $('#customNonInventoryProductSelection').trigger('click');
        }
        $('#product').val('').trigger('change');
        $('.to_date_lbl,.from_date_lbl').removeClass('hidden');
        $('#to_date,#from_date').show();
        $('#to_date,#from_date').val('');
        $('#div_serial').addClass('hidden');
        $('.inv-level-radio').addClass('hidden');
        $('.inv-level-reach-radio').addClass('hidden');
    });
    $('#stockAging').on('click', function() {
        reportType = 'stockAging';
        hideStockReportIFrame();
        $('#from_date_div,#to_date_div').show();
        $('#to_date_div2').hide();
        $('.product-content').show();
        $('.category-content').show();
        $('.location-pro-content').show();
        $('.item-wise-expire-content').hide();
        $('.location-content').hide();
        $('.document-radio').addClass('hidden');
        if (isAllProducts) {
            $('#customProductSelection').trigger('click');
        }
        $('#product').val('').trigger('change');
        $('.to_date_lbl,.from_date_lbl').addClass('hidden');
        $('#to_date,#from_date').hide();
        $('#div_serial').addClass('hidden');
        $('.non-inventory-products').addClass('hidden');
        $('.inv-level-radio').addClass('hidden');
        $('.inv-level-reach-radio').addClass('hidden');
    });
    $('#locationWiseDate').on('click', function() {

        $('.document-radio').removeClass('hidden');
        reportType = 'locationWiseDate';
        hideStockReportIFrame();
        $('.category-content').show();
        $('.product-content').show();
        $('.item-wise-expire-content').hide();
        $('.location-pro-content').show();
        $('.location-content').show();
        if (isAllProducts) {
            $('#customProductSelection').trigger('click');
        }
        $('#product').val('').trigger('change');
        $('.to_date_lbl,.from_date_lbl').addClass('hidden');
        $('#to_date,#from_date').hide();
        $('.to_date_lbl,.from_date_lbl').removeClass('hidden');
        $('#to_date,#from_date').show();
        $('#to_date,#from_date').val('');
        $('#from_date_div,#to_date_div').hide();
        $('#to_date_div2').show();
        $('#div_serial').addClass('hidden');
        $('.non-inventory-products').addClass('hidden');
        $('.inv-level-radio').addClass('hidden');
        $('.inv-level-reach-radio').addClass('hidden');
    });
     $('#locationWiseBatchDate').on('click', function() {
        reportType = 'locationWiseBatchData';
        hideStockReportIFrame();
        $('#from_date_div,#to_date_div').show();
        $('#to_date_div2').hide();
        $('.category-content').show();
        $('.product-content').show();
        $('.item-wise-expire-content').hide();
        $('.location-pro-content').show();
        $('.location-content').show();
        $('.document-radio').addClass('hidden');
        if (isAllProducts) {
            $('#customProductSelection').trigger('click');
        }
        $('#product').val('').trigger('change');
        $('.to_date_lbl,.from_date_lbl').addClass('hidden');
        $('#to_date,#from_date').hide();
        $('.non-inventory-products').addClass('hidden');
        $('#div_serial').addClass('hidden');
        $('.inv-level-radio').addClass('hidden');
        $('.inv-level-reach-radio').addClass('hidden');
    });
    $('#serialItemMoving').on('click', function() {
        reportType = 'serialItemMoving';
        hideStockReportIFrame();
        $('#from_date_div,#to_date_div').hide();
        $('#to_date_div2').hide();
        $('.category-content').hide();
        $('.product-content').show();
        $('.item-wise-expire-content').hide();
        $('.document-radio').addClass('hidden');
        $('.location-pro-content').hide();
        $('.location-content').hide();
        if (isAllProducts) {
            $('#customProductSelection').trigger('click');
        }
        $('#product').val('').trigger('change');
        $('.to_date_lbl,.from_date_lbl').addClass('hidden');
        $('#to_date,#from_date').hide();
        $('.non-inventory-products').addClass('hidden');
        $('#div_serial').removeClass('hidden');
        $('.inv-level-radio').addClass('hidden');
        $('.inv-level-reach-radio').addClass('hidden');
    });

    $('#viewReport').on('click', function() {
        setValues();
        if (reportType == 'itemDetails') {
            var selectedProducts = $('#product').val();
            var locIds = $('#location').val();
            if (selectedProducts || isAllProducts == true) {
                var url = BASE_URL + '/api/stock-in-hand-report/item-details-view';
                eb.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: {productIds: selectedProducts, isAllProducts: isAllProducts, categoryIds: categoryIds, locationIdList: locIds},
                    success: function(respond) {
                        if (respond.status == true) {
                            setContentToReport('stockReport', respond);

                        }
                    }
                });
            } else if (selectedProducts && selectedProducts.length == 0) {
                p_notification(false, eb.getMessage('ERR_NORECORDS_IN_SELECTPICKER'));
            } else {
                p_notification(false, eb.getMessage('ERR_STOCKREPO_SELEONEMORE'));
            }
        }
        else if (reportType == 'globalItemDetails') {
            var selectedProducts = $('#product').val();
            if (selectedProducts || isAllProducts == true) {
                var url = BASE_URL + '/api/stock-in-hand-report/global-item-details-view';
                eb.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: {productIds: selectedProducts, isAllProducts: isAllProducts, categoryIds: categoryIds},
                    success: function(respond) {
                        if (respond.status == true) {
                            setContentToReport('stockReport', respond);
                        }
                    }
                });
            } else if (selectedProducts && selectedProducts.length == 0) {
                p_notification(false, eb.getMessage('ERR_NORECORDS_IN_SELECTPICKER'));

            } else {
                p_notification(false, eb.getMessage('ERR_STOCKREPO_SELEONEMORE'));
            }
        }
        else if (reportType == 'itemWise') {
            var selectedProducts = $('#product').val();
            if (selectedProducts || isAllProducts == true) {
                var url = BASE_URL + '/api/stock-in-hand-report/index';
                eb.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: {productIds: selectedProducts, isAllProducts: isAllProducts, categoryIds: categoryIds},
                    success: function(respond) {
                        if (respond.status == true) {
                            setContentToReport('stockReport', respond);
                        }
                    }
                });
            } else if (selectedProducts && selectedProducts.length == 0) {
                p_notification(false, eb.getMessage('ERR_NORECORDS_IN_SELECTPICKER'));

            } else {
                p_notification(false, eb.getMessage('ERR_STOCKREPO_SELEONEMORE'));
            }
        }
        else if (reportType == 'categoryWise') {
            var locationIds = $('#location').val();
            if(categoryIds == null){
                p_notification(false, eb.getMessage('ERR_STOCKREPO_SELECATG'));
            } else if(locationIds == null){
                p_notification(false, eb.getMessage('ERR_PAYVOUCHER_SELELOC'));
            } else {
                var url = BASE_URL + '/api/stock-in-hand-report/view-category-wise-details';
                eb.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: {categoryIds: categoryIds, locationIds: locationIds},
                    success: function(respond) {
                        if (respond.status == true) {
                            setContentToReport('stockReport', respond);
                        }
                    }
                });
            }
        }
        else if (reportType == 'locationWise') {
            var locIds = $('#location').val();
            var selectedProducts = $('#product').val();
            if (selectedProducts || isAllProducts == true) {
                var url = BASE_URL + '/api/stock-in-hand-report/view-location-wise-stock-details';
                eb.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: {productIds: selectedProducts, locationIdList: locIds, isAllProducts: isAllProducts, batchSerialType: false},
                    success: function(respond) {
                        if (respond.status == true) {
                            setContentToReport('stockReport', respond);
                        }
                    }
                });
            } else if (selectedProducts && selectedProducts.length == 0) {
                p_notification(false, eb.getMessage('ERR_NORECORDS_IN_SELECTPICKER'));

            } else {
                p_notification(false, eb.getMessage('ERR_STOCKREPO_SELEONEMORE'));
            }
        }
        else if (reportType == 'itemWiseExpire') {
            var fromDate = $('#from_date').val();
            var toDate = $('#to_date').val();
            var selectedProducts = $('#product').val();
            if (isAllProducts != true && selectedProducts && selectedProducts.length == 0) {
                p_notification(false, eb.getMessage('ERR_NORECORDS_IN_SELECTPICKER'));
            } else if (isAllProducts != true && selectedProducts == null) {
                p_notification(false, eb.getMessage('ERR_STOCKREPO_SELEONEMORE'));
            } else if (fromDate == '' || fromDate == null) {
                p_notification(false, eb.getMessage('ERR_DAISALE_FROMD'));
                document.getElementById("from_date").focus();
            } else if (toDate == '' || toDate == null) {
                p_notification(false, eb.getMessage('ERR_DAISALE_TOD'));
                document.getElementById("to_date").focus();
            } else if (toDate < fromDate) {
                p_notification(false, eb.getMessage('ERR_SALEREPORT_DEFF'));

            }
            else {
                var url = BASE_URL + '/api/stock-in-hand-report/view-item-wise-expire';
                eb.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: {sbProductIds: selectedProducts, isAllProducts: isAllProducts, fromDate: fromDate, toDate: toDate},
                    success: function(respond) {
                        if (respond.status == true) {
                            setContentToReport('stockReport', respond);
                        }
                    }
                });
            }
        }
        else if (reportType == 'itemWiseMIL') {
            var locIds = $('#location').val();
            var selectedProducts = $('#product').val();
            var isMILType = false;

            if (isMILWise == true && !isROLWise) {
                var isMILType = true;
            } 
            if (isROLWise == true && !isMILWise) {
                var isMILType = false;
            }

            var isReach = false;

            if (isReachWise == true && !isAllWise) {
                var isReach = true;
            } 
            if (isAllWise == true && !isReachWise) {
                var isReach = false;
            }



            if (selectedProducts || isAllProducts == true) {
                var url = BASE_URL + '/api/stock-in-hand-report/view-item-wise-minimum-inventory-level';
                eb.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: {productIds: selectedProducts, isAllProducts: isAllProducts, locationIdList: locIds, isMILType: isMILType, isReach: isReach},
                    success: function(respond) {
                        if (respond.status == true) {
                            setContentToReport('stockReport', respond);
                        }
                    }
                });
            } else if (selectedProducts && selectedProducts.length == 0) {
                p_notification(false, eb.getMessage('ERR_NORECORDS_IN_SELECTPICKER'));
            } else {
                p_notification(false, eb.getMessage('ERR_STOCKREPO_SELEONEMORE'));
            }
        }
        else if (reportType == 'categoryWiseMIL') {

            var isMILType = false;

            if (isMILWise == true && !isROLWise) {
                var isMILType = true;
            } 
            if (isROLWise == true && !isMILWise) {
                var isMILType = false;
            }

            var isReach = false;

            if (isReachWise == true && !isAllWise) {
                var isReach = true;
            } 
            if (isAllWise == true && !isReachWise) {
                var isReach = false;
            }

            if (categoryIds) {
                var url = BASE_URL + '/api/stock-in-hand-report/view-category-wise-minimum-inventory-level';
                eb.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: {categoryIds: JSON.stringify(categoryIds), isMILType : isMILType, isReach: isReach},
                    success: function(respond) {
                        if (respond.status == true) {
                            setContentToReport('stockReport', respond);
                        }
                    }
                });
            } else if (lengthOfAvailableCategorys == 0) {
                p_notification(false, eb.getMessage('ERR_NORECORDS_IN_SELECTPICKER'));
            } else {
                p_notification(false, eb.getMessage('ERR_STOCKREPO_SELECATG'));
            }
        }
        else if (reportType == 'globalStockValue') {
            var selectedProducts = $('#product').val();
            if (selectedProducts || isAllProducts == true) {
                var url = BASE_URL + '/api/stock-in-hand-report/view-global-stock-value';
                eb.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: {productIds: selectedProducts, isAllProducts: isAllProducts, avgCosting : false, categoryIds: categoryIds},
                    success: function(respond) {
                        if (respond.status == true) {
                            setContentToReport('stockReport', respond);
                        }
                    }
                });
            } else if (selectedProducts && selectedProducts.length == 0) {
                p_notification(false, eb.getMessage('ERR_NORECORDS_IN_SELECTPICKER'));
            } else {
                p_notification(false, eb.getMessage('ERR_STOCKREPO_SELEONEMORE'));
            }
        }
        else if (reportType == 'locationWiseStockValue') {
            var selectedProducts = $('#product').val();
            var locIds = $('#location').val();
            if (locIds == null) {
                p_notification(false, eb.getMessage('ERR_CUSREPO_SELELOCATION'));
                document.getElementById("location").focus();
            } else if (selectedProducts || isAllProducts == true) {
                var url = BASE_URL + '/api/stock-in-hand-report/location-wise-stock-value-view';
                eb.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: {productIds: selectedProducts, isAllProducts: isAllProducts, categoryIds: categoryIds ,locationIdList: locIds},
                    success: function(respond) {
                        if (respond.status == true) {
                            setContentToReport('stockReport', respond);
                        }
                    }
                });
            } else if (selectedProducts && selectedProducts.length == 0) {
                p_notification(false, eb.getMessage('ERR_NORECORDS_IN_SELECTPICKER'));
            } else {
                p_notification(false, eb.getMessage('ERR_STOCKREPO_SELEONEMORE'));
            }
        }
        else if (reportType == 'globalItemMoving') {
            var fromDate = $('#from_date').val();
            var toDate = $('#to_date').val();
            var selectedProducts = $('#product').val();
            var locIds = $('#location').val();
            if (fromDate == null || fromDate == "") {
                p_notification(false, eb.getMessage('ERR_DAISALE_FROMD'));
                document.getElementById("from_date").focus();
            } else if (toDate == null || toDate == "") {
                p_notification(false, eb.getMessage('ERR_DAISALE_TOD'));
                document.getElementById("to_date").focus();
            } else if (toDate < fromDate) {
                p_notification(false, eb.getMessage('ERR_DAISALE_TODFROMD'));
                document.getElementById("to_date").focus();
            } else if (selectedProducts || isAllProducts == true) {
                var url = BASE_URL + '/api/stock-in-hand-report/view-global-item-moving';
                eb.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: {productIds: selectedProducts, isAllProducts: isAllProducts, fromDate: fromDate, toDate: toDate, locationIds: locIds,documentDateFlag : false},
                    success: function(respond) {
                        if (respond.status == true) {
                            setContentToReport('stockReport', respond);
                        }
                    }
                });
            } else if (selectedProducts && selectedProducts.length == 0) {
                p_notification(false, eb.getMessage('ERR_NORECORDS_IN_SELECTPICKER'));
            } else {
                p_notification(false, eb.getMessage('ERR_STOCKREPO_SELEONEMORE'));
            }
        } else if (reportType == 'globalItemMovingByDocumentDate') {
            var fromDate = $('#from_date').val();
            var toDate = $('#to_date').val();
            var selectedProducts = $('#product').val();
            var locIds = $('#location').val();
            if (fromDate == null || fromDate == "") {
                p_notification(false, eb.getMessage('ERR_DAISALE_FROMD'));
                document.getElementById("from_date").focus();
            } else if (toDate == null || toDate == "") {
                p_notification(false, eb.getMessage('ERR_DAISALE_TOD'));
                document.getElementById("to_date").focus();
            } else if (toDate < fromDate) {
                p_notification(false, eb.getMessage('ERR_DAISALE_TODFROMD'));
                document.getElementById("to_date").focus();
            } else if (selectedProducts || isAllProducts == true) {
                var url = BASE_URL + '/api/stock-in-hand-report/view-global-item-moving';
                eb.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: {productIds: selectedProducts, isAllProducts: isAllProducts, fromDate: fromDate, toDate: toDate, locationIds: locIds,documentDateFlag : true},
                    success: function(respond) {
                        if (respond.status == true) {
                            setContentToReport('stockReport', respond);
                        }
                    }
                });
            } else if (selectedProducts && selectedProducts.length == 0) {
                p_notification(false, eb.getMessage('ERR_NORECORDS_IN_SELECTPICKER'));
            } else {
                p_notification(false, eb.getMessage('ERR_STOCKREPO_SELEONEMORE'));
            }
        } else if (reportType == 'invoicedNonInventoryItem') {
            var fromDate = $('#from_date').val();
            var toDate = $('#to_date').val();
            var selectedProducts = $('#nonInventoryProduct').val();
            if (fromDate == null || fromDate == "") {
                p_notification(false, eb.getMessage('ERR_DAISALE_FROMD'));
                document.getElementById("from_date").focus();
            } else if (toDate == null || toDate == "") {
                p_notification(false, eb.getMessage('ERR_DAISALE_TOD'));
                document.getElementById("to_date").focus();
            } else if (toDate < fromDate) {
                p_notification(false, eb.getMessage('ERR_DAISALE_TODFROMD'));
                document.getElementById("to_date").focus();
            } else if (selectedProducts || isAllNonInventoryProducts == true) {
                var url = BASE_URL + '/api/stock-in-hand-report/view-invoiced-non-inventory-item';
                eb.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: {productIds: selectedProducts, isAllProducts: isAllNonInventoryProducts, fromDate: fromDate, toDate: toDate},
                    success: function(respond) {
                        if (respond.status == true) {
                            setContentToReport('stockReport', respond);
                        }
                    }
                });
            } else if (selectedProducts && selectedProducts.length == 0) {
                p_notification(false, eb.getMessage('ERR_NORECORDS_IN_SELECTPICKER'));
            } else {
                p_notification(false, eb.getMessage('ERR_STOCKREPO_SELEONEMORE'));
            }
        } else if (reportType == 'globalItemMovingWithValue') {
            var fromDate = $('#from_date').val();
            var toDate = $('#to_date').val();
            var selectedProducts = $('#product').val();
            var locIds = $('#location').val();
            
            if (fromDate == null || fromDate == "") {
                p_notification(false, eb.getMessage('ERR_DAISALE_FROMD'));
                document.getElementById("from_date").focus();
            } else if (toDate == null || toDate == "") {
                p_notification(false, eb.getMessage('ERR_DAISALE_TOD'));
                document.getElementById("to_date").focus();
            } else if (toDate < fromDate) {
                p_notification(false, eb.getMessage('ERR_DAISALE_TODFROMD'));
                document.getElementById("to_date").focus();
            } else if (locIds == null) {
                p_notification(false, eb.getMessage('ERR_CUSREPO_SELELOCATION'));
                document.getElementById("location").focus();
            } else if (selectedProducts || isAllProducts == true) {
                var url = BASE_URL + '/api/stock-in-hand-report/item-moving-with-value';
                eb.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: {productIds: selectedProducts, isAllProducts: isAllProducts, fromDate: fromDate, toDate: toDate, locationIds: locIds},
                    success: function(respond) {
                        if (respond.status == true) {
                            setContentToReport('stockReport', respond);
                        }
                    }
                });
            } else if (selectedProducts && selectedProducts.length == 0) {
                p_notification(false, eb.getMessage('ERR_NORECORDS_IN_SELECTPICKER'));
            } else {
                p_notification(false, eb.getMessage('ERR_STOCKREPO_SELEONEMORE'));
            }
        } else if (reportType == 'stockAging') {
            var selectedProducts = $('#product').val();
            if (selectedProducts || isAllProducts == true) {
                var url = BASE_URL + '/api/stock-in-hand-report/stock-aging';
                eb.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: {productIds: selectedProducts, isAllProducts: isAllProducts, categoryIds: categoryIds},
                    success: function(respond) {
                        if (respond.status == true) {
                            setContentToReport('stockReport', respond);

                        }
                    }
                });
            } else if (selectedProducts && selectedProducts.length == 0) {
                p_notification(false, eb.getMessage('ERR_NORECORDS_IN_SELECTPICKER'));
            } else {
                p_notification(false, eb.getMessage('ERR_STOCKREPO_SELEONEMORE'));
            }
        } else if(reportType == 'globalStockValueWithAvg' || reportType == 'globalStockValueWithGrnPrice'){
            var selectedProducts = $('#product').val();
            var locationIds = $('#location').val();
            var grnCosting = false;
            if (reportType == 'globalStockValueWithGrnPrice') {
                grnCosting = true;
            }
            if (selectedProducts || isAllProducts == true) {
                var url = BASE_URL + '/api/stock-in-hand-report/view-global-stock-value';
                eb.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: {productIds: selectedProducts, isAllProducts: isAllProducts, avgCosting : true, locationIds: locationIds, categoryIds: categoryIds, grnCosting: grnCosting},
                    success: function(respond) {
                        if (respond.status == true) {
                            setContentToReport('stockReport', respond);
                        }
                    }
                });
            } else if (selectedProducts && selectedProducts.length == 0) {
                p_notification(false, eb.getMessage('ERR_NORECORDS_IN_SELECTPICKER'));
            } else {
                p_notification(false, eb.getMessage('ERR_STOCKREPO_SELEONEMORE'));
            }
        }else if (reportType == 'locationWiseDate') {
            var locIds = $('#location').val();
            var selectedProducts = $('#product').val();
            var toDate = $('#to_date2').val();
            if (toDate == null || toDate == "") {
                p_notification(false, eb.getMessage('ERR_DAISALE_TOD'));
                document.getElementById("to_date2").focus();
            }else if (selectedProducts || isAllProducts == true) {

                if (isDocumentDateWise == true && !isActDateWise) {
                    var isDocDateWise = true;
                } 
                if (isActDateWise == true && !isDocumentDateWise) {
                    var isDocDateWise = false;
                }


                var url = BASE_URL + '/api/stock-in-hand-report/view-location-and-date-wise-stock-details';
                eb.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: {productIds: selectedProducts, locationIdList: locIds, isAllProducts: isAllProducts, toDate: toDate, categoryIds: categoryIds, isDocDateWise: isDocDateWise},
                    success: function(respond) {
                        if (respond.status == true) {
                            setContentToReport('stockReport', respond);
                        }
                    }
                });
            } else if (selectedProducts && selectedProducts.length == 0) {
                p_notification(false, eb.getMessage('ERR_NORECORDS_IN_SELECTPICKER'));

            } else {
                p_notification(false, eb.getMessage('ERR_STOCKREPO_SELEONEMORE'));
            }
        } else if(reportType == 'locationWiseBatchData'){
            var locIds = $('#location').val();
            var selectedProducts = $('#product').val();
            if (selectedProducts || isAllProducts == true) {
                var url = BASE_URL + '/api/stock-in-hand-report/view-location-wise-stock-details';
                eb.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: {productIds: selectedProducts, locationIdList: locIds, isAllProducts: isAllProducts, batchSerialType: true, categoryIds: categoryIds},
                    success: function(respond) {
                        if (respond.status == true) {
                            setContentToReport('stockReport', respond);
                        }
                    }
                });
            } else if (selectedProducts && selectedProducts.length == 0) {
                p_notification(false, eb.getMessage('ERR_NORECORDS_IN_SELECTPICKER'));

            } else {
                p_notification(false, eb.getMessage('ERR_STOCKREPO_SELEONEMORE'));
            }
        } 
        else if(reportType == 'serialItemMoving'){
            var selectedProducts = $('#product').val();
            var serialIDS = $('#serialName').val();
                                    
            if ((selectedProducts || isAllProducts == true) && serialIDS || isAllSerials == true ) {
                var url = BASE_URL + '/api/stock-in-hand-report/view-serial-movement-details';
                eb.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: {productIds: selectedProducts, isAllProducts: isAllProducts, serialIds: serialIDS, isAllSerial: isAllSerials},
                    success: function(respond) {
                        if (respond.status == true) {
                            setContentToReport('stockReport', respond);
                        }
                    }
                });
            } else if (!selectedProducts && isAllProducts == false) {
                p_notification(false, eb.getMessage('ERR_STOCKREPO_SELEONEMORE'));

            } else if (!serialIDS && isAllSerials == false) {
                p_notification(false, eb.getMessage('ERR_NO_SERIAL'));

            } else {
                p_notification(false, eb.getMessage('ERR_NORECORDS_IN_SELECTPICKER'));
            }
        }
        else {
            p_notification(false, eb.getMessage('ERR_SALEREPORT_CORRETYPE'));
        }
    });

    $('#generatePdf').on('click', function() {
        setValues();
        if (reportType == 'itemDetails') {
            var selectedProducts = $('#product').val();
            var locIds = $('#location').val();
            if (selectedProducts || isAllProducts == true) {
                var url = BASE_URL + '/api/stock-in-hand-report/generate-location-wise-item-details-pdf';
                eb.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: {productIds: selectedProducts, isAllProducts: isAllProducts, categoryIds: categoryIds, locationIdList: locIds},
                    success: function(respond) {
                        if (respond.status == true) {
                            window.open(BASE_URL + '/' + respond.data, '_blank');
                        }
                    }
                });
            } else if (selectedProducts && selectedProducts.length == 0) {
                p_notification(false, eb.getMessage('ERR_NORECORDS_IN_SELECTPICKER'));
            } else {
                p_notification(false, eb.getMessage('ERR_STOCKREPO_SELEONEMORE'));
            }
        }
        else if (reportType == 'globalItemDetails') {
            var selectedProducts = $('#product').val();
            if (selectedProducts || isAllProducts == true) {
                var url = BASE_URL + '/api/stock-in-hand-report/generate-global-item-details-pdf';
                eb.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: {productIds: selectedProducts, isAllProducts: isAllProducts, categoryIds: categoryIds},
                    success: function(respond) {
                        if (respond.status == true) {
                            window.open(BASE_URL + '/' + respond.data, '_blank');
                        }
                    }
                });
            } else if (selectedProducts && selectedProducts.length == 0) {
                p_notification(false, eb.getMessage('ERR_NORECORDS_IN_SELECTPICKER'));
            } else {
                p_notification(false, eb.getMessage('ERR_STOCKREPO_SELEONEMORE'));
            }
        }
        else if (reportType == 'itemWise') {
            var selectedProducts = $('#product').val();
            if (selectedProducts || isAllProducts == true) {
                var url = BASE_URL + '/api/stock-in-hand-report/generate-item-wise-stock-pdf';
                eb.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: {productIds: selectedProducts, isAllProducts: isAllProducts, categoryIds: categoryIds},
                    success: function(respond) {
                        if (respond.status == true) {
                            window.open(BASE_URL + '/' + respond.data, '_blank');
                        }
                    }
                });
            } else if (selectedProducts && selectedProducts.length == 0) {
                p_notification(false, eb.getMessage('ERR_NORECORDS_IN_SELECTPICKER'));
            } else {
                p_notification(false, eb.getMessage('ERR_STOCKREPO_SELEONEMORE'));
            }
        }
        else if (reportType == 'categoryWise') {
            var locationIds = $('#location').val();
            if(categoryIds == null){
                p_notification(false, eb.getMessage('ERR_STOCKREPO_SELECATG'));
            } else if(locationIds == null){
                p_notification(false, eb.getMessage('ERR_PAYVOUCHER_SELELOC'));
            } else {
                var url = BASE_URL + '/api/stock-in-hand-report/generate-category-wise-stock-pdf';
                eb.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: {categoryIds: categoryIds, locationIds : locationIds},
                    success: function(respond) {
                        if (respond.status == true) {
                            window.open(BASE_URL + '/' + respond.data, '_blank');
                        }
                    }
                });
            }
        }
        else if (reportType == 'locationWise') {
            var locId = $('#location').val();
            var selectedProducts = $('#product').val();
            if (selectedProducts || isAllProducts == true) {
                var url = BASE_URL + '/api/stock-in-hand-report/generate-location-wise-stock-pdf';
                eb.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: {productIds: selectedProducts, locationIdList: locId, isAllProducts: isAllProducts, batchSerialType: false},
                    success: function(respond) {
                        if (respond.status == true) {
                            window.open(BASE_URL + '/' + respond.data, '_blank');
                        }
                    }
                });
            } else if (selectedProducts && selectedProducts.length == 0) {
                p_notification(false, eb.getMessage('ERR_NORECORDS_IN_SELECTPICKER'));
            } else {
                p_notification(false, eb.getMessage('ERR_STOCKREPO_SELEONEMORE'));
            }
        }
        else if (reportType == 'itemWiseExpire') {
            var selectedProducts = $('#product').val();
            var fromDate = $('#from_date').val();
            var toDate = $('#to_date').val();
            if (isAllProducts != true && selectedProducts && selectedProducts.length == 0) {
                p_notification(false, eb.getMessage('ERR_NORECORDS_IN_SELECTPICKER'));
            } else if (isAllProducts != true && selectedProducts == null) {
                p_notification(false, eb.getMessage('ERR_STOCKREPO_SELEONEMORE'));
            } else if (fromDate == '' || fromDate == null) {
                p_notification(false, eb.getMessage('ERR_DAISALE_FROMD'));
                document.getElementById("from_date").focus();
            } else if (toDate == '' || toDate == null) {
                p_notification(false, eb.getMessage('ERR_DAISALE_TOD'));
                document.getElementById("to_date").focus();
            } else if (toDate < fromDate) {
                p_notification(false, eb.getMessage('ERR_SALEREPORT_DEFF'));
                return false;
            }
            else {
                var url = BASE_URL + '/api/stock-in-hand-report/generate-item-wise-expire-pdf';
                eb.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: {fromDate: fromDate, toDate: toDate, sbProductIds: selectedProducts, isAllProducts: isAllProducts},
                    success: function(respond) {
                        if (respond.status == true) {
                            window.open(BASE_URL + '/' + respond.data, '_blank');
                        }
                    }
                });
            }
        }
        else if (reportType == 'itemWiseMIL') {
            var locIds = $('#location').val();
            var selectedProducts = $('#product').val();
            var isMILType = false;

            if (isMILWise == true && !isROLWise) {
                var isMILType = true;
            } 
            if (isROLWise == true && !isMILWise) {
                var isMILType = false;
            }

            var isReach = false;

            if (isReachWise == true && !isAllWise) {
                var isReach = true;
            } 
            if (isAllWise == true && !isReachWise) {
                var isReach = false;
            }

            if (selectedProducts || isAllProducts == true) {
                var url = BASE_URL + '/api/stock-in-hand-report/generate-item-wise-minimum-inventory-level-pdf';
                eb.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: {productIds: selectedProducts, isAllProducts: isAllProducts, locationIdList: locIds, isMILType : isMILType, isReach: isReach},
                    success: function(respond) {
                        if (respond.status == true) {
                            window.open(BASE_URL + '/' + respond.data, '_blank');
                        }
                    }
                });
            } else if (selectedProducts && selectedProducts.length == 0) {
                p_notification(false, eb.getMessage('ERR_NORECORDS_IN_SELECTPICKER'));
            } else {
                p_notification(false, eb.getMessage('ERR_STOCKREPO_SELEONEMORE'));
            }
        }
        else if (reportType == 'categoryWiseMIL') {
            var isMILType = false;

            if (isMILWise == true && !isROLWise) {
                var isMILType = true;
            } 
            if (isROLWise == true && !isMILWise) {
                var isMILType = false;
            }

            var isReach = false;

            if (isReachWise == true && !isAllWise) {
                var isReach = true;
            } 
            if (isAllWise == true && !isReachWise) {
                var isReach = false;
            }


            if (categoryIds) {
                var url = BASE_URL + '/api/stock-in-hand-report/generate-category-wise-minimum-inventory-level-pdf';
                eb.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: {categoryIds: JSON.stringify(categoryIds), isMILType : isMILType, isReach: isReach},
                    success: function(respond) {
                        if (respond.status == true) {
                            window.open(BASE_URL + '/' + respond.data, '_blank');
                        }
                    }
                });
            } else if (lengthOfAvailableCategorys == 0) {
                p_notification(false, eb.getMessage('ERR_NORECORDS_IN_SELECTPICKER'));
            } else {
                p_notification(false, eb.getMessage('ERR_STOCKREPO_SELECATG'));
            }
        }
        else if (reportType == 'globalStockValue') {
            var selectedProducts = $('#product').val();
            if (selectedProducts || isAllProducts == true) {
                var url = BASE_URL + '/api/stock-in-hand-report/generate-global-stock-value-pdf';
                eb.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: {productIds: selectedProducts, isAllProducts: isAllProducts, avgCosting: false, categoryIds: categoryIds},
                    success: function(respond) {
                        if (respond.status == true) {
                            p_notification(true, respond.msg);
                        }
                    }
                });
            } else if (selectedProducts && selectedProducts.length == 0) {
                p_notification(false, eb.getMessage('ERR_NORECORDS_IN_SELECTPICKER'));
            } else {
                p_notification(false, eb.getMessage('ERR_STOCKREPO_SELEONEMORE'));
            }
        }
        else if (reportType == 'locationWiseStockValue') {
            var selectedProducts = $('#product').val();
            var locIds = $('#location').val();
            if (locIds == null) {
                p_notification(false, eb.getMessage('ERR_CUSREPO_SELELOCATION'));
                document.getElementById("location").focus();
            } else if (selectedProducts || isAllProducts == true) {
                var url = BASE_URL + '/api/stock-in-hand-report/generate-location-wise-stock-value-pdf';
                eb.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: {productIds: selectedProducts, isAllProducts: isAllProducts, categoryIds: categoryIds, locationIdList: locIds},
                    success: function(respond) {
                        if (respond.status == true) {
                            p_notification(true, respond.msg);
                        }
                    }
                });
            } else if (selectedProducts && selectedProducts.length == 0) {
                p_notification(false, eb.getMessage('ERR_NORECORDS_IN_SELECTPICKER'));
            } else {
                p_notification(false, eb.getMessage('ERR_STOCKREPO_SELEONEMORE'));
            }
        }
        else if (reportType == 'globalItemMoving') {
            var fromDate = $('#from_date').val();
            var toDate = $('#to_date').val();
            var selectedProducts = $('#product').val();
            var locIds = $('#location').val();
            if (fromDate == null || fromDate == "") {
                p_notification(false, eb.getMessage('ERR_DAISALE_FROMD'));
                document.getElementById("from_date").focus();
            } else if (toDate == null || toDate == "") {
                p_notification(false, eb.getMessage('ERR_DAISALE_TOD'));
                document.getElementById("to_date").focus();
            } else if (toDate < fromDate) {
                p_notification(false, eb.getMessage('ERR_DAISALE_TODFROMD'));
                document.getElementById("to_date").focus();
            } else if (selectedProducts || isAllProducts == true) {
                var url = BASE_URL + '/api/stock-in-hand-report/generate-global-item-moving-pdf';
                eb.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: {productIds: selectedProducts, fromDate: fromDate, toDate: toDate, isAllProducts: isAllProducts, locationIds: locIds, documentDateFlag : false},
                    success: function(respond) {
                        if (respond.status == true) {
                            p_notification(true, respond.msg);
                        }
                    }
                });
            } else if (selectedProducts && selectedProducts.length == 0) {
                p_notification(false, eb.getMessage('ERR_NORECORDS_IN_SELECTPICKER'));
            } else {
                p_notification(false, eb.getMessage('ERR_STOCKREPO_SELEONEMORE'));
            }
        }
        else if (reportType == 'globalItemMovingByDocumentDate') {
            var fromDate = $('#from_date').val();
            var toDate = $('#to_date').val();
            var selectedProducts = $('#product').val();
            var locIds = $('#location').val();
            if (fromDate == null || fromDate == "") {
                p_notification(false, eb.getMessage('ERR_DAISALE_FROMD'));
                document.getElementById("from_date").focus();
            } else if (toDate == null || toDate == "") {
                p_notification(false, eb.getMessage('ERR_DAISALE_TOD'));
                document.getElementById("to_date").focus();
            } else if (toDate < fromDate) {
                p_notification(false, eb.getMessage('ERR_DAISALE_TODFROMD'));
                document.getElementById("to_date").focus();
            } else if (selectedProducts || isAllProducts == true) {
                var url = BASE_URL + '/api/stock-in-hand-report/generate-global-item-moving-pdf';
                eb.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: {productIds: selectedProducts, fromDate: fromDate, toDate: toDate, isAllProducts: isAllProducts, locationIds: locIds, documentDateFlag : true},
                    success: function(respond) {
                        if (respond.status == true) {
                            p_notification(true, respond.msg);
                        }
                    }
                });
            } else if (selectedProducts && selectedProducts.length == 0) {
                p_notification(false, eb.getMessage('ERR_NORECORDS_IN_SELECTPICKER'));
            } else {
                p_notification(false, eb.getMessage('ERR_STOCKREPO_SELEONEMORE'));
            }
        }
        else if (reportType == 'invoicedNonInventoryItem') {
            var fromDate = $('#from_date').val();
            var toDate = $('#to_date').val();
            var selectedProducts = $('#nonInventoryProduct').val();
            if (fromDate == null || fromDate == "") {
                p_notification(false, eb.getMessage('ERR_DAISALE_FROMD'));
                document.getElementById("from_date").focus();
            } else if (toDate == null || toDate == "") {
                p_notification(false, eb.getMessage('ERR_DAISALE_TOD'));
                document.getElementById("to_date").focus();
            } else if (toDate < fromDate) {
                p_notification(false, eb.getMessage('ERR_DAISALE_TODFROMD'));
                document.getElementById("to_date").focus();
            } else if (selectedProducts || isAllNonInventoryProducts == true) {
                var url = BASE_URL + '/api/stock-in-hand-report/generate-invoiced-non-inventory-item-pdf';
                eb.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: {productIds: selectedProducts, fromDate: fromDate, toDate: toDate, isAllProducts: isAllNonInventoryProducts},
                    success: function(respond) {
                        if (respond.status == true) {
                            window.open(BASE_URL + '/' + respond.data, '_blank');
                        }
                    }
                });
            } else if (selectedProducts && selectedProducts.length == 0) {
                p_notification(false, eb.getMessage('ERR_NORECORDS_IN_SELECTPICKER'));
            } else {
                p_notification(false, eb.getMessage('ERR_STOCKREPO_SELEONEMORE'));
            }
        } else if (reportType == 'globalItemMovingWithValue') {
            var fromDate = $('#from_date').val();
            var toDate = $('#to_date').val();
            var selectedProducts = $('#product').val();
            var locIds = $('#location').val();
            if (fromDate == null || fromDate == "") {
                p_notification(false, eb.getMessage('ERR_DAISALE_FROMD'));
                document.getElementById("from_date").focus();
            } else if (toDate == null || toDate == "") {
                p_notification(false, eb.getMessage('ERR_DAISALE_TOD'));
                document.getElementById("to_date").focus();
            } else if (toDate < fromDate) {
                p_notification(false, eb.getMessage('ERR_DAISALE_TODFROMD'));
                document.getElementById("to_date").focus();
            } else if (locIds == null) {
                p_notification(false, eb.getMessage('ERR_CUSREPO_SELELOCATION'));
                document.getElementById("location").focus();
            } else if (selectedProducts || isAllProducts == true) {
                var url = BASE_URL + '/api/stock-in-hand-report/item-moving-with-value-pdf';
                eb.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: {productIds: selectedProducts, isAllProducts: isAllProducts, fromDate: fromDate, toDate: toDate, locationIds: locIds},
                    success: function(respond) {
                        if (respond.status == true) {
                            window.open(BASE_URL + '/' + respond.data, '_blank');
                        }
                    }
                });
            } else if (selectedProducts && selectedProducts.length == 0) {
                p_notification(false, eb.getMessage('ERR_NORECORDS_IN_SELECTPICKER'));
            } else {
                p_notification(false, eb.getMessage('ERR_STOCKREPO_SELEONEMORE'));
            }
        } else if (reportType == 'stockAging') {
            var selectedProducts = $('#product').val();
            if (selectedProducts || isAllProducts == true) {
                var url = BASE_URL + '/api/stock-in-hand-report/stock-aging-pdf';
                eb.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: {productIds: selectedProducts, isAllProducts: isAllProducts, categoryIds: categoryIds},
                    success: function(respond) {
                        if (respond.status == true) {
                            window.open(BASE_URL + '/' + respond.data, '_blank');
                        }
                    }
                });
            } else if (selectedProducts && selectedProducts.length == 0) {
                p_notification(false, eb.getMessage('ERR_NORECORDS_IN_SELECTPICKER'));
            } else {
                p_notification(false, eb.getMessage('ERR_STOCKREPO_SELEONEMORE'));
            }
        } else if(reportType == 'globalStockValueWithAvg' || reportType == 'globalStockValueWithGrnPrice'){
            var selectedProducts = $('#product').val();
            var locationIds = $('#location').val();
            var grnCosting = false;
            if (reportType == 'globalStockValueWithGrnPrice') {
                grnCosting = true;
            }
            if (selectedProducts || isAllProducts == true) {
                var url = BASE_URL + '/api/stock-in-hand-report/generate-global-stock-value-pdf';
                eb.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: {productIds: selectedProducts, isAllProducts: isAllProducts, avgCosting : true, locationIds: locationIds, categoryIds: categoryIds, grnCosting: grnCosting},
                    success: function(respond) {
                        if (respond.status == true) {
                            p_notification(true, respond.msg);
                        }
                    }
                });
            }
        } else if (reportType == 'locationWiseDate') {
            var locId = $('#location').val();
            var selectedProducts = $('#product').val();
            var toDate = $('#to_date2').val();
            if (toDate == null || toDate == "") {
                p_notification(false, eb.getMessage('ERR_DAISALE_TOD'));
                document.getElementById("to_date2").focus();
            }else if (selectedProducts || isAllProducts == true) {
                if (isDocumentDateWise == true && !isActDateWise) {
                    var isDocDateWise = true;
                } 
                if (isActDateWise == true && !isDocumentDateWise) {
                    var isDocDateWise = false;
                }
                var url = BASE_URL + '/api/stock-in-hand-report/generate-location-and-date-wise-stock-pdf';
                eb.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: {productIds: selectedProducts, locationIdList: locId, isAllProducts: isAllProducts, toDate: toDate, categoryIds: categoryIds, isDocDateWise: isDocDateWise},
                    success: function(respond) {
                        if (respond.status == true) {
                            window.open(BASE_URL + '/' + respond.data, '_blank');
                        }
                    }
                });
            } else if (selectedProducts && selectedProducts.length == 0) {
                p_notification(false, eb.getMessage('ERR_NORECORDS_IN_SELECTPICKER'));
            } else {
                p_notification(false, eb.getMessage('ERR_STOCKREPO_SELEONEMORE'));
            }
        } 
        else if (reportType == 'locationWiseBatchData') 
        {
            var locId = $('#location').val();
            var selectedProducts = $('#product').val();
            if (selectedProducts || isAllProducts == true) {
                var url = BASE_URL + '/api/stock-in-hand-report/generate-location-wise-stock-pdf';
                eb.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: {productIds: selectedProducts, locationIdList: locId, isAllProducts: isAllProducts, batchSerialType: true, categoryIds: categoryIds},
                    success: function(respond) {
                        if (respond.status == true) {
                            window.open(BASE_URL + '/' + respond.data, '_blank');
                        }
                    }
                });
            } else if (selectedProducts && selectedProducts.length == 0) {
                p_notification(false, eb.getMessage('ERR_NORECORDS_IN_SELECTPICKER'));
            } else {
                p_notification(false, eb.getMessage('ERR_STOCKREPO_SELEONEMORE'));
            }
        } 
        else if(reportType == 'serialItemMoving'){
            var selectedProducts = $('#product').val();
            var serialIDS = $('#serialName').val();
                                    
            if ((selectedProducts || isAllProducts == true) && serialIDS || isAllSerials == true ) {
                var url = BASE_URL + '/api/stock-in-hand-report/view-serial-movement-details-pdf';
                eb.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: {productIds: selectedProducts, isAllProducts: isAllProducts, serialIds: serialIDS, isAllSerial: isAllSerials},
                    success: function(respond) {
                       if (respond.status == true) {
                            p_notification(true, respond.msg);
                        }
                    }
                });
            } else if (!selectedProducts && isAllProducts == false) {
                p_notification(false, eb.getMessage('ERR_STOCKREPO_SELEONEMORE'));

            } else if (!serialIDS && isAllSerials == false) {
                p_notification(false, eb.getMessage('ERR_NO_SERIAL'));

            } else {
                p_notification(false, eb.getMessage('ERR_NORECORDS_IN_SELECTPICKER'));
            }
        } 
        else {
            p_notification(false, eb.getMessage('ERR_SALEREPORT_CORRETYPE'));
        }
    });

    $('#csvReport').on('click', function() {
        setValues();
        if (reportType == 'itemDetails') {
            var selectedProducts = $('#product').val();
            var locIds = $('#location').val();
            if (selectedProducts || isAllProducts == true) {
                var url = BASE_URL + '/api/stock-in-hand-report/generate-location-wise-item-details-sheet';
                eb.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: {productIds: selectedProducts, isAllProducts: isAllProducts, categoryIds: categoryIds, locationIdList: locIds},
                    success: function(respond) {
                        if (respond.status == true) {
                            window.open(BASE_URL + '/' + respond.data, '_blank');
                        }
                    }
                });
            } else if (selectedProducts && selectedProducts.length == 0) {
                p_notification(false, eb.getMessage('ERR_NORECORDS_IN_SELECTPICKER'));
            } else {
                p_notification(false, eb.getMessage('ERR_STOCKREPO_SELEONEMORE'));
            }
        } else if (reportType == 'globalItemDetails') {
            var selectedProducts = $('#product').val();
            if (selectedProducts || isAllProducts == true) {
                var url = BASE_URL + '/api/stock-in-hand-report/generate-global-item-details-sheet';
                eb.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: {productIds: selectedProducts, isAllProducts: isAllProducts, categoryIds: categoryIds},
                    success: function(respond) {
                        if (respond.status == true) {
                            window.open(BASE_URL + '/' + respond.data, '_blank');
                        }
                    }
                });
            } else if (selectedProducts && selectedProducts.length == 0) {
                p_notification(false, eb.getMessage('ERR_NORECORDS_IN_SELECTPICKER'));
            } else {
                p_notification(false, eb.getMessage('ERR_STOCKREPO_SELEONEMORE'));
            }
        } else if (reportType == 'itemWise') {
            var selectedProducts = $('#product').val();
            if (selectedProducts || isAllProducts == true) {
                var url = BASE_URL + '/api/stock-in-hand-report/generate-item-wise-stock-sheet';
                eb.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: {productIds: selectedProducts, isAllProducts: isAllProducts, categoryIds: categoryIds},
                    success: function(respond) {
                        if (respond.status == true) {
                            window.open(BASE_URL + '/' + respond.data, '_blank');
                        }
                    }
                });
            } else if (selectedProducts && selectedProducts.length == 0) {
                p_notification(false, eb.getMessage('ERR_NORECORDS_IN_SELECTPICKER'));
            } else {
                p_notification(false, eb.getMessage('ERR_STOCKREPO_SELEONEMORE'));
            }
        } else if (reportType == 'categoryWise') {
            var locationIds = $('#location').val();
            if(categoryIds == null){
                p_notification(false, eb.getMessage('ERR_STOCKREPO_SELECATG'));
            } else if(locationIds == null){
                p_notification(false, eb.getMessage('ERR_PAYVOUCHER_SELELOC'));
            } else {
                var url = BASE_URL + '/api/stock-in-hand-report/generate-category-wise-stock-sheet';
                eb.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: {categoryIds: categoryIds, locationIds : locationIds},
                    success: function(respond) {
                        if (respond.status == true) {
                            window.open(BASE_URL + '/' + respond.data, '_blank');
                        }
                    }
                });
            }
        } else if (reportType == 'locationWise') {
            var locId = $('#location').val();
            var selectedProducts = $('#product').val();
            if (selectedProducts || isAllProducts == true) {
                var url = BASE_URL + '/api/stock-in-hand-report/generate-location-wise-stock-sheet';
                eb.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: {productIds: selectedProducts, locationIdList: locId, isAllProducts: isAllProducts, batchSerialType: false},
                    success: function(respond) {
                        if (respond.status == true) {
                            window.open(BASE_URL + '/' + respond.data, '_blank');
                        }
                    }
                });
            } else if (selectedProducts && selectedProducts.length == 0) {
                p_notification(false, eb.getMessage('ERR_NORECORDS_IN_SELECTPICKER'));
            } else {
                p_notification(false, eb.getMessage('ERR_STOCKREPO_SELEONEMORE'));
            }
        } else if (reportType == 'itemWiseExpire') {
            var selectedProducts = $('#product').val();
            var fromDate = $('#from_date').val();
            var toDate = $('#to_date').val();
            if (isAllProducts == true && selectedProducts && selectedProducts.length == 0) {
                p_notification(false, eb.getMessage('ERR_NORECORDS_IN_SELECTPICKER'));
            } else if (isAllProducts == true && selectedProducts && selectedProducts == null) {
                p_notification(false, eb.getMessage('ERR_STOCKREPO_SELEONEMORE'));
            } else if (fromDate == '' || fromDate == null) {
                p_notification(false, eb.getMessage('ERR_DAISALE_FROMD'));
                document.getElementById("from_date").focus();
            } else if (toDate == '' || toDate == null) {
                p_notification(false, eb.getMessage('ERR_DAISALE_TOD'));
                document.getElementById("to_date").focus();
            } else if (toDate < fromDate) {
                p_notification(false, eb.getMessage('ERR_SALEREPORT_DEFF'));
                return false;
            }
            else {
                var url = BASE_URL + '/api/stock-in-hand-report/generate-item-wise-expire-sheet';
                eb.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: {fromDate: fromDate, toDate: toDate, sbProductIds: selectedProducts, isAllProducts: isAllProducts},
                    success: function(respond) {
                        if (respond.status == true) {
                            window.open(BASE_URL + '/' + respond.data, '_blank');
                        }
                    }
                });
            }
        } else if (reportType == 'itemWiseMIL') {
            var locIds = $('#location').val();
            var selectedProducts = $('#product').val();
            var isMILType = false;

            if (isMILWise == true && !isROLWise) {
                var isMILType = true;
            } 
            if (isROLWise == true && !isMILWise) {
                var isMILType = false;
            }

            var isReach = false;

            if (isReachWise == true && !isAllWise) {
                var isReach = true;
            } 
            if (isAllWise == true && !isReachWise) {
                var isReach = false;
            }


            if (selectedProducts || isAllProducts == true) {
                var url = BASE_URL + '/api/stock-in-hand-report/generate-item-wise-minimum-inventory-level-sheet';
                eb.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: {productIds: selectedProducts, isAllProducts: isAllProducts, locationIdList: locIds, isMILType: isMILType, isReach: isReach},
                    success: function(respond) {
                        if (respond.status == true) {
                            window.open(BASE_URL + '/' + respond.data, '_blank');
                        }
                    }
                });
            } else if (selectedProducts && selectedProducts.length == 0) {
                p_notification(false, eb.getMessage('ERR_NORECORDS_IN_SELECTPICKER'));
            } else {
                p_notification(false, eb.getMessage('ERR_STOCKREPO_SELEONEMORE'));
            }
        } else if (reportType == 'categoryWiseMIL') {
            var isMILType = false;

            if (isMILWise == true && !isROLWise) {
                var isMILType = true;
            } 
            if (isROLWise == true && !isMILWise) {
                var isMILType = false;
            }

            var isReach = false;

            if (isReachWise == true && !isAllWise) {
                var isReach = true;
            } 
            if (isAllWise == true && !isReachWise) {
                var isReach = false;
            }

            if (categoryIds) {
                var url = BASE_URL + '/api/stock-in-hand-report/generate-category-wise-minimum-inventory-level-sheet';
                eb.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: {categoryIds: JSON.stringify(categoryIds), isMILType: isMILType, isReach : isReach},
                    success: function(respond) {
                        if (respond.status == true) {
                            window.open(BASE_URL + '/' + respond.data, '_blank');
                        }
                    }
                });
            } else if (lengthOfAvailableCategorys == 0) {
                p_notification(false, eb.getMessage('ERR_NORECORDS_IN_SELECTPICKER'));
            } else {
                p_notification(false, eb.getMessage('ERR_STOCKREPO_SELECATG'));
            }
        } else if (reportType == 'globalStockValue') {
            var selectedProducts = $('#product').val();
            var locIds = $('#location').val();
            if (selectedProducts || isAllProducts == true) {
                var url = BASE_URL + '/api/stock-in-hand-report/generate-global-stock-value-sheet';
                eb.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: {productIds: selectedProducts, isAllProducts: isAllProducts, locationIds: locIds, categoryIds: categoryIds},
                    success: function(respond) {
                        if (respond.status == true) {
                            p_notification(true, respond.msg);
                        }
                    }
                });
            } else if (selectedProducts && selectedProducts.length == 0) {
                p_notification(false, eb.getMessage('ERR_NORECORDS_IN_SELECTPICKER'));
            } else {
                p_notification(false, eb.getMessage('ERR_STOCKREPO_SELEONEMORE'));
            }
        } else if (reportType == 'locationWiseStockValue') {
            var selectedProducts = $('#product').val();
            var locIds = $('#location').val();
            if (locIds == null) {
                p_notification(false, eb.getMessage('ERR_CUSREPO_SELELOCATION'));
                document.getElementById("location").focus();
            } else if (selectedProducts || isAllProducts == true) {
                var url = BASE_URL + '/api/stock-in-hand-report/generate-location-wise-stock-value-sheet';
                eb.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: {productIds: selectedProducts, isAllProducts: isAllProducts, avgCosting: false, categoryIds: categoryIds ,locationIdList: locIds},
                    success: function(respond) {
                        if (respond.status == true) {
                            p_notification(true, respond.msg);
                        }
                    }
                });
            } else if (selectedProducts && selectedProducts.length == 0) {
                p_notification(false, eb.getMessage('ERR_NORECORDS_IN_SELECTPICKER'));
            } else {
                p_notification(false, eb.getMessage('ERR_STOCKREPO_SELEONEMORE'));
            }
        }
        else if (reportType == 'globalItemMoving') {
            var fromDate = $('#from_date').val();
            var toDate = $('#to_date').val();
            var selectedProducts = $('#product').val();
            var locIds = $('#location').val();
            if (fromDate == null || fromDate == "") {
                p_notification(false, eb.getMessage('ERR_DAISALE_FROMD'));
                document.getElementById("from_date").focus();
            } else if (toDate == null || toDate == "") {
                p_notification(false, eb.getMessage('ERR_DAISALE_TOD'));
                document.getElementById("to_date").focus();
            } else if (toDate < fromDate) {
                p_notification(false, eb.getMessage('ERR_DAISALE_TODFROMD'));
                document.getElementById("to_date").focus();
            } else if (selectedProducts || isAllProducts == true) {
                var url = BASE_URL + '/api/stock-in-hand-report/generate-global-item-moving-sheet';
                eb.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: {productIds: selectedProducts, fromDate: fromDate, toDate: toDate, isAllProducts: isAllProducts, locationIds: locIds, documentDateFlag : false},
                    success: function(respond) {
                        if (respond.status == true) {
                            p_notification(true, respond.msg);
                        }
                    }
                });
            } else if (selectedProducts && selectedProducts.length == 0) {
                p_notification(false, eb.getMessage('ERR_NORECORDS_IN_SELECTPICKER'));
            } else {
                p_notification(false, eb.getMessage('ERR_STOCKREPO_SELEONEMORE'));
            }
        } else if (reportType == 'globalItemMovingByDocumentDate') {
            var fromDate = $('#from_date').val();
            var toDate = $('#to_date').val();
            var selectedProducts = $('#product').val();
            var locIds = $('#location').val();
            if (fromDate == null || fromDate == "") {
                p_notification(false, eb.getMessage('ERR_DAISALE_FROMD'));
                document.getElementById("from_date").focus();
            } else if (toDate == null || toDate == "") {
                p_notification(false, eb.getMessage('ERR_DAISALE_TOD'));
                document.getElementById("to_date").focus();
            } else if (toDate < fromDate) {
                p_notification(false, eb.getMessage('ERR_DAISALE_TODFROMD'));
                document.getElementById("to_date").focus();
            } else if (selectedProducts || isAllProducts == true) {
                var url = BASE_URL + '/api/stock-in-hand-report/generate-global-item-moving-sheet';
                eb.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: {productIds: selectedProducts, fromDate: fromDate, toDate: toDate, isAllProducts: isAllProducts, locationIds: locIds, documentDateFlag : true},
                    success: function(respond) {
                        if (respond.status == true) {
                            p_notification(true, respond.msg);
                        }
                    }
                });
            } else if (selectedProducts && selectedProducts.length == 0) {
                p_notification(false, eb.getMessage('ERR_NORECORDS_IN_SELECTPICKER'));
            } else {
                p_notification(false, eb.getMessage('ERR_STOCKREPO_SELEONEMORE'));
            }
        } else if (reportType == 'invoicedNonInventoryItem') {
            var fromDate = $('#from_date').val();
            var toDate = $('#to_date').val();
            var selectedProducts = $('#nonInventoryProduct').val();
            if (fromDate == null || fromDate == "") {
                p_notification(false, eb.getMessage('ERR_DAISALE_FROMD'));
                document.getElementById("from_date").focus();
            } else if (toDate == null || toDate == "") {
                p_notification(false, eb.getMessage('ERR_DAISALE_TOD'));
                document.getElementById("to_date").focus();
            } else if (toDate < fromDate) {
                p_notification(false, eb.getMessage('ERR_DAISALE_TODFROMD'));
                document.getElementById("to_date").focus();
            } else if (selectedProducts || isAllNonInventoryProducts == true) {
                var url = BASE_URL + '/api/stock-in-hand-report/generate-non-inventory-item-moving-sheet';
                eb.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: {productIds: selectedProducts, fromDate: fromDate, toDate: toDate, isAllProducts: isAllNonInventoryProducts},
                    success: function(respond) {
                        if (respond.status == true) {
                            window.open(BASE_URL + '/' + respond.data, '_blank');
                        }
                    }
                });
            } else if (selectedProducts && selectedProducts.length == 0) {
                p_notification(false, eb.getMessage('ERR_NORECORDS_IN_SELECTPICKER'));
            } else {
                p_notification(false, eb.getMessage('ERR_STOCKREPO_SELEONEMORE'));
            }
        } else if (reportType == 'globalItemMovingWithValue') {
            var fromDate = $('#from_date').val();
            var toDate = $('#to_date').val();
            var selectedProducts = $('#product').val();
            var locIds = $('#location').val();
            if (fromDate == null || fromDate == "") {
                p_notification(false, eb.getMessage('ERR_DAISALE_FROMD'));
                document.getElementById("from_date").focus();
            } else if (toDate == null || toDate == "") {
                p_notification(false, eb.getMessage('ERR_DAISALE_TOD'));
                document.getElementById("to_date").focus();
            } else if (toDate < fromDate) {
                p_notification(false, eb.getMessage('ERR_DAISALE_TODFROMD'));
                document.getElementById("to_date").focus();
            } else if (locIds == null) {
                p_notification(false, eb.getMessage('ERR_CUSREPO_SELELOCATION'));
                document.getElementById("location").focus();
            } else if (selectedProducts || isAllProducts == true) {
                var url = BASE_URL + '/api/stock-in-hand-report/item-moving-with-value-csv';
                eb.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: {productIds: selectedProducts, isAllProducts: isAllProducts, fromDate: fromDate, toDate: toDate, locationIds: locIds},
                    success: function(respond) {
                        if (respond.status == true) {
                            window.open(BASE_URL + '/' + respond.data, '_blank');
                        }
                    }
                });
            } else if (selectedProducts && selectedProducts.length == 0) {
                p_notification(false, eb.getMessage('ERR_NORECORDS_IN_SELECTPICKER'));
            } else {
                p_notification(false, eb.getMessage('ERR_STOCKREPO_SELEONEMORE'));
            }
        } else if (reportType == 'stockAging') {
            var selectedProducts = $('#product').val();
            if (selectedProducts || isAllProducts == true) {
                var url = BASE_URL + '/api/stock-in-hand-report/stock-aging-csv';
                eb.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: {productIds: selectedProducts, isAllProducts: isAllProducts, categoryIds: categoryIds},
                    success: function(respond) {
                        if (respond.status == true) {
                            window.open(BASE_URL + '/' + respond.data, '_blank');
                        }
                    }
                });
            } else if (selectedProducts && selectedProducts.length == 0) {
                p_notification(false, eb.getMessage('ERR_NORECORDS_IN_SELECTPICKER'));
            } else {
                p_notification(false, eb.getMessage('ERR_STOCKREPO_SELEONEMORE'));
            }
        } else if(reportType == 'globalStockValueWithAvg' || reportType == 'globalStockValueWithGrnPrice') {
            var selectedProducts = $('#product').val();
            var locationIds = $('#location').val();
             var grnCosting = false;
            if (reportType == 'globalStockValueWithGrnPrice') {
                grnCosting = true;
            }
            if (selectedProducts || isAllProducts == true) {
                var url = BASE_URL + '/api/stock-in-hand-report/generate-global-stock-value-sheet';
                eb.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: {productIds: selectedProducts, isAllProducts: isAllProducts, avgCosting : true, locationIds: locationIds, categoryIds: categoryIds, grnCosting: grnCosting},
                    success: function(respond) {
                        if (respond.status == true) {
                            p_notification(true, respond.msg);
                        }
                    }
                });
            } else if (selectedProducts && selectedProducts.length == 0) {
                p_notification(false, eb.getMessage('ERR_NORECORDS_IN_SELECTPICKER'));
            } else {
                p_notification(false, eb.getMessage('ERR_STOCKREPO_SELEONEMORE'));
            }
        } else if (reportType == 'locationWiseDate') {
            var locId = $('#location').val();
            var selectedProducts = $('#product').val();
            var toDate = $('#to_date2').val();
            if (toDate == null || toDate == "") {
                p_notification(false, eb.getMessage('ERR_DAISALE_TOD'));
                document.getElementById("to_date2").focus();
            } else if (selectedProducts || isAllProducts == true) {
                if (isDocumentDateWise == true && !isActDateWise) {
                    var isDocDateWise = true;
                } 
                if (isActDateWise == true && !isDocumentDateWise) {
                    var isDocDateWise = false;
                }
                var url = BASE_URL + '/api/stock-in-hand-report/generate-location-and-date-wise-stock-sheet';
                eb.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: {productIds: selectedProducts, locationIdList: locId, isAllProducts: isAllProducts, toDate: toDate, categoryIds: categoryIds, isDocDateWise: isDocDateWise},
                    success: function(respond) {
                        if (respond.status == true) {
                            window.open(BASE_URL + '/' + respond.data, '_blank');
                        }
                    }
                });
            } else if (selectedProducts && selectedProducts.length == 0) {
                p_notification(false, eb.getMessage('ERR_NORECORDS_IN_SELECTPICKER'));
            } else {
                p_notification(false, eb.getMessage('ERR_STOCKREPO_SELEONEMORE'));
            }
        }
        else if (reportType == 'locationWiseBatchData') 
        {
            var locId = $('#location').val();
            var selectedProducts = $('#product').val();
            if (selectedProducts || isAllProducts == true) {
                var url = BASE_URL + '/api/stock-in-hand-report/generate-location-wise-stock-sheet';
                eb.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: {productIds: selectedProducts, locationIdList: locId, isAllProducts: isAllProducts, batchSerialType: true, categoryIds: categoryIds},
                    success: function(respond) {
                        if (respond.status == true) {
                            window.open(BASE_URL + '/' + respond.data, '_blank');
                        }
                    }
                });
            } else if (selectedProducts && selectedProducts.length == 0) {
                p_notification(false, eb.getMessage('ERR_NORECORDS_IN_SELECTPICKER'));
            } else {
                p_notification(false, eb.getMessage('ERR_STOCKREPO_SELEONEMORE'));
            }
        }
        else if(reportType == 'serialItemMoving'){
            var selectedProducts = $('#product').val();
            var serialIDS = $('#serialName').val();
                                    
            if ((selectedProducts || isAllProducts == true) && serialIDS || isAllSerials == true ) {
                var url = BASE_URL + '/api/stock-in-hand-report/view-serial-movement-details-sheet';
                eb.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: {productIds: selectedProducts, isAllProducts: isAllProducts, serialIds: serialIDS, isAllSerial: isAllSerials},
                    success: function(respond) {
                       if (respond.status == true) {
                            p_notification(true, respond.msg);
                        }
                    }
                });
            } else if (!selectedProducts && isAllProducts == false) {
                p_notification(false, eb.getMessage('ERR_STOCKREPO_SELEONEMORE'));

            } else if (!serialIDS && isAllSerials == false) {
                p_notification(false, eb.getMessage('ERR_NO_SERIAL'));

            } else {
                p_notification(false, eb.getMessage('ERR_NORECORDS_IN_SELECTPICKER'));
            }
        }

         else {
            p_notification(false, eb.getMessage('ERR_SALEREPORT_CORRETYPE'));
        }
    });

    function getSelectedLocNameList() {
        var locNameList = [];
        $("#location option:selected").each(function() {
            var $this = $(this);
            if ($this.length) {
                var selText = $this.text();
                locNameList.push(selText);
            }
        });
        return locNameList;
    }

    function hideStockReportIFrame() {
        $('#stockReport').hide();
        $('#stockReport').contents().find('body').html('');
        $('#stockReport').height(0);
        $('.selectpicker').selectpicker('deselectAll');
    }

    function divLoading() {
        hideStockReportIFrame();
        $('.category-content').hide();
        $('.location-content').hide();
        $('.item-wise-expire-content').hide();
        $('#to_date,#from_date').hide();
        $('.to_date_lbl,.from_date_lbl').addClass('hidden');
    }

    function setValues() {
        productIds = $('#product').val();
        lengthOfAvailableProducts = $("#product").find("option").length;
        categoryIds = $('#category').val();
        lengthOfAvailableCategorys = $("#category").find("option").length;
        locProductIds = $('#locProduct').val();
        lengthOfAvailableLocProducts = $("#locProduct").find("option").length;
        sbProductIds = $('#sbProduct').val();
        lengthOfAvailableSBProducts = $("#sbProduct").find("option").length;

        if (productIds) {
            removeMultiSelect(productIds);
        }
        if (categoryIds) {
            removeMultiSelect(categoryIds);
        }
        if (locProductIds) {
            removeMultiSelect(locProductIds);
        }
        if (sbProductIds) {
            removeMultiSelect(sbProductIds);
        }
    }

    $('.isAllProducts').on('click', function() {
        isAllProducts = $(this).val();
        if (isAllProducts == true) {
            hideStockReportIFrame();
            $('#product').val('').trigger('change');
            $('#product').prop('disabled', true);
            p_notification('info', eb.getMessage('INFO_CUS_REPORT_ALL_CUS_SELECTION'));
        } else {
            hideStockReportIFrame();
            $('#product').prop('disabled', false);
        }
    });

    $('.isAllNonInventoryProducts').on('click', function() {
        isAllNonInventoryProducts = $(this).val();
        if (isAllNonInventoryProducts == true) {
            hideStockReportIFrame();
            $('#nonInventoryProduct').val('').trigger('change');
            $('#nonInventoryProduct').prop('disabled', true);
            p_notification('info', eb.getMessage('INFO_CUS_REPORT_ALL_CUS_SELECTION'));
        } else {
            hideStockReportIFrame();
            $('#nonInventoryProduct').prop('disabled', false);
        }
    });
    $('.isAllSerials').on('click', function() {
        isAllSerials = $(this).val();
        if (isAllSerials == true) {
            $('#serialName').val('').trigger('change');
            $('#serialName').prop('disabled', true);
        } else {
            $('#serialName').prop('disabled', false);
        }
    });

    $('.isDocumentDateWise').on('click', function() {
        isDocumentDateWise = $(this).val();
        isActDateWise = false;
        $("#isActualDateWise").prop('checked', false);;
    });

    $('.isActualDateWise').on('click', function() {
        isActDateWise = $(this).val();
        isDocumentDateWise = false;
        $("#isDocumentDateWise").prop('checked', false);
    });


    $('.isROLWise').on('click', function() {
        isROLWise = $(this).val();
        isMILWise = false;
        $("#isMILWise").prop('checked', false);;
    });

    $('.isMILWise').on('click', function() {
        isMILWise = $(this).val();
        isROLWise = false;
        $("#isROLWise").prop('checked', false);
    });

    $('.isReachWise').on('click', function() {
        isReachWise = $(this).val();
        isAllWise = false;
        $("#isAllWise").prop('checked', false);;
    });

    $('.isAllWise').on('click', function() {
        isAllWise = $(this).val();
        isReachWise = false;
        $("#isReachWise").prop('checked', false);
    });
});


