var dimensionTypeID = null;
var dimensionValue = null;
$(document).ready(function() {
    initialSetUp();
    var reportType = 'incomeExpenseReport';
    $('#budgetReport').hide();
    $('#incomeExpense').click(function(e) {
        reportType = 'incomeExpenseReport';
    });

    var accountIDs;
    loadDropDownFromDatabase('/accounts-api/search-active-accounts-for-dropdown', "", "", '#accountID');
    $('#accountID').on('change', function() {
        accountIDs = $(this).val();

    });

    var fiscalPeriodID;
    $('#fiscalPeriods').on('change',function(){
        fiscalPeriodID = $(this).val();
    });

    var accountType;
    $('#acFilter').on('change',function(){
        accountType = $(this).val();
    });

    var checkin = $('#trStartDate').datepicker({}).on('changeDate', function(ev) {
        if (ev.date.valueOf() < checkout.date.valueOf()) {
            var newDate = new Date(ev.date)
            newDate.setDate(newDate.getDate());
            checkout.setValue(newDate);
        }
        checkin.hide();
        $('#trEndDate')[0].focus();
    }).data('datepicker');
    var checkout = $('#trEndDate').datepicker({
        onRender: function(date) {
            return date.valueOf() < checkin.date.valueOf() ? 'disabled' : '';
        }
    }).on('changeDate', function(ev) {
        checkout.hide();
    }).data('datepicker');

     var checkout2 = $('#trEndDate').datepicker({

    }).on('changeDate', function(ev) {
        checkout2.hide();
    }).data('datepicker');


    $('#viewReport').click(function(e) {


        if(reportType == 'incomeExpenseReport'){
            var fromDate = $('#trStartDate').val();
            var toDate = $('#trEndDate').val();

            var summaryData = false;
            if($('#summaryRow').prop('checked') == true){
                summaryData = true;
            } 

            if(incomeExpenseValidation()){
                eb.ajax({
                    type: 'POST',
                    url: BASE_URL + '/income-expense-report-api/incomeExpenseReportView',
                    data: {
                        fromDate: fromDate,
                        toDate: toDate,
                        dimensionType : dimensionTypeID,
                        dimensionValues : dimensionValue,
                        isSummery : summaryData
                    },
                    success: function(respond) {
                        if(respond.status){
                            setContentToReport('incomeExpenseReport', respond);
                        }else{
                            p_notification(respond.status, respond.msg);
                        }
                    }
                });
            }
        } 
    });

    $('#generatePdf').click(function(e) {
        if(reportType == 'incomeExpenseReport'){
            var fromDate = $('#trStartDate').val();
            var toDate = $('#trEndDate').val();

            var summaryData = false;
            if($('#summaryRow').prop('checked') == true){
                summaryData = true;
            } 

            if(incomeExpenseValidation()){
                var params = {
                    fromDate: fromDate,
                    toDate: toDate,
                    dimensionType : dimensionTypeID,
                    dimensionValues : dimensionValue,
                    isSummery : summaryData
                }
          var url = BASE_URL + '/income-expense-report-api/income-expense-report-pdf';
            eb.ajax({
                type: 'POST',
                url: url,
                dataType: 'json',
                data: params,
                success: function(respond) {
                    if (respond.status) {
                        window.open(BASE_URL + '/' + respond.data, '_blank');
                    } 
                }
            });
            }
        } 
    });

    $('#csvReport').click(function(e){
        if(reportType == 'incomeExpenseReport'){
            var fromDate = $('#trStartDate').val();
            var toDate = $('#trEndDate').val();
            // var dimensionValue = $('#dimensionVal').val();

            var summaryData = false;
            if($('#summaryRow').prop('checked') == true){
                summaryData = true;
            } 
            if(incomeExpenseValidation()){
                var params = {
                    fromDate: fromDate,
                    toDate: toDate,
                    dimensionType : dimensionTypeID,
                    dimensionValues : dimensionValue,
                    isSummery : summaryData
                }
                var url = BASE_URL + '/income-expense-report-api/incomeExpenseReportSheet';
                eb.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: params,
                    success: function(respond) {
                        if (respond.status) {
                            window.open(BASE_URL + '/' + respond.data, '_blank');
                        }
                    }
                });
            }
        } 
    });

    $('.dimenisonchange').on('change', function(e) {
        dimensionTypeID = this.value;
        getDimensionValueListByDimensionType(dimensionTypeID, function() {
        });
    });

    function initialSetUp () {
        $('.summary-view-option').removeClass('hidden');
    }

    function getDimensionValueListByDimensionType(dimensionTypeID, callback) {
        switch (dimensionTypeID) {
            case "job":
                $('.dimensionVal').addClass('hidden');
                $('.dimensionProjectNo').addClass('hidden');
                $('.dimensionJobNo').removeClass('hidden');
                break;
            case "project":
                $('.dimensionVal').addClass('hidden');
                $('.dimensionProjectNo').removeClass('hidden');
                $('.dimensionJobNo').addClass('hidden');
                break;
           default:
                $('.dimensionVal').removeClass('hidden');
                $('.dimensionProjectNo').addClass('hidden');
                $('.dimensionJobNo').addClass('hidden');
                $.ajax({
                    type: 'POST',
                    url: BASE_URL + '/dimension-api/getRelatedDimensionValues',
                    data: {dimensionId: dimensionTypeID},
                    success: function(respond) {
                        //empty options
                        $('#dimensionVal').empty();
                        // $('#dimensionVal').append($("<option>", {value: '', html: 'Select dimension value'}));
                        console.log(respond.data);
                        $(respond.data).each(function(index, dimValue) {
                            $.each(dimValue, function(ind, val) {
                                $('#dimensionVal').append($("<option>", {value: ind, html: val.value}));
                            });
                        });
                        $('#dimensionVal').selectpicker('refresh');
                        callback();
                    }
                });
                break;
        }
    }

    loadDropDownFromDatabase('/job-api/searchJobsForDropdown', "", 0, '.dimensionJobNo');
    $('.dimensionJobNo').selectpicker('refresh');
    $('.dimensionJobNo').on('change', function() {

        if ($(this).val() != 0) {
            dimensionValue = $(this).val();
        }
    });

    loadDropDownFromDatabase('/api/project/searchProjectsForDropdown', "", 0, '.dimensionProjectNo');
    $('.dimensionProjectNo').selectpicker('refresh');
    $('.dimensionProjectNo').on('change', function() {
        if ($(this).val() != 0) {
            dimensionValue = $(this).val();
        }
    });

    $('.dimensionVal').on('change', function() {
        if ($(this).val() != 0) {
            dimensionValue = $(this).val();
        }
    });

});

function incomeExpenseValidation()
{
    var fromDate = $('#trStartDate').val();
    var toDate = $('#trEndDate').val();

    if (fromDate == '' || fromDate == null) {
        p_notification(false, eb.getMessage('ERR_DAISALE_FROMD'));
        document.getElementById("trStartDate").focus();
        return false;
    } else if (toDate == '' || toDate == null) {
        p_notification(false, eb.getMessage('ERR_DAISALE_TOD'));
        document.getElementById("trEndDate").focus();
        return false;
    } else if (toDate < fromDate) {
        p_notification(false, eb.getMessage('ERR_SALEREPORT_DEFF'));
        return false;
    } else if (dimensionTypeID == '' || dimensionTypeID == null) {
        p_notification(false, eb.getMessage('ERR_DIM_TYPE_EMPTY'));
        document.getElementById("dimensionTypeId").focus();
        return false;
    } else if (dimensionValue == null) {
        p_notification(false, eb.getMessage('ERR_DIM_VAL_EMPTY'));
        document.getElementById("dimensionVal").focus();
        return false;
    }

    return true;
}
