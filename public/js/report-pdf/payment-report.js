
$(document).ready(function () {
    ///////DatePicker for two text Filed\\\\\\\
    var fm, tm;
    var reportType = 'Monthly';
    var checkin = $('#fromDate').datepicker({
    }).on('changeDate', function (ev) {
        if (ev.date.valueOf() < checkout.date.valueOf()) {
            var newDate = new Date(ev.date)
            newDate.setDate(newDate.getDate());
            checkout.setValue(newDate);
        }
        checkin.hide();
        $('#toDate')[0].focus();
    }).data('datepicker');
    var checkout = $('#toDate').datepicker({
        onRender: function (date) {
            return date.valueOf() <= checkin.date.valueOf();
        }
    }).on('changeDate', function (ev) {
        checkout.hide();
    }).data('datepicker');

    $("#fromMonth").datepicker({
        format: "yyyy-mm",
        viewMode: "months",
        minViewMode: 'months'
    });
    $("#toMonth").datepicker({
        format: "yyyy-mm",
        viewMode: "months",
        minViewMode: 'months'
    });
    ///////Month Pick for two text Filed\\\\\\\
    var checkinMonth = $('#fromMonth').datepicker({
    }).on('changeDate', function (ev) {
        var newD = new Date(ev.date.getFullYear(), ev.date.getMonth() + 1);
        checkoutMonth.setValue(newD);
        checkinMonth.hide();
        $('#toMonth')[0].focus();

    }).data('datepicker');

    var checkoutMonth = $('#toMonth').datepicker({
    }).on('changeDate', function (ev) {
        checkoutMonth.hide();
    }).data('datepicker');

    /////Pick date/////////////
    $('#date').datepicker();
    //////Pick year////////////
    $("#year").datepicker({
        format: " yyyy",
        viewMode: "years",
        minViewMode: 'years'
    });

    var checkinYear = $('#year').datepicker({
    }).on('changeDate', function (ev) {
        var newD = new Date(ev.date.getYear());
        checkinYear.hide();

    }).data('datepicker');

    initialCondition();


    $('#Monthly').click(function (e) {
        reportType = 'Monthly';

        $('.selectpicker').selectpicker('deselectAll');
        $('#tax').attr('checked', false);
        $('#toMonth,#fromMonth').val('');
        //hide
        $('#year,#fromDate,#toDate,#date').hide();
        $('#salesReport').hide();
        $('#salesGraphReport').hide();
        $('.from_year_lbl').addClass('hidden');
        $('.to_date_lbl').addClass('hidden');
        $('.from_date_lbl').addClass('hidden');

        //show
        $('#viewGraph,#tax,#lblTax').show();
        $('#fromMonth,#toMonth').show();
        $('.to').show();
        $('.to_month_lbl').show();
        $('.from_month_lbl').show();
        $('.tax_chk_lbl').show();
    });

    $('#Daily').click(function (e) {
        reportType = 'Daily';
        $('.selectpicker').selectpicker('deselectAll');
        $('#tax').attr('checked', false);
        $('#fromDate,#toDate').val('');

        //hidden
        $('#fromMonth,#toMonth,#year').hide();
        $('#salesReport').hide();
        $('#salesGraphReport').hide();
        $('#salessummery_report_view').hide();
        $('#paymentsummery_report_view').hide();
        $('.from_month_lbl').hide();
        $('.to_month_lbl').hide();
        $('.from_year_lbl').addClass('hidden');

        //show
        $('#fromDate,#toDate,#date').show();
        $('#viewGraph,#tax,#lblTax').show();
        $('.to').show();
        $('.from_date_lbl').removeClass('hidden');
        $('.to_date_lbl').removeClass('hidden');
        $('.tax_chk_lbl').show();
    });

    $('#Annual').click(function (e) {
        reportType = 'Annual';
        $('#tax').attr('checked', false);
        $('#year').val('');

        //hide
        $('#fromDate,#toDate,#fromMonth,#toMonth,#date').hide();
        $('#salesReport').hide();
        $('#salesGraphReport').hide();
        $('.to_date_lbl').addClass('hidden');
        $('.from_date_lbl').addClass('hidden');
        $('.from_month_lbl').hide();
        $('.to_month_lbl').hide();
        $('.to').hide();

        //show
        $('#viewGraph,#tax,#lblTax,#year').show();
        $('.from_year_lbl').removeClass('hidden');
        $('.tax_chk_lbl').show();
    });

    $('#Summery').click(function (e) {
        reportType = 'Summery';
        //hide
        $('#tax,#lblTax').hide();
        $('#viewGraph').hide();
        $('#fromMonth,#toMonth,#date,#year').hide();
        $('#salesReport').hide();
        $('#salesGraphReport').hide();
        $('.from_month_lbl').hide();
        $('.to_month_lbl').hide();
        $('.from_year_lbl').addClass('hidden');
//        $('.tax_chk_lbl').hide();

        //show
        $('.to').show();
        $('.to_date_lbl').removeClass('hidden');
        $('.from_date_lbl').removeClass('hidden');
        $('#fromDate,#toDate').show();
        $('#fromDate,#toDate').val('');
    });

    $('#Payments').click(function (e) {
        reportType = 'Payments';
        $('#fromDate,#toDate').val('');

        //hide
        $('#fromMonth,#toMonth,#date,#year').hide();
        $('#tax,#lblTax').hide();
        $('#salesReport').hide();
        $('#viewGraph').hide();
        $('.from_month_lbl').hide();
        $('.to_month_lbl').hide();
        $('.from_year_lbl').addClass('hidden');
        $('.tax_chk_lbl').hide();

        //show
        $('#fromDate,#toDate').show();
        $('.from_date_lbl').removeClass('hidden');
        $('.to_date_lbl').removeClass('hidden');
        $('.to').show();

        //active_link_appearnce
        $(this).addClass('active_payment');
    });

    function refresh() {
        $('.commanClass').each(function () {
            $('#locationList').val('');
            $('#paymentList').val('');


        });
    }


//    $('#locationradio').on('click', function() {
//        $('#paymentList').val(null);
//        $('.paymentpp').hide();
//        $('.locationpp').show();
//
//    });
//
//    $('#paymentradio').on('click', function() {
//        $('#locationList').val(null);
//        $('.paymentpp').show();
//        $('.locationpp').hide();
//
//
//    });





    function hideGraphDiv() {
        $('#salesGraphReport').hide();
        $('#salesGraphReport').html('');
    }

    $('#viewReport').click(function (e) {


        if ($('#paymentList').val() != null && $('#locationList').val() != null) {
            var paymentNumber = $('#paymentList').val();
            var locationNumber = $('#locationList').val();
        }
        else if ($('#locationList').val() == null) {
            p_notification(false, "Please select Location(s).")
            $('#locationList').focus();
            return false;
        }

        else if ($('#paymentList').val() == null) {
            p_notification(false, "Please select Payment Method(s).");
            $('#paymentList').focus();
            return false;
        }



        hideGraphDiv();
        if (reportType == 'Monthly') {
//            var checkTaxStatus = getCheckedStatus();



            var getpdfdataurl = BASE_URL + '/report/get/payment/monthly-payment-view';
            var sort;
            var number;
            var fromMonth = $('#fromMonth').val();
            var fromMonthSplit = fromMonth.split("-");
            var toMonth = $('#toMonth').val();
            var toMonthSplit = toMonth.split("-");
            var sort = $("input[type=radio][name=sort]:checked").val();




            fromday(fromMonthSplit[1]);
            today(toMonthSplit[1], toMonthSplit[0]);

            if ($('#fromMonth').val() == null || $('#fromMonth').val() == "") {
                p_notification(false, "Please select From Month.");
                $('#fromMonth').focus();
                return false;
            }
            else if ($('#toMonth').val() == null || $('#toMonth').val() == "") {
                p_notification(false, "Please select To Month.");
                $('#toMonth').focus();
                return false;

            }
            else if ($('#toMonth').val() < $('#fromMonth').val()) {
                p_notification(false, "Please enter valid range.");

                return false;

            }
            else {
                var getpdfdatarequest = eb.post(getpdfdataurl, {
                    fromMonth: fm,
                    toMonth: tm,
                    sort: sort,
                    paymentNumber: paymentNumber,
                    locationNumber: locationNumber

                });

                getpdfdatarequest.done(function (respond) {
                    if (respond.status == true) {
                        setContentToReport('salesReport', respond);
                    }
                });

            }

        }
        else if (reportType == 'Daily') {

            $('#btnDaily').hide();
//            var checkTaxStatus = getCheckedStatus();
            var sort = $("input[type=radio][name=sort]:checked").val();
//            if ($('#paymentList').val() != null && $('#locationList').val() != null) {
//                var paymentNumber = $('#paymentList').val();
//                var locationNumber = $('#locationList').val();
//            }
//
//            else {
//                p_notification(false, "Please fill sorting Data");
//
//                return false;
//            }



            var getpdfdataurl = BASE_URL + '/report/get/payment/daily-payment-view';


            inputs = new Array(
                    $('#fromDate').val(),
                    $('#toDate').val());

            if (validteinput(inputs)) {
                var getpdfdatarequest = eb.post(getpdfdataurl, {
                    fromDate: $('#fromDate').val(),
                    toDate: $('#toDate').val(),
                    sort: sort,
                    paymentNumber: paymentNumber,
                    locationNumber: locationNumber
                });
                getpdfdatarequest.done(function (respond) {
                    if (respond.status == true) {
                        setContentToReport('salesReport', respond);
                    }
                });
            }

        }
        else if (reportType == 'Annual') {

            $('#btnAnnual').hide();
//            var checkTaxStatus = getCheckedStatus();

            var year = $('#year').val();
//            if ($('#paymentList').val() !== null && $('#locationList').val() !== null) {
//                var paymentNumber = $('#paymentList').val();
//                var locationNumber = $('#locationList').val();
//            }
//
//            else {
//                p_notification(false, "Please fill sorting Data");
//
//                return false;
//            }
            year = year.replace(/\s+/g, "");
            $.trim(year);




            var getannualdataurl = BASE_URL + '/report/get/payment/annual-payment-view';

            inputs = new Array(
                    $('#year').val()
                    );
            if ($('#year').val() == null || $('#year').val() == "") {
                p_notification(false, "Please select Year.");
                $('#year').focus();
                return false;
            } else {
                var getannualdatarequest = eb.post(getannualdataurl, {
                    year: year,
                    paymentNumber: paymentNumber,
                    locationNumber: locationNumber
                });
                getannualdatarequest.done(function (respond) {
                    if (respond.status == true) {
                        setContentToReport('salesReport', respond);
                    }
                });
            }

        }
        else if (reportType == 'Summery') {



            var getdataurl = BASE_URL + '/report/get/payment/summery-payment-view';
            var sort = $("input[type=radio][name=sort]:checked").val();
            if ($('#paymentList').val() != null && $('#locationList').val() != null) {
                var paymentNumber = $('#paymentList').val();
                var locationNumber = $('#locationList').val();
            }

            else {
                p_notification(false, "Please fill sorting Data");

                return false;
            }
            inputs = new Array(
                    $('#fromDate').val(),
                    $('#toDate').val()
                    );
            if (validteinput(inputs)) {
                var getdatarequest = eb.post(getdataurl, {
                    fromDate: $('#fromDate').val(),
                    toDate: $('#toDate').val(),
                    sort: sort,
                    paymentNumber: paymentNumber,
                    locationNumber: locationNumber
                });
                getdatarequest.done(function (respond) {
                    if (respond.status == true) {
                        setContentToReport('salesReport', respond);
                    }
                });
            }
        }
        else if (reportType == 'Payments') {



            var getpdfdataurl = BASE_URL + '/report/get/invoice/view-payments-summery';

            inputs = new Array(
                    $('#fromDate').val(),
                    $('#toDate').val()
                    );
            if (validteinput(inputs)) {
                var getpdfdatarequest = eb.post(getpdfdataurl, {
                    fromDate: $('#fromDate').val(),
                    toDate: $('#toDate').val()
                });
                getpdfdatarequest.done(function (respond) {
                    if (respond.status == true) {
                        setContentToReport('salesReport', respond);
                    }
                });
            }

        }
        else {
            p_notification(false, "Please select a correct report type first");
        }
    });

    /* function for Pdf generate Reporting
     * @author SANDUN <sandun@thinkcube.com>
     * */
    $('#generatePdf').click(function (e) {

        if ($('#paymentList').val() != null && $('#locationList').val() != null) {
            var paymentNumber = $('#paymentList').val();
            var locationNumber = $('#locationList').val();
        }
        else if ($('#locationList').val() == null) {
            p_notification(false, "Please select Location(s).")
            $('#locationList').focus();
            return false;
        }

        else if ($('#paymentList').val() == null) {
            p_notification(false, "Please select Payment Method(s).");
            $('#paymentList').focus();
            return false;
        }




        if (reportType == 'Monthly') {
//            var checkTaxStatus = getCheckedStatus();
            var fromMonth = $('#fromMonth').val();
            var fromMonthSplit = fromMonth.split("-");
            var toMonth = $('#toMonth').val();
            var toMonthSplit = toMonth.split("-");
            fromday(fromMonthSplit[1]);
            today(toMonthSplit[1], toMonthSplit[0]);
            var sort = $("input[type=radio][name=sort]:checked").val();
//            if ($('#paymentList').val() != null && $('#locationList').val() != null) {
//                var paymentNumber = $('#paymentList').val();
//                var locationNumber = $('#locationList').val();
//            }
//
//            else {
//                p_notification(false, "Please fill sorting Data");
//
//                return false;
//            }

            if ($('#fromMonth').val() == null || $('#fromMonth').val() == "") {
                p_notification(false, "Please select From Month.");
                $('#fromMonth').focus();
                return false;
            }
            else if ($('#toMonth').val() == null || $('#toMonth').val() == "") {
                p_notification(false, "Please select To Month.");
                $('#toMonth').focus();
                return false;

            }
            else if ($('#toMonth').val() < $('#fromMonth').val()) {
                p_notification(false, "Please enter valid range.");

                return false;

            }
            else {



                var url = BASE_URL + "/report/get/payment/monthly-payment-pdf";
                eb.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: {fromMonth: fm, toMonth: tm, sort: sort, paymentNumber: paymentNumber, locationNumber: locationNumber},
                    success: function (respond) {
                        if (respond.status == true) {
                            window.open(BASE_URL + '/' + respond.data, '_blank');
                        }
                    }
                });




            }
        }
        else if (reportType == 'Daily') {

            var fromDate = $('#fromDate').val();
            var toDate = $('#toDate').val();
            var sort = $("input[type=radio][name=sort]:checked").val();
            if ($('#paymentList').val() != null && $('#locationList').val() != null) {
                var paymentNumber = $('#paymentList').val();
                var locationNumber = $('#locationList').val();
            }

            else {
                p_notification(false, "Please fill sorting Data");

                return false;
            }
            inputs = new Array(fromDate, toDate);

            if (validteinput(inputs)) {
                var url = BASE_URL + "/report/get/payment/daily-payment-pdf";
                eb.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: {fromDate: fromDate, toDate: toDate, sort: sort, paymentNumber: paymentNumber, locationNumber: locationNumber},
                    success: function (respond) {
                        if (respond.status == true) {
                            window.open(BASE_URL + '/' + respond.data, '_blank');
                        }
                    }
                });
            }
        }
        else if (reportType == 'Annual') {
            if ($('#year').val() == null || $('#year').val() == "") {
                p_notification(false, "Please select Year.");
                $('#year').focus();
                return false;
            } else {
                var year = $("#year").val();
                year = year.replace(/\s+/g, "");
                $.trim(year);
                var sort = $("input[type=radio][name=sort]:checked").val();
                var url = BASE_URL + "/report/get/payment/annual-payment-pdf";
                eb.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: {year: year, sort: sort, paymentNumber: paymentNumber, locationNumber: locationNumber},
                    success: function (respond) {
                        if (respond.status == true) {
                            window.open(BASE_URL + '/' + respond.data, '_blank');
                        }
                    }
                });
            }
        }
        else if (reportType == 'Summery') {

            inputs = new Array(
                    $('#fromDate').val(),
                    $('#toDate').val()

                    );
            if (validteinput(inputs)) {
                var fromDate = $("#fromDate").val();
                var toDate = $("#toDate").val();
                var sort = $("input[type=radio][name=sort]:checked").val();
                if ($('#paymentList').val() != null && $('#locationList').val() != null) {
                    var paymentNumber = $('#paymentList').val();
                    var locationNumber = $('#locationList').val();
                }

                else {
                    p_notification(false, "Please fill sorting Data");

                    return false;
                }

                var url = BASE_URL + "/report/get/payment/summery-payment-pdf";
                eb.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: {fromDate: fromDate, toDate: toDate, sort: sort, paymentNumber: paymentNumber, locationNumber: locationNumber},
                    success: function (respond) {
                        if (respond.status == true) {
                            window.open(BASE_URL + '/' + respond.data, '_blank');
                        }
                    }
                });
            }
        }
        else if (reportType == 'Payments') {

            var fromDate = $('#fromDate').val();
            var toDate = $('#toDate').val();
            inputs = new Array(fromDate, toDate);

            if (validteinput(inputs)) {
                window.open(BASE_URL + "/report/get/invoice/generate-payments-summery-pdf?fromDate=" + fromDate + "&toDate=" + toDate, '_blank');

            }
        }
        else {
            p_notification(false, "Please select a report type first");
        }

    });

    /* function for Excel Reporting
     * @author SANDUN <sandun@thinkcube.com>
     * */
    $('#csvReport').on('click', function (e) {

        if ($('#paymentList').val() != null && $('#locationList').val() != null) {
            var paymentNumber = $('#paymentList').val();
            var locationNumber = $('#locationList').val();
        }
        else if ($('#locationList').val() == null) {
            p_notification(false, "Please select Location(s).")
            $('#locationList').focus();
            return false;
        }

        else if ($('#paymentList').val() == null) {
            p_notification(false, "Please select Payment Method(s).");
            $('#paymentList').focus();
            return false;
        }
        if (reportType == 'Monthly') {
            var fromMonth = $('#fromMonth').val();
            var fromMonthSplit = fromMonth.split("-");
            var toMonth = $('#toMonth').val();
            var toMonthSplit = toMonth.split("-");
            fromday(fromMonthSplit[1]);
            today(toMonthSplit[1], toMonthSplit[0]);
            var sort = $("input[type=radio][name=sort]:checked").val();

            if ($('#fromMonth').val() == null || $('#fromMonth').val() == "") {
                p_notification(false, "Please select From Month.");
                $('#fromMonth').focus();
                return false;
            }
            else if ($('#toMonth').val() == null || $('#toMonth').val() == "") {
                p_notification(false, "Please select To Month.");
                $('#toMonth').focus();
                return false;

            }
            else if ($('#toMonth').val() < $('#fromMonth').val()) {
                p_notification(false, "Please enter valid range.");

                return false;

            }
            else {

                var url = BASE_URL + "/report/get/payment/monthly-payment-csv";
                var abc = fm;
                var acb = tm;
                eb.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: {fromMonth: abc, toMonth: acb, sort: sort, paymentNumber: paymentNumber, locationNumber: locationNumber},
                    success: function (respond) {
                        if (respond.status == true) {
                            window.open(BASE_URL + '/' + respond.data, '_blank');
                        }
                    }
                });

            }
        }
        else if (reportType == 'Daily') {

            var fromDate = $('#fromDate').val();
            var toDate = $('#toDate').val();
            var sort = $("input[type=radio][name=sort]:checked").val();
            if ($('#paymentList').val() != null && $('#locationList').val() != null) {
                var paymentNumber = $('#paymentList').val();
                var locationNumber = $('#locationList').val();
            }

            else {
                p_notification(false, "Please fill sorting Data");

                return false;
            }

            inputs = new Array(fromDate, toDate);

            if (validteinput(inputs)) {
                var url = BASE_URL + "/report/get/payment/daily-payment-csv";
                eb.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: {fromDate: fromDate, toDate: toDate, sort: sort, paymentNumber: paymentNumber, locationNumber: locationNumber},
                    success: function (respond) {
                        if (respond.status == true) {
                            window.open(BASE_URL + '/' + respond.data, '_blank');
                        }
                    }
                });

            }

        }
        else if (reportType == 'Annual') {

            if ($('#year').val() == null || $('#year').val() == "") {
                p_notification(false, "Please select Year.");
                $('#year').focus();
                return false;
            } else {
                var year = $("#year").val();
                year = year.replace(/\s+/g, "");
                $.trim(year);
                var sort = $("input[type=radio][name=sort]:checked").val();
                var url = BASE_URL + "/report/get/payment/annual-payment-csv";
                eb.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: {year: year, sort: sort, paymentNumber: paymentNumber, locationNumber: locationNumber},
                    success: function (respond) {
                        if (respond.status == true) {
                            window.open(BASE_URL + '/' + respond.data, '_blank');
                        }
                    }
                });

            }

        } else if (reportType == 'Summery') {

            inputs = new Array(
                    $('#fromDate').val(),
                    $('#toDate').val(),
                    $("input[type=radio][name=sort]:checked").val()
                    );
            if (validteinput(inputs)) {
                var fromDate = $("#fromDate").val();
                var toDate = $("#toDate").val();
                var sort = $("input[type=radio][name=sort]:checked").val();
                if ($('#paymentList').val() != null && $('#locationList').val() != null) {
                    var paymentNumber = $('#paymentList').val();
                    var locationNumber = $('#locationList').val();
                }

                else {
                    p_notification(false, "Please fill sorting Data");

                    return false;
                }
                var url = BASE_URL + "/report/get/payment/summery-payment-csv";
                eb.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: {fromDate: fromDate, toDate: toDate, sort: sort, paymentNumber: paymentNumber, locationNumber: locationNumber},
                    success: function (respond) {
                        if (respond.status == true) {
                            window.open(BASE_URL + '/' + respond.data, '_blank');
                        }
                    }
                });

            }
        }
        else if (reportType == 'Payments') {
            var fromDate = $('#fromDate').val();
            var toDate = $('#toDate').val();
            inputs = new Array(fromDate, toDate);

            if (validteinput(inputs)) {

                window.open(BASE_URL + "/report/get/invoice/generate-payments-summery-sheet?fromDate=" + fromDate + "&toDate=" + toDate, '_blank');

            }
        }
        else {
            p_notification(false, "Please select a correct report type first");
        }

    });

    $('#viewGraph').on('click', function (e) {
        $('#salesReport').hide();
        $('#salesGraphReport').show();
        $('#salesGraphReport').html('');
        if (reportType == 'Monthly') {
            $('#btnMonthly').show();
            var checkTaxStatus = getCheckedStatus();
            var getpdfdataurl = BASE_URL + '/api/sales-report/get-monthly-sales-graph-data';
            var fromMonth = $('#fromMonth').val();
            var fromMonthSplit = fromMonth.split("-");
            var toMonth = $('#toMonth').val();
            var toMonthSplit = toMonth.split("-");
            fromday(fromMonthSplit[1]);
            today(toMonthSplit[1], toMonthSplit[0]);
            if ($('#fromMonth').val() == null || $('#fromMonth').val() == "") {
                p_notification(false, "From Month must be filled out");
                $('#fromMonth').focus();
                return false;
            }
            else if ($('#toMonth').val() == null || $('#toMonth').val() == "") {
                p_notification(false, "To Month must be filled out");
                $('#toMonth').focus();
                return false;
            } else {

                var getpdfdatarequest = eb.post(getpdfdataurl, {
                    fromMonth: fm,
                    toMonth: tm
                });
                getpdfdatarequest.done(function (respond) {
                    var cSymbol = respond.data['cSymbol'];

                    var monthlySalesData = Array();
                    $.each(respond.data['mothlySalesDetails'], function (index, value) {
                        $.each(value, function (subindex, subvalue) {
                            var month = subvalue.Month;
                            var totalAmount = parseFloat(Math.round((subvalue.TotalAmount) * 100) / 100).toFixed(2);
                            totalAmount = totalAmount ? totalAmount : 0.00;
                            var totalTax = parseFloat(Math.round((subvalue.TotalTax) * 100) / 100).toFixed(2);
                            totalTax = totalTax ? totalTax : 0.00;
                            var totalSales = totalAmount - totalTax;

                            monthlySalesData.push({x: month, y: totalSales, z: totalTax});
                        });
                    });

                    if (checkTaxStatus == true) {
                        Morris.Bar({
                            element: 'salesGraphReport',
                            data: monthlySalesData,
                            xkey: 'x',
                            ykeys: ['y', 'z'],
                            labels: ['Sales Excluding Tax', 'Total Tax'],
                            parseTime: false,
                            yLabelFormat: function (y) {
                                return cSymbol + '. ' + accounting.formatMoney(y);
                            },
                            barColors: ['#FF8000', '#663300'],
                            hoverCallback: function (index, options, content) {
                                return(content);
                            },
                            stacked: true,
                            xLabelAngle: 60,
                            hideHover: 'auto'
                        });
                    } else {
                        Morris.Bar({
                            element: 'salesGraphReport',
                            data: monthlySalesData,
                            xkey: 'x',
                            ykeys: ['y'],
                            labels: ['Sales Excluding Tax'],
                            parseTime: false,
                            yLabelFormat: function (y) {
                                return cSymbol + '. ' + accounting.formatMoney(y);
                            },
                            barColors: ['#FF8000'],
                            xLabelAngle: 60,
                            hideHover: 'auto'
                        });
                    }
                });
            }

        }

        else if (reportType == 'Daily') {
            $('#btnDaily').show();
            var checkTaxStatus = getCheckedStatus();
            var getdataurl = BASE_URL + '/api/sales-report/get-daily-sales-graph-data';
            inputs = new Array(
                    $('#fromDate').val(),
                    $('#toDate').val());
            if (validteinput(inputs)) {
                var getdatarequest = eb.post(getdataurl, {
                    fromDate: $('#fromDate').val(),
                    toDate: $('#toDate').val()
                });
                getdatarequest.done(function (respond) {
                    if (respond.status == true) {

                        var dailySalesRespond = respond.data['dailySalesData'];
                        var cSymbol = respond.data['cSymbol'];
                        var start = new Date($('#fromDate').val());
                        var end = new Date($('#toDate').val());

                        var dayArray = Array();
                        while (start <= end) {
                            var date = start.toString();
                            var dateSplit = date.split(" ");

                            monthToNumber(dateSplit[1]);
                            var new_date = (dateSplit[3] + '-' + (beforemonth) + '-' + dateSplit[2]);
                            dayArray.push(new_date);

                            var newDate = start.setDate(start.getDate() + 1);
                            start = new Date(newDate);
                        }

                        var dailySalesData = Array();
                        for (var i = 0; i < dayArray.length; i++) {
                            var amount;
                            var tax;
                            if (dailySalesRespond[dayArray[i]].length == 0) {
                                amount = 0;
                                tax = 0;
                            } else {
                                amount = parseFloat(Math.round((dailySalesRespond[dayArray[i]].salesAmount) * 100) / 100).toFixed(2);
                                amount = amount ? amount : 0.00;
                                tax = parseFloat(Math.round((dailySalesRespond[dayArray[i]].taxAmount) * 100) / 100).toFixed(2);
                                tax = tax ? tax : 0.00;
                            }
                            var totalSales = amount - tax;
                            dailySalesData.push({x: dayArray[i], y: totalSales, z: tax});

                        }

                        if (checkTaxStatus == true) {
                            Morris.Bar({
                                element: 'salesGraphReport',
                                data: dailySalesData,
                                hoverCallback: function (index, options, content) {
                                    return(content);
                                },
                                xkey: 'x',
                                ykeys: ['y', 'z'],
                                stacked: true,
                                labels: ['Sales Excluding Tax', 'Tax'],
                                barColors: ['#FF8000', '#663300'],
                                yLabelFormat: function (y) {
                                    return cSymbol + '. ' + accounting.formatMoney(y);
                                },
                                parseTime: false,
                                hideHover: 'auto',
                                xLabelAngle: 60
                            });
                        } else {
                            Morris.Bar({
                                element: 'salesGraphReport',
                                data: dailySalesData,
                                xkey: 'x',
                                ykeys: ['y'],
                                labels: ['Sales Excluding Tax'],
                                barColors: ['#FF8000'],
                                yLabelFormat: function (y) {
                                    return cSymbol + '. ' + accounting.formatMoney(y);
                                },
                                parseTime: false,
                                hideHover: 'auto',
                                xLabelAngle: 60
                            });


                        }
                    }
                });
            }

        }

        else if (reportType == 'Annual') {

            var checkTaxStatus = getCheckedStatus();
            var year = $('#year').val();
            year = year.replace(/\s+/g, "");
            $.trim(year);
            var getdataurl = BASE_URL + '/api/sales-report/get-annuval-sales-graph-data';
            var inputs = new Array($('#year').val());
            if ($('#year').val() == null || $('#year').val() == "") {
                p_notification(false, "Year must be filled out");
                $('#year').focus();
                return false;
            } else {
                $('#graph_div').show();
                var getdatarequest = eb.post(getdataurl, {
                    year: year
                });
                getdatarequest.done(function (respond) {
                    if (respond.status == true) {
                        var cSymbol = respond.data['cSymbol'];
                        var annualSalesData = respond.data['annualSalesData'];
                        var qa1 = parseFloat(Math.round((annualSalesData[0].Q1Amount) * 100) / 100).toFixed(2);
                        qa1 = qa1 ? qa1 : 0.00;
                        var qa2 = parseFloat(Math.round((annualSalesData[0].Q2Amount) * 100) / 100).toFixed(2);
                        qa2 = qa2 ? qa2 : 0.00;
                        var qa3 = parseFloat(Math.round((annualSalesData[0].Q3Amount) * 100) / 100).toFixed(2);
                        qa3 = qa3 ? qa3 : 0.00;
                        var qa4 = parseFloat(Math.round((annualSalesData[0].Q4Amount) * 100) / 100).toFixed(2);
                        qa4 = qa4 ? qa4 : 0.00;
                        var qt1 = parseFloat(Math.round((annualSalesData[0].Q1Tax) * 100) / 100).toFixed(2);
                        qt1 = qt1 ? qt1 : 0.00;
                        var qt2 = parseFloat(Math.round((annualSalesData[0].Q2Tax) * 100) / 100).toFixed(2);
                        qt2 = qt2 ? qt2 : 0.00;
                        var qt3 = parseFloat(Math.round((annualSalesData[0].Q3Tax) * 100) / 100).toFixed(2);
                        qt3 = qt3 ? qt3 : 0.00;
                        var qt4 = parseFloat(Math.round((annualSalesData[0].Q4Tax) * 100) / 100).toFixed(2);
                        qt4 = qt4 ? qt4 : 0.00;

                        if (checkTaxStatus == true) {
                            Morris.Bar({
                                element: 'salesGraphReport',
                                data: [
                                    {y: 'Q1', a: qa1 - qt1, b: qt1},
                                    {y: 'Q2', a: qa2 - qt2, b: qt2},
                                    {y: 'Q3', a: qa3 - qt3, b: qt3},
                                    {y: 'Q4', a: qa4 - qt4, b: qt4}
                                ],
                                xkey: 'y',
                                ykeys: ['a', 'b'],
                                labels: ['Sales Excluding Tax', 'Total Tax'],
                                barColors: ['#FF8000', '#663300'],
                                yLabelFormat: function (y) {
                                    return cSymbol + '. ' + accounting.formatMoney(y);
                                },
                                hideHover: 'auto'
                            });
                        } else {
                            Morris.Bar({
                                element: 'salesGraphReport',
                                data: [
                                    {y: 'Q1', a: qa1 - qt1},
                                    {y: 'Q2', a: qa2 - qt2},
                                    {y: 'Q3', a: qa3 - qt3},
                                    {y: 'Q4', a: qa4 - qt4}
                                ],
                                xkey: 'y',
                                ykeys: ['a'],
                                labels: ['Sales Excluding Tax'],
                                barColors: ['#FF8000'],
                                yLabelFormat: function (y) {
                                    return cSymbol + '. ' + accounting.formatMoney(y);
                                },
                                hideHover: 'auto'
                            });
                        }
                    }
                }
                );
            }
        }

        else {
            p_notification(false, "Please select a correct report type first");
        }
    });
    var fday1 = '-01';
    var tday30 = '-30';
    var tday31 = '-31';
    /* Set day to from_month text field
     * @author SANDUN <sandun@thinkcube.com>
     * */

    function fromday(fromMonthSplit) {
        var fromMonth = $('#fromMonth').val();

        if (fromMonthSplit == '01') {
            fm = fromMonth.concat(fday1);
        }
        else if (fromMonthSplit == '02') {
            fm = fromMonth.concat(fday1);
        }
        else if (fromMonthSplit == '03') {
            fm = fromMonth.concat(fday1);
        }
        else if (fromMonthSplit == '04') {
            fm = fromMonth.concat(fday1);
        }
        else if (fromMonthSplit == '05') {
            fm = fromMonth.concat(fday1);
        }
        else if (fromMonthSplit == '06') {
            fm = fromMonth.concat(fday1);
        }
        else if (fromMonthSplit == '07') {
            fm = fromMonth.concat(fday1);
        }
        else if (fromMonthSplit == '08') {
            fm = fromMonth.concat(fday1);
        }
        else if (fromMonthSplit == '09') {
            fm = fromMonth.concat(fday1);
        }
        else if (fromMonthSplit == '10') {
            fm = fromMonth.concat(fday1);
        }
        else if (fromMonthSplit == '11') {
            fm = fromMonth.concat(fday1);
        }
        else if (fromMonthSplit == '12') {
            fm = fromMonth.concat(fday1);
        }
    }

    /* Set day to to_month text field
     * @author SANDUN <sandun@thinkcube.com>
     * */
    function today(toMonthSplit, year) {
        var toMonth = $('#toMonth').val();
        if (toMonthSplit == '01') {
            tm = toMonth.concat(tday31);
        }
        else if (toMonthSplit == '02') {
            if (leapYear(year)) {
                var tday = '-29';
            } else {
                var tday = '-28';
            }
            tm = toMonth.concat(tday);
        }
        else if (toMonthSplit == '03') {
            tm = toMonth.concat(tday31);
        }
        else if (toMonthSplit == '04') {
            tm = toMonth.concat(tday30);
        }
        else if (toMonthSplit == '05') {
            tm = toMonth.concat(tday30);
        }
        else if (toMonthSplit == '06') {
            tm = toMonth.concat(tday30);
        }
        else if (toMonthSplit == '07') {
            tm = toMonth.concat(tday31);
        }
        else if (toMonthSplit == '08') {
            tm = toMonth.concat(tday31);
        }
        else if (toMonthSplit == '09') {
            tm = toMonth.concat(tday30);
        }
        else if (toMonthSplit == '10') {
            tm = toMonth.concat(tday31);
        }
        else if (toMonthSplit == '11') {
            tm = toMonth.concat(tday30);
        }
        else if (toMonthSplit == '12') {
            tm = toMonth.concat(tday31);
        }

    }

    /* validation function for Reporting Form
     * @author SANDUN <sandun@thinkcube.com>
     * */
    function validteinput(inputs) {
        var fromDate = inputs[0];
        var toDate = inputs[1];
        if (fromDate == null || fromDate == "") {
            p_notification(false, "Plese select From Date");
            document.getElementById("fromDate").focus();
        } else if (toDate == null || toDate == "") {
            p_notification(false, "Please select To Date");
            document.getElementById("toDate").focus();
        } else if (toDate < fromDate) {
            p_notification(false, "Please select valid date range.")

        }
        else
            return true;
    }

    /*
     * @author SANDUN <sandun@thinkcube.com>
     * return month number using month string name
     */
    function monthToNumber(month) {
        if (month == 'Jan') {
            beforemonth = '01';
        } else if (month == 'Feb') {

            beforemonth = '02';
        } else if (month == 'Mar') {


            beforemonth = '03';
        } else if (month == 'Apr') {
            beforemonth = '04';
        } else if (month == 'May') {
            beforemonth = '05';
        } else if (month == 'Jun') {
            beforemonth = '06';
        } else if (month == 'Jul') {
            beforemonth = '07';
        } else if (month == 'Aug') {
            beforemonth = '08';
        } else if (month == 'Sep') {
            beforemonth = '09';
        } else if (month == 'Oct') {
            beforemonth = 10;
        } else if (month == 'Nov') {
            beforemonth = 11;
        } else if (month == 'Dec') {
            beforemonth = 12;
        }
    }

    function initialCondition() {
        //show
        $('#viewGraph').show();
        $('#fromMonth,#toMonth').show();
        //hide
        $('#tax,#lblTax').show();
        $('#salesReport').hide();
        $('#monthlySalesGraph,#dailySalesGraph,#annualSalesGraph').hide();
        $('#fromDate,#toDate,#date,#year').hide();
//        $('#paymentList').hide();
//        $('.paymentpp').hide();
    }

//    function getCheckedStatus() {
//        var checkTaxStatus;
//        if (tax.checked == false) {
//            checkTaxStatus = false;
//        } else {
//            checkTaxStatus = true;
//        }
//        return checkTaxStatus;
//    }
    /*
     * @author SANDUN <sandun@thinkcube.com>
     * return boolean
     * */
    function leapYear(year) {
        return ((year % 4 == 0) && (year % 100 != 0)) || (year % 400 == 0);
    }

    //responsive issue fix
//    $(window).bind('resize ready', function () {
//        if ($(window).width() <= 480) {
//            $('.report_from_month').css('margin-bottom', '15px').removeClass('col-xs-4').addClass('col-xs-12');
//            $('.set_to_date').css('margin-bottom', '15px').removeClass('col-xs-4').addClass('col-xs-12');
//            $('.btn_panel_height_high').removeClass('btn-group');
//            $('#view_report, #view_graph, #generate_pdf, #excel_report').removeClass('btn-default').addClass('col-xs-12 btn-primary').css('margin-bottom', '8px');
//            $('#btnMonthly, #btnDaily, #btn_annual').css('margin-bottom', '15px');
//        } else {
//            $('.report_from_month').css('margin-bottom', '0').removeClass('col-xs-12').addClass('col-xs-4');
//            $('.set_to_date').css('margin-bottom', '0').removeClass('col-xs-12').addClass('col-xs-4');
//            $('.btn_panel_height_high').addClass('btn-group');
//            $('#view_report, #view_graph, #generate_pdf, #excel_report').addClass('btn-default').removeClass('col-xs-12 btn-primary').css('margin-bottom', '0px');
//            $('#btnMonthly, #btnDaily, #btn_annual').css('margin-bottom', '0');
//        }
//    });

});

