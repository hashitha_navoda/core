$(document).ready(function() {
    var reportType = 'payments';
    var isAllInvoices = 0;
    var isAllPayments = 0;
    var locationIds = $('#branch-locations').val();
    initialize();

    var checkin = $('#fromDate').datepicker({
    }).on('changeDate', function(ev) {
        if (ev.date.valueOf() < checkout.date.valueOf()) {
            var currentDate = new Date(ev.date);
            var newDate = new Date();
            newDate.setDate(currentDate.getDate() + 1);
            checkout.setValue(newDate);
        }
        checkin.hide();
        $('#toDate')[0].focus();
    }).data('datepicker');
    var checkout = $('#toDate').datepicker({
        onRender: function(date) {
            return date.valueOf() < checkin.date.valueOf() ? 'disabled' : '';
        }
    }).on('changeDate', function(ev) {
        checkout.hide();
    }).data('datepicker');

    $('#date').datepicker();

    var checkinYear = $('#year').datepicker({
    }).on('changeDate', function(ev) {
        var newD = new Date(ev.date.getYear());
        checkinYear.hide();

    }).data('datepicker');

    $('#payments').click(function(e) {
        reportType = 'payments';
        $('#div_invoice').addClass('hidden');
        $('#div_location').addClass('hidden');
        $('#div_sales_person').addClass('hidden');
        $('#bankList').removeClass('hidden');
        $('#accountList').removeClass('hidden');
        $('#div_card_types').addClass('hidden');
        $('#div_payment').addClass('hidden');
        $('#div_selection').removeClass('hidden');
        $('#bankDiv').removeClass('hidden');
        $('#accountDiv').removeClass('hidden');
        $('#categorizedPayMod').removeClass('hidden');
        initialize();
    });
    $('#cancelledPayments').click(function(e) {
        reportType = 'cancelledPayments';
        $('#div_invoice').addClass('hidden');
        $('#bankList').addClass('hidden');
        $('#bankDiv').addClass('hidden');
        $('#div_sales_person').addClass('hidden');
        $('#accountDiv').addClass('hidden');
        $('#accountList').addClass('hidden');
        $('#div_location').addClass('hidden');
        $('#div_card_types').addClass('hidden');
        $('#div_payment').addClass('hidden');
        $('#div_selection').removeClass('hidden');
        $('#categorizedPayMod').addClass('hidden');
        initialize();
    });
    $('#paymentDetails').click(function(e) {
        reportType = 'paymentDetails';
        $('#bankList').removeClass('hidden');
        $('#accountList').removeClass('hidden');
        $('#bankDiv').removeClass('hidden');
        $('#accountDiv').removeClass('hidden');
        $('#div_invoice').addClass('hidden');
        $('#div_location').removeClass('hidden');
        $('#div_sales_person').removeClass('hidden');
        $('#div_payment').removeClass('hidden');
        $('#div_selection').addClass('hidden');
        $('#div_card_types').addClass('hidden');
        $('#categorizedPayMod').addClass('hidden');
        initialize();
    });
    $('#cardTypePayments').click(function(e) {
        reportType = 'cardTypePayments';
        $('#div_card_types').removeClass('hidden');
        $('#div_invoice').addClass('hidden');
        $('#bankList').addClass('hidden');
        $('#accountList').addClass('hidden');
        $('#bankDiv').addClass('hidden');
        $('#div_sales_person').addClass('hidden');
        $('#accountDiv').addClass('hidden');
        $('#div_location').addClass('hidden');
        $('#div_selection').addClass('hidden');
        $('#div_payment').addClass('hidden');        
        $('#categorizedPayMod').addClass('hidden');
        initialize();
    });

    function initialize() {
        $('#paymentReport').hide();
        $('#fromDate, #toDate').val('');
        $('#locations, #paymentTypes, #cardTypes').selectpicker('val', '');
    }

    //load account list
    $(document).on('change', '#bankList', function () {
        var bankId = $(this).val();
        if (bankId) {
            getBankAccountListByBankId(bankId);
        } else {
            $('#accountList').empty();
            $('#accountList').append($("<option>", {value: '', html: '----- Select Account -----'}));
        }
    });

    $(document).on('change', '#branch-locations', function() {
        locationIds = $('#branch-locations').val();
        $('#invoiceName').siblings('div.bootstrap-select').remove();
        $('#invoiceName').removeData().empty();
        loadDropDownFromDatabase('/invoice-api/search-location-wise-open-sales-invoices-for-document-dropdown', locationIds, 0, '#invoiceName', "", true);
        $('button[data-id="invoiceName"]').removeClass('disabled');
    });

    $('.isAllInvoices').on('click', function() {
        isAllInvoices = $(this).val();
        if (isAllInvoices == true) {
            $('#invoiceName').val('').trigger('change');
            $('#invoiceName').prop('disabled', true);
        } else {
            $('#invoiceName').prop('disabled', false);
        }
    });
    
    $('.isAllPayments').on('click', function() {
        isAllPayments = $(this).val();
        if (isAllPayments == true) {
            $('#paymentName').val('').trigger('change');
            $('#paymentName').prop('disabled', true);
        } else {
            $('#paymentName').prop('disabled', false);
        }
    });

    loadDropDownFromDatabase('/invoice-api/search-location-wise-open-sales-invoices-for-document-dropdown', locationIds, '', '#invoiceName', "", true);
    loadDropDownFromDatabase('/customerPaymentsAPI/searchPaymentsForDocumentDropdown', '', '', '#paymentName', "", true);

    $('#viewReport').click(function(e) {
        if (reportType === 'payments') {
            var getpdfdataurl = BASE_URL + '/reports/get/sales-payment/view-payments-summery';

            var inputs = {
                fromDate: $('#fromDate').val(),
                toDate: $('#toDate').val(),
                locations: $('#locations').val(),
                paymentTypes: $('#paymentTypes').val(),
                accountId: $('#accountList').val(),
                categorizedPayModOpt: $('#categorizedPayModOpt').prop("checked")
            };

            if (validteinput(inputs)) {
                var getpdfdatarequest = eb.post(getpdfdataurl, inputs);
                getpdfdatarequest.done(function(respond) {
                    if (respond.status) {
                        setContentToReport('paymentReport', respond);
                    }
                });
            }

        } else if (reportType === 'cancelledPayments') {
            var getpdfdataurl = BASE_URL + '/reports/get/sales-payment/view-cancelled-payments-summery';

            var inputs = {
                fromDate: $('#fromDate').val(),
                toDate: $('#toDate').val(),
                locations: $('#locations').val(),
                paymentTypes: $('#paymentTypes').val()
            };

            if (validteinput(inputs)) {
                var getpdfdatarequest = eb.post(getpdfdataurl, inputs);
                getpdfdatarequest.done(function(respond) {
                    if (respond.status) {
                        setContentToReport('paymentReport', respond);
                    }
                });
            }
        } else if (reportType === 'paymentDetails') {
            var url = BASE_URL + '/reports/get/sales-payment/sales-details-report';
            var input = {
                fromDate: $('#fromDate').val(),
                toDate: $('#toDate').val(),
                isAllPayments: isAllPayments,
                paymentIds: $('#paymentName').val(),
                salesPersons: $('#salesPersons').val(),
                locationIds: locationIds,
                accountId: $('#accountList').val()
            };

            if (paymentDetailsFromValidation(input)) {
                var request = eb.post(url, input);
                request.done(function(respond) {
                    if (respond.status) {
                        setContentToReport('paymentReport', respond);
                    }
                });
            }
        } else if (reportType === 'cardTypePayments') {
            var url = BASE_URL + '/reports/get/sales-payment/card-type-payment-report';
            var input = {
                fromDate: $('#fromDate').val(),
                toDate: $('#toDate').val()
            };

            if (dateValidation(input)) {
                var postInput = {
                    fromDate: $('#fromDate').val(),
                    toDate: $('#toDate').val(),
                    cardTypeIds: $("#cardTypes").val()
                };
                var request = eb.post(url, postInput);
                request.done(function(respond) {
                    if (respond.status) {
                        setContentToReport('paymentReport', respond);
                    }
                });
            }
        }
    });

    $('#generatePdf').click(function(e) {
        if (reportType == 'payments') {

            var inputs = {
                fromDate: $('#fromDate').val(),
                toDate: $('#toDate').val(),
                locations: $('#locations').val(),
                paymentTypes: $('#paymentTypes').val(),
                accountId: $('#accountList').val(),
                categorizedPayModOpt: $('#categorizedPayModOpt').prop("checked")
            };

            if (validteinput(inputs)) {
                eb.ajax({
                    type: 'POST',
                    url: BASE_URL + "/reports/get/sales-payment/generate-payments-summery-pdf",
                    dataType: 'json',
                    data: inputs,
                    success: function(respond) {
                        if (respond.status) {
                            window.open(BASE_URL + '/' + respond.data, '_blank');
                        }
                    }
                });
            }
        } else if (reportType == 'cancelledPayments') {

            var inputs = {
                fromDate: $('#fromDate').val(),
                toDate: $('#toDate').val(),
                locations: $('#locations').val(),
                paymentTypes: $('#paymentTypes').val()
            };

            if (validteinput(inputs)) {
                eb.ajax({
                    type: 'POST',
                    url: BASE_URL + "/reports/get/sales-payment/generate-canclled-payments-summery-pdf",
                    dataType: 'json',
                    data: inputs,
                    success: function(respond) {
                        if (respond.status) {
                            window.open(BASE_URL + '/' + respond.data, '_blank');
                        }
                    }
                });
            }
        } else if (reportType === 'paymentDetails') {
            var url = BASE_URL + '/reports/get/sales-payment/sales-details-report-pdf';
            var input = {
                fromDate: $('#fromDate').val(),
                toDate: $('#toDate').val(),
                isAllPayments: isAllPayments,
                paymentIds: $('#paymentName').val(),
                salesPersons: $('#salesPersons').val(),
                locationIds: locationIds,
                accountId: $('#accountList').val()
            };

            if (paymentDetailsFromValidation(input)) {
                eb.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: input,
                    success: function(respond) {
                        if (respond.status) {
                            window.open(BASE_URL + '/' + respond.data, '_blank');
                        }
                    }
                });
            }
        } else if (reportType === 'cardTypePayments') {
            var url = BASE_URL + '/reports/get/sales-payment/card-type-payment-report-pdf';
            var input = {
                fromDate: $('#fromDate').val(),
                toDate: $('#toDate').val()
            };

            if (dateValidation(input)) {
                var postInput = {
                    fromDate: $('#fromDate').val(),
                    toDate: $('#toDate').val(),
                    cardTypeIds: $("#cardTypes").val()
                };
                var request = eb.post(url, postInput);
                request.done(function(respond) {
                    if (respond.status) {
                        window.open(BASE_URL + '/' + respond.data, '_blank');
                    }
                });
            }
        }

    });

    $('#csvReport').on('click', function(e) {
        if (reportType == 'payments') {

            var inputs = {
                fromDate: $('#fromDate').val(),
                toDate: $('#toDate').val(),
                locations: $('#locations').val(),
                paymentTypes: $('#paymentTypes').val(),
                accountId: $('#accountList').val(),
                categorizedPayModOpt: $('#categorizedPayModOpt').prop("checked")
            };

            if (validteinput(inputs)) {
                var url = BASE_URL + '/reports/get/sales-payment/generate-payments-summery-sheet';
                eb.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: inputs,
                    success: function(respond) {
                        if (respond.status) {
                            window.open(BASE_URL + '/' + respond.data, '_blank');
                        }
                    }
                });
            }
        } else if (reportType == 'cancelledPayments') {

            var inputs = {
                fromDate: $('#fromDate').val(),
                toDate: $('#toDate').val(),
                locations: $('#locations').val(),
                paymentTypes: $('#paymentTypes').val()
            };

            if (validteinput(inputs)) {
                var url = BASE_URL + '/reports/get/sales-payment/generate-cancelled-payments-summery-sheet';
                eb.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: inputs,
                    success: function(respond) {
                        if (respond.status) {
                            window.open(BASE_URL + '/' + respond.data, '_blank');
                        }
                    }
                });
            }
        } else if (reportType == 'paymentDetails') {
            var url = BASE_URL + '/reports/get/sales-payment/sales-details-report-csv';
            var input = {
                fromDate: $('#fromDate').val(),
                toDate: $('#toDate').val(),
                isAllPayments: isAllPayments,
                paymentIds: $('#paymentName').val(),
                salesPersons: $('#salesPersons').val(),
                locationIds: locationIds,
                accountId: $('#accountList').val()
            };

            if (paymentDetailsFromValidation(input)) {
                eb.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: input,
                    success: function(respond) {
                        if (respond.status) {
                            window.open(BASE_URL + '/' + respond.data, '_blank');
                        }
                    }
                });
            }
        } else if (reportType === 'cardTypePayments') {
            var url = BASE_URL + '/reports/get/sales-payment/card-type-payment-report-csv';
            var input = {
                fromDate: $('#fromDate').val(),
                toDate: $('#toDate').val()
            };

            if (dateValidation(input)) {
                var postInput = {
                    fromDate: $('#fromDate').val(),
                    toDate: $('#toDate').val(),
                    cardTypeIds: $("#cardTypes").val()
                };
                var request = eb.post(url, postInput);
                request.done(function(respond) {
                    if (respond.status) {
                        window.open(BASE_URL + '/' + respond.data, '_blank');
                    }
                });
            }
        }
    });

    function validteinput(inputs) {
        var fromDate = inputs.fromDate;
        var toDate = inputs.toDate;
        var locations = inputs.locations;
        var paymentTypes = inputs.paymentTypes;

        if (fromDate === null || fromDate === "") {
            p_notification(false, eb.getMessage('ERR_DAISALE_FROMD'));
            $('#fromDate').focus();
            return false;
        } else if (toDate === null || toDate === "") {
            p_notification(false, eb.getMessage('ERR_DAISALE_TOD'));
            $('#toDate').focus();
            return false;
        } else if (toDate < fromDate) {
            p_notification(false, eb.getMessage('ERR_DAISALE_TODFROMD'));
            $('#toDate').focus();
            return false;
        } else if (locations === null) {
            p_notification(false, "Please select Location(s).")
            $('#locations').focus();
            return false;
        } else if (paymentTypes === null) {
            p_notification(false, "Please select Payment Method(s).");
            $('#paymentTypes').focus();
            return false;
        } else {
            return true;
        }
    }

    function paymentDetailsFromValidation(input) {

        if (input.locationIds == null) {
            p_notification(false, 'Please select atleast one location.');
            return false;
        } else if (input.isAllPayments == false && input.paymentIds == null) {
            p_notification(false, 'Please select atleast one payment.');
            return false;
        } else if ((input.toDate) && (input.fromDate == null || input.fromDate == "")) {
            p_notification(false, 'Please select From Date.');
            document.getElementById("fromDate").focus();
            return false;
        } else if ((input.fromDate) && (input.toDate == null || input.toDate == "")) {
            p_notification(false, 'Please select To Date.');
            document.getElementById("toDate").focus();
            return false;
        } else if ((input.fromDate && input.toDate) && input.fromDate > input.toDate) {
            p_notification(false, 'Please select a valid date range.');
            document.getElementById("toDate").focus();
            return false;
        } else {
            return true;
        }
    }

    function dateValidation(input) {

        if ((input.toDate) && (input.fromDate == null || input.fromDate == "")) {
            p_notification(false, 'Please select From Date.');
            document.getElementById("fromDate").focus();
            return false;
        } else if ((input.fromDate) && (input.toDate == null || input.toDate == "")) {
            p_notification(false, 'Please select To Date.');
            document.getElementById("toDate").focus();
            return false;
        } else if ((input.fromDate && input.toDate) && input.fromDate > input.toDate) {
            p_notification(false, 'Please select a valid date range.');
            document.getElementById("toDate").focus();
            return false;
        } else {
            return true;
        }
    }

    //get bank account list
    function getBankAccountListByBankId(bankId) {
        $.ajax({
            type: 'POST',
            url: BASE_URL + '/account-api/bank-account-list-for-dropdown',
            data: {bankId: bankId},
            success: function (respond) {
                console.log(respond.data.list);
                //empty options
                $('#accountList').empty();
                $('#accountList').append($("<option>", {value: '', html: '----- Select Account -----'}));
                $(respond.data.list).each(function (i, v) {
                    $('#accountList').append($("<option>", {value: v.value, html: v.text}));
                });
            }
        });
    }

});

