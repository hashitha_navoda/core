$(document).ready(function() {
    var reportType = 'pettyCashVoucher';

//    Set Date picker
    var checkin = $('#fromDate').datepicker({
    }).on('changeDate', function(ev) {
        if (ev.date.valueOf() < checkout.date.valueOf()) {
            var currentDate = new Date(ev.date);
            var newDate = new Date();
            newDate.setDate(currentDate.getDate() + 1);
            checkout.setValue(newDate);
        }
        checkin.hide();
        $('#toDate')[0].focus();
    }).data('datepicker');

    var checkout = $('#toDate').datepicker({
        onRender: function(date) {
            return date.valueOf() <= checkin.date.valueOf() ? 'disabled' : '';
        }
    }).on('changeDate', function(ev) {
        checkout.hide();
    }).data('datepicker');

    $('#pettyCashVoucher').click(function() {
        reportType = 'pettyCashVoucher';
        resetInputField();
    });

    $('#viewReport').click(function() {
        var inputs = {
            pvoucherType: $('#pettyCashVoucherType').val(),
            pExpenseType: $('#pettyCashExpenetype').val(),
            fromDate: $('#fromDate').val(),
            toDate: $('#toDate').val(),
        };

        if (reportType === 'pettyCashVoucher') {
            var getpdfdataurl = BASE_URL + '/report/get/petty-cash-voucher/view-report';

            var getpdfdatarequest = eb.post(getpdfdataurl, inputs);
            getpdfdatarequest.done(function(respond) {
                if (respond.status == true) {
                    setContentToReport('pettyCashReportIFrame', respond);
                    $('#pettyCashReportIFrame').removeClass('hidden');
                }
            });
        }
    });

    $('#generatePdf').click(function() {
        var inputs = {
            pvoucherType: $('#pettyCashVoucherType').val(),
            pExpenseType: $('#pettyCashExpenetype').val(),
            fromDate: $('#fromDate').val(),
            toDate: $('#toDate').val(),
        };
        if (reportType === 'pettyCashVoucher') {
            eb.ajax({
                type: 'POST',
                url: BASE_URL + '/report/get/petty-cash-voucher/generate-pdf',
                dataType: 'json',
                data: inputs,
                success: function(respond) {
                    if (respond.status == true) {
                        window.open(BASE_URL + '/' + respond.data, '_blank');
                    }
                }
            });
        }
    });

    $('#csvReport').click(function() {
        var inputs = {
            pvoucherType: $('#pettyCashVoucherType').val(),
            pExpenseType: $('#pettyCashExpenetype').val(),
            fromDate: $('#fromDate').val(),
            toDate: $('#toDate').val(),
        };

        if (reportType === 'pettyCashVoucher') {
            var url = BASE_URL + '/report/get/petty-cash-voucher/generate-csv';
            eb.ajax({
                type: 'POST',
                url: url,
                dataType: 'json',
                data: inputs,
                success: function(respond) {
                    if (respond.status == true) {
                        window.open(BASE_URL + '/' + respond.data, '_blank');
                    }
                }
            });
        }
    });
});
function resetInputField() {
    $('#pettyCashVoucherType').val('').selectpicker('render');
    $('#pettyCashExpenetype').val('').selectpicker('render');
    $('#fromDate').val('');
    $('#toDate').val('');
}