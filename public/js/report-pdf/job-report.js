/*
 * @author SANDUN <sandun@thinkcube.com>
 * Job card Reporting functions
 */
var projectId = '';
var jobId = '';
var activityId = '';
var activityCode = '';
var isAllSelect = 0;
var userId = '';
var customerId = '';
var dutyManagerId = '';
var tempProductId = '';
var fromDate = '';
var toDate = '';
var typeId = 'activityNo';
var vehicleIds = null;
var employeeIds = null;
var serviceIds = null;
var locationIds = null;
var isAllEmployees = false;
var isAllServices = false;
var fromDate = null;
var toDate = null;
var isAmountBase = false;
var isQtyBase = true;

$(document).ready(function() {
    initialSetUp();
    var reportType = 'employeeIncentive';
    var checkin = $('#from_date').datepicker({}).on('changeDate', function(ev) {
        if (ev.date.valueOf() < checkout.date.valueOf()) {
            var newDate = new Date(ev.date)
            newDate.setDate(newDate.getDate());
            checkout.setValue(newDate);
        }
        checkin.hide();
        $('#to_date')[0].focus();
    }).data('datepicker');
    var checkout = $('#to_date').datepicker({
        onRender: function(date) {
            return date.valueOf() < checkin.date.valueOf() ? 'disabled' : '';
        }
    }).on('changeDate', function(ev) {
        checkout.hide();
    }).data('datepicker');

     var checkout2 = $('#to_date2').datepicker({

    }).on('changeDate', function(ev) {
        checkout2.hide();
    }).data('datepicker');


    $("#employeeIncentiveReport").on("click", function() {
        reportType = 'employeeIncentive';
        hideReportIFrame('jobCardReport');
        // $(".invoice-value-selectpickers").removeClass('hidden');
        // $(".types,.activity-list,.job-list,.project-list").removeClass('hidden');
        // $(".temporary-product-list").addClass('hidden');
        // $(".inquiry-log-selectpickers").addClass('hidden');
        // $(".temp-product-list").addClass('hidden');
        // $(".date-range").addClass('hidden');
        // $("input[type='radio'][id='customSelect']").prop('checked', true);
        // $("input[type='radio'][id='allSelect']").prop('checked', false);
        // $('#job-and-activity-types').addClass('hidden');
        // $('#locationSelection').addClass('hidden');
        // $("#isAllSelectlbl").removeClass('hidden');
        // $('.vehical-allocation-selectpickers').addClass('hidden');
        $(".employee-selectpickers").removeClass('hidden');
        $(".service-selectpickers").removeClass('hidden');
        $(".hide_services").removeClass('hidden');
        // resetIsAllSelect();
    });

    $('.isAllEmployee').on('click', function() {
        isAllEmployees = $(this).val();
        if (isAllEmployees == true) {
            $('#employeeList').val('').trigger('change');
            $('#employeeList').prop('disabled', true);
        } else {
            $('#employeeList').prop('disabled', false);
        }
    });

    $('.isAllService').on('click', function() {
        isAllServices = $(this).val();
        if (isAllServices == true) {
            $('#serviceList').val('').trigger('change');
            $('#serviceList').prop('disabled', true);
        } else {
            $('#serviceList').prop('disabled', false);
        }
    });

    loadDropDownFromDatabase('/employee-api/searchEmployeeForDropdown', "", 0, '#employeeList', "", true);
    loadDropDownFromDatabase('/task_setup_api/searchTasksForDropdown', "", 0, '#serviceList', "", true);



    $("#viewReport").on('click', function() {
        setValues();
        if (reportType == 'employeeIncentive') {
            var input = {};
            input.isAllEmployees = isAllEmployees;
            input.isAllServices = isAllServices;
            input.employeeIds = employeeIds;
            input.serviceIds = serviceIds;
            input.toDate = toDate;
            input.fromDate = fromDate;
            input.isQtyBase = isQtyBase;
            input.isAmountBase = isAmountBase;

            var zeroService = false;
            if($('#zeroService').prop('checked') == true){
                zeroService = true;
            }
            input.zeroService = zeroService;

            if (validteEmployeeActivity(input)) {
                var url = BASE_URL + '/reports/api/job/view-employee-incentive-report';
                eb.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: input,
                    success: function(respond) {
                        if (respond.status == true) {
                            setContentToReport('jobCardReport', respond);

                        }
                    }
                });
            }
        } 
    });

    $('#generatePdf').on('click', function() {
        
        if (reportType == 'employeeIncentive') {
            setValues();
            var input = {};
            input.isAllEmployees = isAllEmployees;
            input.isAllServices = isAllServices;
            input.employeeIds = employeeIds;
            input.serviceIds = serviceIds;
            input.toDate = toDate;
            input.fromDate = fromDate;
            input.isQtyBase = isQtyBase;
            input.isAmountBase = isAmountBase;

            var zeroService = false;
            if($('#zeroService').prop('checked') == true){
                zeroService = true;
            }
            input.zeroService = zeroService;

            if (validteEmployeeActivity(input)) {
                var url = BASE_URL + '/reports/api/job/employee-incentive-report-pdf';
                eb.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: input,
                    success: function(respond) {
                        if (respond.status == true) {
                            window.open(BASE_URL + '/' + respond.data, '_blank');
                        }
                    }
                });
            }
        } 
    });

    $('#csvReport').on('click', function() {
        // setValues();
        if (reportType == 'employeeIncentive') {
            setValues();
            var input = {};
            input.isAllEmployees = isAllEmployees;
            input.isAllServices = isAllServices;
            input.employeeIds = employeeIds;
            input.serviceIds = serviceIds;
            input.toDate = toDate;
            input.fromDate = fromDate;
            input.isQtyBase = isQtyBase;
            input.isAmountBase = isAmountBase;

            var zeroService = false;
            if($('#zeroService').prop('checked') == true){
                zeroService = true;
            }
            input.zeroService = zeroService;

            if (validteEmployeeActivity(input)) {
                var url = BASE_URL + '/reports/api/job/generate-employee-incentive-sheet';
                eb.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: input,
                    success: function(respond) {
                        if (respond.status == true) {
                            window.open(BASE_URL + '/' + respond.data, '_blank');
                        }
                    }
                });
            }
        } 
    });

    function initialSetUp() {
        $(".employee-selectpickers").removeClass('hidden');
        $(".incentiveReportType-radio").removeClass('hidden');
        $(".service-selectpickers").removeClass('hidden');
        $('.to_date_lbl,.from_date_lbl').removeClass('hidden');
        $(".hide_services").removeClass('hidden');
        hideReportIFrame('jobCardReport');
    }

    function setValues() {
        fromDate = $("#from_date").val();
        toDate = $("#to_date").val();
        serviceIds = $('#serviceList').val();
        employeeIds = $('#employeeList').val();
        locationIds = $('#locationList').val();
    }

    function validteEmployeeActivity(input) {
        var fromDate = input.fromDate;
        var toDate = input.toDate;
        var isAllEmployees = input.isAllEmployees;
        var employeeIds = input.employeeIds;
        var isAllServices = input.isAllServices;
        var serviceIds = input.serviceIds;

        if (isAllEmployees != true && (employeeIds == null || employeeIds == "")) {
            p_notification(false, 'Please select at least one Employee.');
        } else if (isAllServices != true && (serviceIds == null || serviceIds == "")) {
            p_notification(false, 'Please select at least one Service.');
        }
        else if (fromDate == null || fromDate == "") {
            p_notification(false, 'Please select From Date.');
            document.getElementById("from_date").focus();
        }
        else if (toDate == null || toDate == "") {
            p_notification(false, 'Please select To Date.');
            document.getElementById("to_date").focus();
        }
        else if (fromDate > toDate) {
            p_notification(false, 'Please select a valid date range.');
        }
        else {
            return true;
        }
    }

    $('.isQtyBase').on('click', function() {
        isQtyBase = ($(this).val() == 1) ? true:false;
        isAmountBase = false;
        $("#isAmountBase").prop('checked', false);;
    });

    $('.isAmountBase').on('click', function() {
        isAmountBase = ($(this).val() == 1) ? true:false;
        isQtyBase = false;
        $("#isQtyBase").prop('checked', false);
    });
});


