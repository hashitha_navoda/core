$(document).ready( function(){
    
    reset();
    
    //initialize datetimepicker
    $('.datetimepicker').datetimepicker({
        format: 'yyyy-mm-dd',
        minView: 4,
        autoclose: true,
    });
    
    //load account list
    $(document).on('change','#bankList',function (){
        var bankId = $(this).val();
        if(bankId){
            getBankAccountListByBankId(bankId);
        } else {
            $('#accountList').empty();
            $('#accountList').append($("<option>", { value: '', html: '----- Select Account -----' }));
        }
    });
    
    $(document).on('click','#reconciliationDetails',function (){
        reset();
    });
    
    //for report btn
    $('.report-btn').on('click',function(){
        
        var btnId = $(this).prop('id');
        
        var checked = [];
        var accountId = $('#accountList').val();
        var fromDate  = $('#fromDate').val();
        var toDate    = $('#toDate').val();
        var chequeType = $('#chequeType').val();
        var checked    = $('#reconcileStatus').val();
        
        if(!accountId){
            p_notification(false, 'please select account');
            $('#cheque-report').hide();
        } else if((fromDate && !toDate) || (!fromDate && toDate)){
            p_notification(false, 'invalid date range');
            $('#cheque-report').hide();
        } else if(fromDate && toDate && (fromDate > toDate)){
            p_notification(false, 'invalid date range');
            $('#cheque-report').hide();
        } else {
            switch (btnId){
                case 'viewReport' :
                    viewReport(accountId,fromDate,toDate,chequeType,checked);
                    break;

                case 'generatePdf' :
                    pdfReport(accountId,fromDate,toDate,chequeType,checked);
                    break;

                case 'csvReport' :
                    csvReport(accountId,fromDate,toDate,chequeType,checked);
                    break;

                default :
                    console.log('invalid option');
            }
        }
    });
    
    function reset(){
        $('#bankList,#accountList').val("");
        $('#startDate,#endDate').val("");
        $('#cheque-report').hide();
    }
    
    //get bank account list
    function getBankAccountListByBankId(bankId){
        $.ajax({
            type: 'POST',
            url: BASE_URL + '/account-api/bank-account-list-for-dropdown',
            data: {bankId : bankId},
            success: function(respond) {
                console.log(respond.data.list);
                //empty options
                $('#accountList').empty();
                $('#accountList').append($("<option>", { value: '', html: '----- Select Account -----' }));
                $(respond.data.list).each(function(i, v){ 
                    $('#accountList').append($("<option>", { value: v.value, html: v.text }));
                });
            }
        });
    }
    
    var viewReport = function(accountId,fromDate,toDate,chequeType,reconcile){
        eb.ajax({
            type: 'POST',
            url: BASE_URL + '/api/cheque-report/generate-cheque-report',
            dataType: 'json',
            data: {accountId: accountId, fromDate: fromDate, toDate: toDate, chequeType:chequeType, reconcile:reconcile},
            success: function(respond) {
                if (respond.status == true) {
                    setContentToReport('cheque-report', respond);
                }
            }
        });
    }
    
    var pdfReport = function(accountId,fromDate,toDate,chequeType,reconcile){
        eb.ajax({
            type: 'POST',
            url: BASE_URL + '/api/cheque-report/generate-cheque-report-pdf',
            dataType: 'json',
            data: {accountId: accountId, fromDate: fromDate, toDate: toDate, chequeType:chequeType, reconcile:reconcile},
            success: function(respond) {
                if (respond.status == true) {
                    window.open(BASE_URL + '/' + respond.data, '_blank');
                }
            }
        });
    }
    
    var csvReport = function(accountId,fromDate,toDate,chequeType,reconcile){
        eb.ajax({
            type: 'POST',
            url: BASE_URL + '/api/cheque-report/generate-cheque-report-csv',
            dataType: 'json',
            data: {accountId: accountId, fromDate: fromDate, toDate: toDate, chequeType:chequeType,reconcile:reconcile},
            success: function(respond) {
                if (respond.status == true) {
                    window.open(BASE_URL + '/' + respond.data, '_blank');
                }
            }
        });
    }
    
});


