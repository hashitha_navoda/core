$(document).ready( function(){

    var reportType = 'salesOrderWiseInvoice';
    var isAllRepresentative = false;
    var isAllCustomers = false;
    var isAllSalesOrders = false;

    var checkin = $('#fromDate').datepicker({
    }).on('changeDate', function(ev) {
        if (ev.date.valueOf() < checkout.date.valueOf()) {
            var currentDate = new Date(ev.date);
            var newDate = new Date();
            newDate.setDate(currentDate.getDate() + 1);
            checkout.setValue(newDate);
        }
        checkin.hide();
        $('#toDate')[0].focus();
    }).data('datepicker');

    var checkout = $('#toDate').datepicker({
        onRender: function(date) {
            return date.valueOf() < checkin.date.valueOf() ? 'disabled' : '';
        }
    }).on('changeDate', function(ev) {
        checkout.hide();
    }).data('datepicker');


    //customer search
    loadDropDownFromDatabase('/customerAPI/search-customers-for-dropdown', "", "withDeactivatedCustomers", '#customerName', "", true);
    loadDropDownFromDatabase('/api/salesOrders/search-sales-orders-for-dropdown', "", 0, '#SO', '', '', '');

    $('.filer_by').addClass('hidden');
    $('.salesOrder').addClass('hidden');
    $('.deliveryNote').addClass('hidden');

    $('#sales_order_wise_invoice_report').on('click',function(){
    	reportType = 'salesOrderWiseInvoice';
    	$('.customer').removeClass('hidden');
    	$('.location').removeClass('hidden');
    	$('.sales-person').removeClass('hidden');
    	$('.filer_by').addClass('hidden');
    	$('.salesOrder').addClass('hidden');
        $('.deliveryNote').addClass('hidden');
    	$('.status_div').addClass('hidden');
    });

    $('#sales_order_wise_delivery_note_report').on('click',function(){
        reportType = 'salesOrderWiseDeliveryNote';
        $('.customer').addClass('hidden');
        $('.location').addClass('hidden');
        $('.sales-person').addClass('hidden');
        $('.filer_by').removeClass('hidden');
        $('.salesOrder').removeClass('hidden');
        $('.deliveryNote').removeClass('hidden');
        $('#DN').attr('disabled',true);
        $('#SO').attr('disabled',false);
        $('#salesOrderID').prop("checked",true);
        $('#customeSalesOrders').prop("checked",true);
        $('.status_div').addClass('hidden');
    });
    
    $('#sales_order_due_date_report').on('click',function(){
    	reportType = 'salesOrderDueDate';
    	$('.customer').addClass('hidden');
    	$('.location').addClass('hidden');
    	$('.sales-person').addClass('hidden');
    	$('.filer_by').addClass('hidden');
    	$('.salesOrder').addClass('hidden');
    	$('.deliveryNote').addClass('hidden');
    	$('#DN').attr('disabled',true);
    	$('#SO').attr('disabled',false);
    	$('#salesOrderID').prop("checked",true);
    	$('#customeSalesOrders').prop("checked",true);
        $('.status_div').removeClass('hidden');
    });

	$('input[name="filterRadio"]').on('click',function(){
		if($(this).hasClass('salesOrderID')){
			$('#SO').attr('disabled',false);
			$('#DN').attr('disabled',true);
		}else if($(this).hasClass('deliveryNoteID')){
			$('#SO').attr('disabled',true);
			$('#DN').attr('disabled',false);
		}
	});

	$('input[name="isAllSalesOrders"]').on('click',function(){
		if($(this).hasClass('customeSalesOrders')){
			$('#SO').attr('disabled',false);
			isAllSalesOrders = false;
		}else if($(this).hasClass('AllSalesOrders')){
			$('#SO').attr('disabled',true);
			isAllSalesOrders = true;
		}
	});


    $('#viewReport').click(function() {
        switch (reportType) {
            case 'salesOrderWiseInvoice' :
                var input = {};
                input.fromDate = $('#fromDate').val();
                input.toDate = $('#toDate').val();
                input.salesPersonIds = $('#representativeList').val();
                input.customerIds = $('#customerName').val();
                input.locationIds = $('#branchLocation').val();
                input.isAllCustomers = isAllCustomers;
                input.isAllRepresentative = isAllRepresentative;
                input.cusCategories = $('#customerCategoryList').val();

                if(validateSalesOrderWiseInvoice(input)) {
                    eb.ajax({
                        type: 'POST',
                        url: BASE_URL + '/reports/sales-order-report-api/generate-sales-order-wise-invoice-report',
                        data: input,
                        success: function(respond) {
                            if(respond.status){
                                $('#salesOrderReport').removeClass('hidden');
                                setContentToReport('salesOrderReport', respond);
                            }
                        }
                    });
                }
                break;
            
            case 'salesOrderDueDate' :
                var input = {};
                input.fromDate = $('#fromDate').val();
                input.toDate = $('#toDate').val();
                input.status = $('#soStatus').val();

                if(validateSalesOrderDueDate(input)) {
                    eb.ajax({
                        type: 'POST',
                        url: BASE_URL + '/reports/sales-order-report-api/generate-sales-order-due-date-report',
                        data: input,
                        success: function(respond) {
                            if(respond.status){
                                $('#salesOrderReport').removeClass('hidden');
                                setContentToReport('salesOrderReport', respond);
                            }
                        }
                    });
                }
                break;

            case 'salesOrderWiseDeliveryNote' :
				var input = {};
                input.fromDate = $('#fromDate').val();
                input.toDate = $('#toDate').val();
                input.isAllSalesOrders = isAllSalesOrders;
                input.salesOrderIds = $('#SO').val();

                if(validateSalesOrderWiseDeliveryNote(input)) {
                    eb.ajax({
                        type: 'POST',
                        url: BASE_URL + '/reports/sales-order-report-api/generate-sales-order-wise-delivery-note-report',
                        data: input,
                        success: function(respond) {
                            if(respond.status){
                                $('#salesOrderReport').removeClass('hidden');
                                setContentToReport('salesOrderReport', respond);
                            }
                        }
                    });
                }
                break;
            default :
                console.log('invalid option');
        }

    });

    $('#generatePdf').click(function() {
        switch (reportType) {
            case 'salesOrderWiseInvoice' :
                var input = {};
                input.fromDate = $('#fromDate').val();
                input.toDate = $('#toDate').val();
                input.salesPersonIds = $('#representativeList').val();
                input.customerIds = $('#customerName').val();
                input.locationIds = $('#branchLocation').val();
                input.isAllCustomers = isAllCustomers;
                input.isAllRepresentative = isAllRepresentative;
                input.cusCategories = $('#customerCategoryList').val();

                if(validateSalesOrderWiseInvoice(input)) {
                    eb.ajax({
                        type: 'POST',
                        url: BASE_URL + '/reports/sales-order-report-api/generate-sales-order-wise-invoice-pdf',
                        data: input,
                        success: function(respond) {
                            if(respond.status){
                                window.open(BASE_URL + '/' + respond.data, '_blank');
                            }
                        }
                    });
                }
                break;
            
            case 'salesOrderDueDate' :
                var input = {};
                input.fromDate = $('#fromDate').val();
                input.toDate = $('#toDate').val();
                input.status = $('#soStatus').val();

                if(validateSalesOrderDueDate(input)) {
                    eb.ajax({
                        type: 'POST',
                        url: BASE_URL + '/reports/sales-order-report-api/generate-sales-order-due-date-pdf',
                        data: input,
                        success: function(respond) {
                            if(respond.status){
                                window.open(BASE_URL + '/' + respond.data, '_blank');
                            }
                        }
                    });
                }
                break;

            case 'salesOrderWiseDeliveryNote' :
				var input = {};
                input.fromDate = $('#fromDate').val();
                input.toDate = $('#toDate').val();
                input.isAllSalesOrders = isAllSalesOrders;
                input.salesOrderIds = $('#SO').val();

                if(validateSalesOrderWiseDeliveryNote(input)) {
                    eb.ajax({
                        type: 'POST',
                        url: BASE_URL + '/reports/sales-order-report-api/generate-sales-order-wise-delivery-note-pdf',
                        data: input,
                        success: function(respond) {
                            if(respond.status){
                                window.open(BASE_URL + '/' + respond.data, '_blank');
                            }
                        }
                    });
                }
                break;

            default :
                console.log('invalid option');
        }
    });

    $('#csvReport').click(function() {
        switch (reportType) {
            case 'salesOrderWiseInvoice' :
                var input = {};
                input.fromDate = $('#fromDate').val();
                input.toDate = $('#toDate').val();
                input.salesPersonIds = $('#representativeList').val();
                input.customerIds = $('#customerName').val();
                input.locationIds = $('#branchLocation').val();
                input.isAllCustomers = isAllCustomers;
                input.isAllRepresentative = isAllRepresentative;
                input.cusCategories = $('#customerCategoryList').val();

                if(validateSalesOrderWiseInvoice(input)) {
                    eb.ajax({
                        type: 'POST',
                        url: BASE_URL + '/reports/sales-order-report-api/generate-sales-order-wise-invoice-csv',
                        data: input,
                        success: function(respond) {
                            if(respond.status){
                                window.open(BASE_URL + '/' + respond.data, '_blank');
                            }
                        }
                    });
                }
                break;

            case 'salesOrderDueDate' :
                var input = {};
                input.fromDate = $('#fromDate').val();
                input.toDate = $('#toDate').val();
                input.status = $('#soStatus').val();

                if(validateSalesOrderDueDate(input)) {
                    eb.ajax({
                        type: 'POST',
                        url: BASE_URL + '/reports/sales-order-report-api/generate-sales-order-due-date-csv',
                        data: input,
                        success: function(respond) {
                            if(respond.status){
                                window.open(BASE_URL + '/' + respond.data, '_blank');
                            }
                        }
                    });
                }
                break;

            case 'salesOrderWiseDeliveryNote' :
				var input = {};
                input.fromDate = $('#fromDate').val();
                input.toDate = $('#toDate').val();
                input.isAllSalesOrders = isAllSalesOrders;
                input.salesOrderIds = $('#SO').val();

                if(validateSalesOrderWiseDeliveryNote(input)) {
                    eb.ajax({
                        type: 'POST',
                        url: BASE_URL + '/reports/sales-order-report-api/generate-sales-order-wise-delivery-note-csv',
                        data: input,
                        success: function(respond) {
                            if(respond.status){
                                window.open(BASE_URL + '/' + respond.data, '_blank');
                            }
                        }
                    });
                }
                break;

            default :
                console.log('invalid option');
        }
    });

    $('.isAllRepresentative').on('click', function() {
        isAllRepresentative = $(this).val();
        if (isAllRepresentative == true) {
            $('#representativeList').val('').trigger('change');
            $('#representativeList').prop('disabled', true);
        } else {
            $('#representativeList').prop('disabled', false);
        }
    });

    $('.isAllCustomers').on('click', function() {
        isAllCustomers = $(this).val();
        if (isAllCustomers == 1) {
            $('#customerName').val('').trigger('change');
            $('#customerName').prop('disabled', true);
            $('#cusCategory').removeClass('hidden');
            $('#customerCategoryList').prop('disabled', false);
        } else {
            $('#customerName').prop('disabled', false);
            $('#cusCategory').addClass('hidden');
            $('#customerCategoryList').val('').trigger('change');
            $('#customerCategoryList').prop('disabled', true);
        }
    });

    function validateSalesOrderWiseInvoice(input) {
        if (input.fromDate === null || input.fromDate === "") {
            p_notification(false, eb.getMessage('ERR_DAISALE_FROMD'));
            $('#fromDate').focus();
            return false;
        } else if (input.toDate === null || input.toDate === "") {
            p_notification(false, eb.getMessage('ERR_DAISALE_TOD'));
            $('#toDate').focus();
            return false;
        } else if (input.toDate < input.fromDate) {
            p_notification(false, eb.getMessage('ERR_DAISALE_TODFROMD'));
            $('#toDate').focus();
            return false;
        } else if (input.locationIds == null) {
            p_notification(false, eb.getMessage('ERR_CUSREPO_SELELOCATION'));
            return false;
        } else if (input.customerIds == null && input.isAllCustomers == false) {
            p_notification(false, eb.getMessage('ERR_CUSREPO_SELECUST'));
            return false;
        } else if (input.salesPersonIds == null && input.isAllRepresentative == false) {
            p_notification(false, eb.getMessage('ERR_INVOICE_REPORT_SELECT_SALESPERSON'));
            return false;
        } else {
            return true;
        }
    }

     function validateSalesOrderWiseDeliveryNote(input) {
        if (input.fromDate === null || input.fromDate === "") {
            p_notification(false, eb.getMessage('ERR_DAISALE_FROMD'));
            $('#fromDate').focus();
            return false;
        } else if (input.toDate === null || input.toDate === "") {
            p_notification(false, eb.getMessage('ERR_DAISALE_TOD'));
            $('#toDate').focus();
            return false;
        } else if (input.toDate < input.fromDate) {
            p_notification(false, eb.getMessage('ERR_DAISALE_TODFROMD'));
            $('#toDate').focus();
            return false;
        } else if (input.salesOrderIds == null && input.isAllSalesOrders == false) {
            p_notification(false, eb.getMessage('ERR_SALES_ORDER_WISE_DN_REPORT_EMPTY_SO'));
            return false;
        } else {
            return true;
        }
    }
    
    function validateSalesOrderDueDate(input) {
        if (input.fromDate === null || input.fromDate === "") {
            p_notification(false, eb.getMessage('ERR_DAISALE_FROMD'));
            $('#fromDate').focus();
            return false;
        } else if (input.toDate === null || input.toDate === "") {
            p_notification(false, eb.getMessage('ERR_DAISALE_TOD'));
            $('#toDate').focus();
            return false;
        } else if (input.toDate < input.fromDate) {
            p_notification(false, eb.getMessage('ERR_DAISALE_TODFROMD'));
            $('#toDate').focus();
            return false;
        } else {
            return true;
        }
    }

})