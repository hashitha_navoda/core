/*
 * @author SANDUN <sandun@thinkcube.com>
 * Sales Reporting functions
 */
$(document).ready(function() {
    var reportType = 'posInvoice';
    /////Pick date/////////////
    var date = $('#date').datepicker({
    }).on('changeDate', function(ev) {
        var newD = new Date(ev.date);
        date.hide();
    }).data('datepicker');

    var checkinYear = $('#year').datepicker({
    }).on('changeDate', function(ev) {
        var newD = new Date(ev.date.getYear());
        checkinYear.hide();
    }).data('datepicker');
    
    var checkin = $('#salesSummeryFromDate').datepicker({
    }).on('changeDate', function(ev) {
        if (ev.date.valueOf() < checkout.date.valueOf()) {
            var currentDate = new Date(ev.date);
            var newDate = new Date();
            newDate.setDate(currentDate.getDate());
            checkout.setValue(newDate);
        }
        checkin.hide();
        $('#salesSummeryToDate')[0].focus();
    }).data('datepicker');
    var checkout = $('#salesSummeryToDate').datepicker({
        onRender: function(date) {
            return date.valueOf() < checkin.date.valueOf() ? 'disabled' : '';
        }
    }).on('changeDate', function(ev) {
        checkout.hide();
    }).data('datepicker');

    var checkin = $('#from-Date').datepicker({
    }).on('changeDate', function(ev) {
        if (ev.date.valueOf() < checkout.date.valueOf()) {
            var currentDate = new Date(ev.date);
            var newDate = new Date();
            newDate.setDate(currentDate.getDate());
            checkout.setValue(newDate);
        }
        checkin.hide();
        $('#to-Date')[0].focus();
    }).data('datepicker');
    var checkout = $('#to-Date').datepicker({
        onRender: function(date) {
            return date.valueOf() < checkin.date.valueOf() ? 'disabled' : '';
        }
    }).on('changeDate', function(ev) {
        checkout.hide();
    }).data('datepicker');

    initialCondition();
    setSelectPicker();

    $('#posInvoice').click(function(e) {
        reportType = 'posInvoice';
        hidePOSReportIFrame();
        $('.payment-method').hide();
        $('.user,.location,.report_from_date, .report_to_date').show();
        $('#sales-summery-div,#posPrint').addClass('hidden');
    });

    $('#userWiseBalanceSummary').click(function(e) {
        reportType = 'userWiseBalanceSummary';
        hidePOSReportIFrame();
        $('.payment-method').hide();
        $('.user,.location,.report_from_date, .report_to_date').show();
        $('#sales-summery-div,#posPrint').addClass('hidden');
    });

    $('#posPayment').click(function(e) {
        reportType = 'posPayment';
        hidePOSReportIFrame();
        $('.payment-method,.location,.report_from_date, .report_to_date').show();
        $('.user').show();
        $('#sales-summery-div,#posPrint').addClass('hidden');
    });

    $('#posItemDiscount').click(function(e) {
        reportType = 'posItemDiscount';
        hidePOSReportIFrame();
        $('.payment-method').hide();
        $('.user,.location,.report_from_date, .report_to_date').show();
        $('#sales-summery-div,#posPrint').addClass('hidden');
    });
    
    $('#salesSummery').click(function(e) {
        reportType = 'posSalesSummery';
        hidePOSReportIFrame();
        $('#sales-summery-div, #posPrint').removeClass('hidden');
        $('.payment-method,.user,.report_from_date, .report_to_date,.location').hide();
    });

    $('#viewReport').click(function(e) {
        var fromDate = $('#from-Date').val();
        var toDate = $('#to-Date').val();
        var userIds = $('#user').val();
        var locIds = $('#location').val();
        var paymentMethodsIds = $('#paymentMethod').val();
        var obj = {};
        obj.fromDate = $('#from-Date').val();
        obj.toDate = $('#to-Date').val();
        unSetMultiSelect(userIds, locIds, paymentMethodsIds);
        if (reportType == 'posInvoice') {

            if (posSalesSummeryInputValidation(obj)) {
                var url = BASE_URL + '/api/pos-report/pos-invoice-view';
                eb.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: {fromDate: fromDate, toDate: toDate, userIds: userIds, locIds: locIds},
                    success: function(respond) {
                        if (respond.status == true) {
                            setContentToReport('posReport', respond);
                        }
                    }
                });
            }
        } else if (reportType == 'userWiseBalanceSummary') {
            if (posSalesSummeryInputValidation(obj)) {
                var url = BASE_URL + '/api/pos-report/user-wise-balance-summary-view';
                eb.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: {fromDate: fromDate, toDate: toDate, userIds: userIds, locIds: locIds},
                    success: function(respond) {
                        if (respond.status == true) {
                            setContentToReport('posReport', respond);
                        }
                    }
                });
            }
        } else if (reportType == 'posPayment') {
            if (posSalesSummeryInputValidation(obj)) {
                var url = BASE_URL + '/api/pos-report/pos-payment-view';
                eb.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: {fromDate: fromDate, toDate: toDate, paymentMethodsIds: paymentMethodsIds, locIds: locIds,userIds: userIds},
                    success: function(respond) {
                        if (respond.status == true) {
                            setContentToReport('posReport', respond);
                        }
                    }
                });
            }
        } else if (reportType == 'posItemDiscount') {
            if (posSalesSummeryInputValidation(obj)) {
                var url = BASE_URL + '/api/pos-report/pos-item-discount-view';
                eb.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: {fromDate: fromDate, toDate: toDate, userIds: userIds, locIds: locIds},
                    success: function(respond) {
                        if (respond.status == true) {
                            setContentToReport('posReport', respond);
                        }
                    }
                });
            }
        } else if (reportType == 'posSalesSummery') {
            var obj = {};
            obj.fromDate = $('#salesSummeryFromDate').val();
            obj.toDate = $('#salesSummeryToDate').val();
            obj.userIds = $('#salesSummeryUsers').val();
            obj.locationIds = $('#salesSummeryLocations').val();
            obj.printView = 0;
            
            if (posSalesSummeryInputValidation(obj)) {
                var url = BASE_URL + '/api/pos-report/pos-sales-summery-report';
                eb.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: obj,
                    success: function(respond) {
                        if (respond.status == true) {
                            setContentToReport('posReport', respond);
                        }
                    }
                });
            }
        } else {
            p_notification(false, eb.getMessage('ERR_POSRE_CORRTYPE'));
        }
    });

    $('#generatePdf').click(function(e) {
        var fromDate = $('#from-Date').val();
        var toDate = $('#to-Date').val();
        var obj = {};
        obj.fromDate = $('#from-Date').val();
        obj.toDate = $('#to-Date').val();
        var userIds = $('#user').val();
        var locIds = $('#location').val();
        var paymentMethodsIds = $('#paymentMethod').val();
        unSetMultiSelect(userIds, locIds, paymentMethodsIds);
        if (reportType == 'posInvoice') {
            if (posSalesSummeryInputValidation(obj)) {
                window.open(BASE_URL + "/api/pos-report/generate-pos-invoice-pdf?fromDate=" + fromDate + "&toDate=" + toDate + "&userIds=" + userIds + "&locIds=" + locIds, '_blank');
            }
        } else if (reportType == 'userWiseBalanceSummary') {
            if (posSalesSummeryInputValidation(obj)) {
                window.open(BASE_URL + "/api/pos-report/generate-pos-balance-summary-pdf?fromDate=" + fromDate + "&toDate=" + toDate + "&userIds=" + userIds + "&locIds=" + locIds, '_blank');
            }
        } else if (reportType == 'posPayment') {
            if (posSalesSummeryInputValidation(obj)) {
                window.open(BASE_URL + "/api/pos-report/generate-pos-payment-pdf?fromDate=" + fromDate + "&toDate=" + toDate + "&paymentMethodsIds=" + paymentMethodsIds + "&userIds=" + userIds +"&locIds=" + locIds, '_blank');
            }
        } else if (reportType == 'posItemDiscount') {
            if (posSalesSummeryInputValidation(obj)) {
                window.open(BASE_URL + "/api/pos-report/generate-pos-item-discount-pdf?fromDate=" + fromDate + "&toDate=" + toDate + "&userIds=" + userIds + "&locIds=" + locIds, '_blank');
            }
        } else if (reportType == 'posSalesSummery') {
            var obj = {};
            obj.fromDate = $('#salesSummeryFromDate').val();
            obj.toDate = $('#salesSummeryToDate').val();
            obj.userIds = $('#salesSummeryUsers').val();
            obj.locationIds = $('#salesSummeryLocations').val();
            obj.printView = 0;
            
            if (posSalesSummeryInputValidation(obj)) {
                eb.ajax({
                    type: 'POST',
                    url: BASE_URL + '/api/pos-report/pos-sales-summery-pdf',
                    dataType: 'json',
                    data: obj,
                     success: function(respond) {
                        if (respond.status) {
                            window.open(BASE_URL + '/' + respond.data, '_blank');
                        }
                    }
                });
            }            
        } else {
            p_notification(false, eb.getMessage('ERR_POSRE_CORRTYPE'));
        }
    });

    $('#csvReport').click(function(e) {
        var fromDate = $('#from-Date').val();
        var toDate = $('#to-Date').val();
        var obj = {};
        obj.fromDate = $('#from-Date').val();
        obj.toDate = $('#to-Date').val();
        var userIds = $('#user').val();
        var locIds = $('#location').val();
        var paymentMethodsIds = $('#paymentMethod').val();
        unSetMultiSelect(userIds, locIds, paymentMethodsIds);
        if (reportType == 'posInvoice') {
            if (posSalesSummeryInputValidation(obj)) {
                window.open(BASE_URL + "/api/pos-report/generate-pos-invoice-sheet?fromDate=" + fromDate + "&toDate=" + toDate + "&userIds=" + userIds + "&locIds=" + locIds, '_blank');
            }
        } else if (reportType == 'userWiseBalanceSummary') {
            if (posSalesSummeryInputValidation(obj)) {
                window.open(BASE_URL + "/api/pos-report/generate-pos-balance-summary-sheet?fromDate=" + fromDate + "&toDate=" + toDate + "&userIds=" + userIds + "&locIds=" + locIds, '_blank');
            }
        } else if (reportType == 'posPayment') {
            if (posSalesSummeryInputValidation(obj)) {
                window.open(BASE_URL + "/api/pos-report/generate-pos-payment-sheet?fromDate=" + fromDate + "&toDate=" + toDate + "&paymentMethodsIds=" + paymentMethodsIds + "&userIds=" + userIds + "&locIds=" + locIds, '_blank');
            }
        } else if (reportType == 'posItemDiscount') {
            if (posSalesSummeryInputValidation(obj)) {
                window.open(BASE_URL + "/api/pos-report/generate-pos-item-discount-csv?fromDate=" + fromDate + "&toDate=" + toDate + "&userIds=" + userIds + "&locIds=" + locIds, '_blank');
            }
        } else if (reportType == 'posSalesSummery') {
            var obj = {};
            obj.fromDate = $('#salesSummeryFromDate').val();
            obj.toDate = $('#salesSummeryToDate').val();
            obj.userIds = $('#salesSummeryUsers').val();
            obj.locationIds = $('#salesSummeryLocations').val();
            
            if (posSalesSummeryInputValidation(obj)) {
                eb.ajax({
                    type: 'POST',
                    url: BASE_URL + '/api/pos-report/pos-sales-summery-csv',
                    dataType: 'json',
                    data: obj,
                     success: function(respond) {
                        if (respond.status) {
                            window.open(BASE_URL + '/' + respond.data, '_blank');
                        }
                    }
                });
            }            
        } else {
            p_notification(false, eb.getMessage('ERR_POSRE_CORRTYPE'));
        }
    });
    
    $('#posPrint').on('click', function() {
        var obj = {};
        obj.fromDate = $('#salesSummeryFromDate').val();
        obj.toDate = $('#salesSummeryToDate').val();
        obj.userIds = $('#salesSummeryUsers').val();
        obj.locationIds = $('#salesSummeryLocations').val();
        obj.printView = 1;
        
        if (posSalesSummeryInputValidation(obj)) {
            eb.ajax({
                type: 'POST',
                url: BASE_URL + '/api/pos-report/pos-sales-summery-report',
                dataType: 'json',
                data: obj,
                 success: function(respond) {
                    if (respond.status) {
                        setContentToReport('documentpreview', respond);
                        $('#preview').modal('show');
                    }
                }
            });
        }
    });
    
    $('#cancel').on('click', function() {
        $('#preview').modal('hide');
    });
    
    $('#document_print').on('click', function() {
        printIframe('documentpreview');
    });

    function initialCondition() {
        $('#posReport').hide();
        $('#date').show();
        $('.payment-method').hide();
    }

    function hidePOSReportIFrame() {
        $('#posReport').contents().find('body').html('');
        $('#posReport').hide();
        $('#date').val('');
        if ($('#user').val()) {
            $('#user').selectpicker('deselectAll');
        }
        if ($('#location').val()) {
            $('#location').selectpicker('deselectAll');
        }
    }

    function autoResizeIframe() {
        var iFrameID = document.getElementById('monthlySalesReport');
        if (iFrameID) {
            // here you can make the height, I delete it first, then I make it again
            iFrameID.height = "";
            iFrameID.height = iFrameID.contentWindow.document.body.scrollHeight + "px";
        }
    }

    //unset multiselect elemet from userIds, locIds and paymentMethodsIds arrays
    function unSetMultiSelect(userIds, locIds, paymentMethodsIds) {
        if (userIds) {
            removeMultiSelect(userIds);

        }
        if (locIds) {
            removeMultiSelect(locIds);

        }
        if (paymentMethodsIds) {
            removeMultiSelect(paymentMethodsIds);

        }
    }

    function deSelectPicker() {
        var userIds = $('#user').val();
        var locIds = $('#location').val();
        var paymentMethodsIds = $('#paymentMethod').val();
        if (userIds) {
            $('#user').selectpicker('deselectAll');
        }
        if (locIds) {
            $('#location').selectpicker('deselectAll');
        }
        if (paymentMethodsIds) {
            $('#paymentMethod').selectpicker('deselectAll');
        }
    }
    //responsive issue fix
    $(window).bind('resize ready', function() {
        if ($(window).width() <= 480) {
            $('.report_from_month').css('margin-bottom', '15px').removeClass('col-xs-4').addClass('col-xs-12');
            $('.set_to_date').css('margin-bottom', '15px').removeClass('col-xs-4').addClass('col-xs-12');
            $('.btn_panel_height_high').removeClass('btn-group');
            $('#view_report, #view_graph, #generate_pdf, #excel_report').removeClass('btn-default').addClass('col-xs-12 btn-primary').css('margin-bottom', '8px');
            $('#btnMonthly, #btnDaily, #btn_annual').css('margin-bottom', '15px');
        } else {
            $('.report_from_month').css('margin-bottom', '0').removeClass('col-xs-12').addClass('col-xs-4');
            $('.set_to_date').css('margin-bottom', '0').removeClass('col-xs-12').addClass('col-xs-4');
            $('.btn_panel_height_high').addClass('btn-group');
            $('#view_report, #view_graph, #generate_pdf, #excel_report').addClass('btn-default').removeClass('col-xs-12 btn-primary').css('margin-bottom', '0px');
            $('#btnMonthly, #btnDaily, #btn_annual').css('margin-bottom', '0');
        }
    });
    
    function posSalesSummeryInputValidation(input) {        
        if (!input.fromDate) {
            p_notification(false, eb.getMessage('ERR_POS_SALES_SUMMERY_FROM_DATE'));
            return false;
        } else if (!input.toDate) {
            p_notification(false, eb.getMessage('ERR_POS_SALES_SUMMERY_TO_DATE'));
            return false;
        } else if (input.fromDate > input.toDate) {
            p_notification(false, eb.getMessage('ERR_POS_SALES_SUMMERY_DATE_RANGE'));
            return false;
        } else {
            return true;
        }
    }

});
