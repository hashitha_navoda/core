$(document).ready(function() {

    var reportType = 'invoicedSalesItems';
    var isAllInvoices = true;
    var isAllProducts = true;
    var isAllSerials = true;
    var isAllLocation = true;
    var isAllProductsWSales = true;
    var isAllItems = false;
    var isAllRepresentative = false;
    var isAllCategory = false;
    var isAllProductsForForecast = 0;

    //$('#AllInvoices').prop("checked", true);

    ///////DatePicker for two text Filed\\\\\\\
    var checkin = $('#fromDate').datepicker({
    }).on('changeDate', function(ev) {
        if (ev.date.valueOf() < checkout.date.valueOf()) {
            var currentDate = new Date(ev.date);
            var newDate = new Date();
            newDate.setDate(currentDate.getDate() + 1);
            checkout.setValue(newDate);
        }
        checkin.hide();
        $('#toDate')[0].focus();
    }).data('datepicker');

    var checkout = $('#toDate').datepicker({
        onRender: function(date) {
            return date.valueOf() < checkin.date.valueOf() ? 'disabled' : '';
        }
    }).on('changeDate', function(ev) {
        checkout.hide();
    }).data('datepicker');
    
    $('#locations').prop('disabled', true).selectpicker('refresh');
    $('#productWSales').prop('disabled', true).selectpicker('refresh');
    $('#bomProducts').prop('disabled', true).selectpicker('refresh');
    $('#forecastDate').datepicker();

    $('#invoicedSalesItems').click(function() {
        reportType = 'invoicedSalesItems';
        $('.location,#div_product_wsales,#dailySalesReport, #bom-item-pannel,#div_invoice,#div_product,#div_serial,#representative_div,#item_div,#category_div,#demandForecastDiv').addClass('hidden');
        $('#div_period, #batchSerialDiv').removeClass('hidden');
        reSetDateFields();
    });

    $('#invoicedNoneInventorySalesItems').click(function() {
        reportType = 'invoicedNoneInventorySalesItems';
        $('.location,#div_product_wsales,#batchSerialDiv, #bom-item-pannel, #dailySalesReport,#div_invoice,#div_product,#div_serial,#representative_div,#item_div,#category_div,#demandForecastDiv').addClass('hidden');
        $('#div_period').removeClass('hidden');
        reSetDateFields();
    });

    $('#deliveryNoteItems').click(function() {
        reportType = 'deliveryNote';
        $('.location,#div_product_wsales,#batchSerialDiv, #bom-item-pannel, #dailySalesReport,#div_invoice,#div_product,#div_serial,#representative_div,#item_div,#category_div,#demandForecastDiv').addClass('hidden');
        $('#div_period').removeClass('hidden');
        reSetDateFields();
    });

    $('#invoice-DeliveryNote').click(function() {
        reportType = 'inv-d_note-compare';
        $('.location,#div_product_wsales,#batchSerialDiv, #bom-item-pannel, #dailySalesReport,#div_invoice,#div_product,#div_serial,#representative_div,#item_div,#category_div,#demandForecastDiv').addClass('hidden');
        $('#div_period').removeClass('hidden');
        reSetDateFields();
    });
    $('#posSalesItems').click(function() {
        reportType = 'posSalesItems';
        $('.location,#div_product_wsales,#batchSerialDiv, #bom-item-pannel, #dailySalesReport,#div_invoice,#div_product,#div_serial,#representative_div,#item_div,#category_div,#demandForecastDiv').addClass('hidden');
        $('#div_period').removeClass('hidden');
        reSetDateFields();
    });
    $('#pos-invoiceSalesItems').click(function() {
        reportType = 'pos-invoiceSalesItems';
        $('.location,#div_product_wsales,#batchSerialDiv, #bom-item-pannel, #dailySalesReport,#div_invoice,#div_product,#div_serial,#representative_div,#item_div,#category_div,#demandForecastDiv').addClass('hidden');
        $('#div_period').removeClass('hidden');
        reSetDateFields();
    });
    $('#serialItemMovement').click(function() {
        reportType = 'serialItemMovement';
        $('.location,#div_product_wsales,#batchSerialDiv, #bom-item-pannel, #dailySalesReport,#div_period,#representative_div,#item_div,#category_div,#demandForecastDiv').addClass('hidden');
        $('#div_invoice,#div_product,#div_serial').removeClass('hidden');
        reSetDateFields();
    });
    $('#invoicedBatchSerial').click(function() {
        reportType = 'invoicedBatchSerial';
        $('.location,#div_product_wsales,#batchSerialDiv, #bom-item-pannel, #div_serial,#dailySalesReport,#div_period,#representative_div,#item_div,#category_div,#demandForecastDiv').addClass('hidden');
        $('#div_invoice,#div_product_batchSerial').removeClass('hidden');
        reSetDateFields();
    });
    $('#sales-rep-wise-category').click(function() {
        reportType = 'salesRepWiseCategory';
        $('#representative_div,#category_div,#batchSerialDiv, #bom-item-pannel, #representative_div,#div_period').removeClass('hidden');
        $('.location,#div_product_wsales,#dailySalesReport,#item_div,#div_invoice,#div_product,#div_serial,#demandForecastDiv').addClass('hidden');
        reSetDateFields();
    });
    $('#sales-rep-wise-item').click(function() {
        reportType = 'salesRepWiseItem';
        $('#representative_div,#item_div,#batchSerialDiv, #bom-item-pannel, #representative_div,#item_div,#category_div,#div_period').removeClass('hidden');
        $('.location,#div_product_wsales,#dailySalesReport,#div_invoice,#div_product,#div_serial,#demandForecastDiv').addClass('hidden');
        reSetDateFields();
    });
    $('#sales-rep-wise-item-with-invoices').click(function() {
        reportType = 'salesRepWiseItemWithInvoices';
        $('#representative_div,#item_div,#batchSerialDiv, #bom-item-pannel, #representative_div,#item_div,#div_period').removeClass('hidden');
        $('.location,#div_product_wsales,#dailySalesReport,#category_div,#div_invoice,#div_product,#div_serial,#demandForecastDiv').addClass('hidden');
        reSetDateFields();
    });
    $("#freeIssueItems").click(function() {
        reportType = 'freeIssueItems';
        $('.location,#div_product_wsales,#batchSerialDiv, #bom-item-pannel, #dailySalesReport,#div_invoice,#div_product,#div_serial,#representative_div,#item_div,#category_div,#demandForecastDiv').addClass('hidden');
        $('#div_period').removeClass('hidden');
        reSetDateFields();
    });
    $('#serialItemDetail').click(function() {
        reportType = 'serialItemDetail';
        $('.location,#div_product_wsales,#batchSerialDiv, #bom-item-pannel, #dailySalesReport,#div_period,#representative_div,#item_div,#category_div,#div_invoice,#demandForecastDiv').addClass('hidden');
        $('#div_product,#div_serial').removeClass('hidden');
        reSetDateFields();
    });
    $("#demandForecast").click(function() {
        reportType = 'demandForecast';
        $('.location,#div_product_wsales,#batchSerialDiv, #bom-item-pannel, #dailySalesReport,#div_invoice,#div_product,#div_serial,#representative_div,#item_div,#div_period').addClass('hidden');
        $('#demandForecastDiv').removeClass('hidden');
        $('#category_div').removeClass('hidden');
        reSetDateFields();
    });
    $("#itemWiseSales").click(function() {
        reportType = 'itemWiseSales';
        $('#dailySalesReport,#div_product_wsales, #bom-item-pannel,#batchSerialDiv, #div_invoice,#div_product,#div_serial,#representative_div,#item_div,#category_div,#div_period,#demandForecastDiv').addClass('hidden');
        $('#div_period,#div_product_wsales,.location').removeClass('hidden');
        reSetDateFields();
    });
    $("#invoicedSalesItemsWithCost").click(function() {
        reportType = 'invoicedSalesItemsWithCost';
        $('.location,#div_product_wsales, #bom-item-pannel, #batchSerialDiv, #dailySalesReport,#div_invoice,#div_product,#div_serial,#representative_div,#item_div,#demandForecastDiv').addClass('hidden');
        $('#div_period').removeClass('hidden');
        $('#category_div').removeClass('hidden');
        reSetDateFields();
    });
    $("#invoicedSubItemBasedBom").click(function() {
        reportType = 'invoicedSubItemBasedBom';
        $('.location,#div_product_wsales, #div_product_batchSerial,  #batchSerialDiv, #dailySalesReport,#div_invoice,#div_product,#div_serial,#representative_div,#item_div,#category_div,#demandForecastDiv').addClass('hidden');
        $('#div_period, #bom-item-pannel').removeClass('hidden');
        $('.location').removeClass('hidden');
        reSetDateFields();
    });


    loadDropDownFromDatabase('/invoice-api/search-all-sales-invoices-for-dropdown', "", 0, '#invoiceName', "", true);
    loadDropDownFromDatabase('/productAPI/search-serial-products-for-dropdown', "", 0, '#productName', "", true);
    loadDropDownFromDatabase('/productAPI/search-batch-serial-products-for-dropdown', "", 0, '#batchSerialproductName', "", true);
    loadDropDownFromDatabase('/productAPI/search-product-serials-for-dropdown', "", 0, '#serialName', "", true);
    loadDropDownFromDatabase('/salesPerson/search-sales-person-for-dropdown', "", 0, '#representativeList', "", true);
    loadDropDownFromDatabase('/productAPI/search-user-active-location-products-for-dropdown', "", 0, '#itemList', "", true);
    loadDropDownFromDatabase('/api/company/category/search-categories-for-dropdown', "", 0, '#categoryList', "", true);
    loadDropDownFromDatabase('/productAPI/search-user-active-location-products-for-dropdown', "", 0, '#forecastProductSelect', "", true);
    loadDropDownFromDatabase('/productAPI/search-user-active-location-products-for-dropdown', "", 0, '#productWSales', "", true);
    loadDropDownFromDatabase('/bom-api/search-sub-item-base-bom-items-for-dropdown', "", 0, '#bomProducts', "", true);

    $('#viewReport').click(function() {
        if (reportType === 'invoicedSalesItems') {
            var getpdfdataurl = BASE_URL + '/reports/daily-sales-items-report/view-invoiced-daily-sales-item';

            inputs = new Array(
                    $('#fromDate').val(),
                    $('#toDate').val());

            if (validteinput(inputs)) {
                var getpdfdatarequest = eb.post(getpdfdataurl, {
                    fromDate: $('#fromDate').val(),
                    toDate: $('#toDate').val(),
                    batchSerial: $('#batchSerilPreview').is(":checked")
                });
                getpdfdatarequest.done(function(respond) {
                    if (respond.status == true) {
                        $('#dailySalesReport').removeClass('hidden');
                        setContentToReport('dailySalesReport', respond);
                    }
                });
            }
        }
        else if (reportType === 'invoicedSalesItemsWithCost') {
            var getpdfdataurl = BASE_URL + '/reports/daily-sales-items-report/view-invoiced-daily-sales-item-with-cost';

            inputs = new Array(
                    $('#fromDate').val(),
                    $('#toDate').val());

            var categoryIds = $('#categoryList').val();
            var isAllCategories = $('input[name="isAllCategories"]:checked').val();
            if (validteinput(inputs)) {
                var getpdfdatarequest = eb.post(getpdfdataurl, {
                    fromDate: $('#fromDate').val(),
                    toDate: $('#toDate').val(),
                    categoryIds: categoryIds,
                    isAllCategories: isAllCategories,

                });
                getpdfdatarequest.done(function(respond) {
                    if (respond.status == true) {
                        $('#dailySalesReport').removeClass('hidden');
                        setContentToReport('dailySalesReport', respond);
                    }
                });
            }
        }
        else if (reportType === 'invoicedNoneInventorySalesItems') {
            var getpdfdataurl = BASE_URL + '/reports/daily-sales-items-report/view-invoiced-daily-sales-none-inventory-item';

            inputs = new Array(
                    $('#fromDate').val(),
                    $('#toDate').val());

            if (validteinput(inputs)) {
                var getpdfdatarequest = eb.post(getpdfdataurl, {
                    fromDate: $('#fromDate').val(),
                    toDate: $('#toDate').val(),
                });
                getpdfdatarequest.done(function(respond) {
                    if (respond.status == true) {
                        $('#dailySalesReport').removeClass('hidden');
                        setContentToReport('dailySalesReport', respond);
                    }
                });
            }
        }
        else if (reportType === 'posSalesItems') {
            var getpdfdataurl = BASE_URL + '/reports/daily-sales-items-report/view-pos-daily-sales-items';

            inputs = new Array(
                    $('#fromDate').val(),
                    $('#toDate').val());

            if (validteinput(inputs)) {
                var getpdfdatarequest = eb.post(getpdfdataurl, {
                    fromDate: $('#fromDate').val(),
                    toDate: $('#toDate').val(),
                });
                getpdfdatarequest.done(function(respond) {
                    if (respond.status == true) {
                        $('#dailySalesReport').removeClass('hidden');
                        setContentToReport('dailySalesReport', respond);
                    }
                });
            }
        }
        else if (reportType === 'pos-invoiceSalesItems') {
            var getpdfdataurl = BASE_URL + '/reports/daily-sales-items-report/view-pos-and-invoiced-daily-sales-items';

            inputs = new Array(
                    $('#fromDate').val(),
                    $('#toDate').val());

            if (validteinput(inputs)) {
                var getpdfdatarequest = eb.post(getpdfdataurl, {
                    fromDate: $('#fromDate').val(),
                    toDate: $('#toDate').val(),
                });
                getpdfdatarequest.done(function(respond) {
                    if (respond.status == true) {
                        $('#dailySalesReport').removeClass('hidden');
                        setContentToReport('dailySalesReport', respond);
                    }
                });
            }
        }
        else if (reportType === 'serialItemMovement') {
            $('#dailySalesReport').addClass('hidden');
            var getpdfdataurl = BASE_URL + '/reports/daily-sales-items-report/generate-serial-item-movement-report';
            var invoiceIds = $('#invoiceName').val();
            var productIds = $('#productName').val();
            var serialIds = $('#serialName').val();

            if (isAllInvoices != true && (invoiceIds == null || invoiceIds == "")) {
                p_notification(false, 'Please select at least one Invoice.');
            } else if (isAllProducts != true && (productIds == null || productIds == "")) {
                p_notification(false, 'Please select at least one Product.');
            } else if (isAllSerials != true && (serialIds == null || serialIds == "")) {
                p_notification(false, 'Please select at least one Serial Number.');
            } else {
                var getpdfdatarequest = eb.post(getpdfdataurl, {
                    isAllInvoices: isAllInvoices,
                    invoiceIds: $('#invoiceName').val(),
                    isAllProducts: isAllProducts,
                    productIds: $('#productName').val(),
                    isAllSerials: isAllSerials,
                    serialIds: $('#serialName').val()
                });

                getpdfdatarequest.done(function(respond) {
                    if (respond.status == true) {
                        $('#dailySalesReport').removeClass('hidden');
                        setContentToReport('dailySalesReport', respond);
                    }
                });
            }
        } 
        else if (reportType === 'invoicedBatchSerial') {
            $('#dailySalesReport').addClass('hidden');
            var getpdfdataurl = BASE_URL + '/reports/daily-sales-items-report/generate-invoiced-batch-serial-item-details-report';
            var invoiceIds = $('#invoiceName').val();
            var productIds = $('#batchSerialproductName').val();

            if (isAllInvoices != true && (invoiceIds == null || invoiceIds == "")) {
                p_notification(false, 'Please select at least one Invoice.');
            } else if (isAllProducts != true && (productIds == null || productIds == "")) {
                p_notification(false, 'Please select at least one Product.');
            } else {
                var getpdfdatarequest = eb.post(getpdfdataurl, {
                    isAllInvoices: isAllInvoices,
                    invoiceIds: $('#invoiceName').val(),
                    isAllProducts: isAllProducts,
                    productIds: $('#batchSerialproductName').val()
                });

                getpdfdatarequest.done(function(respond) {
                    if (respond.status == true) {
                        $('#dailySalesReport').removeClass('hidden');
                        setContentToReport('dailySalesReport', respond);
                    }
                });
            }
        } 
        else if (reportType === 'serialItemDetail') {
            $('#dailySalesReport').addClass('hidden');
            var getpdfdataurl = BASE_URL + '/reports/daily-sales-items-report/generate-serial-item-detail-report';
            var productIds = $('#productName').val();
            var serialIds  = $('#serialName').val();

            if (isAllProducts != true && (productIds == null || productIds == "")) {
                p_notification(false, 'Please select at least one Product.');
            } else if (isAllSerials != true && (serialIds == null || serialIds == "")) {
                p_notification(false, 'Please select at least one Serial Number.');
            } else {
                var getpdfdatarequest = eb.post(getpdfdataurl, {
                    isAllProducts: isAllProducts,
                    productIds: $('#productName').val(),
                    isAllSerials: isAllSerials,
                    serialIds: $('#serialName').val()
                });

                getpdfdatarequest.done(function(respond) {
                    if (respond.status == true) {
                        $('#dailySalesReport').removeClass('hidden');
                        setContentToReport('dailySalesReport', respond);
                    }
                });
            }
        }
        else if (reportType === 'salesRepWiseItem') {
            var getpdfdataurl = BASE_URL + '/reports/daily-sales-items-report/generate-representative-wise-item-report';

            inputs = new Array(
                    $('#fromDate').val(),
                    $('#toDate').val()
                    );

            var isAllRepresentative = $('input[name="isAllRepresentative"]:checked').val();
            var isAllItems = $('input[name="isAllItems"]:checked').val();
            var representativeIds = $('#representativeList').val();
            var itemIds = $('#itemList').val();
            var categoryIds = $('#categoryList').val();

            if (validteinput(inputs)) {
                if (isAllRepresentative != true && (representativeIds == null || representativeIds == "")) {
                    p_notification(false, 'Please select at least one representative.');
                } else if (isAllItems != true && (itemIds == null || itemIds == "")) {
                    p_notification(false, 'Please select at least one product.');
                } else {
                    var getpdfdatarequest = eb.post(getpdfdataurl, {
                        fromDate: $('#fromDate').val(),
                        toDate: $('#toDate').val(),
                        isAllRepresentative: isAllRepresentative,
                        representativeIds: representativeIds,
                        isAllItems: isAllItems,
                        itemIds: itemIds,
                        itemCategory : categoryIds
                    });
                    getpdfdatarequest.done(function(respond) {
                        if (respond.status == true) {
                            $('#dailySalesReport').removeClass('hidden');
                            setContentToReport('dailySalesReport', respond);
                        }
                    });
                }
            }
        } else if (reportType === 'salesRepWiseCategory') {
            var getpdfdataurl = BASE_URL + '/reports/daily-sales-items-report/generate-representative-wise-category-report';

            inputs = new Array(
                    $('#fromDate').val(),
                    $('#toDate').val()
                    );

            var isAllRepresentative = $('input[name="isAllRepresentative"]:checked').val();
            var isAllCategories = $('input[name="isAllCategories"]:checked').val();
            var representativeIds = $('#representativeList').val();
            var categoryIds = $('#categoryList').val();

            if (validteinput(inputs)) {
                if (isAllRepresentative != true && (representativeIds == null || representativeIds == "")) {
                    p_notification(false, 'Please select at least one representative.');
                } else if (isAllCategories != true && (categoryIds == null || categoryIds == "")) {
                    p_notification(false, 'Please select at least one category.');
                } else {
                    var getpdfdatarequest = eb.post(getpdfdataurl, {
                        fromDate: $('#fromDate').val(),
                        toDate: $('#toDate').val(),
                        isAllRepresentative: isAllRepresentative,
                        representativeIds: representativeIds,
                        isAllCategories: isAllCategories,
                        categoryIds: categoryIds
                    });
                    getpdfdatarequest.done(function(respond) {
                        if (respond.status == true) {
                            $('#dailySalesReport').removeClass('hidden');
                            setContentToReport('dailySalesReport', respond);
                        }
                    });
                }
            }
        } else if (reportType === 'freeIssueItems') {
            var getpdfdataurl = BASE_URL + '/reports/daily-sales-items-report/view-free-issued-item';

            inputs = new Array(
                    $('#fromDate').val(),
                    $('#toDate').val());

            if (validteinput(inputs)) {
                var getpdfdatarequest = eb.post(getpdfdataurl, {
                    fromDate: $('#fromDate').val(),
                    toDate: $('#toDate').val(),
                });
                getpdfdatarequest.done(function(respond) {
                    if (respond.status == true) {
                        $('#dailySalesReport').removeClass('hidden');
                        setContentToReport('dailySalesReport', respond);
                    }
                });
            }
        } else if(reportType === 'demandForecast') {
            var getpdfdataurl = BASE_URL + '/reports/daily-sales-items-report/generate-demand-forecast-report';

            var obj = {};
            obj.date = $('#forecastDate').val();
            obj.productIds = $('#forecastProductSelect').val();
            obj.isAllProducts = isAllProductsForForecast;
            obj.locationIds = $('#forecastLocationSelect').val();
            obj.isAllCategories = $('input[name="isAllCategories"]:checked').val();
            obj.categoryIds = $('#categoryList').val();

            if(demandForecastVaidation(obj)){
                var getpdfdatarequest = eb.post(getpdfdataurl, obj);
                getpdfdatarequest.done(function(respond) {
                    if (respond.status == true) {
                        $('#dailySalesReport').removeClass('hidden');
                        setContentToReport('dailySalesReport', respond);
                    }
                });
            }
        } else if(reportType === 'itemWiseSales') {
            products = $('#productWSales').val();
            locations = $('#locations').val();
            console.log(isAllLocation);
            var fromDate = $('#fromDate').val();
            var endDate = $('#toDate').val();
            if (isAllProductsWSales != true && (products == '' || products == null)) {
                p_notification(false, eb.getMessage('ERR_DAISALE_ITEMWISETBL_PRODUCT_ERR'));
            } else if(isAllLocation != true && (locations == '' || locations == null)){
                p_notification(false, eb.getMessage('ERR_CUSREPO_SELELOCATION'));
            } else if((fromDate != null && fromDate != null) && (endDate < fromDate)){
                p_notification(false, eb.getMessage('ERR_CUSREPO_FROMDATE_CANT_BE_MORETHAN_ENDDATE'));
            } else {
                var url = BASE_URL + '/reports/daily-sales-items-report/view-item-wise-sales-report';
                eb.ajax({
                    type: 'POST',
                    url: url,
                    data: {
                        products: products,
                        isAllProducts: isAllProductsWSales,
                        locations: locations,
                        isAllLocation: isAllLocation,
                        fromDate: fromDate,
                        endDate: endDate,
                    },
                    success: function(respond) {
                         $('#dailySalesReport').removeClass('hidden');
                        setContentToReport('dailySalesReport', respond);
                    },
                    async: false
                });
            }
        } else if (reportType === 'salesRepWiseItemWithInvoices') {
            var getpdfdataurl = BASE_URL + '/reports/daily-sales-items-report/generate-representative-wise-item-with-invoice-report';

            input = new Array(
                $('#fromDate').val(),
                $('#toDate').val()
            );

            var isAllRepresentative = $('input[name="isAllRepresentative"]:checked').val();
            var isAllItems = $('input[name="isAllItems"]:checked').val();
            var representativeIds = $('#representativeList').val();
            var itemIds = $('#itemList').val();

            if (validteinput(input)) {
                if (isAllRepresentative != true && (representativeIds == null || representativeIds == "")) {
                    p_notification(false, 'Please select at least one representative.');
                } else if (isAllItems != true && (itemIds == null || itemIds == "")) {
                    p_notification(false, 'Please select at least one product.');
                } else {
                    var getpdfdatarequest = eb.post(getpdfdataurl, {
                        fromDate: $('#fromDate').val(),
                        toDate: $('#toDate').val(),
                        isAllRepresentative: isAllRepresentative,
                        representativeIds: representativeIds,
                        isAllItems: isAllItems,
                        itemIds: itemIds
                    });
                    getpdfdatarequest.done(function (respond) {
                        if (respond.status == true) {
                            $('#dailySalesReport').removeClass('hidden');
                            setContentToReport('dailySalesReport', respond);
                        }
                    });
                }
            }
        } else if (reportType === 'invoicedSubItemBasedBom') {
            var getpdfdataurl = BASE_URL + '/reports/daily-sales-items-report/generate-invoiced-sub-item-based-bom-product-report';

            input = new Array(
                $('#fromDate').val(),
                $('#toDate').val()
            );

            var isAllItems = $('input[name="isAllBomProducts"]:checked').val();
            var representativeIds = $('#representativeList').val();
            var bomIds = $('#bomProducts').val();
            locations = $('#locations').val();
            console.log(isAllLocation);

            if (validteinput(input)) {
                if (isAllItems != true && (bomIds == null || bomIds == "")) {
                    p_notification(false, 'Please select at least one product.');
                }else if(isAllLocation != true && (locations == '' || locations == null)){
                    p_notification(false, eb.getMessage('ERR_CUSREPO_SELELOCATION'));
                } else {
                    var getpdfdatarequest = eb.post(getpdfdataurl, {
                        fromDate: $('#fromDate').val(),
                        toDate: $('#toDate').val(),
                        isAllItems: isAllItems,
                        locations: locations,
                        isAllLocation: isAllLocation,
                        bomIds: bomIds
                    });
                    getpdfdatarequest.done(function (respond) {
                        if (respond.status == true) {
                            $('#dailySalesReport').removeClass('hidden');
                            setContentToReport('dailySalesReport', respond);
                        }
                    });
                }
            }
        } 
    });

    $('#generatePdf').click(function() {
        if (reportType === 'invoicedSalesItems') {
            var fromDate = $('#fromDate').val();
            var toDate = $('#toDate').val();
            var batchSerial = $('#batchSerilPreview').is(":checked");
            inputs = new Array(
                    fromDate,
                    toDate,
                    batchSerial
                    );

            if (validteinput(inputs)) {
                window.open(BASE_URL + "/reports/daily-sales-items-report/generate-invoiced-daily-sales-items-pdf?fromDate=" + fromDate + "&toDate=" + toDate + "&batchSerial=" + batchSerial, '_blank');
            }
        }
        else if (reportType === 'invoicedSalesItemsWithCost') {
            var fromDate = $('#fromDate').val();
            var toDate = $('#toDate').val();
            inputs = new Array(
                    fromDate,
                    toDate
                    );
            var categoryIds = $('#categoryList').val();
            var isAllCategories = $('input[name="isAllCategories"]:checked').val();
            getpdfdataurl = BASE_URL + "/reports/daily-sales-items-report/generate-invoiced-daily-sales-items-with-cost-pdf";


            if (validteinput(inputs)) {

                eb.ajax({
                    type: 'POST',
                    url: getpdfdataurl,
                    dataType: 'json',
                    data: {
                        fromDate: fromDate,
                        toDate: toDate,
                        categoryIds: categoryIds,
                        isAllCategories: isAllCategories, 
                    },
                    success: function(respond) {
                        if (respond.status == true) {
                            window.open(BASE_URL + '/' + respond.data, '_blank');
                        }
                    }
                });




                // window.open(BASE_URL + "/reports/daily-sales-items-report/generate-invoiced-daily-sales-items-with-cost-pdf?fromDate=" + fromDate + "&toDate=" + toDate, '_blank');
            }
        }
        else if (reportType === 'invoicedNoneInventorySalesItems') {
            var fromDate = $('#fromDate').val();
            var toDate = $('#toDate').val();
            inputs = new Array(
                    fromDate,
                    toDate
                    );

            if (validteinput(inputs)) {
                window.open(BASE_URL + "/reports/daily-sales-items-report/generate-invoiced-daily-sales-none-inventory-item-pdf?fromDate=" + fromDate + "&toDate=" + toDate, '_blank');
            }
        }
        if (reportType === 'posSalesItems') {
            var fromDate = $('#fromDate').val();
            var toDate = $('#toDate').val();
            inputs = new Array(
                    fromDate,
                    toDate
                    );

            if (validteinput(inputs)) {
                window.open(BASE_URL + "/reports/daily-sales-items-report/generate-pos-daily-sales-items-pdf?fromDate=" + fromDate + "&toDate=" + toDate, '_blank');
            }
        } else if (reportType === 'pos-invoiceSalesItems') {
            var fromDate = $('#fromDate').val();
            var toDate = $('#toDate').val();
            inputs = new Array(
                    fromDate,
                    toDate
                    );

            if (validteinput(inputs)) {
                window.open(BASE_URL + "/reports/daily-sales-items-report/generate-pos-and-invoice-daily-sales-items-pdf?fromDate=" + fromDate + "&toDate=" + toDate, '_blank');
            }
        } else if (reportType === 'serialItemMovement') {
            var getpdfdataurl = BASE_URL + '/reports/daily-sales-items-report/generate-serial-item-movement-pdf';
            var invoiceIds = $('#invoiceName').val();
            var productIds = $('#productName').val();
            var serialIds = $('#serialName').val();

            if (isAllInvoices != true && (invoiceIds == null || invoiceIds == "")) {
                p_notification(false, 'Please select at least one Invoice.');
            } else if (isAllProducts != true && (productIds == null || productIds == "")) {
                p_notification(false, 'Please select at least one Product.');
            } else if (isAllSerials != true && (serialIds == null || serialIds == "")) {
                p_notification(false, 'Please select at least one Serial Number.');
            } else {
                eb.ajax({
                    type: 'POST',
                    url: getpdfdataurl,
                    dataType: 'json',
                    data: {
                        isAllInvoices: isAllInvoices,
                        invoiceIds: $('#invoiceName').val(),
                        isAllProducts: isAllProducts,
                        productIds: $('#productName').val(),
                        isAllSerials: isAllSerials,
                        serialIds: $('#serialName').val()
                    },
                    success: function(respond) {
                        if (respond.status == true) {
                            window.open(BASE_URL + '/' + respond.data, '_blank');
                        }
                    }
                });
            }
        } else if (reportType === 'invoicedBatchSerial') {
            var getpdfdataurl = BASE_URL + '/reports/daily-sales-items-report/generate-invoiced-batch-serial-item-details-pdf';
            var invoiceIds = $('#invoiceName').val();
            var productIds = $('#batchSerialproductName').val();

            if (isAllInvoices != true && (invoiceIds == null || invoiceIds == "")) {
                p_notification(false, 'Please select at least one Invoice.');
            } else if (isAllProducts != true && (productIds == null || productIds == "")) {
                p_notification(false, 'Please select at least one Product.');
            } else {
                eb.ajax({
                    type: 'POST',
                    url: getpdfdataurl,
                    dataType: 'json',
                    data: {
                        isAllInvoices: isAllInvoices,
                        invoiceIds: $('#invoiceName').val(),
                        isAllProducts: isAllProducts,
                        productIds: $('#batchSerialproductName').val()
                    },
                    success: function(respond) {
                        if (respond.status == true) {
                            window.open(BASE_URL + '/' + respond.data, '_blank');
                        }
                    }
                });
            }
        } else if (reportType === 'serialItemDetail') {
            $('#dailySalesReport').addClass('hidden');
            var getpdfdataurl = BASE_URL + '/reports/daily-sales-items-report/generate-serial-item-detail-pdf';
            var productIds = $('#productName').val();
            var serialIds  = $('#serialName').val();

            if (isAllProducts != true && (productIds == null || productIds == "")) {
                p_notification(false, 'Please select at least one Product.');
            } else if (isAllSerials != true && (serialIds == null || serialIds == "")) {
                p_notification(false, 'Please select at least one Serial Number.');
            } else {
                eb.ajax({
                    type: 'POST',
                    url: getpdfdataurl,
                    dataType: 'json',
                    data: {
                        isAllProducts: isAllProducts,
                        productIds: $('#productName').val(),
                        isAllSerials: isAllSerials,
                        serialIds: $('#serialName').val()
                    },
                    success: function(respond) {
                        if (respond.status == true) {
                            window.open(BASE_URL + '/' + respond.data, '_blank');
                        }
                    }
                });
            }
        } else if (reportType === 'salesRepWiseItem') {
            var getpdfdataurl = BASE_URL + '/reports/daily-sales-items-report/generate-representative-wise-item-pdf';

            inputs = new Array(
                    $('#fromDate').val(),
                    $('#toDate').val()
                    );

            var isAllRepresentative = $('input[name="isAllRepresentative"]:checked').val();
            var isAllItems = $('input[name="isAllItems"]:checked').val();
            var representativeIds = $('#representativeList').val();
            var itemIds = $('#itemList').val();
            var categoryIds = $('#categoryList').val();


            if (validteinput(inputs)) {
                if (isAllRepresentative != true && (representativeIds == null || representativeIds == "")) {
                    p_notification(false, 'Please select at least one representative.');
                } else if (isAllItems != true && (itemIds == null || itemIds == "")) {
                    p_notification(false, 'Please select at least one product.');
                } else {
                    eb.ajax({
                        type: 'POST',
                        url: getpdfdataurl,
                        dataType: 'json',
                        data: {
                            fromDate: $('#fromDate').val(),
                            toDate: $('#toDate').val(),
                            isAllRepresentative: isAllRepresentative,
                            representativeIds: representativeIds,
                            isAllItems: isAllItems,
                            itemIds: itemIds,
                            itemCategory : categoryIds
                        },
                        success: function(respond) {
                            if (respond.status == true) {
                                p_notification(true, respond.msg);
                            }
                        }
                    });
                }
            }
        } else if (reportType === 'salesRepWiseCategory') {
            var getpdfdataurl = BASE_URL + '/reports/daily-sales-items-report/generate-representative-wise-category-pdf';

            inputs = new Array(
                    $('#fromDate').val(),
                    $('#toDate').val()
                    );

            var isAllRepresentative = $('input[name="isAllRepresentative"]:checked').val();
            var isAllCategories = $('input[name="isAllCategories"]:checked').val();
            var representativeIds = $('#representativeList').val();
            var categoryIds = $('#categoryList').val();

            if (validteinput(inputs)) {
                if (isAllRepresentative != true && (representativeIds == null || representativeIds == "")) {
                    p_notification(false, 'Please select at least one representative.');
                } else if (isAllCategories != true && (categoryIds == null || categoryIds == "")) {
                    p_notification(false, 'Please select at least one category.');
                } else {
                    eb.ajax({
                        type: 'POST',
                        url: getpdfdataurl,
                        dataType: 'json',
                        data: {
                            fromDate: $('#fromDate').val(),
                            toDate: $('#toDate').val(),
                            isAllRepresentative: isAllRepresentative,
                            representativeIds: representativeIds,
                            isAllCategories: isAllCategories,
                            categoryIds: categoryIds
                        },
                        success: function(respond) {
                            if (respond.status == true) {
                                p_notification(true, respond.msg);
                            }
                        }
                    });
                }
            }
        } else if (reportType === 'freeIssueItems') {
            var fromDate = $('#fromDate').val();
            var toDate = $('#toDate').val();
            inputs = new Array(
                    fromDate,
                    toDate
                    );

            if (validteinput(inputs)) {
                window.open(BASE_URL + "/reports/daily-sales-items-report/generate-free-issued-item-pdf?fromDate=" + fromDate + "&toDate=" + toDate, '_blank');
            }
        } else if(reportType === 'demandForecast') {
            var getpdfdataurl = BASE_URL + '/reports/daily-sales-items-report/generate-demand-forecast-pdf';

            var obj = {};
            obj.date = $('#forecastDate').val();
            obj.productIds = $('#forecastProductSelect').val();
            obj.isAllProducts = isAllProductsForForecast;
            obj.locationIds = $('#forecastLocationSelect').val();
            obj.isAllCategories = $('input[name="isAllCategories"]:checked').val();
            obj.categoryIds = $('#categoryList').val();

            if(demandForecastVaidation(obj)){
                var getpdfdatarequest = eb.post(getpdfdataurl, obj);
                getpdfdatarequest.done(function(respond) {
                    if (respond.status == true) {
                        window.open(BASE_URL + '/' + respond.data, '_blank');
                    }
                });
            }
        } else if(reportType === 'itemWiseSales') {
            products = $('#productWSales').val();
            locations = $('#locations').val();
            var fromDate = $('#fromDate').val();
            var endDate = $('#toDate').val();
            if (isAllProductsWSales != true && (products == '' || products == null)) {
                p_notification(false, eb.getMessage('ERR_DAISALE_ITEMWISETBL_PRODUCT_ERR'));
            } else if(isAllLocation != true && (locations == '' || locations == null)){
                p_notification(false, eb.getMessage('ERR_CUSREPO_SELELOCATION'));
            } else if((fromDate != null && fromDate != null) && (endDate < fromDate)){
                p_notification(false, eb.getMessage('ERR_CUSREPO_FROMDATE_CANT_BE_MORETHAN_ENDDATE'));
            } else {
                var url = BASE_URL + '/reports/daily-sales-items-report/generate-item-wise-sales-report-pdf';
                eb.ajax({
                    type: 'POST',
                    url: url,
                    data: {
                        products: products,
                        isAllProducts: isAllProductsWSales,
                        locations: locations,
                        isAllLocation: isAllLocation,
                        fromDate: fromDate,
                        endDate: endDate,
                    },
                    success: function(respond) {
                        if(respond.status){
                          window.open(BASE_URL + '/' + respond.data, '_blank');
                        }
                    },
                    async: false
                });
            }
        } else if (reportType === 'salesRepWiseItemWithInvoices') {
            var getpdfdataurl = BASE_URL + '/reports/daily-sales-items-report/generate-representative-wise-item-with-invoice-pdf';

            inputs = new Array(
                $('#fromDate').val(),
                $('#toDate').val()
            );

            var isAllRepresentative = $('input[name="isAllRepresentative"]:checked').val();
            var isAllItems = $('input[name="isAllItems"]:checked').val();
            var representativeIds = $('#representativeList').val();
            var itemIds = $('#itemList').val();

            if (validteinput(inputs)) {
                if (isAllRepresentative != true && (representativeIds == null || representativeIds == "")) {
                    p_notification(false, 'Please select at least one representative.');
                } else if (isAllItems != true && (itemIds == null || itemIds == "")) {
                    p_notification(false, 'Please select at least one product.');
                } else {
                    eb.ajax({
                        type: 'POST',
                        url: getpdfdataurl,
                        dataType: 'json',
                        data: {
                            fromDate: $('#fromDate').val(),
                            toDate: $('#toDate').val(),
                            isAllRepresentative: isAllRepresentative,
                            representativeIds: representativeIds,
                            isAllItems: isAllItems,
                            itemIds: itemIds
                        },
                        success: function(respond) {
                            if (respond.status == true) {
                                p_notification(true, respond.msg);
                            }
                        }
                    });
                }
            }
        } else if (reportType === 'invoicedSubItemBasedBom') {
            var getpdfdataurl = BASE_URL + '/reports/daily-sales-items-report/generate-invoiced-sub-item-based-bom-product-report-pdf';

            inputs = new Array(
                $('#fromDate').val(),
                $('#toDate').val()
            );

            var isAllItems = $('input[name="isAllBomProducts"]:checked').val();
            var representativeIds = $('#representativeList').val();
            var bomIds = $('#bomProducts').val();
            locations = $('#locations').val();

            if (validteinput(inputs)) {
                if (isAllItems != true && (bomIds == null || bomIds == "")) {
                    p_notification(false, 'Please select at least one product.');
                }else if(isAllLocation != true && (locations == '' || locations == null)){
                    p_notification(false, eb.getMessage('ERR_CUSREPO_SELELOCATION'));
                } else {
                    eb.ajax({
                        type: 'POST',
                        url: getpdfdataurl,
                        dataType: 'json',
                        data: {
                            fromDate: $('#fromDate').val(),
                            toDate: $('#toDate').val(),
                            isAllItems: isAllItems,
                            locations: locations,
                            isAllLocation: isAllLocation,
                            bomIds: bomIds
                        },
                        success: function(respond) {
                            if(respond.status){
                              window.open(BASE_URL + '/' + respond.data, '_blank');
                            }
                        }
                    });
                }
            }
        } 
    });

    $('#csvReport').click(function() {        
        if (reportType === 'invoicedSalesItems') {
            var fromDate = $('#fromDate').val();
            var toDate = $('#toDate').val();
            var batchSerial = $('#batchSerilPreview').is(":checked");
            inputs = new Array(
                    fromDate,
                    toDate,
                    batchSerial
                    );

            if (validteinput(inputs)) {
                var url = BASE_URL + '/reports/daily-sales-items-report/generate-invoiced-daily-sales-items-sheet';
                eb.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: {fromDate: fromDate, toDate: toDate, batchSerial:batchSerial},
                    success: function(respond) {
                        if (respond.status == true) {
                            window.open(BASE_URL + '/' + respond.data, '_blank');
                        }
                    }
                });
            }
        } else if (reportType === 'invoicedSalesItemsWithCost') {
            var fromDate = $('#fromDate').val();
            var toDate = $('#toDate').val();
            inputs = new Array(
                    fromDate,
                    toDate
                    );
            var categoryIds = $('#categoryList').val();
            var isAllCategories = $('input[name="isAllCategories"]:checked').val();

            if (validteinput(inputs)) {
                var url = BASE_URL + '/reports/daily-sales-items-report/generate-invoiced-daily-sales-items-with-cost-sheet';
                eb.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: {fromDate: fromDate, toDate: toDate, isAllCategories: isAllCategories, categoryIds: categoryIds},
                    success: function(respond) {
                        if (respond.status == true) {
                            window.open(BASE_URL + '/' + respond.data, '_blank');
                        }
                    }
                });
            }
        } else if (reportType === 'invoicedNoneInventorySalesItems') {
            var fromDate = $('#fromDate').val();
            var toDate = $('#toDate').val();
            inputs = new Array(
                    fromDate,
                    toDate
                    );

            if (validteinput(inputs)) {
                var url = BASE_URL + '/reports/daily-sales-items-report/generate-invoiced-daily-sales-none-inventory-item-sheet';
                eb.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: {fromDate: fromDate, toDate: toDate},
                    success: function(respond) {
                        if (respond.status == true) {
                            window.open(BASE_URL + '/' + respond.data, '_blank');
                        }
                    }
                });
            }
        } else if (reportType === 'posSalesItems') {
            var fromDate = $('#fromDate').val();
            var toDate = $('#toDate').val();
            inputs = new Array(
                    fromDate,
                    toDate
                    );

            if (validteinput(inputs)) {
                var url = BASE_URL + '/reports/daily-sales-items-report/generate-pos-daily-sales-items-sheet';
                eb.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: {fromDate: fromDate, toDate: toDate},
                    success: function(respond) {
                        if (respond.status == true) {
                            window.open(BASE_URL + '/' + respond.data, '_blank');
                        }
                    }
                });
            }
        }
        else if (reportType === 'pos-invoiceSalesItems') {
            var fromDate = $('#fromDate').val();
            var toDate = $('#toDate').val();
            inputs = new Array(
                    fromDate,
                    toDate
                    );

            if (validteinput(inputs)) {
                var url = BASE_URL + '/reports/daily-sales-items-report/generate-pos-and-invoiced-daily-sales-items-sheet';
                eb.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: {fromDate: fromDate, toDate: toDate},
                    success: function(respond) {
                        if (respond.status == true) {
                            window.open(BASE_URL + '/' + respond.data, '_blank');
                        }
                    }
                });
            }
        }
        else if (reportType === 'serialItemMovement') {
            var getcsvurl = BASE_URL + '/reports/daily-sales-items-report/generate-serial-item-movement-csv';
            var invoiceIds = $('#invoiceName').val();
            var productIds = $('#productName').val();
            var serialIds = $('#serialName').val();

            if (isAllInvoices != true && (invoiceIds == null || invoiceIds == "")) {
                p_notification(false, 'Please select at least one Invoice.');
            } else if (isAllProducts != true && (productIds == null || productIds == "")) {
                p_notification(false, 'Please select at least one Product.');
            } else if (isAllSerials != true && (serialIds == null || serialIds == "")) {
                p_notification(false, 'Please select at least one Serial Number.');
            } else {
                eb.ajax({
                    type: 'POST',
                    url: getcsvurl,
                    dataType: 'json',
                    data: {
                        isAllInvoices: isAllInvoices,
                        invoiceIds: $('#invoiceName').val(),
                        isAllProducts: isAllProducts,
                        productIds: $('#productName').val(),
                        isAllSerials: isAllSerials,
                        serialIds: $('#serialName').val()
                    },
                    success: function(respond) {
                        if (respond.status == true) {
                            window.open(BASE_URL + '/' + respond.data, '_blank');
                        }
                    }
                });
            }
        }
        else if (reportType === 'invoicedBatchSerial') {
            var getcsvurl = BASE_URL + '/reports/daily-sales-items-report/generate-invoiced-batch-serial-item-details-csv';
            var invoiceIds = $('#invoiceName').val();
            var productIds = $('#batchSerialproductName').val();

            if (isAllInvoices != true && (invoiceIds == null || invoiceIds == "")) {
                p_notification(false, 'Please select at least one Invoice.');
            } else if (isAllProducts != true && (productIds == null || productIds == "")) {
                p_notification(false, 'Please select at least one Product.');
            } else {
                eb.ajax({
                    type: 'POST',
                    url: getcsvurl,
                    dataType: 'json',
                    data: {
                        isAllInvoices: isAllInvoices,
                        invoiceIds: $('#invoiceName').val(),
                        isAllProducts: isAllProducts,
                        productIds: $('#batchSerialproductName').val()
                    },
                    success: function(respond) {
                        if (respond.status == true) {
                            window.open(BASE_URL + '/' + respond.data, '_blank');
                        }
                    }
                });
            }
        }
        else if (reportType === 'serialItemDetail') {
            $('#dailySalesReport').addClass('hidden');
            var getcsvurl = BASE_URL + '/reports/daily-sales-items-report/generate-serial-item-detail-csv';
            var productIds = $('#productName').val();
            var serialIds  = $('#serialName').val();

            if (isAllProducts != true && (productIds == null || productIds == "")) {
                p_notification(false, 'Please select at least one Product.');
            } else if (isAllSerials != true && (serialIds == null || serialIds == "")) {
                p_notification(false, 'Please select at least one Serial Number.');
            } else {
                eb.ajax({
                    type: 'POST',
                    url: getcsvurl,
                    dataType: 'json',
                    data: {
                        isAllProducts: isAllProducts,
                        productIds: $('#productName').val(),
                        isAllSerials: isAllSerials,
                        serialIds: $('#serialName').val()
                    },
                    success: function(respond) {
                        if (respond.status == true) {
                            window.open(BASE_URL + '/' + respond.data, '_blank');
                        }
                    }
                });
            }
        }
        else if (reportType === 'salesRepWiseItem') {            
            var getcsvdataurl = BASE_URL + '/reports/daily-sales-items-report/generate-representative-wise-item-csv';

            inputs = new Array(
                    $('#fromDate').val(),
                    $('#toDate').val()
                    );

            var isAllRepresentative = $('input[name="isAllRepresentative"]:checked').val();
            var isAllItems = $('input[name="isAllItems"]:checked').val();
            var representativeIds = $('#representativeList').val();
            var itemIds = $('#itemList').val();
            var categoryIds = $('#categoryList').val();

            if (validteinput(inputs)) {
                if (isAllRepresentative != true && (representativeIds == null || representativeIds == "")) {
                    p_notification(false, 'Please select at least one representative.');
                } else if (isAllItems != true && (itemIds == null || itemIds == "")) {
                    p_notification(false, 'Please select at least one product.');
                } else {
                    eb.ajax({
                        type: 'POST',
                        url: getcsvdataurl,
                        dataType: 'json',
                        data: {
                            fromDate: $('#fromDate').val(),
                            toDate: $('#toDate').val(),
                            isAllRepresentative: isAllRepresentative,
                            representativeIds: representativeIds,
                            isAllItems: isAllItems,
                            itemIds: itemIds,
                            itemCategory : categoryIds
                        },
                        success: function(respond) {
                            if (respond.status == true) {
                                p_notification(true, respond.msg);
                            }
                        }
                    });
                }
            }
        }
        else if (reportType === 'salesRepWiseCategory') {
            var getcsvdataurl = BASE_URL + '/reports/daily-sales-items-report/generate-representative-wise-category-csv';

            inputs = new Array(
                    $('#fromDate').val(),
                    $('#toDate').val()
                    );

            var isAllRepresentative = $('input[name="isAllRepresentative"]:checked').val();
            var isAllCategories = $('input[name="isAllCategories"]:checked').val();
            var representativeIds = $('#representativeList').val();
            var categoryIds = $('#categoryList').val();

            if (validteinput(inputs)) {
                if (isAllRepresentative != true && (representativeIds == null || representativeIds == "")) {
                    p_notification(false, 'Please select at least one representative.');
                } else if (isAllCategories != true && (categoryIds == null || categoryIds == "")) {
                    p_notification(false, 'Please select at least one category.');
                } else {
                    eb.ajax({
                        type: 'POST',
                        url: getcsvdataurl,
                        dataType: 'json',
                        data: {
                            fromDate: $('#fromDate').val(),
                            toDate: $('#toDate').val(),
                            isAllRepresentative: isAllRepresentative,
                            representativeIds: representativeIds,
                            isAllCategories: isAllCategories,
                            categoryIds: categoryIds
                        },
                        success: function(respond) {
                            if (respond.status == true) {
                                p_notification(true, respond.msg);
                            }
                        }
                    });
                }
            }
        }
        else if (reportType === 'freeIssueItems') {
            var fromDate = $('#fromDate').val();
            var toDate = $('#toDate').val();
            inputs = new Array(
                    fromDate,
                    toDate
                    );

            if (validteinput(inputs)) {
                var url = BASE_URL + '/reports/daily-sales-items-report/generate-free-issued-item-sheet';
                eb.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: {fromDate: fromDate, toDate: toDate},
                    success: function(respond) {
                        if (respond.status == true) {
                            window.open(BASE_URL + '/' + respond.data, '_blank');
                        }
                    }
                });
            }
        } else if(reportType === 'demandForecast') {
            var getpdfdataurl = BASE_URL + '/reports/daily-sales-items-report/generate-demand-forecast-csv';

            var obj = {};
            obj.date = $('#forecastDate').val();
            obj.productIds = $('#forecastProductSelect').val();
            obj.isAllProducts = isAllProductsForForecast;
            obj.locationIds = $('#forecastLocationSelect').val();
            obj.isAllCategories = $('input[name="isAllCategories"]:checked').val();
            obj.categoryIds = $('#categoryList').val();

            if(demandForecastVaidation(obj)){
                var getpdfdatarequest = eb.post(getpdfdataurl, obj);
                getpdfdatarequest.done(function(respond) {
                    if (respond.status == true) {
                        window.open(BASE_URL + '/' + respond.data, '_blank');
                    }
                });
            }
        } else if(reportType === 'itemWiseSales') {
            products = $('#productWSales').val();
            locations = $('#locations').val();
            var fromDate = $('#fromDate').val();
            var endDate = $('#toDate').val();
            if (isAllProductsWSales != true && (products == '' || products == null)) {
                p_notification(false, eb.getMessage('ERR_DAISALE_ITEMWISETBL_PRODUCT_ERR'));
            } else if(isAllLocation != true && (locations == '' || locations == null)){
                p_notification(false, eb.getMessage('ERR_CUSREPO_SELELOCATION'));
            } else if((fromDate != null && fromDate != null) && (endDate < fromDate)){
                p_notification(false, eb.getMessage('ERR_CUSREPO_FROMDATE_CANT_BE_MORETHAN_ENDDATE'));
            } else {
                var url = BASE_URL + '/reports/daily-sales-items-report/generate-item-wise-sales-report-csv';
                eb.ajax({
                    type: 'POST',
                    url: url,
                    data: {
                        products: products,
                        isAllProducts: isAllProductsWSales,
                        locations: locations,
                        isAllLocation: isAllLocation,
                        fromDate: fromDate,
                        endDate: endDate,
                    },
                    success: function(respond) {
                        if(respond.status){
                          window.open(BASE_URL + '/' + respond.data, '_blank');
                        }
                    },
                    async: false
                });
            }
        } else if (reportType === 'salesRepWiseItemWithInvoices') {
            var getcsvdataurl = BASE_URL + '/reports/daily-sales-items-report/generate-representative-wise-item-with-invoice-csv';

            inputs = new Array(
                    $('#fromDate').val(),
                    $('#toDate').val()
                    );

            var isAllRepresentative = $('input[name="isAllRepresentative"]:checked').val();
            var isAllItems = $('input[name="isAllItems"]:checked').val();
            var representativeIds = $('#representativeList').val();
            var itemIds = $('#itemList').val();

            if (validteinput(inputs)) {
                if (isAllRepresentative != true && (representativeIds == null || representativeIds == "")) {
                    p_notification(false, 'Please select at least one representative.');
                } else if (isAllItems != true && (itemIds == null || itemIds == "")) {
                    p_notification(false, 'Please select at least one product.');
                } else {
                    eb.ajax({
                        type: 'POST',
                        url: getcsvdataurl,
                        dataType: 'json',
                        data: {
                            fromDate: $('#fromDate').val(),
                            toDate: $('#toDate').val(),
                            isAllRepresentative: isAllRepresentative,
                            representativeIds: representativeIds,
                            isAllItems: isAllItems,
                            itemIds: itemIds
                        },
                        success: function(respond) {
                            if (respond.status == true) {
                                p_notification(true, respond.msg);
                            }
                        }
                    });
                }
            }
        } else if (reportType === 'invoicedSubItemBasedBom') {
            inputs = new Array(
                $('#fromDate').val(),
                $('#toDate').val()
            );

            var isAllItems = $('input[name="isAllBomProducts"]:checked').val();
            var representativeIds = $('#representativeList').val();
            var bomIds = $('#bomProducts').val();
            locations = $('#locations').val();

            if (validteinput(inputs)) {
                if (isAllItems != true && (bomIds == null || bomIds == "")) {
                    p_notification(false, 'Please select at least one product.');
                }else if(isAllLocation != true && (locations == '' || locations == null)){
                    p_notification(false, eb.getMessage('ERR_CUSREPO_SELELOCATION'));
                } else {
                    var url = BASE_URL + '/reports/daily-sales-items-report/generate-invoiced-sub-item-based-bom-product-detils-sheet';
                    eb.ajax({
                        type: 'POST',
                        url: url,
                        dataType: 'json',
                        data: {
                            fromDate: $('#fromDate').val(),
                            toDate: $('#toDate').val(),
                            isAllItems: isAllItems,
                            locations: locations,
                            isAllLocation: isAllLocation,
                            bomIds: bomIds
                        },
                        success: function(respond) {
                            if (respond.status == true) {
                                window.open(BASE_URL + '/' + respond.data, '_blank');
                            }
                        }
                    });
                }
            }
        }
    });

    $('.isAllInvoices').on('click', function() {
        isAllInvoices = $(this).val();
        if (isAllInvoices == true) {
            $('#invoiceName').val('').trigger('change');
            $('#invoiceName').prop('disabled', true);
        } else {
            $('#invoiceName').prop('disabled', false);
        }
    });

    $('.isAllProducts').on('click', function() {
        isAllProducts = $(this).val();
        if (isAllProducts == true) {
            $('#productName').val('').trigger('change');
            $('#productName').prop('disabled', true);
        } else {
            $('#productName').prop('disabled', false);
        }
    });

    $('.isAllBomProducts').on('click', function() {
        isAllProducts = $(this).val();
        if (isAllProducts == true) {
            $('#bomProducts').val('').trigger('change');
            $('#bomProducts').prop('disabled', true).selectpicker('refresh');
        } else {
            $('#bomProducts').prop('disabled', false).selectpicker('refresh');
        }
    });
    
    $('.isAllBatchSerialProducts').on('click', function() {
        isAllProducts = $(this).val();
        if (isAllProducts == true) {
            $('#batchSerialproductName').val('').trigger('change');
            $('#batchSerialproductName').prop('disabled', true);
        } else {
            $('#batchSerialproductName').prop('disabled', false);
        }
    });

    $.each(LOCATION, function(index, value) {
        $('#locations').append($("<option value='" + index + "'>" + value + "</option>"));
    });
    $('.isAllLocation').on('click', function() {
        isAllLocation = $(this).val();
        if (isAllLocation == true) {
            $('#locations').val('').trigger('change');
            $('#locations').prop('disabled', true);
            p_notification('info', eb.getMessage('INFO_CUS_REPORT_ALL_CUS_SELECTION'));
        } else {
            $('#locations').prop('disabled', false);
        }
        $('#locations').selectpicker('refresh');
    });

    $('.isAllProductsWSales').on('click', function() {
        isAllProductsWSales = $(this).val();
        if (isAllProductsWSales == true) {
            $('#productWSales').val('').trigger('change');
            $('#productWSales').prop('disabled', true);
            p_notification('info', eb.getMessage('INFO_CUS_REPORT_ALL_CUS_SELECTION'));
        } else {
            $('#productWSales').prop('disabled', false);
        }
        $('#productWSales').selectpicker('refresh');
    });

    $('.isAllSerials').on('click', function() {
        isAllSerials = $(this).val();
        if (isAllSerials == true) {
            $('#serialName').val('').trigger('change');
            $('#serialName').prop('disabled', true);
        } else {
            $('#serialName').prop('disabled', false);
        }
    });

    $('.isAllItems').on('click', function() {
        isAllItems = $(this).val();
        if (isAllItems == true) {
            $('#itemList').val('').trigger('change');
            $('#itemList').prop('disabled', true);
        } else {
            $('#itemList').prop('disabled', false);
        }
    });

    $('.isAllRepresentative').on('click', function() {
        isAllRepresentative = $(this).val();
        if (isAllRepresentative == true) {
            $('#representativeList').val('').trigger('change');
            $('#representativeList').prop('disabled', true);
        } else {
            $('#representativeList').prop('disabled', false);
        }
    });

    $('.isAllCategory').on('click', function() {
        isAllCategory = $(this).val();
        if (isAllCategory == true) {
            $('#categoryList').val('').trigger('change');
            $('#categoryList').prop('disabled', true);
        } else {
            $('#categoryList').prop('disabled', false);
        }
    });
    
    $('.is_all_forecast_products').on('click', function() {
        isAllProductsForForecast = ($(this).val() == 1) ? 1 : 0;
        if (isAllProductsForForecast == true) {
            $('#forecastProductSelect').val('').trigger('change');
            $('#forecastProductSelect').prop('disabled', true);
        } else {
            $('#forecastProductSelect').prop('disabled', false);
        }
    });

    $('.isAllInvoices').click();
    $('.isAllSerials').click();
    $('.isAllProducts').click();

});

function reSetDateFields() {
    $('#fromDate').val('');
    $('#toDate').val('');
}
// date validation
function validteinput(inputs) {
    var fromDate = inputs[0];
    var toDate = inputs[1];
    if (fromDate == null || fromDate == "") {
        p_notification(false, eb.getMessage('ERR_DAISALE_FROMD'));
        document.getElementById("fromDate").focus();
    }
    else if (toDate == null || toDate == "") {
        p_notification(false, eb.getMessage('ERR_DAISALE_TOD'));
        document.getElementById("toDate").focus();
    } else if (fromDate > toDate) {
        p_notification(false, eb.getMessage('ERR_DAISALE_TODFROMD'));
        document.getElementById("toDate").focus();
    } else {
        return true;
    }
}

function demandForecastVaidation(input) {
    if(!input.date){
        p_notification(false, eb.getMessage('ERR_DEMAND_FORECAST_REPORT_ON_DATE'));
        return false;
    } else if (input.isAllProducts == 0 && !input.productIds) {
        p_notification(false, eb.getMessage('ERR_DEMAND_FORECAST_REPORT_PRODUCT'));
        return false;
    } else if (!input.locationIds) {
        p_notification(false, eb.getMessage('ERR_DEMAND_FORECAST_REPORT_LOCATION'));
        return false;
    }
    return true;
}