var dimensionTypeID = null;
var dimensionValue = null;
$(document).ready(function() {
	var reportType = 'TrialBalance';
  if (reportType == "TrialBalance" && $('#fiscalPeriods').val() == 0) {
      $('#trStartDate').attr('disabled',true);
      $('#trEndDate').attr('disabled',true);
  } else {
      $('#trStartDate').attr('disabled',false);
      $('#trEndDate').attr('disabled',false);
  }

  if (reportType == 'TrialBalance') {
    $('#locationFilterDiv').removeClass('hidden');
    $('#isAccLoca').removeClass('hidden');
    $('#isHideZero').removeClass('hidden');
  }

	$('#salesReport').hide();

  	$('#trialBalance').click(function(e) {
  		resetPage();
  		reportType = 'TrialBalance';
      $('.fiscalPeriod_div').removeClass('hidden');
      $('#orderByDateDiv').addClass('hidden');
      $('.trialBalanceDate').removeClass('hidden');
      $('.normalDate').addClass('hidden');
      $('.dimension_div').removeClass('hidden');
      if (reportType == "TrialBalance" && $('#fiscalPeriods').val() == 0) {
          $('#trStartDate').attr('disabled',true);
          $('#trEndDate').attr('disabled',true);
      } else {
          $('#trStartDate').attr('disabled',false);
          $('#trEndDate').attr('disabled',false);
      }
    });

    $('#balanceSheet').click(function(e) {
      resetPage();
      reportType = 'BalanceSheet';
      $('.report_from_start_date').addClass('hidden');
      $('#orderByDateDiv').addClass('hidden');
      $('.fiscalPeriod_div').addClass('hidden');
      $('.trialBalanceDate').addClass('hidden');
      $('.normalDate').removeClass('hidden');
      $('.dimension_div').removeClass('hidden');
    });

    $('#profitAndLost').click(function(e) {
      resetPage();
      reportType = 'ProfitAndLost';
      $('.fiscalPeriod_div').addClass('hidden');
      $('#orderByDateDiv').addClass('hidden');
      $('.trialBalanceDate').addClass('hidden');
      $('.normalDate').removeClass('hidden');
      $('.dimension_div').removeClass('hidden');
    });

    $('#generalLedger').click(function(e) {
      resetPage();
      reportType = 'GeneralLedger';
      $('.fiscalPeriod_div').addClass('hidden');
      $('#orderByDateDiv').removeClass('hidden');
      $('.trialBalanceDate').addClass('hidden');
      $('.normalDate').removeClass('hidden');
      $('.dimension_div').removeClass('hidden');
    });

    $('#auditShedule').click(function(e) {
      resetPage();
      reportType = 'AuditShedule';
      $('.fiscalPeriod_div').addClass('hidden');
      $('#orderByDateDiv').removeClass('hidden');
      $('.trialBalanceDate').addClass('hidden');
      $('.normalDate').removeClass('hidden');
      $('.dimension_div').removeClass('hidden');
      $('#orderByDateDiv').addClass('hidden');
    });

    $('#cashflow').click(function(e) {
      resetPage();
      reportType = 'CashFlowStatemnt';
      $('.fiscalPeriod_div').addClass('hidden');
      $('#orderByDateDiv').addClass('hidden');
      $('.trialBalanceDate').addClass('hidden');
      $('.dimension_div').addClass('hidden');
      $('.normalDate').removeClass('hidden');
    });

    $('#auditLogs').click(function(e) {
      resetPage();
      reportType = 'AuditLogs';
  		$('.fiscalPeriod_div').addClass('hidden');
      $('#orderByDateDiv').addClass('hidden');
      $('.trialBalanceDate').addClass('hidden');
      $('.normalDate').removeClass('hidden');
  	});

    $('.report_pills').on('click', function(){
      if(reportType == 'GeneralLedger' || reportType == "AuditShedule"){
        $('.account_filter').removeClass('hidden');
      } else {
        $('.account_filter').addClass('hidden');
      }

      if (reportType == "AuditShedule" || reportType == 'BalanceSheet' || reportType == 'ProfitAndLost' || reportType == 'TrialBalance') {
        $('#locationFilterDiv').removeClass('hidden');
      } else {
        $('#locationFilterDiv').addClass('hidden');
      }

      if (reportType == 'BalanceSheet' || reportType == 'ProfitAndLost' || reportType == 'TrialBalance') {
        $('#isAccLoca').removeClass('hidden');
        $('#isHideZero').removeClass('hidden');
      } else {
        $('#isAccLoca').addClass('hidden');
        $('#isHideZero').addClass('hidden');
      }


      if (reportType == 'AuditShedule') {
        $('#isHideZero').removeClass('hidden');
      }

    });

  	function resetPage()
  	{
  		$('.report_from_start_date').removeClass('hidden')
		$('#financeReport').html('');
  		$('#financeReport').hide();
  		$('#startDate').val('');
  		$('#endDate').val('');
  	}
    var accountIDs ;
    loadDropDownFromDatabase('/accounts-api/search-active-accounts-for-dropdown', "", "", '#accountID');
    $('#accountID').on('change', function() {
        accountIDs = $(this).val();

    });

    $('#viewReport').click(function(e) {
  		if(reportType == 'TrialBalance'){
  			if(trialvalidation()){
  				eb.ajax({
  					type: 'POST',
  					url: BASE_URL + '/finance-report-api/trialBalance',
  					data: {
  						startDate: $('#trStartDate').val(),
  						endDate: $('#trEndDate').val(),
  						reportModel : 'View',
              dimensionType : dimensionTypeID,
              dimensionValue : dimensionValue,
              locationID : $("#locationList").val(),
              sepLocBal : $('#showAccuBal').is(":checked"),
              hideZeroVal : $('#hideZeroVal').is(":checked")
  					},
  					success: function(respond) {
  						if(respond.status){
  							$('#financeReport').show();
  							$('#financeReport').html(respond.html);
  						}else{
  							p_notification(respond.status, respond.msg);
  						}
  					}
  				});
  			}
  		} else if( reportType == 'BalanceSheet' ){
  			if(balanceSheetValidation()){
  				eb.ajax({
  					type: 'POST',
  					url: BASE_URL + '/finance-report-api/balanceSheet',
  					data: {
  						endDate: $('#endDate').val(),
  						reportModel : 'View',
              dimensionType : dimensionTypeID,
              dimensionValue : dimensionValue,
              locationID : $("#locationList").val(),
              sepLocBal : $('#showAccuBal').is(":checked"),
              hideZeroVal : $('#hideZeroVal').is(":checked")
  					},
  					success: function(respond) {
  						if(respond.status){
  							$('#financeReport').show();
  							$('#financeReport').html(respond.html);
  						}else{
  							p_notification(respond.status, respond.msg);
  						}
  					}
  				});
  			}
  		} else if( reportType == 'ProfitAndLost' ){
  			if(pAndLvalidation()){
  				eb.ajax({
  					type: 'POST',
  					url: BASE_URL + '/finance-report-api/profitAndLost',
  					data: {
  						startDate: $('#startDate').val(),
  						endDate: $('#endDate').val(),
  						reportModel : 'View',
              dimensionType : dimensionTypeID,
              dimensionValue : dimensionValue,
              locationID : $("#locationList").val(),
              sepLocBal : $('#showAccuBal').is(":checked"),
              hideZeroVal : $('#hideZeroVal').is(":checked")
  					},
  					success: function(respond) {
  						if(respond.status){
  							$('#financeReport').show();
  							$('#financeReport').html(respond.html);
  						}else{
  							p_notification(respond.status, respond.msg);
  						}
  					}
  				});
  			}
  		} else if( reportType == 'GeneralLedger' ){
        if(validation()){
  				eb.ajax({
  					type: 'POST',
  					url: BASE_URL + '/finance-report-api/generalLedger',
  					data: {
  						startDate: $('#startDate').val(),
  						endDate: $('#endDate').val(),
  						reportModel : 'View',
              accountID : accountIDs,
              dimensionType : dimensionTypeID,
              dimensionValue : dimensionValue
  					},
  					success: function(respond) {
  						if(respond.status){
  							$('#financeReport').show();
  							$('#financeReport').html(respond.html);
  						}else{
  							p_notification(respond.status, respond.msg);
  						}
  					}
  				});
  			}
  		} else if( reportType == 'AuditShedule' ){
        if(validation()){
          eb.ajax({
            type: 'POST',
            url: BASE_URL + '/finance-report-api/auditShedule',
            data: {
              startDate: $('#startDate').val(),
              endDate: $('#endDate').val(),
              reportModel : 'View',
              accountID : accountIDs,
              dimensionType : dimensionTypeID,
              dimensionValue : dimensionValue,
              locationID : $("#locationList").val(),
              hideZeroVal : $('#hideZeroVal').is(":checked")
            },
            success: function(respond) {
              if(respond.status){
                $('#financeReport').show();
                $('#financeReport').html(respond.html);
              }else{
                p_notification(respond.status, respond.msg);
              }
            }
          });
        }
      } else if( reportType == 'CashFlowStatemnt' ){
        if(validation()){
          eb.ajax({
            type: 'POST',
            url: BASE_URL + '/finance-report-api/cashFlowReport',
            data: {
              startDate: $('#startDate').val(),
              endDate: $('#endDate').val()
            },
            success: function(respond) {
              if(respond.status){
                $('#financeReport').show();
                $('#financeReport').html(respond.html);
              }else{
                p_notification(respond.status, respond.msg);
              }
            }
          });
        }
      }
  	});

  	$('#generatePdf').click(function(e) {
  		if(reportType == 'TrialBalance'){
  			if(trialvalidation()){
  				var params = {
  					startDate: $('#trStartDate').val(),
  					endDate: $('#trEndDate').val(),
            dimensionType : dimensionTypeID,
            dimensionValue : dimensionValue,
            locationID : $("#locationList").val(),
            sepLocBal : $('#showAccuBal').is(":checked"),
            hideZeroVal : $('#hideZeroVal').is(":checked")
  				}
          var url = BASE_URL + '/finance-report-api/generate-trial-balance-pdf';
            eb.ajax({
                type: 'POST',
                url: url,
                dataType: 'json',
                data: params,
                success: function(respond) {
                    if (respond.status) {
                        p_notification(true, respond.msg);
                    }
                }
            });
  			}
  		} else if( reportType == 'BalanceSheet'){
  			if(balanceSheetValidation()){
  				var params = {
  					endDate: $('#endDate').val(),
            dimensionType : dimensionTypeID,
            dimensionValue : dimensionValue,
            locationID : $("#locationList").val(),
            sepLocBal : $('#showAccuBal').is(":checked"),
            hideZeroVal : $('#hideZeroVal').is(":checked")
  				}

          var url = BASE_URL + '/finance-report-api/generate-balance-sheet-pdf';
            eb.ajax({
                type: 'POST',
                url: url,
                dataType: 'json',
                data: params,
                success: function(respond) {
                    if (respond.status) {
                        p_notification(true, respond.msg);
                    }
                }
            });
  			}
  		} else if( reportType == 'ProfitAndLost' ){
  			if(pAndLvalidation()){
  				var params = {
  					startDate: $('#startDate').val(),
  					endDate: $('#endDate').val(),
            dimensionType : dimensionTypeID,
            dimensionValue : dimensionValue,
            locationID : $("#locationList").val(),
            sepLocBal : $('#showAccuBal').is(":checked"),
            hideZeroVal : $('#hideZeroVal').is(":checked")
  				}
           var url = BASE_URL + '/finance-report-api/generate-profit-and-lost-pdf';
            eb.ajax({
                type: 'POST',
                url: url,
                dataType: 'json',
                data: params,
                success: function(respond) {
                    if (respond.status) {
                        p_notification(true, respond.msg);
                    }
                }
            });
  			}
  		} else if( reportType == 'GeneralLedger' ){
  			if(validation()){
  				var params = {
  					startDate: $('#startDate').val(),
  					endDate: $('#endDate').val(),
            accountID : accountIDs,
            dimensionType : dimensionTypeID,
            dimensionValue : dimensionValue,
            isOrderByDate : $('#jeOrderByDate').is(":checked"),
            balState : $('#showAccBl').is(":checked")
  				}
           var url = BASE_URL + '/finance-report-api/generate-general-ledger-pdf';
            eb.ajax({
                type: 'POST',
                url: url,
                dataType: 'json',
                data: params,
                success: function(respond) {
                    if (respond.status) {
                        p_notification(true, respond.msg);
                    }
                }
            });
  			}
  		} else if( reportType == 'AuditShedule' ){
        if(validation()){
          var params = {
            startDate: $('#startDate').val(),
            endDate: $('#endDate').val(),
            accountID : accountIDs,
            dimensionType : dimensionTypeID,
            dimensionValue : dimensionValue,
            locationID : $("#locationList").val(),
            hideZeroVal : $('#hideZeroVal').is(":checked")
          }
           var url = BASE_URL + '/finance-report-api/generate-audit-shedule-pdf';
            eb.ajax({
                type: 'POST',
                url: url,
                dataType: 'json',
                data: params,
                success: function(respond) {
                    if (respond.status) {
                        window.open(BASE_URL + '/' + respond.data, '_blank');
                    }
                }
            });
        }
      } else if(reportType == 'CashFlowStatemnt'){
        if(validation()){
          eb.ajax({
            type: 'POST',
            url: BASE_URL + '/finance-report-api/cashFlowReportPdf',
            data: {
              startDate: $('#startDate').val(),
              endDate: $('#endDate').val()
            },
            success: function(respond) {
              if (respond.status) {
                  p_notification(true, respond.msg);
              }
            }
          });
        }
      } 

  	});

  	$('#csvReport').click(function(e){
  		if(reportType == 'TrialBalance'){
  			if(trialvalidationCsv()){
  				var params = {
  					startDate: $('#trStartDate').val(),
  					endDate: $('#trEndDate').val(),
  					reportModel : 'Csv',
            dimensionType : dimensionTypeID,
            dimensionValue : dimensionValue,
            locationID : $("#locationList").val(),
            sepLocBal : $('#showAccuBal').is(":checked"),
            hideZeroVal : $('#hideZeroVal').is(":checked")
  				}
  				var url = BASE_URL + '/finance-report-api/trialBalance';
                eb.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: params,
                    success: function(respond) {
                        if (respond.status) {
                            p_notification(true, respond.msg);
                        }
                    }
                });
  			}
  		} else if(reportType == 'BalanceSheet'){
  			if(balanceSheetValidationForCsv()){
  				var params = {
  					endDate: $('#endDate').val(),
  					reportModel : 'Csv',
            dimensionType : dimensionTypeID,
            dimensionValue : dimensionValue,
            locationID : $("#locationList").val(),
            sepLocBal : $('#showAccuBal').is(":checked"),
            hideZeroVal : $('#hideZeroVal').is(":checked")
  				}
  				var url = BASE_URL + '/finance-report-api/balanceSheet';
                eb.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: params,
                    success: function(respond) {
                        if (respond.status) {
                            p_notification(true, respond.msg);
                        }
                    }
                });
  			}
  		} else if( reportType == 'ProfitAndLost' ){
  			if(pAndLvalidationForCsv()){
  				var params = {
  					startDate: $('#startDate').val(),
  					endDate: $('#endDate').val(),
  					reportModel : 'Csv',
            dimensionType : dimensionTypeID,
            dimensionValue : dimensionValue,
            locationID : $("#locationList").val(),
            sepLocBal : $('#showAccuBal').is(":checked"),
            hideZeroVal : $('#hideZeroVal').is(":checked")
  				}
  				var url = BASE_URL + '/finance-report-api/profitAndLost';
                eb.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: params,
                    success: function(respond) {
                        if (respond.status) {
                            p_notification(true, respond.msg);
                        }
                    }
                });
  			}
  		} else if( reportType == 'GeneralLedger' ){
  			if(validation()){
  				var params = {
  					startDate: $('#startDate').val(),
  					endDate: $('#endDate').val(),
  					reportModel : 'Csv',
            accountID : accountIDs,
            dimensionType : dimensionTypeID,
            dimensionValue : dimensionValue,
            isOrderByDate : $('#jeOrderByDate').is(":checked"),
            balState : $('#showAccBl').is(":checked")
  				}
  				var url = BASE_URL + '/finance-report-api/generalLedger';
                eb.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: params,
                    success: function(respond) {
                        if (respond.status) {
                            p_notification(true, respond.msg);
                        }
                    }
                });
  			}
  		} else if( reportType == 'AuditShedule' ){
        if(validation()){
          var params = {
            startDate: $('#startDate').val(),
            endDate: $('#endDate').val(),
            reportModel : 'Csv',
            accountID : accountIDs,
            dimensionType : dimensionTypeID,
            dimensionValue : dimensionValue,
            locationID : $("#locationList").val(),
            hideZeroVal : $('#hideZeroVal').is(":checked")

          }
          var url = BASE_URL + '/finance-report-api/auditSheduleSheet';
          eb.ajax({
              type: 'POST',
              url: url,
              dataType: 'json',
              data: params,
              success: function(respond) {
                  if (respond.status) {
                      window.open(BASE_URL + '/' + respond.data, '_blank');
                  }
              }
          });
        }
      } else if(reportType == 'CashFlowStatemnt'){
        if(validation()){
          eb.ajax({
            type: 'POST',
            url: BASE_URL + '/finance-report-api/cashFlowReportCsv',
            data: {
              startDate: $('#startDate').val(),
              endDate: $('#endDate').val()
            },
            success: function(respond) {
              if (respond.status) {
                  p_notification(true, respond.msg);
              }
            }
          });
        }
      } 
  	});

  	$('#financeReport').on('click','.financeAccountChild',function(e){
  		e.preventDefault();

		var parentID = $(this).parents('tr').attr('id');
		if($(this).parents('tr').data('autotrigger') == true){
			$(this).parents('tr').data('autotrigger',false);
			if($('#financeReport').find('.'+parentID).hasClass('hidden')){
				$('#financeReport').find('.'+parentID).removeClass('hidden');
			}
		}

		if($('#financeReport').find('.'+parentID).hasClass('hidden')){
			$('#financeReport').find('.'+parentID).removeClass('hidden');
		}else{
			$('#financeReport').find('.'+parentID).addClass('hidden').data('autotrigger',true);
			$('#financeReport').find('.'+parentID).find('.financeAccountChild').trigger('click');
		}
	});

	$('#financeReport').on('click','.collapseAllAccounts',function(){
		if($(this).is(':checked')){
			$('#financeReport').find('.collapsAll').removeClass('hidden');
		}else{
			$('#financeReport').find('.collapsAll').addClass('hidden');
		}
	});

	$('#financeReport').on('click','.showAccounts',function(){
		if($(this).find('.plus').hasClass('fa fa-plus-square')){
			$(this).siblings('.accountsDiv').removeClass('hidden');
			$(this).find('.plus').removeClass('fa fa-plus-square');
			$(this).find('.plus').addClass('fa fa-minus-square');

		}else{
			$(this).siblings('.accountsDiv').addClass('hidden');
			$(this).find('.plus').removeClass('fa fa-minus-square');
			$(this).find('.plus').addClass('fa fa-plus-square');

		}
	});

	$('#financeReport').on('click','.JEntries',function(){
    var finaceAcId = $(this).data('account-id');
    if($(this).siblings('.JEtable').hasClass('hidden')){
      getJournalEntriesForGeneralLedgerPreview(finaceAcId);
			$(this).siblings('.JEtable').removeClass('hidden')
			$(this).find('.plus').removeClass('fa fa-plus-square');
			$(this).find('.plus').addClass('fa fa-minus-square');
		}else{
      $('#'+finaceAcId+'generalLedgerJETable .journalTbody .addedJEs').remove();
			$(this).siblings('.JEtable').addClass('hidden')
			$(this).find('.plus').removeClass('fa fa-minus-square');
			$(this).find('.plus').addClass('fa fa-plus-square');
		}
	});

  $('#financeReport').on('click', '.journalComment', function(e) {
        var docID = $(this).attr('data-docid');
        var docTypeID = $(this).attr('data-typeid');
        redirectdocumentPreview(docTypeID, docID);
  });

  	///////DatePicker\\\\\\\
    var nowTemp = new Date();
    var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);
    var checkin = $('#startDate').datepicker({onRender: function(date) {
            return date.valueOf() < now.valueOf() ? ((U_DATE_R_OVERRIDE)?'':'disabled') : '';
        }
    }).on('changeDate', function(ev) {
        checkin.hide();
    }).data('datepicker');
    // checkin.setValue(now);

    ///////DatePicker\\\\\\\
    var checkOut = $('#endDate').datepicker({onRender: function(date) {
            return date.valueOf() < now.valueOf() ? ((U_DATE_R_OVERRIDE)?'':'disabled') : '';
        }
    }).on('changeDate', function(ev) {
        checkOut.hide();
    }).data('datepicker');


    $('#fiscalPeriods').on('change',function(){
        if ($(this).val() == 0) {
            $('#trStartDate').attr('disabled',true);
            $('#trEndDate').attr('disabled',true);
            $('#trStartDate').val('');
            $('#trEndDate').val('');
        } else {
            $('#trStartDate').attr('disabled',false);
            $('#trEndDate').attr('disabled',false);
            inDate.setValue($('#fiscalPeriods').val().split('_')[0]);
            outDate.setValue($('#fiscalPeriods').val().split('_')[1]);
        }
    });

    var inDate = $('#trStartDate').datepicker({onRender: function(date) {
            return date.valueOf() < now.valueOf() ? ((U_DATE_R_OVERRIDE)?'':'disabled') : '';
        }
    }).on('changeDate', function(ev) {
        if ($('#fiscalPeriods').val() != 0) {
            var fiscalPeriodStartDate = new Date($('#fiscalPeriods').val().split('_')[0]);
            if (ev.date.valueOf() < fiscalPeriodStartDate.valueOf()) {
                p_notification(false, "please select start date within selected fiscal period");
                $(this).val($('#fiscalPeriods').val().split('_')[0]);
            }
        }
        inDate.hide();
    }).data('datepicker');

    ///////DatePicker\\\\\\\
    var outDate = $('#trEndDate').datepicker({onRender: function(date) {
            return date.valueOf() < now.valueOf() ? ((U_DATE_R_OVERRIDE)?'':'disabled') : '';
        }
    }).on('changeDate', function(ev) {
        var fiscalPeriodEndDate = new Date($('#fiscalPeriods').val().split('_')[1]);
        if (ev.date.valueOf() > fiscalPeriodEndDate.valueOf()) {
            p_notification(false, "please select end date within selected fiscal period");
            $(this).val($('#fiscalPeriods').val().split('_')[1]);
        }
        outDate.hide();
    }).data('datepicker');

    $('.dimenisonchange').on('change', function(e) {
        dimensionTypeID = this.value;
        getDimensionValueListByDimensionType(dimensionTypeID, function() {
        });
    });

    function getDimensionValueListByDimensionType(dimensionTypeID, callback) {
        switch (dimensionTypeID) {
            case "job":
                $('.dimensionVal').addClass('hidden');
                $('.dimensionProjectNo').addClass('hidden');
                $('.dimensionJobNo').removeClass('hidden');
                break;
            case "project":
                $('.dimensionVal').addClass('hidden');
                $('.dimensionProjectNo').removeClass('hidden');
                $('.dimensionJobNo').addClass('hidden');
                break;
           default:
                $('.dimensionVal').removeClass('hidden');
                $('.dimensionProjectNo').addClass('hidden');
                $('.dimensionJobNo').addClass('hidden');
                $.ajax({
                    type: 'POST',
                    url: BASE_URL + '/dimension-api/getRelatedDimensionValues',
                    data: {dimensionId: dimensionTypeID},
                    success: function(respond) {
                        //empty options
                        $('#dimensionVal').empty();
                        $('#dimensionVal').append($("<option>", {value: '', html: 'Select dimension value'}));
                        $(respond.data).each(function(index, dimValue) {
                            $.each(dimValue, function(ind, val) {
                                $('#dimensionVal').append($("<option>", {value: ind, html: val.value}));
                            });
                        });
                        callback();
                    }
                });
                break;
        }
    }

    loadDropDownFromDatabase('/job-api/searchJobsForDropdown', "", 0, '.dimensionJobNo');
    $('.dimensionJobNo').selectpicker('refresh');
    $('.dimensionJobNo').on('change', function() {
        dimensionValue = $(this).val();
    });

    loadDropDownFromDatabase('/api/project/searchProjectsForDropdown', "", 0, '.dimensionProjectNo');
    $('.dimensionProjectNo').selectpicker('refresh');
    $('.dimensionProjectNo').on('change', function() {
        dimensionValue = $(this).val();
    });

    $('.dimensionVal').on('change', function() {
        dimensionValue = $(this).val();
    });

});

function redirectdocumentPreview(docType, docID)
{
  switch (docType){
    case '1' :
        showInvoicePreview(BASE_URL + "/invoice/invoiceView/" + docID);
        break;
    case '4' :
        showDeliveryNotePreview(BASE_URL + "/delivery-note/preview/" + docID);
        break;
    case '7' :
        showCustomerPaymentsPreview(BASE_URL + "/customerPayments/viewReceipt/" + docID);
        break;
    case '5' :
        showReturnPreview(BASE_URL + "/return/viewReturnReceipt/" + docID);
        break;
    case '6' :
        showCreditNotePreview(BASE_URL + "/credit-note/viewCreditNoteReceipt/" + docID);
        break;
    case '17' :
        showCreditNotePaymentPreview(BASE_URL + "/credit-note-payments/viewCreditNotePaymentReceipt/" + docID);
        break;
    case '12' :
        showPVPreview(BASE_URL + "/pi/preview/" + docID);
        break;
    case '14' :
        showSupplierPaymentsPreview(BASE_URL + "/supplierPayments/viewReceipt/" + docID);
        break;
    case '11' :
        showPurchaseReturnsPreview(BASE_URL + "/pr/preview/" + docID);
        break;
    case '13' :
        showDebitNotePreview(BASE_URL + "/debit-note/viewDebitNoteReceipt/" + docID);
        break;
    case '18' :
        showDebitNotePaymentPreview(BASE_URL + "/debit-note-payments/viewDebitNotePaymentReceipt/" + docID);
        break;
    case '10' :
        showGrnPreview(BASE_URL + "/grn/preview/" + docID);
        break;
    case '15' :
        showTransferPreview(BASE_URL + "/transfer/transferView/" + docID);
        break;
    case '16' :
        showAdjustmetPreview(BASE_URL + "/inventory-adjustment/preview/" + docID);
        break;
    case '20' :
        showExpPVPreview(BASE_URL + "/expense-purchase-invoice/preview/" + docID);
        break;
    default :
        p_notification(false, eb.getMessage('ERR_NO_PREVIEW'));
        break;
  }
}

function getJournalEntriesForGeneralLedgerPreview(finaceAcId)
{
    eb.ajax({
        type: 'POST',
        url: BASE_URL + '/finance-report-api/getGeneralLedgerJournalEntriesByAccountId',
        dataType: 'json',
        data: {
              startDate: $('#startDate').val(),
              endDate: $('#endDate').val(),
              accountID : finaceAcId,
              dimensionType : dimensionTypeID,
              dimensionValue : dimensionValue,
              isOrderByDate : $('#jeOrderByDate').is(":checked")
        },
        success: function(respond) {
           $.each(respond.data, function(index, val) {
             var openDebit = parseFloat($("#id_deb_"+index).attr('data-openDebValue'));
             var openCredit = parseFloat($("#id_cre_"+index).attr('data-openCreValue'));
             var accountType = $("#id_ftype_"+index).val();
              if  (openDebit > openCredit) {
                var accBal = parseFloat($("#id_bal_"+index).attr('data-openValue'));
              } else {
                var accBal = parseFloat($("#id_bal_"+index).attr('data-openValue')) * -1;
              }


              $.each(val.journalEntries, function(ind, value) {
                var jBalance = 0.00;
                 if(value.journalEntryAccountsDebitAmount >= value.journalEntryAccountsCreditAmount){
                    jBalance = value.journalEntryAccountsDebitAmount - value.journalEntryAccountsCreditAmount;
                 }else{
                    jBalance = (value.journalEntryAccountsCreditAmount - value.journalEntryAccountsDebitAmount) * -1;
                 }

                 accBal += jBalance;


                loadJournalEntriesForPreview(value, accBal, accountType);
              });
           });
        },
        async:false
    });
}

function loadJournalEntriesForPreview(value, accBal, accountType)
{
   var balState = $('#showAccBl').is(":checked")
   var jentryBalance = 0.00;
   if(value.journalEntryAccountsDebitAmount >= value.journalEntryAccountsCreditAmount){
      jentryBalance = value.journalEntryAccountsDebitAmount - value.journalEntryAccountsCreditAmount;
   }else{
      jentryBalance = (value.journalEntryAccountsCreditAmount - value.journalEntryAccountsDebitAmount) * -1;
   }

   if (accountType == 1 || accountType == 3 || accountType == 6) {

      var balText = (accBal < 0) ? '( '+accounting.formatMoney(accBal * -1)+' )' : accounting.formatMoney(accBal);  
      var normalbalText = (jentryBalance < 0) ? '( '+accounting.formatMoney(jentryBalance * -1)+' )' : accounting.formatMoney(jentryBalance);  
   } else if (accountType == 2 || accountType == 4 || accountType == 5  || accountType == 7) {
      var balText = (accBal > 0) ? '( '+accounting.formatMoney(accBal)+' )' : accounting.formatMoney(accBal * -1);  
      var normalbalText = (jentryBalance > 0) ? '( '+accounting.formatMoney(jentryBalance)+' )' : accounting.formatMoney(jentryBalance * -1);
   }


    var newTrID = 'tr_' + value.journalEntryID;
    var clonedRow = $($('#'+value.financeAccountsID+'preSetSample').clone()).attr('id', newTrID).addClass('addedJEs');
    $("#glJournalEntryDate", clonedRow).text(value.journalEntryDate);
    $("#glJournalEntryCode", clonedRow).text(value.journalEntryCode);
    $("#glJournalEntryComment", clonedRow).text(value.journalEntryComment);
    $("#glJournalEntryComment", clonedRow).attr('data-typeid',value.documentTypeID);
    $("#glJournalEntryComment", clonedRow).attr('data-docid',value.journalEntryDocumentID);
    $("#glDocumentType", clonedRow).text(value.documentType);
    $("#glCusName", clonedRow).text(value.cusName);
    $("#glDocumentCode", clonedRow).text(value.documentCode);
    var relatedDocComment = (value.relatedDocComment == "") ? '-' : value.relatedDocComment;
    $("#glRelatedDocComment", clonedRow).text(relatedDocComment);
    $("#glChildDoc", clonedRow).text(value.childDoc);
    $("#glJournalEntryAccountsDebitAmount", clonedRow).text(accounting.formatMoney(value.journalEntryAccountsDebitAmount));
    $("#glJournalEntryAccountsCreditAmount", clonedRow).text(accounting.formatMoney(value.journalEntryAccountsCreditAmount));

    if (balState) {
      $("#glJentryBalance", clonedRow).text(balText);
    } else {
      $("#glJentryBalance", clonedRow).text(normalbalText);
    }
    clonedRow.removeClass('hidden');
    clonedRow.insertBefore('#'+value.financeAccountsID+'preSetSample');
}


function validation()
{
	var startDate = $('#startDate').val();
	var endDate = $('#endDate').val();
	if(startDate == ''){
		p_notification(false, eb.getMessage('ERR_FINANCE_REPORT_START_DATE_CNT_BE_NULL'));
        $('#startDate').focus();
        return false;
	}else if(endDate == ''){
		p_notification(false, eb.getMessage('ERR_FINANCE_REPORT_END_DATE_CNT_BE_NULL'));
        $('#endDate').focus();
        return false;
	} else if(eb.convertDateFormat('#endDate') < eb.convertDateFormat('#startDate')){
		p_notification(false, eb.getMessage('ERR_FINANCE_REPORT_DATE_RANGE'));
        $('#endDate').focus();
        return false;
	}
	return true;
}

function pAndLvalidation()
{

  var locationIDs = $("#locationList").val();

  var startDate = $('#startDate').val();
  var endDate = $('#endDate').val();
  if(startDate == ''){
    p_notification(false, eb.getMessage('ERR_FINANCE_REPORT_START_DATE_CNT_BE_NULL'));
        $('#startDate').focus();
        return false;
  }else if(endDate == ''){
    p_notification(false, eb.getMessage('ERR_FINANCE_REPORT_END_DATE_CNT_BE_NULL'));
        $('#endDate').focus();
        return false;
  } else if(eb.convertDateFormat('#endDate') < eb.convertDateFormat('#startDate')){
    p_notification(false, eb.getMessage('ERR_FINANCE_REPORT_DATE_RANGE'));
        $('#endDate').focus();
        return false;
  } else if ($("#locationList").val() == null && $('#showAccuBal').is(":checked")) {
    p_notification(false, eb.getMessage('ERR_SELECT_LOCATION_FOR_LOCATION_COLUMN'));
        return false; 
  } else if (locationIDs != null && locationIDs.length > 3 && $('#showAccuBal').is(":checked")) {
    p_notification(false, eb.getMessage('ERR_SELECT_LOCATION_FOR_LOCATION_COLUMN_LIMIT'));
        return false;
  }



  return true;
}

function pAndLvalidationForCsv()
{

  var locationIDs = $("#locationList").val();

  var startDate = $('#startDate').val();
  var endDate = $('#endDate').val();
  if(startDate == ''){
    p_notification(false, eb.getMessage('ERR_FINANCE_REPORT_START_DATE_CNT_BE_NULL'));
        $('#startDate').focus();
        return false;
  }else if(endDate == ''){
    p_notification(false, eb.getMessage('ERR_FINANCE_REPORT_END_DATE_CNT_BE_NULL'));
        $('#endDate').focus();
        return false;
  } else if(eb.convertDateFormat('#endDate') < eb.convertDateFormat('#startDate')){
    p_notification(false, eb.getMessage('ERR_FINANCE_REPORT_DATE_RANGE'));
        $('#endDate').focus();
        return false;
  } else if ($("#locationList").val() == null && $('#showAccuBal').is(":checked")) {
    p_notification(false, eb.getMessage('ERR_SELECT_LOCATION_FOR_LOCATION_COLUMN'));
        return false; 
  }
  
  return true;
}




function trialvalidation()
{
  var startDate = $('#trStartDate').val();
  var endDate = $('#trEndDate').val();
  var locationIDs = $("#locationList").val();
  if(startDate == ''){
    p_notification(false, eb.getMessage('ERR_FINANCE_REPORT_START_DATE_CNT_BE_NULL'));
        $('#trStartDate').focus();
        return false;
  }else if(endDate == ''){
    p_notification(false, eb.getMessage('ERR_FINANCE_REPORT_END_DATE_CNT_BE_NULL'));
        $('#trEndDate').focus();
        return false;
  } else if(eb.convertDateFormat('#trEndDate') < eb.convertDateFormat('#trStartDate')){
    p_notification(false, eb.getMessage('ERR_FINANCE_REPORT_DATE_RANGE'));
        $('#trEndDate').focus();
        return false;
  }

  if ($("#locationList").val() == null && $('#showAccuBal').is(":checked")) {
    p_notification(false, eb.getMessage('ERR_SELECT_LOCATION_FOR_LOCATION_COLUMN'));
        return false; 
  }

  if (locationIDs != null && locationIDs.length > 3 && $('#showAccuBal').is(":checked")) {
    p_notification(false, eb.getMessage('ERR_SELECT_LOCATION_FOR_LOCATION_COLUMN_LIMIT'));
        return false;
  }

  return true;
}

function trialvalidationCsv()
{
  var startDate = $('#trStartDate').val();
  var endDate = $('#trEndDate').val();
  var locationIDs = $("#locationList").val();
  if(startDate == ''){
    p_notification(false, eb.getMessage('ERR_FINANCE_REPORT_START_DATE_CNT_BE_NULL'));
        $('#trStartDate').focus();
        return false;
  }else if(endDate == ''){
    p_notification(false, eb.getMessage('ERR_FINANCE_REPORT_END_DATE_CNT_BE_NULL'));
        $('#trEndDate').focus();
        return false;
  } else if(eb.convertDateFormat('#trEndDate') < eb.convertDateFormat('#trStartDate')){
    p_notification(false, eb.getMessage('ERR_FINANCE_REPORT_DATE_RANGE'));
        $('#trEndDate').focus();
        return false;
  }

  if ($("#locationList").val() == null && $('#showAccuBal').is(":checked")) {
    p_notification(false, eb.getMessage('ERR_SELECT_LOCATION_FOR_LOCATION_COLUMN'));
        return false; 
  }

  return true;
}

function balanceSheetValidation()
{
  var locationIDs = $("#locationList").val();

	var endDate = $('#endDate').val();
	if(endDate == ''){
		p_notification(false, eb.getMessage('ERR_FINANCE_REPORT_END_DATE_CNT_BE_NULL'));
        $('#endDate').focus();
        return false;
	}
  if ($("#locationList").val() == null && $('#showAccuBal').is(":checked")) {
    p_notification(false, eb.getMessage('ERR_SELECT_LOCATION_FOR_LOCATION_COLUMN'));
        return false; 
  }

  if (locationIDs != null && locationIDs.length > 3 && $('#showAccuBal').is(":checked")) {
    p_notification(false, eb.getMessage('ERR_SELECT_LOCATION_FOR_LOCATION_COLUMN_LIMIT'));
        return false;
  }

	return true;
}

function balanceSheetValidationForCsv()
{
  var locationIDs = $("#locationList").val();

  var endDate = $('#endDate').val();
  if(endDate == ''){
    p_notification(false, eb.getMessage('ERR_FINANCE_REPORT_END_DATE_CNT_BE_NULL'));
        $('#endDate').focus();
        return false;
  }
  if ($("#locationList").val() == null && $('#showAccuBal').is(":checked")) {
    p_notification(false, eb.getMessage('ERR_SELECT_LOCATION_FOR_LOCATION_COLUMN'));
        return false; 
  }

  return true;
}
