var dimensionTypeID = null;
var dimensionValue = null;
$(document).ready(function() {
    var reportType = 'expensePaymentVoucher';
    initialLoading();
//    Set Date picker
    var checkin = $('#fromDate').datepicker({
    }).on('changeDate', function(ev) {
        if (ev.date.valueOf() < checkout.date.valueOf()) {
            var currentDate = new Date(ev.date);
            var newDate = new Date();
            newDate.setDate(currentDate.getDate() + 1);
            checkout.setValue(newDate);
        }
        checkin.hide();
        $('#toDate')[0].focus();
    }).data('datepicker');

    var checkout = $('#toDate').datepicker({
        onRender: function(date) {
            return date.valueOf() <= checkin.date.valueOf() ? 'disabled' : '';
        }
    }).on('changeDate', function(ev) {
        checkout.hide();
    }).data('datepicker');

    $('#pettyCashVoucher').click(function() {
        reportType = 'expensePaymentVoucher';
        resetInputField();
    });

    function initialLoading() {
        $('#expensePaymentVoucherReportIFrame').hide();
    }

    $('#viewReport').click(function() {
        var inputs = {
            expenseType: $('#expeneType').val(),
            fromDate: $('#fromDate').val(),
            toDate: $('#toDate').val(),
            dimensionType : dimensionTypeID,
            dimensionValue : dimensionValue
        };
        if(validateFields(inputs)){
            if (reportType === 'expensePaymentVoucher') {
                var getpdfdataurl = BASE_URL + '/reports/get/expense-payment-voucher-report/view-report';

                var getpdfdatarequest = eb.post(getpdfdataurl, inputs);
                getpdfdatarequest.done(function(respond) {
                    if (respond.status == true) {
                        setContentToReport('expensePaymentVoucherReportIFrame', respond);
                        // $('#expensePaymentVoucherReportIFrame').removeClass('hidden');
                        // $('#expensePaymentVoucherReportIFrame').show();
                        // $('#expensePaymentVoucherReportIFrame').html(respond.html);
                    }
                });
            }
            
        }

    });

    $('.dimenisonchange').on('change', function(e) {
        dimensionTypeID = this.value;
        getDimensionValueListByDimensionType(dimensionTypeID, function() {
        });
    });

    function getDimensionValueListByDimensionType(dimensionTypeID, callback) {
        switch (dimensionTypeID) {
            case "job":
                $('.dimensionVal').addClass('hidden');
                $('.dimensionProjectNo').addClass('hidden');
                $('.dimensionJobNo').removeClass('hidden');
                break;
            case "project":
                $('.dimensionVal').addClass('hidden');
                $('.dimensionProjectNo').removeClass('hidden');
                $('.dimensionJobNo').addClass('hidden');
                break;
           default:
                $('.dimensionVal').removeClass('hidden');
                $('.dimensionProjectNo').addClass('hidden');
                $('.dimensionJobNo').addClass('hidden');
                $.ajax({
                    type: 'POST',
                    url: BASE_URL + '/dimension-api/getRelatedDimensionValues',
                    data: {dimensionId: dimensionTypeID},
                    success: function(respond) {
                        //empty options
                        $('#dimensionVal').empty();
                        $('#dimensionVal').append($("<option>", {value: '', html: 'Select dimension value'}));
                        $(respond.data).each(function(index, dimValue) {
                            $.each(dimValue, function(ind, val) {
                                $('#dimensionVal').append($("<option>", {value: ind, html: val.value}));
                            });
                        });
                        callback();
                    }
                });
                break;
        }
    }

    loadDropDownFromDatabase('/job-api/searchJobsForDropdown', "", 0, '.dimensionJobNo');
    $('.dimensionJobNo').selectpicker('refresh');
    $('.dimensionJobNo').on('change', function() {
        dimensionValue = $(this).val();
    });

    loadDropDownFromDatabase('/api/project/searchProjectsForDropdown', "", 0, '.dimensionProjectNo');
    $('.dimensionProjectNo').selectpicker('refresh');
    $('.dimensionProjectNo').on('change', function() {
        dimensionValue = $(this).val();
    });

    $('.dimensionVal').on('change', function() {
        dimensionValue = $(this).val();
    });

    $('#generatePdf').click(function() {
        var inputs = {
            expenseType: $('#expeneType').val(),
            fromDate: $('#fromDate').val(),
            toDate: $('#toDate').val(),
            dimensionType : dimensionTypeID,
            dimensionValue : dimensionValue
        };
        if(validateFields(inputs)){
            if (reportType === 'expensePaymentVoucher') {
                eb.ajax({
                    type: 'POST',
                    url: BASE_URL + '/reports/get/expense-payment-voucher-report/generete-pdf',
                    dataType: 'json',
                    data: inputs,
                    success: function(respond) {
                        if (respond.status == true) {
                            window.open(BASE_URL + '/' + respond.data, '_blank');
                        }
                    }
                });
            }
            
        }
    });

    $('#csvReport').click(function() {
        var inputs = {
            expenseType: $('#expeneType').val(),
            fromDate: $('#fromDate').val(),
            toDate: $('#toDate').val(),
            dimensionType : dimensionTypeID,
            dimensionValue : dimensionValue
        };
        if(validateFields(inputs)){
            if (reportType === 'expensePaymentVoucher') {
                var url = BASE_URL + '/reports/get/expense-payment-voucher-report/generate-csv';
                eb.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: inputs,
                    success: function(respond) {
                        if (respond.status == true) {
                            window.open(BASE_URL + '/' + respond.data, '_blank');
                        }
                    }
                });
            }
            
        }

    });
});
function validateFields(dataSet)
{
    if(dataSet.fromDate == ''){
        p_notification(false, eb.getMessage('ERR_FROM_DATE_PV_REPORT'));
        return false;
    } else if(dataSet.fromDate == ''){
        p_notification(false, eb.getMessage('ERR_TO_DATE_PV_REPORT'));
        return false;
    } else {
        return true;
    }
}

function resetInputField() {
    $('#pettyCashVoucherType').val('').selectpicker('render');
    $('#pettyCashExpenetype').val('').selectpicker('render');
    $('#fromDate').val('');
    $('#toDate').val('');
}