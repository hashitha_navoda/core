/*
 * @author SANDUN <sandun@thinkcube.com>
 * Customer Reporting functions
 */
$(document).ready(function() {
    var reportType = 'agedCustomerAnalysis';
    var isAllCustomers;
    var isAllCategory;
    var isAllLocation;
    var isAllItem;
    var categoryIDList;
    var cusIds = {};
    $('.customer-category-div').addClass('hidden');
///////DatePicker for two text Filed\\\\\\\
    var nowTemp = new Date();
    var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);
    var checkin = $('#from_date').datepicker({
    }).on('changeDate', function(ev) {
        if (ev.date.valueOf() < checkout.date.valueOf()) {
            var newDate = new Date(ev.date)
            newDate.setDate(newDate.getDate() + 1);
            checkout.setValue(newDate);
        }
        checkin.hide();
        $('#to_date')[0].focus();
    }).data('datepicker');
    var checkout = $('#to_date').datepicker({
        onRender: function(date) {
            return date.valueOf() <= checkin.date.valueOf() ? 'disabled' : '';
        }
    }).on('changeDate', function(ev) {
        checkout.hide();
    }).data('datepicker');


    var enddate = $('#endDate').datepicker({
    }).on('changeDate', function(ev) {
        if (ev.date.valueOf() < checkout.date.valueOf()) {
            var newDate = new Date(ev.date)
            newDate.setDate(newDate.getDate() + 1);
        }
        enddate.hide();
    }).data('datepicker');
    //////END datepicker/////////////////
    initialLoading();
    setSelectPicker();//set select picker

    $(".report_pills").on("click", function() {
        $(".report_pills").removeClass("active");
        $(this).addClass("active");
    });
    loadDropDownFromDatabase('/customerAPI/search-customers-for-dropdown', "", "withDeactivatedCustomers", '#customerName', "", true);
    $('#customerName').on('change', function() {
        var thisValue = $(this).val();
        if (thisValue == null) {
            reSetReportContent('customerReportIFrame');
        }
    });

    loadDropDownFromDatabase('/api/company/category/searchCategoriesForDropdown', "", 0, '#categorys', "", true);
    $('#categorys').on('change', function() {
        var thisValue = $(this).val();
        categoryIDList = thisValue;
        if (thisValue == null) {
            reSetReportContent('customerReportIFrame');
        }
    });

    $.each(LOCATION, function(index, value) {
        $('#locations').append($("<option value='" + index + "'>" + value + "</option>"));
    });
    $('#locations').on('change', function() {
        var thisValue = $(this).val();
        if (thisValue == null) {
            reSetReportContent('customerReportIFrame');
        }
    });

    $('#agedCustomerAnalysis').click(function(e) {
        reportType = 'agedCustomerAnalysis';
        customerIFrameHide();
        $('#endDate').show();
        $('#endDate').val('');
        $('#customerList').show();
        $('#from_date').hide();
        $('#to_date').hide();
        $('.remove_label').hide();
        $('.from_date_label').addClass('hidden');
        $('.end_date_label').text('Date').removeClass('hidden');
        $('.customerSelection').removeClass('hidden');
        $('.item_category').addClass('hidden');
        $('.advance_columns').removeClass('hidden');
        $('#salesPers').removeClass('hidden');
        $('.summary_columns').addClass('hidden');
        $('.order_filter').addClass('hidden');
        if (isAllCustomers) {
            $('#customCustomerSelection').trigger('click');
        }
        $('#customerName').val('').trigger('change');
        $('.location').addClass('hidden');
    });

    $('#agedAnalysis').click(function(e) {
        reportType = 'agedAnalysis';
        customerIFrameHide();
        $('#reportHead').fadeIn();
        $('#customerList').hide();
        $('#endDate').val('');
        $('#endDate').show();
        $('#from_date').hide();
        $('.remove_label').hide();
        $('.from_date_label').addClass('hidden');
        $('.end_date_label').text('Date').removeClass('hidden');
        $('.customerSelection').removeClass('hidden');
        $('#customerName').selectpicker('deselectAll');
        $('.item_category').addClass('hidden');
        $('.advance_columns').removeClass('hidden');
        $('#salesPers').removeClass('hidden');
        $('.summary_columns').addClass('hidden');
        if (isAllCustomers) {
            $('#customCustomerSelection').trigger('click');
        }
        $('#customerName').val('').trigger('change');
        $('.location').addClass('hidden');
        $('.order_filter').removeClass('hidden');
    });

    $('#customerDetails').click(function(e) {
        reportType = 'customerDetails';
        customerIFrameHide();
        $('#reportHead').fadeIn();
        $('#customerList').show();
        $('#endDate').val('');
        $('#endDate').hide();
        $('#from_date').hide();
        $('.remove_label').hide();
        $('.from_date_label').addClass('hidden');
        $('.end_date_label').addClass('hidden');
        $('.customerSelection').removeClass('hidden');
        $('.item_category').addClass('hidden');
        $('.location').addClass('hidden');
        $('.advance_columns').addClass('hidden');
        $('.summary_columns').addClass('hidden');
        $('#salesPers').addClass('hidden');
        if (isAllCustomers) {
            $('#customCustomerSelection').trigger('click');
        }
        $('#customerName').val('').trigger('change');
        $('.order_filter').addClass('hidden');
    });

    $('#customerWiseItem').click(function(e) {
        reportType = 'customerWiseItem';
        customerIFrameHide();
        $('#reportHead').fadeIn();
        $('#customerList').show();
        $('#from_date').val('').show();
        $('#endDate').val('').show();
        $('.remove_label').hide();
        $('.from_date_label').removeClass('hidden');
        $('.end_date_label').text('End Date').removeClass('hidden');
        $('.customerSelection').removeClass('hidden');
        $('.item_category').removeClass('hidden');
        $('.location').addClass('hidden');
        $('.advance_columns').addClass('hidden');
        $('.summary_columns').addClass('hidden');
        $('#AllCustomers').trigger('click');
        $('#customerName').val('').trigger('change');
        $('#AllCategory').trigger('click');
        $('#categorys').val('').trigger('change');
        $('#AllItem').trigger('click');
        $('#items').val('').trigger('change');
        $('#salesPers').addClass('hidden');
        $('.order_filter').addClass('hidden');
    });

     $('#customerWiseSales').click(function(e) {
        reportType = 'customerWiseSales';
        customerIFrameHide();
        $('#reportHead').fadeIn();
        $('#customerList').show();
        $('#from_date').val('').show();
        $('#endDate').val('').show();
        $('.remove_label').hide();
        $('.from_date_label').removeClass('hidden');
        $('.end_date_label').text('End Date').removeClass('hidden');
        $('.customerSelection').removeClass('hidden');
        $('.location').removeClass('hidden');
        $('.advance_columns').addClass('hidden');
        $('.summary_columns').addClass('hidden');
        $('#AllCustomers').trigger('click');
        $('#customerName').val('').trigger('change');
        $('#AllLocation').trigger('click');
        $('#locations').val('').trigger('change');
        $('.item_category').addClass('hidden');
        $('#salesPers').addClass('hidden');
        $('.order_filter').addClass('hidden');
    });

    $('#customerAllTransactions').click(function(e) {
        reportType = 'customerAllTransactions';
        customerIFrameHide();
       customerIFrameHide();
        $('#reportHead').fadeIn();
        $('#customerList').show();
        $('#from_date').val('').show();
        $('#endDate').val('').show();
        $('.remove_label').hide();
        $('.from_date_label').removeClass('hidden');
        $('.advance_columns').addClass('hidden');
        $('.summary_columns').removeClass('hidden');
        $('.end_date_label').text('End Date').removeClass('hidden');
        $('.customerSelection').removeClass('hidden');
        $('.location').removeClass('hidden');
        $('#AllCustomers').trigger('click');
        $('#customerName').val('').trigger('change');
        $('#AllLocation').trigger('click');
        $('#locations').val('').trigger('change');
        $('.item_category').addClass('hidden');
        $('#salesPers').addClass('hidden');
        $('.order_filter').addClass('hidden');
    });
    
    $('#customerBalance').click(function(e) {
        reportType = 'customerBalance';
        customerIFrameHide();
        customerIFrameHide();
        $('#reportHead').fadeIn();
        $('#customerList').show();
        $('#from_date').val('').show();
        $('#endDate').val('').show();
        $('.remove_label').hide();
        $('.from_date_label').removeClass('hidden');
        $('.advance_columns').addClass('hidden');
        $('.summary_columns').addClass('hidden');
        $('.end_date_label').text('End Date').removeClass('hidden');
        $('.customerSelection').removeClass('hidden');
        $('.location').removeClass('hidden');
        $('#AllCustomers').trigger('click');
        $('#customerName').val('').trigger('change');
        $('#AllLocation').trigger('click');
        $('#locations').val('').trigger('change');
        $('.item_category').addClass('hidden');
        $('#salesPers').addClass('hidden');
        $('.order_filter').addClass('hidden');
    });

    $('#customerAgedAnalysisForDonePayment').click(function(e) {
        reportType = 'customerAgedAnalysisForDonePayment';
        customerIFrameHide();
        $('#endDate').hide();
        $('#endDate').val('');
        $('#customerList').show();
        $('#from_date').hide();
        $('#to_date').hide();
        $('.remove_label').hide();
        $('.from_date_label').addClass('hidden');
        $('.end_date_label').text('Date').addClass('hidden');
        $('.customerSelection').removeClass('hidden');
        $('.item_category').addClass('hidden');
        $('.advance_columns').removeClass('hidden');
        $('#salesPers').addClass('hidden');
        $('.summary_columns').addClass('hidden');
        $('.order_filter').addClass('hidden');
        if (isAllCustomers) {
            $('#customCustomerSelection').trigger('click');
        }
        $('#customerName').val('').trigger('change');
        $('.location').addClass('hidden');
    });

    $('#viewReport').click(function(e) {
        e.preventDefault();
        if (reportType == 'agedCustomerAnalysis') {
            cusIds = $('#customerName').val();
            var endDate = $('#endDate').val();
            var advanceData = false;
            if($('#advRow').prop('checked') == true){
                advanceData = true;
            } else {
                advanceData = false;
            }

            var cusCategory = $('#customerCategoryList').val();
            var salesPersons = $('#salesPersons').val();
            if (isAllCustomers != true && (cusIds == null || cusIds == "")) {
                p_notification(false, eb.getMessage('ERR_CUSREPO_SELECUST'));
            } else if (endDate == null || endDate == "") {
                p_notification(false, eb.getMessage('ERR_CUSREPO_DATEFILL'));
                document.getElementById("endDate").focus();
            } else {
                var url = BASE_URL + '/api/customer-report/view-aged-customer-data';
                var getpdfdatarequest = eb.post(url, {
                    cusIds: cusIds,
                    endDate: $('#endDate').val(),
                    isAllCustomers: isAllCustomers,
                    cusCategory : cusCategory,
                    advanceDataFlag : advanceData,
                    salesPersons : salesPersons,
                });
                getpdfdatarequest.done(function(respond) {
                    if (respond.status == true) {
                        setContentToReport('customerReportIFrame', respond);
                    }
                });
            }
        }
        else if (reportType == 'agedAnalysis') {
            cusIds = $('#customerName').val();
            var endDate = $('#endDate').val();
            var advanceData = false;
            if($('#advRow').prop('checked') == true){
                advanceData = true;
            } else {
                advanceData = false;
            }
            var cusCategory = $('#customerCategoryList').val();
            var salesPersons = $('#salesPersons').val();
            var sortBy = $('#orderFilter').val();
            if (isAllCustomers != true && (cusIds == null || cusIds == "")) {
                p_notification(false, eb.getMessage('ERR_CUSREPO_SELECUST'));
            } else if (endDate == null || endDate == "") {
                p_notification(false, eb.getMessage('ERR_CUSREPO_DATEFILL'));
                document.getElementById("endDate").focus();
            } else {
                var url = BASE_URL + '/api/customer-report/view-aged-analysis';
                var getpdfdatarequest = eb.post(url, {
                    cusIds: cusIds,
                    endDate: $('#endDate').val(),
                    isAllCustomers: isAllCustomers,
                    cusCategory : cusCategory,
                    advanceDataFlag : advanceData,
                    salesPersons : salesPersons,
                    sortBy : sortBy,
                });
                getpdfdatarequest.done(function(respond) {
                    if (respond.status == true) {
                        setContentToReport('customerReportIFrame', respond);
                    }
                });
            }

        }
        else if (reportType == 'customerDetails') {
            cusIds = $('#customerName').val();
            var cusCategory = $('#customerCategoryList').val();
            if (isAllCustomers != true && (cusIds == '' || cusIds == null)) {
                p_notification(false, eb.getMessage('ERR_CUSREPO_SELECUST'));
            } else {
                var url = BASE_URL + '/api/customer-report/view-customer-details'
                eb.ajax({
                    type: 'POST',
                    url: url,
                    data: {cusIds: cusIds, isAllCustomers: isAllCustomers, cusCategory: cusCategory},
                    success: function(respond) {
                        setContentToReport('customerReportIFrame', respond);
                    },
                    async: false
                });
            }

        }
        else if (reportType == 'customerWiseItem') {
        	cusIds = $('#customerName').val();
        	categoryIds = $('#categorys').val();
            itemIDs = $('#items').val();
        	var fromDate = $('#from_date').val();
        	var endDate = $('#endDate').val();
            var cusCategory = $('#customerCategoryList').val();
            if (isAllCustomers != true && (cusIds == '' || cusIds == null)) {
                p_notification(false, eb.getMessage('ERR_CUSREPO_SELECUST'));
            } else if(isAllCategory != true && (categoryIds == '' || categoryIds == null)){
                p_notification(false, eb.getMessage('ERR_CUSREPO_SELECATEGORY'));
            } else if(isAllItem != true && (itemIDs == '' || itemIDs ==null)){
                p_notification(false, eb.getMessage('ERR_ITEM_SELECATEGORY'));
            } else if((fromDate != null && fromDate != null) && (endDate < fromDate)){
 				p_notification(false, eb.getMessage('ERR_CUSREPO_FROMDATE_CANT_BE_MORETHAN_ENDDATE'));
            } else {
                var url = BASE_URL + '/api/customer-report/view-customer-wise-sales-item-details'
                eb.ajax({
                    type: 'POST',
                    url: url,
                    data: {
                    	cusIds: cusIds,
                    	isAllCustomers: isAllCustomers,
                    	categoryIds: categoryIds,
                    	isAllCategory: isAllCategory,
                        isAllItem:isAllItem,
                        itemIDs:itemIDs,
                    	fromDate: fromDate,
                    	endDate: endDate,
                        cusCategory: cusCategory
                    },
                    success: function(respond) {
                        setContentToReport('customerReportIFrame', respond);
                    },
                    async: false
                });
            }
        }
        else if (reportType == 'customerWiseSales') {
        	cusIds = $('#customerName').val();
        	locations = $('#locations').val();
        	var fromDate = $('#from_date').val();
        	var endDate = $('#endDate').val();
            var cusCategory = $('#customerCategoryList').val();
            if (isAllCustomers != true && (cusIds == '' || cusIds == null)) {
                p_notification(false, eb.getMessage('ERR_CUSREPO_SELECUST'));
            } else if(isAllLocation != true && (locations == '' || locations == null)){
                p_notification(false, eb.getMessage('ERR_CUSREPO_SELELOCATION'));
            } else if((fromDate != null && fromDate != null) && (endDate < fromDate)){
 				p_notification(false, eb.getMessage('ERR_CUSREPO_FROMDATE_CANT_BE_MORETHAN_ENDDATE'));
            } else {
                var url = BASE_URL + '/api/customer-report/view-customer-wise-sales-report'
                eb.ajax({
                    type: 'POST',
                    url: url,
                    data: {
                    	cusIds: cusIds,
                    	isAllCustomers: isAllCustomers,
                    	locations: locations,
                    	isAllLocation: isAllLocation,
                    	fromDate: fromDate,
                    	endDate: endDate,
                        cusCategory: cusCategory
                    },
                    success: function(respond) {
                        setContentToReport('customerReportIFrame', respond);
                    },
                    async: false
                });
            }
        } else if( reportType == 'customerAllTransactions') {
            cusIds = $('#customerName').val();
            locations = $('#locations').val();
            var fromDate = $('#from_date').val();
            var endDate = $('#endDate').val();
            var cusCategory = $('#customerCategoryList').val();
            var summaryData = false;
            if($('#summaryRow').prop('checked') == true){
                summaryData = true;
            } else {
                summaryData = false;
            }
            if (isAllCustomers != true && (cusIds == '' || cusIds == null)) {
                p_notification(false, eb.getMessage('ERR_CUSREPO_SELECUST'));
            } else if(isAllLocation != true && (locations == '' || locations == null)){
                p_notification(false, eb.getMessage('ERR_CUSREPO_SELELOCATION'));
            } else if((fromDate != null && fromDate != null) && (endDate < fromDate)){
                p_notification(false, eb.getMessage('ERR_CUSREPO_FROMDATE_CANT_BE_MORETHAN_ENDDATE'));
            } else {
                var url = BASE_URL + '/api/customer-report/view-customer-all-transactions'
                eb.ajax({
                    type: 'POST',
                    url: url,
                    data: {
                        cusIds: cusIds,
                        isAllCustomers: isAllCustomers,
                        locations: locations,
                        isAllLocation: isAllLocation,
                        fromDate: fromDate,
                        endDate: endDate,
                        cusCategory: cusCategory,
                        summaryData : summaryData
                    },
                    success: function(respond) {
                        setContentToReport('customerReportIFrame', respond);
                    },
                    async: false
                });
            }
        }  else if( reportType == 'customerBalance') {
            cusIds = $('#customerName').val();
            locations = $('#locations').val();
            var fromDate = $('#from_date').val();
            var endDate = $('#endDate').val();
            var cusCategory = $('#customerCategoryList').val();
            if (isAllCustomers != true && (cusIds == '' || cusIds == null)) {
                p_notification(false, eb.getMessage('ERR_CUSREPO_SELECUST'));
            } else if(isAllLocation != true && (locations == '' || locations == null)){
                p_notification(false, eb.getMessage('ERR_CUSREPO_SELELOCATION'));
            } else if(fromDate == "" || endDate == ""){
                p_notification(false, eb.getMessage('ERR_FROM_TO_DATE'));
            } else if((fromDate != null && endDate != null) && (endDate < fromDate)){
                p_notification(false, eb.getMessage('ERR_CUSREPO_FROMDATE_CANT_BE_MORETHAN_ENDDATE'));
            } else {
                var url = BASE_URL + '/api/customer-report/viewCustomerBalance'
                eb.ajax({
                    type: 'POST',
                    url: url,
                    data: {
                        cusIds: cusIds,
                        isAllCustomers: isAllCustomers,
                        locations: locations,
                        isAllLocation: isAllLocation,
                        fromDate: fromDate,
                        endDate: endDate,
                        cusCategory: cusCategory
                    },
                    success: function(respond) {
                        setContentToReport('customerReportIFrame', respond);
                    },
                    async: false
                });
            }
        } else if (reportType == 'customerAgedAnalysisForDonePayment') {
            cusIds = $('#customerName').val();
            var advanceData = false;
            if($('#advRow').prop('checked') == true){
                advanceData = true;
            } else {
                advanceData = false;
            }

            var cusCategory = $('#customerCategoryList').val();
            if (isAllCustomers != true && (cusIds == null || cusIds == "")) {
                p_notification(false, eb.getMessage('ERR_CUSREPO_SELECUST'));
            } else {
                var url = BASE_URL + '/api/customer-report/generate-customer-aged-analysis-for-done-payments-report';
                var getpdfdatarequest = eb.post(url, {
                    cusIds: cusIds,
                    isAllCustomers: isAllCustomers,
                    cusCategory : cusCategory,
                    advanceDataFlag : advanceData,
                });
                getpdfdatarequest.done(function(respond) {
                    if (respond.status == true) {
                        setContentToReport('customerReportIFrame', respond);
                    }
                });
            }
        } else {
            p_notification(false, eb.getMessage('ERR_CUSREPO_SELETYPE'));
        }
    });

    $('#generatePdf').click(function(e) {
        if (reportType == 'agedCustomerAnalysis') {
            cusIds = $('#customerName').val();
            var endDate = $('#endDate').val();
            var cusCategory = $('#customerCategoryList').val();
            var salesPersons = $('#salesPersons').val();
            var advanceData = false;
            if($('#advRow').prop('checked') == true){
                advanceData = true;
            } else {
                advanceData = false;
            }

            if (isAllCustomers != true && (cusIds == null || cusIds == "")) {
                p_notification(false, eb.getMessage('ERR_CUSREPO_SELECUST'));
            } else if (endDate == null || endDate == "") {
                p_notification(false, eb.getMessage('ERR_CUSREPO_DATEFILL'));
                document.getElementById("endDate").focus();
            } else {
                var url = BASE_URL + '/api/customer-report/generate-aged-cus-pdf';
                eb.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: {endDate: endDate, cusIds: cusIds, isAllCustomers: isAllCustomers, cusCategory: cusCategory, advanceData: advanceData, salesPersons: salesPersons},
                    success: function(respond) {
                        if (respond.status == true) {
                            window.open(BASE_URL + '/' + respond.data, '_blank');
                        }
                    }
                });
            }
        }
        else if (reportType == 'agedAnalysis') {
            cusIds = $('#customerName').val();
            var endDate = $('#endDate').val();
            var cusCategory = $('#customerCategoryList').val();
            var advanceData = false;
            var salesPersons = $('#salesPersons').val();
            var sortBy = $('#orderFilter').val();
            if($('#advRow').prop('checked') == true){
                advanceData = true;
            } else {
                advanceData = false;
            }
            
            if (isAllCustomers != true && (cusIds == null || cusIds == "")) {
                p_notification(false, eb.getMessage('ERR_CUSREPO_SELECUST'));
            } else if (endDate == null || endDate == "") {
                p_notification(false, eb.getMessage('ERR_CUSREPO_DATEFILL'));
                document.getElementById("endDate").focus();
            } else {
                var url = BASE_URL + '/api/customer-report/generate-aged-analysis-pdf';
                eb.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: {endDate: endDate, cusIds: cusIds, isAllCustomers: isAllCustomers, cusCategory: cusCategory, advanceDataFlag: advanceData, salesPersons: salesPersons, sortBy: sortBy},
                    success: function(respond) {
                        if (respond.status == true) {
                            window.open(BASE_URL + '/' + respond.data, '_blank');
                        }
                    }
                });
            }

        } else if (reportType == 'customerDetails') {
            cusIds = $('#customerName').val();
            var cusCategory = $('#customerCategoryList').val();

            if (isAllCustomers != true && (cusIds == '' || cusIds == null)) {
                p_notification(false, eb.getMessage('ERR_CUSREPO_SELECUST'));
            } else {
                var url = BASE_URL + '/api/customer-report/generate-customer-details-pdf';
                eb.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: {cusIds: cusIds, isAllCustomers: isAllCustomers, cusCategory: cusCategory},
                    success: function(respond) {
                        if (respond.status == true) {
                            window.open(BASE_URL + '/' + respond.data, '_blank');
                        }
                    }
                });
            }
        } else if (reportType == 'customerWiseItem') {
        	cusIds = $('#customerName').val();
        	categoryIds = $('#categorys').val();
            itemIDs = $('#items').val();
        	var fromDate = $('#from_date').val();
        	var endDate = $('#endDate').val();
            var cusCategory = $('#customerCategoryList').val();
            if (isAllCustomers != true && (cusIds == '' || cusIds == null)) {
                p_notification(false, eb.getMessage('ERR_CUSREPO_SELECUST'));
            } else if(isAllCategory != true && (categoryIds == '' || categoryIds == null)){
                p_notification(false, eb.getMessage('ERR_CUSREPO_SELECATEGORY'));
            } else if(isAllItem != true && (itemIDs == '' || itemIDs ==null)){
                p_notification(false, eb.getMessage('ERR_ITEM_SELECATEGORY'));
            } else if((fromDate != null && fromDate != null) && (endDate < fromDate)){
 				p_notification(false, eb.getMessage('ERR_CUSREPO_FROMDATE_CANT_BE_MORETHAN_ENDDATE'));
            } else {
                var url = BASE_URL + '/api/customer-report/view-customer-wise-sales-item-details-pdf'
                eb.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: {
                    	cusIds: cusIds,
                    	isAllCustomers: isAllCustomers,
                    	categoryIds: categoryIds,
                    	isAllCategory: isAllCategory,
                        isAllItem:isAllItem,
                        itemIDs:itemIDs,
                    	fromDate: fromDate,
                    	endDate: endDate,
                        cusCategory: cusCategory
                    },
                    success: function(respond) {
                        if (respond.status == true) {
                            window.open(BASE_URL + '/' + respond.data, '_blank');
                        }
                    }
                });
            }
        } else if (reportType == 'customerWiseSales') {
        	cusIds = $('#customerName').val();
        	locations = $('#locations').val();
        	var fromDate = $('#from_date').val();
        	var endDate = $('#endDate').val();
            var cusCategory = $('#customerCategoryList').val();
            if (isAllCustomers != true && (cusIds == '' || cusIds == null)) {
                p_notification(false, eb.getMessage('ERR_CUSREPO_SELECUST'));
            } else if(isAllLocation != true && (locations == '' || locations == null)){
                p_notification(false, eb.getMessage('ERR_CUSREPO_SELELOCATION'));
            } else if((fromDate != null && fromDate != null) && (endDate < fromDate)){
 				p_notification(false, eb.getMessage('ERR_CUSREPO_FROMDATE_CANT_BE_MORETHAN_ENDDATE'));
            } else {
                var url = BASE_URL + '/api/customer-report/view-customer-wise-sales-report-pdf'
                eb.ajax({
                    type: 'POST',
                    url: url,
                    data: {
                    	cusIds: cusIds,
                    	isAllCustomers: isAllCustomers,
                    	locations: locations,
                    	isAllLocation: isAllLocation,
                    	fromDate: fromDate,
                    	endDate: endDate,
                        cusCategory: cusCategory
                    },
                    success: function(respond) {
                        if (respond.status == true) {
                            window.open(BASE_URL + '/' + respond.data, '_blank');
                        }
                    },
                    async: false
                });
            }
        } else if( reportType == 'customerAllTransactions'){
            cusIds = $('#customerName').val();
            locations = $('#locations').val();
            var fromDate = $('#from_date').val();
            var endDate = $('#endDate').val();
            var cusCategory = $('#customerCategoryList').val();
            var summaryData = false;
            if($('#summaryRow').prop('checked') == true){
                summaryData = true;
            } else {
                summaryData = false;
            }
            if (isAllCustomers != true && (cusIds == '' || cusIds == null)) {
                p_notification(false, eb.getMessage('ERR_CUSREPO_SELECUST'));
            } else if(isAllLocation != true && (locations == '' || locations == null)){
                p_notification(false, eb.getMessage('ERR_CUSREPO_SELELOCATION'));
            } else if((fromDate != null && fromDate != null) && (endDate < fromDate)){
                p_notification(false, eb.getMessage('ERR_CUSREPO_FROMDATE_CANT_BE_MORETHAN_ENDDATE'));
            } else {
                var url = BASE_URL + '/api/customer-report/generate-customer-all-transactions-pdf'
                eb.ajax({
                    type: 'POST',
                    url: url,
                    data: {
                        cusIds: cusIds,
                        isAllCustomers: isAllCustomers,
                        locations: locations,
                        isAllLocation: isAllLocation,
                        fromDate: fromDate,
                        endDate: endDate,
                        cusCategory: cusCategory,
                        summaryData: summaryData
                    },
                    success: function(respond) {
                        if (respond.status == true) {
                            window.open(BASE_URL + '/' + respond.data, '_blank');
                        }
                    },
                    async: false
                });
            }
        } else if( reportType == 'customerBalance'){
            cusIds = $('#customerName').val();
            locations = $('#locations').val();
            var fromDate = $('#from_date').val();
            var endDate = $('#endDate').val();
            var cusCategory = $('#customerCategoryList').val();
    
            if (isAllCustomers != true && (cusIds == '' || cusIds == null)) {
                p_notification(false, eb.getMessage('ERR_CUSREPO_SELECUST'));
            } else if(isAllLocation != true && (locations == '' || locations == null)){
                p_notification(false, eb.getMessage('ERR_CUSREPO_SELELOCATION'));
            } else if(fromDate == "" || endDate == ""){
                p_notification(false, eb.getMessage('ERR_FROM_TO_DATE'));
            } else if((fromDate != null && endDate != null) && (endDate < fromDate)){
                p_notification(false, eb.getMessage('ERR_CUSREPO_FROMDATE_CANT_BE_MORETHAN_ENDDATE'));
            }  else {
                var url = BASE_URL + '/api/customer-report/generate-customer-balance-details-pdf'
                eb.ajax({
                    type: 'POST',
                    url: url,
                    data: {
                        cusIds: cusIds,
                        isAllCustomers: isAllCustomers,
                        locations: locations,
                        isAllLocation: isAllLocation,
                        fromDate: fromDate,
                        endDate: endDate,
                        cusCategory: cusCategory
                    },
                    success: function(respond) {
                        if (respond.status == true) {
                            window.open(BASE_URL + '/' + respond.data, '_blank');
                        }
                    },
                    async: false
                });
            }
        } else if (reportType == 'customerAgedAnalysisForDonePayment') {
            cusIds = $('#customerName').val();
            var cusCategory = $('#customerCategoryList').val();
            var advanceData = false;
            if($('#advRow').prop('checked') == true){
                advanceData = true;
            } else {
                advanceData = false;
            }

            if (isAllCustomers != true && (cusIds == null || cusIds == "")) {
                p_notification(false, eb.getMessage('ERR_CUSREPO_SELECUST'));
            } else {
                var url = BASE_URL + '/api/customer-report/generate-aged-cus-for-done-payment-pdf';
                eb.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: {cusIds: cusIds, isAllCustomers: isAllCustomers, cusCategory: cusCategory, advanceData: advanceData},
                    success: function(respond) {
                        if (respond.status == true) {
                            window.open(BASE_URL + '/' + respond.data, '_blank');
                        }
                    }
                });
            }
        } else {
            p_notification(false, eb.getMessage('ERR_CUSREPO_SELETYPE'));
        }
    });

    $('#csvReport').click(function(e) {
        e.preventDefault();
        if (reportType == 'agedCustomerAnalysis') {
            var cusIds = $('#customerName').val();
            var endDate = $('#endDate').val();
            var cusCategory = $('#customerCategoryList').val();
            var salesPersons = $('#salesPersons').val();
            var advanceData = false;
            if($('#advRow').prop('checked') == true){
                advanceData = true;
            } else {
                advanceData = false;
            }
            if (isAllCustomers != true && (cusIds == null || cusIds == "")) {
                p_notification(false, eb.getMessage('ERR_CUSREPO_SELECUST'));
            } else if (endDate == null || endDate == "") {
                p_notification(false, eb.getMessage('ERR_CUSREPO_DATEFILL'));
                document.getElementById("endDate").focus();
            } else {
                var url = BASE_URL + '/api/customer-report/generate-aged-cus-sheet';
                eb.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: {endDate: endDate, cusIds: cusIds, isAllCustomers: isAllCustomers, cusCategory: cusCategory, advanceData : advanceData, salesPersons: salesPersons},
                    success: function(respond) {
                        if (respond.status == true) {
                            window.open(BASE_URL + '/' + respond.data, '_blank');
                        }
                    }
                });
            }

        }
        else if (reportType == 'agedAnalysis') {
            var cusIds = $('#customerName').val();
            var endDate = $('#endDate').val();
            var cusCategory = $('#customerCategoryList').val();
            var advanceData = false;
            var salesPersons = $('#salesPersons').val();
            var sortBy = $('#orderFilter').val();
            if($('#advRow').prop('checked') == true){
                advanceData = true;
            } else {
                advanceData = false;
            }
            if (isAllCustomers != true && (cusIds == null || cusIds == "")) {
                p_notification(false, eb.getMessage('ERR_CUSREPO_SELECUST'));
            } else if (endDate == null || endDate == "") {
                p_notification(false, eb.getMessage('ERR_CUSREPO_DATEFILL'));
                document.getElementById("endDate").focus();
            } else {
                var url = BASE_URL + '/api/customer-report/generate-aged-analysis-sheet';
                eb.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: {endDate: endDate, cusIds: cusIds, isAllCustomers: isAllCustomers, cusCategory: cusCategory, advanceDataFlag : advanceData, salesPersons: salesPersons, sortBy: sortBy},
                    success: function(respond) {
                        if (respond.status == true) {
                            window.open(BASE_URL + '/' + respond.data, '_blank');
                        }
                    }
                });
            }
        }
        else if (reportType == 'customerDetails') {
            var cusIds = $('#customerName').val();
            var cusCategory = $('#customerCategoryList').val();

            if (isAllCustomers != true && (cusIds == '' || cusIds == null)) {
                p_notification(false, eb.getMessage('ERR_CUSREPO_SELECUST'));
            } else {
                var url = BASE_URL + '/api/customer-report/generate-customer-details-sheet';
                eb.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: {cusIds: cusIds, isAllCustomers: isAllCustomers, cusCategory: cusCategory},
                    success: function(respond) {
                        if (respond.status == true) {
                            window.open(BASE_URL + '/' + respond.data, '_blank');
                        }
                    }
                });
            }
        }
        else if (reportType == 'customerWiseItem') {
        	cusIds = $('#customerName').val();
        	categoryIds = $('#categorys').val();
            itemIDs = $('#items').val();
        	var fromDate = $('#from_date').val();
        	var endDate = $('#endDate').val();
            var cusCategory = $('#customerCategoryList').val();
            if (isAllCustomers != true && (cusIds == '' || cusIds == null)) {
                p_notification(false, eb.getMessage('ERR_CUSREPO_SELECUST'));
            } else if(isAllCategory != true && (categoryIds == '' || categoryIds == null)){
                p_notification(false, eb.getMessage('ERR_CUSREPO_SELECATEGORY'));
            } else if(isAllItem != true && (itemIDs == '' || itemIDs ==null)){
                p_notification(false, eb.getMessage('ERR_ITEM_SELECATEGORY'));
            } else if((fromDate != null && fromDate != null) && (endDate < fromDate)){
 				p_notification(false, eb.getMessage('ERR_CUSREPO_FROMDATE_CANT_BE_MORETHAN_ENDDATE'));
            } else {
                var url = BASE_URL + '/api/customer-report/generate-customer-wise-sales-item-details-sheet'
                eb.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: {
                    	cusIds: cusIds,
                    	isAllCustomers: isAllCustomers,
                    	categoryIds: categoryIds,
                    	isAllCategory: isAllCategory,
                        isAllItem:isAllItem,
                        itemIDs:itemIDs,
                    	fromDate: fromDate,
                    	endDate: endDate,
                        cusCategory: cusCategory,
                    },
                    success: function(respond) {
                        if (respond.status == true) {
                            window.open(BASE_URL + '/' + respond.data, '_blank');
                        }
                    }
                });
            }
        } else if (reportType == 'customerWiseSales') {
        	cusIds = $('#customerName').val();
        	locations = $('#locations').val();
        	var fromDate = $('#from_date').val();
        	var endDate = $('#endDate').val();
            if (isAllCustomers != true && (cusIds == '' || cusIds == null)) {
                p_notification(false, eb.getMessage('ERR_CUSREPO_SELECUST'));
            } else if(isAllLocation != true && (locations == '' || locations == null)){
                p_notification(false, eb.getMessage('ERR_CUSREPO_SELELOCATION'));
            } else if((fromDate != null && fromDate != null) && (endDate < fromDate)){
 				p_notification(false, eb.getMessage('ERR_CUSREPO_FROMDATE_CANT_BE_MORETHAN_ENDDATE'));
            } else {
                var url = BASE_URL + '/api/customer-report/generate-customer-wise-sales-report-sheet'
                eb.ajax({
                    type: 'POST',
                    url: url,
                    data: {
                    	cusIds: cusIds,
                    	isAllCustomers: isAllCustomers,
                    	locations: locations,
                    	isAllLocation: isAllLocation,
                    	fromDate: fromDate,
                    	endDate: endDate,
                    },
                    success: function(respond) {
                        if (respond.status == true) {
                            window.open(BASE_URL + '/' + respond.data, '_blank');
                        }
                    },
                    async: false
                });
            }
        } else if( reportType == 'customerAllTransactions'){
            cusIds = $('#customerName').val();
            locations = $('#locations').val();
            var fromDate = $('#from_date').val();
            var endDate = $('#endDate').val();
            var cusCategory = $('#customerCategoryList').val();
            var summaryData = false;
            if($('#summaryRow').prop('checked') == true){
                summaryData = true;
            } else {
                summaryData = false;
            }
            if (isAllCustomers != true && (cusIds == '' || cusIds == null)) {
                p_notification(false, eb.getMessage('ERR_CUSREPO_SELECUST'));
            } else if(isAllLocation != true && (locations == '' || locations == null)){
                p_notification(false, eb.getMessage('ERR_CUSREPO_SELELOCATION'));
            } else if((fromDate != null && fromDate != null) && (endDate < fromDate)){
                p_notification(false, eb.getMessage('ERR_CUSREPO_FROMDATE_CANT_BE_MORETHAN_ENDDATE'));
            } else {
                var url = BASE_URL + '/api/customer-report/generate-customer-all-transactions-sheet'
                eb.ajax({
                    type: 'POST',
                    url: url,
                    data: {
                        cusIds: cusIds,
                        isAllCustomers: isAllCustomers,
                        locations: locations,
                        isAllLocation: isAllLocation,
                        fromDate: fromDate,
                        endDate: endDate,
                        cusCategory: cusCategory,
                        summaryData: summaryData
                    },
                    success: function(respond) {
                        if (respond.status == true) {
                            window.open(BASE_URL + '/' + respond.data, '_blank');
                        }
                    },
                    async: false
                });
            }
        } else if( reportType == 'customerBalance'){
            cusIds = $('#customerName').val();
            locations = $('#locations').val();
            var fromDate = $('#from_date').val();
            var endDate = $('#endDate').val();
            var cusCategory = $('#customerCategoryList').val();
            var summaryData = false;
            if($('#summaryRow').prop('checked') == true){
                summaryData = true;
            } else {
                summaryData = false;
            }
            if (isAllCustomers != true && (cusIds == '' || cusIds == null)) {
                p_notification(false, eb.getMessage('ERR_CUSREPO_SELECUST'));
            } else if(isAllLocation != true && (locations == '' || locations == null)){
                p_notification(false, eb.getMessage('ERR_CUSREPO_SELELOCATION'));
            } else if(fromDate == "" || endDate == ""){
                p_notification(false, eb.getMessage('ERR_FROM_TO_DATE'));
            } else if((fromDate != null && endDate != null) && (endDate < fromDate)){
                p_notification(false, eb.getMessage('ERR_CUSREPO_FROMDATE_CANT_BE_MORETHAN_ENDDATE'));
            } else {
                var url = BASE_URL + '/api/customer-report/generate-customer-balance-details-sheet'
                eb.ajax({
                    type: 'POST',
                    url: url,
                    data: {
                        cusIds: cusIds,
                        isAllCustomers: isAllCustomers,
                        locations: locations,
                        isAllLocation: isAllLocation,
                        fromDate: fromDate,
                        endDate: endDate,
                        cusCategory: cusCategory,
                        summaryData: summaryData
                    },
                    success: function(respond) {
                        if (respond.status == true) {
                            window.open(BASE_URL + '/' + respond.data, '_blank');
                        }
                    },
                    async: false
                });
            }
        } else if (reportType == 'customerAgedAnalysisForDonePayment') {
            var cusIds = $('#customerName').val();
            var cusCategory = $('#customerCategoryList').val();
            var advanceData = false;
            if($('#advRow').prop('checked') == true){
                advanceData = true;
            } else {
                advanceData = false;
            }
            if (isAllCustomers != true && (cusIds == null || cusIds == "")) {
                p_notification(false, eb.getMessage('ERR_CUSREPO_SELECUST'));
            } else {
                var url = BASE_URL + '/api/customer-report/generate-aged-cus-for-done-payment-sheet';
                eb.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: {cusIds: cusIds, isAllCustomers: isAllCustomers, cusCategory: cusCategory, advanceData : advanceData},
                    success: function(respond) {
                        if (respond.status == true) {
                            window.open(BASE_URL + '/' + respond.data, '_blank');
                        }
                    }
                });
            }

        } else {
            p_notification(false, eb.getMessage('ERR_CUSREPO_SELETYPE'));
        }

    });

    function initialLoading() {
        $('#customerReportIFrame').hide();
        $('#reportHead').show();
        $('#from_date').val('');
        $('#to_date').val('');
        $('#endDate').show();
        $('.end_date_label').removeClass('hidden');
        $('.from_date_label').addClass('hidden');
        $('#from_date').hide();
        $('#to_date').hide();
        $('.remove_label').hide();
    }

    function customerIFrameHide() {
        $("#customerReportIFrame").hide();
        $("#customerReportIFrame").contents().find("body").html('');
    }


    //responsive issue fixes
    $(window).bind('resize ready', function() {
        if ($(window).width() <= 480) {
            $('#custom_name').removeClass('col-xs-4').addClass('col-xs-12').css('margin-bottom', '15px');
            $('.customer_report_from_date, .customer_report_end_date').removeClass('col-xs-4').addClass('col-xs-12').css('margin-bottom', '15px');
            $('.btn_panel_normal').removeClass('btn-group');
            $('#view_report, #generate_pdf, #excel_report').removeClass('btn-default').addClass('col-xs-12 btn-primary').css('margin-bottom', '8px');
        } else {
            $('#custom_name').addClass('col-xs-4').removeClass('col-xs-12').css('margin-bottom', '0px');
            $('.customer_report_from_date, .customer_report_end_date').addClass('col-xs-4').removeClass('col-xs-12').css('margin-bottom', '0px');
            $('.btn_panel_normal').addClass('btn-group');
            $('#view_report, #generate_pdf, #excel_report').addClass('btn-default').removeClass('col-xs-12 btn-primary').css('margin-bottom', '8px');
        }
    });

    $('.isAllCustomers').on('click', function() {
        isAllCustomers = $(this).val();
        if (isAllCustomers == true) {
            $('.customer-category-div').removeClass('hidden');
            $('#customerCategoryList').val('').trigger('change');
            $('#customerName').val('').trigger('change');
            $('#customerName').prop('disabled', true);
            p_notification('info', eb.getMessage('INFO_CUS_REPORT_ALL_CUS_SELECTION'));
        } else {
            $('#customerName').prop('disabled', false);
            $('#customerCategoryList').val('').trigger('change');
            $('.customer-category-div').addClass('hidden');
        }
        $('#customerName').selectpicker('refresh');
    });

    $('.isAllCategory').on('click', function() {
        isAllCategory = $(this).val();
        if (isAllCategory == true) {
            $('#categorys').val('').trigger('change');
            $('#categorys').prop('disabled', true);
            p_notification('info', eb.getMessage('INFO_CUS_REPORT_ALL_CUS_SELECTION'));
        } else {
            $('#categorys').prop('disabled', false).selectpicker('refresh');
            $('#items').val('').trigger('change');
            $('#AllItem').trigger('click');
            $('#items').selectpicker('refresh');
        }
        $('#categorys').selectpicker('refresh');
    });

    $('.isAllItem').on('click',function(){
        isAllItem = $(this).val();
        if (isAllItem == true) {
            $('#items').val('').trigger('change');
            $('#items').prop('disabled', true);
            // p_notification('info', eb.getMessage('INFO_CUS_REPORT_ALL_CUS_SELECTION'));
        } else {
            $('#items').prop('disabled', false).selectpicker('refresh');
                loadDropDownFromDatabase('/productAPI/getProductByCatagories', "", 0, '#items', "", true,categoryIDList);
        }
        $('#items').selectpicker('refresh');
    })

     $('.isAllLocation').on('click', function() {
        isAllLocation = $(this).val();
        if (isAllLocation == true) {
            $('#locations').val('').trigger('change');
            $('#locations').prop('disabled', true);
            p_notification('info', eb.getMessage('INFO_CUS_REPORT_ALL_CUS_SELECTION'));
        } else {
            $('#locations').prop('disabled', false).selectpicker('refresh');
        }
        $('#locations').selectpicker('refresh');
    });

});
