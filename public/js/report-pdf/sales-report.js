/*
 * @author SANDUN <sandun@thinkcube.com>
 * Sales Reporting functions
 */
$(document).ready(function() {
    ///////DatePicker for two text Filed\\\\\\\
    var fm, tm;
    var reportType = 'Monthly';
    var checkin = $('#fromDate').datepicker({
    }).on('changeDate', function(ev) {
        if (ev.date.valueOf() < checkout.date.valueOf()) {
            var currentDate = new Date(ev.date);
            var newDate = new Date();
            newDate.setDate(currentDate.getDate() + 1);
            checkout.setValue(newDate);
        }
        checkin.hide();
        $('#toDate')[0].focus();
    }).data('datepicker');
    var checkout = $('#toDate').datepicker({
        onRender: function(date) {
            return date.valueOf() < checkin.date.valueOf() ? 'disabled' : '';
        }
    }).on('changeDate', function(ev) {
        checkout.hide();
    }).data('datepicker');

    $("#fromMonth").datepicker({
        format: "yyyy-mm",
        viewMode: "months",
        minViewMode: 'months'
    });
    $("#toMonth").datepicker({
        format: "yyyy-mm",
        viewMode: "months",
        minViewMode: 'months'
    });
    ///////Month Pick for two text Filed\\\\\\\
    var checkinMonth = $('#fromMonth').datepicker({
    }).on('changeDate', function(ev) {
        var newD = new Date(ev.date.getFullYear(), ev.date.getMonth() + 1);
        checkoutMonth.setValue(newD);
        checkinMonth.hide();
        $('#toMonth')[0].focus();

    }).data('datepicker');

    var checkoutMonth = $('#toMonth').datepicker({
    }).on('changeDate', function(ev) {
        checkoutMonth.hide();
    }).data('datepicker');

    /////Pick date/////////////
    $('#date').datepicker();
    //////Pick year////////////
    $("#year").datepicker({
        format: " yyyy",
        viewMode: "years",
        minViewMode: 'years'
    });

    var checkinYear = $('#year').datepicker({
    }).on('changeDate', function(ev) {
        var newD = new Date(ev.date.getYear());
        checkinYear.hide();

    }).data('datepicker');

    initialCondition();
    setSelectPicker();
    $('.sales-person').removeClass('hidden');
    $('#monthly').click(function(e) {
        reportType = 'Monthly';
        $("#locationList").val('').selectpicker('refresh');
        $('#tax').attr('checked', false);
        $('#toMonth,#fromMonth').val('');
        //hide
        $('#year,#fromDate,#toDate,#date').hide();
        $('#salesReport').hide();
        $('#salesGraphReport').hide();
        $('.from_year_lbl').addClass('hidden');
        $('.to_date_lbl').addClass('hidden');
        $('.from_date_lbl').addClass('hidden');
        $('.sort').hide();
        $('.payment-list').hide();
        $('#sortlocation').hide();
        $('#locationradio').hide();
        $('#paymentradio').hide();
        $('.sales-person').removeClass('hidden');
        $('.sales-person-div').addClass('hidden');
        //show
        $('#viewGraph,#tax,#lblTax').show();
        $('#fromMonth,#toMonth').show();
        $('.to').show();
        $('.to_month_lbl').show();
        $('.from_month_lbl').show();
        $('.tax_chk_lbl').show();
    });
    $('#daily').click(function(e) {
        reportType = 'Daily';
        $("#locationList").val('').selectpicker('refresh');
        $('#tax').attr('checked', false);
        $('#fromDate,#toDate').val('');
        //hidden
        $('#fromMonth,#toMonth,#year').hide();
        $('#salesReport').hide();
        $('#salesGraphReport').hide();
        $('#salessummery_report_view').hide();
        $('#paymentsummery_report_view').hide();
        $('.from_month_lbl').hide();
        $('.to_month_lbl').hide();
        $('.from_year_lbl').addClass('hidden');
        $('.sort').hide();
        $('.payment-list').hide();
        $('#sortlocation').hide();
        $('#locationradio').hide();
        $('#paymentradio').hide();
        $('.sales-person-div').addClass('hidden');
        //show
        $('#fromDate,#toDate,#date').show();
        $('#viewGraph,#tax,#lblTax').show();
        $('.to').show();
        $('.from_date_lbl').removeClass('hidden');
        $('.to_date_lbl').removeClass('hidden');
        $('.tax_chk_lbl').show();
        $('.sales-person').removeClass('hidden');
    });
    $('#annual').click(function(e) {
        reportType = 'Annual';
        $("#locationList").val('').selectpicker('refresh');
        $('#tax').attr('checked', false);
        $('#year').val('');
        //hide
        $('#fromDate,#toDate,#fromMonth,#toMonth,#date').hide();
        $('#salesReport').hide();
        $('#salesGraphReport').hide();
        $('.to_date_lbl').addClass('hidden');
        $('.from_date_lbl').addClass('hidden');
        $('.from_month_lbl').hide();
        $('.to_month_lbl').hide();
        $('.to').hide();
        $('.sort').hide();
        $('.payment-list').hide();
        $('#sortlocation').hide();
        $('#locationradio').hide();
        $('#paymentradio').hide();
        $('.sales-person').addClass('hidden');
        $('.sales-person-div').addClass('hidden');
        //show
        $('#viewGraph,#tax,#lblTax,#year').show();
        $('.from_year_lbl').removeClass('hidden');
        $('.tax_chk_lbl').show();
    });
    $('#summery').click(function(e) {
        reportType = 'Summery';
        $("#locationList").val('').selectpicker('refresh');
        //hide
        $('#tax,#lblTax').hide();
        $('#viewGraph').hide();
        $('#fromMonth,#toMonth,#date,#year').hide();
        $('#salesReport').hide();
        $('#salesGraphReport').hide();
        $('.from_month_lbl').hide();
        $('.to_month_lbl').hide();
        $('.from_year_lbl').addClass('hidden');
        $('.tax_chk_lbl').hide();
        $('.sort').hide();
        $('.payment-list').hide();
        $('#sortlocation').hide();
        $('#locationradio').hide();
        $('#paymentradio').hide();
        $('.sales-person').addClass('hidden');
        $('.sales-person-div').addClass('hidden');
        //show
        $('.to').show();
        $('.to_date_lbl').removeClass('hidden');
        $('.from_date_lbl').removeClass('hidden');
        $('#fromDate,#toDate').show();
        $('#fromDate,#toDate').val('');
    });
    $('#payments').click(function(e) {
        reportType = 'Payments';
        $("#locationList").val('').selectpicker('refresh');
        $('#fromDate,#toDate').val('');
        $('.selectpicker').selectpicker('val', '');
        //hide
        $('#fromMonth,#toMonth,#date,#year').hide();
        $('#tax,#lblTax').hide();
        $('#salesReport').hide();
        $('#viewGraph').hide();
        $('.from_month_lbl').hide();
        $('.to_month_lbl').hide();
        $('.from_year_lbl').addClass('hidden');
        $('.tax_chk_lbl').hide();
        $('.sales-person').addClass('hidden');
        $('.sales-person-div').addClass('hidden');
        //show
        $('#fromDate,#toDate').show();
        $('.from_date_lbl').removeClass('hidden');
        $('.to_date_lbl').removeClass('hidden');
        $('.to').show();
        $('.sort').show();
        $('.payment-list').show();
        //active_link_appearnce
        $(this).addClass('active_payment');
    });

    $('#grossProfit').click(function(e) {
        reportType = 'GrossProfit';
        $("#locationList").val('').selectpicker('refresh');
        $('#fromDate,#toDate').val('');
        $('.selectpicker').selectpicker('val', '');
        //hide
        $('#fromMonth,#toMonth,#date,#year').hide();
        $('#tax,#lblTax').hide();
        $('#salesReport').hide();
        $('#viewGraph').hide();
        $('.from_month_lbl').hide();
        $('.to_month_lbl').hide();
        $('.from_year_lbl').addClass('hidden');
        $('.tax_chk_lbl').hide();
        $('.sort').hide();
        $('.payment-list').hide();
        $('.sales-person').addClass('hidden');
        $('.sales-person-div').addClass('hidden');
        //show
        $('#fromDate,#toDate').show();
        $('.from_date_lbl').removeClass('hidden');
        $('.to_date_lbl').removeClass('hidden');
        $('.to').show();
        //active_link_appearnce
        $(this).addClass('active_payment');
    });

    $('#creditNote').click(function(e) {
        reportType = 'creditNote';
        $("#locationList").val('').selectpicker('refresh');
        $('#fromDate,#toDate').val('');
        $('.selectpicker').selectpicker('val', '');
        //hide
        $('#fromMonth,#toMonth,#date,#year').hide();
        $('#tax,#lblTax').hide();
        $('#salesReport').hide();
        $('#viewGraph').hide();
        $('.from_month_lbl').hide();
        $('.to_month_lbl').hide();
        $('.from_year_lbl').addClass('hidden');
        $('.tax_chk_lbl').hide();
        $('.sort').hide();
        $('.payment-list').hide();
        $('.sales-person').addClass('hidden');        
        //show
        $('#fromDate,#toDate').show();
        $('.from_date_lbl').removeClass('hidden');
        $('.to_date_lbl').removeClass('hidden');
        $('.sales-person-div').removeClass('hidden');
        $('.to').show();
        //active_link_appearnce
        $(this).addClass('active_payment');
    });

    function hideGraphDiv() {
        $('#salesGraphReport').hide();
        $('#salesGraphReport').html('');
    }

    $('#viewReport').click(function(e) {
        hideGraphDiv();
        if (reportType == 'Monthly') {
            var checkTaxStatus = getCheckedStatus();
            var getpdfdataurl = BASE_URL + '/api/sales-report/view-monthly-sales';
            var fromMonth = $('#fromMonth').val();
            var fromMonthSplit = fromMonth.split("-");
            var toMonth = $('#toMonth').val();
            var toMonthSplit = toMonth.split("-");
            var locationID = $("#locationList").val();
            var cusCategories = $('#customerCategoryList').val();
            fromday(fromMonthSplit[1]);
            today(toMonthSplit[1], toMonthSplit[0]);

            if ($('#fromMonth').val() == null || $('#fromMonth').val() == "") {
                p_notification(false, eb.getMessage('ERR_SALEREPORT_FROMMONTH'));
                $('#fromMonth').focus();
                return false;
            }
            else if ($('#toMonth').val() == null || $('#toMonth').val() == "") {
                p_notification(false, eb.getMessage('ERR_SALEREPORT_TOMONTH'));
                $('#toMonth').focus();
                return false;
            }
            else if ($('#toMonth').val() < $('#fromMonth').val()) {
                p_notification(false, eb.getMessage('ERR_SALEREPORT_DEFF'))
                return false;
            }
            else {
                var getpdfdatarequest = eb.post(getpdfdataurl, {
                    fromMonth: fm,
                    toMonth: tm,
                    checkTaxStatus: checkTaxStatus,
                    locationID: locationID,
                    salesPersonIds: $("#salesPersons").val(),
                    cusCategories: cusCategories
                });
                getpdfdatarequest.done(function(respond) {
                    if (respond.status == true) {
                        setContentToReport('salesReport', respond);
                    }
                });

            }
        }
        else if (reportType == 'Daily') {
            $('#btnDaily').hide();
            var checkTaxStatus = getCheckedStatus();
            var locationID = $("#locationList").val();
            var cusCategories = $('#customerCategoryList').val(); 
            var getpdfdataurl = BASE_URL + '/api/sales-report/view-daily-sales';

            inputs = new Array(
                    $('#fromDate').val(),
                    $('#toDate').val());

            if (validteinput(inputs)) {
                var getpdfdatarequest = eb.post(getpdfdataurl, {
                    fromDate: $('#fromDate').val(),
                    toDate: $('#toDate').val(),
                    checkTaxStatus: checkTaxStatus,
                    salesPersonIds: $("#salesPersons").val(),
                    locationID: locationID,
                    cusCategories: cusCategories
                });
                getpdfdatarequest.done(function(respond) {
                    if (respond.status == true) {
                        setContentToReport('salesReport', respond);
                    }
                });
            }

        }
        else if (reportType == 'Annual') {
            $('#btnAnnual').hide();
            var checkTaxStatus = getCheckedStatus();
            var year = $('#year').val();
            year = year.replace(/\s+/g, "");
            $.trim(year);
            var locationID = $("#locationList").val();
            var cusCategories = $('#customerCategoryList').val();

            var getannualdataurl = BASE_URL + '/api/sales-report/view-annual-sales';
            inputs = new Array(
                    $('#year').val()
                    );
            if ($('#year').val() == null || $('#year').val() == "") {
                p_notification(false, eb.getMessage('ERR_REPOPDF_YEARFILL'));
                $('#year').focus();
                return false;
            } else {
                var getannualdatarequest = eb.post(getannualdataurl, {
                    year: year,
                    checkTaxStatus: checkTaxStatus,
                    locationID: locationID,
                    cusCategories: cusCategories
                });
                getannualdatarequest.done(function(respond) {
                    if (respond.status == true) {
                        setContentToReport('salesReport', respond);
                    }
                });
            }

        }
        else if (reportType == 'Summery') {
            var locationID = $("#locationList").val();
            var cusCategories = $('#customerCategoryList').val();
            var getdataurl = BASE_URL + '/api/sales-report/view-sales-summery';
            inputs = new Array(
                    $('#fromDate').val(),
                    $('#toDate').val()
                    );
            if (validteinput(inputs)) {
                var getdatarequest = eb.post(getdataurl, {
                    fromDate: $('#fromDate').val(),
                    toDate: $('#toDate').val(),
                    locationID: locationID,
                    cusCategories: cusCategories
                });
                getdatarequest.done(function(respond) {
                    if (respond.status == true) {
                        setContentToReport('salesReport', respond);
                    }
                });
            }
        } else if (reportType == 'GrossProfit') {
            var url = BASE_URL + '/api/sales-report/view-gross-profit';
            var fromDate = $('#fromDate').val();
            var toDate = $('#toDate').val();
            var locationList = $('#locationList').val();
            var cusCategories = $('#customerCategoryList').val();
            inputs = new Array(fromDate, toDate);
            if (locationList == null) {
                p_notification(false, "Please select Location(s).")
                return false;
            } else {
                if (validteinput(inputs)) {
                    eb.ajax({
                        type: 'POST',
                        url: url,
                        data: {fromDate: fromDate, toDate: toDate, locationList: locationList, cusCategories: cusCategories},
                        success: function(respond) {
                            if (respond.status == true) {
                                setContentToReport('salesReport', respond);
                            }
                        }
                    });
                }
            }
        } else if (reportType == 'creditNote') {
            var url = BASE_URL + '/api/sales-report/view-credit-note';
            var input = {};
            input.fromDate = $('#fromDate').val();
            input.toDate = $('#toDate').val();
            input.locationList = $('#locationList').val();
            input.salesPersonIds = $('#salesPersonList').val();
            input.cusCategories = $('#customerCategoryList').val();            
            if (creditNoteFormValidation(input)) {
                eb.ajax({
                    type: 'POST',
                    url: url,
                    data: input,
                    success: function(respond) {
                        if (respond.status == true) {
                            setContentToReport('salesReport', respond);
                        }
                    }
                });
            }
        }
        else {
            p_notification(false, eb.getMessage('ERR_SALEREPORT_CORRETYPE'));
        }
    });

    /* function for Pdf generate Reporting
     * @author SANDUN <sandun@thinkcube.com>
     * */
    $('#generatePdf').click(function(e) {
        if (reportType == 'Monthly') {
            var checkTaxStatus = getCheckedStatus();
            var fromMonth = $('#fromMonth').val();
            var fromMonthSplit = fromMonth.split("-");
            var toMonth = $('#toMonth').val();
            var toMonthSplit = toMonth.split("-");
            var locationID = $("#locationList").val();
            var cusCategories = $('#customerCategoryList').val();
            fromday(fromMonthSplit[1]);
            today(toMonthSplit[1], toMonthSplit[0]);            
            var params = {
                fromMonth: fm,
                toMonth: tm,
                checkTaxStatus: getCheckedStatus(),
                locationID: locationID,
                salesPersonIds: $("#salesPersons").val(),
                cusCategories: cusCategories
            };

            if ($('#fromMonth').val() == null || $('#fromMonth').val() == "") {
                p_notification(false, eb.getMessage('ERR_SALEREPORT_FROMMONTH'));
                $('#fromMonth').focus();
                return false;
            }
            else if ($('#toMonth').val() == null || $('#toMonth').val() == "") {
                p_notification(false, eb.getMessage('ERR_SALEREPORT_TOMONTH'));
                $('#toMonth').focus();
                return false;
            } else if ($('#toMonth').val() < $('#fromMonth').val()) {
                p_notification(false, eb.getMessage('ERR_SALEREPORT_DEFF'))
                return false;
            }
            else {
                window.open(BASE_URL + "/api/sales-report/generate-monthly-sales-pdf?" + $.param(params), '_blank');
            }
        }
        else if (reportType == 'Daily') {
            var fromDate = $('#fromDate').val();
            var toDate = $('#toDate').val();
            var cusCategories = $('#customerCategoryList').val();            
            var params = {
                fromDate: fromDate,
                toDate: toDate,
                checkTaxStatus: getCheckedStatus(),
                salesPersonIds: $("#salesPersons").val(),
                locationID: $("#locationList").val(),
                cusCategories: cusCategories
            };
            
            inputs = new Array(fromDate, toDate);
            if (validteinput(inputs)) {
                window.open(BASE_URL + "/api/sales-report/generate-daily-sales-pdf?" + $.param(params), '_blank');
            }
        }
        else if (reportType == 'Annual') {
            var checkTaxStatus = getCheckedStatus();
            var locationID = $("#locationList").val();
            var cusCategories = $('#customerCategoryList').val();
            if ($('#year').val() == null || $('#year').val() == "") {
                p_notification(false, eb.getMessage('ERR_REPOPDF_YEARFILL'));
                $('#year').focus();
                return false;
            } else {
                var year = $("#year").val();
                year = year.replace(/\s+/g, "");
                $.trim(year);
                window.open(BASE_URL + "/api/sales-report/generate-annual-sales-pdf?year=" + year + "&checkTaxStatus=" + checkTaxStatus + "&locationID=" + locationID + "&cusCategories=" + cusCategories, '_blank');
            }
        }
        else if (reportType == 'Summery') {
            inputs = new Array(
                    $('#fromDate').val(),
                    $('#toDate').val()
                    );
            if (validteinput(inputs)) {
                var fromDate = $("#fromDate").val();
                var toDate = $("#toDate").val();
                var locationID = $("#locationList").val();
                var cusCategories = $('#customerCategoryList').val();
                window.open(BASE_URL + "/api/sales-report/generate-sales-summery-pdf?fromDate=" + fromDate + "&toDate=" + toDate + "&locationID=" + locationID + "&cusCategories=" + cusCategories, '_blank');
            }
        } else if (reportType == 'GrossProfit') {
            var url = BASE_URL + '/api/sales-report/generate-gross-profit-pdf';
            var fromDate = $('#fromDate').val();
            var toDate = $('#toDate').val();
            var locationList = $('#locationList').val();
            var cusCategories = $('#customerCategoryList').val();
            inputs = new Array(fromDate, toDate);
            if (locationList == null) {
                p_notification(false, "Please select Location(s).")
                return false;
            } else {
                if (validteinput(inputs)) {
                    eb.ajax({
                        type: 'POST',
                        url: url,
                        data: {fromDate: fromDate, toDate: toDate, locationList: locationList, cusCategories: cusCategories},
                        success: function(respond) {
                            if (respond.status == true) {
                                p_notification(true, respond.msg);
                            }
                        }
                    });
                }
            }
        } else if (reportType == 'creditNote') {
            var url = BASE_URL + '/api/sales-report/generate-credit-note-pdf';
            var input = {};
            input.fromDate = $('#fromDate').val();
            input.toDate = $('#toDate').val();
            input.locationList = $('#locationList').val();
            input.salesPersonIds = $('#salesPersonList').val();
            input.cusCategories = $('#customerCategoryList').val();           
            if (creditNoteFormValidation(input)) {
                eb.ajax({
                    type: 'POST',
                    url: url,
                    data: input,
                    success: function(respond) {
                        if (respond.status == true) {
                            window.open(BASE_URL + '/' + respond.data, '_blank');
                        }
                    }
                });
            }
        } else {
            p_notification(false, eb.getMessage('ERR_SALEREPORT_CORRETYPE'));
        }

    });

    /* function for Excel Reporting
     * @author SANDUN <sandun@thinkcube.com>
     * */
    $('#csvReport').on('click', function(e) {
        if (reportType == 'Monthly') {
            var checkTaxStatus = getCheckedStatus();
            var fromMonth = $('#fromMonth').val();
            var fromMonthSplit = fromMonth.split("-");
            var toMonth = $('#toMonth').val();
            var toMonthSplit = toMonth.split("-");
            var locationID = $("#locationList").val();
            var cusCategories = $('#customerCategoryList').val();
            fromday(fromMonthSplit[1]);
            today(toMonthSplit[1], toMonthSplit[0]);            
            var params = {
                fromMonth: fm,
                toMonth: tm,
                checkTaxStatus: getCheckedStatus(),
                locationID: locationID,
                salesPersonIds: $("#salesPersons").val(),
                cusCategories: cusCategories
            };

            if ($('#fromMonth').val() == null || $('#fromMonth').val() == "") {
                p_notification(false, eb.getMessage('ERR_SALEREPORT_FROMMONTH'));
                $('#fromMonth').focus();
                return false;
            }
            else if ($('#toMonth').val() == null || $('#toMonth').val() == "") {
                p_notification(false, eb.getMessage('ERR_SALEREPORT_TOMONTH'));
                $('#toMonth').focus();
                return false;
            } else if ($('#toMonth').val() < $('#fromMonth').val()) {
                p_notification(false, eb.getMessage('ERR_SALEREPORT_DEFF'))
                return false;
            }
            else {
                var url = BASE_URL + '/api/sales-report/generate-monthly-sales-sheet';
                eb.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: params,
                    success: function(respond) {
                        if (respond.status) {
                            window.open(BASE_URL + '/' + respond.data, '_blank');
                        }
                    }
                });
            }
        }
        else if (reportType == 'Daily') {
            var checkTaxStatus = getCheckedStatus();
            var fromDate = $('#fromDate').val();
            var toDate = $('#toDate').val();
            var salesPersonIds = $("#salesPersons").val();
            var locationID = $("#locationList").val();
            var cusCategories = $('#customerCategoryList').val();
            var params = {
                fromDate: fromDate,
                toDate: toDate,
                checkTaxStatus: getCheckedStatus(),
                salesPersonIds: salesPersonIds,
                locationID: locationID,
                cusCategories: cusCategories
            };
            inputs = new Array(fromDate, toDate);
            
            if (validteinput(inputs)) {
                var url = BASE_URL + '/api/sales-report/generate-daily-sales-sheet';
                eb.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: params,
                    success: function(respond) {
                        if (respond.status) {
                            window.open(BASE_URL + '/' + respond.data, '_blank');
                        }
                    }
                });
            }
        }
        else if (reportType == 'Annual') {            
            if ($('#year').val() == null || $('#year').val() == "") {
                p_notification(false, eb.getMessage('ERR_REPOPDF_YEARFILL'));
                $('#year').focus();
                return false;
            } else {
                var year = $("#year").val();
                year = year.replace(/\s+/g, "");
                $.trim(year);
                var locationID = $("#locationList").val();
                var checkTaxStatus = getCheckedStatus();
                var cusCategories = $('#customerCategoryList').val();
                var params = {
                    year : year,
                    checkTaxStatus : checkTaxStatus,
                    locationID : locationID,
                    cusCategories : cusCategories
                };
                
                eb.ajax({
                    type: 'POST',
                    url: BASE_URL + "/api/sales-report/generate-annual-sales-sheet",
                    dataType: 'json',
                    data: params,
                    success: function(respond) {
                        if (respond.status) {
                            window.open(BASE_URL + '/' + respond.data, '_blank');
                        }
                    }
                });
            }

        } else if (reportType == 'Summery') {
            inputs = new Array(
                    $('#fromDate').val(),
                    $('#toDate').val()
                    );
            if (validteinput(inputs)) {
                var fromDate = $("#fromDate").val();
                var toDate = $("#toDate").val();
                var locationID = $("#locationList").val();
                var cusCategories = $('#customerCategoryList').val();                
                var params = {
                    fromDate : fromDate,
                    toDate : toDate,
                    locationID : locationID,
                    cusCategories : cusCategories
                };
                
                eb.ajax({
                    type: 'POST',
                    url: BASE_URL + "/api/sales-report/generate-sales-summery-sheet",
                    dataType: 'json',
                    data: params,
                    success: function(respond) {
                        if (respond.status) {
                            window.open(BASE_URL + '/' + respond.data, '_blank');
                        }
                    }
                });                
            }
        } else if (reportType == 'GrossProfit') {
            var url = BASE_URL + '/api/sales-report/generate-gross-profit-sheet';
            var fromDate = $('#fromDate').val();
            var toDate = $('#toDate').val();
            var locationList = $('#locationList').val();
            var cusCategories = $('#customerCategoryList').val();
            inputs = new Array(fromDate, toDate);
            if (locationList == null) {
                p_notification(false, "Please select Location(s).")
                return false;
            } else {
                if (validteinput(inputs)) {
                    eb.ajax({
                        type: 'POST',
                        url: url,
                        data: {fromDate: fromDate, toDate: toDate, locationList: locationList, cusCategories: cusCategories},
                        success: function(respond) {
                            if (respond.status == true) {
                                p_notification(true, respond.msg);
                            }
                        }
                    });
                }
            }
        } else if (reportType == 'creditNote') {
            var url = BASE_URL + '/api/sales-report/generate-credit-note-csv';
            var input = {};
            input.fromDate = $('#fromDate').val();
            input.toDate = $('#toDate').val();
            input.locationList = $('#locationList').val();
            input.salesPersonIds = $('#salesPersonList').val();
            input.cusCategories = $('#customerCategoryList').val();              
            if (creditNoteFormValidation(input)) {
                eb.ajax({
                    type: 'POST',
                    url: url,
                    data: input,
                    success: function(respond) {
                        if (respond.status == true) {
                            window.open(BASE_URL + '/' + respond.data, '_blank');
                        }
                    }
                });
            }
            
        } else {
            p_notification(false, eb.getMessage('ERR_SALEREPORT_CORRETYPE'));
        }

    });

    $('#viewGraph').on('click', function(e) {
        $('#salesReport').hide();
        $('#salesGraphReport').show();
        $('#salesGraphReport').html('');
        if (reportType == 'Monthly') {
            $('#btnMonthly').show();
            var checkTaxStatus = getCheckedStatus();
            var getpdfdataurl = BASE_URL + '/api/sales-report/get-monthly-sales-graph-data';
            var fromMonth = $('#fromMonth').val();
            var fromMonthSplit = fromMonth.split("-");
            var toMonth = $('#toMonth').val();
            var toMonthSplit = toMonth.split("-");
            var cusCategories = $('#customerCategoryList').val();  
            fromday(fromMonthSplit[1]);
            today(toMonthSplit[1], toMonthSplit[0]);
            var locationID = $('#locationList').val();
            if ($('#fromMonth').val() == null || $('#fromMonth').val() == "") {
                p_notification(false, eb.getMessage('ERR_SALEREPORT_FROMMONTH'));
                $('#fromMonth').focus();
                return false;
            }
            else if ($('#toMonth').val() == null || $('#toMonth').val() == "") {
                p_notification(false, eb.getMessage('ERR_SALEREPORT_TOMONTH'));
                $('#toMonth').focus();
                return false;
            } else if ($('#toMonth').val() < $('#fromMonth').val()) {
                p_notification(false, eb.getMessage('ERR_SALEREPORT_DEFF'))
                return false;
            }
            else {

                var getpdfdatarequest = eb.post(getpdfdataurl, {
                    fromMonth: fm,
                    toMonth: tm,
                    locationID: locationID,
                    cusCategories: cusCategories
                });
                getpdfdatarequest.done(function(respond) {
                    var cSymbol = respond.data['cSymbol'];

                    var monthlySalesData = Array();
                    $.each(respond.data['mothlySalesDetails'], function(index, value) {
                        $.each(value, function(subindex, subvalue) {                            
                            var totalSalesWithCreditNote = subvalue.TotalAmount - subvalue.creditNoteProductTotal - subvalue.creditNoteProductTax;
                            var totalTaxWithCreditNote = subvalue.TotalTax - subvalue.creditNoteProductTax;
                            
                            var month = subvalue.Month;
                            var totalAmount = parseFloat(Math.round((totalSalesWithCreditNote) * 100) / 100).toFixed(2);
                            totalAmount = totalAmount ? totalAmount : 0.00;
                            var totalTax = parseFloat(Math.round((totalTaxWithCreditNote) * 100) / 100).toFixed(2);
                            totalTax = totalTax ? totalTax : 0.00;
                            var totalSales = totalAmount - totalTax;

                            monthlySalesData.push({x: month, y: totalSales, z: totalTax});
                        });
                    });

                    if (checkTaxStatus == true) {
                        Morris.Bar({
                            element: 'salesGraphReport',
                            data: monthlySalesData,
                            xkey: 'x',
                            ykeys: ['y', 'z'], labels: ['Sales Excluding Tax', 'Total Tax'],
                            parseTime: false,
                            yLabelFormat: function(y) {
                                return cSymbol + '. ' + accounting.formatMoney(y);
                            },
                            barColors: ['#FF8000', '#663300'],
                            hoverCallback: function(index, options, content) {
                                return(content);
                            },
                            stacked: true, xLabelAngle: 60,
                            hideHover: 'auto'
                        });
                    } else {
                        Morris.Bar({
                            element: 'salesGraphReport',
                            data: monthlySalesData,
                            xkey: 'x',
                            ykeys: ['y'],
                            labels: ['Sales Excluding Tax'],
                            parseTime: false,
                            yLabelFormat: function(y) {
                                return cSymbol + '. ' + accounting.formatMoney(y);
                            },
                            barColors: ['#FF8000'],
                            xLabelAngle: 60,
                            hideHover: 'auto'
                        });
                    }
                });
            }

        }

        else if (reportType == 'Daily') {
            $('#btnDaily').show();
            var checkTaxStatus = getCheckedStatus();
            var locationID = $('#locationList').val();
            var cusCategories = $('#customerCategoryList').val();
            var getdataurl = BASE_URL + '/api/sales-report/get-daily-sales-graph-data';
            inputs = new Array(
                    $('#fromDate').val(),
                    $('#toDate').val());
            if (validteinput(inputs)) {
                var getdatarequest = eb.post(getdataurl, {
                    fromDate: $('#fromDate').val(),
                    toDate: $('#toDate').val(),
                    locationID: locationID,
                    cusCategories: cusCategories
                });
                getdatarequest.done(function(respond) {
                    if (respond.status == true) {

                        var dailySalesRespond = respond.data['dailySalesData'];
                        var cSymbol = respond.data['cSymbol'];
                        var start = new Date($('#fromDate').val());
                        var end = new Date($('#toDate').val());

                        var dayArray = Array();
                        while (start <= end) {
                            var date = start.toString();
                            var dateSplit = date.split(" ");

                            monthToNumber(dateSplit[1]);
                            var new_date = (dateSplit[3] + '-' + (beforemonth) + '-' + dateSplit[2]);
                            dayArray.push(new_date);

                            var newDate = start.setDate(start.getDate() + 1);
                            start = new Date(newDate);
                        }

                        var dailySalesData = Array();
                        for (var i = 0; i < dayArray.length; i++) {
                            var amount;
                            var tax;
                            if (dailySalesRespond[dayArray[i]].length == 0) {
                                amount = 0;
                                tax = 0;
                            } else {                                
                                amount = parseFloat(Math.round((dailySalesRespond[dayArray[i]].salesAmount) * 100) / 100).toFixed(2);
                                amount = amount ? amount : 0.00;
                                tax = parseFloat(Math.round((dailySalesRespond[dayArray[i]].taxAmount) * 100) / 100).toFixed(2);
                                tax = tax ? tax : 0.00;
                            }
                            var totalSales = amount - tax;
                            dailySalesData.push({x: dayArray[i], y: totalSales, z: tax});

                        }

                        if (checkTaxStatus == true) {
                            Morris.Bar({
                                element: 'salesGraphReport',
                                data: dailySalesData,
                                hoverCallback: function(index, options, content) {
                                    return(content);
                                },
                                xkey: 'x',
                                ykeys: ['y', 'z'], stacked: true, labels: ['Sales Excluding Tax', 'Tax'],
                                barColors: ['#FF8000', '#663300'],
                                yLabelFormat: function(y) {
                                    return cSymbol + '. ' + accounting.formatMoney(y);
                                },
                                parseTime: false,
                                hideHover: 'auto',
                                xLabelAngle: 60
                            });
                        } else {
                            Morris.Bar({
                                element: 'salesGraphReport',
                                data: dailySalesData,
                                xkey: 'x',
                                ykeys: ['y'],
                                labels: ['Sales Excluding Tax'],
                                barColors: ['#FF8000'],
                                yLabelFormat: function(y) {
                                    return cSymbol + '. ' + accounting.formatMoney(y);
                                },
                                parseTime: false,
                                hideHover: 'auto',
                                xLabelAngle: 60
                            });


                        }
                    }
                });
            }

        }

        else if (reportType == 'Annual') {

            var checkTaxStatus = getCheckedStatus();
            var year = $('#year').val();
            year = year.replace(/\s+/g, "");
            $.trim(year);
            var locationID = $("#locationList").val();
            var cusCategories = $('#customerCategoryList').val();
            var getdataurl = BASE_URL + '/api/sales-report/get-annuval-sales-graph-data';
            var inputs = new Array($('#year').val());
            if ($('#year').val() == null || $('#year').val() == "") {
                p_notification(false, eb.getMessage('ERR_REPOPDF_YEARFILL'));
                $('#year').focus();
                return false;
            } else {
                $('#graph_div').show();
                var getdatarequest = eb.post(getdataurl, {
                    year: year,
                    locationID: locationID,
                    cusCategories: cusCategories
                });
                getdatarequest.done(function(respond) {
                    if (respond.status == true) {
                        var cSymbol = respond.data['cSymbol'];
                        var annualSalesData = respond.data['annualSalesData'];
                        
                        var q1SalesWithCreditNote = annualSalesData[0].Q1Amount - annualSalesData[0].Q1CreditNote - annualSalesData[0].Q1CreditNoteTax;
                        var q2SalesWithCreditNote = annualSalesData[0].Q2Amount - annualSalesData[0].Q2CreditNote - annualSalesData[0].Q2CreditNoteTax;
                        var q3SalesWithCreditNote = annualSalesData[0].Q3Amount - annualSalesData[0].Q3CreditNote - annualSalesData[0].Q3CreditNoteTax;
                        var q4SalesWithCreditNote = annualSalesData[0].Q4Amount - annualSalesData[0].Q4CreditNote - annualSalesData[0].Q4CreditNoteTax;
                        
                        var q1TaxWithCreditNote = annualSalesData[0].Q1Tax - annualSalesData[0].Q1CreditNoteTax;
                        var q2TaxWithCreditNote = annualSalesData[0].Q2Tax - annualSalesData[0].Q2CreditNoteTax;
                        var q3TaxWithCreditNote = annualSalesData[0].Q3Tax - annualSalesData[0].Q3CreditNoteTax;
                        var q4TaxWithCreditNote = annualSalesData[0].Q4Tax - annualSalesData[0].Q4CreditNoteTax;
                        
                        var qa1 = parseFloat(Math.round((q1SalesWithCreditNote) * 100) / 100).toFixed(2);
                        qa1 = qa1 ? qa1 : 0.00;
                        var qa2 = parseFloat(Math.round((q2SalesWithCreditNote) * 100) / 100).toFixed(2);
                        qa2 = qa2 ? qa2 : 0.00;
                        var qa3 = parseFloat(Math.round((q3SalesWithCreditNote) * 100) / 100).toFixed(2);
                        qa3 = qa3 ? qa3 : 0.00;
                        var qa4 = parseFloat(Math.round((q4SalesWithCreditNote) * 100) / 100).toFixed(2);
                        qa4 = qa4 ? qa4 : 0.00;
                        var qt1 = parseFloat(Math.round((q1TaxWithCreditNote) * 100) / 100).toFixed(2);
                        qt1 = qt1 ? qt1 : 0.00;
                        var qt2 = parseFloat(Math.round((q2TaxWithCreditNote) * 100) / 100).toFixed(2);
                        qt2 = qt2 ? qt2 : 0.00;
                        var qt3 = parseFloat(Math.round((q3TaxWithCreditNote) * 100) / 100).toFixed(2);
                        qt3 = qt3 ? qt3 : 0.00;
                        var qt4 = parseFloat(Math.round((q4TaxWithCreditNote) * 100) / 100).toFixed(2);
                        qt4 = qt4 ? qt4 : 0.00;
                        if (checkTaxStatus == true) {
                            Morris.Bar({
                                element: 'salesGraphReport',
                                data: [
                                    {y: 'Q1', a: qa1 - qt1, b: qt1}, {y: 'Q2', a: qa2 - qt2, b: qt2}, {y: 'Q3', a: qa3 - qt3, b: qt3}, {y: 'Q4', a: qa4 - qt4, b: qt4}
                                ],
                                xkey: 'y',
                                ykeys: ['a', 'b'], labels: ['Sales Excluding Tax', 'Total Tax'],
                                barColors: ['#FF8000', '#663300'],
                                yLabelFormat: function(y) {
                                    return cSymbol + '. ' + accounting.formatMoney(y);
                                },
                                hideHover: 'auto'
                            });
                        } else {
                            Morris.Bar({
                                element: 'salesGraphReport',
                                data: [
                                    {y: 'Q1', a: qa1 - qt1}, {y: 'Q2', a: qa2 - qt2}, {y: 'Q3', a: qa3 - qt3}, {y: 'Q4', a: qa4 - qt4}
                                ],
                                xkey: 'y',
                                ykeys: ['a'],
                                labels: ['Sales Excluding Tax'],
                                barColors: ['#FF8000'],
                                yLabelFormat: function(y) {
                                    return cSymbol + '. ' + accounting.formatMoney(y);
                                },
                                hideHover: 'auto'
                            });
                        }
                    }
                }
                );
            }
        }

        else {
            p_notification(false, eb.getMessage('ERR_SALEREPORT_CORRETYPE'));
        }
    });
    var fday1 = '-01';
    var tday30 = '-30';
    var tday31 = '-31';
    /* Set day to from_month text field
     * @author SANDUN <sandun@thinkcube.com>
     * */

    function fromday(fromMonthSplit) {
        var fromMonth = $('#fromMonth').val();

        if (fromMonthSplit == '01') {
            fm = fromMonth.concat(fday1);
        }
        else if (fromMonthSplit == '02') {
            fm = fromMonth.concat(fday1);
        }
        else if (fromMonthSplit == '03') {
            fm = fromMonth.concat(fday1);
        }
        else if (fromMonthSplit == '04') {
            fm = fromMonth.concat(fday1);
        }
        else if (fromMonthSplit == '05') {
            fm = fromMonth.concat(fday1);
        }
        else if (fromMonthSplit == '06') {
            fm = fromMonth.concat(fday1);
        }
        else if (fromMonthSplit == '07') {
            fm = fromMonth.concat(fday1);
        }
        else if (fromMonthSplit == '08') {
            fm = fromMonth.concat(fday1);
        }
        else if (fromMonthSplit == '09') {
            fm = fromMonth.concat(fday1);
        }
        else if (fromMonthSplit == '10') {
            fm = fromMonth.concat(fday1);
        }
        else if (fromMonthSplit == '11') {
            fm = fromMonth.concat(fday1);
        }
        else if (fromMonthSplit == '12') {
            fm = fromMonth.concat(fday1);
        }
    }

    /* Set day to to_month text field
     * @author SANDUN <sandun@thinkcube.com>
     * */
    function today(toMonthSplit, year) {
        var toMonth = $('#toMonth').val();
        if (toMonthSplit == '01') {
            tm = toMonth.concat(tday31);
        }
        else if (toMonthSplit == '02') {
            if (leapYear(year)) {
                var tday = '-29';
            } else {
                var tday = '-28';
            }
            tm = toMonth.concat(tday);
        }
        else if (toMonthSplit == '03') {
            tm = toMonth.concat(tday31);
        }
        else if (toMonthSplit == '04') {
            tm = toMonth.concat(tday30);
        }
        else if (toMonthSplit == '05') {
            tm = toMonth.concat(tday31);
        }
        else if (toMonthSplit == '06') {
            tm = toMonth.concat(tday30);
        }
        else if (toMonthSplit == '07') {
            tm = toMonth.concat(tday31);
        }
        else if (toMonthSplit == '08') {
            tm = toMonth.concat(tday31);
        }
        else if (toMonthSplit == '09') {
            tm = toMonth.concat(tday30);
        }
        else if (toMonthSplit == '10') {
            tm = toMonth.concat(tday31);
        }
        else if (toMonthSplit == '11') {
            tm = toMonth.concat(tday30);
        }
        else if (toMonthSplit == '12') {
            tm = toMonth.concat(tday31);
        }

    }

    /* validation function for Reporting Form
     * @author SANDUN <sandun@thinkcube.com>
     * */
    function validteinput(inputs) {
        var fromDate = inputs[0];
        var toDate = inputs[1];
        if (fromDate == null || fromDate == "") {
            p_notification(false, eb.getMessage('ERR_DAISALE_FROMD'));
            document.getElementById("fromDate").focus();
        } else if (toDate == null || toDate == "") {
            p_notification(false, eb.getMessage('ERR_DAISALE_TOD'));
            document.getElementById("toDate").focus();
        } else if (toDate < fromDate) {
            p_notification(false, eb.getMessage('ERR_DAISALE_TODFROMD'));
            document.getElementById("toDate").focus();
        }
        else {
            return true;
        }
    }

    /*
     * @author SANDUN <sandun@thinkcube.com>
     * return month number using month string name
     */
    function monthToNumber(month) {
        if (month == 'Jan') {
            beforemonth = '01';
        } else if (month == 'Feb') {
            beforemonth = '02';
        } else if (month == 'Mar') {
            beforemonth = '03';
        } else if (month == 'Apr') {
            beforemonth = '04';
        } else if (month == 'May') {
            beforemonth = '05';
        } else if (month == 'Jun') {
            beforemonth = '06';
        } else if (month == 'Jul') {
            beforemonth = '07';
        } else if (month == 'Aug') {
            beforemonth = '08';
        } else if (month == 'Sep') {
            beforemonth = '09';
        } else if (month == 'Oct') {
            beforemonth = 10;
        } else if (month == 'Nov') {
            beforemonth = 11;
        } else if (month == 'Dec') {
            beforemonth = 12;
        }
    }

    function initialCondition() {
        //show
        $('#viewGraph').show();
        $('#fromMonth,#toMonth').show();
        $('#tax,#lblTax').show();
        //hide
        $('#salesReport').hide();
        $('#monthlySalesGraph,#dailySalesGraph,#annualSalesGraph').hide();
        $('#fromDate,#toDate,#date,#year').hide();
        $('.sort').hide();
        $('#locationListt').hide();
        $('#sortlocation').hide();
        $('#locationradio').hide();
        $('#paymentradio').hide();
        $('.payment-list').hide();
    }

    function getCheckedStatus() {
        var checkTaxStatus;
        if (tax.checked == false) {
            checkTaxStatus = false;
        } else {
            checkTaxStatus = true;
        }
        return checkTaxStatus;
    }
    /*
     * @author SANDUN <sandun@thinkcube.com>
     * return boolean
     * */
    function leapYear(year) {
        return ((year % 4 == 0) && (year % 100 != 0)) || (year % 400 == 0);
    }

    //responsive issue fix
    $(window).bind('resize ready', function() {
        if ($(window).width() <= 480) {
            $('.report_from_month').css('margin-bottom', '15px').removeClass('col-xs-4').addClass('col-xs-12');
            $('.set_to_date').css('margin-bottom', '15px').removeClass('col-xs-4').addClass('col-xs-12');
            $('.btn_panel_height_high').removeClass('btn-group');
            $('#view_report, #view_graph, #generate_pdf, #excel_report').removeClass('btn-default').addClass('col-xs-12 btn-primary').css('margin-bottom', '8px');
            $('#btnMonthly, #btnDaily, #btn_annual').css('margin-bottom', '15px');
        } else {
            $('.report_from_month').css('margin-bottom', '0').removeClass('col-xs-12').addClass('col-xs-4');
            $('.set_to_date').css('margin-bottom', '0').removeClass('col-xs-12').addClass('col-xs-4');
            $('.btn_panel_height_high').addClass('btn-group');
            $('#view_report, #view_graph, #generate_pdf, #excel_report').addClass('btn-default').removeClass('col-xs-12 btn-primary').css('margin-bottom', '0px');
            $('#btnMonthly, #btnDaily, #btn_annual').css('margin-bottom', '0');
        }
    });
    
    function creditNoteFormValidation(input)
    {
        if (input.fromDate == null || input.fromDate == "") {
            p_notification(false, eb.getMessage('ERR_DAISALE_FROMD'));
            document.getElementById("fromDate").focus();
            return false;
        } else if (input.toDate == null || input.toDate == "") {
            p_notification(false, eb.getMessage('ERR_DAISALE_TOD'));
            document.getElementById("toDate").focus();
            return false;
        } else if (input.toDate < input.fromDate) {
            p_notification(false, eb.getMessage('ERR_DAISALE_TODFROMD'));
            document.getElementById("toDate").focus();
            return false;
        } else if (input.locationList == null) {
            p_notification(false, eb.getMessage('ERR_DEMAND_FORECAST_REPORT_LOCATION'));
            return false;
        } else if (input.salesPersonIds == null) {
            p_notification(false, eb.getMessage('ERR_INVOICE_REPORT_SELECT_SALESPERSON'));
            return false;
        } else {
            return true;
        }
    }

});
