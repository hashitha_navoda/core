/*
 * @auther sandun@thinkcube.com
 * this file has contained common functions of reporting
 */
var hideReportIFrame;

hideReportIFrame = function(reportIFrameId) {
    $('#' + reportIFrameId).hide();
    $("#" + reportIFrameId).contents().find('body').html('');
    $("#" + reportIFrameId).height(0);
    $('.selectpicker').selectpicker('deselectAll');
}

var setContentToReport;

setContentToReport = function(reportId, respond) {
    $('#' + reportId).css('height', '0');
    $("#" + reportId).show();
    $("#" + reportId).contents().find("body").html(respond.html);
    $("#" + reportId).height($("#" + reportId).contents().find("html").height());
    $("html, body").animate({scrollTop: $("#" + reportId).offset().top});//scroll to report Iframe
};

var setSelectPicker;
setSelectPicker = function() {
    $('.selectpicker').selectpicker();
    $('.selectpicker').selectpicker({
        style: 'btn-default',
        size: '10'
    });
};

var reSetReportContent;
reSetReportContent = function(reportId) {
    $("#" + reportId).contents().find('body').html('');
    $("#" + reportId).hide();
    $("#" + reportId).height(0);
};

var removeMultiSelect;
removeMultiSelect = function(ids) {
    var shift = ids.indexOf("multiselect");
    if (shift === 0) {
        ids.shift();
    }
};

var openPdfOnNewTab;
openPdfOnNewTab = function(data) {
    window.open(BASE_URL + '/' + data, '_blank');

};

