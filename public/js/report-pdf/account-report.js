$(document).ready(function () {

    var reportType = 'account-transcation';
    var bankId     = null;
    var accountId  = null;
    var transactionType = null;
    var fromDate   = null;
    var toDate     = null;

    reset();

    //initialize datetimepicker
    $('.datetimepicker').datetimepicker({
        format: 'yyyy-mm-dd',
        minView: 4,
        autoclose: true,
    });

    //for select report
    $(document).on('click', '.report_pills', function () {
        var reportId = $(this).attr('id');
        switch (reportId) {
            case 'accountTransaction' :
                reportType = 'account-transcation';
                break;

            default :
                console.log('invalid option');
        }
    });

    //load account list
    $(document).on('change', '#bankList', function () {
        var bankId = $(this).val();
        if (bankId) {
            getBankAccountListByBankId(bankId);
        } else {
            $('#accountList').empty();
            $('#accountList').append($("<option>", {value: '', html: '----- Select Account -----'}));
        }
    });

    //for view report
    $('#viewReport').click(function () {
        
        var accountId = $('#accountList').val();
        var fromDate  = $('#fromDate').val();
        var toDate    = $('#toDate').val();
        var transactionType = $('#transactionList').val();
        
        switch (reportType) {
            case 'account-transcation' :                
                var input = {
                    accountId : accountId,
                    fromDate  : fromDate,
                    toDate    : toDate,
                    transactionType : transactionType
                };
                
                if(validateAccountTransactionInput(input)){
                    eb.ajax({
                        type: 'POST',
                        url: BASE_URL + '/reports/get/account-report/generate-account-transaction-report',
                        dataType: 'json',
                        data: input,
                        success: function (respond) {
                            if (respond.status == true) {
                                setContentToReport('account-report', respond);
                            }
                        }
                    });
                }
                break;

            default :
                console.log('invalid option');
        }
    });

    //for view pdf
    $('#generatePdf').click(function () {
        
        var accountId = $('#accountList').val();
        var fromDate  = $('#fromDate').val();
        var toDate    = $('#toDate').val();
        var transactionType = $('#transactionList').val();
        
        switch (reportType) {
            case 'account-transcation' :
                var input = {
                    accountId : accountId,
                    fromDate  : fromDate,
                    toDate    : toDate,
                    transactionType : transactionType
                };
                
                if(validateAccountTransactionInput(input)){
                    eb.ajax({
                        type: 'POST',
                        url: BASE_URL + '/reports/get/account-report/generate-account-transaction-pdf',
                        dataType: 'json',
                        data: input,
                        success: function (respond) {
                            if (respond.status == true) {
                                window.open(BASE_URL + '/' + respond.data, '_blank');
                            }
                        }
                    });
                }
                break;

            default :
                console.log('invalid option');
        }
    });

    //for view csv
    $('#csvReport').click(function () {
        
        var accountId = $('#accountList').val();
        var fromDate  = $('#fromDate').val();
        var toDate    = $('#toDate').val();
        var transactionType = $('#transactionList').val();
        
        switch (reportType) {
            case 'account-transcation' :
                var input = {
                    accountId : accountId,
                    fromDate  : fromDate,
                    toDate    : toDate,
                    transactionType : transactionType
                };
                
                if(validateAccountTransactionInput(input)){
                    eb.ajax({
                        type: 'POST',
                        url: BASE_URL + '/reports/get/account-report/generate-account-transaction-csv',
                        dataType: 'json',
                        data: input,
                        success: function (respond) {
                            if (respond.status == true) {
                                window.open(BASE_URL + '/' + respond.data, '_blank');
                            }
                        }
                    });
                }
                break;

            default :
                console.log('invalid option');
        }
    });

    //reset fields
    function reset() {
        $('#bankList,#accountList,#transactionList').val("");
        $('#startDate,#endDate').val("");
        $('#account-report').hide();
    }

    //get bank account list
    function getBankAccountListByBankId(bankId) {
        $.ajax({
            type: 'POST',
            url: BASE_URL + '/account-api/bank-account-list-for-dropdown',
            data: {bankId: bankId},
            success: function (respond) {
                console.log(respond.data.list);
                //empty options
                $('#accountList').empty();
                $('#accountList').append($("<option>", {value: '', html: '----- Select Account -----'}));
                $(respond.data.list).each(function (i, v) {
                    $('#accountList').append($("<option>", {value: v.value, html: v.text}));
                });
            }
        });
    }

    function validateAccountTransactionInput(input) {
        $('#account-report').hide();
        if (!input.accountId) {
            p_notification(false, 'please select account');
        } else if ((input.fromDate && !input.toDate) || (!input.fromDate && input.toDate)) {
            p_notification(false, 'invalid date range');
        } else if (input.fromDate && input.toDate && (input.fromDate > input.toDate)) {
            p_notification(false, 'invalid date range');
        } else {
            return true;
        }
    }

});
