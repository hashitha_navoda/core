$(document).ready(function() {

    var reportType = 'loyaltyCardHolder';
    var isAllCustomers = false;
    var isAllCards = false;
    var isAllCardNumbers = false;

    loyaltyCardIFrameHide();

    loadDropDownFromDatabase('/customerAPI/search-customers-for-dropdown', "", "withDeactivatedCustomers", '#customerName', "", true);

    loadDropDownFromDatabase('/loyalty/search-loyalty-cards-for-dropdown', "", 0, '#cardName', "", true);

    loadDropDownFromDatabase('/loyalty/search-loyalty-card-numbers-for-dropdown', "", 0, '#cardCode', "", true);

    $('.isAllCustomers').on('click', function() {
        isAllCustomers = $(this).val();
        if (isAllCustomers == true) {
            $('#customerName').val('').trigger('change');
            $('#customerName').prop('disabled', true);
        } else {
            $('#customerName').prop('disabled', false);
        }
        $('#customerName').selectpicker('refresh');
    });

    $('.isAllCards').on('click', function() {
        isAllCards = $(this).val();
        if (isAllCards == true) {
            $('#cardName').val('').trigger('change');
            $('#cardName').prop('disabled', true);
        } else {
            $('#cardName').prop('disabled', false);
        }
        $('#cardName').selectpicker('refresh');
    });

    $('.isAllCardCodes').on('click', function() {
        isAllCardNumbers = $(this).val();
        if (isAllCardNumbers == true) {
            $('#cardCode').val('').trigger('change');
            $('#cardCode').prop('disabled', true);
        } else {
            $('#cardCode').prop('disabled', false);
        }
        $('#cardCode').selectpicker('refresh');
    });

    $('#viewReport').click(function(e) {
        e.preventDefault();
        if (reportType == 'loyaltyCardHolder') {
            var obj = {};
            obj.isAllCustomers = isAllCustomers;
            obj.customerIds = $('#customerName').val();
            obj.isAllCards = isAllCards;
            obj.cardIds = $('#cardName').val();
            obj.isAllCardNumbers = isAllCardNumbers;
            obj.cardNumbers = $('#cardCode').val();

            if (validateLoyaltyCardReport(obj)) {
                var url = BASE_URL + '/reports/loyalty-card-api/loyalty-card-holder-view';
                var getpdfdatarequest = eb.post(url, obj);
                getpdfdatarequest.done(function(respond) {
                    if (respond.status == true) {
                        setContentToReport('loyalty-report-iframe', respond);
                    }
                });
            }
        }
    });

    $('#generatePdf').on('click', function(e) {
        e.preventDefault();
        if (reportType == 'loyaltyCardHolder') {
            var obj = {};
            obj.isAllCustomers = isAllCustomers;
            obj.customerIds = $('#customerName').val();
            obj.isAllCards = isAllCards;
            obj.cardIds = $('#cardName').val();
            obj.isAllCardNumbers = isAllCardNumbers;
            obj.cardNumbers = $('#cardCode').val();

            if (validateLoyaltyCardReport(obj)) {
                var url = BASE_URL + '/reports/loyalty-card-api/loyalty-card-holder-pdf';
                eb.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: obj,
                    success: function(respond) {
                        if (respond.status == true) {
                            window.open(BASE_URL + '/' + respond.data, '_blank');
                        }
                    }
                });
            }
        }
    });

    $('#csvReport').on('click', function(e) {
        e.preventDefault();
        if (reportType == 'loyaltyCardHolder') {
            var obj = {};
            obj.isAllCustomers = isAllCustomers;
            obj.customerIds = $('#customerName').val();
            obj.isAllCards = isAllCards;
            obj.cardIds = $('#cardName').val();
            obj.isAllCardNumbers = isAllCardNumbers;
            obj.cardNumbers = $('#cardCode').val();

            if (validateLoyaltyCardReport(obj)) {
                var url = BASE_URL + '/reports/loyalty-card-api/loyalty-card-holder-csv';
                eb.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: obj,
                    success: function(respond) {
                        if (respond.status == true) {
                            window.open(BASE_URL + '/' + respond.data, '_blank');
                        }
                    }
                });
            }
        }
    });

    function loyaltyCardIFrameHide()
    {
        $("#loyalty-report-iframe").hide();
        $("#loyalty-report-iframe").contents().find("body").html('');
    }

    function validateLoyaltyCardReport(input)
    {
        if (input.isAllCustomers == false && (input.customerIds == null || input.customerIds == "")) {
            p_notification(false, eb.getMessage('ERR_CUSREPO_SELECUST'));
            return false;
        } else if (input.isAllCards == false && (input.cardIds == null || input.cardIds == "")) {
            p_notification(false, eb.getMessage('ERR_SELECT_LOYAL_CARD'));
            return false;
        } else if (input.isAllCardNumbers == false && (input.cardNumbers == null || input.cardNumbers == "")) {
            p_notification(false, eb.getMessage('ERR_SELECT_LOYAL_CARD_NUMBER'));
            return false;
        } else {
            return true;
        }
    }

});