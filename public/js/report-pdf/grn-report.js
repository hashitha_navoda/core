/*
 * @author SANDUN <sandun@thinkcube.com>
 * GRN Reporting functions
 */
$(document).ready(function() {
    var reportType = 'grnStatus';
    var grnIds = $('#grnLists').val();
    var isAllGRNs = false;
    var isAllSuppliers = false;
    var isAllItems = false;
    var lengthOfSelectPicker = $(".selectpicker").find("option").length;
    divLoading();
    setSelectPicker();//select picker setting

    var checkin = $('#fromDate').datepicker({}).on('changeDate', function(ev) {
        if (ev.date.valueOf() < checkout.date.valueOf()) {
            var newDate = new Date(ev.date)
            newDate.setDate(newDate.getDate());
            checkout.setValue(newDate);
        }
        checkin.hide();
        $('#toDate')[0].focus();
    }).data('datepicker');
    var checkout = $('#toDate').datepicker({
        onRender: function(date) {
            return date.valueOf() < checkin.date.valueOf() ? 'disabled' : '';
        }
    }).on('changeDate', function(ev) {
        checkout.hide();
    }).data('datepicker');

    $('#grnStatus').on('click', function() {
        reportType = 'grnStatus';
        grnIds = $('#grnLists').val();
        $('#item_div,#supplier_div').addClass('hidden');
        $('#location_div').removeClass('hidden');
    });
    
    $('#grnItemDetails').on('click', function() {
        reportType = 'grnItemDetails';
        $('#item_div,#supplier_div,#location_div').removeClass('hidden');
        isAllGRNs = $('.isAllGRNs:checked').val();
        isAllItems = $('.isAllItems:checked').val();
        isAllSuppliers = $('.isAllSuppliers:checked').val();
    });
    $('#grnWisePurchaseReturn').on('click', function() {
        reportType = 'grnWisePurchaseReturn';
        $('#item_div,#supplier_div,#location_div').addClass('hidden');
        isAllGRNs = $('.isAllGRNs:checked').val();
    });
    $('#grnWisePurchaseInvoice').on('click', function() {
        reportType = 'grnWisePurchaseInvoice';
        $('#item_div,#supplier_div,#location_div').addClass('hidden');
        isAllGRNs = $('.isAllGRNs:checked').val();
    });
    $('#editedGrn').on('click', function() {
        reportType = 'editedGrn';
        $('#supplier_div').removeClass('hidden');
        $('#item_div,#location_div,#grn_div').addClass('hidden');
        isAllSuppliers = $('.isAllSuppliers:checked').val();
    });

    loadDropDownFromDatabase('/api/grn/search-all-grn-for-dropdown', "", 0, '#grnLists', "", true);
    loadDropDownFromDatabase('/productAPI/search-user-active-location-products-for-dropdown', "", 0, '#itemList', "", true);
    loadDropDownFromDatabase('/supplierAPI/search-suppliers-for-dropdown', "", 0, '#supplierLists', "", true);
    
    $('#grnLists').on('change', function() {
        var thisValue = $(this).val();
        if (thisValue === null) {
            reSetReportContent('grnStatusReport');
        }
    });

    $('#viewReport').on('click', function() {
        if (reportType == 'grnStatus') {

            var input = {};
            input.isAllGRlNs = isAllGRNs;
            input.grnIds     = $('#grnLists').val();
            input.fromDate   = $('#fromDate').val();
            input.toDate     = $('#toDate').val();
            input.locations  = $('#locations').val();

            if(validteGrnStatusReportInput(input)){
                var url = BASE_URL + '/api/grn-report/view-grn-status';
                eb.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: input,
                    success: function(respond) {
                        if (respond.status == true) {
                            setContentToReport('grnStatusReport', respond);
                        }
                    }
                });
            }
        } else if (reportType === 'grnItemDetails') {
            var input = {};
            input.isAllGRNs      = isAllGRNs;
            input.grnIds         = $('#grnLists').val();
            input.isAllItems     = isAllItems;
            input.itemIds        = $('#itemList').val();
            input.isAllSuppliers = isAllSuppliers;
            input.supplierIds    = $('#supplierLists').val();
            input.fromDate       = $('#fromDate').val();
            input.toDate         = $('#toDate').val();
            input.locationIds    = $('#locations').val();
            
            if(validateGrnItemDetailsReport(input)) {
                var url = BASE_URL + '/api/grn-report/grn-item-details-report';
                eb.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: input,
                    success: function(respond) {
                        if (respond.status) {
                            setContentToReport('grnStatusReport', respond);
                        }
                    }
                });
            }
        } else if(reportType ==='grnWisePurchaseReturn'){
            var url = BASE_URL + '/api/grn-report/grn-wise-purchase-return-report';
            var input = {};
            input.fromDate = $('#fromDate').val();
            input.toDate = $('#toDate').val();
            input.isAllGRNs = isAllGRNs;
            input.grnIds = $('#grnLists').val();
            if (grnWisePRReportValidation(input)) {
                var getpdfdatarequest = eb.post(url, input);
                getpdfdatarequest.done(function(respond) {
                    if (respond.status == true) {
                        setContentToReport('grnStatusReport', respond);
                    }
                });
            }
        } else if(reportType ==='grnWisePurchaseInvoice'){
            var url = BASE_URL + '/api/grn-report/grn-wise-purchase-invoice-report';
            var input = {};
            input.fromDate = $('#fromDate').val();
            input.toDate = $('#toDate').val();
            input.isAllGRNs = isAllGRNs;
            input.grnIds = $('#grnLists').val();
            if (grnWisePRReportValidation(input)) {
                var getpdfdatarequest = eb.post(url, input);
                getpdfdatarequest.done(function(respond) {
                    if (respond.status == true) {
                        setContentToReport('grnStatusReport', respond);
                    }
                });
            }
        } else if(reportType ==='editedGrn'){
            var url = BASE_URL + '/api/grn-report/edited-grn-report';

            var inputs = [
                $('#fromDate').val(),
                $('#toDate').val(),
            ];
            if (!($('#fromDate').val() && $('#toDate').val()) || validteinput(inputs))
            {
                var getpdfdatarequest = eb.post(url, {
                    fromDate: $('#fromDate').val(),
                    toDate: $('#toDate').val(),
                    isAllSuppliers : isAllSuppliers,
                    supplierIds    : $('#supplierLists').val(),
                });
                getpdfdatarequest.done(function(respond) {
                    if (respond.status) {
                        setContentToReport('grnStatusReport', respond);
                    }
                });
            }
        }
    });

    $('#generatePdf').on('click', function() {
        if (reportType == 'grnStatus') {

            var input = {};
            input.isAllGRlNs = isAllGRNs;
            input.grnIds     = $('#grnLists').val();
            input.fromDate   = $('#fromDate').val();
            input.toDate     = $('#toDate').val();
            input.locations  = $('#locations').val();

            if (validteGrnStatusReportInput(input)) {
                var url = BASE_URL + '/api/grn-report/generate-grn-status-pdf';
                eb.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: input,
                    success: function(respond) {
                        if (respond.status == true) {
                            setContentToReport('grnStatusReport', respond);
                            window.open(BASE_URL + '/' + respond.data, '_blank');
                        }
                    }
                });
            }
        } else if (reportType === 'grnItemDetails') {
            var input = {};
            input.isAllGRNs      = isAllGRNs;
            input.grnIds         = $('#grnLists').val();
            input.isAllItems     = isAllItems;
            input.itemIds        = $('#itemList').val();
            input.isAllSuppliers = isAllSuppliers;
            input.supplierIds    = $('#supplierLists').val();
            input.fromDate       = $('#fromDate').val();
            input.toDate         = $('#toDate').val();
            input.locationIds    = $('#locations').val();
            
            if(validateGrnItemDetailsReport(input)) {
                var url = BASE_URL + '/api/grn-report/grn-item-details-pdf';
                eb.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: input,
                    success: function(respond) {
                        if (respond.status) {
                            setContentToReport('grnStatusReport', respond);
                            window.open(BASE_URL + '/' + respond.data, '_blank');
                        }
                    }
                });
            }
        } else if(reportType ==='grnWisePurchaseReturn'){
            var url = BASE_URL + '/api/grn-report/grn-wise-purchase-return-pdf';
            var input = {};
            input.fromDate = $('#fromDate').val();
            input.toDate = $('#toDate').val();
            input.isAllGRNs = isAllGRNs;
            input.grnIds = $('#grnLists').val();
            if (grnWisePRReportValidation(input)) {
                var getpdfdatarequest = eb.post(url, input);
                getpdfdatarequest.done(function(respond) {
                    if (respond.status == true) {
                        setContentToReport('grnStatusReport', respond);
                        window.open(BASE_URL + '/' + respond.data, '_blank');
                    }
                });
            }
        } else if(reportType ==='grnWisePurchaseInvoice'){
            var url = BASE_URL + '/api/grn-report/grn-wise-purchase-invoice-pdf';
            var input = {};
            input.fromDate = $('#fromDate').val();
            input.toDate = $('#toDate').val();
            input.isAllGRNs = isAllGRNs;
            input.grnIds = $('#grnLists').val();
            if (grnWisePRReportValidation(input)) {
                var getpdfdatarequest = eb.post(url, input);
                getpdfdatarequest.done(function(respond) {
                    if (respond.status == true) {
                        setContentToReport('grnStatusReport', respond);
                        window.open(BASE_URL + '/' + respond.data, '_blank');
                    }
                });
            }
        } else if(reportType ==='editedGrn'){
            var url = BASE_URL + '/api/grn-report/edited-grn-pdf';

            var inputs = [
                $('#fromDate').val(),
                $('#toDate').val(),
            ];
            if (!($('#fromDate').val() && $('#toDate').val()) || validteinput(inputs))
            {
                var getpdfdatarequest = eb.post(url, {
                    fromDate: $('#fromDate').val(),
                    toDate: $('#toDate').val(),
                    isAllSuppliers : isAllSuppliers,
                    supplierIds    : $('#supplierLists').val(),
                });
                getpdfdatarequest.done(function(respond) {
                    if (respond.status) {
                        setContentToReport('grnStatusReport', respond);
                        window.open(BASE_URL + '/' + respond.data, '_blank');
                    }
                });
            }
        }
    });

    $('#csvReport').on('click', function() {
        if (reportType == 'grnStatus') {

            var input = {};
            input.isAllGRlNs = isAllGRNs;
            input.grnIds     = $('#grnLists').val();
            input.fromDate   = $('#fromDate').val();
            input.toDate     = $('#toDate').val();
            input.locations  = $('#locations').val();

            if (validteGrnStatusReportInput(input)) {
                var url = BASE_URL + '/api/grn-report/generate-grn-status-sheet';
                eb.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: input,
                    success: function(respond) {
                        if (respond.status == true) {
                            setContentToReport('grnStatusReport', respond);
                            window.open(BASE_URL + '/' + respond.data, '_blank');
                        }
                    }
                });
            }
        } else if (reportType === 'grnItemDetails') {
            var input = {};
            input.isAllGRNs      = isAllGRNs;
            input.grnIds         = $('#grnLists').val();
            input.isAllItems     = isAllItems;
            input.itemIds        = $('#itemList').val();
            input.isAllSuppliers = isAllSuppliers;
            input.supplierIds    = $('#supplierLists').val();
            input.fromDate       = $('#fromDate').val();
            input.toDate         = $('#toDate').val();
            input.locationIds    = $('#locations').val();
            
            if(validateGrnItemDetailsReport(input)) {
                var url = BASE_URL + '/api/grn-report/grn-item-details-csv';
                eb.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: input,
                    success: function(respond) {
                        if (respond.status) {
                            setContentToReport('grnStatusReport', respond);
                            window.open(BASE_URL + '/' + respond.data, '_blank');
                        }
                    }
                });
            }
        }  else if(reportType ==='grnWisePurchaseReturn'){
            var url = BASE_URL + '/api/grn-report/grn-wise-purchase-return-csv';
            var input = {};
            input.fromDate = $('#fromDate').val();
            input.toDate = $('#toDate').val();
            input.isAllGRNs = isAllGRNs;
            input.grnIds = $('#grnLists').val();
            if (grnWisePRReportValidation(input)) {
                var getpdfdatarequest = eb.post(url, input);
                getpdfdatarequest.done(function(respond) {
                    if (respond.status == true) {
                        setContentToReport('grnStatusReport', respond);
                        window.open(BASE_URL + '/' + respond.data, '_blank');
                    }
                });
            }
        } else if(reportType ==='grnWisePurchaseInvoice'){
            var url = BASE_URL + '/api/grn-report/grn-wise-purchase-invoice-csv';
            var input = {};
            input.fromDate = $('#fromDate').val();
            input.toDate = $('#toDate').val();
            input.isAllGRNs = isAllGRNs;
            input.grnIds = $('#grnLists').val();
            if (grnWisePRReportValidation(input)) {
                var getpdfdatarequest = eb.post(url, input);
                getpdfdatarequest.done(function(respond) {
                    if (respond.status == true) {
                        setContentToReport('grnStatusReport', respond);
                        window.open(BASE_URL + '/' + respond.data, '_blank');
                    }
                });
            }
        } else if(reportType ==='editedGrn'){
            var url = BASE_URL + '/api/grn-report/edited-grn-sheet';

            var inputs = [
                $('#fromDate').val(),
                $('#toDate').val(),
            ];
            if (!($('#fromDate').val() && $('#toDate').val()) || validteinput(inputs))
            {
                var getpdfdatarequest = eb.post(url, {
                    fromDate: $('#fromDate').val(),
                    toDate: $('#toDate').val(),
                    isAllSuppliers : isAllSuppliers,
                    supplierIds    : $('#supplierLists').val(),
                });
                getpdfdatarequest.done(function(respond) {
                    if (respond.status) {
                        setContentToReport('grnStatusReport', respond);
                        window.open(BASE_URL + '/' + respond.data, '_blank');
                    }
                });
            }
        }
    });

    function divLoading() {
        $('#grnStatusReport').hide();
    }

    $('.isAllGRNs').on('click', function() {
        isAllGRNs = $(this).val();
        if (isAllGRNs == true) {
            reSetReportContent('grnStatusReport');
            $('#grnLists').val('').trigger('change');
            $('#grnLists').prop('disabled', true);
            p_notification('info', eb.getMessage('INFO_CUS_REPORT_ALL_CUS_SELECTION'));
        } else {
            reSetReportContent('grnStatusReport');
            $('#grnLists').prop('disabled', false);
        }
        hideReportIFrame();
    });
    
    $('.isAllItems').on('click', function() {
        isAllItems = $(this).val();
        if (isAllItems == true) {
            $('#itemList').val('').trigger('change');
            $('#itemList').prop('disabled', true);
            p_notification('info', eb.getMessage('INFO_CUS_REPORT_ALL_CUS_SELECTION'));
        } else {
            $('#itemList').prop('disabled', false);
        }
        hideReportIFrame();
    });
    
    $('.isAllSuppliers').on('click', function() {
        isAllSuppliers = $(this).val();
        if (isAllSuppliers == true) {            
            $('#supplierLists').val('').trigger('change');
            $('#supplierLists').prop('disabled', true);
            p_notification('info', eb.getMessage('INFO_CUS_REPORT_ALL_CUS_SELECTION'));
        } else {
            $('#supplierLists').prop('disabled', false);
        }
        hideReportIFrame();
    });
    
    function validateGrnItemDetailsReport(input)
    {
        if (input.isAllGRNs == false && input.grnIds == null) {
            p_notification(false, eb.getMessage('ERR_GRNREPO_SELEGRNCODE'));
            return false;
        } else if (input.isAllItems == false && input.itemIds == null) {
            p_notification(false, eb.getMessage('ERR_DAISALE_ITEMWISETBL_PRODUCT_ERR'));
            return false;
        } else if (input.isAllSuppliers == false && input.supplierIds == null) {
            p_notification(false, eb.getMessage('ERR_STOCKREPO_SELESUPPLIER'));
            return false;
        } else if (input.locationIds == null) {
            p_notification(false, eb.getMessage('ERR_DEMAND_FORECAST_REPORT_LOCATION'));
            return false;
        } else if (input.fromDate == null || input.fromDate == "") {
            p_notification(false, eb.getMessage('ERR_DAISALE_FROMD'));
            document.getElementById("fromDate").focus();
            return false;
        } else if (input.toDate == null || input.toDate == "") {
            p_notification(false, eb.getMessage('ERR_DAISALE_TOD'));
            document.getElementById("toDate").focus();
            return false;
        } else if (input.toDate < input.fromDate) {
            p_notification(false, eb.getMessage('ERR_DAISALE_TODFROMD'));
            document.getElementById("toDate").focus();
            return false;
        } else {
            return true;
        }
    }

    //for validate GRN status report input
    function validteGrnStatusReportInput(input)
    {
        if(input.isAllGRlNs == false && input.grnIds == null){
            p_notification(false, eb.getMessage('ERR_GRNREPO_SELEGRNCODE'));
            return false;
        } else if ((input.toDate) && (input.fromDate == null || input.fromDate == "")) {
            p_notification(false, eb.getMessage('ERR_DAISALE_FROMD'));
            document.getElementById("fromDate").focus();
            return false;
        } else if ((input.fromDate) && (input.toDate == null || input.toDate == "")) {
            p_notification(false, eb.getMessage('ERR_DAISALE_TOD'));
            document.getElementById("toDate").focus();
            return false;
        } else if ((input.fromDate && input.toDate) && input.fromDate > input.toDate) {
            p_notification(false, eb.getMessage('ERR_DAISALE_TODFROMD'));
            document.getElementById("toDate").focus();
            return false;
        } else {
            return true;
        }
    }

    function grnWisePRReportValidation(input)
    {
        if (input.fromDate == null || input.fromDate == "") {
            p_notification(false, eb.getMessage('ERR_DAISALE_FROMD'));
            document.getElementById("fromDate").focus();
            return false;
        } else if (input.isAllGRlNs == false && input.grnIds == null) {
            p_notification(false, eb.getMessage('ERR_GRNREPO_SELEGRNCODE'));
            return false;
        } else {
            return true;
        }
    }

    function validteinput(inputs) {
        var fromDate = inputs[0];
        var toDate = inputs[1];
        if (fromDate == null || fromDate == "") {
            p_notification(false, eb.getMessage('ERR_DAISALE_FROMD'));
            document.getElementById("fromDate").focus();
        }
        else if (toDate == null || toDate == "") {
            p_notification(false, eb.getMessage('ERR_DAISALE_TOD'));
            document.getElementById("toDate").focus();
        } else if (fromDate > toDate) {
            p_notification(false, eb.getMessage('ERR_DAISALE_TODFROMD'));
            document.getElementById("toDate").focus();
        } else {
            return true;
        }
    }

});
