$(document).ready(function(){
	var allCustomers = true;

	loadDropDownFromDatabase('/customerAPI/search-customers-for-dropdown', "", "withDeactivatedCustomers", '#customerName');
    $('#customerName').selectpicker('refresh');
    $('#AllCustomers').attr('checked',true);
    $('#customerName').attr('disabled', true);

    $('#customCustomerSelection').on('click',function(){
    	$('#customerName').attr('disabled', false);
    	$('#customerName').selectpicker('refresh');
    	$('#customerCategoryList').val('').trigger('change');
        $('#customerCategoryList').prop('disabled', true);
        $('.cusCatgry').addClass('hidden');
        allCustomers = false;
    });
    $('#AllCustomers').on('click',function(){
    	$('#customerName').html('');
    	$('#customerName').selectpicker('refresh');
    	$('#customerName').attr('disabled', true);
        $('#customerCategoryList').prop('disabled', false);
    	$('.cusCatgry').removeClass('hidden');
        allCustomers = true;
    });

    var checkin = $('#fromDate').datepicker({
    }).on('changeDate', function(ev) {
        if (ev.date.valueOf() < checkout.date.valueOf()) {
            var newDate = new Date(ev.date)
            newDate.setDate(newDate.getDate());
            checkout.setValue(newDate);
        }
        checkin.hide();
        $('#toDate')[0].focus();
    }).data('datepicker');
    var checkout = $('#toDate').datepicker({
        onRender: function(date) {
            return date.valueOf() <= checkin.date.valueOf();
        }
    }).on('changeDate', function(ev) {
        checkout.hide();
    }).data('datepicker');


    $('#viewReport').on('click', function(){
    	var viewUrl = BASE_URL +'/reports/api/quotation-report/quotation-data-view';
    	var detailsSet = collectReferenceData();
    	if(detailsSet){
    		var getpdfdatarequest = eb.post(
    		viewUrl,
    		detailsSet
            );

        	getpdfdatarequest.done(function(respond) {
                if (respond.status == true) {
                    setContentToReport('salesReport', respond);
                }
            });
    	}

    });

    $('#generatePdf').on('click', function(){
    	var viewUrl = BASE_URL +'/reports/api/quotation-report/quotation-data-pdf';
    	var detailsSet = collectReferenceData();
    	if(detailsSet) {
    		eb.ajax({
                type: 'POST',
                url: viewUrl,
                dataType: 'json',
                data: detailsSet,
                	success: function(respond) {
                    if (respond.status == true) {
                        window.open(BASE_URL + '/' + respond.data, '_blank');
                    }
                }
            });
    	}

    });

    $('#csvReport').on('click', function(){
    	var viewUrl = BASE_URL +'/reports/api/quotation-report/quotation-csv';
    	var detailsSet = collectReferenceData();
    	if(detailsSet) {
    		eb.ajax({
            	type: 'POST',
            	url: viewUrl,
            	dataType: 'json',
            	data: detailsSet,
            	success: function(respond) {
                	if (respond.status == true) {
                    	window.open(BASE_URL + '/' + respond.data, '_blank');
                	}
            	}
        	});
    	}

    });


    /**
    * add input data to the array
    **/
    function collectReferenceData(){
    	var dataSet = {
    		fromDate : $('#fromDate').val(),
    		toDate : $('#toDate').val(),
    		allCustomers : allCustomers,
    		customer : $('#customerName').val(),
    		locations : $('#locationList').val(),
    		paymentTermList : $('#paymentTermList').val(),
    		quotationStatus : $('#quotationStatus').val(),
    		salesPersons : $('#salesPersons').val(),
            cusCategories : $('#customerCategoryList').val()
    		};
    	if(validateDataSet(dataSet)){
    		return dataSet;
    	} else {
    		return false;
    	}


    }
    /**
    * validate input fields
    **/
    function validateDataSet(dataSet) {
    	if(dataSet.fromDate == '' || dataSet.fromDate == null){
    		p_notification(false ,eb.getMessage('ERR_FROM_DATE'));
    		$('#fromDate').focus();
    		return false;
       	} else if(dataSet.toDate == '' || dataSet.toDate == null){
       		p_notification(false, eb.getMessage('ERR_TO_DATE'));
       		$('#toDate').focus();
       		return false;
       	} else if(dataSet.toDate < dataSet.fromDate){
       		p_notification(false, eb.getMessage('ERR_FROM_TO_DATE'));
       		$('#toDate').focus();
       		return false;
       	} else if(!dataSet.allCustomers && (dataSet.customer == null || dataSet.customer == '')){
      		p_notification(false, eb.getMessage('ERR_CUS_SELECT'));
      		$('#customer').focus();
      		return false;
      	}
      	 return true;

    }

});