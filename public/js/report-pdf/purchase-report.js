/*
 * @author SANDUN <sandun@thinkcube.com>
 * GRN Reporting functions
 */
$(document).ready(function() {
    var reportType = 'poStatus';
    var poIds = $('#poLists').val();
    var productIds;
    var lengthOfPOList = $('#poLists').find('option').length;
    var isAllPO;
    var allProducts;
    divLoading();
    setSelectPicker();//set select picker

    var checkin = $('#fromDate').datepicker({
    }).on('changeDate', function(ev) {
        if (ev.date.valueOf() < checkout.date.valueOf()) {
            var newDate = new Date(ev.date)
            newDate.setDate(newDate.getDate());
            checkout.setValue(newDate);
        }
        checkin.hide();
        $('#toDate')[0].focus();
    }).data('datepicker');
    var checkout = $('#toDate').datepicker({
        onRender: function(date) {
            return date.valueOf() <= checkin.date.valueOf();
        }
    }).on('changeDate', function(ev) {
        checkout.hide();
    }).data('datepicker');

    loadDropDownFromDatabase('/productAPI/search-user-active-location-products-for-dropdown',
         "", 0, '#productLists', "", true);
    $('#productLists').on('change', function() {
        productIds = $(this).val();
    });


    loadDropDownFromDatabase('/api/po/search-allPo-for-dropdown', "", 0, '#poLists', "", true);
    $('#poLists').on('change', function() {
        var thisValue = $(this).val();
        if (thisValue == null) {
            reSetReportContent('purchaseOrderReport');
        }
    });

    $('#poWithGrn').on('click', function(){
        $('.date-div').removeClass('hidden');
        reportType = 'grnAndPo';
    });
    $('#poStatus').on('click', function(){
        $('.product-div').addClass('hidden');
        $('.date-div').addClass('hidden');
        reportType = 'poStatus';
    });

    $('#viewReport').on('click', function() {
        poIds = $('#poLists').val();
        if (reportType == 'poStatus') {
            if (poIds || isAllPO == true) {
                $("#poStatus_report_body").html('');
                var url = BASE_URL + '/api/purchase-report/view-purchase-order';
                eb.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: {poIds: poIds, isAllPO: isAllPO},
                    success: function(respond) {
                        if (respond.status == true) {
                            setContentToReport('purchaseOrderReport', respond);
                        }
                    }
                });
            } else if (lengthOfPOList == 0) {
                p_notification(false, eb.getMessage('ERR_NORECORDS_IN_SELECTPICKER'));
            } else {
                p_notification(false, eb.getMessage('ERR_PURCHREPO_PCODE'));
            }
        } else if(reportType == 'grnAndPo'){
            var validateInputs = validateInputSet();
            if(validateInputs){
                $("#poStatus_report_body").html('');
                var url = BASE_URL + '/api/purchase-report/view-purchase-order-with-grn';
                eb.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: {
                        poIds: poIds,
                        isAllPO: isAllPO,
                        productIds: productIds,
                        allProducts:allProducts,
                        fromDate: $('#fromDate').val(),
                        toDate:  $('#toDate').val(),
                    },
                    success: function(respond) {
                        if (respond.status == true) {
                            setContentToReport('purchaseOrderReport', respond);
                        }
                    }
                });
            }
        }
    });

    $('#generatePdf').on('click', function() {
        poIds = $('#poLists').val();
        if (reportType == 'poStatus') {
            if (poIds || isAllPO == true) {
                var url = BASE_URL + '/api/purchase-report/generate-po-status-pdf';
                eb.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: {poIds: poIds, isAllPO: isAllPO},
                    success: function(respond) {
                        if (respond.status == true) {
                            window.open(BASE_URL + '/' + respond.data, '_blank');
                        }
                    }
                });
            } else if (lengthOfPOList == 0) {
                p_notification(false, eb.getMessage('ERR_NORECORDS_IN_SELECTPICKER'));
            } else {
                p_notification(false, eb.getMessage('ERR_PURCHREPO_PCODE'));
            }
        } else if(reportType == 'grnAndPo'){
            var validateInputs = validateInputSet();
            if(validateInputs){
                $("#poStatus_report_body").html('');
                var url = BASE_URL + '/api/purchase-report/view-purchase-order-with-grn-pdf';
                eb.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: {
                        poIds: poIds,
                        isAllPO: isAllPO,
                        productIds: productIds,
                        allProducts:allProducts,
                        fromDate: $('#fromDate').val(),
                        toDate:  $('#toDate').val(),
                    },
                    success: function(respond) {
                        if (respond.status == true) {
                            window.open(BASE_URL + '/' + respond.data, '_blank');
                        }
                    }
                });
            }
        }
    });

    $('#sheetReport').on('click', function() {
        poIds = $('#poLists').val();
        if (reportType == 'poStatus') {
            if (poIds || isAllPO == true) {
                var url = BASE_URL + '/api/purchase-report/generate-po-status-sheet';
                eb.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: {poIds: poIds, isAllPO: isAllPO},
                    success: function(respond) {
                        if (respond.status == true) {
                            window.open(BASE_URL + '/' + respond.data, '_blank');
                        }
                    }
                });
            } else if (lengthOfPOList == 0) {
                p_notification(false, eb.getMessage('ERR_NORECORDS_IN_SELECTPICKER'));
            } else {
                p_notification(false, eb.getMessage('ERR_PURCHREPO_PCODE'));
            }
        } else if(reportType == 'grnAndPo'){
            var validateInputs = validateInputSet();
            if(validateInputs){
                $("#poStatus_report_body").html('');
                var url = BASE_URL + '/api/purchase-report/view-purchase-order-with-grn-csv';
                eb.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: {
                        poIds: poIds,
                        isAllPO: isAllPO,
                        productIds: productIds,
                        allProducts:allProducts,
                        fromDate: $('#fromDate').val(),
                        toDate:  $('#toDate').val(),
                    },
                    success: function(respond) {
                        if (respond.status == true) {
                            window.open(BASE_URL + '/' + respond.data, '_blank');
                        }
                    }
                });
            }
        }
    });

    function divLoading() {
        $('#purchaseOrderReport').hide();
    }

    $('.isAllPO').on('change', function() {
        isAllPO = $(this).val();
        if (isAllPO == true) {
            reSetReportContent('purchaseOrderReport');
            $('#poLists').val('').trigger('change');
            $('#poLists').prop('disabled', true);
            p_notification('info', eb.getMessage('INFO_CUS_REPORT_ALL_CUS_SELECTION'));
        } else {
            reSetReportContent('purchaseOrderReport');
            $('#poLists').prop('disabled', false);
        }
    });
    $('.isAllPdu').on('change', function(){
        if ($('#selectAllPrducts').is(":checked")){
            allProducts = true;
            reSetReportContent('purchaseOrderReport');
            $('#productLists').val('').trigger('change');
            $('#productLists').prop('disabled', true);
            p_notification('info', eb.getMessage('INFO_CUS_REPORT_ALL_CUS_SELECTION'));
        } else {
            allProducts = false;
            reSetReportContent('purchaseOrderReport');
            $('#productLists').prop('disabled', false);
        }
    });

    function validateInputSet()
    {
        if(poIds == null && !isAllPO){
            p_notification(false, eb.getMessage('ERR_PURCHREPO_PCODE'));
            return false;
        } else if($('#fromDate').val() == ''){
            p_notification(false, eb.getMessage('ERR_DATE_NOT_SET_PO_&_GRN'));
            return false;
        } else if($('#fromDate').val() > $('#toDate').val()){
            p_notification(false, eb.getMessage('ERR_DATE_RANGE_NOT_VALID_PO_&_GRN'));
            return false;
        } else{
            return true;
        }
    }
});



