/*
 * @author SANDUN <sandun@thinkcube.com>
 * Job card Reporting functions
 */
var projectId = '';
var jobId = '';
var activityId = '';
var activityCode = '';
var isAllSelect = 0;
var userId = '';
var customerId = '';
var dutyManagerId = '';
var tempProductId = '';
var fromDate = '';
var toDate = '';
var typeId = 'activityNo';
var vehicleIds = null;
var isAllVehicles = false;
var employeeTypes = null;
var employeeIds = null;
var isAllEmployees = false;

$(document).ready(function() {
    initialSetUp();
    var reportType = 'invoiceValue';
    var checkin = $('#fromDate').datepicker({}).on('changeDate', function(ev) {
        if (ev.date.valueOf() < checkout.date.valueOf()) {
            var newDate = new Date(ev.date)
            newDate.setDate(newDate.getDate() + 1);
            checkout.setValue(newDate);
        }
        checkin.hide();
        $('#toDate')[0].focus();
    }).data('datepicker');
    var checkout = $('#toDate').datepicker({
        onRender: function(date) {
            return date.valueOf() <= checkin.date.valueOf() ? 'disabled' : '';
        }
    }).on('changeDate', function(ev) {
        checkout.hide();
    }).data('datepicker');

    $("#invoiceValueReport").on("click", function() {
        reportType = 'invoiceValue';
        hideReportIFrame('jobCardReport');
        $(".invoice-value-selectpickers").removeClass('hidden');
        $(".types,.activity-list,.job-list,.project-list").removeClass('hidden');
        $(".temporary-product-list").addClass('hidden');
        $(".inquiry-log-selectpickers").addClass('hidden');
        $(".temp-product-list").addClass('hidden');
        $(".date-range").addClass('hidden');
        $("input[type='radio'][id='customSelect']").prop('checked', true);
        $("input[type='radio'][id='allSelect']").prop('checked', false);
        $('#job-and-activity-types').addClass('hidden');
        $('#locationSelection').addClass('hidden');
        $("#isAllSelectlbl").removeClass('hidden');
        $('.vehical-allocation-selectpickers').addClass('hidden');
        $(".employee-selectpickers").addClass('hidden');
        resetIsAllSelect();
    });

    $("#inquiryLogReport").on("click", function() {
        reportType = 'inquiryLog';
        hideReportIFrame('jobCardReport');
        $("#userList").val('').trigger('change');
        $("#customerList").val('').trigger('change');
        $("#dutyManagerList").val('').trigger('change');
        $("#fromDate").val('');
        $("#toDate").val('');
        $(".user-list").removeClass('hidden');
        $(".customer-list").removeClass('hidden');
        $(".duty-manager-list").removeClass('hidden');
        $(".temp-product-list").addClass('hidden');
        $(".invoice-value-selectpickers").addClass('hidden');
        $(".inquiry-log-selectpickers").removeClass('hidden');
        $(".user-list").removeClass('hidden');
        $(".duty-manager-list").removeClass('hidden');
        $(".date-range").removeClass('hidden');
        $('#job-and-activity-types').addClass('hidden');
        $('#locationSelection').addClass('hidden');
        $("#isAllSelectlbl").removeClass('hidden');
        $('.vehical-allocation-selectpickers').addClass('hidden');
        $(".employee-selectpickers").addClass('hidden');
        resetIsAllSelect();
    });

    $("#weightageReport").on('click', function() {
        reportType = 'weightageReport';
        hideReportIFrame('jobCardReport');
        $(".types,.job-card-list").removeClass('hidden');
        $(".invoice-value-selectpickers").removeClass('hidden');
        $(".inquiry-log-selectpickers").addClass('hidden');
        $(".date-range").removeClass('hidden');
        $(".temp-product-list").addClass('hidden');
        $("input[type='radio'][id='customSelect']").prop('checked', true);
        $("input[type='radio'][id='allSelect']").prop('checked', false);
        $('#locationSelection').addClass('hidden');
        $("#isAllSelectlbl").removeClass('hidden');
        $('.vehical-allocation-selectpickers').addClass('hidden');
        $(".employee-selectpickers").addClass('hidden');
        resetIsAllSelect();
    });

    $("#progressReport").on('click', function() {
        reportType = 'progressReport';
        hideReportIFrame('jobCardReport');
        $("input[type='radio'][id='customSelect']").prop('checked', true);
        $("input[type='radio'][id='allSelect']").prop('checked', false);
        $(".types,.job-card-list").removeClass('hidden');
        $(".invoice-value-selectpickers").removeClass('hidden');
        $(".inquiry-log-selectpickers").addClass('hidden');
        $(".temp-product-list").addClass('hidden');
        $(".date-range").addClass('hidden');
        $('#locationSelection').addClass('hidden');
        $("#isAllSelectlbl").removeClass('hidden');
        $('.vehical-allocation-selectpickers').addClass('hidden');
        $(".employee-selectpickers").addClass('hidden');
        resetIsAllSelect();
    });

    $('#selectType').on('change', function(e) {
        e.preventDefault();
        typeId = $(this).children(":selected").attr("id");
        activityId = '';
        jobId = '';
        projectId = '';
        resetSelectPicker();
        if (isAllSelect) {
            $('#customSelect').trigger('click');
        }
        if (typeId == 'activityNo') {
            $('#activityList').val('').trigger('change');
            $('#activityList').selectpicker('show');
            $('#activityList').selectpicker('render');
            $('#jobList').selectpicker('hide');
            $('#projectList').selectpicker('hide');
        } else if (typeId == 'jobNo') {
            $('#jobList').val('').trigger('change');
            $('#jobList').selectpicker('show');
            $('#jobList').selectpicker('render');
            $('#activityList').selectpicker('hide');
            $('#projectList').selectpicker('hide');
        } else if (typeId == 'projectNo') {
            $('#projectList').val('').trigger('change');
            $('#projectList').selectpicker('show');
            $('#projectList').selectpicker('render');
            $('#activityList').selectpicker('hide');
            $('#jobList').selectpicker('hide');
        }
    });

    $("#costingReport").on("click", function() {
        reportType = 'costingReport';
        hideReportIFrame('jobCardReport');
        $("input[type='radio'][id='customSelect']").prop('checked', true);
        $("input[type='radio'][id='allSelect']").prop('checked', false);
        $(".types,.job-card-list").removeClass('hidden');
        $(".invoice-value-selectpickers").removeClass('hidden');
        $(".inquiry-log-selectpickers").addClass('hidden');
        $(".date-range").addClass('hidden');
        $(".temp-product-list").addClass('hidden');
        $('#job-and-activity-types').addClass('hidden');
        $('#locationSelection').addClass('hidden');
        $("#isAllSelectlbl").removeClass('hidden');
        $('.vehical-allocation-selectpickers').addClass('hidden');
        $(".employee-selectpickers").addClass('hidden');
        resetIsAllSelect();
    });

    $("#openDetailCostingReport").on("click", function() {
        reportType = 'openDetailCostingReport';
        hideReportIFrame('jobCardReport');
        $(".types,.job-card-list").removeClass('hidden');
        $(".invoice-value-selectpickers").removeClass('hidden');
        $(".inquiry-log-selectpickers").addClass('hidden');
        $(".date-range").addClass('hidden');
        $(".temp-product-list").addClass('hidden');
        $("input[type='radio'][id='customSelect']").prop('checked', true);
        $("input[type='radio'][id='allSelect']").prop('checked', false);
        resetIsAllSelect();
    });

    $("#detailCostingReport").on("click", function() {
        reportType = 'detailCostingReport';
        hideReportIFrame('jobCardReport');
        $(".types,.job-card-list").removeClass('hidden');
        $(".invoice-value-selectpickers").removeClass('hidden');
        $(".inquiry-log-selectpickers").addClass('hidden');
        $(".date-range").addClass('hidden');
        $(".temp-product-list").addClass('hidden');
        $('.vehical-allocation-selectpickers').addClass('hidden');
        $("input[type='radio'][id='customSelect']").prop('checked', true);
        $("input[type='radio'][id='allSelect']").prop('checked', false);
        $('#job-and-activity-types').addClass('hidden');
        $('#locationSelection').addClass('hidden');
        $("#isAllSelectlbl").removeClass('hidden');
        $(".employee-selectpickers").addClass('hidden');
        resetIsAllSelect();
    });

    $("#tempProductReport").on("click", function() {
        reportType = 'tempProductReport';
        hideReportIFrame('jobCardReport');
        $(".types").addClass('hidden');
        $(".activity-list,.job-list,.project-list").addClass('hidden');
        $(".customer-list").addClass('hidden');
        $(".duty-manager-list").addClass('hidden');
        $(".temporary-product-list").removeClass('hidden');
        $("#fromDate").val('');
        $("#toDate").val('');
        $(".invoice-value-selectpickers").removeClass('hidden');
        $(".inquiry-log-selectpickers").addClass('hidden');
        $(".date-range").addClass('hidden');
        $('.vehical-allocation-selectpickers').addClass('hidden');
        $("input[type='radio'][id='customSelect']").prop('checked', true);
        $("input[type='radio'][id='allSelect']").prop('checked', false);
        $('#job-and-activity-types').addClass('hidden');
        $("#isAllSelectlbl").removeClass('hidden');
        $('#locationSelection').addClass('hidden');
        $(".employee-selectpickers").addClass('hidden');
        resetIsAllSelect();
    });

    $("#activityWiseTempProductReport").on("click", function() {
        reportType = 'activityWiseTempProductReport';
        hideReportIFrame('jobCardReport');
        $(".job-list,.project-list").addClass('hidden');
        $(".activity-list").removeClass('hidden');
        $(".types,.job-card-list .temporary-product-list").addClass('hidden');
        $(".customer-list").addClass('hidden');
        $(".duty-manager-list").addClass('hidden');
        $("#fromDate").val('');
        $("#toDate").val('');
        $(".invoice-value-selectpickers").removeClass('hidden');
        $(".inquiry-log-selectpickers").addClass('hidden');
        $(".date-range").addClass('hidden');
        $('.vehical-allocation-selectpickers').addClass('hidden');
        $("input[type='radio'][id='customSelect']").prop('checked', true);
        $("input[type='radio'][id='allSelect']").prop('checked', false);
        $('#job-and-activity-types').addClass('hidden');
        $('#locationSelection').addClass('hidden');
        $("#isAllSelectlbl").removeClass('hidden');
        $(".employee-selectpickers").addClass('hidden');
        resetIsAllSelect();
    });

    $("#quotationReport").on('click', function() {
        reportType = 'quotationReport';
        hideReportIFrame('jobCardReport');
        $(".types,.job-card-list").removeClass('hidden');
        $(".invoice-value-selectpickers").removeClass('hidden');
        $(".inquiry-log-selectpickers").addClass('hidden');
        $(".date-range").removeClass('hidden');
        $(".temp-product-list").addClass('hidden');
        $('.vehical-allocation-selectpickers').addClass('hidden');
        $("input[type='radio'][id='customSelect']").prop('checked', true);
        $("input[type='radio'][id='allSelect']").prop('checked', false);
        $('#job-and-activity-types').addClass('hidden');
        $('#locationSelection').addClass('hidden');
        $("#isAllSelectlbl").removeClass('hidden');
        resetIsAllSelect();
    });

    $("#repeatReport").on('click', function() {
        reportType = 'repeatReport';
        hideReportIFrame('jobCardReport');
        $(".types,.job-card-list").addClass('hidden');
        $(".invoice-value-selectpickers").removeClass('hidden');
        $(".inquiry-log-selectpickers").addClass('hidden');
        $("#isAllSelectlbl").addClass('hidden');
        $(".date-range").removeClass('hidden');
        $(".temp-product-list").addClass('hidden');
        $("input[type='radio'][id='customSelect']").prop('checked', true);
        $("input[type='radio'][id='allSelect']").prop('checked', false);
        $('#job-and-activity-types').removeClass('hidden');
        $('#locationSelection').removeClass('hidden');
        $(".employee-selectpickers").addClass('hidden');
        $('.vehical-allocation-selectpickers').addClass('hidden');
        resetIsAllSelect();
    });

    $("#vehicleAllocationReport").on('click', function() {
        reportType = 'vehicleAllocationReport';
        hideReportIFrame('jobCardReport');
        $(".types").addClass('hidden');
        $(".activity-list,.job-list,.project-list").addClass('hidden');
        $(".customer-list").addClass('hidden');
        $(".duty-manager-list").addClass('hidden');
        $(".temporary-product-list").addClass('hidden');
        $("#fromDate").val('');
        $("#toDate").val('');
        $(".invoice-value-selectpickers").addClass('hidden');
        $(".inquiry-log-selectpickers").addClass('hidden');
        $(".date-range").removeClass('hidden');
        $('.vehical-allocation-selectpickers').removeClass('hidden');
        $("input[type='radio'][id='customSelect']").prop('checked', true);
        $("input[type='radio'][id='allSelect']").prop('checked', false);
        $(".employee-selectpickers").addClass('hidden');
        resetIsAllSelect();
    });

    $('#employeeWiseActivityReport').on('click', function() {
        reportType = 'employeeWiseActivityReport';
        hideReportIFrame('jobCardReport');
        $(".types").addClass('hidden');
        $(".activity-list,.job-list,.project-list").addClass('hidden');
        $(".customer-list").addClass('hidden');
        $(".duty-manager-list").addClass('hidden');
        $(".temporary-product-list").addClass('hidden');
        $("#fromDate").val('');
        $("#toDate").val('');
        $(".invoice-value-selectpickers").addClass('hidden');
        $(".inquiry-log-selectpickers").addClass('hidden');
        $('.vehical-allocation-selectpickers').addClass('hidden');
        $(".date-range").removeClass('hidden');
        $(".employee-selectpickers").removeClass('hidden');
        $("input[type='radio'][id='customSelect']").prop('checked', true);
        $("input[type='radio'][id='allSelect']").prop('checked', false);
        resetIsAllSelect();
    });

    $('.isAllVehicles').on('click', function() {
        isAllVehicles = $(this).val();
        if (isAllVehicles == true) {
            $('#vehicleList').val('').trigger('change');
            $('#vehicleList').prop('disabled', true);
        } else {
            $('#vehicleList').prop('disabled', false);
        }
    });

    $('.isAllEmployee').on('click', function() {
        isAllEmployees = $(this).val();
        if (isAllEmployees == true) {
            $('#employeeList').val('').trigger('change');
            $('#employeeList').prop('disabled', true);
        } else {
            $('#employeeList').prop('disabled', false);
        }
    });

    loadDropDownFromDatabase('/project-api/searchProjectsForDropdown', "", 0, '#projectList', "", true);
    loadDropDownFromDatabase('/job-api/searchJobsForDropdown', "", '', '#jobList', '', true);
    loadDropDownFromDatabase('/api/activity/searchActivitiesForDropdown', "", "", '#activityList', '', true);
    loadDropDownFromDatabase('/customerAPI/searchCustomersForDropdown', "", "withDeactivatedCustomers", '#customerList', "", true);
    loadDropDownFromDatabase('/employee_api/searchEmployeeForDropdown', "", "", '#dutyManagerList', '', true);
    loadDropDownFromDatabase('/api/temporary-product/getTemporaryProductsBySearchKey', "", "", '#tempProductList', '', true);
    loadDropDownFromDatabase('/vehicle-api/searchVehicleForDropdown', "", 0, '#vehicleList', "", true);
    loadDropDownFromDatabase('/employee_api/searchEmployeeForDropdown', "", 0, '#employeeList', "", true);

    $('#projectList,#jobList,#activityList').on('change', function() {
        var thisValue = $(this).val();
        if (thisValue === null) {
            reSetReportContent('jobCardReport');
        }
    });

    $("#viewReport").on('click', function() {
        setValues();
        if (reportType == 'invoiceValue') {
            if ((activityId || jobId || projectId) || isAllSelect == true) {
                var url = BASE_URL + '/reports/get/job-card/view-job-card-invoice-value';
                eb.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: {activityId: activityId, jobId: jobId, projectId: projectId, isAllSelect: isAllSelect, typeId: typeId},
                    success: function(respond) {
                        if (respond.status == true) {
                            setContentToReport('jobCardReport', respond);

                        }
                    }
                });
            } else {
                p_notification(false, eb.getMessage('ERR_STOCKREPO_SELEONEMORE'));
            }
        } else if (reportType == 'inquiryLog') {
            var url = BASE_URL + '/reports/get/job-card/view-inquiry-log';
            eb.ajax({
                type: 'POST',
                url: url,
                dataType: 'json',
                data: {userId: userId, customerId: customerId, dutyManagerId: dutyManagerId, fromDate: fromDate, toDate: toDate},
                success: function(respond) {
                    if (respond.status == true) {
                        setContentToReport('jobCardReport', respond);

                    }
                }
            });
        } else if (reportType == 'weightageReport') {
            if ((activityId || jobId || projectId) || isAllSelect == true) {
                var url = BASE_URL + '/reports/get/job-card/view-weightage';
                eb.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: {
                        activityId: activityId,
                        jobId: jobId,
                        projectId: projectId,
                        isAllSelect: isAllSelect,
                        typeId: typeId,
                        fromDate: fromDate,
                        toDate: toDate
                    },
                    success: function(respond) {
                        if (respond.status == true) {
                            setContentToReport('jobCardReport', respond);

                        }
                    }
                });
            } else {
                p_notification(false, eb.getMessage('ERR_STOCKREPO_SELEONEMORE'));
            }
        } else if (reportType == 'progressReport') {
            if ((typeId == 'activityNo') && customerId != null) {
                p_notification(false, 'There is no customers in activity.Please select another type.');
            } else if ((activityId || jobId || projectId || customerId) || isAllSelect == true) {
                var url = BASE_URL + '/reports/get/job-card/view-job-card-progress';
                eb.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: {
                        activityId: activityId,
                        jobId: jobId,
                        projectId: projectId,
                        customerId: customerId,
                        isAllSelect: isAllSelect,
                        typeId: typeId},
                    success: function(respond) {
                        if (respond.status == true) {
                            setContentToReport('jobCardReport', respond);

                        }
                    }
                });
            } else {
                p_notification(false, eb.getMessage('ERR_STOCKREPO_SELEONEMORE'));
            }
        } else if (reportType == 'costingReport') {
            if ((activityId || jobId || projectId) || isAllSelect == true) {
                var url = BASE_URL + '/reports/get/job-card/view-job-card-costing';
                eb.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: {activityId: activityId, jobId: jobId, projectId: projectId, isAllSelect: isAllSelect, typeId: typeId},
                    success: function(respond) {
                        if (respond.status == true) {
                            setContentToReport('jobCardReport', respond);

                        }
                    }
                });
            } else {
                p_notification(false, eb.getMessage('ERR_STOCKREPO_SELEONEMORE'));
            }
        } else if (reportType == 'detailCostingReport') {

            if ((activityId || jobId || projectId) || isAllSelect == true) {
                var url = BASE_URL + '/reports/get/job-card/view-job-card-detail-costing';
                eb.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: {activityId: activityId, jobId: jobId, projectId: projectId, isAllSelect: isAllSelect, typeId: typeId},
                    success: function(respond) {
                        if (respond.status == true) {
                            setContentToReport('jobCardReport', respond);

                        }
                    }
                });
            } else {
                p_notification(false, eb.getMessage('ERR_STOCKREPO_SELEONEMORE'));
            }

        } else if (reportType == 'tempProductReport') {

            if ((tempProductId) || isAllSelect == true) {
                var url = BASE_URL + '/reports/get/job-card/view-temporary-product';
                eb.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: {tempProductId: tempProductId, isAllSelect: isAllSelect},
                    success: function(respond) {
                        if (respond.status == true) {
                            setContentToReport('jobCardReport', respond);

                        }
                    }
                });
            } else {
                p_notification(false, eb.getMessage('ERR_STOCKREPO_SELEONEMORE'));
            }
        } else if (reportType == 'activityWiseTempProductReport') {

            if ((activityId) || isAllSelect == true) {
                var url = BASE_URL + '/reports/get/job-card/view-activity-wise-temporary-product';
                eb.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: {activityId: activityId, isAllSelect: isAllSelect},
                    success: function(respond) {
                        if (respond.status == true) {
                            setContentToReport('jobCardReport', respond);

                        }
                    }
                });
            } else {
                p_notification(false, eb.getMessage('ERR_STOCKREPO_SELEONEMORE'));
            }
        } else if (reportType == 'quotationReport') {
            if ((activityId || jobId || projectId) || isAllSelect == true) {
                var url = BASE_URL + '/reports/get/job-card/view-job-card-quotation';
                eb.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: {
                        activityId: activityId,
                        jobId: jobId,
                        projectId: projectId,
                        isAllSelect: isAllSelect,
                        typeId: typeId,
                        fromDate: fromDate,
                        toDate: toDate
                    },
                    success: function(respond) {
                        if (respond.status == true) {
                            setContentToReport('jobCardReport', respond);

                        }
                    }
                });
            } else {
                p_notification(false, eb.getMessage('ERR_STOCKREPO_SELEONEMORE'));
            }
        } else if (reportType == 'repeatReport') {
            var locations = $('#locations').val();
            if (locations == null) {
                p_notification(false, eb.getMessage('ERR_LOCATION_SELECT_EMPTY'));
                return false;
            }
            var url = BASE_URL + '/reports/get/job-card/viewRepeatReport';
            eb.ajax({
                type: 'POST',
                url: url,
                dataType: 'json',
                data: {
                    type: $('#repeatTypeSelection').val(),
                    locations: locations,
                    fromDate: fromDate,
                    toDate: toDate
                },
                success: function(respond) {
                    if (respond.status == true) {
                        setContentToReport('jobCardReport', respond);

                    }
                }
            });
        } else if (reportType == 'vehicleAllocationReport') {
            var input = {};
            input.isAllVehicles = isAllVehicles;
            input.vehicleIds = vehicleIds;
            input.toDate = toDate;
            input.fromDate = fromDate;

            if (validteVehicalAlloction(input)) {
                var url = BASE_URL + '/reports/get/job-card/vehicle-allocation-report';
                eb.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: input,
                    success: function(respond) {
                        if (respond.status == true) {
                            setContentToReport('jobCardReport', respond);

                        }
                    }
                });
            }
        } else if (reportType == 'employeeWiseActivityReport') {
            var input = {};
            input.isAllEmployees = isAllEmployees;
            input.employeeIds = employeeIds;
            input.employeeTypes = employeeTypes;
            input.toDate = toDate;
            input.fromDate = fromDate;

            if (validteEmployeeActivity(input)) {
                var url = BASE_URL + '/reports/get/job-card/employee-wise-activity-report';
                eb.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: input,
                    success: function(respond) {
                        if (respond.status == true) {
                            setContentToReport('jobCardReport', respond);

                        }
                    }
                });
            }
        } else if (reportType == 'openDetailCostingReport') {
            if ((activityId || jobId || projectId) || isAllSelect == true) {
                var url = BASE_URL + '/reports/get/job-card/view-open-job-card-detail-costing';
                eb.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: {
                        activityId: activityId,
                        jobId: jobId,
                        projectId: projectId,
                        isAllSelect: isAllSelect,
                        typeId: typeId,
                        fromDate: fromDate,
                        toDate: toDate
                    },
                    success: function(respond) {
                        if (respond.status == true) {
                            setContentToReport('jobCardReport', respond);
                        }
                    }
                });
            }
        }
    });

    $('#generatePdf').on('click', function() {
        setValues();
        if (reportType == 'invoiceValue') {
            if ((activityId || jobId || projectId) || isAllSelect == true) {
                var url = BASE_URL + '/reports/get/job-card/generate-job-card-invoice-value-pdf';
                eb.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: {activityId: activityId, jobId: jobId, projectId: projectId, isAllSelect: isAllSelect, typeId: typeId},
                    success: function(respond) {
                        if (respond.status == true) {
                            window.open(BASE_URL + '/' + respond.data, '_blank');
                        }
                    }
                });
            } else {
                p_notification(false, eb.getMessage('ERR_STOCKREPO_SELEONEMORE'));
            }
        } else if (reportType == 'inquiryLog') {
            var url = BASE_URL + '/reports/get/job-card/generate-inquiry-log-pdf';
            eb.ajax({
                type: 'POST',
                url: url,
                dataType: 'json',
                data: {userId: userId, customerId: customerId, dutyManagerId: dutyManagerId, fromDate: fromDate, toDate: toDate},
                success: function(respond) {
                    if (respond.status == true) {
                        window.open(BASE_URL + '/' + respond.data);
                    }
                }
            });
        } else if (reportType == 'weightageReport') {
            if ((activityId || jobId || projectId) || isAllSelect == true) {
                var url = BASE_URL + '/reports/get/job-card/generate-weightage-pdf';
                eb.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: {
                        activityId: activityId,
                        jobId: jobId,
                        projectId: projectId,
                        isAllSelect: isAllSelect,
                        typeId: typeId,
                        fromDate: fromDate,
                        toDate: toDate
                    },
                    success: function(respond) {
                        if (respond.status == true) {
                            window.open(BASE_URL + '/' + respond.data, '_blank');
                        }
                    }
                });
            } else {
                p_notification(false, eb.getMessage('ERR_STOCKREPO_SELEONEMORE'));
            }
        } else if (reportType == 'progressReport') {
            if ((typeId == 'activityNo') && customerId != null) {
                p_notification(false, 'There is no customers in activity.Please select another type.');
            } else if ((activityId || jobId || projectId || customerId) || isAllSelect == true) {
                var url = BASE_URL + '/reports/get/job-card/generate-job-card-progress-pdf';
                eb.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: {
                        activityId: activityId,
                        jobId: jobId,
                        projectId: projectId,
                        customerId: customerId,
                        isAllSelect: isAllSelect,
                        typeId: typeId},
                    success: function(respond) {
                        if (respond.status == true) {
                            window.open(BASE_URL + '/' + respond.data, '_blank');
                        }
                    }
                });
            } else {
                p_notification(false, eb.getMessage('ERR_STOCKREPO_SELEONEMORE'));
            }
        } else if (reportType == 'costingReport') {
            if ((activityId || jobId || projectId) || isAllSelect == true) {
                var url = BASE_URL + '/reports/get/job-card/generate-job-card-costing-pdf';
                eb.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: {activityId: activityId, jobId: jobId, projectId: projectId, isAllSelect: isAllSelect, typeId: typeId},
                    success: function(respond) {
                        if (respond.status == true) {
                            window.open(BASE_URL + '/' + respond.data, '_blank');
                        }
                    }
                });
            } else {
                p_notification(false, eb.getMessage('ERR_STOCKREPO_SELEONEMORE'));
            }
        } else if (reportType == 'detailCostingReport') {
            if ((activityId || jobId || projectId) || isAllSelect == true) {
                var url = BASE_URL + '/reports/get/job-card/generate-job-card-detail-costing-pdf';
                eb.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: {activityId: activityId, jobId: jobId, projectId: projectId, isAllSelect: isAllSelect, typeId: typeId},
                    success: function(respond) {
                        if (respond.status == true) {
                            window.open(BASE_URL + '/' + respond.data, '_blank');
                        }
                    }
                });
            } else {
                p_notification(false, eb.getMessage('ERR_STOCKREPO_SELEONEMORE'));
            }
        } else if (reportType == 'tempProductReport') {
            if ((tempProductId) || isAllSelect == true) {
                var url = BASE_URL + '/reports/get/job-card/generate-temporary-product-pdf';
                eb.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: {tempProductId: tempProductId, isAllSelect: isAllSelect},
                    success: function(respond) {

                        if (respond.status == true) {
                            window.open(BASE_URL + '/' + respond.data, '_blank');
                        }
                    }
                });
            } else {
                p_notification(false, eb.getMessage('ERR_STOCKREPO_SELEONEMORE'));
            }
        } else if (reportType == 'activityWiseTempProductReport') {

            if ((activityId) || isAllSelect == true) {
                var url = BASE_URL + '/reports/get/job-card/generate-activity-wise-temporary-product-pdf';
                eb.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: {activityId: activityId, isAllSelect: isAllSelect},
                    success: function(respond) {

                        if (respond.status == true) {
                            window.open(BASE_URL + '/' + respond.data, '_blank');
                        }
                    }
                });
            } else {
                p_notification(false, eb.getMessage('ERR_STOCKREPO_SELEONEMORE'));
            }
        } else if (reportType == 'quotationReport') {
            if ((activityId || jobId || projectId) || isAllSelect == true) {
                var url = BASE_URL + '/reports/get/job-card/generate-job-card-quotation-pdf';
                eb.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: {
                        activityId: activityId,
                        jobId: jobId,
                        projectId: projectId,
                        isAllSelect: isAllSelect,
                        typeId: typeId,
                        fromDate: fromDate,
                        toDate: toDate
                    },
                    success: function(respond) {
                        if (respond.status == true) {
                            window.open(BASE_URL + '/' + respond.data, '_blank');
                        }
                    }
                });
            } else {
                p_notification(false, eb.getMessage('ERR_STOCKREPO_SELEONEMORE'));
            }
        } else if (reportType == 'repeatReport') {
            var locations = $('#locations').val();
            if (locations == null) {
                p_notification(false, eb.getMessage('ERR_LOCATION_SELECT_EMPTY'));
                return false;
            }
            var url = BASE_URL + '/reports/get/job-card/generate-repeat-pdf';
            eb.ajax({
                type: 'POST',
                url: url,
                dataType: 'json',
                data: {
                    type: $('#repeatTypeSelection').val(),
                    locations: locations,
                    fromDate: fromDate,
                    toDate: toDate
                },
                success: function(respond) {
                    if (respond.status == true) {
                        window.open(BASE_URL + '/' + respond.data, '_blank');
                    }
                }
            });
        } else if (reportType == 'vehicleAllocationReport') {
            var input = {};
            input.isAllVehicles = isAllVehicles;
            input.vehicleIds = vehicleIds;
            input.toDate = toDate;
            input.fromDate = fromDate;

            if (validteVehicalAlloction(input)) {
                var url = BASE_URL + '/reports/get/job-card/vehicle-allocation-pdf';
                eb.ajax({
                    type: 'POST',
                    url: url,
                    data: input,
                    success: function(respond) {
                        if (respond.status == true) {
                            window.open(BASE_URL + '/' + respond.data, '_blank');
                        }
                    }
                });
            }
        } else if (reportType == 'employeeWiseActivityReport') {
            var input = {};
            input.isAllEmployees = isAllEmployees;
            input.employeeIds = employeeIds;
            input.employeeTypes = employeeTypes;
            input.toDate = toDate;
            input.fromDate = fromDate;

            if (validteEmployeeActivity(input)) {
                var url = BASE_URL + '/reports/get/job-card/employee-wise-activity-pdf';
                eb.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: input,
                    success: function(respond) {
                        if (respond.status == true) {
                            window.open(BASE_URL + '/' + respond.data, '_blank');
                        }
                    }
                });
            }
        } else if (reportType == 'openDetailCostingReport') {
            if ((activityId || jobId || projectId) || isAllSelect == true) {
                var url = BASE_URL + '/reports/get/job-card/generate-open-job-card-detail-costing-pdf';
                eb.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: {
                        activityId: activityId,
                        jobId: jobId,
                        projectId: projectId,
                        isAllSelect: isAllSelect,
                        typeId: typeId,
                        fromDate: fromDate,
                        toDate: toDate
                    },
                    success: function(respond) {
                        if (respond.status == true) {
                            window.open(BASE_URL + '/' + respond.data, '_blank');
                        }
                    }
                });
            }
        }
    });

    $('#csvReport').on('click', function() {
        setValues();
        if (reportType == 'invoiceValue') {
            if ((activityId || jobId || projectId) || isAllSelect == true) {
                var url = BASE_URL + '/reports/get/job-card/generate-job-card-invoice-value-sheet';
                eb.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: {activityId: activityId, jobId: jobId, projectId: projectId, isAllSelect: isAllSelect, typeId: typeId},
                    success: function(respond) {
                        if (respond.status == true) {
                            window.open(BASE_URL + '/' + respond.data, '_blank');
                        }
                    }
                });
            } else {
                p_notification(false, eb.getMessage('ERR_STOCKREPO_SELEONEMORE'));
            }
        } else if (reportType == 'inquiryLog') {
            var url = BASE_URL + '/reports/get/job-card/generate-inquiry-log-sheet';
            eb.ajax({
                type: 'POST',
                url: url,
                dataType: 'json',
                data: {userId: userId, customerId: customerId, dutyManagerId: dutyManagerId, fromDate: fromDate, toDate: toDate},
                success: function(respond) {
                    if (respond.status == true) {
                        window.open(BASE_URL + '/' + respond.data, '_blank');
                    }
                }
            });
        } else if (reportType == 'weightageReport') {
            if ((activityId || jobId || projectId) || isAllSelect == true) {
                var url = BASE_URL + '/reports/get/job-card/generate-weightage-sheet';
                eb.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: {
                        activityId: activityId,
                        jobId: jobId,
                        projectId: projectId,
                        isAllSelect: isAllSelect,
                        typeId: typeId,
                        fromDate: fromDate,
                        toDate: toDate
                    },
                    success: function(respond) {
                        if (respond.status == true) {
                            window.open(BASE_URL + '/' + respond.data, '_blank');
                        }
                    }
                });
            } else {
                p_notification(false, eb.getMessage('ERR_STOCKREPO_SELEONEMORE'));
            }
        } else if (reportType == 'progressReport') {
            if ((typeId == 'activityNo') && customerId != null) {
                p_notification(false, 'There is no customers in activity.Please select another type.');
            } else if ((activityId || jobId || projectId || customerId) || isAllSelect == true) {
                var url = BASE_URL + '/reports/get/job-card/generate-job-card-progress-sheet';
                eb.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: {
                        activityId: activityId,
                        jobId: jobId,
                        projectId: projectId,
                        customerId: customerId,
                        isAllSelect: isAllSelect,
                        typeId: typeId},
                    success: function(respond) {
                        if (respond.status == true) {
                            window.open(BASE_URL + '/' + respond.data, '_blank');
                        }
                    }
                });
            } else {
                p_notification(false, eb.getMessage('ERR_STOCKREPO_SELEONEMORE'));
            }
        } else if (reportType == 'costingReport') {
            if ((activityId || jobId || projectId) || isAllSelect == true) {
                var url = BASE_URL + '/reports/get/job-card/generate-job-card-costing-sheet';
                eb.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: {activityId: activityId, jobId: jobId, projectId: projectId, isAllSelect: isAllSelect, typeId: typeId},
                    success: function(respond) {
                        if (respond.status == true) {
                            window.open(BASE_URL + '/' + respond.data, '_blank');
                        }
                    }
                });
            } else {
                p_notification(false, eb.getMessage('ERR_STOCKREPO_SELEONEMORE'));
            }
        } else if (reportType == 'detailCostingReport') {
            if ((activityId || jobId || projectId) || isAllSelect == true) {
                var url = BASE_URL + '/reports/get/job-card/generate-job-card-detail-costing-sheet';
                eb.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: {activityId: activityId, jobId: jobId, projectId: projectId, isAllSelect: isAllSelect, typeId: typeId},
                    success: function(respond) {
                        if (respond.status == true) {
                            window.open(BASE_URL + '/' + respond.data, '_blank');
                        }
                    }
                });
            } else {
                p_notification(false, eb.getMessage('ERR_STOCKREPO_SELEONEMORE'));
            }
        } else if (reportType == 'tempProductReport') {
            if ((tempProductId) || isAllSelect == true) {
                var url = BASE_URL + '/reports/get/job-card/generate-temporary-product-sheet';
                eb.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: {tempProductId: tempProductId, isAllSelect: isAllSelect},
                    success: function(respond) {
                        if (respond.status == true) {
                            window.open(BASE_URL + '/' + respond.data, '_blank');
                        }
                    }
                });
            } else {
                p_notification(false, eb.getMessage('ERR_STOCKREPO_SELEONEMORE'));
            }
        } else if (reportType == 'activityWiseTempProductReport') {

            if ((activityId) || isAllSelect == true) {
                var url = BASE_URL + '/reports/get/job-card/generate-activity-wise-temporary-product-sheet';
                eb.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: {activityId: activityId, isAllSelect: isAllSelect},
                    success: function(respond) {

                        if (respond.status == true) {
                            window.open(BASE_URL + '/' + respond.data, '_blank');
                        }
                    }
                });
            } else {
                p_notification(false, eb.getMessage('ERR_STOCKREPO_SELEONEMORE'));
            }
        } else if (reportType == 'quotationReport') {
            if ((activityId || jobId || projectId) || isAllSelect == true) {
                var url = BASE_URL + '/reports/get/job-card/generate-job-card-quotation-csv';
                eb.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: {
                        activityId: activityId,
                        jobId: jobId,
                        projectId: projectId,
                        isAllSelect: isAllSelect,
                        typeId: typeId,
                        fromDate: fromDate,
                        toDate: toDate
                    },
                    success: function(respond) {
                        if (respond.status == true) {
                            window.open(BASE_URL + '/' + respond.data, '_blank');
                        }
                    }
                });
            } else {
                p_notification(false, eb.getMessage('ERR_STOCKREPO_SELEONEMORE'));
            }
        } else if (reportType == 'repeatReport') {
            var locations = $('#locations').val();
            if (locations == null) {
                p_notification(false, eb.getMessage('ERR_LOCATION_SELECT_EMPTY'));
                return false;
            }

            var url = BASE_URL + '/reports/get/job-card/generate-repeat-csv';
            eb.ajax({
                type: 'POST',
                url: url,
                dataType: 'json',
                data: {
                    type: $('#repeatTypeSelection').val(),
                    locations: locations,
                    fromDate: fromDate,
                    toDate: toDate
                },
                success: function(respond) {
                    if (respond.status == true) {
                        window.open(BASE_URL + '/' + respond.data, '_blank');
                    }
                }
            });
        } else if (reportType == 'vehicleAllocationReport') {
            var input = {};
            input.isAllVehicles = isAllVehicles;
            input.vehicleIds = vehicleIds;
            input.toDate = toDate;
            input.fromDate = fromDate;

            if (validteVehicalAlloction(input)) {
                var url = BASE_URL + '/reports/get/job-card/vehicle-allocation-csv';
                eb.ajax({
                    type: 'POST',
                    url: url,
                    data: input,
                    success: function(respond) {
                        if (respond.status == true) {
                            window.open(BASE_URL + '/' + respond.data, '_blank');
                        }
                    }
                });
            }
        } else if (reportType == 'employeeWiseActivityReport') {
            var input = {};
            input.isAllEmployees = isAllEmployees;
            input.employeeIds = employeeIds;
            input.employeeTypes = employeeTypes;
            input.toDate = toDate;
            input.fromDate = fromDate;

            if (validteEmployeeActivity(input)) {
                var url = BASE_URL + '/reports/get/job-card/employee-wise-activity-csv';
                eb.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: input,
                    success: function(respond) {
                        if (respond.status == true) {
                            window.open(BASE_URL + '/' + respond.data, '_blank');
                        }
                    }
                });
            }
        } else if (reportType == 'openDetailCostingReport') {
            if ((activityId || jobId || projectId) || isAllSelect == true) {
                var url = BASE_URL + '/reports/get/job-card/generate-open-job-card-detail-costing-csv';
                eb.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: {
                        activityId: activityId,
                        jobId: jobId,
                        projectId: projectId,
                        isAllSelect: isAllSelect,
                        typeId: typeId,
                        fromDate: fromDate,
                        toDate: toDate
                    },
                    success: function(respond) {
                        if (respond.status == true) {
                            window.open(BASE_URL + '/' + respond.data, '_blank');
                        }
                    }
                });
            }
        }
    });

    $('.isAllSelect').on('click', function() {
        isAllSelect = $(this).val();
        if (isAllSelect == true) {
            hideJobCardReportIFrame();
            if (typeId == 'activityNo') {
                $('#activityList').val('').trigger('change');
                $('#activityList').prop('disabled', true);
            } else if (typeId == 'jobNo') {
                $('#jobList').val('').trigger('change');
                $('#jobList').prop('disabled', true);

            } else if (typeId == 'projectNo') {
                $('#projectList').val('').trigger('change');
                $('#projectList').prop('disabled', true);
            }
            p_notification('info', eb.getMessage('INFO_CUS_REPORT_ALL_CUS_SELECTION'));
        } else {
            hideJobCardReportIFrame();
            $('#activityList').prop('disabled', false);
            $('#jobList').prop('disabled', false);
            $('#projectList').prop('disabled', false);
        }
    });

    function resetIsAllSelect() {
        $('#activityList').prop('disabled', false);
        $('#jobList').prop('disabled', false);
        $('#projectList').prop('disabled', false);
    }
    function resetSelectPicker() {
        $("#activityList").val('');
        $("#jobList").val('');
        $("#projectList").val('');
    }

    function setValues() {
        activityId = $("#activityList").val();
        jobId = $("#jobList").val();
        projectId = $("#projectList").val();
        userId = $("#userList").val();
        customerId = $("#customerList").val();
        dutyManagerId = $("#dutyManagerList").val();
        tempProductId = $("#tempProductList").val();
        fromDate = $("#fromDate").val();
        toDate = $("#toDate").val();
        isAllVehicles = $('input[name="isAllVehicles"]:checked').val();
        vehicleIds = $('#vehicleList').val();
        employeeTypes = $('#employeeTypes').val();
        isAllEmployees = $('input[name="isAllEmployee"]:checked').val();
        employeeIds = $('#employeeList').val();
    }

    function hideJobCardReportIFrame() {
        $('#jobCardReport').hide();
        $('#jobCardReport').contents().find('body').html('');
        $('#jobCardReport').height(0);
        $('.selectpicker').selectpicker('deselectAll');
    }

    function initialSetUp() {
        $('#jobCardReport').hide();
        $("#jobList").selectpicker('hide');
        $("#projectList").selectpicker('hide');
        $(".inquiry-log-selectpickers").addClass('hidden');
        $(".date-range").addClass('hidden');
        $(".temporary-product-list").addClass('hidden');
    }

    function validteVehicalAlloction(input) {
        var fromDate = input.fromDate;
        var toDate = input.toDate;
        var isAllVehicles = input.isAllVehicles;
        var vehicleIds = input.vehicleIds;

        if (isAllVehicles != true && (vehicleIds == null || vehicleIds == "")) {
            p_notification(false, 'Please select at least one Vehicle.');
        }
        else if (fromDate == null || fromDate == "") {
            p_notification(false, 'Please select From Date.');
            document.getElementById("fromDate").focus();
        }
        else if (toDate == null || toDate == "") {
            p_notification(false, 'Please select To Date.');
            document.getElementById("toDate").focus();
        }
        else if (fromDate > toDate) {
            p_notification(false, 'Please select a valid date range.');
        }
        else {
            return true;
        }
    }

    function validteEmployeeActivity(input) {
        var fromDate = input.fromDate;
        var toDate = input.toDate;
        var isAllEmployees = input.isAllEmployees;
        var employeeIds = input.employeeIds;

        if (isAllEmployees != true && (employeeIds == null || employeeIds == "")) {
            p_notification(false, 'Please select at least one Employee.');
        }
        else if (fromDate == null || fromDate == "") {
            p_notification(false, 'Please select From Date.');
            document.getElementById("fromDate").focus();
        }
        else if (toDate == null || toDate == "") {
            p_notification(false, 'Please select To Date.');
            document.getElementById("toDate").focus();
        }
        else if (fromDate > toDate) {
            p_notification(false, 'Please select a valid date range.');
        }
        else {
            return true;
        }
    }
});


