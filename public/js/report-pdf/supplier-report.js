/*
 * @author SANDUN <sandun@thinkcube.com>
 * Supplier Reporting functions
 */
$(document).ready(function() {
    var reportType = 'supplierDetails';
    var supplierIds = $('#supplierLists').val();
    var isAllSuppliers;
    var lengthOfSuppliersList = $('#supplierLists').find('option').length;
    var pOWise = true;
    var pIWise = false;
    var grnWise = false;
    divLoading();
    setSelectPicker();

    var checkin = $('#fromDate').datepicker({
    }).on('changeDate', function(ev) {

        if (ev.date.valueOf() < checkout.date.valueOf()) {
            var newDate = new Date(ev.date);
            checkout.setValue(newDate);
        }
        checkin.hide();
        $('#toDate')[0].focus();
    }).data('datepicker');

    var checkout = $('#toDate').datepicker({
        onRender: function(date) {
            return date.valueOf() <= checkin.date.valueOf();
        }
    }).on('changeDate', function(ev) {
        checkout.hide();
    }).data('datepicker');

    loadDropDownFromDatabase('/supplierAPI/search-suppliers-for-dropdown', "", 0, '#supplierLists', "", true);
    $('#supplierLists').on('change', function() {
        var thisValue = $(this).val();
        if (thisValue == null) {
            reSetReportContent('supplierReport');
        }
    });
    $('#supplierDetails').on('click', function() {
        reportType = 'supplierDetails';
        supplierIds = $('#supplierLists').val();
        if (isAllSuppliers) {
            $('#customSupplierSelection').trigger('click');
        }
        $('#supplierLists').selectpicker('deselectAll');
        $('.report_from_date').addClass('hidden');
        $('.document-radio').addClass('hidden');
        $('.set_to_date').addClass('hidden');
        $('.advance_columns').addClass('hidden');
        $('.summary_columns').addClass('hidden');
        hideReportIFrame();
    });
    $('#supplierWiseItem').on('click', function() {
        reportType = 'supplierWiseItem';
        supplierIds = $('#supplierLists').val();
        if (isAllSuppliers) {
            $('#customSupplierSelection').trigger('click');
        }
        $('#supplierLists').selectpicker('deselectAll');
        $('.document-radio').addClass('hidden');
        $('.report_from_date').addClass('hidden');
        $('.set_to_date').addClass('hidden');
        $('.advance_columns').addClass('hidden');
        $('.summary_columns').addClass('hidden');
        hideReportIFrame();
    });

    $('#supplierTransaction').on('click', function() {
        reportType = 'supplierTransaction';
        supplierIds = $('#supplierLists').val();
        if (isAllSuppliers) {
            $('#customSupplierSelection').trigger('click');
        }
        $('#supplierLists').selectpicker('deselectAll');
        $('.report_from_date').removeClass('hidden');
        $('.document-radio').addClass('hidden');
        $('.advance_columns').addClass('hidden');
        $('.set_to_date').removeClass('hidden');
        $('.end_date_label').text('End Date').removeClass('hidden');
        $('.summary_columns').removeClass('hidden');
        hideReportIFrame();
    });
    
    $('#supplierBalance').on('click', function() {
        reportType = 'supplierBalance';
        supplierIds = $('#supplierLists').val();
        if (isAllSuppliers) {
            $('#customSupplierSelection').trigger('click');
        }
        $('#supplierLists').selectpicker('deselectAll');
        $('.report_from_date').removeClass('hidden');
        $('.document-radio').addClass('hidden');
        $('.advance_columns').addClass('hidden');
        $('.set_to_date').removeClass('hidden');
        $('.end_date_label').text('End Date').removeClass('hidden');
        $('.summary_columns').addClass('hidden');
        hideReportIFrame();
    });
    
    $('#agedSupplierAnalysis').on('click', function() {
        reportType = 'agedSupplierAnalysis';
        supplierIds = $('#supplierLists').val();
        if (isAllSuppliers) {
            $('#customSupplierSelection').trigger('click');
        }
        $('.document-radio').addClass('hidden');
        $('#supplierLists').selectpicker('deselectAll');
        $('.report_from_date').addClass('hidden');
        $('.set_to_date').removeClass('hidden');
        $('.end_date_label').text('Date').removeClass('hidden');
        $('.advance_columns').removeClass('hidden');
        $('.summary_columns').addClass('hidden');
        hideReportIFrame();
    });

    $('#supplierAndDocumentWiseItem').on('click', function() {
        reportType = 'supplierAndDocumentWiseItem';
        supplierIds = $('#supplierLists').val();
        if (isAllSuppliers) {
            $('#customSupplierSelection').trigger('click');
        }
        $('#supplierLists').selectpicker('deselectAll');
        $('.report_from_date').removeClass('hidden');
        $('.set_to_date').removeClass('hidden');
        $('.document-radio').removeClass('hidden');
        $('.end_date_label').text('To Date').removeClass('hidden');
        $('.advance_columns').addClass('hidden');
        $('.summary_columns').addClass('hidden');
        hideReportIFrame();
    });


    $('.pOWise').on('click', function() {
        pOWise = $(this).val();
        pIWise = false;
        grnWise = false;
        $("#pIWise").prop('checked', false);
        $("#grnWise").prop('checked', false);
    });

    $('.pIWise').on('click', function() {
        pIWise = $(this).val();
        pOWise = false;
        grnWise = false;
        $("#pOWise").prop('checked', false);
        $("#grnWise").prop('checked', false);
    });

    $('.grnWise').on('click', function() {
        grnWise = $(this).val();
        pOWise = false;
        pIWise = false;
        $("#pOWise").prop('checked', false);
        $("#pIWise").prop('checked', false);
    });

    $('#viewReport').on('click', function() {
        supplierIds = $('#supplierLists').val();
        if (reportType == 'agedSupplierAnalysis') {
            var selectedSuppliers = $('#supplierLists').val();
            var toDate = $('#toDate').val();
            var advancedColoumn = false;
            if($('#advRow').prop('checked') == true){
                advancedColoumn = true;
            } else {
                advancedColoumn = false;
            } 
            if(toDate == ''){
                 p_notification(false, eb.getMessage('ERR_DAISALE_FROMD'));
            } else if (selectedSuppliers || isAllSuppliers == true) {
                var url = BASE_URL + '/api/supplier-report/view-aged-supplier-data';
                eb.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: {
                        supplierIds: selectedSuppliers,
                        isAllSuppliers: isAllSuppliers,
                        toDate: toDate,
                        advancedColoumn: advancedColoumn
                    },
                    success: function(respond) {
                        if (respond.status == true) {
                            setContentToReport('supplierReport', respond);
                        }
                    }
                });
            } else if (selectedSuppliers && selectedSuppliers.length == 0) {
                p_notification(false, eb.getMessage('ERR_NORECORDS_IN_SELECTPICKER'));
            } else {
                p_notification(false, eb.getMessage('ERR_STOCKREPO_SELESUPPLIER'));
            }
        } else if (reportType == 'supplierDetails') {
            var selectedSuppliers = $('#supplierLists').val();
            if (isAllSuppliers == true || (selectedSuppliers && selectedSuppliers.length > 0)) {
                var url = BASE_URL + '/api/supplier-report/view-supplier-details';
                eb.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: {supplierIds: selectedSuppliers, isAllSuppliers: isAllSuppliers},
                    success: function(respond) {
                        if (respond.status == true) {
                            setContentToReport('supplierReport', respond);
                        }
                    }
                });
            } else if (selectedSuppliers && selectedSuppliers.length == 0) {
                p_notification(false, eb.getMessage('ERR_NORECORDS_IN_SELECTPICKER'));
            } else {
                p_notification(false, eb.getMessage('ERR_STOCKREPO_SELESUPPLIER'));
            }
        } else if (reportType == 'supplierWiseItem') {
            var selectedSuppliers = $('#supplierLists').val();
            if (selectedSuppliers || isAllSuppliers == true) {
                var url = BASE_URL + '/api/supplier-report/supplier-wise-item-details';
                eb.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: {supplierIds: selectedSuppliers, isAllSuppliers: isAllSuppliers},
                    success: function(respond) {
                        if (respond.status == true) {
                            setContentToReport('supplierReport', respond);
                        }
                    }

                });
            } else if (selectedSuppliers && selectedSuppliers.length == 0) {
                p_notification(false, eb.getMessage('ERR_NORECORDS_IN_SELECTPICKER'));
            } else {
                p_notification(false, eb.getMessage('ERR_STOCKREPO_SELESUPPLIER'));
            }
        } else if (reportType == 'supplierAndDocumentWiseItem') {
            var selectedSuppliers = $('#supplierLists').val();
            var fromDate = $('#fromDate').val();
            var toDate = $('#toDate').val();

            if (pIWise == true && !pOWise && !grnWise) {
                var docType = 'pi';
            } 
            if (pOWise == true && !pIWise && !grnWise) {
                var docType = 'po';
            }
            if (grnWise == true && !pIWise && !pOWise) {
                var docType = 'grn';
            }


            if(toDate != '' && fromDate == ''){
                 p_notification(false, eb.getMessage('ERR_DAISALE_FROMD'));
            } else if (selectedSuppliers || isAllSuppliers == true) {
                var url = BASE_URL + '/api/supplier-report/view-supplier-and-document-wise-item-details';
                eb.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: {supplierIds: selectedSuppliers, isAllSuppliers: isAllSuppliers, fromDate: fromDate, toDate:toDate, docType: docType},
                    success: function(respond) {
                        if (respond.status == true) {
                            setContentToReport('supplierReport', respond);
                        }
                    }

                });
            } else if (selectedSuppliers && selectedSuppliers.length == 0) {
                p_notification(false, eb.getMessage('ERR_NORECORDS_IN_SELECTPICKER'));
            } else {
                p_notification(false, eb.getMessage('ERR_STOCKREPO_SELESUPPLIER'));
            }
        } else if (reportType == 'supplierTransaction') {
            var selectedSuppliers = $('#supplierLists').val();
            var fromDate = $('#fromDate').val();
            var toDate = $('#toDate').val();
            var summaryData = false;
            if($('#summaryRow').prop('checked') == true){
                summaryData = true;
            } else {
                summaryData = false;
            }
            if(toDate != '' && fromDate == ''){
                 p_notification(false, eb.getMessage('ERR_DAISALE_FROMD'));
            } else if (selectedSuppliers || isAllSuppliers == true) {
                var url = BASE_URL + '/api/supplier-report/supplier-wise-transaction';
                eb.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: {
                        supplierIds: selectedSuppliers, 
                        isAllSuppliers: isAllSuppliers, 
                        fromDate: fromDate, 
                        toDate:toDate,
                        summaryData: summaryData
                    },
                    success: function(respond) {
                        if (respond.status == true) {
                            setContentToReport('supplierReport', respond);
                        }
                    }

                });
            } else if (selectedSuppliers && selectedSuppliers.length == 0) {
                p_notification(false, eb.getMessage('ERR_NORECORDS_IN_SELECTPICKER'));
            } else {
                p_notification(false, eb.getMessage('ERR_STOCKREPO_SELESUPPLIER'));
            }
        } else if (reportType == 'supplierBalance') {
            var selectedSuppliers = $('#supplierLists').val();
            var fromDate = $('#fromDate').val();
            var toDate = $('#toDate').val();
           
            if(toDate != '' && fromDate == ''){
                 p_notification(false, eb.getMessage('ERR_DAISALE_FROMD'));
            } else if(fromDate == "" || toDate == ""){
                p_notification(false, eb.getMessage('ERR_FROM_TO_DATE'));
            } else if((fromDate != null && toDate != null) && (toDate < fromDate)){
                p_notification(false, eb.getMessage('ERR_CUSREPO_FROMDATE_CANT_BE_MORETHAN_ENDDATE'));
            } else if (selectedSuppliers || isAllSuppliers == true) {
                var url = BASE_URL + '/api/supplier-report/viewSupplierBalance';
                eb.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: {
                        supplierIds: selectedSuppliers, 
                        isAllSuppliers: isAllSuppliers, 
                        fromDate: fromDate, 
                        toDate:toDate
                    },
                    success: function(respond) {
                        if (respond.status == true) {
                            setContentToReport('supplierReport', respond);
                        }
                    }

                });
            } else if (selectedSuppliers && selectedSuppliers.length == 0) {
                p_notification(false, eb.getMessage('ERR_NORECORDS_IN_SELECTPICKER'));
            } else {
                p_notification(false, eb.getMessage('ERR_STOCKREPO_SELESUPPLIER'));
            }
        }
    });

    $('#generatePdf').on('click', function() {
        supplierIds = $('#supplierLists').val();
         if (reportType == 'agedSupplierAnalysis') {
            var selectedSuppliers = $('#supplierLists').val();
            var toDate = $('#toDate').val();
            var advancedColoumn = false;
            if($('#advRow').prop('checked') == true){
                advancedColoumn = true;
            } else {
                advancedColoumn = false;
            } 
            if(toDate == ''){
                 p_notification(false, eb.getMessage('ERR_DAISALE_FROMD'));
            } else if (selectedSuppliers || isAllSuppliers == true) {
                var url = BASE_URL + '/api/supplier-report/generate-aged-supplier-data-pdf';
                eb.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: {
                        supplierIds: selectedSuppliers,
                        isAllSuppliers: isAllSuppliers,
                        toDate:toDate,
                        advancedColoumn: advancedColoumn
                        },
                    success: function(respond) {
                        if (respond.status == true) {
                            window.open(BASE_URL + '/' + respond.data, '_blank');
                        }
                    }
                });
            } else if (selectedSuppliers && selectedSuppliers.length == 0) {
                p_notification(false, eb.getMessage('ERR_NORECORDS_IN_SELECTPICKER'));
            } else {
                p_notification(false, eb.getMessage('ERR_STOCKREPO_SELESUPPLIER'));
            }
        } else if (reportType == 'supplierDetails') {
            var selectedSuppliers = $('#supplierLists').val();
            if (selectedSuppliers || isAllSuppliers == true) {
                var url = BASE_URL + '/api/supplier-report/generate-supplier-details-pdf';
                eb.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: {supplierIds: selectedSuppliers, isAllSuppliers: isAllSuppliers},
                    success: function(respond) {
                        if (respond.status == true) {
                            window.open(BASE_URL + '/' + respond.data, '_blank');
                        }
                    }
                });
            } else if (selectedSuppliers && selectedSuppliers.length == 0) {
                p_notification(false, eb.getMessage('ERR_NORECORDS_IN_SELECTPICKER'));
            } else {
                p_notification(false, eb.getMessage('ERR_STOCKREPO_SELESUPPLIER'));
            }
        } else if (reportType == 'supplierWiseItem') {
            var selectedSuppliers = $('#supplierLists').val();
            if (selectedSuppliers || isAllSuppliers == true) {
                var url = BASE_URL + '/api/supplier-report/generate-supplier-wise-item-pdf';
                eb.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: {supplierIds: selectedSuppliers, isAllSuppliers: isAllSuppliers},
                    success: function(respond) {
                        if (respond.status == true) {
                            window.open(BASE_URL + '/' + respond.data, '_blank');
                        }
                    }
                });
            } else if (selectedSuppliers && selectedSuppliers.length == 0) {
                p_notification(false, eb.getMessage('ERR_NORECORDS_IN_SELECTPICKER'));
            } else {
                p_notification(false, eb.getMessage('ERR_STOCKREPO_SELESUPPLIER'));
            }
        } else if (reportType == 'supplierAndDocumentWiseItem') {
            var selectedSuppliers = $('#supplierLists').val();
            var fromDate = $('#fromDate').val();
            var toDate = $('#toDate').val();

            if (pIWise == true && !pOWise && !grnWise) {
                var docType = 'pi';
            } 
            if (pOWise == true && !pIWise && !grnWise) {
                var docType = 'po';
            }
            if (grnWise == true && !pIWise && !pOWise) {
                var docType = 'grn';
            }

            if(toDate != '' && fromDate == ''){
                 p_notification(false, eb.getMessage('ERR_DAISALE_FROMD'));
            } else if (selectedSuppliers || isAllSuppliers == true) {
                var url = BASE_URL + '/api/supplier-report/generate-supplier-and-document-wise-item-pdf';
                eb.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: {supplierIds: selectedSuppliers, isAllSuppliers: isAllSuppliers, fromDate: fromDate, toDate:toDate, docType: docType},
                    success: function(respond) {
                        if (respond.status == true) {
                            window.open(BASE_URL + '/' + respond.data, '_blank');
                        }
                    }
                });
            } else if (selectedSuppliers && selectedSuppliers.length == 0) {
                p_notification(false, eb.getMessage('ERR_NORECORDS_IN_SELECTPICKER'));
            } else {
                p_notification(false, eb.getMessage('ERR_STOCKREPO_SELESUPPLIER'));
            }
        } else if (reportType == 'supplierTransaction') {
            var selectedSuppliers = $('#supplierLists').val();
            var fromDate = $('#fromDate').val();
            var toDate = $('#toDate').val();
            var summaryData = false;
            if($('#summaryRow').prop('checked') == true){
                summaryData = true;
            } else {
                summaryData = false;
            }
            if(toDate != '' && fromDate == ''){
                 p_notification(false, eb.getMessage('ERR_DAISALE_FROMD'));
            } else if (selectedSuppliers || isAllSuppliers == true) {
                var url = BASE_URL + '/api/supplier-report/generate-supplier-wise-transaction-pdf';
                eb.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: {
                        supplierIds: selectedSuppliers, 
                        isAllSuppliers: isAllSuppliers, 
                        fromDate: fromDate, 
                        toDate:toDate,
                        summaryData: summaryData
                    },
                    success: function(respond) {
                        if (respond.status == true) {
                            window.open(BASE_URL + '/' + respond.data, '_blank');
                        }
                    }
                });
            } else if (selectedSuppliers && selectedSuppliers.length == 0) {
                p_notification(false, eb.getMessage('ERR_NORECORDS_IN_SELECTPICKER'));
            } else {
                p_notification(false, eb.getMessage('ERR_STOCKREPO_SELESUPPLIER'));
            }
        } else if (reportType == 'supplierBalance') {
            var selectedSuppliers = $('#supplierLists').val();
            var fromDate = $('#fromDate').val();
            var toDate = $('#toDate').val();
            
            if(toDate != '' && fromDate == ''){
                 p_notification(false, eb.getMessage('ERR_DAISALE_FROMD'));
            } else if(fromDate == "" || toDate == ""){
                p_notification(false, eb.getMessage('ERR_FROM_TO_DATE'));
            } else if((fromDate != null && toDate != null) && (toDate < fromDate)){
                p_notification(false, eb.getMessage('ERR_CUSREPO_FROMDATE_CANT_BE_MORETHAN_ENDDATE'));
            } else if (selectedSuppliers || isAllSuppliers == true) {
                var url = BASE_URL + '/api/supplier-report/generate-supplier-balance-details-pdf';
                eb.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: {
                        supplierIds: selectedSuppliers, 
                        isAllSuppliers: isAllSuppliers, 
                        fromDate: fromDate, 
                        toDate:toDate
                    },
                    success: function(respond) {
                        if (respond.status == true) {
                            window.open(BASE_URL + '/' + respond.data, '_blank');
                        }
                    }
                });
            } else if (selectedSuppliers && selectedSuppliers.length == 0) {
                p_notification(false, eb.getMessage('ERR_NORECORDS_IN_SELECTPICKER'));
            } else {
                p_notification(false, eb.getMessage('ERR_STOCKREPO_SELESUPPLIER'));
            }
        }
    });
    $('#csvReport').on('click', function() {
        supplierIds = $('#supplierLists').val();
        if (reportType == 'agedSupplierAnalysis') {
            var selectedSuppliers = $('#supplierLists').val();
            var toDate = $('#toDate').val();
            var advancedColoumn = false;
            if($('#advRow').prop('checked') == true){
                advancedColoumn = true;
            } else {
                advancedColoumn = false;
            } 
            if(toDate == ''){
                 p_notification(false, eb.getMessage('ERR_DAISALE_FROMD'));
            } else if (selectedSuppliers || isAllSuppliers == true) {
                var url = BASE_URL + '/api/supplier-report/generate-aged-supplier-data-sheet';
                eb.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: {
                        supplierIds: selectedSuppliers,
                        isAllSuppliers: isAllSuppliers,
                        toDate:toDate,
                        advancedColoumn: advancedColoumn
                        },
                    success: function(respond) {
                        if (respond.status == true) {
                            window.open(BASE_URL + '/' + respond.data, '_blank');
                        }
                    }
                });
            } else if (selectedSuppliers && selectedSuppliers.length == 0) {
                p_notification(false, eb.getMessage('ERR_NORECORDS_IN_SELECTPICKER'));
            } else {
                p_notification(false, eb.getMessage('ERR_STOCKREPO_SELESUPPLIER'));
            }
        } else if (reportType == 'supplierDetails') {
            var selectedSuppliers = $('#supplierLists').val();
            if (selectedSuppliers || isAllSuppliers == true) {
                var url = BASE_URL + '/api/supplier-report/generate-supplier-details-sheet';
                eb.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: {supplierIds: supplierIds, isAllSuppliers: isAllSuppliers},
                    success: function(respond) {
                        if (respond.status == true) {
                            window.open(BASE_URL + '/' + respond.data, '_blank');
                        }
                    }
                });
            } else if (selectedSuppliers && selectedSuppliers.length == 0) {
                p_notification(false, eb.getMessage('ERR_NORECORDS_IN_SELECTPICKER'));
            } else {
                p_notification(false, eb.getMessage('ERR_STOCKREPO_SELESUPPLIER'));
            }
        } else if (reportType == 'supplierWiseItem') {
            var selectedSuppliers = $('#supplierLists').val();
            if (selectedSuppliers || isAllSuppliers == true) {
                var url = BASE_URL + '/api/supplier-report/generate-supplier-wise-item-sheet';
                eb.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: {supplierIds: selectedSuppliers, isAllSuppliers: isAllSuppliers},
                    success: function(respond) {
                        if (respond.status == true) {
                            window.open(BASE_URL + '/' + respond.data, '_blank');
                        }
                    }
                });
            } else if (selectedSuppliers.length == 0) {
                p_notification(false, eb.getMessage('ERR_NORECORDS_IN_SELECTPICKER'));
            } else {
                p_notification(false, eb.getMessage('ERR_STOCKREPO_SELESUPPLIER'));
            }
        } else if (reportType == 'supplierAndDocumentWiseItem') {
            var selectedSuppliers = $('#supplierLists').val();
            var fromDate = $('#fromDate').val();
            var toDate = $('#toDate').val();

            if (pIWise == true && !pOWise && !grnWise) {
                var docType = 'pi';
            } 
            if (pOWise == true && !pIWise && !grnWise) {
                var docType = 'po';
            }
            if (grnWise == true && !pIWise && !pOWise) {
                var docType = 'grn';
            }

            if (toDate != '' && fromDate == ''){
                 p_notification(false, eb.getMessage('ERR_DAISALE_FROMD'));
            } else if (selectedSuppliers || isAllSuppliers == true) {
                var url = BASE_URL + '/api/supplier-report/generate-supplier-and-document-wise-item-sheet';
                eb.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: {supplierIds: selectedSuppliers, isAllSuppliers: isAllSuppliers, fromDate: fromDate, toDate:toDate, docType: docType},
                    success: function(respond) {
                        if (respond.status == true) {
                            window.open(BASE_URL + '/' + respond.data, '_blank');
                        }
                    }
                });
            } else if (selectedSuppliers.length == 0) {
                p_notification(false, eb.getMessage('ERR_NORECORDS_IN_SELECTPICKER'));
            } else {
                p_notification(false, eb.getMessage('ERR_STOCKREPO_SELESUPPLIER'));
            }
        } else if (reportType == 'supplierTransaction') {
            var selectedSuppliers = $('#supplierLists').val();
            var fromDate = $('#fromDate').val();
            var toDate = $('#toDate').val();
            var summaryData = false;
            if($('#summaryRow').prop('checked') == true){
                summaryData = true;
            } else {
                summaryData = false;
            }
            if(toDate != '' && fromDate == ''){
                 p_notification(false, eb.getMessage('ERR_DAISALE_FROMD'));
            } else if (selectedSuppliers || isAllSuppliers == true) {
                var url = BASE_URL + '/api/supplier-report/generate-supplier-transaction-sheet';
                eb.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: {
                        supplierIds: selectedSuppliers, 
                        isAllSuppliers: isAllSuppliers, 
                        fromDate: fromDate, 
                        toDate:toDate,
                        summaryData: summaryData,
                    },
                    success: function(respond) {
                        if (respond.status == true) {
                            window.open(BASE_URL + '/' + respond.data, '_blank');
                        }
                    }
                });
            } else if (selectedSuppliers.length == 0) {
                p_notification(false, eb.getMessage('ERR_NORECORDS_IN_SELECTPICKER'));
            } else {
                p_notification(false, eb.getMessage('ERR_STOCKREPO_SELESUPPLIER'));
            }
        } else if (reportType == 'supplierBalance') {
            var selectedSuppliers = $('#supplierLists').val();
            var fromDate = $('#fromDate').val();
            var toDate = $('#toDate').val();
           
            if(toDate != '' && fromDate == ''){
                 p_notification(false, eb.getMessage('ERR_DAISALE_FROMD'));
            } else if(fromDate == "" || toDate == ""){
                p_notification(false, eb.getMessage('ERR_FROM_TO_DATE'));
            } else if((fromDate != null && toDate != null) && (toDate < fromDate)){
                p_notification(false, eb.getMessage('ERR_CUSREPO_FROMDATE_CANT_BE_MORETHAN_ENDDATE'));
            } else if (selectedSuppliers || isAllSuppliers == true) {
                var url = BASE_URL + '/api/supplier-report/generate-supplier-balance-details-sheet';
                eb.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: {
                        supplierIds: selectedSuppliers, 
                        isAllSuppliers: isAllSuppliers, 
                        fromDate: fromDate, 
                        toDate:toDate
                    },
                    success: function(respond) {
                        if (respond.status == true) {
                            window.open(BASE_URL + '/' + respond.data, '_blank');
                        }
                    }
                });
            } else if (selectedSuppliers.length == 0) {
                p_notification(false, eb.getMessage('ERR_NORECORDS_IN_SELECTPICKER'));
            } else {
                p_notification(false, eb.getMessage('ERR_STOCKREPO_SELESUPPLIER'));
            }
        }
    });

    function divLoading() {
        $('#supplierReport').hide();
    }

    function hideReportIFrame() {
        $('#supplierReport').hide();
        $('#supplierReport').contents().find('body').html('');
    }

    $('.isAllSuppliers').on('click', function() {
        isAllSuppliers = $(this).val();
        if (isAllSuppliers == true) {
            hideReportIFrame();
            $('#supplierLists').val('').trigger('change');
            $('#supplierLists').prop('disabled', true);
            p_notification('info', eb.getMessage('INFO_CUS_REPORT_ALL_CUS_SELECTION'));
        } else {
            hideReportIFrame();
            $('#supplierLists').prop('disabled', false);
        }
    });

});



