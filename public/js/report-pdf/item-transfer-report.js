/*
 * @author SANDUN <sandun@thinkcube.com>
 * Item Transfer Reporting functions
 */

var reportType = 'itemWiseTransfer';
var locationFromIds;
var locationToIds;
var dateFrom;
var dateTo;
var cusProducts;
var allProducts;

$(document).ready(function() {

    divLoading();
    setSelectPicker();//select picker setting
    loadDropDownFromDatabase('/productAPI/search-user-active-location-products-for-dropdown', "", 0, '#product', "", true);

    $('#itemTransfer').on('click', function() {
        reportType = 'itemTransfer';
        locationFromIds = $('#locationFrom').val();
        locationToIds = $('#locationTo').val();
        dateFrom = $('#fromDate').val();
        dateTo = $('#toDate').val();
        cusProducts = null;
        allProducts = null;
        $('.set-from-location').show();
        $('.set-to-location').show();
        $('.set-from-date').show();
        $('.set-to-date').show();
        $('.set-custom-products').hide();
        $('.set-all-products').hide();
    });

    $('#itemWiseTransfer').on('click', function() {
        reportType = 'itemWiseTransfer';
        locationFromIds = $('#locationFrom').val();
        locationToIds = $('#locationTo').val();
        dateFrom = $('#fromDate').val();
        dateTo = $('#toDate').val();
        cusProducts = $('#product').val();
        allProducts = $('input[name=isAllProducts]:checked').val();
        $('.set-from-location').show();
        $('.set-to-location').show();
        $('.set-from-date').show();
        $('.set-to-date').show();
        $('.set-custom-products').show();
        $('.set-all-products').show();
    });

    $('#locationFrom,#locationTo').on('change', function() {
        locationFromIds = $('#locationFrom').val();
        locationToIds = $('#locationTo').val();
        if (locationFromIds === null || locationToIds === null) {
            reSetReportContent('itemTransferReport');
        }
    });

    $('#fromDate,#toDate').datepicker().on('changeDate', function () {
        dateFrom = $('#fromDate').val();
        dateTo = $('#toDate').val();
        if (dateFrom === null || dateTo === null) {
            reSetReportContent('itemTransferReport');
        }
        $(this).datepicker('hide');
    });

    $('#product').on('change', function() {
        cusProducts = $(this).val();
        if (cusProducts === null) {
            reSetReportContent('itemTransferReport');
        }
    });

    $('input[name=isAllProducts]').on('change', function() {
        allProducts = $('input[name=isAllProducts]:checked').val();
        if (allProducts === null) {
            reSetReportContent('itemTransferReport');
        }
    });

    $('#viewReport').on('click', function() {
        locationFromIds = $('#locationFrom').val();
        locationToIds = $('#locationTo').val();
        dateFrom = $('#fromDate').val();
        dateTo = $('#toDate').val();
        cusProducts = $('#product').val();
        allProducts = $('input[name=isAllProducts]:checked').val();
        inputs = {'reportType' : reportType,
                    'locationFromIds' : locationFromIds,
                    'locationToIds' : locationToIds,
                    'dateFrom' : dateFrom,
                    'dateTo' : dateTo,
                    'cusProducts' : cusProducts,
                    'allProducts' : allProducts};
        if (inputsValidation(inputs)) {
            var url;
            if (reportType == 'itemTransfer') {
                url = BASE_URL + '/api/item-transfer-report/view';
            } else if (reportType == 'itemWiseTransfer') {
                url = BASE_URL + '/api/item-transfer-report/item-wise-view';
            }
            eb.ajax({
                type: 'POST',
                url: url,
                dataType: 'json',
                data: inputs,
                success: function(respond) {
                    if (respond.status == true) {
                        setContentToReport('itemTransferReport', respond);
                    }
                }
            });
        }
    });
    $('#generatePdf').on('click', function() {
        locationFromIds = $('#locationFrom').val();
        locationToIds = $('#locationTo').val();
        dateFrom = $('#fromDate').val();
        dateTo = $('#toDate').val();
        cusProducts = $('#product').val();
        allProducts = $('input[name=isAllProducts]:checked').val();
        inputs = {'reportType' : reportType,
                    'locationFromIds' : locationFromIds,
                    'locationToIds' : locationToIds,
                    'dateFrom' : dateFrom,
                    'dateTo' : dateTo,
                    'cusProducts' : cusProducts,
                    'allProducts' : allProducts};
        if (inputsValidation(inputs)) {
            var url;
            if (reportType == 'itemTransfer') {
                url = BASE_URL + '/api/item-transfer-report/generate-item-transfer-pdf';
            } else if (reportType == 'itemWiseTransfer') {
                url = BASE_URL + '/api/item-transfer-report/generate-item-wise-transfer-pdf';
            }
            eb.ajax({
                type: 'POST',
                url: url,
                dataType: 'json',
                data: inputs,
                success: function(respond) {
                    if (respond.status == true) {
                        openPdfOnNewTab(respond.data);
                    }
                }
            });
        }
    });

    $('#csvReport').on('click', function() {
        locationFromIds = $('#locationFrom').val();
        locationToIds = $('#locationTo').val();
        dateFrom = $('#fromDate').val();
        dateTo = $('#toDate').val();
        cusProducts = $('#product').val();
        allProducts = $('input[name=isAllProducts]:checked').val();
        inputs = {'reportType' : reportType,
                    'locationFromIds' : locationFromIds,
                    'locationToIds' : locationToIds,
                    'dateFrom' : dateFrom,
                    'dateTo' : dateTo,
                    'cusProducts' : cusProducts,
                    'allProducts' : allProducts};
        if (inputsValidation(inputs)) {
            var url;
            if (reportType == 'itemTransfer') {
                url = BASE_URL + '/api/item-transfer-report/generate-item-transfer-sheet';
            } else if (reportType == 'itemWiseTransfer') {
                url = BASE_URL + '/api/item-transfer-report/generate-item-wise-transfer-sheet';
            }
            eb.ajax({
                type: 'POST',
                url: url,
                dataType: 'json',
                data: inputs,
                success: function(respond) {
                    if (respond.status == true) {
                        window.open(BASE_URL + '/' + respond.data, '_blank');
                    }
                }
            });
        }
    });

    function divLoading() {
        $('#itemTransferReport').hide();
        $('.datepicker').datepicker();
        // $('.set-custom-products').hide();
        // $('.set-all-products').hide();
    }

    function inputsValidation(inputs) {
        var locFromIds = inputs['locationFromIds'];
        var locToIds = inputs['locationToIds'];
        var lengthOfAvailableFromLocs = $('#locationFrom').find('option').length;
        if (lengthOfAvailableFromLocs == 0) {
            p_notification(false, eb.getMessage('ERR_NORECORDS_IN_SELECTPICKER'));
            return false;
        } else if (locFromIds == null && locToIds == null) {
            p_notification(false, eb.getMessage('ERR_ITEMTRANS_SELEFROMTO'));
            return false;
        } else if (locFromIds == null && locToIds != null) {
            p_notification(false, eb.getMessage('ERR_ITEMTRANS_SELEFROMLOC'));
            return false;
        } else if (locFromIds != null && locToIds == null) {
            p_notification(false, eb.getMessage('ERR_ITEMTRANS_SELETOLOC'));
            return false;
        }

        var fromDate = inputs['dateFrom'];
        var toDate = inputs['dateTo'];
        if (fromDate == null && toDate != null) {
            p_notification(false, eb.getMessage('ERR_ITEMTRANS_FRMDATE'));
            return false;
        } else if (fromDate != null && toDate == null) {
            p_notification(false, eb.getMessage('ERR_ITEMTRANS_TODATE'));
            return false;
        } else if (fromDate > toDate) {
            p_notification(false, eb.getMessage('ERR_ITEMTRANS_DIFDATE'));
            return false;
        }

        var reportType = inputs['reportType'];
        var allProducts = inputs['allProducts'];
        var cusProducts = inputs['cusProducts'];
        if (reportType == 'itemWiseTransfer' && allProducts == 0 && cusProducts == null) {
            p_notification(false, eb.getMessage('ERR_ITEMTRANS_PRO'));
            return false;
        }

        return true;
    }

    function clearTransferFrame() {
        if (locationFromIds === null && locationToIds === null) {
            $("#itemTransferReport").contents().find("body").html('');
            $("#itemTransferReport").hide();
        }
    }
});



