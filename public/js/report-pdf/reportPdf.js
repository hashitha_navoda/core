/*
 * @author SANDUN <sandun@thinkcube.com>
 * Reporting functions
 */

$(document).ready(function () {
///////DatePicker for two text Filed\\\\\\\
    var nowTemp = new Date();
    var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);
    var checkin = $('#from_date').datepicker({
    }).on('changeDate', function (ev) {
        if (ev.date.valueOf() < checkout.date.valueOf()) {
            var newDate = new Date(ev.date)
            newDate.setDate(newDate.getDate() + 1);
            checkout.setValue(newDate);
        }
        checkin.hide();
        $('#to_date')[0].focus();
    }).data('datepicker');
    var checkout = $('#to_date').datepicker({
        onRender: function (date) {
            return date.valueOf() <= checkin.date.valueOf() ? 'disabled' : '';
        }
    }).on('changeDate', function (ev) {
        checkout.hide();
    }).data('datepicker');
    /////Pick date/////////////
    $('#date').datepicker();
    //////Pick year////////////
    $("#year").datepicker({
        format: " yyyy",
        viewMode: "years",
        minViewMode: 'years'
    });
    $("#month").datepicker({
        format: " mm",
        viewMode: "months",
        minViewMode: 'months'
    });
/////EndOFDatePickers\\\\\\\\\
    $('#view-div').hide();
    $('#graph_div').hide();
    $("#view_p_listing_report").click(function (e) {
        e.preventDefault();
        var e = document.getElementById("product");
        var product_value = e.options[e.selectedIndex].value;

        $('#product_report_body').html('');
        var getdataurl = BASE_URL + '/salesReportAPI/getProductDetails';

        if (product_value == 'empty') {
            p_notification(false, eb.getMessage('ERR_REPOPDF_VALIDCATG'));
        } else {

            var getdatarequest = eb.post(getdataurl, {
                product_value: product_value
            });
            getdatarequest.done(function (data) {
                var productData = data['product'];

                if (data == "nodata") {
                    p_notification(false, eb.getMessage('ERR_REPOPDF_SALESDURATION'));
                    $('#view-div').hide();
                } else {
                    $('#view-div').show();

                    for (var i in productData) {
                        var item = productData[i].productName;
                        var description = productData[i].description;
                        var UoM = productData[i].unitOfMeasure;
                        var price = parseFloat(Math.round(productData[i].price * 100) / 100).toFixed(2);
                        var tbl_row = "<tr><td align='left'>" + item + "</td>"
                                + "<td align='left'>" + description + "</td>"
                                + "<td align='right'>" + UoM + "</td>"
                                + "<td align='right'>" + accounting.formatMoney(price) + "</td>"
                        tbl_row += "</tr>";
                        $("#product_report_body").append(tbl_row);
                        i++;
                    }
                }

            });
        }
    });
    $("#excel_p_listing_report").click(function (e) {
        e.preventDefault();
        var e = document.getElementById("product");
        var product_value = e.options[e.selectedIndex].value;
        if (product_value == 'empty') {
            p_notification(false, eb.getMessage('ERR_REPOPDF_VALIDCATG'));
        } else {
            window.open(BASE_URL + "/salesReportAPI/productListingExcelSheet?product_value=" + product_value, '_blank');
        }
    });
    $("#generate_p_listing_pdf").click(function (e) {
        e.preventDefault();
        var e = document.getElementById("product");
        var product_value = e.options[e.selectedIndex].value;
        if (product_value == 'empty') {
            p_notification(false, eb.getMessage('ERR_REPOPDF_VALIDCATG'));
        } else {
            window.open(BASE_URL + "/salesReport/generatePriceListingPdf?product_value=" + product_value, '_blank');
        }
    });
    $("#view_summery_report").click(function (e) {
        e.preventDefault();
        $('#summery_report_body').html('');
        var getdataurl = BASE_URL + '/salesReportAPI/getSalesSummeryDetails';
        inputs = new Array(
                $('#from_date').val(),
                $('#to_date').val()
                );
        if (validteinput(inputs)) {
            $('#view-div').show();
            var getdatarequest = eb.post(getdataurl, {
                from_date: $('#from_date').val(),
                to_date: $('#to_date').val()
            });
            getdatarequest.done(function (salessummerydata) {

                if (salessummerydata == "nodata") {
                    p_notification(false, eb.getMessage('ERR_REPOPDF_SALESDURATION'));
                    $('#view-div').hide();
                } else {

                    for (var i in salessummerydata.summery) {

                        var cusName = salessummerydata.summery[i].customerName;
                        var customer_splitname = cusName.split(" -");
                        var total_tax = parseFloat(salessummerydata.summery[i].TotalTax);
                        total_tax = total_tax ? total_tax : 0.00;
                        var tax_val = parseFloat(Math.round(total_tax * 100) / 100).toFixed(2);
                        var total_amount = parseFloat(salessummerydata.summery[i].TotalAmount);
                        total_amount = total_amount ? total_amount : 0.00;
                        var amount_val = parseFloat(Math.round(total_amount * 100) / 100).toFixed(2);
                        var ex_tax = parseFloat(amount_val) - parseFloat(tax_val);
                        ex_tax = ex_tax ? ex_tax : 0.00;
                        var ex_val = parseFloat(Math.round(ex_tax * 100) / 100).toFixed(2);
                        var tbl_row = "<tr><td>" + i + "</td>"
                                + "<td>" + customer_splitname[0] + "</td>"
                                + "<td align='right'>" + accounting.formatMoney(ex_val) + "</td>"
                                + "<td align='right'>" + accounting.formatMoney(tax_val) + "</td>"
                        tbl_row += "</tr>";
                        $("#summery_report_body").append(tbl_row);
                        i++;
                    }
                }
            });
        }
    });
    $("#excel_summery_report").click(function (e) {
        e.preventDefault();
        inputs = new Array(
                $('#from_date').val(),
                $('#to_date').val()
                );
        if (validteinput(inputs)) {
            var from_date = $("#from_date").val();
            var to_date = $("#to_date").val();
            window.open(BASE_URL + "/salesReportAPI/salesSummeryExcelSheet?from_date=" + from_date + "&to_date=" + to_date, '_blank');
        }
    });


    $('#view_monthly_report').click(function (e) {
        e.preventDefault();
        var check_tax;
        if (tax.checked == false) {

            check_tax = false;
        } else {

            check_tax = true;
        }


        $("#monthly_report_body").html('');
        var getpdfdataurl = BASE_URL + '/salesReportAPI/getMonthlySalesDetails';
        inputs = new Array(
                $('#from_date').val(),
                $('#to_date').val()
                );
        if (validteinput(inputs)) {
            $('#view-div').show();
            var getpdfdatarequest = eb.post(getpdfdataurl, {
                from_date: $('#from_date').val(),
                to_date: $('#to_date').val()
            });
            getpdfdatarequest.done(function (monthlyinvoicedata) {

                if (monthlyinvoicedata == "nodata") {
                    p_notification(false, eb.getMessage('ERR_REPOPDF_SALESDURATION'));
                    $('#view-div').hide();
                } else {

                    var c = 1;
                    var tax_net = 0.00;
                    var amount_net = 0.00;
                    if (check_tax == false) {

                        $('#tax_row').hide();
                        for (var i in monthlyinvoicedata.invoice) {

                            var month = monthlyinvoicedata.invoice[i].Month;
                            var year = monthlyinvoicedata.invoice[i].Year;
                            var total_tax = parseFloat(monthlyinvoicedata.invoice[i].TotalTax);
                            var tax_val = parseFloat(Math.round(total_tax * 100) / 100).toFixed(2);
                            var total_amount = parseFloat(monthlyinvoicedata.invoice[i].TotalAmount);
                            var amount_val = parseFloat(Math.round(total_amount * 100) / 100).toFixed(2);
                            tax_net += parseFloat(tax_val);
                            amount_net += parseFloat(amount_val);
                            var tbl_row = "<tr><td>" + c + "</td>"
                                    + "<td>" + year + "</td>"
                                    + "<td>" + month + "</td>"
                                    + "<td align='right'>" + amount_val + "</td>"
                            tbl_row += "</tr>";
                            $("#monthly_report_body").append(tbl_row);
                            c++;
                        }

                        $('#totalSales').html(amount_net);
                    } else {
                        $('#tax_row').show();
                        for (var i in monthlyinvoicedata.invoice) {

                            var month = monthlyinvoicedata.invoice[i].Month;
                            var year = monthlyinvoicedata.invoice[i].Year;
                            var total_tax = parseFloat(monthlyinvoicedata.invoice[i].TotalTax);
                            var tax_val = parseFloat(Math.round(total_tax * 100) / 100).toFixed(2);
                            var total_amount = parseFloat(monthlyinvoicedata.invoice[i].TotalAmount);
                            var amount_val = parseFloat(Math.round(total_amount * 100) / 100).toFixed(2);
                            tax_net += parseFloat(tax_val);
                            amount_net += parseFloat(amount_val);
                            var tbl_row = "<tr><td>" + c + "</td>"
                                    + "<td>" + year + "</td>"
                                    + "<td>" + month + "</td>"
                                    + "<td align='right'>" + tax_val + "</td>"
                                    + "<td align='right'>" + amount_val + "</td>"
                            tbl_row += "</tr>";
                            $("#monthly_report_body").append(tbl_row);
                            c++;
                        }

                        $('#totalTaxAmount').html(tax_net);
                        $('#totalSales').html(amount_net);
                        //$('#tax_row').hide();
                    }
                }

            });
        }

    });
    /* Generate Excel report for Monthly report
     * @author SANDUN <sandun@thinkcube.com>
     * */
    $("#excel_monthly_report").click(function (e) {
        e.preventDefault();
        var check_tax;
        if (tax.checked == false) {

            check_tax = 0;
        } else {

            check_tax = 1;
        }

        inputs = new Array(
                $('#from_date').val(),
                $('#to_date').val()
                );
        if (validteinput(inputs)) {
            var from_date = $("#from_date").val();
            var to_date = $("#to_date").val();
            window.open(BASE_URL + "/salesReportAPI/monthlyExcelSheet?from_date=" + from_date + "&to_date=" + to_date + "&check_tax=" + check_tax, '_blank');
        }
    });
    /*
     * Generate PDF report for Monthly report
     * @author SANDUN <sandun@thinkcube.com>
     * */

    $("#generate_monthly_pdf").click(function (e) {
        e.preventDefault();
        var check_tax;
        if (tax.checked == false) {

            check_tax = 0;
        } else {

            check_tax = 1;
        }

        inputs = new Array(
                $('#from_date').val(),
                $('#to_date').val()
                );
        if (validteinput(inputs)) {

            var from_date = $("#from_date").val();
            var to_date = $("#to_date").val();
            window.open(BASE_URL + "/salesReport/generateMonthlyPdf?from_date=" + from_date + "&to_date=" + to_date + "&check_tax=" + check_tax, '_blank');
        }
    });
    /*
     * Daily report Functions
     * @author SANDUN <sandun@thinkcube.com>
     *
     * */

    $('#monthly_graph').click(function (e) {
        e.preventDefault();
        var check_tax;
        if (tax.checked == false) {

            check_tax = false;
        } else {

            check_tax = true;
        }


        $('#view-div').hide();
        $('#sales_monthly').html('');
        var getpdfdataurl = BASE_URL + '/salesReportAPI/getMonthlySalesDetailsGraph';
        inputs = new Array(
                $('#from_date').val(),
                $('#to_date').val()
                );
        if (validteinput(inputs)) {
            $('#graph_div').show();
            var getpdfdatarequest = eb.post(getpdfdataurl, {
                from_date: $('#from_date').val(),
                to_date: $('#to_date').val()
            });
            getpdfdatarequest.done(function (monthlysales) {

                var graphData = monthlysales['salesGraphData'];
                var a = Array();
                for (var i = 0; i < graphData.length; i++) {
                    var Month = graphData[i].Month;
                    var TotalAmount = parseFloat(Math.round((graphData[i].TotalAmount) * 100) / 100).toFixed(2);
                    var TotalTax = parseFloat(Math.round((graphData[i].TotalTax) * 100) / 100).toFixed(2);
                    a.push({x: Month, y: TotalAmount, z: TotalTax});

                }


                if (check_tax == true) {

                    Morris.Bar({
                        element: 'sales_monthly',
                        data: a,
                        xkey: 'x',
                        ykeys: ['y', 'z'],
                        labels: ['TotalAmount', 'Total Tax'],
                        parseTime: false,
                        ymax: 100000
                    });
                } else {

                    Morris.Bar({
                        element: 'sales_monthly',
                        data: a,
                        xkey: 'x',
                        ykeys: ['y'],
                        labels: ['TotalAmount'],
                        parseTime: false,
                        ymax: 100000
                    });
                }
            });
        }
    });
    /*
     * Generate view of Daily Report (View Button)
     * @author SANDUN <sandun@thinkcube.com>
     *
     * */

    $('#view_daily_report').click(function (e) {
        e.preventDefault();
        var check_tax;
        if (tax.checked == false) {

            check_tax = false;
        } else {

            check_tax = true;
        }
        var date = $('#date').val();
        $("#daily_report_body").html('');
        var getpdfdataurl = BASE_URL + '/salesReportAPI/getDailySalesDetails';
        if ($('#date').val() == null || $('#date').val() == "") {
            p_notification(false, eb.getMessage('ERR_CUSREPO_DATEFILL'));
            $('#date').focus();
            return false;
        } else {

            $('#view-div').show();
            var getpdfdatarequest = eb.post(getpdfdataurl, {
                date: $('#date').val()
            });
            getpdfdatarequest.done(function (dailyinvoicedata) {

                if (dailyinvoicedata == "nodata") {
                    p_notification(false, eb.getMessage('ERR_REPOPDF_SALESDURATION'));
                    $('#view-div').hide();
                } else {
                    $('#tax_row').show();
                    var c = 1;
                    var tax_net = 0.00;
                    var amount_net = 0.00;
                    if (check_tax == false) {
                        $('#tax_row').hide();
                        var tbl_row =
                                "<tr><td>" + date + "</td>"
                                + "<td></td>"
                                + "<td></td>"
                                + "<td align='right'></td>"

                        tbl_row += "</tr>";
                        $("#daily_report_body").append(tbl_row);
                        for (var i in dailyinvoicedata.invoice) {

                            var id = dailyinvoicedata.invoice[i].id;
                            var custName = dailyinvoicedata.invoice[i].customerName;
                            var total_tax = parseFloat(dailyinvoicedata.invoice[i].taxAmount);
                            var tax_val = parseFloat(Math.round(total_tax * 100) / 100).toFixed(2);
                            var total_amount = parseFloat(dailyinvoicedata.invoice[i].totalAmount);
                            var amount_val = parseFloat(Math.round(total_amount * 100) / 100).toFixed(2);
                            tax_net += parseFloat(tax_val);
                            amount_net += parseFloat(amount_val);
                            var tbl_row =
                                    "<tr><td></td>"
                                    + "<td>" + id + "</td>"
                                    + "<td>" + custName + "</td>"
                                    + "<td align='right' id='tax_row'>" + tax_val + "</td>"
                            tbl_row += "</tr>";
                            $("#daily_report_body").append(tbl_row);
                            c++;
                        }

                        $('#totalTaxAmount').html(tax_net);
                        $('#totalSales').html(amount_net);
                        //$('#tax_row').hide();
                    } else {
                        $('#tax_row').show();
                        var tbl_row =
                                "<tr><td>" + date + "</td>"
                                + "<td></td>"
                                + "<td></td>"
                                + "<td align='right'></td>"
                                + "<td align='right'></td>"
                        tbl_row += "</tr>";
                        $("#daily_report_body").append(tbl_row);
                        for (var i in dailyinvoicedata.invoice) {

                            var id = dailyinvoicedata.invoice[i].id;
                            var custName = dailyinvoicedata.invoice[i].customerName;
                            var total_tax = parseFloat(dailyinvoicedata.invoice[i].taxAmount);
                            var tax_val = parseFloat(Math.round(total_tax * 100) / 100).toFixed(2);
                            var total_amount = parseFloat(dailyinvoicedata.invoice[i].totalAmount);
                            var amount_val = parseFloat(Math.round(total_amount * 100) / 100).toFixed(2);
                            tax_net += parseFloat(tax_val);
                            amount_net += parseFloat(amount_val);
                            var tbl_row =
                                    "<tr><td></td>"
                                    + "<td>" + id + "</td>"
                                    + "<td>" + custName + "</td>"
                                    + "<td align='right'>" + tax_val + "</td>"
                                    + "<td align='right'>" + amount_val + "</td>"
                            tbl_row += "</tr>";
                            $("#daily_report_body").append(tbl_row);
                        }

                        $('#totalTaxAmount').html(tax_net);
                        $('#totalSales').html(amount_net);
                    }
                }
            });
        }

    });
    /* Generate Excel report for Daily report
     * @author SANDUN <sandun@thinkcube.com>
     * */


    $("#excel_daily_report").click(function (e) {
        e.preventDefault();
        inputs = new Array(
                $('#from_date').val(),
                $('#to_date').val()
                );
        var check_tax;
        if (tax.checked == false) {

            check_tax = 0;
        } else {

            check_tax = 1;
        }

        if ($('#date').val() == null || $('#date').val() == "") {
            p_notification(false, eb.getMessage('ERR_CUSREPO_DATEFILL'));
            $('#date').focus();
            return false;
        } else {
            var date = $("#date").val();
            window.open(BASE_URL + "/salesReportAPI/dailyExcelSheet?date=" + date + "&check_tax=" + check_tax, '_blank');
        }
    });
    /* Generate PDF report for Daily report
     * @author SANDUN <sandun@thinkcube.com>
     * */


    $("#generate_daily_pdf").click(function (e) {
        e.preventDefault();
        var check_tax;
        if (tax.checked == false) {

            check_tax = 0;
        } else {

            check_tax = 1;
        }

        if ($('#date').val() == null || $('#date').val() == "") {
            p_notification(false, eb.getMessage('ERR_CUSREPO_DATEFILL'));
            $('#date').focus();
            return false;
        } else {
            var date = $("#date").val();
            window.open(BASE_URL + "/salesReport/generateDailyPdf?date=" + date + "&check_tax=" + check_tax, '_blank');
        }
    });
    //////END Generate Daily PDF File////////////

    $('#daily_graph').click(function (e) {
        e.preventDefault();
        var check_tax;
        if (tax.checked == false) {

            check_tax = false;
        } else {

            check_tax = true;
        }


        $('#view-div').hide();
        $('#sales_daily').html('');
        var getdataurl = BASE_URL + '/salesReportAPI/getDailySalesDetailsGraph';
        var inputs = new Array($('#date').val());
        if ($('#date').val() == null || $('#date').val() == "") {
            p_notification(false, eb.getMessage('ERR_CUSREPO_DATEFILL'));
            $('#date').focus();
            return false;
        } else {
            $('#graph_div').show();
            var getdatarequest = eb.post(getdataurl, {
                date: $('#date').val()
            });
            getdatarequest.done(function (dailysales) {

                var graphData = dailysales['dailySales'];
                var a = Array();
                for (var i = 0; i < graphData.length; i++) {
                    var Date = graphData[i].Date;
                    var TotalAmount = parseFloat(Math.round((graphData[i].TotalAmount) * 100) / 100).toFixed(2);
                    var TotalTax = parseFloat(Math.round((graphData[i].TotalTax) * 100) / 100).toFixed(2);
                    a.push({x: Date, y: TotalAmount, z: TotalTax});

                }

                if (check_tax == true) {

                    Morris.Bar({
                        element: 'sales_daily',
                        data: a,
                        xkey: 'x',
                        ykeys: ['y', 'z'],
                        labels: ['TotalAmount', 'Total Tax'],
                        parseTime: false,
                        ymax: 100000
                    });
                } else {

                    Morris.Bar({
                        element: 'sales_daily',
                        data: a,
                        xkey: 'x',
                        ykeys: ['y'],
                        labels: ['TotalAmount', 'Total Tax'],
                        parseTime: false,
                        ymax: 100000
                    });
                }
            });
        }
    });
    /*
     * Generate VIEW of Annual report (View Button)
     * @author SANDUN <sandun@thinkcube.com>
     *
     * */
    $('#view_annual_report').click(function (e) {
        e.preventDefault();
        var check_tax;
        if (tax.checked == false) {

            check_tax = false;
        } else {

            check_tax = true;
        }

        var year = $('#year').val();
        year = year.replace(/\s+/g, "");
        $.trim(year);
        $("#annual_report_body").html('');
        var getannualdataurl = BASE_URL + '/salesReportAPI/getAnnualSalesDetails';
        inputs = new Array(
                $('#year').val()
                );
        if ($('#year').val() == null || $('#year').val() == "") {
            p_notification(false, eb.getMessage('ERR_REPOPDF_YEARFILL'));
            $('#year').focus();
            return false;
        } else {

            $('#view-div').show();
            var getannualdatarequest = eb.post(getannualdataurl, {
                year: year
            });
            getannualdatarequest.done(function (annualinvoicedata) {
                if (annualinvoicedata == "nodata") {
                    p_notification(false, eb.getMessage('ERR_REPOPDF_SALESDURATION'));
                    $('#view-div').hide();
                } else {
                    var c = 1;
                    var tax_net = 0.00;
                    var amount_net = 0.00;
                    if (check_tax == false) {
                        $('#tax_row').hide();
                        $('#totalTaxAmount').hide();
                        $('#tax_total_title').hide();
                        for (var i in annualinvoicedata.invoice) {

                            var qa1 = parseFloat(Math.round((annualinvoicedata.invoice[i].Q1Amount) * 100) / 100).toFixed(2);
                            qa1 = qa1 ? qa1 : 0.00;
                            var qa2 = parseFloat(Math.round((annualinvoicedata.invoice[i].Q2Amount) * 100) / 100).toFixed(2);
                            qa2 = qa2 ? qa2 : 0.00;
                            var qa3 = parseFloat(Math.round((annualinvoicedata.invoice[i].Q3Amount) * 100) / 100).toFixed(2);
                            qa3 = qa3 ? qa3 : 0.00;
                            var qa4 = parseFloat(Math.round((annualinvoicedata.invoice[i].Q4Amount) * 100) / 100).toFixed(2);
                            qa4 = qa4 ? qa4 : 0.00;
                            amount_net = parseFloat(qa1) + parseFloat(qa2) + parseFloat(qa3) + parseFloat(qa4);
                            var tbl_row =
                                    "<tr><td>" + year + "</td>"
                                    + "<td>" + "Quarter1" + "</td>"
                                    + "<td align='right'>" + qa1 + "</td></tr>";
                            tbl_row += "<tr><td></td>"
                                    + "<td>" + "Quarter2" + "</td>"
                                    + "<td align='right'>" + qa2 + "</td></tr>";
                            tbl_row += "<tr><td></td>"
                                    + "<td>" + "Quarter3" + "</td>"
                                    + "<td align='right'>" + qa3 + "</td></tr>";
                            tbl_row += "<tr><td></td>"
                                    + "<td>" + "Quarter4" + "</td>"
                                    + "<td align='right'>" + qa4 + "</td></tr>";
                            $("#annual_report_body").append(tbl_row);
                        }

                        $('#totalTaxAmount').html(tax_net);
                        $('#totalSales').html(amount_net);
                    } else {

                        $('#tax_row').show();
                        $('#totalTaxAmount').show();
                        $('#tax_total_title').show();
                        for (var i in annualinvoicedata.invoice) {

                            var qa1 = parseFloat(Math.round((annualinvoicedata.invoice[i].Q1Amount) * 100) / 100).toFixed(2);
                            qa1 = qa1 ? qa1 : 0.00;
                            var qa2 = parseFloat(Math.round((annualinvoicedata.invoice[i].Q2Amount) * 100) / 100).toFixed(2);
                            qa2 = qa2 ? qa2 : 0.00;
                            var qa3 = parseFloat(Math.round((annualinvoicedata.invoice[i].Q3Amount) * 100) / 100).toFixed(2);
                            qa3 = qa3 ? qa3 : 0.00;
                            var qa4 = parseFloat(Math.round((annualinvoicedata.invoice[i].Q4Amount) * 100) / 100).toFixed(2);
                            qa4 = qa4 ? qa4 : 0.00;
                            var qt1 = parseFloat(Math.round((annualinvoicedata.invoice[i].Q1Tax) * 100) / 100).toFixed(2);
                            qt1 = qt1 ? qt1 : 0.00;
                            var qt2 = parseFloat(Math.round((annualinvoicedata.invoice[i].Q2Tax) * 100) / 100).toFixed(2);
                            qt2 = qt2 ? qt2 : 0.00;
                            var qt3 = parseFloat(Math.round((annualinvoicedata.invoice[i].Q3Tax) * 100) / 100).toFixed(2);
                            qt3 = qt3 ? qt3 : 0.00;
                            var qt4 = parseFloat(Math.round((annualinvoicedata.invoice[i].Q4Tax) * 100) / 100).toFixed(2);
                            qt4 = qt4 ? qt4 : 0.00;
                            tax_net = parseFloat(qt1) + parseFloat(qt2) + parseFloat(qt3) + parseFloat(qt4);
                            amount_net = parseFloat(qa1) + parseFloat(qa2) + parseFloat(qa3) + parseFloat(qa4);
                            var tbl_row =
                                    "<tr><td>" + year + "</td>"
                                    + "<td>" + "Quarter1" + "</td>"
                                    + "<td align='right' id='tax_row'>" + qt1 + "</td>"
                                    + "<td align='right'>" + qa1 + "</td></tr>";
                            tbl_row += "<tr><td></td>"
                                    + "<td>" + "Quarter2" + "</td>"
                                    + "<td align='right' id='tax_row'>" + qt2 + "</td>"
                                    + "<td align='right'>" + qa2 + "</td></tr>";
                            tbl_row += "<tr><td></td>"
                                    + "<td>" + "Quarter3" + "</td>"
                                    + "<td align='right' id='tax_row'>" + qt3 + "</td>"
                                    + "<td align='right'>" + qa3 + "</td></tr>";
                            tbl_row += "<tr><td></td>"
                                    + "<td>" + "Quarter4" + "</td>"
                                    + "<td align='right' id='tax_row'>" + qt4 + "</td>"
                                    + "<td align='right'>" + qa4 + "</td></tr>";
                            $("#annual_report_body").append(tbl_row);
                        }

                        $('#totalTaxAmount').html(tax_net);
                        $('#totalSales').html(amount_net);
                    }
                }
            });
        }
    });
    /* Generate Excel report for Daily report
     * @author SANDUN <sandun@thinkcube.com>
     * */
    $("#excel_annual_report").click(function (e) {
        e.preventDefault();
        var check_tax;
        if (tax.checked == false) {

            check_tax = 0;
        } else {

            check_tax = 1;
        }

        if ($('#year').val() == null || $('#year').val() == "") {
            p_notification(false, eb.getMessage('ERR_REPOPDF_YEARFILL'));
            $('#year').focus();
            return false;
        } else {
            var year = $("#year").val();
            year = year.replace(/\s+/g, "");
            $.trim(year);
            window.open(BASE_URL + "/salesReportAPI/annualExcelSheet?year=" + year + "&check_tax=" + check_tax, '_blank');
        }
    });
    //////////////////END Excel file Generation function//////////

    /*
     * Generate PDF of Annual report
     * @author SANDUN <sandun@thinkcube.com>
     *
     * */
    $("#generate_annual_pdf").click(function (e) {
        e.preventDefault();
        var check_tax;
        if (tax.checked == false) {

            check_tax = 0;
        } else {

            check_tax = 1;
        }

        if ($('#year').val() == null || $('#year').val() == "") {
            p_notification(false, eb.getMessage('ERR_REPOPDF_YEARFILL'));
            $('#year').focus();
            return false;
        } else {
            var year = $("#year").val();
            year = year.replace(/\s+/g, "");
            $.trim(year);
            window.open(BASE_URL + "/salesReport/generateAnnualPdf?year=" + year + "&check_tax=" + check_tax, '_blank');
        }
    });
    //////END Generate Annual PDF File////////////

    $('#annual_graph').click(function (e) {
        e.preventDefault();
        var check_tax;
        if (tax.checked == false) {

            check_tax = false;
        } else {

            check_tax = true;
        }

        var year = $('#year').val();
        year = year.replace(/\s+/g, "");
        $.trim(year);
        $('#view-div').hide();
        $('#sales_annuval').html('');
        var getdataurl = BASE_URL + '/dashboardAPI/getAnnuvalSalesDetails';
        var inputs = new Array($('#year').val());
        if ($('#year').val() == null || $('#year').val() == "") {
            p_notification(false, eb.getMessage('ERR_REPOPDF_YEARFILL'));
            $('#year').focus();
            return false;
        } else {
            $('#graph_div').show();
            var getdatarequest = eb.post(getdataurl, {
                year: year
            });
            getdatarequest.done(function (annuvalsales) {
                var salesData = annuvalsales['salesData'];
                var qa1 = parseFloat(Math.round((salesData[0].Q1Amount) * 100) / 100).toFixed(2);
                qa1 = qa1 ? qa1 : 0.00;
                var qa2 = parseFloat(Math.round((salesData[0].Q2Amount) * 100) / 100).toFixed(2);
                qa2 = qa2 ? qa2 : 0.00;
                var qa3 = parseFloat(Math.round((salesData[0].Q3Amount) * 100) / 100).toFixed(2);
                qa3 = qa3 ? qa3 : 0.00;
                var qa4 = parseFloat(Math.round((salesData[0].Q4Amount) * 100) / 100).toFixed(2);
                qa4 = qa4 ? qa4 : 0.00;
                var qt1 = parseFloat(Math.round((salesData[0].Q1Tax) * 100) / 100).toFixed(2);
                qt1 = qt1 ? qt1 : 0.00;
                var qt2 = parseFloat(Math.round((salesData[0].Q2Tax) * 100) / 100).toFixed(2);
                qt2 = qt2 ? qt2 : 0.00;
                var qt3 = parseFloat(Math.round((salesData[0].Q3Tax) * 100) / 100).toFixed(2);
                qt3 = qt3 ? qt3 : 0.00;
                var qt4 = parseFloat(Math.round((salesData[0].Q4Tax) * 100) / 100).toFixed(2);
                qt4 = qt4 ? qt4 : 0.00;

                if (check_tax == true) {

                    Morris.Bar({
                        element: 'sales_annuval',
                        data: [
                            {y: 'Q1', a: qa1, b: qt1},
                            {y: 'Q2', a: qa2, b: qt2},
                            {y: 'Q3', a: qa3, b: qt3},
                            {y: 'Q4', a: qa4, b: qt4}
                        ],
                        xkey: 'y',
                        ykeys: ['a', 'b'],
                        labels: ['Gross Sales', 'Net Sales'],
                        ymax: 10000,
                        ymin: 0
                    });
                } else {

                    Morris.Bar({
                        element: 'sales_annuval',
                        data: [
                            {y: 'Q1', a: qa1},
                            {y: 'Q2', a: qa2},
                            {y: 'Q3', a: qa3},
                            {y: 'Q4', a: qa4}
                        ],
                        xkey: 'y',
                        ykeys: ['a'],
                        labels: ['Gross Sales'],
                        ymax: 10000,
                        ymin: 0
                    });
                }
            });
        }
    });

    $('#view_p_summery_report').click(function (e) {
        e.preventDefault();
        $("#p_summery_report_body").html('');
        var getpdfdataurl = BASE_URL + '/salesReportAPI/getPaymentSummeryDetails';
        inputs = new Array(
                $('#from_date').val(),
                $('#to_date').val()
                );
        if (validteinput(inputs)) {

            var getpdfdatarequest = eb.post(getpdfdataurl, {
                from_date: $('#from_date').val(),
                to_date: $('#to_date').val()
            });
            getpdfdatarequest.done(function (paydata) {

                var paySummeryData = paydata['paySummData'];

                if (paydata == "nodata") {
                    p_notification(false, eb.getMessage('ERR_REPOPDF_SALESDURATION'));
                    $('#view-div').hide();
                } else {
                    $('#view-div').show();

                    for (var i in paySummeryData) {

                        var date = paySummeryData[i].date;
                        var payNo = paySummeryData[i].paymentID;
                        var customer = paySummeryData[i].customerName;
                        var payMethod = paySummeryData[i].method;
                        var cheqNo = paySummeryData[i].refnumber;

                        var remarks = paySummeryData[i].memo;
                        if (remarks == null) {
                            remarks = 'NO';
                        }

                        var amount = parseFloat(Math.round(paySummeryData[i].amount * 100) / 100).toFixed(2);

                        var tbl_row = "<tr><td align='left'>" + (++i) + "</td>"
                                + "<td align='left'>" + date + "</td>"
                                + "<td align='left'>" + payNo + "</td>"
                                + "<td align='left'>" + customer + "</td>"
                                + "<td align='right'>" + accounting.formatMoney(amount) + "</td>"
                                + "<td align='left'>" + payMethod + "</td>"
                                + "<td align='left'>" + cheqNo + "</td>"
                                + "<td align='right'>" + 'cheqDate' + "</td>"
                                + "<td align='right'>" + remarks + "</td>"
                        tbl_row += "</tr>";
                        $("#p_summery_report_body").append(tbl_row);
                        i++;
                    }
                }
            });
        }
    });

    $('#excel_p_summery_report').click(function (e) {

        var from_date = $('#from_date').val();
        var to_date = $('#to_date').val();

        inputs = new Array(from_date, to_date);

        if (validteinput(inputs)) {

            window.open(BASE_URL + "/salesReportAPI/paymentSummeryExcelSheet?from_date=" + from_date + "&to_date=" + to_date, '_blank');
        }
    });
    $('#generate_p_summery_pdf').click(function (e) {

        var from_date = $('#from_date').val();
        var to_date = $('#to_date').val();

        inputs = new Array(from_date, to_date);

        if (validteinput(inputs)) {

            window.open(BASE_URL + "/reports/salesReport/generatePaymentsSummery?from_date=" + from_date + "&to_date=" + to_date, '_blank');
        }
    });
    $('#invPay_graph').click(function (e) {
        from_date: $('#from_date').val();
        to_date: $('#to_date').val();

        $('#inv_pay').html('');
        var getpdfdataurl = BASE_URL + '/salesReportAPI/getMonthlySalesDetailsGraph';
        inputs = new Array(
                from_date,
                to_date
                );
        if (validteinput(inputs)) {

            //$('#graph_div').show();
            var getpdfdatarequest = eb.post(getpdfdataurl, {
                from_date: from_date,
                to_date: to_date

            });

            getpdfdatarequest.done(function (invpaydata) {
                $('#graph_div').show();
                var graphData = invpaydata['salesGraphData'];
                var a = Array();
                for (var i = 0; i < graphData.length; i++) {
                    var Month = graphData[i].Month;
                    var TotalAmount = parseFloat(Math.round((graphData[i].TotalAmount) * 100) / 100).toFixed(2);
                    var TotalPayed = parseFloat(Math.round((graphData[i].TotalPayed) * 100) / 100).toFixed(2);
                    a.push({x: Month, y: TotalAmount, z: TotalPayed});
                }

                Morris.Bar({
                    element: 'inv_pay',
                    data: a, xkey: 'x',
                    ykeys: ['y', 'z'],
                    labels: ['TotalAmount', 'Total Payed'],
                    parseTime: false
                });
            });
        }

    });

    //////////// check box pick function//////////
    //var tax;
    if (tax.checked == false) {
        document.getElementById('tax_value').innerHTML = "Off";
    } else {
        document.getElementById('tax_value').innerHTML = "On";
    }

    $('#tax').click(function () {
        if (tax.checked == true) {
            document.getElementById('tax_value').innerHTML = "On";
        } else {
            document.getElementById('tax_value').innerHTML = "Off";
        }
    });
    /* validation function for Monthly Form
     * @author SANDUN <sandun@thinkcube.com>
     * */
    function validteinput(inputs) {
        var from_date = inputs[0];
        var to_date = inputs[1];
        if (from_date == null || from_date == "") {
            p_notification(false, eb.getMessage('ERR_DAISALE_FROMD'));
            document.getElementById("from_date").focus();
        } else if (to_date == null || to_date == "") {
            p_notification(false, eb.getMessage('ERR_DAISALE_TOD'));
            document.getElementById("to_date").focus();
        } else {
            return true;
        }

    }
    //////////END Validation Function//////////////
});






