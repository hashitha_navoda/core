$(function() {
    $(window).resize(function() {
        var height = $(window).height();
        $('.header').height('80px');
        $('.footer').height('80px');
        $('.body').css('min-height', height - 160 + 'px');
        var footer_height = $('.footer').height();
        var header_height = $('.header').height();
        $('.footer').css('line-height', footer_height + 'px');
        $('.header h1').css('line-height', header_height + 'px');
    }).trigger('resize');

    //initialize the tooltip
    $('.password_recover').tooltip({
        placement: 'auto top',
        container: 'body'
    });

    //show the pass_word recover section
    $('.password_recover').on('click', function() {
        $('.heading').text('Reset password')
        $('.form_wrapper').slideUp();
        $('.request_password').slideDown(function() {
            $('#email').focus();
            $('.reset_password').addClass('slide_down');
        });
    });

    //back to show login section
    $('.back_btn').on('click', function() {
        $('.heading').text('Sign In')
        $('.request_password').slideUp();
        $('.form_wrapper').slideDown(function() {
            $('#user_name').focus();
        });
    });

    /**
     if ($('#logMsgPanel').is(':visible')) {
     setTimeout(function() {
     $('#logMsgPanel').slideUp('400');
     }, 2500);
     }
     if ($('#passMsgPanel').is(':visible')) {
     setTimeout(function() {
     $('#passMsgPanel').slideUp('400');
     }, 2500);
     }
     **/

    $('#forgotPasswordForm').submit(function(e) {
        e.preventDefault();
        if ($('#usernameForgot').val() == '') {
            $('#forgotPassMsg').html('Please provide a username');
            $('#forgotPassMsgPanel', '#forgotPasswordForm').slideDown('400').removeClass('hidden');
            hideMsgPanel();
        } else {
            var param = {username: $('#usernameForgot').val()};
            $('#forgotPasswordSubmitButton', '#forgotPasswordForm').prop('disabled', true);
            $.ajax({
                type: 'POST',
                url: '/user/forgotPassword',
                data: param,
                success: function(respond) {
                    if (respond.status == false) {
                        $('#forgotPasswordSubmitButton', '#forgotPasswordForm').prop('disabled', false);
                        $('#forgotPassMsg').html(respond.msg);
                        $('#forgotPassMsgPanel', '#forgotPasswordForm').slideDown('400').removeClass('hidden');
                    } else {
                        $('#forgotPassMsg').html(respond.msg);
                        $('#forgotPassMsgPanel', '#forgotPasswordForm').slideDown('400').removeClass('hidden').addClass('login_success');
                        setTimeout(function() {
                            window.location.reload(1);
                        }, 5000);
                    }
                },
            });
        }
    });

    function hideMsgPanel(timeout) {

        timeout = (timeout === undefined) ? 3000 : timeout;
        if ($('#logMsgPanel').is(':visible')) {
            setTimeout(function() {
                $('#logMsgPanel').slideUp('400');
            }, timeout);
        }
        if ($('#forgotPassMsgPanel').is(':visible')) {
            setTimeout(function() {
                $('#forgotPassMsgPanel').slideUp('400');
            }, timeout);
        }
    }
});