$(document).ready(function() {
    var saveData = new Array();
    var bomsubDataArray = new Array();
    var selectedbomID;
    var locationID = $('#bom_parent').attr('data-locationID');
    var aDType = 'assembly';
    $('#assembly-id').attr('checked', true);
    $('#assembly-id ,#dissambly-id').on('click', function() {
        aDType = $(this).val();
        if (aDType == 'assembly') {
            $('#save-assembly').removeClass('hidden');
            $('#save-disambly').addClass('hidden');
            if (selectedbomID) {
                loadRelatedBomSubItemDetails(selectedbomID);
            }
        } else {
            $('#save-assembly').addClass('hidden');
            $('#save-disambly').removeClass('hidden');
            if (selectedbomID) {
                loadRelatedBomSubItemDetails(selectedbomID);
            }
        }
    });
    loadDropDownFromDatabase('/bom-api/search-bom-items-for-dropdown?adList=true', locationID, '', '#bom_parent', '', '', '');
    $('#bom_parent').selectpicker('refresh');
    $('#bom_parent').on('change', function() {
        if ($(this).val() > 0 && $(this).val() != selectedbomID) {
            selectedbomID = $(this).val();
            loadRelatedBomSubItemDetails(selectedbomID);
        }
    });

    $('#bom-ad-quantity,#bom-ad-need-quantity').on('keyup', function() {
        var adQuonValue = $(this).val();
        if (aDType == 'assembly') {
            var maxAssembleValue = $('#bom-ad-max-quantity').val();
            if (isNaN(adQuonValue)) {
                p_notification(false, eb.getMessage('ERROR_BOM_ASSEMBLY_DATA_FORMAT'));
                $('#bom-ad-quantity').val('');
                return false;
            } else if (isInteger(adQuonValue)) {
                p_notification(false, eb.getMessage('ERROR_BOM_ASSEMBLY_FLOAT_DATA'));
                $('#bom-ad-quantity').val('');
                return false;
            }
            else if (parseInt(maxAssembleValue) < parseInt(adQuonValue)) {
                p_notification(false, eb.getMessage('ERROR_EXEED_BOM_ASSEMBLY_VALUE'));
                $('#bom-ad-quantity').val('');
                return false;
            }

        } else {
            if ($('#bom-ad-avail-quantity').val() == '0') {
                p_notification(false, eb.getMessage('ERROR_NO_EXIST_BOM_PARENT_DATA'));
                $('#bom-ad-need-quantity').val('');
                return false;
            } else if (isNaN(adQuonValue)) {
                p_notification(false, eb.getMessage('ERROR_BOM_ASSEMBLY_DATA_FORMAT'));
                $('#bom-ad-need-quantity').val('');
                return false;
            } else if (isInteger(adQuonValue)) {
                p_notification(false, eb.getMessage('ERROR_BOM_ASSEMBLY_FLOAT_DATA'));
                $('#bom-ad-need-quantity').val('');
                return false;
            } else if (parseInt($('#bom-ad-avail-quantity').val()) < parseInt(adQuonValue)) {
                p_notification(false, eb.getMessage('ERROR_EXEED_BOM_DISSEMBLY_VALUE'));
                $('#bom-ad-need-quantity').val('');
                return false;
            }
        }
    });

    $('.submit-bom-ad').on('click', function() {
        saveData = {
            'adRef': $('#bom-ad-reference').val(),
            'adType': aDType,
            'parentID': selectedbomID,
            'comment': $('#bom-ad-comments').val(),
            'subData': bomsubDataArray,
            'createQuantity': $('#bom-ad-quantity').val(),
            'disamblyQuantity': $('#bom-ad-need-quantity').val(),
            'locationID': locationID,
        };
        if (validateSaveData(saveData)) {
            eb.ajax({
                url: BASE_URL + '/bom-api/saveBomAD',
                method: 'post',
                data: saveData,
                dataType: 'json',
                success: function(data) {
                    if (data.status == true) {
                        p_notification(data.status, data.msg);
                        window.setTimeout(function() {
                            location.reload();
                        }, 1000);
                    }
                }
            });
        }
    });


    function loadRelatedBomSubItemDetails(bomID) {
        if (aDType == 'assembly') {
            var dataArray = {
                'bomID': bomID,
                'locationID': locationID,
                'type': 'assembly'
            };
            eb.ajax({
                url: BASE_URL + '/bom-api/getBomSubItemDetailsForAssemble',
                method: 'post',
                data: dataArray,
                dataType: 'json',
                success: function(data) {
                    if (data.status == true) {
                        bomsubDataArray = data.data.dataArray;
                        $('.sub-item-table').removeClass('hidden');
                        $('.bom-assembley-user-data').removeClass('hidden');
                        $('.bom-dissembley-user-data').addClass('hidden');
                        for (var i = 0; i < Object.keys(data.data.dataArray).length; i++) {
                            var cloneId = 'sub-item-' + i;
                            $('#sample_data_set').clone().appendTo('#bom_sub_item_body').attr('id', cloneId);
                            $('#' + cloneId).removeClass('hidden');
                            $('#' + cloneId).find('.sub-item').val(data.data.dataArray[i]['productName']).attr('disabled', true);
                            $('#' + cloneId).find('.bom_sub_item_quo').val(data.data.dataArray[i]['bomSubItemQuantity']).attr('disabled', true);
                            $('#' + cloneId).find('.product-available-quo').val(data.data.dataArray[i]['locationProductQuantity']).attr('disabled', true);
                        }
                        if(data.data.minimumAssemblyOrDissambly === null){
                            data.data.minimumAssemblyOrDissambly = '-';
                        }
                        $('#bom-ad-max-quantity').val(data.data.minimumAssemblyOrDissambly).attr('disabled', true);
                    }
                }
            });

        } else {
            var dataArray = {
                'bomID': bomID,
                'locationID': locationID,
                'type': 'dissembly'
            };
            eb.ajax({
                url: BASE_URL + '/bom-api/getBomSubItemDetailsForAssemble',
                method: 'post',
                data: dataArray,
                dataType: 'json',
                success: function(data) {
                    if (data.status == true) {
                        bomsubDataArray = data.data.dataArray;
                        $('.sub-item-table').removeClass('hidden');
                        $('.bom-dissembley-user-data').removeClass('hidden');
                        $('.bom-assembley-user-data').addClass('hidden');
                        for (var i = 0; i < Object.keys(data.data.dataArray).length; i++) {
                            var cloneId = 'sub-item-' + i;
                            $('#sample_data_set').clone().appendTo('#bom_sub_item_body').attr('id', cloneId);
                            $('#' + cloneId).removeClass('hidden');
                            $('#' + cloneId).children().find('.sub-item').val(data.data.dataArray[i]['productName']).attr('disabled', true);
                            $('#' + cloneId).children().find('.bom_sub_item_quo').val(data.data.dataArray[i]['bomSubItemQuantity']).attr('disabled', true);
                            $('#' + cloneId).children().find('.product-available-quo').val(data.data.dataArray[i]['locationProductQuantity']).attr('disabled', true);
                        }
                        $('#bom-ad-avail-quantity').val(data.data.minimumAssemblyOrDissambly).attr('disabled', true);
                    }
                }
            });

        }
    }
});

function isInteger(x) {
    return (typeof x === 'number') && (x % 1 === 0);
}
function validateSaveData(data) {
    if (data.adRef == '' || data.adRef == null) {
        p_notification(false, eb.getMessage('ERROR_AD_BOM_REFERENCE_NULL'));
        $('#bom-ad-reference').onfocus();
        return false;
    } else if (data.parentID == '' || data.parentID == null) {
        p_notification(false, eb.getMessage('ERROR_AD_BOM_PARENT_NULL'));
        $('#bom_parent').onfocus();
        return false;
    } else if (data.adType == 'assembly' && (data.createQuantity == '' || data.createQuantity == null)) {
        p_notification(false, eb.getMessage('ERROR_AD_BOM_PARENT_CREATE_QUO_NULL'));
        $('#bom-ad-quantity').onfocus();
        return false;
    } else if (data.adType == 'dissembly' && (data.disamblyQuantity == '' || data.disamblyQuantity == null)) {
        p_notification(false, eb.getMessage('ERROR_AD_BOM_PARENT_DISAMBLE_QUO_NULL'));
        $('#bom-ad-need-quo').onfocus();
        return false;
    }
    else {
        return true;
    }
}