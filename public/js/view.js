/**
 * @author Damith Thamara <damith@thinkcube.com>
 * This javascript contains GRN,PO,PI View related functions
 */
var selectedPvID;
var selectedSupplierID;
var selectedGrnID;
$(document).ready(function() {

    $('#pvSearchByPV').selectpicker('hide');
    $('#pvSearchSelector').change(function() {
        var selecter = $(this).val();
        if (selecter === 'searchBySupplier') {
            $('#pvSearchByPV').selectpicker('hide');
            $('#pvSearchBySupplier').selectpicker('show');
        } else {
            $('#pvSearchBySupplier').selectpicker('hide');
            $('#pvSearchByPV').selectpicker('show');
        }
    });


    loadDropDownFromDatabase('/api/pi/search-allPV-for-dropdown', "", 0, '#pvSearchByPV');
    $('#pvSearchByPV').change(function() {
        if ($(this).val() > 0 && selectedPvID != $(this).val()) {
            selectedPvID = $(this).val();
            var param = {PVID: $(this).val()};
            getViewAndLoad('/api/pi/searchPVByPVID', 'piList', param);
        }
    });

    loadDropDownFromDatabase('/supplierAPI/search-suppliers-for-dropdown', "", 0, '#pvSearchBySupplier');
    $('#pvSearchBySupplier').change(function() {
        if ($(this).val() > 0 && selectedSupplierID != $(this).val()) {
            selectedSupplierID = $(this).val();
            var param = {supplierID: $(this).val()};
            getViewAndLoad('/api/pi/searchPVByPVID', 'piList', param);
        }
    });

    $('#pvSearchClear').on('click', function() {
        getViewAndLoad('/api/pi/searchPVByPVID/' + getCurrPage(), 'piList', {});
    });

    $('#grnSearchByGRN').selectpicker('hide');
    $('#grnSearchSelector').change(function() {
        var selecter = $(this).val();
        if (selecter === 'searchBySupplier') {
            $('#grnSearchByGRN').selectpicker('hide');
            $('#grnSearchBySupplier').selectpicker('show');
        } else {
            $('#grnSearchBySupplier').selectpicker('hide');
            $('#grnSearchByGRN').selectpicker('show');
        }
    });


    loadDropDownFromDatabase('/api/grn/search-all-grn-for-dropdown', "", 0, '#grnSearchByGRN');
    $('#grnSearchByGRN').trigger('change');
    $('#grnSearchByGRN').change(function() {
        if ($(this).val() > 0 && selectedGrnID != $(this).val()) {
            selectedGrnID = $(this).val();
            var param = {GRNID: $(this).val()};
            getViewAndLoad('/api/grn/get-grns-by-search', 'grnList', param);
        }
    });

    loadDropDownFromDatabase('/supplierAPI/search-suppliers-for-dropdown', "", 0, '#grnSearchBySupplier');
    $('#grnSearchBySupplier').trigger('change');
    $('#grnSearchBySupplier').change(function() {
        if ($(this).val() > 0 && selectedSupplierID != $(this).val()) {
            selectedSupplierID = $(this).val();
            var param = {supplierID: $(this).val()};
            getViewAndLoad('/api/grn/get-grns-by-search', 'grnList', param);
        }
    });

    $('#grnSearchClear').on('click', function() {
        getViewAndLoad('/api/grn/get-grns-by-search/' + getCurrPage(), 'grnList', {});
        $('#grnSearchSelector').selectpicker('val', 'searchBySupplier');
        $('#grnSearchByGRN').selectpicker('hide');
        $('#grnSearchBySupplier').selectpicker('show');
        $('#grnSearchBySupplier,#grnSearchByGRN').val('').trigger('change');
        selectedGrnID = selectedSupplierID = null;
    });

    $('#searchBySerialCode').on('click', function() {
        if ($('#grnSerialCodeSearch').val() == '') {
            p_notification(false, eb.getMessage('ERR_VIEWINV_COMMENTFIELD_FOR_SEARCH'));
        } else {
            var param = {searchString: $('#grnSerialCodeSearch').val()};
            getViewAndLoad('/api/grn/get-grns-by-serial-id-search', 'grnList', param);
        }

    });

    $('#resetSerchBySerialCode').on('click', function(){
        getViewAndLoad('/api/grn/get-grns-by-search/' + getCurrPage(), 'grnList', {});
        $('#grnSearchSelector').selectpicker('val', 'searchBySupplier');
        $('#grnSearchByGRN').selectpicker('hide');
        $('#grnSearchBySupplier').selectpicker('show');
        $('#grnSerialCodeSearch').val('');
        $('#grnSearchBySupplier,#grnSearchByGRN').val('').trigger('change');
        selectedGrnID = selectedSupplierID = null;
    });


    $('#searchPIBySerialCode').on('click', function() {
        if ($('#piSerialCodeSearch').val() == '') {
            p_notification(false, eb.getMessage('ERR_VIEWINV_COMMENTFIELD_FOR_SEARCH'));
        } else {
            var param = {searchString: $('#piSerialCodeSearch').val()};
            getViewAndLoad('/api/pi/searchPVBySerialCode', 'piList', param);
        }

    });

    $('#resetPISerchBySerialCode').on('click', function(){
        getViewAndLoad('/api/pi/searchPVByPVID/' + getCurrPage(), 'piList', {});
        $('#piSerialCodeSearch').val('');
        
    });

    $('form.po-search-form').submit(function(e) {
        e.stopPropagation();
        var param = {searchKey: $('#poSearchBox').val()};
        getViewAndLoad('/api/po/getPosForSearch', 'poList', param);
        return false;
    });

    $("form.po-search-form button.poReset").click(function(e) {
        $("#poSearchBox").val('');
        $("form.po-search-form").submit();
    });

    $(document).on('click', '.poChangeStatus', function(e) {
        e.preventDefault();
        $('#statusModal').modal('show');
        $('#statusValue').val($(this).data('status'));
        $('#statusModal').data('poID', this.id);
    });

    $(document).on('click', '.grnChangeStatus', function(e) {
        e.preventDefault();
        $('#statusModal').modal('show');
        $('#statusValue').val($(this).data('status'));
        $('#statusModal').data('grnID', this.id);
    });

    $('#statusChangeButton', '#statusModal').on('click', function() {
        var entity = $('#statusModal').data('entity');
        var url = '';
        var statusCode = $('#statusValue', '#statusModal').val();
        if (entity == 'po') {
            var poID = $('#statusModal').data('poID');
            eb.ajax({
                type: 'POST',
                url: BASE_URL + '/api/po/changePoStatus',
                data: {statusCode: statusCode, poID: poID},
                success: function(respond) {
                    if (respond.status == true) {
                        window.location.reload();
                    } else {
                        p_notification(respond.status, respond.msg);
                    }
                },
            });
        } else if (entity == 'grn') {
            var grnID = $('#statusModal').data('grnID');
            eb.ajax({
                type: 'POST',
                url: BASE_URL + '/api/grn/changeGrnStatus',
                data: {statusCode: statusCode, grnID: grnID},
                success: function(respond) {
                    if (respond.status == true) {
                        window.location.reload();
                    } else {
                        p_notification(respond.status, respond.msg);
                    }
                },
            });

        } else if (entity == 'purchase-requisition') {
            var poID = $('#statusModal').data('poID');
            eb.ajax({
                type: 'POST',
                url: BASE_URL + '/api/purchase-requistion/changePReqStatus',
                data: {statusCode: statusCode, poID: poID},
                success: function(respond) {
                    if (respond.status == true) {
                        window.location.reload();
                    } else {
                        p_notification(respond.status, respond.msg);
                    }
                },
            });
        }
    });

});