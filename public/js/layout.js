var openChat;

$(document).ready(function () {

    $('.side_bar_menu_call').on('click', function () {
        if ($('.side_bar').is(':visible') === false) {
            $('.side_bar').slideDown();
            $('.side_bar_menu_call').text('Hide Menu');
        } else if ($('.side_bar').is(':visible') === true) {
            $('.side_bar').slideUp();
            $('.side_bar_menu_call').text('Menu');
        }
    });

    //initialize the tool tip
    $('.tool').tooltip();


    //responsive issue fix
    var bottom_margin = "margin_bottom_normal";
    $(window).bind('resize ready', function () {
        //add bottom margin to search_bar in col-xs-12 view (sales-order-/create_sales_order/)
        ($(window).width() <= 768) ? $('#get-so-select:parent').addClass(bottom_margin) : $('#get-so-select:parent').removeClass(bottom_margin);
    });

    //narrow_window toggle_ment
    $('.navbar-default .navbar-toggle').on('click', function () {
        if (!$('.navbar-collapse').is(':visible') === true) {
            $('.navbar-default .navbar-toggle').css({
                '-webkit-transform': 'rotate3D(0,0,1,90deg)'
            });
        } else {
            $('.navbar-default .navbar-toggle').css({
                '-webkit-transform': 'rotate3D(0,0,1,0deg)'
            });
        }
    });

    //layout initialize the tool tip
    $('.dashboard').tooltip();

    $('.dashboard.help').click(function (e) {
        e.preventDefault();

        var $container = $('.help-container');

        $container.toggleClass('hidden');

        if ($container.is(':visible')) {
            var $iframe = $('<iframe />');
            $iframe.attr('src', HELP_TOOL_URL + '?tags=' + $container.data('tags'));
            $container.html($iframe);
        } else {
            $container.find('iframe').remove();
        }
    });

    // pop up user feedback form
    $("#feedbackFrom").click(function () {
        var href = $(this).attr('href');
        $.get(href, function (data) {
            $("#feedbackModal").html(data);
            $.getScript('/assets/rateit/jquery.rateit.min.js');
            $.getScript('/js/user/feedback.js');
            $('#userFeedback').modal('show');
        });

        return false;
    });

    //add drop down icon for side bar link only if that link contain child links
    var current_link = $('.side_bar li.active');
    if ($(current_link).children('ul').find('li').length !== 0) {
        $('> a', current_link).append("<i class='fa fa-caret-down has_children'></i>");
    }

    //Sales -> payment
    $("#payMethod_1 .jsPaidAmount").removeClass('col-lg-5').addClass('col-lg-6');

    openChat = function () {
        $('.admin_dropdown .chat-link').click();
    };

    //side-bar toggle
    $('.sidbar-toggle-menu').click(function (e) {
        e.stopPropagation();
        $('.main_body').toggleClass('hide-side-bar');
        if ($('.main_body').hasClass('hide-side-bar')) {
            document.cookie = 'sidebarIsVisible=hidden;path=/';
        } else {
            document.cookie = 'sidebarIsVisible=visible;path=/';
        }
    });
});