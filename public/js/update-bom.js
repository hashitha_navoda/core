$(document).ready(function() {
    $('.add-exits-sub-item').on('click', function() {
        $(this).parents('tr').find('.sub-item-name, .sub-item-quo, .sub-item-price').attr('disabled', true);
        $(this).parents('tr').find('.add_button').addClass('hidden');

    });
    $('#back-bom-list').on('click', function() {
        window.history.back();
    });

});