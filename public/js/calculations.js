/**
 * @author Damith Thamra <damith@thinkcube.com>
 * This JS file contains all calculation related functions
 */

/**
 *@author Damith Thamara <damith@thinkcube.com>
 * @param float unitPrice
 * @param array ProductTaxes
 * @returns Tax Results
 * @requires TAXSTATUS and TAXES Globally
 */
function calculateItemCustomTax(unitPrice, ProductTaxes) {
    if (unitPrice == null) {
        unitPrice = 0;
    }
    var taxResults = {};
    var Ptaxes = {};
    var taxAmount = 0;
    if (TAXSTATUS == true) {
        for (var i in ProductTaxes) {
            var taxID = ProductTaxes[i];
            var taxData = TAXES[taxID];
            if (taxData !== undefined && taxData.tS == 1) {
                if (taxData.tT == 'v') {
                    var thisTaxVal = (toFloat(unitPrice) * toFloat(taxData.tP) / toFloat(100)).toFixed(2);
                    taxAmount = toFloat(taxAmount) + toFloat(thisTaxVal);
                    Ptaxes[taxID] = {'tN': taxData.tN, 'tP': taxData.tP, 'tA': thisTaxVal, 'susTax':taxData.susTax};
                } else {
                    var simpleTaxVal = 0;
                    for (var j in taxData.sT) {
                        var tsT = taxData.sT[j];
                        var taxIncluded = ProductTaxes.indexOf(j);
                        if (taxIncluded != -1 && tsT.stS == 1) {
                            simpleTaxVal = toFloat(simpleTaxVal) + toFloat(((toFloat(unitPrice) * toFloat(tsT.stP))) / toFloat(100));
                        }
                    }
                    var cTVal = ((toFloat(toFloat(simpleTaxVal) + toFloat(unitPrice))) * toFloat(taxData.tP) / toFloat(100)).toFixed(2);
                    taxAmount = toFloat(taxAmount) + toFloat(cTVal);
                    Ptaxes[taxID] = {'tN': taxData.tN, 'tP': taxData.tP, 'tA': cTVal, 'susTax':taxData.susTax};
                }
            }
            taxResults['tTA'] = taxAmount.toFixed(2);
            taxResults['tL'] = Ptaxes;
        }
        return  taxResults;
    } else {
        return null;
    }
}

function calculateTaxFreeItemCost(fUnitPrice, fQty, fDiscount) {
    var thisDiscount = isNaN(fDiscount) ? 0 : fDiscount;
    var thisItemCost = (toFloat(fUnitPrice) * toFloat(fQty));
    if (thisDiscount > 0) {
        thisItemCost -= ((thisItemCost * toFloat(fDiscount)) / 100);
    }
    return thisItemCost;
}
/**
* use to calculate item cost for items that has value discounts
**/
function calculateTaxFreeItemCostByValueDiscount(fUnitPrice, fQty, fDiscount)
{
    var thisDiscount = isNaN(fDiscount) ? 0 : fDiscount;
    var unitPrice = toFloat(fUnitPrice) - toFloat(thisDiscount);
    var thisItemCost = (toFloat(unitPrice) * toFloat(fQty));
    
    return thisItemCost;
}

/**
 * This function is used to calulate the real unit price in incusive tax unitprice
 */
function calculateUnitePriceByInclusiveTax(unitPrice, ProductTaxes) {
    if (unitPrice == null) {
        unitPrice = 0;
    }
    var taxResults = {};
    var Ptaxes = {};
    var taxAmount = 0;
    if (TAXSTATUS == true) {
        for (var i in ProductTaxes) {
            var taxID = ProductTaxes[i];
            var taxData = TAXES[taxID];
            if (taxData !== undefined && taxData.tS == 1) {
                if (taxData.tT == 'v') {
                    var thisTaxVal = toFloat(taxData.tP) / toFloat(100);
                    taxAmount = toFloat(taxAmount) + toFloat(thisTaxVal);
                } else {
                    var simpleTaxVal = 0;
                    for (var j in taxData.sT) {
                        var tsT = taxData.sT[j];
                        var taxIncluded = ProductTaxes.indexOf(j);
                        if (taxIncluded != -1 && tsT.stS == 1) {
                            simpleTaxVal = toFloat(simpleTaxVal) + toFloat(toFloat(tsT.stP) / toFloat(100));
                        }
                    }
                    var cTVal = (toFloat(toFloat(simpleTaxVal) + toFloat(1))) * toFloat(taxData.tP) / toFloat(100);
                    taxAmount = toFloat(taxAmount) + toFloat(cTVal);
                }
            }
        }

        taxAmount = 1 + taxAmount;

        var realUnitprice = unitPrice / taxAmount;
        return  realUnitprice;
    } else {
        return null;
    }
}