var tmptotal;
var paymentID;
var supplierID;

var loadPaymentsPreview;
var ignoreBudgetLimitFlag = false; 
var printPaymentID = null;

$(document).ready(function () {
    var paymentId;
    if (!$('#filter-button').length) {
        return false;
    }

    var whichPayment = $('#whichPayment').val();
    $('.deli_row').hide();
    $('#deli_form_group').hide();
    $('#tax_td').hide();
    $('#pay-sup-search').selectpicker('hide');
    $('#view-div').hide();
    $('#supdiv').hide();
    $('#back-pay-cust').hide();
    $('#button-print').hide();
    $('#button-email').hide();
    $('#button-print').on('click', function () {
        printdiv('view-div');
    });
    $('#back-pay-cust').on('click', function () {
        $('#view-div').hide();
        $('#supdiv').show();
        $('#back-pay-cust').hide();
        $('#button-print').hide();
    });

    $('#pay-search-select').on('change', function () {
        if ($(this).val() == 'Payments No') {
            $('#pay-sup-search').selectpicker('hide');
            $('#pay-search').selectpicker('show');
            $('#pay-sup-search').val('');
            $('#pay-sup-search').selectpicker('render');
            $('#view-div').hide();
            $('#supdiv').hide();
            $('#back-pay-cust').hide();
            $('#button-print').hide();
            $('#paymentsview').show();
            $('#button-email').hide();
        } else if ($(this).val() == 'Supplier Name') {
            $('#pay-search').selectpicker('hide');
            $('#pay-sup-search').selectpicker('show');
            $('#pay-search').val('');
            $('#pay-search').selectpicker('render');
            $('#view-div').hide();
            $('#supdiv').hide();
            $('#back-pay-cust').hide();
            $('#button-print').hide();
            $('#paymentsview').show();
            $('#button-email').hide();
        }
    });

    loadDropDownFromDatabase('/supplierPaymentsAPI/search-supplier-payments-dropdown', "", 'Cheque', '#pay-search');
    $('#pay-search').selectpicker('refresh');

    $('#pay-search').on('change', function (e) {
        e.preventDefault();
        if ($(this).val() > 0 && paymentId != $(this).val()) {
            paymentId = $(this).val();
            eb.ajax({
                type: 'POST',
                url: BASE_URL + '/supplierPaymentsAPI/retriveCheques',
                data: {paymentId: paymentId},
                success: function(data) {
                    $('#paymentsview').html(data);
                }
            });
        }
    });

    loadDropDownFromDatabase('/supplierAPI/search-suppliers-for-dropdown', "", 0, '#pay-sup-search');
    $('#pay-sup-search').selectpicker('refresh');

    $('#pay-sup-search').on('change', function (e) {
        e.preventDefault();
        if ($(this).val() > 0 && supplierID != $(this).val()) {
            supplierID = $(this).val();
            eb.ajax({
                type: 'POST',
                url: BASE_URL + '/supplierPaymentsAPI/retriveChequesBySupplier',
                data: {supplierID: supplierID},
                success: function(data) {
                    $('#paymentsview').html(data);
                }
            });
        }
    });

    $('#clear-search').on('click', function () {
        window.location.reload();
    });

    function printdiv(divID) {
        var DocumentContainer = document.getElementById(divID);
        var WindowObject = window.open('', 'PrintWindow', 'width=750,height=650,top=50,left=50,toolbars=no,scrollbars=yes,status=no,resizable=yes');

        WindowObject.document.writeln('<!DOCTYPE html>');
        WindowObject.document.writeln('<html><head><title></title>');
        WindowObject.document.writeln('<link rel="stylesheet" type="text/css" href="/assets/bootstrap/css/bootstrap.min.css">');
        WindowObject.document.writeln('</head><body>');
        WindowObject.document.writeln(DocumentContainer.innerHTML);
        WindowObject.document.writeln('</body></html>');

        WindowObject.document.close();
        WindowObject.focus();
        WindowObject.print();
        WindowObject.close();

    }

    $('#filter-button').on('click', function () {
        if ($('#from-date').val() == '' || $('#to-date').val() == '') {
            p_notification(false, eb.getMessage('ERR_VIEWPAY_FILLDATA'));
        } else {
            var filterURL = BASE_URL + '/supplierPaymentsAPI/getSupplierChequesByFilter';
            var filterrequest = eb.post(filterURL, {
                'fromdate': $('#from-date').val() ,
                'todate': $('#to-date').val(),
            });
            filterrequest.done(function(data) {
                $('#paymentsview').html(data);
            });
        }
    });

    $('.print-cheque').on('click', function(event) {
        event.preventDefault();
        printPaymentID = $(this).attr('data-id');
        $('#printConfirmModal').modal('show');
    });

    $('#proceedPrint').on('click', function(event) {
        event.preventDefault();

        eb.ajax({
            type: 'POST',
            url: BASE_URL + '/supplierPaymentsAPI/printCheque',
            data: {paymentID: printPaymentID, comment: $('#print-message').val()},
            success: function(data) {
                if (data.status) {
                    $('#cheque_date').html(data.data.chequeDayFisrtDigit+'&nbsp;&nbsp'+data.data.chequeDaySecondDigit+'&nbsp;&nbsp;'+data.data.chequeMonthFisrtDigit+'&nbsp;&nbsp;'+data.data.chequeMonthSecondDigit+'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+data.data.chequeYearFisrtDigit+'&nbsp;&nbsp'+data.data.chequeYearSecondDigit);
                    $('#cheque_payee').html(data.data.supplier);
                    $('#cheque_amount').html(data.data.paymentAmount);
                    $('#amount_words').html(data.data.chequeValueInWords);
                    $('#printConfirmModal').modal('hide');

                    printCheque();
                } else {
                    p_notification(false, data.msg);
                    return false;                    
                }
            },
            async: false
        });
        window.location.reload();
    });

    function printContent(el) {
        var restorepage = document.body.innerHTML;
        var printcontent = document.getElementById(el).innerHTML;
        document.body.innerHTML = printcontent;
        window.print();
        document.body.innerHTML = restorepage;
    }

    function printCheque()
    {
        var mywindow = window.open('', 'PrintWindow', 'width=750,height=650,top=50,left=50,toolbars=no,scrollbars=yes,status=no,resizable=yes');
        mywindow.document.write('<html><head><title>' + document.title  + '</title>');
        mywindow.document.write('</head><body >');
        // mywindow.document.write(document.getElementById('cheque-div').innerHTML);
        mywindow.document.write(document.getElementById('printContent').innerHTML);
        mywindow.document.write('</body></html>');

        mywindow.document.close(); // necessary for IE >= 10
        mywindow.focus(); // necessary for IE >= 10*/

        mywindow.print();
        mywindow.close();
        return true;
    }

    function printChequeDiv() {
        var contents = document.getElementById('cheque-div').innerHTML;
        var frame1 = document.createElement('iframe');
        frame1.name = "frame1";
        frame1.style.position = "absolute";
        frame1.style.top = "-1000000px";
        document.body.appendChild(frame1);
        var frameDoc = frame1.contentWindow ? frame1.contentWindow : frame1.contentDocument.document ? frame1.contentDocument.document : frame1.contentDocument;
        frameDoc.document.open();
        frameDoc.document.write('<html><head><title>DIV Contents</title>');
        frameDoc.document.write('</head><body>');
        frameDoc.document.write(contents);
        frameDoc.document.write('</body></html>');
        frameDoc.document.close();
        setTimeout(function () {
            window.frames["frame1"].focus();
            window.frames["frame1"].print();
            document.body.removeChild(frame1);
        }, 500);
        return false;
    }

///////DatePicker\\\\\\\
    var nowTemp = new Date();
    var frmdate;
    var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);

    var checkin = $('#from-date').datepicker().on('changeDate', function (ev) {
        checkin.hide();
        frmdate = ev.date;
        $('#to-date')[0].focus();
    }).data('datepicker');
    var checkout = $('#to-date').datepicker({
        onRender: function (date) {
            return date.valueOf() < frmdate ? 'disabled' : '';
        }
    }).on('changeDate', function (ev) {
        checkout.hide();
    }).data('datepicker');
/////EndOFDatePicker\\\\\\\\\
});