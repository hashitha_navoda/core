var ignoreBudgetLimitFlag = false;
var dimensionData = {};
$(document).ready(function() {

    var dimensionArray = {};

	loadDropDownFromDatabase('/accounts-api/search-active-accounts-for-dropdown', "", "", '#pettyCashFloatFinanceAccountID');
	loadDropDownFromDatabase('/accounts-api/search-active-accounts-for-dropdown', "", "", '#pettyCashFloatIssueFinanceAccountID');

	var pettyCashFloatFinanceAccountID = $('#pettyCashFloatFinanceAccountID').data('id');
    if(pettyCashFloatFinanceAccountID!=''){
    	var pettyCashFloatFinanceAccountName = $('#pettyCashFloatFinanceAccountID').data('value');
    	var pettyCashFloatBalance = $('#pettyCashFloatBalance').data('value');
    	$('#pettyCashFloatFinanceAccountID').append("<option value='"+pettyCashFloatFinanceAccountID+"'>"+pettyCashFloatFinanceAccountName+"</option>")
    	$('#pettyCashFloatFinanceAccountID').val(pettyCashFloatFinanceAccountID).selectpicker('refresh');
    	$('#pettyCashFloatBalance').val(pettyCashFloatBalance);
    }

    var pettyCashFloatIssueFinanceAccountID = $('#pettyCashFloatIssueFinanceAccountID').data('id');
    if(pettyCashFloatIssueFinanceAccountID!=''){
    	var pettyCashFloatIssueFinanceAccountName = $('#pettyCashFloatIssueFinanceAccountID').data('value');
    	var pettyCashFloatIssueBalance = $('#pettyCashFloatIssueBalance').data('value');
    	$('#pettyCashFloatIssueFinanceAccountID').append("<option value='"+pettyCashFloatIssueFinanceAccountID+"'>"+pettyCashFloatIssueFinanceAccountName+"</option>")
    	$('#pettyCashFloatIssueFinanceAccountID').val(pettyCashFloatIssueFinanceAccountID).selectpicker('refresh');
    	$('#pettyCashFloatIssueBalance').val(pettyCashFloatIssueBalance);
    }

	var fAccountID = 0;
    $('#pettyCashFloatFinanceAccountID').on('change',function(){
    	if($(this).val() > 0 &&  $(this).val() != fAccountID ){
    		fAccountID = $(this).val();
    		getFinanceAccountsData(fAccountID ,'PCFFAID');
    	}
    });

    var issueFAccountID = 0;
    $('#pettyCashFloatIssueFinanceAccountID').on('change',function(){
    	if($(this).val() > 0 &&  $(this).val() != issueFAccountID ){
    		issueFAccountID = $(this).val();
    		getFinanceAccountsData(issueFAccountID ,'PCFIFAID');
    	}
    });

    function getFinanceAccountsData(fAccountID, type)
    {
    	eb.ajax({
    		url: BASE_URL + '/accounts-api/getFinanceAccountsByAccountsID',
    		method: 'post',
    		data: {financeAccountsID:fAccountID},
    		success: function(respond) {
    			if (respond.status) {
    				var financeAccountsData = respond.data.financeAccounts;
    				var financeAccountDebitAmount =  financeAccountsData.financeAccountsDebitAmount
    				var financeAccountCreditAmount = financeAccountsData.financeAccountsCreditAmount
    				if(type == "PCFFAID"){
    					$('#pettyCashFloatBalance').val((financeAccountDebitAmount - financeAccountCreditAmount).toFixed(2));
    				}else if(type == "PCFIFAID"){
    					$('#pettyCashFloatIssueBalance').val((financeAccountDebitAmount - financeAccountCreditAmount).toFixed(2));
    				}

    			}else{
    				p_notification(respond.status, respond.msg);
    			}
    		}
    	});
    }

    $('#pettyCashFloatDate').datetimepicker({
        format: 'yyyy-mm-dd',
        minView: 4,
        todayBtn: true,
        autoclose: true,
        startDate: ((U_DATE_R_OVERRIDE)?'': new Date()),
    });

    $('#dimensionView').on('click',function(){
        clearDimensionModal();
        // var journalEntryCode = $('#journalEntryCode').val();

        if (!$.isEmptyObject(dimensionData)) {
            $.each(dimensionData['data'], function(index, val) {
                dimensionArray[index] = val;                
            });
            $.each(dimensionArray, function(index, value) {
                var newTrID = 'tr_' + value.dimensionTypeId;
                var clonedRow = $($('#dimensionPreSample').clone()).attr('id', newTrID).addClass('addedDimensions');
                $("input[name='dimensionType']", clonedRow).val(value.dimensionTypeTxt);
                $("input[name='dimensionValue']", clonedRow).val(value.dimensionValueTxt);
                clonedRow.removeClass('hidden');
                clonedRow.insertBefore('#dimensionPreSample');
                clearAddedNewDimensionRow();
            });
        }
        $('#addDimensionModal').modal('show');

    });


    $('#add_dimension').on('click', function(e) {
        e.preventDefault();
        //if plus button is clicked then disabled the button
        //if button is clicked more than one, then it does not continue
        if ($(this).is(':disabled')) {
            return false;
        } else {
            $(this).attr('disabled', true);
            var $thisRow = $(this).parents('tr');
            if ($('.dimenisonchange').val() == 0 || $('.dimenisonchange').val() == '' || $('.dimenisonchange').val() == null) {
                p_notification(false, eb.getMessage('ERR_DIMENSION_SELCET'));
                $(this).attr('disabled', false);
            } else {

                if ($('.dimensionVal').val() == 0 || $('.dimensionVal').val() == '' || $('.dimensionVal').val() == null) {
                    p_notification(false, eb.getMessage('ERR_DIMENSION_VALUE_SELCET'));
                    $(this).attr('disabled', false);
                    return;
                }

                if (dimensionArray[dimensionTypeID]) {
                    p_notification(false, eb.getMessage('ERR_DIMENSION_ALREADY_INSERTED'));
                    $('.dimenisonchange').val('');
                    $('.dimenisonchange').selectpicker('render');
                    $('.dimenisonchange').trigger('change');
                } else {
                    addNewDimensionRow($(this).parents('tr'));
                }
                $(this).attr('disabled', false);
            }
        }
    });

    function addNewDimensionRow(thisRow) {

        var dimensionTypeId = $('.dimenisonchange', thisRow).val();
        var dimensionTypeTxt = $('.dimenisonchange', thisRow).find("option:selected").text();

        switch (dimensionTypeID) {
            case "job":
                var dimensionValueTxt = $('.dimensionJobNo', thisRow).find("option:selected").text();
                var dimensionValueId = $('.dimensionJobNo', thisRow).val();
                break;
            case "project":
                var dimensionValueTxt = $('.dimensionProjectNo', thisRow).find("option:selected").text();
                var dimensionValueId = $('.dimensionProjectNo', thisRow).val();
                break;
           default:
                var dimensionValueTxt = $('.dimensionVal', thisRow).find("option:selected").text();
                var dimensionValueId = $('.dimensionVal', thisRow).val();
                break;
        }
       
        var dimensionDuplicateCheck = false;
        if (!$.isEmptyObject(dimensionArray)) {
            $.each(dimensionArray, function(index, value) {
                if (value.dimensionTypeId == dimensionTypeId) {
                    dimensionDuplicateCheck = true;
                }
            });   
        }
       
        if (dimensionDuplicateCheck) {
            p_notification(false, eb.getMessage("ERR_DIMENSION_ALREADY_INSERTED"));
            return false;
        }

        var newTrID = 'tr_' + dimensionTypeId;
        var clonedRow = $($('#dimensionPreSample').clone()).attr('id', newTrID).addClass('addedDimensions');
        $("input[name='dimensionType']", clonedRow).val(dimensionTypeTxt);
        $("input[name='dimensionValue']", clonedRow).val(dimensionValueTxt);
        clonedRow.removeClass('hidden');
        clonedRow.insertBefore('#dimensionPreSample');
        dimensionArray[dimensionTypeId] = new dimension(dimensionTypeId, dimensionValueId,dimensionTypeTxt , dimensionValueTxt);
        clearAddedNewDimensionRow();
    }


    function dimension(dimensionTypeId, dimensionValueId, dimensionTypeTxt, dimensionValueTxt) {
        this.dimensionTypeId = dimensionTypeId;
        this.dimensionValueId = dimensionValueId;
        this.dimensionTypeTxt = dimensionTypeTxt;
        this.dimensionValueTxt = dimensionValueTxt;
    }

    function clearAddedNewDimensionRow() {
        $('.dimenisonchange').val('');
        $('.dimenisonchange').selectpicker('render');
        $('.dimenisonchange').trigger('change');
    }

    $('.addDimensions').on('click', function(e) {
        e.preventDefault();

        // validate products before closing modal
        if (!dimensionModalValidate(e)) {
            e.stopPropagation();
            return false;
        } else {
            $('#addDimensionModal').modal('hide');
        }
    });

    function dimensionModalValidate(e) {
        // var PFCode = $('#journalEntryCode').val();
        if ($.isEmptyObject(dimensionArray)) {
            dimensionData = {};
            return true;   
        } else {
            var dimensionSingleData = {};
            $.each(dimensionArray, function(index, val) {
                dimensionSingleData[index] = val;
            });
            dimensionData['data'] = dimensionSingleData;
            return true;
        }
    }

    $("#addDimensionModal #dimensionTable").on('click', '.deleteDimensionType', function(e) {
        e.preventDefault();
        var deleteDTrID = $(this).closest('tr').attr('id');
        var deleteDID = deleteDTrID.split('tr_')[1].trim();
        bootbox.confirm('Are you sure you want to remove this dimension ?', function(result) {
            if (result == true) {
                delete dimensionArray[deleteDID];
                $('#' + deleteDTrID).remove();
            }
        });
    });

    loadDropDownFromDatabase('/job-api/searchJobsForDropdown', "", 0, '.dimensionJobNo');
    $('.dimensionJobNo').selectpicker('refresh');
    $('.dimensionJobNo').on('change', function() {
        var dimensionValue = $(this).val();
    });

    loadDropDownFromDatabase('/api/project/searchProjectsForDropdown', "", 0, '.dimensionProjectNo');
    $('.dimensionProjectNo').selectpicker('refresh');
    $('.dimensionProjectNo').on('change', function() {
        var dimensionValue = $(this).val();
    });

    // dimension modal
    function clearDimensionModal()
    {
        $("#addDimensionModal #dimensionTable tbody .addedDimensions").remove();
        dimensionArray = {};
    }

    $('.dimenisonchange').on('change', function(e) {
        dimensionTypeID = this.value;
        getDimensionValueListByDimensionType(dimensionTypeID, function() {
        });
    });

    function getDimensionValueListByDimensionType(dimensionTypeID, callback) {
        switch (dimensionTypeID) {
            case "job":
                $('.dimensionVal').addClass('hidden');
                $('.dimensionProjectNo').addClass('hidden');
                $('.dimensionJobNo').removeClass('hidden');
                break;
            case "project":
                $('.dimensionVal').addClass('hidden');
                $('.dimensionProjectNo').removeClass('hidden');
                $('.dimensionJobNo').addClass('hidden');
                break;
           default:
                $('.dimensionVal').removeClass('hidden');
                $('.dimensionProjectNo').addClass('hidden');
                $('.dimensionJobNo').addClass('hidden');
                $.ajax({
                    type: 'POST',
                    url: BASE_URL + '/dimension-api/getRelatedDimensionValues',
                    data: {dimensionId: dimensionTypeID},
                    success: function(respond) {
                        //empty options
                        $('#dimensionVal').empty();
                        $('#dimensionVal').append($("<option>", {value: '', html: 'Select dimension value'}));
                        $(respond.data).each(function(index, dimValue) {
                            $.each(dimValue, function(ind, val) {
                                $('#dimensionVal').append($("<option>", {value: ind, html: val.value}));
                            });
                        });
                        callback();
                    }
                });
                break;
        }
    }

    $('#create-petty-cash-float-form').submit(function(e) {
        e.preventDefault();
        var form_data = {
            pettyCashFloatAmount: $('#pettyCashFloatAmount').val(),
            pettyCashFloatIssueBalance:$('#pettyCashFloatIssueBalance').val(),
            pettyCashFloatDate: $('#pettyCashFloatDate').val(),
            pettyCashFloatFinanceAccountID: $('#pettyCashFloatFinanceAccountID').val(),
            pettyCashFloatIssueFinanceAccountID: $('#pettyCashFloatIssueFinanceAccountID').val(),
            ignoreBudgetLimit : ignoreBudgetLimitFlag,
            dimensionData: dimensionData,

        };

//      validate Form
      	if(form_data.pettyCashFloatFinanceAccountID == ''){
         	p_notification(false, eb.getMessage('ERR_PLEASE_SELECT_PETTY_CASH_ACCOUNT'));
            $('#pettyCashFloatFinanceAccountID').focus();
            return false;
        }else if (form_data.pettyCashFloatAmount == '' || form_data.pettyCashFloatAmount == null) {
            $('#pettyCashFloatAmount').focus();
            p_notification(false, eb.getMessage('ERR_PETTY_CASH_FLOAT_AMOUNT_EMPTY'));
            return false;
        } else if (isNaN(form_data.pettyCashFloatAmount) || form_data.pettyCashFloatAmount <= 0) {
            p_notification(false, eb.getMessage('ERR_PETTY_CASH_FLOAT_AMOUNT_VALID'));
            $('#pettyCashFloatAmount').focus();
            return false;
        }  else if (form_data.pettyCashFloatDate == '') {
            p_notification(false, eb.getMessage('ERR_PETTYCASH_CASH_FLOAT_DATE_EMPTY'));
            $('#pettyCashFloatDate').focus();
            return false;
        } else  if(form_data.pettyCashFloatIssueFinanceAccountID == ''){
        	p_notification(false, eb.getMessage('ERR_PLEASE_SELECT_PETTY_CASH_ISSUE_ACCOUNT'));
            $('#pettyCashFloatIssueFinanceAccountID').focus();
            return false;
        }

        savePettyCashFloat(form_data);
        
        return false;
    });


    function savePettyCashFloat(form_data)
    {
        eb.ajax({
            url: BASE_URL + '/api/petty-cash-float-api/save',
            method: 'post',
            data: form_data,
            success: function(respond) {
                if (respond.status) {
                    p_notification(respond.status, respond.msg);
                    window.location.reload();
                } else {
                    if (respond.data == "NotifyBudgetLimit") {
                        bootbox.confirm(respond.msg+' ,Are you sure you want to save ?', function(result) {
                            if (result == true) {
                                ignoreBudgetLimitFlag = true;
                                form_data.ignoreBudgetLimit = true
                                savePettyCashFloat(form_data);
                            } else {
                                setTimeout(function(){ 
                                    location.reload();
                                }, 3000);
                            }
                        });
                    } else {
                        p_notification(respond.status, respond.msg);
                    }
                }
            }
        });
    }
});