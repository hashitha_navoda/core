var ignoreBudgetLimitFlag = false;
var dimensionData = {};
$(document).ready(function() {
    var dimensionArray = {};
    var pettyCashID;
	loadDropDownFromDatabase('/accounts-api/search-active-accounts-for-dropdown', "", "", '#pettyCashVoucherFinanceAccountID');
    loadDropDownFromDatabase('/api/expense-type/get-expense-type-with-finance-accounts-by-search', "", 0, '.ex-type-selectbox');

    loadDropDownFromDatabase('/api/petty-cash-voucher/search-petty-cash-voucher-dropdown', "", "", '#petty-search');
    $('#petty-search').selectpicker('refresh');

    $('#petty-search').on('change', function () {
        if ($(this).val() > 0 && $(this).val() != pettyCashID) {
            pettyCashID = $(this).val();
            var getpettybyurl = '/api/petty-cash-voucher/retrive-petty-cash-voucher';
            var requestquobycust = eb.post(getpettybyurl, {
                PettyCashID: pettyCashID,
                PettyCashNo : $(this).find("option:selected").text(),
                fromdate: null,
                todate: null
            });
            requestquobycust.done(function (retdata) {
                $('#petty-cash-voucher-list').html(retdata);
            });
        }
    });

    $('#filter-button').on('click', function () {
        if ($('#from-date').val() == '' || $('#to-date').val() == '') {
            p_notification(false, eb.getMessage('ERR_PETTY_CASH_VOUCHER_DATE_RANGE'));
        } else if ($('#to-date').val() < $('#from-date').val()) {
            p_notification(false, eb.getMessage('ERR_DATE_RANGE'));
        } else {
            var pettycashfilter = BASE_URL + '/api/petty-cash-voucher/retrive-petty-cash-voucher';
            var filterrequest = eb.post(pettycashfilter, {
                PettyCashID: null,
                PettyCashNo : null,
                fromdate: $('#from-date').val(),
                todate: $('#to-date').val(),
            });
            filterrequest.done(function (retdata) {
                $('#petty-cash-voucher-list').html(retdata);
            });
        }
    });

    $('#clear-search').on('click', function () {
        var requestquobycust = eb.post('/petty-cash-voucher/get-petty-cash-voucher-list');
        requestquobycust.done(function (retdata) {
            if (retdata.status == false) {
                p_notification(false, retdata.msg);
            } else {
                $('#petty-cash-voucher-list').html(retdata);
                $('#from-date,#to-date').val('');
                $("#petty-search").val('').trigger('change');
                $('#petty-search').empty().selectpicker('refresh');
                pettyCashID=null;
            }

        });
    });

	var pettyCashVoucherFinanceAccountID = $('#pettyCashVoucherFinanceAccountID').data('id');
    if(pettyCashVoucherFinanceAccountID!=''){
    	var pettyCashVoucherFinanceAccountName = $('#pettyCashVoucherFinanceAccountID').data('value');
    	var pettyCashFloatBalance = $('#pettyCashFloatBalance').data('value');
    	$('#pettyCashVoucherFinanceAccountID').append("<option value='"+pettyCashVoucherFinanceAccountID+"'>"+pettyCashVoucherFinanceAccountName+"</option>")
    	$('#pettyCashVoucherFinanceAccountID').val(pettyCashVoucherFinanceAccountID).selectpicker('refresh');
    	$('#pettyCashFloatBalance').val(pettyCashFloatBalance);
    }

	var fAccountID = 0;
    $('#pettyCashVoucherFinanceAccountID').on('change',function(){
    	if($(this).val() > 0 &&  $(this).val() != fAccountID ){
    		fAccountID = $(this).val();
    		getFinanceAccountsData(fAccountID);
    	}
    });

    function getFinanceAccountsData(fAccountID)
    {
    	eb.ajax({
    		url: BASE_URL + '/accounts-api/getFinanceAccountsByAccountsID',
    		method: 'post',
    		data: {financeAccountsID:fAccountID},
    		success: function(respond) {
    			if (respond.status) {
    				var financeAccountsData = respond.data.financeAccounts;
    				var financeAccountDebitAmount =  financeAccountsData.financeAccountsDebitAmount
    				var financeAccountCreditAmount = financeAccountsData.financeAccountsCreditAmount
    				$('#pettyCashFloatBalance').val((financeAccountDebitAmount - financeAccountCreditAmount).toFixed(2));
    			}else{
    				p_notification(respond.status, respond.msg);
    			}
    		}
    	});
    }

    $('#pettyCashVoucherDate').datetimepicker({
        format: 'yyyy-mm-dd',
        minView: 4,
        todayBtn: true,
        autoclose: true,
        startDate: ((U_DATE_R_OVERRIDE)?'': new Date()),
    });

    $('#from-date').datetimepicker({
        minView: 4,
        todayBtn: true,
        autoclose: true,
        startDate: ((U_DATE_R_OVERRIDE)?'': new Date()),
    });

    $('#to-date').datetimepicker({
        minView: 4,
        todayBtn: true,
        autoclose: true,
        startDate: ((U_DATE_R_OVERRIDE)?'': new Date()),
    });


    $('#pettyCashVoucherType').on('change', function() {
        if ($(this).val() == 1) {
            $('#petty-cash-voucher-selection').addClass('hidden');
            $('#expense-type-selection').removeClass('hidden');
        } else {
            $('#expense-type-selection').addClass('hidden');
            $('#petty-cash-voucher-selection').removeClass('hidden');
        }
    });

    $('#create-petty-cash-voucher-form').submit(function(e) {
        e.preventDefault();
        var form_inputs = {
            pettyCashVoucherAmount: $('#pettyCashVoucherAmount').val(),
            pettyCashVoucherDate: $('#pettyCashVoucherDate').val(),
            pettyCashVoucherType: $('#pettyCashVoucherType').val(),
            userID: $('#userID').val(),
            pettyCashExpenseTypeID: $('.ex-type-selectbox').val(),
            outgoingPettyCashVoucherID: $('#outgoingPettyCashVoucherID').val(),
            pettyCashVoucherFinanceAccountID: $('#pettyCashVoucherFinanceAccountID').val(),
            pettyCashFloatBalance: $('#pettyCashFloatBalance').val(),
            pettyCashVoucherComment: $('#pettyCashVoucherComment').val(),
            ignoreBudgetLimit: ignoreBudgetLimitFlag,
            dimensionData: dimensionData,

        };

//      validations
        if (formValidation(form_inputs)) {
            savePettyCashVoucher(form_inputs);
            return false;
        }
    });

    function savePettyCashVoucher(form_inputs)
    {
        eb.ajax({
            url: BASE_URL + '/api/petty-cash-voucher/save',
            type: 'POST',
            data: form_inputs,
            success: function(respond) {
                if (!respond.status) {
                    if (respond.data == "NotifyBudgetLimit") {
                        bootbox.confirm(respond.msg+' ,Are you sure you want to save ?', function(result) {
                            if (result == true) {
                                ignoreBudgetLimitFlag = true;
                                form_inputs.ignoreBudgetLimit = true;
                                savePettyCashVoucher(form_inputs);
                            } else {
                                setTimeout(function(){ 
                                    location.reload();
                                }, 3000);
                            }
                        });
                    } else {
                        p_notification(respond.status, respond.msg);
                    }
                } else {
                    window.location.reload();
                }
            }
        });
    }

    $('#petty-cash-voucher-list').on('click', 'a.delete', function(e) {
        e.preventDefault();
        var voucherID = $(this).parents('tr').attr('data-pettyCashVoucherID');
        bootbox.confirm("Are You sure to delete this voucher?", function(result) {
            if (result === true) {
                eb.ajax({
                    url: BASE_URL + '/api/petty-cash-voucher/delete',
                    method: 'POST',
                    data: {voucherID: voucherID},
                    success: function(respond) {
                        p_notification(respond.status, respond.msg);
                        if (respond.status) {
                            $('#petty-cash-voucher-list').html(respond.html);
                        }
                    }

                });
            }
        });
        return false;

    });

    $('#dimensionView').on('click',function(){
        clearDimensionModal();
        var pettyVoucherCode = $('#pettyCashVoucherNo').val().trim();

        if (!$.isEmptyObject(dimensionData)) {
            $.each(dimensionData[pettyVoucherCode], function(index, val) {
                dimensionArray[index] = val;                
            });
            $.each(dimensionArray, function(index, value) {
                var newTrID = 'tr_' + value.dimensionTypeId;
                var clonedRow = $($('#dimensionPreSample').clone()).attr('id', newTrID).addClass('addedDimensions');
                $("input[name='dimensionType']", clonedRow).val(value.dimensionTypeTxt);
                $("input[name='dimensionValue']", clonedRow).val(value.dimensionValueTxt);
                clonedRow.removeClass('hidden');
                clonedRow.insertBefore('#dimensionPreSample');
                clearAddedNewDimensionRow();
            });
        }
        $('#addDimensionModal').modal('show');

    });


    $('#add_dimension').on('click', function(e) {
        e.preventDefault();
        //if plus button is clicked then disabled the button
        //if button is clicked more than one, then it does not continue
        if ($(this).is(':disabled')) {
            return false;
        } else {
            $(this).attr('disabled', true);
            var $thisRow = $(this).parents('tr');
            if ($('.dimenisonchange').val() == 0 || $('.dimenisonchange').val() == '' || $('.dimenisonchange').val() == null) {
                p_notification(false, eb.getMessage('ERR_DIMENSION_SELCET'));
                $(this).attr('disabled', false);
            } else {

                if ($('.dimensionVal').val() == 0 || $('.dimensionVal').val() == '' || $('.dimensionVal').val() == null) {
                    p_notification(false, eb.getMessage('ERR_DIMENSION_VALUE_SELCET'));
                    $(this).attr('disabled', false);
                    return;
                }

                if (dimensionArray[dimensionTypeID]) {
                    p_notification(false, eb.getMessage('ERR_DIMENSION_ALREADY_INSERTED'));
                    $('.dimenisonchange').val('');
                    $('.dimenisonchange').selectpicker('render');
                    $('.dimenisonchange').trigger('change');
                } else {
                    addNewDimensionRow($(this).parents('tr'));
                }
                $(this).attr('disabled', false);
            }
        }
    });

    function addNewDimensionRow(thisRow) {

        var dimensionTypeId = $('.dimenisonchange', thisRow).val();
        var dimensionTypeTxt = $('.dimenisonchange', thisRow).find("option:selected").text();

        switch (dimensionTypeID) {
            case "job":
                var dimensionValueTxt = $('.dimensionJobNo', thisRow).find("option:selected").text();
                var dimensionValueId = $('.dimensionJobNo', thisRow).val();
                break;
            case "project":
                var dimensionValueTxt = $('.dimensionProjectNo', thisRow).find("option:selected").text();
                var dimensionValueId = $('.dimensionProjectNo', thisRow).val();
                break;
           default:
                var dimensionValueTxt = $('.dimensionVal', thisRow).find("option:selected").text();
                var dimensionValueId = $('.dimensionVal', thisRow).val();
                break;
        }
       
        var dimensionDuplicateCheck = false;
        if (!$.isEmptyObject(dimensionArray)) {
            $.each(dimensionArray, function(index, value) {
                if (value.dimensionTypeId == dimensionTypeId) {
                    dimensionDuplicateCheck = true;
                }
            });   
        }
       
        if (dimensionDuplicateCheck) {
            p_notification(false, eb.getMessage("ERR_DIMENSION_ALREADY_INSERTED"));
            return false;
        }

        var newTrID = 'tr_' + dimensionTypeId;
        var clonedRow = $($('#dimensionPreSample').clone()).attr('id', newTrID).addClass('addedDimensions');
        $("input[name='dimensionType']", clonedRow).val(dimensionTypeTxt);
        $("input[name='dimensionValue']", clonedRow).val(dimensionValueTxt);
        clonedRow.removeClass('hidden');
        clonedRow.insertBefore('#dimensionPreSample');
        dimensionArray[dimensionTypeId] = new dimension(dimensionTypeId, dimensionValueId,dimensionTypeTxt , dimensionValueTxt);
        clearAddedNewDimensionRow();
    }


    function dimension(dimensionTypeId, dimensionValueId, dimensionTypeTxt, dimensionValueTxt) {
        this.dimensionTypeId = dimensionTypeId;
        this.dimensionValueId = dimensionValueId;
        this.dimensionTypeTxt = dimensionTypeTxt;
        this.dimensionValueTxt = dimensionValueTxt;
    }

    function clearAddedNewDimensionRow() {
        $('.dimenisonchange').val('');
        $('.dimenisonchange').selectpicker('render');
        $('.dimenisonchange').trigger('change');
    }

    $('.addDimensions').on('click', function(e) {
        e.preventDefault();

        // validate products before closing modal
        if (!dimensionModalValidate(e)) {
            e.stopPropagation();
            return false;
        } else {
            $('#addDimensionModal').modal('hide');
        }
    });

    function dimensionModalValidate(e) {
        var pettyVoucherCode = $('#pettyCashVoucherNo').val().trim();
        if ($.isEmptyObject(dimensionArray)) {
            dimensionData = {};
            return true;   
        } else {
            var dimensionSingleData = {};
            $.each(dimensionArray, function(index, val) {
                dimensionSingleData[index] = val;
            });
            dimensionData[pettyVoucherCode] = dimensionSingleData;
            return true;
        }
    }

    $("#addDimensionModal #dimensionTable").on('click', '.deleteDimensionType', function(e) {
        e.preventDefault();
        var deleteDTrID = $(this).closest('tr').attr('id');
        var deleteDID = deleteDTrID.split('tr_')[1].trim();
        bootbox.confirm('Are you sure you want to remove this dimension ?', function(result) {
            if (result == true) {
                delete dimensionArray[deleteDID];
                $('#' + deleteDTrID).remove();
            }
        });
    });

    loadDropDownFromDatabase('/job-api/searchJobsForDropdown', "", 0, '.dimensionJobNo');
    $('.dimensionJobNo').selectpicker('refresh');
    $('.dimensionJobNo').on('change', function() {
        var dimensionValue = $(this).val();
    });

    loadDropDownFromDatabase('/api/project/searchProjectsForDropdown', "", 0, '.dimensionProjectNo');
    $('.dimensionProjectNo').selectpicker('refresh');
    $('.dimensionProjectNo').on('change', function() {
        var dimensionValue = $(this).val();
    });

    // dimension modal
    function clearDimensionModal()
    {
        $("#addDimensionModal #dimensionTable tbody .addedDimensions").remove();
        dimensionArray = {};
    }

    $('.dimenisonchange').on('change', function(e) {
        dimensionTypeID = this.value;
        getDimensionValueListByDimensionType(dimensionTypeID, function() {
        });
    });

    function getDimensionValueListByDimensionType(dimensionTypeID, callback) {
        switch (dimensionTypeID) {
            case "job":
                $('.dimensionVal').addClass('hidden');
                $('.dimensionProjectNo').addClass('hidden');
                $('.dimensionJobNo').removeClass('hidden');
                break;
            case "project":
                $('.dimensionVal').addClass('hidden');
                $('.dimensionProjectNo').removeClass('hidden');
                $('.dimensionJobNo').addClass('hidden');
                break;
           default:
                $('.dimensionVal').removeClass('hidden');
                $('.dimensionProjectNo').addClass('hidden');
                $('.dimensionJobNo').addClass('hidden');
                $.ajax({
                    type: 'POST',
                    url: BASE_URL + '/dimension-api/getRelatedDimensionValues',
                    data: {dimensionId: dimensionTypeID},
                    success: function(respond) {
                        //empty options
                        $('#dimensionVal').empty();
                        $('#dimensionVal').append($("<option>", {value: '', html: 'Select dimension value'}));
                        $(respond.data).each(function(index, dimValue) {
                            $.each(dimValue, function(ind, val) {
                                $('#dimensionVal').append($("<option>", {value: ind, html: val.value}));
                            });
                        });
                        callback();
                    }
                });
                break;
        }
    }


    function formValidation(inputs) {
        var amount = inputs.pettyCashVoucherAmount;
        var date = inputs.pettyCashVoucherDate;
        var type = inputs.pettyCashVoucherType;
        var pettyCashExpenseTypeID = inputs.pettyCashExpenseTypeID;
        var outgoingvoucherID = inputs.outgoingPettyCashVoucherID;
        var pettyCashVoucherFinanceAccountID = inputs.pettyCashVoucherFinanceAccountID;
        var pettyCashFloatBalance = inputs.pettyCashFloatBalance;

        if(pettyCashVoucherFinanceAccountID == '' || pettyCashVoucherFinanceAccountID == null){
        	$('#pettyCashVoucherFinanceAccountID').focus();
            p_notification(false, eb.getMessage('ERR_PETTY_CASH_VOUCHER_EMPTY_ACCOUNT'));
            return false;
        }else if (amount == '' || amount == null) {
            $('#pettyCashVoucherAmount').focus();
            p_notification(false, eb.getMessage('ERR_PETTY_CASH_VOUCHER_EMPTY_AMOUNT'));
            return false;
        } else if (isNaN(amount)) {
            $('#pettyCashVoucherAmount').focus();
            p_notification(false, eb.getMessage('ERR_PETTY_CASH_VOUCHER_VALID_AMOUNT'));
            return false;
        } else if (parseFloat(amount) <= 0) {
            $('#pettyCashVoucherAmount').focus();
            p_notification(false, eb.getMessage('ERR_PETTY_CASH_VOUCHER_NEGATVE_AMOUNT'));
            return false;
        }
        else if (date == '' || date == null) {
            $('#pettyCashVoucherDate').focus();
            p_notification(false, eb.getMessage('ERR_PETTY_CASH_VOUCHER_EMPTY_DATE'));
            return false;
        } else if (type == '1' && (pettyCashExpenseTypeID == '' || pettyCashExpenseTypeID == null)) {
            $('#pettyCashExpenseTypeID').focus();
            p_notification(false, eb.getMessage('ERR_PETTY_CASH_VOUCHER_EMPTY_EXPENSE'));
            return false;
        } else if (type == '2' && (outgoingvoucherID == '' || outgoingvoucherID == null)) {
            $('#outgoingPettyCashVoucherID').focus();
            p_notification(false, eb.getMessage('ERR_PETTY_CASH_VOUCHER_EMPTY_VOUCHER'));
            return false;
        } else if(parseFloat(pettyCashFloatBalance) < parseFloat(amount)){
        	$('#pettyCashVoucherAmount').focus();
            p_notification(false, eb.getMessage('ERR_PETTY_CASH_VOUCHER_AMOUNT_NOT_ENOUGH'));
            return false;
        }else{
            return true;
        }
    }
});