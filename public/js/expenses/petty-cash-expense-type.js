$(document).ready(function() {

	loadDropDownFromDatabase('/accounts-api/search-active-accounts-for-dropdown', "", "", '#pettyCashExpenseTypeAccountID');

    $('#petty-cash-expense-type-list').on('click', 'a.edit', function(e) {
        e.preventDefault();
        var pettyCashExpenseTypeID = $(this).parents('tr').attr('data-pettyCashExpenseTypeID');
        eb.ajax({
            url: BASE_URL + '/api/petty-cash-expense-type/get-petty-cash-expense-type/' + getCurrPage(),
            method: 'post',
            data: {pettyCashExpenseTypeID: pettyCashExpenseTypeID},
            success: function(respond) {
                clearForm();
                if (respond.status) {
                    $(".category-form-container").addClass('update');
                    $("html, body").animate({scrollTop: $(".category-form-container").offset().top - 80});
                    setExpenseTypeFormDetails(respond.data);
                } else {
                    p_notification(false, '');
                }
            },
        });
        return false;
    });

    $('#petty-cash-expense-type-list').on('click', 'a.delete', function(e) {
        e.preventDefault();
        var pettyCashExpenseTypeID = $(this).parents('tr').attr('data-pettyCashExpenseTypeID');
        bootbox.confirm("Are you sure to delete this Expense Type?", function(result) {
            if (result === true) {
                eb.ajax({
                    url: BASE_URL + '/api/petty-cash-expense-type/delete-petty-cash-expense-type/' + getCurrPage(),
                    method: 'POST',
                    data: {pettyCashExpenseTypeID: pettyCashExpenseTypeID},
                    success: function(respond) {
                        p_notification(respond.status, respond.msg);
                        if (respond.status) {
                            $("#petty-cash-expense-type-list").html(respond.html);
                        }
                    }
                });
            }
        });

        return false;
    });

    $('#petty-cash-expense-type-list').on('click', 'a.status', function(e) {
        e.preventDefault();
        var pettyCashExpenseTypeID = $(this).parents('tr').attr('data-pettyCashExpenseTypeID');
        bootbox.confirm("Are you sure to change the status of this Expense Type? ", function(result) {
            if (result === true) {
                eb.ajax({
                    url: BASE_URL + '/api/petty-cash-expense-type/update-petty-cash-expense-type-status/' + getCurrPage(),
                    method: 'POST',
                    data: {pettyCashExpenseTypeID: pettyCashExpenseTypeID},
                    success: function(respond) {
                        p_notification(respond.status, respond.msg);
                        if (respond.status) {
                            $('#petty-cash-expense-type-list').html(respond.html);
                        }
                    }
                })
            }
        });
        return false;
    });

    $('#create-petty-cash-expense-type-form').submit(function(e) {
        var form_data = {
            pettyCashExpenseTypeName: $('#pettyCashExpenseTypeName').val().trim(),
            pettyCashExpenseTypeDescription: $('#pettyCashExpenseTypeDescription').val().trim(),
            pettyCashExpenseTypeMinAmount: $('#pettyCashExpenseTypeMinAmount').val().trim(),
            pettyCashExpenseTypeMaxAmount: $('#pettyCashExpenseTypeMaxAmount').val().trim(),
            pettyCashExpenseTypeAccountID: $('#pettyCashExpenseTypeAccountID').val(),
        };

        var submitted_btn = $("[type='submit']:visible", $(this)).parents('.button-set');
        var update = submitted_btn.hasClass('update');
        if (update) {
            form_data = $.extend(form_data, {pettyCashExpenseTypeID: $("[name='pettyCashExpenseTypeID']", this).val()});
        }

        var action = (update) ? 'update-expense-type' : 'save-expense-type';

        if (validateForm(form_data)) {
            eb.ajax({
                url: BASE_URL + '/api/petty-cash-expense-type/' + action + '/' + getCurrPage(),
                method: 'post',
                data: form_data,
                success: function(responod) {
                    p_notification(responod.status, responod.msg);
                    if (responod.status == true) {
                        clearForm();
                        $(".category-form-container").removeClass('update');
                        $("#petty-cash-expense-type-list").html(responod.html);
                    }
                }
            });
        }

        e.stopPropagation();
        return false;
    });

    $('#create-petty-cash-expense-type-form').on('reset', function(e) {
        e.preventDefault();
        clearForm();
        $(".category-form-container").removeClass('update');
        return false;
    });

    $('#petty-cash-expense-type-search-form').submit(function(e) {
        e.preventDefault();
        var searchKey = $('#petty-cash-expense-type-search-keyword').val();
        if (searchKey.trim() == '') {
            $('#petty-cash-expense-type-search-keyword').focus();
            return false;
        }
        eb.ajax({
            url: BASE_URL + '/api/petty-cash-expense-type/search-expense-type',
            method: 'POST',
            data: {searchKey: searchKey},
            success: function(respond) {
                if (respond.status) {
                    $('#petty-cash-expense-type-search-keyword').val('');
                    $("#petty-cash-expense-type-list").html(respond.html);
                } else {
                    p_notification(respond.status, respond.msg);
                }

            }
        });
        return false;
    });

    $('#petty-cash-expense-type-search-form').on('click', '.reset', function(e) {
        e.preventDefault();
        $('#petty-cash-expense-type-search-keyword').val('');
        eb.ajax({
            url: BASE_URL + '/api/petty-cash-expense-type/search-expense-type/' + getCurrPage(),
            method: 'POST',
            data: {searchKey: ''},
            success: function(respond) {
                if (respond.status) {
                    $('#petty-cash-expense-type-search-keyword').val('');
                    $("#petty-cash-expense-type-list").html(respond.html);
                } else {
                    p_notification(respond.status, respond.msg);
                }

            }
        });
        return false;
    });

    function clearForm() {
        $('#pettyCashExpenseTypeID').val('');
        $('#pettyCashExpenseTypeName').val('');
        $('#pettyCashExpenseTypeDescription').val('');
        $('#pettyCashExpenseTypeMinAmount').val('');
        $('#pettyCashExpenseTypeMaxAmount').val('');

        $('#pettyCashExpenseTypeAccountID').find('option').remove();
        $('#pettyCashExpenseTypeAccountID').append("<option value=''>Select an Account</option>");
        $('#pettyCashExpenseTypeAccountID').val('').selectpicker('refresh');
    }

    function validateForm(expenseType) {
        var eTName = expenseType.pettyCashExpenseTypeName;
        var eTMinAmnt = expenseType.pettyCashExpenseTypeMinAmount;
        var eTMaxAmnt = expenseType.pettyCashExpenseTypeMaxAmount;


        if (eTName == '' || eTName == null) {
            p_notification(false, eb.getMessage('ERR_PETTY_CASH_EXPENSE_TYPE_NAME_REQUIRED'));
            $('#pettyCashExpenseTypeName').focus();
            return false;
        } else if (eTMinAmnt != '' && isNaN(eTMinAmnt)) {
            p_notification(false, eb.getMessage('ERR_PETTY_CASH_EXPENSE_TYPE_AMOUNT_VALID'));
            $('#pettyCashExpenseTypeMinAmount').focus();
            return false;
        } else if (eTMaxAmnt != '' && isNaN(eTMaxAmnt)) {
            p_notification(false, eb.getMessage('ERR_EXPENSE_TYPE_AMOUNT_VALID'));
            $('#pettyCashExpenseTypeMaxAmount').focus();
            return false;
        }  else if (parseFloat(eTMinAmnt) < 0) {
            p_notification(false, eb.getMessage('ERR_PETTY_CASH_EXPENSE_TYPE_AMOUNT_MIN'));
            $('#pettyCashExpenseTypeMinAmount').focus();
            return false;
        } else if (parseFloat(eTMaxAmnt) < 0) {
            p_notification(false, eb.getMessage('ERR_PETTY_CASH_EXPENSE_TYPE_AMOUNT_MAX'));
            $('#pettyCashExpenseTypeMaxAmount').focus();
            return false;
        }else if ((eTMinAmnt != '' && eTMaxAmnt != '') && (parseFloat(eTMinAmnt) > parseFloat(eTMaxAmnt))) {
            p_notification(false, eb.getMessage('ERR_PETTY_CASH_EXPENSE_TYPE_AMOUNT_DIFF_VALID'));
            $('#pettyCashExpenseTypeMaxAmount').focus();
            return false;
        }
        else {
            return true;
        }
    }

    function setExpenseTypeFormDetails(expenseType) {
        $('#pettyCashExpenseTypeName').val(expenseType.pettyCashExpenseTypeName);
        $('#pettyCashExpenseTypeDescription').val(expenseType.pettyCashExpenseTypeDescription);
        $('#pettyCashExpenseTypeMinAmount').val(Number(expenseType.pettyCashExpenseTypeMinAmount).toFixed(2));
        $('#pettyCashExpenseTypeMaxAmount').val(Number(expenseType.pettyCashExpenseTypeMaxAmount).toFixed(2));
        $('#pettyCashExpenseTypeID').val(expenseType.pettyCashExpenseTypeID);
        if(expenseType.pettyCashExpenseTypeAccountID != null){
        	$('#pettyCashExpenseTypeAccountID').append("<option value='"+expenseType.pettyCashExpenseTypeAccountID+"'>"+expenseType.pettyCashExpenseTypeAccountName+"</option>");
        	$('#pettyCashExpenseTypeAccountID').val(expenseType.pettyCashExpenseTypeAccountID).selectpicker('refresh');
        }
    }
});
