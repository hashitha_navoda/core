var locationProducts;
var locProductCodes;
var locProductNames;
var selectedProduct;
var selectedSupplier;
var selectedAccount;
var selectedAuthourType;
var selectedProductQuantity;
var selectedProductUom;
var batchCount = 0;
var currentlySelectedProduct;
var existTaxArray = [];
var dimensionData = {};
var uploadedAttachments = {};
var deletedAttachmentIds = [];
var deletedAttachmentIdArray = [];
var ignoreBudgetLimitFlag = false;
//var nonItemRowId = 2;
//var nonItemProduct = new Array();
$(document).ready(function() {
    var selectedLocation;
    var selectedGrn;
    var selectedPo;
    var decimalPoints = 0;
    var currentItemTaxResults;
    var currentTaxAmount;
    var products = new Array();
    var batchProducts = {};
    var serialProducts = {};
    var totalTaxList = {};
    var locRefID;
    var grnProducts = {};
    var poProducts = {};
    var addingGrnPro = '';
    var thisIsGrnAddFlag = false;
    var thisIsPoAddFlag = false;
    var idNumber = 1;
    var quon = 0;
    var expenseType;
    var nonItemRowId = 2;
    var issueDte;
    var nonItemDataSet = null;
    var dimensionArray = {};
    var dimensionTypeID = null;
    var linkedPayments = false;

//    var nonItemProduct;
    function product(lPID, pID, pCode, pN, pQ, pD, pUP, pUom, pTotal, pTax, bProducts, sProducts, productType, productExType) {
        this.locationPID = lPID;
        this.pID = pID;
        this.pCode = pCode;
        this.pName = pN;
        this.pQuantity = pQ;
        this.pDiscount = pD;
        this.pUnitPrice = pUP;
        this.pUom = pUom;
        this.pTotal = pTotal;
        this.pTax = pTax;
        this.bProducts = bProducts;
        this.sProducts = sProducts;
        this.productType = productType;
        this.productExType = productExType;
    }
    function batchProduct(bCode, bQty, mDate, eDate, warnty, bID) {
        this.bCode = bCode;
        this.bQty = bQty;
        this.mDate = mDate;
        this.eDate = eDate;
        this.warnty = warnty;
        this.bID = bID;
    }
    function serialProduct(sCode, sWarranty, sBCode, sEdate, sID,sWarrantyType) {
        this.sCode = sCode;
        this.sWarranty = sWarranty;
        this.sBCode = sBCode;
        this.sEdate = sEdate;
        this.sID = sID;
        this.sWarrantyType = sWarrantyType;
    }
    /////////////////////
    // for update operations
    if($('#editMode').val() == '1'){
        var reqPvID = $('#editMode').attr('data-pvid');
        loadDataToPv(reqPvID);
    }

    $('#viewUploadedFiles').on('click', function(e) {
        $('#doc-attach-table tbody tr').remove();
        $('#doc-attach-table tfoot div').remove();
        $('#doc-attach-table tbody div').remove();

        if (!$.isEmptyObject(uploadedAttachments)) {
            $('#doc-attach-table thead tr').removeClass('hidden');
            $.each(uploadedAttachments, function(index, value) {
                if (!deletedAttachmentIds.includes(value['documentAttachemntMapID'])) {
                    tableBody = "<tr id="+value['documentAttachemntMapID']+" data-attachId=" + value['documentAttachemntMapID'] + "><td><a href="+value.docLink+">"+value.documentRealName+"</a></td><td class='text-center'><span class='glyphicon glyphicon-trash delete_attachment' style='cursor:pointer'></span></td></tr>";
                    $('#doc-attach-table tbody').append(tableBody);
                }
            });
        } else {
            $('#doc-attach-table thead tr').addClass('hidden');
            var noDataFooter = "<div class= ' col-lg-6 text-right'><label><b>No Data</b></label>";
            $('#doc-attach-table tbody').append(noDataFooter);
        }
        $('#attach_view_footer').removeClass('hidden');
        $('#viewAttachmentModal').modal('show');
    });

    $('#doc-attach-table').on('click', '.delete_attachment', function() {
        var attachementID = $(this).parents('tr').attr('data-attachid');
        bootbox.confirm('Are you sure you want to remove this attachemnet?', function(result) {
            if (result == true) {
                $('#' + attachementID).remove();
                deletedAttachmentIdArray.push(attachementID);
            }
        });
    });

    $('#viewAttachmentModal').on('click', '#saveAttachmentEdit', function(event) {
        event.preventDefault();

        $.each(deletedAttachmentIdArray, function(index, val) {
            deletedAttachmentIds.push(val);
        });

        $('#viewAttachmentModal').modal('hide');
    });

    ////////////////////

    loadDropDownFromDatabase('/accounts-api/search-active-accounts-for-dropdown', "", "", '#glAccount');
    loadDropDownFromDatabase('/supplierAPI/search-suppliers-for-dropdown', "", 0, '#supplier');
    changeAuthorType();
    $('#authorType').on('change', function(){
        changeAuthorType();
    });
    $('#supplier').selectpicker('refresh');
    $('#supplier').on('change', function() {
        if ($(this).val() > 0 && $(this).val() != selectedSupplier) {
            selectedSupplier = $(this).val();
            getSupplierDetails(selectedSupplier);
        }
    });

    $('#glAccount').on('change', function() {
        if ($(this).val() > 0 && $(this).val() != selectedAccount) {
            selectedAccount = $(this).val();
            setDueDate(0);
        }
    });

    loadDropDownFromDatabase('/api/expense-type/get-expense-type-by-search', "", 0, '#expenseType');
    $('#expenseType').selectpicker('refresh');
    $('#voucherType').on('change', function() {
        if ($(this).val() > 0 && $(this).val() != expenseType) {
            expenseType = $(this).val();
        }
    });

    loadDropDownFromDatabase('/api/expense-type/get-expense-type-by-search', "", 'expenseType', '#itemExType');
    loadDropDownFromDatabase('/api/expense-type/get-expense-type-by-search', "", 'expenseType', '.non-item-ex-type-selectbox');

    $.each(LOCATION, function(index, value) {
        $('#retrieveLocation').append($("<option value='" + index + "'>" + value + "</option>"));
    });

    $('#retrieveLocation').selectpicker('refresh');
    $('#retrieveLocation').on('change', function() {
        if ($(this).val() == '') {
            selectedLocation = ''
            clearProductScreen();
            $('#productAddOverlay').addClass('overlay');
            $('#piNo').val('');
            $('#locRefID').val('');
            $('#dimension_div').addClass('hidden');
        } else {
            selectedLocation = $(this).val();
            setDataForLocation($(this).val());
            $('#dimension_div').removeClass('hidden');
        }
    });

    var currentLocation = $('#navBarSelectedLocation').attr('data-locationid');
    if (!$('#retrieveLocation').prop('disabled')) {
        $('#retrieveLocation').val(currentLocation).change();
    }

    $('#dimensionView').on('click', function(e) {
        clearDimensionModal();
        var piNo = $('#piNo').val();

        if (!$.isEmptyObject(dimensionData)) {
            $.each(dimensionData[piNo], function(index, val) {
                dimensionArray[index] = val;                
            });
            $.each(dimensionArray, function(index, value) {
                var newTrID = 'tr_' + value.dimensionTypeId;
                var clonedRow = $($('#dimensionPreSample').clone()).attr('id', newTrID).addClass('addedDimensions');
                $("input[name='dimensionType']", clonedRow).val(value.dimensionTypeTxt);
                $("input[name='dimensionValue']", clonedRow).val(value.dimensionValueTxt);
                clonedRow.removeClass('hidden');
                clonedRow.insertBefore('#dimensionPreSample');
                clearAddedNewDimensionRow();
            });
        }
        $('#addDimensionModal').modal('show');
    });

    // dimension modal
    function clearDimensionModal()
    {
        $("#addDimensionModal #dimensionTable tbody .addedDimensions").remove();
        dimensionArray = {};
    }


    $('.dimenisonchange').on('change', function(e) {
        dimensionTypeID = this.value;
        getDimensionValueListByDimensionType(dimensionTypeID, function() {
        });
    });



    function getDimensionValueListByDimensionType(dimensionTypeID, callback) {
        switch (dimensionTypeID) {
            case "job":
                $('.dimensionVal').addClass('hidden');
                $('.dimensionProjectNo').addClass('hidden');
                $('.dimensionJobNo').removeClass('hidden');
                break;
            case "project":
                $('.dimensionVal').addClass('hidden');
                $('.dimensionProjectNo').removeClass('hidden');
                $('.dimensionJobNo').addClass('hidden');
                break;
           default:
                $('.dimensionVal').removeClass('hidden');
                $('.dimensionProjectNo').addClass('hidden');
                $('.dimensionJobNo').addClass('hidden');
                $.ajax({
                    type: 'POST',
                    url: BASE_URL + '/dimension-api/getRelatedDimensionValues',
                    data: {dimensionId: dimensionTypeID},
                    success: function(respond) {
                        //empty options
                        $('#dimensionVal').empty();
                        $('#dimensionVal').append($("<option>", {value: '', html: 'Select dimension value'}));
                        $(respond.data).each(function(index, dimValue) {
                            $.each(dimValue, function(ind, val) {
                                $('#dimensionVal').append($("<option>", {value: ind, html: val.value}));
                            });
                        });
                        callback();
                    }
                });
                break;
        }
    }

    $('#add_dimension').on('click', function(e) {
        e.preventDefault();
        //if plus button is clicked then disabled the button
        //if button is clicked more than one, then it does not continue
        if ($(this).is(':disabled')) {
            return false;
        } else {
            $(this).attr('disabled', true);
            var $thisRow = $(this).parents('tr');
            if ($('.dimenisonchange').val() == 0 || $('.dimenisonchange').val() == '' || $('.dimenisonchange').val() == null) {
                p_notification(false, eb.getMessage('ERR_DIMENSION_SELCET'));
                $(this).attr('disabled', false);
            } else {

                if ($('.dimenisonchange').val() != 'job' && $('.dimenisonchange').val() != 'project') {
                    if ($('.dimensionVal').val() == 0 || $('.dimensionVal').val() == '' || $('.dimensionVal').val() == null) {
                        p_notification(false, eb.getMessage('ERR_DIMENSION_VALUE_SELCET'));
                        $(this).attr('disabled', false);
                        return;
                    }
                }

                if ($('.dimenisonchange').val() == 'project') {
                    if ($('.dimensionProjectNo').val() == 0 || $('.dimensionProjectNo').val() == '' || $('.dimensionProjectNo').val() == null) {
                        p_notification(false, eb.getMessage('ERR_DIMENSION_VALUE_SELCET'));
                        $(this).attr('disabled', false);
                        return;
                    }
                }

                if ($('.dimenisonchange').val() == 'job') {
                    if ($('.dimensionJobNo').val() == 0 || $('.dimensionJobNo').val() == '' || $('.dimensionJobNo').val() == null) {
                        p_notification(false, eb.getMessage('ERR_DIMENSION_VALUE_SELCET'));
                        $(this).attr('disabled', false);
                        return;
                    }
                }
                
                if (dimensionArray[dimensionTypeID]) {
                    p_notification(false, eb.getMessage('ERR_DIMENSION_ALREADY_INSERTED'));
                    $('.dimenisonchange').val('');
                    $('.dimenisonchange').selectpicker('render');
                    $('.dimenisonchange').trigger('change');
                } else {
                    addNewDimensionRow($(this).parents('tr'));
                }
                $(this).attr('disabled', false);
            }
        }
    });

    function addNewDimensionRow(thisRow) {

        var dimensionTypeId = $('.dimenisonchange', thisRow).val();
        var dimensionTypeTxt = $('.dimenisonchange', thisRow).find("option:selected").text();

        switch (dimensionTypeID) {
            case "job":
                var dimensionValueTxt = $('.dimensionJobNo', thisRow).find("option:selected").text();
                var dimensionValueId = $('.dimensionJobNo', thisRow).val();
                break;
            case "project":
                var dimensionValueTxt = $('.dimensionProjectNo', thisRow).find("option:selected").text();
                var dimensionValueId = $('.dimensionProjectNo', thisRow).val();
                break;
           default:
                var dimensionValueTxt = $('.dimensionVal', thisRow).find("option:selected").text();
                var dimensionValueId = $('.dimensionVal', thisRow).val();
                break;
        }
       
        var dimensionDuplicateCheck = false;
        if (!$.isEmptyObject(dimensionArray)) {
            $.each(dimensionArray, function(index, value) {
                if (value.dimensionTypeId == dimensionTypeId) {
                    dimensionDuplicateCheck = true;
                }
            });   
        }
       
        if (dimensionDuplicateCheck) {
            p_notification(false, eb.getMessage("ERR_DIMENSION_ALREADY_INSERTED"));
            return false;
        }

        var newTrID = 'tr_' + dimensionTypeId;
        var clonedRow = $($('#dimensionPreSample').clone()).attr('id', newTrID).addClass('addedDimensions');
        $("input[name='dimensionType']", clonedRow).val(dimensionTypeTxt);
        $("input[name='dimensionValue']", clonedRow).val(dimensionValueTxt);
        clonedRow.removeClass('hidden');
        clonedRow.insertBefore('#dimensionPreSample');
        dimensionArray[dimensionTypeId] = new dimension(dimensionTypeId, dimensionValueId,dimensionTypeTxt , dimensionValueTxt);
        clearAddedNewDimensionRow();
    }


    function dimension(dimensionTypeId, dimensionValueId, dimensionTypeTxt, dimensionValueTxt) {
        this.dimensionTypeId = dimensionTypeId;
        this.dimensionValueId = dimensionValueId;
        this.dimensionTypeTxt = dimensionTypeTxt;
        this.dimensionValueTxt = dimensionValueTxt;
    }

    function clearAddedNewDimensionRow() {
        $('.dimenisonchange').val('');
        $('.dimenisonchange').selectpicker('render');
        $('.dimenisonchange').trigger('change');
    }

    $('.addDimensions').on('click', function(e) {
        e.preventDefault();

        // validate products before closing modal
        if (!dimensionModalValidate(e)) {
            e.stopPropagation();
            return false;
        } else {
            $('#addDimensionModal').modal('hide');
        }
    });

    function dimensionModalValidate(e) {
        var piNo = $('#piNo').val();
        if ($.isEmptyObject(dimensionArray)) {
            dimensionData = {};
            return true;   
        } else {
            var dimensionSingleData = {};
            $.each(dimensionArray, function(index, val) {
                dimensionSingleData[index] = val;
            });
            dimensionData[piNo] = dimensionSingleData;
            return true;
        }
    }

    $("#addDimensionModal #dimensionTable").on('click', '.deleteDimensionType', function(e) {
        e.preventDefault();
        var deleteDTrID = $(this).closest('tr').attr('id');
        var deleteDID = deleteDTrID.split('tr_')[1].trim();
        bootbox.confirm('Are you sure you want to remove this dimension ?', function(result) {
            if (result == true) {
                delete dimensionArray[deleteDID];
                $('#' + deleteDTrID).remove();
            }
        });
    });

    loadDropDownFromDatabase('/job-api/searchJobsForDropdown', "", 0, '.dimensionJobNo');
    $('.dimensionJobNo').selectpicker('refresh');
    $('.dimensionJobNo').on('change', function() {
        var dimensionValue = $(this).val();
    });

    loadDropDownFromDatabase('/api/project/searchProjectsForDropdown', "", 0, '.dimensionProjectNo');
    $('.dimensionProjectNo').selectpicker('refresh');
    $('.dimensionProjectNo').on('change', function() {
        var dimensionValue = $(this).val();
    });

    $('#itemCode').on('change', function() {
        $parentRow = $(this).parents('tr');
        clearAddNewRow($parentRow);
        clearModalWindow();
        $('#addNewTax').attr('disabled', false);
        if ($(this).val() > 0 && $(this).val() != currentlySelectedProduct) {
            currentlySelectedProduct = $(this).val();
            selectedProduct = $(this).val();
            eb.ajax({
                type: 'POST',
                url: BASE_URL + '/productAPI/get-location-product-details',
                data: {productID: selectedProduct, locationID: selectedLocation},
                success: function(respond) {
                    var currentElem = new Array();
                    currentElem[selectedProduct] = respond.data;
                    locationProducts = $.extend({}, currentElem);
                    $('#addNewTax').attr('disabled', false);
                    setTaxListForProduct(selectedProduct);
                    $('#qty').parent().addClass('input-group');
                    $('#qty').addUom(locationProducts[selectedProduct].uom);
                    $('.uomqty').attr("id", "itemQuantity");
                    $('#itemCode').data('PN', locationProducts[selectedProduct].pN);
                    $("#itemCode").data('PT', locationProducts[selectedProduct].pT);
                    $("#itemCode").data('PC', locationProducts[selectedProduct].pC);
                    $("#itemCode").data('pId', locationProducts[selectedProduct].pID);

                    if (locationProducts[selectedProduct].pPDV) {
                        $('#piDiscount',$parentRow).val(locationProducts[selectedProduct].pPDV);
                        $(".sign", $parentRow).text('Rs');
                        $('#piDiscount',$parentRow).attr('disabled', false);
                    } else if (locationProducts[selectedProduct].pPDP) {
                        $('#piDiscount',$parentRow).val(locationProducts[selectedProduct].pPDP);
                        $('#piDiscount',$parentRow).attr('disabled', false);
                        $(".sign", $parentRow).text('%');
                    } else if (locationProducts[selectedProduct].pPDP == null && locationProducts[selectedProduct].pPDV == null) {
                        $('#piDiscount',$parentRow).attr('disabled', true);
                    }
                }
            });
        }
    });
    $('#refreshStartBy').on('click', function() {
        selectedGrn = '';
        $('#supplier').prop('disabled', false);
        $('#supplier').empty().selectpicker('refresh');
        $('#grn').empty().val('').selectpicker('refresh');
        $('#po').empty().val('').selectpicker('refresh');
        $('#retrieveLocation').prop('disabled', false);
        $('#retrieveLocation').val('').selectpicker('render');
        $('#startByCode').val('');
        $('#startByCode').prop('disabled', false);
        $('#startByCode').val('');
        $('#deliveryDate').prop('disabled', false);
        $('#deliveryDate').val('');
        $('#supplierReference').prop('disabled', false);
        $('#supplierReference').val('');
        $('#comment').prop('disabled', false);
        $('#comment').val('');
        $('#retrieveLocation').prop('disabled', false);
        $('#retrieveLocation').val('');
        $('#piNo').val('');
        $('.tempGrnPro').remove();
        $('.tempPoPro').remove();
        selectedSupplier = '';
        selectedAccount = '';
        selectedLocation = '';
        $('#deliveryCharge').val('');
        $('.deliCharges').addClass('hidden');
        $('#deliveryChargeEnable').prop('checked', false);
        clearProductScreen();
        $('#productAddOverlay').addClass('overlay');
        $('#addSupplierBtn').prop('disabled', false);
        $('#addSupplierBtn').unbind('click');
        $('#addNewItemTr').removeClass('hidden');
    });
    $('#addItem').on('click', function() {
        var row = $(this).parents('tr');
        var uomqty = $("#qty").siblings('.uomqty').val();
        $("#qty").siblings('.uomqty').change();
        $('.tempy').remove();
        idNumber = 1;
        quon = 0;
        $('#prefix').val('');
//        $('#serialAddTablePi').addClass('hidden');
        var issDate = new Date(Date.parse($('#issueDate').val()));
        var expD = $('#expire_date_autoFill').val('').datepicker({
            onRender: function(date) {
                return date.valueOf() < issDate.valueOf() ? ((U_DATE_R_OVERRIDE) ? '' : 'disabled') : '';
            }
        }).on('changeDate', function(ev) {
            expD.hide();
        }).data('datepicker');
        $('#quontity').val('');
        $('#startNumber').val('');
        $('#batch_code').val('');
        $('#wrtyDays').val('');
        $('#mf_date_autoFill').val('');
        var mfD = $('#mf_date_autoFill').datepicker({
            onRender: function(date) {
                return date.valueOf() > issDate.valueOf() ? ((U_DATE_R_OVERRIDE) ? '' : 'disabled') : '';
            }
        }).on('changeDate', function(ev) {
            mfD.hide();
        }).data('datepicker');
        $('#temp_1').find('input[name=submit]').attr('disabled', false);
        $('#temp_1').find('input[name=prefix]').attr('disabled', false);
        $('#temp_1').find('input[name=startNumber]').attr('disabled', false);
        $('#temp_1').find('input[name=quontity]').attr('disabled', false);
        $('#temp_1').find('input[name=expireDateAutoFill]').attr('disabled', false);
        $('#temp_1').find('input[name=batch_code]').attr('disabled', false);
        $('#temp_1').find('input[name=mfDateAutoFill]').attr('disabled', false);
        $('#temp_1').find('input[name=wrtyDays]').attr('disabled', false);
        selectedProductUom = $("#qty").siblings('.uom-select').children('button').find('.selected').data('uomID');
        if (typeof (selectedProduct) == 'undefined' || selectedProduct == '') {
            p_notification(false, eb.getMessage('ERR_GRN_SELECT_PROD'));
        }
        else if (typeof (selectedProductUom) == 'undefined' || selectedProductUom == '') {
            p_notification(false, eb.getMessage('ERR_GRN_SELECT_UOM'));
        }
        else {
            var checkingPrice = toFloat($('#unitPrice').val()).toFixed(2);
            if ($('#unitPrice').val() == '') {
                p_notification(false, eb.getMessage('ERR_GRN_ENTER_UNITPRI'));
                $('#unitPrice').focus();
            } else if ($('#itemCode').data('PT') != 2 && $('#qty').val() == '') {
                p_notification(false, eb.getMessage('ERR_GRN_ENTER_QUANTITY'));
            } else if ((uomqty == 0) || (uomqty == '')) {
                p_notification(false, eb.getMessage('ERR_GRN_ENTER_QUANTITY'));
            } else if (products.indexOf(_.findWhere(products, {'locationPID': locationProducts[selectedProduct].lPID, 'pUnitPrice': checkingPrice})) >= 0) {
                p_notification(false, eb.getMessage('ERR_GRN_ENTER_SAMEPROD_SAMEPRIC'));
                $('#unitPrice').focus();
            } else {
                clearModalWindow();
                $('.cloneSerials').remove();
                $('#serialSample').children().children('#newSerialNumber').removeClass('serialNumberList');
                $('#serialSample').children().children('#newSerialBatch').removeClass('serialBatchList');
                $('#serialSample').children().children('#newSerialBatch').prop('disabled', false);
                if ($('#itemCode').data('PT') == 2) {
                    locationProducts[selectedProduct].bP = 0;
                    locationProducts[selectedProduct].sP = 0;
                }
                if (locationProducts[selectedProduct].bP == 1) {
                    $('#batchAddScreen').removeClass('hidden');
                    $('#newBatchMDate').addClass('white_bg');
                    $('#newBatchEDate').addClass('white_bg');
                }
                if (locationProducts[selectedProduct].sP == 1) {
                    $('#serialAddScreen').removeClass('hidden');
                }
                if (locationProducts[selectedProduct].sP == 1 && locationProducts[selectedProduct].bP == 0) {
                    $('#temp_1').find('input[name=mfDateAutoFill]').attr('disabled', true).removeClass('white_bg');
                    $('#temp_1').find('input[name=batch_code]').attr('disabled', true);
                    $('#temp_1').find('input[name=expireDateAutoFill]').addClass('white_bg');
                }

                if (locationProducts[selectedProduct].sP == 1 && locationProducts[selectedProduct].bP == 1) {
                    $('#batchAddScreen').addClass('hidden');
                    $('#temp_1').find('input[name=batch_code]').attr('disabled', false);
                    $('#temp_1').find('input[name=mfDateAutoFill]').attr('disabled', false).addClass('white_bg');
                }

                if (locationProducts[selectedProduct].bP == 0 && locationProducts[selectedProduct].sP == 0) {
                    $('#unitPrice', row).trigger('focusout');
                    addNewProductRow({}, {});
                    $('#qty').parent().removeClass('input-group');
                    $('#itemCode').focus();
                } else {
                    selectedProductQuantity = $('#qty').val();
                    if (locationProducts[selectedProduct].bP == 1) {
                        $('#piProductCodeBatch').html(locationProducts[selectedProduct].pC);
                        $('#piProductQuantityBatch').html($('#qty').val());
                    }
                    if (locationProducts[selectedProduct].sP == 1) {
                        $('#addNewSerial input[type=text]').val('');
                        $('#piProductCodeSerial').html(locationProducts[selectedProduct].pC);
                        $('#piProductQuantitySerial').html($('#qty').val());
                        $('#serialSample').children().children('#newSerialBatch').addClass('serialBatchList');
                        $('#serialSample').children().children('#newSerialNumber').addClass('serialNumberList');
                        if (locationProducts[selectedProduct].bP == 0) {
                            $('#serialSample').children().children('#newSerialBatch').prop('disabled', true);
                        }
                        for (i = 0; i < selectedProductQuantity - 1; i++) {
                            var serialAddRowClone = $('#serialSample').clone().attr('id', i).addClass('cloneSerials');
                            $('#serialSample').children().children('#newSerialNumber').addClass('serialNumberList');
                            $('#serialSample').children().children('#newSerialBatch').addClass('serialBatchList');
                            serialAddRowClone.children().children('#newSerialNumber').addClass('serialNumberList');
                            serialAddRowClone.children().children('#newSerialBatch').addClass('serialBatchList');
                            serialAddRowClone.children().children('#newSerialEDate').attr('id', 'sEDate' + i).addClass('sEDate');
                            if (locationProducts[selectedProduct].bP == 0) {
                                $('#serialSample').children().children('#newSerialBatch').prop('disabled', true);
                                serialAddRowClone.children().children('#newSerialBatch').prop('disabled', true);
                            }
                            serialAddRowClone.insertBefore('#serialSample');
                        }
                        if (selectedProductQuantity <= 100) {
                            var dDate = new Date(Date.parse($('#issueDate').val()));
                            $('.sEDate').datepicker({
                                onRender: function(date) {
                                    return date.valueOf() < dDate.valueOf() ? ((U_DATE_R_OVERRIDE) ? '' : 'disabled') : '';
                                }
                            });
                        }

                    }
                    $('#addPiProductsModal').modal('show');
                }
            }

        }
    });
    $('table#piProductTable>tbody').on('keypress', 'input#qty, input#unitPrice, input#piDiscount,.uomqty', function(e) {

        if (e.which == 13) {
            $('#addItem', $(this).parents('tr')).trigger('click');
            e.preventDefault();
        }
    });
    $('#piProductTable').on('click', '.dropdown-menu, .dropdown-menu label', function(e) {
        e.stopPropagation();
    });
    $('#piProductTable').on('keyup', '#qty,#unitPrice,#piDiscount', function() {
        if ($('#itemCode').val() != '') {
            if ($(this).val().indexOf('.') > 0) {
                decimalPoints = $(this).val().split('.')[1].length;
            }
            if (this.id == 'unitPrice' && (!$.isNumeric($('#unitPrice').val()) || $('#unitPrice').val() < 0 || decimalPoints > 2)) {
                decimalPoints = 0;
                $('#unitPrice').val('');
            }
            if ((this.id == 'qty') && (!$.isNumeric($('#qty').val()) || $('#qty').val() < 0)) {
                decimalPoints = 0;
                $('#qty').val('');
            }
            if ((this.id == 'piDiscount') && (!$.isNumeric($('#piDiscount').val()) || $('#piDiscount').val() < 0 || (decimalPoints > 2))) {
                decimalPoints = 0;
                $('#piDiscount').val('');
            }
        }
    });
    $('#piProductTable').on('focusout', '#qty,#unitPrice,#piDiscount', function() {
        var checkedTaxes = Array();
        var row = $(this).parents('tr');
        var productID = $('#itemCode', $(this).parents('tr')).data('pId');
        if (productID) {
            productID = productID;
        } else {
            productID = row.attr('id').split("_");
            productID = productID[1];            
        }
        $('input:checkbox.addNewTaxCheck').each(function() {
            if (this.checked) {
                var cliTID = this.id;
                var tTaxID = cliTID.split('_')[1];
                checkedTaxes.push(tTaxID);
            }
        });
        var qty = $('#qty').val();
        var uomqty = $("#qty").siblings('.uomqty').val();
        var unitPrice = $('#unitPrice').val();
        var discount = $('#piDiscount').val();
        if ($('#itemCode', row).data('PT') == 2 && qty == 0) {
            qty = 1;
        }
        var discountType;
        if (locationProducts[productID]) {
            if (locationProducts[productID].pPDP != null) {
                discountType = "Per";
            } else if (locationProducts[productID].pPDV != null) {
                discountType = "Val";
            } else {
               discountType = "Non";
            }
            if (discountType == "Val") {
                var newDiscount = validateDiscount(productID,discountType,discount, unitPrice);
                var tmpItemCost = calculateTaxFreeItemCostByValueDiscount(unitPrice, uomqty, newDiscount);
                if (tmpItemCost < 0) {
                    tmpItemCost = 0;
                }
            } else if (discountType == "Per") {
                var newDiscount = validateDiscount(productID,discountType,discount, unitPrice);
                var tmpItemCost = calculateTaxFreeItemCost(unitPrice, uomqty, newDiscount);
            } else {
                newDiscount = 0;
                var tmpItemCost = calculateTaxFreeItemCost(unitPrice, uomqty, newDiscount);
            }
            $("input[name='discount']", $(this).parents('tr')).val(newDiscount);
            currentItemTaxResults = calculateItemCustomTax(tmpItemCost, checkedTaxes);
            currentTaxAmount = (currentItemTaxResults == null) ? null : currentItemTaxResults.tTA;
            var itemCost = toFloat(tmpItemCost) + toFloat(currentTaxAmount);
            $('#addNewTotal').html(accounting.formatMoney(itemCost));
        }
    });
    $('#non-item-tbody').on('focusout', '#addNewNonItemTax,#non-item-unit-prc', function() {
        var nonItemTax = Array();
        $(this).parents('tr').find('input:checkbox.addNewTaxCheck').each(function() {
            if (this.checked) {
                var cliTID = this.id;
                var taxID = cliTID.split('_')[1];
                nonItemTax.push(taxID);
            }
        });
        var unitPrice = $(this).parents('tr').find('input[name=non-item-unit-prc]').val();
//        var qty = $(this).parents('tr').find('input[name=non-item-qty]').val();
        var qty = 1;
        var tmpNonItemCost = toFloat(unitPrice) * toFloat(qty);
        currentItemTaxResults = calculateItemCustomTax(tmpNonItemCost, nonItemTax);
        currentTaxAmount = (currentItemTaxResults == null) ? null : currentItemTaxResults.tTA;
        var itemCost = toFloat(tmpNonItemCost) + toFloat(currentTaxAmount);
        $(this).parents('tr').find('.non-item-total-price').html(accounting.formatMoney(itemCost));
    });
    $('#non-item-tbody').on('click', '#addNonItem', function() {
        var nonItemTax = Array();
        $(this).parents('tr').find('input:checkbox.addNewTaxCheck').each(function() {
            if (this.checked) {
                var cliTID = this.id;
                var taxID = cliTID.split('_')[1];
                nonItemTax.push(taxID);
            }
        });
        var unitPrice = $(this).parents('tr').find('input[name=non-item-unit-prc]').val();

//        var qty = $(this).parents('tr').find('input[name=non-item-qty]').val();
        var qty = 1;
        var tmpNonItemCost = toFloat(unitPrice) * toFloat(qty);
        currentItemTaxResults = calculateItemCustomTax(tmpNonItemCost, nonItemTax);
        currentTaxAmount = (currentItemTaxResults == null) ? null : currentItemTaxResults.tTA;
        var itemCost = toFloat(tmpNonItemCost) + toFloat(currentTaxAmount);
        $(this).parents('tr').find('.non-item-total-price').html(accounting.formatMoney(itemCost));
    });

    $('#non-item-tbody').on('change', '.taxChecks.addNewTaxCheck', function() {
        var $element = $(this).closest('ul');
        var $checkBox = $(this).closest('td').find('.nonItemTaxApplied');

        var allchecked = false;
        $($element).find('.taxChecks.addNewTaxCheck').each(function() {
            if (this.checked) {
                allchecked = true;
            }
        });
        if (allchecked == false) {
            $checkBox.removeClass('glyphicon glyphicon-check');
            $checkBox.addClass('glyphicon glyphicon-unchecked');
        } else {
            $checkBox.removeClass('glyphicon glyphicon-unchecked');
            $checkBox.addClass('glyphicon glyphicon-check');
        }
    });

    $('#piProductTable').on('change', '.taxChecks.addNewTaxCheck', function() {
        var allchecked = false;
        $('input:checkbox.addNewTaxCheck').each(function() {
            if (this.checked && $(this).prop('disabled') === false) {
                allchecked = true;
            }
        });
        if (allchecked == false) {
            $('#taxApplied').removeClass('glyphicon glyphicon-check');
            $('#taxApplied').addClass('glyphicon glyphicon-unchecked');
        } else {
            $('#taxApplied').removeClass('glyphicon glyphicon-unchecked');
            $('#taxApplied').addClass('glyphicon glyphicon-check');
        }
        $("#unitPrice").trigger(jQuery.Event("focusout"));
    });
    $('#piProductTable').on('change', '.addNewUomR', function() {
        $('#uomAb').html(locationProducts[selectedProduct].uom[$(this).val()].uA);
        selectedProductUom = $(this).val();
    });
    $('#piProductTable').on('click', '#selectAll', function() {
        $(this).parents('.dropdown-menu').find('input:checkbox.addNewTaxCheck').each(function() {
            if (!this.checked) {
                this.checked = true;
            }
        });
        $(this).parents('tr').find(".taxChecks.addNewTaxCheck").trigger(jQuery.Event("change"));
    });

    $('#piProductTable').on('click', '#deselectAll', function() {
        $(this).parents('.dropdown-menu').find('input:checkbox.addNewTaxCheck').each(function() {
            if (this.checked) {
                this.checked = false;
            }
        });
        $(this).parents('tr').find(".taxChecks.addNewTaxCheck").trigger(jQuery.Event("change"));
    });
    $('#deliveryChargeEnable').on('click', function() {
        if (this.checked) {
            $('.deliCharges').removeClass('hidden');
            $('#deliveryCharge').focus();
        }
        else {
            $('#deliveryCharge').val('');
            $('.deliCharges').addClass('hidden');
        }
        setPiTotalCost();
    });
    $('#deliveryCharge').on('change keyup', function() {
        if (!$.isNumeric($('#deliveryCharge').val()) || $(this).val() < 0) {
            $(this).val('');
            setPiTotalCost();
        } else {
            setPiTotalCost();
        }
    });
    $('#piProductTable').on('click', '.piDeleteProduct', function() {
        var deletePTrID = $(this).closest('tr').attr('id');
        var deletePID = deletePTrID.split('tr_')[1].trim();
        var deleteItemPrice = accounting.unformat($(this).closest('tr').find('#unit').html()).toFixed(2);
        var deleteProductIndex = products.indexOf(_.findWhere(products, {'locationPID': deletePID, 'pUnitPrice': deleteItemPrice}));
        bootbox.confirm('Are you sure you want to remove this product ?', function(result) {
            if (result == true) {
                products.splice(deleteProductIndex, 1);
                $('#' + deletePTrID).remove();
                setPiTotalCost();
                setTotalTax();
            }
        });
    });
    $('#piProductTable').on('click', '.piViewProducts', function() {
        var viewPTrID = $(this).closest('tr').attr('id');
        var viewPID = viewPTrID.split('tr_')[1].trim();
        var viewItemPrice = accounting.unformat($(this).closest('tr').find('#unit').html()).toFixed(2);
        var viewProductIndex = products.indexOf(_.findWhere(products, {'locationPID': viewPID, 'pUnitPrice': viewItemPrice}));
        var vPID = products[viewProductIndex].pID;
        if (locationProducts[vPID].bP == 0 && locationProducts[vPID].sP == 0) {
            p_notification(false, eb.getMessage('ERR_GRN_SUBPROD'));
        } else {
            setProductsView(viewProductIndex);
            $('#viewPiSubProductsModal').modal('show');
        }

    });
    $('#showTax').on('click', function() {
        if (this.checked) {
            if (!jQuery.isEmptyObject(products)) {
                $('#totalTaxShow').removeClass('hidden');
            } else {
                p_notification(false, eb.getMessage('ERR_GRN_PROD_TAX'));
            }

        } else {
            $('#totalTaxShow').addClass('hidden');
        }

    });
    $('#piCancel').on('click', function() {
        window.location.reload();
    });
    $('#grnForm').on('submit', function(e) {
        e.preventDefault();
        saveAndUpdatePaymentVoucher();
    });

    function saveAndUpdatePaymentVoucher()
    {
        var selectedid = "#" + document.activeElement.id;
        var tableId = $(selectedid).closest('table').attr("id");
        selectedSupplier = $('#supplier').val();
        selectedAccount = $('#glAccount').val();
        if (selectedAuthourType == 1 && (typeof(selectedSupplier) == 'undefined' || selectedSupplier == '' || selectedSupplier == 0)) {
            p_notification(false, eb.getMessage('ERR_SELECT_PI_SUPPLIER'));
        } else if (selectedAuthourType == 2 && (typeof(selectedAccount) == 'undefined' || selectedAccount == '' || selectedAccount == 0)) {
            p_notification(false, eb.getMessage('ERR_SELECT_PI_ACCOUNT'));
        } else if (typeof (selectedLocation) == 'undefined' || selectedLocation == '') {
            p_notification(false, eb.getMessage('ERR_GRN_SELECT_LOCAT'));
        }
        else if ($('#deliveryDate').val() == '') {
            p_notification(false, eb.getMessage('ERR_PI_VALID_DUE'));
        }
        else if (eb.convertDateFormat('#issueDate') > eb.convertDateFormat('#deliveryDate')) {
            p_notification(false, eb.getMessage('ERR_PI_INV_DUE'));
            $('#deliveryDate').focus();
        } else if ($('#piNo').val() == '') {
            p_notification(false, eb.getMessage('ERR_PLEASE_SET_REFERENCE_FOR_THE_PAYMENT_VOUCHER'));
        } else if(products.length==0 && getNonItemProductList().length==0){
            p_notification(false,eb.getMessage('ERR_NO_PV_DATA'));
        } else {
            var saveFlag = true;
            if($('#editMode').val() == '1') {
                $('#non-item-tbody tr.edited').each(function(){
                    if(!($(this).find('#addNonItem').parents().hasClass('hidden'))){
                        p_notification(false,eb.getMessage('ERR_NO_PV_DATA'));
                        saveFlag = false;
                    }
                });
            }
            if(saveFlag){

                $('#piSave').prop('disabled', true);
                var gFT = accounting.unformat($('#finaltotal').html());
                var sID = (selectedAuthourType == 1) ? selectedSupplier : null;
                var accountID = (selectedAuthourType == 2) ? selectedAccount : null;
                var showTax = 0;
                if ($('#showTax').is(':checked')) {
                    showTax = 1;
                }
                var piPostData = {
                    sID: sID,
                    accountID: accountID,
                    piC: $('#piNo').val(),
                    dD: $('#deliveryDate').val(),
                    iD: $('#issueDate').val(),
                    sR: $('#supplierReference').val(),
                    rL: selectedLocation,
                    cm: $('#comment').val(),
                    pr: products,
                    nIpr: getNonItemProductList(),
                    dC: $('#deliveryCharge').val(),
                    fT: gFT,
                    sT: showTax,
                    lRID: $('#locRefID').val(),
                    startByGrnID: selectedGrn,
                    startByPoID: selectedPo,
                    pT: $('#paymentTerm').val(),
                    exTy: expenseType,
                    dimensionData: dimensionData,
                    ignoreBudgetLimit: ignoreBudgetLimitFlag,
                    linkedPayments: linkedPayments
                };
                if($('#editMode').val() == '1'){
                    var existingAttachemnts = {};
                    var deletedAttachments = {};
                    $.each(uploadedAttachments, function(index, val) {
                        if (!deletedAttachmentIds.includes(val['documentAttachemntMapID'])) {
                            existingAttachemnts[index] = val;
                        } else {
                            deletedAttachments[index] = val;
                        }
                    });

                    piPostData['editMode'] = true;
                    piPostData['pVID'] = $('#editMode').attr('data-pvid');
                    piPostData['iD'] = $('#issueDate').val();//issueDte;
                    eb.ajax({
                        type: 'POST',
                        url: BASE_URL + '/expense-purchase-invoice-api/update',
                        data: piPostData,
                        success: function(respond) {
                            if (respond.status == true) {
                                p_notification(respond.status, respond.msg);
                                var fileInput = document.getElementById('editDocumentFiles');
                                var form_data = false;
                                if (window.FormData) {
                                    form_data = new FormData();
                                }
                                form_data.append("documentID", respond.data.pVID);
                                form_data.append("documentTypeID", 20);
                                form_data.append("updateFlag", true);
                                form_data.append("editedDocumentID", $('#editMode').attr('data-pvid'));
                                form_data.append("documentCode", $('#piNo').val());
                                form_data.append("deletedAttachmentIds", deletedAttachmentIds);
                                
                                if(fileInput.files.length > 0) {
                                    for (var i = 0; i < fileInput.files.length; i++) {
                                        form_data.append("files[]", fileInput.files[i]);
                                    }
                                }

                                eb.ajax({
                                    url: BASE_URL + '/store-files',
                                    type: 'POST',
                                    processData: false,
                                    contentType: false,
                                    data: form_data,
                                    success: function(res) {
                                    }
                                });

                                window.location.assign(BASE_URL + "/expense-purchase-invoice/list");
                            } else {
                                if (respond.data == "NotifyBudgetLimit") {
                                    bootbox.confirm(respond.msg+' ,Are you sure you want to update ?', function(result) {
                                        if (result == true) {
                                            ignoreBudgetLimitFlag = true;
                                            saveAndUpdatePaymentVoucher();
                                        } else {
                                            setTimeout(function(){ 
                                                location.reload();
                                            }, 3000);
                                        }
                                    });
                                } else {
                                    p_notification(respond.status, respond.msg);
                                    $('#piSave').prop('disabled', false);
                                }
                            }
                        }
                    });
                } else {
                    eb.ajax({
                        type: 'POST',
                        url: BASE_URL + '/expense-purchase-invoice-api/savePi',
                        data: piPostData,
                        success: function(respond) {
                            if (respond.status == true) {
                                p_notification(respond.status, respond.msg);
                                var fileInput = document.getElementById('documentFiles');
                                if(fileInput.files.length > 0) {
                                    var form_data = false;
                                    if (window.FormData) {
                                        form_data = new FormData();
                                    }
                                    form_data.append("documentID", respond.data.pVID);
                                    form_data.append("documentTypeID", 20);
                                    
                                    for (var i = 0; i < fileInput.files.length; i++) {
                                        form_data.append("files[]", fileInput.files[i]);
                                    }

                                    eb.ajax({
                                        url: BASE_URL + '/store-files',
                                        type: 'POST',
                                        processData: false,
                                        contentType: false,
                                        data: form_data,
                                        success: function(res) {
                                        }
                                    });
                                }

                                var expensePvPath = '/expense-purchase-invoice-api/send-pV-email';
                                documentPreview(BASE_URL + '/expense-purchase-invoice/preview/' + respond.data.pVID, 'documentpreview', expensePvPath, function($preview) {
                                    $('iframe', $preview).bind('load', function() {

                                        // unbind event because if user changes template, then this event
                                        // will be triggered again and send the email multiple times
                                        $('iframe', $preview).unbind('load');
                                        sendEmailToApprover(respond.data.pVID, respond.data.paymentVoucherCode, respond.data.approver, respond.data.hashValue, respond.data.approversFlag, $preview, function($preview) {
                                            $preview.on('hidden.bs.modal', function() {
                                                if (!$("#preview:visible").length) {
                                                    window.location.reload();
                                                }
                                            });
                                        });
                                    });
                                });
                            } else {
                                if (respond.data == "NotifyBudgetLimit") {
                                    bootbox.confirm(respond.msg+' ,Are you sure you want to save ?', function(result) {
                                        if (result == true) {
                                            ignoreBudgetLimitFlag = true;
                                            saveAndUpdatePaymentVoucher();
                                        } else {
                                            setTimeout(function(){ 
                                                location.reload();
                                            }, 3000);
                                        }
                                    });
                                } else {
                                    p_notification(respond.status, respond.msg);
                                    $('#piSave').prop('disabled', false);
                                }
                            }
                        }
                    });

                }
            }
        }
    }

    $('#piProductTable').on('click', '.grnRemoveItem', function() {
        var tr = $(this).parents("tr");
        var removeProductID = tr.attr('id');
        $('#' + removeProductID).remove();
    });
    $('#piProductTable').on('click', '.grnAddItem', function() {
        thisIsGrnAddFlag = true;
        var tr = $(this).parents("tr");
        var addProductID = tr.attr('id').split('grnPro_')[1].trim();
        addingGrnPro = locationProducts[addProductID].pC;
        if (products[locationProducts[addProductID].lPID]) {
            p_notification(false, eb.getMessage('ERR_GRN_INSERT_PROD'));
        }
        else {
            selectedProduct = addProductID;
            var grnBatchProducts = grnProducts[addingGrnPro].bP;
            var grnSerialProducts = grnProducts[addingGrnPro].sP;
            for (var b in grnBatchProducts) {
                batchProducts[grnBatchProducts[b].bC] = new batchProduct(grnBatchProducts[b].bC, grnBatchProducts[b].bQ, grnBatchProducts[b].bMD, grnBatchProducts[b].bED, grnBatchProducts[b].bW, b);
            }
            for (var b in grnSerialProducts) {
                serialProducts[grnSerialProducts[b].sC] = new serialProduct(grnSerialProducts[b].sC, grnSerialProducts[b].sW, grnSerialProducts[b].sBC, grnSerialProducts[b].sED, b,grnSerialProducts[b].sWT);
            }
            addNewProductRow(batchProducts, serialProducts);
        }

    });
    $('#piProductTable').on('click', '.poAddItem', function() {
        thisIsPoAddFlag = true;
        var tr = $(this).parents("tr");
        var addProductID = tr.attr('id').split('poPro_')[1].trim();
        addingPoPro = locationProducts[addProductID].pC;
        selectedProduct = addProductID;
        var addProductQty = tr.find("input[name='qty']").val();
        $('.tempy').remove();
        idNumber = 1;
        quon = 0;
        $('#prefix').val('');
        var dDate = new Date(Date.parse($('#deliveryDate').val()));
        var epd = $('#expire_date_autoFill').val('').datepicker({
            onRender: function(date) {
                return date.valueOf() < dDate.valueOf() ? ((U_DATE_R_OVERRIDE) ? '' : 'disabled') : '';
            }
        }).on('changeDate', function(ev) {
            epd.hide();
        }).data('datepicker');
        $('#quontity').val('');
        $('#startNumber').val('');
        $('#wrtyDays').val('');
        $('#batch_code').val('');
        var mfd = $('#mf_date_autoFill').val('').datepicker({
            onRender: function(date) {
                return date.valueOf() > dDate.valueOf() ? ((U_DATE_R_OVERRIDE) ? '' : 'disabled') : '';
            }
        }).on('changeDate', function(ev) {
            mfd.hide();
        }).data('datepicker');
        $('#temp_1').find('input[name=submit]').attr('disabled', false);
        $('#temp_1').find('input[name=prefix]').attr('disabled', false);
        $('#temp_1').find('input[name=startNumber]').attr('disabled', false);
        $('#temp_1').find('input[name=quontity]').attr('disabled', false);
        $('#temp_1').find('input[name=expireDateAutoFill]').attr('disabled', false);
        $('#temp_1').find('input[name=batch_code]').attr('disabled', false);
        $('#temp_1').find('input[name=mfDateAutoFill]').attr('disabled', false);
        $('#temp_1').find('input[name=wrtyDays]').attr('disabled', false);
        clearModalWindow();
        $('.cloneSerials').remove();
        $('#serialSample').children().children('#newSerialNumber').removeClass('serialNumberList');
        $('#serialSample').children().children('#newSerialBatch').removeClass('serialBatchList');
        $('#serialSample').children().children('#newSerialBatch').prop('disabled', false);
        if (tr.find("input[name='itemCode']").data('PT') == 2) {
            locationProducts[addProductID].bP = 0;
            locationProducts[addProductID].sP = 0;
        }
        if (locationProducts[addProductID].bP == 1) {
            $('#batchAddScreen').removeClass('hidden');
        }
        if (locationProducts[addProductID].sP == 1) {
            $('#serialAddScreen').removeClass('hidden');
        }
        if (locationProducts[selectedProduct].sP == 1 && locationProducts[selectedProduct].bP == 0) {
            $('#temp_1').find('input[name=mfDateAutoFill]').attr('disabled', true).removeClass('white_bg');
            $('#temp_1').find('input[name=batch_code]').attr('disabled', true);
            $('#temp_1').find('input[name=expireDateAutoFill]').addClass('white_bg');
        }
        if (locationProducts[addProductID].bP == 1 && locationProducts[addProductID].sP == 1) {
            $('#batchAddScreen').addClass('hidden');
            $('#temp_1').find('input[name=batch_code]').attr('disabled', false);
            $('#temp_1').find('input[name=mfDateAutoFill]').attr('disabled', false).addClass('white_bg').datepicker();
            $('#temp_1').find('input[name=expireDateAutoFill]').addClass('white_bg');
        }

        if (locationProducts[addProductID].bP == 0 && locationProducts[addProductID].sP == 0) {
            addNewProductRow({}, {});
            $('#itemCode').focus();
        } else {
            selectedProductQuantity = addProductQty;
            selectedProduct = addProductID;
            var addProductQty = tr.find("input[name='qty']").val();
            $('.cloneSerials').remove();
            $('#serialSample').children().children('#newSerialNumber').removeClass('serialNumberList');
            $('#serialSample').children().children('#newSerialBatch').removeClass('serialBatchList');
            $('#serialSample').children().children('#newSerialBatch').prop('disabled', false);
            if (tr.find("#itemCode").data('PT') == 2) {
                locationProducts[addProductID].bP = 0;
                locationProducts[addProductID].sP = 0;
            }
            if (locationProducts[addProductID].bP == 1) {
                $('#piProductCodeBatch').html(locationProducts[addProductID].pC);
                $('#piProductQuantityBatch').html(addProductQty);
            }
            if (locationProducts[addProductID].sP == 1) {
                $('#addNewSerial input[type=text]').val('');
                $('#piProductCodeSerial').html(locationProducts[addProductID].pC);
                $('#piProductQuantitySerial').html(addProductQty);
                $('#serialSample').children().children('#newSerialBatch').addClass('serialBatchList');
                $('#serialSample').children().children('#newSerialNumber').addClass('serialNumberList');
                for (i = 0; i < addProductQty - 1; i++) {
                    var serialAddRowClone = $('#serialSample').clone().attr('id', i).addClass('cloneSerials');
                    $('#serialSample').children().children('#newSerialNumber').addClass('serialNumberList');
                    $('#serialSample').children().children('#newSerialBatch').addClass('serialBatchList');
                    serialAddRowClone.children().children('#newSerialNumber').addClass('serialNumberList');
                    serialAddRowClone.children().children('#newSerialBatch').addClass('serialBatchList');
                    serialAddRowClone.children().children('#newSerialEDate').attr('id', 'sEDate' + i);
                    if (locationProducts[addProductID].bP == 0) {
                        $('#serialSample').children().children('#newSerialBatch').prop('disabled', true);
                        serialAddRowClone.children().children('#newSerialBatch').prop('disabled', true);
                    }
                    serialAddRowClone.insertBefore('#serialSample');
                }
                var dDate = new Date(Date.parse($('#deliveryDate').val()));
                for (j = 0; j < i; j++) {
                    $('#sEDate' + j).datepicker({
                        onRender: function(date) {
                            return date.valueOf() < dDate.valueOf() ? ((U_DATE_R_OVERRIDE) ? '' : 'disabled') : '';
                        }
                    });
                }
            }
            $('#addPiProductsModal').modal('show');
        }
    });
///////////Add batch & serial products //////////////
    $(document).on('keyup', '#newBatchQty,#newBatchWarrenty', function() {
        if ((this.id == 'newBatchQty') && ((!$.isNumeric($('#newBatchQty').val()) || $('#newBatchQty').val() <= 0))) {
            $('#newBatchQty').val('');
        }
        if ((this.id == 'newBatchWarrenty') && ((!$.isNumeric($('#newBatchWarrenty').val()) || $('#newBatchWarrenty').val() < 0))) {
            $('#newBatchWarrenty').val('');
        }
    });
    $(document).on('keyup', '.serialWarrenty', function() {
        if (!$.isNumeric($(this).val()) || $(this).val() < 0) {
            $(this).val('');
        }
    });
    $('#addBatchItem').on('click', function() {

        if ($('#newBatchNumber').val() == null || $('#newBatchQty').val() == '' || $('#newBatchNumber').val().trim() === '') {
            p_notification(false, eb.getMessage('ERR_GRN_BATCH_INFO'));
        } else {
            var newBCode = $('#newBatchNumber').val();
            var newBQty = $('#newBatchQty').val();
            var newBMDate = $('#newBatchMDate').val();
            var newBEDate = $('#newBatchEDate').val();
            var newBWrnty = $('#newBatchWarrenty').val();
            if ((toFloat(batchCount) + toFloat(newBQty)) > selectedProductQuantity) {
                p_notification(false, eb.getMessage('ERR_GRN_BATCH_COUNT'));
            } else {
                if (batchProducts[newBCode] != null) {
                    p_notification(false, eb.getMessage('ERR_GRN_BATCH_CODE'));
                } else {
                    if ((toFloat(batchCount) + toFloat(newBQty)) == selectedProductQuantity) {
                        $('#proBatchAddNew').addClass('hidden');
                    }
                    batchCount += toFloat(newBQty);
                    batchProducts[newBCode] = new batchProduct(newBCode, newBQty, newBMDate, newBEDate, newBWrnty, null);
                    var cloneBatchAdd = $($('#proBatchSample').clone()).attr('id', newBCode).removeClass('hidden').addClass('tempProducts');
                    cloneBatchAdd.children('#batchNmbr').html(newBCode);
                    cloneBatchAdd.children('#batchQty').html(newBQty);
                    cloneBatchAdd.children('#batchMDate').html(newBMDate);
                    cloneBatchAdd.children('#batchEDate').html(newBEDate);
                    cloneBatchAdd.children('#batchW').html(newBWrnty);
                    cloneBatchAdd.insertBefore('#proBatchSample');
                    clearAddBatchProductRow();
                }
            }
        }
    });
    $('#addPiProductsModal').on('change', 'input.serialNumberList', function() {
        var matchSerialNumberCount = 0;
        var typedVal = $(this).val();
        var serialCheckStatus = false;
        eb.ajax({
            type: 'POST',
            url: BASE_URL + '/productAPI/checkSerialProductCodeExists',
            data: {serialCode: typedVal, productCode: locationProducts[selectedProduct].pC, },
            success: function(respond) {
                if (respond.data == true) {
                    serialCheckStatus = true;
                }
            },
            async: false
        });
        if (serialCheckStatus == true) {
            p_notification(false, eb.getMessage('ERR_GRN_SERIAL_CODE'));
            $(this).val('');
        } else {
            $('input.serialNumberList').each(function() {
                if ($(this).val() == typedVal) {
                    matchSerialNumberCount++;
                }
            });
            if (matchSerialNumberCount > 1) {
                p_notification(false, eb.getMessage('ERR_GRN_SAME_SERIAL_CODE'));
                $(this).val('');
            }
        }
    });
    $('#addPiProductsModal').on('change', 'input.serialBatchList,#newSerialBatch', function() {
        if (!batchProducts.hasOwnProperty($(this).val())) {
            p_notification(false, eb.getMessage('ERR_GRN_ENTER_BATCHNUM'));
            $(this).val('');
        } else {
            var enteredBatchCode = $(this).val();
            var sameBatchCodeCount = 0;
            $('input.serialBatchList,#newSerialBatch').each(function() {
                if ($(this).val() == enteredBatchCode) {
                    sameBatchCodeCount++;
                }
            });
            if (sameBatchCodeCount > batchProducts[enteredBatchCode].bQty) {
                p_notification(false, eb.getMessage('ERR_GRN_ENTER_PRODLIMIT'));
                $(this).val('');
            }
        }
    });
    $('#savePiProducts').on('click', function(event) {
        var serialEmpty = false;
        $('input.serialNumberList').each(function() {
            if ($(this).val() == '') {
                serialEmpty = true;
                return false;
            }
        });
        if (serialEmpty == true) {
            p_notification(false, eb.getMessage('ERR_GRN_FILL_SERIALNUM'));
            return false;
        }

        var batchEmpty = false;
        if (locationProducts[selectedProduct].bP == 1 && locationProducts[selectedProduct].sP == 0) {
            if (batchCount < selectedProductQuantity) {
                batchEmpty = true;
            }
        }
        if (batchEmpty == true) {
            p_notification(false, eb.getMessage('ERR_GRN_ADD_BATCH_DETAILS'));
            return false;
        }
        var serialBatchEmpty = false;
        if ((locationProducts[selectedProduct].bP == 1) && (locationProducts[selectedProduct].sP == 1)) {
            $('input.serialBatchList').each(function() {
                if ($(this).val() == '') {
                    serialBatchEmpty = true;
                    return false;
                }
            });
        }
        if (serialBatchEmpty == true) {
            p_notification(false, eb.getMessage('ERR_INVENTADJUS_FILLBATCHNUM'));
            return false;
        }
        if (serialEmpty == false && batchEmpty == false) {
            $('#addNewSerial > tr').each(function() {
                var sNmbr = $(this).children().children('#newSerialNumber').val();
                var sWrnty = $(this).children().children('#newSerialWarranty').val();
                var sBt = $(this).children().children('#newSerialBatch').val();
                var sED = $(this).children().children('.serialEDate').val();
                var sWrntyType = $(this).children().children('#newSerialWarrantyType').val();
                if (sNmbr != '') {
                    serialProducts[sNmbr] = new serialProduct(sNmbr, sWrnty, sBt, sED, null,sWrntyType);
                }
            });
            $('#addPiProductsModal').modal('hide');
            addNewProductRow(batchProducts, serialProducts);
            clearModalWindow();
            $('#itemCode').focus();
        } else {
            p_notification(false, eb.getMessage('ERR_GRN_BATCH_PRODDETAILS'));
        }

    });
    $('#addPiProductsModal').on('click', '.batchDelete', function() {
        var deleteBID = $(this).closest('tr').attr('id');
        var deleteBQty = batchProducts[deleteBID].bQty;
        batchCount -= toFloat(deleteBQty);
        delete(batchProducts[deleteBID]);
        $('#' + deleteBID).remove();
        $('#proBatchAddNew').removeClass('hidden');
    });
    $('#piProductTable').on('click', '.poRemoveItem', function() {
        var tr = $(this).parents("tr");
        var removeProductID = tr.attr('id');
        $('#' + removeProductID).remove();
    });
    $('#paymentTerm').on('change', function(e) {
        e.preventDefault();
        var days = 0;
        var i = parseInt($(this).val());
        switch (i) {
            case 2:
                days = 7;
                break;
            case 3:
                days = 14;
                break;
            case 4:
                days = 30;
                break;
            case 5:
                days = 60;
                break;
            case 6:
                days = 90;
                break;
            case 7:
                days = 21;
                break;
            case 8:
                days = 28;
                break;
            case 9:
                days = 45;
                break;
            case 10:
                days = 35;
                break;
            default:
                days = 0;
                break;
        }
        setDueDate(days);
    });
    function setDueDate(days) {
        var day = parseInt(days);
        var joindate = eb.convertDateFormat('#issueDate');
        joindate.setDate(joindate.getDate() + day);
        var dd = joindate.getDate() < 10 ? '0' + joindate.getDate() : joindate.getDate();
        var mm = (joindate.getMonth() + 1) < 10 ? '0' + (joindate.getMonth() + 1) : (joindate.getMonth() + 1);
        var y = joindate.getFullYear();
        var joinFormattedDate = eb.getDateForDocumentEdit('#deliveryDate', dd, mm, y);
        $("#deliveryDate").val(joinFormattedDate);
    }

    function getSupplierDetails(supplierID) {
        eb.ajax({
            type: 'POST',
            url: BASE_URL + '/supplierAPI/getSupplierDetails',
            data: {sID: supplierID}, success: function(respond) {
                $('#supplierCurrentBalance').val(parseFloat(respond.data.sOB).toFixed(2));
                $('#supplierCurrentCredit').val(parseFloat(respond.data.sCB).toFixed(2));
                if (respond.data.sP == 'null') {
                    $('#paymentTerm').val(1);
                } else {
                    $('#paymentTerm').val(respond.data.sP);
                }
                $('#paymentTerm').trigger('change');
            }
        });
    }

////////////////////////////////////////////////////

    function clone() {
        var sendStartingValue = quon;
        var originalQuontity = $('#qty').val();
        var poQuontity = $('.poAddItem').parents("tr").find("input[name='qty']").val();
        if (originalQuontity == "") {
            originalQuontity = poQuontity;
        }
        id = '#temp_' + idNumber;
        var quonnn = $(id).find('input[name=quontity]').val();
        var number = $(id).find('input[name=startNumber]').val();
        var startNumberStr = $(id).find('input[name=startNumber]').val();
        var prefix = $(id).find('input[name=prefix]').val();
        var pCode = locationProducts[selectedProduct].pC;
        if (quonnn == '') {
            quonnn = 0;
        }
        if (number == '') {
            p_notification(false, eb.getMessage('NO_START_NO'));
            return false;
        }
        if (number == 0) {
            p_notification(false, eb.getMessage('START_NO_ZERO'));
            return false;
        }
        var validationValue = true;
        validationValue = validation(number, prefix, quonnn, startNumberStr, pCode, sendStartingValue, id);
        if (validationValue == true) {

            if (toFloat(quonnn) <= originalQuontity && toFloat(quon) + toFloat(quonnn) <= originalQuontity && quonnn !== 0) {
                quon = toFloat(quon) + toFloat(quonnn);
                if (toFloat(quonnn) == originalQuontity || toFloat(quon) == originalQuontity) {
                    $(id).find('input[name=submit]').attr('disabled', true);
                    $(id).find('input[name=prefix]').attr('disabled', true);
                    $(id).find('input[name=startNumber]').attr('disabled', true);
                    $(id).find('input[name=quontity]').attr('disabled', true);
                    $(id).find('input[name=expireDateAutoFill]').attr('disabled', true);
                    $(id).find('input[name=batch_code]').attr('disabled', true);
                    $(id).find('input[name=mfDateAutoFill]').attr('disabled', true);
                    $(id).find('input[name=wrtyDays]').attr('disabled', true);
                    setValue(sendStartingValue, quonnn, id);
                }
                else {
                    var row = document.getElementById('temp_' + idNumber);
                    var table = document.getElementById('tempTbody');
                    var clone = row.cloneNode(true);
                    idNumber += 1;
                    clone.id = 'temp_' + idNumber;
                    table.appendChild(clone);
                    nextId = '#temp_' + idNumber;
                    $(nextId).addClass('tempy');
                    $(nextId).find('input[name=prefix]').val('');
                    $(nextId).find('input[name=startNumber]').val('');
                    $(nextId).find('input[name=quontity]').val('');
                    $(nextId).find('input[name=wrtyDays]').val('');
                    var issuDate = new Date(Date.parse($('#issueDate').val()));
                    $(nextId).find('input[name=expireDateAutoFill]').val('');
                    $(nextId).find('input[name=batch_code]').val('');
                    $(nextId).find('input[name=mfDateAutoFill]').val('');
                    var cloneMfD = $(nextId).find('input[name=mfDateAutoFill]').datepicker({
                        onRender: function(date) {
                            return date.valueOf() > issuDate.valueOf() ? ((U_DATE_R_OVERRIDE) ? '' : 'disabled') : '';
                        }
                    }).on('changeDate', function(ev) {
                        cloneMfD.hide();
                    }).data('datepicker');
                    $(id).find('input[name=submit]').attr('disabled', true);
                    $(id).find('input[name=prefix]').attr('disabled', true);
                    $(id).find('input[name=startNumber]').attr('disabled', true);
                    $(id).find('input[name=quontity]').attr('disabled', true);
                    $(id).find('input[name=expireDateAutoFill]').attr('disabled', true);
                    $(id).find('input[name=batch_code]').attr('disabled', true);
                    $(id).find('input[name=mfDateAutoFill]').attr('disabled', true);
                    $(id).find('input[name=wrtyDays]').attr('disabled', true);
                    if (locationProducts[selectedProduct].sP == 1 && locationProducts[selectedProduct].bP == 1) {
                        $(nextId).find('input[name=mfDateAutoFill]').attr('disabled', false);
                        $(nextId).find('input[name=batch_code]').attr('disabled', false);
                    }
                    var cloneExpD = $(nextId).find('input[name=expireDateAutoFill]').datepicker({
                        onRender: function(date) {
                            return date.valueOf() < issuDate.valueOf() ? ((U_DATE_R_OVERRIDE) ? '' : 'disabled') : '';
                        }
                    }).on('changeDate', function(ev) {
                        cloneExpD.hide();
                    }).data('datepicker');
                    setValue(sendStartingValue, quonnn, id);
                }
            }
            else {
                p_notification(false, eb.getMessage('WRNG_QUON_PI'));
                return false;
            }
        }
    }

    $('#tempTbody').on('click', '#submit_serial_number', function()
    {
        if ((locationProducts[selectedProduct].bP == 1) && (locationProducts[selectedProduct].sP == 1)) {
            if (!$(this).parents("tr").find("input[name='batch_code']").val()) {
                p_notification(false, eb.getMessage('ERR_NO_GRN_ADD_SERIAL_BATCH_DETAILS'));
                return false;
            }
        }
        if(isNaN($(this).parents('tr').find("input[name='startNumber']").val())){
            p_notification(false, eb.getMessage('ERR_SERIAL_START_NO'));
            $(this).parents('tr').find("input[name='startNumber']").val("");
            return false;
        }

        clone();
        if ($('#submit_serial_number').attr('disabled')) {
            $(this).parent().parent().find('#expire_date_autoFill').removeClass('white_bg');
            $(this).parent().parent().find('#mf_date_autoFill').removeClass('white_bg');
        }
    });
    function validation(startNumber, prefix, bQuntity, startNumberStr, productCode, sendStartingValue, id) {
        var strNumberLength = startNumberStr.length;
        var matchSerialNumberCount = 0;
        var serialCheckStatus = false;
        eb.ajax({
            type: 'POST',
            url: BASE_URL + '/productAPI/checkSerialProductCode',
            data: {startingNumber: startNumber, prefix: prefix, serialQuntity: bQuntity, strNumberLength: strNumberLength, productCode: productCode, },
            success: function(respond) {
                if (respond.data == true) {
                    serialCheckStatus = true;
                }
            },
            async: false
        });
        if (serialCheckStatus == true) {
            p_notification(false, eb.getMessage('ERR_GRN_SERIAL_CODE'));
            return false;
        }
        else {
            $('input.serialNumberList').each(function() {
                for (i = startNumber; i < startNumber + bQuntity; i++) {
                    if (prefix) {
                        var startingValue = prefix + pad(i, strNumberLength);
                        if ($(this).val() == startingValue) {
                            matchSerialNumberCount++;
                        }
                    } else {
                        var startingValue = pad(i, strNumberLength);
                        if ($(this).val() == startingValue) {
                            matchSerialNumberCount++;
                        }
                    }

                }

            });
            if (matchSerialNumberCount >= 1) {
                p_notification(false, eb.getMessage('ERR_GRN_SAME_SERIAL_CODE'));
                return false;
            }
            else {
                return true;
            }
        }
    }


    function setValue(quon, quonnn, id) {
        var abc = 0;
        var bQuntity = toFloat(quonnn);
        var max = toFloat(quon) + toFloat(quonnn);
        var prefix = $(id).find('input[name=prefix]').val();
        var startNumber = parseInt($(id).find('input[name=startNumber]').val());
        var startNumberString = $(id).find('input[name=startNumber]').val();
        var expire_date = $(id).find('input[name=expireDateAutoFill]').val();
        var mf_date = $(id).find('input[name=mfDateAutoFill]').val();
        var wrnty = $(id).find('input[name=wrtyDays]').val();
        var wrntyType = $(id).find('#wrtyType').val();
        if (locationProducts[selectedProduct].sP == 1 && locationProducts[selectedProduct].bP == 1) {
            var batchNumber = $(id).find('input[name=batch_code]').val();
            batchProducts[batchNumber] = new batchProduct(batchNumber, bQuntity, mf_date, expire_date, wrnty, null);
            if (startNumber) {
                $('#addNewSerial tr').each(function() {
                    if (abc >= toFloat(quon) && abc < toFloat(max)) {
                        var numberLength = startNumberString.length;
                        var startingValue = pad(startNumber, numberLength);
                        if (prefix == '') {
                            $(this).find('input[name=serialCode]:first').val(startingValue);
                            $(this).find('input[name=serialEDate]').val(expire_date);
                            $(this).find('input[name=batchCode]').val(batchNumber);
                            $(this).find('input[name=serialWarronty]').val(wrnty);
                        } else if (prefix != '') {
                            var num = prefix + startingValue;
                            $(this).find('input[name=serialCode]:first').val(num);
                            $(this).find('input[name=serialEDate]').val(expire_date);
                            $(this).find('input[name=batchCode]').val(batchNumber);
                            $(this).find('input[name=serialWarronty]').val(wrnty);
                        }
                        startNumber += 1;
                    }
                    abc++;
                });
            }
        }
        else {
            if (startNumber) {
                $('#addNewSerial tr').each(function() {
                    if (abc >= toFloat(quon) && abc < toFloat(max)) {
                        var numberLength = startNumberString.length;
                        var startingValue = pad(startNumber, numberLength);
                        if (prefix == '') {
                            $(this).find('input[name=serialCode]:first').val(startingValue);
                            $(this).find('input[name=serialEDate]').val(expire_date);
                            $(this).find('input[name=serialWarronty]').val(wrnty);
                            $(this).find("#newSerialWarrantyType").val(wrntyType).change();
                        } else if (prefix != '') {
                            var num = prefix + startingValue;
                            $(this).find('input[name=serialCode]:first').val(num);
                            $(this).find('input[name=serialEDate]').val(expire_date);
                            $(this).find('input[name=serialWarronty]').val(wrnty);
                            $(this).find("#newSerialWarrantyType").val(wrntyType).change();
                        }
                        startNumber += 1;
                    }
                    abc++;
                });
            }
        }
    }

    function pad(n, width, z) {
        z = z || '0';
        n = n + '';
        return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
    }


///////DatePicker\\\\\\\

    var myDate = new Date();
    var day = myDate.getDate() < 10 ? '0' + myDate.getDate() : myDate.getDate();
    var month = (myDate.getMonth() + 1) < 10 ? '0' + (myDate.getMonth() + 1) : (myDate.getMonth() + 1);
    var prettyDate = myDate.getFullYear() + '-' + ((myDate.getMonth() < 10 ? '0' : '') + (myDate.getMonth() + 1)) + '-' + (myDate.getDate() < 10 ? '0' : '') + myDate.getDate();
    $("#issueDate").val(eb.getDateForDocumentEdit('#issueDate', day, month, myDate.getFullYear()));
    var nowTemp = new Date();
    var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);
    var checkin = $('#deliveryDate').datepicker({
        onRender: function(date) {
            return date.valueOf() < now.valueOf() ? ((U_DATE_R_OVERRIDE) ? '' : 'disabled') : '';
        }
    }).on('changeDate', function(ev) {
        checkin.hide();
    }).data('datepicker');
    var batchMD = $('#newBatchMDate').datepicker({
        onRender: function(date) {
            return date.valueOf() > now.valueOf() ? ((U_DATE_R_OVERRIDE) ? '' : 'disabled') : '';
        }
    }).on('changeDate', function(ev) {
        batchMD.hide();
    }).data('datepicker');
    var batchED = $('#newBatchEDate').datepicker({
        onRender: function(date) {
            return date.valueOf() < now.valueOf() ? ((U_DATE_R_OVERRIDE) ? '' : 'disabled') : '';
        }
    }).on('changeDate', function(ev) {
        batchED.hide();
    }).data('datepicker');
    var sED = $('.serialEDate').datepicker({
        onRender: function(date) {
            return date.valueOf() < now.valueOf() ? ((U_DATE_R_OVERRIDE) ? '' : 'disabled') : '';
        }
    }).on('changeDate', function(ev) {
        sED.hide();
    }).data('datepicker');
    var issueD = $('#issueDate').datepicker({
        onRender: function(date) {
            return date.valueOf() < now.valueOf() ? ((U_DATE_R_OVERRIDE) ? '' : 'disabled') : '';
        }
    }).on('changeDate', function(ev) {
        issueD.hide();
    }).data('datepicker');
/////EndOFDatePicker\\\\\\\\\

    function setDataForLocation(locationID) {
        clearProductScreen();
        selectedLocation = locationID;
        var documentType = 'paymentVoucher';
        loadDropDownFromDatabase('/productAPI/search-location-products-for-dropdown', locationID, 0, '#itemCode', '', '', documentType);
        eb.ajax({
            type: 'POST',
            url: BASE_URL + '/expense-purchase-invoice-api/get-expense-purchase-invoice-reference-for-location',
            data: {locationID: locationID},
            success: function(respond) {
                if (respond.status == false) {
                    p_notification(false, respond.msg);
                    $('#piNo').val('');
                    $('#refNotSet').modal('show');
                }
                else if (respond.status == true) {
                    $('#piNo').val(respond.data.refNo);
                    $('#locRefID').val(respond.data.locRefID);
                }
            },
            async: false
        });
//        getProductsFromLocation(selectedLocation);
        if (TAXSTATUS == false) {
            p_notification('info', eb.getMessage('ERR_GRN_ENABLE_TAX'));
        }
        $('#productAddOverlay').removeClass('overlay');
    }

    function setProductsView(viewProductID) {
        $('.tempView').remove();
        $('#batchProductsView').addClass('hidden');
        $('#serialViewScreen').addClass('hidden');
        if (!jQuery.isEmptyObject(products[viewProductID].bProducts)) {
            $('#batchProductsView').removeClass('hidden');
            $('#batchCodeView').html(products[viewProductID].pCode);
            $('#batchQuantityView').html(products[viewProductID].pQuantity);
            for (var b in products[viewProductID].bProducts) {
                var viewBPr = products[viewProductID].bProducts[b];
                var cloneBatchProductView = $($('#proBatchViewSample').clone()).attr('id', '').removeClass('hidden').addClass('tempView');
                cloneBatchProductView.children('#batchNmbrView').html(viewBPr.bCode);
                cloneBatchProductView.children('#batchQtyView').html(viewBPr.bQty);
                cloneBatchProductView.children('#batchMDateView').html(viewBPr.mDate);
                cloneBatchProductView.children('#batchEDateView').html(viewBPr.eDate);
                cloneBatchProductView.children('#batchWView').html(viewBPr.warnty);
                cloneBatchProductView.insertBefore('#proBatchViewSample');
            }
        }
        if (!jQuery.isEmptyObject(products[viewProductID].sProducts)) {
            $('#serialViewScreen').removeClass('hidden');
            $('#piserialCodeView').html(products[viewProductID].pCode);
            $('#piSerialQuantityView').html(products[viewProductID].pQuantity);
            for (var h in products[viewProductID].sProducts) {
                var viewSPr = products[viewProductID].sProducts[h];
                var cloneSerialProductView = $($('#serialViewSample').clone()).attr('id', '').removeClass('hidden').addClass('tempView');
                cloneSerialProductView.children('#serialNumberView').html(viewSPr.sCode);
                cloneSerialProductView.children('#serialWarrentyView').html(viewSPr.sWarranty);
                cloneSerialProductView.children('#serialBIDView').html(viewSPr.sBCode);
                cloneSerialProductView.children('#serialExDView').html(viewSPr.sEdate);
                cloneSerialProductView.insertBefore('#serialViewSample');
            }
        }
    }

    function setTotalTax() {
        totalTaxList = {};
        for (var k in products) {
            if (products[k].pTax != null) {
                for (var l in products[k].pTax.tL) {
                    if (totalTaxList.hasOwnProperty(l)) {
                        var curTA = totalTaxList[l].tA;
                        var newTA = (toFloat(curTA) + toFloat(products[k].pTax.tL[l].tA));
                        totalTaxList[l].tA = newTA.toFixed(2);
                    } else {
                        totalTaxList[l] = {tN: products[k].pTax.tL[l].tN, tP: products[k].pTax.tL[l].tP, tA: products[k].pTax.tL[l].tA};
                    }
                }
            }
        }

        var totalTaxHtml = "";
        for (var t in totalTaxList) {
            totalTaxHtml += totalTaxList[t].tN + "(" + totalTaxList[t].tP + "%)&nbsp" + accounting.formatMoney((totalTaxList[t].tA)) + "<br>";
        }
        $('#totalTaxShow').html(totalTaxHtml);
    }
    function setPiTotalCost() {
        var piTotal = 0;
        for (var i in products) {
            piTotal += toFloat(products[i].pTotal);
        }
        var tempNonItemTotal = 0;
        var tempTotal = piTotal;
        var tempSubTotal = piTotal;
        var nonItemDetails = getNonItemProductList();

        for (j = 0; j < (nonItemDetails.length); j++) {
            tempNonItemTotal += accounting.unformat(nonItemDetails[j]['totalPrice']);
        }
        var tempAllSubTotal = parseFloat(tempSubTotal) + parseFloat(tempNonItemTotal);
        $('#subtotal').html(accounting.formatMoney(tempAllSubTotal));
        var deliveryAmount = $('#deliveryCharge').val();
        tempAllSubTotal += toFloat(deliveryAmount);
        $('#finaltotal').html(accounting.formatMoney(tempAllSubTotal));
    }
    function addNewProductRow(batchProducts, serialProducts) {
        var iC = $('#itemCode').data('PC');
        var pT = $('#itemCode').data('PT');
        var iN = $('#itemCode').data('PN');
        var uPrice = accounting.formatMoney($('#unitPrice').val());
        var uFPrice = $('#unitPrice').val();
        var qt = $('#qty').val();
        if (qt == 0) {
            qt = 0;
        }
        var uomqty = $("#qty").siblings('.uomqty').val();
        var nTotal = $('#addNewTotal').html();
        var calNTotal = accounting.unformat(nTotal);
        var gD = $('#piDiscount').val();
        if ($('#piDiscount').val() == '')
            gD = 0;
        var locationProductID = locationProducts[selectedProduct].lPID;
        var itemExTypeText = $('#itemExType').find('optgroup option:selected').text();
        itemExTypeText = (itemExTypeText) ? itemExTypeText : '-';
        var itemExType = $('#itemExType').val();
        selectedProductQuantity = $('#qty').val();
        var newTrID = 'tr_' + locationProductID;
        var clonedRow = $($('#preSetSample').clone()).attr('id', newTrID).addClass('addedProducts');
        clonedRow.children('#code').html(iC + ' - ' + iN);
        clonedRow.children('#quantity').html(uomqty);
        clonedRow.children('#unit').html(uPrice);
        clonedRow.children('#ex-type').html(itemExTypeText);
        clonedRow.children().children('#uomName').html(locationProducts[selectedProduct].uom[selectedProductUom].uA);
        clonedRow.children('#disc').html(gD);
        clonedRow.children('#ttl').html(nTotal);
        if (locationProducts[selectedProduct].bP == 0 && locationProducts[selectedProduct].sP == 0) {
            clonedRow.children().children('#viewSubProducts').addClass('hidden');
        }
        var clonedTax = $($('#addNewTaxDiv').clone()).attr('id', '');
        clonedTax.children('#addNewTax').children('#taxApplied').attr('id', '');
        clonedTax.children('#addNewTax').attr('id', '');
        clonedTax.children('#addTaxUl').children().removeClass('tempLi');
        clonedTax.children('#addTaxUl').children().children().removeClass('addNewTaxCheck');
        clonedTax.children('#addTaxUl').children().children().attr('disabled', true);
        clonedTax.children('#addTaxUl').children('#sampleLi').remove();
        clonedTax.children('#addTaxUl').attr('id', '');
        clonedTax.find('#toggleSelection').addClass('hidden');
        clonedRow.children('#tax').html(clonedTax);
        clonedRow.removeClass('hidden');
        clonedRow.insertBefore('#preSetSample');
        var newProduct = locationProducts[selectedProduct];
        products.push(new product(newProduct.lPID, selectedProduct, iC, iN, qt, gD, toFloat(uFPrice).toFixed(2), selectedProductUom, calNTotal, currentItemTaxResults, batchProducts, serialProducts, pT, itemExType));
        setPiTotalCost();
        setTotalTax();
        clearAddNewRow();
        $('#itemExType').val(0).trigger('change').empty().selectpicker('refresh');
        $('#').val(0).trigger('change').empty().selectpicker('refresh');
        clearModalWindow();
    }
    function clearModalWindow() {
        $('.tempProducts').remove();
        $('.cloneSerials').remove();
        $('#piProductCodeBatch').html('');
        $('#piProductQuantityBatch').html('');
        $('#piProductCodeSerial').html('');
        $('#piProductQuantitySerial').html('');
        $('#batchAddScreen').addClass('hidden');
        $('#serialAddScreen').addClass('hidden');
        $('#addNewSerial input[type=text]').val('');
        $('#proBatchAddNew').removeClass('hidden');
        batchCount = 0;
        batchProducts = {};
        serialProducts = {};
    }

    function clearProductScreen() {
        products = new Array();
        clearAddBatchProductRow();
        clearAddNewRow();
        clearModalWindow();
        setPiTotalCost();
        setTotalTax();
        $('.addedProducts').remove();
        locationProducts = '';
        locProductCodes = '';
        locProductNames = '';
        selectedProduct = '';
        selectedProductQuantity = '';
        selectedProductUom = '';
        batchCount = '';
    }



    $('#non-item-table').on('click', '.non-item-desc',function() {
        var $element = $(this).closest('tr');
        var taxes = TAXES;
        var taxStatus = TAXSTATUS;

        if (taxStatus) {
            $element.find('.nonItemTaxApplied').removeClass('glyphicon glyphicon-check');
            $element.find('.nonItemTaxApplied').addClass('glyphicon glyphicon-unchecked');
            $element.find('.tempLi').remove();

            for (var i in taxes) {
                if (taxes[i].tS == 1) {
                    var clonedLi = $($('.samplennItemLi:first').clone()).removeClass('hidden samplennItemLi').attr('id', 'linon_' + i).addClass("tempLi");
                    clonedLi.children(".taxChecks").attr('id', taxes[i].tID + '_' + i).prop('checked', false).addClass('addNewTaxCheck');
                    clonedLi.children(".taxName").html('&nbsp&nbsp' + taxes[i].tN + '&nbsp&nbsp' + taxes[i].tP + '%').attr('for', taxes[i].tID + '_' + i);
                    clonedLi.insertBefore('.samplennItemLi');
                }
            }
        }
    });


    $('#non-item-tbody').on('click', '.add-non-item', function() {
        var descript = $(this).parents('tr').find('input[name=non-item-description]').val();
        var nonItemQty = $(this).parents('tr').find('input[name=non-item-qty]').val();
        var nonItemUnitPrice = $(this).parents('tr').find('input[name=non-item-unit-prc]').val();
        var nonItemTotalPrice = $(this).parents('tr').find('#addNewNonItemTotal').text();
        var nonItemExType = $(this).parents('tr').find('.non-item-ex-type-selectbox');
        var nonItemExTypeId = nonItemExType.val();
        var nonItemExTypeText = nonItemExType.find('optgroup option:selected').text();
        var data = {
            descript: descript,
            nonItemQty: nonItemQty,
            nonItemUnitPrice: nonItemUnitPrice,
            nonItemTotalPrice: nonItemTotalPrice,
            nonItemExType: nonItemExTypeId
        };
        if (checkNonItemValidation(data))
        {
            if(!($(this).parent('td').hasClass('editedRow'))){
                cloneNonItem(nonItemRowId);
                nonItemRowId++;
            }

            $(this).parents('tr').find('.pv-non-item-edit').removeClass('hidden');
            $(this).parents('tr').find('input[name=non-item-description]').attr('disabled', true);
            $(this).parents('tr').find('input[name=non-item-qty]').attr('disabled', true);
            $(this).parents('tr').find('input[name=non-item-unit-prc]').attr('disabled', true);
            $(this).parents('tr').find('.non-item-tax').attr('disabled', true);
            $(this).parents('tr').find('.add-non-item').parent('td').addClass('hidden');
            $(this).parents('tr').find('#removeNonItem').removeClass('hidden');
            $(this).parents('tr').addClass('non-item');
            var $selectPicker = $(this).parents('tr').find('.non-item-ex-type-selectbox');
            $selectPicker.prop('disabled', true).selectpicker('refresh');
            $selectPicker.removeClass('ex-type');

        }
        setPiTotalCost();
    });

    $('#non-item-tbody').on('click', '.remove-non-item', function() {
        deleteNonItem($(this).parents('tr').attr('id'));
    });
    function deleteNonItem(rowID) {
        $('#' + rowID).remove();
        setPiTotalCost();
    }

    $('#item-add-show').on('change', function() {
        if (this.checked) {
            $('#piProductTable').removeClass('hidden');
        }
        else {
            var incrValue = 0;
            $('#form_row tr').each(function() {
                incrValue++;
            });
            if (incrValue == 1) {
                $('#piProductTable').addClass('hidden');
            }
        }
    });

    $('.ex-type').selectpicker('refresh');

    $('#line-expense-type-add').on('change', function() {
        if (this.checked) {
            $('.ex-type').prop('disabled', false);
            $('.ex-type').selectpicker('refresh');
        } else {
            $('.ex-type').prop('disabled', true);
            $('.ex-type').selectpicker('refresh');
        }
    });

    function addNonInventItemByEdit(value, incrementKey, hasLinkedPayments, objLength)
    {
        var lineID = "non-item-row-"+incrementKey;
        var row = $('#'+lineID);
        row.find('input[name=non-item-description]').val(value.paymentVoucherNonItemProductDiscription).attr('disabled',true);
        row.find('input[name=non-item-qty]').val(value.paymentVoucherNonItemProductQuantity).attr('disabled',true);
        row.find('input[name=non-item-unit-prc]').val(value.paymentVoucherNonItemProductUnitPrice).attr('disabled',true);
        row.find('#addNewNonItemTotal').text(value.paymentVoucherNonItemProductTotalPrice);
        row.find('.non-item-ex-type-selectbox').
                        append($("<option></option>").
                                attr("value", value.paymentVoucherNonItemProductExpenseTypeFormat+'_'+value.paymentVoucherNonItemProductExpenseTypeID).
                                text(value.paymentVoucherNonItemProductExpenseType));
        row.find('.non-item-ex-type-selectbox').val(value.paymentVoucherNonItemProductExpenseTypeFormat+'_'+value.paymentVoucherNonItemProductExpenseTypeID);
        row.find('.non-item-ex-type-selectbox').selectpicker('render');
        row.find('.add-non-item').parent('td').addClass('hidden');
        row.find('.non-item-ex-type-selectbox option').addClass('hidden');
        row.find('#removeNonItem').removeClass('hidden');
        row.addClass('non-item edited');
        row.find('.pv-non-item-edit').removeClass('hidden');
        var $selectPicker = row.find('.non-item-ex-type-selectbox');
        $selectPicker.prop('disabled', true).selectpicker('refresh');
        $selectPicker.removeClass('ex-type');
        if(value.tax != null){
            row.find('#nonItemTaxApplied').removeClass('glyphicon-unchecked');
            row.find('#nonItemTaxApplied').addClass('glyphicon-check');
            existTaxArray[incrementKey] = value.tax;
            var taxes = TAXES;

            for (var i in taxes) {
                    var clonedLi = row.find('.samplennItemLi:first').clone().removeClass('hidden samplennItemLi').attr('id', 'linon_' + i).addClass("tempLi");
                    if(value.tax[i] != null){
                        clonedLi.children(".taxChecks").attr('id', taxes[i].tID + '_' + i).prop('checked', true).addClass('addNewTaxCheck');
                    } else {
                        clonedLi.children(".taxChecks").attr('id', taxes[i].tID + '_' + i).prop('checked', false).addClass('addNewTaxCheck');
                    }
                    clonedLi.children(".taxName").html('&nbsp&nbsp' + taxes[i].tN + '&nbsp&nbsp' + taxes[i].tP + '%').attr('for', taxes[i].tID + '_' + i);
                    clonedLi.insertBefore(row.find('.samplennItemLi'));
            }
        }
        row.find('.non-item-tax').attr('disabled', true);
        row.attr('data-lienid', value.productID);


        incrementKey++;
        nonItemRowId = toFloat(incrementKey) + 1;
        if (hasLinkedPayments == true) {
            row.find('.pv-non-item-edit').addClass('hidden');
            row.find('.remove-non-item').addClass('hidden');
        }

        if (!(hasLinkedPayments == true && incrementKey > objLength)) {
            cloneNonItem(incrementKey);
        } else {
            row.find('.pv-non-item-edit').addClass('hidden');
            row.find('.remove-non-item').addClass('hidden');
        }


        setPiTotalCost();
    }

    function loadDataToPv(PVID) {
        eb.ajax({
            type: 'POST',
            url: BASE_URL + '/expense-purchase-invoice-api/get-data-for-edit',
            data: {pVID: PVID},
            success: function(respond) {
                if(!respond.status){
                    window.location.assign(BASE_URL + "/expense-purchase-invoice/list")
                    p_notification(false, respond.msg);
                } else {
                    $('.createPoDiv').addClass('hidden');
                    $('.updatePoDiv').removeClass('hidden');
                    $('.updatePoDiv_view').removeClass('hidden');
                    if (!$.isEmptyObject(respond.data.uploadedAttachments)) {
                        uploadedAttachments = respond.data.uploadedAttachments;
                    }

                    $incKey = Object.keys(respond.data.nonItemProduct).length;
                    var hideId = 'non-item-row-'+$incKey;
                    linkedPayments = respond.data.hasLinkedPayments;
                    // console.log('#non-item-table #'+hideId);
                    if (respond.data.hasLinkedPayments == true) {
                        $('#deliveryChargeEnable').attr('disabled', true);
                        $('#showTax').attr('disabled', true);
                        $('#supplier').attr('disabled', true);
                        $('#paymentTerm').attr('disabled', true);
                        $('#dimensionView').attr('disabled', true);
                        $('#item-add-show').attr('disabled', true);
                        $('#supplierReference').attr('disabled', true);
                        $('#viewUploadedFiles').attr('disabled', true);
                        $('#voucherType').attr('disabled', true);
                        $('#authorType').attr('disabled', true);
                        $('#glAccount').attr('disabled', true);
                    } else {
                        $('#deliveryChargeEnable').attr('disabled', false);
                        $('#showTax').attr('disabled', false);
                        $('#supplier').attr('disabled', false);
                        $('#paymentTerm').attr('disabled', false);
                        $('#dimensionView').attr('disabled', false);
                        $('#item-add-show').attr('disabled', false);
                        $('#supplierReference').attr('disabled', false);
                        $('#viewUploadedFiles').attr('disabled', false);
                        $('#voucherType').attr('disabled', false);
                        $('#authorType').attr('disabled', false);
                        $('#glAccount').attr('disabled', false);
                    }

                    $('#dimension_div').removeClass('hidden');
                    var pVData = respond.data;
                    if(pVData.pVSID != null){
                        $('#authorType').val(1);
                        $('#authorType').selectpicker('render');
                        $('#supplier').empty().
                            append($("<option></option>").
                                    attr("value", pVData.pVSID).
                                    text(pVData.pVSN));
                        $('#supplier').val(pVData.pVSID);
                        $('#supplier').selectpicker('render');
                    } else {
                        $('#authorType').val(2);
                        $('#authorType').selectpicker('render');
                        $('#glAccount').empty().
                            append($("<option></option>").
                                    attr("value", pVData.supAccountID).
                                    text(pVData.supAccName));
                        $('#glAccount').val(pVData.supAccountID);
                        $('#glAccount').selectpicker('render');

                    }

                    if (!$.isEmptyObject(respond.data.dimensionData)) {
                        dimensionData = respond.data.dimensionData;
                    }
                    
                    $("#retrieveLocation").empty().
                        append($("<option></option>").
                                attr("value", pVData.pVLocID).
                                text(pVData.pVRL));
                    $('#retrieveLocation').val(pVData.pVLocID);
                    $('#retrieveLocation').prop('disabled', true);
                    $('#retrieveLocation').selectpicker('render');
                    setDataForLocation(pVData.pVLocID);
                    $('#piNo').val(pVData.pVCd);
                    $('#deliveryDate').val(pVData.pVDD);
                    $('#supplierReference').val(pVData.pVSR);
                    $('#issueDate').val(pVData.pVIssueD);
                    $('#comment').val(pVData.pVC);

                    if (pVData.paymentVoucherExpenseType != null) {
                        $('#voucherType').val(pVData.paymentVoucherExpenseType);
                    } 

                    issueDte = pVData.pVIssueD;
                    $('#productAddOverlay').removeClass('overlay');
                    var incrementKey = 1;
                    nonItemDataSet = respond.data.nonItemProduct;
                    $.each(respond.data.nonItemProduct, function(key, value){
                        addNonInventItemByEdit(value, incrementKey, respond.data.hasLinkedPayments, $incKey);
                        incrementKey++;
                    });


                }

            },
            async: false
        });
    }

    if($('#editMode').val() == '1'){
        $('#issueDate').val(issueDte);
    }
    $('#non-item-tbody').on('click', '#editNonItemLine', function(){
        var idValue = $(this).parents('tr').attr('id');
        delete(existTaxArray[idValue.split('-')[3]]);
        $(this).parents('tr').find('.pv-non-item-edit').addClass('hidden');
        $(this).parents('tr').find('.pv-non-item-add').removeClass('hidden').addClass('editedRow');
        $(this).parents('tr').find('input[name=non-item-description]').attr('disabled',false);
        $(this).parents('tr').find('input[name=non-item-qty]').attr('disabled',false);
        $(this).parents('tr').find('input[name=non-item-unit-prc]').attr('disabled',false);
        $(this).parents('tr').find('.non-item-tax').attr('disabled', false);
        var lineID = $(this).parents('tr').attr('data-lienid');
        var $selectPicker = $(this).parents('tr').find('.non-item-ex-type-selectbox');
        $selectPicker.prop('disabled', false).selectpicker('refresh');
        $selectPicker.addClass('ex-type');

    });
});

function sendEmailToApprover(pvId, pvCode, approvers, token, flag, preview, callback) {
    if (flag) {
        eb.ajax({
            type: 'POST',
            url: BASE_URL + '/expense-purchase-invoice-api/sendApproverEmail',
            data: {
                pvId: pvId,
                pvCode: pvCode,
                approvers: approvers,
                token: token,
            },
            success: function(respond) {
                if (respond.status == true) {
                    p_notification(respond.status, respond.msg);
                    if (callback) {
                        callback(preview);
                    }
                } else {
                    p_notification(respond.status, respond.msg);
                }
            }

        });
    } else {
        callback(preview);
    }
}

function getNonItemProductList() {
    var i = 0;
    var nonItemProduct = new Array();
    $('#non-item-tbody tr.non-item').each(function() {
        var $tr = $(this);
        var nonItemTax = Array();
        var rowID = $(this).attr('id');

        $($tr.find('.taxChecks.addNewTaxCheck')).each(function() {
            if (this.checked) {
                var cliTID = this.id;
                var taxID = cliTID.split('_')[1];
                var rowID = cliTID.split('_')[0];
                nonItemTax.push(taxID);
            }
        });
        // if($(this).hasClass('edited') && existTaxArray[rowID.split('-')[3]] != undefined){
        //     $.each(existTaxArray[rowID.split('-')[3]], function(key, value){
        //         nonItemTax.push(value.paymentVoucherTaxID);
        //     });
        // }

        nonItemProduct[i] = {
            description: $(this).find('input[name=non-item-description]').val(),
            qty: 1,
            unitPrice: $(this).find('input[name=non-item-unit-prc]').val(),
            totalPrice: accounting.unformat($(this).find('#addNewNonItemTotal').text()),
            tax: nonItemTax,
            exType: getNonItemExpenseType($(this).find('.non-item-ex-type-selectbox').val()),
            exFormat: getNonItemExpenseFormat($(this).find('.non-item-ex-type-selectbox').val())
        };
        i++;
    });
    return nonItemProduct;
}

// expense type dopdown option list has merge with GLAccounts
// there vale has a prifix to identify whether it is a expense type or a GLAccount
// eg: exp_14 - for expense format
// eg: acc_14 - for GLAccount format
function getNonItemExpenseType(str){
    return str.split('_')[1];
}

// expense type dopdown option list has merge with GLAccounts
// there vale has a prifix to identify whether it is a expense type or a GLAccount
// eg: exp_14 - for expense format
// eg: acc_14 - for GLAccount format
function getNonItemExpenseFormat(str){
    return str.split('_')[0];
}

function checkNonItemValidation(data)
{
    if (data.descript == '' || data.descript == null) {
        p_notification(false, eb.getMessage('NON_ITEM_NOT_SELECT'));
        return false;
    } else if (data.nonItemUnitPrice == '' || data.nonItemUnitPrice == null) {
        p_notification(false, eb.getMessage('NON_ITEM_UNIT_PRICE_NOT_SET'));
        return false;
    } else if(isNaN(data.nonItemUnitPrice)) {
            p_notification(false, eb.getMessage('ERR_NUMBER_FORMET_PV'));
            return false;
    } else if(data.nonItemUnitPrice < 0) {
            p_notification(false, eb.getMessage('ERR_MINUS_UNIT_PRICE_PV'));
            return false;
    } else if (data.nonItemExType == '' || data.nonItemExType == null || data.nonItemExType == false) {
        p_notification(false, eb.getMessage('ERR_EXPENSE_TYPE_NO_SELECT'));
        return false;
    } else {
        return true;
    }
}

function cloneNonItem(nonItemRowId) {
    var row = document.getElementById('non-item-row-sample');
    var tableBody = document.getElementById('non-item-tbody');
    var clone = row.cloneNode(true);
    clone.id = 'non-item-row-' + nonItemRowId;
    var createdID = 'non-item-row-' + nonItemRowId;
    tableBody.appendChild(clone);
    $("#" + createdID).removeClass('hidden');
    clearNonItemProductRow('non-item-row-' + nonItemRowId);
}
function clearNonItemProductRow(id) {
    $('#' + id).find('input[name=non-item-description]').val('').attr('disabled', false);
    $('#' + id).find('input[name=non-item-qty]').val('').attr('disabled', false);
    $('#' + id).find('input[name=non-item-unit-prc]').val('').attr('disabled', false);
    $('#' + id).find('.non-item-total-price').html(accounting.formatMoney('0.00'));
    $('#' + id).find('.non-item-tax').attr('disabled', false);
    $('#' + id).find('.add-non-item').parent('td').removeClass('hidden');
    $('#' + id).find('#removeNonItem').addClass('hidden');

    var $exTypePicker = $('#' + id).find('select.non-item-ex-type-selectbox');
    $exTypePicker.siblings('.bootstrap-select').remove();
    $exTypePicker.removeData();
    loadDropDownFromDatabase('/api/expense-type/get-expense-type-by-search', "", 'expenseType', '#' + id + ' .non-item-ex-type-selectbox');

}
function clearAddBatchProductRow() {
    $('#newBatchNumber').val('');
    $('#newBatchQty').val('');
    $('#newBatchMDate').val('');
    $('#newBatchEDate').val('');
    $('#newBatchWarrenty').val('');
}
function clearAddNewRow() {
    $('#unitPrice').val('');
    $("#qty").val('').siblings('.uomqty,.uom-select').remove();
    $("#qty").show();
    $('#piDiscount').val('');
    $('#addNewTotal').html('0.00');
    $('#taxApplied').removeClass('glyphicon glyphicon-checked');
    $('#taxApplied').addClass('glyphicon glyphicon-unchecked');
    $('.tempLi').remove();
    $('.uomLi').remove();
    $('#uomAb').html('');
    selectedProduct = '';
    selectedProductQuantity = '';
    selectedProductUom = '';
}

function setAddedSupplierToTheList(addedSupplier) {
    if (!jQuery.isEmptyObject(addedSupplier)) {
        var newSupplier = addedSupplier.supplierCode + '-' + addedSupplier.supplierName;
        $('#supplier').append($("<option value='" + addedSupplier.supplierID + "'>" + newSupplier + "</option>"));
        $('#supplier').selectpicker('refresh');
        $('#supplier').val(addedSupplier.supplierID);
        $('#supplier').selectpicker('render');
        selectedSupplier = addedSupplier.supplierID;
        $('#addSupplierModal').modal('hide');
        $('#supplierCurrentBalance').val(0.00);
        $('#supplierCurrentCredit').val(0.00);
    }
}

function setTaxListForProduct(productID) {
    if ((!jQuery.isEmptyObject(locationProducts[productID].tax))) {
        productTax = locationProducts[productID].tax;
        $('#taxApplied').removeClass('glyphicon glyphicon-unchecked');
        $('#taxApplied').addClass('glyphicon glyphicon-check');
        $('.tempLi').remove();
        for (var i in productTax) {
            var clonedLi = $($('#sampleLi').clone()).removeClass('hidden').attr('id', 'li_' + i).addClass("tempLi");
            clonedLi.children(".taxChecks").attr('id', productID + '_' + i).prop('checked', true).addClass('addNewTaxCheck');
            if (productTax[i].tS == 0) {
                clonedLi.children(".taxChecks").attr('disabled', 'disabled');
                clonedLi.children(".taxName").addClass('crossText');
            }
            clonedLi.children(".taxName").html('&nbsp&nbsp' + productTax[i].tN + '&nbsp&nbsp' + productTax[i].tP + '%').attr('for', productID + '_' + i);
            clonedLi.insertBefore('#sampleLi');
        }
    } else {
        $('#taxApplied').addClass('glyphicon glyphicon-unchecked');
        $('#addNewTax').attr('disabled', 'disabled');
    }
}

function changeAuthorType() {
    selectedAuthourType = $('#authorType').val();
    if (selectedAuthourType == 1) { // if supplier
        $('#supplier_div').removeClass('hidden');
        $('#gl_account_div').addClass('hidden');
    } else { // gl account
        $('#gl_account_div').removeClass('hidden');
        $('#supplier_div').addClass('hidden');
    }
}

function validateDiscount(productID, discountType, currentDiscount, unitPrice) {
    var defaultProductData = locationProducts[productID];
    var newDiscount = 0;
    if (defaultProductData.dPEL == 1) {
        if (discountType == 'Val') {
            if (toFloat(defaultProductData.pPDV) !=0 && toFloat(currentDiscount) > toFloat(defaultProductData.pPDV)) {
                    newDiscount = toFloat(defaultProductData.pPDV);
                    p_notification(false, eb.getMessage('ERR_INVO_DISCOUNT_VALUE'));
            } else if (toFloat(defaultProductData.pPDV) == 0) {
                if (toFloat(currentDiscount) > toFloat(unitPrice)) {
                    newDiscount = toFloat(unitPrice);
                } else {
                    newDiscount = toFloat(currentDiscount);    
                }
            } else {
                newDiscount = toFloat(currentDiscount);
            }

        } else {
            if (toFloat(defaultProductData.pPDP) != 0 && toFloat(currentDiscount) > toFloat(defaultProductData.pPDP)) {
                    newDiscount = toFloat(defaultProductData.pPDP);
                    p_notification(false, eb.getMessage('ERR_INVO_DISC_PERCENTAGE'));
            } else if (toFloat(defaultProductData.pPDP) == 0) {
                if (toFloat(currentDiscount) > 100 ) {
                    newDiscount = 100;
                    p_notification(false, eb.getMessage('ERR_PI_DISC_PERCENTAGE'));   
                } else {
                    newDiscount = toFloat(currentDiscount);    
                }
            } else {
                newDiscount = toFloat(currentDiscount);
            }
        }
    } 
    return newDiscount;
}
