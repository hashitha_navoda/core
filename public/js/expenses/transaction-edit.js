$(document).ready(function () {

    var startDate = null;
    var endDate = null;
    var accountId = null;
    var accountBalance = 0;

    var newStatementBalance = 0;
    var recBalance = null;
    var currentBalance = null;

    var regExBalance = /^[-\d.]+$/;

    //for new statement balance field focus out
    $('#acc-details-div').on('focusout', '#new-statement-balance', function () {
        newStatementBalance = $('#new-statement-balance').val();
        if (!newStatementBalance.trim()) {
            newStatementBalance = 0;
        } else {
            if (!regExBalance.test(newStatementBalance)) {
                p_notification(false, 'statement balance can only contain only digits');
                $('#new-statement-balance').val('0.00');
                newStatementBalance = 0;
            }
            calculateReconciliation();
        }
    });

   
    getDepositedTransactionsByDateRange();

    //load account list
    $(document).on('change', '#bank-select', function () {
        var bankId = $(this).val();
        if (bankId) {
            getBankAccountListByBankId(bankId);
        } else {
            $('#account-select').empty();
            $('#account-select').append($("<option>", {value: '', html: '----- Select Account -----'}));
        }
        $('.datepicker').val('');
        // $('.datepicker,#search-btn').prop('disabled', true);
        $('#cheque-list').html('');
        $('#acc-details-div').addClass('hidden');
        $('#account-balance,#reconciliation-balance').val('');
    });

    //load account list
    $(document).on('change', '#account-select', function () {
        accountId = $(this).val();
        $('.datepicker').val('');
        if (accountId) {
            // getDepositedTransactions(accountId, function () {
            //     $('.datepicker').prop('disabled', false);
                // $('#acc-details-div').removeClass('hidden');
                // accountBalance = parseFloat($('#cheque-list-table').data('account-balance'));
                // $('#account-balance').val(accountBalance.toFixed(2));
                // recBalance = calculateReconciliation();
            // });
        } else {
            $('.datepicker').prop('disabled', true);
            $('#acc-details-div').addClass('hidden');
            $('#cheque-list').html('');
            $('#account-balance,#reconciliation-balance').val('');
        }
        // $('.datepicker').val('');
        // $('#search-btn').prop('disabled', true);
        $('#new-statement-balance').val('')
        $('#reconciliation-balance').val('');
        $('#on-date').val('');
        $('#on-account-balance').val('');

    });

    //for select all cheques
    $('#cheque-list').on('change', '#allChequesBox', function () {
        if (this.checked) {
            $('.cheque-box').prop('checked', true);
        } else {
            $('.cheque-box').prop('checked', false);
        }
        $('#reconciliation-balance').val(calculateReconciliation());
    });

    //for toggle saveBtn
    $(document).on('change', '#allChequesBox,.cheque-box', function () {
        if ($('.cheque-box:checkbox:checked').length > 0) {
            $('#saveBtn').prop('disabled', false);
        } else {
            $('#saveBtn').prop('disabled', true);
        }
    });

    //for calculate reconciliation balance
    $('#cheque-list').on('change', '.cheque-box', function () {
        $('#reconciliation-balance').val(calculateReconciliation());
    });

    //for save btn
    $('#reconciliationModal').on('click', '#btnConfirm', function () {
        var chequeDepositArr = [];
        var chequeIssueArr = [];
        var bankOutgoingTransferArr = [];
        var bankIncomingTransferArr = [];
        var bankWithdrawalArr = [];
        var withdrawalArr = [];
        var bankDepositArr = [];
        var directJEWithdrawalArr = [];
        var directJEDepositArr = [];
        var otherJEWithdrawalArr = [];
        var otherJEDepositArr = [];
        var outgoingAdvancedIncomePaymentsArr = [];
        var incomingAdvancedIncomePaymentsArr = [];
        var depositArr = [];
        var accountIncomingTransferArr = [];
        var accountOutgoingTransferArr = [];
        var outgoingAdvancedPaymentsArr = [];
        var incomingAdvancedPaymentsArr = [];
        var comment = $('#reconciliation-comment').val();
        var reconciliationEditID = $("#reconciliationId").val();
        var transactions = {};

        $('.cheque-box').each(function (index) {
            if ($(this).is(':checked')) {
                var type = $(this).closest('tr').data('transaction-type');
                var id = $(this).closest('tr').data('transaction-id');
                var amount = $(this).closest('tr').data('transaction-amount');
                switch (type) {
                    case 'cheque-deposit':
                        chequeDepositArr.push({id: id, amount: amount});
                        break;
                    case 'cheque-issue':
                        chequeIssueArr.push({id: id, amount: amount});
                        break;
                    case 'outgoing-bank-transfer' :
                        bankOutgoingTransferArr.push({id: id, amount: amount});
                        break;
                    case 'incoming-bank-transfer' :
                        bankIncomingTransferArr.push({id: id, amount: amount});
                        break;
                    case 'bank-withrawal' :
                        bankWithdrawalArr.push({id: id, amount: amount});
                        break;
                    case 'bank-deposit' :
                        bankDepositArr.push({id: id, amount: amount});
                        break;
                    case 'direct-journal-entry-withrawal' :
                        directJEWithdrawalArr.push({id: id, amount: amount});
                        break;
                    case 'direct-journal-entry-deposit' :
                        directJEDepositArr.push({id: id, amount: amount});
                        break;
                    case 'outgoing-advanced-income-payments' :
                        outgoingAdvancedIncomePaymentsArr.push({id: id, amount: amount});
                        break;
                    case 'incoming-advanced-income-payments' :
                        incomingAdvancedIncomePaymentsArr.push({id: id, amount: amount});
                        break;
                    case 'withdrwal' :
                        withdrawalArr.push({id: id, amount: amount});
                        break;
                    case 'deposit' :
                        depositArr.push({id: id, amount: amount});
                        break;
                    case 'account-incoming-transfer' :
                        accountIncomingTransferArr.push({id: id, amount: amount});
                        break;
                    case 'account-outgoing-transfer' :
                        accountOutgoingTransferArr.push({id: id, amount: amount});
                        break;
                    case 'outgoing-advanced-payments' :
                        outgoingAdvancedPaymentsArr.push({id: id, amount: amount});
                        break;
                    case 'incoming-advanced-payments' :
                        incomingAdvancedPaymentsArr.push({id: id, amount: amount});
                        break;
                    case 'other-journal-entry-withrawal' :
                        otherJEWithdrawalArr.push({id: id, amount: amount});
                        break;
                    case 'other-journal-entry-deposit' :
                        otherJEDepositArr.push({id: id, amount: amount});
                        break;
                    default:
                        console.log('invalid option');
                }
            }
        });
        transactions.chequeDeposit = chequeDepositArr;
        transactions.bankOutgoingTransfer = bankOutgoingTransferArr;
        transactions.bankIncomingTransfer = bankIncomingTransferArr;
        transactions.chequeIssue = chequeIssueArr;
        transactions.bankWithdrawal = bankWithdrawalArr;
        transactions.bankDeposit = bankDepositArr;
        transactions.withdrawal = withdrawalArr;
        transactions.jeDiposit = directJEDepositArr;
        transactions.jeWithdrawal = directJEWithdrawalArr;
        transactions.otherJeDiposit = otherJEDepositArr;
        transactions.otherJeWithdrawal = otherJEWithdrawalArr;
        transactions.outgoingAdvancedIncomePayments = outgoingAdvancedIncomePaymentsArr;
        transactions.incomingAdvancedIncomePayments = incomingAdvancedIncomePaymentsArr;
        transactions.deposit = depositArr;
        transactions.accountIncomingTranfer = accountIncomingTransferArr;
        transactions.accountOutgoingTransfer = accountOutgoingTransferArr;
        transactions.incomingAdvancedPayments = incomingAdvancedPaymentsArr;
        transactions.outgoingAdvancedPayments = outgoingAdvancedPaymentsArr;

        $.ajax({
            type: 'POST',
            url: BASE_URL + '/reconciliation-api/create',
            data: {
                transactions: transactions,
                // accountId: accountId,
                amount: $('#reconciliation-balance').val(),
                statementBalance: $('#new-statement-balance').val(),
                comment: comment,
                reconciliationEditID: reconciliationEditID
            },
            success: function (respond) {
                if (respond.status) {
                     setTimeout(function(){ 
                        window.location.assign(BASE_URL + "/reconciliation/reconciliation")
                    }, 1000);
                }
                p_notification(respond.status, respond.msg);
            }
        });
    });

    function calculateReconciliation() {
        var reconciliationBalance = 0;
        var uncheckedBalance = 0;

        $('.cheque-box').each(function (index) {
            if ($(this).is(':checked')) {
                //get total cheque amount
                reconciliationBalance = reconciliationBalance + parseFloat($(this).closest('tr').data('transaction-amount'));
            } else {
                uncheckedBalance = uncheckedBalance + parseFloat($(this).closest('tr').data('transaction-amount'));
            }
        });
        $('#new-statement-balance').val(parseFloat(newStatementBalance).toFixed(2))
        $('#reconciliation-balance').val(parseFloat(reconciliationBalance).toFixed(2));

        //for calculate statement diff
        if ('null' != newStatementBalance && 'null' != accountBalance) {
            var diff = accountBalance - newStatementBalance - uncheckedBalance;
            $('#balance-difference').val(parseFloat(diff).toFixed(2));
        }

        return reconciliationBalance.toFixed(2);
    }

    //get bank account list
    function getBankAccountListByBankId(bankId) {
        $.ajax({
            type: 'POST',
            url: BASE_URL + '/account-api/bank-account-list-for-dropdown',
            data: {bankId: bankId},
            success: function (respond) {
                //empty options
                $('#account-select').empty();
                $('#account-select').append($("<option>", {value: '', html: '----- Select Account -----'}));
                $(respond.data.list).each(function (i, v) {
                    $('#account-select').append($("<option>", {value: v.value, html: v.text}));
                });
            }
        });
    }

    //get deposit cheque list
    function getDepositedTransactions(accountId, callback) {
        $.ajax({
            type: 'POST',
            url: BASE_URL + '/reconciliation-api/getAccountTransactions',
            data: {
                accountId: accountId
            },
            success: function (respond) {
                $('#cheque-list').html(respond);
                callback();
            }
        });
    }

    //get deposit cheque list by date range
    function getDepositedTransactionsByDateRange() {
        $.ajax({
            type: 'POST',
            url: BASE_URL + '/reconciliation-api/getAccountTransactionsByDraft',
            data: {
                reconciliationId: $("#reconciliationId").val()
            },
            success: function (respond) {
                $('#cheque-list').html(respond);
                $('#acc-details-div').removeClass('hidden');
                accountBalance = parseFloat($('#cheque-list-table').data('account-balance'));
                var rec = $('#reconciliationAmount').val();
                newStatementBalance = parseFloat(rec);
                $('#account-balance').val(accountBalance.toFixed(2));
                $('#new-statement-balance').val(newStatementBalance.toFixed(2));
                recBalance = calculateReconciliation();
            }
        });
    }

     //for save btn
    $(document).on('click', '#saveBtnDarft', function () {
        var chequeDepositArr = [];
        var chequeIssueArr = [];
        var bankOutgoingTransferArr = [];
        var bankIncomingTransferArr = [];
        var bankWithdrawalArr = [];
        var withdrawalArr = [];
        var bankDepositArr = [];
        var directJEWithdrawalArr = [];
        var directJEDepositArr = [];
        var otherJEWithdrawalArr = [];
        var otherJEDepositArr = [];
        var outgoingAdvancedIncomePaymentsArr = [];
        var incomingAdvancedIncomePaymentsArr = [];
        var depositArr = [];
        var accountIncomingTransferArr = [];
        var accountOutgoingTransferArr = [];
        var outgoingAdvancedPaymentsArr = [];
        var incomingAdvancedPaymentsArr = [];
        var comment = $('#reconciliation-comment').val();
        var reconciliationEditID = $("#reconciliationId").val();
        var transactions = {};

        $('.cheque-box').each(function (index) {
            if ($(this).is(':checked')) {
                var type = $(this).closest('tr').data('transaction-type');
                var id = $(this).closest('tr').data('transaction-id');
                var amount = $(this).closest('tr').data('transaction-amount');
                switch (type) {
                    case 'cheque-deposit':
                        chequeDepositArr.push({id: id, amount: amount});
                        break;
                    case 'cheque-issue':
                        chequeIssueArr.push({id: id, amount: amount});
                        break;
                    case 'outgoing-bank-transfer' :
                        bankOutgoingTransferArr.push({id: id, amount: amount});
                        break;
                    case 'incoming-bank-transfer' :
                        bankIncomingTransferArr.push({id: id, amount: amount});
                        break;
                    case 'bank-withrawal' :
                        bankWithdrawalArr.push({id: id, amount: amount});
                        break;
                    case 'bank-deposit' :
                        bankDepositArr.push({id: id, amount: amount});
                        break;
                    case 'direct-journal-entry-withrawal' :
                        directJEWithdrawalArr.push({id: id, amount: amount});
                        break;
                    case 'direct-journal-entry-deposit' :
                        directJEDepositArr.push({id: id, amount: amount});
                        break;
                    case 'outgoing-advanced-income-payments' :
                        outgoingAdvancedIncomePaymentsArr.push({id: id, amount: amount});
                        break;
                    case 'incoming-advanced-income-payments' :
                        incomingAdvancedIncomePaymentsArr.push({id: id, amount: amount});
                        break;
                    case 'withdrwal' :
                        withdrawalArr.push({id: id, amount: amount});
                        break;
                    case 'deposit' :
                        depositArr.push({id: id, amount: amount});
                        break;
                    case 'account-incoming-transfer' :
                        accountIncomingTransferArr.push({id: id, amount: amount});
                        break;
                    case 'account-outgoing-transfer' :
                        accountOutgoingTransferArr.push({id: id, amount: amount});
                        break;
                    case 'outgoing-advanced-payments' :
                        outgoingAdvancedPaymentsArr.push({id: id, amount: amount});
                        break;
                    case 'incoming-advanced-payments' :
                        incomingAdvancedPaymentsArr.push({id: id, amount: amount});
                        break;
                    case 'other-journal-entry-withrawal' :
                        otherJEWithdrawalArr.push({id: id, amount: amount});
                        break;
                    case 'other-journal-entry-deposit' :
                        otherJEDepositArr.push({id: id, amount: amount});
                        break;
                    default:
                        console.log('invalid option');
                }
            }
        });
        transactions.chequeDeposit = chequeDepositArr;
        transactions.bankOutgoingTransfer = bankOutgoingTransferArr;
        transactions.bankIncomingTransfer = bankIncomingTransferArr;
        transactions.chequeIssue = chequeIssueArr;
        transactions.bankWithdrawal = bankWithdrawalArr;
        transactions.bankDeposit = bankDepositArr;
        transactions.withdrawal = withdrawalArr;
        transactions.jeDiposit = directJEDepositArr;
        transactions.jeWithdrawal = directJEWithdrawalArr;
        transactions.otherJeDiposit = otherJEDepositArr;
        transactions.otherJeWithdrawal = otherJEWithdrawalArr;
        transactions.outgoingAdvancedIncomePayments = outgoingAdvancedIncomePaymentsArr;
        transactions.incomingAdvancedIncomePayments = incomingAdvancedIncomePaymentsArr;
        transactions.deposit = depositArr;
        transactions.accountIncomingTranfer = accountIncomingTransferArr;
        transactions.accountOutgoingTransfer = accountOutgoingTransferArr;
        transactions.incomingAdvancedPayments = incomingAdvancedPaymentsArr;
        transactions.outgoingAdvancedPayments = outgoingAdvancedPaymentsArr;

        $.ajax({
            type: 'POST',
            url: BASE_URL + '/reconciliation-api/createDraft',
            data: {
                transactions: transactions,
                // accountId: accountId,
                // startDate: startDate,
                // endDate: endDate,
                reconciliationEditID: reconciliationEditID,
                amount: $('#reconciliation-balance').val(),
                statementBalance: $('#new-statement-balance').val(),
                comment: comment
            },
            success: function (respond) {
                if (respond.status) {
                    setTimeout(function(){ 
                        window.location.assign(BASE_URL + "/reconciliation/reconciliation")
                    }, 1000);
                }
                p_notification(respond.status, respond.msg);
            }
        });
    });
});


