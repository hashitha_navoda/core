$(document).ready(function () {

    var startDate = null;
    var endDate = null;
    var accountId = null;
    var accountBalance = 0;

    var newStatementBalance = 0;
    var recBalance = null;
    var currentBalance = null;

    var regExBalance = /^[-\d.]+$/;

    // $('#start-date').datepicker({
    //     format: 'yyyy-mm-dd',
    //     minView: 4,
    //     autoclose: true,
    // }).on('changeDate', function (ev) {
    //     startDate = this.value;
    //     if (startDate && endDate) {
    //         $('#search-btn').prop('disabled', false);
    //     }
    // });

    // $('#end-date').datepicker({
    //     format: 'yyyy-mm-dd',
    //     minView: 4,
    //     autoclose: true
    // }).on('changeDate', function (ev) {
    //     endDate = this.value;
    //     if (startDate && endDate) {
    //         $('#search-btn').prop('disabled', false);
    //     }
    // });

    var checkin = $('#start-date').datepicker({
        format: 'yyyy-mm-dd',
    }).on('changeDate', function(ev) {
        if (ev.date.valueOf() < checkout.date.valueOf()) {
            var currentDate = new Date(ev.date);
            var newDate = new Date();
            newDate.setDate(currentDate.getDate() + 1);
            checkout.setValue(newDate);
        }
        startDate = this.value;
        if (startDate && endDate) {
            $('#search-btn').prop('disabled', false);
        }
        checkin.hide();
        $('#end-date')[0].focus();
    }).data('datepicker');

    var checkout = $('#end-date').datepicker({
        format: 'yyyy-mm-dd',
        onRender: function(date) {
            return date.valueOf() <= checkin.date.valueOf() ? 'disabled' : '';
        }
    }).on('changeDate', function(ev) {
         endDate = this.value;
        if (startDate && endDate) {
            $('#search-btn').prop('disabled', false);
        }
        checkout.hide();
    }).data('datepicker');

    $('.datepicker').prop('disabled', false);
    $('.datepicker,#search-btn').prop('disabled', false);

    //for new statement balance field focus out
    $('#acc-details-div').on('focusout', '#new-statement-balance', function () {
        newStatementBalance = $('#new-statement-balance').val();
        if (!newStatementBalance.trim()) {
            newStatementBalance = 0;
        } else {
            if (!regExBalance.test(newStatementBalance)) {
                p_notification(false, 'statement balance can only contain only digits');
                $('#new-statement-balance').val('0.00');
                newStatementBalance = 0;
            }
            calculateReconciliation();
        }
    });

    //for serach btn
    $(document).on('click', '#search-btn', function () {
        getDepositedTransactionsByDateRange(accountId, startDate, endDate);
    });

    //load account list
    $(document).on('change', '#bank-select', function () {
        var bankId = $(this).val();
        if (bankId) {
            getBankAccountListByBankId(bankId);
        } else {
            $('#account-select').empty();
            $('#account-select').append($("<option>", {value: '', html: '----- Select Account -----'}));
        }
        $('.datepicker').val('');
        // $('.datepicker,#search-btn').prop('disabled', true);
        $('#cheque-list').html('');
        $('#acc-details-div').addClass('hidden');
        $('#account-balance,#reconciliation-balance').val('');
    });

    //load account list
    $(document).on('change', '#account-select', function () {
        accountId = $(this).val();
        $('.datepicker').val('');
        if (accountId) {
            // getDepositedTransactions(accountId, function () {
            //     $('.datepicker').prop('disabled', false);
                // $('#acc-details-div').removeClass('hidden');
                // accountBalance = parseFloat($('#cheque-list-table').data('account-balance'));
                // $('#account-balance').val(accountBalance.toFixed(2));
                // recBalance = calculateReconciliation();
            // });
        } else {
            $('.datepicker').prop('disabled', true);
            $('#acc-details-div').addClass('hidden');
            $('#cheque-list').html('');
            $('#account-balance,#reconciliation-balance').val('');
        }
        // $('.datepicker').val('');
        // $('#search-btn').prop('disabled', true);
        $('#new-statement-balance').val('')
        $('#reconciliation-balance').val('');
        $('#on-date').val('');
        $('#on-account-balance').val('');

    });

    //for select all cheques
    $('#cheque-list').on('change', '#allChequesBox', function () {
        if (this.checked) {
            $('.cheque-box').prop('checked', true);
        } else {
            $('.cheque-box').prop('checked', false);
        }
        $('#reconciliation-balance').val(calculateReconciliation());
    });

    //for toggle saveBtn
    $(document).on('change', '#allChequesBox,.cheque-box', function () {
        if ($('.cheque-box:checkbox:checked').length > 0) {
            $('#saveBtn').prop('disabled', false);
        } else {
            $('#saveBtn').prop('disabled', true);
        }
    });

    //for calculate reconciliation balance
    $('#cheque-list').on('change', '.cheque-box', function () {
        $('#reconciliation-balance').val(calculateReconciliation());
    });

    //for save btn
    $('#reconciliationModal').on('click', '#btnConfirm', function () {
        var chequeDepositArr = [];
        var chequeIssueArr = [];
        var bankOutgoingTransferArr = [];
        var bankIncomingTransferArr = [];
        var bankWithdrawalArr = [];
        var withdrawalArr = [];
        var bankDepositArr = [];
        var directJEWithdrawalArr = [];
        var directJEDepositArr = [];
        var otherJEWithdrawalArr = [];
        var otherJEDepositArr = [];
        var outgoingAdvancedIncomePaymentsArr = [];
        var incomingAdvancedIncomePaymentsArr = [];
        var depositArr = [];
        var accountIncomingTransferArr = [];
        var accountOutgoingTransferArr = [];
        var outgoingAdvancedPaymentsArr = [];
        var incomingAdvancedPaymentsArr = [];
        var comment = $('#reconciliation-comment').val();
        var transactions = {};

        $('.cheque-box').each(function (index) {
            if ($(this).is(':checked')) {
                var type = $(this).closest('tr').data('transaction-type');
                var id = $(this).closest('tr').data('transaction-id');
                var amount = $(this).closest('tr').data('transaction-amount');
                switch (type) {
                    case 'cheque-deposit':
                        chequeDepositArr.push({id: id, amount: amount});
                        break;
                    case 'cheque-issue':
                        chequeIssueArr.push({id: id, amount: amount});
                        break;
                    case 'outgoing-bank-transfer' :
                        bankOutgoingTransferArr.push({id: id, amount: amount});
                        break;
                    case 'incoming-bank-transfer' :
                        bankIncomingTransferArr.push({id: id, amount: amount});
                        break;
                    case 'bank-withrawal' :
                        bankWithdrawalArr.push({id: id, amount: amount});
                        break;
                    case 'bank-deposit' :
                        bankDepositArr.push({id: id, amount: amount});
                        break;
                    case 'direct-journal-entry-withrawal' :
                        directJEWithdrawalArr.push({id: id, amount: amount});
                        break;
                    case 'direct-journal-entry-deposit' :
                        directJEDepositArr.push({id: id, amount: amount});
                        break;
                    case 'outgoing-advanced-income-payments' :
                        outgoingAdvancedIncomePaymentsArr.push({id: id, amount: amount});
                        break;
                    case 'incoming-advanced-income-payments' :
                        incomingAdvancedIncomePaymentsArr.push({id: id, amount: amount});
                        break;
                    case 'withdrwal' :
                        withdrawalArr.push({id: id, amount: amount});
                        break;
                    case 'deposit' :
                        depositArr.push({id: id, amount: amount});
                        break;
                    case 'account-incoming-transfer' :
                        accountIncomingTransferArr.push({id: id, amount: amount});
                        break;
                    case 'account-outgoing-transfer' :
                        accountOutgoingTransferArr.push({id: id, amount: amount});
                        break;
                    case 'outgoing-advanced-payments' :
                        outgoingAdvancedPaymentsArr.push({id: id, amount: amount});
                        break;
                    case 'incoming-advanced-payments' :
                        incomingAdvancedPaymentsArr.push({id: id, amount: amount});
                        break;
                    case 'other-journal-entry-withrawal' :
                        otherJEWithdrawalArr.push({id: id, amount: amount});
                        break;
                    case 'other-journal-entry-deposit' :
                        otherJEDepositArr.push({id: id, amount: amount});
                        break;
                    default:
                        console.log('invalid option');
                }
            }
        });
        transactions.chequeDeposit = chequeDepositArr;
        transactions.bankOutgoingTransfer = bankOutgoingTransferArr;
        transactions.bankIncomingTransfer = bankIncomingTransferArr;
        transactions.chequeIssue = chequeIssueArr;
        transactions.bankWithdrawal = bankWithdrawalArr;
        transactions.bankDeposit = bankDepositArr;
        transactions.withdrawal = withdrawalArr;
        transactions.jeDiposit = directJEDepositArr;
        transactions.jeWithdrawal = directJEWithdrawalArr;
        transactions.otherJeDiposit = otherJEDepositArr;
        transactions.otherJeWithdrawal = otherJEWithdrawalArr;
        transactions.outgoingAdvancedIncomePayments = outgoingAdvancedIncomePaymentsArr;
        transactions.incomingAdvancedIncomePayments = incomingAdvancedIncomePaymentsArr;
        transactions.deposit = depositArr;
        transactions.accountIncomingTranfer = accountIncomingTransferArr;
        transactions.accountOutgoingTransfer = accountOutgoingTransferArr;
        transactions.incomingAdvancedPayments = incomingAdvancedPaymentsArr;
        transactions.outgoingAdvancedPayments = outgoingAdvancedPaymentsArr;

        $.ajax({
            type: 'POST',
            url: BASE_URL + '/reconciliation-api/create',
            data: {
                transactions: transactions,
                accountId: accountId,
                startDate: startDate,
                endDate: endDate,
                amount: $('#reconciliation-balance').val(),
                statementBalance: $('#new-statement-balance').val(),
                comment: comment
            },
            success: function (respond) {
                if (respond.status) {
                     setTimeout(function(){ 
                        window.location.assign(BASE_URL + "/reconciliation/reconciliation")
                    }, 1000);
                }
                p_notification(respond.status, respond.msg);
            }
        });
    });

    function calculateReconciliation() {
        var reconciliationBalance = 0;
        var uncheckedBalance = 0;

        $('.cheque-box').each(function (index) {
            if ($(this).is(':checked')) {
                //get total cheque amount
                reconciliationBalance = reconciliationBalance + parseFloat($(this).closest('tr').data('transaction-amount'));
            } else {
                uncheckedBalance = uncheckedBalance + parseFloat($(this).closest('tr').data('transaction-amount'));
            }
        });
        $('#new-statement-balance').val(parseFloat(newStatementBalance).toFixed(2))
        $('#reconciliation-balance').val(parseFloat(reconciliationBalance).toFixed(2));

        //for calculate statement diff
        if ('null' != newStatementBalance && 'null' != accountBalance) {
            var diff = accountBalance - newStatementBalance - uncheckedBalance;
            $('#balance-difference').val(parseFloat(diff).toFixed(2));
        }

        return reconciliationBalance.toFixed(2);
    }

    //get bank account list
    function getBankAccountListByBankId(bankId) {
        $.ajax({
            type: 'POST',
            url: BASE_URL + '/account-api/bank-account-list-for-dropdown',
            data: {bankId: bankId},
            success: function (respond) {
                //empty options
                $('#account-select').empty();
                $('#account-select').append($("<option>", {value: '', html: '----- Select Account -----'}));
                $(respond.data.list).each(function (i, v) {
                    $('#account-select').append($("<option>", {value: v.value, html: v.text}));
                });
            }
        });
    }

    //get deposit cheque list
    function getDepositedTransactions(accountId, callback) {
        $.ajax({
            type: 'POST',
            url: BASE_URL + '/reconciliation-api/getAccountTransactions',
            data: {
                accountId: accountId
            },
            success: function (respond) {
                $('#cheque-list').html(respond);
                callback();
            }
        });
    }

    //get deposit cheque list by date range
    function getDepositedTransactionsByDateRange(accountId, startDate, endDate) {

        if ($("#bank-select").val() == "") {
            p_notification(false, 'Please select bank before search');
        } else if ($("#account-select").val() == "") {
            p_notification(false, 'Please select account related to selected bank before search');
        } else if (!startDate) {
            p_notification(false, 'please fill start date');
        } else if (!endDate) {
            p_notification(false, 'please fill end date');
        } else if (startDate > endDate) {
            p_notification(false, 'invalid date range');
        } else {
            $.ajax({
                type: 'POST',
                url: BASE_URL + '/reconciliation-api/getAccountTransactions',
                data: {
                    accountId: accountId,
                    startDate: startDate,
                    endDate: endDate
                },
                success: function (respond) {
                    $('#cheque-list').html(respond);
                    $('#acc-details-div').removeClass('hidden');
                    accountBalance = parseFloat($('#cheque-list-table').data('account-balance'));
                    $('#account-balance').val(accountBalance.toFixed(2));
                    recBalance = calculateReconciliation();
                }
            });
        }
    }

     //for save btn
    $(document).on('click', '#saveBtnDarft', function () {
        var chequeDepositArr = [];
        var chequeIssueArr = [];
        var bankOutgoingTransferArr = [];
        var bankIncomingTransferArr = [];
        var bankWithdrawalArr = [];
        var withdrawalArr = [];
        var bankDepositArr = [];
        var directJEWithdrawalArr = [];
        var directJEDepositArr = [];
        var otherJEWithdrawalArr = [];
        var otherJEDepositArr = [];
        var outgoingAdvancedIncomePaymentsArr = [];
        var incomingAdvancedIncomePaymentsArr = [];
        var depositArr = [];
        var accountIncomingTransferArr = [];
        var accountOutgoingTransferArr = [];
        var outgoingAdvancedPaymentsArr = [];
        var incomingAdvancedPaymentsArr = [];
        var comment = $('#reconciliation-comment').val();
        var transactions = {};

        $('.cheque-box').each(function (index) {
            if ($(this).is(':checked')) {
                var type = $(this).closest('tr').data('transaction-type');
                var id = $(this).closest('tr').data('transaction-id');
                var amount = $(this).closest('tr').data('transaction-amount');
                switch (type) {
                    case 'cheque-deposit':
                        chequeDepositArr.push({id: id, amount: amount});
                        break;
                    case 'cheque-issue':
                        chequeIssueArr.push({id: id, amount: amount});
                        break;
                    case 'outgoing-bank-transfer' :
                        bankOutgoingTransferArr.push({id: id, amount: amount});
                        break;
                    case 'incoming-bank-transfer' :
                        bankIncomingTransferArr.push({id: id, amount: amount});
                        break;
                    case 'bank-withrawal' :
                        bankWithdrawalArr.push({id: id, amount: amount});
                        break;
                    case 'bank-deposit' :
                        bankDepositArr.push({id: id, amount: amount});
                        break;
                    case 'direct-journal-entry-withrawal' :
                        directJEWithdrawalArr.push({id: id, amount: amount});
                        break;
                    case 'direct-journal-entry-deposit' :
                        directJEDepositArr.push({id: id, amount: amount});
                        break;
                    case 'outgoing-advanced-income-payments' :
                        outgoingAdvancedIncomePaymentsArr.push({id: id, amount: amount});
                        break;
                    case 'incoming-advanced-income-payments' :
                        incomingAdvancedIncomePaymentsArr.push({id: id, amount: amount});
                        break;
                    case 'withdrwal' :
                        withdrawalArr.push({id: id, amount: amount});
                        break;
                    case 'deposit' :
                        depositArr.push({id: id, amount: amount});
                        break;
                    case 'account-incoming-transfer' :
                        accountIncomingTransferArr.push({id: id, amount: amount});
                        break;
                    case 'account-outgoing-transfer' :
                        accountOutgoingTransferArr.push({id: id, amount: amount});
                        break;
                    case 'outgoing-advanced-payments' :
                        outgoingAdvancedPaymentsArr.push({id: id, amount: amount});
                        break;
                    case 'incoming-advanced-payments' :
                        incomingAdvancedPaymentsArr.push({id: id, amount: amount});
                        break;
                    case 'other-journal-entry-withrawal' :
                        otherJEWithdrawalArr.push({id: id, amount: amount});
                        break;
                    case 'other-journal-entry-deposit' :
                        otherJEDepositArr.push({id: id, amount: amount});
                        break;
                    default:
                        console.log('invalid option');
                }
            }
        });
        transactions.chequeDeposit = chequeDepositArr;
        transactions.bankOutgoingTransfer = bankOutgoingTransferArr;
        transactions.bankIncomingTransfer = bankIncomingTransferArr;
        transactions.chequeIssue = chequeIssueArr;
        transactions.bankWithdrawal = bankWithdrawalArr;
        transactions.bankDeposit = bankDepositArr;
        transactions.withdrawal = withdrawalArr;
        transactions.jeDiposit = directJEDepositArr;
        transactions.jeWithdrawal = directJEWithdrawalArr;
        transactions.otherJeDiposit = otherJEDepositArr;
        transactions.otherJeWithdrawal = otherJEWithdrawalArr;
        transactions.outgoingAdvancedIncomePayments = outgoingAdvancedIncomePaymentsArr;
        transactions.incomingAdvancedIncomePayments = incomingAdvancedIncomePaymentsArr;
        transactions.deposit = depositArr;
        transactions.accountIncomingTranfer = accountIncomingTransferArr;
        transactions.accountOutgoingTransfer = accountOutgoingTransferArr;
        transactions.incomingAdvancedPayments = incomingAdvancedPaymentsArr;
        transactions.outgoingAdvancedPayments = outgoingAdvancedPaymentsArr;

        $.ajax({
            type: 'POST',
            url: BASE_URL + '/reconciliation-api/createDraft',
            data: {
                transactions: transactions,
                accountId: accountId,
                startDate: startDate,
                endDate: endDate,
                amount: $('#reconciliation-balance').val(),
                statementBalance: $('#new-statement-balance').val(),
                comment: comment
            },
            success: function (respond) {
                if (respond.status) {
                    setTimeout(function(){ 
                        window.location.assign(BASE_URL + "/reconciliation/reconciliation")
                    }, 1000);
                }
                p_notification(respond.status, respond.msg);
            }
        });
    });

     $('#cheque-list').on('click', '.related_docs', function() {

        var type = $(this).attr('data-cn-related-type');

        if (type == "incomingPay") {
            setDataToHistoryModal($(this).attr('data-cn-related-id'));
        } else if (type == "outgoingPay") {
            setDataToHistoryModalOfOutgoingPayment($(this).attr('data-cn-related-id'));
        } else if (type == "directJE") {
            setDataToHistoryModalOfOutgoingPayment($(this).attr('data-cn-related-id'), "directJE");
        } else if (type == "otherJE") {
            var documentTypeID = $(this).attr('data-cn-related-document');
            if (documentTypeID == "18") {
                 setDataToHistoryModal($(this).attr('data-cn-related-id'));
            } else if (documentTypeID == "10") {//grn
                 setDataToHistoryModalGrn($(this).attr('data-cn-related-id'));
            }  else if (documentTypeID == "16") {//adj
                setDataToHistoryModalOfOutgoingPayment($(this).attr('data-cn-related-id'), "otherJE", documentTypeID);
            }  else if (documentTypeID == "13") {//dn
                 setDataToHistoryModalDN($(this).attr('data-cn-related-id'));
            }  else if (documentTypeID == "20") {//expv
                 setDataToHistoryModalEXPV($(this).attr('data-cn-related-id'));
            }  else if (documentTypeID == "28") {//petty
                setDataToHistoryModalOfOutgoingPayment($(this).attr('data-cn-related-id'), "otherJE", documentTypeID);
            } else {
                setDataToHistoryModalOfOutgoingPayment($(this).attr('data-je-related-id'), "directJE");
            }
        }
        $('#addDocHistoryModal').modal('show');
    });

     function setDataToHistoryModalEXPV(paymentVoucherID) {
        $('#doc-history-table tbody tr').remove();
        $('#doc-history-table tfoot div').remove();
        $('#doc-history-table tbody div').remove();

        eb.ajax({
            type: 'POST',
            url: BASE_URL + '/expense-purchase-invoice-api/getAllRelatedDocumentDetailsByPaymentVoucherID',
            data: {paymentVoucherID: paymentVoucherID},
            success: function(respond) {
                if (respond.status == true) {
                    $('#doc-history-table thead tr').removeClass('hidden');
                    $.each(respond.data, function(index, value) {
                        if (value != null) {
                            $.each(value, function(index2, value2) {
                                tableBody = "<tr data-docId=" + value2['documentID'] + " data-doctype=" + value2['type'] + "><td class='' >" + value2['type'] + "</td><td>" + value2['code'] + "</td><td>" + value2['amount'] + "</td><td>" + value2['issuedDate'] + "</td><td class='text-center'><span class='glyphicon glyphicon-eye-open openlist link_color document_view ' style='cursor:pointer'></span></td></tr>";
                                $('#doc-history-table tbody').append(tableBody);
                            });
                        }
                    });
                    var footer = " <div class='col-lg-12 col-md-12 col-sm-12 text-right'><button type='button' class='btn btn-primary col-lg-12 col-md-12 col-sm-12' data-dismiss='modal' aril-hidden='true'>Back</button></div>";
                    $('#doc-history-table tfoot').append(footer);
                } else {
                    $('#doc-history-table thead tr').addClass('hidden');
                    var noDataFooter = "<div class= ' col-lg-9 text-right'><label><b>No Data</b></label>";
                    $('#doc-history-table tbody').append(noDataFooter);
                }
            }
        });
    }

     function setDataToHistoryModalDN(debitNoteID) {
        $('#doc-history-table tbody tr').remove();
        $('#doc-history-table tfoot div').remove();
        $('#doc-history-table tbody div').remove();

        eb.ajax({
            type: 'POST',
            url: BASE_URL + '/debit-note-api/getAllRelatedDocumentDetailsByDebitNoteId',
            data: {debitNoteID: debitNoteID},
            success: function(respond) {
                if (respond.status == true) {
                    $('#doc-history-table thead tr').removeClass('hidden');
                    $.each(respond.data, function(index, value) {
                        if (value != null) {
                            $.each(value, function(index2, value2) {
                                tableBody = "<tr data-docId=" + value2['documentID'] + " data-doctype=" + value2['type'] + "><td class='' >" + value2['type'] + "</td><td>" + value2['code'] + "</td><td>" + value2['amount'] + "</td><td>" + value2['issuedDate'] + "</td><td class='text-center'><span class='glyphicon glyphicon-eye-open openlist link_color document_view ' style='cursor:pointer'></span></td></tr>";
                                $('#doc-history-table tbody').append(tableBody);
                            });
                        }
                    });
                    var footer = " <div class='col-lg-12 col-md-12 col-sm-12 text-right'><button type='button' class='btn btn-primary col-lg-12 col-md-12 col-sm-12' data-dismiss='modal' aril-hidden='true'>Back</button></div>";
                    $('#doc-history-table tfoot').append(footer);
                } else {
                    $('#doc-history-table thead tr').addClass('hidden');
                    var noDataFooter = "<div class= ' col-lg-9 text-right'><label><b>No Data</b></label>";
                    $('#doc-history-table tbody').append(noDataFooter);
                }
            }
        });
    }

    function setDataToHistoryModalGrn(grnID) {
        $('#doc-history-table tbody tr').remove();
        $('#doc-history-table tfoot div').remove();
        $('#doc-history-table tbody div').remove();

        eb.ajax({
            type: 'POST',
            url: BASE_URL + '/api/grn/getAllRelatedDocumentDetailsByGrnId',
            data: {grnID: grnID},
            success: function(respond) {
                if (respond.status == true) {
                    $('#doc-history-table thead tr').removeClass('hidden');
                    $.each(respond.data, function(index, value) {
                        if (value != null) {
                            $.each(value, function(index2, value2) {
                                tableBody = "<tr data-docId=" + value2['documentID'] + " data-doctype=" + value2['type'] + "><td class='' >" + value2['type'] + "</td><td>" + value2['code'] + "</td><td>" + value2['amount'] + "</td><td>" + value2['issuedDate'] + "</td><td class='text-center'><span class='glyphicon glyphicon-eye-open openlist link_color document_view ' style='cursor:pointer'></span></td></tr>";
                                $('#doc-history-table tbody').append(tableBody);
                            });
                        }
                    });
                    var footer = " <div class='col-lg-12 col-md-12 col-sm-12 text-right'><button type='button' class='btn btn-primary col-lg-12 col-md-12 col-sm-12' data-dismiss='modal' aril-hidden='true'>Back</button></div>";
                    $('#doc-history-table tfoot').append(footer);
                } else {
                    $('#doc-history-table thead tr').addClass('hidden');
                    var noDataFooter = "<div class= ' col-lg-9 text-right'><label><b>No Data</b></label>";
                    $('#doc-history-table tbody').append(noDataFooter);
                }
            }
        });
    }

    function setDataToHistoryModal(debitNotePaymentID) {
        $('#doc-history-table tbody tr').remove();
        $('#doc-history-table tfoot div').remove();
        $('#doc-history-table tbody div').remove();

        eb.ajax({
            type: 'POST',
            url: BASE_URL + '/debit-note-payments-api/getAllRelatedDocumentDetailsByDebitNotePaymentId',
            data: {debitNotePaymentID: debitNotePaymentID},
            success: function(respond) {
                if (respond.status == true) {
                    $('#doc-history-table thead tr').removeClass('hidden');
                    $.each(respond.data, function(index, value) {
                        if (value != null) {
                            $.each(value, function(index2, value2) {
                                tableBody = "<tr data-docId=" + value2['documentID'] + " data-doctype=" + value2['type'] + "><td class='' >" + value2['type'] + "</td><td>" + value2['code'] + "</td><td>" + value2['amount'] + "</td><td>" + value2['issuedDate'] + "</td><td class='text-center'><span class='glyphicon glyphicon-eye-open openlist link_color document_view ' style='cursor:pointer'></span></td></tr>";
                                $('#doc-history-table tbody').append(tableBody);
                            });
                        }
                    });
                    var footer = " <div class='col-lg-12 col-md-12 col-sm-12 text-right'><button type='button' class='btn btn-primary col-lg-12 col-md-12 col-sm-12' data-dismiss='modal' aril-hidden='true'>Back</button></div>";
                    $('#doc-history-table tfoot').append(footer);
                } else {
                    $('#doc-history-table thead tr').addClass('hidden');
                    var noDataFooter = "<div class= ' col-lg-9 text-right'><label><b>No Data</b></label>";
                    $('#doc-history-table tbody').append(noDataFooter);
                }
            }
        });
    }


    function setDataToHistoryModal(paymentID) {
        $('#doc-history-table tbody tr').remove();
        $('#doc-history-table tfoot div').remove();
        $('#doc-history-table tbody div').remove();

        eb.ajax({
            type: 'POST',
            url: BASE_URL + '/cheque-deposit-api/getAllRelatedDocumentDetailsByPaymentID',
            data: {paymentID: paymentID},
            success: function(respond) {
                if (respond.status == true) {
                    $('#doc-history-table thead tr').removeClass('hidden');
                    $.each(respond.data, function(index, value) {
                        if (value != null) {
                            $.each(value, function(index2, value2) {
                                tableBody = "<tr data-docId=" + value2['documentID'] + " data-doctype=" + value2['type'] + "><td class='' >" + value2['type'] + "</td><td>" + value2['code'] + "</td><td>" + value2['amount'] + "</td><td>" + value2['issuedDate'] + "</td><td class='text-center'><span class='glyphicon glyphicon-eye-open openlist link_color document_view ' style='cursor:pointer'></span></td></tr>";
                                $('#doc-history-table tbody').append(tableBody);
                            });
                        }
                    });
                    var footer = " <div class='col-lg-12 col-md-12 col-sm-12 text-right'><button type='button' class='btn btn-primary col-lg-12 col-md-12 col-sm-12' data-dismiss='modal' aril-hidden='true'>Back</button></div>";
                    $('#doc-history-table tfoot').append(footer);
                } else {
                    $('#doc-history-table thead tr').addClass('hidden');
                    var noDataFooter = "<div class= ' col-lg-9 text-right'><label><b>No Data</b></label>";
                    $('#doc-history-table tbody').append(noDataFooter);
                }
            }
        });
    }

     function setDataToHistoryModalOfOutgoingPayment(payID, type = null, documentTypeID = null) {
        $('#doc-history-table tbody tr').remove();
        $('#doc-history-table tfoot div').remove();
        $('#doc-history-table tbody div').remove();

        eb.ajax({
            type: 'POST',
            url: BASE_URL + '/supplierPaymentsAPI/getAllRelatedDocumentDetailsByPaymentID',
            data: {payID: payID, type: type, documentTypeID: documentTypeID},
            success: function(respond) {
                if (respond.status == true) {
                    $('#doc-history-table thead tr').removeClass('hidden');
                    $.each(respond.data, function(index, value) {
                        if (value != null) {
                            $.each(value, function(index2, value2) {
                                tableBody = "<tr data-docId=" + value2['documentID'] + " data-doctype=" + value2['type'] + "><td class='' >" + value2['type'] + "</td><td>" + value2['code'] + "</td><td>" + value2['amount'] + "</td><td>" + value2['issuedDate'] + "</td><td class='text-center'><span class='glyphicon glyphicon-eye-open openlist link_color document_view ' style='cursor:pointer'></span></td></tr>";
                                $('#doc-history-table tbody').append(tableBody);
                            });
                        }
                    });
                    var footer = " <div class='col-lg-12 col-md-12 col-sm-12 text-right'><button type='button' class='btn btn-primary col-lg-12 col-md-12 col-sm-12' data-dismiss='modal' aril-hidden='true'>Back</button></div>";
                    $('#doc-history-table tfoot').append(footer);
                } else {
                    $('#doc-history-table thead tr').addClass('hidden');
                    var noDataFooter = "<div class= ' col-lg-9 text-right'><label><b>No Data</b></label>";
                    $('#doc-history-table tbody').append(noDataFooter);
                }
            }
        });
    }

     $('#doc-history-table').on('click', '.document_view', function() {
        var documentId = $(this).parents('tr').attr('data-docid');
        var documentType = $(this).parents('tr').attr('data-doctype');
        $('#DocumentViewModal').modal('show');
        getPrintPreviewByDocumentTypeAndDocumentID(documentId, documentType);
    });

    function getPrintPreviewByDocumentTypeAndDocumentID(documentID, documentType) {
        var $iframe = $('#related-document-view');
        $iframe.ready(function() {
            $iframe.contents().find("body div").remove();
        });
        var URL;
        if (documentType == 'SalesInvoice') {
            URL = BASE_URL + '/invoice/document/' + documentID;
        }
        else if (documentType == 'CustomerPayment') {
            URL = BASE_URL + '/customerPayments/document/' + documentID;
        }
        else if (documentType == 'Income') {
            URL = BASE_URL + '/income/document/' + documentID;
        } else  if (documentType == 'PurchaseOrder') {
            URL = BASE_URL + '/po/document/' + documentID;
        } else if (documentType == 'Grn') {
            URL = BASE_URL + '/grn/document/' + documentID;
        }
        else if (documentType == 'PurchaseInvoice') {
            URL = BASE_URL + '/pi/document/' + documentID;
        }
        else if (documentType == 'PurchaseReturn') {
            URL = BASE_URL + '/pr/document/' + documentID;
        }
        else if (documentType == 'DebitNote') {
            URL = BASE_URL + '/debit-note/document/' + documentID;
        }
        else if (documentType == 'SupplierPayment') {
            URL = BASE_URL + '/supplierPayments/document/' + documentID;
        }
        else if (documentType == 'DebitNotePayment') {
            URL = BASE_URL + '/debit-note-payments/document/' + documentID;
        } else if (documentType == 'PaymentVoucher') {
            URL = BASE_URL + '/expense-purchase-invoice/document/' + documentID;
        } else if (documentType == 'JournalEntry') {
            URL = BASE_URL + '/journal-entries/document/' + documentID;
        } else if (documentType == 'GoodIssue') {
            URL = BASE_URL + '/inventory-adjustment/document/' + documentID;
        } else if (documentType == 'PettyCashVoucher') {
            URL = BASE_URL + '/petty-cash-voucher/document/' + documentID;
        }
        
        eb.ajax({
            type: 'POST',
            url: URL,
            success: function(respond) {
                var division = "<div></div>";
                $iframe.ready(function() {
                    $iframe.contents().find("body").append(division);
                });
                $iframe.ready(function() {
                    $iframe.contents().find("body div").append(respond);
                });
            }
        });

    }
});


