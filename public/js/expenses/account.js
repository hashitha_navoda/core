$(document).ready(function() {

    var regExString = /^[a-zA-Z0-9-]+$/;
    var regExBalance = /^-?\d*\.?\d+$/;

    var accountId;
    var accountName;
    var entityId;

    //for hide selectpicker
    $('#account-number-search').selectpicker('hide');
    $('#bank-search').selectpicker('hide');

    //for load all the account names
    loadDropDownFromDatabase('/account-api/account-name-list-for-dropdown', "", 0, '#account-name-search');
    $('#account-name-search').selectpicker('refresh');

    //for load all the account numbers
    loadDropDownFromDatabase('/account-api/account-number-list-for-dropdown', "", 0, '#account-number-search');
    $('#account-number-search').selectpicker('refresh');

    //for load all the customers
    loadDropDownFromDatabase('/bank-api/bank-name-list-for-dropdown', "", 0, '#bank-search');
    $('#bank-search').selectpicker('refresh');

    //for load finance accounts
    loadDropDownFromDatabase('/accounts-api/search-active-accounts-for-dropdown', "", "", '#financeAccountID');

    //for account search
    $('#account-search-select').on("change", function() {
        switch (this.value) {
            case 'account-name':
                $('#account-name-search').selectpicker('show');
                $('#account-number-search').selectpicker('hide');
                $('#bank-search').selectpicker('hide');
                break;

            case 'account-number':
                $('#account-name-search').selectpicker('hide');
                $('#account-number-search').selectpicker('show');
                $('#bank-search').selectpicker('hide');
                break;

            case 'bank-name':
                $('#account-name-search').selectpicker('hide');
                $('#account-number-search').selectpicker('hide');
                $('#bank-search').selectpicker('show');
                break;

            default:
                console.error('Invalid option');
        }
    });

    //for create modal show btn
    $('#create-account').on('click', function() {
    	resetAccountForm();
        //enable account balance field
        $('#accountBalance').prop('disabled', false);
        //enable account balance field
        $('#currencyId').prop('disabled', false);
        //for clear text fields
        $('input[type=text]').val('');
        //reset select boxes
        $('#accountStatus,#bankId,#accountTypeId,#currencyId').prop('selectedIndex', 0);
        //reset bank branch
        $('#bankBranchId').empty().append($("<option>", {value: '', html: 'select branch'}));
        //change modal title
        $('#accountModalLabel').html('Create Account');
        //show create btn
        $('#btnCreate').removeClass('hidden');
        $('#btnUpdate').addClass('hidden');

        $('#isCardPaymentAccount').prop('checked', false).trigger('change');
        $('#cardTypes').empty().selectpicker('refresh');
        // get available card type list
        eb.ajax({
            url: BASE_URL + '/api/card-type/get-un-assigned-card-list',
            method: 'POST',
            success: function(respond) {
                if (respond.status) {
                    // add to card type dropdown
                    $.each(respond.data, function(val, text) {
                        $('#cardTypes').append($('<option></option>').val(val).html(text));
                    });
                    $('#cardTypes').selectpicker('refresh');
                }
            }
        });

    });

	function resetAccountForm()
	{
		$('#financeAccountID').find('option').remove();
		$('#financeAccountID').append("<option value=''>Select an Account</option>");
        $('#financeAccountID').val('').selectpicker('refresh');
	}

    //for create btn
    $('#accountModal').on('click', '#btnCreate', function() {
        var params = {
            accountName: $('#accountName').val(),
            accountNumber: $('#accountNumber').val(),
            accountBalance: '0.00',
            accountStatus: $('#accountStatus').val(),
            accountTypeId: $('#accountTypeId').val(),
            currencyId: $('#currencyId').val(),
            bankId: $('#bankId').val(),
            bankBranchId: $('#bankBranchId').val(),
            isCardPaymentAccount: ($('#isCardPaymentAccount').is(":checked")) ? 1 : 0,
            cardTypes: $('#cardTypes').val(),
            financeAccountID : $('#financeAccountID').val(),
        };
        if (validateForm(params)) {
            createAccount(params);
        }
    });

    //for update btn
    $('#btnUpdate').on('click', function() {

        var params = {
            accountId: accountId,
            entityId: entityId,
            accountName: $('#accountName').val(),
            accountNumber: $('#accountNumber').val(),
            accountBalance: '0.00',
            accountStatus: $('#accountStatus').val(),
            accountTypeId: $('#accountTypeId').val(),
            currencyId: $('#currencyId').val(),
            bankId: $('#bankId').val(),
            bankBranchId: $('#bankBranchId').val(),
            isCardPaymentAccount: ($('#isCardPaymentAccount').is(":checked")) ? 1 : 0,
            cardTypes: $('#cardTypes').val(),
            financeAccountID : $('#financeAccountID').val(),
        };
        if (validateForm(params)) {
            updateAccount(params);
        }
    });

    //for delete btn
    $('#btnDelete').on('click', function() {
        eb.ajax({
            type: 'POST',
            url: BASE_URL + '/account-api/delete-account',
            data: {
                accountId: accountId
            },
            success: function(respond) {
                p_notification(respond.status, respond.msg);
                if (respond.status) {
                    $('#accountDeleteModal').modal('hide');
                    fetchAll();
                }
            }
        });
    });

    //for search btn
    $('#searchBtn').on('click', function() {

        var field = $('#account-search-select').val();
        var value = null;

        switch (field) {
            case 'account-name':
                value = $('#account-name-search').val();
                break;

            case 'account-number':
                value = $('#account-number-search').val();
                break;

            case 'bank-name':
                value = $('#bank-search').val();
                break;

            default:
                console.error('Invalid option');
        }

        $.ajax({
            type: 'POST',
            url: BASE_URL + '/account-api/search',
            data: {
                searchField: field,
                searchValue: value
            },
            success: function(respond) {
                $('#account-list').html(respond);
            }
        });
    });

    //for cancel search
    $('#searchCancelBtn').on('click', function() {
        $('#account-search-select').val('account-name');
        $('#account-name-search').val('').trigger('change');
        $('#account-name-search').selectpicker('show');
        $('#account-number-search').selectpicker('hide');
        $('#bank-search').selectpicker('hide');
        fetchAll();
    });

    //for account actions
    $('#account-list').on('click', '.account-action', function(e) {
    	resetAccountForm();
        var action = $(this).data('action');

        accountId = $(this).closest('tr').data('account-id');
        accountName = $(this).closest('tr').data('account-name');
        entityId = $(this).closest('tr').data('entity-id');

        switch (action) {
            case 'edit':
                e.preventDefault();
                eb.ajax({
                    type: 'POST',
                    url: BASE_URL + '/account-api/get-account',
                    data: {
                        accountId: accountId
                    },
                    success: function(respond) {

                        if(respond.data.accountDetails.financeAccountID != ''){
                        	$('#financeAccountID').append("<option value='"+respond.data.accountDetails.financeAccountID+"'>"+respond.data.accountDetails.financeAccountName+"</option>")
                        	$('#financeAccountID').val(respond.data.accountDetails.financeAccountID).selectpicker('refresh');
                        }
                        //change modal title
                        $('#accountModalLabel').html('Edit Account');
                        //show update btn
                        $('#btnUpdate').removeClass('hidden');
                        $('#btnCreate').addClass('hidden');
                        //show modal
                        $('#accountModal').modal('show');
                        //set data
                        $('#accountName').val(respond.data.accountDetails.accountName);
                        $('#accountNumber').val(respond.data.accountDetails.accountNumber);
                        $('#accountBalance').val(parseFloat(respond.data.accountDetails.accountBalance).toFixed(2));
                        $('#accountStatus').val(respond.data.accountDetails.accountStatus);
                        $('#accountTypeId').val(respond.data.accountDetails.accountTypeId);
                        $('#currencyId').val(respond.data.accountDetails.currencyId);
                        $('#bankId').val(respond.data.accountDetails.bankId);

                        // get available and assigned to current account card type list
                        var assignedCardTypes = [];
                        $('#cardTypes').empty();
                        $.each(respond.data.cardTypes, function(i, cardType) {
                            $('#cardTypes').append($('<option></option>').val(cardType.cardTypeID).html(cardType.cardTypeName));
                            if (cardType.accountID && cardType.accountID != '0') {
                                assignedCardTypes = assignedCardTypes.concat([cardType.cardTypeID]);
                            }
                        });
                        $('#cardTypes').val(assignedCardTypes).selectpicker('refresh');

                        $('#isCardPaymentAccount').prop('checked', false);
                        $('#cardTypeSelection').addClass('hidden');
                        if (respond.data.accountDetails.isCardPaymentAccount == '1') {
                            $('#isCardPaymentAccount').prop('checked', true);
                            $('#cardTypeSelection').removeClass('hidden');
                        }

                        //disable account balance field
                        $('#accountBalance').prop('disabled', true);
                        //disable account balance field
                        $('#currencyId').prop('disabled', true);
                        //get bank branches
                        getBranchListByBankId(respond.data.accountDetails.bankId, function() {
                            $('#bankBranchId').val(respond.data.accountDetails.bankBranchId);
                        });
                    }
                });
                break;

            case 'delete':
                e.preventDefault();
                $('.accountDeleteModalBody').html('<p>Are you sure you want to delete <b>"' + accountName + '"</b>  account ?</p>');
                break;

            default:
                console.error('Invalid action.');
                break;
        }
    });

    //get selected bank branches
    $('#bankId').on('change', function() {
        var bankId = this.value;
        getBranchListByBankId(bankId, function() {
        });
    });

    //this function for fetach all the accounts
    function fetchAll() {
        $.ajax({
            type: 'GET',
            url: BASE_URL + '/account-api/get-accounts',
            success: function(respond) {
                $('#account-list').html(respond);
            }
        });
    }

    //for create account
    function createAccount(params) {
        eb.ajax({
            type: 'POST',
            url: BASE_URL + '/account-api/create-account',
            data: params,
            success: function(respond) {
                p_notification(respond.status, respond.msg);
                if (respond.status) {
                    $('#accountModal').modal('hide');
                    fetchAll();
                }
            }
        });
    }

    //for update account
    function updateAccount(params) {
        eb.ajax({
            type: 'POST',
            url: BASE_URL + '/account-api/update-account',
            data: params,
            success: function(respond) {
                p_notification(respond.status, respond.msg);
                if (respond.status) {
                    $('#accountModal').modal('hide');
                    fetchAll();
                }
            }
        });
    }

    function validateForm(input) {
        if (input.accountName == null || input.accountName == "") {
            p_notification(false, eb.getMessage('ERR_ACCOUNT_NAME_EMPTY'));
            return false;
        } else if (input.accountNumber == null || input.accountNumber == "") {
            p_notification(false, eb.getMessage('ERR_ACCOUNT_NUMBER_EMPTY'));
            return false;
        } else if (!regExString.test(input.accountNumber)) {
            p_notification(false, eb.getMessage('ERR_ACCOUNT_NUMBER_INVALID'));
            return false;
        } else if (input.bankBranchId == null || input.bankBranchId == "") {
            p_notification(false, eb.getMessage('ERR_ACCOUNT_BANK_BRANCH_EMPTY'));
            return false;
        } else if (input.accountTypeId == null || input.accountTypeId == "") {
            p_notification(false, eb.getMessage('ERR_ACCOUNT_TYPE_EMPTY'));
            return false;
        } else if (input.currencyId == null || input.currencyId == "") {
            p_notification(false, eb.getMessage('ERR_ACCOUNT_CURRENCY_EMPTY'));
            return false;
        } else if (input.accountBalance == null || input.accountBalance == "") {
            p_notification(false, eb.getMessage('ERR_ACCOUNT_BALANCE_EMPTY'));
            return false;
        } else if (!regExBalance.test(input.accountBalance)) {
            p_notification(false, eb.getMessage('ERR_ACCOUNT_BALANCE_TYPE'));
            return false;
        } else if (parseFloat(input.accountBalance) >= Math.pow(10, 15) || parseFloat(input.accountBalance) <= Math.pow(10, 15) * -1) {
            p_notification(false, eb.getMessage('ERR_ACCOUNT_BALANCE_LIMIT'));
            return false;
        } else if (input.isCardPaymentAccount && (input.cardTypes === null || input.cardTypes === '')) {
            p_notification(false, eb.getMessage('ERR_ACCOUNT_CARD_TYPE_EMPTY'));
            return false;
        } else if(input.financeAccountID == null || input.financeAccountID == '' ){
        	p_notification(false, eb.getMessage('ERR_ACCOUNT_GL_ACCOUNT_EMPTY'));
            return false;
       	}else{
            return true;
        }
    }

    function getBranchListByBankId(bankId, callback) {
        $.ajax({
            type: 'POST',
            url: BASE_URL + '/bank-api/bank-branch-list-for-dropdown',
            data: {bankId: bankId},
            success: function(respond) {
                //empty options
                $('#bankBranchId').empty();
                $('#bankBranchId').append($("<option>", {value: '', html: 'select branch'}));
                $(respond.data).each(function(i, v) {
                    $('#bankBranchId').append($("<option>", {value: v.branchId, html: v.branchName}));
                });
                callback();
            }
        });
    }

    $('#accountModal').on('change', '#isCardPaymentAccount', function() {
        if ($('#isCardPaymentAccount').is(":checked")) {
            $('#cardTypeSelection').removeClass('hidden');
        } else {
            $('#cardTypeSelection').addClass('hidden');
        }
    });

});
