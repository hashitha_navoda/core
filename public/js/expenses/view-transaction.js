$(document).ready( function(){
    
    var transactionId = null;
    var transactionType = null;
    var $selectedRow = null;
    
    loadDropDownFromDatabase('/account-api/account-list-for-dropdown', "", 0, '#account-name-search');
    
    $('.datepicker').datetimepicker({
        minView: 4,
        todayBtn: true,
        autoclose: true
    });
    
    $('#transaction-category-select').on('change', function (){
        if(this.value == 'cash-in-hand'){
            $('#cash-in-hand-types-div').removeClass('hidden');
            $('#account-types-div').addClass('hidden');
            $('#account-name-search-div').addClass('hidden');
        } else {
            $('#cash-in-hand-types-div').addClass('hidden');
            $('#account-types-div').removeClass('hidden');
            $('#account-name-search-div').removeClass('hidden');
        }
    });
    
    $('#transaction-list').on('click', '.transaction-cancel', function(){
        
        $selectedRow = $(this).closest('tr');
        transactionId = $selectedRow.data('transaction-id');
        transactionType = $selectedRow.data('transaction-type');
        
        
        $('.transactionCancelModalBody').html('<p>Are you sure you want to cancel this transaction ?</p>');
        $('#transactionCancelModal').modal('show');
    });

    $('#transaction-list').on('click', '.transaction-view', function(){
        
        $selectedRow = $(this).closest('tr');
        transactionId = $selectedRow.data('transaction-id');
        transactionType = $selectedRow.data('transaction-type');
        
        eb.ajax({
            type: 'POST',
            url: BASE_URL + '/transaction-api/viewTransaction',
            data: { transactionId : transactionId, transactionType : transactionType},
            success: function(respond) {
                    $('.transactionViewModalBody').html(respond);
        			$('#transactionViewModal').modal('show');
            }
        });
    });
    
    //for cancel transaction
    $('#transactionCancelModal').on('click','#btnCancel',function(){
        eb.ajax({
            type: 'POST',
            url: BASE_URL + '/transaction-api/cancel',
            data: { transactionId : transactionId, transactionType : transactionType},
            success: function(respond) {
                p_notification(respond.status, respond.msg);
                if(respond.status){
                    $('#transactionCancelModal').modal('hide');
                    $selectedRow.addClass('hidden');
                }                
            }
        });
    });
    
    //for search form validation
    $('#filter-button').on('click', function(){
        
        var params = {};
        params.accountId = $('#account-name-search').val();
        params.transactionType =$('#account-transaction-type-select').val();
        params.toDate = $('#to-date').val();
        params.fromDate = $('#from-date').val();
        
        if(validateSeachForm(params)){
            eb.ajax({
                type: 'POST',
                url: BASE_URL + '/transaction-api/search',
                data: params,
                success: function(respond) {
                    $('#transaction-list').html(respond);               
                }
            });
        }        
    })
    
    function validateSeachForm(input){
        if(input.transactionType == '' || input.transactionType == null){
            p_notification(false,'Please selcet atleast one transaction type');
            return false;
        } else if(input.toDate && input.fromDate == ""){
            p_notification(false,'Please selcet start date');
            return false;
        } else if(input.fromDate && input.toDate == ""){
            p_notification(false,'Please selcet end date');
            return false;
        } else if(input.fromDate > input.toDate){
            p_notification(false,'Invalid date range');
            return false;
        } else {
            return true;
        }
    }
    
});