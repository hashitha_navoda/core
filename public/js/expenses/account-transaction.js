var ignoreBudgetLimitFlag = false;
$(document).ready(function() {

    var issuedBankId = null;
    var receivedBankId = null;
    var transaction = null;
    var regExAmount = /^-?\d*\.?\d+$/;
    var accountCurrency = '';

    resetFields();

    loadDropDownFromDatabase('/accounts-api/search-active-accounts-for-dropdown', "", "", '#transactionIssuedFinanceAccountId');
    loadDropDownFromDatabase('/accounts-api/search-active-accounts-for-dropdown', "", "", '#transactionRecivedFinanceAccountId');

    $('#transactionDate').datetimepicker({
        format: 'yyyy-mm-dd',
        minView: 4,
        todayBtn: true,
        autoclose: true
    });

    $(document).on('change', '#transaction', function() {
        transaction = $(this).val();
        resetFields();
        if (transaction == 'deposit') {
            $('#received-account-div').addClass('hidden');
            $('#issued-bank-account-div').removeClass('hidden');
        } else {
            $('#issued-bank-account-div').addClass('hidden');
            $('#received-account-div').removeClass('hidden');
        }
    });

    $(document).on('click', '#saveBtn', function() {
        var input = {};
        input.transaction = $('#transaction').val();
        input.transactionIssuedFinanceAccountId = $('#transactionIssuedFinanceAccountId').val();
        input.transactionRecivedFinanceAccountId = $('#transactionRecivedFinanceAccountId').val();
        input.transactionBankAccountId = $('#transactionBankAccountId').val();
        input.transactionAmount = $('#transactionAmount').val();
        input.transactionDate = $('#transactionDate').val();
        input.transactionComment = $('#transactionComment').val();
        input.ignoreBudgetLimit = ignoreBudgetLimitFlag;

        if (validateForm(input)) {
           createTransaction(input);
        }

    });

    function createTransaction(input)
    {
        eb.ajax({
            type: 'POST',
            url: BASE_URL + '/transaction-api/create',
            data: input,
            success: function(respond) {
                if (respond.status) {
                    p_notification(respond.status, respond.msg);
                    resetFields();
                    $('#transaction').val('withdrawal');
                } else {
                    if (respond.data == "NotifyBudgetLimit") {
                        bootbox.confirm(respond.msg+' ,Are you sure you want to save?', function(result) {
                            if (result == true) {
                                ignoreBudgetLimitFlag = true;
                                input.ignoreBudgetLimit = true;
                                createTransaction(input);
                            } else {
                                setTimeout(function(){ 
                                    location.reload();
                                }, 3000);
                            }
                        });
                    } else {
                        p_notification(respond.status, respond.msg);
                    }
                }
            }
        });
    }

    function validateForm(input) {
        if ((input.transactionIssuedFinanceAccountId == null || input.transactionIssuedFinanceAccountId == "")) {
            p_notification(false, eb.getMessage('ERROR_ISSUED_ACCOUNT_NOT_SET'));
            return false;
        } else if (input.transaction != 'deposit' && (input.transactionRecivedFinanceAccountId == null || input.transactionRecivedFinanceAccountId == "")) {
            p_notification(false, eb.getMessage('ERROR_RECEIVED_ACCOUNT_NOT_SET'));
            return false;
        }else if(input.transaction == 'deposit' && (input.transactionBankAccountId == null || input.transactionBankAccountId == '')){
        	p_notification(false, eb.getMessage('ERROR_BANK_ACCOUNT_NOT_SET'));
            return false;
        }

        if (input.transaction != 'deposit' && input.transactionRecivedFinanceAccountId == input.transactionIssuedFinanceAccountId) {
            p_notification(false, eb.getMessage('ERROR_SAME_ACCOUT'));
            return false;
        }

        if (input.transactionAmount == null || input.transactionAmount == "") {
            p_notification(false, eb.getMessage('ERROR_TRANSACTION_AMOUNT'));
            return false;
        } else if (!regExAmount.test(input.transactionAmount)) {
            p_notification(false, eb.getMessage('ERROR_DATA_TYPE_IN_AMOUT_FIELD'));
            return false;
        } else if (input.transactionAmount <= 0){
            p_notification(false, eb.getMessage('ERROR_NEGATIVE_IN_AMOUNT_FIELD'));
            return false;
        }
        if (input.transactionDate == null || input.transactionDate == "") {
            p_notification(false, eb.getMessage('ERROR_TRANSACTION_DATE'));
            return false;
        } else {
            return true;
        }
    }

    function resetFields() {
        accountCurrency = '';
        $('#currencySymbolLabel').text('');
        $('#transactionAmount,#transactionDate,#transactionComment').val('');
        $('#transactionIssuedFinanceAccountId').empty().append("<option value=''>Select an Account</option>");
        $('#transactionIssuedFinanceAccountId').val(0).selectpicker('refresh');
        $('#transactionRecivedFinanceAccountId').empty().append("<option value=''>Select an Account</option>");
        $('#transactionRecivedFinanceAccountId').val(0).selectpicker('refresh');
        $('#transactionBankAccountId').val('').selectpicker('refresh');
    }

});