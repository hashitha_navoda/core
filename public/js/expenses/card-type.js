$(document).ready(function() {
    $('#card-type-form').submit(function(e) {
        e.preventDefault();
        var formData = {
            cardTypeName: $('#cardTypeName').val().trim(),
            cardTypeID: $('#cardTypeID').val()
        }

//        check card type name validtion
        if (formData.cardTypeName == null || formData.cardTypeName == '') {
            p_notification(false, eb.getMessage('ERR_CARD_TYPE_NAME_EMPTY'));
            $('#cardTypeName').focus();
            return false;
        }

//      check add new or update card type
        var submitted_btn = $("[type='submit']:visible", $(this));
        var action = (submitted_btn.hasClass('update')) ? 'update' : 'save';

        eb.ajax({
            url: BASE_URL + '/api/card-type/' + action,
            data: formData,
            method: 'POST',
            success: function(respond) {
                p_notification(respond.status, respond.msg);
                if (respond.status) {
                    clearForm();
                    $('#account-type-list').html(respond.html);
                }
            }
        });
    });

    $('.reset').on('click', function(e) {
        clearForm();
    });

    $('#account-type-list').on('click', '.card-type-update', function() {
        var currentRow = $(this).parents('tr');
        $('#cardTypeName').val(currentRow.find('.cardTypeName').text());
        $('#cardTypeID').val(currentRow.data('type-id'));
        $(".category-form-container").addClass('update');
        $("html, body").animate({scrollTop: $(".category-form-container").offset().top - 80});
    });

    $('#account-type-list').on('click', '.card-type-delete', function() {
        var currentRow = $(this).parents('tr');

        bootbox.confirm('Are you sure you want to delete this Card Type?', function(result) {
            if (result === true) {
                eb.ajax({
                    url: BASE_URL + '/api/card-type/delete',
                    data: {cardTypeID: currentRow.data('type-id')},
                    method: 'POST',
                    success: function(respond) {
                        p_notification(respond.status, respond.msg);
                        if (respond.status) {
                            clearForm();
                            $('#account-type-list').html(respond.html);
                        }
                    }
                });
            }
        });
    });

    $('#card-type-search-form').submit(function(e) {
        e.preventDefault();
        var searchKey = $('#cardTypeSearch').val().trim();

        if (!searchKey || searchKey == '') {
            p_notification(false, eb.getMessage('ERR_CARD_TYPE_SEARCH_EMPTY'))
            return false;
        }
        searchCardType(searchKey);
    });

    $('#btnSearchCancel').on('click', function() {
        searchCardType();
    });

    var searchCardType = function(searchKey) {
        eb.ajax({
            url: BASE_URL + '/api/card-type/search',
            data: {searchKey: searchKey},
            method: 'POST',
            success: function(respond) {
                if (respond.status) {
                    $('#account-type-list').html(respond.html);
                } else {
                    p_notificaiton(false, eb.getMessage('ERR_CARD_TYPE_SEARCH'))
                }
            }
        });
    }

    var clearForm = function() {
        $('#cardTypeName').val('');
        $('#cardTypeID').val('');
        $(".category-form-container").removeClass('update');
    }
});