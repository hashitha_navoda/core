var ignoreBudgetLimitFlag = false;
$(document).ready(function() {
    var selectedSupplier;
    var pvSearchByPv;
    $('.searchByPayVou').addClass('hidden');

    $('#pvSearchSelector').on('change', function() {
        if ($(this).val() == 'searchBySupplier') {
            $('.searchByPayVou').addClass('hidden');
            $('.searchBySup').removeClass('hidden');
        } else {
            $('.searchByPayVou').removeClass('hidden');
            $('.searchBySup').addClass('hidden');
        }
    });
    $('#pvSearchClear').on('click', function() {
        $('#pvSearchBySupplier').val('').trigger('change');
        $('#pvSearchByPv').val('').trigger('change');
        getPaymentVoucherDetails(null);
    });

    $('#expPVList').on('click', '.doc_attachments', function() {
        setDataToAttachmentViewModal($(this).attr('data-pv-related-id'));
        $('#viewAttachmentModal').modal('show');
    });

    $('#expPVList').on('click', '.payment-voucher_related_docs', function() {
        setDataToHistoryModal($(this).attr('data-payment-voucher-id'));
        $('#addDocHistoryModal').modal('show');
    });

    $('#doc-history-table').on('click', '.document_view', function() {
        var documentId = $(this).parents('tr').attr('data-docid');
        var documentType = $(this).parents('tr').attr('data-doctype');
        $('#DocumentViewModal').modal('show');
        getPrintPreviewByDocumentTypeAndDocumentID(documentId, documentType);
    });

    function getPrintPreviewByDocumentTypeAndDocumentID(documentID, documentType) {
        var $iframe = $('#related-document-view');
        $iframe.ready(function() {
            $iframe.contents().find("body div").remove();
        });
        var URL;
        if (documentType == 'SupplierPayment') {
            URL = BASE_URL + '/supplierPayments/document/' + documentID;
        } else if (documentType == 'PaymentVoucher') {
            URL = BASE_URL + '/expense-purchase-invoice/document/' + documentID;
        }
        eb.ajax({
            type: 'POST',
            url: URL,
            success: function(respond) {
                var division = "<div></div>";
                $iframe.ready(function() {
                    $iframe.contents().find("body").append(division);
                });
                $iframe.ready(function() {
                    $iframe.contents().find("body div").append(respond);
                });
            }
        });

    }

    function setDataToHistoryModal(paymentVoucherID) {
        $('#doc-history-table tbody tr').remove();
        $('#doc-history-table tfoot div').remove();
        $('#doc-history-table tbody div').remove();

        eb.ajax({
            type: 'POST',
            url: BASE_URL + '/expense-purchase-invoice-api/getAllRelatedDocumentDetailsByPaymentVoucherID',
            data: {paymentVoucherID: paymentVoucherID},
            success: function(respond) {
                if (respond.status == true) {
                    $('#doc-history-table thead tr').removeClass('hidden');
                    $.each(respond.data, function(index, value) {
                        if (value != null) {
                            $.each(value, function(index2, value2) {
                                tableBody = "<tr data-docId=" + value2['documentID'] + " data-doctype=" + value2['type'] + "><td class='' >" + value2['type'] + "</td><td>" + value2['code'] + "</td><td>" + value2['amount'] + "</td><td>" + value2['issuedDate'] + "</td><td class='text-center'><span class='glyphicon glyphicon-eye-open openlist link_color document_view ' style='cursor:pointer'></span></td></tr>";
                                $('#doc-history-table tbody').append(tableBody);
                            });
                        }
                    });
                    var footer = " <div class='col-lg-12 col-md-12 col-sm-12 text-right'><button type='button' class='btn btn-primary col-lg-12 col-md-12 col-sm-12' data-dismiss='modal' aril-hidden='true'>Back</button></div>";
                    $('#doc-history-table tfoot').append(footer);
                } else {
                    $('#doc-history-table thead tr').addClass('hidden');
                    var noDataFooter = "<div class= ' col-lg-9 text-right'><label><b>No Data</b></label>";
                    $('#doc-history-table tbody').append(noDataFooter);
                }
            }
        });
    }


    $('#expPVList').on('click', '.deletePV',function(e){
        e.preventDefault();
        var pVID = $(this).data('id');
        deletePV(pVID);
    });

    function deletePV(pVID)
    {
        bootbox.confirm('Are you sure you want to cancel this payment voucher?', function(result) {
            if (result === true) {
                eb.ajax({
                    url: BASE_URL + '/expense-purchase-invoice-api/delete-payment-voucher',
                    method: 'post',
                    data: {pVID: pVID, ignoreBudgetLimit : ignoreBudgetLimitFlag},
                    dataType: 'json',
                    success: function(data) {
                        if (data.status == true) {
                            p_notification(data.status, data.msg);
                            window.location.reload();
                        } else {
                            if (data.data == "NotifyBudgetLimit") {
                                bootbox.confirm(data.msg+' ,Are you sure you want to cancel ?', function(result) {
                                    if (result == true) {
                                        ignoreBudgetLimitFlag = true;
                                        deletePV(pVID);
                                    } else {
                                        setTimeout(function(){ 
                                            location.reload();
                                        }, 3000);
                                    }
                                });
                            } else {
                                p_notification(data.status, data.msg);
                                $('#piSave').prop('disabled', false);
                            }
                        }
                    }
                });
            }
        });
    }


    loadDropDownFromDatabase('/supplierAPI/search-suppliers-for-dropdown', "", 0, '#pvSearchBySupplier');
    $('#pvSearchBySupplier').selectpicker('refresh');
    $('#pvSearchBySupplier').on('change', function() {
        if ($(this).val() > 0 && $(this).val() != selectedSupplier) {
            selectedSupplier = $(this).val();
            getPaymentVoucherDetails(selectedSupplier);

        }
    });
    loadDropDownFromDatabase('/expense-purchase-invoice-api/getPaymentVoucherForDropDown', "", 0, '#pvSearchByPv');
    $('#pvSearchByPv').selectpicker('refresh');
    $('#pvSearchByPv').on('change', function() {
        if ($(this).val() > 0 && $(this).val() != pvSearchByPv) {
            pvSearchByPv = $(this).val();
            getPaymentVoucherDetails('', pvSearchByPv);

        }
    });

    function getPaymentVoucherDetails(selectedSupplier, pvSearchByPv) {
        eb.ajax({
            type: 'POST',
            url: BASE_URL + '/expense-purchase-invoice-api/get-purchase-invoice-view-by-filter',
            data: {supplierID: selectedSupplier, pvID: pvSearchByPv},
            success: function(respond) {
                $('#expPVList').html(respond.html);
            }
        });
    }

    function setDataToAttachmentViewModal(documentID) {
        $('#doc-attach-table tbody tr').remove();
        $('#doc-attach-table tfoot div').remove();
        $('#doc-attach-table tbody div').remove();

        eb.ajax({
            type: 'POST',
            url: BASE_URL + '/get-document-related-attachement',
            data: {
                documentID: documentID,
                documentTypeID: 20
            },
            success: function(respond) {
                if (respond.status == true) {
                    $('#doc-attach-table thead tr').removeClass('hidden');
                    $.each(respond.data, function(index, value) {
                        tableBody = "<tr><td><a href="+value.link+">"+value.docName+"</a></td></tr>";
                        $('#doc-attach-table tbody').append(tableBody);
                    });
                } else {
                    $('#doc-attach-table thead tr').addClass('hidden');
                    var noDataFooter = "<div class= ' col-lg-6 text-right'><label><b>No Data</b></label>";
                    $('#doc-attach-table tbody').append(noDataFooter);
                }
            }
        });
    }
});