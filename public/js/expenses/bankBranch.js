$(document).ready(function(){
    
    var entityId;
    var bankBranchId;
    var bankBranchName;
    var bankBranchCode;
    var bankId = $('#bankHeader').data('bank-id');
    
    //for create modal show btn
    $('#add-bank-branch').on('click', function(){
        //for clear text fields
        $('#bankBranchCode,#bankBranchName,#bankBranchAddress').val('');
        //set modal title
        $('#bankBranchModalLabel').html('Add New Branch');
        //show create btn
        $('#btnAdd').removeClass('hidden');
        $('#btnUpdate').addClass('hidden');
    });
    
    //for add btn
    $('#btnAdd').on( 'click', function(){
        
        var bankBranchCode = $('#bankBranchCode').val();
        var bankBranchName = $('#bankBranchName').val();
        var bankBranchAddress = $('#bankBranchAddress').val();
        
        if(bankBranchCode.trim() && bankBranchName.trim()){
            eb.ajax({
                type: 'POST',
                url: BASE_URL + '/bank-api/add-bank-branch',
                data: {
                    bankBranchCode:bankBranchCode,
                    bankBranchName:bankBranchName,
                    bankBranchAddress:bankBranchAddress,
                    bankId:bankId
                },
                success: function(respond) {
                    p_notification(respond.status, respond.msg);
                    if(respond.status){
                        $('#bankBranchModal').modal('hide');
                        fetchAll(bankId);
                    }                
                }
            });
            
        } else {
            p_notification(false, eb.getMessage('ERR_BRANCH_REQUIRED'));
        }
    });
    
    //for update btn
    $('#btnUpdate').on( 'click',function(){
        
        var bankBranchCode = $('#bankBranchCode').val();
        var bankBranchName = $('#bankBranchName').val();
        var bankBranchAddress = $('#bankBranchAddress').val();
        
        if(bankBranchCode.trim() && bankBranchName.trim()){
            eb.ajax({
                type: 'POST',
                url: BASE_URL + '/bank-api/update-bank-branch',
                data: {
                    bankBranchId : bankBranchId,
                    bankBranchCode : bankBranchCode,
                    bankBranchName : bankBranchName,
                    bankBranchAddress : bankBranchAddress
                },
                success: function(respond) {
                    p_notification(respond.status, respond.msg);
                    if(respond.status){
                        $('#bankBranchModal').modal('hide');
                        fetchAll(bankId);
                    }                
                }
            });
        } else {
            p_notification(false, eb.getMessage('ERR_BRANCH_REQUIRED'));
        }
    });
    
    //for delete btn
    $('#btnDelete').on('click',function (){
        eb.ajax({
            type: 'POST',
            url: BASE_URL + '/bank-api/delete-bank-branch',
            data: {
                bankBranchId : bankBranchId
            },
            success: function(respond) {
                p_notification(respond.status, respond.msg);
                if(respond.status){
                    $('#branchDeleteModal').modal('hide');
                    fetchAll(bankId);
                }
            }
        });
    });
    
    //for search cancel btn
    $('#btnSearchCancel').on( 'click',function(){
        $('#bankBranchSearch').val('');
        fetchAll(bankId);
    });
    
    //for search
    $('#bankBranchSearch').keyup( function(){
        var key = $('#bankBranchSearch').val();
        
        $.ajax({
            type: 'POST',
            url: BASE_URL + '/bank-api/search-bank-branches',
            data: {
                bankId : $('#bankHeader').data('bank-id'),
                searchKey : key
            },
            success: function(respond) {
                $('#branch-list').html(respond);            
            }
        });
        
    });
    
    $('#branch-list').on('click','.branch-action',function (e){
        var action = $(this).data('action');
        
        bankBranchId = $(this).closest('tr').data('branch-id');
        bankBranchName = $(this).closest('tr').data('branch-name');
        bankBranchCode = $(this).closest('tr').data('branch-code');
        entityId = $(this).closest('tr').data('entity-id');
        
        switch(action) {
            
            case 'edit':
                e.preventDefault();
                //retrive selected bank branch
                eb.ajax({
                    type: 'POST',
                    url: BASE_URL + '/bank-api/get-bank-branch',
                    data: {
                        bankBranchId : bankBranchId
                    },
                    success: function(respond) {
                        console.log(respond);
                        //change modal title
                        $('#bankBranchModalLabel').html('Edit Bank Branch');
                        //show update btn
                        $('#btnUpdate').removeClass('hidden');
                        $('#btnAdd').addClass('hidden');
                        //show modal
                        $('#bankBranchModal').modal('show');
                        //set data
                        $('#bankBranchCode').val(respond.data.bankBranchCode),
                        $('#bankBranchName').val(respond.data.bankBranchName),
                        $('#bankBranchAddress').val(respond.data.bankBranchAddress)
                    }
                });
                break;

            case 'delete':
                e.preventDefault();
                $('.branchDeleteModalBody').html('<p>Are you sure you want to delete <b>"'+bankBranchCode+'"</b> ?</p>');
                break;

            default:
                console.error('Invalid action.');
                break;
        }
    });
    
    //this function for fetach all the branches of the given bank
    function fetchAll(bankId){
        $.ajax({
            type: 'GET',
            url: BASE_URL + '/bank-api/get-bank-branches/'+bankId,
            success: function(respond) {
                $('#branch-list').html(respond);            
            }
        });
    }
    
});
