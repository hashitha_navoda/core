var ignoreBudgetLimitFlag = false;
$(document).ready(function () {

    var depositId = null;
    var accountName = null;
    var canceledcheckList = [];
    
    //for confirm modal
    $('#cheque-deposit-list').on('click','.deposit-cancel', function (e) {
        e.preventDefault();
        depositId = $(this).closest('tr').data('cheque-deposit-id');
        accountName = $(this).closest('tr').data('account-name');
        $('.depositDeleteModalBody').html('<p>Are you sure you want to cancel <b>' + accountName + '</b> \'s cheque deposit ?</p>');
        $('#depositDeleteModal').modal('show');
    });

    //for view and edit seected deposit cheques 
    $('#cheque-deposit-list').on('click','.deposit-view', function (e) {
        e.preventDefault();
        $('.bounced_cheque_div').not('.sample_bounced_cheque').remove();//remove last selected elements
        depositId = $(this).closest('tr').data('cheque-deposit-id');
        var currencySymbol = $(this).closest('tr').data('currency-symbol');
        $('.currencySymbolCls').html("<b>("+currencySymbol+")</b>");
        getDepositedChequeListByDepositId(depositId);
    });

    //for deposit cancel btn
    $('#btnConfirm').on('click', function () {
        cancelDeposits();
    });
    
    //for update deposit btn
    $('#depositModal').on('click','#updateDepositBtn',function(){
        var bankCharge = $('#bankCharge').val();
        var regExBalance = /^[\d.]+$/;

        if(bankCharge && !regExBalance.test(bankCharge)){
            p_notification(false, eb.getMessage('ERR_CHEQUEDEPOSIT_CANCEL_BANK_CHARGE'));
        } else {
           updateDeposits();
        }
    });

    function updateDeposits()
    {
        var bankCharge = $('#bankCharge').val();
        var bouncedCheques = [];
        $('.bounced_cheque_cbx').each(function(index, element){
            if ($(this).is(':checked')) {
                var chequeId = $(this).val();
                bouncedCheques.push(chequeId);
            }            
        });

         $.ajax({
            type: 'POST',
            url: BASE_URL + '/cheque-deposit-api/update',
            data: {
                chequeDepositId: depositId,
                paymentMethodsNumbersId:'',
                chequeList:canceledcheckList,
                chequeDepositAmount:$('#checkAmount').val(),
                chequeDepositCancelationCharge:bankCharge,
                chequeDepositCancellationComment:$('#cancelComment').val(),
                bouncedCheques: bouncedCheques,
                ignoreBudgetLimit: ignoreBudgetLimitFlag
            },
            success: function (respond) {
                if (respond.status) {
                    p_notification(respond.status, respond.msg);
                    $('#depositModal').modal('hide');
                    getDeposits();
                } else {
                    if (respond.data == "NotifyBudgetLimit") {
                        bootbox.confirm(respond.msg+' ,Are you sure you want to update ?', function(result) {
                            if (result == true) {
                                ignoreBudgetLimitFlag = true;
                                updateDeposits();
                            } else {
                                setTimeout(function(){ 
                                    location.reload();
                                }, 3000);
                            }
                        });
                    } else {
                        p_notification(respond.status, respond.msg);
                    }
                }
            }
        });
    }


    //for cheque deposit modal actions
    $('#depositModal').on('click','.cheque-deposit', function (e) {

        e.preventDefault();
        var $closestTr = $(this).closest('tr');
        var action = $(this).data('action');
        var chequeId = $closestTr.data('incoming-payment-method-cheque-id');
        var chequeRefNumber = $closestTr.data('refno');

        switch (action) {
            case 'cheque-remove':
                $(this).closest('tr').addClass('text-muted');//for disable row
                $(this).prop('disabled', true);//disable delete btn
                $(this).closest('tr').find('.fa-undo').prop('disabled',false);//enable undo btn
                calculateChequeAmount();
                //for show as bounced cheque
                var bouncedElement = $('.sample_bounced_cheque').clone().removeClass('sample_bounced_cheque hidden');
                bouncedElement.find('.bounced_cheque_cbx').attr('value',chequeId);
                bouncedElement.find('.bounced_cheque_cbx_lbl').text(chequeRefNumber);
                bouncedElement.attr('data-chequeid',chequeId);
                $('#bounced_cheque_div').append(bouncedElement);
                break;

            case 'cheque-undo':
                $(this).closest('tr').removeClass('text-muted');//for enable row
                $(this).prop('disabled', true);//disable undo btn
                $(this).closest('tr').find('.fa-trash-o').prop('disabled',false);//enable delete btn
                calculateChequeAmount();
                //hide bounced cheque
                $('div[data-chequeid="'+chequeId+'"]').remove();
                break;
                
            default :
                console.log('invalid option');
        }

    });

    function cancelDeposits()
    {
        $.ajax({
            type: 'POST',
            url: BASE_URL + '/cheque-deposit-api/cancel',
            data: {
                chequeDepositId: depositId,
                ignoreBudgetLimit: ignoreBudgetLimitFlag
            },
            success: function (respond) {
                if (respond.status) {
                    p_notification(respond.status, respond.msg);
                    $('#depositDeleteModal').modal('hide');
                    getDeposits();
                } else {
                    if (respond.data == "NotifyBudgetLimit") {
                        bootbox.confirm(respond.msg+' ,Are you sure you want to cancel ?', function(result) {
                            if (result == true) {
                                ignoreBudgetLimitFlag = true;
                                cancelDeposits();
                            } else {
                                setTimeout(function(){ 
                                    location.reload();
                                }, 3000);
                            }
                        });
                    } else {
                        p_notification(respond.status, respond.msg);
                    }
                }
            }
        });
    }

    //this function for fetach all the deposit as html table
    function getDeposits() {
        $.ajax({
            type: 'GET',
            url: BASE_URL + '/cheque-deposit-api/get-deposits',
            success: function (respond) {
                $('#cheque-deposit-list').html(respond);
            }
        });
    }

    //get selected deposit cheque list
    function getDepositedChequeListByDepositId(depositId) {
        $.ajax({
            type: 'POST',
            url: BASE_URL + '/cheque-deposit-api/getDeposit',
            data: {
                chequeDepositId: depositId
            },
            success: function (respond) {
                $('#deposit-cheque-list').html(respond);
                calculateChequeAmount();
                $('#bankCharge,#cancelComment').val('');
                $('#depositModal').modal('show');
            }
        });
    }
    
    //get the selected deposit check amount
    function calculateChequeAmount(){
        var chequeAmount = 0;
        canceledcheckList = [];
        
        $('.cheque-tr').each(function() {
            if(!$(this).hasClass('text-muted')){
                //get total cheque amount
                chequeAmount = chequeAmount + parseFloat($( this ).data('amount'));
            } else {
            	var chequeListArray ={
            		'chequeID':$( this ).data('incoming-payment-method-cheque-id'),
            		'amount':$( this ).data('amount'),
            	}
                canceledcheckList.push(chequeListArray);
            }
        });
        //set cheque amount 
        $('#checkAmount').val(chequeAmount.toFixed(2));
        //for toggle update btn
        if(canceledcheckList.length > 0){
            $('#updateDepositBtn').prop('disabled',false);
            $('.bounced_cheque_container').removeClass('hidden');
        } else {
            $('#updateDepositBtn').prop('disabled',true);
            $('.bounced_cheque_container').addClass('hidden');
        }
    }

});