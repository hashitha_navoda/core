$(document).ready( function(){
    
    var chequeAmount;
    var selectedCheques = [];
    var regExString = /^[a-zA-Z0-9-.]+$/;
    var currencyId = null;
    
    //uncheck all checkbox
    $('input[type=checkbox]').prop('checked', false);
    
    //initialize date picker
    var now = new Date();
    now.setDate(now.getDate() - 1);
    $('.datepicker').datepicker({
        format: 'yyyy-mm-dd',
        // Remove "disabled - back date" as request of Namal
        // onRender: function(date) {
        //     return date.valueOf() < now.valueOf() ? 'disabled' : '';
        // }
    }).on('changeDate', function(){
        $(this).datepicker('hide');
    });;


    $('#inv-custa-search,#pay-search, #inc-search').selectpicker('hide');

    $('#inv-search-select').on('change', function() {
        $('#inv-custa-search,#pay-search,#inv-search, #inc-search').val('').trigger('change');
        customerID = invoiceID = incID = payID = '';
        if ($(this).val() == 'Invoice No') {
            $('#inv-custa-search,#pay-search, #inc-search').selectpicker('hide');
            $('#inv-search').empty();
            $('#inv-search').selectpicker('refresh');
            $('#inv-search').selectpicker('show');
            $('#view-div').hide();
            $('#custadiv').hide();
            $('#invoice-list').show();
            selectedsearch = 'Invoice No';
        } else if ($(this).val() == 'Customer Name') {
            $('#inv-search,#inc-search, #pay-search').selectpicker('hide');
            $('#inv-custa-search').empty();
            $('#inv-custa-search').selectpicker('refresh');
            $('#inv-custa-search').selectpicker('show');
            $('#invoice-list').show();
            $('#view-div').hide();
            $('#custadiv').hide();
            selectedsearch = 'Customer Name';
        } else if ($(this).val() == 'Payment No') {
            $('#inv-search,#inv-custa-search,#inc-search').selectpicker('hide');
            $('#pay-search').empty();
            $('#pay-search').selectpicker('refresh');
            $('#pay-search').selectpicker('show');
            $('#invoice-list').show();
            $('#view-div').hide();
            $('#custadiv').hide();
        }  else if ($(this).val() == 'Income No') {
            $('#inv-custa-search').selectpicker('hide');
            $('#inv-search,#pay-search').selectpicker('hide');

            $('#inc-search').empty();
            $('#inc-search').selectpicker('refresh');
            $('#inc-search').selectpicker('show');
            $('#invoice-list').show();
            $('#view-div').hide();
            $('#custadiv').hide();
        }
    });


    loadDropDownFromDatabase('/customerAPI/search-customers-for-dropdown', "", 'withDeactivatedCustomers', '#inv-custa-search');
    $('#inv-custa-search').selectpicker('refresh');
    $('#inv-custa-search').on('change', function() {
        if ($(this).val() > 0 && customerID != $(this).val()) {
            customerID = $(this).val();
            getCustomerUndepositeCheques(customerID);
        }
    });


    function getCustomerUndepositeCheques(customerID) {
        var getinvbycusturl = '/cheque-deposit-api/retriveCustomerWiseUndepositCheques';
        var requestinvbycust = eb.post(getinvbycusturl, {
            customerID: customerID
        });
        requestinvbycust.done(function(retdata) {
            $('#cheque-list').html('');
            $('#cheque-list').html(retdata);
        
        });
    }

    loadDropDownFromDatabase('/customerPaymentsAPI/search-customer-payments-for-dropdown', "", '', '#pay-search');
    $('#pay-search').selectpicker('refresh');
    $('#pay-search').on('change', function() {
        if ($(this).val() > 0 && customerID != $(this).val()) {
            payNo = $(this).val();
            getUndepositedChequesByPayment(payNo);
        }
    });


    $('#reset-button,#resetSerchByComment').on('click', function() {
        window.location.reload();
    });


    function getUndepositedChequesByPayment(payNo) {
        var getinvbycusturl = '/cheque-deposit-api/retrivePaymentWiseUndepositCheques';
        var requestinvbycust = eb.post(getinvbycusturl, {
            paymentID: payNo
        });
        requestinvbycust.done(function(retdata) {
          
            $('#cheque-list').html('');
            $('#cheque-list').html(retdata);
            
        });
    }


    // set second params as 1. for find this come from view
    loadDropDownFromDatabase('/invoice-api/searchSalesInvoicesForDropdown', 1, '', '#inv-search');
    $('#inv-search').selectpicker('refresh');
    $('#inv-search').on('change', function() {
        if ($(this).val() > 0 && invoiceID != $(this).val()) {
            var invoiceID = $(this).val();
            if ($('#inv-search').val() == '') {
                $('#custadiv').html('');
                $('#custadiv').hide('');
                $('#invoice-list').show();
            } else {
                var searchurl = BASE_URL + '/cheque-deposit-api/retriveInvoiceWiseUndepositCheques';
                var searchrequest = eb.post(searchurl, {invoiceID: invoiceID});
                searchrequest.done(function(retdata) {
                    $('#cheque-list').html('');
                    $('#cheque-list').html(retdata);
                });
            }
        }
    });

    loadDropDownFromDatabase('/income-api/search-income-for-dropdown', "", 0, '#inc-search');
    $('#inc-search').on('change', function() {
        if ($(this).val() > 0 && incomeID != $(this).val()) {
            var incomeID = $(this).val();
            var searchurl = BASE_URL + '/cheque-deposit-api/retriveIncomeWiseUndepositCheques';
            var searchrequest = eb.post(searchurl, {incomeID: incomeID});
            searchrequest.done(function(retdata) {
                $('#cheque-list').html('');
                $('#cheque-list').html(retdata);
            });
        } else {
            $('#custadiv').html('');
            $('#custadiv').hide('');
            $('#deliverynote-list').show();
        }
    });

    $('#searchByRef').on('click', function() {
        if ($('#chequeRefSearch').val() == '') {
            p_notification(false, eb.getMessage('ERR_VIEWINV_COMMENTFIELD_FOR_SEARCH'));
        } else {
            var invoicefilter = BASE_URL + '/cheque-deposit-api/retriveUndepositChequesByRef';
            var filterrequest = eb.post(invoicefilter, {
                'searchString': $('#chequeRefSearch').val(),
            });
            filterrequest.done(function(retdata) {
                $('#cheque-list').html('');
                $('#cheque-list').html(retdata);
                
            });
        }
    });

    
    //for select all cheques
    $('#cheque-list').on( 'change','#allChequesBox', function() {
        var currencyArr = [];
        $('.cheque-box').each(function(index) {
            var currencyId = $(this).closest('tr').data('currency-id');
            if($.inArray( currencyId, currencyArr ) === -1){
                currencyArr.push(currencyId);
            }
        });
        
        if(currencyArr.length > 1){
            $('#allChequesBox').prop('checked', false);
            p_notification( false, "please select cheques which have similar currency");
        } else {
            if(this.checked) {
                $('.cheque-box').prop('checked', true);
                currencyId = currencyArr[0];
            } else {
                $('.cheque-box').prop('checked', false);
                selectedCheques = [];
                currencyId = null;
            }
        }
        
        if($('.cheque-box:checkbox:checked').length > 0){
            $('#nxtBtn').removeClass('disabled');
        } else {
            $('#nxtBtn').addClass('disabled');
        }
    });
    
    //for next btn
    $('#nxtBtn').on( 'click', function(){
        chequeAmount = 0;
        selectedCheques = [];
        //for clear text fields
        $('input[type=text]').val('');
        //reset select boxes
        $('#bankId,#accountId').prop('selectedIndex',0);
        
        $('.cheque-box').each(function(index) {
            if($(this).is(':checked')){
                //get total cheque amount
                chequeAmount = chequeAmount + parseFloat($( this ).closest('tr').data('amount'));
                $('#chequeDepositAmount').val(chequeAmount.toFixed(2));
                //selected cheques
                selectedCheques.push($( this ).closest('tr').data('incoming-payment-method-cheque-id'));
            }
        });
    });
    
    $('#resetBtn').on( 'click', function(){
        $('#allChequesBox,.cheque-box').prop('checked', false);
        $('#nxtBtn').addClass('disabled');
        selectedCheques = [];
        currencyId = null;
    });
    
    //for toggle nxtbtn
    $(document).on('change','.cheque-box',function(){
        var _currencyId = $(this).closest('tr').data('currency-id');
        
        if(currencyId === null || currencyId === _currencyId) {
            currencyId = _currencyId;
            if($('.cheque-box:checkbox:checked').length > 0){
                $('#nxtBtn').removeClass('disabled');
            } else {                
                $('#nxtBtn').addClass('disabled');
                currencyId = null;
            }
        } else {
            $(this).prop('checked', false);
            p_notification( false, 'please select cheques which have similar currency');
        }
    });
    
    //get selected bank account list
    $('#bankId').on('change',function(){
        var bankId = this.value;
        getBankAccountListByBankId(bankId);
    });

    $('#cheque-list').on('click', '.related_docs', function() {
        setDataToHistoryModal($(this).attr('data-cn-related-id'));
        $('#addDocHistoryModal').modal('show');
    });


    function setDataToHistoryModal(paymentID) {
        $('#doc-history-table tbody tr').remove();
        $('#doc-history-table tfoot div').remove();
        $('#doc-history-table tbody div').remove();

        eb.ajax({
            type: 'POST',
            url: BASE_URL + '/cheque-deposit-api/getAllRelatedDocumentDetailsByPaymentID',
            data: {paymentID: paymentID},
            success: function(respond) {
                if (respond.status == true) {
                    $('#doc-history-table thead tr').removeClass('hidden');
                    $.each(respond.data, function(index, value) {
                        if (value != null) {
                            $.each(value, function(index2, value2) {
                                tableBody = "<tr data-docId=" + value2['documentID'] + " data-doctype=" + value2['type'] + "><td class='' >" + value2['type'] + "</td><td>" + value2['code'] + "</td><td>" + value2['amount'] + "</td><td>" + value2['issuedDate'] + "</td><td class='text-center'><span class='glyphicon glyphicon-eye-open openlist link_color document_view ' style='cursor:pointer'></span></td></tr>";
                                $('#doc-history-table tbody').append(tableBody);
                            });
                        }
                    });
                    var footer = " <div class='col-lg-12 col-md-12 col-sm-12 text-right'><button type='button' class='btn btn-primary col-lg-12 col-md-12 col-sm-12' data-dismiss='modal' aril-hidden='true'>Back</button></div>";
                    $('#doc-history-table tfoot').append(footer);
                } else {
                    $('#doc-history-table thead tr').addClass('hidden');
                    var noDataFooter = "<div class= ' col-lg-9 text-right'><label><b>No Data</b></label>";
                    $('#doc-history-table tbody').append(noDataFooter);
                }
            }
        });
    }

    $('#doc-history-table').on('click', '.document_view', function() {
        var documentId = $(this).parents('tr').attr('data-docid');
        var documentType = $(this).parents('tr').attr('data-doctype');
        $('#DocumentViewModal').modal('show');
        getPrintPreviewByDocumentTypeAndDocumentID(documentId, documentType);
    });
    
    $('#depositBtn').on('click',function(){
        var params = {
            bankId: $('#bankId').val(),
            accountId: $('#accountId').val(),
            chequeDepositAmount: $('#chequeDepositAmount').val(),
            chequeDepositDate : $('#chequeDepositDate').val(),
            chequeDepositDescription : $('#chequeDepositDescription').val(),
            chequeDepositCurrencyId : currencyId,
            cheques:selectedCheques,
            ignoreBudgetLimit: false
        }
        
        validateForm(params,saveChequeDeposit);
        
    });
    
    //get bank account list
    function getBankAccountListByBankId(bankId){
        $.ajax({
            type: 'POST',
            url: BASE_URL + '/account-api/bank-account-list-for-dropdown',
            data: {bankId : bankId},
            success: function(respond) {
                //empty options
                $('#accountId').empty();
                $('#accountId').append($("<option>", { value: '', html: 'select account' }));
                $(respond.data.list).each(function(i, v){ 
                    $('#accountId').append($("<option>", { value: v.value, html: v.text }));
                });
            }
        });
    }
    
    //save cheque deposit 
    function saveChequeDeposit(data){
        eb.ajax({
            type: 'POST',
            url: BASE_URL + '/cheque-deposit-api/save',
            data: data,
            success: function(respond) {
                if(respond.status){
                    p_notification(respond.status, respond.msg);
                    $('#chequeDepositModal').modal('hide');
                    $('#nxtBtn').addClass('disabled');
                    selectedCheques = [];
                    getUndepositedCheques();
                } else {
                    if (respond.data == "NotifyBudgetLimit") {
                        bootbox.confirm(respond.msg+' ,Are you sure you want to save ?', function(result) {
                            if (result == true) {
                                var params = {
                                    bankId: $('#bankId').val(),
                                    accountId: $('#accountId').val(),
                                    chequeDepositAmount: $('#chequeDepositAmount').val(),
                                    chequeDepositDate : $('#chequeDepositDate').val(),
                                    chequeDepositDescription : $('#chequeDepositDescription').val(),
                                    chequeDepositCurrencyId : currencyId,
                                    cheques:selectedCheques,
                                    ignoreBudgetLimit: true
                                }
                                
                                validateForm(params,saveChequeDeposit);
                            } else {
                                setTimeout(function(){ 
                                    location.reload();
                                }, 3000);
                            }
                        });
                    } else {
                        p_notification(respond.status, respond.msg);
                    }
                }               
            }
        });
    }
    
    //for validate form
    function validateForm(data, callback) {
        var errorMsg = [];
        
        $.each(data,function(key,value){
            
            switch (key) {
                case 'bankId':
                    if (!regExString.test(value)) {
                        errorMsg.push(eb.getMessage('ERR_CHEQUEDEPOSIT_BANK_REQUIRED'));
                    }
                    break;
                    
                case 'accountId':
                    if (!regExString.test(value)) {
                        errorMsg.push(eb.getMessage('ERR_CHEQUEDEPOSIT_ACCOUNT_REQUIRED'));
                    }
                    break;
                    
                case 'chequeDepositAmount':
                    if (!regExString.test(value)) {
                        errorMsg.push(eb.getMessage('ERR_CHEQUEDEPOSIT_AMOUNT_REQUIRED'));
                    }
                    break;
                    
                case 'chequeDepositDate':
                    if (!regExString.test(value)) {
                        errorMsg.push(eb.getMessage('ERR_CHEQUEDEPOSIT_DATE_REQUIRED'));
                    }
                    break;
            }
        });
        
        if(errorMsg.length === 0){
            callback(data);
        } else {
            p_notification( false, errorMsg[0]);
        }
    }
    
    //this function for fetach all the undeposited cheque list as html table
    function getUndepositedCheques(){
        $.ajax({
            type: 'GET',
            url: BASE_URL + '/cheque-deposit-api/get-undeposited-cheques',
            success: function(respond) {
                $('#cheque-list').html(respond);            
            }
        });
    }
});

function getPrintPreviewByDocumentTypeAndDocumentID(documentID, documentType) {
    var $iframe = $('#related-document-view');
    $iframe.ready(function() {
        $iframe.contents().find("body div").remove();
    });
    var URL;
    if (documentType == 'SalesInvoice') {
        URL = BASE_URL + '/invoice/document/' + documentID;
    }
    else if (documentType == 'CustomerPayment') {
        URL = BASE_URL + '/customerPayments/document/' + documentID;
    }
    else if (documentType == 'Income') {
        URL = BASE_URL + '/income/document/' + documentID;
    } 
    
    eb.ajax({
        type: 'POST',
        url: URL,
        success: function(respond) {
            var division = "<div></div>";
            $iframe.ready(function() {
                $iframe.contents().find("body").append(division);
            });
            $iframe.ready(function() {
                $iframe.contents().find("body div").append(respond);
            });
        }
    });

}


