$(document).ready(function() {

    $('#petty-cash-float-list').on('click', 'a.delete', function(e) {
        e.preventDefault();
        var pettyCashFloatID = $(this).parents('tr').attr('data-pettyCashFloatId');
        bootbox.confirm("Are You sure to delete this petty cash float?", function(result) {
            if (result === true) {
                eb.ajax({
                    url: BASE_URL + '/api/petty-cash-float-api/update-status',
                    method: 'POST',
                    data: {pettyCashFloatID: pettyCashFloatID},
                    success: function(respond) {
                        p_notification(respond.status, respond.msg);
                        if (respond.status) {
                            $('#petty-cash-float-list').html(respond.html);
                        }
                    }
                });
            }
        });
        return false;

    });

});