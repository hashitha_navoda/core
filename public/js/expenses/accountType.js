$(document).ready( function(){
    
    var accountTypeId;
    var accountTypeName;
    var entityId;
    
    //for save btn
    $('#btnSave').on('click',function(){        
        var accountTypeName = $('#accountType').val();
        
        if(accountTypeName.trim()){
            eb.ajax({
                type: 'POST',
                url: BASE_URL + '/account-type-api/add',
                data: {
                    accountTypeName : accountTypeName
                },
                success: function(respond) {
                    p_notification(respond.status, respond.msg);                
                    if(respond.status){
                        $('#accountType').val('');
                        fetchAll();                    
                    }                
                }
            });
        } else {
            p_notification(false,eb.getMessage('ERR_ACCOUNT_TYPE_REQUIRED'));
        }       
    });
    
    //for reset btn
    $('#btnRest').on('click',function(){
        $('#accountType').val('');
    });
    
    //for update btn
    $('#btnUpdate').on('click',function(){
        var accountTypeName = $('#accountType').val();
        if(accountTypeName.trim()){
            eb.ajax({
                type: 'POST',
                url: BASE_URL + '/account-type-api/update-account-type',
                data: {
                    accountTypeId : accountTypeId,
                    accountTypeName : $('#accountType').val()
                },
                success: function(respond) {
                    p_notification(respond.status, respond.msg);
                    if(respond.status){                    
                        fetchAll();
                    }
                }
            });
        } else {
            p_notification(false,eb.getMessage('ERR_ACCOUNT_TYPE_REQUIRED'));
        }
    });
    
    //for cancel btn
    $('#btnCancel').on('click',function(){
        $('#accountType').val('');
        accountTypeAddView();
    });
    
    //for delete btn
    $('#btnDelete').on('click',function(){
        eb.ajax({
            type: 'POST',
            url: BASE_URL + '/account-type-api/delete-account-type',
            data: {
                accountTypeId : accountTypeId
            },
            success: function(respond) {
                p_notification(respond.status, respond.msg);
                if(respond.status){
                    $('#accountTypeDeleteModal').modal('hide');                    
                    fetchAll();
                }
            }
        });
    });
    
    //for search cancel
    $('#btnSearchCancel').on('click',function (){
        $('#accountTypeSearch').val('');
        fetchAll();
    });
    
    //for search
    $('#accountTypeSearch').keyup(function() {
        var key = $('#accountTypeSearch').val();
        $.ajax({
            type: 'POST',
            url: BASE_URL + '/account-type-api/search',
            data: {
                searchKey : key
            },
            success: function(respond) {
                $('#account-type-list').html(respond);            
            }
        });
    });
    
    //for bank actions
    $('#account-type-list').on('click','.acc-type-action',function (e){        
        var action = $(this).data('action');
        
        accountTypeId = $(this).closest('tr').data('type-id');
        accountTypeName = $(this).closest('tr').data('type-name');
        entityId = $(this).closest('tr').data('entity-id');
        
        switch(action) {

            case 'edit':
                e.preventDefault();
                accountTypeUpdateView();
                $('#accountType').val(accountTypeName);                
                break;

            case 'delete':
                e.preventDefault();
                $('.accountTypeModalBody').html('<p>Are you sure you want to delete <b>"'+accountTypeName+'"</b>  account type ?</p>');
                break;

            default:
                console.error('Invalid action.');
                break;
        }
    });
    
    //this function for fetach all the accountTypes
    function fetchAll(){
        $.ajax({
            type: 'GET',
            url: BASE_URL + '/account-type-api/get-account-types',
            success: function(respond) {
                $('#account-type-list').html(respond);           
            }
        });
    }
    
    //this function for switch to accountType add view
    function accountTypeAddView(){
        $('.updateTitle').addClass('hidden');
        $('.addTitle').removeClass('hidden');
        $('.updateDiv').addClass('hidden');
        $('.addDiv').removeClass('hidden');
    }
    
    //this function for switch to accountType update view
    function accountTypeUpdateView(){
        $('.addTitle').addClass('hidden');
        $('.updateTitle').removeClass('hidden');
        $('.addDiv').addClass('hidden');
        $('.updateDiv').removeClass('hidden');
    }
    
});

