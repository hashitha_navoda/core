$(document).ready(function(){
    
    var entityId = null;
    var bankId   = null;
    var bankName = null;
    var bankCode = null;
    
    //for register btn
    $('#btnRegister').on('click',function(){
        
        var bankCode = $('#bankCode').val();
        var bankName = $('#bankName').val();
        
        if(bankCode.trim() && bankName.trim()){
            eb.ajax({
                type: 'POST',
                url: BASE_URL + '/bank-api/register',
                data: {
                    bankCode : bankCode,
                    bankName : bankName
                },
                success: function(respond) {
                    p_notification(respond.status, respond.msg);                
                    if(respond.status){
                        $('#bankCode').val('');
                        $('#bankName').val('');
                        fetchAll();                    
                    }                
                }
            });
        } else {
            p_notification(false, eb.getMessage('ERR_BANK_REQUIRED'));
        }
    });
    
    //for rest btn
    $('#btnRest').on('click',function (){
        $('input[type=text]').val('');
    });
    
    //for update btn
    $('#btnUpdate').on('click',function (){
        var bankCode = $('#bankCode').val();
        var bankName = $('#bankName').val();
        
        if(bankCode.trim() && bankName.trim()){
            eb.ajax({
                type: 'POST',
                url: BASE_URL + '/bank-api/update',
                data: {
                    bankId   : bankId,
                    bankCode : bankCode,
                    bankName : bankName,
                    entityId : entityId
                },
                success: function(respond) {
                    p_notification(respond.status, respond.msg);                
                    if(respond.status){
                        fetchAll();
                    }                
                }
            });
        } else {
            p_notification(false, eb.getMessage('ERR_BANK_REQUIRED'));
        }
    });
    
    //for cancel btn
    $('#btnCancel').on('click',function (){
        $('input[type=text]').val('');        
        bankRegisterView();        
    });
    
    //for delete btn
    $('#btnDelete').on('click',function (){
        eb.ajax({
            type: 'POST',
            url: BASE_URL + '/bank-api/delete-bank',
            data: {
                bankId : bankId
            },
            success: function(respond) {
                p_notification(respond.status, respond.msg);
                if(respond.status){
                    $('#bankDeleteModal').modal('hide');
                    fetchAll();
                }
                
            }
        });
    });
    
    //for search cancel btn
    $('#searchCancel').on('click',function (){
        $('#bankSearch').val('');
        fetchAll();
    });
    
    //for search
    $('#bankSearch').keyup(function() {
        var key = $('#bankSearch').val();
        $.ajax({
            type: 'POST',
            url: BASE_URL + '/bank-api/search',
            data: {
                searchKey : key
            },
            success: function(respond) {
                $('#bank-list').html(respond);            
            }
        });
    });
    
    //for bank actions
    $('#bank-list').on('click','.bnk-action',function (e){        
        var action = $(this).data('action');
        bankId   = $(this).closest('tr').data('bank-id');
        entityId = $(this).closest('tr').data('entity-id');
        bankName = $(this).closest('tr').data('bank-name');
        bankCode = $(this).closest('tr').data('bank-code');
        
        switch(action) {
            case 'add-branch':
                //e.preventDefault();
                //alert('add branch for >>>'+bankId);
                break;

            case 'edit':
                e.preventDefault();
                bankUpdateView();
                //set selected bank details
                $('#bankCode').val(bankCode);
                $('#bankName').val(bankName);
                
                break;

            case 'delete':
                e.preventDefault();
                $('.bankModalBody').html('<p>Are you sure you want to delete <b>"'+bankName+'"</b> ?</p>');
                break;

            default:
                console.error('Invalid action.');
                break;
        }
    });
    
    //this function for fetach all the data
    function fetchAll(){
        $.ajax({
            type: 'GET',
            url: BASE_URL + '/bank-api/get-banks',
            success: function(respond) {
                $('#bank-list').html(respond);            
            }
        });
    }
    
    //this function for switch to bank register view
    function bankRegisterView(){
        $('.updateTitle').addClass('hidden');
        $('.addTitle').removeClass('hidden');
        $('.updateDiv').addClass('hidden');
        $('.addDiv').removeClass('hidden');
    }
    
    //this function for switch to bank update view
    function bankUpdateView(){
        $('.addTitle').addClass('hidden');
        $('.updateTitle').removeClass('hidden');
        $('.addDiv').addClass('hidden');
        $('.updateDiv').removeClass('hidden');
    }
    
});