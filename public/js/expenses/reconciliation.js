$(document).ready( function(){
    
    var startDate = null;
    var endDate = null;
    var accountId = null;
    var reconciliationId = null;
    var reconciliationAmount = null;
    var canceledTransactions;
    
    //get all reconciliations
    getReconciliations( null, function (){});
    
    var checkin = $('#start-date').datepicker({
        format: 'yyyy-mm-dd',
    }).on('changeDate', function(ev) {
        if (ev.date.valueOf() < checkout.date.valueOf()) {
            var currentDate = new Date(ev.date);
            var newDate = new Date();
            newDate.setDate(currentDate.getDate() + 1);
            checkout.setValue(newDate);
        }
        startDate = this.value;
        if (startDate && endDate) {
            $('#search-btn').prop('disabled', false);
        }
        checkin.hide();
        $('#end-date')[0].focus();
    }).data('datepicker');

    var checkout = $('#end-date').datepicker({
        format: 'yyyy-mm-dd',
        onRender: function(date) {
            return date.valueOf() <= checkin.date.valueOf() ? 'disabled' : '';
        }
    }).on('changeDate', function(ev) {
         endDate = this.value;
        if (startDate && endDate) {
            $('#search-btn').prop('disabled', false);
        }
        checkout.hide();
    }).data('datepicker');
    
    //for serach btn
    $(document).on('click','#search-btn',function (){
        getReconciliationsByDateRange(accountId,startDate,endDate);
    });
    
    //load account list
    $(document).on('change','#bank-select',function (){
        var bankId = $(this).val();
        if(bankId){
            getBankAccountListByBankId(bankId);
        } else {
            $('#account-select').empty();
            $('#account-select').append($("<option>", { value: '', html: '----- Select Account -----' }));
        }
        $('.datepicker').val('');
        $('.datepicker,#search-btn').prop('disabled',true);
    });
    
    //load account list
    $(document).on('change','#account-select',function (){
        accountId = $(this).val();
        $('.datepicker').val('');
        if(accountId){
            getReconciliations(accountId,function (){
                $('.datepicker').prop('disabled',false);
            });
        } else {
            $('.datepicker').prop('disabled',true);
            $('#reconciliation-list').html('');
        }
        $('.datepicker').val('');
        $('#search-btn').prop('disabled',true);
    });
    
    //for reconciliation cancel modal
    $('#reconciliation-list').on('click','.reconciliation-cancel', function (e) {
        e.preventDefault();
        reconciliationId = $(this).closest('tr').data('reconciliation-id');
        reconciliationAmount = $(this).closest('tr').data('reconciliation-amount');
        $('.reconciliationDeleteModalBody').html('<p>Are you sure you want to cancel this reconciliation ?</p>');
        $('#reconciliationDeleteModal').modal('show');
    });
    
    //for reconciliation cancel confimation
    $('#btnConfirm').on('click', function () {
        $.ajax({
            type: 'POST',
            url: BASE_URL + '/reconciliation-api/cancel',
            data: {
                reconciliationId: reconciliationId
            },
            success: function (respond) {
                p_notification(respond.status, respond.msg);
                if (respond.status) {
                    $('#reconciliationDeleteModal').modal('hide');
                    getReconciliations(accountId,function (){
                        $('.datepicker').prop('disabled',false);
                    });
                }
            }
        });
    });
    
    //for reconciliation update modal
    $('#reconciliation-list').on('click','.reconciliation-edit', function (e) {
        e.preventDefault();
        reconciliationId = $(this).closest('tr').data('reconciliation-id');
        reconciliationAmount = $(this).closest('tr').data('reconciliation-amount');
        
        getReconciliationTransactions( reconciliationId, '0', function (){
            $('#updateReconciliationBtn,.transaction-cls').removeClass('hidden');
            $('#reconciliationUpdateModal').modal('show');
            $('#updateReconciliationBtn').prop('disabled',true);
            $('#reconciliation-net-amount').text(reconciliationAmount)
        });
    });
    
    //for reconciliation view 
    $('#reconciliation-list').on('click','.reconciliation-view', function (e) {
        e.preventDefault();
        reconciliationId = $(this).closest('tr').data('reconciliation-id');
        reconciliationAmount = $(this).closest('tr').data('reconciliation-amount');
        console.log(reconciliationAmount);
        getReconciliationTransactions( reconciliationId, null, function (){
            $('#updateReconciliationBtn,.transaction-cls').addClass('hidden');
            calculateReconciliation();
            $('#reconciliationUpdateModal').modal('show');
        });
    });
    
    //for cheque deposit modal actions
    $('#reconciliationUpdateModal').on('click','.transaction-cls', function (e) {
        e.preventDefault();
        var action = $(this).data('action');

        switch (action) {
            case 'transaction-remove':
                $(this).closest('tr').addClass('text-muted');//for disable row
                $(this).prop('disabled', true);//disable delete btn
                $(this).closest('tr').find('.fa-undo').prop('disabled',false);//enable undo btn
                break;

            case 'transaction-undo':
                $(this).closest('tr').removeClass('text-muted');//for enable row
                $(this).prop('disabled', true);//disable undo btn
                $(this).closest('tr').find('.fa-trash-o').prop('disabled',false);//enable delete btn
                break;
                
            default :
                console.log('invalid option');
        }
        calculateReconciliation();

    });
    
    //for update deposit btn
    $('#reconciliationUpdateModal').on('click','#updateReconciliationBtn',function(){
        $.ajax({
            type: 'POST',
            url: BASE_URL + '/reconciliation-api/update',
            data: {
                canceledTransactions : canceledTransactions,
                reconciliationId : reconciliationId,
                reconciliationAmount  : reconciliationAmount
            },
            success: function(respond) {
                if(respond.status){
                    $('#reconciliationUpdateModal').modal('hide');
                    getReconciliations(accountId,function (){
                        $('.datepicker').prop('disabled',false);
                    });
                }
            }
        });
    });
    
    //get bank account list
    function getBankAccountListByBankId(bankId){
        $.ajax({
            type: 'POST',
            url: BASE_URL + '/account-api/bank-account-list-for-dropdown',
            data: {bankId : bankId},
            success: function(respond) {
                console.log(respond.data.list);
                //empty options
                $('#account-select').empty();
                $('#account-select').append($("<option>", { value: '', html: '----- Select Account -----' }));
                $(respond.data.list).each(function(i, v){ 
                    $('#account-select').append($("<option>", { value: v.value, html: v.text }));
                });
            }
        });
    }
    
    function getReconciliations(accountId,callback) {
        $.ajax({
            type: 'POST',
            url: BASE_URL + '/reconciliation-api/get-reconciliations',
            data: {
                accountId: accountId
            },
            success: function (respond) {
                $('#reconciliation-list').html(respond);
                callback();
            }
        });
    }
    
    function getReconciliationsByDateRange(accountId,startDate,endDate) {
        if(!startDate){
            p_notification( false,  'please fill start date');
        } else if(!endDate){
            p_notification( false,  'please fill end date');
        } else if(startDate > endDate){
            p_notification( false,  'invalid date range');
        } else{
            $.ajax({
                type: 'POST',
                url: BASE_URL + '/reconciliation-api/get-reconciliations',
                data: {
                    accountId: accountId,
                    startDate: startDate,
                    endDate: endDate
                },
                success: function (respond) {
                    $('#reconciliation-list').html(respond);
                }
            });
        }
    }
    
    function getReconciliationTransactions(reconciliationId,filter,callback) {
        $.ajax({
            type: 'POST',
            url: BASE_URL + '/reconciliation-api/get-transactions',
            data: {
                reconciliationId: reconciliationId,
                filter : filter
            },
            success: function (respond) {
                $('#transaction-list').html(respond);
                callback();
            }
        });
    }
    
    function calculateReconciliation(){
        reconciliationAmount = 0;
        canceledTransactions = [];
        
        $('.transaction-tr').each(function() {
            if(!$(this).hasClass('text-muted')){
                reconciliationAmount = reconciliationAmount + parseFloat($( this ).data('transaction-amount'));
            } else {
                var id = $( this ).data('transaction-id');
                canceledTransactions.push(id);
            }
        });
        console.log(JSON.stringify(canceledTransactions));
        $('#reconciliation-net-amount').text(reconciliationAmount.toFixed(2));
        
        if(canceledTransactions.length > 0){
            $('#updateReconciliationBtn').prop('disabled',false);
        } else {
            $('#updateReconciliationBtn').prop('disabled',true);
        }
    }
    
});