$(document).ready(function() {
    
    var refreshIntervalId;
    
    $("#generatedReports").hide();
	
    getQueueReports();
    $("#reportDelete").click(function (event) {
        var url = BASE_URL + '/api/app/delete-viewed-reports';
        var deleteID = $("#reportDelete").attr("data-id");
        eb.ajax({
            type: 'POST',
            url: url,
            dataType: 'json',
            data: {deleteID: deleteID},
            success: function (respond) {
                if (respond.data) {
                    $("#generatedReports").hide();
                    getQueueReports();
                }
            }
        });
    });
    
//    $("#generatePdf").click(function(event) {
//        refreshIntervalId = setInterval(function(){
//            getQueueReports();
//        }, 10000);
//    });
//    
//    $("#csvReport").click(function(event) {
//        refreshIntervalId = setInterval(function(){
//            getQueueReports();
//        }, 10000);
//    });
    
    function getQueueReports() {
        var pathName = window.location.pathname.split('/');
        var firstParam = pathName[1];
        var reportQueuecategory = pathName[2];
        if (firstParam == "reports") {
            var url = BASE_URL + '/api/app/view-queued-reports';
            $.ajax({
                type: 'POST',
                url: url,
                dataType: 'json',
                data: {reportQueuecategory: reportQueuecategory},
                success: function(respond) {
                    if (respond.data.length > 0) {
                        $("#generatedReports").show();
                        clearInterval(refreshIntervalId);
                        for (var i = 0; i < respond.data.length; i++) {
                            var reportType = respond.data[i]['reportQueueUrl'].split('.');
                            $("#reportName").text(respond.data[i]['reportQueueReportName']+" "+ reportType[1].toUpperCase()+" Report");
                            $("#reportUrl").attr('href',respond.data[i]['reportQueueUrl']);
                            $("#reportCreated").text("Created At: "+respond.data[i]['reportQueueCreatedDateTime']);
                            $("#reportDelete").attr('data-id',respond.data[i]['reportQueueID']);
                        }
                    }
                }
            });
        }
    }
});