(function($) {

    $.fn.addUomPrice = function(uoms, selectedUom) {
        selectedUom = typeof selectedUom !== 'undefined' ? selectedUom : false;
        $(this).each(function() {

            var $price = $(this);
            var name = $price.attr('name') + new Date().getUTCMilliseconds();

            // remove existing dropdown
            $price.siblings('.uom-price-select').remove();

            // create new dropdown
            var $dropdown = $('<div class="input-group-btn uom-price-select"></div>');
            var $btn = $('<button data-toggle="dropdown" class="btn btn-default dropdown-toggle uom_price_drop_down" type="button"><span class="selected"></span><em>&nbsp;</em><span class="fa fa-caret-down"></span></button>');
            var $addContainer = $('<ul class="text-left dropdown-menu pull-right"></ul>');
            $btn.appendTo($dropdown);
            $btn.after($addContainer);
            $price.after($dropdown);

            // show original price input
            $price.hide();

            // create new input for uom price
            var $uomPrice = $('<input>').addClass('form-control text-right uomPrice reduce_le_ri_padding');
            $price.before($uomPrice);

            if (uoms !== undefined) {
                $btn.find('li').remove();
                $.each(uoms, function(i) {
                    var $option = $('<li class="col-lg-12"></li>');
                    var $lidiv = $('<div class="radio_button"></div>');
                    var $radiobutton = $('<input type="radio" value="1">').attr('name', name + '_uomtype').attr('id', name + '_uomtype' + i).css('margin-right', '4px');
                    var $label = $('<label></label>').attr('for', name + '_uomtype' + i);
                    $label.text(this.uN + ' (' + this.uA + ')');
                    $option.data('id', this.uomID)
                            .data('state', this.us)
                            .data('abbr', this.uA)
                            .data('conversion', this.uC);
                    $option.appendTo($addContainer);
                    $lidiv.appendTo($option);
                    $radiobutton.appendTo($lidiv);
                    $radiobutton.after($label);


                    if (selectedUom) {
                        if (selectedUom == this.uomID) {
                            $radiobutton.attr('checked', true);
                            $btn.find('.selected').text(this.uA)
                                    .data('uc', this.uC)
                                    .data('uomID', this.uomID);

                            var basePrice = $price.val();
                            var conversionRate = this.uC;
                            var price = basePrice * conversionRate;

                            $uomPrice.val(price);
                        }

                    } else {
                        if (this.pUDisplay == 1) {
                            $radiobutton.attr('checked', true);
                            $btn.find('.selected').text(this.uA)
                                    .data('uc', this.uC)
                                    .data('uomID', this.uomID);

                            var basePrice = $price.val();
                            var conversionRate = this.uC;
                            var price = basePrice * conversionRate;

                            $uomPrice.val(price);
                        }
                    }
                });
            }



            $dropdown.find('input[type="radio"]').on('change', function() {
                var basePrice = $price.val();
                var conversionRate = $(this).parents('li').data('conversion');
                $(this).parents('ul').siblings('button').find('.selected').text($(this).parents('li').data('abbr'))
                        .data('uc', conversionRate)
                        .data('uomID', $(this).parents('li').data('id'));

                var price = basePrice * conversionRate;

                $uomPrice.val(price);
            });

            $uomPrice.change(function() {
                var price = $uomPrice.val();
                var conversionRate = $dropdown.find('.selected').data('uc');
                if (!($.isNumeric(price)) || price < 0) {
                    $uomPrice.val('');
                    price = 0.00;
                }
                var basePrice = price / conversionRate;
                $price.val(basePrice).trigger('change', 'keyup', 'focusout');
            });

            $price.change(function() {
                var basePrice = $price.val();
                var conversionRate = $dropdown.find('.selected').data('uc');
                var price = basePrice * conversionRate;

                price = parseFloat(price).toFixed(getDecimalPlaces(price));

                $uomPrice.val(price);
                if ($price.prop('readonly') == true || $price.prop('disabled') == true) {
                    $uomPrice.attr('disabled', true);
                } else {
                    $uomPrice.attr('disabled', false);
                    $uomPrice.focus().select();
                }
            });

            $price.trigger('change');

        });

        return $(this);
    };

}(jQuery));
