(function($) {

    // TODO - write function to get current UOM ID. Check where this is used and
    // see if it is necessary

    $.fn.addUom = function(uoms, selectedUom) {
        isPriceUom = typeof isPriceUom !== 'undefined' ? isPriceUom : false;
        selectedUom = typeof selectedUom !== 'undefined' ? selectedUom : false;

        console.log(selectedUom);

        $(this).each(function() {

            var $qty = $(this);
            var name = $qty.attr('name') + new Date().getUTCMilliseconds();

            // remove existing dropdown
            $qty.siblings('.uom-select').remove();

            // create new dropdown
            var $dropdown = $('<div class="input-group-btn uom-select"></div>');
            var $btn = $('<button data-toggle="dropdown" class="btn btn-default dropdown-toggle" type="button"><span class="selected"></span><em>&nbsp;</em><span class="fa fa-caret-down"></span></button>');
            var $addContainer = $('<ul class="text-left dropdown-menu pull-right"></ul>');
            $btn.appendTo($dropdown);
            $btn.after($addContainer);
            $qty.after($dropdown);

            // show original qty input
            $qty.hide();

            // create new input for uom qty
            var $uomQty = $('<input>').addClass('form-control text-right uomqty reduce_le_ri_padding');
            $qty.before($uomQty);

            if (uoms !== undefined) {
                $btn.find('li').remove();
                $.each(uoms, function(i) {
                    var $option = $('<li class="col-lg-12"></li>');
                    var $lidiv = $('<div class="radio_button"></div>');
                    var $radiobutton = $('<input type="radio" value="1">').attr('name', name + '_uomtype').attr('id', name + '_uomtype' + i).css('margin-right', '4px');
                    var $label = $('<label></label>').attr('for', name + '_uomtype' + i);
                    $label.text(this.uN + ' (' + this.uA + ')');
                    $option.data('id', this.uomID)
                            .data('state', this.us)
                            .data('abbr', this.uA)
                            .data('decimalPlaces', this.uDP)
                            .data('conversion', this.uC);
                    $option.appendTo($addContainer);
                    $lidiv.appendTo($option);
                    $radiobutton.appendTo($lidiv);
                    $radiobutton.after($label);

                    if (selectedUom) {
                        if (selectedUom == this.uomID) {
                            $radiobutton.attr('checked', true);
                            $btn.find('.selected').text(this.uA)
                                    .data('uc', this.uC)
                                    .data('decimalPlaces', this.uDP)
                                    .data('uomID', this.uomID);

                            var base = $qty.val();
                            var conversionRate = this.uC;
                            var decimalPlaces = this.uDP;
                            var Qty = base / conversionRate;
        
                            if (decimalPlaces == 0) {
                                Qty = Math.floor(parseFloat(Qty));
                            } else {
                                Qty = parseFloat(Qty).toFixed(decimalPlaces);
                            }
                            $uomQty.val(Qty);
                        }
                    } else {
                        if (this.pUDisplay == 1) {
                            $radiobutton.attr('checked', true);
                            $btn.find('.selected').text(this.uA)
                                    .data('uc', this.uC)
                                    .data('decimalPlaces', this.uDP)
                                    .data('uomID', this.uomID);

                            var base = $qty.val();
                            var conversionRate = this.uC;
                            var decimalPlaces = this.uDP;
                            var Qty = base / conversionRate;
        
                            if (decimalPlaces == 0) {
                                Qty = Math.floor(parseFloat(Qty));
                            } else {
                                Qty = parseFloat(Qty).toFixed(decimalPlaces);
                            }
                            $uomQty.val(Qty);
                        }
                    }

                });
            }

            $dropdown.find('input[type="radio"]').on('change', function() {
                var base = $qty.val();
                var conversionRate = $(this).parents('li').data('conversion');
                var decimalPlaces = $(this).parents('li').data('decimalPlaces');
                $(this).parents('ul').siblings('button').find('.selected').text($(this).parents('li').data('abbr'))
                        .data('uc', conversionRate)
                        .data('decimalPlaces', decimalPlaces)
                        .data('uomID', $(this).parents('li').data('id'));

                var qty = base / conversionRate;

                if (decimalPlaces == 0) {
                    qty = Math.floor(parseFloat(qty));
                } else {
                    qty = parseFloat(qty).toFixed(decimalPlaces);
                }
                $uomQty.val(qty);
            });

            $uomQty.change(function() {
                var qtyVal = $uomQty.val();
                var conversionRate = $dropdown.find('.selected').data('uc');
                var decimalPlace = $dropdown.find('.selected').data('decimalPlaces');
                if (!($.isNumeric(qtyVal)) || qtyVal < 0) {
                    $uomQty.val('');
                    qtyVal = 0.00;
                }
                var baseQty = qtyVal * conversionRate;
                if (decimalPlace == 0) {
                    baseQty = Math.floor(parseFloat(baseQty));
                } else {
                    baseQty = parseFloat(baseQty).toFixed(decimalPlace);
                }
                $qty.val(baseQty).trigger('change', 'keyup', 'focusout');
            });

            $qty.change(function() {
                var base = $qty.val();
                var conversionRate = $dropdown.find('.selected').data('uc');
                var decimalPlaces = $dropdown.find('.selected').data('decimalPlaces');
                var fromQty = base / conversionRate;
    
                if (decimalPlaces == 0) {
                    fromQty = Math.floor(parseFloat(fromQty));
                } else {
                    fromQty = parseFloat(fromQty).toFixed(decimalPlaces);
                }
                $uomQty.val(fromQty);
                if ($qty.prop('readonly') == true || $qty.prop('disabled') == true) {
                    $uomQty.attr('disabled', true);
                } else {
                    $uomQty.attr('disabled', false);
                    $uomQty.focus().select();
                }
            });

            $qty.trigger('change');

        });

        return $(this);
    };

}(jQuery));