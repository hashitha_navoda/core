$(document).ready(function () {
    $('input[name="locationTax"]').bootstrapSwitch();
    var defaultUrl = BASE_URL + '/settings/location/';
//    $('#location-header').on('click', '#create-location', function() {
//        $("#location-header").hide();
//        getViewAndLoad('/settings/location/getLocationAddView', 'location-list');
////        getViewAndLoad('/settings/location/create', 'location-list');
//    });

    $('#location-list').on('click', ' #moreDetails', function () {
        $(this).hide();
        $('#locationMore').removeClass('hidden');
    });

    $('#wizardLocationList').on('click', ' #moreDetails', function () {
        $(this).hide();
        $('#locationMore').removeClass('hidden');
    });

    $('form#add-locaion').on('submit', function (e) {
        e.preventDefault();
        var locationID = $('#locationID').val();
        var locationCode = $('#locationCode').val();
        var locationName = $('#locationName').val();
        var locationEmail = $('#locationEmail').val();
        var locationTelephone = $('#locationTelephone').val();
        var locationFax = $('#locationFax').val();
        var locationTax = $('#locationTax:checked').length;
        var locationAddressLine1 = $('#locationAddressLine1').val();
        var locationAddressLine2 = $('#locationAddressLine2').val();
        var locationAddressLine3 = $('#locationAddressLine3').val();
        var locationAddressLine4 = $('#locationAddressLine4').val();
        var locationStatus = $('#locationStatus').val();
        var param = {
            locationID: locationID,
            locationCode: locationCode,
            locationName: locationName,
            locationEmail: locationEmail,
            locationTelephone: locationTelephone,
            locationFax: locationFax,
            locationTax: locationTax,
            locationAddressLine1: locationAddressLine1,
            locationAddressLine2: locationAddressLine2,
            locationAddressLine3: locationAddressLine3,
            locationAddressLine4: locationAddressLine4,
            locationStatus: locationStatus
        };

        if (validate_location_form() && !check_location(locationID, locationCode)) {
            if (locationID === "") {

                eb.ajax({
                    type: 'POST',
                    url: BASE_URL + '/settings/location/addLocation',
                    data: param,
                    success: function (result) {
                        if (result.status == true) {
                            window.location = defaultUrl;
                        } else {
                            p_notification(result.status, result.msg);
                        }
                    },
                });

            } else {

                eb.ajax({
                    type: 'POST',
                    url: BASE_URL + '/settings/location/updateLocation',
                    data: param,
                    success: function (result) {
                        if (result.status == true) {
                            window.location.assign(defaultUrl);
                        } else {
                            p_notification(result.status, result.msg);
                        }
                    },
                });
            }
            $("#location-header").show();
        }
    });

    $('#location-header').on('click', '#list-locations', function () {
        getViewAndLoad('/settings/location/viewLocationList', 'location-list', {}, function () {
            $("#list-locations").html('Create Location');
            $("#list-locations").attr('id', 'create-location');
        });
    });

    $('#location-list').on('click', '#list-locations', function () {
//        $("#location-header").show();
//       getViewAndLoad('/settings/location/viewLocationList', 'location-list');

        window.location.assign(defaultUrl);
    });

    $('#location-list').on('click', '#reset-location-button', function () {
        $("#add-locaion").get(0).reset();
    });

    $('#locationSearchForm').submit(function (e) {
        e.preventDefault();
        var param = {searchKey: $('#searchLocation').val()};
        eb.ajax({
            type: 'POST',
            url: BASE_URL + '/settings/location/getLocationsFromSearch',
            data: param,
            success: function (respond) {
                $('#location-list').html(respond.html);
            }
        });
    });

    $('button.reset', '#locationSearchForm').on('click', function () {
        $('#searchLocation').val('');
        $('#locationSearchForm').submit();
    });

    function validate_location_form() {
        var locationCode = $('#locationCode').val();
        var locationName = $('#locationName').val();
        var locationEmail = $('#locationEmail').val();
        var locationTelephone = $('#locationTelephone').val();
        var locationFax = $('#locationFax').val();

        var atpos = locationEmail.indexOf("@");
        var dotpos = locationEmail.lastIndexOf(".");
        var phoneNO = /^[0-9\-\(\) \+]+$/;

        if (locationCode === null || locationCode === "") {
            p_notification(false, eb.getMessage('ERR_COMMON_LOCACODE'));
        } else if (locationName === null || locationName === "") {
            p_notification(false, eb.getMessage('ERR_COMMON_LOCANAME'));
        } else if ((atpos < 1 || dotpos < atpos + 2 || dotpos + 2 >= locationEmail.length) && (locationEmail !== "")) {
            p_notification(false, eb.getMessage('ERR_SALEP_EMAIL_VALID'));
        } else {
            return true;
        }
    }

    function check_location(locationID, locationCode) {
        var res = ezBiz_post('/settings/location/checkLocation', {locationID: locationID, locationCode: locationCode});

        if (res.status) {
            p_notification('error', res.msg);
            return true;
        } else {
            return false;
        }

    }

    //mobile view large search responsive issue
    $(window).bind('ready resize', function () {
        if ($(window).width() < 480) {
            $('#searchLocation').css('margin-bottom', '15px');
            $('#searchLocation').parent().removeClass('input-group');
            $('#searchLocation').next().removeClass('input-group-btn');
        } else {
            $('#searchLocation').parent().addClass('input-group');
            $('#searchLocation').next().addClass('input-group-btn');
            $('#searchLocation').css('margin-bottom', '0');
        }
    });

});


function locationAction(currentID) {
    var res = currentID.split("_");
    var param = {
        locationID: res[0]
    };
//    if (res[1] === "view") {
//        getViewAndLoad('/settings/location/getLocationView', 'location-list', param, function() {
//            $("#location-header").hide();
//        });
//
//    } else
    if (res[1] === "update") {

        getViewAndLoad('/settings/location/getLocationUpdateForm', 'location-list', param);
        $('.location_update').text(update_location);
        $('#reset-location-button').hide();
        $("#location-header").hide();
        $("#add-location-button").attr('value', update_location);

    } else if (res[1] === "delete") {

        bootbox.confirm("Are you sure you want to delete this location?", function (result) {
            if (result === true) {
                getViewAndLoad('/settings/location/deleteLocation', 'location-list', param, function (res) {
                    p_notification(res.status, res.msg);
                });
            }
        });
    }
}
