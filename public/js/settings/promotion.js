function promotionProduct(productID, minQty, maxQty, discountType, discountAmount) {
    this.productID = productID;
    this.minQty = minQty;
    this.maxQty = maxQty;
    this.discountType = discountType;
    this.amount = discountAmount;
}

function promotionCombination(combinationName, majorCondition, discountType, discountAmount, ruleSet) {
    this.combinationName = combinationName;
    this.majorCondition = majorCondition;
    this.discountType = discountType;
    this.amount = discountAmount;
    this.ruleSet = ruleSet;
}

function combinationRule(productID, rule, quantity, productName) {
    this.productID = productID;
    this.ruleType = rule;
    this.quantity = quantity;
    this.productName = productName;
}

function combinationAttrRule(attributeType, attributeTypeName, attributeValID, attributeValName, ruleType, quantity) {
    this.attributeType = attributeType;
    this.attributeTypeName = attributeTypeName;
    this.attributeValID = attributeValID;
    this.attributeValName = attributeValName;
    this.ruleType = ruleType;
    this.quantity = quantity;
}

var combinationRules = {};
var addedCombinationRules = {};


$(document).ready(function() {
    var selectedProduct = '';
    var selectedProductID = '';
    var promoCustomersList = [];
    var promoCustomersEventList = [];
    var selectedPromoEmailCustomers = [];
    var selectedPromoSmsCustomersName = [];
    var selectedPromoSmsCustomers = [];
    var selectedPromoEmailLocations = [];
    var selectedPromoEmailProducts = [];
    var selectedPromoEmailCustomerEvents = [];
    var promotionProducts = {};
    var promotionCombinations = {};
    var thisIsPromotionEdit = false;
    var alreadySetPromotionProducts = false;
    var editPromotionID = '';
    var $productTable = $('#promoAddNewItems');
    var $combinationTable = $('#promoAddNewCombination');
    var $ruleTable = $('#addRuleModal #ruleAddBody');
    var $attrRuleTable = $('#addAttrRuleModal #attrRuleAddBody');
    var $tempRuleTable = $('#addRuleModal #tempRuleAddBody');
    var $tempAttrRuleTable = $('#addAttrRuleModal #tempAttrRuleAddBody');
    var $addNewCombinationRow = $('tr.promo-comb-add-row', $combinationTable);
    var $addNewRuleRow = $('tr.promo-comb-rule-add-row', $ruleTable);
    var $addNewAttrRuleRow = $('tr.promo-comb-attr-rule-add-row', $attrRuleTable);
    var $addNewProductRow = $('tr.promo-add-row', $productTable);
    var fromDateFormat = $('#promotionFrom').data('date-format');
    var promotionFromDate = $('#promotionFrom').datepicker({
        format: fromDateFormat,
    }).on('changeDate', function(ev) {
        promotionFromDate.hide();
    }).data('datepicker');

    var toDateFormat = $('#promotionTo').data('date-format');
    var promotionToDate = $('#promotionTo').datepicker({
        format: toDateFormat,
    }).on('changeDate', function(ev) {
        promotionToDate.hide();
    }).data('datepicker');


    var locationID = $('#locationID').val();
    var documentType = 'promotion';
    loadDropDownFromDatabase('/productAPI/search-location-products-for-dropdown', locationID, 0, '#addRuleModal #itemCode', '', '', documentType);
    $('#itemCode', $addNewRuleRow).selectpicker('refresh');

    $('#createPromotionForm').submit(function(e) {
        e.preventDefault();
        var chkFromDate = new Date($('#promotionFrom', '#createPromotionForm').val());
        var chkToDate = new Date($('#promotionTo', '#createPromotionForm').val());
        if ($('#promotionName', '#createPromotionForm').val() == '') {
            p_notification(false, "Plese enter a promotion name");
        } else if (chkFromDate >= chkToDate) {
            p_notification(false, "Promotion ending date should be greater than starting date");
        } else {
            var promoType = $('#promotionType', '#createPromotionForm').val();
            if (promoType == 1) {
                if ($('#createPromotionForm').hasClass('edit')) {
                    thisIsPromotionEdit = true;
                    editPromotionID = $('#createPromotionForm').data('id');
                    eb.ajax({
                        type: 'POST',
                        url: BASE_URL + '/api/promotions/getPromotion',
                        data: {promotionID: editPromotionID},
                        success: function(respond) {
                            if (respond.status == true) {
                                setPromotionCombinations(respond.data.combinations, promoType);
                                $('#promoAddMenu').addClass('hidden');
                                $('#createCombinationMenu').removeClass('hidden');
                                if (respond.data.isUsed){
                                    $('#promotionCombSave').addClass('hidden');
                                }
                            }
                        }
                    });

                } else {
                    $('#promoAddMenu').addClass('hidden');
                    $('#createCombinationMenu').removeClass('hidden');
                }
            } else if (promoType == 3) {
                if ($('#createPromotionForm').hasClass('edit')) {
                    thisIsPromotionEdit = true;
                    editPromotionID = $('#createPromotionForm').data('id');
                    eb.ajax({
                        type: 'POST',
                        url: BASE_URL + '/api/promotions/getPromotion',
                        data: {promotionID: editPromotionID},
                        success: function(respond) {
                            if (respond.status == true) {
                               setPromotionCombinations(respond.data.combinations, promoType);
                                $('#promoAddMenu').addClass('hidden');
                                $('#createCombinationMenu').removeClass('hidden');
                                if (respond.data.isUsed){
                                    $('#promotionCombSave').addClass('hidden');
                                }
                            }
                        }
                    });

                } else {
                    $('#promoAddMenu').addClass('hidden');
                    $('#createCombinationMenu').removeClass('hidden');
                }
            } else {
                if ($('#createPromotionForm').hasClass('edit')) {
                    thisIsPromotionEdit = true;
                    editPromotionID = $('#createPromotionForm').data('id');
                    eb.ajax({
                        type: 'POST',
                        url: BASE_URL + '/api/promotions/getPromotion',
                        data: {promotionID: editPromotionID},
                        success: function(respond) {
                            if (respond.status == true) {
                                setInvoicePromotionData(respond.data);
                                $('#promoAddMenu').addClass('hidden');
                                $('#promoValueBasedMenu').removeClass('hidden');
                                if (respond.data.isUsed){
                                    $('#promotionValueSave').addClass('hidden');
                                }

                            }
                        }
                    });
                } else {
                    $('#promoAddMenu').addClass('hidden');
                    $('#promoValueBasedMenu').removeClass('hidden');
                }
            }

        }
    });

    $('#promoItemBasedMenu').on('click', '#promotionItemBack', function() {
        $('#promoItemBasedMenu').addClass('hidden');
        $('#promoAddMenu').removeClass('hidden');
    }); 

    $('#createCombinationMenu').on('click', '#showRuleAddModal , #showRuleModal , #showRuleEditModal', function() {
        $('#addRuleModal #tempRuleAddBody .addedRule').remove();
        $('#addAttrRuleModal #tempAttrRuleAddBody .addedAttrRule').remove();
        var $thisRow = $(this).parents('tr');
        var promoType = $('#promotionType', '#createPromotionForm').val();
        if ($thisRow.hasClass('cloned-promo-comb-add-row')) {

            if (promoType == 1) {
                var id = $thisRow.attr('id');
                $.each(promotionCombinations[id]['ruleSet'], function(index, value) {
                    var newTrID = index;
                    var clonedRow = $($('#addRuleModal #tempRuleAddBody #rulePreSample').clone()).attr('id', newTrID).addClass('addedRule');
                    $("input[name='productName']", clonedRow).val(value.productName);
                    $("input[name='ruleType']", clonedRow).val(value.ruleType);
                    $("input[name='quantity']", clonedRow).val(value.quantity);
                    clonedRow.find("input[name='quantity']").prop('disabled', true);
                    clonedRow.find("input[name='ruleType']").prop('disabled', true);
                    clonedRow.find("input[name='productName']").prop('disabled', true);
                    clonedRow.removeClass('hidden');
                    clonedRow.insertBefore('#rulePreSample');
                });

                if ($(this).attr('id') == 'showRuleModal') {
                    $("#addRuleModal #ruleAddBody").addClass('hidden');
                    $("#addRuleModal #tempRuleAddBody .delete-rule").addClass('hidden');
                    $('#addRuleModal .modal-footer .comb-rule-save').addClass('hidden');
                    $('#addRuleModal .modal-footer .comb-rule-update').addClass('hidden');

                } else {
                    $("#addRuleModal #tempRuleAddBody .delete-rule").removeClass('hidden');
                    $("#addRuleModal #ruleAddBody").removeClass('hidden');
                    $('#addRuleModal .modal-footer .comb-rule-update').removeClass('hidden');
                    $('#addRuleModal .modal-footer .comb-rule-save').addClass('hidden');

                    combinationRules = promotionCombinations[id]['ruleSet'];
                    addedCombinationRules = promotionCombinations[id]['ruleSet'];
                }
                $('#addRuleModal').modal('show');
            } else if (promoType == 3) {

                var id = $thisRow.attr('id');
                $.each(promotionCombinations[id]['ruleSet'], function(index, value) {
                    var newTrID = index;
                    var val = (value['quantity'] == null) ? value['attributeValName'] :  value['quantity'];
                    var clonedRow = $($('#addAttrRuleModal #tempAttrRuleAddBody #ruleAttrPreSample').clone()).attr('id', newTrID).addClass('addedAttrRule');
                    $("input[name='attributeName']", clonedRow).val(value['attributeTypeName']);
                    $("input[name='ruleType']", clonedRow).val(value['ruleType']);
                    $("input[name='value']", clonedRow).val(val);
                    clonedRow.find("input[name='value']").prop('disabled', true);
                    clonedRow.find("input[name='ruleType']").prop('disabled', true);
                    clonedRow.find("input[name='attributeName']").prop('disabled', true);
                    clonedRow.removeClass('hidden');
                    clonedRow.insertBefore('#ruleAttrPreSample');
                    
                });

                if ($(this).attr('id') == 'showRuleModal') {
                    $("#addAttrRuleModal #attrRuleAddBody").addClass('hidden');
                    $("#addAttrRuleModal #tempAttrRuleAddBody .delete-rule").addClass('hidden');
                    $('#addAttrRuleModal .modal-footer .comb-attr-rule-save').addClass('hidden');
                    $('#addAttrRuleModal .modal-footer .comb-attr-rule-update').addClass('hidden');

                } else {
                    $("#addAttrRuleModal #tempAttrRuleAddBody .delete-rule").removeClass('hidden');
                    $("#addAttrRuleModal #attrRuleAddBody").removeClass('hidden');
                    $('#addAttrRuleModal .modal-footer .comb-attr-rule-update').removeClass('hidden');
                    $('#addAttrRuleModal .modal-footer .comb-attr-rule-save').addClass('hidden');

                    combinationRules = promotionCombinations[id]['ruleSet'];
                    addedCombinationRules = promotionCombinations[id]['ruleSet'];
                }
                $('#addAttrRuleModal').modal('show');
            }

        } else {

            if (promoType == 1) {
                $("#addRuleModal #ruleAddBody").removeClass('hidden');
                $("#addRuleModal #tempRuleAddBody .delete-rule").removeClass('hidden');
                $('#addRuleModal .modal-footer .comb-rule-save').removeClass('hidden');
                $('#addRuleModal .modal-footer .comb-rule-update').addClass('hidden');
                if (Object.keys(addedCombinationRules).length > 0) {
                    // employees = taskEmployees[getCurrentTaskData($thisRow)['taskIncID']];
                    $.each(addedCombinationRules, function(index, value) {
                        var newTrID = index;
                        var clonedRow = $($('#addRuleModal #tempRuleAddBody #rulePreSample').clone()).attr('id', newTrID).addClass('addedRule');
                        $("input[name='productName']", clonedRow).val(value.productName);
                        $("input[name='ruleType']", clonedRow).val(value.ruleType);
                        $("input[name='quantity']", clonedRow).val(value.quantity);
                        clonedRow.find("input[name='quantity']").prop('disabled', true);
                        clonedRow.find("input[name='ruleType']").prop('disabled', true);
                        clonedRow.find("input[name='productName']").prop('disabled', true);
                        clonedRow.removeClass('hidden');
                        clonedRow.insertBefore('#rulePreSample');
                        resetAddCombinationRuleRow();

                    });

                }
                $('#addRuleModal').modal('show');
            } else if (promoType == 3) {
                $("#addAttrRuleModal #attrRuleAddBody").removeClass('hidden');
                $("#addAttrRuleModal #tempAttrRuleAddBody .delete-rule").removeClass('hidden');
                $('#addAttrRuleModal .modal-footer .comb-attr-rule-save').removeClass('hidden');
                $('#addAttrRuleModal .modal-footer .comb-attr-rule-update').addClass('hidden');
                if (Object.keys(addedCombinationRules).length > 0) {
                    // employees = taskEmployees[getCurrentTaskData($thisRow)['taskIncID']];
                    $.each(addedCombinationRules, function(index, value) {
                        var newTrID = index;

                        var val = (value['quantity'] == null) ? value['attributeValName'] :  value['quantity'];
                        var clonedRow = $($('#addAttrRuleModal #tempAttrRuleAddBody #ruleAttrPreSample').clone()).attr('id', newTrID).addClass('addedAttrRule');
                        $("input[name='attributeName']", clonedRow).val(value['attributeTypeName']);
                        $("input[name='ruleType']", clonedRow).val(value['ruleType']);
                        $("input[name='value']", clonedRow).val(val);
                        clonedRow.find("input[name='value']").prop('disabled', true);
                        clonedRow.find("input[name='ruleType']").prop('disabled', true);
                        clonedRow.find("input[name='attributeName']").prop('disabled', true);
                        clonedRow.removeClass('hidden');
                        clonedRow.insertBefore('#ruleAttrPreSample');
                        resetAddCombinationAttrRuleRow();

                    });

                }
                $('#addAttrRuleModal').modal('show');
            }
        }

    });

    $('#promoValueBasedMenu').on('click', '#promotionValueBack', function() {
        $('#promoValueBasedMenu').addClass('hidden');
        $('#promoAddMenu').removeClass('hidden');
    });

    $('#createCombinationMenu').on('click', '#promotionCombBack', function() {
        $('#createCombinationMenu').addClass('hidden');
        $('#promoAddMenu').removeClass('hidden');
    });

    $('#itemCode', $addNewProductRow).on('change', function() {
        var locationID = $('#locationID').val();
        if ($(this).val() > 0) {
            selectedProductID = $(this).val();
            eb.ajax({
                type: 'POST',
                url: BASE_URL + '/productAPI/getProductUomList',
                data: {productID: $(this).val(), locationID: locationID},
                success: function(respond) {
                    $("input[name='minQty'],input[name='maxQty']", $addNewProductRow).val('').show().siblings('.uomqty,.uom-select').remove();
                    $("input[name='minQty'],input[name='maxQty']", $addNewProductRow).addUom(respond.data).parent().addClass('input-group');
                }
            });
        }
    });

    $('#itemCode', $addNewRuleRow).on('change', function() {
        var locationID = $('#locationID').val();
        if ($(this).val() > 0) {
            selectedProductID = $(this).val();
            eb.ajax({
                type: 'POST',
                url: BASE_URL + '/productAPI/getProductUomList',
                data: {productID: $(this).val(), locationID: locationID},
                success: function(respond) {
                    $("input[name='quantity']", $addNewRuleRow).val('').show().siblings('.uomqty,.uom-select').remove();
                    $("input[name='quantity']", $addNewRuleRow).addUom(respond.data).parent().addClass('input-group');
                }
            });
        }
    });

    $('#attributeType', $addNewAttrRuleRow).on('change', function() {
        if ($(this).val() == 'quantity') {
            $('#addAttrRuleModal #attrRuleAddBody #attributeValDiv').addClass('hidden');
            $('#addAttrRuleModal #attrRuleAddBody #qty').removeClass('hidden');
            $('#addAttrRuleModal #attrRuleAddBody #rule-type').find('.ruleCon1').removeClass('hidden');
            $('#addAttrRuleModal #attrRuleAddBody #rule-type').find('.ruleCon2').removeClass('hidden');
            
        } else {
            $('#addAttrRuleModal #attrRuleAddBody #qty').addClass('hidden');
            $('#addAttrRuleModal #attrRuleAddBody #attributeValDiv').removeClass('hidden');
            $('#addAttrRuleModal #attrRuleAddBody #rule-type').find('.ruleCon1').removeClass('hidden');
            $('#addAttrRuleModal #attrRuleAddBody #rule-type').find('.ruleCon2').addClass('hidden');

            if ($(this).val() != '0') {
                eb.ajax({
                    type: 'POST',
                    url: BASE_URL + '/api/settings/item-attribute/getItemAttributeRelatedValues',
                    data: {attributeID: $(this).val()},
                    success: function(respond) {
                        $(respond.data).each(function(index, dimValue) {
                            $('#addAttrRuleModal #attrRuleAddBody #attributeValDiv #attributeVal').empty();
                            $('#addAttrRuleModal #attrRuleAddBody #attributeValDiv #attributeVal').append($("<option>", {value: 0, html: 'Select Value'}))
                            $.each(dimValue, function(ind, val) {
                                   $('#addAttrRuleModal #attrRuleAddBody #attributeValDiv #attributeVal').append($("<option>", {value: ind, html: val.description}));
                            });
                        });
                        $('#addAttrRuleModal #attrRuleAddBody #attributeValDiv #attributeVal').selectpicker('refresh');
                    }
                });
            }
        }
    });


    $productTable.on('click', 'button.add', function(e) {
        e.preventDefault();
        var minQty = $("input[name='minQty']", $addNewProductRow).val();
        var maxQty = $("input[name='maxQty']", $addNewProductRow).val();
        var discountType = $("select[name='discountType']", $addNewProductRow).val();
        var $discountAmount = $("input[name='discountAmount']", $addNewProductRow);
        if (selectedProductID == '') {
            p_notification(false, eb.getMessage('ERR_PROMO_SELECT_ITM_CHK'));
        } else if (!jQuery.isEmptyObject(promotionProducts[selectedProductID])) {
            p_notification(false, eb.getMessage('ERR_PROMO_ITM_ADD_CHK'));
        } else if (toFloat(minQty) < 1 || toFloat(maxQty) < toFloat(minQty)) {
            p_notification(false, eb.getMessage('ERR_PROMO_ITM_QTY_CHK'));
        } else if (!validateDiscountAmount(discountType, $discountAmount)) {
        } else {
            var $newRow = $addNewProductRow.clone();
            $newRow.removeClass('promo-add-row').attr('id', selectedProductID);
            $newRow.find("input[name='minQty']").siblings().prop('disabled', true);
            $newRow.find("input[name='maxQty']").siblings().prop('disabled', true);
            $newRow.find("select[name='discountType']").val(discountType).change();
            $newRow.find("input[name='discountAmount'],select[name='discountType']").prop('disabled', true);
            $newRow.find(".selectpicker").prop('disabled', true);
            $newRow.insertBefore($addNewProductRow);
            $('button.add', $newRow).addClass('hidden');
            $('button.edit', $newRow).removeClass('hidden');
            $('button.delete', $newRow).removeClass('disabled');
            promotionProducts[selectedProductID] = new promotionProduct(selectedProductID, minQty, maxQty, discountType, $discountAmount.val());
            resetAddProductRow();
            $('selectItem', $addNewProductRow).focus();
        }
    });

    $combinationTable.on('click', 'button.add', function(e) {
        e.preventDefault();
        var combinationName = $("input[name='combinationName']", $addNewCombinationRow).val();
        var majorCondition = $("select[name='majorConditionType']", $addNewCombinationRow).val();
        var discountType = $("select[name='discountType']", $addNewCombinationRow).val();
        var discountAmount = $("input[name='discountAmount']", $addNewCombinationRow);

        if (combinationName == '') {
            p_notification(false, eb.getMessage('ERR_PROMO_COMB_NAME_CHK'));
        } else if (Object.keys(addedCombinationRules).length == 0){
            p_notification(false, eb.getMessage('ERR_PROMO_COMB_RULES_EMPTY_CHK'));

        }  else if (!validateDiscountAmount(discountType, discountAmount)) {
        } else {

            var randNum = Math.floor((Math.random() * 10000) + 1);
            var rowId = 'comb'+'_'+randNum;



            var $newRow = $addNewCombinationRow.clone();
            $newRow.removeClass('promo-comb-add-row');
            $newRow.addClass('cloned-promo-comb-add-row');
            $newRow.find("input[name='combinationName']").prop('disabled', true);
            $newRow.find("select[name='majorConditionType']").val(majorCondition).change();;
            $newRow.find("select[name='discountType']").val(discountType).change();
            $newRow.find("select[name='majorConditionType']").prop('disabled', true);
            $newRow.find("input[name='discountAmount'],select[name='discountType']").prop('disabled', true);
            $newRow.find("#showRuleModal").removeClass('hidden');
            $newRow.find("#showRuleAddModal").addClass('hidden');
            $newRow.prop('id', rowId);
            $newRow.insertBefore($addNewCombinationRow);
            $('button.add', $newRow).addClass('hidden');
            $('button.edit', $newRow).removeClass('hidden');
            $('button.delete', $newRow).removeClass('disabled');

            var ruleSet = addedCombinationRules;
            addedCombinationRules = {};
            combinationRules = {};

            promotionCombinations[rowId] = new promotionCombination(combinationName, majorCondition, discountType, discountAmount.val(), ruleSet);
            resetAddCombinationRow();

            $('#addRuleModal #ruleAddBody .cloned-promo-comb-rule-add-row').remove();
            $('combinationName', $addNewCombinationRow).focus();
        }
    });


    $ruleTable.on('click', 'button.add-rule', function(e) {
        e.preventDefault();
        var ruleType = $("select[name='ruleType']", $addNewRuleRow).val();
        var quantity = $("input[name='quantity']", $addNewRuleRow).val();
        var productName = $("select[name='itemCode']", $addNewRuleRow).find('option:selected').text();
        var productUom = $("select[name='itemCode']", $addNewRuleRow).find('option:selected').text();


        if (selectedProductID == '') {
            p_notification(false, eb.getMessage('ERR_PROMO_SELECT_ITM_CHK'));
        } else if (quantity == '') {
            p_notification(false, eb.getMessage('ERR_ITEM_RULE_QTY_CHK'));
        } else {


            var randNum = Math.floor((Math.random() * 10000) + 1);
            var rowId = 'rule'+'_'+randNum;


            var newTrID = rowId;
            var clonedRow = $($('#addRuleModal #tempRuleAddBody #rulePreSample').clone()).attr('id', newTrID).addClass('addedRule');
            $("input[name='productName']", clonedRow).val(productName);
            $("input[name='ruleType']", clonedRow).val(ruleType);
            $("input[name='quantity']", clonedRow).val(quantity);
            clonedRow.find("input[name='quantity']").prop('disabled', true);
            clonedRow.find("input[name='ruleType']").prop('disabled', true);
            clonedRow.find("input[name='productName']").prop('disabled', true);
            clonedRow.removeClass('hidden');
            clonedRow.insertBefore('#rulePreSample');
            combinationRules[newTrID] = new combinationRule(selectedProductID, ruleType, quantity, productName);
            resetAddCombinationRuleRow();
        }
    });

    $attrRuleTable.on('click', 'button.add-rule', function(e) {
        e.preventDefault();
        var ruleType = $("select[name='ruleType']", $addNewAttrRuleRow).val();
        var attributeType = $("select[name='attributeType']", $addNewAttrRuleRow).val();
        var quantity = (attributeType == 'quantity') ? $("input[name='quantity']", $addNewAttrRuleRow).val() : null;
        var attributeTypeName = $("select[name='attributeType']", $addNewAttrRuleRow).find('option:selected').text();
        var attributeValID = (attributeType == 'quantity') ? null: $("select[name='attributeVal']", $addNewAttrRuleRow).val();
        var attributeValName = (attributeType == 'quantity') ? null: $("select[name='attributeVal']", $addNewAttrRuleRow).find('option:selected').text();


        if (Object.keys(combinationRules).length > 0) {
            for (var property in combinationRules) {
                if  (attributeType != 'quantity' && combinationRules[property]['attributeType'] == attributeType && combinationRules[property]['attributeValID'] == attributeValID) {
                    p_notification(false, eb.getMessage('ERR_ITEM_ATTR_VALUE_CHK'));
                    return;
                }

                var majorCondition = $("select[name='majorConditionType']", $addNewCombinationRow).val();
                if (attributeType != 'quantity' && majorCondition == 'AND' && combinationRules[property]['attributeType'] == attributeType && combinationRules[property]['ruleType'] == ruleType ) {
                    p_notification(false, eb.getMessage('ERR_ITEM_ATTR_CHK_WITH_MJR_COND'));
                    return;
                }
            }
        }


        if (attributeType == 0) {
            p_notification(false, eb.getMessage('ERR_PROMO_SELECT_ATTR_CHK'));
        } else if (attributeType == 'quantity' && quantity == '') {
            p_notification(false, eb.getMessage('ERR_ITEM_RULE_QTY_CHK'));
        } else {



            var randNum = Math.floor((Math.random() * 10000) + 1);
            var rowId = 'attr_rule'+'_'+randNum;
            var val = (quantity == null) ? attributeValName :  quantity;


            var newTrID = rowId;
            var clonedAttrRow = $($('#addAttrRuleModal #tempAttrRuleAddBody #ruleAttrPreSample').clone()).attr('id', newTrID).addClass('addedAttrRule');
            $("input[name='attributeName']", clonedAttrRow).val(attributeTypeName);
            $("input[name='ruleType']", clonedAttrRow).val(ruleType);
            $("input[name='value']", clonedAttrRow).val(val);
            clonedAttrRow.find("input[name='value']").prop('disabled', true);
            clonedAttrRow.find("input[name='ruleType']").prop('disabled', true);
            clonedAttrRow.find("input[name='attributeName']").prop('disabled', true);
            clonedAttrRow.removeClass('hidden');
            clonedAttrRow.insertBefore('#ruleAttrPreSample');
            resetAddCombinationAttrRuleRow();
            combinationRules[newTrID] = new combinationAttrRule(attributeType, attributeTypeName, attributeValID, attributeValName, ruleType, quantity);
        }
    });

    $productTable.on('click', 'button.save', function() {
        var $saveProductRow = $(this).parents('tr');
        var savingProductID = $saveProductRow.attr('id');
        var minQty = $("input[name='minQty']", $saveProductRow).val();
        var maxQty = $("input[name='maxQty']", $saveProductRow).val();
        var discountType = $("select[name='discountType']", $saveProductRow).val();
        var $discountAmount = $("input[name='discountAmount']", $saveProductRow);
        if (toFloat(minQty) < 1 || toFloat(maxQty) < toFloat(minQty)) {
            p_notification(false, eb.getMessage('ERR_PROMO_ITM_QTY_CHK'));
        } else if (!validateDiscountAmount(discountType, $discountAmount)) {
        } else {
            $saveProductRow.find("input[name='minQty']").siblings().prop('disabled', true);
            $saveProductRow.find("input[name='maxQty']").siblings().prop('disabled', true);
            $saveProductRow.find("input[name='discountAmount'],select[name='discountType']").prop('disabled', true);
            $('button.save', $saveProductRow).addClass('hidden');
            $('button.edit', $saveProductRow).removeClass('hidden');
            promotionProducts[savingProductID] = new promotionProduct(savingProductID, minQty, maxQty, discountType, $discountAmount.val());
        }
    });

    $productTable.on('click', 'button.edit', function() {
        $(this).addClass('hidden');
        $(this).siblings('button.save').removeClass('hidden');
        var $currentEditingItem = $(this).parents('tr');
        $currentEditingItem.find("input[name='minQty'],input[name='maxQty']").siblings().addClass('oldQty');
        var locationID = $('#locationID').val();
        eb.ajax({
            type: 'POST',
            url: BASE_URL + '/productAPI/getProductUomList',
            data: {productID: $currentEditingItem.attr('id'), locationID: locationID},
            success: function(respond) {
                $("input[name='minQty'],input[name='maxQty']", $currentEditingItem).addUom(respond.data).parent().addClass('input-group');
                $(".oldQty", $currentEditingItem).remove();
            }
        });
        $currentEditingItem.find("input[name='discountAmount'],select[name='discountType']").prop('disabled', false);
    });

    $combinationTable.on('click', 'button.edit', function() {
        var $thisRow = $(this).parents('tr');

        var id = $thisRow.attr('id');

        $(this).addClass('hidden');
        $(this).siblings('button.save').removeClass('hidden');


        $thisRow.find("#showRuleModal").addClass('hidden');
        $thisRow.find("#showRuleEditModal").removeClass('hidden');


        $thisRow.find("input[name='combinationName']").prop('disabled', false);
        $thisRow.find("select[name='majorConditionType']").prop('disabled', false);
        $thisRow.find("input[name='discountAmount'],select[name='discountType']").prop('disabled', false);


        $('#promoAddNewCombination .promo-comb-add-row').find("input[name='combinationName']").prop('disabled', true);
        $('#promoAddNewCombination .promo-comb-add-row').find("select[name='majorConditionType']").prop('disabled', true);
        $('#promoAddNewCombination .promo-comb-add-row').find("input[name='discountAmount'],select[name='discountType']").prop('disabled', true);
        $('#promoAddNewCombination .promo-comb-add-row').find("#showRuleAddModal").prop('disabled', true);
        $('#promoAddNewCombination .promo-comb-add-row').find(".add").prop('disabled', true);

        combinationRules = promotionCombinations[id]['ruleSet'];
        addedCombinationRules = promotionCombinations[id]['ruleSet'];
    });

    $combinationTable.on('click', 'button.save', function() {
        var $thisRow = $(this).parents('tr');
        var id = $thisRow.attr('id');

        var editingCombData = promotionCombinations[id];
        console.log(editingCombData);


        var combName = $("input[name='combinationName']", $thisRow).val();
        var combCondition = $("select[name='majorConditionType']", $thisRow).val();
        var combDiscountType = $("select[name='discountType']", $thisRow).val();
        var combDiscountAmount = $("input[name='discountAmount']", $thisRow);

        promotionCombinations[id]['combinationName'] = combName;
        promotionCombinations[id]['majorCondition'] = combCondition;
        promotionCombinations[id]['discountType'] = combDiscountType;
        promotionCombinations[id]['discountAmount'] = combDiscountAmount.val();
        promotionCombinations[id]['ruleSet'] = addedCombinationRules;

        addedCombinationRules = {};
        combinationRules = {};

        $(this).addClass('hidden');
        $(this).siblings('button.edit').removeClass('hidden');


        $thisRow.find("#showRuleModal").removeClass('hidden');
        $thisRow.find("#showRuleEditModal").addClass('hidden');


        $thisRow.find("input[name='combinationName']").prop('disabled', true);
        $thisRow.find("select[name='majorConditionType']").prop('disabled', true);
        $thisRow.find("input[name='discountAmount'],select[name='discountType']").prop('disabled', true);


        $('#promoAddNewCombination .promo-comb-add-row').find("input[name='combinationName']").prop('disabled', false);
        $('#promoAddNewCombination .promo-comb-add-row').find("select[name='majorConditionType']").prop('disabled', false);
        $('#promoAddNewCombination .promo-comb-add-row').find("input[name='discountAmount'],select[name='discountType']").prop('disabled', false);
        $('#promoAddNewCombination .promo-comb-add-row').find("#showRuleAddModal").prop('disabled', false);
        $('#promoAddNewCombination .promo-comb-add-row').find(".add").prop('disabled', false);

    });

    $productTable.on('click', 'button.delete', function() {
        var deleteID = $(this).parents('tr').attr('id');
        $(this).parents('tr').remove();
        delete(promotionProducts[deleteID]);
    });

    $combinationTable.on('click', 'button.delete', function() {
        var deleteID = $(this).parents('tr').attr('id');
        $(this).parents('tr').remove();
        delete(promotionCombinations[deleteID]);
    });

    $tempRuleTable.on('click', 'button.delete-rule', function() {
        var deleteID = $(this).parents('tr').attr('id');
        console.log(deleteID);
        $(this).parents('tr').remove();

        delete combinationRules[deleteID];
    });

    $tempAttrRuleTable.on('click', 'button.delete-rule', function() {
        var deleteID = $(this).parents('tr').attr('id');
        console.log(deleteID);
        $(this).parents('tr').remove();

        delete combinationRules[deleteID];
    });

    $('#addRuleModal .modal-footer').on('click', '.comb-rule-save , .comb-rule-update', function() {

        addedCombinationRules = {};
        $.each(combinationRules, function(index, value) {
            addedCombinationRules[index] = value;
        });

        $('#addRuleModal').modal('hide');
    });

    $('#addRuleModal .modal-footer').on('click', '.comb-rule-close', function() {

        combinationRules = {};
        $('#addRuleModal').modal('hide');
    });

    $('#addAttrRuleModal .modal-footer').on('click', '.comb-attr-rule-save , .comb-attr-rule-update', function() {

        addedCombinationRules = {};
        $.each(combinationRules, function(index, value) {
            addedCombinationRules[index] = value;
        });

        $('#addAttrRuleModal').modal('hide');
    });

    $('#addAttrRuleModal .modal-footer').on('click', '.comb-attr-rule-close', function() {

        combinationRules = {};
        $('#addAttrRuleModal').modal('hide');
    });

    $('#promoItemBasedMenu').on('click', '#promotionItemSave', function(e) {
        e.preventDefault();
        if (jQuery.isEmptyObject(promotionProducts)) {
            p_notification(false, eb.getMessage('ERR_PROMO_ITM_SAVE'));
        } else {
            var promotionPostData = {
                locationID: ($("input[name='globalPromotion']", '#createPromotionForm').is(":checked")) ? '' : $('#locationID').val(),
                promoName: $('#promotionName', '#createPromotionForm').val(),
                promoFromDate: $('#promotionFrom', '#createPromotionForm').val(),
                promoToDate: $('#promotionTo', '#createPromotionForm').val(),
                promoType: $('#promotionType', '#createPromotionForm').val(),
                promoDesc: $('#promotionDescription', '#createPromotionForm').val(),
                promoProducts: promotionProducts,
            };
            savePromotion(promotionPostData);
        }
    });

    $('#createCombinationMenu').on('click', '#promotionCombSave', function(e) {
        e.preventDefault();
        if (jQuery.isEmptyObject(promotionCombinations)) {
            p_notification(false, eb.getMessage('ERR_PROMO_ITM_SAVE'));
        } else {
            var promotionPostData = {
                locationID: ($("input[name='globalPromotion']", '#createPromotionForm').is(":checked")) ? '' : $('#locationID').val(),
                promoName: $('#promotionName', '#createPromotionForm').val(),
                promoFromDate: $('#promotionFrom', '#createPromotionForm').val(),
                promoToDate: $('#promotionTo', '#createPromotionForm').val(),
                promoType: $('#promotionType', '#createPromotionForm').val(),
                promoDesc: $('#promotionDescription', '#createPromotionForm').val(),
                promoProducts: promotionProducts,
                promotionCombinations: promotionCombinations
            };
            savePromotion(promotionPostData);
        }
    });

    $('#promoValueForm').on('keyup', '#promoMinVal,#promoMaxVal', function() {
        var decimalPoints;
        if ($(this).val().indexOf('.') > 0) {
            decimalPoints = $(this).val().split('.')[1].length;
        }
        if ((!$.isNumeric($(this).val()) || $(this).val() < 0 || decimalPoints > 2)) {
            decimalPoints = 0;
            $(this).val('');
        }
    });

    var amount_type = $('#promoValueAmountCurrency');
    $('#promoValueBasedMenu').on('change', "select[name='valueDiscountType']", function() {
        (parseInt($(this).val()) === 1) ? amount_type.text('Rs') : amount_type.text('%');
    });

    $('#promoValueForm').submit(function(e) {
        e.preventDefault();
        var valueDiscountType = $("select[name='valueDiscountType']", '#promoValueForm').val();
        var $valueDiscountAmount = $("#promoValueAmount", '#promoValueForm');
        if ($('#promoMinVal', '#promoValueForm').val() == '' || $('#promoMaxVal', '#promoValueForm').val() == '' || $('#promoValueAmount', '#promoValueForm').val() == '') {
            p_notification(false, eb.getMessage('ERR_PROMO_VAL_REQ_FIELD_CHK'));
            return false;
        } else if (isNaN($('#promoMinVal', '#promoValueForm').val()) || isNaN($('#promoMaxVal', '#promoValueForm').val())) {
            p_notification(false, eb.getMessage('ERR_PROMO_VAL_REQ_FIELD_CHK'));
            return false;
        } else if (parseFloat($('#promoMinVal').val()) > parseFloat($('#promoMaxVal').val())) {
            p_notification(false, eb.getMessage('ERR_PROMO_MAX_LESSTHAN_MIN'));
            return false;
        }
        else if (!validateDiscountAmount(valueDiscountType, $valueDiscountAmount)) {
        } else {
            var promotionPostData = {
                locationID: ($("input[name='globalPromotion']", '#createPromotionForm').is(":checked")) ? '' : $('#locationID').val(),
                promoName: $('#promotionName', '#createPromotionForm').val(),
                promoFromDate: $('#promotionFrom', '#createPromotionForm').val(),
                promoToDate: $('#promotionTo', '#createPromotionForm').val(),
                promoType: $('#promotionType', '#createPromotionForm').val(),
                promoDesc: $('#promotionDescription', '#createPromotionForm').val(),
                promoValueMin: $('#promoMinVal', '#promoValueForm').val(),
                promoValueMax: $('#promoMaxVal', '#promoValueForm').val(),
                promoValueDiscountType: $("select[name='valueDiscountType']", '#promoValueForm').val(),
                promoValueDiscountAmount: $("#promoValueAmount", '#promoValueForm').val(),
            };
            savePromotion(promotionPostData);
        }
    });


    //view list functions
    $('#promotionList').on('click', '.changeStatus', function() {
        var promID = $(this).attr('id');
        var $statusSpan = $(this).find("span");
        var currentStatusClass = $statusSpan.attr('class');
        var changeStatusTo = '';
        if (currentStatusClass == 'glyphicon glyphicon-check') {
            changeStatusTo = 2;
        } else {
            changeStatusTo = 1;
        }
        eb.ajax({
            type: 'POST',
            url: BASE_URL + '/api/promotions/changePromotionStatus',
            data: {promotionID: promID, changeStatusTo: changeStatusTo},
            success: function(respond) {
                if (respond.status) {
                    if (changeStatusTo == 2) {
                        $statusSpan.removeClass('glyphicon glyphicon-check');
                        $statusSpan.addClass('glyphicon glyphicon-unchecked');
                    } else {
                        $statusSpan.removeClass('glyphicon glyphicon-unchecked');
                        $statusSpan.addClass('glyphicon glyphicon-check');
                    }
                }
            }
        });
    });

    //promotion email functions

    tinymce.init({
      selector: "textarea[name='emailBody']",
      statusbar: false,
      content_css: [
        "/assets/bootstrap/css/bootstrap.min.css",
        "/css/template-editor.css",
      ],
      plugins: [
        "advlist autolink lists link image charmap print preview anchor",
        "searchreplace visualblocks code fullscreen",
        "insertdatetime media table contextmenu paste", //autoresize
      ],
      height: 200,
      toolbar:
        "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link",
    });

    $('#promoEmailsWindow').on('click', "#promoEmailEditBack", function() {
        $('#promoEmailsWindow').addClass('hidden');
        if ($('#promoEmailsDiv').length) {
            $('#promoEmailsDiv').removeClass('hidden');
        } else {
            $('#promoCustomerEventsEmailsDiv').removeClass('hidden');
        }
    });

    $("#promoSmsWindow").on("click", "#promoSmsEditBack", function () {
        $("#promoSmsWindow").addClass("hidden");
        if ($("#promoEmailsDiv").length) {
            $("#promoEmailsDiv").removeClass("hidden");
        } else {
            $("#promoCustomerEventsEmailsDiv").removeClass("hidden");
        }
    });

    $("#promoCustomerEventsSmsWindow").on("click", "#promoSmsEditBack", function () {
        $("#promoCustomerEventsSmsWindow").addClass("hidden");
        if ($("#promoEmailsDiv").length) {
            $("#promoEmailsDiv").removeClass("hidden");
        } else {
            $("#promoCustomerEventsEmailsDiv").removeClass("hidden");
        }
    });

    $('#promoEmailsDiv').on('click', '#getCustList', function() {
        if ($("select[name='location[]'] option:selected").text() == '') {
            p_notification(false, eb.getMessage("ERR_PROMO_EMAIL_LOC_EMPTY"));
            return false;
        } else if ($("select[name='product[]'] option:selected").text() == '') {
            p_notification(false, eb.getMessage("ERR_PROMO_EMAIL_PROD_EMPTY"));
            return false;
        } else {
            selectedPromoEmailProducts = [];
            var selectCustCondition = '';
            if ($("input[name='filter_customer']:checked", '#promoEmailsDiv').val() == 'all') {
                selectCustCondition = 'all';
            }
            $("select[name='product[]'] option:selected").each(function() {
                selectedPromoEmailProducts.push($(this).val());
            });
            eb.ajax({
                type: 'POST',
                url: BASE_URL + '/invoice-api/getCustomersForPurchasedProducts',
                data: {products: selectedPromoEmailProducts, condition: selectCustCondition},
                success: function(respond) {
                    if (respond.data) {
                        promoCustomersList = respond.data;
                        $('#promoCustomerTable').removeClass('hidden');
                        $('#promoCustomerTable').find(".promoCustomers").remove();

                        for (var i in respond.data) {
                            var $customerSample = $('#customerSampleRow', '#promoCustomerTable').clone();
                            $customerSample.find('.customerName').html(respond.data[i].name);
                            $customerSample.find('.customerEmail').html(respond.data[i].email);
                            $customerSample
                              .find(".customerPhoneNumber")
                              .html(respond.data[i].telephoneNumber);
                            $customerSample.find('.customerStatus').html(respond.data[i].customerStatus);
                            $customerSample.find("input[name='customer[]']").val(i);
                            $customerSample.attr('id', '');
                            $customerSample.addClass('promoCustomers');
                            $customerSample.removeClass('hidden');
                            $customerSample.removeClass('hidden');
                            $customerSample.insertBefore($('#customerSampleRow', '#promoCustomerTable'));
                        }
                    } else {
                        $('#promoCustomerTable').addClass('hidden');
                        $('#promoCustomerTable').find(".promoCustomers").remove();
                        p_notification('info', eb.getMessage('ERR_PROMO_EMAIL_CUST_LIST'));
                    }
                }
            });
        }
    });

    $('#promoEmailsDiv').on('click', '#getProductList', function() {
        if ($("select[name='location[]'] option:selected").text() == '') {
            p_notification(false, eb.getMessage("ERR_PROMO_EMAIL_LOC_EMPTY"));
            return false;
        } else {
            selectedPromoEmailLocations = []
            $("select[name='location[]'] option:selected").each(function() {
                selectedPromoEmailLocations.push($(this).val());
            });
            $('#product', '#promoEmailsDiv').prop('disabled', false);
            $('#product', '#promoEmailsDiv').selectpicker('refresh');
            $('#getCustList', '#promoEmailsDiv').prop('disabled', false);
            loadDropDownFromDatabase('/productAPI/getProductsForLocations', selectedPromoEmailLocations, 0, '#product');
        }
    });


    $('#promoEmailsDiv').on('click', '#emailPromoNext', function() {
        selectedPromoEmailCustomers = [];
        if ($('.promoCustomerSelect', '#promoCustomerTable').is(':checked')) {
            $('.promoCustomerSelect', '#promoCustomerTable').each(function() {
                if ($(this).is(':checked')) {
                    selectedPromoEmailCustomers.push(this.id);
                }
            });
            $('#promoEmailsDiv').addClass('hidden');
            $('#promoEmailsWindow').removeClass('hidden');

        } else {
            p_notification(false, eb.getMessage('ERR_PROMO_EMAIL_CUST_EMPTY'));
        }
    });

    $("#promoEmailsDiv").on("click", "#smsPromoNext", function () {
        //selectedPromoSmsCustomers = [];
        selectedPromoSmsCustomersName = [];
      if ($(".promoCustomerSelect", "#promoCustomerTable").is(":checked")) {
        $(".promoCustomerSelect", "#promoCustomerTable").each(function () {
            if ($(this).is(":checked")) {
                selectedPromoSmsCustomersName.push(
                  promoCustomersList[$(this).val()].name
                );
            }
        });
        $("#smsPromotionSelectedCustomers").val(
        selectedPromoSmsCustomersName
        );
        $("#promoEmailsDiv").addClass("hidden");
        $("#promoSmsWindow").removeClass("hidden");
        
      } else {
        p_notification(false, eb.getMessage("ERR_PROMO_EMAIL_CUST_EMPTY"));
      }
    });

    $("#promoSmsWindow").on("click", "#promoSmsSave", function () {
         if (jQuery.isEmptyObject(selectedPromoEmailLocations)) {
           p_notification(false, eb.getMessage("ERR_PROMO_EMAIL_LOC_EMPTY"));
           return false;
         } else if (jQuery.isEmptyObject(selectedPromoEmailProducts)) {
           p_notification(false, eb.getMessage("ERR_PROMO_EMAIL_PROD_EMPTY"));
           return false;
         } else if (selectedPromoSmsCustomersName.length == 0 ) {
           p_notification(false, eb.getMessage("ERR_PROMO_EMAIL_CUST_EMPTY"));
           return false;
         } else {
           tinymce.triggerSave();
           var formData = new FormData($("#promoEmailForm")[0]);
           eb.ajax({
             type: "POST",
             url: BASE_URL + "/api/promotions/savePromotionSms",
             data: formData,
             success: function (respond) {
               if (respond.status) {
                   if (respond.data.error_msg.length > 0) {
                     p_notification(false, respond.data.error_msg);
                   }
                   if (respond.data.success_msg.length > 0) {
                     p_notification(true, respond.data.success_msg);
                   }
                   window.setTimeout(function () {
                     location.reload();
                   }, 1500);
               } else {
                 p_notification(false, respond.msg);
               }
             },
             cache: false,
             contentType: false,
             processData: false,
           });
         }
    });



    $('#promoEmailForm').on('submit', function(e) {
        e.preventDefault();
        //alert("working");
        if (jQuery.isEmptyObject(selectedPromoEmailLocations)) {
            p_notification(false, eb.getMessage("ERR_PROMO_EMAIL_LOC_EMPTY"));
            return false;
        } else if (jQuery.isEmptyObject(selectedPromoEmailProducts)) {
            p_notification(false, eb.getMessage("ERR_PROMO_EMAIL_PROD_EMPTY"));
            return false;
        } else if (jQuery.isEmptyObject(selectedPromoEmailCustomers)) {
            p_notification(false, eb.getMessage('ERR_PROMO_EMAIL_CUST_EMPTY'));
            return false;
        } else {
            tinymce.triggerSave();
            var formData = new FormData($(this)[0]);
            eb.ajax({
                type: 'POST',
                url: BASE_URL + '/api/promotions/savePromotionEmail',
                data: formData,
                success: function(respond) {
                    if (respond.status) {
                        window.location.reload();
                    } else {
                        p_notification(false, respond.msg);
                    }
                },
                cache: false,
                contentType: false,
                processData: false
            });

        }
    });

    $("#promotionEmailList").on('click', '.viewPromoEmails', function() {
        var promoEmailID = $(this).parents('tr').attr('id');
        eb.ajax({
            type: 'POST',
            url: BASE_URL + '/api/promotions/getEmailPromotion',
            data: {promEmailID: promoEmailID},
            success: function(respond) {
                if (respond.status) {
                    $('#promoEmailModal').html(respond.data);
                    $('#promoEmailModal').modal('show');
                } else {
                }
            }
        });
    });

    $('#promotionList').on('click', '.delete', function() {
        var promID = $(this).attr('id');
        bootbox.confirm("Are you sure you want to delete this promotion?", function(result) {
            if (result == true) {
                eb.ajax({
                    type: 'POST',
                    url: BASE_URL + '/api/promotions/delete',
                    data: {promotionID: promID},
                    success: function(respond) {
                        if (respond.status) {
                            window.location.reload();
                        } else {
                            p_notification(false, respond.msg);
                        }
                    }
                });
            }
        });
    });

    var fromDate = $('#customerEventFromDate').datepicker().on('changeDate', function(ev) {
        fromDate.hide();
    }).data('datepicker');

    var toDate = $('#customerEventToDate').datepicker().on('changeDate', function(ev) {
        toDate.hide();
    }).data('datepicker');

    $('#promoCustomerEventsEmailForm').on('click', '#getCustomerEventList', function() {
        var fromDate = $('#customerEventFromDate', '#promoCustomerEventsEmailForm').val();
        var toDate = $('#customerEventToDate', '#promoCustomerEventsEmailForm').val();
        var dateValidation = true;
        if (toDate != "" && fromDate != "") {
            if (toDate < fromDate) {
                p_notification(false, eb.getMessage('ERR_PROM_EMAIL_CUS_EVNT_DATE'));
                dateValidation = false;
            }
        }

        if (dateValidation) {
            var sendParams = {'fromDate': fromDate, 'toDate': toDate};
            $("button#getCustomerList", '#promoCustomerEventsEmailForm').prop('disabled', false);
            $("#customerEvents", '#promoCustomerEventsEmailForm').prop('disabled', false);
            loadDropDownFromDatabase('/api/customer-event/getCustomerEvents', sendParams, 0, '#customerEvents');
        }

    });

    $('#promoCustomerEventsEmailForm').on('click', 'button#getCustomerList', function() {

        if ($("select[name='customerEvents[]'] option:selected").text() == "") {
            p_notification(false, eb.getMessage("ERR_PROM_EMAIL_CUS_EVNT_EMPTY"));
        } else {
            selectedPromoEmailCustomerEvents = [];
            $("select[name='customerEvents[]'] option:selected").each(function() {
                if ($(this).val() != 0) {
                    selectedPromoEmailCustomerEvents.push($(this).val());
                }
            });
            eb.ajax({
                type: 'POST',
                url: BASE_URL + "/api/customer-event/getCustomersForCustomerEvents",
                data: {customerEvents: selectedPromoEmailCustomerEvents},
                success: function(respond) {
                    if (respond.data) {
                        promoCustomersEventList = respond.data;
                        $('#promoCustomerTable').removeClass('hidden');
                        $('#promoCustomerTable').find(".promoCustomers").remove();

                        for (var i in respond.data) {
                            var $customerSample = $('#customerSampleRow', '#promoCustomerTable').clone();
                            $customerSample.find('.customerName').html(respond.data[i].name);
                            $customerSample.find('.customerEmail').html(respond.data[i].email);
                            $customerSample.find('.customerStatus').html(respond.data[i].customerStatus);
                            $customerSample.find("input[name='customer[]']").val(i);
                            $customerSample.attr('id', '');
                            $customerSample.addClass('promoCustomers');
                            $customerSample.removeClass('hidden');
                            $customerSample.removeClass('hidden');
                            $customerSample.insertBefore($('#customerSampleRow', '#promoCustomerTable'));
                        }
                    } else {
                        $('#promoCustomerTable').addClass('hidden');
                        $('#promoCustomerTable').find(".promoCustomers").remove();
                        p_notification('info', eb.getMessage('ERR_PROMO_EMAIL_CUST_LIST'));
                    }
                }
            });
        }
    });

    $('#promoCustomerEventsEmailForm').on('click', '#emailPromoNext', function() {
        selectedPromoEmailCustomers = [];
        if ($('.promoCustomerSelect', '#promoCustomerTable').is(':checked')) {
            $('.promoCustomerSelect', '#promoCustomerTable').each(function() {
                if ($(this).is(':checked')) {
                    selectedPromoEmailCustomers.push($(this).val());
                }
            });
            $('#promoCustomerEventsEmailsDiv').addClass('hidden');
            $('#promoEmailsWindow').removeClass('hidden');
        } else {
            p_notification(false, eb.getMessage('ERR_PROMO_EMAIL_CUST_EMPTY'));
        }
    });

    $("#promoCustomerEventsEmailForm").on("click", "#smsPromoNext", function () {
       //selectedPromoSmsCustomers = [];
      selectedPromoSmsCustomersName = [];
      if ($(".promoCustomerSelect", "#promoCustomerTable").is(":checked")) {
        $(".promoCustomerSelect", "#promoCustomerTable").each(function () {
            if ($(this).is(":checked")) {
              selectedPromoSmsCustomersName.push(
                promoCustomersEventList[$(this).val()].name
              );
          }
        });
        $("#smsPromotionSelectedCustomers").val(selectedPromoSmsCustomersName);
        $("#promoCustomerEventsEmailsDiv").addClass("hidden");
        $("#promoCustomerEventsSmsWindow").removeClass("hidden");
      } else {
        p_notification(false, eb.getMessage("ERR_PROMO_EMAIL_CUST_EMPTY"));
      }
    });

    $('#promoCustomerEventsEmailForm').on('submit', function(e) {
        e.preventDefault();
        if (jQuery.isEmptyObject(selectedPromoEmailCustomerEvents)) {
            p_notification(false, eb.getMessage("ERR_PROM_EMAIL_CUS_EVNT_EMPTY"));
            return false;
        } else if (jQuery.isEmptyObject(selectedPromoEmailCustomers)) {
            p_notification(false, eb.getMessage('ERR_PROM_EMAIL_CUS_EMPTY'));
            return false;
        } else {
            tinymce.triggerSave();
            var formData = new FormData($(this)[0]);
            eb.ajax({
                type: 'POST',
                url: BASE_URL + '/api/promotions/savePromotionEmailCustomerEvents',
                data: formData,
                success: function(respond) {
                    if (respond.status) {
                        window.location.reload();
                    } else {
                        p_notification(false, respond.msg);
                    }
                },
                cache: false,
                contentType: false,
                processData: false
            });

        }
    });

    $("#promoCustomerEventsSmsWindow").on("click", "#promoSmsSave", function () {
      if (jQuery.isEmptyObject(selectedPromoEmailCustomerEvents)) {
        p_notification(false, eb.getMessage("ERR_PROM_EMAIL_CUS_EVNT_EMPTY"));
        return false;
      } else if (selectedPromoSmsCustomersName.length == 0) {
        p_notification(false, eb.getMessage("ERR_PROM_EMAIL_CUS_EMPTY"));
        return false;
      } else {
        tinymce.triggerSave();
        var formData = new FormData($("#promoCustomerEventsEmailForm")[0]);
        eb.ajax({
          type: "POST",
          url: BASE_URL + "/api/promotions/savePromotionSms",
          data: formData,
          success: function (respond) {
            if (respond.status) {
              if (respond.data.error_msg.length > 0) {
                p_notification(false, respond.data.error_msg);
              }
              if (respond.data.success_msg.length > 0) {
                p_notification(true, respond.data.success_msg);
              }
              window.setTimeout(function () {
                location.reload();
              }, 1500);
            } else {
              p_notification(false, respond.msg);
            }
          },
          cache: false,
          contentType: false,
          processData: false,
        });
      }
    });

    function savePromotion(promoPostData) {

        if (thisIsPromotionEdit == true) {
            var saveUrl = BASE_URL + '/api/promotions/update/' + editPromotionID;
        } else {
            var saveUrl = BASE_URL + '/api/promotions/save';
        }
        eb.ajax({
            type: 'POST',
            url: saveUrl,
            data: {promoData: promoPostData},
            success: function(respond) {
                if (respond.status == true) {
                    if (thisIsPromotionEdit == true) {
                        window.location.href = BASE_URL + '/promotions/list';
                    } else {
                        window.location.reload();
                    }
                } else {
                    p_notification(false, respond.msg);
                }
            }
        });
    }

    function validateDiscountAmount(discountType, $discountAmount) {
        if (isNaN($discountAmount.val())) {
            p_notification(false, eb.getMessage('ERR_PROMO_DISCNT_AMNT'));
            $discountAmount.val('0');
            return false;
        } else if ($discountAmount.val() <= 0) {
            p_notification(false, eb.getMessage('ERR_PROMO_DISCNT_AMNT_ZERO'));
            $discountAmount.val('0');
            return false;
        }
        if (discountType == '1') {
            if ($discountAmount.val() > parseFloat($('#promoMaxVal').val())) {
                p_notification(false, eb.getMessage('ERR_PROMO_DISCNT_AMNT_EXCEED'));
                return false;
            }else if ($discountAmount.val() > parseFloat($('#promoMinVal').val())) {
                p_notification(false, eb.getMessage('ERR_PROMO_DISCNT_AMNT_EXCEED_IN_MINVAL'));
                return false;
            }else{
                return  true;
            }
        } else if (discountType == '2') {
            if ($discountAmount.val() > 99) {
                p_notification(false, eb.getMessage('ERR_PROMO_AMNT_PERC'));
                $discountAmount.val('0');
                return false;
            } else {
                return true;
            }
        }
    }
    //set products for promotion edit
    function setPromotionProducts(promoProducts) {
        if (alreadySetPromotionProducts == false) {
            for (var promoPro in promoProducts) {
                $("input[name='minQty']", $addNewProductRow).val(promoProducts[promoPro].minQty);
                $("input[name='maxQty']", $addNewProductRow).val(promoProducts[promoPro].maxQty)
                $("input[name='minQty'],input[name='maxQty']", $addNewProductRow).addUom(promoProducts[promoPro].uom).parent().addClass('input-group');
                $("select[name='discountType']", $addNewProductRow).val(promoProducts[promoPro].discountType);
                $("input[name='discountAmount']", $addNewProductRow).val(promoProducts[promoPro].discountAmount);
                var minQty = $("input[name='minQty']", $addNewProductRow).val();
                var maxQty = $("input[name='maxQty']", $addNewProductRow).val();
                var discountType = $("select[name='discountType']", $addNewProductRow).val();
                var $discountAmount = $("input[name='discountAmount']", $addNewProductRow);
                var $newRow = $addNewProductRow.clone();
                $newRow.find("button.selectpicker").children('span.filter-option').html(promoProducts[promoPro].productName);
                $newRow.removeClass('promo-add-row').attr('id', promoProducts[promoPro].productID);
                //to fix later
                $newRow.find("input[name='minQty']").siblings().prop('disabled', true);
                $newRow.find("input[name='maxQty']").siblings().prop('disabled', true);
                var optionSelectedString = "option[value=" + promoProducts[promoPro].discountType + "]";
                $newRow.find("select[name='discountType']").find(optionSelectedString).attr('selected', 'selected');
                $newRow.find("input[name='discountAmount'],select[name='discountType']").prop('disabled', true);
                $newRow.find(".selectpicker").prop('disabled', true);
                $newRow.insertBefore($addNewProductRow);
                $('button.add', $newRow).addClass('hidden');
                $('button.edit', $newRow).removeClass('hidden');
                $('button.delete', $newRow).removeClass('disabled');
                promotionProducts[promoProducts[promoPro].productID] = new promotionProduct(promoProducts[promoPro].productID, minQty, maxQty, discountType, $discountAmount.val());
                resetAddProductRow();
            }
            //flag-stop repeating promotion products when editing
            alreadySetPromotionProducts = true;
        }
    }


    function setPromotionCombinations(combinations, promoType) {
        for (var comb in combinations) {
            $("input[name='combinationName']", $addNewCombinationRow).val(combinations[comb].combinationName);
            $("select[name='majorConditionType']", $addNewCombinationRow).val(combinations[comb].conditionType)
            $("select[name='discountType']", $addNewCombinationRow).val(combinations[comb].discountType);
            $("input[name='discountAmount']", $addNewCombinationRow).val(combinations[comb].discountAmount);
            var combinationName = $("input[name='combinationName']", $addNewCombinationRow).val();
            var majorConditionType = $("select[name='majorConditionType']", $addNewCombinationRow).val();
            var discountType = $("select[name='discountType']", $addNewCombinationRow).val();
            var discountAmount = $("input[name='discountAmount']", $addNewCombinationRow);

            var randNum = Math.floor((Math.random() * 10000) + 1);
            var rowId = 'comb'+'_'+randNum;


            var $newRow = $addNewCombinationRow.clone();
            $newRow.removeClass('promo-comb-add-row');
            $newRow.addClass('cloned-promo-comb-add-row');
            $newRow.find("input[name='combinationName']").prop('disabled', true);
            $newRow.find("select[name='majorConditionType']").val(majorConditionType).change();;
            $newRow.find("select[name='discountType']").val(discountType).change();
            $newRow.find("select[name='majorConditionType']").prop('disabled', true);
            $newRow.find("input[name='discountAmount'],select[name='discountType']").prop('disabled', true);
            $newRow.find("#showRuleModal").removeClass('hidden');
            $newRow.find("#showRuleAddModal").addClass('hidden');
            $newRow.prop('id', rowId);
            $newRow.insertBefore($addNewCombinationRow);
            $('button.add', $newRow).addClass('hidden');
            $('button.edit', $newRow).removeClass('hidden');
            $('button.delete', $newRow).removeClass('disabled');

            var ruleSet = {};

            for (var i = 0; i < combinations[comb].ruleSet.length; i++) {
                var rand = Math.floor((Math.random() * 10000) + 1);
                var idpre= (promoType == 1) ? 'rule' : 'attr_rule';
                var id = idpre+'_'+rand;
                ruleSet[id] = combinations[comb].ruleSet[i];
            }

            addedCombinationRules = {};
            combinationRules = {};

            promotionCombinations[rowId] = new promotionCombination(combinationName, majorConditionType, discountType, discountAmount.val(), ruleSet);
            resetAddCombinationRow();
        }
    }

    function setInvoicePromotionData(promoData) {
        $('#promoMinVal', '#promoValueForm').val(promoData.minValue);
        $('#promoMaxVal', '#promoValueForm').val(promoData.maxValue);
        $("select[name='valueDiscountType']", '#promoValueForm').val(promoData.discountType);
        $("#promoValueAmount", '#promoValueForm').val(promoData.discountAmount);
        $("select[name='valueDiscountType']", '#promoValueBasedMenu').trigger('change');
    }

    function resetAddProductRow() {
        $("input[name='minQty'],input[name='maxQty']", $addNewProductRow).val('').show().siblings('.uomqty,.uom-select').remove();
        $("input[name='discountAmount']", $addNewProductRow).val('');
        $("select[name='discountType']", $addNewProductRow).val('1');
        $('#itemCode', $addNewProductRow).val('0').trigger('change');
        $('#itemCode', $addNewProductRow).text("Select Item");
        $('#itemCode', $addNewProductRow).selectpicker('refresh');
        selectedProductID = "";
        selectedProduct = "";
    }

    function resetAddCombinationRuleRow() {
        $("input[name='quantity']", $addNewRuleRow).val('').show().siblings('.uomqty,.uom-select').remove();
        $("select[name='ruleType']", $addNewRuleRow).val('equal');
        $('#itemCode', $addNewRuleRow).val('0').trigger('change');
        $('#itemCode', $addNewRuleRow).text("Select Item");
        $('#itemCode', $addNewRuleRow).selectpicker('refresh');
        selectedProductID = "";
        selectedProduct = "";
    }

    function resetAddCombinationAttrRuleRow() {
        $("input[name='quantity']", $addNewAttrRuleRow).val('').show().siblings('.uomqty,.uom-select').remove();
        $("select[name='ruleType']", $addNewAttrRuleRow).val('equal');

        $("select[name='attributeType']", $addNewAttrRuleRow).val('0').trigger('change');
        $("select[name='attributeType']", $addNewAttrRuleRow).selectpicker('refresh');
        $('#addAttrRuleModal #attrRuleAddBody #attributeValDiv #attributeVal').empty();
        $('#addAttrRuleModal #attrRuleAddBody #attributeValDiv #attributeVal').append($("<option>", {value: 0, html: 'Select Value'}));
        $('#addAttrRuleModal #attrRuleAddBody #attributeValDiv #attributeVal').selectpicker('refresh');
     
    }

    function resetAddCombinationRow() {
        $("input[name='combinationName']", $addNewCombinationRow).val('');
        $("input[name='discountAmount']", $addNewCombinationRow).val('');
        $("select[name='discountType']", $addNewCombinationRow).val('1').change();
        $("select[name='majorConditionType']", $addNewCombinationRow).val('AND').change();
    }

    $('.reset').on('click', function(e) {
        $('#location').val('default').selectpicker('refresh');
        $('#location').selectpicker('refresh');
        $('#product', '#promoEmailsDiv').prop('disabled', true);
        resetSelectPicker("product","product(s)");
        $('#getCustList', '#promoEmailsDiv').prop('disabled', true);
        $('#promoCustomerTable').addClass('hidden');
        $('#promoCustomerTable').find(".promoCustomers").remove();
         loadDropDownFromDatabase('/productAPI/search-location-products-for-dropdown', locationID, 0, '#itemCode', '', '', documentType);
    });

    function resetSelectPicker(referenceId, string) {
        $('#' + referenceId).val('').empty().selectpicker('refresh');
        $('#' + referenceId).val('').trigger('change').selectpicker('refresh');
    }

});
