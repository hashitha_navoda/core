$(document).ready(function(){

    var $ruleTable = $('#addConditionModal #ruleAddBody');
    var $addNewRuleRow = $('tr.promo-comb-rule-add-row', $ruleTable);
    var schemeRules = {};
    var addedSchemeRules = {};
    var disSign = $('#currency').val();


    function schemeRule(minQty, maxQty, discount) {
        this.minQty = minQty;
        this.maxQty = maxQty;
        this.discount = discount;
    }


    $('#sales-acc-id').on('change', function (){
    	if ($(this).val() != 0 || $(this).val() != ''){
    		productSalesAccountID = $(this).val();
    	}
    });

    $('#inventory-acc-id').on('change', function (){
    	if ($(this).val() != 0 || $(this).val() != ''){
    		productInventoryAccountID = $(this).val();
    	}
    });

    $('#cgs-acc-id').on('change', function (){
    	if ($(this).val() != 0 || $(this).val() != ''){
    		productCOGSAccountID = $(this).val();
    	}
    });

    $('#adjusment-acc-id').on('change', function (){
    	if ($(this).val() != 0 || $(this).val() != ''){
    		productAdjusmentAccountID = $(this).val();
    	}
    });

    $('#save-disc-scheme').on('click', function () {
    	var schemeCode = $('.discount-scheme-code').val();
    	var schemeName = $('.discount-scheme-name').val();
        var discountType = $('#discountType').val();


    	if (schemeCode == '' || schemeCode == null) {
            p_notification(false, eb.getMessage('ERR_NO_SCHEME_CODE'));
        } else if (schemeName == '' || schemeName == null) {
            p_notification(false, eb.getMessage('ERR_NO_SCHEME_NAME'));
        } else if (Object.keys(addedSchemeRules).length == 0) {
            p_notification(false, eb.getMessage('ERR_NO_SCHEME_CONDITIONS'));

        } else {
    		var savedDataSet = {
    			schemeCode: schemeCode,
    			schemeName: schemeName,
    			discountType: discountType,
    			addedSchemeRules: addedSchemeRules
    		};

    		eb.ajax({
	                type: 'POST',
	                url: BASE_URL + '/api/discount-schemes/save',
	                data: savedDataSet,
	                success: function(respond) {
	                	p_notification(respond.status, respond.msg);
	                	if(respond.status) {
                            window.location.reload();
	                	}
	                }
	        });
    		
    	}

    });

    $(document).on('click', '#disc-schem-list-view .delete-scheme',function(e) {
        e.preventDefault();

        var $thisRow = $(this).parents('tr');
        var disType = $thisRow.attr('data-discType') ;

        var dataSet = {
            schemeID: $thisRow.attr('data-discSchemId')
        };


        bootbox.confirm('Are you sure you want to delete this discount scheme ?', function(result) {
            if (result === true) {
                eb.ajax({
                    type: 'POST',
                    url: BASE_URL + '/api/discount-schemes/delete-scheme',
                    data: dataSet,
                    success: function(respond) {
                        p_notification(respond.status, respond.msg);
                        if(respond.status) {
                            window.location.reload();
                        }
                    }
                });
            }
        });

    });

    $('#discount-scheme-search-keyword').on('keyup', function(){
        var searchKey = $('#discount-scheme-search-keyword').val();
        eb.ajax({
                type: 'POST',
                url: BASE_URL + '/api/discount-schemes/search',
                data: { searchKey: searchKey},
                success: function(respond) {
                    if (respond.status) {
                        $("#disc-schem-list-view").html(respond.html);
                    } else {
                        p_notification(respond.status, respond.msg);
                    }  
                }
        });
    });
    $('#cost-type-reset').on('click', function(){
        $('#discount-scheme-search-keyword').val('');
        eb.ajax({
                type: 'POST',
                url: BASE_URL + '/api/discount-schemes/search',
                data: { searchKey: null},
                success: function(respond) {
                    if (respond.status) {
                        $("#disc-schem-list-view").html(respond.html);
                    } else {
                        p_notification(respond.status, respond.msg);
                    }  
                }
        });
    });

    $('#showRuleAddModal').on('click', function() {

        $('#addConditionModal #tempRuleAddBody .addedRule').remove();
        $('#addConditionModal .comb-rule-save').removeClass('hidden');
        $(".sign").text(disSign);
        $addNewRuleRow.removeClass('hidden');
        if (Object.keys(addedSchemeRules).length > 0) {
                    // employees = taskEmployees[getCurrentTaskData($thisRow)['taskIncID']];
            schemeRules = {};
            $.each(addedSchemeRules, function(index, value) {
                var newTrID = index;
                var clonedRow = $($('#addConditionModal #tempRuleAddBody #rulePreSample').clone()).attr('id', newTrID).addClass('addedRule');
                $("input[name='minQty']", clonedRow).val(value.minQty);
                $("input[name='maxQty']", clonedRow).val(value.maxQty);
                $("input[name='discount']", clonedRow).val(value.discount);
                clonedRow.find("input[name='minQty']").prop('disabled', true);
                clonedRow.find("input[name='maxQty']").prop('disabled', true);
                clonedRow.find("input[name='discount']").prop('disabled', true);
                clonedRow.removeClass('hidden');
                clonedRow.insertBefore('#rulePreSample');
                schemeRules[newTrID] = new schemeRule(value.minQty, value.maxQty, value.discount);
                resetAddCombinationRuleRow();

            });
        }

        $('#addConditionModal').modal('show');
    });


    $('#tempRuleAddBody').on('click', 'button.delete-rule', function() {
        var deleteID = $(this).parents('tr').attr('id');
        $(this).parents('tr').remove();

        delete schemeRules[deleteID];
    });

    $ruleTable.on('click', 'button.add-rule', function(e) {
        e.preventDefault();

        var minQuantity = $("input[name='min-quantity']", $addNewRuleRow).val();
        var maxQuantity = $("input[name='max-quantity']", $addNewRuleRow).val();
        var discount = $("input[name='disc']", $addNewRuleRow).val();
        var sign = $(".sign", $addNewRuleRow).text();
        minValiFaildCount = 0;
        maxValiFaildCount = 0;

        if (minQuantity == '') {
            p_notification(false, eb.getMessage('ERR_MIN_QTY_CANNOT_EMPTY'));
            return false;
        }

        if (maxQuantity == '') {
            p_notification(false, eb.getMessage('ERR_MAX_QTY_CANNOT_EMPTY'));
            return false;
        }
        if (discount == '') {
            p_notification(false, eb.getMessage('ERR_DISCOUNT_CANNOT_EMPTY'));
            return false;
        }


        var cc = range(parseFloat(minQuantity), parseFloat(maxQuantity - 1));

        for (var i = 0; i < cc.length; i++) {

            if (Object.keys(schemeRules).length > 0) {
                $.each(schemeRules, function(index1, rule) {
                    if (parseFloat(rule['minQty']) <= parseFloat(cc[i])  && parseFloat(rule['maxQty']) > parseFloat(cc[i])) {
                        minValiFaildCount ++;
                    } else if (parseFloat(rule['minQty']) <= parseFloat(cc[i])  && parseFloat(rule['maxQty']) > parseFloat(cc[i])) {
                        maxValiFaildCount ++;
                    }
                });
            }
        }

        if (minValiFaildCount > 0) {
            p_notification(false, eb.getMessage('ERR_MIN_QTY_IN_ADDED_RANGE'));
            return false;
        } 

        if (maxValiFaildCount > 0) {
            p_notification(false, eb.getMessage('ERR_MAX_QTY_IN_ADDED_RANGE'));
            return false;
        }

        if (parseFloat(maxQuantity) < parseFloat(minQuantity)) {
            p_notification(false, eb.getMessage('ERR_MAX_QTY_SHOULD_GREATER'));
            return false;
        }


        if (minQuantity == 0) {
            p_notification(false, eb.getMessage('ERR_MIN_QTY_CANNOT_BE_ZERO'));
            return false;
        }


        var randNum = Math.floor((Math.random() * 10000) + 1);
        var rowId = 'rule'+'_'+randNum;

        if (validateDiscountAmount(discountType, $("input[name='disc']", $addNewRuleRow))) {
            var newTrID = rowId;
            var clonedRow = $($('#addConditionModal #tempRuleAddBody #rulePreSample').clone()).attr('id', newTrID).addClass('addedRule');
            $("input[name='minQty']", clonedRow).val(minQuantity);
            $("input[name='maxQty']", clonedRow).val(maxQuantity);
            $("input[name='discount']", clonedRow).val(discount);
            $(".temp-sign", clonedRow).text(sign);
            clonedRow.find("input[name='minQty']").prop('disabled', true);
            clonedRow.find("input[name='maxQty']").prop('disabled', true);
            clonedRow.find("input[name='discount']").prop('disabled', true);
            clonedRow.removeClass('hidden');
            clonedRow.insertBefore('#rulePreSample');
            schemeRules[newTrID] = new schemeRule(minQuantity, maxQuantity, discount);
            resetAddCombinationRuleRow();
        }

    });


    function range(a, b, step){
    var A = [];
    if(typeof a == 'number'){
        A[0] = a;
        step = step || 1;
        while(a+step <= b){
            A[A.length]= a+= step;
        }
    }
    return A;
}


    function resetAddCombinationRuleRow() {
        $("input[name='min-quantity']", $addNewRuleRow).val('');
        $("input[name='max-quantity']", $addNewRuleRow).val("");
        $("input[name='disc']", $addNewRuleRow).val("");
        // selectedProductID = "";
        // selectedProduct = "";
    }


    $('#addConditionModal').on('click', '.comb-rule-save , .comb-rule-update', function() {
        addedSchemeRules = {};
        $.each(schemeRules, function(index, value) {
            addedSchemeRules[index] = value;
        });

        $('#addConditionModal').modal('hide');
    });


    $('#addConditionModal').on('click', '.comb-rule-close', function() {
        $('#addConditionModal').modal('hide');
    });


    $("#discountType").on('change', function () {
        discountType = $("#discountType").val();

        if (discountType == '1') {
            disSign = $("#currency").val();
        } else if (discountType == '2') {
            disSign = '%';
        }
        addedSchemeRules = {};
    });


    function validateDiscountAmount(discountType, $discountAmount) {
       
        if (discountType == '2') {
            if ($discountAmount.val() > 99) {
                p_notification(false, eb.getMessage('ERR_PROMO_AMNT_PERC'));
                $discountAmount.val('0');
                return false;
            } else {
                return true;
            }
        } else {
            return true;
        }
    }

    $(document).on('click', '#disc-schem-list-view .view-condtions',function(event) {
    // $('#disc-schem-list-view .view-condtions').on('click', function() {
        var $thisRow = $(this).parents('tr');
        var disType = $thisRow.attr('data-discType') ;

        var dataSet = {
            schemeID: $thisRow.attr('data-discSchemId')
        };

        eb.ajax({
            type: 'POST',
            url: BASE_URL + '/api/discount-schemes/get-scheme-conditions',
            data: dataSet,
            success: function(respond) {
                console.log(respond.data['list']);

                if (respond.data['list'].length > 0) {
                    // employees = taskEmployees[getCurrentTaskData($thisRow)['taskIncID']];
                    $('#addConditionModal #tempRuleAddBody .addedRule').remove();
                    $('#addConditionModal .comb-rule-save').addClass('hidden');
                    $.each(respond.data['list'], function(index, value) {
                        var newTrID = index;

                        if (disType == 'percentage') {
                            var disAmount = parseFloat(value['discountPercentage']);
                        } else if (disType == 'value') {
                            var disAmount = parseFloat(value['discountValue']);

                        }

                        var clonedRow = $($('#addConditionModal #tempRuleAddBody #rulePreSample').clone()).attr('id', newTrID).addClass('addedRule');
                        $("input[name='minQty']", clonedRow).val(parseFloat(value.minQty));
                        $("input[name='maxQty']", clonedRow).val(parseFloat(value.maxQty));
                        $("input[name='discount']", clonedRow).val(disAmount);
                        clonedRow.find("input[name='minQty']").prop('disabled', true);
                        clonedRow.find("input[name='maxQty']").prop('disabled', true);
                        clonedRow.find("input[name='discount']").prop('disabled', true);
                        clonedRow.find(".delete-rule").prop('disabled', true);
                        clonedRow.removeClass('hidden');
                        clonedRow.insertBefore('#rulePreSample');
                        resetAddCombinationRuleRow();

                    });

                    $addNewRuleRow.addClass('hidden');
                }

                $('#addConditionModal').modal('show');


            }
        });


        console.log($thisRow.attr('data-discSchemId'));
    });

});