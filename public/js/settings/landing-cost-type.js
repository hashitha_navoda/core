$(document).ready(function(){
	loadDropDownFromDatabase('/accounts-api/search-active-accounts-for-dropdown', "", "", '#sales-acc-id');
	loadDropDownFromDatabase('/accounts-api/search-active-accounts-for-dropdown', "", "", '#inventory-acc-id');
	loadDropDownFromDatabase('/accounts-api/search-active-accounts-for-dropdown', "", "", '#cgs-acc-id');
	loadDropDownFromDatabase('/accounts-api/search-active-accounts-for-dropdown', "", "", '#adjusment-acc-id');


	var productSalesAccountID = $('#sales-acc-id').data('id');
    if(productSalesAccountID!=''){
    	var productSalesAccountName = $('#sales-acc-id').data('value');
    	$('#sales-acc-id').append("<option value='"+productSalesAccountID+"'>"+productSalesAccountName+"</option>")
    	$('#sales-acc-id').val(productSalesAccountID).selectpicker('refresh');
    }

    var productInventoryAccountID = $('#inventory-acc-id').data('id');
    if(productInventoryAccountID!=''){
    	var productInventoryAccountName = $('#inventory-acc-id').data('value');
    	$('#inventory-acc-id').append("<option value='"+productInventoryAccountID+"'>"+productInventoryAccountName+"</option>")
    	$('#inventory-acc-id').val(productInventoryAccountID).selectpicker('refresh');
    }

    var productCOGSAccountID = $('#cgs-acc-id').data('id');
    if(productCOGSAccountID!=''){
    	var productCOGSAccountName = $('#cgs-acc-id').data('value');
    	$('#cgs-acc-id').append("<option value='"+productCOGSAccountID+"'>"+productCOGSAccountName+"</option>")
    	$('#cgs-acc-id').val(productCOGSAccountID).selectpicker('refresh');
    }

    var productAdjusmentAccountID = $('#adjusment-acc-id').data('id');
    if(productAdjusmentAccountID!=''){
    	var productAdjusmentAccountName = $('#adjusment-acc-id').data('value');
    	$('#adjusment-acc-id').append("<option value='"+productAdjusmentAccountID+"'>"+productAdjusmentAccountName+"</option>")
    	$('#adjusment-acc-id').val(productAdjusmentAccountID).selectpicker('refresh');
    }

    $('#sales-acc-id').on('change', function (){
    	if ($(this).val() != 0 || $(this).val() != ''){
    		productSalesAccountID = $(this).val();
    	}
    });

    $('#inventory-acc-id').on('change', function (){
    	if ($(this).val() != 0 || $(this).val() != ''){
    		productInventoryAccountID = $(this).val();
    	}
    });

    $('#cgs-acc-id').on('change', function (){
    	if ($(this).val() != 0 || $(this).val() != ''){
    		productCOGSAccountID = $(this).val();
    	}
    });

    $('#adjusment-acc-id').on('change', function (){
    	if ($(this).val() != 0 || $(this).val() != ''){
    		productAdjusmentAccountID = $(this).val();
    	}
    });

    $('#save-cost-type').on('click', function () {
    	var costCode = $('.land-cost-type-code').val();
    	var costName = $('.land-cost-type-name').val();
    	if (costCode == '' || costCode == null) {
            p_notification(false, eb.getMessage('ERR_NO_COST_CODE'));
        } else if (costName == '' || costName == null) {
            p_notification(false, eb.getMessage('ERR_NO_COST_NAME'));
        } else if (productSalesAccountID == null || productSalesAccountID == 0) {
            p_notification(false, eb.getMessage('ERR_NO_SALE_ACC'));

        } else if (productInventoryAccountID == null || productInventoryAccountID == 0) {
            p_notification(false, eb.getMessage('ERR_NO_INVENT_ACC'));

        } else if (productCOGSAccountID == null || productCOGSAccountID == 0) {
            p_notification(false, eb.getMessage('ERR_NO_COGS_ACC'));

        } else if (productAdjusmentAccountID == null || productAdjusmentAccountID == 0) {
            p_notification(false, eb.getMessage('ERR_NO_ADJUS_ACC'));

    	} else {
    		var savedDataSet = {
    			costCode: costCode,
    			costName: costName,
    			productSalesAccountID: productSalesAccountID,
    			productInventoryAccountID: productInventoryAccountID,
    			productCOGSAccountID: productCOGSAccountID,
    			productAdjusmentAccountID: productAdjusmentAccountID
    		};

    		eb.ajax({
	                type: 'POST',
	                url: BASE_URL + '/api/settings/landing-cost-type/save',
	                data: savedDataSet,
	                success: function(respond) {
	                	p_notification(respond.status, respond.msg);
	                	if(respond.status) {
                            window.location.reload();
	                	}
	                }
	        });
    		
    	}

    });

    $('#cost-type-list').on('click', '.delete', function(e) {
        e.preventDefault();
        var thisRowID = $(this).parents('tr').attr('data-costtypeid');
        bootbox.confirm('Are you sure you want to delete this cost type ?', function(result) {
            if (result === true) {
                eb.ajax({
                    url: BASE_URL + '/api/settings/landing-cost-type/delete-cost-type',
                    method: 'post',
                    data: {
                        costTypeID: thisRowID,
                    },
                    dataType: 'json',
                    success: function(data) {
                        p_notification(data.status, data.msg);
                        if (data.status == true) {
                            window.location.reload();
                        }
                       
                    }
                });
            }
        });

    });

    $('#landing-cost-type-search-keyword').on('keyup', function(){
        var searchKey = $('#landing-cost-type-search-keyword').val();
        eb.ajax({
                type: 'POST',
                url: BASE_URL + '/api/settings/landing-cost-type/search',
                data: { searchKey: searchKey},
                success: function(respond) {
                    if (respond.status) {
                        $("#cost-type-list").html(respond.html);
                    } else {
                        p_notification(respond.status, respond.msg);
                    }  
                }
        });
    });
    $('#cost-type-reset').on('click', function(){
        $('#landing-cost-type-search-keyword').val('');
        eb.ajax({
                type: 'POST',
                url: BASE_URL + '/api/settings/landing-cost-type/search',
                data: { searchKey: null},
                success: function(respond) {
                    if (respond.status) {
                        $("#cost-type-list").html(respond.html);
                    } else {
                        p_notification(respond.status, respond.msg);
                    }  
                }
        });
    });
});