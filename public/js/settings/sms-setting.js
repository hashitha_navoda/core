$(document).ready(function() {
	if ($('#smsServiceStatus').val() == 0) {
		$('.smsSetting_div').addClass('hidden');
		$("#smsEnable").prop('checked',false);
	} else {
		$("#smsEnable").prop('checked',true);
		$('.smsSetting_div').removeClass('hidden');		
    }
    
	$('#smsEnable').change(function() {
        if($(this).is(":checked")) {
			$('.smsSetting_div').removeClass('hidden');		
        } else {
			$('.smsSetting_div').addClass('hidden');
        	eb.ajax({
                type: 'POST',
                url: BASE_URL + '/sms-settings-api/disableSmsConfiguration',
                data: {},
                success: function(respond) {
                    p_notification(respond.status, respond.msg);                
                    if(respond.status){
                    }                
                }
            });
        }
    });

    $('#save-sms-setting').on('click',function(e){
    	e.preventDefault();
        var formData = {
          serviceProvider: $("#serviceProvider").val(),
          username: $("#uName").val(),
          password: $("#pword").val(),
          alias: $("#alias").val(),
          telephone: $("#telephone").val(),
        };
    
        if (validateFormData(formData)) {
            eb.ajax({
                type: 'POST',
                url: BASE_URL + '/sms-settings-api/saveSmsConfiguration',
                data: formData,
                success: function (respond) {
                    p_notification(respond.status, respond.msg);                
                    if(respond.status){
                        window.setTimeout(function() {
                                location.reload();
                        }, 1500);                    
                    }                
                }
            });
        } 
    });

    function validateFormData(formData) {
        if (formData.serviceProvider == "") {
          p_notification(false, eb.getMessage("ERR_SERVICE_PROVIDER_EMPTY"));
          $("#serviceProvider").focus();
          return false;
        } else if (formData.username == "") {
          p_notification(
            false,
            eb.getMessage("ERR_USER_NAME_OF_SMS_NAME_EMPTY")
          );
          $("#uName").focus();
          return false;
        } else if (formData.password == "") {
          p_notification(false, eb.getMessage("ERR_PWD_OF_SMS_EMPTY"));
          $("#pword").focus();
          return false;
        } else if (formData.alias == "") {
          p_notification(false, eb.getMessage("ERR_ALIAS_EMPTY"));
          $("#alias").focus();
          return false;
        } else if (formData.telephone == "") {
          p_notification(false, eb.getMessage("ERR_TELE_EMPTY"));
          $("#telephone").focus();
          return false;
        } 
        return true;
    }
    
});