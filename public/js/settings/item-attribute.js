$(document).ready(function(){

	var valueData = [];
	var editFlag = false;
	var updatedAttrID = null;
	
	$('#item-attr-value').on('click', function(){
        $('#attrAddBody tr.clonedRow').remove();
		var newRow = $('.sample-body').clone()
            .removeClass('sample-body hidden').addClass('clonedRow');
		(newRow).appendTo('#attrAddBody');
        valueData = [];	
	});

	$('#attr-value-add-modal').on('click','.value-set-attr', function(e){
		e.preventDefault();
		var curntRow = $(this).parents('tr');
		var validateFlag = validateAttrValues(curntRow);
		if(validateFlag){
			$('.value-name', curntRow).attr('disabled', true);
			$('#itemAttributeUom', curntRow).attr('disabled', true);
			$(this).attr('disabled', true);
			$('.value-del-attr', curntRow).removeClass('hidden');
			curntRow.addClass('checked');
			$('.sample-body').clone().removeClass('sample-body hidden')
                .addClass('clonedRow').appendTo('#attrAddBody');		
		}
	});

	$('#attr-value-add-modal').on('click','.value-del-attr', function(e){
		e.preventDefault();
		$(this).parents('tr').remove();
	});

	$('.add-item-attr').on('click', function(e){
		e.preventDefault();
		var validate = validateGenaralDataSet();
		if(validate){
			var dataSet = {
				attrCode: $('#itemAttributeCode').val(),
				attrName: $('#itemAttributeName').val(),
				attrState: $('#itemAttributeState').val(),
				attrValue: valueData,
				updatedAttrID: updatedAttrID,
			}
			var url = '/api/settings/item-attribute/save';
			if(editFlag){
				var url = '/api/settings/item-attribute/update';
			}
			eb.ajax({
            url: url,
            method: 'post',
            data: dataSet,
            dataType: 'json',
            success: function(data) {
                p_notification(data.status, data.msg);
                if (data.status == true) {
                    window.setTimeout(function() {
                        location.reload();
                    }, 1000);
                    editFlag = false;
                    updatedAttrID = null;
                }
            }
        });

		}
	});

	$('.item-value-save').on('click', function(e){
		e.preventDefault();
        $.each($('#attrAddBody tr'), function(key, value){
			var subData = {};
			if($(this).hasClass('checked')){
				subData.description = $(this).find('.value-name').val();
				subData.uomID = $(this).find('#itemAttributeUom').val();
				valueData[key] = subData;
			}			

		});
		if(valueData.length == 0){
			p_notification(false, eb.getMessage('ERR_ITEM_AT_VALUE_NULL'));
			return false;
		} else {
			$('#addValueModal').modal('hide');
		}
		

	});

	$('#item-attr-search-result').on('click', '.view', function(e){
		e.preventDefault();
		var thisRowID = $(this).parents('tr').attr('data-itemattrid');
		$('#attr-value-add-modal .add-header').addClass('hidden');
		$('#attr-value-add-modal .view-header').removeClass('hidden');
		$('#addValueModal #attrAddBody tr.clonedRow').remove();

		eb.ajax({
            url: '/api/settings/item-attribute/viewSubItem',
            method: 'post',
            data: {id: thisRowID},
            dataType: 'json',
            success: function(data) {
                if (data.status == true) {
					$('#addValueModal').modal('show');
					$.each(data.data, function(key, value){
                        var newRow = $('#attr-value-add-modal tbody .sample-body')
                            .clone().removeClass('sample-body hidden')
                            .addClass('clonedRow').attr('id', 'tmp_'+key);
                        newRow.attr('id', 'tmp_'+key).appendTo('#attrAddBody');
                        var tmpID = '#tmp_'+key;
                        $(tmpID).attr('data-docid',value['itemAttributeValueID']);
                        $(tmpID).find('.value-name').val(value['itemAttributeValueDescription']).attr('disabled', true);
                        $(tmpID).find('#itemAttributeUom').val(value['uomID']).attr('disabled', true);
                        $(tmpID).find('.value-set-attr').attr('disabled', true)
                   });
                }
                else {
                	p_notification(false, data.msg);
                }
            }
        });

	});

   $('#item-attr-search-result').on('click', '.delete', function(e) {
        e.preventDefault();
        var thisRowID = $(this).parents('tr').attr('data-itemattrid');
        bootbox.confirm('Are you sure you want to delete this item attribute?', function(result) {
            if (result === true) {
                eb.ajax({
                    url: BASE_URL + '/api/settings/item-attribute/delete-item-attribute-by-item-attribute-id',
                    method: 'post',
                    data: {
                        ItemAttrID: thisRowID,
                        
                    },
                    dataType: 'json',
                    success: function(data) {
                        p_notification(data.status, data.msg);
                        if (data.status == true) {
                            window.location.reload();
                        }
                       
                    }
                });
            }
        });

    });

	$('#item-attr-search-result').on('click', '.status', function(e) {
        e.preventDefault();
        var msg;
        var status;
        var flag = true;
        var itemAttrID = $(this).parents('tr').attr('data-itemattrid');
        var currentDiv = $(this).contents();
        if ($(this).children().hasClass('fa-check-square-o')) {
            msg = 'Are you sure you want to Deactivate this item attribute';
            status = '0';
        }
        else if ($(this).children().hasClass('fa-square-o')) {
            msg = 'Are you sure you want to Activate this item attribute';
            status = '1';
        }
        activeInactiveType(itemAttrID, status, currentDiv, msg, flag);
    });

	$('#item-attr-search-result').on('click', '.edit', function(e){
	 	e.preventDefault();
	 	editFlag = true;
	 	$('.normal-save').addClass('hidden');
	 	$('.normal-update').removeClass('hidden');
	 	var itemAttrID = $(this).parents('tr').attr('data-itemattrid');
	 	updatedAttrID = itemAttrID;

	 	eb.ajax({
            url: BASE_URL + '/api/settings/item-attribute/loadDataToUpdate',
            method: 'post',
            data: {
                itemAttrID: itemAttrID,
            },
            dataType: 'json',
            success: function(data) {
            	if(data.status = 'true'){
            		$('#itemAttributeCode').val(data.data['itemAttributeCode']).attr('disabled', true);
            		$('#itemAttributeName').val(data.data['itemAttributeName']);
            		$('#itemAttributeState').val(data.data['itemAttributeState']);
            		if(itemAttrID == 1){
            			$('#itemAttributeState').attr('disabled',true);
            		}else{
            			$('#itemAttributeState').attr('disabled',false);
            		}
            	}
            }
        });
	});

	$('#item-attr-value-update').on('click', function(e){
        $('#attr-value-add-modal .add-header').removeClass('hidden');
        $('#attr-value-add-modal .view-header').addClass('hidden');
		e.preventDefault();
		eb.ajax({
            url: '/api/settings/item-attribute/viewSubItem',
            method: 'post',
            data: {id: updatedAttrID},
            dataType: 'json',
            success: function(data) {
				$('#addValueModal #attrAddBody tr.clonedRow').remove();
                if (data.status == true) {
                    $('#addValueModal').modal('show');
					$.each(data.data, function(key, value){
                   		var newRow = $('#attr-value-add-modal tbody .sample-body')
                   			.clone().removeClass('sample-body hidden')
                   			.addClass('clonedRow').attr('id', 'tmp_'+key);
						newRow.attr('id', 'tmp_'+key).appendTo('#attrAddBody');
						var tmpID = '#tmp_'+key;
						$(tmpID).addClass('checked');
						$(tmpID).find('.value-name').val(value['itemAttributeValueDescription']).attr('disabled', true);
                   		$(tmpID).find('#itemAttributeUom').val(value['uomID']).attr('disabled', true);
                   		$(tmpID).find('.value-set-attr').attr('disabled', true);
                        if(value['delFlag'] == 'canDel'){
                   		   $(tmpID).find('.value-del-attr').removeClass('hidden');
                        } else {
                            $(tmpID).find('.value-del-attr').addClass('hidden');
                        }
                   		
                   	});
                   	var newRow = $('#attr-value-add-modal tbody .sample-body')
                   		.clone().removeClass('sample-body hidden')
                   		.addClass('clonedRow');
                   	(newRow).appendTo('#attr-value-add-modal #attrAddBody');	
                }
                else {
                	var newRow = $('#attr-value-add-modal tbody .sample-body')
                   		.clone().removeClass('sample-body hidden')
                   		.addClass('clonedRow');
						(newRow).appendTo('#attrAddBody');	
                }
            }
        });



	});

	$('.reset').on('click', function(){
		$('#itemAttributeCode').val('').attr('disabled', false);
        $('#itemAttributeName').val('');
        $('#itemAttributeState').val('');
        $('.normal-save').removeClass('hidden');
	 	$('.normal-update').addClass('hidden');
	 	editFlag = false;
	 	updatedAttrID = null;
	});

    $('#item-attr-search').on('click', '.item-attr-search', function(e) {
        e.preventDefault();
        var searchKey = $('#item-attr-search-keyword').val();
        var formData = {
            searchKey: searchKey,
        }
        if (formData.searchKey) {
            searchValue(formData);
        } else {
            p_notification(false, eb.getMessage('ERR_NO_KEY_WORD'));
        }

    });
    $('#item-attr-search').on('click', '.item-attr-search-reset', function(e) {
        e.preventDefault();
        searchValue('');
        $('#item-attr-search-keyword').val('');
    });




	function validateAttrValues (currentRow)
	{
		if($('.value-name', currentRow).val() == null || $('.value-name', currentRow).val() == ''){
			p_notification(false, eb.getMessage('ERR_DESC_NOT_SET'));
			return false;
		} else if($('#itemAttributeUom', currentRow).val() == null || $('#itemAttributeUom', currentRow).val() == ''){
			p_notification(false, eb.getMessage('ERR_UOM_NOT_SET'));
			return false;
		} else {
            var vadtFlag = true;
            $.each($('#attr-value-add-modal #attrAddBody tr.checked'), function(key, value){
                if($(this).find('.value-name').val() == $('.value-name', currentRow).val()){
                    p_notification(false,eb.getMessage('ERR_SAME_VALUE_NAME'));
                    vadtFlag = false;
                    return false;
                }
            });
            return vadtFlag;
		} 

	}
	function validateGenaralDataSet()
	{
		if($('#itemAttributeCode').val() == null || $('#itemAttributeCode').val() == ''){
			p_notification(false, eb.getMessage('ERR_ATTR_CODE_NOT_SET'));
			return false;
		} else if($('#itemAttributeName').val() == null || $('#itemAttributeName').val() == ''){
			p_notification(false, eb.getMessage('ERR_ATTR_NAME_NOT_SET'));
			return false;
		} else {
			return true;
		}
	}

	 function activeInactiveType(itemAttrID, status, currentDiv, msg, flag) {
        bootbox.confirm(msg, function($result) {
            if ($result == true) {
                eb.ajax({
                    url: BASE_URL + '/api/settings/item-attribute/changeStatusID',
                    method: 'post',
                    dataType: 'json',
                    data: {
                        'itemAttrID': itemAttrID,
                        'status': status,
                    },
                    success: function(data) {
                        if (data.status) {
                            p_notification(data.status, data.msg);
                            if (currentDiv.hasClass('fa-square-o') && flag) {
                                currentDiv.addClass('fa-check-square-o');
                                currentDiv.removeClass('fa-square-o');
                            }
                            else {
                                currentDiv.addClass('fa-square-o');
                                currentDiv.removeClass('fa-check-square-o');
                            }
                        } else {
                            p_notification(data.status, data.msg);
                        }
                    }
                });
            }
        });
    }

    function searchValue(formData) {
        eb.ajax({
            url: BASE_URL + '/api/settings/item-attribute/get-search-value',
            method: 'post',
            data: formData,
            dataType: 'json',
            success: function(data) {
                if (data.status == true) {
                    if (data.status) {
                        $("#item-attr-search-result").html(data.html);
                    } else {
                        p_notification(data.status, data.msg);
                    }
                }
            }
        });
    }



});