
$(function() {
    var wizardLength = $('#wizard-content').length ? $('#wizard-content').length : 0;
    if (wizardLength == 0) {
        $('th').removeAttr('hidden');
        $('td').removeAttr('hidden');
    }
    $("#create-uom-form").submit(function(e) {

        var form_data = {
            uomName: $("[name='uomName']", this).val(),
            uomAbbr: $("[name='uomAbbr']", this).val(),
            uomDecimalPlace: $("[name='uomDecimalPlace']", this).val(),
            uomState: $("[name='uomState']", this).val(),
            wizardLength: wizardLength
        };

        var submitted_btn = $("[type='submit']:visible", $(this));
        var update = submitted_btn.hasClass('update');

        if (form_data.uomName.trim() == "" || form_data.uomAbbr.trim() == "") {
            p_notification(false, eb.getMessage('ERR_CATG'));
            return false;
        }

        var URL = (update) ? 'update' : 'add';
        var verb = (update) ? 'updated' : 'added';

        if (update) {
            form_data = $.extend(form_data, {uomID: $("[name='uomID']", this).val()})
        }

        eb.ajax({
            url: '/api/settings/uom/' + URL + '/' + getCurrPage(),
            method: 'post',
            data: form_data,
            dataType: 'json',
            success: function(data) {
                if (data.status == true) {
                    $("#uom-list").html(data.html);
                    removeHiddenAttr(wizardLength);
                    p_notification(true, data.msg);
                    $("button.reset", ".uom-form-container").click();

                } else {
                    p_notification(false, data.msg);
                }
            }
        });

        e.stopPropagation();
        return false;
    });

    $("form.uom-search").submit(function(e) {
        var form_data = {
            keyword: $("#uom-search-keyword", this).val()
        };

        eb.ajax({
            url: '/api/settings/uom/search',
            method: 'post',
            data: form_data,
            dataType: 'json',
            success: function(data) {
                if (data.status == true) {
                    $("#uom-list").html(data.html);
                    removeHiddenAttr(wizardLength);
                } else {

                }
            }
        });

        e.stopPropagation();
        return false;
    });

    $("form.uom-search button.reset").click(function(e) {
        $("#uom-search-keyword").val('');
        $("form.uom-search").submit();
    });

    $("#uom-list").on('click', 'a.edit', function(e) {
        var uomID = $(this).parents('tr').data('uomrow');

        $(".uom-form-container").addClass('update');
        $("html, body").animate({scrollTop: $(".uom-form-container").offset().top - 80});

        eb.ajax({
            url: '/api/settings/uom/get',
            method: 'post',
            data: {uomID: uomID},
            dataType: 'json',
            success: function(data) {
                if (data.status == true) {
                    var uomData = data.data;
                    var $editForm = $("#create-uom-form");
                    $("[name='uomName']", $editForm).val(uomData.uomName).focus();
                    $("[name='uomAbbr']", $editForm).val(uomData.uomAbbr);
                    $("[name='uomDecimalPlace']", $editForm).val(uomData.uomDecimalPlace);
                    $("[name='uomState']", $editForm).val(uomData.uomState);
                    $("[name='uomID']", $editForm).val(uomData.uomID);
                } else {

                }
            }
        });

        e.stopPropagation();
        return false;
    })

    $(".button-set.update button.reset").click(function() {
        $(".uom-form-container").removeClass('update');
    });

    $("#uom-list").on('click', 'a.statechange', function(e) {
        var uomID = $(this).parents('tr').data('uomrow');
        var state = !$('.glyphicon', this).hasClass('glyphicon-check');
        var state_msg = (state) ? activate : deactivate;

        bootbox.confirm(msgPartOne + state_msg + msgPartTwo, function(result) {
            if (result === true) {
                eb.ajax({
                    url: '/api/settings/uom/change-state/' + getCurrPage(),
                    method: 'post',
                    data: {uomID: uomID, uomState: +state},
                    dataType: 'json',
                    success: function(data) {
                        if (data.status == true) {
                            $("#uom-list").html(data.html);
                            removeHiddenAttr(wizardLength);
                            p_notification(true, data.msg);
                        } else {
                            p_notification(false, data.msg);
                        }
                    }
                });
            }
        });

        e.stopPropagation();
        return false;
    });

    $("#uom-list").on('click', 'a.delete', function(e) {
        var uomID = $(this).parents('tr').data('uomrow');

        bootbox.confirm('Are you sure you want to delete this Unit of Measure?', function(result) {
            if (result === true) {
                eb.ajax({
                    url: '/api/settings/uom/delete/' + getCurrPage(),
                    method: 'post',
                    data: {uomID: uomID},
                    dataType: 'json',
                    success: function(data) {
                        if (data.status == true) {
                            $("#uom-list").html(data.html);
                            removeHiddenAttr(wizardLength);
                            p_notification(true, data.msg);
                        } else {
                            p_notification(false, data.msg);
                        }
                    }
                });
            }
        });

        e.stopPropagation();
        return false;
    });

    function removeHiddenAttr(wizardLength) {
        if (wizardLength == 0) {
            $('th').removeAttr('hidden');
            $('td').removeAttr('hidden');
        }
    }

    //mobile view large search responsive issue
    $(window).bind('ready resize', function() {
        if ($(window).width() < 480) {
            $('#uom-search-keyword').css('margin-bottom', '15px');
            $('#uom-search-keyword').parent().removeClass('input-group');
            $('#uom-search-keyword').next().removeClass('input-group-btn');
        } else {
            $('#uom-search-keyword').parent().addClass('input-group');
            $('#uom-search-keyword').next().addClass('input-group-btn');
            $('#uom-search-keyword').css('margin-bottom', '0');
        }
    });

});