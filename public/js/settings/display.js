$(document).ready(function() {
    var attributeData = {};
    var templateItemAttrData = {};
    $('.selectpicker').selectpicker();

    var url2 = BASE_URL + '/companyAPI/displayAdd';
    $('#create-display-button').on('click', function(e) {
        $("input[name='costingMethod']").attr('disabled',false);
        e.preventDefault();
        var param = $('#create-display-form').serialize();
        var submitForm = eb.post(url2, param);
        submitForm.done(function(data) {
            p_notification(data.status, data.msg);
    	    $("input[name='costingMethod']").attr('disabled',true);
        });
    });

    /**
     * @author Malitta Nanayakkara <malitta@thinkcube.com>
     * Template related methods
     */

    $("a.create-template").on('click', function(e) {

        var $thisParentTr = $(this).parents('.form-group').find('table.templates thead:first tr');

        bootbox.prompt(addTempleteMassage, function(templateName) {
            if (templateName !== null && templateName.trim() != '') {

                var documentTypeID = $thisParentTr.data('documenttypeid');
                var documentSizeID = $thisParentTr.data('documentsizeid');

                eb.ajax({
                    type: 'POST',
                    url: BASE_URL + '/api/template/create',
                    data: {
                        templateName: templateName,
                        documentTypeID: documentTypeID,
                        documentSizeID: documentSizeID,
                    },
                    success: function(respond) {
                        if (respond.status == true) {
                            $('#templateName').val(respond.data.templateName);
                            $("#templateID").val(respond.data.templateID);
                            
                            if (respond.data.documentTypeName != "Sales Invoice" && respond.data.documentTypeName != "Sales Quotation") {
                                $("div.invoice_template_options").hide();
                            } else {
                                if (respond.data.documentTypeName == "Sales Quotation") {
                                    $("label.dln_group").hide();
                                } else {
                                   $("label.dln_group").show(); 
                                }   
                                $("div.invoice_template_options").show();
                            }
                            
                            var blackListedAdvancedTemplates = ["POS (pay only)", "POS (invoice only)", "POS Printout", "POS Credit Note"];
                            if (blackListedAdvancedTemplates.indexOf(respond.data.documentTypeName) == -1) {
                                $('#advance-template-div').hide();
                                $("div.pos_template_options").hide();
                            } else {
                                $("div.pos_template_options").show();
                                $('#advance-template-container').hide();
                                $('#advance-template-div').hide();
                            }
                            resetAdvanceTemplateOptions();
                            // add new row
                            var $newTr = $thisParentTr.parent(0).next().find('tr:first').clone();
                            $newTr.data('templateid', respond.data.templateID);
                            $newTr.attr('data-templateid', respond.data.templateID);
                            $newTr.find('td:first').text(templateName);
                            $newTr.find('a.default').removeClass('fa-dot-circle-o').addClass('fa-circle-o');
                            $thisParentTr.parent(0).next().append($newTr);
                            if (respond.data.templateContent == null) {
                                respond.data.templateContent = "";
                            }
//                            $('div.paper-edit .editable').code(respond.data.templateContent);
                            tinyMCE.activeEditor.setContent((respond.data.templateContent).trim());

                            $('div.paper-edit .note-editable .noneditable').tooltip({
                                title: "Content / data area of the document. This part cannot be edited.",
                                placement: "auto"
                            });

                            $('#edit-template').modal('show');
                            setTimeout(function() { // added delay for content to load
                                $('#edit-template .modal-body').scrollTop(0);
                            }, 100);

                            getDocumentDropdown(respond.data.documentTypeID);

                        }

                        p_notification(respond.status, respond.msg);
                    }
                });

            } else if (templateName !== null) {
                p_notification(false, eb.getMessage('ERR_DISPLAY_TEMP'));
            }
        });

        e.preventDefault();
        return false;
    });

    $("a.edit-pos-template").on('click', function(e) {
        eb.ajax({
            type: 'POST',
            url: BASE_URL + '/api/template/get-pos-template-details',
            data: {},
            success: function(respond) {
                if (respond.status) {
                    $('.model-body-placeholder').empty();
                    respond.data.list.forEach(element => {
                        var checkStatus = element.posTemplateDetailsIsSelected == "1"?'checked':'';
                        var defaultSelectedStatus = element.posTemplateDetailsByDefaultSelected == "1"?'disabled':'';
                        
                        modelBody = "<div class='row'><div class='checkbox mt-1'>";
                        modelBody += "<label><input class='template-details' name='templateDetails[]' type='checkbox' value="+element.posTemplateDetailsID+" "+checkStatus+" "+defaultSelectedStatus+"> "+element.posTemplateDetailsAttribute+"</label>";
                        
                        if(element.posTemplateDetailsID == 29){
                            if(element.posTemplateDetailsIsSelected == "1"){
                                $('#additionalText').empty();
                                modelBody += `<div class="row" id='additionalTextDiv'>
                                <input type="text" placeholder="" autofocus="true" class="form-control" id="additionalText" value="`+respond.data.additionalTextForPos+`">
                                </div>`;
                            }else{
                                modelBody += `<div class="row" id='additionalTextDiv'></div>`;
                            }
                        }
                        modelBody += '</div></div>';

                        $('.model-body-placeholder').append(modelBody);
                        $('#edit-pos-template').modal('show');
                    });
                } else {
                    p_notification(respond.status, respond.msg);
                }
            }
        });

        e.preventDefault();
        return false;
    });

   

    $("#btn-save").on('click', function(){
        var val = [];
        var additionalText = "";
        var additionalTextValidation = true;

        $('.template-details').each(function(i){
          val[i] = {
              id : $(this).val(),
              value : $(this).is(":checked") ? 1 : 0
            };

            if($(this).val() == 29 && $(this).is(":checked")){
                additionalText = $('#additionalText').val();

                if(additionalText.length <= 0){
                    additionalTextValidation = false;
                }
            }
        });

        if(additionalTextValidation){
            eb.ajax({
                type: 'POST',
                url: BASE_URL + '/api/template/update-template-details',
                data: {
                    templateDetails : val,
                    additionalText : additionalText
                },
                success: function(respond) {
                    if (respond.status) {
                        p_notification(true, eb.getMessage('SUCC_UPDATE_POS_TEMPLATE'));
                        $('#edit-pos-template').modal('hide');
                    } else {
                        p_notification(respond.status, respond.msg);
                    }
                }
            });
        }else{
            p_notification(false, eb.getMessage('ERR_ADDITIONAL_TEXT_VALIDATION'));
        }

    });

    $(".templates-container").on('click', 'a.default', function(e) {

        if ($(this).hasClass('fa-dot-circle-o')) {
            e.preventDefault();
            return false;
        }

        var documentTypeName = $(this).parents('table').data('documenttype');

        var currentDefaultTemplate = $(this).parents('table').find('a.default');
        var newDefaultTemplate = $(this);
        var templateID = $(this).parents('tr').data('templateid');

        eb.ajax({
            type: 'POST',
            url: BASE_URL + '/api/template/change-default',
            data: {templateID: templateID},
            success: function(respond) {
                if (respond.status) {
                    currentDefaultTemplate.removeClass('fa-dot-circle-o').addClass('fa-circle-o');
                    newDefaultTemplate.addClass('fa-dot-circle-o');
                    p_notification(true, eb.getMessage('SUCC_DISPLAY_TEMPCHANGE', documentTypeName));
                } else {
                    p_notification(respond.status, respond.msg);
                }
            }
        });

        e.preventDefault();
        return false;
    });

    $(".templates-container").on('click', 'a.edit', function(e) {

        var templateID = $(this).parents('tr').data('templateid');
        eb.ajax({
            type: 'POST',
            url: BASE_URL + '/api/template/get',
            data: {
                templateID: templateID,
            },
            success: function(respond) {
                if (respond.status == true) {
                    // show template options only for sales invoice
                    // TEMP FIX

                    resetAdvanceTemplateOptions();
                    if (respond.data.documentTypeName != "Sales Invoice" && respond.data.documentTypeName != "Sales Quotation") {
                        $("div.invoice_template_options").hide();
                    } else {
                        if (respond.data.documentTypeName == "Sales Quotation") {
                            $("label.dln_group").hide();
                        } else {
                           $("label.dln_group").show(); 
                        }
                        var categorize_products = (respond.data.templateOptions.categorize_products !== undefined && respond.data.templateOptions.categorize_products == "true"); // yes, it's returned as a string
                        var product_table = respond.data.templateOptions.product_table;
                        $("input.templateOptions[name='categorize_products']").prop('checked', categorize_products);
                        $("#edit-product-table-columns").data('product_table', product_table);
                        $("div.invoice_template_options").show();
                    }

                    var blackListedAdvancedTemplates = ["POS (pay only)", "POS (invoice only)", "POS Printout", "POS Credit Note"];

                    if (blackListedAdvancedTemplates.indexOf(respond.data.documentTypeName) == -1) {
                        // for advanced template options
                        if (Object.keys(respond.data.templateAdvancedOptions).length > 0) {
                            updateAdvanceTemplateOptions(respond.data.templateAdvancedOptions);
                        }

                        $('#advance-template-container').show();

                        if (respond.data.isAdvanceTemplateEnabled == 1) {
                            $('#adv-tpl-settings-chk').prop('checked', true);
                            $('#adv-tpl-settings-chk').trigger('change');
                        } else {
                            $('#adv-tpl-settings-chk').prop('checked', false);
                            $('#adv-tpl-settings-chk').trigger('change');
                        }
                    } else {
                        $('#advance-template-container').hide();
                        $('#advance-template-div').hide();
                    }

                    if(respond.data.documentTypeName == "Purchase Order") {
                        $("#signature-vedify-div").show();
                        if (respond.data.isEnableSignatureVerify == 1) {
                            $('#sig-veri-settings-chk').prop('checked', true);
                            $('#sig-veri-settings-chk').trigger('change');
                            $("#signaturePicker").selectpicker('val', respond.data.selectedSignatures);
                            $("#signaturePicker").selectpicker('refresh');
                        } else {
                            $('#sig-veri-settings-chk').prop('checked', false);
                            $('#sig-veri-settings-chk').trigger('change');
                        }
                    } else {
                        $("#signature-vedify-div").hide();

                    }

                    // show remove product details option only for POS related templates
                    // pos related document type ids: 8, 24, 25, 26
                    var posDocTypes = ["8", "24", "25", "26"];
                    if (posDocTypes.indexOf(respond.data.documentTypeID) == -1) {
                        $("div.pos_template_options").hide();
                    } else {
                        var remove_product_details = (respond.data.templateOptions.remove_product_details !== undefined && respond.data.templateOptions.remove_product_details == "true"); // yes, it's returned as a string
                        $("input.templateOptions[name='remove_product_details']").prop('checked', remove_product_details);
                        $("div.pos_template_options").show();
                    }

                    if (respond.data.documentTypeName != "Sales Invoice" && respond.data.documentTypeName != 'POS Printout') {
                    	$("select[name='documentSizeID']").find("option:contains('POS')").hide();
                	} else {
                		$("select[name='documentSizeID']").find("option:contains('POS')").show();
                	}

                    templateItemAttrData = respond.data.templateItemAtrribute;
                    $('#templateName').val(respond.data.templateName);
                    $("select[name='documentSizeID']").val(respond.data.documentSizeID).change();
                    $("select[name='documentSizeID']").val(respond.data.documentSizeID).change();
                    $("#templateID").val(respond.data.templateID);
                    tinyMCE.activeEditor.setContent((respond.data.templateContent).trim());

                    $('div.paper-edit .note-editable .noneditable').tooltip({
                        title: "Content / data area of the document. This part cannot be edited.",
                        placement: "auto"
                    });

                    $('#edit-template').modal('show');
                    setTimeout(function() { // added delay for content to load
                        $('#edit-template .modal-body').scrollTop(0);
                    }, 100);

                    getDocumentDropdown(respond.data.documentTypeID);

                }
            }
        });

        e.preventDefault();
        return false;
    });

    function getDocumentDropdown(documentTypeID) {
        $(".detailFieldsTmp").hide(0);
        $(".detailFieldsTmp[data-doctypeid='" + documentTypeID + "']").show(0);
    }

//    $("table.templates a.preview").click(function(e) {
//
//        $('#edit-template').modal('show');
//
//        e.preventDefault();
//        return false;
//    });

    $(".templates-container").on('click', 'a.delete', function(e) {

        if ($(this).parents('table').find('tr').length <= 1) {
            p_notification(false, eb.getMessage('ERR_DISPLAY_TEMPDELE'));
            e.preventDefault();
            return false;
        }

        if ($(this).parents('tr').find('a.fa-dot-circle-o').length) {
            p_notification(false, eb.getMessage('ERR_DISPLAY_DEFTEMP'));
            e.preventDefault();
            return false;
        }

        var $thisParentTr = $(this).parents('tr');

        bootbox.confirm("Are you sure you want to delete this template?", function(result) {
            if (result == true) {

                var templateID = $thisParentTr.data('templateid');

                eb.ajax({
                    type: 'POST',
                    url: BASE_URL + '/api/template/delete',
                    showLoader: true,
                    data: {templateID: templateID},
                    success: function(respond) {
                        p_notification(respond.status, respond.msg);
                        $thisParentTr.remove();
                    }
                });
            }
        });

        e.preventDefault();
        return false;
    });



    $('form#saveTemplate').submit(function(e) {

        var data = {};
        data.templateID = $("#templateID").val();
        data.templateName = ($("#templateName").val()).trim();
        data.templateContent = tinyMCE.activeEditor.getContent();
        data.documentSizeID = $("select[name='documentSizeID']").val();

        var templateOptions = {};
        if ($("input.templateOptions[name='categorize_products']").is(':visible')) {
            templateOptions.categorize_products = $("input.templateOptions[name='categorize_products']").is(':checked');
            templateOptions.product_table = $("#edit-product-table-columns").data('product_table');
        }

        if ($("input.templateOptions[name='categorize_products_by_dln']").is(':visible')) {
            templateOptions.categorize_products_by_dln = $("input.templateOptions[name='categorize_products_by_dln']").is(':checked');
            templateOptions.product_table = $("#edit-product-table-columns").data('product_table');
        }

        if ($("input.templateOptions[name='remove_product_details']").is(':visible')) {
            templateOptions.remove_product_details = $("input.templateOptions[name='remove_product_details']").is(':checked');
        }

        if ($("input.templateOptions[name='categorize_products_by_dln']").is(':checked') && $("input.templateOptions[name='categorize_products']").is(':checked')) {
            p_notification(false, eb.getMessage('ERR_DISPLAY_TEMPCHANGE_CATEGORIZE'));
            return false;
        }
        var isEnableSignatureVerify = 0;
        var selectedSignatures = [];
        if ($("#sig-veri-settings-chk").is(':checked')) {
            isEnableSignatureVerify = 1;
            if ($("#signaturePicker").val() == "" || $("#signaturePicker").val() == null) {
                p_notification(false, eb.getMessage('ERR_SELECT_ATLEAST_ONE_SIG'));
                return false;
            }

            if ($("#signaturePicker").val().length > 3) {
                p_notification(false, eb.getMessage('ERR_SELECT_SIG_NUM'));
                return false;
            } else {
                selectedSignatures = $("#signaturePicker").val();
            }
        }

        data.templateOptions = templateOptions;
        // advanced template options
        data.isAdvanceTemplateEnabled = ($('#adv-tpl-settings-chk').is(':checked')) ? true : false;
        data.templateAdvancedOptions = getAdvanceTemplateOptions();
        data.attributeData = attributeData;
        data.isEnableSignatureVerify = isEnableSignatureVerify;
        data.selectedSignatures = selectedSignatures;
        eb.ajax({
            type: 'POST',
            url: BASE_URL + '/api/template/update',
            data: data,
            success: function(respond) {
                p_notification(respond.status, respond.msg);
                $('.modal').modal('hide');
                var $tplrow = $("table.templates tr[data-templateid='" + data.templateID + "'] td:first");
                var $headerTr = $tplrow.parents('.form-group').find('table.templates thead:first tr');
                var $container = $tplrow.parents('.form-group').find('table.templates').parent();
                var docTypeId = $headerTr.data('documenttypeid');

                $container.html(respond.data.list[docTypeId].html);
            }
        });

        e.preventDefault();
        return false;

    });

	$("select[name='documentSizeID']").change(function(){
		var sizeName = $(this).find('option:selected').text();

		var height = 1100;

		if(sizeName.indexOf('Landscape') > 0){
			height = 550;
		}

        $('div.paper-edit').css('height', height);
        $("#mce_0_ifr").css('height', height - 72);
	});

    tinymce.init({
        selector: 'div.paper-edit .editable',
        statusbar: false,
        content_css: ["/assets/bootstrap/css/bootstrap.min.css", "/css/template-editor.css"],
        plugins: [
            "advlist autolink lists link image charmap print preview anchor",
            "searchreplace visualblocks code fullscreen",
            "insertdatetime media table contextmenu paste" //autoresize
        ],
        height: 1048,
        extended_valid_elements: "noneditabletable",
        custom_elements: "noneditabletable",
        inline_styles : true,
        valid_children : "+body[style]",
//        autoresize_max_height: 1028,
        toolbar: "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link"
    });

//    $('div.paper-edit .editable').summernote({
//        toolbar: [
//            ['styles', ['style']],
//            ['ur', ['undo', 'redo']],
//            ['formatting', ['bold', 'italic', 'underline', 'clear']],
//            ['fontsize', ['fontsize']],
//            ['color', ['color']],
//            ['para', ['ul', 'ol', 'paragraph']],
//            ['height', ['height']],
//            ['table', ['table']],
//            ['insert', ['link']],
//            ['ops', ['codeview']]
//        ]
//    });

    $('.detailFieldsTmp').on('change', function() {
        var thisVal = $(this).val();

        if (thisVal != "" && thisVal != null) {
            tinymce.activeEditor.execCommand('mceInsertContent', false, '<abbr spellcheck="false" contenteditable="false">' + thisVal + '</abbr>&nbsp;', true);

            // reset dropdown
            $('.detailFieldsTmp').val(0).change();
        }
    });

//    $('#detailFields').on('click', 'option:not(:disabled)', function() {
//        var thisVal = $(this).val();
//
//        setTimeout(function() {
//            $(".note-editable").focus();
//            pasteHtmlAtCaret('sdasdasd', true);
//        });
//    });

//    $('.tempclick').on('click', function() {
//        $(".note-editable").focus();
//        pasteHtmlAtCaret('sdasdasd', true);
//    });

    // select/highlight entire placeholder on click
    $(".note-editable").on('click', 'abbr', function(e) {
        var el = this;
        requestAnimationFrame(function() {
            selectElementContents(el);
        });
    });

    // select/highlight entire placeholder on caret enter
    $(".note-editable").keypress(function(e) {
        var key = (e.keyCode || e.which);
        var arrowKeys = [37, 38, 39, 40];

        if ($(this).selectedText().obj.tagName == 'ABBR') {

            var thisElem = $(this);

            if (arrowKeys.indexOf(key) > -1) {
                selectElementContents(thisElem.selectedText().obj);
            } else {
                var char = String.fromCharCode(key);
                var regx = /^[a-z0-9]+$/i;
                if ((regx.test(char) || key == 13) || (key == 8 || key == 46)) {
                    ($(this).selectedText().obj).remove();
                }
            }
        } else {
            if ((key == 8 || key == 46)) {
//                var thisElem = $(this);
//                console.log(thisElem.selectedText());
            }
        }
    });

    var $edit_products_modal = $('#edit-products-columns');
    var $edit_products_button = $("#edit-product-table-columns");

    $edit_products_button.click(function() {

        var product_table_data = $edit_products_button.data('product_table');

        if (product_table_data) {
            product_table_data.columns = _.indexBy(_.sortBy(product_table_data.columns, 'order'), 'name');
            product_table_data.rows = _.indexBy(_.sortBy(product_table_data.rows, 'order'), 'name');
        }
        getInvoiceProductsTableEditor(product_table_data);

    });

    $("button[type='submit']", $edit_products_modal).click(function() {

        var $products_columns = $('table.products-table-columns');
        var $products_footer_rows = $('table.products-table-footer-rows');

        $edit_products_modal.modal('hide');

        var product_table_data = {
            hide_table_borders: $("input[name='hide_product_table_borders']", $edit_products_modal).prop('checked'),
            hide_table_header: $("input[name='hide_product_table_headers']", $edit_products_modal).prop('checked'),
        };

        var columns = [];
        $('tbody tr', $products_columns).each(function(index, $column) {
            var $tr = $(this);

            columns.push({
                name: $tr.data('item'),
                show: $("input[type='checkbox']", $tr).prop('checked'),
                width: $("input[type='number']", $tr).val(),
                order: index
            });
        });

        var rows = [];
        $('tbody tr', $products_footer_rows).each(function(index, $row) {
            var $tr = $(this);

            rows.push({
                name: $tr.data('item'),
                show: $("input[type='checkbox']", $tr).prop('checked'),
                order: index
            });
        });

        product_table_data.columns = columns;
        product_table_data.rows = rows;
        $edit_products_button.data('product_table', product_table_data);
        
    });

    function getInvoiceProductsTableEditor(product_table_data) {

        var $products_columns = $('table.products-table-columns');
        var $products_footer_rows = $('table.products-table-footer-rows');

        $("input[name='hide_product_table_headers']", $edit_products_modal).prop('checked', (product_table_data.hide_table_header === true || product_table_data.hide_table_header == "true"));
        $("input[name='hide_product_table_borders']", $edit_products_modal).prop('checked', (product_table_data.hide_table_borders === true || product_table_data.hide_table_borders == "true"));

        $edit_products_modal.modal('show');

        $('tbody tr', $products_columns).removeClass('added');
        $('tbody tr', $products_footer_rows).removeClass('added');

        $.each(product_table_data.columns, function(index, column) {
            var $last_added_item = $('tbody tr.added:last', $products_columns);
            var $target_item = $("tbody tr[data-item='" + index + "']", $products_columns);

            if ($last_added_item.length) {
                $last_added_item.after($target_item);
            }

            $("input[type='checkbox']", $target_item).prop('checked', (column.show === true || column.show == "true")).trigger('change');

            if (column.show === true || column.show == "true") {
                $target_item.addClass('added');
                setTimeout(function() {
                    $("input[type='number']", $target_item).val(column.width);
                }, 100)
            }
        });

        $.each(product_table_data.rows, function(index, row) {
            var $last_added_item = $('tbody tr.added:last', $products_footer_rows);
            var $target_item = $("tbody tr[data-item='" + index + "']", $products_footer_rows);

            if ($last_added_item.length) {
                $last_added_item.after($target_item);
            }

            $("input[type='checkbox']", $target_item).prop('checked', (row.show === true || row.show == "true")).trigger('change');
            if (row.show === true || row.show == "true") {
                $target_item.addClass('added');
            }
        });

        // to help maintain table row with when being sorted
        var fixHelper = function(e, ui) {
            ui.children().each(function() {
                $(this).width($(this).width());
            });
            return ui;
        };

        // for both products columns editor table and footer row editor table, do
        $.each([$products_columns, $products_footer_rows], function(index, $table) {

            $("input[type='checkbox']", $table).unbind('click').click(function(e) {
                e.stopPropagation();
            }).unbind('change').change(function(e) {
                var checked = $(this).is(':checked');
                var $parentTr = $(this).parents('tr');

                $("input[type='number']", $parentTr).val('').prop('disabled', !checked);

                if (!checked) {
                    if ($parentTr[0]['dataset']['item'] == "attribute_01" || $parentTr[0]['dataset']['item'] == "attribute_02" || $parentTr[0]['dataset']['item'] == "attribute_03") {
                        $('.attribute', $parentTr).attr('disabled', true);
                        $('.attribute', $parentTr).val("0");
                        delete attributeData[$parentTr[0]['dataset']['item']];
                    } else {
                        $("td.small", $parentTr).addClass('text-muted').css('text-decoration', 'line-through');
                    }
                    $("span.fa-bars", $parentTr).parents('td').addClass('text-muted');
                } else {
                    if ($parentTr[0]['dataset']['item'] == "attribute_01" || $parentTr[0]['dataset']['item'] == "attribute_02" || $parentTr[0]['dataset']['item'] == "attribute_03") {
                        $('.attribute', $parentTr).attr('disabled', false);
                        if (!$.isEmptyObject(templateItemAttrData)) {
                            if ($parentTr[0]['dataset']['item'] == "attribute_01" && templateItemAttrData['attributeOneID'] != null) {
                                $('.attribute', $parentTr).val(templateItemAttrData['attributeOneID']);
                                attributeData[$parentTr[0]['dataset']['item']] = templateItemAttrData['attributeOneID'];
                            } else if ($parentTr[0]['dataset']['item'] == "attribute_02" && templateItemAttrData['attributeTwoID'] != null) {
                                $('.attribute', $parentTr).val(templateItemAttrData['attributeTwoID']);
                                attributeData[$parentTr[0]['dataset']['item']] = templateItemAttrData['attributeTwoID'];
                            } else if ($parentTr[0]['dataset']['item'] == "attribute_03" && templateItemAttrData['attributeThreeID'] != null) {
                                $('.attribute', $parentTr).val(templateItemAttrData['attributeThreeID']);
                                attributeData[$parentTr[0]['dataset']['item']] = templateItemAttrData['attributeThreeID'];
                            }
                        } 
                    } else {
                        $("td.small", $parentTr).removeClass('text-muted').css('text-decoration', 'none');
                    }
                    $("span.fa-bars", $parentTr).parents('td').removeClass('text-muted');
                }

                // products table columns sort
                $('tbody', $table).sortable({
                    handle: ".sorter-handle:not(.text-muted)",
                    helper: fixHelper,
                    axis: 'y',
                    forcePlaceholderSize: true,
                    placeholder: "tr"
                }).disableSelection();

                $('.attribute', $parentTr).change(function(event) {
                    if (checked) {
                        attributeData[$parentTr[0]['dataset']['item']] = $(this).val();
                    }
                });


            }).trigger('change');

            // if clicked outside of the input checkbox, still count as a click on the input
            $("td.check", $table).unbind('click').click(function() {
                var $check = $("input[type='checkbox']", $(this));
                $check.prop('checked', !$check.prop('checked')).trigger('change');
            });

        });

    }

    function updateAdvanceTemplateOptions(options) {
        if (options.isCustomPage) {
            $('input:radio[name="page-size"][value=Custom]').prop("checked", true);
        } else {
            $('input:radio[name="page-size"][value=Standard]').prop("checked", true);
        }
        $('input:radio[name="page-size"]').trigger('change');

        $('#page-size').val(options.pageSize);
        $('#page-height').val(options.pageHeight);
        $('#page-width').val(options.pageWidth);
        $('#margin-top').val(options.marginTop);
        $('#margin-bottom').val(options.marginBottom);
        $('#margin-left').val(options.marginLeft);
        $('#margin-right').val(options.marginRight);
        $('#zoom-factor').val(options.zoomFactor);

        if (options.repeatHeader) {
            $('#chk-repeat-header').prop('checked', true);
        } else {
            $('#chk-repeat-header').prop('checked', false);
        }

        if (options.repeatFooter) {
            $('#chk-repeat-footer').prop('checked', true);
        } else {
            $('#chk-repeat-footer').prop('checked', false);
        }

        if (options.isPortrait) {
            $('input:radio[name="orientation"][value=Portrait]').prop("checked", true);
        } else {
            $('input:radio[name="orientation"][value=Landscape]').prop("checked", true);
        }

        $('#chk-repeat-header').trigger('change');
        $('#chk-repeat-footer').trigger('change');

        $('#header-spacing').val(options.headerSpacing);
        $('#footer-spacing').val(options.footerSpacing);
    }

    function resetAdvanceTemplateOptions() {        
        $('#adv-tpl-settings-chk').prop('checked', false);
        $('input:radio[name="page-size"][value=Standard]').prop("checked", true);
        $('input:radio[name="orientation"][value=Portrait]').prop("checked", true);
        $('#chk-repeat-header').prop('checked', false);
        $('#chk-repeat-footer').prop('checked', false);
        $('#page-size').val('A4');
        $('#page-height, #page-width, #margin-top, #margin-bottom, #margin-left, #margin-right, #header-spacing, #footer-spacing, #zoom-factor').val('');
    }

    function getAdvanceTemplateOptions() {

        var options = {};

        options.isCustomPage = ($('input:radio[name="page-size"]:checked').val() == 'Custom') ? true : false;
        options.pageSize = $('#page-size').val();
        options.pageHeight = $('#page-height').val();
        options.pageWidth = $('#page-width').val();
        options.marginTop = $('#margin-top').val();
        options.marginBottom = $('#margin-bottom').val();
        options.marginLeft = $('#margin-left').val();
        options.marginRight = $('#margin-right').val();
        options.zoomFactor = $('#zoom-factor').val();
        options.repeatHeader = ($('#chk-repeat-header').is(':checked')) ? true : false;
        options.repeatFooter = ($('#chk-repeat-footer').is(':checked')) ? true : false;
        options.headerSpacing = $('#header-spacing').val();
        options.footerSpacing = $('#footer-spacing').val();
        options.isPortrait = ($('input:radio[name="orientation"]:checked').val() == 'Portrait') ? true : false;

        return options;
    }

    $('#adv-tpl-settings-chk').change(function() {
        if ($(this).is(':checked')) {
            $('#advance-template-div').show("slow");
        } else {
            $('#advance-template-div').hide("slow");
        }
    });

    $('#sig-veri-settings-chk').change(function() {
        if ($(this).is(':checked')) {
            $('#sig-veri-template-div').show();
        } else {
            $('#sig-veri-template-div').hide();
            $("#signaturePicker").selectpicker('val', []);
            $("#signaturePicker").selectpicker('refresh');
        }
    });

    $('input:radio[name="page-size"]').change( function() {
        if ($('input:radio[name="page-size"]:checked').val() == 'Standard') {
            $('#statndard-page-size-div').removeClass('hidden');
            $('#custom-page-size-div').addClass('hidden');
        } else {
            $('#statndard-page-size-div').addClass('hidden');
            $('#custom-page-size-div').removeClass('hidden');
        }
    });

    $('#chk-repeat-header').change(function() {
        if ($(this).is(':checked')) {
            $('#header-spacing-div').show();
        } else {
            $('#header-spacing-div').hide();
        }
    });

    $('#chk-repeat-footer').change(function() {
        if ($(this).is(':checked')) {
            $('#footer-spacing-div').show();
        } else {
            $('#footer-spacing-div').hide();
        }
    });

});

$(document).on('change','.template-details', function(){
    if($(this).val() == 29){
        if ($(this).is(":checked")) {
            modelBody = `<input type="text" placeholder="" autofocus="true" class="form-control" id="additionalText">`;
            $('#additionalTextDiv').append(modelBody);
        } else {
            $("#additionalTextDiv").empty();
        }
    }         
});

function selectElementContents(el) {
    var range = document.createRange();
    range.selectNodeContents(el);
    var sel = window.getSelection();
    sel.removeAllRanges();
    sel.addRange(range);
}

function pasteHtmlAtCaret(html, selectPastedContent) {
    var sel, range;
    if (window.getSelection) {
        // IE9 and non-IE
        sel = window.getSelection();
        if (sel.getRangeAt && sel.rangeCount) {
            range = sel.getRangeAt(0);
            range.deleteContents();

            // Range.createContextualFragment() would be useful here but is
            // only relatively recently standardized and is not supported in
            // some browsers (IE9, for one)
            var el = document.createElement("div");
            el.innerHTML = html;
            var frag = document.createDocumentFragment(), node, lastNode;
            while ((node = el.firstChild)) {
                lastNode = frag.appendChild(node);
            }
            var firstNode = frag.firstChild;
            range.insertNode(frag);

            // Preserve the selection
            if (lastNode) {
                range = range.cloneRange();
                range.setStartAfter(lastNode);
                if (selectPastedContent) {
                    range.setStartBefore(firstNode);
                } else {
                    range.collapse(true);
                }
                sel.removeAllRanges();
                sel.addRange(range);
            }
        }
    } else if ((sel = document.selection) && sel.type != "Control") {
        var originalRange = sel.createRange();
        originalRange.collapse(true);
        sel.createRange().pasteHTML(html);
        if (selectPastedContent) {
            range = sel.createRange();
            range.setEndPoint("StartToStart", originalRange);
            range.select();
        }
    }
}

/**
 * Get information about the selected text.
 * @param the scope/window object
 & @return selected element
 */
jQuery.fn.selectedText = function(win) {
    win = win || window;

    var obj = null;
    var text = null;

    // Get parent element to determine the formatting applied to the selected text
    if (win.getSelection) {
        var obj = win.getSelection().anchorNode;

        var text = win.getSelection().toString();
        // Mozilla seems to be selecting the wrong Node, the one that comes before the selected node.
        // I'm not sure if there's a configuration to solve this,
        var sel = win.getSelection();

        if (!sel.isCollapsed && $.browser.mozilla) {
            // If we've selected an element, (note: only works on Anchors, only checked bold and spans)
            // we can use the anchorOffset to find the childNode that has been selected
            if (sel.focusNode.nodeName !== '#text') {
                // Is selection spanning more than one node, then select the parent
                if ((sel.focusOffset - sel.anchorOffset) > 1)
                    console.log("Selected spanning more than one", obj = sel.anchorNode);
                else if (sel.anchorNode.childNodes[sel.anchorOffset].nodeName !== '#text')
                    console.log("Selected non-text", obj = sel.anchorNode.childNodes[sel.anchorOffset]);
                else
                    console.log("Selected whole element", obj = sel.anchorNode);
            }
            // if we have selected text which does not touch the boundaries of an element
            // the anchorNode and the anchorFocus will be identical
            else if (sel.anchorNode.data === sel.focusNode.data) {
                console.log("Selected non bounding text", obj = sel.anchorNode.parentNode);
            }
            // This is the first element, the element defined by anchorNode is non-text.
            // Therefore it is the anchorNode that we want
            else if (sel.anchorOffset === 0 && !sel.anchorNode.data) {
                console.log("Selected whole element at start of paragraph (whereby selected element has not text e.g. &lt;script&gt;", obj = sel.anchorNode);
            }
            // If the element is the first child of another (no text appears before it)
            else if (typeof sel.anchorNode.data !== 'undefined'
                    && sel.anchorOffset === 0
                    && sel.anchorOffset < sel.anchorNode.data.length) {
                console.log("Selected whole element at start of paragraph", obj = sel.anchorNode.parentNode);
            }
            // If we select text preceeding an element. Then the focusNode becomes that element
            // The difference between selecting the preceeding word is that the anchorOffset is less that the anchorNode.length
            // Thus 
            else if (typeof sel.anchorNode.data !== 'undefined'
                    && sel.anchorOffset < sel.anchorNode.data.length) {
                console.log("Selected preceeding element text", obj = sel.anchorNode.parentNode);
            }
            // Selected text which fills an element, i.e. ,.. <b>some text</b> ...
            // The focusNode becomes the suceeding node
            // The previous element length and the anchorOffset will be identical
            // And the focus Offset is greater than zero
            // So basically we are at the end of the preceeding element and have selected 0 of the current.
            else if (typeof sel.anchorNode.data !== 'undefined'
                    && sel.anchorOffset === sel.anchorNode.data.length
                    && sel.focusOffset === 0) {
                console.log("Selected whole element text", obj = (sel.anchorNode.nextSibling || sel.focusNode.previousSibling));
            }
            // if the suceeding text, i.e. it bounds an element on the left
            // the anchorNode will be the preceeding element
            // the focusNode will belong to the selected text
            else if (sel.focusOffset > 0) {
                console.log("Selected suceeding element text", obj = sel.focusNode.parentNode);
            }
        }
        else if (sel.isCollapsed)
            obj = obj.parentNode;

    }
    else if (win.document.selection) {
        var sel = win.document.selection.createRange();
        var obj = sel;

        if (sel.parentElement)
            obj = sel.parentElement();
        else
            obj = sel.item(0);

        text = sel.text || sel;

        if (text.toString)
            text = text.toString();
    }
    else
        throw 'Error';

    // webkit
    if (obj.nodeName === '#text')
        obj = obj.parentNode;

    // if the selected object has no tagName then return false.
    if (typeof obj.tagName === 'undefined')
        return false;

    return {'obj': obj, 'text': text};
};
