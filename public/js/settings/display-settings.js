var displayData = {};

$(document).ready(function() {

    $('.display-rows', '#displaySetupTable').each(function() {

        console.log($(this).attr('id'));

        var id = $(this).attr('id');
        var isActive = ($(this).attr('data-isActive') == '0') ? 0 : 1;

        displayData[id] = isActive;
    });




    $(".active-inactive-btn").on("click", function(e) {
        e.preventDefault();
        if ($(this).hasClass('fa-square-o')) {
            $(this).removeClass('fa-square-o');
            $(this).addClass('fa-check-square-o');
            var id = ($(this).parents('tr').attr('id'));
            displayData[id] = 1;
        } else {
            $(this).addClass('fa-square-o');
            $(this).removeClass('fa-check-square-o');
            var id = ($(this).parents('tr').attr('id'));
            displayData[id] = 0;
        }  


    });

    $("#saveWFDoc").on("click", function(e) {
        e.preventDefault();

        eb.ajax({
            type: 'POST',
            url: BASE_URL + '/settings/api/display-settings/update',
            data: {'displayData': displayData},
            success: function(respond) {
                if (respond.status) {
                    p_notification(true, respond.msg);
                    
                    setTimeout(function(){ 
                        window.location.reload();
                    }, 1000);
                 
                } else {
                    p_notification(false, respond.msg);
                    return;
                }
            },
        });
    });
});
