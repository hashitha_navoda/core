$(document).ready(function() {

    var dimensionValue = null;
    var dimensionValues = {};
    var mainDimensionValues = {};
    var dimensionId = null;
    var rowIncrement = 1;
    var editFlag = false;
 
    $('#btnAdd').on('click',function(){
        var dimensionName = $('#dimensionName').val();

        var formData = {
            dimensionName: dimensionName
        }
    
        if(validateFormData(formData)){
            eb.ajax({
                type: 'POST',
                url: BASE_URL + '/dimension-api/saveDimension',
                data: {
                    dimensionName : dimensionName,
                    mainDimensionValues: mainDimensionValues
                },
                success: function(respond) {
                    p_notification(respond.status, respond.msg);                
                    if(respond.status){
                        $('#dimensionName').val('');
                        window.setTimeout(function() {
                                location.reload();
                        }, 1000);                    
                    }                
                }
            });
        } 
    });

    //for cancel btn
    $('#btnCancel, #btnViewCancel').on('click',function (){
        $('input[type=text]').val('');  
        $('#dimensionValues').text('Add values for dimension');      
        dimensionCreateView();        
    });

    $('#dimension-list').on('click','.dimension-action',function (e){    
        var action = $(this).data('action');
        dimensionId   = $(this).closest('tr').data('dimension-id');
        var dimensionName = $(this).closest('tr').data('dimension-name');

        switch(action) {
            case 'edit':
                editFlag = true;
                e.preventDefault();
                dimensionUpdateView();
                $('#dimensionName').val(dimensionName);
                getRelatedDimensionValues(dimensionId);
                break;
            case 'delete':
                deleteDimension(dimensionId);
                break;
            case 'view':
                e.preventDefault();
                dimensionView();
                //set selected department details
                $('#dimensionName').val(dimensionName).attr('disabled', 'disabled');
                getRelatedDimensionValues(dimensionId);
                $('#addDimensionValueModal #addNewDimensionValueRow').addClass('hidden');
                $('#addDimensionValueModal .deleteAddedDimensionValues').addClass('hidden');
                $('#addDimensionValueModal #addServiceRates').addClass('hidden');
                $('#addDimensionValueModal #viewServiceRates').removeClass('hidden');
                $('#btnViewRateCardServicesClose').removeClass('hidden');
                $('#dimensionValues').text('View values of dimension');
                break;

            default:
                console.error('Invalid action.');
                break;
        }
    });

    //this function for switch to rate card register view
    function dimensionCreateView(){
        $('.updateTitle').addClass('hidden');
        $('.addTitle').removeClass('hidden');
        $('.updateDiv').addClass('hidden');
        $('.addDiv').removeClass('hidden');
        $('.viewTitle').addClass('hidden');
        $('.viewDiv').addClass('hidden');
        $('#dimensionName').attr('disabled', false);

        $('#addDimensionValueModal #addDimensionValueBody .addedDimensionValue').remove();
        $('#addDimensionValueModal #addNewDimensionValueRow').removeClass('hidden');
        $('#addDimensionValueModal .deleteAddedDimensionValues').removeClass('hidden');
        $('#addDimensionValueModal #addServiceRates').removeClass('hidden');
        $('#addDimensionValueModal #viewServiceRates').addClass('hidden');
        mainDepartmentStations = {};
        departmentStations = {};
    }

    //this function for switch to rate card update view
    function dimensionUpdateView(){
        $('.addTitle').addClass('hidden');
        $('.updateTitle').removeClass('hidden');
        $('.addDiv').addClass('hidden');
        $('.viewTitle').addClass('hidden');
        $('.viewDiv').addClass('hidden');
        $('.updateDiv').removeClass('hidden');
        $('#dimensionName').attr('disabled', false);
        $('#addDimensionValueModal #addDimensionValueBody .addedDimensionValue').remove();
        $('#addDimensionValueModal #addNewDimensionValueRow').removeClass('hidden');
        $('#addDimensionValueModal .deleteAddedDimensionValues').removeClass('hidden');
        $('#addDimensionValueModal #addServiceRates').removeClass('hidden');
        $('#addDimensionValueModal #viewServiceRates').addClass('hidden');
    }

    //this function for switch to rate card update view
    function dimensionView(){
       $('.addTitle').addClass('hidden');
       $('.updateTitle').addClass('hidden');
       $('.viewTitle').removeClass('hidden');
       $('.addDiv').addClass('hidden');
       $('.updateDiv').addClass('hidden');
       $('.viewDiv').removeClass('hidden');
    }

    $('#btnUpdate').on('click',function(){
        var dimensionName = $('#dimensionName').val();

        var formData = {
            dimensionName: dimensionName
        }

        if(validateFormData(formData)){
            eb.ajax({
                type: 'POST',
                url: BASE_URL + '/dimension-api/updateDimension',
                data: {
                    dimensionId: dimensionId,
                    dimensionName : dimensionName,
                    mainDimensionValues: mainDimensionValues
                },
                success: function(respond) {
                    p_notification(respond.status, respond.msg);                
                    if(respond.status){
                        $('#dimensionName').val('');
                        window.setTimeout(function() {
                                location.reload();
                        }, 1000);                    
                    }                
                }
            });
        } 
    });

    //for search
    $('#searchDimension').on('click', function() {
        var key = $('#dimensionSearch').val();

        searchDimension(key);
    });


    function searchDimension(key) {  
        $.ajax({
            type: 'POST',
            url: BASE_URL + '/dimension-api/search',
            data: {
                searchKey : key
            },
            success: function(data) {
                if (data.status == true) {
                    if (data.status) {
                        $("#dimension-list").html(data.html);
                    } else {
                        p_notification(data.status, data.msg);
                    }
                }            
            }
        });  
    }

    //for rest btn
    $('#btnReset').on('click',function (){
        $('input[type=text]').val('');
        $('#addDimensionValueModal #addDimensionValueBody .addedDimensionValue').remove();
        dimensionValues = {};
        mainDimensionValues = {};
        subTaskRates = {};
    });  

    //for search cancel btn
    $('#searchCancel').on('click',function (){
        $('#dimensionSearch').val('');
        searchDimension();
    });

    
    function deleteDimension(dimensionId) {
        bootbox.confirm('Are you sure you want to delete this dimension?', function($result) {
            if ($result == true) {
                eb.ajax({
                    url: BASE_URL + '/dimension-api/deleteDimension',
                    method: 'post',
                    data: {
                        dimensionId: dimensionId
                    },
                    dataType: 'json',
                    success: function(data) {
                        if (data.status == true) {
                            p_notification(data.status, data.msg);
                            window.location.reload();
                        } else {
                            p_notification(data.status, data.msg);
                        }
                    }
                });
            }
        });
    }

    $('#dimensionValues').on('click', function() {
        reomoveUnAddedDimensionValues();
    });

     $('#btnSrvcClose').on('click',function(){    
        reomoveUnAddedDimensionValues();
    });

    //add new dimension value
    $('#add_dimension_value').on('click', function(e) {
        e.preventDefault();

        //if plus button is clicked then disabled the button
        //if button is clicked more than one, then it does not continue
        if ($(this).is(':disabled')) {
            return false;
        } else {
            $(this).attr('disabled', true);
            var $thisRow = $(this).parents('tr');
            if (!$('#dimensionValue').val().trim()) {
                p_notification(false, eb.getMessage('ERR_REQUIRED_SRVC_RTE'));
                $(this).attr('disabled', false);
                return false; 
            } else {
                dimensionValue = $('#dimensionValue').val();

                var dimensionValueDuplicateFlag = false;
                if (!$.isEmptyObject(dimensionValues)) {
                    $.each(dimensionValues, function(index, val) {
                        if (val.value == dimensionValue) {
                            dimensionValueDuplicateFlag = true;
                        }
                    });
                }

                if (dimensionValueDuplicateFlag) {
                    p_notification(false, eb.getMessage('ERR_DIMENSION_VALUE_ALREADY_INSERTED'));
                    $(this).attr('disabled', false);
                    return false; 
                }

                addNewDimensionValueRow(false);
                rowIncrement++;
                $(this).attr('disabled', false);
            }
        }
    });

    function addNewDimensionValueRow(autofill) {
        

        var newTrID = 'tr_'+rowIncrement;
        var clonedRow = $($('#preSample').clone()).attr('id', newTrID).addClass('addedDimensionValue');
        $("input[name='dimensionVal']", clonedRow).val(dimensionValue);
        clonedRow.removeClass('hidden');
        clonedRow.insertBefore('#preSample');
        if (!autofill) {
            dimensionValues[rowIncrement] = {
                value: dimensionValue,
            }
        }
        clearAddedNewRow();
    }

    function clearAddedNewRow() {
        $('#dimensionValue').val('');
        $('#dimensionValue').val('').attr('disabled', false);
    }

    $('#btnAddDimensionVal').on('click',function(){
        addDimensionValuesToMainObject();
        $("#addDimensionValueModal").modal('toggle');
    });

    //service Rates footer add button
    function addDimensionValuesToMainObject()
    {
        mainDimensionValues = {};
        for (var key in dimensionValues) {
            if (dimensionValues.hasOwnProperty(key)) {
                mainDimensionValues[key] = dimensionValues[key];
            }
        }
    }

    function reomoveUnAddedDimensionValues() {
       
        var temp1 = [];
        for (var key in mainDimensionValues) {
            if (mainDimensionValues.hasOwnProperty(key)) {
                temp1.push(key);
            }
        }
        var temp2 = [];
        for (var key in dimensionValues) {
            if (dimensionValues.hasOwnProperty(key)) {
                temp2.push(key);
            }
        }

        for (var i = 0; i < temp2.length; i++) {    
            if ($.inArray(temp2[i], temp1) == -1) {
                delete dimensionValues[temp2[i]];
                $('#' + 'tr_'+temp2[i]).remove();
            }           
        }

        if (editFlag) {
            dimensionValues = mainDimensionValues;
        }

        addDimensionValuesToMainObject();
        $("#addDimensionValueModal").modal('toggle');
    }

    $("#addDimensionValueModal").on('click','.deleteAddedDimensionValues', function(e) {
        e.preventDefault();
        var deleteTempDimensionValID = $(this).closest('tr').attr('id');
        var deleteDimValID = deleteTempDimensionValID.split('tr_')[1].trim();

        bootbox.confirm('Are you sure you want to remove this Dimesion Value ?', function(result) {
            if (result == true) {
                delete dimensionValues[deleteDimValID];
                $('#' + deleteTempDimensionValID).remove();
            }
        });
    });
    $("#addDimensionValueModal").on('click','.viewSubTaskRate', function(e) {
        var tempRowID = $(this).closest('tr').attr('id');
        var rowID = tempRowID.split('tr_')[1].trim();

        if (Object.keys(serviceRates[rowID].subTaskRates).length == 0) {
            p_notification(false, eb.getMessage('ERR_NO_SUB_TSK_RET_ASSIGN'));
            return;
        } 

        $('#subTaskRatesViewModal #viewServiceSubTaskRates .viewedSubTaskRates').remove();

        for (var key in serviceRates[rowID].subTaskRates) {
            var newTrID = 'trs_'+key;
            var clonedRow = $($('#subTskRteViewRow').clone()).attr('id', newTrID).addClass('viewedSubTaskRates').data('sub-task-id', key);
            $("#addedSubTskCode", clonedRow).val(serviceRates[rowID].subTaskRates[key].name).attr('disabled', 'disabled');
            $("#addedSubTskRate", clonedRow).val(accounting.formatMoney(serviceRates[rowID].subTaskRates[key].rate)).attr('disabled', 'disabled');
            clonedRow.removeClass('hidden');
            clonedRow.insertBefore('#subTskRteViewRow');
            $('#subTskRteViewRow').addClass('hidden');
        }
        
        $("#subTaskRatesViewModal").modal('toggle');
    
    });

    function getRelatedDimensionValues(Id) {
        eb.ajax({
            type: 'POST', url: BASE_URL + '/dimension-api/getRelatedDimensionValues', 
            data: {dimensionId: Id}, 
            success: function(respond) {
                if (respond.status == true) {
                    $('#addDimensionValueModal #formRow .addedDimensionValue').remove();
                    mainDimensionValues = respond.data;
                    for (var key in respond.data) { 
                        dimensionValue = respond.data[key].value;
                        addNewDimensionValueRow(true);
                        // rowIncrement++;
                    }
                } 
            }
        });
    }

    /**
     *
     *use to change active state
     */
    function activeInactive(type, status, currentDiv, msg, flag) {
        bootbox.confirm(msg, function($result) {
            if ($result == true) {
                eb.ajax({
                    url: BASE_URL + '/rate-card-api/changeStatusID',
                    method: 'post',
                    dataType: 'json',
                    data: {
                        'rateCardID': type,
                        'status': status,
                    },
                    success: function(data) {
                        if (data.status) {
                            p_notification(data.status, data.msg);
                            if (currentDiv.hasClass('fa-check-square-o') && flag) {
                                currentDiv.removeClass('fa-check-square-o');
                                currentDiv.addClass('fa-square-o');
                            }
                            else {
                                currentDiv.removeClass('fa-square-o');
                                currentDiv.addClass('fa-check-square-o');
                            }
                        } else {
                            p_notification(data.status, data.msg);
                        }
                    }
                });
            }
        });
    }

    $('#addDimensionValueModal #subTaskRate').on('click', function(){

        var currentDiv = $(this).contents();
        if (currentDiv.hasClass('fa-check-square-o')) {
            $('.viewedSubTasks').addClass('hidden');
            $('#subTskHeader').addClass('hidden');
            currentDiv.removeClass('fa-check-square-o');
            currentDiv.addClass('fa-square-o');
            $('#serviceRateVal').val('').attr('disabled', false);
            $('#viewServiceSubTasks .viewedSubTasks #subTskRate').val('');
            $('#viewServiceSubTasks .viewedSubTasks .save').addClass('hidden');
            $('#viewServiceSubTasks .viewedSubTasks .edit').addClass('hidden');
            $('#viewServiceSubTasks .viewedSubTasks .add').removeClass('hidden');
        }
        else {
            if ($('#serviceCode').val() == 0) {    
                p_notification(false, eb.getMessage('PLS_SELECT_AT_LEAST_ONE_SRVC'));
                clearServiceCode();
                return false;
            }
            $('.viewedSubTasks').removeClass('hidden');
            $('#subTskHeader').removeClass('hidden');
            $('#serviceRateVal').val('').attr('disabled', 'disabled');
            currentDiv.removeClass('fa-square-o');
            currentDiv.addClass('fa-check-square-o');
        }
    });

    function clearServiceCode($currentRow) {
        $('#serviceCode')
                .empty()
                .val('')
                .append($("<option></option>")
                        .attr("value", "0")
                        .text('Select a Service'))
                .selectpicker('refresh')
                .trigger('change');
    }

    function getRelatedSubTasks(Id) {
        eb.ajax({
            type: 'POST', url: BASE_URL + '/serviceSetup-api/getSubTaskByTaskId', 
            data: {taskID: Id}, 
            success: function(respond) {
                if (respond.status == true) {
                    $('#addDimensionValueModal #viewServiceSubTasks .viewedSubTasks').remove();

                    for (var i = 0; i < respond.data.length; i++) {
                        var newTrID = 'trs_'+respond.data[i]['subTaskID'];
                        var clonedRow = $($('#subTskRow').clone()).attr('id', newTrID).addClass('viewedSubTasks').data('sub-task-id', respond.data[i]['subTaskID']);
                        $("#subTskCode", clonedRow).val(respond.data[i]['code']).attr('disabled', 'disabled');
                        $("#subTskRate", clonedRow).val(respond.data[i]['rate']);
                        clonedRow.insertBefore('#subTskRow');
                    }

                } 
            }
        });
    }

    $('#viewServiceSubTasks').on('click', '.add', function(){

        var rowId = $(this).closest('tr').attr('id');
        var subTskId = $(this).closest('tr').data('sub-task-id');
        if ($('#'+rowId+' #subTskRate').val() == '') {
            p_notification(false, eb.getMessage('PLS_ADD_SUB_CRD_RTE'));
            return false;
        }
        var rateVal = $('#'+rowId+' #subTskRate').val();
        $('#'+rowId+' #subTskRate').val(accounting.formatMoney(rateVal));

        var serviceRateVal = (accounting.unformat($('#serviceRateVal').val()) == '') ? 0 : accounting.unformat($('#serviceRateVal').val());
        var newVal = parseFloat(serviceRateVal) + parseFloat(rateVal);
        $('#serviceRateVal').val(accounting.formatMoney(newVal));
        var subTskCode = $('#'+rowId+' #subTskCode').val();
        subTaskRates[subTskId] = {
            name: subTskCode,
            rate: rateVal
        };
        $('#'+rowId+' #subTskRate').attr('disabled', 'disabled');


        $(this).addClass('hidden');
        $('#'+rowId+' .edit').removeClass('hidden');
    });

    $('#viewServiceSubTasks').on('click', '.edit', function(){

        var rowId = $(this).closest('tr').attr('id');

        editSubTaskRate = $('#'+rowId+' #subTskRate').val();
        $(this).addClass('hidden');
        $('#'+rowId+' .save').removeClass('hidden');
        $('#'+rowId+' #subTskRate').attr('disabled', false);

    });

    $('#viewServiceSubTasks').on('click', '.save', function(){

        var subTskId = $(this).closest('tr').data('sub-task-id');
        var rowId = $(this).closest('tr').attr('id');
        var substractedServiceRate = parseFloat($('#serviceRateVal').val()) - parseFloat(editSubTaskRate);
        $(this).addClass('hidden');
        $('#'+rowId+' .edit').removeClass('hidden');
        var rateVal = $('#'+rowId+' #subTskRate').val();
        var serviceRateVal = (substractedServiceRate == '') ? 0 : substractedServiceRate;
        var newVal = parseFloat(serviceRateVal) + parseFloat(rateVal);
        $('#serviceRateVal').val(newVal);
        subTaskRates[subTskId].rate = rateVal;
        $('#'+rowId+' #subTskRate').attr('disabled', 'disabled');

    });

    function validateFormData(formData) {
        if (formData.dimensionName == "") {
            p_notification(false, eb.getMessage('ERR_DIM_NAME_EMPTY'));
            $('#dimensionName').focus();
            return false;
        }
        return true;
    }

    $('#serviceRateVal').focusout(function(){
        accounting.unformat($('#serviceRateVal').val());
        var formattedVal =  accounting.formatMoney($('#serviceRateVal').val());
        $('#serviceRateVal').val(formattedVal);
    });

});
