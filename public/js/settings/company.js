$(document).ready(function () {

    $('#profile_pic').hide();
    var url4 = BASE_URL + '/companyAPI/preview';
    $('#image-submit').on('click', function (e) {
        input = document.getElementById('image_file');
        var file = input.files[0];
        if (file) {
            formdata = false;
            if (window.FormData) {
                formdata = new FormData();
            }
            e.preventDefault();
            formdata = eb.getImageDimension("images", file, formdata, '200', '200', '0', '0', '200', '200');
            eb.ajax({
                url: BASE_URL + '/image-preview',
                type: 'POST',
                processData: false,
                contentType: false,
                data: formdata,
                success: function (data) {
                    //$('#thumbs').html('<img src="' + data + '" />');
                    $('#thumbs').attr('src', data);
                    $('#imageUploaderModal').modal('hide');
                    $('#image_file').data('flag', true);
                    return data;
                }
            });
        }
    });
    function getTempImagePath(data) {

    }
    function validateForm(valid) {
        var cn = valid[0];
        var ca = valid[1];
        var cp = valid[2];
        var ct = valid[3];
        var ce = valid[4];
        var cbr = valid[5];
        var country = valid[7];
        var atpos = ce.indexOf("@");
        var dotpos = ce.lastIndexOf(".");
        var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        if (cn == null || cn == "") {
            p_notification(false, eb.getMessage('ERR_WIZARD_NAMEBLANK'));
        } else if (ca == null || ca == "") {
            p_notification(false, eb.getMessage('ERR_WIZARD_ADDRBLANK'));
        } else if (isNaN(cp)) {
            p_notification(false, eb.getMessage('ERR_WIZARD_PCODE'));
        } else if (cp.length > 10) {
            p_notification(false, eb.getMessage('ERR_WIZARD_PCODERANGE'));
        } else if (ct == null || ct == "") {
            p_notification(false, eb.getMessage('ERR_WIZARD_TELEBLANK'));
        } else if (isNaN(ct)) {
            p_notification(false, eb.getMessage('ERR_WIZARD_TELENUM'));
        } else if (ct.length < 10 || ct.length > 13) {
            p_notification(false, eb.getMessage('ERR_WIZARD_PHONENO_LIMIT'));
        } else if (ce == null || ce == "") {
            p_notification(false, eb.getMessage('ERR_CUST_EMPTY_EMAIL'));
        } else if (country == null || country == "") {
            p_notification(false, eb.getMessage('ERR_COUNTRY_NAME'));
            return false;
        } else if (ce != '') {
            if (!regex.test(ce)) {
                p_notification(false, eb.getMessage('ERR_CUST_VALID_EMAIL'));
            } else {
                return true;
            }

        }
        else {
            return true;
        }

    }

    var url2 = BASE_URL + '/companyAPI/index';
    $('#create-company-button').on('click', function (e) {        
        e.preventDefault();
        var param = $('#create-company-form').serialize();
        var valid = new Array(
                $('#companyName').val(),
                $('#companyAddress').val(),
                $('#postalCode').val(),
                $('#telephoneNumber').val(),
                $('#email').val(),
                $('#brNumber').val(),
                $('#website').val(),
                $('#country').val(),
                $('#telephoneNumber2').val(),
                $('#telephoneNumber3').val(),
                $('#faxNumber').val()
                );
        if (validateForm(valid)) {
            var submitForm = eb.post(url2, param);
            submitForm.done(function (data) {
                if (data.status == true) {
                    input = document.getElementById('image_file');
                    var file;
                    file = input.files[0];
                    if (file) {
                        formdata = false;
                        if (window.FormData) {
                            formdata = new FormData();
                        }
                        e.preventDefault();
                        formdata.append("companyID", data.data);
                        formdata = eb.getImageDimension("images", file, formdata, '200', '200', '0', '0', '200', '200');
                        eb.ajax({
                            url: url4,
                            type: 'POST',
                            processData: false,
                            contentType: false,
                            data: formdata,
                            success: function (res) {
                                if (res) {
                                    p_notification(true, eb.getMessage('SUCC_COMP_ADD'));
                                    window.setTimeout(function () {
                                        location.reload();
                                    }, 1000);
                                } else {
                                    p_notification(true, eb.getMessage('ERR_COMP_UPDATEDETAIL')); //"An error occurred while updating company details"
                                }
                            }
                        });
                    } else {
                        p_notification(true, eb.getMessage('SUCC_COMP_ADD'));
                        window.setTimeout(function () {
                            location.reload();
                        }, 1000);
                    }
                } else if (data.status == false) {
                    p_notification(true, eb.getMessage('ERR_PRDCTAPI_OCCUR'));
                }
            });
        }
    });


//    $(window).bind('resize ready', function () {
//        if ($(window).width() <= 768) {
//            $('.image_upload').removeClass('remove_right_padding').addClass('remove_left_padding');
//        } else {
//            $('.image_upload').addClass('remove_right_padding').removeClass('remove_left_padding');
//        }
//    });

//    $(window).bind('resize ready', function () {
//        if ($(window).width() <= 480) {
//            $('input[type="reset"]').addClass('col-xs-12');
//            $('#create-company-button').addClass('col-xs-12').css('margin-bottom', '8px');
//        } else {
//            $('input[type="reset"]').removeClass('col-xs-12');
//            $('#create-company-button').removeClass('col-xs-12').css('margin-bottom', '0px');
//        }
//    });


});
