$(function() {
    var createCurrencyForm = $("#create-currency-form");

    createCurrencyForm.submit(function(e) {

        var form_data = {
            currencyName: $("[name='customCurrencyName']", this).val(),
            currencySymbol: $("[name='customCurrencySymbol']", this).val(),
            currencyRate: $("[name='customCurrencyRate']", this).val(),
        };
        var submitted_btn = $("[type='submit']:visible", $(this));
        var update = submitted_btn.hasClass('update');

        if (form_data.currencyName.trim() == "") {
            p_notification(false, eb.getMessage('ERR_REQUIRE_CUSTOM_CURRENCY_NAME'));
            $('#customCurrencyName').focus();
            return false;
        } else if (form_data.currencySymbol.trim() == "") {
            p_notification(false, eb.getMessage('ERR_REQUIRE_CUSTOM_CURRENCY_SYMBOL'));
            $('#customCurrencySymbol').focus();
            return false;
        } else if (form_data.currencyRate.trim() == "") {
            p_notification(false, eb.getMessage('ERR_REQUIRE_CUSTOM_CURRENCY_RATE'));
            $('#customCurrencyRate').focus();
            return false;
        }

        if (update) {
            form_data = $.extend(form_data, {currencyId: $("[name='customCurrencyId']", this).val()});
        }

        eb.ajax({
            url: '/customCurrency/Api/updateCustomCurrency/' + getCurrPage(),
            method: 'post',
            data: form_data,
            dataType: 'json',
            success: function(data) {
                if (data.status == true) {
                    $("#currencyList").html(data.html);
                    p_notification(true, data.msg);
                } else {
                    p_notification(false, data.msg);
                    if (!update) {
                        $('#customCurrencyName').focus();
                    } else {
                        $('#customCurrencyRate').focus();
                    }
                }
                clearCurrencyForm();
            }
        });

        e.stopPropagation();
        return false;
    });

    function clearCurrencyForm() {
        $("#customCurrencyName").attr('disabled', false);
        $('#create-currency-button').removeClass('update').attr('value', 'Save');
        $('#customCurrencyRate').val('');
    }

    $("#currencyList").on('click', '.edits', function(e) {
        $('#customCurrencyName').val(this.id).trigger('change').attr('disabled', true);
        $('#create-currency-button').addClass('update').attr('value', 'Update');
    });

    $('.cancel').on('click', function() {
        $('#create-currency-button').removeClass('update').attr('value', 'Save');
        $("[name='customCurrencyId']").val();
        $("#customCurrencyName").attr('disabled', false);
        $('#customCurrencyRate').val('');
    });

    $("form.custom-currency-search").submit(function(e) {
        var form_data = {
            keyword: $("#custom-currency-keyword", this).val()
        };

        eb.ajax({
            url: '/customCurrency/Api/searchCustomCurrency',
            method: 'post',
            data: form_data,
            dataType: 'json',
            success: function(data) {
                if (data.status == true) {
                    $("#currencyList").html(data.html);
                }
            }
        });

        e.stopPropagation();
        return false;
    });

    $("#customCurrencyName").on('change', function(e) {
        eb.ajax({
            url: '/currencySymbol/getCurrencySymbol',
            method: 'post',
            data: {currencyId: $("#customCurrencyName").val()},
            dataType: 'json',
            success: function(res) {
                if (res.status) {
                    $('#customCurrencyRate').attr('readonly', false);
                    $('#customCurrencySymbol').val(res.data.currencySymbol);
                    $('#customCurrencyRate').val(parseFloat(res.data.currencyRate).toFixed(4));
                    if ($('#companyCurrencyId').val() == $("#customCurrencyName").val()) {
                        $('#customCurrencyRate').attr('readonly', true);
                        p_notification(false, eb.getMessage('ERR_SYSTOM_CURRENCY_CANOT_BE_EDIT'));
                    }
                }
            }
        });

        e.stopPropagation();
        return false;
    });
    $("#customCurrencyName").trigger('change');
});