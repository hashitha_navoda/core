var workFlowDocument = {};

$(document).ready(function() {

    eb.ajax({
        type: 'POST',
        url: BASE_URL + '/api/approval-workflow-settings/getApprovalWorkflowDocuments',
        data: {},
        success: function(respond) {
            
            workFlowDocument = respond.data;
        },
    });


    $(".active-inactive-btn").on("click", function(e) {
        e.preventDefault();
        if ($(this).hasClass('fa-square-o')) {
            $(this).removeClass('fa-square-o');
            $(this).addClass('fa-check-square-o');
            var id = ($(this).parents('tr').attr('id'));

            workFlowDocument[id].isActive = 1;
        } else {
            $(this).addClass('fa-square-o');
            $(this).removeClass('fa-check-square-o');
            var id = ($(this).parents('tr').attr('id'));
            workFlowDocument[id].isActive = 0;
        }  


    });

    $("#saveWFDoc").on("click", function(e) {
        e.preventDefault();

        eb.ajax({
            type: 'POST',
            url: BASE_URL + '/api/approval-workflow-settings/updateWFDoc',
            data: {'workFlowDocument': workFlowDocument},
            success: function(respond) {
                console.log(respond);
                if (respond.status) {
                    p_notification(true, respond.msg);
                    
                    setTimeout(function(){ 
                        window.location.reload();
                    }, 1000);
                 
                } else {
                    p_notification(false, respond.msg);
                    return;
                }
            },
        });
    });
});
