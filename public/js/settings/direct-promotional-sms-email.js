
var combinationRules = {};
var addedCombinationRules = {};
var notifyType = null;


$(document).ready(function() {
    var selectedProduct = '';
    var selectedProductID = '';
    var promoContactList = [];
    var _contactList =[];
    var promoCustomersEventList = [];
    var selectedPromoEmailCustomers = [];
    var selectedPromoContactsEmails = [];
    var selectedPromoContactsNums = [];
    var selectedPromoContactsList =[];
    var selectedPromoSmsCustomersName = [];
    var selectedPromoSmsCustomers = [];
    var selectedPromoEmailLocations = [];
    var selectedPromoEmailProducts = [];
    var selectedPromoEmailCustomerEvents = [];
    var promotionProducts = {};
    var promotionCombinations = {};
    var thisIsPromotionEdit = false;
    var alreadySetPromotionProducts = false;
    var editPromotionID = '';
    var $productTable = $('#promoAddNewItems');
    var $combinationTable = $('#promoAddNewCombination');
    var $ruleTable = $('#addRuleModal #ruleAddBody');
    var $attrRuleTable = $('#addAttrRuleModal #attrRuleAddBody');
    var $tempRuleTable = $('#addRuleModal #tempRuleAddBody');
    var $tempAttrRuleTable = $('#addAttrRuleModal #tempAttrRuleAddBody');
    var $addNewCombinationRow = $('tr.promo-comb-add-row', $combinationTable);
    var $addNewRuleRow = $('tr.promo-comb-rule-add-row', $ruleTable);
    var $addNewAttrRuleRow = $('tr.promo-comb-attr-rule-add-row', $attrRuleTable);
    var $addNewProductRow = $('tr.promo-add-row', $productTable);
    var fromDateFormat = $('#promotionFrom').data('date-format');
    var selectedContactCount = 0;
    var selectedListCount = 0;
    var msgObj = {};

    var promotionFromDate = $('#promotionFrom').datepicker({
        format: fromDateFormat,
    }).on('changeDate', function(ev) {
        promotionFromDate.hide();
    }).data('datepicker');

    var toDateFormat = $('#promotionTo').data('date-format');
    var promotionToDate = $('#promotionTo').datepicker({
        format: toDateFormat,
    }).on('changeDate', function(ev) {
        promotionToDate.hide();
    }).data('datepicker');


    var processJoin = function (element) {
        if(element.value == "sms") {
            resetWindow();
            notifyType = 'Sms';
            $('#emailPromoNext').addClass('hidden');
            $('#smsPromoNext').removeClass('hidden');
            $('.con-email-header').addClass('hidden');
            $('.con-num-header').removeClass('hidden');
        }
        else {
            resetWindow();
            notifyType = 'Email';
            $('.con-num-header').addClass('hidden');
            $('.con-email-header').removeClass('hidden');
            $('#emailPromoNext').removeClass('hidden');
            $('#smsPromoNext').addClass('hidden');
        }
    };

    $(":radio[name=filter_notification]").click(function () {
        processJoin(this);
    }).filter(":checked").each(function () {
        processJoin(this);
    });


    tinymce.init({
      selector: "textarea[name='emailBody']",
      statusbar: false,
      content_css: [
        "/assets/bootstrap/css/bootstrap.min.css",
        "/css/template-editor.css",
      ],
      plugins: [
        "advlist autolink lists link image charmap print preview anchor",
        "searchreplace visualblocks code fullscreen",
        "insertdatetime media table contextmenu paste", //autoresize
      ],
      height: 200,
      toolbar:
        "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link",
    });

    $('#promoEmailsWindow').on('click', "#promoEmailEditBack", function() {
        $('#promoEmailsWindow').addClass('hidden');
        if ($('#promoEmailsDiv').length) {
            $('#promoEmailsDiv').removeClass('hidden');
        } else {
            $('#promoCustomerEventsEmailsDiv').removeClass('hidden');
        }
    });

    $("#promoSmsWindow").on("click", "#promoSmsEditBack", function () {
        $("#promoSmsWindow").addClass("hidden");
        if ($("#promoEmailsDiv").length) {
            $("#promoEmailsDiv").removeClass("hidden");
        } else {
            $("#promoCustomerEventsEmailsDiv").removeClass("hidden");
        }
    });

    $("#promoCustomerEventsSmsWindow").on("click", "#promoSmsEditBack", function () {
        $("#promoCustomerEventsSmsWindow").addClass("hidden");
        if ($("#promoEmailsDiv").length) {
            $("#promoEmailsDiv").removeClass("hidden");
        } else {
            $("#promoCustomerEventsEmailsDiv").removeClass("hidden");
        }
    });


    $('input[type=checkbox][name=selectAll]').change(function() {
        if ($(this).is(':checked')) {
          $('.promoCustomerSelect', '#promoCustomerTable').each(function() {
              if (!$(this).parents('tr').hasClass('hidden')) {    
                if (!$(this).is(':checked')) {

                    if (notifyType == "Sms") {

                        $(this).prop("checked", true);
                        var contactID = $(this).parents('tr').attr('data-cont-id');
                        selectedPromoContactsNums.push(contactID); 
                    } else {

                        $(this).prop("checked", true);
                        var contactID = $(this).parents('tr').attr('data-cont-id');
                        selectedPromoContactsEmails.push(contactID);
                    }
                }
              }
            });
        }
        else {
            $('.promoCustomerSelect', '#promoCustomerTable').each(function() {
              if (!$(this).parents('tr').hasClass('hidden')) {    
                if ($(this).is(':checked')) {

                    if (notifyType == "Sms") {

                        $(this).prop("checked", false);
                        var contactID = $(this).parents('tr').attr('data-cont-id');
                        delete selectedPromoContactsNums[contactID]; 
                    } else {

                        $(this).prop("checked", false);
                        var contactID = $(this).parents('tr').attr('data-cont-id');
                        // selectedPromoContactsEmails.push(contactID);
                        delete selectedPromoContactsEmails[contactID]; 
                    }
                }
              }
            });
        }
    });


    $('#promoEmailsDiv').on('click', '#getContactList', function() {
        // load contact for direct promotion
        eb.ajax({
            type: 'POST',
            url: BASE_URL + '/contactsAPI/getContactsForDirectPromotions',
            data: {notifyType: notifyType},
            success: function(respond) {
                if (respond.data) {
                    promoContactList = respond.data;
                    $('#promoCustomerDiv').removeClass('hidden');
                    $('#promoCustomerTable').removeClass('hidden');
                    $('#promoCustomerTable').find(".promoCustomers").remove();
                    $('#selectedContact').html("0");
                    $('#selectedContactList').html("0");

                    selectedContactCount = 0;
                    selectedListCount = 0;
                    for (var i in respond.data) {
                        var $customerSample = $('#customerSampleRow', '#promoCustomerTable').clone();

                        var firstName = respond.data[i].firstName;
                        var lastName = (respond.data[i].lastName != '' && respond.data[i].lastName != null) ? respond.data[i].lastName : '';
                        var fullName = firstName+' '+lastName;


                        $customerSample.find('.customerName').html(fullName);
                        if(notifyType == "Sms") {
                            $customerSample.find('.customerEmail').addClass('hidden');
                            $customerSample.find(".customerPhoneNumber").removeClass('hidden');
                        }

                        if (notifyType == "Email") {
                            $customerSample.find('.customerEmail').removeClass('hidden');
                            $customerSample.find(".customerPhoneNumber").addClass('hidden');
                        }
                        $customerSample.find('.customerEmail').html(respond.data[i].email);
                        $customerSample
                          .find(".customerPhoneNumber")
                          .html(respond.data[i].tel);
                        // $customerSample.find('.customerStatus').html(respond.data[i].customerStatus);
                        $customerSample.find("input[name='contacts[]']").val(i);
                        $customerSample.attr('id', '');
                        $customerSample.attr('data-cont-id', respond.data[i].contactID);
                        $customerSample.addClass('promoCustomers');
                        $customerSample.removeClass('hidden');
                        $customerSample.removeClass('hidden');
                        $('.table-div').addClass('test-body');
                        $('.select-all-div').removeClass('hidden');
                        $customerSample.insertBefore($('#customerSampleRow', '#promoCustomerTable'));
                    }
                } else {
                    $('#promoCustomerDiv').addClass('hidden');
                    $('#promoCustomerTable').addClass('hidden');
                    $('#promoCustomerTable').find(".promoCustomers").remove();
                    p_notification('info', eb.getMessage('ERR_PROMO_EMAIL_CUST_LIST'));
                }
            }
        });

        // Load contact list for 
        eb.ajax({
            type: 'POST',
            url: BASE_URL + '/contactsAPI/getContactList',
            data: {notifyType: notifyType},
            success: function(respond) {
                if (respond.data) {
                    _contactList = respond.data;
                    $('#promoCustomerDiv').removeClass('hidden');
                    $('#promoCustomerListTable').removeClass('hidden');
                    $('#promoCustomerListTable').find(".promoCustomers").remove();

                    for (var i in respond.data) {
                        var $customerSample = $('#customerListSampleRow', '#promoCustomerListTable').clone();

                        // var firstName = respond.data[i].firstName;
                        // var lastName = (respond.data[i].lastName != '' && respond.data[i].lastName != null) ? respond.data[i].lastName : '';
                        // var fullName = firstName+' '+lastName;


                        $customerSample.find('.listCode').html(respond.data[i].contactListsCode);
                        $customerSample.find('.listName').html(respond.data[i].contactListsName);
                        $customerSample.find(".listNoOFContact").html(respond.data[i].contactListsNoOfContacts);

                        $customerSample.find("input[name='contactsList[]']").val(i);
                        $customerSample.attr('id', '');
                        $customerSample.attr('data-contact-id', respond.data[i].contactListsID);
                        $customerSample.addClass('promoCustomers');
                        $customerSample.removeClass('hidden');
                        $customerSample.removeClass('hidden');
                        $('.select-all-div').removeClass('hidden');
                        $customerSample.insertBefore($('#customerListSampleRow', '#promoCustomerListTable'));
                    }
                } else {
                    $('#promoCustomerDiv').addClass('hidden');
                    $('#promoCustomerListTable').addClass('hidden');
                    $('#promoCustomerListTable').find(".promoCustomers").remove();
                    p_notification('info', eb.getMessage('ERR_PROMO_EMAIL_CUST_LIST'));
                }
            }
        });
    });

    
    $('#promoEmailsDiv').on('click', '#emailPromoNext', function() {
        selectedPromoContactsEmails = [];
        selectedPromoContactsList = [];
        if ($('.promoCustomerSelect', '#promoCustomerTable').is(':checked')) {
            $('.promoCustomerSelect', '#promoCustomerTable').each(function() {
                if ($(this).is(':checked')) {
                    var contactID = $(this).parents('tr').attr('data-cont-id');
                    selectedPromoContactsEmails.push(contactID);
                }
            });


            $('#promoEmailsDiv').addClass('hidden');
            $('#promoEmailsWindow').removeClass('hidden');

        } 
        
        if ($(".promoContactListSelect", "#promoCustomerListTable").is(":checked")) {
            $(".promoContactListSelect", "#promoCustomerListTable").each(function (params) {
                if ($(this).is(":checked")) {
                    var contactListID = $(this).parents('tr').attr('data-contact-id');
                    selectedPromoContactsList.push(contactListID);
                }
            })  
        }
        
        if(selectedPromoContactsList.length > 0 || selectedPromoContactsEmails.length > 0){
            // var _concatArray = [...selectedPromoContactsList, ...selectedPromoContactsNums];
            // $("#smsPromotionSelectedCustomers").val(
            //     _concatArray
            // );

            $('#promoEmailsDiv').addClass('hidden');
            $('#promoEmailsWindow').removeClass('hidden');
        }else {
            p_notification(false, eb.getMessage("ERR_PROMO_EMAIL_CONT_EMPTY"));
        }
    });

    $("#promoEmailsDiv").on("click", "#smsPromoNext", function () {
        
        selectedPromoContactsNums = [];
        selectedPromoContactsList = [];

        // Check is contact selected 
        if ($(".promoCustomerSelect", "#promoCustomerTable").is(":checked")) {
            $(".promoCustomerSelect", "#promoCustomerTable").each(function () {
                if ($(this).is(":checked")) {
                    var contactID = $(this).parents('tr').attr('data-cont-id');
                    selectedPromoContactsNums.push(
                        promoContactList[contactID].firstName
                    );
                }
            });
            
        }
      
        if ($(".promoContactListSelect", "#promoCustomerListTable").is(":checked")) {
            $(".promoContactListSelect", "#promoCustomerListTable").each(function (params) {
                if ($(this).is(":checked")) {
                    var contactListID = $(this).parents('tr').attr('data-contact-id');
                    selectedPromoContactsList.push(
                        _contactList[contactListID].contactListsName
                    );
                }
            })  
        }
        
        if(selectedPromoContactsList.length > 0 || selectedPromoContactsNums.length > 0){
            var _concatArray = [...selectedPromoContactsList, ...selectedPromoContactsNums];
            $("#smsPromotionSelectedCustomers").val(
                _concatArray
            );
            $("#promoEmailsDiv").addClass("hidden");
            $("#promoSmsWindow").removeClass("hidden");
            // console.log("concate :: ",_concatArray);
            // console.log(selectedPromoContactsList);
        }else {
            p_notification(false, eb.getMessage("ERR_PROMO_EMAIL_CONT_EMPTY"));
        }
    });

    $("#promoSmsWindow").on("click", "#promoSmsSave", function () {
        
         if (selectedPromoContactsNums.length == 0 && selectedPromoContactsList.length == 0) {
           p_notification(false, eb.getMessage("ERR_PROMO_SMS_CONT_EMPTY"));
           return false;
         } else {
           tinymce.triggerSave();
           var formData = new FormData($("#promoEmailForm")[0]);
        //    console.log(formData);return;
           eb.ajax({
             type: "POST",
             url: BASE_URL + "/api/promotions/saveDirectPromotionSms",
             data: formData,
             success: function (respond) {
               if (respond.status) {
                   if (respond.data.error_msg.length > 0) {
                     p_notification(false, respond.data.error_msg);
                   }
                   if (respond.data.success_msg.length > 0) {
                     p_notification(true, respond.data.success_msg);
                   }
                   window.setTimeout(function () {
                     location.reload();
                   }, 1500);
               } else {
                 p_notification(false, respond.msg);
               }
             },
             cache: false,
             contentType: false,
             processData: false,
           });
         }
    });

    $('#promoEmailForm').on('submit', function(e) {
        e.preventDefault();
        //alert("working");
        if (jQuery.isEmptyObject(selectedPromoContactsEmails) && jQuery.isEmptyObject(selectedPromoContactsList)) {
            p_notification(false, eb.getMessage('ERR_PROMO_SMS_CONT_EMPTY'));
            return false;
        } else {
            tinymce.triggerSave();
            var formData = new FormData($(this)[0]);
            eb.ajax({
                type: 'POST',
                url: BASE_URL + '/api/promotions/saveDirectPromotionEmail',
                data: formData,
                success: function(respond) {
                    if (respond.status) {
                        window.location.reload();
                    } else {
                        p_notification(false, respond.msg);
                    }
                },
                cache: false,
                contentType: false,
                processData: false
            });

        }
    });

    $('.reset').on('click', function(e) {
        resetWindow();
    });

    function resetWindow() {
        $('#promoCustomerTable').addClass('hidden');
        $('#promoCustomerDiv').addClass('hidden');
        $('#promoCustomerTable').find(".promoCustomers").remove();
        selectedPromoContactsEmails = [];
        selectedPromoContactsNums = [];
        selectedPromoContactsList = [];
        $('.table-div').removeClass('test-body');
        $('.select-all-div').addClass('hidden');
    }

    $(document).on('click', '.view-list', function () {
        var $row = $(this).parents('tr');
        setDataToHistoryModal($row.attr('data-contact-id'));
        $('#contactListViewModal').modal('show');
    });

    function setDataToHistoryModal(contactListId) {
        eb.ajax({
            type: 'POST',
            url: BASE_URL + '/contactsAPI/getContactListDetailsById',
            data: {contactListID: contactListId,isRequestContact:1},
            success: function(respond) {
                if (respond.data) {
                    $('#lblContactListCode').html(respond.data.code);
                    $('#lblContactListName').html(respond.data.name);

                    if(respond.data.contactList){
                        $('#contactTable').removeClass('hidden');
                        $('#contactTable').find(".promoContact").remove();
                        for (var i in respond.data.contactList) {
                            var $customerSample = $('#conSampleRow', '#contactTable').clone();
                            $customerSample.find('.contactName').html(respond.data.contactList[i].name);
                            $customerSample.find('.contactDesignation').html(respond.data.contactList[i].designation);
                            $customerSample
                                .find(".contactPhoneNumber")
                                .html(respond.data.contactList[i].mobileNumber);
                            $customerSample.find('.contactEmail').html(respond.data.contactList[i].emailAddress);
                            
                            $customerSample.attr('id', '');
                            $customerSample.addClass('promoContact');
                            $customerSample.removeClass('hidden');
                            $customerSample.insertBefore($('#conSampleRow', '#contactTable'));
                            
                        }
                    }
                } else {
                    $('#contactTable').addClass('hidden');
                    $('#contactTable').find(".promoContact").remove();
                    p_notification('info', eb.getMessage('ERR_PROMO_EMAIL_CUST_LIST'));
                }
            }
        });
    }

    $(document).on("change", ".promoCustomerSelect", "#promoCustomerTable", function (params) {
        if ($(this).is(":checked")) {
            selectedContactCount++;
            
        }else{
            selectedContactCount--;
        }
        $('#selectedContact').html(selectedContactCount);
    });

    $(document).on("change", ".promoContactListSelect", "#promoCustomerListTable", function (params) {
        if ($(this).is(":checked")) {
            selectedListCount++;
            
        }else{
            selectedListCount--;
        }
        $('#selectedContactList').html(selectedListCount);
    });

    $('#viewDraftMsg').on('click', function (e) {
        setDataToDraftMsgModal();
        $('#draftMessagesViewModal').modal('show');
    })

    function setDataToDraftMsgModal(){
        eb.ajax({
            type: 'POST',
            url: BASE_URL + '/api/draft-message/getDraftMessageList',
            data: {},
            success: function(respond) {
                if (respond.data) {
                    $('#draftMessageTable').removeClass('hidden');
                    $('#draftMessageTable').find(".draftMessage").remove();

                    for (var i in respond.data) {
                        var $customerSample = $('#draftMessageSampleRow', '#draftMessageTable').clone();
                        $customerSample.find('.msgName').html(respond.data[i].draftMessageName);
                        $customerSample.find('.msgDate').html(respond.data[i].draftMessageDate);
                        $customerSample.find('.cpyButton').attr('data-message',respond.data[i].draftMessageMessage);
                        $customerSample.find('.cpyButton').attr('data-id',respond.data[i].draftMessageID);
                        $customerSample.find('.cpyButton').attr('data-message-name',respond.data[i].draftMessageName);
                        // $customerSample.find('.cpTxt').val(respond.data[i].draftMessageMessage);
                        
                        // $customerSample.find('.cpTxt').attr('id', 'msg'+i);
                        $customerSample.addClass('draftMessage');
                        $customerSample.removeClass('hidden');
                        $customerSample.insertBefore($('#draftMessageSampleRow', '#draftMessageTable'));
                    }
                } else {
                    $('#draftMessageTable').addClass('hidden');
                    $('#draftMessageTable').find(".draftMessage").remove();
                    p_notification('info', eb.getMessage('ERR_PROMO_EMAIL_CUST_LIST'));
                }
            }
        });
    }

    $('#saveDraft').on('click', function(e) {
        msgObj.draftMessageName = $('#messageName').val();
        msgObj.draftMessageMessage = $('#smsBody').val();
        msgObj.draftMessageID = $('#draftMessageID').val();

        if(validateMessageData(msgObj)){
           var url = msgObj.draftMessageID ? 'update' : 'save'
            eb.ajax({
                type: 'POST',
                url: BASE_URL + '/api/draft-message/'+url,
                data: msgObj,
                success: function (respond) {
                    p_notification(respond.status, respond.msg);
                    if(respond.status && msgObj.draftMessageID.length == 0){
                        console.log(respond.data);
                        $('#draftMessageID').val(respond.data);
                    }
                }
            });
        }
    })

    function validateMessageData(input) {
        if (input.draftMessageName == '') {
            p_notification(false, eb.getMessage('ERR_PROMO_MSG_NAME'));
            $("input[name='messageName']").focus();
            return false;
        } else if (input.draftMessageMessage == '') {
            p_notification(false, eb.getMessage('ERR_PROMO_MSG'));
            $("input[name='smsBody']").focus();
            return false;
        }  else {
            return true;
        }
    }

    
});

$(document).on('click', '.cpyButton', function (e){
    e.preventDefault();
    message = $(this).attr('data-message');
    messageId = $(this).attr('data-id');
    messageName = $(this).attr('data-message-name');
// console.log(messageId);
    $('#smsBody').val(message);
    $('#draftMessageID').val(messageId);
    $('#messageName').val(messageName);

    $('#draftMessagesViewModal').modal('hide');
});

function copyToClipboard(text) {
    console.log(text);
    var $temp = $("<input>");
    $("body").append($temp);
    $temp.val(text).select();
    document.execCommand("copy");
    $temp.remove();
}