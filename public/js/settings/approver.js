$(document).ready(function() { 
    var nowTemp = new Date();
    var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);
    var nowTemp = new Date();
    var now1 = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);
    var myDate = new Date();
    var prettyDate = myDate.getFullYear() + '-' + (myDate.getMonth() + 1) + '-' + (myDate.getDate() < 10 ? '0' : '') + myDate.getDate();
    var day = myDate.getDate() < 10 ? '0' + myDate.getDate() : myDate.getDate();
    var month = (myDate.getMonth() + 1) < 10 ? '0' + (myDate.getMonth() + 1) : (myDate.getMonth() + 1);
    $("#employeeDOB").val(eb.getDateForDocumentEdit('#employeeDOB', day, month, myDate.getFullYear()));
    var checkin = $('#employeeDOB').datepicker({onRender: function(date) {
            return date.valueOf() < now.valueOf() ? ((U_DATE_R_OVERRIDE) ? '' : 'disabled') : '';
        },
        //  format: 'yyyy-mm-dd',
    }).on('changeDate', function(ev) {
        checkin.hide();
    }).data('datepicker');

    var employeeId   = null;
    var employeeFirstName = null;
    var employeeSecondName = null;
    var employeeAddress = null;
    var employeeTP = null;
    var employeeEmail = null;
    var employeeStatus = null;
    var employeeIDNo = null;
    var employeeDOB = null;
    var employeeGender = null;
    var entityID = null;
    

    
    $('#btnAdd').on('click',function(){
        employeeFirstName = $('#employeeFirstName').val();
        employeeSecondName = $('#employeeLastName').val();
        employeeTP = $('#employeeMobileNumber').val();
        employeeAddress = $('#employeeAddress').val();
        employeeIDNo = $('#employeeIdentityReference').val();
        employeeEmail = $('#employeeEmail').val();
        employeeDOB = $('#employeeDOB').val();
        employeeGender = $('input[name=gender]:checked').val();


        var formData = {
            employeeFirstName: employeeFirstName,
            employeeSecondName: employeeSecondName, 
            employeeAddress: employeeAddress, 
            employeeTP: employeeTP, 
            employeeEmail: employeeEmail,
            employeeIDNo: employeeIDNo,
            employeeDOB: employeeDOB,
            employeeGender: employeeGender
        }

        if (validateFormData(formData)) { 
            
            if (employeeTP.trim()) {
                var phoneno = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;  
                if (!(employeeTP.match(phoneno)))  { 
                    p_notification(false, eb.getMessage('ERR_TP_NUM'));
                    return;
                }
            }

            if (employeeEmail.trim()) {
                var emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

                if (!(emailRegex.test(employeeEmail))) {
                    p_notification(false, eb.getMessage('ERR_EMAIL_NOT_VALID'));
                    return;
                }
            }

            eb.ajax({
                type: 'POST',
                url: BASE_URL + '/api/approver/createApprover',
                data: formData,
                success: function(respond) {
                    p_notification(respond.status, respond.msg);                
                    if(respond.status){
                        window.setTimeout(function() {
                                location.reload();
                        }, 1000);                    
                    }                
                }
            });
        }
    });

    //for cancel btn
    $('#btnCancel').on('click',function (){
        $('input[type=text]').val('');
        $('input[type=email]').val('');  
        employeeRegisterView();        
    });

    $('#employee-list').on('click','.employee-action',function (e){    
        var action = $(this).data('action');
        employeeId   = $(this).closest('tr').data('employee-id');
        employeeFirstName = $(this).closest('tr').data('employee-first-name');
        employeeSecondName = $(this).closest('tr').data('employee-last-name');
        employeeAddress = $(this).closest('tr').data('employee-address');
        employeeTP = $(this).closest('tr').data('employee-employee-tp');
        employeeEmail = $(this).closest('tr').data('employee-email');
        entityID = $(this).closest('tr').data('entity-id');
        employeeIDNo = $(this).closest('tr').data('employee-id-no');
        employeeDOB = $(this).closest('tr').data('employee-dob');
        employeeGender = $(this).closest('tr').data('employee-gender');
        status = $(this).closest('tr').data('employee-status');

        switch(action) {
            case 'edit':
                e.preventDefault();
                employeeUpdateView();
                //set selected department details
                $('#employeeFirstName').val(employeeFirstName);
                $('#employeeLastName').val(employeeSecondName);
                $('#employeeAddress').val(employeeAddress);
                $('#employeeMobileNumber').val(employeeTP);
                $('#employeeEmail').val(employeeEmail);
                $('#employeeIdentityReference').val(employeeIDNo);
                $('#employeeDOB').val(employeeDOB);

                break;

            case 'status':
                var msg;
                var status;
                var currentDiv = $(this).contents();
                var flag = true;
                if ($(this).children().hasClass('fa-check-square-o')) {
                    msg = 'Are you sure you want to Deactivate this approver';
                    status = '0';
                }
                else if ($(this).children().hasClass('fa-square-o')) {
                    msg = 'Are you sure you want to Activate this approver';
                    status = '1';
                }
                activeInactive(employeeId, status, currentDiv, msg, flag);
                break;
            case 'delete':
                deleteEmployee(employeeId);
                break;
            default:
                console.error('Invalid action.');
                break;
        }
    });

    //this function for switch to employee register view
    function employeeRegisterView(){
       $('.updateTitle').addClass('hidden');
       $('.addTitle').removeClass('hidden');
       $('.updateDiv').addClass('hidden');
       $('.addDiv').removeClass('hidden');
    }

    //this function for switch to employee update view
    function employeeUpdateView(){
       $('.addTitle').addClass('hidden');
       $('.updateTitle').removeClass('hidden');
       $('.addDiv').addClass('hidden');
       $('.updateDiv').removeClass('hidden');
    }

    /**
     *
     *use to change active state
     */
    function activeInactive(type, status, currentDiv, msg, flag) {
        bootbox.confirm(msg, function($result) {
            if ($result == true) {
                eb.ajax({
                    url: BASE_URL + '/api/approver/changeStatusID',
                    method: 'post',
                    dataType: 'json',
                    data: {
                        'officialID': type,
                        'status': status,
                    },
                    success: function(data) {
                        if (data.status) {
                            p_notification(data.status, data.msg);
                            if (currentDiv.hasClass('fa-check-square-o') && flag) {
                                currentDiv.removeClass('fa-check-square-o');
                                currentDiv.addClass('fa-square-o');
                            }
                            else {
                                currentDiv.removeClass('fa-square-o');
                                currentDiv.addClass('fa-check-square-o');
                            }
                        } else {
                            p_notification(data.status, data.msg);
                        }
                    }
                });
            }
        });
    }

    $('#btnUpdate').on('click',function(){
        employeeFirstName = $('#employeeFirstName').val();
        employeeSecondName = $('#employeeLastName').val();
        employeeTP = $('#employeeMobileNumber').val();
        employeeAddress = $('#employeeAddress').val();
        employeeIDNo = $('#employeeIdentityReference').val();
        employeeEmail = $('#employeeEmail').val();
        employeeDOB = $('#employeeDOB').val();
        employeeGender = $('input[name=gender]:checked').val();


        var formData = {
            employeeFirstName: employeeFirstName,
            employeeSecondName: employeeSecondName, 
            employeeAddress: employeeAddress, 
            employeeTP: employeeTP, 
            employeeEmail: employeeEmail,
            employeeIDNo: employeeIDNo,
            employeeDOB: employeeDOB,
            employeeGender: employeeGender,
            employeeID: employeeId,
            entityID: entityID
        }        
        if(validateFormData(formData)){
            
            if (employeeTP.trim()) {
                var phoneno = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;  
                if (!(employeeTP.match(phoneno)))  { 
                    p_notification(false, eb.getMessage('ERR_TP_NUM'));
                    return;
                }
            }

            if (employeeEmail.trim()) {
                var emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

                if (!(emailRegex.test(employeeEmail))) {
                    p_notification(false, eb.getMessage('ERR_EMAIL_NOT_VALID'));
                    return;
                }
            }

            eb.ajax({
                type: 'POST',
                url: BASE_URL + '/api/approver/updateApprover',
                data: formData,
                success: function(respond) {
                    p_notification(respond.status, respond.msg);                
                    if(respond.status){
                        window.setTimeout(function() {
                                location.reload();
                        }, 1000);                  
                    }                
                }
            });
        } 
    });

    //for search
    $('#searchEmp').on('click', function() {
        var key = $('#employeeSearch').val();
        getSearchResults(key);
        
    });

    //for rest btn
    $('#btnReset').on('click',function (){
        $('input[type=text]').val('');
        $('input[type=email]').val('');  
        $('select[name=employeeDept]').val('');
        $('select[name=employeeDesig]').val('');
        $('.selectpicker').selectpicker('refresh');
    });  

    //for search cancel btn
    $('#searchCancel').on('click',function (){
        $('#employeeSearch').val('');
        $('select[name=employeeDeptSearch]').val('');
        $('select[name=employeeDesigSearch]').val('');
        $('.selectpicker').selectpicker('refresh');  
        getSearchResults();
    });

    function getSearchResults(key = null) {
        $.ajax({
            type: 'POST',
            url: BASE_URL + '/api/approver/searchApprover',
            data: {
                searchKey : key
            },
            success: function(data) {
                if (data.status == true) {
                    if (data.status) {
                        $("#employee-list").html(data.html);
                    } else {
                        p_notification(data.status, data.msg);
                    }
                }            
            }
        });
    }

    function deleteEmployee(employeeId) {
        bootbox.confirm('Are you sure you want to delete this approver?', function($result) {
            if ($result == true) {
                eb.ajax({
                    url: BASE_URL + '/api/approver/deleteApprover',
                    method: 'post',
                    data: {
                        employeeID: employeeId,
                        entityID: entityID
                    },
                    dataType: 'json',
                    success: function(data) {
                        if (data.status == true) {
                            p_notification(data.status, data.msg);
                            window.location.reload();
                        } else {
                            p_notification(data.status, data.msg);
                        }
                    }
                });
            }
        });
    }

    function validateFormData(formData)
    {
        if (formData.employeeIDNo == '') {
            p_notification(false, eb.getMessage('ERR_APPRO_IDNO_CNT_BE_NULL'));
            return false;
        } else if (formData.employeeEmail == '') {
            p_notification(false, eb.getMessage('ERR_APPRO_EMAIL_CNT_BE_NULL'));
            return false;
        } else if (formData.employeeFirstName == '') {
            p_notification(false, eb.getMessage('ERR_APPRO_FNAME_CNT_BE_NULL'));
            return false;
        } 
        return true;
    }
});
