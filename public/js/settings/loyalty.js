$(document).ready(function() {

	loadDropDownFromDatabase('/accounts-api/search-active-accounts-for-dropdown', "", "", '#loyaltyGlAccountID');

	var accountID = $('#loyaltyGlAccountID').data('id');
	if(!(accountID == null || accountID == 0)){
		var accountName = $('#loyaltyGlAccountID').data('value');
    	$('#loyaltyGlAccountID').append("<option value='"+accountID+"'>"+accountName+"</option>")
    	$('#loyaltyGlAccountID').val(accountID).selectpicker('refresh');
	}

    $('#loyalty_form').submit(function(e) {
        var card_name = $('[name=loyaltyName]').val();
        var no_of_cards = $('[name=loyaltyNoOfCards]').val();
        var code_length = $('[name=loyaltyCodeDegits]').val();
        var starting_no = $('[name=loyaltyCodeCurrentNo]').val();
        var min_val = $('[name=loyaltyActiveMinVal]').val();
        var max_val = $('[name=loyaltyActiveMaxVal]').val();
        var earning = $('[name=loyaltyEarningPerPoint]').val();
        var redeem = $('[name=loyaltyRedeemPerPoint]').val();

        if (!card_name.replace(/\s/g, '').length) {
            p_notification(false, "Loyalty card name cannot be empty.");
            return false;
        }
        if (!/^[0-9]+$/.test(no_of_cards)) {
            p_notification(false, "No of cards is not in a valid format.");
            return false;
        }
        if (no_of_cards == 0) {
            p_notification(false, "No of cards cannot be zero.");
            return false;
        }
        if (!code_length.replace(/\s/g, '').length) {
            p_notification(false, "No. of degits cannot be empty");
            return false;
        }
        if(code_length == 0){
            p_notification(false, "Number of digits cannot be zero.");
            return false;
        }
        if (!/^[0-9]+$/.test(code_length)) {
            p_notification(false, "No. of degits accepts only numeric values.");
            return false;
        }
        if (!starting_no.replace(/\s/g, '').length) {
            p_notification(false, "Starting no. cannot be empty");
            return false;
        }
        if (!/^[0-9]+$/.test(starting_no)) {
            p_notification(false, "Starting no. accepts only numeric values.");
            return false;
        }
        if(starting_no == 0){
            p_notification(false, "Starting no. cannot be zero.");
            return false;
        }
        if (min_val.replace(/\s/g, '').length && !/^[0-9]+$/.test(min_val)) {
            p_notification(false, "Minimum value accepts only numeric values.");
            return false;
        }
        if (max_val.replace(/\s/g, '').length && !/^[0-9]+$/.test(max_val)) {
            p_notification(false, "Maximum value accepts only numeric values.");
            return false;
        }
        if (!earning.replace(/\s/g, '').length) {
            p_notification(false, "Loyalty point earning ratio cannot be empty");
            return false;
        }
        if (!/^[\d\.\d]+$/.test(earning)) {
            p_notification(false, "Please enter valid amount for loyalty point earning ratio.");
            return false;
        }
        if (earning == 0) {
            p_notification(false, "Loyalty point earning ratio cannot be zero.");
            return false;
        }
        if (!redeem.replace(/\s/g, '').length) {
            p_notification(false, "Loyalty point redeem ratio cannot be empty");
            return false;
        }
        if (!/^[\d\.\d]+$/.test(redeem)) {
            p_notification(false, "Please enter valid amount for  loyalty point redeem ratio.");
            return false;
        }
        if (redeem == 0) {
            p_notification(false, "Loyalty point redeem ratio cannot be zero.");
            return false;
        }

        $('.saveLoyalty').addClass('disabled');
    });

    $('#loyaltyStatus').bootstrapSwitch();
});