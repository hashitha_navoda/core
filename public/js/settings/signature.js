$(document).ready(function() {
    $('#profile_pic').hide();
    $('#thumbs').hide();
    $('#image-submit').on('click', function (e) {
        input = document.getElementById('image_file');
        var file = input.files[0];
        if (file) {
            formdata = false;
            if (window.FormData) {
                formdata = new FormData();
            }
            e.preventDefault();
            formdata = eb.getImageDimension("images", file, formdata, '200', '200', '0', '0', '200', '200');
            eb.ajax({
                url: BASE_URL + '/image-preview',
                type: 'POST',
                processData: false,
                contentType: false,
                data: formdata,
                success: function (data) {
                    //$('#thumbs').html('<img src="' + data + '" />');
                    $('#thumbs').attr('src', data);
                    $('#imageUploaderModal').modal('hide');
                    $('#image_file').data('flag', true);
                    return data;
                }
            });
        }
    });



    var dimensionValue = null;
    var dimensionValues = {};
    var mainDimensionValues = {};
    var dimensionId = null;
    var rowIncrement = 1;
    var editFlag = false;
 
    $('#btnAdd').on('click',function(){
        var signatureHolderName = $('#signatureHolderName').val();

        var formData = {
            signatureHolderName: signatureHolderName
        }
    
        if(validateFormData(formData)){
            eb.ajax({
                type: 'POST',
                url: BASE_URL + '/signature-api/saveSignature',
                data: {
                    signatureHolderName : signatureHolderName,
                },
                success: function(respond) {
                    p_notification(respond.status, respond.msg);                
                    if(respond.status){
                        // $('#dimensionName').val('');
                        // window.setTimeout(function() {
                        //         location.reload();
                        // }, 1000);

                        input = document.getElementById('image_file');
                        var file;
                        file = input.files[0];
                        if (file) {
                            imagedata = false;
                            if (window.FormData) {
                                imagedata = new FormData();
                            }
                            // e.preventDefault();
                            imagedata.append("ID", respond.data.signatureDetailsID);
                            imagedata = eb.getImageDimension("images", file, imagedata, '200', '200', '0', '0', '200', '200');
                            eb.ajax({
                                url: BASE_URL + '/signature-api/preview',
                                type: 'POST',
                                processData: false,
                                contentType: false,
                                data: imagedata,
                                success: function(res) {
                                    if (res) {
                                        window.setTimeout(function() {
                                            location.reload();
                                        }, 1000);
                                        // if (!EDIT_MODE) {
                                        //     window.location = (BASE_URL + '/product/location-product-details/' + data.data.productID);
                                        // } else {
                                        //     // redirecting to the same page because location product rows need to update
                                        //     // eg: updating product discounts should update locationwise values too
                                        //     // TODO - use ajax to load the view without refreshing the page
                                        //     window.location = (BASE_URL + '/product/edit/' + data.data.productID);
                                        // }
                                        // imageID = res[0];
                                    }
                                }
                            });
                        } else {
                            window.setTimeout(function() {
                                location.reload();
                            }, 1000);
                        }                    
                    }                
                }
            });
        } 
    });

    //for cancel btn
    $('#btnCancel, #btnViewCancel').on('click',function (){
        $('input[type=text]').val('');  
        $('#dimensionValues').text('Add values for dimension');      
    });

    $('#signature-list').on('click','.signature-action',function (e){    
        var action = $(this).data('action');
        var signatureImageId   = $(this).closest('tr').data('signature-image-id');
        var signatureId   = $(this).closest('tr').data('signature-id');
        var companyFolder   = $(this).closest('tr').data('company-folder');
        // var dimensionName = $(this).closest('tr').data('dimension-name');

        switch(action) {
            case 'delete':
                deleteSignature(signatureId);
                break;
            case 'view':
                e.preventDefault();    
                var previewId = document.getElementById('load_view_img');

                var src = '/userfiles/'+companyFolder+'/signature/thumbs/'+signatureImageId;
                previewId.src = src;
                $('#imageViewModal').modal('show');
                break;

            default:
                console.error('Invalid action.');
                break;
        }
    });

    // $('#btnUpdate').on('click',function(){
    //     var dimensionName = $('#dimensionName').val();

    //     var formData = {
    //         dimensionName: dimensionName
    //     }

    //     if(validateFormData(formData)){
    //         eb.ajax({
    //             type: 'POST',
    //             url: BASE_URL + '/dimension-api/updateDimension',
    //             data: {
    //                 dimensionId: dimensionId,
    //                 dimensionName : dimensionName,
    //                 mainDimensionValues: mainDimensionValues
    //             },
    //             success: function(respond) {
    //                 p_notification(respond.status, respond.msg);                
    //                 if(respond.status){
    //                     $('#dimensionName').val('');
    //                     window.setTimeout(function() {
    //                             location.reload();
    //                     }, 1000);                    
    //                 }                
    //             }
    //         });
    //     } 
    // });

    //for search
    $('#searchSignature').on('click', function() {
        var key = $('#signatureSearch').val();

        searchSignature(key);
    });


    function searchSignature(key) {  
        $.ajax({
            type: 'POST',
            url: BASE_URL + '/signature-api/search',
            data: {
                searchKey : key
            },
            success: function(data) {
                console.log(data.status);
                if (data.status == true) {
                    if (data.status) {
                        $("#signature-list").html(data.html);
                    } else {
                        p_notification(data.status, data.msg);
                    }
                }            
            }
        });  
    }

    //for rest btn
    $('#btnReset').on('click',function (){
        $('input[type=text]').val('');
        $('#addDimensionValueModal #addDimensionValueBody .addedDimensionValue').remove();
        dimensionValues = {};
        mainDimensionValues = {};
        subTaskRates = {};
    });  

    //for search cancel btn
    $('#searchCancel').on('click',function (){
        $('#signatureSearch').val('');
        searchSignature();
    });

    
    function deleteSignature(signatureId) {
        bootbox.confirm('Are you sure you want to delete this signature?', function($result) {
            if ($result == true) {
                eb.ajax({
                    url: BASE_URL + '/signature-api/deleteSignature',
                    method: 'post',
                    data: {
                        signatureId: signatureId
                    },
                    dataType: 'json',
                    success: function(data) {
                        if (data.status == true) {
                            p_notification(data.status, data.msg);
                            window.location.reload();
                        } else {
                            p_notification(data.status, data.msg);
                        }
                    }
                });
            }
        });
    }

    
    function validateFormData(formData) {
        if (formData.signatureHolderName == "") {
            p_notification(false, eb.getMessage('ERR_SIG_HOLDER_NAME_EMPTY'));
            $('#dimensionName').focus();
            return false;
        }
        return true;
    }

});
