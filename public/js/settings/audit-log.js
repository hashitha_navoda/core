$(document).ready(function () {

$('#filter-button').on('click',function(){
	if(validateDates()){
		var fromDate = $('#from-date').val();
		var toDate = $('#to-date').val();

		eb.ajax({
            type: 'POST',
            url: BASE_URL + '/audit-log-api/searchAuditLogByDateFilter',
            data: {
            	fromDate: fromDate,
            	toDate: toDate
            },
            success: function(respond) {
                if (respond.status) {
                	$('#audit-log-list').html(respond.html);
                }
            }
        });

	}
});

$('#audit-log-list').on('click','.viewLogDetails',function(){
	var logID = $(this).data('id');
	eb.ajax({
		type: 'POST',
		url: BASE_URL + '/audit-log-api/searchAuditLogDetaislByLogID',
		data: {
			logID: logID,
		},
		success: function(respond) {
			if (respond.status) {
				$('#viewLogDetails').html(respond.html);
				$('#viewLogDetails').modal('show');
			}
		}
	});
});

function validateDates(){
	if($('#from-date').val() == ''){
		p_notification(false, eb.getMessage('ERR_AUDIT_LOG_PLEASE_SET_START_DATE'));
		$('#from-date').focus();
		return false;
	} else if($('#to-date').val() == ''){
		p_notification(false, eb.getMessage('ERR_AUDIT_LOG_PLEASE_SET_END_DATE'));
		$('#to-date').focus();
		return false;
	} else if(eb.convertDateFormat('#from-date') >= eb.convertDateFormat('#to-date')){
		p_notification(false, eb.getMessage('ERR_AUDIT_LOG_DATE_RANGE'));
		return false;
	} 
	return true;
}

var checkin = $('#from-date').datepicker().on('changeDate', function(ev) {
	checkin.hide();
}).data('datepicker');

var toDate = $('#to-date').datepicker().on('changeDate', function(ev) {
	toDate.hide();
}).data('datepicker');

});