var addCustomer = null;
$(document).ready(function() {

    var newlyInsertedEventID = null;
    $('#saveAndAddCustomer', '#addcustomerform').removeClass('hidden')

    var startDate = new Date();
    var endDate = new Date();
    $('#customerEventStartDate').datetimepicker({
        format: 'yyyy-mm-dd',
        minView: 4,
        todayBtn: true,
        autoclose: true,
    }).on('changeDate', function(ev) {
        startDate = new Date(this.value);
        var dateDiff = parseInt((endDate.getTime() - startDate.getTime()) / (24 * 3600 * 1000))
        $('#customerEventDuration').val(dateDiff);
        if (endDate && startDate.getTime() > endDate.getTime()) {
            p_notification(false, 'Invalid Date Range');
            $('#customerEventDuration').val('');
            $('#customerEventEndDate').focus();
        }
    });
    $('#customerEventEndDate').datetimepicker({
        format: 'yyyy-mm-dd',
        minView: 4,
        todayBtn: true,
        autoclose: true,
    }).on('changeDate', function(ev) {
        endDate = new Date(this.value);
        var dateDiff = parseInt((endDate.getTime() - startDate.getTime()) / (24 * 3600 * 1000))
        $('#customerEventDuration').val(dateDiff);
        if (startDate.getTime() > endDate.getTime()) {
            p_notification(false, 'Invalid Date Range');
            $('#customerEventDuration').val('');
            $('#customerEventEndDate').focus();
        }
    });
    $('#customerEventDuration').on('focusout', function() {
        var duration = $(this).val().trim();
        if (isNaN(duration.trim())) {
            $('#customerEventDuration').val('').focus();
            p_notification(false, 'Insert only integer');
        } else if (startDate != null) {
            endDate.setTime(new Date(startDate.getTime() + parseInt(duration) * 24 * 60 * 60 * 1000))
            $('#customerEventEndDate').datetimepicker('setDate', endDate);
        }
    });
    setInitialDates();
//    set Initial values
    function setInitialDates() {
        startDate = new Date();
        endDate = new Date();
        $('#customerEventStartDate').datetimepicker('setDate', startDate);
        $('#customerEventEndDate').datetimepicker('setDate', endDate);
        var dateDiff = parseInt((endDate.getTime() - startDate.getTime()) / (24 * 3600 * 1000))
        $('#customerEventDuration').val(dateDiff);
    }

    function clearAll() {
        setInitialDates();
        newlyInsertedEventID = null;
        $(".category-form-container").removeClass('update');
        $('#customerEventName').val('');
        $('#customerEventID').val('');
    }

    $('#saveAndCreateCustomer').on('click', function(e) {
        e.preventDefault();
        addCustomer = true;
        $('#create-customer-event-form').submit();

    });

    $('#saveAndAddCustomer', '#addcustomerform').on('click', function(e) {
        e.preventDefault();
        addCustomer = true;
        $('#customerFormsubmit').click();

    });

    $("#customer_more").hide();
    $('#addCustomerModal').on('shown.bs.modal', function() {
        $(this).find("input[name='customerName']").focus();
    });

    //more details button
    $("#moredetails").click(function() {
        $("#customer_more").slideDown();
        $('#moredetails').hide();
        $('#customerCurrentBalance', '#addcustomerform').attr('disabled', true);
        $('#customerCurrentCredit', '#addcustomerform').attr('disabled', true);
    });

    $('#create-customer-event-form').submit(function(e) {
        e.preventDefault();
        var form_data = {
            customerEventName: $('#customerEventName').val(),
            customerEventDuration: $('#customerEventDuration').val(),
            customerEventStartDate: $('#customerEventStartDate').val(),
            customerEventEndDate: $('#customerEventEndDate').val(),
        };

        var submitted_btn = $("[type='submit']:visible", $(this)).parents('.button-set');
        var update = submitted_btn.hasClass('update');
        if (update) {
            form_data = $.extend(form_data, {customerEventID: $("[name='customerEventID']", this).val()});
        }

        var action = (update) ? 'update' : 'save';

        if (validate_form(form_data)) {
            eb.ajax({
                url: BASE_URL + '/api/customer-event/' + action + '/' + getCurrPage(),
                method: 'post',
                data: form_data,
                success: function(respond) {
                    p_notification(respond.status, respond.msg);
                    if (respond.status) {
                        clearAll();
                        if (respond.data != null && respond.data.key) {
                            $('#customerEvent', '#addcustomerform').append($("<option></option>")
                                    .attr("value", respond.data.key)
                                    .text(respond.data.value));
                            newlyInsertedEventID = respond.data.key;
                        }
                        if (addCustomer) {
                            $('#customerEvent', '#addcustomerform').val(newlyInsertedEventID);
                            $('#addCustomerModal').modal('show');
                        }
                        addCustomer = false;
                        $('#customer-event-list').html(respond.html);

                    }
                }
            });
        }
        return false;
    });

    $('#customer-event-list').on('click', 'a.status', function(e) {
        e.preventDefault();
        customerEventID = $(this).parents('tr').attr('data-customereventid');
        eb.ajax({
            url: BASE_URL + '/api/customer-event/update-status/' + getCurrPage(),
            data: {customerEventID: customerEventID},
            method: 'POST',
            success: function(respond) {
                p_notification(respond.status, respond.msg);
                if (respond.status) {
                    $('#customer-event-list').html(respond.html);
                }
            }
        });
        return false;
    });

    $('#customer-event-list').on('click', 'a.edit', function(e) {
        e.preventDefault();
        customerEventID = $(this).parents('tr').attr('data-customereventid');
        eb.ajax({
            url: BASE_URL + '/api/customer-event/edit/' + getCurrPage(),
            data: {customerEventID: customerEventID},
            method: 'POST',
            success: function(respond) {
                if (respond.status) {
                    $('#customerEventID').val(respond.data.customerEventID);
                    $('#customerEventName').val(respond.data.customerEventName);
                    $('#customerEventDuration').val(respond.data.customerEventDuration);
                    $('#customerEventStartDate').val(respond.data.customerEventStartDate);
                    $('#customerEventEndDate').val(respond.data.customerEventEndDate);
                    $("html, body").animate({scrollTop: $(".category-form-container").offset().top - 80});
                    $(".category-form-container").addClass('update');
                } else {
                    p_notification(respond.status, respond.msg);
                }
            }
        });
        return false;
    });

    $('#create-customer-event-form').on('reset', function(e) {
        e.preventDefault();
        clearAll();
        return false;
    });

    $('#customer-event-list').on('click', 'a.delete', function(e) {
        e.preventDefault();
        customerEventID = $(this).parents('tr').attr('data-customereventid');
        bootbox.confirm("Are You Sure To Delete This Customer Type?", function(result) {
            if (result === true) {
                eb.ajax({
                    url: BASE_URL + '/api/customer-event/delete/' + getCurrPage(),
                    data: {customerEventID: customerEventID},
                    method: 'POST',
                    success: function(respond) {
                        p_notification(respond.status, respond.msg);
                        if (respond.status) {
                            $('#customer-event-list').html(respond.html);
                        }
                    }
                });
            }
        });
        return false;
    });

    $('#customer-event-search-form').submit(function(e) {
        e.preventDefault();
        eb.ajax({
            url: BASE_URL + '/api/customer-event/search/' + getCurrPage(),
            data: {searchKey: $('#customer-event-search-keyword').val().trim()},
            method: 'POST',
            success: function(respond) {
                if (respond.status) {
                    $('#customer-event-list').html(respond.html);
                } else {
                    p_notification(respond.status, respond.msg);
                }
                $('#customer-event-search-keyword').val('');
            }
        });
        return false;
    });

    $('#customer-event-search-form').on('reset', function(e) {
        e.preventDefault();
        $('#customer-event-search-keyword').val('');
        $('#customer-event-search-form').submit();
        return false;
    });

    function validate_form(form_data) {
        var name = form_data.customerEventName;
        var duration = form_data.customerEventDuration;
        var startDate = form_data.customerEventStartDate;
        var endDate = form_data.customerEventEndDate;

        if (name === null || name === '') {
            p_notification(false, eb.getMessage('ERR_CUS_EVENT_NAME_EMPTY'));
            $('#customerEventName').focus();
            return false;
        } else if (duration === null || duration === '') {
            p_notification(false, eb.getMessage('ERR_CUS_EVENT_DURATION_EMPTY'));
            $('#customerEventDuration').focus();
            return false;
        } else if (isNaN(duration) || duration < 0) {
            p_notification(false, eb.getMessage('ERR_CUS_EVENT_DURATION_VALID'));
            $('#customerEventDuration').focus();
            return false;
        } else if (startDate === null || startDate === '') {
            p_notification(false, eb.getMessage('ERR_CUS_EVENT_STARTDATE_EMPTY'));
            $('#customerEventStartDate').focus();
            return false;
        } else if (endDate === null || endDate == '') {
            p_notification(false, eb.getMessage('ERR_CUS_EVENT_ENDDATE_EMPTY'));
            $('#customerEventEndDate').focus();
            return false;
        } else {
            return true;
        }

    }
});