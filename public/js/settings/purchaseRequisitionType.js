$(document).ready(function () {

	loadDropDownFromDatabase('/accounts-api/search-active-accounts-for-dropdown', "", "", '#expenseTypeAccountID');

    $('#expenseCategoryAddForm').on('submit', function (e) {
        e.preventDefault();
        var expenseCategoryName = $("input[name='expenseCategoryName']", '#expenseCategoryAddForm').val();
        var expenseCategoryParent = $("select[name='expenseCategoryParent']", '#expenseCategoryAddForm').val();
        if (expenseCategoryName == '') {
            p_notification(false, eb.getMessage('ERR_EXPCAT_NAME'));
            return false;
        } else {
            if ($("input[type='submit']", '#expenseCategoryAddForm').val() == 'Update') {
                var url = BASE_URL + '/purchase-requisition-category-api/update';
                var updateId = $("input[type='submit']", '#expenseCategoryAddForm').attr('id');
            } else {
                var url = BASE_URL + '/purchase-requisition-category-api/add';
                var updateId = '0';
            }
            eb.ajax({
                type: 'POST',
                url: url,
                data: {expenseCategoryName: expenseCategoryName, expenseCategoryParent: expenseCategoryParent, expenseCategoryId: updateId},
                success: function (respond) {
                    if (respond.status = true) {
                        p_notification(true, respond.msg);
                        window.setTimeout(function() {
                                location.reload();
                        }, 1000);
                    } else {
                        p_notification(false, respond.msg);
                    }
                },
            });
        }
    });

    $('#expenseCategoryList').on('click', '.changeStatus', function () {
        var expenseCategoryId = $(this).attr('id');
        var $statusSpan = $(this).find("span");
        var changeStatusTo = ($statusSpan.attr('class') == 'glyphicon glyphicon-check') ? 'inactive' : 'active';
        eb.ajax({
            type: 'POST',
            url: BASE_URL + '/purchase-requisition-category-api/changeCategoryStatus',
            data: {expenseCategoryId: expenseCategoryId, changeStatusTo: changeStatusTo},
            success: function (respond) {
                if (respond.status) {
                    if (changeStatusTo == 'inactive') {
                        $statusSpan.removeClass('glyphicon glyphicon-check');
                        $statusSpan.addClass('glyphicon glyphicon-unchecked');
                    } else {
                        $statusSpan.removeClass('glyphicon glyphicon-unchecked');
                        $statusSpan.addClass('glyphicon glyphicon-check');
                    }
                }
            }
        });
    });

    $('#expenseCategoryList').on('click', '.delete', function () {
        var $expenseCategoryATag = $(this);
        var expenseCategoryId = $expenseCategoryATag.attr('id');
        bootbox.confirm('Are you sure you want to delete this Expense Category?', function(result) {
            if (result == true) {
                eb.ajax({
                    type: 'POST',
                    url: BASE_URL + '/purchase-requisition-category-api/delete',
                    data: {expenseCategoryId: expenseCategoryId},
                    success: function (respond) {
                        if (respond.status) {
                            p_notification(true, respond.msg);
                            $expenseCategoryATag.parents('tr').remove();
                        } else {
                            p_notification(false, respond.msg);
                        }
                    }
                });
            }
        });


    });

    $('#expenseCategoryList').on('click', '.edit', function () {
        var editExpenseCategoryID = $(this).attr('id');
        var expenseCategoryName = $(this).parents('tr').find('.categoryName').html();
        var expenseCategoryParentID = $(this).parents('tr').find('.categoryParentName').attr('id');
        $("input[name='expenseCategoryName']", '#expenseCategoryAddForm').val(expenseCategoryName);
        $("select[name='expenseCategoryParent']", '#expenseCategoryAddForm').val(expenseCategoryParentID).selectpicker('render');
        $("input[name='expenseCategoryName']", '#expenseCategoryAddForm').focus();
        $("input[type='submit']", '#expenseCategoryAddForm').val('Update').attr('id', editExpenseCategoryID);
    });

    $('#approverValueLimitList').on('keyup', "input[name='approverMin'],input[name='approverMax']", function () {
        var decimalPoints = 0;
        if ($(this).val().indexOf('.') > 0) {
            decimalPoints = $(this).val().split('.')[1].length;
        }
        if ((!$.isNumeric($(this).val()) || $(this).val() < 0 || decimalPoints > 2)) {
            decimalPoints = 0;
            $(this).val('');
        }
    });


    $('#expenseTypeForm').on('click', "input[name='approverEnabled']", function () {
        if ($(this).is(':checked')) {
            $('div#approverValues').removeClass('hidden');
        } else {
            $('div#approverValues').addClass('hidden');
        }
    });

    if ($('#expenseTypeForm').hasClass('edit')) {
        $("table#approverValueLimitList > tbody > tr.approverLimits").each(function () {
            $(this).find(".selectpicker").prop('disabled', true);
        });
    }

    $('#approverValueLimitList').on('click', 'button.add', function () {
        var $addApproveLimitTr = $(this).parents('tr');
        var maxValue = $addApproveLimitTr.find("input[name='approverMax']").val();
        var minValue = $addApproveLimitTr.find("input[name='approverMin']").val();
 
        if (minValue == '' || isNaN(minValue) || toFloat(minValue) == 0 || toFloat(minValue) > toFloat(maxValue)) {
            $addApproveLimitTr.find("input[name='approverMin']").val('0.00')
            p_notification(false, eb.getMessage('ERR_EXPTYP_MINVAL'));
            return false;

        } else if (maxValue == '' || isNaN(maxValue) || toFloat(maxValue) == 0 || toFloat(maxValue) < toFloat(minValue)) {
            $addApproveLimitTr.find("input[name='approverMax']").val('0.00')
            p_notification(false, eb.getMessage('ERR_EXPTYP_MAXVAL'));
            return false;
        } else if ($addApproveLimitTr.find("select[name='approvers'] option:selected").text() == '') {
            p_notification(false, eb.getMessage('ERR_EXPTYP_APPROVERS'));
            return false;
        } else {
            var $clonedTr = $addApproveLimitTr.clone();
            $clonedTr.find("input[name='approverMax']").val('');
            $clonedTr.find("input[name='approverMin']").val('');
            $clonedTr.find(".bootstrap-select").remove();
            $clonedTr.find(".selectpicker").selectpicker('refresh');
            $clonedTr.insertAfter($addApproveLimitTr);
            $addApproveLimitTr.attr('id', '');
            $addApproveLimitTr.addClass('approverLimits');
            $addApproveLimitTr.find("input[name='approverMax']").prop('disabled', true);
            $addApproveLimitTr.find("input[name='approverMin']").prop('disabled', true);
            $addApproveLimitTr.find("td[name='add']").addClass('hidden');
            $addApproveLimitTr.find("td[name='edit']").removeClass('hidden');
            $addApproveLimitTr.find("td[name='delete']").removeClass('hidden');
            $addApproveLimitTr.find(".selectpicker").attr('disabled', true);

        }

    });

    $('#approverValueLimitList').on('click', 'button.delete', function () {
        $(this).parents('tr').remove();
    });

    $('#approverValueLimitList').on('click', 'button.edit', function () {
        var $editTr = $(this).parents('tr');
        $editTr.find("input[name='approverMax']").prop('disabled', false);
        $editTr.find("input[name='approverMin']").prop('disabled', false);
        $editTr.find(".selectpicker").attr('disabled', false);
        $editTr.find("td[name='edit']").addClass('hidden');
        $editTr.find("td[name='save']").removeClass('hidden');
        if ($('#expenseTypeForm').hasClass('edit')) {
            $editTr.find(".selectpicker").removeClass('disabled');
            $editTr.find(".bootstrap-select").children().removeClass('disabled');
            $editTr.find(".bootstrap-select").children('.dropdown-menu').find('li').removeClass('disabled');
        }
    });

    $('#approverValueLimitList').on('click', 'button.save', function () {
        var $saveTr = $(this).parents('tr');
        var maxValue = $saveTr.find("input[name='approverMax']").val();
        var minValue = $saveTr.find("input[name='approverMin']").val();

        if (minValue == '' || isNaN(minValue) || toFloat(minValue) == 0 || toFloat(minValue) > toFloat(maxValue)) {
            $saveTr.find("input[name='approverMin']").val('0.00')
            p_notification(false, eb.getMessage('ERR_EXPTYP_MINVAL'));
            return false;

        } else if (maxValue == '' || isNaN(maxValue) || toFloat(maxValue) == 0 || maxValue < minValue) {
            $saveTr.find("input[name='approverMax']").val('0.00')
            p_notification(false, eb.getMessage('ERR_EXPTYP_MAXVAL'));
            return false;
        } else if ($saveTr.find("select[name='approvers'] option:selected").text() == '') {
            p_notification(false, eb.getMessage('ERR_EXPTYP_APPROVERS'));
            return false;
        } else {
            $saveTr.find("input[name='approverMax']").prop('disabled', true);
            $saveTr.find("input[name='approverMin']").prop('disabled', true);
            $saveTr.find(".selectpicker").attr('disabled', true);
            $saveTr.find("td[name='save']").addClass('hidden');
            $saveTr.find("td[name='edit']").removeClass('hidden');
        }
    });

    $('#expenseTypeForm').on('submit', function (e) {
        e.preventDefault();
        var expTypeCat = $("select[name='expenseCategory'] option:selected", '#expenseTypeForm').val();
        var approverLimits = {};
        var approverEnabled = $("input[name='approverEnabled']", "#expenseTypeForm").is(':checked');
        if (approverEnabled) {
            var limitCount = 0;
            $("table#approverValueLimitList > tbody > tr.approverLimits").each(function () {
                var selectedApprovers = [];
                $(this).find("select[name='approvers'] option:selected").each(function () {
                    selectedApprovers.push($(this).val());
                });
                var approverLimitData = {
                    min: $(this).find("input[name='approverMin']").val(),
                    max: $(this).find("input[name='approverMax']").val(),
                    approvers: selectedApprovers
                };
                approverLimits[limitCount] = approverLimitData;
                limitCount++;
            });
        }
        if ($("input[name='expenseTypeName']", '#expenseTypeForm').val() == '') {
            p_notification(false, eb.getMessage('ERR_EXPTYP_NAME'));
            return false;
        } else if (expTypeCat == 0) {
            p_notification(false, eb.getMessage('ERR_EXPTYP_CAT'));
            return false;
        } else if (approverEnabled && jQuery.isEmptyObject(approverLimits)) {
            p_notification(false, eb.getMessage('ERR_EXPTYP_EMP_LMT'));
            return false;

        } else {
            var url;
            var editId;
            var editExpType = false;
            if ($('#expenseTypeForm').hasClass('edit')) {
                url = BASE_URL + '/purchase-requisition-type-api/edit';
                editId = $('#expenseTypeForm').data('id');
                editExpType = true;
            } else {
                url = BASE_URL + '/purchase-requisition-type-api/add';
                editId = '';
            }
            var expTypePostData = {
                expTypeName: $("input[name='expenseTypeName']", '#expenseTypeForm').val(),
                expTypeCat: expTypeCat,
                approverEnabled: (approverEnabled == true) ? 1 : 0,
                expValueLimits: approverLimits,
                expenseTypeAccountID: $("#expenseTypeAccountID", '#expenseTypeForm').val(),
                editId: editId
            };

            eb.ajax({
                type: 'POST',
                url: url,
                data: {expTypePostData: expTypePostData},
                success: function (respond) {
                    if (respond.status) {
                        if (editExpType) {
                            window.location.href = BASE_URL + '/purchase-requisition-type/create';
                        } else {
                            window.location.reload();
                        }
                    } else {
                        p_notification(respond.status, respond.msg);
                    }
                }
            });

        }
    });

    $('#expenseTypeList').on('click', '.delete', function () {
        var $deleteExpTr = $(this).parents('tr');
        var expenseTypeId = $(this).attr('id');
        bootbox.confirm('Are you sure you want to delete this Purchase Requisition Type?', function(result) {
            if (result == true) {
                eb.ajax({
                    type: 'POST',
                    url: BASE_URL + '/purchase-requisition-type-api/delete',
                    data: {expTypeId: expenseTypeId},
                    success: function (respond) {
                        if (respond.status) {
                            $deleteExpTr.remove();
                            p_notification(true, respond.msg);
                        } else {
                            p_notification(respond.status, respond.msg);
                        }
                    }
                });
            }
        });
    });

    $('#expenseTypeList').on('click', '.changeStatus', function () {
        var expenseTypeId = $(this).attr('id');
        var $statusSpan = $(this).find("span");
        var changeStatusTo = ($statusSpan.attr('class') == 'glyphicon glyphicon-check') ? 'inactive' : 'active';
        eb.ajax({
            type: 'POST',
            url: BASE_URL + '/purchase-requisition-type-api/changeExpenseTypeStatus',
            data: {expenseTypeId: expenseTypeId, changeStatusTo: changeStatusTo},
            success: function (respond) {
                if (respond.status) {
                    if (changeStatusTo == 'inactive') {
                        $statusSpan.removeClass('glyphicon glyphicon-check');
                        $statusSpan.addClass('glyphicon glyphicon-unchecked');
                    } else {
                        $statusSpan.removeClass('glyphicon glyphicon-unchecked');
                        $statusSpan.addClass('glyphicon glyphicon-check');
                    }
                }
            }
        });
    });

});