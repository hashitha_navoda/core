$(document).ready(function() {
//sdsd
    array = new Array();
    array[1] = 'Quotation';
    array[2] = 'Sales Order';
    array[3] = 'Invoice';
    array[4] = 'Payment';
    array[5] = 'Advance Payment';
    array[6] = 'Delivery Note';
    array[7] = 'Sales Returns';
    array[8] = 'Credit Note';
    array[9] = 'Purchase Order';
    array[10] = 'Grn';
    array[11] = 'Debit Note';
    array[12] = 'Adjustment';
    array[13] = 'Supplier Payments';
    array[14] = 'Purchase Invoice';
    array[15] = 'Purchase Returns';
    array[16] = 'Transfer';
    array[17] = 'Credit Note Payments';
    array[18] = 'Debit Note Payments';
    array[20] = 'Customer';
    array[21] = 'Project';
    array[22] = 'Job';
    array[23] = 'Activity';
    array[24] = 'Inquiry Log';
    array[25] = 'Suspended Tax Invoice';
    array[26] = 'Expense Payment Voucher';
    array[27] = 'Petty Cash Voucher';
    array[29] = 'Dispatch Note';
    array[30] = 'Journal Entry';
    array[31] = 'Journal Entry Template';
    array[33] = 'Item';
    array[34] = 'Supplier';
    array[39] = 'Purchase Requisition';
    array[42] = 'Income';

    saveButton = new Array();
    saveButton[1] = 'saveQuotationPrefix';
    saveButton[2] = 'saveSalesOrderPrefix';
    saveButton[3] = 'saveInvoicePrefix';
    saveButton[4] = 'savePaymentPrefix';
    saveButton[5] = 'saveAdvancePaymentPrefix';
    saveButton[6] = 'saveDeliveryNotePrefix';
    saveButton[7] = 'saveSalesReturnsPrefix';
    saveButton[8] = 'saveCreditNotePrefix';
    saveButton[9] = 'savePurchaseOrderPrefix';
    saveButton[10] = 'saveGrnPrefix';
    saveButton[11] = 'saveDebitNotePrefix';
    saveButton[12] = 'saveAdjustmentPrefix';
    saveButton[13] = 'saveSupplierPaymentsPrefix';
    saveButton[14] = 'savePurchaseInvoicePrefix';
    saveButton[15] = 'savePurchaseReturnsPrefix';
    saveButton[16] = 'saveTransferPrefix';
    saveButton[17] = 'saveCreditNotePaymentsPrefix';
    saveButton[18] = 'saveDebitNotePaymentsPrefix';
    saveButton[20] = 'saveCustomerPrefix';
    saveButton[21] = 'saveProjectPrefix';
    saveButton[22] = 'saveJobPrefix';
    saveButton[23] = 'saveActivityPrefix';
    saveButton[24] = 'saveInquiryLogPrefix';
    saveButton[25] = 'saveSuspendedTaxInvoicePrefix';
    saveButton[26] = 'saveExpensePaymentPrefix';
    saveButton[27] = 'savePettyCashVoucherPrefix';
    saveButton[29] = 'saveDispatchNotePrefix';
    saveButton[30] = 'saveJournalEntryPrefix';
    saveButton[31] = 'saveJournalEntryTemplatePrefix';
    saveButton[33] = 'saveItemPrefix';
    saveButton[34] = 'saveSupplierPrefix';
    saveButton[39] = 'savePRequPrefix';
    saveButton[42] = 'saveIncomePrefix';
//    $('#example1').hide();
//    $('#example2').hide();
//    $('#example3').hide();
//    $('#example4').hide();
//    $('#example5').hide();
//    $('#example6').hide();
//    $('#example7').hide();
//    $('#example8').hide();
//    $('#example9').hide();
//    $('#example10').hide();
//    $('#example11').hide();
//    $('#example12').hide();
//    $('#example13').hide();
//    $('#example14').hide();
//    $('#example15').hide();
//    $('#example16').hide();
    //
    (function() {
        var pridd = 2;
        var reference;
        var exValue;
        $.ajax({
            type: 'post',
            url: BASE_URL + '/companyAPI/getLocationPrefixDataForPreview',
            data: {referenceID: pridd},
            dataType: "json",
            async: false,
            success: function(respond) {
                if (respond.status == true) {
                    for (var j = 1; j < 43; j++) {
                        if (respond.data[j] == '') {
                            continue;
                        }
                        else if (j == '19' || j == '28' || j == '32' || j == '35' || j == '36' || j == '37' || j == '38' || j == '40' || j == '41') {
                            continue;
                        } 
                        else {

                            var exnumber = 'inputExample' + j;
                            reference = respond.data[j][j].referencePrefixCHAR;
                            stNumber = respond.data[j][j].referencePrefixCurrentReference;
                            digits = respond.data[j][j].referencePrefixNumberOfDigits;
                            exValue = reference + pad(parseInt(stNumber), digits);
                            document.getElementById(exnumber).value = exValue;
                        }

                    }
                }
            }


        });
    }
    )();


    $('.ShowForm').hide();
    $('.lableExample', '#location-prefix-data').hide();
    if ($('input:radio[name=quotation]:nth(0)').is(':checked')) {
        $('#quotationShowForm').show();
        $('#saveQuotationPrefix').hide();
        $('#editQuotationPrefix').data('rm', 0);
    } else if ($('input:radio[name=quotation]:nth(1)').is(':checked')) {
        $('#saveQuotationPrefix').hide();
        $('#editQuotationPrefix').data('rm', 1);
    } else {
        $('#editQuotationPrefix').hide();
    }
    if ($('input:radio[name=salesOrder]:nth(0)').is(':checked')) {
        $('#salesOrderShowForm').show();
        $('#saveSalesOrderPrefix').hide();
        $('#editSalesOrderPrefix').data('rm', 0);
    } else if ($('input:radio[name=salesOrder]:nth(1)').is(':checked')) {
        $('#saveSalesOrderPrefix').hide();
        $('#editSalesOrderPrefix').data('rm', 1);
    } else {
        $('#editSalesOrderPrefix').hide();
    }
    if ($('input:radio[name=invoice]:nth(0)').is(':checked')) {
        $('#invoiceShowForm').show();
        $('#saveInvoicePrefix').hide();
        $('#editInvoicePrefix').data('rm', 0);
    } else if ($('input:radio[name=invoice]:nth(1)').is(':checked')) {
        $('#saveInvoicePrefix').hide();
        $('#editInvoicePrefix').data('rm', 1);
    } else {
        $('#editInvoicePrefix').hide();
    }
    if ($('input:radio[name=payment]:nth(0)').is(':checked')) {
        $('#paymentShowForm').show();
        $('#savePaymentPrefix').hide();
        $('#editPaymentPrefix').data('rm', 0);
    } else if ($('input:radio[name=payment]:nth(1)').is(':checked')) {
        $('#savePaymentPrefix').hide();
        $('#editPaymentPrefix').data('rm', 1);
    } else {
        $('#editPaymentPrefix').hide();

    }
    if ($('input:radio[name=advancePayment]:nth(0)').is(':checked')) {
        $('#advancePaymentShowForm').show();
        $('#saveAdvancePaymentPrefix').hide();
        $('#editAdvancePaymentPrefix').data('rm', 0);
    } else if ($('input:radio[name=advancePayment]:nth(1)').is(':checked')) {
        $('#saveAdvancePaymentPrefix').hide();
        $('#editAdvancePaymentPrefix').data('rm', 1);
    } else {
        $('#editAdvancePaymentPrefix').hide();
    }
    if ($('input:radio[name=deliveryNote]:nth(0)').is(':checked')) {
        $('#deliveryNoteShowForm').show();
        $('#saveDeliveryNotePrefix').hide();
        $('#editDeliveryNotePrefix').data('rm', 0);
    } else if ($('input:radio[name=deliveryNote]:nth(1)').is(':checked')) {
        $('#saveDeliveryNotePrefix').hide();
        $('#editDeliveryNotePrefix').data('rm', 1);
    } else {
        $('#editDeliveryNotePrefix').hide();
    }
    if ($('input:radio[name=salesReturns]:nth(0)').is(':checked')) {
        $('#salesReturnsShowForm').show();
        $('#saveSalesReturnsPrefix').hide();
        $('#editSalesReturnsPrefix').data('rm', 0);
    } else if ($('input:radio[name=salesReturns]:nth(1)').is(':checked')) {
        $('#saveSalesReturnsPrefix').hide();
        $('#editSalesReturnsPrefix').data('rm', 1);
    } else {
        $('#editSalesReturnsPrefix').hide();
    }
    if ($('input:radio[name=creditNote]:nth(0)').is(':checked')) {
        $('#creditNoteShowForm').show();
        $('#saveCreditNotePrefix').hide();
        $('#editCreditNotePrefix').data('rm', 0);
    } else if ($('input:radio[name=creditNote]:nth(1)').is(':checked')) {
        $('#saveCreditNotePrefix').hide();
        $('#editCreditNotePrefix').data('rm', 1);
    } else {
        $('#editCreditNotePrefix').hide();

    }
    if ($('input:radio[name=purchaseOrder]:nth(0)').is(':checked')) {
        $('#purchaseOrderShowForm').show();
        $('#savePurchaseOrderPrefix').hide();
        $('#editPurchaseOrderPrefix').data('rm', 0);
    } else if ($('input:radio[name=purchaseOrder]:nth(1)').is(':checked')) {
        $('#savePurchaseOrderPrefix').hide();
        $('#editPurchaseOrderPrefix').data('rm', 1);
    } else {
        $('#editPurchaseOrderPrefix').hide();
    }
    if ($('input:radio[name=grn]:nth(0)').is(':checked')) {
        $('#grnShowForm').show();
        $('#saveGrnPrefix').hide();
        $('#editGrnPrefix').data('rm', 0);
    } else if ($('input:radio[name=grn]:nth(1)').is(':checked')) {
        $('#saveGrnPrefix').hide();
        $('#editGrnPrefix').data('rm', 1);
    } else {
        $('#editGrnPrefix').hide();
    }
    if ($('input:radio[name=debitNote]:nth(0)').is(':checked')) {
        $('#debitNoteShowForm').show();
        $('#saveDebitNotePrefix').hide();
        $('#editDebitNotePrefix').data('rm', 0);
    } else if ($('input:radio[name=debitNote]:nth(1)').is(':checked')) {
        $('#saveDebitNotePrefix').hide();
        $('#editDebitNotePrefix').data('rm', 1);
    } else {
        $('#editDebitNotePrefix').hide();
    }
    if ($('input:radio[name=adjustment]:nth(0)').is(':checked')) {
        $('#adjustmentShowForm').show();
        $('#saveAdjustmentPrefix').hide();
        $('#editAdjustmentPrefix').data('rm', 0);
    } else if ($('input:radio[name=adjustment]:nth(1)').is(':checked')) {
        $('#saveAdjustmentPrefix').hide();
        $('#editAdjustmentPrefix').data('rm', 1);
    } else {
        $('#editAdjustmentPrefix').hide();
    }
    if ($('input:radio[name=supplierPayments]:nth(0)').is(':checked')) {
        $('#supplierPaymentsShowForm').show();
        $('#saveSupplierPaymentsPrefix').hide();
        $('#editSupplierPaymentsPrefix').data('rm', 0);
    } else if ($('input:radio[name=supplierPayments]:nth(1)').is(':checked')) {
        $('#saveSupplierPaymentsPrefix').hide();
        $('#editSupplierPaymentsPrefix').data('rm', 1);
    } else {
        $('#editSupplierPaymentsPrefix').hide();
    }
    if ($('input:radio[name=purchaseInvoice]:nth(0)').is(':checked')) {
        $('#purchaseInvoiceShowForm').show();
        $('#savePurchaseInvoicePrefix').hide();
        $('#editPurchaseInvoicePrefix').data('rm', 0);
    } else if ($('input:radio[name=purchaseInvoice]:nth(1)').is(':checked')) {
        $('#savePurchaseInvoicePrefix').hide();
        $('#editPurchaseInvoicePrefix').data('rm', 1);
    } else {
        $('#editPurchaseInvoicePrefix').hide();
    }
    if ($('input:radio[name=purchaseReturns]:nth(0)').is(':checked')) {
        $('#purchaseReturnsShowForm').show();
        $('#savePurchaseReturnsPrefix').hide();
        $('#editPurchaseReturnsPrefix').data('rm', 0);
    } else if ($('input:radio[name=purchaseReturns]:nth(1)').is(':checked')) {
        $('#savePurchaseReturnsPrefix').hide();
        $('#editPurchaseReturnsPrefix').data('rm', 1);
    } else {
        $('#editPurchaseReturnsPrefix').hide();
    }
    if ($('input:radio[name=supplier]:nth(0)').is(':checked')) {
        $('#supplierShowForm').show();
        $('#saveSupplierPrefix').hide();
        $('#editSupplierPrefix').data('rm', 0);
    } else if ($('input:radio[name=supplier]:nth(1)').is(':checked')) {
        $('#saveSupplierPrefix').hide();
        $('#editSupplierPrefix').data('rm', 1);
    } else {
        $('#editSupplierPrefix').hide();
    }

    if ($('input:radio[name=pRequ]:nth(0)').is(':checked')) {
        $('#pRequShowForm').show();
        $('#savePRequPrefix').hide();
        $('#editPRequPrefix').data('rm', 0);
    } else if ($('input:radio[name=pRequ]:nth(1)').is(':checked')) {
        $('#savePRequPrefix').hide();
        $('#editPRequPrefix').data('rm', 1);
    } else {
        $('#editPRequPrefix').hide();
    }

    if ($('input:radio[name=income]:nth(0)').is(':checked')) {
        $('#incomeShowForm').show();
        $('#saveIncomePrefix').hide();
        $('#editIncomePrefix').data('rm', 0);
    } else if ($('input:radio[name=income]:nth(1)').is(':checked')) {
        $('#saveIncomePrefix').hide();
        $('#editIncomePrefix').data('rm', 1);
    } else {
        $('#editIncomePrefix').hide();
    }

    if ($('input:radio[name=transfer]:nth(0)').is(':checked')) {
        $('#transferShowForm').show();
        $('#saveTransferPrefix').hide();
        $('#editTransferPrefix').data('rm', 0);
    } else if ($('input:radio[name=transfer]:nth(1)').is(':checked')) {
        $('#saveTransferPrefix').hide();
        $('#editTransferPrefix').data('rm', 1);
    } else {
        $('#editTransferPrefix').hide();
    }

    if ($('input:radio[name=creditNotePayments]:nth(0)').is(':checked')) {
        $('#creditNotePaymentsShowForm').show();
        $('#saveCreditNotePaymentsPrefix').hide();
        $('#editCreditNotePaymentsPrefix').data('rm', 0);
    } else if ($('input:radio[name=creditNotePayments]:nth(1)').is(':checked')) {
        $('#saveCreditNotePaymentsPrefix').hide();
        $('#editCreditNotePaymentsPrefix').data('rm', 1);
    } else {
        $('#editCreditNotePaymentsPrefix').hide();
    }
    if ($('input:radio[name=debitNotePayments]:nth(0)').is(':checked')) {
        $('#debitNotePaymentsShowForm').show();
        $('#saveDebitNotePaymentsPrefix').hide();
        $('#editDebitNotePaymentsPrefix').data('rm', 0);
    } else if ($('input:radio[name=debitNotePayments]:nth(1)').is(':checked')) {
        $('#saveDebitNotePaymentsPrefix').hide();
        $('#editDebitNotePaymentsPrefix').data('rm', 1);
    } else {
        $('#editDebitNotePaymentsPrefix').hide();
    }
    if ($('input:radio[name=customer]:nth(0)').is(':checked')) {
        $('#customerShowForm').show();
        $('#saveCustomerPrefix').hide();
        $('#editCustomerPrefix').data('rm', 0);
    } else if ($('input:radio[name=customer]:nth(1)').is(':checked')) {
        $('#saveCustomerPrefix').hide();
        $('#editCustomerPrefix').data('rm', 1);
    } else {
        $('#editCustomerPrefix').hide();
    }
    if ($('input:radio[name=project]:nth(0)').is(':checked')) {
        $('#projectShowForm').show();
        $('#saveProjectPrefix').hide();
        $('#editProjectPrefix').data('rm', 0);
    } else if ($('input:radio[name=project]:nth(1)').is(':checked')) {
        $('#saveProjectPrefix').hide();
        $('#editProjectPrefix').data('rm', 1);
    } else {
        $('#editProjectPrefix').hide();
    }
    if ($('input:radio[name=job]:nth(0)').is(':checked')) {
        $('#jobShowForm').show();
        $('#saveJobPrefix').hide();
        $('#editJobPrefix').data('rm', 0);
    } else if ($('input:radio[name=job]:nth(1)').is(':checked')) {
        $('#saveJobPrefix').hide();
        $('#editJobPrefix').data('rm', 1);
    } else {
        $('#editJobPrefix').hide();
    }
    if ($('input:radio[name=activity]:nth(0)').is(':checked')) {
        $('#activityShowForm').show();
        $('#saveActivityPrefix').hide();
        $('#editActivityPrefix').data('rm', 0);
    } else if ($('input:radio[name=activity]:nth(1)').is(':checked')) {
        $('#saveActivityPrefix').hide();
        $('#editActivityPrefix').data('rm', 1);
    } else {
        $('#editActivityPrefix').hide();
    }
    if ($('input:radio[name=inquiryLog]:nth(0)').is(':checked')) {
        $('#inquiryLogShowForm').show();
        $('#saveInquiryLogPrefix').hide();
        $('#editInquiryLogPrefix').data('rm', 0);
    } else if ($('input:radio[name=inquiryLog]:nth(1)').is(':checked')) {
        $('#saveInquiryLogPrefix').hide();
        $('#editInquiryLogPrefix').data('rm', 1);
    } else {
        $('#editInquiryLogPrefix').hide();
    }
    if ($('input:radio[name=suspendedTaxInvoice]:nth(0)').is(':checked')) {
        $('#suspendedTaxInvoiceShowForm').show();
        $('#saveSuspendedTaxInvoicePrefix').hide();
        $('#editSuspendedTaxInvoicePrefix').data('rm', 0);
    } else if ($('input:radio[name=suspendedTaxInvoice]:nth(1)').is(':checked')) {
        $('#saveSuspendedTaxInvoicePrefix').hide();
        $('#editSuspendedTaxInvoicePrefix').data('rm', 1);
    } else {
        $('#editSuspendedTaxInvoicePrefix').hide();
    }
    if ($('input:radio[name=expensePaymentVoucher]:nth(0)').is(':checked')) {
        $('#expensePaymentVoucherShowForm').show();
        $('#saveExpensePaymentPrefix').hide();
        $('#editExpensePaymentPrefix').data('rm', 0);
    } else if ($('input:radio[name=expensePaymentVoucher]:nth(1)').is(':checked')) {
        $('#saveExpensePaymentPrefix').hide();
        $('#editExpensePaymentPrefix').data('rm', 1);
    } else {
        $('#editExpensePaymentPrefix').hide();
    }
    if ($('input:radio[name=petyCashVoucher]:nth(0)').is(':checked')) {
        $('#pettyCashVoucherShowForm').show();
        $('#savePettyCashVoucherPrefix').hide();
        $('#editPettyCashVoucherPrefix').data('rm', 0);
    } else if ($('input:radio[name=petyCashVoucher]:nth(1)').is(':checked')) {
        $('#savePettyCashVoucherPrefix').hide();
        $('#editPettyCashVoucherPrefix').data('rm', 1);
    } else {
        $('#editPettyCashVoucherPrefix').hide();
    }
    if ($('input:radio[name=dispatchNote]:nth(0)').is(':checked')) {
        $('#dispatchNoteShowForm').show();
        $('#saveDispatchNotePrefix').hide();
        $('#editDispatchNotePrefix').data('rm', 0);
    } else if ($('input:radio[name=dispatchNote]:nth(1)').is(':checked')) {
        $('#saveDispatchNotePrefix').hide();
        $('#editDispatchNotePrefix').data('rm', 1);
    } else {
        $('#editDispatchNotePrefix').hide();
    }
    if ($('input:radio[name=journalEntry]:nth(0)').is(':checked')) {
        $('#journalEntryShowForm').show();
        $('#saveJournalEntryPrefix').hide();
        $('#editJournalEntryPrefix').data('rm', 0);
    } else if ($('input:radio[name=journalEntry]:nth(1)').is(':checked')) {
        $('#saveJournalEntryPrefix').hide();
        $('#editJournalEntryPrefix').data('rm', 1);
    } else {
        $('#editJournalEntryPrefix').hide();
    }

    if ($('input:radio[name=journalEntryTemplate]:nth(0)').is(':checked')) {
        $('#journalEntryTemplateShowForm').show();
        $('#saveJournalEntryTemplatePrefix').hide();
        $('#editJournalEntryTemplatePrefix').data('rm', 0);
    } else if ($('input:radio[name=journalEntryTemplate]:nth(1)').is(':checked')) {
        $('#saveJournalEntryTemplatePrefix').hide();
        $('#editJournalEntryTemplatePrefix').data('rm', 1);
    } else {
        $('#editJournalEntryTemplatePrefix').hide();
    }

    if ($('input:radio[name=item]:nth(0)').is(':checked')) {
        $('#itemShowForm').show();
        $('#saveItemPrefix').hide();
        $('#editItemPrefix').data('rm', 0);
    } else if ($('input:radio[name=item]:nth(1)').is(':checked')) {
        $('#saveItemPrefix').hide();
        $('#editItemPrefix').data('rm', 1);
    } else {
        $('#editItemPrefix').hide();
    }

    function resetLocationPrefix() {
        $('#location-prefix-data tr').each(function() {
            $(this).find('input[name^="prefix"]').val('');
            $(this).find('.exampleNumber').val('');
            $(this).find('input[name^="numberOfDigits"]').val('');
            $(this).find('input[name^="startingNumber"]').val('');

        });
    }

    function pad(n, width, z) {
        z = z || '0';
        n = n + '';
        return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
    }

    $('.referenceType').on('click', function() {

        var refereneceModel = "";
        if ($(this).val() == "global") {
            refereneceModel = 0;
        } else {
            refereneceModel = 1;
        }
        $(this).parents().find('.editPrefixData').data('rm', refereneceModel);
        $(this).parents('.referenceEachView').find('.editPrefixData').trigger('click');
    });


    $('.editPrefixData', '').on('click', function() {
        var TypeID = $(this).data('rm');
        var prid = $(this).data('id');
        var $row = this;
        $('#prefixTypeID').val(prid);
        if (TypeID == '0') {
            $($row).parent().find('.savePrefixData').show();
            $($row).parent().find('.savePrefixData').data('id', prid);
            $($row).parent().find('.savePrefixData').data('type', 'global');
            $($row).parent().find('.editPrefixData').hide();
            if ($($row).parent('div').parent('div').parent('div').find('input[class^="prefix"]').val()) {
                $($row).parent('div').parent('div').parent('div').find('input[class^="prefix"]').attr('disabled', false);
                $($row).parent('div').parent('div').parent('div').find('input[class^="digits"]').attr('disabled', false);
                $($row).parent('div').parent('div').parent('div').find('input[class^="nextNumber"]').attr('disabled', false);
            } else {
                eb.ajax({
                    type: 'post',
                    url: BASE_URL + '/companyAPI/getGlobalPrefixData',
                    data: {referenceID: prid},
                    dataType: "json",
                    success: function(data) {

                        if (data.status == true) {
                            $($row).parent('div').parent('div').parent('div').find('input[class^="prefix"]').val(data.data[1].referencePrefixCHAR);
                            $($row).parent('div').parent('div').parent('div').find('input[class^="digits"]').val(data.data[1].referencePrefixNumberOfDigits);
                            $($row).parent('div').parent('div').parent('div').find('input[class^="nextNumber"]').val(data.data[1].referencePrefixCurrentReference);
                        }

                    }
                });
            }

        } else if (TypeID == '1') {
            eb.ajax({
                type: 'post',
                url: BASE_URL + '/companyAPI/getLocationPrefixData',
                data: {referenceID: prid},
                dataType: "json",
                success: function(data) {

                    if (data.status == true) {
                        $('#location-prefix-data tr').each(function() {
                            $(this).find('input[name^="prefix"]').val('');
                            $(this).find('input[name^="numberOfDigits"]').val('');
                            $(this).find('input[name^="startingNumber"]').val('');
                            $(this).find('input[name^="exampleNumber"]').val('');
                            if (data.data[$(this).data('id')]) {
                                $(this).find('input[name^="prefix"]').val(data.data[$(this).data('id')].referencePrefixCHAR);
                                $(this).find('input[name^="numberOfDigits"]').val(data.data[$(this).data('id')].referencePrefixNumberOfDigits);
                                $(this).find('input[name^="startingNumber"]').val(data.data[$(this).data('id')].referencePrefixCurrentReference);
                                $(this).find('input[name^="exampleNumber"]').val(data.data[$(this).data('id')].referencePrefixCHAR + pad(parseInt(data.data[$(this).data('id')].referencePrefixCurrentReference), data.data[$(this).data('id')].referencePrefixNumberOfDigits));
                            }
                        });
                        $('#location-prefix-data tr').parents().find('#saveLocationPrefix').data('id', prid);
                        $('#location-prefix-data tr').parents().find('#saveLocationPrefix').data('type', 'local');
                    } else {
                        p_notification(data.status, data.msg);
                    }
                }
            });
            $('#locationPrefixForm').modal('show');

        }
    });

    $('input:radio[name=quotation]:nth(0),input:radio[name=quotation]:nth(1)').on('click', function() {
        $('#prefixTypeID').val(1);
        if ($(this).val() == 'global') {
            $('#quotationShowForm').show();
            $('#saveQuotationPrefix').show();
            $(this).parents().find('.savePrefixData').data('type', $(this).val());
            $(this).parents().find('.savePrefixData').data('id', 1);
        } else {
            resetLocationPrefix();
            $('#saveQuotationPrefix').hide();
            $('#locationPrefixForm').modal('show');
            $('#locationPrefixForm').find('.savePrefixData').data('type', $(this).val());
            $('#locationPrefixForm').find('.savePrefixData').data('id', 1);
            $('#quotationShowForm').hide();

        }
    });

    $('input:radio[name=salesOrder]:nth(0),input:radio[name=salesOrder]:nth(1)').on('click', function() {
        $('#prefixTypeID').val(2);
        if ($(this).val() == 'global') {
            $('#salesOrderShowForm').show();
            $('#saveSalesOrderPrefix').show();
            $(this).parents().find('.savePrefixData').data('type', $(this).val());
            $(this).parents().find('.savePrefixData').data('id', 2);
        } else {
            resetLocationPrefix();
            $('#locationPrefixForm').modal('show');
            $('#saveSalesOrderPrefix').hide();
            $('#locationPrefixForm').find('.savePrefixData').data('type', $(this).val());
            $('#locationPrefixForm').find('.savePrefixData').data('id', 2);
            $('#salesOrderShowForm').hide();
        }
    });

    $('input:radio[name=invoice]:nth(0),input:radio[name=invoice]:nth(1)').on('click', function() {
        $('#prefixTypeID').val(3);
        if ($(this).val() == 'global') {
            $('#invoiceShowForm').show();
            $('#saveInvoicePrefix').show();
            $(this).parents().find('.savePrefixData').data('type', $(this).val());
            $(this).parents().find('.savePrefixData').data('id', 3);
        } else {
            resetLocationPrefix();
            $('#locationPrefixForm').modal('show');
            $('#saveInvoicePrefix').hide();
            $('#locationPrefixForm').find('.savePrefixData').data('type', $(this).val());
            $('#locationPrefixForm').find('.savePrefixData').data('id', 3);
            $('#invoiceShowForm').hide();
        }
    });

    $('input:radio[name=payment]:nth(0),input:radio[name=payment]:nth(1)').on('click', function() {
        $('#prefixTypeID').val(4);
        if ($(this).val() == 'global') {
            $('#paymentShowForm').show();
            $('#savePaymentPrefix').show();
            $(this).parents().find('.savePrefixData').data('type', $(this).val());
            $(this).parents().find('.savePrefixData').data('id', 4);
        } else {
            resetLocationPrefix();
            $('#locationPrefixForm').modal('show');
            $('#savePaymentPrefix').hide();
            $('#locationPrefixForm').find('.savePrefixData').data('type', $(this).val());
            $('#locationPrefixForm').find('.savePrefixData').data('id', 4);
            $('#paymentShowForm').hide();
        }
    });

    $('input:radio[name=advancePayment]:nth(0),input:radio[name=advancePayment]:nth(1)').on('click', function() {
        $('#prefixTypeID').val(5);
        if ($(this).val() == 'global') {
            $('#advancePaymentShowForm').show();
            $('#saveAdvancePaymentPrefix').show();
            $(this).parents().find('.savePrefixData').data('type', $(this).val());
            $(this).parents().find('.savePrefixData').data('id', 5);
        } else {
            resetLocationPrefix();
            $('#locationPrefixForm').modal('show');
            $('#saveAdvancePaymentPrefix').hide();
            $('#locationPrefixForm').find('.savePrefixData').data('type', $(this).val());
            $('#locationPrefixForm').find('.savePrefixData').data('id', 5);
            $('#advancePaymentShowForm').hide();
        }
    });

    $('input:radio[name=deliveryNote]:nth(0),input:radio[name=deliveryNote]:nth(1)').on('click', function() {
        $('#prefixTypeID').val(6);
        if ($(this).val() == 'global') {
            $('#deliveryNoteShowForm').show();
            $('#saveDeliveryNotePrefix').show();
            $(this).parents().find('.savePrefixData').data('type', $(this).val());
            $(this).parents().find('.savePrefixData').data('id', 6);
        } else {
            resetLocationPrefix();
            $('#locationPrefixForm').modal('show');
            $('#saveDeliveryNotePrefix').hide();
            $('#locationPrefixForm').find('.savePrefixData').data('type', $(this).val());
            $('#locationPrefixForm').find('.savePrefixData').data('id', 6);
            $('#deliveryNoteShowForm').hide();
        }
    });

    $('input:radio[name=salesReturns]:nth(0),input:radio[name=salesReturns]:nth(1)').on('click', function() {
        $('#prefixTypeID').val(7);
        if ($(this).val() == 'global') {
            $('#salesReturnsShowForm').show();
            $('#saveSalesReturnsPrefix').show();
            $(this).parents().find('.savePrefixData').data('type', $(this).val());
            $(this).parents().find('.savePrefixData').data('id', 7);
        } else {
            resetLocationPrefix();
            $('#locationPrefixForm').modal('show');
            $('#saveSalesReturnsPrefix').hide();
            $('#locationPrefixForm').find('.savePrefixData').data('type', $(this).val());
            $('#locationPrefixForm').find('.savePrefixData').data('id', 7);
            $('#salesReturnsShowForm').hide();
        }
    });

    $('input:radio[name=creditNote]:nth(0),input:radio[name=creditNote]:nth(1)').on('click', function() {
        $('#prefixTypeID').val(8);
        if ($(this).val() == 'global') {
            $('#creditNoteShowForm').show();
            $('#saveCreditNotePrefix').show();
            $(this).parents().find('.savePrefixData').data('type', $(this).val());
            $(this).parents().find('.savePrefixData').data('id', 8);
        } else {
            resetLocationPrefix();
            $('#locationPrefixForm').modal('show');
            $('#saveCreditNotePrefix').hide();
            $('#locationPrefixForm').find('.savePrefixData').data('type', $(this).val());
            $('#locationPrefixForm').find('.savePrefixData').data('id', 8);
            $('#creditNoteShowForm').hide();
        }
    });

    $('input:radio[name=purchaseOrder]:nth(0),input:radio[name=purchaseOrder]:nth(1)').on('click', function() {
        $('#prefixTypeID').val(9);
        if ($(this).val() == 'global') {
            $('#purchaseOrderShowForm').show();
            $('#savePurchaseOrderPrefix').show();
            $(this).parents().find('.savePrefixData').data('type', $(this).val());
            $(this).parents().find('.savePrefixData').data('id', 9);
        } else {
            resetLocationPrefix();
            $('#locationPrefixForm').modal('show');
            $('#savePurchaseOrderPrefix').hide();
            $('#locationPrefixForm').find('.savePrefixData').data('type', $(this).val());
            $('#locationPrefixForm').find('.savePrefixData').data('id', 9);
            $('#purchaseOrderShowForm').hide();
        }
    });

    $('input:radio[name=grn]:nth(0),input:radio[name=grn]:nth(1)').on('click', function() {
        $('#prefixTypeID').val(10);
        if ($(this).val() == 'global') {
            $('#grnShowForm').show();
            $('#saveGrnPrefix').show();
            $(this).parents().find('.savePrefixData').data('type', $(this).val());
            $(this).parents().find('.savePrefixData').data('id', 10);
        } else {
            resetLocationPrefix();
            $('#locationPrefixForm').modal('show');
            $('#saveGrnPrefix').hide();
            $('#locationPrefixForm').find('.savePrefixData').data('type', $(this).val());
            $('#locationPrefixForm').find('.savePrefixData').data('id', 10);
            $('#grnShowForm').hide();
        }
    });

    $('input:radio[name=debitNote]:nth(0),input:radio[name=debitNote]:nth(1)').on('click', function() {
        $('#prefixTypeID').val(11);
        if ($(this).val() == 'global') {
            $('#debitNoteShowForm').show();
            $('#saveDebitNotePrefix').show();
            $(this).parents().find('.savePrefixData').data('type', $(this).val());
            $(this).parents().find('.savePrefixData').data('id', 11);
        } else {
            resetLocationPrefix();
            $('#locationPrefixForm').modal('show');
            $('#saveDebitNotePrefix').hide();
            $('#locationPrefixForm').find('.savePrefixData').data('type', $(this).val());
            $('#locationPrefixForm').find('.savePrefixData').data('id', 11);
            $('#debitNoteShowForm').hide();
        }
    });

    $('input:radio[name=adjustment]:nth(0),input:radio[name=adjustment]:nth(1)').on('click', function() {
        $('#prefixTypeID').val(12);
        if ($(this).val() == 'global') {
            $('#adjustmentShowForm').show();
            $('#saveAdjustmentPrefix').show();
            $(this).parents().find('.savePrefixData').data('type', $(this).val());
            $(this).parents().find('.savePrefixData').data('id', 12);
        } else {
            resetLocationPrefix();
            $('#locationPrefixForm').modal('show');
            $('#saveAdjustmentPrefix').hide();
            $('#locationPrefixForm').find('.savePrefixData').data('type', $(this).val());
            $('#locationPrefixForm').find('.savePrefixData').data('id', 12);
            $('#adjustmentShowForm').hide();
        }
    });

    $('input:radio[name=supplierPayments]:nth(0),input:radio[name=supplierPayments]:nth(1)').on('click', function() {
        $('#prefixTypeID').val(13);
        if ($(this).val() == 'global') {
            $('#supplierPaymentsShowForm').show();
            $('#saveSupplierPaymentPrefix').show();
            $(this).parents().find('.savePrefixData').data('type', $(this).val());
            $(this).parents().find('.savePrefixData').data('id', 13);
        } else {
            resetLocationPrefix();
            $('#locationPrefixForm').modal('show');
            $('#saveSupplierPaymentPrefix').hide();
            $('#locationPrefixForm').find('.savePrefixData').data('type', $(this).val());
            $('#locationPrefixForm').find('.savePrefixData').data('id', 13);
            $('#supplierPaymentsShowForm').hide();
        }
    });

    $('input:radio[name=purchaseInvoice]:nth(0),input:radio[name=purchaseInvoice]:nth(1)').on('click', function() {
        $('#prefixTypeID').val(14);
        if ($(this).val() == 'global') {
            $('#purchaseInvoiceShowForm').show();
            $('#savePurchaseInvoicePrefix').show();
            $(this).parents().find('.savePrefixData').data('type', $(this).val());
            $(this).parents().find('.savePrefixData').data('id', 14);
        } else {
            resetLocationPrefix();
            $('#locationPrefixForm').modal('show');
            $('#savePurchaseInvoicePrefix').hide();
            $('#locationPrefixForm').find('.savePrefixData').data('type', $(this).val());
            $('#locationPrefixForm').find('.savePrefixData').data('id', 14);
            $('#purchaseInvoiceShowForm').hide();
        }
    });

    $('input:radio[name=purchaseReturns]:nth(0),input:radio[name=purchaseReturns]:nth(1)').on('click', function() {
        $('#prefixTypeID').val(15);
        if ($(this).val() == 'global') {
            $('#purchaseReturnsShowForm').show();
            $('#savePurchaseReturnsPrefix').show();
            $(this).parents().find('.savePrefixData').data('type', $(this).val());
            $(this).parents().find('.savePrefixData').data('id', 15);
        } else {
            resetLocationPrefix();
            $('#locationPrefixForm').modal('show');
            $('#savePurchaseReturnsPrefix').hide();
            $('#locationPrefixForm').find('.savePrefixData').data('type', $(this).val());
            $('#locationPrefixForm').find('.savePrefixData').data('id', 15);
            $('#purchaseReturnsShowForm').hide();
        }
    });

    $('input:radio[name=supplier]:nth(0),input:radio[name=supplier]:nth(1)').on('click', function() {
        $('#prefixTypeID').val(34);
        if ($(this).val() == 'global') {
            $('#supplierShowForm').show();
            $('#saveSupplierPrefix').show();
            $(this).parents().find('.savePrefixData').data('type', $(this).val());
            $(this).parents().find('.savePrefixData').data('id', 34);
        } else {
            resetLocationPrefix();
            $('#locationPrefixForm').modal('show');
            $('#saveSupplierPrefix').hide();
            $('#locationPrefixForm').find('.savePrefixData').data('type', $(this).val());
            $('#locationPrefixForm').find('.savePrefixData').data('id', 34);
            $('#supplierShowForm').hide();
        }
    });

    $('input:radio[name=income]:nth(0),input:radio[name=income]:nth(1)').on('click', function() {
        $('#prefixTypeID').val(42);
        if ($(this).val() == 'global') {
            $('#incomeShowForm').show();
            $('#saveIncomePrefix').show();
            $(this).parents().find('.savePrefixData').data('type', $(this).val());
            $(this).parents().find('.savePrefixData').data('id', 42);
        } else {
            resetLocationPrefix();
            $('#locationPrefixForm').modal('show');
            $('#saveIncomePrefix').hide();
            $('#locationPrefixForm').find('.savePrefixData').data('type', $(this).val());
            $('#locationPrefixForm').find('.savePrefixData').data('id', 42);
            $('#incomeShowForm').hide();
        }
    });

    $('input:radio[name=pRequ]:nth(0),input:radio[name=pRequ]:nth(1)').on('click', function() {
        $('#prefixTypeID').val(39);
        if ($(this).val() == 'global') {
            $('#pRequShowForm').show();
            $('#savePRequPrefix').show();
            $(this).parents().find('.savePrefixData').data('type', $(this).val());
            $(this).parents().find('.savePrefixData').data('id', 39);
        } else {
            resetLocationPrefix();
            $('#locationPrefixForm').modal('show');
            $('#savePRequPrefix').hide();
            $('#locationPrefixForm').find('.savePrefixData').data('type', $(this).val());
            $('#locationPrefixForm').find('.savePrefixData').data('id', 39);
            $('#pRequShowForm').hide();
        }
    });

    $('input:radio[name=transfer]:nth(0),input:radio[name=transfer]:nth(1)').on('click', function() {
        $('#prefixTypeID').val(16);

        if ($(this).val() == 'global') {
            $('#transferShowForm').show();
            $('#saveTransferPrefix').show();
            $(this).parents().find('.savePrefixData').data('type', $(this).val());
            $(this).parents().find('.savePrefixData').data('id', 16);
        } else {
            resetLocationPrefix();
            $('#locationPrefixForm').modal('show');
            $('#saveTransferPrefix').hide();
            $('#locationPrefixForm').find('.savePrefixData').data('type', $(this).val());
            $('#locationPrefixForm').find('.savePrefixData').data('id', 16);
            $('#transferShowForm').hide();
        }
    });

    $('input:radio[name=creditNotePayments]:nth(0),input:radio[name=creditNotePayments]:nth(1)').on('click', function() {
        $('#prefixTypeID').val(17);

        if ($(this).val() == 'global') {
            $('#creditNotePaymentsShowForm').show();
            $('#saveCreditNotePaymentsPrefix').show();
            $(this).parents().find('.savePrefixData').data('type', $(this).val());
            $(this).parents().find('.savePrefixData').data('id', 17);
        } else {
            resetLocationPrefix();
            $('#locationPrefixForm').modal('show');
            $('#saveCreditNotePaymentsPrefix').hide();
            $('#locationPrefixForm').find('.savePrefixData').data('type', $(this).val());
            $('#locationPrefixForm').find('.savePrefixData').data('id', 17);
            $('#creditNotePaymentsShowForm').hide();
        }
    });
    $('input:radio[name=debitNotePayments]:nth(0),input:radio[name=debitNotePayments]:nth(1)').on('click', function() {
        $('#prefixTypeID').val(18);

        if ($(this).val() == 'global') {
            $('#debitNotePaymentsShowForm').show();
            $('#saveDebitNotePaymentsPrefix').show();
            $(this).parents().find('.savePrefixData').data('type', $(this).val());
            $(this).parents().find('.savePrefixData').data('id', 18);
        } else {
            resetLocationPrefix();
            $('#locationPrefixForm').modal('show');
            $('#saveDebitNotePaymentsPrefix').hide();
            $('#locationPrefixForm').find('.savePrefixData').data('type', $(this).val());
            $('#locationPrefixForm').find('.savePrefixData').data('id', 18);
            $('#debitNotePaymentsShowForm').hide();
        }
    });
    $('input:radio[name=customer]:nth(0),input:radio[name=customer]:nth(1)').on('click', function() {
        $('#prefixTypeID').val(20);

        if ($(this).val() == 'global') {
            $('#customerShowForm').show();
            $('#saveCustomerPrefix').show();
            $(this).parents().find('.savePrefixData').data('type', $(this).val());
            $(this).parents().find('.savePrefixData').data('id', 20);
        } else {
            resetLocationPrefix();
            $('#locationPrefixForm').modal('show');
            $('#saveCustomerPrefix').hide();
            $('#locationPrefixForm').find('.savePrefixData').data('type', $(this).val());
            $('#locationPrefixForm').find('.savePrefixData').data('id', 20);
            $('#customerShowForm').hide();
        }
    });
    $('input:radio[name=project]:nth(0),input:radio[name=project]:nth(1)').on('click', function() {
        $('#prefixTypeID').val(21);

        if ($(this).val() == 'global') {
            $('#projectShowForm').show();
            $('#saveProjectPrefix').show();
            $(this).parents().find('.savePrefixData').data('type', $(this).val());
            $(this).parents().find('.savePrefixData').data('id', 21);
        } else {
            resetLocationPrefix();
            $('#locationPrefixForm').modal('show');
            $('#saveProjectPrefix').hide();
            $('#locationPrefixForm').find('.savePrefixData').data('type', $(this).val());
            $('#locationPrefixForm').find('.savePrefixData').data('id', 21);
            $('#projectShowForm').hide();
        }
    });
    $('input:radio[name=job]:nth(0),input:radio[name=job]:nth(1)').on('click', function() {
        $('#prefixTypeID').val(22);

        if ($(this).val() == 'global') {
            $('#jobShowForm').show();
            $('#saveJobPrefix').show();
            $(this).parents().find('.savePrefixData').data('type', $(this).val());
            $(this).parents().find('.savePrefixData').data('id', 22);
        } else {
            resetLocationPrefix();
            $('#locationPrefixForm').modal('show');
            $('#saveJobPrefix').hide();
            $('#locationPrefixForm').find('.savePrefixData').data('type', $(this).val());
            $('#locationPrefixForm').find('.savePrefixData').data('id', 22);
            $('#jobShowForm').hide();
        }
    });
    $('input:radio[name=activity]:nth(0),input:radio[name=activity]:nth(1)').on('click', function() {
        $('#prefixTypeID').val(23);

        if ($(this).val() == 'global') {
            $('#activityShowForm').show();
            $('#saveActivityPrefix').show();
            $(this).parents().find('.savePrefixData').data('type', $(this).val());
            $(this).parents().find('.savePrefixData').data('id', 23);
        } else {
            resetLocationPrefix();
            $('#locationPrefixForm').modal('show');
            $('#saveActivityPrefix').hide();
            $('#locationPrefixForm').find('.savePrefixData').data('type', $(this).val());
            $('#locationPrefixForm').find('.savePrefixData').data('id', 23);
            $('#activityShowForm').hide();
        }
    });
    $('input:radio[name=inquiryLog]:nth(0),input:radio[name=inquiryLog]:nth(1)').on('click', function() {
        $('#prefixTypeID').val(24);

        if ($(this).val() == 'global') {
            $('#inquiryLogShowForm').show();
            $('#saveInquiryLogPrefix').show();
            $(this).parents().find('.savePrefixData').data('type', $(this).val());
            $(this).parents().find('.savePrefixData').data('id', 24);
        } else {
            resetLocationPrefix();
            $('#locationPrefixForm').modal('show');
            $('#saveInquiryLogPrefix').hide();
            $('#locationPrefixForm').find('.savePrefixData').data('type', $(this).val());
            $('#locationPrefixForm').find('.savePrefixData').data('id', 24);
            $('#inquiryLogShowForm').hide();
        }
    });
    $('input:radio[name=suspendedTaxInvoice]:nth(0),input:radio[name=suspendedTaxInvoice]:nth(1)').on('click', function() {
        $('#prefixTypeID').val(25);

        if ($(this).val() == 'global') {
            $('#suspendedTaxInvoiceShowForm').show();
            $('#saveSuspendedTaxInvoicePrefix').show();
            $(this).parents().find('.savePrefixData').data('type', $(this).val());
            $(this).parents().find('.savePrefixData').data('id', 25);
        } else {
            resetLocationPrefix();
            $('#locationPrefixForm').modal('show');
            $('#saveSuspendedTaxInvoicePrefix').hide();
            $('#locationPrefixForm').find('.savePrefixData').data('type', $(this).val());
            $('#locationPrefixForm').find('.savePrefixData').data('id', 25);
            $('#suspendedTaxInvoiceShowForm').hide();
        }
    });
    $('input:radio[name=expensePaymentVoucher]:nth(0),input:radio[name=expensePaymentVoucher]:nth(1)').on('click', function() {
        $('#prefixTypeID').val(26);

        if ($(this).val() == 'global') {
            $('#expensePaymentVoucherShowForm').show();
            $('#saveExpensePaymentPrefix').show();
            $(this).parents().find('.savePrefixData').data('type', $(this).val());
            $(this).parents().find('.savePrefixData').data('id', 26);
        } else {
            resetLocationPrefix();
            $('#locationPrefixForm').modal('show');
            $('#saveExpensePaymentPrefix').hide();
            $('#locationPrefixForm').find('.savePrefixData').data('type', $(this).val());
            $('#locationPrefixForm').find('.savePrefixData').data('id', 26);
            $('#expensePaymentVoucherShowForm').hide();
        }
    });
    $('input:radio[name=petyCashVoucher]:nth(0),input:radio[name=petyCashVoucher]:nth(1)').on('click', function() {
        $('#prefixTypeID').val(27);

        if ($(this).val() == 'global') {
            $('#pettyCashVoucherShowForm').show();
            $('#savePettyCashVoucherPrefix').show();
            $(this).parents().find('.savePrefixData').data('type', $(this).val());
            $(this).parents().find('.savePrefixData').data('id', 27);
        } else {
            resetLocationPrefix();
            $('#locationPrefixForm').modal('show');
            $('#savePettyCashVoucherPrefix').hide();
            $('#locationPrefixForm').find('.savePrefixData').data('type', $(this).val());
            $('#locationPrefixForm').find('.savePrefixData').data('id', 27);
            $('#pettyCashVoucherShowForm').hide();
        }
    });
    $('input:radio[name=dispatchNote]:nth(0),input:radio[name=dispatchNote]:nth(1)').on('click', function() {
        $('#prefixTypeID').val(29);

        if ($(this).val() == 'global') {
            $('#dispatchNoteShowForm').show();
            $('#saveDispatchNotePrefix').show();
            $(this).parents().find('.savePrefixData').data('type', $(this).val());
            $(this).parents().find('.savePrefixData').data('id', 29);
        } else {
            resetLocationPrefix();
            $('#locationPrefixForm').modal('show');
            $('#saveDispatchNotePrefix').hide();
            $('#locationPrefixForm').find('.savePrefixData').data('type', $(this).val());
            $('#locationPrefixForm').find('.savePrefixData').data('id', 29);
            $('#dispatchNoteShowForm').hide();
        }
    });
    $('input:radio[name=journalEntry]:nth(0),input:radio[name=journalEntry]:nth(1)').on('click', function() {
        $('#prefixTypeID').val(30);

        if ($(this).val() == 'global') {
            $('#journalEntryShowForm').show();
            $('#saveJournalEntryPrefix').show();
            $(this).parents().find('.savePrefixData').data('type', $(this).val());
            $(this).parents().find('.savePrefixData').data('id', 30);
        } else {
            resetLocationPrefix();
            $('#locationPrefixForm').modal('show');
            $('#saveJournalEntryPrefix').hide();
            $('#locationPrefixForm').find('.savePrefixData').data('type', $(this).val());
            $('#locationPrefixForm').find('.savePrefixData').data('id', 30);
            $('#journalEntryShowForm').hide();
        }
    });
    $('input:radio[name=journalEntryTemplate]:nth(0),input:radio[name=journalEntryTemplate]:nth(1)').on('click', function() {
        $('#prefixTypeID').val(31);

        if ($(this).val() == 'global') {
            $('#journalEntryTemplateShowForm').show();
            $('#saveJournalEntryTemplatePrefix').show();
            $(this).parents().find('.savePrefixData').data('type', $(this).val());
            $(this).parents().find('.savePrefixData').data('id', 31);
        } else {
            resetLocationPrefix();
            $('#locationPrefixForm').modal('show');
            $('#saveJournalEntryTemplatePrefix').hide();
            $('#locationPrefixForm').find('.savePrefixData').data('type', $(this).val());
            $('#locationPrefixForm').find('.savePrefixData').data('id', 31);
            $('#journalEntryTemplateShowForm').hide();
        }
    });
    $('input:radio[name=item]:nth(0),input:radio[name=item]:nth(1)').on('click', function() {
        $('#prefixTypeID').val(33);
        if ($(this).val() == 'global') {
            $('#itemShowForm').show();
            $('#saveItemPrefix').show();
            $(this).parents().find('.savePrefixData').data('type', $(this).val());
            $(this).parents().find('.savePrefixData').data('id', 33);
        } else {
            resetLocationPrefix();
            $('#locationPrefixForm').modal('show');
            $('#saveItemPrefix').hide();
            $('#locationPrefixForm').find('.savePrefixData').data('type', $(this).val());
            $('#locationPrefixForm').find('.savePrefixData').data('id', 33);
            $('#itemShowForm').hide();
        }
    });

    tinymce.init({selector: 'textarea'});




    function updateReference() {
        this.id = '';
        this.locationID = null;
        this.referenceName = '';
        this.type = '';
        this.digits = '';
        this.nextNumber = '';
        this.localGlobleType = '';
    }


    $('.nextNumber, .digits, .prefix').on('keyup', function() {
        var prefixID = $(this).parents('.panel').data('id');
        $('#prefixTypeID').val(prefixID);
        var prefixType;
        var digits;
        var startingNumber;
        switch (prefixID) {
            case 1:
                prefixType = $('#quotationPrefix').val();
                digits = $('#quotationDigits').val();
                startingNumber = $('#QuotationNextNumber').val();

                break;
            case 2:
                prefixType = $('#salesOrderPrefix').val();
                digits = $('#salesOrderDigits').val();
                startingNumber = $('#salesOrderNextNumber').val();

                break;
            case 3:
                prefixType = $('#invoicePrefix').val();
                digits = $('#invoiceDigits').val();
                startingNumber = $('#invoiceNextNumber').val();
                break;
            case 4:
                prefixType = $('#paymentPrefix').val();
                digits = $('#paymentDigits').val();
                startingNumber = $('#paymentNextNumber').val();
                break;
            case 5:
                prefixType = $('#advancePaymentPrefix').val();
                digits = $('#advancePaymentDigits').val();
                startingNumber = $('#advancePaymentNextNumber').val();
                break;
            case 6:
                prefixType = $('#deliveryNotePrefix').val();
                digits = $('#diliveryNoteDigits').val();
                startingNumber = $('#deliveryNoteNextNumber').val();
                break;
            case 7:
                prefixType = $('#salesReturnsPrefix').val();
                digits = $('#salesReturnsDigits').val();
                startingNumber = $('#salesReturnsNextNumber').val();
                break;
            case 8:
                prefixType = $('#creditNotePrefix').val();
                digits = $('#creditNoteDigits').val();
                startingNumber = $('#creditNoteNextNumber').val();
                break;
            case 9:
                prefixType = $('#purchaseOrderPrefix').val();
                digits = $('#purchaseOrderDigits').val();
                startingNumber = $('#purchaseOrderNextNumber').val();
                break;
            case 10:
                prefixType = $('#grnPrefix').val();
                digits = $('#grnDigits').val();
                startingNumber = $('#grnNextNumber').val();
                break;
            case 11:
                prefixType = $('#debitNotePrefix').val();
                digits = $('#debitNoteDigits').val();
                startingNumber = $('#debitNoteNextNumber').val();
                break;
            case 12:
                prefixType = $('#adjustmentPrefix').val();
                digits = $('#adjustmentDigits').val();
                startingNumber = $('#adjustmentNextNumber').val();
                break;
            case 13:
                prefixType = $('#supplierPaymentsPrefix').val();
                digits = $('#supplierPaymentsDigits').val();
                startingNumber = $('#supplierPaymentsNextNumber').val();
                break;
            case 14:
                prefixType = $('#purchaseInvoicePrefix').val();
                digits = $('#purchaseInvoiceDigits').val();
                startingNumber = $('#purchaseInvoiceNextNumber').val();
                break;
            case 15:
                prefixType = $('#purchaseReturnsPrefix').val();
                digits = $('#purchaseReturnsDigits').val();
                startingNumber = $('#purchaseReturnsNextNumber').val();
                break;
            case 16:
                prefixType = $('#transferPrefix').val();
                digits = $('#transferDigits').val();
                startingNumber = $('#transferNextNumber').val();
                break;
            case 17:
                prefixType = $('#creditNotePaymentsPrefix').val();
                digits = $('#creditNotePaymentsDigits').val();
                startingNumber = $('#creditNotePaymentsNextNumber').val();
                break;
            case 18:
                prefixType = $('#debitNotePaymentsPrefix').val();
                digits = $('#debitNotePaymentsDigits').val();
                startingNumber = $('#debitNotePaymentsNextNumber').val();
                break;
            case 20:
                prefixType = $('#customerPrefix').val();
                digits = $('#customerDigits').val();
                startingNumber = $('#customerNextNumber').val();
                break;
            case 21:
                prefixType = $('#projectPrefix').val();
                digits = $('#projectDigits').val();
                startingNumber = $('#projectNextNumber').val();
                break;
            case 22:
                prefixType = $('#jobPrefix').val();
                digits = $('#jobDigits').val();
                startingNumber = $('#jobNextNumber').val();
                break;
            case 23:
                prefixType = $('#activityPrefix').val();
                digits = $('#activityDigits').val();
                startingNumber = $('#activityNextNumber').val();
                break;
            case 24:
                prefixType = $('#inquiryLogPrefix').val();
                digits = $('#inquiryLogDigits').val();
                startingNumber = $('#inquiryLogNextNumber').val();
                break;
            case 25:
                prefixType = $('#suspendedTaxInvoicePrefix').val();
                digits = $('#suspendedTaxInvoiceDigits').val();
                startingNumber = $('#suspendedTaxInvoiceNextNumber').val();
                break;
            case 26:
                prefixType = $('#expensePaymentVoucherPrefix').val();
                digits = $('#expensePaymentVoucherDigits').val();
                startingNumber = $('#expensePaymentVoucherNextNumber').val();
            case 27:
                prefixType = $('#pettyCashVoucherPrefix').val();
                digits = $('#pettyCashVoucherDigits').val();
                startingNumber = $('#pettyCashVoucherNextNumber').val();
                break;
            case 29:
                prefixType     = $('#dispatchNotePrefix').val();
                digits         = $('#dispatchNoteDigits').val();
                startingNumber = $('#dispatchNoteNextNumber').val();
                break;
            case 30:
                prefixType     = $('#journalEntryPrefix').val();
                digits         = $('#journalEntryDigits').val();
                startingNumber = $('#journalEntryNextNumber').val();
                break;
            case 31:
                prefixType     = $('#journalEntryTemplatePrefix').val();
                digits         = $('#journalEntryTemplateDigits').val();
                startingNumber = $('#journalEntryTemplateNextNumber').val();
                break;
            case 33:
                prefixType     = $('#itemPrefix').val();
                digits         = $('#itemDigits').val();
                startingNumber = $('#itemNextNumber').val();
                break;
            case 34:
                prefixType     = $('#supplierPrefix').val();
                digits         = $('#supplierDigits').val();
                startingNumber = $('#supplierNextNumber').val();
                break;
            case 39:
                prefixType     = $('#pRequPrefix').val();
                digits         = $('#pRequDigits').val();
                startingNumber = $('#pRequNextNumber').val();
                break;
            case 42:
                prefixType     = $('#incomePrefix').val();
                digits         = $('#incomeDigits').val();
                startingNumber = $('#incomeNextNumber').val();
                break;
        }
        if ($(this).is('.nextNumber')) {

            if (prefixType) {
                if (isNaN(startingNumber)) {
                    $(this).val('');
                    p_notification(false, eb.getMessage('ERR_REFER_NEXTINT', array[prefixID]));
                }
            } else {
                $(this).val('');
                p_notification(false, eb.getMessage('ERR_REFER_PREFIXFIRST', array[prefixID]));
            }
        } else if ($(this).is('.digits')) {

            if (prefixType) {
                if (isNaN(digits)) {
                    $(this).val('');
                    p_notification(false, eb.getMessage('ERR_REFER_INTEGER', array[prefixID]));
                }
            } else {
                $(this).val('');
                p_notification(false, eb.getMessage('ERR_REFER_PREFIXFIRST', array[prefixID]));
            }
        } else if ($(this).is('.prefix')) {
            if (!$(this).val()) {
                $(this).parent('div').parent('div').find('input[class^=digits]').val('');
                $(this).parent('div').parent('div').find('input[class^=nextNumber]').val('');
            }

        }
    });

    $('.locationNextNumber, .locationDigits, .locationPrefix').on('keyup', function() {
        var locationName = $(this).parent('td').parent('tr').find('.locationName').text();
        if ($(this).is('.locationNextNumber')) {
            if ($(this).parent('td').parent('tr').find('input[name^=prefix]').val()) {
                if (isNaN($(this).val())) {
                    $(this).val('');
                    p_notification(false, eb.getMessage('ERR_REFER_LOCNEXT_INT', locationName));
                }
            } else {
                $(this).val('');
                p_notification(false, eb.getMessage('ERR_REFER_LOCPREF', locationName));
            }
        } else if ($(this).is('.locationDigits')) {
            if ($(this).parent('td').parent('tr').find('input[name^=prefix]').val()) {
                if (isNaN($(this).val())) {
                    $(this).val('');
                    p_notification(false, eb.getMessage('ERR_REFER_LOCDIGIT', locationName));
                }
            } else {
                $(this).val('');
                p_notification(false, eb.getMessage('ERR_REFER_LOCPREF', locationName));
            }
        } else if ($(this).is('.locationPrefix')) {
            if (!$(this).val()) {
                $(this).parent('td').parent('tr').find('input[name^=numberOfDigits]').val('');
                $(this).parent('td').parent('tr').find('input[name^=startingNumber]').val('');
            }
        }
    });


    $('.exaPrefix,.exaDigits,.exaNextNumber').on('keyup', function() {
        var $b;
        var locationName = $(this).parent('td').parent('tr').find('.locationName').text();
        $b = $(this).attr('data-id');

        var prefixTemp = 'exaPrefix_' + $b;
        prefixType = $('.exaPrefix_' + $b).val();
        if (!(prefixType.substr(prefixType.length - 1).match(/^[0-9a-zA-Z_]+$/))) {

            document.getElementById(prefixTemp).value = prefixType.substring(0, prefixType.length - 1);
            p_notification(false, eb.getMessage('ERR_REF_PRE', locationName));
            return false;
        }

        digits = $('.exaDigits_' + $b).val();
        startingNumber = $('.exaNextNumber_' + $b).val();

        output = prefixType + pad(parseInt(startingNumber), digits);
        var last = output.substr(output.length - 3);
        if (last == 'NaN') {
            output = output.substring(0, output.length - 3);
        }
        if (prefixType == null) {
            output = "";
            $('#exampleNumber_' + $b).val(output);
        }
        $('#exampleNumber_' + $b).val(output);
        temp = 'example_' + $b;




    });

    $('#saveLocationPrefix,.close,#locationPrefixForm').on('click', function() {
        $('.inputExample').show();
        $('.lableExample').hide();

    });

    $('.exNextNumber1, .exDigits1, .exPrefix1').on('keyup', function() {
        prefixType = $('.exPrefix1').val();
        digits = $('.exDigits1').val();
        startingNumber = $('.exNextNumber1').val();

        output = prefixType + pad(parseInt(startingNumber), digits);
        var last = output.substr(output.length - 3);
        if (last == 'NaN') {
            output = output.substring(0, output.length - 3);
        }
        $('#inputExample1').val(output);

    });

    $('.exNextNumber2, .exDigits2, .exPrefix2').on('keyup', function() {
        prefixType = $('.exPrefix2').val();
        digits = $('.lahiru').val();
        startingNumber = $('.exNextNumber2').val();
        output = prefixType + pad(parseInt(startingNumber), digits);
        var last = output.substr(output.length - 3);
        if (last == 'NaN') {
            output = output.substring(0, output.length - 3);
        }
        $('#inputExample2').val(output);
    });

    $('.exNextNumber3, .exDigits3, .exPrefix3').on('keyup', function() {
        prefixType = $('.exPrefix3').val();
        digits = $('.exDigits3').val();
        startingNumber = $('.exNextNumber3').val();
        output = prefixType + pad(parseInt(startingNumber), digits);
        var last = output.substr(output.length - 3);
        if (last == 'NaN') {
            output = output.substring(0, output.length - 3);
        }
        $('#inputExample3').val(output);
    });

    $('.exNextNumber4, .exDigits4, .exPrefix4').on('keyup', function() {
        prefixType = $('.exPrefix4').val();
        digits = $('.exDigits4').val();
        startingNumber = $('.exNextNumber4').val();
        output = prefixType + pad(parseInt(startingNumber), digits);
        var last = output.substr(output.length - 3);
        if (last == 'NaN') {
            output = output.substring(0, output.length - 3);
        }
        $('#inputExample4').val(output);
    });

    $('.exNextNumber5, .exDigits5, .exPrefix5').on('keyup', function() {
        prefixType = $('.exPrefix5').val();
        digits = $('.exDigits5').val();
        startingNumber = $('.exNextNumber5').val();
        output = prefixType + pad(parseInt(startingNumber), digits);
        var last = output.substr(output.length - 3);
        if (last == 'NaN') {
            output = output.substring(0, output.length - 3);
        }
        $('#inputExample5').val(output);
    });

    $('.exNextNumber6, .exDigits6, .exPrefix6').on('keyup', function() {
        prefixType = $('.exPrefix6').val();
        digits = $('.exDigits6').val();
        startingNumber = $('.exNextNumber6').val();
        output = prefixType + pad(parseInt(startingNumber), digits);
        var last = output.substr(output.length - 3);
        if (last == 'NaN') {
            output = output.substring(0, output.length - 3);
        }
        $('#inputExample6').val(output);
    });

    $('.exNextNumber7, .exDigits7, .exPrefix7').on('keyup', function() {
        prefixType = $('.exPrefix7').val();
        digits = $('.exDigits7').val();
        startingNumber = $('.exNextNumber7').val();
        output = prefixType + pad(parseInt(startingNumber), digits);
        var last = output.substr(output.length - 3);
        if (last == 'NaN') {
            output = output.substring(0, output.length - 3);
        }
        $('#inputExample7').val(output);
    });

    $('.exNextNumber8, .exDigits8, .exPrefix8').on('keyup', function() {
        prefixType = $('.exPrefix8').val();
        digits = $('.exDigits8').val();
        startingNumber = $('.exNextNumber8').val();
        output = prefixType + pad(parseInt(startingNumber), digits);
        var last = output.substr(output.length - 3);
        if (last == 'NaN') {
            output = output.substring(0, output.length - 3);
        }
        $('#inputExample8').val(output);
    });

    $('.exNextNumber9, .exDigits9, .exPrefix9').on('keyup', function() {
        prefixType = $('.exPrefix9').val();
        digits = $('.exDigits9').val();
        startingNumber = $('.exNextNumber9').val();
        output = prefixType + pad(parseInt(startingNumber), digits);
        var last = output.substr(output.length - 3);
        if (last == 'NaN') {
            output = output.substring(0, output.length - 3);
        }
        $('#inputExample9').val(output);
    });

    $('.exNextNumber10, .exDigits10, .exPrefix10').on('keyup', function() {
        prefixType = $('.exPrefix10').val();
        digits = $('.exDigits10').val();
        startingNumber = $('.exNextNumber10').val();
        output = prefixType + pad(parseInt(startingNumber), digits);
        var last = output.substr(output.length - 3);
        if (last == 'NaN') {
            output = output.substring(0, output.length - 3);
        }
        $('#inputExample10').val(output);
    });

    $('.exNextNumber11, .exDigits11, .exPrefix11').on('keyup', function() {
        prefixType = $('.exPrefix11').val();
        digits = $('.exDigits11').val();
        startingNumber = $('.exNextNumber11').val();
        output = prefixType + pad(parseInt(startingNumber), digits);
        var last = output.substr(output.length - 3);
        if (last == 'NaN') {
            output = output.substring(0, output.length - 3);
        }
        $('#inputExample11').val(output);
    });

    $('.exNextNumber12, .exDigits12, .exPrefix12').on('keyup', function() {
        prefixType = $('.exPrefix12').val();
        digits = $('.exDigits12').val();
        startingNumber = $('.exNextNumber12').val();
        output = prefixType + pad(parseInt(startingNumber), digits);
        var last = output.substr(output.length - 3);
        if (last == 'NaN') {
            output = output.substring(0, output.length - 3);
        }
        $('#inputExample12').val(output);
    });

    $('.exNextNumber13, .exDigits13, .exPrefix13').on('keyup', function() {
        prefixType = $('.exPrefix13').val();
        digits = $('.exDigits13').val();
        startingNumber = $('.exNextNumber13').val();
        output = prefixType + pad(parseInt(startingNumber), digits);
        var last = output.substr(output.length - 3);
        if (last == 'NaN') {
            output = output.substring(0, output.length - 3);
        }
        $('#inputExample13').val(output);
    });

    $('.exNextNumber14, .exDigits14, .exPrefix14').on('keyup', function() {
        prefixType = $('.exPrefix14').val();
        digits = $('.exDigits14').val();
        startingNumber = $('.exNextNumber14').val();
        output = prefixType + pad(parseInt(startingNumber), digits);
        var last = output.substr(output.length - 3);
        if (last == 'NaN') {
            output = output.substring(0, output.length - 3);
        }
        $('#inputExample14').val(output);
    });

    $('.exNextNumber15, .exDigits15, .exPrefix15').on('keyup', function() {
        prefixType = $('.exPrefix15').val();
        digits = $('.exDigits15').val();
        startingNumber = $('.exNextNumber15').val();
        output = prefixType + pad(parseInt(startingNumber), digits);
        var last = output.substr(output.length - 3);
        if (last == 'NaN') {
            output = output.substring(0, output.length - 3);
        }
        $('#inputExample15').val(output);
    });

    $('.exNextNumber16, .exDigits16, .exPrefix16').on('keyup', function() {
        prefixType = $('.exPrefix16').val();
        digits = $('.exDigits16').val();
        startingNumber = $('.exNextNumber16').val();
        output = prefixType + pad(parseInt(startingNumber), digits);
        var last = output.substr(output.length - 3);
        if (last == 'NaN') {
            output = output.substring(0, output.length - 3);
        }
        $('#inputExample16').val(output);
    });
    $('.exNextNumber17, .exDigits17, .exPrefix17').on('keyup', function() {
        prefixType = $('.exPrefix17').val();
        digits = $('.exDigits17').val();
        startingNumber = $('.exNextNumber17').val();
        output = prefixType + pad(parseInt(startingNumber), digits);
        var last = output.substr(output.length - 3);
        if (last == 'NaN') {
            output = output.substring(0, output.length - 3);
        }
        $('#inputExample17').val(output);
    });
    $('.exNextNumber18, .exDigits18, .exPrefix18').on('keyup', function() {
        prefixType = $('.exPrefix18').val();
        digits = $('.exDigits18').val();
        startingNumber = $('.exNextNumber18').val();
        output = prefixType + pad(parseInt(startingNumber), digits);
        var last = output.substr(output.length - 3);
        if (last == 'NaN') {
            output = output.substring(0, output.length - 3);
        }
        $('#inputExample18').val(output);
    });

    $('.exNextNumber20, .exDigits20, .exPrefix20').on('keyup', function() {
        prefixType = $('.exPrefix20').val();
        digits = $('.exDigits20').val();
        startingNumber = $('.exNextNumber20').val();
        output = prefixType + pad(parseInt(startingNumber), digits);
        var last = output.substr(output.length - 3);
        if (last == 'NaN') {
            output = output.substring(0, output.length - 3);
        }
        $('#inputExample20').val(output);
    });
    $('.exNextNumber21, .exDigits21, .exPrefix21').on('keyup', function() {
        prefixType = $('.exPrefix21').val();
        digits = $('.exDigits21').val();
        startingNumber = $('.exNextNumber21').val();
        output = prefixType + pad(parseInt(startingNumber), digits);
        var last = output.substr(output.length - 3);
        if (last == 'NaN') {
            output = output.substring(0, output.length - 3);
        }
        $('#inputExample21').val(output);
    });
    $('.exNextNumber22, .exDigits22, .exPrefix22').on('keyup', function() {
        prefixType = $('.exPrefix22').val();
        digits = $('.exDigits22').val();
        startingNumber = $('.exNextNumber22').val();
        output = prefixType + pad(parseInt(startingNumber), digits);
        var last = output.substr(output.length - 3);
        if (last == 'NaN') {
            output = output.substring(0, output.length - 3);
        }
        $('#inputExample22').val(output);
    });
    $('.exNextNumber23, .exDigits23, .exPrefix23').on('keyup', function() {
        prefixType = $('.exPrefix23').val();
        digits = $('.exDigits23').val();
        startingNumber = $('.exNextNumber23').val();
        output = prefixType + pad(parseInt(startingNumber), digits);
        var last = output.substr(output.length - 3);
        if (last == 'NaN') {
            output = output.substring(0, output.length - 3);
        }
        $('#inputExample23').val(output);
    });
    $('.exNextNumber24, .exDigits24, .exPrefix24').on('keyup', function() {
        prefixType = $('.exPrefix24').val();
        digits = $('.exDigits24').val();
        startingNumber = $('.exNextNumber24').val();
        output = prefixType + pad(parseInt(startingNumber), digits);
        var last = output.substr(output.length - 3);
        if (last == 'NaN') {
            output = output.substring(0, output.length - 3);
        }
        $('#inputExample24').val(output);
    });
    $('.exNextNumber25, .exDigits25, .exPrefix25').on('keyup', function() {
        prefixType = $('.exPrefix25').val();
        digits = $('.exDigits25').val();
        startingNumber = $('.exNextNumber25').val();
        output = prefixType + pad(parseInt(startingNumber), digits);
        var last = output.substr(output.length - 3);
        if (last == 'NaN') {
            output = output.substring(0, output.length - 3);
        }
        $('#inputExample25').val(output);
    });
    $('.exNextNumber26, .exDigits26, .exPrefix26').on('keyup', function() {
        prefixType = $('.exPrefix26').val();
        digits = $('.exDigits26').val();
        startingNumber = $('.exNextNumber26').val();
        output = prefixType + pad(parseInt(startingNumber), digits);
        var last = output.substr(output.length - 3);
        if (last == 'NaN') {
            output = output.substring(0, output.length - 3);
        }
        $('#inputExample26').val(output);
    });
    $('.exNextNumber27, .exDigits27, .exPrefix27').on('keyup', function() {
        prefixType = $('.exPrefix27').val();
        digits = $('.exDigits27').val();
        startingNumber = $('.exNextNumber27').val();
        output = prefixType + pad(parseInt(startingNumber), digits);
        var last = output.substr(output.length - 3);
        if (last == 'NaN') {
            output = output.substring(0, output.length - 3);
        }
        $('#inputExample27').val(output);
    });
    $('.exNextNumber29, .exDigits29, .exPrefix29').on('keyup', function() {
        prefixType = $('.exPrefix29').val();
        digits     = $('.exDigits29').val();
        startingNumber = $('.exNextNumber29').val();
        output     = prefixType + pad(parseInt(startingNumber), digits);
        var last   = output.substr(output.length - 3);
        if (last == 'NaN') {
            output = output.substring(0, output.length - 3);
        }        
        $('#inputExample29').val(output);
    });
    $('.jeNextNumber30, .jeDigits30, .jePrefix30').on('keyup', function() {
        prefixType = $('.jePrefix30').val();
        digits     = $('.jeDigits30').val();
        startingNumber = $('.jeNextNumber30').val();
        output     = prefixType + pad(parseInt(startingNumber), digits);
        var last   = output.substr(output.length - 3);
        if (last == 'NaN') {
            output = output.substring(0, output.length - 3);
        }        
        $('#inputExample30').val(output);
    });
    $('.jeNextNumber31, .jeDigits31, .jePrefix31').on('keyup', function() {
        prefixType = $('.jePrefix31').val();
        digits     = $('.jeDigits31').val();
        startingNumber = $('.jeNextNumber31').val();
        output     = prefixType + pad(parseInt(startingNumber), digits);
        var last   = output.substr(output.length - 3);
        if (last == 'NaN') {
            output = output.substring(0, output.length - 3);
        }        
        $('#inputExample31').val(output);
    });

    $('.exNextNumber33, .exDigits33, .exPrefix33').on('keyup', function() {
        prefixType = $('.exPrefix33').val();
        digits     = $('.exDigits33').val();
        startingNumber = $('.exNextNumber33').val();
        output     = prefixType + pad(parseInt(startingNumber), digits);
        var last   = output.substr(output.length - 3);
        if (last == 'NaN') {
            output = output.substring(0, output.length - 3);
        }        
        $('#inputExample33').val(output);
    });

    $('.exNextNumber34, .exDigits34, .exPrefix34').on('keyup', function() {
        prefixType = $('.exPrefix34').val();
        digits     = $('.exDigits34').val();
        startingNumber = $('.exNextNumber34').val();
        output     = prefixType + pad(parseInt(startingNumber), digits);
        var last   = output.substr(output.length - 3);
        if (last == 'NaN') {
            output = output.substring(0, output.length - 3);
        }        
        $('#inputExample34').val(output);
    });

    $('.exNextNumber39, .exDigits39, .exPrefix39').on('keyup', function() {
        prefixType = $('.exPrefix39').val();
        digits     = $('.exDigits39').val();
        startingNumber = $('.exNextNumber39').val();
        output     = prefixType + pad(parseInt(startingNumber), digits);
        var last   = output.substr(output.length - 3);
        if (last == 'NaN') {
            output = output.substring(0, output.length - 3);
        }        
        $('#inputExample39').val(output);
    });

    $('.exNextNumber42, .exDigits42, .exPrefix42').on('keyup', function() {
        prefixType = $('.exPrefix42').val();
        digits     = $('.exDigits42').val();
        startingNumber = $('.exNextNumber42').val();
        output     = prefixType + pad(parseInt(startingNumber), digits);
        var last   = output.substr(output.length - 3);
        if (last == 'NaN') {
            output = output.substring(0, output.length - 3);
        }        
        $('#inputExample42').val(output);
    });

    $('.savePrefixData').on('click', function(e) {
        e.preventDefault();
        arr = new Array();

        if ($(this).data('id') === undefined) {

            p_notification(false, "Please check global or local reference");

            p_notification(false, eb.getMessage('ERR_REFER_GLOBAl_REF'));

            return false;
        }
        var prefixID = $(this).data('id').toString();
        var type = $(this).data('type').toString();
        var prefixType;
        var digits;
        var startingNumber;
        var flag;
        var output

        if (type == 'global') {
            //get global reference data for save
            switch (prefixID) {
                case '1':
                    prefixType = $('#quotationPrefix').val();
                    digits = $('#quotationDigits').val();
                    startingNumber = $('#QuotationNextNumber').val();
                    break;
                case '2':
                    prefixType = $('#salesOrderPrefix').val();
                    digits = $('#salesOrderDigits').val();
                    startingNumber = $('#salesOrderNextNumber').val();
                    break;
                case '3':
                    prefixType = $('#invoicePrefix').val();
                    digits = $('#invoiceDigits').val();
                    startingNumber = $('#invoiceNextNumber').val();
                    break;
                case '4':
                    prefixType = $('#paymentPrefix').val();
                    digits = $('#paymentDigits').val();
                    startingNumber = $('#paymentNextNumber').val();
                    break;
                case '5':
                    prefixType = $('#advancePaymentPrefix').val();
                    digits = $('#advancePaymentDigits').val();
                    startingNumber = $('#advancePaymentNextNumber').val();
                    break;
                case '6':
                    prefixType = $('#deliveryNotePrefix').val();
                    digits = $('#diliveryNoteDigits').val();
                    startingNumber = $('#deliveryNoteNextNumber').val();
                    break;
                case '7':
                    prefixType = $('#salesReturnsPrefix').val();
                    digits = $('#salesReturnsDigits').val();
                    startingNumber = $('#salesReturnsNextNumber').val();
                    break;
                case '8':
                    prefixType = $('#creditNotePrefix').val();
                    digits = $('#creditNoteDigits').val();
                    startingNumber = $('#creditNoteNextNumber').val();
                    break;
                case '9':
                    prefixType = $('#purchaseOrderPrefix').val();
                    digits = $('#purchaseOrderDigits').val();
                    startingNumber = $('#purchaseOrderNextNumber').val();
                    break;
                case '10':
                    prefixType = $('#grnPrefix').val();
                    digits = $('#grnDigits').val();
                    startingNumber = $('#grnNextNumber').val();
                    break;
                case '11':
                    prefixType = $('#debitNotePrefix').val();
                    digits = $('#debitNoteDigits').val();
                    startingNumber = $('#debitNoteNextNumber').val();
                    break;
                case '12':
                    prefixType = $('#adjustmentPrefix').val();
                    digits = $('#adjustmentDigits').val();
                    startingNumber = $('#adjustmentNextNumber').val();
                    break;
                case '13':
                    prefixType = $('#supplierPaymentsPrefix').val();
                    digits = $('#supplierPaymentsDigits').val();
                    startingNumber = $('#supplierPaymentsNextNumber').val();
                    break;
                case '14':
                    prefixType = $('#purchaseInvoicePrefix').val();
                    digits = $('#purchaseInvoiceDigits').val();
                    startingNumber = $('#purchaseInvoiceNextNumber').val();
                    break;
                case '15':
                    prefixType = $('#purchaseReturnsPrefix').val();
                    digits = $('#purchaseReturnsDigits').val();
                    startingNumber = $('#purchaseReturnsNextNumber').val();
                    break;
                case '16':
                    prefixType = $('#transferPrefix').val();
                    digits = $('#transferDigits').val();
                    startingNumber = $('#transferNextNumber').val();
                    break;
                case '17':
                    prefixType = $('#creditNotePaymentsPrefix').val();
                    digits = $('#creditNotePaymentsDigits').val();
                    startingNumber = $('#creditNotePaymentsNextNumber').val();
                    break;
                case '18':
                    prefixType = $('#debitNotePaymentsPrefix').val();
                    digits = $('#debitNotePaymentsDigits').val();
                    startingNumber = $('#debitNotePaymentsNextNumber').val();
                    break;
                case '20':
                    prefixType = $('#customerPrefix').val();
                    digits = $('#customerDigits').val();
                    startingNumber = $('#customerNextNumber').val();
                    break;
                case '21':
                    prefixType = $('#projectPrefix').val();
                    digits = $('#projectDigits').val();
                    startingNumber = $('#projectNextNumber').val();
                    break;
                case '22':
                    prefixType = $('#jobPrefix').val();
                    digits = $('#jobDigits').val();
                    startingNumber = $('#jobNextNumber').val();
                    break;
                case '23':
                    prefixType = $('#activityPrefix').val();
                    digits = $('#activityDigits').val();
                    startingNumber = $('#activityNextNumber').val();
                    break;
                case '24':
                    prefixType = $('#inquiryLogPrefix').val();
                    digits = $('#inquiryLogDigits').val();
                    startingNumber = $('#inquiryLogNextNumber').val();
                    break;
                case '25':
                    prefixType = $('#suspendedTaxInvoicePrefix').val();
                    digits = $('#suspendedTaxInvoiceDigits').val();
                    startingNumber = $('#suspendedTaxInvoiceNextNumber').val();
                    break;
                case '26':
                    prefixType = $('#expensePaymentVoucherPrefix').val();
                    digits = $('#expensePaymentVoucherDigits').val();
                    startingNumber = $('#expensePaymentVoucherNextNumber').val();
                    break;
                case '27':
                    prefixType = $('#pettyCashVoucherPrefix').val();
                    digits = $('#pettyCashVoucherDigits').val();
                    startingNumber = $('#pettyCashVoucherNextNumber').val();
                    break;
                case '29':
                    prefixType     = $('#dispatchNotePrefix').val();
                    digits         = $('#dispatchNoteDigits').val();
                    startingNumber = $('#dispatchNoteNextNumber').val();
                    break;
                case '30':
                    prefixType     = $('#journalEntryPrefix').val();
                    digits         = $('#journalEntryDigits').val();
                    startingNumber = $('#journalEntryNextNumber').val();
                    break;
                case '31':
                    prefixType     = $('#journalEntryTemplatePrefix').val();
                    digits         = $('#journalEntryTemplateDigits').val();
                    startingNumber = $('#journalEntryTemplateNextNumber').val();
                    break;
                case '33':
                    prefixType     = $('#itemPrefix').val();
                    digits         = $('#itemDigits').val();
                    startingNumber = $('#itemNextNumber').val();
                    break;
                case '34':
                    prefixType     = $('#supplierPrefix').val();
                    digits         = $('#supplierDigits').val();
                    startingNumber = $('#supplierNextNumber').val();
                    break;
                case '39':
                    prefixType     = $('#pRequPrefix').val();
                    digits         = $('#pRequDigits').val();
                    startingNumber = $('#pRequNextNumber').val();
                    break;
                case '42':
                    prefixType     = $('#incomePrefix').val();
                    digits         = $('#incomeDigits').val();
                    startingNumber = $('#incomeNextNumber').val();
                    break;
            }
            arr[prefixID] = new updateReference();
            arr[prefixID].id = prefixID;
            arr[prefixID].referenceName = array[prefixID];
            arr[prefixID].type = prefixType;
            arr[prefixID].digits = digits;
            arr[prefixID].nextNumber = startingNumber;
            arr[prefixID].localGlobleType = '0';
            flag = true;

            if (!prefixType) {
                p_notification(false, eb.getMessage('ERR_REFER_PREFNULL', array[prefixID]));
                $(this).parent().parent().find('.prefix').focus();
                flag = false;
            } else if (!digits) {
                p_notification(false, eb.getMessage('ERR_REFER_DIGITNULL', array[prefixID]));

                $(this).parent().parent().find('.digits').focus();
                flag = false;
            } else if (digits < 1) {
                p_notification(false, eb.getMessage('ERR_REFER_DIGILIMIT', array[prefixID]));
                $(this).parent().parent().find('.digits').focus();
                flag = false;
            } else if (!startingNumber) {
                p_notification(false, eb.getMessage('ERR_REFER_NEXTNUM_NULL', array[prefixID]));
                $(this).parent().parent().find('.nextNumber').focus();
                flag = false;
            } else if (!prefixType.match(/^[\S]+$/)) {
                p_notification(false, array[prefixID] + ' Prefix cann\'t have Spaces.');
                $(this).parent().parent().find('.prefix').focus();
                flag = false;
            }

        }
        else if (type == 'local') {
            //get location wise reference data for save
            $('#location-prefix-data tr').each(function() {
                if ($(this).find('input[name^="prefix"]').val() || $(this).find('input[name^="numberOfDigits"]').val() || $(this).find('input[name^="startingNumber"]').val()) {
                    arr[$(this).data('id')] = new updateReference();
                    //check if arr have same prefix, @auther sandun dissanayake
                    for (var i in arr) {
                        if (arr[i].type == $(this).find('input[name^="prefix"]').val()) {
                            if (arr[i].digits == $(this).find('input[name^="numberOfDigits"]').val()) {
                                p_notification(false, eb.getMessage('ERR_REFER_KEEPDIGIT'));
                                $(this).find('input[name^="numberOfDigits"]').focus();
                                return flag = false;
                            }
                        }
                    }
                    flag = true;
                    arr[$(this).data('id')].id = prefixID;
                    arr[$(this).data('id')].referenceName = array[prefixID];
                    arr[$(this).data('id')].locationID = $(this).data('id');
                    arr[$(this).data('id')].type = $(this).find('input[name^="prefix"]').val();
                    arr[$(this).data('id')].digits = $(this).find('input[name^="numberOfDigits"]').val();
                    arr[$(this).data('id')].nextNumber = $(this).find('input[name^="startingNumber"]').val();
                    arr[$(this).data('id')].localGlobleType = '1';


                    var locationName = $(this).find('.locationName').text();
                    if (!$(this).find('input[name^="prefix"]').val()) {
                        p_notification(false, eb.getMessage('ERR_REFER_NOPREFNULL', locationName, array[prefixID]));
                        $(this).parent().parent().find('.prefix').focus();
                        return flag = false;

                    } else if (!($(this).find('input[name^="prefix"]').val()).match(/^[0-9a-zA-Z]+$/)) {
                        p_notification(false, locationName + ' ' + array[prefixID] + ' Prefix can only Alphanumeric Values.');
                        $(this).parent().parent().find('.prefix').focus();
                        return flag = false;
                    } else if (!$(this).find('input[name^="numberOfDigits"]').val()) {
                        p_notification(false, eb.getMessage('ERR_REFER_DIGIT_NONULL', locationName, array[prefixID]));
                        $(this).parent().parent().find('.digits').focus();
                        return flag = false;
                    } else if ($(this).find('input[name^="numberOfDigits"]').val() < 1) {
                        p_notification(false, eb.getMessage('ERR_REFER_DIGITLIMIT', locationName, array[prefixID]));
                        $(this).parent().parent().find('.digits').focus();
                        return flag = false;
                    } else if (!$(this).find('input[name^="startingNumber"]').val()) {
                        p_notification(false, eb.getMessage('ERR_REFER_NEXTNUM_NONULL', locationName, array[prefixID]));
                        $(this).parent().parent().find('.nextNumber').focus();
                        return flag = false;
                    }
                }

            });
        }
        //if prefix,numberOfDigits texfeild validation is okay,@auther sandun dissanayake
        if (flag === true) {

            var prefixData = {};
            for (var i in arr) {
                if (arr[i].type != 0 || arr[i].id != 0) {
                    prefixData[i] = JSON.stringify(arr[i]);
                }
            }
            //ajax post for upadate and save acton of reference
            eb.ajax({
                type: 'post',
                url: BASE_URL + '/companyAPI/prefixUpdate',
                data: {
                    prefixData: prefixData,
                },
                dataType: "json",
                success: function(data) {
                    p_notification(data.status, data.msg);
                    if (data.status) {
                        $('#locationPrefixForm').modal('hide');
                        var now = $('#' + saveButton[prefixID]);
                        $(now).parent().find('.editPrefixData').show();
                        $(now).parent().find('.savePrefixData').hide();
//                        $(now).parent('div').parent('div').parent('div').find('input[type=radio]').attr('disabled', true);
                        if ($(now).parent().parent().find('input:radio[value=global]').is(':checked')) {

                            $(now).parent().find('.editPrefixData').show();
                            $(now).parent().find('.savePrefixData').hide();
                            $(now).parent('div').parent('div').parent('div').find('input[class^="prefix"]').attr('disabled', true);
                            $(now).parent('div').parent('div').parent('div').find('input[class^="digits"]').attr('disabled', true);
                            $(now).parent('div').parent('div').parent('div').find('input[class^="nextNumber"]').attr('disabled', true);
                        }
                    }
                }
            });
        } else {
            if (jQuery.isEmptyObject(arr)) {
                p_notification(false, eb.getMessage('ERR_REFER_COULD_NOT_SET', array[prefixID]));
            }
        }
    });
});
