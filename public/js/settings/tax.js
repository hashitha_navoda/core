var editTaxID;
$(document).ready(function () {
    var taxEnabled = true;

    loadDropDownFromDatabase('/accounts-api/search-active-accounts-for-dropdown', "", "", '#taxSalesAccountID');
    loadDropDownFromDatabase('/accounts-api/search-active-accounts-for-dropdown', "", "", '#taxPurchaseAccountID');

    if ($("input[type=radio][name=has_tax]:checked").val() == "n") {
        $(".to_hide").hide();
        $("#taxpage").hide();
        taxEnabled = false;
    }

    $("input[name=has_tax]").on("change", function () {
        if ($(this).val() == "n") {
            taxEnabled = false;
            $(".to_hide").fadeOut();
            $("#tax_reg").val(null);
            var param = $('#addTax').serializeArray();
            $req = eb.post('/taxAPI/updateTRnumber', param);
            $req.done(function (res) {
                p_notification(true, eb.getMessage('ERR_TAX_TDISABLE'));
                $(".to_hide").hide();
                $("#taxpage").hide();
            });
        } else {
            $(".to_hide").fadeIn();
            $('#tax_reg').val($('#oldTaxRID').val());
        }
    });

    $('#taxRegister').submit(function (e) {
        e.preventDefault();
        if ($('#tax_reg').val() != '') {
            var data = {'TRnumber': $('#tax_reg').val(), };
            $req = eb.post('/taxAPI/updateTRnumber', data);
            $req.done(function (res) {
                if (res.data == true) {
                    taxEnabled = true;
                    $("#taxpage").show();
                    $('#oldTaxRID').val($('#tax_reg').val());
                }
                p_notification(res.data, res.msg);
            });
        } else {
            p_notification(false, eb.getMessage('ERR_TAX_REGNUM'));
            $('#tax_reg').focus();
        }
    });

    $('#addTax').submit(function (e) {
        e.preventDefault();
        if ($('#tax_reg').val() != '' && taxEnabled == true) {
            if ($('#tax_name').val() != '' && $('#amount').val() != '' && $('#amount').val() > 0) {
                if ($('#compound').is(':checked')) {
                    if ($('.taxes').is(':checked')) {
                        if ($('#saveTax').attr('value') == 'update') {
                            updateTax();
                        } else {
                            addTax();
                        }
                    } else {
                        p_notification(false, eb.getMessage('ERR_TAX_COMPONDTAX'));
                    }
                } else {
                    if ($('#saveTax').attr('value') == 'update') {
                        updateTax();
                    } else {
                        addTax();
                    }

                }
            }
            else if ($('#tax_name').val() == '') {
                p_notification(false, eb.getMessage('ERR_TAX_TNAME'));
            }
            else if ($('#amount').val() == '') {
                p_notification(false, eb.getMessage('ERR_TAX_TPERCENT'));
            } else if ($('#amount').val() < 0) {
                p_notification(false, eb.getMessage('ERR_TAX_NEGTPERCENT'));
            }
        } else {
            p_notification(false, eb.getMessage('ERR_TAX_TREGNUM'));
        }
    });

    $('#cancelTax').on('click', function (e) {
        e.preventDefault();
        $('#tax_id').val('');
        $('#tax_name').val('');
        $('#amount').val('');
        $('#saveTax').text('Add Tax').attr('value', 'save');
        $('#taxName').html('');
        $('#compound').prop('checked', false);
        $('.taxes').prop('checked', false);
        $('#tax_list').addClass('hidden');
        $('#compound').prop('disabled', false);
		$('#vd_tax').find('.taxes').attr('disabled',false);
		$('#taxSuspendable').attr('disabled',false).prop('checked',false);
    });

    $('#compound').on('click', function () {
        if ($('#compound').is(':checked')) {
            $(":checkbox[value='" + $('#tax_id').val() + "']").parent().addClass("hidden");
            $('#taxName').html($('#tax_name').val());
            $('#tax_list').removeClass('hidden');
        }
        else {
            $('#tax_list').addClass('hidden');
            $('#taxName').html('');
            $(":checkbox[value='" + $('#tax_id').val() + "']").parent().removeClass("hidden");
        }
    });
    $('#amount').on('blur', function () {
        if (isNaN($('#amount').val())) {
            p_notification(false, eb.getMessage('ERR_TAX_PERCENTNUM'));
            $('#amount').val("");
        }
    });

});

function editTax(id) {
    clearTaxForm();
    editTaxID = id;
    $('#tax_id').val(id);
    $('#saveTax').attr('value', 'update');
    $('#saveTax').text('Update Tax');
    $('#taxName').html('');
    $('#compound').prop('disabled', false);
    $('#vd_tax').find('.taxes').attr('disabled',false);
    $('#taxSuspendable').attr('disabled',false).prop('checked',false);
    $(window).scrollTop($('#detail_tax_edit').offset().top);
    $('#tax_name').focus();
    $req = eb.post('/taxAPI/getTax', {id: id});
    $req.done(function (res) {
        if (res) {
            if (res.tT == 'c') {
                $('#compound').prop('checked', true);
                $('#taxName').html($('#tax_name').val());
                for (var i in res.cT) {
                    $(":checkbox[value='" + res.cT[i] + "']").prop('checked', true);
                }
                $('#vd_tax').find('.taxes').attr('disabled','disabled');
                $('#tax_list').removeClass('hidden');
            } else {
                $('#tax_list').addClass('hidden');
            }
            $('#tax_name').val(res.tN);
            $('#amount').val(res.tP);
            $('#compound').prop('disabled', 'disabled');
            $('#taxSuspendable').prop('checked',(res.tS == 1));
            $('#taxSuspendable').attr('disabled','disabled');

    		if(res.tSAID != ''){
    			$('#taxSalesAccountID').append("<option value='"+res.tSAID+"'>"+res.tSAN+"</option>")
    			$('#taxSalesAccountID').val(res.tSAID).selectpicker('refresh');
   	 		}

   	 		if(res.tPAID != ''){
    			$('#taxPurchaseAccountID').append("<option value='"+res.tPAID+"'>"+res.tPAN+"</option>")
    			$('#taxPurchaseAccountID').val(res.tPAID).selectpicker('refresh');
   	 		}
        }
    });
}
function addTax() {
    var param = $('#addTax').serializeArray();
    $req = eb.post('/taxAPI/addTax', param);
    $req.done(function (res) {
        if (res.status == false) {
            p_notification(res.status, res.msg);
        } else {
            p_notification(res.status, res.msg);
            window.setTimeout(function () {
                location.reload();
            }, 1000);
        }
    });

}

function updateTax() {
	$('#compound').prop('disabled', false);
	$('#vd_tax').find('.taxes').attr('disabled',false);
	$('#taxSuspendable').attr('disabled',false);
    var param = $('#addTax').serialize();
    $req = eb.post('/taxAPI/updateTax', param);
    $req.done(function (res) {
        p_notification(res.status, res.msg);
        if (res.status) {
            clearTaxForm();
            $('#saveTax').attr('value', 'Add Tax');
            $('#cancelTax').trigger('click');
        }
        $('#taxList').html(res.html);
    });

}

function deleteTax(id, taxName) {
    bootbox.confirm("Are you sure you want to delete this Tax '" + taxName + "' ?", function (result) {
        if (result == true) {
            eb.ajax({
                type: 'POST',
                url: BASE_URL + '/taxAPI/deleteTax',
                data: {taxID: id},
                success: function (respond) {
                    p_notification(respond.status, respond.msg);
                    if (respond.status == true) {
                        $('#taxList').html(respond.html);
                        window.setTimeout(function () {
                            location.reload();
                        }, 1000);
                    }
                }
            });
        }
    });
}

function inactiveTax(id) {
    var currentState = $("#" + id + ".taxState").html();
    var sendState;
    if (currentState == 'Active') {
        sendState = 0;
    } else {
        sendState = 1;
    }
    $req = eb.post('/taxAPI/changeTaxState', {'id': id, 'state': sendState});
    $req.done(function (res) {
        if (sendState == 0) {
            $("#" + id + ".taxState").html('Inactive');
            $("#" + id + ".taxState").removeClass('label-success');
            $("#" + id + "-inactive").removeClass('fa fa-check-square-o');
            $("#" + id + "-inactive").addClass('fa fa-square-o');
            $("#" + id + ".taxState").addClass('label-danger');
        } else {
            $("#" + id + ".taxState").html('Active');
            $("#" + id + ".taxState").removeClass('label-danger');
            $("#" + id + "-inactive").removeClass('fa fa-square-o');
            $("#" + id + "-inactive").addClass('fa fa-check-square-o');
            $("#" + id + ".taxState").addClass('label-success');
        }
        p_notification(res.status, res.msg);
    });
}

function clearTaxForm() {
    $('#tax_name').val('');
    $('#amount').val('');
    $('#compound').prop('checked', false);
    $('.taxes').prop('checked', false);
    $('#taxSalesAccountID').find("option").remove().selectpicker("refresh");
    $('#taxSalesAccountID').append("<option value=''>Select an account</option>")
   	$('#taxSalesAccountID').val('').selectpicker('refresh');

    $('#taxPurchaseAccountID').find("option").remove().selectpicker("refresh");
   	$('#taxPurchaseAccountID').append("<option value=''>Select an account</option>")
   	$('#taxPurchaseAccountID').val('').selectpicker('refresh');
}
