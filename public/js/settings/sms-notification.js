$(document).ready(function () {
  var customerList = [];

  if ($("#smsTypeStatus").val() == 0) {
    $(".smsSetting_div").addClass("hidden");
    $("#smsEnable").prop("checked", false);
  } else {
    $("#smsEnable").prop("checked", true);
    $(".smsSetting_div").removeClass("hidden");
  }

  if ($("#hasIncludeInvoiceDetailsCheckbox").prop("checked") == true) {
    $("#sms_included_div").removeClass("hidden");
  }

  if (parseInt($('input[name="smsSendType"]:checked').val()) == 1) {
    $(".automatic-info").removeClass("hidden");
    $(".manual-info").addClass("hidden");
  } else {
    $(".automatic-info").addClass("hidden");
    $(".manual-info").removeClass("hidden");
  }

  if ($('input[name="day-frame"]:checked').val() == "month") {
    $("#month-input-tip").removeClass("hidden");
    $("#month-input").removeAttr("disabled");

    $("#week-input-tip").addClass("hidden");
    $("#week-input").prop("disabled", "disabled");

    $("#custom-input-tip").addClass("hidden");
    $("#custom-input").prop("disabled", "disabled");
  } else if ($('input[name="day-frame"]:checked').val() == "week") {
    $("#month-input-tip").addClass("hidden");
    $("#month-input").prop("disabled", "disabled");

    $("#week-input-tip").removeClass("hidden");
    $("#week-input").removeAttr("disabled");

    $("#custom-input-tip").addClass("hidden");
    $("#custom-input").prop("disabled", "disabled");
  } else if ($('input[name="day-frame"]:checked').val() == "custom") {
    $("#month-input-tip").addClass("hidden");
    $("#month-input").prop("disabled", "disabled");

    $("#week-input-tip").addClass("hidden");
    $("#week-input").prop("disabled", "disabled");

    $("#custom-input-tip").removeClass("hidden");
    $("#custom-input").removeAttr("disabled");
  }

  $("#smsEnable").change(function () {
    if ($(this).is(":checked")) {
      $(".smsSetting_div").removeClass("hidden");
    } else {
      $(".smsSetting_div").addClass("hidden");
      data = {
        smsTypeId: $("#smsTypeId").val(),
      };
      eb.ajax({
        type: "POST",
        url: BASE_URL + "/sms-notification-api/disableSmsType",
        data: {
          smsTypeId: $("#smsTypeId").val(),
        },
        success: function (respond) {
          p_notification(respond.status, respond.msg);
          if (respond.status) {
          }
        },
      });
    }
  });

  $("#hasIncludeInvoiceDetailsCheckbox").change(function () {
    if ($(this).is(":checked")) {
      $("#sms_included_div").removeClass("hidden");
    } else {
      $("#sms_included_div").addClass("hidden");
    }
  });

  $(".smsSendType", ".after-invoice-div").change(function () {
    if (parseInt(this.value) == 1) {
      $(".automatic-info").removeClass("hidden");
      $(".manual-info").addClass("hidden");
    } else {
      $(".automatic-info").addClass("hidden");
      $(".manual-info").removeClass("hidden");
    }
  });

  $(".smsSendType", ".due-balance-div").change(function () {
    if (parseInt(this.value) == 2) {
      eb.ajax({
        type: "POST",
        url: BASE_URL + "/invoice-api/getCustomersForDueBalanceCustomers",
        data: {},
        success: function (respond) {
          if (respond.data) {
            customerList = respond.data;
            $(".automatic-info").addClass("hidden");
            $(".manual-info").removeClass("hidden");
            $("#promoCustomerTable").removeClass("hidden");
            $("#promoCustomerTable").find(".promoCustomers").remove();

            for (var i in respond.data) {
              var $customerSample = $(
                "#customerSampleRow",
                "#promoCustomerTable"
              ).clone();
              $customerSample
                .find(".customerCode")
                .html(respond.data[i].customerCode);
              $customerSample
                .find(".customerName")
                .html(respond.data[i].customerName);
              $customerSample
                .find(".customerPhoneNumber")
                .html(respond.data[i].customerTelephoneNumber);
              $customerSample
                .find(".dueAmount")
                .html(respond.data[i].dueBalance);
              $customerSample
                .find(".noOfInvoices")
                .html(respond.data[i].invoiceCount);
              $customerSample.find("input[name='customer[]']").val(i);
              $customerSample.attr("id", "");
              $customerSample.addClass("promoCustomers");
              $customerSample.removeClass("hidden");
              $customerSample.removeClass("hidden");
              $customerSample.insertBefore(
                $("#customerSampleRow", "#promoCustomerTable")
              );
            }
          } else {
            $("#promoCustomerTable").addClass("hidden");
            $("#promoCustomerTable").find(".promoCustomers").remove();
            p_notification("info", eb.getMessage("ERR_PROMO_EMAIL_CUST_LIST"));
          }
        },
      });
    } else {
      $(".automatic-info").removeClass("hidden");
      $(".manual-info").addClass("hidden");
    }
  });

  $("#save-message").on("click", function (e) {
    var formData = {
      smsTypeId: $("#smsTypeId").val(),
      smsTypeMessage: $("#messageDescription").val(),
    };
    if (validateMessageData($("#messageDescription").val())) {
      eb.ajax({
        type: "POST",
        url: BASE_URL + "/sms-notification-api/saveSmsTypeMessage",
        data: formData,
        success: function (respond) {
          p_notification(respond.status, respond.msg);
          if (respond.status) {
          }
        },
      });
    }
  });

  function validateMessageData(data) {
    if (data == "") {
      p_notification(false, eb.getMessage("ERR_MESSAGE_EMPTY"));
      $("#messageDescription").focus();
      return false;
    }
    return true;
  }

  function validateDayFrameData(data) {
    if (data == "") {
      p_notification(false, eb.getMessage("ERR_DAY_FRAME_EMPTY"));
      $("#month-input").focus();
      return false;
    }
    return true;
  }

  $("#save-sms-setting", ".div-normal").on("click", function (e) {
    e.preventDefault();
    var checkedInvoicedDetails = [];
    $(".taxes").each(function (i) {
      if ($(this).is(":checked")) {
        checkedInvoicedDetails[i] = {
          value: $(this).val(),
          isChecked: 1,
        };
      } else {
        checkedInvoicedDetails[i] = {
          value: $(this).val(),
          isChecked: 0,
        };
      }
    });
    var formData = {
      smsTypeId: $("#smsTypeId").val(),
      smsTypeMessage: $("#messageDescription").val(),
      isIncludeInvoiceDetails: $("#hasIncludeInvoiceDetailsCheckbox").is(
        ":checked"
      )
        ? 1
        : 0,
      smsSendType: $('input[name="smsSendType"]:checked').val(),
      includeInVoiceDetails: checkedInvoicedDetails,
    };
    if (validateMessageData($("#messageDescription").val())) {
      eb.ajax({
        type: "POST",
        url: BASE_URL + "/sms-notification-api/updateSmsType",
        data: formData,
        success: function (respond) {
          p_notification(respond.status, respond.msg);
          if (respond.status) {
            window.setTimeout(function () {
              location.reload();
            }, 1500);
          }
        },
      });
    }
  });

  $("#btnSendSms").on("click", function () {
    selectedSmsCustomers = [];
    if ($(".customerSelected", "#promoCustomerTable").is(":checked")) {
      $(".customerSelected", "#promoCustomerTable").each(function () {
        if ($(this).is(":checked")) {
          if (typeof customerList[$(this).val()] !== "undefined") {
            selectedSmsCustomers.push({
              id: $(this).val(),
              dueAmount: customerList[$(this).val()].dueBalance,
              noOfDueInvoices: customerList[$(this).val()].invoiceCount,
              customerTelephoneNumber:
                customerList[$(this).val()].customerTelephoneNumber,
              customerName: customerList[$(this).val()].customerName,
              invoicesDetails: customerList[$(this).val()].invoicesDetails,
              lastInvoiceDate: customerList[$(this).val()].lastInvoiceDate,
              salesinvoiceTotalAmount:
                customerList[$(this).val()].salesinvoiceTotalAmount,
              salesPersonSortName:
                customerList[$(this).val()].salesPersonSortName,
              postdatedChequeDate:
                customerList[$(this).val()].postdatedChequeDate,
              incomingPaymentPaidAmount:
                customerList[$(this).val()].incomingPaymentPaidAmount,
              chequeNumber:
                customerList[$(this).val()].chequeNumber,
            });
          }
        }
      });

      var checkedInvoicedDetails = [];
      $(".taxes").each(function (i) {
        if ($(this).is(":checked")) {
          checkedInvoicedDetails[i] = $(this).val();
        }
      });

      var formData = {
        smsTypeMessage: $("#messageDescription").val(),
        selectedSmsCustomers: selectedSmsCustomers,
        smsTypeId: $("#smsTypeId").val(),
        isIncludeInvoiceDetails: $("#hasIncludeInvoiceDetailsCheckbox").is(
          ":checked"
        )
          ? 1
          : 0,
        smsIncluded: checkedInvoicedDetails,
      };
      if (validateMessageData($("#messageDescription").val())) {
        eb.ajax({
          type: "POST",
          url: BASE_URL + "/sms-notification-api/sendNotificationSms",
          data: formData,
          success: function (respond) {
            if (respond.status) {
              if (respond.data.error_msg.length > 0) {
                p_notification(false, respond.data.error_msg);
              }
              if (respond.data.success_msg.length > 0) {
                p_notification(true, respond.data.success_msg);
              }
              // window.setTimeout(function () {
              //   location.reload();
              // }, 1500);
            } else {
              p_notification(false, respond.msg);
            }
          },
        });
      }
    } else {
      p_notification(false, eb.getMessage("ERR_PROMO_EMAIL_CUST_EMPTY"));
    }
  });

  $("#select-all").click(function () {
    $(".customerSelected").prop("checked", this.checked).change();
  });

  $("input[name = 'day-frame']").change(function () {
    if (this.value == "month") {
      $("#month-input-tip").removeClass("hidden");
      $("#month-input").removeAttr("disabled");

      $("#week-input-tip").addClass("hidden");
      $("#week-input").prop("disabled", "disabled");
      $("#week-input").val('');

      $("#custom-input-tip").addClass("hidden");
      $("#custom-input").prop("disabled", "disabled");
    } else if (this.value == "week") {
      $("#month-input-tip").addClass("hidden");
      $("#month-input").prop("disabled", "disabled");

      $("#week-input-tip").removeClass("hidden");
      $("#week-input").removeAttr("disabled");

      $("#custom-input-tip").addClass("hidden");
      $("#custom-input").prop("disabled", "disabled");
    } else if (this.value == "custom") {
      $("#month-input-tip").addClass("hidden");
      $("#month-input").prop("disabled", "disabled");

      $("#week-input-tip").addClass("hidden");
      $("#week-input").prop("disabled", "disabled");

      $("#custom-input-tip").removeClass("hidden");
      $("#custom-input").removeAttr("disabled");
    }
  });
    
    $("#save-sms-setting", ".div-alt").on("click", function (e) {
        e.preventDefault();
        var checkedInvoicedDetails = [];
        $(".taxes").each(function (i) {
            if ($(this).is(":checked")) {
                checkedInvoicedDetails[i] = {
                value: $(this).val(),
                isChecked: 1,
                };
            } else {
                checkedInvoicedDetails[i] = {
                value: $(this).val(),
                isChecked: 0,
                };
            }
        });

        var selectedDayFrame = $('input[name="day-frame"]:checked').val();
        var smsTypeDayFrameDay = 0;

        if (selectedDayFrame == "month") {
            smsTypeDayFrameDay = $("#month-input").val();
        } else if (selectedDayFrame == "week") {
            smsTypeDayFrameDay = $("#week-input").val();
        } else if (selectedDayFrame == "custom") {
            smsTypeDayFrameDay = $("#custom-input").val();
        }

        var formData = {
          smsTypeId: $("#smsTypeId").val(),
          smsTypeMessage: $("#messageDescription").val(),
          isIncludeInvoiceDetails: $("#hasIncludeInvoiceDetailsCheckbox").is(
            ":checked"
          )
            ? 1
            : 0,
          smsSendType: $('input[name="smsSendType"]:checked').val(), // Automatic or manual
          includeInVoiceDetails: checkedInvoicedDetails,
          dayFrame: selectedDayFrame,
          smsTypeDayFrameDay: smsTypeDayFrameDay,
          smsTypeSelectedCustomerType: $(
            'input[name="customerSelection"]:checked'
          ).val(),
        };
        if (
          validateMessageData($("#messageDescription").val()) &&
          validateDayFrameData(smsTypeDayFrameDay)
        ) {
          eb.ajax({
            type: "POST",
            url: BASE_URL + "/sms-notification-api/updateSmsType",
            data: formData,
            success: function (respond) {
              p_notification(respond.status, respond.msg);
              if (respond.status) {
                window.setTimeout(function () {
                  location.reload();
                }, 1500);
              }
            },
          });
        }
    });
    
  $(".smsSendType", ".due-invoice-div").change(function () {
    if (parseInt(this.value) == 2) {
      $(".automatic-info").addClass("hidden");
      $(".manual-info").removeClass("hidden");
    } else {
      $(".automatic-info").removeClass("hidden");
      $(".manual-info").addClass("hidden");
    }
  });

  $("#retrieveCustomers", ".due-invoice-manual-div").on("click",function () {
    if ($("#startDate").val() != "") {
        if ($("#endDate").val() != "") {
          var data = {
            startDate: $("#startDate").val(),
            endDate: $("#endDate").val(),
          };
          eb.ajax({
            type: "POST",
            url: BASE_URL + "/invoice-api/getCustomersForDueInvoiceCustomers",
            data: data,
            success: function (respond) {
              if (respond.data) {
                customerList = respond.data;

                $("#promoCustomerTable").removeClass("hidden");
                $("#promoCustomerTable").find(".promoCustomers").remove();
                 for (var i in respond.data) {
                   var $customerSample = $(
                     "#customerSampleRow",
                     "#promoCustomerTable"
                   ).clone();
                   $customerSample
                     .find(".customerCode")
                     .html(respond.data[i].customerCode);
                   $customerSample
                     .find(".customerName")
                     .html(respond.data[i].customerName);
                   $customerSample
                     .find(".customerPhoneNumber")
                     .html(respond.data[i].customerTelephoneNumber);
                   $customerSample
                     .find(".dueAmount")
                     .html(respond.data[i].dueBalance);
                   $customerSample
                     .find(".noOfInvoices")
                     .html(respond.data[i].invoiceCount);
                   $customerSample.find("input[name='customer[]']").val(i);
                   $customerSample.attr("id", "");
                   $customerSample.addClass("promoCustomers");
                   $customerSample.removeClass("hidden");
                   $customerSample.removeClass("hidden");
                   $customerSample.insertBefore(
                     $("#customerSampleRow", "#promoCustomerTable")
                   );
                 }
              } else {
                $("#promoCustomerTable").addClass("hidden");
                $("#promoCustomerTable").find(".promoCustomers").remove();
                p_notification(
                  "info",
                  eb.getMessage("ERR_PROMO_EMAIL_CUST_LIST")
                );
              }
            },
          });
        } else {
          p_notification(false, eb.getMessage("ERR_END_DATE_EMPTY"));
        }  
     } else {
       p_notification(false, eb.getMessage("ERR_START_DAY_EMPTY"));
     }
  });
    

  $("#retrieveCustomers", ".service-reminder-manual-div").on(
    "click",
    function () {
      if ($("#startDate").val() != "") {
        if ($("#endDate").val() != "") {
          var data = {
            startDate: $("#startDate").val(),
            endDate: $("#endDate").val(),
          };
          eb.ajax({
            type: "POST",
            url: BASE_URL + "/invoice-api/getCustomersLastInvoice",
            data: data,
            success: function (respond) {
              if (respond.data) {
                customerList = respond.data;
                // $(".automatic-info").addClass("hidden");
                // $(".manual-info").removeClass("hidden");
                $("#promoCustomerTable").removeClass("hidden");
                $("#promoCustomerTable").find(".promoCustomers").remove();
                for (var i in respond.data) {
                  var $customerSample = $(
                    "#customerSampleRow",
                    "#promoCustomerTable"
                  ).clone();
                  $customerSample
                    .find(".customerCode")
                    .html(respond.data[i].customerCode);
                  $customerSample
                    .find(".customerName")
                    .html(respond.data[i].customerName);
                  $customerSample
                    .find(".customerPhoneNumber")
                    .html(respond.data[i].customerTelephoneNumber);
                  $customerSample
                    .find(".dueAmount")
                    .html(respond.data[i].lastInvoiceDate);
                  $customerSample
                    .find(".noOfInvoices")
                    .html(respond.data[i].salesinvoiceTotalAmount);
                  $customerSample.find("input[name='customer[]']").val(i);
                  $customerSample.attr("id", "");
                  $customerSample.addClass("promoCustomers");
                  $customerSample.removeClass("hidden");
                  $customerSample.removeClass("hidden");
                  $customerSample.insertBefore(
                    $("#customerSampleRow", "#promoCustomerTable")
                  );
                }
              } else {
                $("#promoCustomerTable").addClass("hidden");
                $("#promoCustomerTable").find(".promoCustomers").remove();
                p_notification(
                  "info",
                  eb.getMessage("ERR_PROMO_EMAIL_CUST_LIST")
                );
              }
            },
          });
        } else {
          p_notification(false, eb.getMessage("ERR_END_DATE_EMPTY"));
        }
      } else {
        p_notification(false, eb.getMessage("ERR_START_DAY_EMPTY"));
      }
    }
  );
    
  $("#retrieveCustomers", ".postdated-cheque-manual-div").on(
    "click",
    function () {
      if ($("#startDate").val() != "") {
        if ($("#endDate").val() != "") {
          var data = {
            startDate: $("#startDate").val(),
            endDate: $("#endDate").val(),
          };
          eb.ajax({
            type: "POST",
            url: BASE_URL + "/sms-notification-api/getPostDatedCheque",
            data: data,
            success: function (respond) {
              if (respond.data) {
                customerList = respond.data;
                // $(".automatic-info").addClass("hidden");
                // $(".manual-info").removeClass("hidden");
                $("#promoCustomerTable").removeClass("hidden");
                $("#promoCustomerTable").find(".promoCustomers").remove();
                for (var i in respond.data) {
                  var $customerSample = $(
                    "#customerSampleRow",
                    "#promoCustomerTable"
                  ).clone();
                  $customerSample
                    .find(".customerCode")
                    .html(respond.data[i].customerCode);
                  $customerSample
                    .find(".customerName")
                    .html(respond.data[i].customerName);
                  $customerSample
                    .find(".customerPhoneNumber")
                    .html(respond.data[i].customerTelephoneNumber);
                  $customerSample
                    .find(".dueAmount")
                    .html(respond.data[i].postdatedChequeDate);
                  $customerSample
                    .find(".noOfInvoices")
                    .html(respond.data[i].incomingPaymentPaidAmount);
                  $customerSample.find("input[name='customer[]']").val(i);
                  $customerSample.attr("id", "");
                  $customerSample.addClass("promoCustomers");
                  $customerSample.removeClass("hidden");
                  $customerSample.removeClass("hidden");
                  $customerSample.insertBefore(
                    $("#customerSampleRow", "#promoCustomerTable")
                  );
                }
              } else {
                $("#promoCustomerTable").addClass("hidden");
                $("#promoCustomerTable").find(".promoCustomers").remove();
                p_notification(
                  "info",
                  eb.getMessage("ERR_PROMO_EMAIL_CUST_LIST")
                );
              }
            },
          });
        } else {
          p_notification(false, eb.getMessage("ERR_END_DATE_EMPTY"));
        }
      } else {
        p_notification(false, eb.getMessage("ERR_START_DAY_EMPTY"));
      }
    }
  );
    
  
});
