var uploadedAttachments = {};
var deletedAttachmentIds = [];
var deletedAttachmentIdArray = [];

$(document).ready(function() {

    $('#viewUploadedFiles').on('click', function(e) {
        $('#doc-attach-table tbody tr').remove();
        $('#doc-attach-table tfoot div').remove();
        $('#doc-attach-table tbody div').remove();

        if (!$.isEmptyObject(uploadedAttachments)) {
            $('#doc-attach-table thead tr').removeClass('hidden');
            $.each(uploadedAttachments, function(index, value) {
                if (!deletedAttachmentIds.includes(value['documentAttachemntMapID'])) {
                    tableBody = "<tr id="+value['documentAttachemntMapID']+" data-attachId=" + value['documentAttachemntMapID'] + "><td><a href="+value.docLink+">"+value.documentRealName+"</a></td><td class='text-center'><span class='glyphicon glyphicon-trash delete_attachment' style='cursor:pointer'></span></td></tr>";
                    $('#doc-attach-table tbody').append(tableBody);
                }
            });
        } else {
            $('#doc-attach-table thead tr').addClass('hidden');
            var noDataFooter = "<div class= ' col-lg-6 text-right'><label><b>No Data</b></label>";
            $('#doc-attach-table tbody').append(noDataFooter);
        }
        $('#attach_view_footer').removeClass('hidden');
        $('#viewAttachmentModal').modal('show');
    });

    $('#doc-attach-table').on('click', '.delete_attachment', function() {
        var attachementID = $(this).parents('tr').attr('data-attachid');
        bootbox.confirm('Are you sure you want to remove this attachemnet?', function(result) {
            if (result == true) {
                $('#' + attachementID).remove();
                deletedAttachmentIdArray.push(attachementID);
            }
        });
    });

    $('#viewAttachmentModal').on('click', '#saveAttachmentEdit', function(event) {
        event.preventDefault();

        $.each(deletedAttachmentIdArray, function(index, val) {
            deletedAttachmentIds.push(val);
        });

        $('#viewAttachmentModal').modal('hide');
    });

    var dimensionId = null;
    var editFlag = false;
 
    $('#btnAdd').on('click',function(){
        var addiName = $('#addiName').val();

        var formData = {
            addiName: addiName
        }
    
        if(validateFormData(formData)){
            eb.ajax({
                type: 'POST',
                url: BASE_URL + '/companyAPI/saveAdditionalDetail',
                data: {
                    addiName : addiName
                },
                success: function(respond) {
                    p_notification(respond.status, respond.msg);                
                    if(respond.status){
                        var fileInput = document.getElementById('documentFiles');
                        if(fileInput.files.length > 0) {
                            var form_data = false;
                            if (window.FormData) {
                                form_data = new FormData();
                            }
                            form_data.append("documentID", respond.data);
                            form_data.append("documentTypeID", 37);
                            
                            for (var i = 0; i < fileInput.files.length; i++) {
                                form_data.append("files[]", fileInput.files[i]);
                            }

                            eb.ajax({
                                url: BASE_URL + '/store-files',
                                type: 'POST',
                                processData: false,
                                contentType: false,
                                data: form_data,
                                success: function(res) {
                                }
                            });
                        }
                        window.setTimeout(function() {
                                location.reload();
                        }, 1000);                    
                    }                
                }
            });
        } 
    });

    //for cancel btn
    $('#btnCancel, #btnViewCancel').on('click',function (){
        $('input[type=text]').val('');  
        $('.updatePoDiv_view').addClass('hidden');
        dimensionCreateView();        
    });

    $('#dimension-list').on('click','.dimension-action',function (e){    
        var action = $(this).data('action');
        dimensionId   = $(this).closest('tr').data('dimension-id');
        var addiName = $(this).closest('tr').data('dimension-name');

        switch(action) {
            case 'edit':
                editFlag = true;
                e.preventDefault();
                dimensionUpdateView();
                $('#addiName').val(addiName);
                $('.createPoDiv').addClass('hidden');
                $('.updatePoDiv').removeClass('hidden');
                $('.updatePoDiv_view').removeClass('hidden');
                getUploadedAttachemnts(dimensionId);
                break;
            case 'delete':
                deleteDimension(dimensionId);
                break;
            default:
                console.error('Invalid action.');
                break;
        }
    });

    function getUploadedAttachemnts(id) {
        var participantTypeIds = [];            
        $.ajax({
            type: 'GET',
            data: {
                addID: id
            },
            url: BASE_URL + '/companyAPI/getUploadedAttachemntsOfAdditionalDetail',
            success: function(respond) {
                if (!$.isEmptyObject(respond.data)) {
                    uploadedAttachments = respond.data;
                }

            }
        });
    }

    //this function for switch to rate card register view
    function dimensionCreateView(){
        $('.updateTitle').addClass('hidden');
        $('.addTitle').removeClass('hidden');
        $('.updateDiv').addClass('hidden');
        $('.addDiv').removeClass('hidden');
        $('.viewTitle').addClass('hidden');
        $('.viewDiv').addClass('hidden');
        $('#addiName').attr('disabled', false);
    }

    //this function for switch to rate card update view
    function dimensionUpdateView(){
        $('.addTitle').addClass('hidden');
        $('.updateTitle').removeClass('hidden');
        $('.addDiv').addClass('hidden');
        $('.viewTitle').addClass('hidden');
        $('.viewDiv').addClass('hidden');
        $('.updateDiv').removeClass('hidden');
        $('#addiName').attr('disabled', false);
    }

    //this function for switch to rate card update view
    function dimensionView(){
       $('.addTitle').addClass('hidden');
       $('.updateTitle').addClass('hidden');
       $('.viewTitle').removeClass('hidden');
       $('.addDiv').addClass('hidden');
       $('.updateDiv').addClass('hidden');
       $('.viewDiv').removeClass('hidden');
    }

    $('#btnUpdate').on('click',function(){
        var addiName = $('#addiName').val();

        var formData = {
            addiName: addiName
        }

        var existingAttachemnts = {};
        var deletedAttachments = {};
        $.each(uploadedAttachments, function(index, val) {
            if (!deletedAttachmentIds.includes(val['documentAttachemntMapID'])) {
                existingAttachemnts[index] = val;
            } else {
                deletedAttachments[index] = val;
            }
        });

        if(validateFormData(formData)){
            eb.ajax({
                type: 'POST',
                url: BASE_URL + '/companyAPI/updateAdditionalDetails',
                data: {
                    addiId: dimensionId,
                    addiName : addiName
                },
                success: function(respond) {
                    p_notification(respond.status, respond.msg);                
                    if(respond.status){
                        var fileInput = document.getElementById('editDocumentFiles');
                        var form_data = false;
                        if (window.FormData) {
                            form_data = new FormData();
                        }
                        form_data.append("documentID", respond.data);
                        form_data.append("documentTypeID", 37);
                        form_data.append("updateFlag", true);
                        form_data.append("editedDocumentID", dimensionId);
                        form_data.append("documentCode", addiName);
                        form_data.append("deletedAttachmentIds", deletedAttachmentIds);
                        
                        if(fileInput.files.length > 0) {
                            for (var i = 0; i < fileInput.files.length; i++) {
                                form_data.append("files[]", fileInput.files[i]);
                            }
                        }

                        eb.ajax({
                            url: BASE_URL + '/store-files',
                            type: 'POST',
                            processData: false,
                            contentType: false,
                            data: form_data,
                            success: function(res) {
                            }
                        });

                        window.setTimeout(function() {
                                location.reload();
                        }, 1000);                    
                    }                
                }
            });
        } 
    });

    //for search
    $('#searchDimension').on('click', function() {
        var key = $('#dimensionSearch').val();

        searchDimension(key);
    });


    function searchDimension(key) {  
        $.ajax({
            type: 'POST',
            url: BASE_URL + '/companyAPI/searchAdditionalDetails',
            data: {
                searchKey : key
            },
            success: function(data) {
                if (data.status == true) {
                    if (data.status) {
                        $("#dimension-list").html(data.html);
                    } else {
                        p_notification(data.status, data.msg);
                    }
                }            
            }
        });  
    }

    //for rest btn
    $('#btnReset').on('click',function (){
        $('input[type=text]').val('');
        document.getElementById("documentFiles").value = "";
    });  

    //for search cancel btn
    $('#searchCancel').on('click',function (){
        $('#dimensionSearch').val('');
        searchDimension();
    });

    
    function deleteDimension(addiID) {
        bootbox.confirm('Are you sure you want to delete this additional detail?', function($result) {
            if ($result == true) {
                eb.ajax({
                    url: BASE_URL + '/companyAPI/deleteAdditionDetail',
                    method: 'post',
                    data: {
                        addiID: addiID
                    },
                    dataType: 'json',
                    success: function(data) {
                        if (data.status == true) {
                            p_notification(data.status, data.msg);
                            window.location.reload();
                        } else {
                            p_notification(data.status, data.msg);
                        }
                    }
                });
            }
        });
    }

    function validateFormData(formData) {
        if (formData.addiName == "") {
            p_notification(false, eb.getMessage('ERR_ADDI_NAME_EMPTY'));
            $('#addiName').focus();
            return false;
        }
        return true;
    }

    $('#dimension-list').on('click', '.doc_attachments', function() {
        setDataToAttachmentViewModal($(this).attr('data-par-related-id'));
        $('#viewAttachmentModal').modal('show');
    });

    function setDataToAttachmentViewModal(documentID) {
        $('#doc-attach-table tbody tr').remove();
        $('#doc-attach-table tfoot div').remove();
        $('#doc-attach-table tbody div').remove();

        eb.ajax({
            type: 'POST',
            url: BASE_URL + '/get-document-related-attachement',
            data: {
                documentID: documentID,
                documentTypeID: 37
            },
            success: function(respond) {
                if (respond.status == true) {
                    $('#doc-attach-table thead tr').removeClass('hidden');
                    $.each(respond.data, function(index, value) {
                        tableBody = "<tr><td><a href="+value.link+">"+value.docName+"</a></td></tr>";
                        $('#doc-attach-table tbody').append(tableBody);
                    });
                } else {
                    $('#doc-attach-table thead tr').addClass('hidden');
                    var noDataFooter = "<div class= ' col-lg-6 text-right'><label><b>No Data</b></label>";
                    $('#doc-attach-table tbody').append(noDataFooter);
                }
            }
        });
    }
});
