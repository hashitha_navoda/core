$(document).ready(function() {
    var idInc = 1;
    var selectedItemID;
    var uomIncID = 1;
    var bom_ID = 'bom_item_1';
    var totalMoneyValue = 0;
    var selectedItemMoneyValue = 0;
    var itemList = getAddedItemIdList();
    var documentType = 'bomDocument';
    var totalCostInEdit = parseFloat($('#total-val').attr('data-costVal'));
    $('#bom_sub_item_body').on('click', '#addSubItem,#remove_bom_sub_item,#edit_bom_sub_item,#save_bom_sub_item', function() {
        var defaultData = new Array();
        defaultData = {
            'itemID': $(this).parents('tr').find('#' + bom_ID).val(),
            'itemquntity': $(this).parents('tr').find('#bom_sub_item_quo').val(),
            'itemUnitPrice': $(this).parents('tr').find('#bom_sub_item_unitPrice').val(),
            'edit': $(this).parents('tr').find('#addSubItem').hasClass('edit'),
        };
        $(this).parents('tr').find('#bom_sub_item_Total').html(accounting.formatMoney(setTotalSubItemVaue(defaultData)));

        if(selectedItemID){
            $(this).parents('tr').find('.bom_item').attr('disabled', true);            
        }

        if ($(this).hasClass('remove_bom_record')) {
            totalMoneyValue = parseFloat(totalMoneyValue) - parseFloat(setTotalSubItemVaue(defaultData));
            $(this).parents('tr').remove();
            var itemId = $(this).parents('tr').find('.bom_item').val();
            itemList.splice(itemList.indexOf(itemId),1);
        } else if($(this).hasClass('edit_bom_record')){
            selectedItemMoneyValue = setTotalSubItemVaue(defaultData);
            totalMoneyValue = parseFloat(totalMoneyValue) - selectedItemMoneyValue;
            $(this).parents('tr').find('.edit_button,.delete_button').addClass('hidden');
            $(this).parents('tr').find('.save_button').removeClass('hidden');
            $(this).parents('tr').find('.uomqty,.uomPrice').prop("disabled", false);
            //save_bom_sub_item
        } else if($(this).hasClass('save_bom_record')){
            totalMoneyValue = parseFloat(totalMoneyValue) + (setTotalSubItemVaue(defaultData));
            $(this).parents('tr').find('.edit_button,.delete_button').removeClass('hidden');
            $(this).parents('tr').find('.save_button').addClass('hidden');
            $(this).parents('tr').find('.uomqty,.uomPrice').prop("disabled", true);
        } else {
            //if item not selected
            if(!selectedItemID){
                p_notification( false, eb.getMessage('ERROR_BOM_SUB_ITEM_NULL'));
                return false;
            }            
            //check whether item already exist
            if(itemList.indexOf(selectedItemID) == -1){                
                if (checkNameAndQuntity(defaultData)) {
                    idInc++;
                    var cloneId = 'bom_sub_item_' + idInc;
                    $('#sample_data_set').clone().appendTo('#bom_sub_item_body').attr('id', cloneId);
                    $('#' + cloneId).removeClass('hidden');
                    $(this).parents('tr').find('.add_button').addClass('hidden');
                    $(this).parents('tr').find('.edit_button').removeClass('hidden');
                    $(this).parents('tr').find('.delete_button').removeClass('hidden');
                    $(this).parents('tr').find('.uomqty,.uomPrice').prop("disabled", true);
                    bom_ID = 'bom_item_' + idInc;
                    $('#' + cloneId).children('td').find('.bom_item').attr('id', bom_ID);
                    $('#' + cloneId).children('td').find('.bom_item').attr('name', bom_ID);
                    $('#' + cloneId).children('td').find('.bom_item').attr('data-id', 'abc');
                    loadDropDownFromDatabase('/productAPI/search-location-products-for-dropdown', '', '', '#' + bom_ID, '', '', documentType);
                    $('#' + bom_ID).selectpicker('refresh');
                    $('#' + bom_ID).on('change', function() {
                        if ($(this).val() > 0 && $(this).val() != selectedItemID) {
                            selectedItemID = $(this).val();
                            itemChange($(this).val(), $(this).parents('tr').attr('id'));
                        }
                    });
                    totalMoneyValue = parseFloat(totalMoneyValue) + (setTotalSubItemVaue(defaultData));
                    itemList.push(selectedItemID);                    
                }                
            } else {
                $(this).parents('tr').find('.bom_item').attr('disabled', false);
                p_notification(false, eb.getMessage('ERROR_BOM_INSERT_SUB_ITEM'));
                return false;
            }   
        }
        if ($(this).hasClass('edit_bom_new_sub')) {
            if(itemList.indexOf(selectedItemID) == -1){
                $(this).parents('tr').find('.sub-item-unit-div-orig').addClass('hidden');
                $(this).parents('tr').find('.sub-item-unit-div-fake').removeClass('hidden');
                $(this).parents('tr').find('.sub-item-unit-fake').val($(this).parents('tr').find('.uomPrice').val());
                $(this).parents('tr').find('.sub-item-quo-div-orig').addClass('hidden');
                $(this).parents('tr').find('.sub-item-quo-div-fake').removeClass('hidden');
                $(this).parents('tr').find('.sub-item-quo-fake').val($(this).parents('tr').find('.uomqty').val());
                $(this).parents('tr').find('.edit-bom-new-item, #bom_sub_item_quo, #bom_sub_item_unitPrice').attr('disabled', true);
                
            } else {
                totalCostInEdit += parseFloat(setTotalSubItemVaue(defaultData));
                $('#total-val').html("<h4>" + accounting.formatMoney(totalCostInEdit) + "</h4>");
            }
        } else {           
            $('#total-val').html("<h4>" + accounting.formatMoney(totalMoneyValue) + "</h4>");
        }
        
        if(itemList.indexOf(selectedItemID) != -1){
            selectedItemID = null;
        }
    });

    $('#save_bom').on('click', function() {
        saveBom(totalMoneyValue);      
    });
    
    $('#update_bom').on('click', function() {
        update(totalMoneyValue);
    });
    
    $('.uom-list').on('click', '.delete', function() {
        $(this).parents('tr').remove();
    });
    
    $(document).on('change', '.bom_item', function() {
        $(this).parents('tr').find('.uomqty').remove();
        $(this).parents('tr').find('.uomPrice').remove();
    });

    $('#bom_sub_item_table').on('click', '.edit-exits-sub-item', function() {
        var itemWieTotal = $(this).parents('tr').find('#bom_sub_item_Total').html();
        $(this).parents('tr').find('.sub-item-quo').attr('disabled', false);
        $(this).parents('tr').find('.sub-item-price').attr('disabled', false);
        totalCostInEdit = totalCostInEdit - parseFloat(removeCommaSeparater(itemWieTotal.replace(/\,/g, "")));
        $('#total-val').html(accounting.formatMoney(totalCostInEdit));
        $(this).parents('tr').find('.edit_button').addClass('hidden');
        $(this).parents('tr').find('.delete_button').addClass('hidden');
        $(this).parents('tr').find('.add_button').removeClass('hidden');
        $(this).parents('tr').removeClass('added');
    });
    $('.add-exits-sub-item').on('click', function() {
        
        var itemQnty = $(this).parents('tr').find('.sub-item-quo').val();
        var itemUntPrc = $(this).parents('tr').find('.sub-item-price').val();
        itemQnty.replace(/\,/g, "");
        itemUntPrc.replace(/\,/g, "");
        var exitDataArray = {
            'itemUnitPrice': removeCommaSeparater(itemUntPrc),
            'itemquntity': removeCommaSeparater(itemQnty)

        };
        if(validateEditBOMData(exitDataArray)){
            
            $(this).parents('tr').find('.sub-item-name, .sub-item-quo, .sub-item-price').attr('disabled', true);
            $(this).parents('tr').find('.add_button').addClass('hidden');
            $(this).parents('tr').find('.edit_button').removeClass('hidden');
            $(this).parents('tr').find('.delete_button').removeClass('hidden');
            $(this).parents('tr').addClass('added');
        
            $(this).parents('tr').find('#bom_sub_item_Total').html(accounting.formatMoney(setTotalSubItemVaue(exitDataArray)));
            totalCostInEdit = (setTotalSubItemVaue(exitDataArray)) + parseFloat(totalCostInEdit);
            $('#total-val').html(accounting.formatMoney(totalCostInEdit));  
        }

        
    });

    $('#bom_sub_item_body').on('click', '.remove-exits-sub-item', function() {
        $(this).parents('tr').remove();
        var $tr = $(this).parents('tr');
        var itemId = null;
        if($tr.hasClass('edit-bom added')){
            itemId = $tr.find('.sub-item-name').data('proid');
        } else {
            itemId = $tr.find('#bom_item_1').val();
        }        
        if(itemId){
            itemList.splice(itemList.indexOf(itemId.toString()),1);
            var itemWieTotal = $(this).parents('tr').find('#bom_sub_item_Total').html();
            totalCostInEdit = totalCostInEdit - parseFloat(removeCommaSeparater(itemWieTotal.replace(/\,/g, "")));
            $('#total-val').html(accounting.formatMoney(totalCostInEdit));
        }
    });
    
    loadDropDownFromDatabase('/productAPI/search-location-products-for-dropdown', '', '', '#bom_item_1', '', '', documentType);
    $('#bom_item_1').on('change', function() {
        if ($(this).val() > 0 && $(this).val() != selectedItemID) {
            selectedItemID = $(this).val();
            itemChange($(this).val(), $(this).parents('tr').attr('id'));
        }
    });

    function saveBom(totalMoneyValue) {
        var bomDataArray = new Array();
        var submitData = new Array();
        var bomAutoInc = 0;

        var jeState = ($('#jeState').is(":checked")) ? 1 : 0;
        $('#bom_sub_item_body tr').each(function() {
            if ($(this).hasClass('hidden') || $(this).find('.delete_button').hasClass('hidden')) {
                //do nothing
            } else {
                bomDataArray[bomAutoInc] = {
                    'itemID': $(this).find('.bom_item').val(),
                    'quontity': $(this).find('#bom_sub_item_quo').val(),
                    'unitPrice': $(this).find('#bom_sub_item_unitPrice').val(),
                    'totatPrice': accounting.unformat($(this).children().find('#bom_sub_item_Total').text())
                }
                bomAutoInc++;
            }
        });

        var locations = $("input.locationCheck:checked").map(function() {
            return $(this).data('locationid');
        }).get();
        submitData = {
            'bomParentName': $('#BOMName').val(),
            'bomReference': $('#BomRef').val(),
            'bomCategory': $('select[name=categoryParentID]').val(),
            'discription': $('#productDescription').val(),
            'subItemDetails': bomDataArray,
            'totalValue': totalMoneyValue,
            'productGlobalProduct': $("[name='productGlobalProduct']:checked").val(),
            'locations': locations,
            'uomDetails': saveUoms(),
            'jeState' : jeState
        };
        if (isValidBomData(submitData)) {
            eb.ajax({
                url: BASE_URL + '/bom-api/addBom',
                method: 'post',
                data: submitData,
                dataType: 'json',
                success: function(data) {
                    p_notification(data.status, data.msg);
                    if (data.status == true) {
                        itemList = [];
                        window.setTimeout(function() {
                            location.reload();
                        }, 1000);
                    }
                }
            });
        }
    }

    function update(totalMoneyValue) {
        var bomUpdateArray = new Array();
        var bomUpdateAutoInc = 0;
        var totalUpdatedValue = 0;
        $('#bom_sub_item_body tr').each(function() {
            if ($(this).hasClass('edit-bom') && $(this).hasClass('added')) {
                totalUpdatedValue = totalUpdatedValue + accounting.unformat($(this).children().find('#bom_sub_item_Total').text());
                bomUpdateArray[bomUpdateAutoInc] = {
                    'itemID': $(this).find('.sub-item-name').attr('data-proid'),
                    'quontity': (removeCommaSeparater($(this).find('.sub-item-quo').val())) * $(this).find('.sub-item-quo').attr('data-uomconval'),
                    'unitPrice': removeCommaSeparater($(this).find('.sub-item-price').val()) / $(this).find('.sub-item-quo').attr('data-uomconval'),
                    'totatPrice': accounting.unformat($(this).children().find('#bom_sub_item_Total').text())
                }
                bomUpdateAutoInc++;
            }
            else if ($(this).hasClass('hidden') || $(this).find('.delete_button').hasClass('hidden')) {
                //do nothing
            } else {
                totalUpdatedValue = totalUpdatedValue + accounting.unformat($(this).children().find('#bom_sub_item_Total').text());
                bomUpdateArray[bomUpdateAutoInc] = {
                    'itemID': $(this).find('.bom_item').val(),
                    'quontity': $(this).find('#bom_sub_item_quo').val(),
                    'unitPrice': $(this).find('#bom_sub_item_unitPrice').val(),
                    'totatPrice': accounting.unformat($(this).children().find('#bom_sub_item_Total').text())
                }
                bomUpdateAutoInc++;
            }
        });

        var locations = $("input.locationCheck:checked").map(function() {
            return $(this).data('locationid');
        }).get();
        var updatedData = {
            'bomParentName': $('#BOMName').val(),
            'bomReference': $('#BomRef').val(),
            'bomCategory': $('select[name=categoryParentID]').val(),
            'discription': $('#productDescription').val(),
            'subItemDetails': bomUpdateArray,
            'totalValue': totalUpdatedValue,
            'productGlobalProduct': $("[name='productGlobalProduct']:checked").val(),
            'locations': locations,
            'bomID': $('.bom-ref').attr('data-bomid'),
            'productID': $('.bom-ref').attr('data-proid'),
            'uomDetails': true
        };
        if (isValidBomData(updatedData)) {
            eb.ajax({
                url: BASE_URL + '/bom-api/updateBom',
                method: 'post',
                data: updatedData,
                dataType: 'json',
                success: function(data) {
                    if (data.status == true) {
                        p_notification(data.status, data.msg);
                        window.setTimeout(function() {
                            location.reload();
                        }, 1000);
                    }
                }
            });
        }

    }

    $("[name='productGlobalProduct']").on('change', function() {
        if ($("#locationItem").prop('checked')) {
            $('#itemLocations').show();
        } else {
            $('#itemLocations').hide();
        }
    }).trigger('change');

    $('.add-uom').on('click', function() {
        $('.uom-list .sample').clone().appendTo('.uom-list tbody').attr('id', 'uom_type_' + uomIncID);
        $('#uom_type_' + uomIncID).removeClass('sample');
        uomIncID++;
    });

    $("table.uom-list tbody").on('change', "input[name='productBaseUom']", function() {
        $('table.uom-list tbody tr').each(function() {
            $(this).removeClass('base');
            $(this).children().find("input[name='productUomConversion[]']").removeAttr('readonly');
            $(this).children().find("input[name='productUomConversion[]']").val('');
        });
        $(this).parents('tr').addClass('base');
        $(this).parents('tr').find("input[name='productUomConversion[]']").val(1);
        $(this).parents('tr').find("input[name='productUomConversion[]']").attr('readonly', true);

    });


    $('#back-bom-list').on('click', function() {
        window.history.back();
    });

    $('#search-bom').on('click', function(e) {
        e.preventDefault();
        var searchKey = $('#productSearch').val();
        var formData = {
            searchKey: searchKey,
        }
        if (formData.searchKey) {
            searchValue(formData);
        } else {
            p_notification(false, eb.getMessage('ERR_NO_KEY_WORD'));
        }
    });

    $('#reset-bom-search').on('click', function() {
        searchValue();
    });

    function searchValue(formData) {
        eb.ajax({
            url: BASE_URL + '/bom-api/getBomBySearchKey',
            method: 'post',
            data: formData,
            dataType: 'json',
            success: function(data) {
                if (data.status == true) {
                    if (data.status) {
                        $("#bom-list-data").html(data.html);
                    } else {
                        p_notification(data.status, data.msg);
                    }
                }
            }
        });
    }

    $('#bom-ad-search').on('click', function(e) {
        e.preventDefault();
        var searchKey = $('#bomAdSearch').val();
        var formData = {
            searchKey: searchKey,
        }
        if (formData.searchKey) {
            searchBomADValue(formData);
        } else {
            p_notification(false, eb.getMessage('ERR_NO_KEY_WORD'));
        }
    });

    $('#bom-ad-reset').on('click', function() {
        searchBomADValue();
    });

    function searchBomADValue(formData) {
        eb.ajax({
            url: BASE_URL + '/bom-api/getBomAdBySearchKey',
            method: 'post',
            data: formData,
            dataType: 'json',
            success: function(data) {
                if (data.status == true) {
                    if (data.status) {
                        $("#ad-list-data").html(data.html);
                    } else {
                        p_notification(data.status, data.msg);
                    }
                }
            }
        });
    }



});


function saveUoms() {
    var uomIncrement = 0;
    var flag = false;
    var tempUomArray = new Array();
    $('.uom-list tbody tr:not(.sample)').each(function() {
        var uomID = $("[name='uomID[]']", $(this)).val();
        var convertionFac = $("[name='productUomConversion[]']", $(this)).val();
        var base = $("[name='productBaseUom']:checked", $(this)).val();
        var display = $("[name='productDisplayUom']:checked", $(this)).val();
        if (valueExistInArray(tempUomArray, convertionFac, 'coRate')) {
            p_notification(false, eb.getMessage('ERRO_SAME_CONVERSION_VAlUE'));
            flag = false;
            return false;
        }
        else if (valueExistInArray(tempUomArray, uomID, 'uomID')) {
            p_notification(false, eb.getMessage('ERRO_SAME_UOM'));
            flag = false;
            return false;
        }
        else if (convertionFac == null || convertionFac == '') {
            p_notification(false, eb.getMessage('ERRO_CONVERTION_FACT_NULL'));
            flag = false;
            return false;
        } else if (uomID == '' || uomID == null) {
            p_notification(false, eb.getMessage('ERRO_NO_UOM'));
            flag = false;
            return false;
        }
        flag = true;
        tempUomArray[uomIncrement] = {
            'uomID': uomID,
            'coRate': convertionFac,
            'base': base,
            'display': display
        };
        uomIncrement++;
    });
    function valueExistInArray(array, id, value)
    {
        return array.some(function(item) {
            return item[value] === id;
        });
    }
    if (flag) {
        return tempUomArray;
    } else {
        return false;
    }
}


function isValidBomData(data) {
    if (data.bomReference == '' || data.bomReference == 'undefined') {
        p_notification(false, eb.getMessage('ERROR_BOM_REF_EMPTY'));
        return false;
    } else if (data.bomParentName == '' || data.bomParentName == 'undefined') {
        p_notification(false, eb.getMessage('ERROR_PARENT_NAME'));
        return false;
    } else if (data.bomCategory == '' || data.bomCategory == 'undefined') {
        p_notification(false, eb.getMessage('ERROR_BOM_CATEGORY'));
        return false;
    } else if (!data.uomDetails) {
        return false;
    } else if (data.subItemDetails.length == 0) {
        p_notification(false, eb.getMessage('ERROR_BOM_SUB_ITEMS_NOT_EXIST'));
        return false;
    } else if ($.isEmptyObject(data.locations) && data.productGlobalProduct == "0") {
        p_notification(false, eb.getMessage('ERROR_BOM_LOCATION_NOT_SELECT'));
        return false;
    }
    return true;

}
function checkNameAndQuntity(data) {
    if (data.itemID == '' || data.itemID == 'undefine') {
        p_notification(false, eb.getMessage('ERROR_BOM_SUB_ITEM_NULL'));
        return false;
    } else if (data.itemquntity == '') {
        p_notification(false, eb.getMessage('ERROR_BOM_SUB_ITEM_QUOT_NULL'));
        return false;
    } else if (isNaN(data.itemquntity)) {
        p_notification(false, eb.getMessage('ERROR_QUONTITY_DATA_TYPE'));
        return false;
    } else if(data.itemquntity <= 0) {
        p_notification( false, eb.getMessage('ERROR_BOM_SUB_ITEM_NULL_QTY'));
        return false;
    } else if (data.itemUnitPrice < 0) {
        p_notification( false, eb.getMessage('ERROR_BOM_SUB_ITEM_UNIT_PRICE_NULL'));
        return false;    
    } else if (data.edit) {
        checkAddToTheList();
        return true;
    }
    return true;

}
function validateEditBOMData(data){
    if(data.itemUnitPrice < 0 ) {
        p_notification( false, eb.getMessage('ERROR_BOM_SUB_ITEM_UNIT_PRICE_NULL'));
        return false;
    } else if (data.itemquntity <= 0) {
        p_notification( false, eb.getMessage('ERROR_BOM_SUB_ITEM_NULL_QTY'));
        return false;
    } else {
        return true;
    }

}

function setTotalSubItemVaue(data) {
    var totalValue = 0;
    if (data.itemUnitPrice) {
        totalValue = parseFloat(data.itemUnitPrice) * parseFloat(data.itemquntity);
        return totalValue;
    } else {
        var totalValue = 0;
        return totalValue;
    }
}

function deleteBom(proId) {
    var proID = {
        'productID': proId
    };
    eb.ajax({
        url: BASE_URL + '/bom-api/checkSelectedIdUsage',
        method: 'post',
        data: proID,
        dataType: 'json',
        success: function(data) {
            if (data.status == true) {
                bootbox.confirm('Are you sure you want to delete this Bom?', function(result) {
                    if (result === true) {
                        deleteProduct(proID);
                    }
                });
            } else {
                p_notification(data.status, data.msg);
                return false;
            }
        }
    });
}
function deleteProduct(proID) {
    eb.ajax({
        url: BASE_URL + '/productAPI/deleteProduct',
        method: 'post',
        data: proID,
        dataType: 'json',
        success: function(data) {
            if (data.status == true) {
                p_notification(true, eb.getMessage("SUCC_BOM_DELETE"));
                window.setTimeout(function() {
                    location.reload();
                }, 1000);
            }
        }
    });
}

function checkAddToTheList() {
    $('#bom_sub_item_body tr').each(function() {
        if ($(this).hasClass('edit-bom')) {
            if ($(this).hasClass('added')) {
                return true;
            } else {
                p_notification(false, eb.getMessage('ERROR_ADD_EXIT_ITEM'));
                return false;
            }
        }
    });


}


//this function return to the float value that given value with comma separetes...
function removeCommaSeparater(data) {
    var param1 = data;
    param1.replace(/\,/g, "");
    return parseFloat(param1.replace(/\,/g, ""));

}
//this function use to get data that related to the selected item.
function itemChange(itemkey, addRow) {
    var data = new Array();
    itemDecimalPoints = 3;
    var $addrow = addRow;
    if (itemkey == '') {
        $('#uom', $addrow).html('');
        return false;
    }

    eb.ajax({
        type: 'POST',
        url: BASE_URL + '/productAPI/get-location-product-details',
        data: {productID: itemkey, locationID: $('#itemLocations').attr('data-current-location-id')},
        success: function(respond) {
            data[0] = respond.data;
            if (data[0] == 'error') {
                $('#uom', $addrow).html('');
                dbflag = false;
            } else {
                dbflag = true;
                $('#' + $addrow).find('#bom_sub_item_unitPrice').val(data[0].dSP).addUomPrice(data[0].uom);
                $('#uom', $addrow).html(data[0].abbrevation);
                $('#' + $addrow).find('#bom_sub_item_quo').addUom(data[0].uom);
            }
        }
    });

}

function getAddedItemIdList(){
    var itemArray = [];    
    $('#bom_sub_item_body tr').each(function() {
        if (!($(this).hasClass('hidden') || $(this).find('.delete_button').hasClass('hidden'))) {
            var itemId = $(this).find('.sub-item-name').data('proid');
            if(itemId){
                itemArray.push(itemId.toString());
            }
            
        }
    });
    return itemArray;
}

