var gulp = require('gulp');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var imagemin = require('gulp-imagemin');
var sourcemaps = require('gulp-sourcemaps');
var minifyCss = require('gulp-minify-css');
var inject = require('gulp-inject');
var es = require('event-stream');
var clean = require('gulp-clean');
var gutil = require('gulp-util');

var paths = {
    scripts: [
        'public/assets/jquery/js/*.js',
        'public/assets/bootstrap/js/bootstrap.min.js',
        'public/js/bootstrap-select.js',
        'public/assets/bootstrap/js/bootstrap-multiselect.js',
        'public/assets/bootstrap/js/bootstrap-typeahead.js',
        'public/assets/bootstrap/js/ajax-bootstrap-select.min.js',
        'public/assets/bootstrap/js/bootstrap-datetimepicker.js',
        'public/js/bootstrap-datepicker.js',
        'public/assets/bootstrap-switch-3.0/build/js/bootstrap-switch.min.js',
        'public/js/bootbox.js',
        'public/js/morris.js',
        'public/js/apexcharts.min.js',
        'public/js/zingchart.min.js',
        'public/js/raphael-2.1.0.min.js',
        'public/js/underscore-min.js',
        'public/js/accounting.js',
        'public/js/layout.js',
        'public/js/select2.min.js',
        'public/js/notifications.js',
        'public/js/user/feedback.js',
        'public/assets/pnotify-1.2.0/ezBiz_pnotify.js',
        'public/assets/pnotify-1.2.0/jquery.pnotify.min.js',
        'public/js/uom.js',
        'public/js/uomPrice.js',
        'public/js/preview-document.js',
        'public/js/send_email.js',
        'public/js/common.js',
        'public/js/posposLogIn.js',
    ],
    css: [
        'public/css/bootstrap-select.css',
        'public/css/morris.css',
        'public/assets/bootstrap/css/bootstrap.min.css',
        'public/assets/bootstrap-switch-3.0/build/css/bootstrap3/bootstrap-switch.css',
        'public/css/datepicker.css',
        'public/css/select2.min.css',
        'public/css/font-awesome.min.css',
        'public/css/jquery.mCustomScrollbar.css',
        'public/css/style.css',
        'public/css/specific/layout.css',
        'public/assets/pnotify-1.2.0/jquery.pnotify.default.css',
        'public/assets/bootstrap/css/bootstrap-multiselect.css',
        'public/assets/bootstrap/css/bootstrap-datetimepicker.min.css',
    ],
    images: [
        'public/assets/img/*',
        'public/assets/bootstrap-modal/img/*'
    ],
    fonts: [
        'public/assets/bootstrap/fonts/*',
        'public/fonts/*'
    ],
};


// Not all tasks need to use streams
// A gulpfile is just another node program and you can use any package available on npm
gulp.task('clean', function() {
    return gulp.src(['public/build/js', 'public/build/css'], {read: false})
            .pipe(clean({force: true}));
});

    var scriptsTask = gulp.src(paths.scripts)
            .pipe(uglify({
                mangle: false,
            }).on('error', gutil.log))
            .pipe(concat('all.min-' + Date.now() + '.js'))
            .pipe(gulp.dest('public/build/js'));

    var minifyCssTask = gulp.src(paths.css)
            .pipe(minifyCss({compatibility: 'ie9'}))
            .pipe(concat('all.min-' + Date.now() + '.css'))
            .pipe(sourcemaps.write())
            .pipe(gulp.dest('public/build/css'));

gulp.task('build-html', ['clean'], function() {
    // Minify and copy all JavaScript (except vendor scripts)
    // with sourcemaps all the way down
    return gulp.src('./module/Core/view/layout/layout.phtml')
            .pipe(inject(es.merge(scriptsTask, minifyCssTask), {ignorePath: 'public'}))
            .pipe(gulp.dest('./module/Core/view/layout/'));
});

gulp.task('wizard-build-html', ['clean'], function() {
    // Minify and copy all JavaScript (except vendor scripts)
    // with sourcemaps all the way down
    return gulp.src('./module/Settings/view/settings/layout/wizard-layout.phtml')
            .pipe(inject(es.merge(scriptsTask, minifyCssTask), {ignorePath: 'public'}))
            .pipe(gulp.dest('./module/Settings/view/settings/layout/'));
});

// copy all fonts
gulp.task('copy-fonts', function() {
    return gulp.src(paths.fonts)
            .pipe(gulp.dest('public/build/fonts'));
});

// Copy all static images
gulp.task('images', function() {
    return gulp.src(paths.images)
            // Pass in options to the task
            .pipe(gulp.dest('public/build/img'));
});

// The default task (called when you run `gulp` from cli)
gulp.task('default', [
    'build-html',
    'wizard-build-html',
    'images',
    'copy-fonts'
]);

// Retrun the task when a file changes
gulp.task('watch', function() {
  gulp.watch([paths.scripts, paths.css], ['build-html']);
  gulp.watch(paths.images, ['images']);
  gulp.watch(paths.fonts, ['copy-fonts']);
});