<?php
namespace App;

use Ratchet\MessageComponentInterface;
use Ratchet\ConnectionInterface;


class WsHandler implements MessageComponentInterface {

	private $clients;

    public function __construct()
    {
        $this->clients = new \SplObjectStorage;
    }

    public function onOpen(ConnectionInterface $conn) {
        error_log('Hooooray....');
        $this->clients->attach($conn);
    }

    public function onMessage(ConnectionInterface $from, $msg) {
        error_log('Fking new msg....');

        foreach ($this->clients as $client) {
            if ($from !== $client) {
                $client->send($msg);
            }
        }
    }

    public function onClose(ConnectionInterface $conn) {
        error_log('A bitch has left.....');
        $this->clients->detach($conn);
    }

    public function onError(ConnectionInterface $conn, \Exception $e) {
        error_log('What the fuck is going wrong.....\n\n');
        error_log('An error has occurred: {$e->getMessage()}\n');

        $conn->close();
    }
}
