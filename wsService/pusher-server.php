<?php
    require 'vendor/autoload.php';

    $serverip = '127.0.0.1';
    $pusherport = '5555';
    $wsport = '0.0.0.0:8889';
    
    $loop   = React\EventLoop\Factory::create();

    $pusher = new App\Pusher;


    // Listen for the web server to make a ZeroMQ push after an ajax request
    $context = new React\ZMQ\Context($loop);
    
    $pull = $context->getSocket(ZMQ::SOCKET_PULL);
    
    $pull->bind('tcp://' . $serverip . ':' . $pusherport); // Binding to 127.0.0.1 means the only client that can connect is itself
    
    $pull->on('message', array($pusher, 'onInventoryUpdate'));

    // Set up our WebSocket server for clients wanting real-time updates
    $webSock = new React\Socket\Server($wsport, $loop); // Binding to 0.0.0.0 means remotes can connect

    echo "Listening on {$webSock->getAddress()}\n";
    
    $webServer = new Ratchet\Server\IoServer(
    
        new Ratchet\Http\HttpServer(
            new Ratchet\WebSocket\WsServer(new Ratchet\Wamp\WampServer($pusher))
        ),
        $webSock
    );

    $loop->run();
