#!/bin/bash
# install mysqldb
mysql_install_db

# start apache mongo and mysql
service httpd start
#remove socket file
rm /var/lib/mysql/mysql.sock
service mysqld start

#...and dnsmasq
service dnsmasq start
