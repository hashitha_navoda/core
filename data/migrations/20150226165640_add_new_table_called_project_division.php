<?php

use Phinx\Migration\AbstractMigration;

class AddNewTableCalledProjectDivision extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
      public function change()
      {
      }
     */

    /**
     * Migrate Up.
     */
    public function up()
    {
        $this->execute("CREATE TABLE IF NOT EXISTS `projectDivision` (
  `projectDivisionId` INT NOT NULL,
  `projectId` INT NULL,
  `divisionId` INT NULL,
  PRIMARY KEY (`projectDivisionId`));");
    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }

}
