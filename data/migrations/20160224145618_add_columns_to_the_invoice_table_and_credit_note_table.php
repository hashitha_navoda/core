<?php

use Phinx\Migration\AbstractMigration;

class AddColumnsToTheInvoiceTableAndCreditNoteTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
    public function change()
    {
    }
    */
    
    /**
     * Migrate Up.
     */
    public function up()
    {
        $table = $this->table('salesInvoice');
        
        if (!$table->hasColumn('salesInvoiceRemainingDiscValue')) {
            $table->addColumn('salesInvoiceRemainingDiscValue', 'decimal', [
                'after' => 'salesInvoiceDiscountRate',
                'null'=>'allow',
                'precision'=>20,
                'scale'=>2
            ])->update();
        }

        $cNTable = $this->table('creditNote');

        if (!$cNTable->hasColumn('pos')) {
            $cNTable->addColumn('pos', 'boolean', [
                'after' => 'statusID',
                'default'=>false
            ])->update();
        }

    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }
}