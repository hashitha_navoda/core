<?php

use Phinx\Migration\AbstractMigration;

class UpdateImageUploadFunctionMigration extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
      public function change()
      {
      }
     */

    /**
     * Migrate Up.
     */
    public function up()
    {
        $this->execute("UPDATE `feature` SET `featureController`='JobCard\\\Controller\\\API\\\Activity' "
                . "WHERE "
                . "`featureController`='JobCard\\\Controller\\\Activity' AND "
                . "`featureAction`= 'saveImage' AND "
                . "`featureCategory` = 'Application' AND "
                . "`featureType` = '1' AND "
                . "`moduleID` = '8' ;");

        $this->execute("UPDATE `feature` SET `featureController`='JobCard\\\Controller\\\API\\\Activity' "
                . "WHERE "
                . "`featureController`='JobCard\\\Controller\\\Activity' AND "
                . "`featureAction`= 'deleteImage' AND "
                . "`featureCategory` = 'Application' AND "
                . "`featureType` = '1' AND "
                . "`moduleID` = '8' ;");
    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }

}
