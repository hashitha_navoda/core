<?php

use Phinx\Migration\AbstractMigration;

class AddPromotionEmailTables extends AbstractMigration
{

    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     */
    public function up()
    {
        //create promotionEmail table
        $this->execute("CREATE TABLE IF NOT EXISTS `promotionEmail` (
  `promotionEmailID` int(11) NOT NULL AUTO_INCREMENT,
  `promotionEmailName` text NOT NULL,
  `promotionEmailSubject` text NOT NULL,
  `promotionEmailContent` text,
  `promotionEmailImage` varchar(255) DEFAULT NULL,
  `entityID` int(11) NOT NULL,
  PRIMARY KEY (`promotionEmailID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;");

        //create promotionEmailLocation table
        $this->execute("CREATE TABLE IF NOT EXISTS `promotionEmailLocation` (
  `promotionEmailLocationID` int(11) NOT NULL AUTO_INCREMENT,
  `promotionEmail` int(11) NOT NULL,
  `promotionEmailLocation` int(11) NOT NULL,
  PRIMARY KEY (`promotionEmailLocationID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;");

        //create promotionEmailProduct Table
        $this->execute("CREATE TABLE IF NOT EXISTS `promotionEmailProduct` (
  `promotionEmailProductID` int(11) NOT NULL AUTO_INCREMENT,
  `promotionEmail` int(11) NOT NULL,
  `promotionEmailProduct` int(11) NOT NULL,
  PRIMARY KEY (`promotionEmailProductID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;");

        //create promotionEmailCustomer Table
        $this->execute("CREATE TABLE IF NOT EXISTS `promotionEmailCustomer` (
  `promotionEmailCustomerID` int(11) NOT NULL AUTO_INCREMENT,
  `promotionEmail` int(11) NOT NULL,
  `promotionEmailCustomer` int(11) NOT NULL,
  PRIMARY KEY (`promotionEmailCustomerID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;");
    }

}
