<?php

use Phinx\Migration\AbstractMigration;

class AddValuesToTheCurrencyTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
      public function change()
      {
      }
     */

    /**
     * Migrate Up.
     */
    public function up()
    {


        $value = $this->fetchRow("SELECT (`currencyID`) FROM `displaySetup` WHERE `displaySetupID`=1");


        $this->execute('SET character_set_results=utf8');
        $this->execute('SET character_set_client=utf8');
        $this->execute('ALTER TABLE `displaySetup` DROP FOREIGN KEY `fk_displaySetup_currencyID`');
        $this->execute('ALTER TABLE `displaySetup` ADD CONSTRAINT `fk_displaySetup_currencyID` FOREIGN KEY ( `currencyID` ) REFERENCES `currency`(`currencyID`)ON DELETE SET NULL ON UPDATE SET NULL');
        $this->execute('DELETE FROM `currency`');
        $this->execute("INSERT INTO `currency` (`currencyID`,`currencyName`,`currencySymbol`) VALUES ('1','AUD','$');");
        $this->execute("INSERT INTO `currency` (`currencyID`,`currencyName`,`currencySymbol`) VALUES ('2','CAD','$');");
        $this->execute("INSERT INTO `currency` (`currencyID`,`currencyName`,`currencySymbol`) VALUES ('3','CHF','CHF');");
        $this->execute("INSERT INTO `currency` (`currencyID`,`currencyName`,`currencySymbol`) VALUES ('4','EUR','€');");
        $this->execute("INSERT INTO `currency` (`currencyID`,`currencyName`,`currencySymbol`) VALUES ('5','GBP','£');");
        $this->execute("INSERT INTO `currency` (`currencyID`,`currencyName`,`currencySymbol`) VALUES ('6','HKD','HK$');");
        $this->execute("INSERT INTO `currency` (`currencyID`,`currencyName`,`currencySymbol`) VALUES ('7','JPY','¥');");
        $this->execute("INSERT INTO `currency` (`currencyID`,`currencyName`,`currencySymbol`) VALUES ('8','LKR','Rs');");
        $this->execute("INSERT INTO `currency` (`currencyID`,`currencyName`,`currencySymbol`) VALUES ('9','MYR','RM');");
        $this->execute("INSERT INTO `currency` (`currencyID`,`currencyName`,`currencySymbol`) VALUES ('10','NZD','$');");
        $this->execute("INSERT INTO `currency` (`currencyID`,`currencyName`,`currencySymbol`) VALUES ('11','Peso','P');");
        $this->execute("INSERT INTO `currency` (`currencyID`,`currencyName`,`currencySymbol`) VALUES ('12','SGD','$');");
        $this->execute("INSERT INTO `currency` (`currencyID`,`currencyName`,`currencySymbol`) VALUES ('13','USD','$');");

        if ($value) {
            if ($value['currencyID'] == '1') {
                $this->execute("UPDATE `displaySetup` SET `currencyID`='8' WHERE `displaySetupID`='1' ");
            } else if ($value['currencyID'] == '2') {
                $this->execute("UPDATE `displaySetup` SET `currencyID`='13' WHERE `displaySetupID`='1' ");
            }
        }
    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }

}
