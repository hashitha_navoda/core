<?php

use Phinx\Migration\AbstractMigration;

class AddBouncedStatueToIncomingPaymentMethodChequeTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
    public function change()
    {
    }
    */
    
    /**
     * Migrate Up.
     */
    public function up()
    {
        $pdo = $this->getAdapter()->getConnection();        
        $roles = $this->fetchAll("SELECT roleID FROM `role`"); //role list
        
        //feature list
        $featureList = array(
            array(
                'name' =>'Bounced Cheques',
                'controller'=>'Expenses\Controller\ChequeDeposit',
                'action'=>'bouncedCheques',
                'category'=>'Cheque Deposit',
                'type'=>'0',
                'moduleId'=>'9'
            ), 
            array(
                'name' =>'Default',
                'controller'=>'Invoice\Controller\CustomerPaymentsAPI',
                'action'=>'getPaymentReceiptPath',
                'category'=>'Application',
                'type'=>'1',
                'moduleId'=>'3'
            )
        );
        
        foreach ($featureList as $feature)
        {            
            $featureInsert = <<<SQL
                INSERT INTO `feature` 
                    (
                        `featureName`, 
                        `featureController`, 
                        `featureAction`, 
                        `featureCategory`, 
                        `featureType`, 
                        `moduleID`
                    ) VALUES (
                        :featureName, 
                        :featureController, 
                        :featureAction, 
                        :featureCategory, 
                        :featureType, 
                        :featureModuleId
                    );
SQL;
            $pdo->prepare($featureInsert)->execute(array(
                'featureName' => $feature['name'], 
                'featureController' => $feature['controller'], 
                'featureAction' => $feature['action'], 
                'featureCategory' => $feature['category'], 
                'featureType' => $feature['type'], 
                'featureModuleId' => $feature['moduleId']
            ));
            
            $featureID = $this->fetchRow("select LAST_INSERT_ID()")[0];
            
            foreach ($roles as $role) {
                $roleID = $role['roleID'];
                if ($roleID == 1) {
                    $enabled = 1;
                } else {
                    //for enable api calls for every role
                    if($feature['type'] == 1){
                        $enabled = 1;
                    } else {
                        $enabled = 0;
                    }
                }
                //insert role feature to role feature table
                $roleFeatureInsert = <<<SQL
                INSERT INTO `roleFeature` 
                    (
                        `roleID`, 
                        `featureID`, 
                        `roleFeatureEnabled`
                    ) VALUES (
                        :roleID, 
                        :featureID, 
                        :roleFeatureEnabled
                    );
SQL;
                $pdo->prepare($roleFeatureInsert)->execute(array(
                    'roleID' => $roleID, 
                    'featureID' => $featureID, 
                    'roleFeatureEnabled' => $enabled
                ));                
            }
        }        
        
        $this->execute("SET SESSION old_alter_table=1; ALTER TABLE `incomingPaymentMethodCheque` ADD `incomingPaymentMethodChequeBouncedStatus` TINYINT(1) NOT NULL DEFAULT '0';");
        
        $this->execute("SET SESSION old_alter_table=1; ALTER TABLE `incomingPaymentMethod` ADD `entityId` INT(11) NULL DEFAULT NULL;");
        
        //for add entity id for incomingPaymentMethod table
        $ipms = $this->fetchAll("SELECT * FROM `incomingPaymentMethod`");//get all incoming payment methods
        
        foreach ($ipms as $ipm) {
            $incomingPaymentDetails = $this->fetchRow("SELECT incomingPayment.incomingPaymentID, entity.createdBy, entity.createdTimeStamp FROM `incomingPayment` LEFT JOIN `entity` ON incomingPayment.entityID = entity.entityID WHERE incomingPayment.incomingPaymentID = {$ipm['incomingPaymentId']}");
                        
            $entityInsert = <<<SQL
                INSERT INTO `entity` 
                    (
                        `createdBy`, 
                        `createdTimeStamp`, 
                        `updatedBy`, 
                        `updatedTimeStamp`, 
                        `deleted`, 
                        `deletedBy`,
                        `deletedTimeStamp`
                    ) VALUES (
                        :createdBy, 
                        :createdTimeStamp, 
                        :updatedBy, 
                        :updatedTimeStamp, 
                        :deleted, 
                        :deletedBy,
                        :deletedTimeStamp
                    );
SQL;
            $pdo->prepare($entityInsert)->execute(array(
                'createdBy' => $incomingPaymentDetails['createdBy'],
                'createdTimeStamp' => $incomingPaymentDetails['createdTimeStamp'],
                'updatedBy' => $incomingPaymentDetails['createdBy'],
                'updatedTimeStamp' => $incomingPaymentDetails['createdTimeStamp'],
                'deleted' => '0', 
                'deletedBy' => NULL,
                'deletedTimeStamp' => NULL
            ));
            
            $entityId = $this->fetchRow("select LAST_INSERT_ID()");
            
            $updatePaymentMethod = <<<SQL
                UPDATE `incomingPaymentMethod` SET entityId = :entityId WHERE incomingPaymentMethodId = :incomingPaymentMethodId;
SQL;
            $pdo->prepare($updatePaymentMethod)->execute(array(
                'entityId' => $entityId[0],
                'incomingPaymentMethodId' => $ipm['incomingPaymentMethodId']
            ));
        }
    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }
}