<?php

use Phinx\Migration\AbstractMigration;

class AddNewCloumnForSmsTypeTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->table('smsType');
        $column = $table->hasColumn('smsTypeDayFrame');
        $column2 = $table->hasColumn('smsTypeDayFrameDay');
        $column3 = $table->hasColumn('smsTypeSelectedCustomerType');

        if (!$column) {
            $table->addColumn('smsTypeDayFrame', 'string', ['limit' => 200,'default' => null, 'null' => true])
              ->update();
        }

        if (!$column2) {
            $table->addColumn('smsTypeDayFrameDay', 'integer', ['limit' => 20,'default' => null, 'null' => true])
              ->update();
        }

        if (!$column3) {
            $table->addColumn('smsTypeSelectedCustomerType', 'integer', ['limit' => 20,'default' => null, 'null' => true])
              ->update();
        }
    }
}
