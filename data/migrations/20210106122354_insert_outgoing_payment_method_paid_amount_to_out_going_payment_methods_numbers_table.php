<?php

use Phinx\Migration\AbstractMigration;

class InsertOutgoingPaymentMethodPaidAmountToOutGoingPaymentMethodsNumbersTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $pdo = $this->getAdapter()->getConnection();
        $paymentData = $this->fetchAll("SELECT * FROM `outgoingPayment`");
        $arr = [];

        if (sizeof($paymentData) > 0) {
            foreach ($paymentData as $key => $value) {
                $payID = $value['outgoingPaymentID'];

                $temp = [];
                if ($value['outgoingPaymentType'] != 'advance') {

                    $paymentInvoiceData = $this->fetchAll("SELECT * FROM `outgoingPaymentInvoice`  WHERE `paymentID`= $payID");

                    foreach ($paymentInvoiceData as $key1 => $value1) {
                        if (!empty($value1['paymentMethodID'])) {
                            $tempKey = $value1['paymentMethodID'];

                            if (array_key_exists($tempKey, $temp)) {
                                $temp[$tempKey] +=  $value1['outgoingInvoiceCashAmount'];
                            } else  {
                                $temp[$tempKey] =  $value1['outgoingInvoiceCashAmount'];

                            }


                        }

                    }
                } else {

                    $temp[$value['paymentMethodID']] = $value['outgoingPaymentAmount'];

                }

                $outgoingPaymentMethodNumbersData = $this->fetchAll("SELECT * FROM `outGoingPaymentMethodsNumbers`  WHERE `outGoingPaymentID`= $payID");

                if (sizeof($outgoingPaymentMethodNumbersData) > 0) {

                    foreach ($outgoingPaymentMethodNumbersData as $key44 => $value44) {

                        $outGoingPaymentMethodsNumbersID = $value44['outGoingPaymentMethodsNumbersID'];
                        $outGoingPaymentMethodPaidAmount = (array_key_exists($value44['outGoingPaymentMethodID'], $temp)) ? floatval($temp[$value44['outGoingPaymentMethodID']]) : 0;

                        $this->execute("UPDATE `outGoingPaymentMethodsNumbers` SET `outGoingPaymentMethodPaidAmount` = $outGoingPaymentMethodPaidAmount WHERE `outGoingPaymentMethodsNumbersID` = $outGoingPaymentMethodsNumbersID"); 
                    }

                }

            }
        }
    }
}
