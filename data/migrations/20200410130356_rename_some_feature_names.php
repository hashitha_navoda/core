<?php

use Phinx\Migration\AbstractMigration;

class RenameSomeFeatureNames extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        ini_set('memory_limit', '-1');
        $features = $this->fetchAll("SELECT featureID,featureName,featureAction,featureCategory,featureType FROM `feature` WHERE moduleID = 13 AND featureCategory = 'Material Management'  OR featureCategory = 'Service Cost'");

        foreach ($features as $feature) {
            $strArr = preg_split('/(?=[A-Z])/',$feature['featureAction']);
            $feature['featureName'] = "";
            $featureName = "";
            $featureID = $feature['featureID'];

            if (sizeof($strArr) == 2) {
                $feature['featureName'] = $strArr[0].' '.strtolower( $strArr[1] );
            } elseif (sizeof($strArr) == 1) {
                $feature['featureName'] = $strArr[0];
            }
            $featureName = "'".$feature['featureName']."'";

            if (!empty($feature['featureName'])) {
                 $this->execute("UPDATE `feature` SET `featureName` = $featureName WHERE `feature`.`featureID` = $featureID");       
            }
        }
    }
}
