<?php

use Phinx\Migration\AbstractMigration;

class AddNewActionsToTournament extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $pdo = $this->getAdapter()->getConnection();
        $rows = $this->fetchAll("SELECT roleID FROM `role`");

        $dataArray = array(
            array(
                'featureID' => 'NULL',
                'featureName' => 'create',
                'featureController' => 'Jobs\Controller\Event',
                'featureAction' => 'create',
                'featureCategory' => 'Event',
                'featureType' => '0',
                'moduleID' => '13'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'list',
                'featureController' => 'Jobs\Controller\Event',
                'featureAction' => 'list',
                'featureCategory' => 'Event',
                'featureType' => '0',
                'moduleID' => '13'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'view',
                'featureController' => 'Jobs\Controller\Event',
                'featureAction' => 'view',
                'featureCategory' => 'Event',
                'featureType' => '0',
                'moduleID' => '13'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'edit',
                'featureController' => 'Jobs\Controller\Event',
                'featureAction' => 'edit',
                'featureCategory' => 'Event',
                'featureType' => '0',
                'moduleID' => '13'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'index',
                'featureController' => 'Jobs\Controller\EventSetup',
                'featureAction' => 'index',
                'featureCategory' => 'Event Setup',
                'featureType' => '0',
                'moduleID' => '13'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'create',
                'featureController' => 'Jobs\Controller\Participant',
                'featureAction' => 'create',
                'featureCategory' => 'Participant',
                'featureType' => '0',
                'moduleID' => '13'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'create',
                'featureController' => 'Jobs\Controller\ParticipantSubType',
                'featureAction' => 'create',
                'featureCategory' => 'Participant Sub Type',
                'featureType' => '0',
                'moduleID' => '13'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'create',
                'featureController' => 'Jobs\Controller\ParticipantType',
                'featureAction' => 'create',
                'featureCategory' => 'Participant Type',
                'featureType' => '0',
                'moduleID' => '13'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'list',
                'featureController' => 'Jobs\Controller\ScoreUpdate',
                'featureAction' => 'list',
                'featureCategory' => 'Score Update',
                'featureType' => '0',
                'moduleID' => '13'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'scoreUpdate',
                'featureController' => 'Jobs\Controller\ScoreUpdate',
                'featureAction' => 'scoreUpdate',
                'featureCategory' => 'Score Update',
                'featureType' => '0',
                'moduleID' => '13'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'index',
                'featureController' => 'Jobs\Controller\Tournament',
                'featureAction' => 'index',
                'featureCategory' => 'Tournament',
                'featureType' => '0',
                'moduleID' => '13'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'list',
                'featureController' => 'Jobs\Controller\Tournament',
                'featureAction' => 'list',
                'featureCategory' => 'Tournament',
                'featureType' => '0',
                'moduleID' => '13'
            ),
             array(
                'featureID' => 'NULL',
                'featureName' => 'index',
                'featureController' => 'Jobs\Controller\TournamentSetup',
                'featureAction' => 'index',
                'featureCategory' => 'Tournament Setup',
                'featureType' => '0',
                'moduleID' => '13'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'create',
                'featureController' => 'Jobs\Controller\TournamentTeam',
                'featureAction' => 'create',
                'featureCategory' => 'Tournament Team',
                'featureType' => '0',
                'moduleID' => '13'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'Default',
                'featureController' => 'Jobs\Controller\API\Event',
                'featureAction' => 'saveJob',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '13'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'Default',
                'featureController' => 'Jobs\Controller\API\Event',
                'featureAction' => 'searchJobsForDropdown',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '13'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'Default',
                'featureController' => 'Jobs\Controller\API\Event',
                'featureAction' => 'loadJobsForDropdown',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '13'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'Default',
                'featureController' => 'Jobs\Controller\API\Event',
                'featureAction' => 'getJobBySearchKey',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '13'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'Default',
                'featureController' => 'Jobs\Controller\API\Event',
                'featureAction' => 'searchByProjectAndJob',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '13'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'Default',
                'featureController' => 'Jobs\Controller\API\Event',
                'featureAction' => 'getJobSupervisorsByJobID',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '13'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'Default',
                'featureController' => 'Jobs\Controller\API\Event',
                'featureAction' => 'delete',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '13'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'Default',
                'featureController' => 'Jobs\Controller\API\Event',
                'featureAction' => 'getDataForUpdate',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '13'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'Default',
                'featureController' => 'Jobs\Controller\API\Event',
                'featureAction' => 'updateJob',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '13'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'Default',
                'featureController' => 'Jobs\Controller\API\Event',
                'featureAction' => 'getJobContractorsByJobID',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '13'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'Default',
                'featureController' => 'Jobs\Controller\API\Event',
                'featureAction' => 'searchJobProjectForDropdown',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '13'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'Default',
                'featureController' => 'Jobs\Controller\API\Event',
                'featureAction' => 'getJobTaskWithProductDetails',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '13'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'Default',
                'featureController' => 'Jobs\Controller\API\Event',
                'featureAction' => 'getParticipants',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '13'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'Default',
                'featureController' => 'Jobs\Controller\API\Participant',
                'featureAction' => 'createParticipant',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '13'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'Default',
                'featureController' => 'Jobs\Controller\API\Participant',
                'featureAction' => 'changeStatusID',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '13'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'Default',
                'featureController' => 'Jobs\Controller\API\Participant',
                'featureAction' => 'update',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '13'
            ),
             array(
                'featureID' => 'NULL',
                'featureName' => 'Default',
                'featureController' => 'Jobs\Controller\API\Participant',
                'featureAction' => 'search',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '13'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'Default',
                'featureController' => 'Jobs\Controller\API\Participant',
                'featureAction' => 'getEmployeeDesignation',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '13'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'Default',
                'featureController' => 'Jobs\Controller\API\Participant',
                'featureAction' => 'getPartcipantTypes',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '13'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'Default',
                'featureController' => 'Jobs\Controller\API\Participant',
                'featureAction' => 'getUploadedAttachemnts',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '13'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'Default',
                'featureController' => 'Jobs\Controller\API\Participant',
                'featureAction' => 'deleteEmployee',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '13'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'Default',
                'featureController' => 'Jobs\Controller\API\Participant',
                'featureAction' => 'searchJobSupervisorsForDropdown',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '13'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'Default',
                'featureController' => 'Jobs\Controller\API\Participant',
                'featureAction' => 'searchJobManagersForDropdown',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '13'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'Default',
                'featureController' => 'Jobs\Controller\API\Participant',
                'featureAction' => 'getEmployeeDetails',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '13'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'Default',
                'featureController' => 'Jobs\Controller\API\Participant',
                'featureAction' => 'searchDesignationWiseEmployeesForDropdown',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '13'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'Default',
                'featureController' => 'Jobs\Controller\API\Participant',
                'featureAction' => 'getDepartmentWiseEmployees',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '13'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'Default',
                'featureController' => 'Jobs\Controller\API\Participant',
                'featureAction' => 'getParticipants',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '13'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'Default',
                'featureController' => 'Jobs\Controller\API\ScoreUpdate',
                'featureAction' => 'searchByProjectAndJob',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '13'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'Default',
                'featureController' => 'Jobs\Controller\API\ScoreUpdate',
                'featureAction' => 'updateScore',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '13'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'Default',
                'featureController' => 'Jobs\Controller\API\ScoreUpdate',
                'featureAction' => 'getScore',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '13'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'Default',
                'featureController' => 'Jobs\Controller\API\Tournament',
                'featureAction' => 'create',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '13'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'Default',
                'featureController' => 'Jobs\Controller\API\Tournament',
                'featureAction' => 'getEditDetails',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '13'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'Default',
                'featureController' => 'Jobs\Controller\API\Tournament',
                'featureAction' => 'update',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '13'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'Default',
                'featureController' => 'Jobs\Controller\API\Tournament',
                'featureAction' => 'getProjectManagers',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '13'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'Default',
                'featureController' => 'Jobs\Controller\API\Tournament',
                'featureAction' => 'search',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '13'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'Default',
                'featureController' => 'Jobs\Controller\API\Tournament',
                'featureAction' => 'delete',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '13'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'Default',
                'featureController' => 'Jobs\Controller\API\Tournament',
                'featureAction' => 'searchProjectsForDropdown',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '13'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'Default',
                'featureController' => 'Jobs\Controller\API\Tournament',
                'featureAction' => 'getProjectDetails',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '13'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'Default',
                'featureController' => 'Jobs\Controller\API\TournamentTeam',
                'featureAction' => 'createTeam',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '13'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'Default',
                'featureController' => 'Jobs\Controller\API\TournamentTeam',
                'featureAction' => 'changeStatusID',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '13'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'Default',
                'featureController' => 'Jobs\Controller\API\TournamentTeam',
                'featureAction' => 'update',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '13'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'Default',
                'featureController' => 'Jobs\Controller\API\TournamentTeam',
                'featureAction' => 'search',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '13'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'Default',
                'featureController' => 'Jobs\Controller\API\TournamentTeam',
                'featureAction' => 'searchParticipant',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '13'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'Default',
                'featureController' => 'Jobs\Controller\API\TournamentTeam',
                'featureAction' => 'getTeamEmployees',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '13'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'Default',
                'featureController' => 'Jobs\Controller\API\TournamentTeam',
                'featureAction' => 'getTeamPartcipants',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '13'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'Default',
                'featureController' => 'Jobs\Controller\API\TournamentTeam',
                'featureAction' => 'deleteTeamByTeamID',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '13'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'Default',
                'featureController' => 'Jobs\Controller\API\TournamentTeam',
                'featureAction' => 'searchTeamsForDropdown',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '13'
            ),
            
         );

        foreach($dataArray as $feature){

            $featureInsert = <<<SQL
                INSERT INTO `feature`
                (
                    `featureName`,
                    `featureController`,
                    `featureAction`,
                    `featureCategory`,
                    `featureType`,
                    `moduleID`
                ) VALUES (
                    :featureName,
                    :featureController,
                    :featureAction,
                    :featureCategory,
                    :featureType,
                    :featureModuleId
                );

SQL;

            $pdo->prepare($featureInsert)->execute(array(
                'featureName' => $feature['featureName'],
                'featureController' => $feature['featureController'],
                'featureAction' => $feature['featureAction'],
                'featureCategory' => $feature['featureCategory'],
                'featureType' => $feature['featureType'],
                'featureModuleId' => $feature['moduleID']
            ));

            $featureID = $this->fetchRow("select LAST_INSERT_ID()")[0];

            foreach ($rows as $row) {
                $roleID = $row['roleID'];

                if ($roleID == 1) {
                    $enabled = 1;
                } else {

                   //for enable api calls for every role
                    if($feature['featureType'] == 1){
                        $enabled = 1;
                    } else {
                        $enabled = 0;
                    }

                }

                $roleFeatureInsert =<<<SQL
                    INSERT INTO `roleFeature`
                    (
                        `roleID`,
                        `featureID`,
                        `roleFeatureEnabled`
                    ) VALUES(
                        :roleID,
                        :featureID,
                        :roleFeatureEnabled
                    )
SQL;
                $pdo->prepare($roleFeatureInsert)->execute(array(
                    'roleID' => $roleID,
                    'featureID' => $featureID,
                    'roleFeatureEnabled' => $enabled
                ));
            }
        }
    }
}
