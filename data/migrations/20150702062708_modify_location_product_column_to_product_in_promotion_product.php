<?php

use Phinx\Migration\AbstractMigration;

class ModifyLocationProductColumnToProductInPromotionProduct extends AbstractMigration
{

    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     */
    public function up()
    {
        $this->execute("SET SESSION old_alter_table=1; ALTER TABLE `promotionProduct` CHANGE `promotionProductLocationProductID` `promotionProductProductID` INT( 11 ) NOT NULL ");
    }

}
