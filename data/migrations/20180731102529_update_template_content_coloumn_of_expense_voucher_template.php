<?php

use Phinx\Migration\AbstractMigration;

class UpdateTemplateContentColoumnOfExpenseVoucherTemplate extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        // $conn = $this->getAdapter()->getConnection();
        // $quotedString = $conn->quote('org\foo\Bar');
        $this->execute('UPDATE `template` SET `templateContent` = REPLACE(`templateContent`,"\<td style\=\"text\-align\: right\;\"\>\&nbsp\;\<abbr contenteditable\=\"false\" spellcheck\=\"false\"\>\[Payment Voucher Code\]\<\/abbr\>\<\/td\>","\<td style\=\"text\-align\: right\;\"\>\<abbr contenteditable\=\"false\" spellcheck\=\"false\"\>\[Payment Voucher Code\]\<\/abbr\>\&nbsp\;\&nbsp\;\<\/td\>") WHERE `templateID` = 71');
    }
}
