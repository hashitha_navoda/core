<?php

use Phinx\Migration\AbstractMigration;

class CreateNewTableCalledCustomCurrency extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
      public function change()
      {
      }
     */

    /**
     * Migrate Up.
     */
    public function up()
    {
        $this->execute("CREATE TABLE IF NOT EXISTS `customCurrency` (
  `customCurrencyId` int(11) NOT NULL AUTO_INCREMENT,
  `customCurrencyName` varchar(255) NOT NULL,
  `customCurrencySymbol` varchar(255) NOT NULL,
  `customCurrencyRate` decimal(20,5) NOT NULL DEFAULT '0.00000',
  PRIMARY KEY (`customCurrencyId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;");
    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }

}
