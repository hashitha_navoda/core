<?php

use Phinx\Migration\AbstractMigration;

class AddNewDuplicateTemplateIntoTemplateTableForPurchaseInvoice extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    // public function change()
    // {

    // }

    public function up()
    {
        $sql = <<<EOF
                INSERT INTO `template` (`templateName`, `documentTypeID`, `documentSizeID`, `templateContent`, `templateFooterDetails`, `templateFooterShow`, `templateDefault`, `templateSample`) VALUES
('Template (with payment view)', 12, 1, '<table class="table table-bordered" width="100%">
<tbody>
<tr>
<td><img class="logo" style="background: #e0e0e0; max-height: 75px; display: block; text-align: center;" src="[Company Logo]" alt="Company Logo" /><span style="font-weight: bold;"><abbr contenteditable="false" spellcheck="false">[Company Name]</abbr> </span><br /><abbr contenteditable="false" spellcheck="false">[Company Address]</abbr> <br />Tel: <abbr contenteditable="false" spellcheck="false">[Company Tel. No]</abbr> <br />Email: <abbr contenteditable="false" spellcheck="false">[Company Email]</abbr> <br />Tax No: <abbr contenteditable="false" spellcheck="false">[Company Tax Reg. No]</abbr>&nbsp;</td>
<td><h2 align="right">Payment Voucher</h2></td></tr><tr><td><div class="add_block"><hr /></div></td><td class="relative_data" rowspan="2" align="right">
<table class="table table-bordered table-condensed" width="100%">
<tbody><tr><td>PV Code:</td><td style="text-align: right;"><abbr contenteditable="false" spellcheck="false">[PV No]</abbr>&nbsp;</td>
</tr><tr><td>Delivery Date:</td><td style="text-align: right;"><abbr contenteditable="false" spellcheck="false">[Delivery Date]</abbr></td>
</tr><tr><td style="vertical-align: top;">Retrieve Location:</td><td style="vertical-align: top; text-align: right;">&nbsp; <abbr contenteditable="false" spellcheck="false">[Retrieve Location]</abbr> </td>
</tr><tr><td>Supplier Reference:</td><td style="text-align: right;"><abbr contenteditable="false" spellcheck="false">[Supplier Ref]</abbr></td></tr></tbody></table></td></tr><tr><td><div class="add_block"><div id="cust_name"><strong><abbr contenteditable="false" spellcheck="false">[Supplier]</abbr>&nbsp;&nbsp;&nbsp; <br /></strong><abbr contenteditable="false" spellcheck="false">[Supplier Address]</abbr>&nbsp;<strong><br /></strong></div>
</div></td></tr></tbody></table><noneditabletable><table class="noneditable table table-bordered" style="width: 100%; margin-bottom: 100px;" title="" contenteditable="false" data-original-title="">
<tbody><tr>
<td colspan="2"><div class="qout_body"><hr /><table id="item_tbl" class="table table-bordered table-striped" width="100%"><colgroup><col width="10px" /><col width="90px" /><col width="290px" /><col width="50px" /><col width="100px" /><col width="100px" /> </colgroup>
<thead>
<tr><th colspan="2">Product code</th><th colspan="1">Product</th><th colspan="1">Qty</th><th colspan="1">Price</th><th colspan="1">Total</th></tr>
</thead>
<tbody><tr><td>1</td><td align="left">12</td><td align="left">dell_lap<br /><small>(Disc. 1.00)</small></td><td align="left">1</td>
<td align="right">100.00</td><td align="right">99.00</td></tr></tbody></table><div class="summary"><br /> <br /> <br /> <br />
<table id="" class="table" width="100%"><tbody>
<tr><td colspan="2" rowspan="3">&nbsp;</td><td colspan="3">Sub Total</td><td class="text-right">99.00</td></tr><tr><td colspan="3">
<h3>Total</h3>
</td><td class="text-right"><h3>99.00</h3></td></tr>
<!--<tr><td colspan="6"><div></div></td></tr>--></tbody></table></div></div></td></tr></tbody></table></noneditabletable><p></p><table class="table table-bordered" width="100%"><tbody><tr><td>Comment:</td><td style="text-align: right;"><abbr contenteditable="false" spellcheck="false">[Comment]</abbr>&nbsp;<br />&nbsp;</td>
</tr></tbody></table><footer class="doc"><table class="table table-bordered" width="100%"><tbody><tr>
<td><abbr contenteditable="false" spellcheck="false">[Company Name]</abbr>&nbsp;</td><td>&nbsp;</td><td style="text-align: right;"><abbr contenteditable="false" spellcheck="false">[Today Date]</abbr>&nbsp;</td>
</tr></tbody></table></footer>', '', 0, 0, 0);
EOF;
        $this->execute($sql);
    }
    /**
     * Migrate Down.
     */
    public function down()
    {
    }
}
