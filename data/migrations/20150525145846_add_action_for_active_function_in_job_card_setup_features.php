<?php

use Phinx\Migration\AbstractMigration;

class AddActionForActiveFunctionInJobCardSetupFeatures extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
      public function change()
      {
      }
     */

    /**
     * Migrate Up.
     */
    public function up()
    {
        $this->execute("INSERT INTO `feature` (`featureID`, `featureName`, `featureController`, `featureAction`, `featureCategory`, `featureType`, `moduleID`) VALUES (NULL, 'Default', 'JobCard\\\Controller\\\API\\\ProjectType', 'changeStatusID', 'Application', '1', '8');");
        $id1 = $this->fetchRow("select LAST_INSERT_ID()")[0];
        $this->execute("INSERT INTO `roleFeature` (`roleFeatureID`, `roleID`, `featureID`, `roleFeatureEnabled`) VALUES (NULL, '1', $id1, '1');");

        $this->execute("INSERT INTO `feature` (`featureID`, `featureName`, `featureController`, `featureAction`, `featureCategory`, `featureType`, `moduleID`) VALUES (NULL, 'Default', 'JobCard\\\Controller\\\API\\\JobType', 'changeStatusID', 'Application', '1', '8');");
        $id2 = $this->fetchRow("select LAST_INSERT_ID()")[0];
        $this->execute("INSERT INTO `roleFeature` (`roleFeatureID`, `roleID`, `featureID`, `roleFeatureEnabled`) VALUES (NULL, '1', $id2, '1');");

        $this->execute("INSERT INTO `feature` (`featureID`, `featureName`, `featureController`, `featureAction`, `featureCategory`, `featureType`, `moduleID`) VALUES (NULL, 'Default', 'JobCard\\\Controller\\\API\\\ActivityType', 'changeStatusID', 'Application', '1', '8');");
        $id3 = $this->fetchRow("select LAST_INSERT_ID()")[0];
        $this->execute("INSERT INTO `roleFeature` (`roleFeatureID`, `roleID`, `featureID`, `roleFeatureEnabled`) VALUES (NULL, '1', $id3, '1');");

        $this->execute("INSERT INTO `feature` (`featureID`, `featureName`, `featureController`, `featureAction`, `featureCategory`, `featureType`, `moduleID`) VALUES (NULL, 'Default', 'JobCard\\\Controller\\\API\\\Designation', 'changeStatusID', 'Application', '1', '8');");
        $id4 = $this->fetchRow("select LAST_INSERT_ID()")[0];
        $this->execute("INSERT INTO `roleFeature` (`roleFeatureID`, `roleID`, `featureID`, `roleFeatureEnabled`) VALUES (NULL, '1', $id4, '1');");

        $this->execute("INSERT INTO `feature` (`featureID`, `featureName`, `featureController`, `featureAction`, `featureCategory`, `featureType`, `moduleID`) VALUES (NULL, 'Default', 'JobCard\\\Controller\\\API\\\Division', 'changeStatusID', 'Application', '1', '8');");
        $id5 = $this->fetchRow("select LAST_INSERT_ID()")[0];
        $this->execute("INSERT INTO `roleFeature` (`roleFeatureID`, `roleID`, `featureID`, `roleFeatureEnabled`) VALUES (NULL, '1', $id5, '1');");

        $this->execute("INSERT INTO `feature` (`featureID`, `featureName`, `featureController`, `featureAction`, `featureCategory`, `featureType`, `moduleID`) VALUES (NULL, 'Default', 'JobCard\\\Controller\\\API\\\Employee', 'changeStatusID', 'Application', '1', '8');");
        $id6 = $this->fetchRow("select LAST_INSERT_ID()")[0];
        $this->execute("INSERT INTO `roleFeature` (`roleFeatureID`, `roleID`, `featureID`, `roleFeatureEnabled`) VALUES (NULL, '1', $id6, '1');");

        $this->execute("INSERT INTO `feature` (`featureID`, `featureName`, `featureController`, `featureAction`, `featureCategory`, `featureType`, `moduleID`) VALUES (NULL, 'Default', 'JobCard\\\Controller\\\API\\\CostType', 'changeStatusID', 'Application', '1', '8');");
        $id7 = $this->fetchRow("select LAST_INSERT_ID()")[0];
        $this->execute("INSERT INTO `roleFeature` (`roleFeatureID`, `roleID`, `featureID`, `roleFeatureEnabled`) VALUES (NULL, '1', $id7, '1');");

        $this->execute("INSERT INTO `feature` (`featureID`, `featureName`, `featureController`, `featureAction`, `featureCategory`, `featureType`, `moduleID`) VALUES (NULL, 'Default', 'JobCard\\\Controller\\\API\\\InquiryType', 'changeStatusID', 'Application', '1', '8');");
        $id8 = $this->fetchRow("select LAST_INSERT_ID()")[0];
        $this->execute("INSERT INTO `roleFeature` (`roleFeatureID`, `roleID`, `featureID`, `roleFeatureEnabled`) VALUES (NULL, '1', $id8, '1');");

        $this->execute("INSERT INTO `feature` (`featureID`, `featureName`, `featureController`, `featureAction`, `featureCategory`, `featureType`, `moduleID`) VALUES (NULL, 'Default', 'JobCard\\\Controller\\\API\\\InquiryStatus', 'changeStatusID', 'Application', '1', '8');");
        $id9 = $this->fetchRow("select LAST_INSERT_ID()")[0];
        $this->execute("INSERT INTO `roleFeature` (`roleFeatureID`, `roleID`, `featureID`, `roleFeatureEnabled`) VALUES (NULL, '1', $id9, '1');");

        $this->execute("INSERT INTO `feature` (`featureID`, `featureName`, `featureController`, `featureAction`, `featureCategory`, `featureType`, `moduleID`) VALUES (NULL, 'Default', 'JobCard\\\Controller\\\API\\\JobStation', 'changeStatusID', 'Application', '1', '8');");
        $id10 = $this->fetchRow("select LAST_INSERT_ID()")[0];
        $this->execute("INSERT INTO `roleFeature` (`roleFeatureID`, `roleID`, `featureID`, `roleFeatureEnabled`) VALUES (NULL, '1', $id10, '1');");

        $this->execute("INSERT INTO `feature` (`featureID`, `featureName`, `featureController`, `featureAction`, `featureCategory`, `featureType`, `moduleID`) VALUES (NULL, 'Default', 'JobCard\\\Controller\\\API\\\Contractor', 'changeStatusID', 'Application', '1', '8');");
        $id11 = $this->fetchRow("select LAST_INSERT_ID()")[0];
        $this->execute("INSERT INTO `roleFeature` (`roleFeatureID`, `roleID`, `featureID`, `roleFeatureEnabled`) VALUES (NULL, '1', $id11, '1');");

        $this->execute("SET SESSION old_alter_table=1;ALTER TABLE `contractor` ADD `contractorStatus` INT NOT NULL DEFAULT '1';");
        $this->execute("SET SESSION old_alter_table=1;ALTER TABLE `jobStation` ADD `jobStationStatus` INT NOT NULL DEFAULT '1';");
        $this->execute("SET SESSION old_alter_table=1;ALTER TABLE `inquiryStatus` ADD `inquiryStatusStatus` INT NOT NULL DEFAULT '1';");
        $this->execute("SET SESSION old_alter_table=1;ALTER TABLE `inquiryType` ADD `inquiryTypeStatus` INT NOT NULL DEFAULT '1';");
        $this->execute("SET SESSION old_alter_table=1;ALTER TABLE `costType` ADD `costTypeStatus` INT NOT NULL DEFAULT '1';");
        $this->execute("SET SESSION old_alter_table=1;ALTER TABLE `employee` ADD `employeeStatus` INT NOT NULL DEFAULT '1';");
        $this->execute("SET SESSION old_alter_table=1;ALTER TABLE `division` ADD `divisionStatus` INT NOT NULL DEFAULT '1';");
        $this->execute("SET SESSION old_alter_table=1;ALTER TABLE `designation` ADD `designationStatus` INT NOT NULL DEFAULT '1';");
        $this->execute("SET SESSION old_alter_table=1;ALTER TABLE `activityType` ADD `activityTypeStatus` INT NOT NULL DEFAULT '1';");
        $this->execute("SET SESSION old_alter_table=1;ALTER TABLE `jobType` ADD `jobTypeStatus` INT NOT NULL DEFAULT '1';");
        $this->execute("SET SESSION old_alter_table=1;ALTER TABLE `projectType` ADD `projectTypeStatus` INT NOT NULL DEFAULT '1';");
    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }

}
