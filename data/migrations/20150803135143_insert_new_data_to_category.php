<?php

use Phinx\Migration\AbstractMigration;

class InsertNewDataToCategory extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
      public function change()
      {
      }
     */

    /**
     * Migrate Up.
     */
    public function up()
    {

        $id = $this->fetchRow("SELECT categoryID
FROM `category`
LEFT JOIN entity ON category.entityID = entity.entityID
WHERE `categoryName` LIKE 'General'
AND `deleted` =0
LIMIT 0 , 30")[0];

        if (!$id) {
            $users = $this->query("SELECT *
FROM `user`
LEFT JOIN entity ON user.entityID = entity.entityID
WHERE deleted = 0 LIMIT 1");
            $user = (object) $users->fetch(PDO::FETCH_ASSOC);
            $userID = $user->userID;
            $currentTime = gmdate('Y-m-d H:i:s');
            $this->execute("INSERT INTO `entity` (`entityID`, `createdBy`, `createdTimeStamp`, `updatedBy`, `updatedTimeStamp`, `deleted`, `deletedBy`, `deletedTimeStamp`) VALUES (NULL, '{$userID}', '{$currentTime}', '{$userID}', '{$currentTime}', '0', NULL, NULL);");
            $eID = $this->fetchRow("select LAST_INSERT_ID()")[0];
            $this->execute("INSERT INTO `category` (
`categoryID` ,
`categoryName` ,
`categoryParentID` ,
`categoryLevel` ,
`categoryPrefix` ,
`categoryState` ,
`entityID`
)
VALUES (
NULL , 'General', '0' , '0', NULL , '1', '{$eID}'
);");
        }
    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }

}
