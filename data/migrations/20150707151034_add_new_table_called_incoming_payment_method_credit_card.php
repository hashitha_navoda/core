<?php

use Phinx\Migration\AbstractMigration;

class AddNewTableCalledIncomingPaymentMethodCreditCard extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
      public function change()
      {
      }
     */

    /**
     * Migrate Up.
     */
    public function up()
    {
        $this->execute("CREATE TABLE IF NOT EXISTS `incomingPaymentMethodCreditCard` (
  `incomingPaymentMethodCreditCardId` int(11) NOT NULL AUTO_INCREMENT,
  `incomingPaymentMethodCreditCardNumber` varchar(20) DEFAULT NULL,
  `incomingPaymentMethodCreditReceiptNumber` varchar(20) NOT NULL,
  PRIMARY KEY (`incomingPaymentMethodCreditCardId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;
");
    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }

}
