<?php

use Phinx\Migration\AbstractMigration;

class AddActivityJobRepeatReport extends AbstractMigration
{

    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     */
//    public function change()
//    {
//
//    }

    public function up()
    {
        $this->execute("SET SESSION old_alter_table=1; ALTER TABLE  `activity` ADD  `activityRepeatEnabled` BOOLEAN NOT NULL AFTER  `activityStatus` ,
ADD  `activityRepeatComment` TEXT NULL AFTER  `activityRepeatEnabled` ;");

        $this->execute("SET SESSION old_alter_table=1; ALTER TABLE  `job` ADD  `jobRepeatEnabled` BOOLEAN NOT NULL AFTER  `locationID` ,
ADD  `jobRepeatComment` TEXT NULL AFTER  `jobRepeatEnabled` ;");

        $rows = $this->fetchAll("SELECT roleID FROM `role`");

        $this->execute("INSERT INTO `feature` (`featureID`, `featureName`, `featureController`, `featureAction`, `featureCategory`, `featureType`, `moduleID`) VALUES (NULL, 'View Report', 'Reporting\\\Controller\\\Generate\\\JobCardReport', 'viewRepeatReport', 'Repeat Activity/Job Report', '0', '8');");
        $id1 = $this->fetchRow("select LAST_INSERT_ID()")[0];
        $this->execute("INSERT INTO `feature` (`featureID`, `featureName`, `featureController`, `featureAction`, `featureCategory`, `featureType`, `moduleID`) VALUES (NULL, 'Generate PDF', 'Reporting\\\Controller\\\Generate\\\JobCardReport', 'generateRepeatPdf', 'Repeat Activity/Job Report', '0', '8');");
        $id2 = $this->fetchRow("select LAST_INSERT_ID()")[0];
        $this->execute("INSERT INTO `feature` (`featureID`, `featureName`, `featureController`, `featureAction`, `featureCategory`, `featureType`, `moduleID`) VALUES (NULL, 'Generate CSV', 'Reporting\\\Controller\\\Generate\\\JobCardReport', 'generateRepeatCsv', 'Repeat Activity/Job Report', '0', '8');");
        $id3 = $this->fetchRow("select LAST_INSERT_ID()")[0];

        foreach ($rows as $row) {
            $roleID = $row['roleID'];
            $enabled = 1;
            $this->execute("INSERT INTO `roleFeature` (`roleFeatureID`, `roleID`, `featureID`, `roleFeatureEnabled`) VALUES (NULL, $roleID, $id1,$enabled);");
            $this->execute("INSERT INTO `roleFeature` (`roleFeatureID`, `roleID`, `featureID`, `roleFeatureEnabled`) VALUES (NULL, $roleID, $id2,$enabled);");
            $this->execute("INSERT INTO `roleFeature` (`roleFeatureID`, `roleID`, `featureID`, `roleFeatureEnabled`) VALUES (NULL, $roleID, $id3,$enabled);");
        }
    }

}
