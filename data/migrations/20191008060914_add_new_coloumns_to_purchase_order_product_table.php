<?php

use Phinx\Migration\AbstractMigration;

class AddNewColoumnsToPurchaseOrderProductTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->table('purchaseOrderProduct');
        $column1 = $table->hasColumn('purchaseOrderProductDocumentId');
        if (!$column1) {
            $table->addColumn('purchaseOrderProductDocumentId', 'integer', ['default' => null, 'null' => true])
              ->update();
        }

        $column2 = $table->hasColumn('purchaseOrderProductDocumentTypeId');
        if (!$column2) {
            $table->addColumn('purchaseOrderProductDocumentTypeId', 'integer', ['default' => null, 'null' => true])
              ->update();
        }

        $column3 = $table->hasColumn('purchaseOrderProductDoumentTypeCopiedQuantity');
        if (!$column3) {
            $table->addColumn('purchaseOrderProductDoumentTypeCopiedQuantity', 'decimal', ['precision' => 20, 'scale' => 5,'default' => null, 'null' => true])
              ->update();
        }
    }
}
