<?php

use Phinx\Migration\AbstractMigration;

class AddExpenseTypeTables extends AbstractMigration
{

    public function up()
    {
        //add ExpenseType Table
        $this->execute("CREATE TABLE IF NOT EXISTS `expenseType` (
  `expenseTypeId` int(11) NOT NULL AUTO_INCREMENT,
  `expenseTypeName` varchar(255) NOT NULL,
  `expenseTypeCategory` int(11) NOT NULL,
  `expenseTypeApproverEnabled` tinyint(1) NOT NULL,
  `expenseTypeStatus` int(11) NOT NULL,
  `entityId` int(11) NOT NULL,
  PRIMARY KEY (`expenseTypeId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;");

        //add ExpenseTypeLimit Table
        $this->execute("CREATE TABLE IF NOT EXISTS `expenseTypeLimit` (
  `expenseTypeLimitId` int(11) NOT NULL AUTO_INCREMENT,
  `expenseTypeId` int(11) NOT NULL,
  `expenseTypeLimitMin` decimal(12,2) NOT NULL,
  `expenseTypeLimitMax` decimal(12,2) NOT NULL,
  PRIMARY KEY (`expenseTypeLimitId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;");

        //add ExpenseTypeLimitApprover Table
        $this->execute("CREATE TABLE IF NOT EXISTS `expenseTypeLimitApprover` (
  `expenseTypeLimitApproverId` int(11) NOT NULL AUTO_INCREMENT,
  `expenseTypeLimitId` int(11) NOT NULL,
  `expenseTypeLimitApprover` int(11) NOT NULL,
  PRIMARY KEY (`expenseTypeLimitApproverId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;");
    }

}
