<?php

use Phinx\Migration\AbstractMigration;

class UpdateDiscountValueOnItemInAndOutTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
      public function change()
      {
      }
     */

    /**
     * Migrate Up.
     */
    public function up()
    {
        //updates invoice discount value on itemout table
        $salesInvoiceRows = $this->fetchAll("SELECT * FROM  `salesInvoiceProduct`");
        foreach ($salesInvoiceRows as $value) {

            if ($value['salesInvoiceProductDiscountType'] == "precentage" || $value['salesInvoiceProductDiscountType'] == "value") {
                $invoiceID = $value['salesInvoiceID'];

                //calculate discount value
                $discountValue = 0;
                if (isset($value['salesInvoiceProductDiscountType']) && $value['salesInvoiceProductDiscount'] != NULL) {
                    if ($value['salesInvoiceProductDiscountType'] == "precentage") {
                        $discountValue = $value['salesInvoiceProductPrice'] / 100 * $value['salesInvoiceProductDiscount'];
                    } else if ($value['salesInvoiceProductDiscountType'] == "value") {
                        $discountValue = $value['salesInvoiceProductDiscount'];
                    }
                }
                if ($discountValue > 0) {
                    $this->execute("UPDATE `itemOut` SET `itemOutDiscount` = '{$discountValue}' WHERE `itemOutDocumentID` = '{$invoiceID}' AND itemOutDocumentType = 'Sales Invoice';");
                }
            }
        }

//        //updates invoice discount value on itemout table
        $creditNoteRows = $this->fetchAll("SELECT * FROM  `creditNoteProduct`");
        foreach ($creditNoteRows as $value) {

            if ($value['creditNoteProductDiscountType'] == "precentage" || $value['creditNoteProductDiscountType'] == "value") {
                $creditNoteID = $value['creditNoteID'];

                //calculate discount value
                $discountValue = 0;
                if (isset($value['creditNoteProductDiscountType']) && $value['creditNoteProductDiscount'] != NULL) {
                    if ($value['creditNoteProductDiscountType'] == "precentage") {
                        $discountValue = $value['creditNoteProductPrice'] / 100 * $value['creditNoteProductDiscount'];
                    } else if ($value['creditNoteProductDiscountType'] == "value") {
                        $discountValue = $value['creditNoteProductDiscount'];
                    }
                }
                if ($discountValue > 0) {
                    $this->execute("UPDATE `itemIn` SET `itemInDiscount` = '{$discountValue}' WHERE `itemInDocumentID` = '{$creditNoteID}' AND itemInDocumentType = 'Credit Note';");
                }
            }
        }
        //updates invoice discount value on itemout table
        $deliveryNoteProductRows = $this->fetchAll("SELECT * FROM  `deliveryNoteProduct`");
        foreach ($deliveryNoteProductRows as $value) {

            if ($value['deliveryNoteProductDiscountType'] == "precentage" || $value['deliveryNoteProductDiscountType'] == "value") {
                $deliveryNoteID = $value['deliveryNoteID'];
                //calculate discount value
                $discountValue = 0;
                if (isset($value['deliveryNoteProductDiscountType']) && $value['deliveryNoteProductDiscount'] != NULL) {
                    if ($value['deliveryNoteProductDiscountType'] == "precentage") {
                        $discountValue = $value['deliveryNoteProductPrice'] / 100 * $value['deliveryNoteProductDiscount'];
                    } else if ($value['deliveryNoteProductDiscountType'] == "value") {
                        $discountValue = $value['deliveryNoteProductDiscount'];
                    }
                }
                if ($discountValue > 0) {
                    $this->execute("UPDATE `itemOut` SET `itemOutDiscount` = '{$discountValue}' WHERE `itemOutDocumentID` = '{$deliveryNoteID}' AND itemOutDocumentType = 'Delivery Note';");
                }
            }
        }
        //updates sales Return Product discount value on itemIn table
        $salesReturnProduct = $this->fetchAll("SELECT * FROM  `salesReturnProduct`");
        foreach ($salesReturnProduct as $value) {

            if ($value['salesReturnProductDiscountType'] == "precentage" || $value['salesReturnProductDiscountType'] == "value") {
                $salesReturnID = $value['salesReturnID'];
                //calculate discount value
                $discountValue = 0;
                if (isset($value['salesReturnProductDiscountType']) && $value['salesReturnProductDiscount'] != NULL) {
                    if ($value['salesReturnProductDiscountType'] == "precentage") {
                        $discountValue = $value['salesReturnProductPrice'] / 100 * $value['salesReturnProductDiscount'];
                    } else if ($value['salesReturnProductDiscountType'] == "value") {
                        $discountValue = $value['salesReturnProductDiscount'];
                    }
                }
                if ($discountValue > 0) {
                    $this->execute("UPDATE `itemIn` SET `itemInDiscount` = '{$discountValue}' WHERE `itemInDocumentID` = '{$salesReturnID}' AND itemInDocumentType = 'Sales Returns';");
                }
            }
        }
        //grn discount value
        $grnProduct = $this->fetchAll("SELECT * FROM  `grnProduct`");
        foreach ($grnProduct as $value) {

            if ($value['grnProductDiscount'] > 0) {
                $grnID = $value['grnID'];
                //calculate discount value
                $discountValue = 0;
                if (isset($value['grnProductPrice']) && $value['grnProductPrice'] != NULL) {
                    $discountValue = $value['grnProductPrice'] / 100 * $value['grnProductDiscount'];
                }
                if ($discountValue > 0) {
                    $this->execute("UPDATE `itemIn` SET `itemInDiscount` = '{$discountValue}' WHERE `itemInDocumentID` = '{$grnID}' AND itemInDocumentType = 'Goods Received Note';");
                }
            }
        }
        //updates sales Return Product discount value on itemIn table
        $debitNoteProduct = $this->fetchAll("SELECT * FROM  `debitNoteProduct`");
        foreach ($debitNoteProduct as $value) {

            if ($value['debitNoteProductDiscountType'] == "precentage" || $value['debitNoteProductDiscount'] == "value") {
                $debitNoteID = $value['debitNoteID'];
                //calculate discount value
                $discountValue = 0;
                if (isset($value['debitNoteProductDiscountType']) && $value['debitNoteProductDiscount'] != NULL) {
                    if ($value['debitNoteProductDiscountType'] == "precentage") {
                        $discountValue = $value['debitNoteProductPrice'] / 100 * $value['debitNoteProductDiscount'];
                    } else if ($value['debitNoteProductDiscountType'] == "value") {
                        $discountValue = $value['debitNoteProductDiscount'];
                    }
                }
                if ($discountValue > 0) {
                    $this->execute("UPDATE `itemOut` SET `itemOutDiscount` = '{$discountValue}' WHERE `itemOutDocumentID` = '{$debitNoteID}' AND itemOutDocumentType = 'Debit Note';");
                }
            }
        }
        //purchase return discount value
        $purchaseReturnProduct = $this->fetchAll("SELECT * FROM  `purchaseReturnProduct`");
        foreach ($purchaseReturnProduct as $value) {

            if ($value['purchaseReturnProductDiscount'] > 0) {
                $purchaseReturnID = $value['purchaseReturnID'];
                //calculate discount value
                $discountValue = 0;
                if (isset($value['purchaseReturnProductPrice']) && $value['purchaseReturnProductPrice'] != NULL) {
                    $discountValue = $value['purchaseReturnProductPrice'] / 100 * $value['purchaseReturnProductDiscount'];
                }
                if ($discountValue > 0) {
                    $this->execute("UPDATE `itemOut` SET `itemOutDiscount` = '{$discountValue}' WHERE `itemOutDocumentID` = '{$purchaseReturnID}' AND itemOutDocumentType = 'Purchase Return';");
                }
            }
        }
    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }

}
