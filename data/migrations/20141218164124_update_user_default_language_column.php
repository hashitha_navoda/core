<?php

use Phinx\Migration\AbstractMigration;

class UpdateUserDefaultLanguageColumn extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
    public function change()
    {
    }
    */
    
    /**
     * Migrate Up.
     */
    public function up()
    {
        $this->execute("UPDATE `user` SET `defaultLanguage`= 'en_US'");
    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }
}