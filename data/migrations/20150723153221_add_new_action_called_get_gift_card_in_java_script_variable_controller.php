<?php

use Phinx\Migration\AbstractMigration;

class AddNewActionCalledGetGiftCardInJavaScriptVariableController extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
      public function change()
      {
      }
     */

    /**
     * Migrate Up.
     */
    public function up()
    {

        $rows = $this->fetchAll("SELECT roleID FROM `role`");

        $this->execute("INSERT INTO `feature` (`featureID`, `featureName`, `featureController`, `featureAction`, `featureCategory`, `featureType`, `moduleID`) VALUES (NULL, 'Default', 'Core\\\Controller\\\JavascriptVariable', 'getGiftCard', 'Application', '1', '1');");
        $id1 = $this->fetchRow("select LAST_INSERT_ID()")[0];
        foreach ($rows as $row) {
            $roleID = $row['roleID'];
            if ($roleID == 1) {
                $enabled = 1;
            } else {
                $enabled = 0;
            }
            $this->execute("INSERT INTO `roleFeature` (`roleFeatureID`, `roleID`, `featureID`, `roleFeatureEnabled`) VALUES (NULL, $roleID, $id1,$enabled);");
        }
    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }

}
