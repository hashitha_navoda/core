<?php

use Phinx\Migration\AbstractMigration;

class AddTableChangesForActivity extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
      public function change()
      {
      }
     */

    /**
     * Migrate Up.
     */
    public function up()
    {
        $this->execute("SET SESSION old_alter_table=1; ALTER TABLE `activity` ADD `activityEstimateTime` DECIMAL( 12, 2 ) NOT NULL AFTER `activityEndTime`;");
        $this->execute("SET SESSION old_alter_table=1; ALTER TABLE `activity` ADD `entityID` INT( 11 ) NOT NULL AFTER `activitySortOrder`;");
        $this->execute("SET SESSION old_alter_table=1; ALTER TABLE `activity` ADD `activityName` VARCHAR( 200 ) NOT NULL AFTER `activityCode`;");
        $this->execute("ALTER TABLE `activityFixedAsset` CHANGE `activityFixedAssetID` `activityFixedAssetID` INT( 11 ) NOT NULL AUTO_INCREMENT;");
        $this->execute("ALTER TABLE `activity` CHANGE `activityId` `activityId` INT( 11 ) NOT NULL AUTO_INCREMENT;");
        $this->execute("ALTER TABLE `activityContractor` CHANGE `activityContractorId` `activityContractorId` INT( 11 ) NOT NULL AUTO_INCREMENT;");
        $this->execute("ALTER TABLE `activity` CHANGE `activityEstimateTime` `activityEstimateTime` DECIMAL( 12, 2 ) NULL;");
        $this->execute("ALTER TABLE `activityRawMaterial` CHANGE `activityRawMaterialQuantity` `activityRawMaterialQuantity` DECIMAL( 12, 2 ) NULL DEFAULT NULL;");
        $this->execute("ALTER TABLE `activityRawMaterial` CHANGE `activityRawMaterialId` `activityRawMaterialId` INT( 11 ) NOT NULL AUTO_INCREMENT;");
        $this->execute("ALTER TABLE `activitySerial` CHANGE `activityBatchID` `activityBatchID` INT( 11 ) NULL;");
        $this->execute("ALTER TABLE `temporaryProduct` CHANGE `temporaryProductSerial` `temporaryProductSerial` TINYINT( 1 ) NULL DEFAULT '0';");
        $this->execute("ALTER TABLE `temporaryProduct` CHANGE `temporaryProductBatch` `temporaryProductBatch` TINYINT( 1 ) NULL DEFAULT '0';");

        $this->execute("INSERT INTO `feature` (`featureID`, `featureName`, `featureController`, `featureAction`, `featureCategory`, `featureType`, `moduleID`) VALUES (NULL, 'Default', 'JobCard\\\Controller\\\API\\\TemporaryProduct', 'save', 'Temporary Product', '1', '8');");
        $id1 = $this->fetchRow("select LAST_INSERT_ID()")[0];
        $this->execute("INSERT INTO `roleFeature` (`roleFeatureID` ,`roleID` ,`featureID` ,`roleFeatureEnabled`)
                                VALUES (NULL , '1', $id1, '1');");

        $this->execute("INSERT INTO `feature` (`featureID`, `featureName`, `featureController`, `featureAction`, `featureCategory`, `featureType`, `moduleID`) VALUES (NULL, 'Default', 'Core\\\Controller\\\JavascriptVariable', 'temporaryProducts', 'Application', '1', '1');");
        $id2 = $this->fetchRow("select LAST_INSERT_ID()")[0];
        $this->execute("INSERT INTO `roleFeature` (`roleFeatureID` ,`roleID` ,`featureID` ,`roleFeatureEnabled`)
                                VALUES (NULL , '1', $id2, '1');");

        $this->execute("INSERT INTO `feature` (`featureID`, `featureName`, `featureController`, `featureAction`, `featureCategory`, `featureType`, `moduleID`) VALUES (NULL, 'Default', 'Core\\\Controller\\\JavascriptVariable', 'jobReference', 'Application', '1', '1');");
        $id3 = $this->fetchRow("select LAST_INSERT_ID()")[0];
        $this->execute("INSERT INTO `roleFeature` (`roleFeatureID` ,`roleID` ,`featureID` ,`roleFeatureEnabled`)
                                VALUES (NULL , '1', $id3, '1');");

        $this->execute("INSERT INTO `feature` (`featureID`, `featureName`, `featureController`, `featureAction`, `featureCategory`, `featureType`, `moduleID`) VALUES (NULL, 'Default', 'JobCard\\\Controller\\\API\\\Activity', 'save', 'Activity', '1', '8');");
        $id4 = $this->fetchRow("select LAST_INSERT_ID()")[0];
        $this->execute("INSERT INTO `roleFeature` (`roleFeatureID` ,`roleID` ,`featureID` ,`roleFeatureEnabled`)
                                VALUES (NULL , '1', $id4, '1');");
    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }

}
