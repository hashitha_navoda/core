<?php

use Phinx\Migration\AbstractMigration;

class AddFeatureToSalesPersonWiseOutStandingReport extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
      public function change()
      {
      }
     */

    /**
     * Migrate Up.
     */
    public function up()
    {
        $rows = $this->fetchAll("SELECT roleID FROM `role`");

        $this->execute("INSERT INTO `feature` (`featureID`, `featureName`, `featureController`, `featureAction`, `featureCategory`, `featureType`, `moduleID`) VALUES (NULL, 'View Report', 'Reporting\\\Controller\\\API\\\InvoiceReport', 'viewOutstandingBalance', 'Sales Person Wise Outstanding Balance', '0', '5');");
        $id1 = $this->fetchRow("select LAST_INSERT_ID()")[0];

        $this->execute("INSERT INTO `feature` (`featureID`, `featureName`, `featureController`, `featureAction`, `featureCategory`, `featureType`, `moduleID`) VALUES (NULL, 'Generate PDF', 'Reporting\\\Controller\\\API\\\InvoiceReport', 'generateOutstandingBalancePdf', 'Sales Person Wise Outstanding Balance', '0', '5');");
        $id2 = $this->fetchRow("select LAST_INSERT_ID()")[0];

        $this->execute("INSERT INTO `feature` (`featureID`, `featureName`, `featureController`, `featureAction`, `featureCategory`, `featureType`, `moduleID`) VALUES (NULL, 'Generate Sheet', 'Reporting\\\Controller\\\API\\\InvoiceReport', 'generateOutstandingBalanceSheet', 'Sales Person Wise Outstanding Balance', '0', '5');");
        $id3 = $this->fetchRow("select LAST_INSERT_ID()")[0];

        foreach ($rows as $row) {
            $roleID = $row['roleID'];
            $enabled = 1;
            $this->execute("INSERT INTO `roleFeature` (`roleFeatureID`, `roleID`, `featureID`, `roleFeatureEnabled`) VALUES (NULL, $roleID, $id1,$enabled);");
            $this->execute("INSERT INTO `roleFeature` (`roleFeatureID`, `roleID`, `featureID`, `roleFeatureEnabled`) VALUES (NULL, $roleID, $id2,$enabled);");
            $this->execute("INSERT INTO `roleFeature` (`roleFeatureID`, `roleID`, `featureID`, `roleFeatureEnabled`) VALUES (NULL, $roleID, $id3,$enabled);");
        }
    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }

}
