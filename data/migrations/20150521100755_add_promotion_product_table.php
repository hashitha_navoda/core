<?php

use Phinx\Migration\AbstractMigration;

class AddPromotionProductTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
      public function change()
      {
      }
     */

    /**
     * Migrate Up.
     */
    public function up()
    {
        $this->execute("CREATE TABLE IF NOT EXISTS `promotionProduct` (
  `promotionProductID` int(11) NOT NULL AUTO_INCREMENT,
  `promotionProductPromotionID` int(11) NOT NULL,
  `promotionProductLocationProductID` int(11) NOT NULL,
  `promotionProductMinQty` float NOT NULL,
  `promotionProductMaxQty` float NOT NULL,
  `promotionProductDiscountType` int(11) NOT NULL,
  `promotionProductDiscountAmount` decimal(12,2) NOT NULL,
  PRIMARY KEY (`promotionProductID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;");
    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }

}
