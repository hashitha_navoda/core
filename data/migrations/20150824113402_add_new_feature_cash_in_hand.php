<?php

use Phinx\Migration\AbstractMigration;

class AddNewFeatureCashInHand extends AbstractMigration
{

    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     */
//    public function change()
//    {
//
//    }

    public function up()
    {
        $this->execute("SET SESSION old_alter_table=1; ALTER TABLE  `location` ADD  `locationCashInHand` DECIMAL( 20, 5 ) NULL DEFAULT  '0.00000' AFTER  `locationStatus` ; ");
        $this->execute("SET SESSION old_alter_table=1; ALTER TABLE  `pettyCashFloat` CHANGE  `accountID`  `accountID` INT( 11 ) NULL ; ");
        $this->execute("SET SESSION old_alter_table=1; ALTER TABLE  `pettyCashFloat` ADD  `cashType` INT NOT NULL AFTER  `accountID` ; ");
        $this->execute("UPDATE `pettyCashFloat` SET `cashType`= 2 ;");
    }

}
