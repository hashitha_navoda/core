<?php

use Phinx\Migration\AbstractMigration;

class AddNewTableCalledPurchaseRequisitionTypeLimitTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->hasTable('purchaseRequisitionTypeLimit');

        if (!$table) {
            $table = $this->table('purchaseRequisitionTypeLimit', ['id' => 'purchaseRequisitionTypeLimitId']);
            $table->addColumn('purchaseRequisitionTypeId', 'integer', ['limit' => 20]);
            $table->addColumn('purchaseRequisitionTypeLimitMin', 'decimal', ['precision' => 20, 'scale' => 5]);
            $table->addColumn('purchaseRequisitionTypeLimitMax', 'decimal', ['precision' => 20, 'scale' => 5]);
            $table->save();
        }
    }
}
