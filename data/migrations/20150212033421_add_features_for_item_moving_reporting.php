<?php

use Phinx\Migration\AbstractMigration;

class AddFeaturesForItemMovingReporting extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
      public function change()
      {
      }
     */

    /**
     * Migrate Up.
     */
    public function up()
    {
        $this->execute("UPDATE `itemIn` SET `itemInDocumentType` = 'Inventory Positive Adjustment' WHERE `itemInDocumentType` = 'Inventory Adjustment';");
        $this->execute("UPDATE `itemOut` SET `itemOutDocumentType` = 'Inventory Negative Adjustment' WHERE `itemOutDocumentType` = 'Inventory Adjustment';");

        $this->execute("INSERT INTO `feature`(`featureID`, `featureName`, `featureController`, `featureAction`, `featureCategory`, `featureType`, `moduleID`) VALUES (NULL, 'View Report', 'Reporting\\\Controller\\\API\\\StockInHandReport', 'viewGlobalItemMoving', 'Item Moving', '0', '5');");
        $id1 = $this->fetchRow("select LAST_INSERT_ID()")[0];
        $this->execute("INSERT INTO `roleFeature` (`roleFeatureID` ,`roleID` ,`featureID` ,`roleFeatureEnabled`)
                                VALUES (NULL , '1', $id1, '1');");
        $this->execute("INSERT INTO `feature`(`featureID`, `featureName`, `featureController`, `featureAction`, `featureCategory`, `featureType`, `moduleID`) VALUES (NULL, 'Generate PDF', 'Reporting\\\Controller\\\API\\\StockInHandReport', 'generateGlobalItemMovingPdf', 'Item Moving', '0', '5');");
        $id2 = $this->fetchRow("select LAST_INSERT_ID()")[0];
        $this->execute("INSERT INTO `roleFeature` (`roleFeatureID` ,`roleID` ,`featureID` ,`roleFeatureEnabled`)
                                VALUES (NULL , '1', $id2, '1');");
        $this->execute("INSERT INTO `feature`(`featureID`, `featureName`, `featureController`, `featureAction`, `featureCategory`, `featureType`, `moduleID`) VALUES (NULL, 'Generate PDF', 'Reporting\\\Controller\\\API\\\StockInHandReport', 'generateGlobalItemMovingSheet', 'Item Moving', '0', '5');");
        $id3 = $this->fetchRow("select LAST_INSERT_ID()")[0];
        $this->execute("INSERT INTO `roleFeature` (`roleFeatureID` ,`roleID` ,`featureID` ,`roleFeatureEnabled`)
                                VALUES (NULL , '1', $id3, '1');");
    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }

}
