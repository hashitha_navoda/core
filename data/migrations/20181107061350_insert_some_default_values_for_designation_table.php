<?php

use Phinx\Migration\AbstractMigration;

class InsertSomeDefaultValuesForDesignationTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {

        $pdo = $this->getAdapter()->getConnection();

        $rows = $this->fetchAll("SELECT userID FROM `user` WHERE roleID = 1");
        $userID = $rows[0]['userID'];         
        $timeStamp = date( "Y-m-d H:i:s"); 
        
        $dataArray = array(
            array(
                'designationCode' => 'DES001',
                'designationName' => 'Project Manager',
                'designationStatus' => 1
            ),
            array(
                'designationCode' => 'DES002',
                'designationName' => 'Project Supervisor',
                'designationStatus' => 1
            ),
            array(
                'designationCode' => 'DES003',
                'designationName' => 'Job Manager',
                'designationStatus' => 1
            ),
            array(
                'designationCode' => 'DES004',
                'designationName' => 'Job Supervisor',
                'designationStatus' => 1
            ),

        );

        foreach($dataArray as $designation){

            $entityInsert = <<<SQL
                INSERT INTO `entity` 
                (
                    `createdBy`, 
                    `createdTimeStamp`, 
                    `updatedBy`, 
                    `updatedTimeStamp`, 
                    `deleted`, 
                    `deletedBy`,
                    `deletedTimeStamp`
                ) VALUES (
                    :createdBy, 
                    :createdTimeStamp, 
                    :updatedBy, 
                    :updatedTimeStamp, 
                    :deleted, 
                    :deletedBy,
                    :deletedTimeStamp
                );
    
SQL;

            $pdo->prepare($entityInsert)->execute(array(
                'createdBy' => $userID, 
                'createdTimeStamp' => $timeStamp, 
                'updatedBy' => $userID, 
                'updatedTimeStamp' =>  $timeStamp, 
                'deleted' => 0, 
                'deletedBy' => NULL,
                'deletedTimeStamp' => NULL
            ));

            $entityID = $this->fetchRow("select LAST_INSERT_ID()")[0];
            
            $designationInsert = <<<SQL
                INSERT INTO `designation` 
                ( 
                    `designationCode`, 
                    `designationName`, 
                    `entityID`,
                    `designationStatus`
                ) VALUES (
                    :designationCode, 
                    :designationName,  
                    :entityID,
                    :designationStatus
                );
    
SQL;
    
            $pdo->prepare($designationInsert)->execute(array( 
                'designationCode' => $designation['designationCode'], 
                'designationName' => $designation['designationName'], 
                'entityID' => $entityID,
                'designationStatus' => $designation['designationStatus'] 
            ));

        }


    }
}
