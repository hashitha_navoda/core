<?php

use Phinx\Migration\AbstractMigration;

class FindAndReplaceTemplateTableImgWidthAndHight extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
      public function change()
      {
      }
     */

    /**
     * Migrate Up.
     */
    public function up()
    {
        $sql = <<<EOF
        UPDATE `template` SET `templateContent` = replace( templateContent, 'style="background: #e0e0e0; width: 75px; height: 75px; display: block; text-align: center;"', 'style="background: #e0e0e0; max-height: 75px; display: block; text-align: center;"' );
EOF;
        $this->execute($sql);
    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }

}
