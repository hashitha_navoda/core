<?php

use Phinx\Migration\AbstractMigration;

class AddColumnsToTheJobTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
      public function change()
      {
      }
     */

    /**
     * Migrate Up.
     */
    public function up()
    {
        $this->execute("ALTER TABLE `job` ADD `customerProfileID` INT(11) NOT NULL;");
        $this->execute("ALTER TABLE `job` ADD `jobProgress` DECIMAL( 3, 2 ) NULL;");
    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }

}
