<?php

use Phinx\Migration\AbstractMigration;

class AddPromotionTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
      public function change()
      {
      }
     */

    /**
     * Migrate Up.
     */
    public function up()
    {
        $this->execute("CREATE TABLE IF NOT EXISTS `promotion` (
  `promotionID` int(11) NOT NULL AUTO_INCREMENT,
  `promotionName` varchar(255) NOT NULL,
  `promotionFromDate` date NOT NULL,
  `promotionToDate` date NOT NULL,
  `promotionType` int(11) NOT NULL,
  `promotionLocation` int(11) DEFAULT NULL,
  `promotionStatus` int(11) DEFAULT NULL,
  `promotionDescription` text,
  `entityID` int(11) NOT NULL,
  PRIMARY KEY (`promotionID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;");
    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }

}
