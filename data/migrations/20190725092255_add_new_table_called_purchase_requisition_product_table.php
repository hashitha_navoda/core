<?php

use Phinx\Migration\AbstractMigration;

class AddNewTableCalledPurchaseRequisitionProductTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->hasTable('purchaseRequisitionProduct');

        if (!$table) {
            $table = $this->table('purchaseRequisitionProduct', ['id' => 'purchaseRequisitionProductID']);
            $table->addColumn('purchaseRequisitionID', 'integer', ['limit' => 20]);
            $table->addColumn('locationProductID', 'integer', ['limit' => 20]);
            $table->addColumn('purchaseRequisitionProductQuantity', 'decimal', ['precision' => 20, 'scale' => 5,'default' => null, 'null' => true]);
            $table->addColumn('purchaseRequisitionProductPrice', 'decimal', ['precision' => 20, 'scale' => 10,'default' => null, 'null' => true]);
            $table->addColumn('purchaseRequisitionProductDiscount', 'float', ['default' => 0, 'null' => true]);
            $table->addColumn('purchaseRequisitionProductTotal', 'decimal', ['precision' => 20, 'scale' => 10,'default' => null, 'null' => true]);
            $table->addColumn('copied', 'boolean', ['default' => 0]);
            $table->addColumn('purchaseRequisitionProductCopiedQuantity', 'decimal', ['precision' => 20, 'scale' => 5,'default' => null, 'null' => true]);
            $table->save();
        }
    }
}
