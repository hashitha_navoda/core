<?php

use Phinx\Migration\AbstractMigration;

class AddNewTwoColumnsToOutGoingPaymentTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->table('outgoingPayment');
        $column = $table->hasColumn('outgoingPaymentPaidAmount');
        $column1 = $table->hasColumn('outgoingPaymentBalanceAmount');

        if (!$column) {
            $table->addColumn('outgoingPaymentPaidAmount', 'decimal', ['precision' => 20, 'scale' => 5, 'default' => null, 'null' => true])
              ->update();
        }

        if (!$column1) {
            $table->addColumn('outgoingPaymentBalanceAmount', 'decimal', ['precision' => 20, 'scale' => 5, 'default' => null, 'null' => true])
              ->update();
        }
    }
}
