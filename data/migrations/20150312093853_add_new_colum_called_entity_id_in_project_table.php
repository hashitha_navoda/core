<?php

use Phinx\Migration\AbstractMigration;

class AddNewColumCalledEntityIdInProjectTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
      public function change()
      {
      }
     */

    /**
     * Migrate Up.
     */
    public function up()
    {
        $this->execute("SET SESSION old_alter_table=1;ALTER TABLE `project` ADD `entityID` INT( 11 ) NOT NULL AFTER `projectProgress` ,
ADD UNIQUE (
`entityID`
);");
    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }

}
