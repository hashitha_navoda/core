<?php

use Phinx\Migration\AbstractMigration;

class AddReferenceForPoDraft extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $addRow = $this->execute("INSERT INTO `reference` (`referenceNameID`, `referenceName`, `referenceTypeID`) VALUES ('32', 'PODRFT', '0');");

        $addRowToReferencePrefix = $this->execute("INSERT INTO `referencePrefix` (`referencePrefixID`,`referenceNameID`, `referencePrefixCHAR`, `referencePrefixNumberOfDigits`, `referencePrefixCurrentReference`) VALUES ('NULL','32', 'PODFT', '4', '1')");

    }
}
