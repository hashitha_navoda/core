<?php

use Phinx\Migration\AbstractMigration;

class AddIdToInvoiceIssueDateAndTime extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
    public function change()
    {
    }
    */

    /**
     * Migrate Up.
     */
    public function up()
    {
        $sql = 'update template SET templateContent = REPLACE(templateContent,
                    "<abbr contenteditable=\"false\" spellcheck=\"false\">[Sales Inv. Issue Date]</abbr>",
                    "<abbr contenteditable=\"false\" spellcheck=\"false\" id=\"date\">[Sales Inv. Issue Date]</abbr>")
                    WHERE documentSizeID=3;';
        $this->execute($sql);

        $sql1 = 'update template SET templateContent = REPLACE(templateContent,
                    "<abbr contenteditable=\"false\" spellcheck=\"false\">[Time]</abbr>",
                    "<abbr contenteditable=\"false\" spellcheck=\"false\" id=\"time\">[Time]</abbr>")
                    WHERE documentSizeID=3;';
        $this->execute($sql1);
    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }
}