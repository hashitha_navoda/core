<?php

use Phinx\Migration\AbstractMigration;

class AddNewTableCalledBudgetDetails extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->hasTable('budgetDetails');

        if (!$table) {
            $table = $this->table('budgetDetails', ['id' => 'budgetDetailsId']);
            $table->addColumn('budgetId', 'integer', ['limit' => 11]);
            $table->addColumn('fiscalPeriodID', 'integer', ['limit' => 11]);
            $table->addColumn('financeAccountID', 'integer', ['limit' => 11]);
            $table->addColumn('budgetValue', 'decimal',['default' => null, 'null' => true, 'precision' => 20, 'scale' => 5]);
            $table->save();
        }
    }
}
