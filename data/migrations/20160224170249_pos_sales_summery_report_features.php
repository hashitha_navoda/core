<?php

use Phinx\Migration\AbstractMigration;

class PosSalesSummeryReportFeatures extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
    public function change()
    {
    }
    */
    
    /**
     * Migrate Up.
     */
    public function up()
    {
        $pdo = $this->getAdapter()->getConnection();
        $roles = $this->fetchAll("SELECT roleID FROM `role`"); //role list
        
        //feature list
        $featureList = array(
            array(
                'name' =>'View Report',
                'controller'=>'Reporting\Controller\API\PosReport',
                'action'=>'posSalesSummeryReport',
                'category'=>'Pos Sales Summery Report',
                'type'=>'0',
                'moduleId'=>'6'
            ),
            array(
                'name' =>'Generate PDF',
                'controller'=>'Reporting\Controller\API\PosReport',
                'action'=>'posSalesSummeryPdf',
                'category'=>'Pos Sales Summery Report',
                'type'=>'0',
                'moduleId'=>'6'
            ),
            array(
                'name' =>'Generate CSV',
                'controller'=>'Reporting\Controller\API\PosReport',
                'action'=>'posSalesSummeryCsv',
                'category'=>'Pos Sales Summery Report',
                'type'=>'0',
                'moduleId'=>'6'
            )
        );
        
        foreach ($featureList as $feature){
            //insert feature to feature table
            $featureInsert = <<<SQL
                INSERT INTO `feature` 
                    (
                        `featureName`, 
                        `featureController`, 
                        `featureAction`, 
                        `featureCategory`, 
                        `featureType`, 
                        `moduleID`
                    ) VALUES (
                        :featureName, 
                        :featureController, 
                        :featureAction, 
                        :featureCategory, 
                        :featureType, 
                        :featureModuleId
                    );
SQL;
            $pdo->prepare($featureInsert)->execute(array(
                'featureName' => $feature['name'], 
                'featureController' => $feature['controller'], 
                'featureAction' => $feature['action'], 
                'featureCategory' => $feature['category'], 
                'featureType' => $feature['type'], 
                'featureModuleId' => $feature['moduleId']
            ));
            
            $featureID = $this->fetchRow("select LAST_INSERT_ID()")[0];
            
            foreach ($roles as $role) {
                $roleID = $role['roleID'];
                if ($roleID == 1) {
                    $enabled = 1;
                } else {
                    //for enable api calls for every role
                    if($feature['type'] == 1){
                        $enabled = 1;
                    } else {
                        $enabled = 0;
                    }
                }
                //insert role feature to role feature table
                $roleFeatureInsert = <<<SQL
                INSERT INTO `roleFeature` 
                    (
                        `roleID`, 
                        `featureID`, 
                        `roleFeatureEnabled`
                    ) VALUES (
                        :roleID, 
                        :featureID, 
                        :roleFeatureEnabled
                    );
SQL;
                $pdo->prepare($roleFeatureInsert)->execute(array(
                    'roleID' => $roleID, 
                    'featureID' => $featureID, 
                    'roleFeatureEnabled' => $enabled
                ));                
            }
        }
    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }
}