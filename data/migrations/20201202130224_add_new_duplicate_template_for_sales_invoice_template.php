<?php

use Phinx\Migration\AbstractMigration;

class AddNewDuplicateTemplateForSalesInvoiceTemplate extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $html = <<<HTML
        <table class="table table-bordered" valign="top" width="100%">
            <tbody>
                <tr>
                    <td><img style="background: #e0e0e0; max-height: 75px; display: block; text-align: center;" src="[Company Logo]" alt="Company Logo" class="logo"><span style="font-weight: bold;"><abbr spellcheck="false" contenteditable="false">[Company Name]</abbr> </span><br><abbr spellcheck="false" contenteditable="false">[Company Address]</abbr> <br>Tel: <abbr spellcheck="false" contenteditable="false">[Company Tel. No]</abbr> <br>Email: <abbr spellcheck="false" contenteditable="false">[Company Email]</abbr> <br>Tax No: <abbr spellcheck="false" contenteditable="false">[Company Tax Reg. No]</abbr>&nbsp;</td><td><h2 align="right">Invoice</h2></td>
                </tr>
                <tr>
                    <td>
                        <div class="add_block">
                            <hr>
                        </div>
                    </td>
                    <td class="relative_data" rowspan="2" align="right">
                        <table class="table table-bordered table-condensed" width="100%">
                            <tbody><tr><td>Date: <br></td><td style="text-align: right;"><abbr spellcheck="false" contenteditable="false">[Sales Inv. Issue Date]</abbr><br></td></tr>
                                <tr><td>Valid Till: <br></td><td style="text-align: right;"><abbr spellcheck="false" contenteditable="false">[Sales Inv. Overdue Date]</abbr><br></td></tr>
                                <tr><td style="vertical-align: top;">Payment Term: <br></td><td style="vertical-align: top; text-align: right;"><abbr spellcheck="false" contenteditable="false">[Sales Inv. Payment Term]</abbr><br></td></tr><tr><td>Invoice No: <br></td><td style="text-align: right;"><abbr spellcheck="false" contenteditable="false">[Sales Inv. Code]</abbr><br></td></tr>
                            </tbody></table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="add_block">
                            <div id="cust_name"><strong><abbr spellcheck="false" contenteditable="false">[Customer Name]</abbr>&nbsp;</strong></div>
                        </div>
                    </td>
                </tr>
            </tbody>
        </table>
        <noneditabletable>
            <table class="noneditable table table-bordered" style="width: 100%; margin-bottom: 100px;" data-original-title="" title="" contenteditable="false">
                <tbody>
                    <tr>
                        <td colspan="2">
                            <div class="qout_body">
                                <hr>
                                <table class="table table-bordered table-striped" id="item_tbl" width="100%">
                                    <colgroup><col width="10px"><col width="90px"><col width="290px"><col width="50px"><col width="100px"><col width="100px">                                        </colgroup><thead>
                                        <tr>
                                            <th colspan="2">Product code</th><th colspan="1">Product</th><th colspan="1">Qty</th><th colspan="1">Price</th><th colspan="1">Total</th>                                            </tr>
                                    </thead>
                                    <tbody>
                                        <tr><td>1</td><td align="left">12</td><td align="left">dell_lap<br><small>(Disc. 1.00)</small></td><td align="left">1</td><td align="right">100.00</td><td align="right">99.00</td></tr>                                        </tbody>
                                </table>

                                <div class="summary">
                                    <br>
                                    <br>
                                    <br>
                                    <br>
                                    <table id="" class="table" width="100%">
                                        <tbody><tr>
                                                <td colspan="2" rowspan="3">
                                                </td>
                                                <td colspan="3">Sub Total</td>
                                                <td class="text-right">99.00</td>
                                            </tr>
                                            <tr>
                                                <td colspan="3"><h3>Total</h3></td>
                                                <td class="text-right"><strong><h3>99.00</h3></strong></td>
                                            </tr>
            <!--                                <tr>
                                                <td colspan="6"><div></div></td>
                                            </tr>-->
                                        </tbody></table>
                                </div>
                            </div>
                        </td>
                    </tr>
                    
                </tbody>
            </table>
        </noneditabletable>
        <p><abbr contenteditable="false" spellcheck="false">[Payment Table]</abbr>&nbsp;&nbsp;</p>

<font style="font-size: 12px;"><footer class="doc"><table class="table table-bordered" width="100%"><tbody><tr><td><abbr spellcheck="false" contenteditable="false">[Company Name]</abbr>&nbsp;<br></td><td><br></td><td style="text-align: right;"><abbr spellcheck="false" contenteditable="false">[Sales Inv. Issue Date]</abbr>&nbsp;<br></td></tr></tbody></table></footer></font>
HTML;

        $this->execute("INSERT IGNORE INTO `template` (`templateID`, `templateName`, `documentTypeID`, `documentSizeID`, `templateContent`, `templateFooterDetails`, `templateFooterShow`, `templateDefault`, `templateSample`) VALUES (NULL, 'Template (with payment view)', '1', '1', '{$html}', '', '0', '0', '0');");
    }
}
