<?php

use Phinx\Migration\AbstractMigration;

class AddNewTableCalledCustomerOrder extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $customerOrder = $this->hasTable('customerOrders');
        if (!$customerOrder) {
            $newTable = $this->table('customerOrders', ['id' => 'customerOrderID']);
            $newTable->addColumn('customerOrderCode', 'string', ['limit' => 200])
                    ->addColumn('customerID', 'integer', ['limit' => 20])
                    ->addColumn('customerOrderDate', 'date', ['default' => null, 'null' => true])
                    ->addColumn('customerOrderAmount', 'decimal', ['precision' => 20, 'scale' => 5, 'default' => null, 'null' => true])
                    ->addColumn('customerOrderComment', 'text', ['default' => null, 'null' => true])
                    ->addColumn('customerOrderReference', 'text', ['default' => null, 'null' => true])
                    ->addColumn('customerOrderStatus', 'integer', ['limit' => 20,'default' => 3])
                    ->addColumn('locationId', 'integer', ['limit' => 20])
                    ->addColumn('entityID', 'integer', ['limit' => 20,'default' => null, 'null' => true])
                    ->save();
        }
    }
}
