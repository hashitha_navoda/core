<?php

use Phinx\Migration\AbstractMigration;

class AddActionForSearchBomAndBomAssemblyDetails extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
      public function change()
      {
      }
     */

    /**
     * Migrate Up.
     */
    public function up()
    {
        $rows = $this->fetchAll("SELECT roleID FROM `role`");
        $dataArray[1] = array('featureID' => 'NULL', 'featureName' => 'Default', 'featureController' => 'Inventory\\\Controller\\\API\\\Bom', 'featureAction' => 'getBomBySearchKey', 'featureCategory' => 'Application', 'featureType' => '1', 'moduleID' => '2');
        $dataArray[2] = array('featureID' => 'NULL', 'featureName' => 'Default', 'featureController' => 'Inventory\\\Controller\\\API\\\Bom', 'featureAction' => 'getBomAdBySearchKey', 'featureCategory' => 'Application', 'featureType' => '1', 'moduleID' => '2');

        for ($i = 1; $i < 3; $i++) {
            $featureName = $dataArray[$i]['featureName'];
            $featureController = $dataArray[$i]['featureController'];
            $featureAction = $dataArray[$i]['featureAction'];
            $featureCategory = $dataArray[$i]['featureCategory'];
            $featureType = $dataArray[$i]['featureType'];
            $moduleID = $dataArray[$i]['moduleID'];

            $this->execute("INSERT INTO `feature` (`featureID`, `featureName`, `featureController`, `featureAction`, `featureCategory`, `featureType`, `moduleID`) VALUES (NULL, '$featureName', '$featureController', '$featureAction', '$featureCategory', '$featureType', '$moduleID');");
            $id = $this->fetchRow("select LAST_INSERT_ID()")[0];

            foreach ($rows as $row) {
                $roleID = $row['roleID'];
                $enabled = 1;
                $this->execute("INSERT INTO `roleFeature` (`roleFeatureID`, `roleID`, `featureID`, `roleFeatureEnabled`) VALUES (NULL, $roleID, $id,$enabled);");
            }
        }
    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }

}
