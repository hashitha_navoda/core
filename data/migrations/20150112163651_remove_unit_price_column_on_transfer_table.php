<?php

use Phinx\Migration\AbstractMigration;

class RemoveUnitPriceColumnOnTransferTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
      public function change()
      {
      }
     */

    /**
     * Migrate Up.
     */
    public function up()
    {
        $this->execute("SET SESSION OLD_ALTER_TABLE=1;ALTER TABLE `transferProduct` DROP `transferUnitPrice`;");
    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }

}
