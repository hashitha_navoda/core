<?php

use Phinx\Migration\AbstractMigration;

class UpdateFeatureNameInFeatureTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
      public function change()
      {
      }
     */

    /**
     * Migrate Up.
     */
    public function up()
    {
        $this->execute("UPDATE `feature` SET `featureName`='Import' WHERE `featureController`='Inventory\\\Controller\\\Product' AND `featureAction`='import';");
        $this->execute("UPDATE `feature` SET `featureCategory`='Items' WHERE `featureController`='Inventory\\\Controller\\\Product' AND `featureAction`='import';");
        $this->execute("UPDATE `feature` SET `featureCategory`='Returns' WHERE `featureController`='Inventory\\\Controller\\\PurchaseReturnsController' AND `featureAction`='list';");
    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }

}
