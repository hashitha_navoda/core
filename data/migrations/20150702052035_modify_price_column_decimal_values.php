<?php

use Phinx\Migration\AbstractMigration;

class ModifyPriceColumnDecimalValues extends AbstractMigration
{

    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     */
    public function change()
    {
        $sql = <<<SQL
                SET SESSION old_alter_table=1;
                ALTER TABLE  `activity` CHANGE  `activityEstimatedCost`  `activityEstimatedCost` DECIMAL( 20, 5 ) NULL DEFAULT NULL ;
                ALTER TABLE  `activityContractor` CHANGE  `activityContractorCost`  `activityContractorCost` DECIMAL( 20, 5 ) NULL DEFAULT NULL ;
                ALTER TABLE  `activityCostType` CHANGE  `activityCostTypeEstimatedCost`  `activityCostTypeEstimatedCost` DECIMAL( 20, 5 ) NULL DEFAULT NULL ;
                ALTER TABLE  `activityCostType` CHANGE  `activityCostTypeActualCost`  `activityCostTypeActualCost` DECIMAL( 20, 5 ) NULL DEFAULT NULL ;
                ALTER TABLE  `activityFixedAsset` CHANGE  `activityFixedAssetCost`  `activityFixedAssetCost` DECIMAL( 20, 5 ) NULL DEFAULT NULL ;
                ALTER TABLE  `activityRawMaterial` CHANGE  `activityRawMaterialCost`  `activityRawMaterialCost` DECIMAL( 20, 5 ) NULL DEFAULT NULL ;
                ALTER TABLE  `assemble` CHANGE  `assembleUnitPrice`  `assembleUnitPrice` DECIMAL( 20, 5 ) NULL DEFAULT NULL ;
                ALTER TABLE  `costType` CHANGE  `costTypeMinimumValue`  `costTypeMinimumValue` DECIMAL( 20, 5 ) NULL DEFAULT  '0.00';
                ALTER TABLE  `costType` CHANGE  `costTypeMaximumValue`  `costTypeMaximumValue` DECIMAL( 20, 5 ) NULL DEFAULT  '0.00';
                ALTER TABLE  `creditNote` CHANGE  `creditNotePaymentAmount`  `creditNotePaymentAmount` DECIMAL( 20, 5 ) NULL DEFAULT NULL ;
                ALTER TABLE  `creditNote` CHANGE  `creditNoteSettledAmount`  `creditNoteSettledAmount` DECIMAL( 20, 5 ) NULL DEFAULT  '0.00';
                ALTER TABLE  `creditNotePayment` CHANGE  `creditNotePaymentAmount`  `creditNotePaymentAmount` DECIMAL( 20, 5 ) NOT NULL ;
                ALTER TABLE  `creditNoteProduct` CHANGE  `creditNoteProductPrice`  `creditNoteProductPrice` DECIMAL( 20, 5 ) NULL DEFAULT NULL ;
                ALTER TABLE  `creditNoteProduct` CHANGE  `creditNoteProductDiscount`  `creditNoteProductDiscount` DECIMAL( 20, 5 ) NULL DEFAULT NULL ;
                ALTER TABLE  `creditNoteProduct` CHANGE  `creditNoteProductTotal`  `creditNoteProductTotal` DECIMAL( 20, 5 ) NOT NULL ;
                ALTER TABLE  `creditNoteProductTax` CHANGE  `creditNoteTaxAmount`  `creditNoteTaxAmount` DECIMAL( 20, 5 ) NULL DEFAULT NULL ;
                ALTER TABLE  `customer` CHANGE  `customerCreditLimit`  `customerCreditLimit` DECIMAL( 20, 5 ) NULL DEFAULT NULL ;
                ALTER TABLE  `customer` CHANGE  `customerCurrentBalance`  `customerCurrentBalance` DECIMAL( 20, 5 ) NULL DEFAULT NULL ;
                ALTER TABLE  `customer` CHANGE  `customerCurrentCredit`  `customerCurrentCredit` DECIMAL( 20, 5 ) NULL DEFAULT NULL ;
                ALTER TABLE  `debitNote` CHANGE  `debitNoteTotal`  `debitNoteTotal` DECIMAL( 20, 5 ) NULL DEFAULT  '0.00';
                ALTER TABLE  `debitNote` CHANGE  `debitNotePaymentAmount`  `debitNotePaymentAmount` DECIMAL( 20, 5 ) NULL DEFAULT  '0.00';
                ALTER TABLE  `debitNote` CHANGE  `debitNoteSettledAmount`  `debitNoteSettledAmount` DECIMAL( 20, 5 ) NULL DEFAULT  '0.00';
                ALTER TABLE  `debitNotePayment` CHANGE  `debitNotePaymentAmount`  `debitNotePaymentAmount` DECIMAL( 20, 5 ) NOT NULL DEFAULT  '0.00';
                ALTER TABLE  `debitNotePayment` CHANGE  `debitNotePaymentDiscount`  `debitNotePaymentDiscount` DECIMAL( 20, 5 ) NULL DEFAULT  '0.00';
                ALTER TABLE  `debitNotePaymentDetails` CHANGE  `debitNotePaymentDetailsAmount`  `debitNotePaymentDetailsAmount` DECIMAL( 20, 5 ) NOT NULL DEFAULT  '0.00';
                ALTER TABLE  `debitNoteProduct` CHANGE  `debitNoteProductPrice`  `debitNoteProductPrice` DECIMAL( 20, 5 ) NULL DEFAULT  '0.00';
                ALTER TABLE  `debitNoteProduct` CHANGE  `debitNoteProductDiscount`  `debitNoteProductDiscount` DECIMAL( 20, 5 ) NULL DEFAULT '0.00';
                ALTER TABLE  `debitNoteProduct` CHANGE  `debitNoteProductTotal`  `debitNoteProductTotal` DECIMAL( 20, 5 ) NULL DEFAULT  '0.00';
                ALTER TABLE  `debitNoteProductTax` CHANGE  `debitNoteProductTaxPercentage`  `debitNoteProductTaxPercentage` DECIMAL( 20, 5 ) NULL DEFAULT  '0.00';
                ALTER TABLE  `debitNoteProductTax` CHANGE  `debitNoteProductTaxAmount`  `debitNoteProductTaxAmount` DECIMAL( 20, 5 ) NULL DEFAULT  '0.00';
                ALTER TABLE  `deliveryNote` CHANGE  `deliveryNoteCharge`  `deliveryNoteCharge` DECIMAL( 20, 5 ) NULL DEFAULT NULL ;
                ALTER TABLE  `deliveryNote` CHANGE  `deliveryNotePriceTotal`  `deliveryNotePriceTotal` DECIMAL( 20, 5 ) NULL DEFAULT NULL ;
                ALTER TABLE  `deliveryNoteProduct` CHANGE  `deliveryNoteProductPrice`  `deliveryNoteProductPrice` DECIMAL( 20, 5 ) NULL DEFAULT '0.00';
                ALTER TABLE  `deliveryNoteProduct` CHANGE  `deliveryNoteProductDiscount`  `deliveryNoteProductDiscount` DECIMAL( 20, 5 ) NULL DEFAULT NULL ;
                ALTER TABLE  `deliveryNoteProduct` CHANGE  `deliveryNoteProductTotal`  `deliveryNoteProductTotal` DECIMAL( 20, 5 ) NULL DEFAULT '0.00';
                ALTER TABLE  `deliveryNoteProductTax` CHANGE  `deliveryNoteTaxAmount`  `deliveryNoteTaxAmount` DECIMAL( 20, 5 ) NULL DEFAULT NULL ;
                ALTER TABLE  `disassembleProduct` CHANGE  `disassembleProductPrice`  `disassembleProductPrice` DECIMAL( 20, 5 ) NULL DEFAULT NULL ;
                ALTER TABLE  `goodsIssueProduct` CHANGE  `unitOfPrice`  `unitOfPrice` DECIMAL( 20, 5 ) NULL DEFAULT NULL ;
                ALTER TABLE  `goodsReceiptProduct` CHANGE  `goodsReceiptProductPrice`  `goodsReceiptProductPrice` DECIMAL( 20, 5 ) NULL DEFAULT NULL ;
                ALTER TABLE  `grn` CHANGE  `grnDeliveryCharge`  `grnDeliveryCharge` DECIMAL( 20, 5 ) NULL DEFAULT  '0.00';
                ALTER TABLE  `grn` CHANGE  `grnTotal`  `grnTotal` DECIMAL( 20, 5 ) NULL DEFAULT  '0.00';
                ALTER TABLE  `grnProduct` CHANGE  `grnProductPrice`  `grnProductPrice` DECIMAL( 20, 5 ) NULL DEFAULT  '0.00000';
                ALTER TABLE  `grnProduct` CHANGE  `grnProductTotal`  `grnProductTotal` DECIMAL( 20, 5 ) NULL DEFAULT  '0.00';
                ALTER TABLE  `grnProductTax` CHANGE  `grnTaxAmount`  `grnTaxAmount` DECIMAL( 20, 5 ) NULL DEFAULT NULL ;
                ALTER TABLE  `incomingInvoicePayment` CHANGE  `incomingInvoiceCashAmount`  `incomingInvoiceCashAmount` DECIMAL( 20, 5 ) NULL DEFAULT NULL ;
                ALTER TABLE  `incomingInvoicePayment` CHANGE  `incomingInvoiceCreditAmount`  `incomingInvoiceCreditAmount` DECIMAL( 20, 5 ) NULL DEFAULT NULL ;
                ALTER TABLE  `incomingPayment` CHANGE  `incomingPaymentAmount`  `incomingPaymentAmount` DECIMAL( 20, 5 ) NOT NULL ;
                ALTER TABLE  `incomingPayment` CHANGE  `incomingPaymentDiscount`  `incomingPaymentDiscount` DECIMAL( 20, 5 ) NULL DEFAULT NULL ;
                ALTER TABLE  `incomingPayment` CHANGE  `incomingPaymentPaidAmount`  `incomingPaymentPaidAmount` DECIMAL( 20, 5 ) NULL DEFAULT NULL ;
                ALTER TABLE  `incomingPayment` CHANGE  `incomingPaymentBalance`  `incomingPaymentBalance` DECIMAL( 20, 5 ) NULL DEFAULT NULL ;
                ALTER TABLE  `itemIn` CHANGE  `itemInPrice`  `itemInPrice` DECIMAL( 20, 5 ) NOT NULL ;
                ALTER TABLE  `itemOut` CHANGE  `itemOutPrice`  `itemOutPrice` DECIMAL( 20, 5 ) NOT NULL ;
                ALTER TABLE  `landedCost` CHANGE  `landedCostTotal`  `landedCostTotal` DECIMAL( 20, 5 ) NULL DEFAULT NULL ;
                ALTER TABLE  `landedCostProduct` CHANGE  `landedCostAmount`  `landedCostAmount` DECIMAL( 20, 5 ) NULL DEFAULT NULL ;
                ALTER TABLE  `locationProduct` CHANGE  `locationDiscountValue`  `locationDiscountValue` DECIMAL( 20, 5 ) NOT NULL ;
                ALTER TABLE  `locationProduct` CHANGE  `defaultSellingPrice`  `defaultSellingPrice` DECIMAL( 20, 5 ) NOT NULL ;
                ALTER TABLE  `locationProduct` CHANGE  `lastSellingPrice`  `lastSellingPrice` DECIMAL( 20, 5 ) NULL DEFAULT NULL ;
                ALTER TABLE  `locationProduct` CHANGE  `lastPurchasePrice`  `lastPurchasePrice` DECIMAL( 20, 5 ) NULL DEFAULT NULL ;
                ALTER TABLE  `outgoingPayment` CHANGE  `outgoingPaymentAmount`  `outgoingPaymentAmount` DECIMAL( 20, 5 ) NOT NULL ;
                ALTER TABLE  `outgoingPayment` CHANGE  `outgoingPaymentDiscount`  `outgoingPaymentDiscount` DECIMAL( 20, 5 ) NULL DEFAULT NULL ;
                ALTER TABLE  `outgoingPaymentInvoice` CHANGE  `outgoingInvoiceCashAmount`  `outgoingInvoiceCashAmount` DECIMAL( 20, 5 ) NULL DEFAULT NULL ;
                ALTER TABLE  `outgoingPaymentInvoice` CHANGE  `outgoingInvoiceCreditAmount`  `outgoingInvoiceCreditAmount` DECIMAL( 20, 5 ) NULL DEFAULT NULL ;
                ALTER TABLE  `product` CHANGE  `productDiscountValue`  `productDiscountValue` DECIMAL( 20, 5 ) NOT NULL ;
                ALTER TABLE  `product` CHANGE  `productDefaultSellingPrice`  `productDefaultSellingPrice` DECIMAL( 20, 5 ) NULL DEFAULT  '0.00';
                ALTER TABLE  `product` CHANGE  `customDutyValue`  `customDutyValue` DECIMAL( 20, 5 ) NOT NULL ;
                ALTER TABLE  `project` CHANGE  `projectEstimatedCost`  `projectEstimatedCost` DECIMAL( 20, 5 ) NULL DEFAULT NULL ;
                ALTER TABLE  `purchaseInvoice` CHANGE  `purchaseInvoiceDeliveryCharge`  `purchaseInvoiceDeliveryCharge` DECIMAL( 20, 5 ) NOT NULL DEFAULT  '0.00';
                ALTER TABLE  `purchaseInvoice` CHANGE  `purchaseInvoiceTotal`  `purchaseInvoiceTotal` DECIMAL( 20, 5 ) NOT NULL DEFAULT '0.00';
                ALTER TABLE  `purchaseInvoice` CHANGE  `purchaseInvoicePayedAmount`  `purchaseInvoicePayedAmount` DECIMAL( 20, 5 ) NULL DEFAULT  '0.00';
                ALTER TABLE  `purchaseInvoiceProduct` CHANGE  `purchaseInvoiceProductPrice`  `purchaseInvoiceProductPrice` DECIMAL( 20, 5 ) NULL DEFAULT  '0.00';
                ALTER TABLE  `purchaseInvoiceProduct` CHANGE  `purchaseInvoiceProductTotal`  `purchaseInvoiceProductTotal` DECIMAL( 20, 5 ) NULL DEFAULT  '0.00';
                ALTER TABLE  `purchaseInvoiceProductTax` CHANGE  `purchaseInvoiceTaxAmount`  `purchaseInvoiceTaxAmount` DECIMAL( 20, 5 ) NULL DEFAULT NULL ;
                ALTER TABLE  `purchaseOrder` CHANGE  `purchaseOrderDeliveryCharge`  `purchaseOrderDeliveryCharge` DECIMAL( 20, 5 ) NULL DEFAULT  '0.00';
                ALTER TABLE  `purchaseOrder` CHANGE  `purchaseOrderTotal`  `purchaseOrderTotal` DECIMAL( 20, 5 ) NULL DEFAULT  '0.00';
                ALTER TABLE  `purchaseOrderProduct` CHANGE  `purchaseOrderProductPrice`  `purchaseOrderProductPrice` DECIMAL( 20, 5 ) NULL DEFAULT NULL ;
                ALTER TABLE  `purchaseOrderProduct` CHANGE  `purchaseOrderProductTotal`  `purchaseOrderProductTotal` DECIMAL( 20, 5 ) NULL DEFAULT  '0.00';
                ALTER TABLE  `purchaseOrderProductTax` CHANGE  `purchaseOrderTaxAmount`  `purchaseOrderTaxAmount` DECIMAL( 20, 5 ) NULL DEFAULT NULL ;
                ALTER TABLE  `purchaseReturn` CHANGE  `purchaseReturnTotal`  `purchaseReturnTotal` DECIMAL( 20, 5 ) NULL DEFAULT  '0.00';
                ALTER TABLE  `purchaseReturnProduct` CHANGE  `purchaseReturnProductPrice`  `purchaseReturnProductPrice` DECIMAL( 20, 5 ) NULL DEFAULT NULL ;
                ALTER TABLE  `purchaseReturnProduct` CHANGE  `purchaseReturnProductTotal`  `purchaseReturnProductTotal` DECIMAL( 20, 5 ) NULL DEFAULT NULL ;
                ALTER TABLE  `purchaseReturnProductTax` CHANGE  `purchaseReturnTaxAmount`  `purchaseReturnTaxAmount` DECIMAL( 20, 5 ) NULL DEFAULT NULL ;
                ALTER TABLE  `quotation` CHANGE  `quotationTotalDiscount`  `quotationTotalDiscount` DECIMAL( 20, 5 ) NULL DEFAULT NULL ;
                ALTER TABLE  `quotation` CHANGE  `quotationTotalAmount`  `quotationTotalAmount` DECIMAL( 20, 5 ) NULL DEFAULT NULL ;
                ALTER TABLE  `quotationProduct` CHANGE  `quotationProductUnitPrice`  `quotationProductUnitPrice` DECIMAL( 20, 5 ) NULL DEFAULT NULL ;
                ALTER TABLE  `quotationProduct` CHANGE  `quotationProductDiscount`  `quotationProductDiscount` DECIMAL( 20, 5 ) NULL DEFAULT NULL ;
                ALTER TABLE  `quotationProduct` CHANGE  `quotationProductTotal`  `quotationProductTotal` DECIMAL( 20, 5 ) NULL DEFAULT NULL ;
                ALTER TABLE  `quotationProductTax` CHANGE  `quotTaxAmount`  `quotTaxAmount` DECIMAL( 20, 5 ) NULL DEFAULT NULL ;
                ALTER TABLE  `salesInvoice` CHANGE  `salesinvoiceTotalAmount`  `salesinvoiceTotalAmount` DECIMAL( 20, 5 ) NULL DEFAULT NULL ;
                ALTER TABLE  `salesInvoice` CHANGE  `salesInvoicePayedAmount`  `salesInvoicePayedAmount` DECIMAL( 20, 5 ) NULL DEFAULT NULL ;
                ALTER TABLE  `salesInvoice` CHANGE  `salesInvoiceDeliveryCharge`  `salesInvoiceDeliveryCharge` DECIMAL( 20, 5 ) NULL DEFAULT NULL ;
                ALTER TABLE  `salesInvoiceProduct` CHANGE  `salesInvoiceProductPrice`  `salesInvoiceProductPrice` DECIMAL( 20, 5 ) NULL DEFAULT NULL ;
                ALTER TABLE  `salesInvoiceProduct` CHANGE  `salesInvoiceProductTotal`  `salesInvoiceProductTotal` DECIMAL( 20, 5 ) NULL DEFAULT NULL ;
                ALTER TABLE  `salesInvoiceProductTax` CHANGE  `salesInvoiceProductTaxAmount`  `salesInvoiceProductTaxAmount` DECIMAL( 20, 5 ) NOT NULL ;
                ALTER TABLE  `salesOrderProductTax` CHANGE  `soTaxAmount`  `soTaxAmount` DECIMAL( 20, 5 ) NULL DEFAULT NULL ;
                ALTER TABLE  `salesOrders` CHANGE  `totalDiscount`  `totalDiscount` DECIMAL( 20, 5 ) NULL DEFAULT NULL ;
                ALTER TABLE  `salesOrders` CHANGE  `totalAmount`  `totalAmount` DECIMAL( 20, 5 ) NULL DEFAULT NULL ;
                ALTER TABLE  `salesOrdersProduct` CHANGE  `unitPrice`  `unitPrice` DECIMAL( 20, 5 ) NULL DEFAULT NULL ;
                ALTER TABLE  `salesOrdersProduct` CHANGE  `discount`  `discount` DECIMAL( 20, 5 ) NULL DEFAULT NULL ;
                ALTER TABLE  `salesOrdersProduct` CHANGE  `total`  `total` DECIMAL( 20, 5 ) NULL DEFAULT NULL ;
                ALTER TABLE  `salesReturnProduct` CHANGE  `salesReturnProductPrice`  `salesReturnProductPrice` DECIMAL( 20, 5 ) NULL DEFAULT NULL ;
                ALTER TABLE  `salesReturnProduct` CHANGE  `salesReturnProductDiscount`  `salesReturnProductDiscount` DECIMAL( 20, 5 ) NULL DEFAULT NULL ;
                ALTER TABLE  `salesReturnProduct` CHANGE  `salesReturnProductTotal`  `salesReturnProductTotal` DECIMAL( 20, 5 ) NOT NULL ;
                ALTER TABLE  `salesReturnProductTax` CHANGE  `salesReturnProductTaxAmount`  `salesReturnProductTaxAmount` DECIMAL( 20, 5 ) NULL DEFAULT NULL ;
                ALTER TABLE  `stockRevaluationProduct` CHANGE  `stockRevaluationProductPrice`  `stockRevaluationProductPrice` DECIMAL( 20, 5 ) NULL DEFAULT NULL ;
                ALTER TABLE  `supplier` CHANGE  `supplierCreditLimit`  `supplierCreditLimit` DECIMAL( 20, 5 ) NULL DEFAULT NULL ;
                ALTER TABLE  `supplier` CHANGE  `supplierOutstandingBalance`  `supplierOutstandingBalance` DECIMAL( 20, 5 ) NULL DEFAULT NULL ;
                ALTER TABLE  `supplier` CHANGE  `supplierCreditBalance`  `supplierCreditBalance` DECIMAL( 20, 5 ) NULL DEFAULT NULL ;
                ALTER TABLE  `temporaryProduct` CHANGE  `temporaryProductPrice`  `temporaryProductPrice` DECIMAL( 20, 5 ) NULL DEFAULT NULL ;
                ALTER TABLE  `pos` CHANGE  `openingBalance`  `openingBalance` DECIMAL( 20, 5 ) NOT NULL ;
                ALTER TABLE  `pos` CHANGE  `closingBalance`  `closingBalance` DECIMAL( 20, 5 ) NOT NULL ;
                ALTER TABLE  `employee` CHANGE  `employeeHourlyCost`  `employeeHourlyCost` DECIMAL( 20, 5 ) NULL DEFAULT NULL ;
                ALTER TABLE  `creditNotePaymentDetails` CHANGE  `creditNotePaymentDetailsAmount`  `creditNotePaymentDetailsAmount` DECIMAL( 20, 5 ) NOT NULL ;
                ALTER TABLE  `price` CHANGE  `priceAmount`  `priceAmount` DECIMAL( 20, 5 ) NULL DEFAULT NULL ;
                ALTER TABLE  `price` CHANGE  `priceDiscount`  `priceDiscount` DECIMAL( 20, 5 ) NULL DEFAULT NULL ;

SQL;
        $this->execute($sql);
    }

}
