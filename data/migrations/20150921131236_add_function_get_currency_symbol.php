<?php

use Phinx\Migration\AbstractMigration;

class AddFunctionGetCurrencySymbol extends AbstractMigration
{

    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     */
//    public function change()
//    {
//
//    }

    public function up()
    {
        $rows = $this->fetchAll("SELECT roleID FROM `role`");
        $this->execute("INSERT INTO `feature` (`featureID`, `featureName`, `featureController`, `featureAction`, `featureCategory`, `featureType`, `moduleID`) VALUES (NULL, 'Default', 'Core\\\Controller\\\CoreController', 'getCurrencySymbol', 'Application', '1', '1');");
        $id1 = $this->fetchRow("select LAST_INSERT_ID()")[0];
        foreach ($rows as $row) {
            $roleID = $row['roleID'];
            if ($roleID == 1) {
                $enabled = 1;
            } else {
                $enabled = 0;
            }
            $this->execute("INSERT INTO `roleFeature` (`roleFeatureID`, `roleID`, `featureID`, `roleFeatureEnabled`) VALUES (NULL, $roleID, $id1,$enabled);");
        }
    }

}
