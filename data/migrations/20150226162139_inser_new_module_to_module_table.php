<?php

use Phinx\Migration\AbstractMigration;

class InserNewModuleToModuleTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
      public function change()
      {
      }
     */

    /**
     * Migrate Up.
     */
    public function up()
    {
        $this->execute("INSERT IGNORE INTO `module` (
`moduleID` ,
`moduleName`
)
VALUES (
8, 'JobCard'
);");
    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }

}
