<?php

use Phinx\Migration\AbstractMigration;

class AddNewColumnCalledTaskCardStatusToTaskCardTableAndTaskCardTaskTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->table('taskCard');
        $table1 = $this->table('taskCardTask');
        $column = $table->hasColumn('status');
        $column1 = $table1->hasColumn('taskCardTaskStatus');
        $column2 = $table1->hasColumn('taskCardTaskDeleted');

        if (!$column) {
            $table->addColumn('status', 'integer', ['default' => true, 'null' => true])
                ->update();
        }
        if (!$column1) {
            $table1->addColumn('taskCardTaskStatus', 'integer', ['default' => true, 'null' => true])
                ->update();
        }
        if (!$column2) {
            $table1->addColumn('taskCardTaskDeleted', 'boolean', ['default' => false, 'null' => true])
                ->update();
        }
    }
}
