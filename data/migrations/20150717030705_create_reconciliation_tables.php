<?php

use Phinx\Migration\AbstractMigration;

class CreateReconciliationTables extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
    public function change()
    {
    }
    */
    
    /**
     * Migrate Up.
     */
    public function up()
    {
        //remove incomingPaymentMethodChequeReconciliationStatus column
        $this->execute("ALTER TABLE `incomingPaymentMethodCheque` DROP COLUMN `chequeDepositId`;");
        
        //create reconciliation table
        $this->execute("CREATE TABLE IF NOT EXISTS `reconciliation` (
            `reconciliationId` int(11) NOT NULL AUTO_INCREMENT,
            `reconciliationAmount` decimal(10,2) NOT NULL DEFAULT '0.00',
            `reconciliationComment` text,
            `reconciliationDate` date DEFAULT NULL,
            `accountId` int(11) DEFAULT NULL,
            `entityId` int(11) DEFAULT NULL,
            PRIMARY KEY (`reconciliationId`)
          ) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;"
        );
        
        //craete reconciliationTransaction table
        $this->execute("CREATE TABLE IF NOT EXISTS `reconciliationTransaction` (
            `reconciliationTransactionId` int(11) NOT NULL AUTO_INCREMENT,
            `reconciliationTransactionCreditAmount` decimal(10,2) DEFAULT '0.00',
            `reconciliationTransactionDebitAmount` decimal(10,2) DEFAULT '0.00',
            `reconciliationTransactionType` varchar(255) NOT NULL,
            `incomingPaymentMethodChequeId` int(11) DEFAULT NULL,
            `outGoingPaymentMethodsNumbersId` int(11) DEFAULT NULL,
            `incomingPaymentMethodBankTransferId` int(11) DEFAULT NULL,
            `reconciliationId` int(11) DEFAULT NULL,
            `entityId` int(11) DEFAULT NULL,
            PRIMARY KEY (`reconciliationTransactionId`)
          ) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;"
        );
        
        //create chequeDepositCheque table
        $this->execute("CREATE TABLE IF NOT EXISTS `chequeDepositCheque` (
            `chequeDepositChequeId` int(11) NOT NULL AUTO_INCREMENT,
            `chequeDepositId` int(11) DEFAULT NULL,
            `incomingPaymentMethodChequeId` int(11) DEFAULT NULL,
            `entityId` int(11) DEFAULT NULL,
            PRIMARY KEY (`chequeDepositChequeId`)
          ) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;"
        );
        
        //change chequeDepositDate data type
        $this->execute("ALTER TABLE  `chequeDeposit` CHANGE  `chequeDepositDate`  `chequeDepositDate` DATE NULL DEFAULT NULL ;");
        //add outGoingPaymentMethodReconciliationStatus column to outGoingPaymentMethodsNumbers table
        $this->execute("ALTER TABLE  `outGoingPaymentMethodsNumbers` ADD  `outGoingPaymentMethodReconciliationStatus` TINYINT( 1 ) NOT NULL DEFAULT  '0';");
        //add incomingPaymentMethodBankTransferReconciliationStatus column to incomingPaymentMethodBankTransfer table
        $this->execute("ALTER TABLE  `incomingPaymentMethodBankTransfer` ADD  `incomingPaymentMethodBankTransferReconciliationStatus` TINYINT( 1 ) NULL DEFAULT  '0';");
        
        //role list
        $roles = $this->fetchAll("SELECT roleID FROM `role`");
        
        //reconciliation feature list
        $featureList = array(
            array('name' =>'View Reconciliations','controller'=>'Expenses\\\Controller\\\Reconciliation','action'=>'reconciliation','category'=>'Reconciliation','type'=>'0','moduleId'=>'9'),
            array('name' =>'Default','controller'=>'Expenses\\\Controller\\\API\\\ReconciliationAPI','action'=>'create','category'=>'Reconciliation','type'=>'1','moduleId'=>'9'),
            array('name' =>'Default','controller'=>'Expenses\\\Controller\\\API\\\ReconciliationAPI','action'=>'getReconciliations','category'=>'Reconciliation','type'=>'1','moduleId'=>'9'),
            array('name' =>'Default','controller'=>'Expenses\\\Controller\\\API\\\ReconciliationAPI','action'=>'cancel','category'=>'Reconciliation','type'=>'1','moduleId'=>'9'),
            array('name' =>'Default','controller'=>'Expenses\\\Controller\\\API\\\ReconciliationAPI','action'=>'getTransactions','category'=>'Reconciliation','type'=>'1','moduleId'=>'9'),
            array('name' =>'Default','controller'=>'Expenses\\\Controller\\\API\\\ReconciliationAPI','action'=>'update','category'=>'Reconciliation','type'=>'1','moduleId'=>'9')
        );
        
        foreach ($featureList as $feature){            
            //insert feature to feature table
            $this->execute("INSERT INTO `feature` (`featureID`, `featureName`, `featureController`, `featureAction`, `featureCategory`, `featureType`, `moduleID`) VALUES (NULL,'".$feature['name']."', '".$feature['controller']."', '".$feature['action']."', '".$feature['category']."', '".$feature['type']."', '".$feature['moduleId']."');");
            $id = $this->fetchRow("select LAST_INSERT_ID()")[0];
            
            foreach ($roles as $role) {
                $roleID = $role['roleID'];
                if ($roleID == 1) {
                    $enabled = 1;
                } else {
                    //for enable api calls for every role
                    if($feature['type'] === '1'){
                        $enabled = 1;
                    } else {
                        $enabled = 0;
                    }
                }
                $this->execute("INSERT INTO `roleFeature` (`roleFeatureID`, `roleID`, `featureID`, `roleFeatureEnabled`) VALUES (NULL, $roleID, $id,$enabled);");
            }
        }
        
        //update feature table
        $feature1 = $this->fetchRow("SELECT * FROM `feature` WHERE `featureController` = 'Expenses\\\Controller\\\Reconciliation' AND `featureAction` = 'index'");
        $this->execute("UPDATE `feature` SET `featureName`='Transactions' WHERE `featureID` = '".$feature1['featureID']."'");
        
        $feature2 = $this->fetchRow("SELECT * FROM `feature` WHERE `featureController` = 'Expenses\\\Controller\\\API\\\ReconciliationAPI' AND `featureAction` = 'getAccountCheques'");
        $this->execute("UPDATE `feature` SET `featureAction`='getAccountTransactions' WHERE `featureID` = '".$feature2['featureID']."'");
        
    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }
}