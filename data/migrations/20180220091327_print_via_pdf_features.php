<?php

use Phinx\Migration\AbstractMigration;

class PrintViaPdfFeatures extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $pdo = $this->getAdapter()->getConnection();
        $rows = $this->fetchAll("SELECT roleID FROM `role`");

        $dataArray = array(
            array(
                'featureName' => 'Print Via PDF',
                'featureController' => 'Invoice\Controller\Invoice',
                'featureAction' => 'documentPdf',
                'featureCategory' => 'Invoice',
                'featureType' => '0',
                'moduleID' => '3'
            ),
            array(
                'featureName' => 'Print Via PDF',
                'featureController' => 'Invoice\Controller\QuotationController',
                'featureAction' => 'documentPdf',
                'featureCategory' => 'Quotation',
                'featureType' => '0',
                'moduleID' => '3'
            ),
            array(
                'featureName' => 'Print Via PDF',
                'featureController' => 'Invoice\Controller\SalesOrders',
                'featureAction' => 'documentPdf',
                'featureCategory' => 'Sales Order',
                'featureType' => '0',
                'moduleID' => '3'
            ),
            array(
                'featureName' => 'Print Via PDF',
                'featureController' => 'Invoice\Controller\DeliveryNote',
                'featureAction' => 'documentPdf',
                'featureCategory' => 'Delivery Note',
                'featureType' => '0',
                'moduleID' => '3'
            ),
            array(
                'featureName' => 'Print Via PDF',
                'featureController' => 'Invoice\Controller\Return',
                'featureAction' => 'documentPdf',
                'featureCategory' => 'Return',
                'featureType' => '0',
                'moduleID' => '3'
            ),
            array(
                'featureName' => 'Print Via PDF',
                'featureController' => 'Invoice\Controller\CreditNote',
                'featureAction' => 'documentPdf',
                'featureCategory' => 'Credit Note',
                'featureType' => '0',
                'moduleID' => '3'
            ),
            array(
                'featureName' => 'Print Via PDF',
                'featureController' => 'Invoice\Controller\CustomerPayments',
                'featureAction' => 'documentPdf',
                'featureCategory' => 'Customer Payment',
                'featureType' => '0',
                'moduleID' => '3'
            ),
            array(
                'featureName' => 'Print Via PDF',
                'featureController' => 'Inventory\Controller\PurchaseOrderController',
                'featureAction' => 'documentPdf',
                'featureCategory' => 'Purchase Order',
                'featureType' => '0',
                'moduleID' => '11'
            ),
            array(
                'featureName' => 'Print Via PDF',
                'featureController' => 'Inventory\Controller\GrnController',
                'featureAction' => 'documentPdf',
                'featureCategory' => 'GRN',
                'featureType' => '0',
                'moduleID' => '2'
            ),
            array(
                'featureName' => 'Print Via PDF',
                'featureController' => 'Inventory\Controller\PurchaseReturnsController',
                'featureAction' => 'documentPdf',
                'featureCategory' => 'Returns',
                'featureType' => '0',
                'moduleID' => '11'
            ),
            array(
                'featureName' => 'Print Via PDF',
                'featureController' => 'Inventory\Controller\PurchaseInvoiceController',
                'featureAction' => 'documentPdf',
                'featureCategory' => 'Purchase Invoice',
                'featureType' => '0',
                'moduleID' => '11'
            ),
            array(
                'featureName' => 'Print Via PDF',
                'featureController' => 'Inventory\Controller\DebitNote',
                'featureAction' => 'documentPdf',
                'featureCategory' => 'Debit Note',
                'featureType' => '0',
                'moduleID' => '11'
            ),
            array(
                'featureName' => 'Print Via PDF',
                'featureController' => 'Inventory\Controller\OutGoingPayment',
                'featureAction' => 'documentPdf',
                'featureCategory' => 'Supplier Payment',
                'featureType' => '0',
                'moduleID' => '11'
            ),
            array(
                'featureName' => 'Print Via PDF',
                'featureController' => 'Inventory\Controller\Transfer',
                'featureAction' => 'documentPdf',
                'featureCategory' => 'Transfer',
                'featureType' => '0',
                'moduleID' => '2'
            ),            
            array(
                'featureName' => 'Print Via PDF',
                'featureController' => 'Inventory\Controller\InventoryAdjustment',
                'featureAction' => 'documentPdf',
                'featureCategory' => 'Adjustment',
                'featureType' => '0',
                'moduleID' => '2'
            ),
            array(
                'featureName' => 'Print Via PDF',
                'featureController' => 'Invoice\Controller\CreditNotePayments',
                'featureAction' => 'documentPdf',
                'featureCategory' => 'Credit Note Payments',
                'featureType' => '0',
                'moduleID' => '3'
            ),
            array(
                'featureName' => 'Print Via PDF',
                'featureController' => 'Inventory\Controller\DebitNotePayments',
                'featureAction' => 'documentPdf',
                'featureCategory' => 'Debit Note Payments',
                'featureType' => '0',
                'moduleID' => '11'
            ),
            array(
                'featureName' => 'Print Via PDF',
                'featureController' => 'JobCard\Controller\Job',
                'featureAction' => 'documentPdf',
                'featureCategory' => 'Job',
                'featureType' => '0',
                'moduleID' => '8'
            ),
            array(
                'featureName' => 'Print Via PDF',
                'featureController' => 'Expenses\Controller\ExpensePurchaseInvoice',
                'featureAction' => 'documentPdf',
                'featureCategory' => 'payment voucher',
                'featureType' => '0',
                'moduleID' => '9'
            ),
            array(
                'featureName' => 'Print Via PDF',
                'featureController' => 'Expenses\Controller\PettyCashVoucher',
                'featureAction' => 'documentPdf',
                'featureCategory' => 'Petty Cash Voucher',
                'featureType' => '0',
                'moduleID' => '9'
            ),
            array(
                'featureName' => 'Print Via PDF',
                'featureController' => 'Accounting\Controller\JournalEntries',
                'featureAction' => 'documentPdf',
                'featureCategory' => 'Journal Entries',
                'featureType' => '0',
                'moduleID' => '12'
            ),
            array(
                'featureName' => 'Print Via PDF',
                'featureController' => 'Invoice\Controller\DispatchNote',
                'featureAction' => 'documentPdf',
                'featureCategory' => 'Dispatch Note',
                'featureType' => '0',
                'moduleID' => '3'
            )
        );

        foreach($dataArray as $feature){

            $featureInsert = <<<SQL
                INSERT INTO `feature`
                (
                    `featureName`,
                    `featureController`,
                    `featureAction`,
                    `featureCategory`,
                    `featureType`,
                    `moduleID`
                ) VALUES (
                    :featureName,
                    :featureController,
                    :featureAction,
                    :featureCategory,
                    :featureType,
                    :featureModuleId
                );

SQL;

            $pdo->prepare($featureInsert)->execute(array(
                'featureName' => $feature['featureName'],
                'featureController' => $feature['featureController'],
                'featureAction' => $feature['featureAction'],
                'featureCategory' => $feature['featureCategory'],
                'featureType' => $feature['featureType'],
                'featureModuleId' => $feature['moduleID']
            ));

            $featureID = $this->fetchRow("select LAST_INSERT_ID()")[0];

            foreach ($rows as $row) {
                $roleID = $row['roleID'];
                $enabled = 1; // enable for every role

                $roleFeatureInsert =<<<SQL
                    INSERT INTO `roleFeature`
                    (
                        `roleID`,
                        `featureID`,
                        `roleFeatureEnabled`
                    ) VALUES(
                        :roleID,
                        :featureID,
                        :roleFeatureEnabled
                    )
SQL;
                $pdo->prepare($roleFeatureInsert)->execute(array(
                    'roleID' => $roleID,
                    'featureID' => $featureID,
                    'roleFeatureEnabled' => $enabled
                ));
            }

        }
    }
}
