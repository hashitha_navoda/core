<?php

use Phinx\Migration\AbstractMigration;

class CreateTableCallBomAssemblyDisassembly extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
      public function change()
      {
      }
     */

    /**
     * Migrate Up.
     */
    public function up()
    {
        $tableExist = $this->hasTable('bomAssemblyDisassembly');
        if (!$tableExist) {
            $table = $this->table('bomAssemblyDisassembly', array('id' => 'bomAssemblyDisassemblyID'));
            $table->addColumn('bomAssemblyDisassemblyType', 'string', array('null' => 'allow'))
                    ->addColumn('bomAssemblyDisassemblyReference', 'string', array('null' => 'allow'))
                    ->addColumn('bomID', 'integer')
                    ->addColumn('bomAssemblyDisassemblyParentQuantity', 'integer')
                    ->addColumn('bomAssemblyDisassemblyComment', 'text', array('null' => 'allow'))
                    ->save();
        }
    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }

}
