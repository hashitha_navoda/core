<?php

use Phinx\Migration\AbstractMigration;

class UpdateTaxFeatureName extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
      public function change()
      {
      }
     */

    /**
     * Migrate Up.
     */
    public function up()
    {
        $this->execute("UPDATE `feature` SET `featureName`='Add & View List' WHERE `featureController`='Settings\\\Controller\\\Tax' AND `featureAction`='index'");
    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }

}
