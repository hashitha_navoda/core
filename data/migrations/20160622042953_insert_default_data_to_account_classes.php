<?php

use Phinx\Migration\AbstractMigration;

class InsertDefaultDataToAccountClasses extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $pdo = $this->getAdapter()->getConnection();

        $rows = $this->fetchAll("SELECT userID FROM `user` WHERE roleID = 1");
        $userID = $rows[0]['userID'];         
        $timeStamp = date( "Y-m-d H:i:s"); 
        
        $dataArray = array(
            array(
                'financeAccountClassID' => 1,
                'financeAccountClassName' => 'Current assets - Cash and cash equivalents',
                'financeAccountTypesID' => 1,
            ),
            array(
                'financeAccountClassID' => 2,
                'financeAccountClassName' => 'Current assets - Accounts receivable',
                'financeAccountTypesID' => 1,
            ),
            array(
                'financeAccountClassID' => 3,
                'financeAccountClassName' => 'Current assets - Inventory',
                'financeAccountTypesID' => 1,
            ),
            array(
                'financeAccountClassID' => 4,
                'financeAccountClassName' => 'Current assets - Other current assets',
                'financeAccountTypesID' => 1,
            ),
            array(
                'financeAccountClassID' => 5,
                'financeAccountClassName' => 'Long term assets - Property, plant and equipment',
                'financeAccountTypesID' => 1,
            ),
            array(
                'financeAccountClassID' => 6,
                'financeAccountClassName' => 'Current liabilities - Accounts payable',
                'financeAccountTypesID' => 2,
            ),
            array(
                'financeAccountClassID' => 7,
                'financeAccountClassName' => 'Current liabilities - Other current liabilities',
                'financeAccountTypesID' => 2,
            ),
            array(
                'financeAccountClassID' => 8,
                'financeAccountClassName' => 'Long-term liabilities - Mortgages',
                'financeAccountTypesID' => 2,
            ),
            array(
                'financeAccountClassID' => 9,
                'financeAccountClassName' => 'Long-term liabilities - Loans',
                'financeAccountTypesID' => 2,
            ),
            array(
                'financeAccountClassID' => 10,
                'financeAccountClassName' => 'Equity - Capital',
                'financeAccountTypesID' => 5,
            ),
            array(
                'financeAccountClassID' => 11,
                'financeAccountClassName' => 'Equity - Retained earnings',
                'financeAccountTypesID' => 5,
            ),
            array(
                'financeAccountClassID' => 13,
                'financeAccountClassName' => 'Income - Revenue',
                'financeAccountTypesID' => 4,
            ),
            array(
                'financeAccountClassID' => 14,
                'financeAccountClassName' => 'Cost of sales - Cost of sales',
                'financeAccountTypesID' => 6,
            ),
            array(
                'financeAccountClassID' => 15,
                'financeAccountClassName' => 'Expense - Research and development',
                'financeAccountTypesID' => 3,
            ),
            array(
                'financeAccountClassID' => 16,
                'financeAccountClassName' => 'Expense - Sales and marketing',
                'financeAccountTypesID' => 3,
            ),
            array(
                'financeAccountClassID' => 17,
                'financeAccountClassName' => 'Expense - General and administrative',
                'financeAccountTypesID' => 3,
            ),
            array(
                'financeAccountClassID' => 18,
                'financeAccountClassName' => 'Expense - Depreciation',
                'financeAccountTypesID' => 3,
            ),
            array(
                'financeAccountClassID' => 19,
                'financeAccountClassName' => 'Expense - Finance costs',
                'financeAccountTypesID' => 3,
            ),
            array(
                'financeAccountClassID' => 20,
                'financeAccountClassName' => 'Income - Other Income',
                'financeAccountTypesID' => 7,
            ),
            array(
                'financeAccountClassID' => 21,
                'financeAccountClassName' => 'Expense - Income tax expense',
                'financeAccountTypesID' => 3,
            ),
        );

        foreach($dataArray as $accountClass){


            $entityInsert = <<<SQL
                INSERT INTO `entity` 
                (
                    `createdBy`, 
                    `createdTimeStamp`, 
                    `updatedBy`, 
                    `updatedTimeStamp`, 
                    `deleted`, 
                    `deletedBy`,
                    `deletedTimeStamp`
                ) VALUES (
                    :createdBy, 
                    :createdTimeStamp, 
                    :updatedBy, 
                    :updatedTimeStamp, 
                    :deleted, 
                    :deletedBy,
                    :deletedTimeStamp
                );
    
SQL;

            $pdo->prepare($entityInsert)->execute(array(
                'createdBy' => $userID, 
                'createdTimeStamp' => $timeStamp, 
                'updatedBy' => $userID, 
                'updatedTimeStamp' =>  $timeStamp, 
                'deleted' => 0, 
                'deletedBy' => NULL,
                'deletedTimeStamp' => NULL
            ));

            $entityID = $this->fetchRow("select LAST_INSERT_ID()")[0];
           
            $accountClassInsert = <<<SQL
                INSERT INTO `financeAccountClass` 
                (
                    `financeAccountClassID`, 
                    `financeAccountClassName`, 
                    `financeAccountTypesID`, 
                    `entityID` 
                ) VALUES (
                    :financeAccountClassID, 
                    :financeAccountClassName, 
                    :financeAccountTypesID, 
                    :entityID
                );
    
SQL;
    
            $pdo->prepare($accountClassInsert)->execute(array(
                'financeAccountClassID' => $accountClass['financeAccountClassID'], 
                'financeAccountClassName' => $accountClass['financeAccountClassName'], 
                'financeAccountTypesID' => $accountClass['financeAccountTypesID'], 
                'entityID' => $entityID,
            ));

        }
    }
}
