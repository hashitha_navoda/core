<?php

use Phinx\Migration\AbstractMigration;

class UpdateTaxSuspendableColumnInTaxTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     */
    public function change()
    {
        $this->execute("SET SESSION old_alter_table=1; ALTER TABLE `tax` CHANGE `taxSuspendable` `taxSuspendable` TINYINT(1) NOT NULL DEFAULT '0'; ");
    }
}
