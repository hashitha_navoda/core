<?php

use Phinx\Migration\AbstractMigration;

class AddNewTableCalledIncomeProductTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table1 = $this->hasTable('incomeProduct');

        if (!$table1) {
            $table1 = $this->table('incomeProduct', ['id' => 'incomeProductID']);
            $table1->addColumn('incomeID', 'integer', ['limit' => 20,'default' => null, 'null' => true]);
            $table1->addColumn('productID', 'integer', ['limit' => 20,'default' => null, 'null' => true]);
            $table1->addColumn('incomeProductPrice', 'decimal', ['precision' => 20, 'scale' => 5,'default' => 0, 'null' => true]);
            $table1->addColumn('incomeProductDiscount', 'decimal', ['precision' => 20, 'scale' => 5, 'default' => null, 'null' => true]);
            $table1->addColumn('incomeProductDiscountType', 'string', ['limit' => 100 ,'default' => null, 'null' => true]);
            $table1->addColumn('incomeProductTotal', 'decimal', ['precision' => 20, 'scale' => 5,'default' => 0, 'null' => true]);
            $table1->addColumn('incomeProductQuantity', 'decimal', ['precision' => 20, 'scale' => 5,'default' => null, 'null' => true]);
            $table1->save();
        }

        $table2 = $this->hasTable('incomeSubProduct');

        if (!$table2) {
            $table2 = $this->table('incomeSubProduct', ['id' => 'incomeSubProductID']);
            $table2->addColumn('incomeProductID', 'integer', ['limit' => 20,'default' => null, 'null' => true]);
            $table2->addColumn('productBatchID', 'integer', ['limit' => 20,'default' => null, 'null' => true]);
            $table2->addColumn('productSerialID', 'integer', ['limit' => 20,'default' => null, 'null' => true]);
            $table2->addColumn('incomeProductSubQuantity', 'decimal', ['precision' => 20, 'scale' => 5]);
            $table2->addColumn('incomeSubProductsWarranty', 'integer', ['limit' => 20,'default' => null, 'null' => true]);
            $table2->save();
        }
    }
}
