<?php

use Phinx\Migration\AbstractMigration;

class UpdatePurchaseReturnProductTableValues extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        //load all purchase return details
        $purchaseReturnValues = $this->fetchAll('Select * from purchaseReturn');
        foreach ($purchaseReturnValues as $key => $singlePrvalue) {
            $prID = $singlePrvalue['purchaseReturnID'];
            $purchaseReturnProductSet = $this
                ->fetchAll("SELECT * FROM `purchaseReturnProduct` WHERE `purchaseReturnID`= '".$prID."' AND `purchaseReturnProductGrnProductID` = 0 ");
            
            if(sizeof($purchaseReturnProductSet)>1){
                foreach ($purchaseReturnProductSet as $key => $singlePrProductvalue) {
                    if($singlePrProductvalue['purchaseReturnProductBatchID'] == NULL && 
                            $singlePrProductvalue['purchaseReturnProductSerialID'] == NULL){
                        $locProID = $singlePrProductvalue['purchaseReturnLocationProductID'];
                        $purchPrice = $singlePrProductvalue['purchaseReturnProductPrice'];
                        
                        $grnProductDetails = $this
                            ->fetchAll("SELECT `grnProductID` FROM `grnProduct` WHERE 
                                `grnID` = '".$singlePrvalue['purchaseReturnGrnID']."' && 
                                `locationProductID` = '".$locProID."' && 
                                `grnProductPrice` = '".$purchPrice."'
                                ");
                
                    //update purchaseReturn table grnProductID column
                        $updateGrnPrID = $this->execute("UPDATE `purchaseReturnProduct` SET `purchaseReturnProductGrnProductID` = '".$grnProductDetails[0]['grnProductID']."'
                            WHERE `purchaseReturnProductID`= '".$singlePrProductvalue['purchaseReturnProductID']."'");

                    } else if($singlePrProductvalue['purchaseReturnProductBatchID'] != NULL ) {
                        //update batch products
                        if($singlePrProductvalue['purchaseReturnProductGrnProductID'] != 0){
                            continue;
                        }
                        $locProID = $singlePrProductvalue['purchaseReturnLocationProductID'];
                        $purchPrice = $singlePrProductvalue['purchaseReturnProductPrice'];
                        $batchID = $singlePrProductvalue['purchaseReturnProductBatchID'];
                       
                        $grnProductDetails = $this
                            ->fetchAll("SELECT `grnProductID` FROM `grnProduct` WHERE 
                                `grnID` = '".$singlePrvalue['purchaseReturnGrnID']."' && 
                                `locationProductID` = '".$locProID."' && 
                                `grnProductPrice` = '".$purchPrice."' &&
                                `productBatchID` = '".$batchID."'
                                ");

                        //update purchaseReturn table grnProductID column
                        $updateGrnPrID = $this->execute("UPDATE `purchaseReturnProduct` SET `purchaseReturnProductGrnProductID` = '".$grnProductDetails[0]['grnProductID']."'
                            WHERE `purchaseReturnID`= '".$singlePrProductvalue['purchaseReturnID']."' AND 
                            `purchaseReturnLocationProductID`= '".$singlePrProductvalue['purchaseReturnLocationProductID']."' AND 
                            `purchaseReturnProductPrice` = '".$singlePrProductvalue['purchaseReturnProductPrice']."'");
                    } else if($singlePrProductvalue['purchaseReturnProductSerialID'] != NULL) {
                        //update serial products
                        if($singlePrProductvalue['purchaseReturnProductGrnProductID'] != 0){
                            continue;
                        }
                        $locProID = $singlePrProductvalue['purchaseReturnLocationProductID'];
                        $purchPrice = $singlePrProductvalue['purchaseReturnProductPrice'];
                        $serialID = $singlePrProductvalue['purchaseReturnProductSerialID'];
                       
                        $grnProductDetails = $this
                            ->fetchAll("SELECT `grnProductID` FROM `grnProduct` WHERE 
                                `grnID` = '".$singlePrvalue['purchaseReturnGrnID']."' && 
                                `locationProductID` = '".$locProID."' && 
                                `grnProductPrice` = '".$purchPrice."' &&
                                `productSerialID` = '".$serialID."'
                                ");

                        //update purchaseReturn table grnProductID column
                        $updateGrnPrID = $this->execute("UPDATE `purchaseReturnProduct` SET `purchaseReturnProductGrnProductID` = '".$grnProductDetails[0]['grnProductID']."'
                            WHERE `purchaseReturnID`= '".$singlePrProductvalue['purchaseReturnID']."' AND 
                            `purchaseReturnLocationProductID`= '".$singlePrProductvalue['purchaseReturnLocationProductID']."' AND 
                            `purchaseReturnProductPrice` = '".$singlePrProductvalue['purchaseReturnProductPrice']."'");
                    }

                }
                
            } else if (sizeof($purchaseReturnProductSet) == 1){
                $locProID = $purchaseReturnProductSet[0]['purchaseReturnLocationProductID'];
                $purchPrice = $purchaseReturnProductSet[0]['purchaseReturnProductPrice'];
                
                $grnProductDetails = $this
                    ->fetchAll("SELECT `grnProductID` FROM `grnProduct` WHERE 
                        `grnID` = '".$singlePrvalue['purchaseReturnGrnID']."' && 
                        `locationProductID` = '".$locProID."' && 
                        `grnProductPrice` = '".$purchPrice."'
                        ");
                
                //update purchaseReturn table grnProductID column
                $updateGrnPrID = $this->execute("UPDATE `purchaseReturnProduct` SET `purchaseReturnProductGrnProductID` = '".$grnProductDetails[0]['grnProductID']."'
                        WHERE `purchaseReturnProductID`= '".$purchaseReturnProductSet[0]['purchaseReturnProductID']."'");

               
            }
        }
    }
}
