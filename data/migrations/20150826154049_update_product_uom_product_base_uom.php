<?php

use Phinx\Migration\AbstractMigration;

class UpdateProductUomProductBaseUom extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
      public function change()
      {
      }
     */

    /**
     * Migrate Up.
     */
    public function up()
    {
        $rows = $this->fetchAll("SELECT * FROM `productUom`");

        foreach ($rows as $row) {
            if ($row['productUomConversion'] == 1) {
                $id = $row['productUoMID'];
                $this->execute("UPDATE `productUom` SET `productUomBase`=1 WHERE `productUoMID`=$id");
            }
        }
    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }

}
