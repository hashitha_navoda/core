<?php

use Phinx\Migration\AbstractMigration;

class RemoveQuotationCodeFromQuotationProduct extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
      public function change()
      {
      }
     */

    /**
     * Migrate Up.
     */
    public function up()
    {
        $this->execute("UPDATE `quotationProduct`, `quotation` "
                . "SET `quotationProduct`.`quotationID` = `quotation`.`quotationID` "
                . "WHERE `quotationProduct`.`quotationCode` = `quotation`.`quotationCode` "
                . "AND `quotationProduct`.`quotationCode` != '' ;");

        $this->execute("SET SESSION old_alter_table=1; ALTER TABLE  `quotationProduct` DROP  `quotationCode` ;");
    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }

}
