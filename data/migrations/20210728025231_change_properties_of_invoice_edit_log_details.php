<?php

use Phinx\Migration\AbstractMigration;

class ChangePropertiesOfInvoiceEditLogDetails extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->table('invoiceEditLogDetails');
        $column = $table->hasColumn('invoiceEditLogDetailsNewState');
        if ($column) {
            $table->changeColumn('invoiceEditLogDetailsNewState', 'text', ['limit' => 4294967295,'default' => null, 'null' => true])
                ->save();
        }

        $table1 = $this->table('deliveryNoteEditLogDetails');
        $column1 = $table1->hasColumn('deliveryNoteEditLogDetailsNewState');
        if ($column1) {
            $table1->changeColumn('deliveryNoteEditLogDetailsNewState', 'text', ['limit' => 4294967295,'default' => null, 'null' => true])
                ->save();
        }

        $table2 = $this->table('inquiryLog');
        $column2 = $table2->hasColumn('inquiryLogDescription');
        if ($column2) {
            $table2->changeColumn('inquiryLogDescription', 'text', ['limit' => 4294967295,'default' => null, 'null' => true])
                ->save();
        }
    }
}
