<?php

use Phinx\Migration\AbstractMigration;

class ChangePaymentVoucherTableDataTypes extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {

        $this->execute("SET SESSION old_alter_table=1;
            ALTER TABLE `paymentVoucher` CHANGE `paymentVoucherPaidAmount` `paymentVoucherPaidAmount` DECIMAL(25,10) NULL DEFAULT '0';
            ALTER TABLE `paymentVoucher` CHANGE `paymentVoucherTotal` `paymentVoucherTotal` DECIMAL(25,10) NULL DEFAULT '0';
            ");

        //get all paymentVoucher details with null paidAmounts
        $this->execute("UPDATE `paymentVoucher` set `paymentVoucherPaidAmount` = '0' WHERE `paymentVoucherPaidAmount` IS NULL");

    }
}
