<?php

use Phinx\Migration\AbstractMigration;

class AddNonInventoryItemMovingReport extends AbstractMigration
{

    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     */
//    public function change()
//    {
//
//    }

    public function up()
    {
        $rows = $this->fetchAll("SELECT roleID FROM `role`");

        $this->execute("INSERT INTO `feature` (`featureID`, `featureName`, `featureController`, `featureAction`, `featureCategory`, `featureType`, `moduleID`) VALUES (NULL, 'View Report', 'Reporting\\\Controller\\\API\\\StockInHandReport', 'viewGlobalNonInventoryItemMoving', 'Non Inventory Item Moving', '0', '5');");
        $id1 = $this->fetchRow("select LAST_INSERT_ID()")[0];

        $this->execute("INSERT INTO `feature` (`featureID`, `featureName`, `featureController`, `featureAction`, `featureCategory`, `featureType`, `moduleID`) VALUES (NULL, 'Generate PDF', 'Reporting\\\Controller\\\API\\\StockInHandReport', 'generateNonInventoryItemMovingPdf', 'Non Inventory Item Moving', '0', '5');");
        $id2 = $this->fetchRow("select LAST_INSERT_ID()")[0];

        $this->execute("INSERT INTO `feature` (`featureID`, `featureName`, `featureController`, `featureAction`, `featureCategory`, `featureType`, `moduleID`) VALUES (NULL, 'Generate Sheet', 'Reporting\\\Controller\\\API\\\StockInHandReport', 'generateNonInventoryItemMovingSheet', 'Non Inventory Item Moving', '0', '5');");
        $id3 = $this->fetchRow("select LAST_INSERT_ID()")[0];

        foreach ($rows as $row) {
            $roleID = $row['roleID'];
            $enabled = 1;
            $this->execute("INSERT INTO `roleFeature` (`roleFeatureID`, `roleID`, `featureID`, `roleFeatureEnabled`) VALUES (NULL, $roleID, $id1,$enabled);");
            $this->execute("INSERT INTO `roleFeature` (`roleFeatureID`, `roleID`, `featureID`, `roleFeatureEnabled`) VALUES (NULL, $roleID, $id2,$enabled);");
            $this->execute("INSERT INTO `roleFeature` (`roleFeatureID`, `roleID`, `featureID`, `roleFeatureEnabled`) VALUES (NULL, $roleID, $id3,$enabled);");
        }
    }

}
