<?php

use Phinx\Migration\AbstractMigration;

class AddNewTableForContacts extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $contacts = $this->hasTable('contacts');
        if (!$contacts) {
            $newTable = $this->table('contacts', ['id' => 'contactID']);
            $newTable->addColumn('firstName', 'string', ['limit' => 200])
                ->addColumn('lastName', 'string', ['limit' => 200, 'default' => null, 'null' => true])
                ->addColumn('title', 'string', ['limit' => 200, 'default' => null, 'null' => true])
                ->addColumn('designation', 'string', ['limit' => 200, 'default' => null, 'null' => true])
                ->addColumn('entityID', 'integer', ['limit' => 20])
                ->addColumn('isConvertToCustomer', 'boolean', ['default' => 0, 'null' => true])
                ->addColumn('relatedCusID', 'integer', ['limit' => 20, 'default' => null, 'null' => true])
                ->save();
        }

        $contactsMobileNumbers = $this->hasTable('contactMobileNumbers');
        if (!$contactsMobileNumbers) {
            $newTable = $this->table('contactMobileNumbers', ['id' => 'contactMobileNumberID']);
            $newTable->addColumn('contactID', 'integer', ['limit' => 20])
                ->addColumn('mobileNumber', 'string', ['limit' => 200])
                ->addColumn('mobileNumberType', 'integer', ['limit' => 20])
                ->save();
        }

        $contactEmails = $this->hasTable('contactEmails');
        if (!$contactEmails) {
            $newTable = $this->table('contactEmails', ['id' => 'contactEmailID']);
            $newTable->addColumn('contactID', 'integer', ['limit' => 20])
                ->addColumn('emailAddress', 'string', ['limit' => 200])
                ->addColumn('emailType', 'integer', ['limit' => 20])
                ->save();
        }

    }
}
