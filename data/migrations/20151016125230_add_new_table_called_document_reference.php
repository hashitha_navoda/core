<?php

use Phinx\Migration\AbstractMigration;

class AddNewTableCalledDocumentReference extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
      public function change()
      {
      }
     */

    /**
     * Migrate Up.
     */
    public function up()
    {
        $this->execute("CREATE TABLE IF NOT EXISTS `documentReference` (
  `documentReferenceId` int(11) NOT NULL AUTO_INCREMENT,
  `sourceDocumentTypeId` int(11) NOT NULL,
  `sourceDocumentId` int(11) NOT NULL,
  `referenceDocumentTypeId` int(11) NOT NULL,
  `referenceDocumentId` int(11) NOT NULL,
  PRIMARY KEY (`documentReferenceId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;");
    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }

}
