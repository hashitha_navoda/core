<?php

use Phinx\Migration\AbstractMigration;

class AddNewTableCalledIncomingPaymentMethod extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
      public function change()
      {
      }
     */

    /**
     * Migrate Up.
     */
    public function up()
    {
        $this->execute("CREATE TABLE IF NOT EXISTS `incomingPaymentMethod` (
  `incomingPaymentMethodId` int(11) NOT NULL AUTO_INCREMENT,
  `incomingPaymentId` int(11) NOT NULL,
  `incomingPaymentMethodAmount` decimal(20,5) NOT NULL,
  `incomingPaymentMethodCashId` int(11) DEFAULT NULL,
  `incomingPaymentMethodChequeId` int(11) DEFAULT NULL,
  `incomingPaymentMethodCreditCardId` int(11) DEFAULT NULL,
  PRIMARY KEY (`incomingPaymentMethodId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;");
    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }

}
