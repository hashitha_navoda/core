<?php

use Phinx\Migration\AbstractMigration;

class UpdateFeatureTableForGivePermissionToFinanceReportsSeperatly extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    
    /**
     * Migrate Up.
     */
    public function up()
    {
        $this->execute("UPDATE `feature` SET `featureName` = 'Trail Balance CSV and View Report', `featureType` = 0, `featureCategory` = 'Finance Report' WHERE `featureController` = 'Reporting\\\Controller\\\API\\\FinanceReport' AND `featureAction` = 'trialBalance'");
        $this->execute("UPDATE `feature` SET `featureName` = 'Trail Balance PDF Report', `featureType` = 0, `featureCategory` = 'Finance Report' WHERE `featureController` = 'Reporting\\\Controller\\\API\\\FinanceReport' AND `featureAction` = 'generateTrialBalancePdf'");
        $this->execute("UPDATE `feature` SET `featureName` = 'Balance Sheet CSV and View Report', `featureType` = 0, `featureCategory` = 'Finance Report' WHERE `featureController` = 'Reporting\\\Controller\\\API\\\FinanceReport' AND `featureAction` = 'balanceSheet'");
        $this->execute("UPDATE `feature` SET `featureName` = 'Balance Sheet PDF Report', `featureType` = 0, `featureCategory` = 'Finance Report' WHERE `featureController` = 'Reporting\\\Controller\\\API\\\FinanceReport' AND `featureAction` = 'generateBalanceSheetPdf'");
        $this->execute("UPDATE `feature` SET `featureName` = 'Profit and Loss CSV and View Report', `featureType` = 0, `featureCategory` = 'Finance Report' WHERE `featureController` = 'Reporting\\\Controller\\\API\\\FinanceReport' AND `featureAction` = 'profitAndLost'");
        $this->execute("UPDATE `feature` SET `featureName` = 'Proft and Loss PDF Report', `featureType` = 0, `featureCategory` = 'Finance Report' WHERE `featureController` = 'Reporting\\\Controller\\\API\\\FinanceReport' AND `featureAction` = 'generateProfitAndLostPdf'");
        $this->execute("UPDATE `feature` SET `featureName` = 'General Ledger CSV and View Report', `featureType` = 0, `featureCategory` = 'Finance Report' WHERE `featureController` = 'Reporting\\\Controller\\\API\\\FinanceReport' AND `featureAction` = 'generalLedger'");
        $this->execute("UPDATE `feature` SET `featureName` = 'General Ledger PDF Report', `featureType` = 0, `featureCategory` = 'Finance Report' WHERE `featureController` = 'Reporting\\\Controller\\\API\\\FinanceReport' AND `featureAction` = 'generateGeneralLedgerPdf'");
        $this->execute("UPDATE `feature` SET `featureName` = 'Default', `featureType` = 1 WHERE `featureController` = 'Reporting\\\Controller\\\FinanceReport' AND `featureAction` = 'index'");
    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }
}
