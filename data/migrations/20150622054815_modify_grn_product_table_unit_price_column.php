<?php

use Phinx\Migration\AbstractMigration;

class ModifyGrnProductTableUnitPriceColumn extends AbstractMigration
{

    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     */
    public function change()
    {
        $this->execute("SET SESSION old_alter_table=1; "
                . "ALTER TABLE `grnProduct` ADD `grnProductUom` INT NULL AFTER `grnProductQuantity` ;"
                . "ALTER TABLE `grnProduct` CHANGE `grnProductPrice` `grnProductPrice` DECIMAL( 12, 5 ) NULL DEFAULT '0.00';");
    }

}
