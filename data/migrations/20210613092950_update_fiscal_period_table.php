<?php

use Phinx\Migration\AbstractMigration;

class UpdateFiscalPeriodTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $pdo = $this->getAdapter()->getConnection();
        $fiscalPeriods = $this->fetchAll("SELECT * FROM `fiscalPeriod` WHERE `fiscalPeriodParentID` IS NULL");


        if (sizeof($fiscalPeriods) > 0) {
            foreach ($fiscalPeriods as $key => $value) {
                $startDate = date('Y-m-d', strtotime($value['fiscalPeriodStartDate']));;
                $endDate = date('Y-m-d', strtotime($value['fiscalPeriodEndDate']));


                $qryString = "SELECT * FROM `journalEntry` WHERE `journalEntryDate` BETWEEN '".$startDate."' AND '".$endDate."' AND journalEntryComment LIKE 'Created by Year End Closing on%' AND yearEndClosed = 1 LIMIT 10";

                $jes = $this->fetchAll($qryString);
                $fiscalPeriodID = $value['fiscalPeriodID'];                
                if (sizeof($jes) == 0) {
                    $this->execute("UPDATE `fiscalPeriod` SET `isCompleteStepOne` = false , `isCompleteStepTwo` = false WHERE `fiscalPeriodID` = $fiscalPeriodID "); 
                }
            }
        }
    }
}
