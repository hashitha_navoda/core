<?php

use Phinx\Migration\AbstractMigration;

class AddNewTableCalledSmsSetting extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->hasTable('smsConfiguration');

        if (!$table) {
            $table = $this->table('smsConfiguration', ['id' => 'smsConfigurationId']);
            $table->addColumn('userName', 'string', ['limit' => 200]);
            $table->addColumn('password', 'string', ['limit' => 200]);
            $table->addColumn('alias', 'string', ['limit' => 200]);
            $table->save();
        }
    }
}
