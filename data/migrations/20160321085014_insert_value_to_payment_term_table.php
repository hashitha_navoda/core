<?php

use Phinx\Migration\AbstractMigration;

class InsertValueToPaymentTermTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {

        $pdo = $this->getAdapter()->getConnection();
        
        $dataArray = array(
            array(
                'paymentTermID' => '7',
                'paymentTermName' => 'Within 21 Days',
                'paymentTermDescription' => NULL
            ),
             array(
                'paymentTermID' => '8',
                'paymentTermName' => 'Within 28 Days',
                'paymentTermDescription' => NULL
            ),
              array(
                'paymentTermID' => '9',
                'paymentTermName' => 'Within 45 Days',
                'paymentTermDescription' => NULL
            ),
        );

        foreach($dataArray as $data){

            $dataInsert = <<<SQL
                INSERT INTO `paymentTerm` 
                (
                    `paymentTermID`, 
                    `paymentTermName`, 
                    `paymentTermDescription`
                ) VALUES (
                    :paymentTermID, 
                    :paymentTermName, 
                    :paymentTermDescription
                );
    
SQL;
    
            $pdo->prepare($dataInsert)->execute(array(
                'paymentTermID' => $data['paymentTermID'], 
                'paymentTermName' => $data['paymentTermName'], 
                'paymentTermDescription' => $data['paymentTermDescription'], 
            ));
        }
    }
}
