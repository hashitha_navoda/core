<?php

use Phinx\Migration\AbstractMigration;

class CloseAllPaidSalesInvoices extends AbstractMigration
{
    /**
    * this migration use to close invoices that already paid but not closed. 
    **/
     
    public function change()
    {
        // get all invoices that invoice amount + credit note amount and paid amout difference less that 0.01 and alredy open (or overdue)
        $invoiceWithCreditNoteSet = $this->query("SELECT si.salesInvoiceID,si.salesinvoiceTotalAmount,si.salesInvoicePayedAmount,cn.creditNoteTotal,si.statusID from salesInvoice as si left join creditNote as cn on si.salesInvoiceID = cn.invoiceID where si.statusID in (3,6) and cn.statusID != 5 and (si.salesinvoiceTotalAmount- (si.salesInvoicePayedAmount + cn.creditNoteTotal)) < 0.01 and si.salesinvoiceTotalAmount != 0")->fetchAll();
        $invoiceIDArray = []; 
        foreach ($invoiceWithCreditNoteSet as $key => $invoiceWithCNData) {
                $invoiceIDArray[] = $invoiceWithCNData['salesInvoiceID'];
        }

        // get all invoices that invoice amount - paid amount < 001 
        $invoiceOnlyDetails = $this->query("SELECT si.salesInvoiceID,si.salesinvoiceTotalAmount,si.salesInvoicePayedAmount,si.statusID from salesInvoice as si where si.statusID in (3,6) and (si.salesinvoiceTotalAmount- (si.salesInvoicePayedAmount)) < 0.01 and si.salesinvoiceTotalAmount != 0")->fetchAll();
                
        foreach ($invoiceOnlyDetails as $key => $onlySIDetails) {
                $invoiceIDArray[] = $onlySIDetails['salesInvoiceID'];
        }

        if (sizeof($invoiceIDArray) > 0) {
            // now update salesInvoice 
            $updateTable = $this->query("UPDATE `salesInvoice` SET `statusID` = '4' WHERE `salesInvoiceID` in (" . implode(', ', $invoiceIDArray) ." )");
        }
    }
}
