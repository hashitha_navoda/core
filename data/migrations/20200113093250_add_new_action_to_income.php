<?php

use Phinx\Migration\AbstractMigration;

class AddNewActionToIncome extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $pdo = $this->getAdapter()->getConnection();
        $rows = $this->fetchAll("SELECT roleID FROM `role`");

        $dataArray = array(
            array(
                'featureID' => 'NULL',
                'featureName' => 'Default',
                'featureController' => 'Accounting\Controller\API\Income',
                'featureAction' => 'getIncomeReferenceForLocation',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '12'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'Default',
                'featureController' => 'Accounting\Controller\API\Income',
                'featureAction' => 'saveIncome',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '12'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'Default',
                'featureController' => 'Accounting\Controller\API\Income',
                'featureAction' => 'editIncome',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '12'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'Default',
                'featureController' => 'Accounting\Controller\API\Income',
                'featureAction' => 'retriveCustomerIncome',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '12'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'Default',
                'featureController' => 'Accounting\Controller\API\Income',
                'featureAction' => 'searchIncomeForDropdown',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '12'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'Default',
                'featureController' => 'Accounting\Controller\API\Income',
                'featureAction' => 'getIncomeFromSearch',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '12'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'Default',
                'featureController' => 'Accounting\Controller\API\Income',
                'featureAction' => 'getIncomeByDatefilter',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '12'
            ),
             array(
                'featureID' => 'NULL',
                'featureName' => 'Create',
                'featureController' => 'Accounting\Controller\Income',
                'featureAction' => 'create',
                'featureCategory' => 'Income',
                'featureType' => '0',
                'moduleID' => '12'
            ),
             array(
                'featureID' => 'NULL',
                'featureName' => 'List',
                'featureController' => 'Accounting\Controller\Income',
                'featureAction' => 'list',
                'featureCategory' => 'Income',
                'featureType' => '0',
                'moduleID' => '12'
            ),
             array(
                'featureID' => 'NULL',
                'featureName' => 'Edit',
                'featureController' => 'Accounting\Controller\Income',
                'featureAction' => 'edit',
                'featureCategory' => 'Income',
                'featureType' => '0',
                'moduleID' => '12'
            ),
             array(
                'featureID' => 'NULL',
                'featureName' => 'Preview',
                'featureController' => 'Accounting\Controller\Income',
                'featureAction' => 'preview',
                'featureCategory' => 'Income',
                'featureType' => '0',
                'moduleID' => '12'
            ),
             array(
                'featureID' => 'NULL',
                'featureName' => 'document',
                'featureController' => 'Accounting\Controller\Income',
                'featureAction' => 'document',
                'featureCategory' => 'Income',
                'featureType' => '0',
                'moduleID' => '12'
            ),
            
         );

        foreach($dataArray as $feature){

            $featureInsert = <<<SQL
                INSERT INTO `feature`
                (
                    `featureName`,
                    `featureController`,
                    `featureAction`,
                    `featureCategory`,
                    `featureType`,
                    `moduleID`
                ) VALUES (
                    :featureName,
                    :featureController,
                    :featureAction,
                    :featureCategory,
                    :featureType,
                    :featureModuleId
                );

SQL;

            $pdo->prepare($featureInsert)->execute(array(
                'featureName' => $feature['featureName'],
                'featureController' => $feature['featureController'],
                'featureAction' => $feature['featureAction'],
                'featureCategory' => $feature['featureCategory'],
                'featureType' => $feature['featureType'],
                'featureModuleId' => $feature['moduleID']
            ));

            $featureID = $this->fetchRow("select LAST_INSERT_ID()")[0];

            foreach ($rows as $row) {
                $roleID = $row['roleID'];

                if ($roleID == 1) {
                    $enabled = 1;
                } else {

                   //for enable api calls for every role
                    if($feature['featureType'] == 1){
                        $enabled = 1;
                    } else {
                        $enabled = 0;
                    }

                }

                $roleFeatureInsert =<<<SQL
                    INSERT INTO `roleFeature`
                    (
                        `roleID`,
                        `featureID`,
                        `roleFeatureEnabled`
                    ) VALUES(
                        :roleID,
                        :featureID,
                        :roleFeatureEnabled
                    )
SQL;
                $pdo->prepare($roleFeatureInsert)->execute(array(
                    'roleID' => $roleID,
                    'featureID' => $featureID,
                    'roleFeatureEnabled' => $enabled
                ));
            }
        }
    }
}
