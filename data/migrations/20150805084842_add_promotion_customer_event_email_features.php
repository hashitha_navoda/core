<?php

use Phinx\Migration\AbstractMigration;

class AddPromotionCustomerEventEmailFeatures extends AbstractMigration
{

    public function up()
    {
        $queries = array(
            array('query' => "INSERT INTO `feature` (`featureID`, `featureName`, `featureController`, `featureAction`, `featureCategory`, `featureType`, `moduleID`) VALUES (NULL, 'Send Customer Event Emails', 'Settings\\\Controller\\\Promotion', 'customerEvents', 'Promotions', '0', '7');", 'type' => '0'),
            array('query' => "INSERT INTO `feature` (`featureID`, `featureName`, `featureController`, `featureAction`, `featureCategory`, `featureType`, `moduleID`) VALUES (NULL, 'Default', 'Settings\\\Controller\\\API\\\CustomerEvent', 'getCustomerEvents', 'Application', '1', '7');", 'type' => '1'),
            array('query' => "INSERT INTO `feature` (`featureID`, `featureName`, `featureController`, `featureAction`, `featureCategory`, `featureType`, `moduleID`) VALUES (NULL, 'Default', 'Settings\\\Controller\\\API\\\CustomerEvent', 'getCustomersForCustomerEvents', 'Application', '1', '7');", 'type' => '1'),
            array('query' => "INSERT INTO `feature` (`featureID`, `featureName`, `featureController`, `featureAction`, `featureCategory`, `featureType`, `moduleID`) VALUES (NULL, 'Default', 'Settings\\\Controller\\\API\\\Promotion', 'savePromotionEmailCustomerEvents', 'Application', '1', '7');", 'type' => '1'),
        );
        $rows = $this->fetchAll("SELECT roleID FROM `role`");
        foreach ($queries as $query) {
            $this->execute($query['query']);
            $id = $this->fetchRow("select LAST_INSERT_ID()")[0];
            foreach ($rows as $row) {
                $roleID = $row['roleID'];
                if ($roleID == 1) {
                    $enabled = 1;
                } else {
                    if ($query['type'] == '0') {
                        $enabled = 0;
                    } else {
                        $enabled = 1;
                    }
                }
                $this->execute("INSERT INTO `roleFeature` (`roleFeatureID`, `roleID`, `featureID`, `roleFeatureEnabled`) VALUES (NULL, $roleID, $id,$enabled);");
            }
        }
    }

}
