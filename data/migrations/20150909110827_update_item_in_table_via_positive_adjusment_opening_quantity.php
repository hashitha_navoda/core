<?php

use Phinx\Migration\AbstractMigration;

class UpdateItemInTableViaPositiveAdjusmentOpeningQuantity extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
      public function change()
      {
      }
     */

    /**
     * Migrate Up.
     */
    public function up()
    {
        $rows = $this->fetchAll("SELECT * FROM `goodsIssue` INNER JOIN goodsIssueProduct ON goodsIssue.goodsIssueID=goodsIssueProduct.goodsIssueID WHERE goodsIssueReason='Adding opening Quantity'; ");
        foreach ($rows as $val) {
            $price = $val['unitOfPrice'];
            $gid = $val['goodsIssueID'];
            $this->execute("UPDATE  `itemIn` SET `itemInPrice`={$price} WHERE `itemInDocumentID`={$gid} AND `itemInDocumentType`='Inventory Positive Adjustment';");
        }
    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }

}
