<?php

use Phinx\Migration\AbstractMigration;

class AddNewColumnToThePaymentVoucherProductTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
      public function change()
      {
      }
     */

    /**
     * Migrate Up.
     */
    public function up()
    {
        $table = $this->table('paymentVoucherProduct');
        $columnExits = $table->hasColumn('paymentVoucherProductDiscount');
        if (!$columnExits) {
            $table->addColumn('paymentVoucherProductDiscount', 'float', array('null' => 'allow', 'after' => 'paymentVoucherProductTotalPrice'))
                    ->update();
        }
    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }

}
