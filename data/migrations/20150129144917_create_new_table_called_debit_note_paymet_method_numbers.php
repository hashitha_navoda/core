<?php

use Phinx\Migration\AbstractMigration;

class CreateNewTableCalledDebitNotePaymetMethodNumbers extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
      public function change()
      {
      }
     */

    /**
     * Migrate Up.
     */
    public function up()
    {
        $this->execute("CREATE TABLE IF NOT EXISTS `debitNotePaymentMethodNumbers` (
  `debitNotePaymentMethodNumbersID` int(11) NOT NULL AUTO_INCREMENT,
  `debitNotePaymentID` int(11) NOT NULL,
  `paymentMethodID` int(11) NOT NULL,
  `debitNotePaymentMethodReferenceNumber` varchar(20) NOT NULL,
  `debitNotePaymentMethodBank` varchar(50) DEFAULT NULL,
  `debitNotePaymentMethodCardID` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`debitNotePaymentMethodNumbersID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;");
    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }

}
