<?php

use Phinx\Migration\AbstractMigration;

class AddNewColumCalledEntityIdInJobTypeTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
      public function change()
      {
      }
     */

    /**
     * Migrate Up.
     */
    public function up()
    {
        $this->execute("SET SESSION old_alter_table=1; ALTER TABLE `jobType` ADD `entityID` INT( 11 ) NOT NULL AFTER `jobTypeCode` ,
ADD UNIQUE (
`entityID`
);");
        $this->execute("SET SESSION old_alter_table=1;ALTER TABLE `jobType` CHANGE `jobTypeId` `jobTypeId` INT( 11 ) NOT NULL AUTO_INCREMENT ;");
    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }

}
