<?php

use Phinx\Migration\AbstractMigration;

class AddNewFeatureForDefaultReference extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
      public function change()
      {
      }
     */

    /**
     * Migrate Up.
     */
    public function up()
    {
        $this->execute("INSERT INTO `feature`(`featureID`, `featureName`, `featureController`, `featureAction`, `featureCategory`, `featureType`, `moduleID`) VALUES (NULL, 'Default', 'Settings\\\Controller\\\CompanyAPI', 'getLocationPrefixDataForPreview', 'Application', '1', '7');");
        $id = $this->fetchRow("select LAST_INSERT_ID()")[0];
        $this->execute("INSERT INTO `roleFeature` (`roleFeatureID` ,`roleID` ,`featureID` ,`roleFeatureEnabled`)
                                VALUES (NULL , '1', $id, '1');");
        $this->execute("INSERT INTO `feature` (`featureID`, `featureName`, `featureController`, `featureAction`, `featureCategory`, `featureType`, `moduleID`) VALUES (NULL, 'Default', 'Settings\\\Controller\\\CompanyAPI', 'getGlobalPrefixData', 'Application', '1', '7');");
        $id = $this->fetchRow("select LAST_INSERT_ID()")[0];
        $this->execute("INSERT INTO `roleFeature` (`roleFeatureID` ,`roleID` ,`featureID` ,`roleFeatureEnabled`)
                                VALUES (NULL , '1', $id, '1');");
        $this->execute("INSERT INTO `feature`(`featureID`, `featureName`, `featureController`, `featureAction`, `featureCategory`, `featureType`, `moduleID`) VALUES (NULL, 'Default', 'Settings\\\Controller\\\CompanyAPI', 'getGlobalLocalType', 'Application', '1', '7');");
        $id = $this->fetchRow("select LAST_INSERT_ID()")[0];
        $this->execute("INSERT INTO `roleFeature` (`roleFeatureID` ,`roleID` ,`featureID` ,`roleFeatureEnabled`)
                                VALUES (NULL , '1', $id, '1');");
    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }

}
