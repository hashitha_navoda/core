<?php

use Phinx\Migration\AbstractMigration;

class AddFeaturesForLoyaltyCardChanges extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
      public function change()
      {
      }
     */

    /**
     * Migrate Up.
     */
    public function up()
    {
        $rows = $this->fetchAll("SELECT roleID FROM `role`");

        // Loyalty Index
        $this->execute("INSERT INTO `feature` (`featureID`, `featureName`, `featureController`, `featureAction`, `featureCategory`, `featureType`, `moduleID`) VALUES (NULL, 'Loyalty Card List View', 'Settings\\\Controller\\\Loyalty', 'index', 'Loyalty Card', '0', '7')");
        $id1 = $this->fetchRow("select LAST_INSERT_ID()")[0];

        foreach ($rows as $row) {
            $roleID = $row['roleID'];
            if ($roleID == 1) {
                $enabled = 1;
            } else {
                $enabled = 0;
            }
            $this->execute("INSERT INTO `roleFeature` (`roleFeatureID`, `roleID`, `featureID`, `roleFeatureEnabled`) VALUES (NULL, $roleID, $id1,$enabled);");
        }

        // Loyalty Create|Edit
        $this->execute("INSERT INTO `feature` (`featureID`, `featureName`, `featureController`, `featureAction`, `featureCategory`, `featureType`, `moduleID`) VALUES (NULL, 'Create Loyalty Card', 'Settings\\\Controller\\\Loyalty', 'create', 'Loyalty Card', '0', '7')");
        $id1 = $this->fetchRow("select LAST_INSERT_ID()")[0];

        foreach ($rows as $row) {
            $roleID = $row['roleID'];
            if ($roleID == 1) {
                $enabled = 1;
            } else {
                $enabled = 0;
            }
            $this->execute("INSERT INTO `roleFeature` (`roleFeatureID`, `roleID`, `featureID`, `roleFeatureEnabled`) VALUES (NULL, $roleID, $id1,$enabled);");
        }

        // Loyalty card customer list
        $this->execute("INSERT INTO `feature` (`featureID`, `featureName`, `featureController`, `featureAction`, `featureCategory`, `featureType`, `moduleID`) VALUES (NULL, 'View loyalty card holders list', 'Settings\\\Controller\\\Loyalty', 'customer-list', 'Loyalty Card', '0', '7')");
        $id1 = $this->fetchRow("select LAST_INSERT_ID()")[0];

        foreach ($rows as $row) {
            $roleID = $row['roleID'];
            if ($roleID == 1) {
                $enabled = 1;
            } else {
                $enabled = 0;
            }
            $this->execute("INSERT INTO `roleFeature` (`roleFeatureID`, `roleID`, `featureID`, `roleFeatureEnabled`) VALUES (NULL, $roleID, $id1,$enabled);");
        }

        // Loyalty Store [AJAX & Form submit]
        $this->execute("INSERT INTO `feature` (`featureID`, `featureName`, `featureController`, `featureAction`, `featureCategory`, `featureType`, `moduleID`) VALUES (NULL, 'Save Loyalty Card', 'Settings\\\Controller\\\Loyalty', 'store', 'Loyalty Card', '1', '7')");
        $id1 = $this->fetchRow("select LAST_INSERT_ID()")[0];

        foreach ($rows as $row) {
            $roleID = $row['roleID'];
            $enabled = 1;
            $this->execute("INSERT INTO `roleFeature` (`roleFeatureID`, `roleID`, `featureID`, `roleFeatureEnabled`) VALUES (NULL, $roleID, $id1,$enabled);");
        }

        // Get loyalty card details [AJAX]
        $this->execute("INSERT INTO `feature` (`featureID`, `featureName`, `featureController`, `featureAction`, `featureCategory`, `featureType`, `moduleID`) VALUES (NULL, 'Get Loyalty Card details', 'Pos\\\Controller\\\API\\\API', 'getCustomerLoyaltyCard', 'Pos', '1', '6')");
        $id1 = $this->fetchRow("select LAST_INSERT_ID()")[0];

        foreach ($rows as $row) {
            $roleID = $row['roleID'];
            $enabled = 1;
            $this->execute("INSERT INTO `roleFeature` (`roleFeatureID`, `roleID`, `featureID`, `roleFeatureEnabled`) VALUES (NULL, $roleID, $id1,$enabled);");
        }
    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }

}
