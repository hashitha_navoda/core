<?php

use Phinx\Migration\AbstractMigration;

class UpdateUserRoles extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
      public function change()
      {
      }
     */

    /**
     * Migrate Up.
     */
    public function up()
    {
        $this->execute("INSERT IGNORE INTO `module` (`moduleID` ,`moduleName`)VALUES ('10', 'CRM');");
        $this->execute("INSERT IGNORE INTO `module` (`moduleID` ,`moduleName`)VALUES ('11', 'Purchasing');");
        $this->execute("UPDATE `feature` SET `featureCategory` = 'Update Progress' WHERE `featureController` = 'JobCard\\\Controller\\\Progress' AND `featureAction` = 'create'");
        $this->execute("UPDATE `feature` SET `featureCategory` = 'Update Progress' WHERE `featureController` = 'JobCard\\\Controller\\\API\\\Progress' AND `featureAction` = 'updateActivity'");
        $this->execute("UPDATE `feature` SET `featureCategory` = 'Customer Payment' WHERE `featureController` = 'Invoice\\\Controller\\\CustomerPayments' AND `featureAction` = 'document'");
        $this->execute("UPDATE `feature` SET `featureCategory` = 'Customer Payment' WHERE `featureController` = 'Invoice\\\Controller\\\CustomerPayments' AND `featureAction` = 'viewReceipt'");
        $this->execute("UPDATE `feature` SET `moduleID` = 2,`featureCategory` = 'Location Wise Item Minimum Inventory Level Report' WHERE `featureController` = 'Reporting\\\Controller\\\API\\\StockInHandReport' AND `featureAction` = 'viewItemWiseMinimumInventoryLevel'");
        $this->execute("UPDATE `feature` SET `moduleID` = 2,`featureCategory` = 'Location Wise Item Minimum Inventory Level Report' WHERE `featureController` = 'Reporting\\\Controller\\\API\\\StockInHandReport' AND `featureAction` = 'generateItemWiseMinimumInventoryLevelPdf'");
        $this->execute("UPDATE `feature` SET `moduleID` = 2,`featureCategory` = 'Location Wise Item Minimum Inventory Level Report' WHERE `featureController` = 'Reporting\\\Controller\\\API\\\StockInHandReport' AND `featureAction` = 'generateItemWiseMinimumInventoryLevelSheet'");
        $this->execute("UPDATE `feature` SET `moduleID` = 2,`featureCategory` = 'Location Wise Category Minimum Inventory Level Report' WHERE `featureController` = 'Reporting\\\Controller\\\API\\\StockInHandReport' AND `featureAction` = 'viewCategoryWiseMinimumInventoryLevel'");
        $this->execute("UPDATE `feature` SET `moduleID` = 2,`featureCategory` = 'Location Wise Category Minimum Inventory Level Report' WHERE `featureController` = 'Reporting\\\Controller\\\API\\\StockInHandReport' AND `featureAction` = 'generateCategoryWiseMinimumInventoryLevelPdf'");
        $this->execute("UPDATE `feature` SET `moduleID` = 2,`featureCategory` = 'Location Wise Category Minimum Inventory Level Report' WHERE `featureController` = 'Reporting\\\Controller\\\API\\\StockInHandReport' AND `featureAction` = 'generateCategoryWiseMinimumInventoryLevelSheet'");
        $this->execute("UPDATE `feature` SET `moduleID` = 11,`featureCategory` = 'Supplier Wise Item Details Report' WHERE `featureController` = 'Reporting\\\Controller\\\API\\\SupplierReport' AND `featureAction` = 'supplierWiseItemDetails'");
        $this->execute("UPDATE `feature` SET `moduleID` = 11,`featureCategory` = 'Supplier Wise Item Details Report' WHERE `featureController` = 'Reporting\\\Controller\\\API\\\SupplierReport' AND `featureAction` = 'generateSupplierWiseItemSheet'");
        $this->execute("UPDATE `feature` SET `moduleID` = 11,`featureCategory` = 'Supplier Wise Item Details Report' WHERE `featureController` = 'Reporting\\\Controller\\\API\\\SupplierReport' AND `featureAction` = 'generateSupplierWiseItemPdf'");
        $this->execute("UPDATE `feature` SET `moduleID` = 11,`featureCategory` = 'Supplier Details Report' WHERE `featureController` = 'Reporting\\\Controller\\\SupplierReport' AND `featureAction` = 'index'");
        $this->execute("UPDATE `feature` SET `moduleID` = 11,`featureCategory` = 'Supplier Details Report' WHERE `featureController` = 'Reporting\\\Controller\\\API\\\SupplierReport' AND `featureAction` = 'viewSupplierDetails'");
        $this->execute("UPDATE `feature` SET `moduleID` = 11,`featureCategory` = 'Supplier Details Report' WHERE `featureController` = 'Reporting\\\Controller\\\API\\\SupplierReport' AND `featureAction` = 'generateSupplierDetailsSheet'");
        $this->execute("UPDATE `feature` SET `moduleID` = 11,`featureCategory` = 'Supplier Details Report' WHERE `featureController` = 'Reporting\\\Controller\\\API\\\SupplierReport' AND `featureAction` = 'generateSupplierDetailsPdf'");
        $this->execute("UPDATE `feature` SET `moduleID` = 7,`featureCategory` = 'Uom' WHERE `featureController` = 'Settings\\\Controller\\\Uom' AND `featureAction` = 'index'");

        $expence_Array = array(
            array('Reporting\\\Controller\\\ReconciliationReport', 'index'),
            array('Reporting\\\Controller\\\API\\\ReconciliationReport', 'generateAccountReconciliationCsv'),
            array('Reporting\\\Controller\\\API\\\ReconciliationReport', 'generateAccountReconciliationPdf'),
            array('Reporting\\\Controller\\\API\\\ReconciliationReport', 'generateAccountReconciliation'),
            array('Reporting\\\Controller\\\ChequeReport', 'index'),
            array('Reporting\\\Controller\\\API\\\ChequeReport', 'generateChequeReportCsv'),
            array('Reporting\\\Controller\\\API\\\ChequeReport', 'generateChequeReportPdf'),
            array('Reporting\\\Controller\\\API\\\ChequeReport', 'generateChequeReport'),
            array('Reporting\\\Controller\\\API\\\PettyCashVoucherReport', 'generateCsv'),
            array('Reporting\\\Controller\\\API\\\PettyCashVoucherReport', 'generatePdf'),
            array('Reporting\\\Controller\\\API\\\PettyCashVoucherReport', 'viewReport'),
            array('Reporting\\\Controller\\\PaymentVoucherReport', 'index'),
            array('Reporting\\\Controller\\\Generate\\\PaymentVoucherReport', 'generatePaymentVoucherDetailsSheet'),
            array('Reporting\\\Controller\\\Generate\\\PaymentVoucherReport', 'generatePaymentVoucherDetailsPdf'),
            array('Reporting\\\Controller\\\Generate\\\PaymentVoucherReport', 'viewPaymentVoucherDetails'),
            array('Reporting\\\Controller\\\API\\\ExpensePaymentVoucherReport', 'viewReport'),
            array('Reporting\\\Controller\\\API\\\ExpensePaymentVoucherReport', 'generetePdf'),
            array('Reporting\\\Controller\\\API\\\ExpensePaymentVoucherReport', 'generateCsv'),
        );

        foreach ($expence_Array as $expenceValue) {
            $this->execute("UPDATE `feature` SET `moduleID` = 9 WHERE `featureController` = '{$expenceValue[0]}' AND `featureAction` = '{$expenceValue[1]}'");
        }

        $crm_Array = array(
            array('Invoice\\\Controller\\\Customer', 'add'),
            array('Invoice\\\Controller\\\CustomerAPI', 'deleteCustomer'),
            array('Invoice\\\Controller\\\CustomerAPI', 'updatecustomer'),
            array('Invoice\\\Controller\\\Customer', 'edit'),
            array('Invoice\\\Controller\\\Customer', 'import'),
            array('Invoice\\\Controller\\\Customer', 'index'),
            array('Invoice\\\Controller\\\Customer', 'massedit'),
            array('Invoice\\\Controller\\\Customer', 'salesCustomerAdd'),
            array('Invoice\\\Controller\\\Customer', 'salesCustomerEdit'),
            array('Invoice\\\Controller\\\Customer', 'salesCustomerIndex'),
            array('Invoice\\\Controller\\\CustomerCategory', 'index'),
            array('Settings\\\Controller\\\Loyalty', 'create'),
            array('Settings\\\Controller\\\Loyalty', 'getAvailableLoyaltyCodes'),
            array('Settings\\\Controller\\\Loyalty', 'index'),
            array('Settings\\\Controller\\\Loyalty', 'store'),
            array('Settings\\\Controller\\\Loyalty', 'customer-list'),
            array('Settings\\\Controller\\\Promotion', 'create'),
            array('Settings\\\Controller\\\Promotion', 'edit'),
            array('Settings\\\Controller\\\Promotion', 'list'),
            array('Settings\\\Controller\\\Promotion', 'customerEvents'),
            array('Settings\\\Controller\\\Promotion', 'emails'),
            array('Settings\\\Controller\\\Promotion', 'view'),
            array('Settings\\\Controller\\\Promotion', 'viewEmails'),
            array('Settings\\\Controller\\\CustomerEvent', 'index'),
        );

        foreach ($crm_Array as $crmValue) {
            $this->execute("UPDATE `feature` SET `moduleID` = 10 WHERE `featureController` = '{$crmValue[0]}' AND `featureAction` = '{$crmValue[1]}'");
        }

        $job_Card_Array = array(
            array('Reporting\\\Controller\\\JobCardReport', 'index'),
            array('Reporting\\\Controller\\\Generate\\\JobCardReport', 'generateInquiryLogSheet'),
            array('Reporting\\\Controller\\\Generate\\\JobCardReport', 'generateInquiryLogPdf'),
            array('Reporting\\\Controller\\\Generate\\\JobCardReport', 'viewInquiryLog'),
            array('Reporting\\\Controller\\\Generate\\\JobCardReport', 'generateWeightageSheet'),
            array('Reporting\\\Controller\\\Generate\\\JobCardReport', 'generateWeightagePdf'),
            array('Reporting\\\Controller\\\Generate\\\JobCardReport', 'viewWeightage'),
            array('Reporting\\\Controller\\\Generate\\\JobCardReport', 'generateJobCardProgressSheet'),
            array('Reporting\\\Controller\\\Generate\\\JobCardReport', 'generateJobCardProgressPdf'),
            array('Reporting\\\Controller\\\Generate\\\JobCardReport', 'viewJobCardProgress'),
            array('Reporting\\\Controller\\\Generate\\\JobCardReport', 'viewJobCardCosting'),
            array('Reporting\\\Controller\\\Generate\\\JobCardReport', 'generateJobCardCostingPdf'),
            array('Reporting\\\Controller\\\Generate\\\JobCardReport', 'generateJobCardCostingSheet'),
            array('Reporting\\\Controller\\\Generate\\\JobCardReport', 'viewJobCardDetailCosting'),
            array('Reporting\\\Controller\\\Generate\\\JobCardReport', 'generateJobCardDetailCostingPdf'),
            array('Reporting\\\Controller\\\Generate\\\JobCardReport', 'generateJobCardDetailCostingSheet'),
            array('Reporting\\\Controller\\\Generate\\\JobCardReport', 'viewTemporaryProduct'),
            array('Reporting\\\Controller\\\Generate\\\JobCardReport', 'generateTemporaryProductPdf'),
            array('Reporting\\\Controller\\\Generate\\\JobCardReport', 'generateTemporaryProductSheet'),
            array('Reporting\\\Controller\\\Generate\\\JobCardReport', 'viewActivityWiseTemporaryProduct'),
            array('Reporting\\\Controller\\\Generate\\\JobCardReport', 'generateActivityWiseTemporaryProductPdf'),
            array('Reporting\\\Controller\\\Generate\\\JobCardReport', 'generateActivityWiseTemporaryProductSheet'),
            array('Reporting\\\Controller\\\Generate\\\JobCardReport', 'viewJobCardQuotation'),
            array('Reporting\\\Controller\\\Generate\\\JobCardReport', 'generateJobCardQuotationPdf'),
            array('Reporting\\\Controller\\\Generate\\\JobCardReport', 'generateJobCardQuotationCsv'),
            array('Reporting\\\Controller\\\Generate\\\JobCardReport', 'generateJobCardInvoiceValueSheet'),
            array('Reporting\\\Controller\\\Generate\\\JobCardReport', 'generateJobCardInvoiceValuePdf'),
            array('Reporting\\\Controller\\\Generate\\\JobCardReport', 'viewJobCardInvoiceValue'),
        );

        foreach ($job_Card_Array as $jobCardValue) {
            $this->execute("UPDATE `feature` SET `moduleID` = 8 WHERE `featureController` = '{$jobCardValue[0]}' AND `featureAction` = '{$jobCardValue[1]}'");
        }

        $sales_Array = array(
            array('Reporting\\\Controller\\\API\\\SalesReport', 'viewMonthlySales'),
            array('Reporting\\\Controller\\\API\\\SalesReport', 'getMonthlySalesGraphData'),
            array('Reporting\\\Controller\\\API\\\SalesReport', 'generateMonthlySalesSheet'),
            array('Reporting\\\Controller\\\API\\\SalesReport', 'generateMonthlySalesPdf'),
            array('Reporting\\\Controller\\\API\\\SalesReport', 'viewDailySales'),
            array('Reporting\\\Controller\\\API\\\SalesReport', 'getDailySalesGraphData'),
            array('Reporting\\\Controller\\\API\\\SalesReport', 'generateDailySalesSheet'),
            array('Reporting\\\Controller\\\API\\\SalesReport', 'generateDailySalesPdf'),
            array('Reporting\\\Controller\\\API\\\SalesReport', 'viewAnnualSales'),
            array('Reporting\\\Controller\\\API\\\SalesReport', 'getAnnuvalSalesGraphData'),
            array('Reporting\\\Controller\\\API\\\SalesReport', 'generateAnnualSalesSheet'),
            array('Reporting\\\Controller\\\API\\\SalesReport', 'generateAnnualSalesPdf'),
            array('Reporting\\\Controller\\\API\\\SalesReport', 'viewSalesSummery'),
            array('Reporting\\\Controller\\\API\\\SalesReport', 'generateSalesSummerySheet'),
            array('Reporting\\\Controller\\\API\\\SalesReport', 'generateSalesSummeryPdf'),
            array('Reporting\\\Controller\\\Generate\\\SalesPaymentReport', 'viewPaymentsSummery'),
            array('Reporting\\\Controller\\\Generate\\\SalesPaymentReport', 'generatePaymentsSummeryPdf'),
            array('Reporting\\\Controller\\\Generate\\\SalesPaymentReport', 'generatePaymentsSummerySheet'),
            array('Reporting\\\Controller\\\API\\\SalesReport', 'viewGrossProfit'),
            array('Reporting\\\Controller\\\API\\\SalesReport', 'generateGrossProfitPdf'),
            array('Reporting\\\Controller\\\API\\\SalesReport', 'generateGrossProfitSheet'),
            array('Reporting\\\Controller\\\API\\\SalesReport', 'viewCreditNote'),
            array('Reporting\\\Controller\\\API\\\SalesReport', 'generateCreditNotePdf'),
            array('Reporting\\\Controller\\\API\\\SalesReport', 'generateCreditNoteCsv'),
            array('Reporting\\\Controller\\\API\\\CustomerReport', 'generateAgedCusPdf'),
            array('Reporting\\\Controller\\\API\\\CustomerReport', 'viewAgedCustomerData'),
            array('Reporting\\\Controller\\\API\\\CustomerReport', 'generateAgedCusSheet'),
            array('Reporting\\\Controller\\\API\\\CustomerReport', 'viewAgedAnalysis'),
            array('Reporting\\\Controller\\\API\\\CustomerReport', 'generateAgedAnalysisPdf'),
            array('Reporting\\\Controller\\\API\\\CustomerReport', 'generateAgedAnalysisSheet'),
            array('Reporting\\\Controller\\\API\\\CustomerReport', 'generateCustomerDetailsPdf'),
            array('Reporting\\\Controller\\\API\\\CustomerReport', 'viewCustomerDetails'),
            array('Reporting\\\Controller\\\API\\\CustomerReport', 'generateCustomerDetailsSheet'),
            array('Reporting\\\Controller\\\DailySalesReport', 'viewInvoicedDailySalesItem'),
            array('Reporting\\\Controller\\\DailySalesReport', 'generateInvoicedDailySalesItemsPdf'),
            array('Reporting\\\Controller\\\DailySalesReport', 'generateInvoicedDailySalesItemsSheet'),
            array('Reporting\\\Controller\\\DailySalesReport', 'viewPosDailySalesItems'),
            array('Reporting\\\Controller\\\DailySalesReport', 'generatePosDailySalesItemsPdf'),
            array('Reporting\\\Controller\\\DailySalesReport', 'generatePosDailySalesItemsSheet'),
            array('Reporting\\\Controller\\\DailySalesReport', 'viewPosAndInvoicedDailySalesItems'),
            array('Reporting\\\Controller\\\DailySalesReport', 'generatePosAndInvoiceDailySalesItemsPdf'),
            array('Reporting\\\Controller\\\DailySalesReport', 'generatePosAndInvoicedDailySalesItemsSheet'),
            array('Reporting\\\Controller\\\DailySalesReport', 'generateSerialItemMovementReport'),
            array('Reporting\\\Controller\\\DailySalesReport', 'generateSerialItemMovementPdf'),
            array('Reporting\\\Controller\\\DailySalesReport', 'generateSerialItemMovementCsv'),
            array('Reporting\\\Controller\\\DailySalesReport', 'generateRepresentativeWiseCategoryReport'),
            array('Reporting\\\Controller\\\DailySalesReport', 'generateRepresentativeWiseCategoryPdf'),
            array('Reporting\\\Controller\\\DailySalesReport', 'generateRepresentativeWiseCategoryCsv'),
            array('Reporting\\\Controller\\\DailySalesReport', 'generateRepresentativeWiseItemReport'),
            array('Reporting\\\Controller\\\DailySalesReport', 'generateRepresentativeWiseItemPdf'),
            array('Reporting\\\Controller\\\DailySalesReport', 'generateRepresentativeWiseItemCsv'),
            array('Reporting\\\Controller\\\PosReport', 'index'),
            array('Reporting\\\Controller\\\API\\\PosReport', 'posInvoiceView'),
            array('Reporting\\\Controller\\\API\\\PosReport', 'generatePosInvoicePdf'),
            array('Reporting\\\Controller\\\API\\\PosReport', 'generatePosInvoiceSheet'),
            array('Reporting\\\Controller\\\API\\\PosReport', 'userWiseBalanceSummaryView'),
            array('Reporting\\\Controller\\\API\\\PosReport', 'generatePosBalanceSummaryPdf'),
            array('Reporting\\\Controller\\\API\\\PosReport', 'generatePosBalanceSummarySheet'),
            array('Reporting\\\Controller\\\API\\\PosReport', 'posPaymentView'),
            array('Reporting\\\Controller\\\API\\\PosReport', 'generatePosPaymentPdf'),
            array('Reporting\\\Controller\\\API\\\PosReport', 'generatePosPaymentSheet'),
            array('Reporting\\\Controller\\\DeliveryReport', 'index'),
            array('Reporting\\\Controller\\\Generate\\\DeliveryReport', 'dailyDeliveryValueView'),
            array('Reporting\\\Controller\\\Generate\\\DeliveryReport', 'generateDailyDeliveryValuePdf'),
            array('Reporting\\\Controller\\\Generate\\\DeliveryReport', 'generateDailyDeliveryValueSheet'),
            array('Reporting\\\Controller\\\Generate\\\DeliveryReport', 'dailyDeliveryItemView'),
            array('Reporting\\\Controller\\\Generate\\\DeliveryReport', 'generateDailyDeliveryItemPdf'),
            array('Reporting\\\Controller\\\Generate\\\DeliveryReport', 'generateDailyDeliveryItemSheet'),
            array('Reporting\\\Controller\\\Generate\\\DeliveryReport', 'invoicedDeliveryNoteValuesView'),
            array('Reporting\\\Controller\\\Generate\\\DeliveryReport', 'invoicedDeliveryNoteValuesPdf'),
            array('Reporting\\\Controller\\\Generate\\\DeliveryReport', 'invoicedDeliveryNoteValuesSheet'),
            array('Reporting\\\Controller\\\Generate\\\DeliveryReport', 'invoicedDeliveryNoteItemsView'),
            array('Reporting\\\Controller\\\Generate\\\DeliveryReport', 'invoicedDeliveryNoteItemsPdf'),
            array('Reporting\\\Controller\\\Generate\\\DeliveryReport', 'invoicedDeliveryNoteItemsSheet'),
            array('Reporting\\\Controller\\\Generate\\\DeliveryReport', 'notInvoicedDeliveryNoteValuesView'),
            array('Reporting\\\Controller\\\Generate\\\DeliveryReport', 'notInvoicedDeliveryNoteValuesPdf'),
            array('Reporting\\\Controller\\\Generate\\\DeliveryReport', 'notInvoicedDeliveryNoteValuesSheet'),
            array('Reporting\\\Controller\\\Generate\\\DeliveryReport', 'notInvoiceDeliveryItemView'),
            array('Reporting\\\Controller\\\Generate\\\DeliveryReport', 'generateNotInvoiceDeliveryItemPdf'),
            array('Reporting\\\Controller\\\Generate\\\DeliveryReport', 'generateNotInvoiceDeliveryItemSheet'),
            array('Reporting\\\Controller\\\API\\\InvoiceReport', 'monthlyInvoiceView'),
            array('Reporting\\\Controller\\\API\\\InvoiceReport', 'monthlyInvoicePdf'),
            array('Reporting\\\Controller\\\API\\\InvoiceReport', 'monthlyInvoiceCsv'),
            array('Reporting\\\Controller\\\API\\\InvoiceReport', 'dailyInvoiceView'),
            array('Reporting\\\Controller\\\API\\\InvoiceReport', 'dailyInvoicePdf'),
            array('Reporting\\\Controller\\\API\\\InvoiceReport', 'dailyInvoiceCsv'),
            array('Reporting\\\Controller\\\API\\\InvoiceReport', 'annualInvoiceView'),
            array('Reporting\\\Controller\\\API\\\InvoiceReport', 'annualInvoicePdf'),
            array('Reporting\\\Controller\\\API\\\InvoiceReport', 'annualInvoiceCsv'),
            array('Reporting\\\Controller\\\API\\\InvoiceReport', 'openInvoiceReport'),
            array('Reporting\\\Controller\\\API\\\InvoiceReport', 'openInvoicePdf'),
            array('Reporting\\\Controller\\\API\\\InvoiceReport', 'openInvoiceCsv'),
            array('Reporting\\\Controller\\\API\\\InvoiceReport', 'viewOutstandingBalance'),
            array('Reporting\\\Controller\\\API\\\InvoiceReport', 'generateOutstandingBalancePdf'),
            array('Reporting\\\Controller\\\API\\\InvoiceReport', 'generateOutstandingBalanceSheet'),
            array('Reporting\\\Controller\\\API\\\InvoiceReport', 'viewCancelledInvoice'),
            array('Reporting\\\Controller\\\API\\\InvoiceReport', 'getCancelledInvoicePdf'),
            array('Reporting\\\Controller\\\API\\\InvoiceReport', 'getCancelledInvoiceSheet'),
            array('Reporting\\\Controller\\\Generate\\\SalesPaymentReport', 'viewCancelledPaymentsSummery'),
            array('Reporting\\\Controller\\\Generate\\\SalesPaymentReport', 'generateCanclledPaymentsSummeryPdf'),
            array('Reporting\\\Controller\\\Generate\\\SalesPaymentReport', 'generateCancelledPaymentsSummerySheet'),
            array('Reporting\\\Controller\\\DailySalesReport', 'index'),
            array('Reporting\\\Controller\\\CustomerReport', 'index'),
            array('Reporting\\\Controller\\\InvoiceReport', 'index'),
            array('Reporting\\\Controller\\\API\\\InvoiceReport', 'summeryInvoiceView'),
            array('Reporting\\\Controller\\\API\\\InvoiceReport', 'summeryInvoicePdf'),
            array('Reporting\\\Controller\\\API\\\InvoiceReport', 'summeryInvoiceCsv'),
            array('Reporting\\\Controller\\\API\\\SalesReport', 'viewPaymentsSummery'),
            array('Reporting\\\Controller\\\API\\\SalesReport', 'generatePaymentsSummerySheet'),
            array('Reporting\\\Controller\\\API\\\SalesReport', 'generatePaymentsSummeryPdf'),
            array('Reporting\\\Controller\\\SalesReport', 'index'),
        );

        foreach ($sales_Array as $salesValue) {
            $this->execute("UPDATE `feature` SET `moduleID` = 3 WHERE `featureController` = '{$salesValue[0]}' AND `featureAction` = '{$salesValue[1]}'");
        }

        $inventory_Array = array(
            array('Reporting\\\Controller\\\API\\\StockInHandReport', 'itemDetailsView'),
            array('Reporting\\\Controller\\\API\\\StockInHandReport', 'generateLocationWiseItemDetailsPdf'),
            array('Reporting\\\Controller\\\API\\\StockInHandReport', 'generateLocationWiseItemDetailsSheet'),
            array('Reporting\\\Controller\\\API\\\StockInHandReport', 'globalItemDetailsView'),
            array('Reporting\\\Controller\\\API\\\StockInHandReport', 'generateGlobalItemDetailsPdf'),
            array('Reporting\\\Controller\\\API\\\StockInHandReport', 'generateGlobalItemDetailsSheet'),
            array('Reporting\\\Controller\\\StockInHandReport', 'index'),
            array('Reporting\\\Controller\\\API\\\StockInHandReport', 'generateItemWiseStockPdf'),
            array('Reporting\\\Controller\\\API\\\StockInHandReport', 'index'),
            array('Reporting\\\Controller\\\API\\\StockInHandReport', 'generateItemWiseStockSheet'),
            array('Reporting\\\Controller\\\API\\\StockInHandReport', 'generateCategoryWiseStockPdf'),
            array('Reporting\\\Controller\\\API\\\StockInHandReport', 'viewCategoryWiseDetails'),
            array('Reporting\\\Controller\\\API\\\StockInHandReport', 'generateCategoryWiseStockSheet'),
            array('Reporting\\\Controller\\\API\\\StockInHandReport', 'generateLocationWiseStockPdf'),
            array('Reporting\\\Controller\\\API\\\StockInHandReport', 'viewLocationWiseStockDetails'),
            array('Reporting\\\Controller\\\API\\\StockInHandReport', 'generateLocationWiseStockSheet'),
            array('Reporting\\\Controller\\\API\\\StockInHandReport', 'viewIssuedInvoice'),
            array('Reporting\\\Controller\\\API\\\StockInHandReport', 'generateIssuedInvoiceItemPdf'),
            array('Reporting\\\Controller\\\API\\\StockInHandReport', 'generateIssuedInvoiceItemSheet'),
            array('Reporting\\\Controller\\\API\\\StockInHandReport', 'viewItemWiseExpire'),
            array('Reporting\\\Controller\\\API\\\StockInHandReport', 'generateItemWiseExpirePdf'),
            array('Reporting\\\Controller\\\API\\\StockInHandReport', 'generateItemWiseExpireSheet'),
            array('Reporting\\\Controller\\\API\\\StockInHandReport', 'viewGlobalStockValue'),
            array('Reporting\\\Controller\\\API\\\StockInHandReport', 'generateGlobalStockValuePdf'),
            array('Reporting\\\Controller\\\API\\\StockInHandReport', 'generateGlobalStockValueSheet'),
            array('Reporting\\\Controller\\\API\\\StockInHandReport', 'locationWiseStockValueView'),
            array('Reporting\\\Controller\\\API\\\StockInHandReport', 'generateLocationWiseStockValuePdf'),
            array('Reporting\\\Controller\\\API\\\StockInHandReport', 'generateLocationWiseStockValueSheet'),
            array('Reporting\\\Controller\\\API\\\StockInHandReport', 'viewGlobalItemMoving'),
            array('Reporting\\\Controller\\\API\\\StockInHandReport', 'generateGlobalItemMovingPdf'),
            array('Reporting\\\Controller\\\API\\\StockInHandReport', 'generateGlobalItemMovingSheet'),
            array('Reporting\\\Controller\\\API\\\StockInHandReport', 'viewGlobalNonInventoryItemMoving'),
            array('Reporting\\\Controller\\\API\\\StockInHandReport', 'generateNonInventoryItemMovingPdf'),
            array('Reporting\\\Controller\\\API\\\StockInHandReport', 'generateNonInventoryItemMovingSheet'),
            array('Reporting\\\Controller\\\API\\\ItemTransferReport', 'view'),
            array('Reporting\\\Controller\\\API\\\ItemTransferReport', 'generateItemTransferPdf'),
            array('Reporting\\\Controller\\\API\\\ItemTransferReport', 'generateItemTransferSheet'),
            array('Reporting\\\Controller\\\ItemTransferReport', 'index'),
            array('Reporting\\\Controller\\\API\\\GrnReport', 'viewGrnStatus'),
            array('Reporting\\\Controller\\\API\\\GrnReport', 'generateGrnStatusSheet'),
            array('Reporting\\\Controller\\\API\\\GrnReport', 'generateGrnStatusPdf'),
            array('Reporting\\\Controller\\\GrnReport', 'index'),
        );

        foreach ($inventory_Array as $inventoryValue) {
            $this->execute("UPDATE `feature` SET `moduleID` = 2 WHERE `featureController` = '{$inventoryValue[0]}' AND `featureAction` = '{$inventoryValue[1]}'");
        }

        $purchasing_Array = array(
            array('Inventory\\\Controller\\\Supplier', 'view'),
            array('Inventory\\\Controller\\\Supplier', 'add'),
            array('Inventory\\\Controller\\\Supplier', 'supplierCategory'),
            array('Inventory\\\Controller\\\PurchaseOrderController', 'document'),
            array('Inventory\\\Controller\\\API\\\PurchaseOrderController', 'sendPoEmail'),
            array('Inventory\\\Controller\\\PurchaseOrderController', 'create'),
            array('Inventory\\\Controller\\\PurchaseOrderController', 'list'),
            array('Inventory\\\Controller\\\PurchaseOrderController', 'view'),
            array('Inventory\\\Controller\\\PurchaseOrderController', 'preview'),
            array('Inventory\\\Controller\\\API\\\PurchaseOrderController', 'changePoStatus'),
            array('Inventory\\\Controller\\\PurchaseInvoiceController', 'document'),
            array('Inventory\\\Controller\\\PurchaseInvoiceController', 'create'),
            array('Inventory\\\Controller\\\PurchaseInvoiceController', 'list'),
            array('Inventory\\\Controller\\\PurchaseInvoiceController', 'view'),
            array('Inventory\\\Controller\\\PurchaseInvoiceController', 'preview'),
            array('Inventory\\\Controller\\\OutGoingPayment', 'viewReceipt'),
            array('Inventory\\\Controller\\\OutGoingPayment', 'document'),
            array('Inventory\\\Controller\\\API\\\OutGoingPayment', 'sendSupplierPaymentEmail'),
            array('Inventory\\\Controller\\\OutGoingPayment', 'edit'),
            array('Inventory\\\Controller\\\OutGoingPayment', 'create'),
            array('Inventory\\\Controller\\\OutGoingPayment', 'view'),
            array('Inventory\\\Controller\\\OutGoingPayment', 'advancePayment'),
            array('Inventory\\\Controller\\\PurchaseReturnsController', 'list'),
            array('Inventory\\\Controller\\\PurchaseReturnsController', 'document'),
            array('Inventory\\\Controller\\\PurchaseReturnsController', 'preview'),
            array('Inventory\\\Controller\\\API\\\PurchaseReturnsController', 'sendPrEmail'),
            array('Inventory\\\Controller\\\PurchaseReturnsController', 'create'),
            array('Inventory\\\Controller\\\DebitNote', 'create'),
            array('Inventory\\\Controller\\\DebitNote', 'view'),
            array('Inventory\\\Controller\\\DebitNote', 'viewDebitNoteReceipt'),
            array('Inventory\\\Controller\\\DebitNote', 'documentPreview'),
            array('Inventory\\\Controller\\\DebitNote', 'document'),
            array('Inventory\\\Controller\\\DebitNotePayments', 'create'),
            array('Inventory\\\Controller\\\DebitNotePayments', 'view'),
            array('Inventory\\\Controller\\\DebitNotePayments', 'viewDebitNotePaymentReceipt'),
            array('Inventory\\\Controller\\\DebitNotePayments', 'documentPreview'),
            array('Inventory\\\Controller\\\DebitNotePayments', 'document'),
            array('Reporting\\\Controller\\\API\\\PurchaseReport', 'viewPurchaseOrder'),
            array('Reporting\\\Controller\\\API\\\PurchaseReport', 'generatePoStatusSheet'),
            array('Reporting\\\Controller\\\PurchaseReport', 'index'),
            array('Reporting\\\Controller\\\API\\\PurchaseReport', 'generatePoStatusPdf'),
            array('Reporting\\\Controller\\\PaymentReport', 'index'),
            array('Reporting\\\Controller\\\API\\\PaymentReport', 'monthlyPaymentView'),
            array('Reporting\\\Controller\\\API\\\PaymentReport', 'monthlyPaymentPdf'),
            array('Reporting\\\Controller\\\API\\\PaymentReport', 'monthlyPaymentCsv'),
            array('Reporting\\\Controller\\\API\\\PaymentReport', 'dailyPaymentView'),
            array('Reporting\\\Controller\\\API\\\PaymentReport', 'dailyPaymentPdf'),
            array('Reporting\\\Controller\\\API\\\PaymentReport', 'dailyPaymentCsv'),
            array('Reporting\\\Controller\\\API\\\PaymentReport', 'annualPaymentView'),
            array('Reporting\\\Controller\\\API\\\PaymentReport', 'annualPaymentPdf'),
            array('Reporting\\\Controller\\\API\\\PaymentReport', 'annualPaymentCsv'),
            array('Reporting\\\Controller\\\API\\\PaymentReport', 'summeryPaymentView'),
            array('Reporting\\\Controller\\\API\\\PaymentReport', 'summeryPaymentPdf'),
            array('Reporting\\\Controller\\\API\\\PaymentReport', 'summeryPaymentCsv'),
            array('Reporting\\\Controller\\\PaymentVoucherReport', 'index'),
            array('Reporting\\\Controller\\\Generate\\\PaymentVoucherReport', 'viewPaymentVoucherDetails'),
            array('Reporting\\\Controller\\\Generate\\\PaymentVoucherReport', 'generatePaymentVoucherDetailsPdf'),
            array('Reporting\\\Controller\\\Generate\\\PaymentVoucherReport', 'generatePaymentVoucherDetailsSheet'),
        );

        foreach ($purchasing_Array as $purchasingValue) {
            $this->execute("UPDATE `feature` SET `moduleID` = 11 WHERE `featureController` = '{$purchasingValue[0]}' AND `featureAction` = '{$purchasingValue[1]}'");
        }

        $setting_Array = array(
            array('Inventory\\\Controller\\\API\\\Category', 'add'),
            array('Inventory\\\Controller\\\API\\\Category', 'changeState'),
            array('Inventory\\\Controller\\\API\\\Category', 'get'),
            array('Inventory\\\Controller\\\Category', 'index'),
        );

        foreach ($setting_Array as $settingValue) {
            $this->execute("UPDATE `feature` SET `moduleID` = 7 WHERE `featureController` = '{$settingValue[0]}' AND `featureAction` = '{$settingValue[1]}'");
        }
    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }

}
