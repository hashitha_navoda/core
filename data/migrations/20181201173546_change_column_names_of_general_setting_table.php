<?php

use Phinx\Migration\AbstractMigration;

class ChangeColumnNamesOfGeneralSettingTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->table('jobGeneralSettings');
        $column1 = $table->hasColumn('maintain_project');
        $column2 = $table->hasColumn('maintain_designation');
        $column3 = $table->hasColumn('maintain_division');
        $column4 = $table->hasColumn('raw_materials');
        $column5 = $table->hasColumn('finished_goods');
        $column6 = $table->hasColumn('fixed_assets');
        $column7 = $table->hasColumn('vehicles');
        $column8 = $table->hasColumn('maintain_contractors');
        $column9 = $table->hasColumn('assign_for_jobs');
        $column10 = $table->hasColumn('assign_for_tasks');
        if ($column1) {
            $table->renameColumn('maintain_project', 'maintainProject')
                ->save();
        }
        if ($column2) {
            $table->renameColumn('maintain_designation', 'maintainDesignation')
                ->save();
        }
        if ($column3) {
            $table->renameColumn('maintain_division', 'maintainDivision')
                ->save();
        }
        if ($column4) {
            $table->renameColumn('raw_materials', 'rawMaterials')
                ->save();
        }
        if ($column5) {
            $table->renameColumn('finished_goods', 'finishedGoods')
                ->save();
        }
        if ($column6) {
            $table->renameColumn('fixed_assets', 'fixedAssets')
                ->save();
        }
        if ($column8) {
            $table->renameColumn('maintain_contractors', 'maintainContractors')
                ->save();
        }
        if ($column9) {
            $table->renameColumn('assign_for_jobs', 'assignForJob')
                ->save();
        }
        if ($column10) {
            $table->renameColumn('assign_for_tasks', 'assignForTask')
                ->save();
        }

    }
}
