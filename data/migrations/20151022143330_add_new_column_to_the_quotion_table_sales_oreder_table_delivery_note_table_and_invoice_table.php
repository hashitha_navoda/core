<?php

use Phinx\Migration\AbstractMigration;

class AddNewColumnToTheQuotionTableSalesOrederTableDeliveryNoteTableAndInvoiceTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
      public function change()
      {
      }
     */

    /**
     * Migrate Up.
     */
    public function up()
    {
        $tableQuo = $this->table('quotation');
        $rowExist = $tableQuo->hasColumn('customerProfileID');
        if (!$rowExist) {
            $tableQuo->addColumn('customerProfileID', 'integer', array('after' => 'quotationStatus', 'null' => 'allow'))
                    ->update();
        }
        $tableSO = $this->table('salesOrders');
        $rowExistSO = $tableSO->hasColumn('customerProfileID');
        if (!$rowExistSO) {
            $tableSO->addColumn('customerProfileID', 'integer', array('after' => 'priceListId', 'null' => 'allow'))
                    ->update();
        }
        $tableDN = $this->table('deliveryNote');
        $rowExistDN = $tableDN->hasColumn('customerProfileID');
        if (!$rowExistDN) {
            $tableDN->addColumn('customerProfileID', 'integer', array('after' => 'priceListId', 'null' => 'allow'))
                    ->update();
        }
        $tableInv = $this->table('salesInvoice');
        $rowExistInv = $tableInv->hasColumn('customerProfileID');
        if (!$rowExistInv) {
            $tableInv->addColumn('customerProfileID', 'integer', array('after' => 'priceListId', 'null' => 'allow'))
                    ->update();
        }
    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }

}
