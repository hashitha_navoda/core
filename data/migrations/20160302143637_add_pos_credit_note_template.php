<?php

use Phinx\Migration\AbstractMigration;

class AddPosCreditNoteTemplate extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
    public function change()
    {
    }
    */
    
    /**
     * Migrate Up.
     */
    public function up()
    {
        $pdo = $this->getAdapter()->getConnection();
        $this->execute("INSERT INTO `documentType` (`documentTypeID`, `documentTypeName`) 
            VALUES (NULL, 'POS Credit Note');");
        $id = $this->fetchRow("select LAST_INSERT_ID()")[0];
            
        $html = <<<HTML
        <table class="table table-bordered" width="100%">
<tbody>
<tr>
<td style="text-align: center;" colspan="2">
<h4><strong>POS CREDIT NOTE</strong></h4>
<h4><strong><abbr contenteditable="false" spellcheck="false">[Company Name]</abbr></strong></h4>
<p style="font-size: 11px;"><abbr contenteditable="false" spellcheck="false">[Company Address]</abbr> <br />Tel:&nbsp; <abbr contenteditable="false" spellcheck="false">[Company Tel. No]</abbr> <br />Email:&nbsp; <abbr contenteditable="false" spellcheck="false">[Company Email]</abbr> <br />Tax No:&nbsp; <abbr contenteditable="false" spellcheck="false">[Company Tax Reg. No]</abbr>&nbsp;</p>
<hr /></td>
</tr>
<tr>
<td colspan="2" rowspan="2">
<table class="table table-bordered table-condensed" width="100%">
<tbody>
<tr>
<td colspan="2" width="46%">Date/Time:</td>
<td style="text-align: right;" colspan="3"><abbr contenteditable="false" spellcheck="false">[Credit Note Date]</abbr>&nbsp;<abbr contenteditable="false" spellcheck="false">[Credit Note Created Time]</abbr>&nbsp;&nbsp;</td>
</tr>
<tr>
<td width="23%">Credit Note Receipt:</td>
<td style="text-align: right;" width="23%"><abbr contenteditable="false" spellcheck="false">[Credit Note Code]</abbr>&nbsp;&nbsp;</td>
<td width="8%">&nbsp;</td>
<td width="23%">User:</td>
<td style="text-align: right;" width="23%"><abbr contenteditable="false" spellcheck="false">[User Name]</abbr>&nbsp;&nbsp;</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
<noneditabletable>
<table class="noneditable table table-bordered" style="width: 100%; margin-bottom: 20px;" title="" contenteditable="false" data-original-title="">
<tbody>
<tr>
<td colspan="2">
<div class="qout_body"><hr />
<table id="item_tbl" class="table table-bordered table-striped" width="100%"><colgroup><col width="10px" /><col width="90px" /><col width="290px" /><col width="50px" /><col width="100px" /><col width="100px" /> </colgroup>
<thead>
<tr><th colspan="2">Product code</th><th colspan="1">Product</th><th colspan="1">Qty</th><th colspan="1">Price</th><th colspan="1">Total</th></tr>
</thead>
<tbody>
<tr>
<td>1</td>
<td align="left">12</td>
<td align="left">dell_lap<br /><small>(Disc. 1.00)</small></td>
<td align="left">1</td>
<td align="right">100.00</td>
<td align="right">99.00</td>
</tr>
</tbody>
</table>
<div class="summary"><br /> <br /> <br /> <br />
<table id="" class="table" width="100%">
<tbody>
<tr>
<td colspan="3">Sub Total</td>
<td class="text-right">99.00</td>
</tr>
<tr>
<td colspan="3">
<h3>Total</h3>
</td>
<td class="text-right">
<h3>99.00</h3>
</td>
</tr>
<!--                                <tr>
                                                                <td colspan="6"><div></div></td>
                                                            </tr>--></tbody>
</table>
</div>
</div>
</td>
</tr>
</tbody>
</table>
</noneditabletable><footer class="doc">
<table class="table table-bordered" width="100%">
<tbody>
<tr>
<td><abbr contenteditable="false" spellcheck="false">[Company Name]</abbr>&nbsp;</td>
<td>&nbsp;</td>
<td style="text-align: right;"><abbr contenteditable="false" spellcheck="false">[Today Date]</abbr>&nbsp;</td>
</tr>
</tbody>
</table>
</footer>
HTML;

        $insert = <<<SQL
            INSERT INTO `template` 
            (
                `templateName`, 
                `documentTypeID`, 
                `documentSizeID`, 
                `templateContent`, 
                `templateFooterDetails`,
                `templateFooterShow`,
                `templateDefault`,
                `templateSample`
            ) VALUES (
                :templateName, 
                :documentTypeID, 
                :documentSizeID,
                :templateContent, 
                :templateFooterDetails, 
                :templateFooterShow, 
                :templateDefault,
                :templateSample
            );
SQL;
    
        $pdo->prepare($insert)->execute(array(
            'templateName' => 'Template', 
            'documentTypeID' => $id, 
            'documentSizeID' => '3', 
            'templateContent' => $html, 
            'templateFooterDetails' => '', 
            'templateFooterShow' => '1',
            'templateDefault' => '1',
            'templateSample' =>'0'
        ));

    }

   /**
     * Migrate Down.
     */
    public function down()
    {

    }
}