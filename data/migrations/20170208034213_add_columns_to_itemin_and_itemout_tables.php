<?php

use Phinx\Migration\AbstractMigration;

class AddColumnsToIteminAndItemoutTables extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        //add column to the item in table
        $inTable = $this->table('itemIn');
        $inColumn = $inTable->hasColumn('itemInDeletedFlag');

        if (!$inColumn) {
            $inTable->addColumn('itemInDeletedFlag', 'boolean', array('after' => 'itemInSoldQty', 'default' => 0))
              ->update();
        }
        //add column to the item out table
        $outTable = $this->table('itemOut');
        $outColumn = $outTable->hasColumn('itemOutDeletedFlag');

        if (!$outColumn) {
            $outTable->addColumn('itemOutDeletedFlag', 'boolean', array('after' => 'itemOutJobCardReferenceID', 'default' => 0))
              ->update();
        }
    }
}
