<?php

use Phinx\Migration\AbstractMigration;

class PosSaveOnlyInvoiceTemplate extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function up()
    {
        $id = 25;
        $pdo = $this->getAdapter()->getConnection();
        $this->execute("INSERT INTO `documentType` (`documentTypeID`, `documentTypeName`) VALUES (". $id .", 'POS (invoice only)');");
            
        $html = <<<HTML
        <table class="table table-bordered" width="100%">
<tbody>
<tr>
<td style="text-align: center;" colspan="2">
<h4><strong><abbr contenteditable="false" spellcheck="false">[Company Name]</abbr></strong></h4>
<p style="font-size: 11px;"><abbr contenteditable="false" spellcheck="false">[Company Address]</abbr> <br />Tel:&nbsp; <abbr contenteditable="false" spellcheck="false">[Company Tel. No]</abbr> <br />Email:&nbsp; <abbr contenteditable="false" spellcheck="false">[Company Email]</abbr> <br />Tax No:&nbsp; <abbr contenteditable="false" spellcheck="false">[Company Tax Reg. No]</abbr>&nbsp;</p>
<hr /></td>
</tr>
<tr>
<td colspan="2" rowspan="2">
<table class="table table-bordered table-condensed" width="100%">
<tbody>
<tr>
<td width="46%" colspan="2">Date/Time:</td>
<td style="text-align: right;" colspan="3"><abbr contenteditable="false" spellcheck="false" id="date">[Sales Inv. Issue Date]</abbr>&nbsp;<abbr contenteditable="false" spellcheck="false" id="time">[Time]</abbr>&nbsp;</td>
</tr>
<tr>
<td width="23%">Receipt:</td>
<td style="text-align: right;" width="23%"><abbr contenteditable="false" spellcheck="false">[Sales Inv. Code]</abbr>&nbsp;</td>
<td width="8%">&nbsp;</td>
<td width="23%">User:</td>
<td style="text-align: right;" width="23%"><abbr contenteditable="false" spellcheck="false">[Current User]</abbr>&nbsp;</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
<noneditabletable>
<table class="noneditable table table-bordered" style="width: 100%; margin-bottom: 20px;" title="" contenteditable="false" data-original-title="">
<tbody>
<tr>
<td colspan="2">
<div class="qout_body"><hr />
<table id="item_tbl" class="table table-bordered table-striped" width="100%"><colgroup><col width="10px" /><col width="90px" /><col width="290px" /><col width="50px" /><col width="100px" /><col width="100px" /> </colgroup>
<thead>
<tr><th colspan="2">Product code</th><th colspan="1">Product</th><th colspan="1">Qty</th><th colspan="1">Price</th><th colspan="1">Total</th></tr>
</thead>
<tbody>
<tr>
<td>1</td>
<td align="left">12</td>
<td align="left">dell_lap<br /><small>(Disc. 1.00)</small></td>
<td align="left">1</td>
<td align="right">100.00</td>
<td align="right">99.00</td>
</tr>
</tbody>
</table>
<div class="summary"><br /> <br /> <br /> <br />
<table id="" class="table" width="100%">
<tbody>
<tr>
<td colspan="3">Sub Total</td>
<td class="text-right">99.00</td>
</tr>
<tr>
<td colspan="3">
<h3>Total</h3>
</td>
<td class="text-right">
<h3>99.00</h3>
</td>
</tr>
<!--                                <tr>
                                                                <td colspan="6"><div></div></td>
                                                            </tr>--></tbody>
</table>
</div>
</div>
</td>
</tr>
</tbody>
</table>
</noneditabletable><footer class="doc">
<table class="table table-bordered" width="100%">
<tbody>
<tr>
<td><abbr contenteditable="false" spellcheck="false">[Company Name]</abbr>&nbsp;</td>
<td>&nbsp;</td>
<td style="text-align: right;"><abbr contenteditable="false" spellcheck="false">[Today Date]</abbr>&nbsp;</td>
</tr>
</tbody>
</table>
</footer>
HTML;

        $insert = <<<SQL
            INSERT INTO `template` 
            (
                `templateName`, 
                `documentTypeID`, 
                `documentSizeID`, 
                `templateContent`, 
                `templateFooterDetails`,
                `templateFooterShow`,
                `templateDefault`,
                `templateSample`
            ) VALUES (
                :templateName, 
                :documentTypeID, 
                :documentSizeID,
                :templateContent, 
                :templateFooterDetails, 
                :templateFooterShow, 
                :templateDefault,
                :templateSample
            );
SQL;
    
        $pdo->prepare($insert)->execute(array(
            'templateName' => 'Template', 
            'documentTypeID' => $id, 
            'documentSizeID' => '3', 
            'templateContent' => $html, 
            'templateFooterDetails' => '', 
            'templateFooterShow' => '1',
            'templateDefault' => '1',
            'templateSample' =>'0'
        ));
    }
}
