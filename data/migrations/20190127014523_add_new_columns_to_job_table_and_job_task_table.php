<?php

use Phinx\Migration\AbstractMigration;

class AddNewColumnsToJobTableAndJobTaskTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->table('job');
        $table2 = $this->table('jobTask');
        $column1 = $table->hasColumn('jobInDate');
        $column2 = $table2->hasColumn('startedAt');
        $column3 = $table2->hasColumn('endedAt');
        $column4 = $table->hasColumn('isRestarted');

        if (!$column1) {
            $table->addColumn('jobInDate', 'date', ['null' => true])
                ->update();
        }
        if (!$column2) {
            $table2->addColumn('startedAt', 'datetime', ['null' => true])
                ->update();
        }

        if (!$column3) {
            $table2->addColumn('endedAt', 'datetime', ['null' => true])
                ->update();
        }

        if (!$column4) {
            $table->addColumn('isRestarted', 'boolean', ['default' => false, 'null' => true])
                ->update();
        }

    }
}
