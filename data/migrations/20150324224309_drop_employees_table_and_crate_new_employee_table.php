<?php

use Phinx\Migration\AbstractMigration;

class DropEmployeesTableAndCrateNewEmployeeTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
      public function change()
      {
      }
     */

    /**
     * Migrate Up.
     */
    public function up()
    {
        $this->execute("DROP TABLE IF EXISTS `employees`;");
        $this->execute("CREATE TABLE IF NOT EXISTS `employee` (
  `employeeID` int(11) NOT NULL AUTO_INCREMENT,
  `employeeFirstName` VARCHAR(100) NOT NULL,
  `employeeSecondName` VARCHAR(100) NOT NULL,
  `designationID` int(11) NOT NULL,
  `divisionID` int(11) NOT NULL,
  `employeeTP` VARCHAR(20) DEFAULT NULL ,
  `employeeHourlyCost` DOUBLE,
  PRIMARY KEY (`employeeID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;");
    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }

}
