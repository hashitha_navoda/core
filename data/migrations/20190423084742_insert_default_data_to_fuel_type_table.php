<?php

use Phinx\Migration\AbstractMigration;

class InsertDefaultDataToFuelTypeTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $pdo = $this->getAdapter()->getConnection();
        
        $dataArray = array(
            array(
                'fuelTypeName' => 'Petrol'
            ),
            array(
                'fuelTypeName' => 'Diesel'
            ),
            array(
                'fuelTypeName' => 'Electric'
            ),
            array(
                'fuelTypeName' => 'Hybrid'
            ),
        );

        foreach($dataArray as $data){

            $dataInsert = <<<SQL
                INSERT INTO `fuelType` 
                (
                    `fuelTypeName` 
                ) VALUES (
                    :fuelTypeName 
                );
    
SQL;
    
            $pdo->prepare($dataInsert)->execute(array(
                'fuelTypeName' => $data['fuelTypeName'] 
            ));
        }
    }
}
