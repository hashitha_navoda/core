<?php

use Phinx\Migration\AbstractMigration;

class AddNewColumnToProductTableAndLocationProductTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->table('product');
        $table2 = $this->table('locationProduct');
        $column = $table->hasColumn('isUseDiscountScheme');
        $column3 = $table->hasColumn('discountSchemID');
        $column2 = $table2->hasColumn('isUseDiscountSchemeForLocation');
        $column4 = $table2->hasColumn('locationDiscountSchemID');
        
        if (!$column) {
            $table->addColumn('isUseDiscountScheme',  'boolean', ['default' => 0, 'null' => true])
                ->update();
        }

        if (!$column3) {
            $table->addColumn('discountSchemID', 'integer', ['limit' => 20, 'default' => null, 'null' => true])
                ->update();
        }

        if (!$column2) {
            $table2->addColumn('isUseDiscountSchemeForLocation', 'boolean', ['default' => 0, 'null' => true])
                ->update();
        }

        if (!$column4) {
            $table2->addColumn('locationDiscountSchemID', 'integer', ['limit' => 20, 'default' => null, 'null' => true])
                ->update();
        }
    }
}
