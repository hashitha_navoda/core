<?php

use Phinx\Migration\AbstractMigration;

class RemoveSomeColoumnsInJobGeneralSettingTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->table('jobGeneralSettings');

        if ($table->hasColumn('maintainDesignation')) {
            $table->removeColumn('maintainDesignation');
        }
        if ($table->hasColumn('maintainDivision')) {
            $table->removeColumn('maintainDivision');
        }
        if ($table->hasColumn('maintainContractors')) {
            $table->removeColumn('maintainContractors');
        }
        if ($table->hasColumn('assignForJob')) {
            $table->renameColumn('assignForJob', 'contractorForJob');
        }
        if ($table->hasColumn('assignForTask')) {
            $table->renameColumn('assignForTask', 'contractorForTask');
        }

        $table->save();
    }
}
