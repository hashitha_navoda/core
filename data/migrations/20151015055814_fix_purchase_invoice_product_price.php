<?php

use Phinx\Migration\AbstractMigration;

class FixPurchaseInvoiceProductPrice extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
    public function change()
    {
    }
    */
    
    /**
     * Migrate Up.
     */
    public function up()
    {
        //get purchaseInvoiceProducts 
        $invoiceProducts = $this->fetchAll("SELECT * FROM purchaseInvoiceProduct");
        foreach ($invoiceProducts as $invoiceProduct){ 
            $productTax = $this->fetchRow("SELECT SUM(`purchaseInvoiceTaxAmount`) AS totalTax FROM `purchaseInvoiceProductTax` WHERE `purchaseInvoiceProductID` = {$invoiceProduct['purchaseInvoiceProductID']}");
            $totalTax = is_null($productTax['totalTax']) ? 0 : $productTax['totalTax'];
            if(( $invoiceProduct['purchaseInvoiceProductTotalQty'] * ( 100 - $invoiceProduct['purchaseInvoiceProductDiscount'])) > 0){
                $productPrice = ( 100 * ($invoiceProduct['purchaseInvoiceProductTotal'] - $totalTax )) / ( $invoiceProduct['purchaseInvoiceProductTotalQty'] * ( 100 - $invoiceProduct['purchaseInvoiceProductDiscount']));
                //update product unit price
                $this->execute("UPDATE `purchaseInvoiceProduct` SET `purchaseInvoiceProductPrice` = {$productPrice} WHERE `purchaseInvoiceProduct`.`purchaseInvoiceProductID` = {$invoiceProduct['purchaseInvoiceProductID']}");
            }
        }
    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }
}