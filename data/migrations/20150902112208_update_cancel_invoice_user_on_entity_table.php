<?php

use Phinx\Migration\AbstractMigration;

class UpdateCancelInvoiceUserOnEntityTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
      public function change()
      {
      }
     */

    /**
     * Migrate Up.
     */
    public function up()
    {
        $itemInData = $this->fetchAll("SELECT * FROM  `itemIn` WHERE  `itemInDocumentType` = 'Invoice Cancel' GROUP BY `itemInDocumentID`");

        foreach ($itemInData as $v) {
            $itemInDocumentID = $v['itemInDocumentID'];
            $logTimeStamp = $v['itemInDateAndTime'];

            if ($logTimeStamp != NULL) {
                $logUserData = $this->fetchAll("SELECT * FROM  `log` WHERE  `logTimeStamp` = '$logTimeStamp';");
                $userID = NULL;

                if (isset($logUserData[0])) {
                    $userID = $logUserData[0]['logUserID'];
                    $salesInvoiceData = $this->fetchAll("SELECT * FROM  `salesInvoice` WHERE  `salesInvoiceID` = $itemInDocumentID;");

                    if (isset($salesInvoiceData[0])) {

                        $entityID = $salesInvoiceData[0]['entityID'];
                        $this->execute("UPDATE `entity` SET `deletedBy` = {$userID}, `deletedTimeStamp` = '{$logTimeStamp}' WHERE `entityID` = {$entityID};");
                    }
                }
            }
        }
    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }

}
