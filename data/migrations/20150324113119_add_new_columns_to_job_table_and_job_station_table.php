<?php

use Phinx\Migration\AbstractMigration;

class AddNewColumnsToJobTableAndJobStationTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
      public function change()
      {
      }
     */

    /**
     * Migrate Up.
     */
    public function up()
    {
        $this->execute("ALTER TABLE `job` ADD `jobStatus` TINYINT NOT NULL ;");
        $this->execute("ALTER TABLE `job` ADD `locationID` INT NOT NULL ;");
        $this->execute("ALTER TABLE `jobStation` ADD `locationID` INT NOT NULL ;");
    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }

}
