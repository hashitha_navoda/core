<?php

use Phinx\Migration\AbstractMigration;

class DropForiegnKeyInOutgoingPaymentAndAlterDebitNoteDebitNoteId extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
      public function change()
      {
      }
     */

    /**
     * Migrate Up.
     */
    public function up()
    {
        $this->execute("SET SESSION OLD_ALTER_TABLE=1;ALTER TABLE outgoingPayment DROP FOREIGN KEY outgoingPayment_ibfk_3");
        $this->execute("SET SESSION OLD_ALTER_TABLE=1;ALTER TABLE `debitNote` CHANGE `debitNoteID` `debitNoteID` INT( 11 ) NOT NULL AUTO_INCREMENT");
        $this->execute("SET SESSION OLD_ALTER_TABLE=1;ALTER TABLE creditNoteProduct DROP FOREIGN KEY fk_creditNoteProduct_creditNoteID0");
        $this->execute("SET SESSION OLD_ALTER_TABLE=1;ALTER TABLE `creditNote` CHANGE `creditNoteID` `creditNoteID` INT( 11 ) NOT NULL AUTO_INCREMENT");
    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }

}
