<?php

use Phinx\Migration\AbstractMigration;

class AddNewDuplicateTemplateIntoTemplateTableForPettyCashVoucher extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    // public function change()
    // {

    // }

    public function up()
    {
        $sql = <<<EOF
                INSERT INTO `template` (`templateName`, `documentTypeID`, `documentSizeID`, `templateContent`, `templateFooterDetails`, `templateFooterShow`, `templateDefault`, `templateSample`) VALUES
('Template', 28, 1, '<table class="table table-bordered" width="100%"><tbody>
<tr><td><img class="logo" style="background: #e0e0e0; max-height: 75px; display: block; text-align: center;" src="[Company Logo]" alt="Company Logo" /><span style="font-weight: bold;"><abbr contenteditable="false" spellcheck="false">[Company Name]</abbr> </span><br /><abbr contenteditable="false" spellcheck="false">[Company Address]</abbr> <br />Tel: <abbr contenteditable="false" spellcheck="false">[Company Tel. No]</abbr> <br />Email: <abbr contenteditable="false" spellcheck="false">[Company Email]</abbr> <br />Tax No: <abbr contenteditable="false" spellcheck="false">[Company Tax Reg. No]</abbr>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</td><td><h2 align="right">Petty Cash Voucher</h2><p>&nbsp;</p></td></tr><tr><td><div class="add_block">&nbsp;</div></td><td class="relative_data" rowspan="2" align="right"><table class="table table-bordered table-condensed" style="height: 62px;" width="303"><tbody><tr><td>Voucher Date:</td><td style="text-align: right;"><abbr contenteditable="false" spellcheck="false">[Petty Cash Voucher Date]</abbr>&nbsp;</td></tr><tr><td>Voucher&nbsp;No:</td><td style="text-align: right;"><abbr contenteditable="false" spellcheck="false">[Petty Cash Voucher Code]</abbr>&nbsp;</td></tr></tbody></table></td></tr></tbody></table><noneditabletable><table class="noneditable table table-bordered" style="width: 100%; margin-bottom: 100px;" title="" contenteditable="false" data-original-title=""><tbody><tr><td colspan="2"><hr /><div class="qout_body"><table id="item_tbl" class="table bigtable table-bordered table-striped" width="100%"><colgroup><col width="20px" /><col width="20px" /><col width="10px" /><col width="10px" /></colgroup><thead><tr><th style="text-align: center;">Voucher No</th><th style="text-align: center;">Voucher type</th><th style="text-align: center;">Petty cash<br/>expense type</th><th style="text-align: center;">Voucher Amount<br/>(Rs)</th></tr></thead><tbody><tr><td align="">PVOU010</td><td align="">Cash outgoing</td><td align="">1010 - Bank savings account</td><td align="" class="text-right">1500.00</td></tr></tbody></table><br /><br /><br /><div class="summary"><table id="" class="table" width="100%"><colgroup><col width="90px" /><col width="290px" /><col width="75px" /><col width="100px" /><col width="50px" /><col width="100px" /></colgroup><tbody><tr><td colspan="2" rowspan="4"><div id="comment"><strong>Comment&nbsp;:</strong><p><em>Sample memo text for payment</em></p></div></td><td colspan="3">Sub Total</td><td class="text-right">1500.00</td></tr><tr><td colspan="3"><h3>Total Rs</h3></td><td class="text-right"><h3>1500.00</h3></td></tr></tbody></table></div></div></td></tr></tbody></table></noneditabletable><footer class="doc"><table class="table table-bordered" width="100%"><tbody><tr><td><abbr contenteditable="false" spellcheck="false">[Company Name]</abbr>&nbsp;</td><td>&nbsp;</td><td style="text-align: right;"><abbr contenteditable="false" spellcheck="false">[Today Date]</abbr>&nbsp;</td></tr></tbody></table></footer>', '', 1, 0, 1);
EOF;
        $this->execute($sql);
    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }
}
