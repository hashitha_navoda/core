<?php

use Phinx\Migration\AbstractMigration;

class AddNewTablesForFreeItemIssues extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $freeIssueDetails = $this->hasTable('freeIssueDetails');
        if (!$freeIssueDetails) {
            $newTable = $this->table('freeIssueDetails', ['id' => 'freeIssueDetailID']);
            $newTable->addColumn('majorProductID', 'integer', ['limit' => 20])
                    ->addColumn('majorConditionType' , 'integer', ['limit' => 20])
                    ->addColumn('majorQuantity', 'decimal', ['precision' => 20, 'scale' => 5, 'default' => null, 'null' => true])
                    ->addColumn('selectedMajorUom', 'integer', ['limit' => 20])
                    ->addColumn('locationID', 'integer', ['limit' => 20])
                    ->save();
        }

        $freeIssueItems = $this->hasTable('freeIssueItems');
        if (!$freeIssueItems) {
            $newTable = $this->table('freeIssueItems', ['id' => 'freeIssueItemsID']);
            $newTable->addColumn('freeIssueDetailsID', 'integer', ['limit' => 20])
                    ->addColumn('productID', 'integer', ['limit' => 20])
                    ->addColumn('quantity', 'decimal', ['precision' => 20, 'scale' => 5, 'default' => null, 'null' => true])
                    ->addColumn('selectedUom', 'integer', ['limit' => 20])
                    ->save();
        }
    }
}
