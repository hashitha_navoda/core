<?php

use Phinx\Migration\AbstractMigration;

class AddNewTableForContactListAndContactListContact extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $contactLists = $this->hasTable('contactLists');
        if (!$contactLists) {
            $newTable = $this->table('contactLists', ['id' => 'contactListsID']);
            $newTable->addColumn('name', 'string', ['default' => null, 'limit' => 255, 'null' => true])
                ->addColumn('code', 'string', ['default' => null, 'limit' => 255, 'null' => true])
                ->addColumn('noOfContacts', 'integer', ['limit' => 20])
                ->addColumn('entityID', 'integer', ['limit' => 20])
                ->save();
        }

        $contactListContacts = $this->hasTable('contactListContacts');
        if (!$contactListContacts) {
            $newTable = $this->table('contactListContacts', ['id' => 'contactListContactsID']);
            $newTable->addColumn('contactID', 'integer', ['limit' => 20])
                ->addColumn('contactListID', 'integer', ['limit' => 20])
                ->save();
        }
    }
}
