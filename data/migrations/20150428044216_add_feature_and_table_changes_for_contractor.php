<?php

use Phinx\Migration\AbstractMigration;

class AddFeatureAndTableChangesForContractor extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
      public function change()
      {
      }
     */

    /**
     * Migrate Up.
     */
    public function up()
    {

        $this->execute("INSERT INTO `feature` (`featureID`, `featureName`, `featureController`, `featureAction`, `featureCategory`, `featureType`, `moduleID`) VALUES (NULL, 'Create', 'JobCard\\\Controller\\\Contractor', 'create', 'Contractor', '0', '8');");
        $id1 = $this->fetchRow("select LAST_INSERT_ID()")[0];
        $this->execute("INSERT INTO `roleFeature` (`roleFeatureID`, `roleID`, `featureID`, `roleFeatureEnabled`) VALUES (NULL, '1', $id1, '1');");

        $this->execute("INSERT INTO `feature` (`featureID`, `featureName`, `featureController`, `featureAction`, `featureCategory`, `featureType`, `moduleID`) VALUES (NULL, 'Default', 'JobCard\\\Controller\\\API\\\Contractor', 'deleteContractor', 'Application', '1', '8');");
        $id2 = $this->fetchRow("select LAST_INSERT_ID()")[0];
        $this->execute("INSERT INTO `roleFeature` (`roleFeatureID`, `roleID`, `featureID`, `roleFeatureEnabled`) VALUES (NULL, '1', $id2, '1');");

        $this->execute("INSERT INTO `feature` (`featureID`, `featureName`, `featureController`, `featureAction`, `featureCategory`, `featureType`, `moduleID`) VALUES (NULL, 'Default', 'JobCard\\\Controller\\\API\\\Contractor', 'getContractorBySearchKey', 'Application', '1', '8');");
        $id3 = $this->fetchRow("select LAST_INSERT_ID()")[0];
        $this->execute("INSERT INTO `roleFeature` (`roleFeatureID`, `roleID`, `featureID`, `roleFeatureEnabled`) VALUES (NULL, '1', $id3, '1');");

        $this->execute("SET SESSION old_alter_table=1;ALTER TABLE `contractor` CHANGE `contractorDesignation` `designationID` INT( 11 ) NULL DEFAULT NULL ;");
        $this->execute("SET SESSION old_alter_table=1;ALTER TABLE `contractor` ADD `entityID` INT( 11 ) NOT NULL AFTER `contractorTP` ;");
    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }

}
