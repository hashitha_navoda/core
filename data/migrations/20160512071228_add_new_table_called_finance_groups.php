<?php

use Phinx\Migration\AbstractMigration;

class AddNewTableCalledFinanceGroups extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $Sql = <<<SQL
        CREATE TABLE IF NOT EXISTS `financeGroups` (
`financeGroupsID` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `financeGroupsName` varchar(100) NOT NULL,
  `financeGroupsShortName` varchar(100) NOT NULL,
  `financeGroupTypesID` int(11) NOT NULL,
  `financeGroupsAddress` varchar(100) NULL,
  `financeGroupsParentID` int(11) NULL,
  `entityID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

SQL;

    $this->execute($Sql);
    }
}
