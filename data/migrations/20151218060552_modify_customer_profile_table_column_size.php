<?php

use Phinx\Migration\AbstractMigration;

class ModifyCustomerProfileTableColumnSize extends AbstractMigration
{

    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     */
    // public function change()
    // {
    // }

    public function up()
    {
        $sql = <<<SQL
                SET SESSION old_alter_table=1;
                ALTER TABLE  `customerProfile` CHANGE  `customerProfileMobileTP1`  `customerProfileMobileTP1` VARCHAR( 255 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL ;
                ALTER TABLE  `customerProfile` CHANGE  `customerProfileMobileTP2`  `customerProfileMobileTP2` VARCHAR( 255 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL ;
                ALTER TABLE  `customerProfile` CHANGE  `customerProfileLandTP1`  `customerProfileLandTP1` VARCHAR( 255 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL ;
                ALTER TABLE  `customerProfile` CHANGE  `customerProfileLandTP2`  `customerProfileLandTP2` VARCHAR( 255 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL ;
                ALTER TABLE  `customerProfile` CHANGE  `customerProfileFaxNo`  `customerProfileFaxNo` VARCHAR( 255 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL ;
                ALTER TABLE  `customerProfile` CHANGE  `customerProfileEmail`  `customerProfileEmail` VARCHAR( 255 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL ;
                ALTER TABLE  `customerProfile` CHANGE  `customerProfileContactPersonNumber`  `customerProfileContactPersonNumber` VARCHAR( 255 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL ;
                ALTER TABLE  `customerProfile` CHANGE  `customerProfileLocationNo`  `customerProfileLocationNo` VARCHAR( 255 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL ;
                ALTER TABLE  `customerProfile` CHANGE  `customerProfileLocationSubTown`  `customerProfileLocationSubTown` VARCHAR( 255 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL ;
                ALTER TABLE  `customerProfile` CHANGE  `customerProfileLocationTown`  `customerProfileLocationTown` VARCHAR( 255 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL ;
                ALTER TABLE  `customerProfile` CHANGE  `customerProfileLocationPostalCode`  `customerProfileLocationPostalCode` VARCHAR( 255 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL ;
                ALTER TABLE  `customerProfile` CHANGE  `customerProfileLocationCountry`  `customerProfileLocationCountry` VARCHAR( 255 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL ;
                ALTER TABLE  `customerProfile` CHANGE  `customerProfileName`  `customerProfileName` VARCHAR( 255 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL ;
SQL;
        $this->execute($sql);
    }

}
