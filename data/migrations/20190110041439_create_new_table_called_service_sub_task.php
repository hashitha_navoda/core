<?php

use Phinx\Migration\AbstractMigration;

class CreateNewTableCalledServiceSubTask extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $serviceSubTask = $this->hasTable('serviceSubTask');

        if (!$serviceSubTask) {
            $table = $this->table('serviceSubTask', ['id' => 'subTaskId']);
            $table->addColumn('subTaskCode', 'string', ['limit' => 30, 'default' => null]);
            $table->addColumn('subTaskName', 'string', ['limit' => 30, 'default' => null]);
            $table->addColumn('taskId', 'integer', ['limit' => 11]);
            $table->addColumn('subTaskStatus', 'integer', ['limit' => 11]);
            $table->addColumn('deleted', 'boolean', ['default' => false]);
            $table->save();
        }


    }
}
