<?php

use Phinx\Migration\AbstractMigration;

class AddNewTableToStoreInvoicedBomSubItemData extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $invoicedBomSubItems = $this->hasTable('invoicedBomSubItems');
        if (!$invoicedBomSubItems) {
            $newTable = $this->table('invoicedBomSubItems', ['id' => 'invoicedBomSubItemID']);
            $newTable->addColumn('salesInvoiceID', 'integer', ['limit' => 20])
                ->addColumn('salesInvoiceProductID', 'integer', ['limit' => 20])
                ->addColumn('bomID', 'integer', ['limit' => 20])
                ->addColumn('subProductID', 'integer', ['limit' => 20, 'default' => null, 'null' => true])
                ->addColumn('subProductBomPrice', 'decimal', ['precision' => 25, 'scale' => 10])
                ->addColumn('subProductBomQty', 'decimal', ['precision' => 25, 'scale' => 10])
                ->addColumn('subProductTotalQty', 'decimal', ['precision' => 25, 'scale' => 10])
                ->addColumn('subProductTotal', 'decimal', ['precision' => 25, 'scale' => 10])
                ->save();
        }
    }
}
