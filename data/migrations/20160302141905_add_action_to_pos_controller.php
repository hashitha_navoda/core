<?php

use Phinx\Migration\AbstractMigration;

class AddActionToPosController extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
    public function change()
    {
    }
    */
    
    /**
     * Migrate Up.
     */
    public function up()
    {
        $pdo = $this->getAdapter()->getConnection();
        $rows = $this->fetchAll("SELECT roleID FROM `role`");
        //this migration for activate bom Controller Actions.
        
        $dataArray = array(
            array(
                'featureID' => 'NULL',
                'featureName' => 'Default',
                'featureController' => 'Pos\Controller\Pos\API',
                'featureAction' => 'searchDataForPOSCreditNote',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '6'
            ),
        
            array(
                'featureName' => 'Default',
                'featureController' => 'Pos\Controller\Pos\API',
                'featureAction' => 'getDataForPOSCreditNote',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '6'
            ),
        
            array(
                'featureName' => 'Default',
                'featureController' => 'Pos\Controller\Pos\API',
                'featureAction' => 'savePOSCreditNote',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '6'
            ),
        
            array(
                'featureName' => 'Default',
                'featureController' => 'Pos\Controller\Pos\API',
                'featureAction' => 'getCreditNoteDetails',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '6'
            ),
        
            array(
                'featureName' => 'Default',
                'featureController' => 'Pos\Controller\Pos\API',
                'featureAction' => 'updateClosingBalance',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '6'
            ),
        );

        foreach($dataArray as $feature){

            $featureInsert = <<<SQL
                INSERT INTO `feature` 
                (
                    `featureName`, 
                    `featureController`, 
                    `featureAction`, 
                    `featureCategory`, 
                    `featureType`, 
                    `moduleID`
                ) VALUES (
                    :featureName, 
                    :featureController, 
                    :featureAction, 
                    :featureCategory, 
                    :featureType, 
                    :featureModuleId
                );
    
SQL;
    
            $pdo->prepare($featureInsert)->execute(array(
                'featureName' => $feature['featureName'], 
                'featureController' => $feature['featureController'], 
                'featureAction' => $feature['featureAction'], 
                'featureCategory' => $feature['featureCategory'], 
                'featureType' => $feature['featureType'], 
                'featureModuleId' => $feature['moduleID']
            ));

            $featureID = $this->fetchRow("select LAST_INSERT_ID()")[0];

            foreach ($rows as $row) {
                $roleID = $row['roleID'];
                
                if ($roleID == 1) {
                    $enabled = 1;
                } else {

                   //for enable api calls for every role
                    if($feature['featureType'] == 1){
                        $enabled = 1;
                    } else {
                        $enabled = 0;
                    }

                }

                $roleFeatureInsert =<<<SQL
                    INSERT INTO `roleFeature`
                    (
                        `roleID`,
                        `featureID`,
                        `roleFeatureEnabled`
                    ) VALUES(
                        :roleID,
                        :featureID,
                        :roleFeatureEnabled
                    )
SQL;
                $pdo->prepare($roleFeatureInsert)->execute(array(
                    'roleID' => $roleID, 
                    'featureID' => $featureID, 
                    'roleFeatureEnabled' => $enabled
                ));
            }

        }
    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }
}