<?php

use Phinx\Migration\AbstractMigration;

class ChangePurchaseInvoiceNotificationTitle extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
    public function change()
    {
    }
    */
    
    /**
     * Migrate Up.
     */
    public function up()
    {
        $pdo = $this->getAdapter()->getConnection();
        //get all notification details that relatedto the purchase invoice
        $notificationDetails = $this->fetchAll("SELECT * FROM `notification` left join `purchaseInvoice` 
                    on `notification`.`notificationReferID`= `purchaseInvoice`.`purchaseInvoiceID`
                    left join `supplier`
                    on `purchaseInvoice`.`purchaseInvoiceSupplierID`=`supplier`.`supplierID`
                    WHERE `notification`.`notificationType` ='2' && `notification`.`status` = '0';");
        //update notification title and notification body 
        foreach($notificationDetails as $singleValue){
            $updatePINotification =<<<SQL
                UPDATE `notification`   
                SET `notificationTitle` = :notificationTitle,
                    `notificationBody` = :notificationBody
                   
                WHERE `notificationID` = :notificationID
                    
SQL;
            $pdo->prepare($updatePINotification)->execute(array(
                'notificationTitle' => "An Overdue Purchase Invoice", 
                'notificationBody' => "Purchase Invoice ".$singleValue['purchaseInvoiceCode']." received from ". $singleValue['supplierCode']." has reached due Date.", 
                'notificationID' => $singleValue['notificationID']
            ));

        }
    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }
}