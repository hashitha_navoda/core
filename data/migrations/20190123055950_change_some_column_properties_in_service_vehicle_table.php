<?php

use Phinx\Migration\AbstractMigration;

class ChangeSomeColumnPropertiesInServiceVehicleTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function up()
    {
        $table = $this->table('serviceVehicle');
        $column = $table->hasColumn('customerID');
        $column1 = $table->hasColumn('vehicleType');
        $column2 = $table->hasColumn('vehicleModel');
        if ($column) {
            $table->changeColumn('customerID', 'integer', ['null'=>true])
              ->save();
        }

        if ($column1) {
            $table->changeColumn('vehicleType', 'integer', ['null'=>true])
              ->save();
        }
        if ($column2) {
            $table->changeColumn('vehicleModel', 'integer', ['null'=>true])
              ->save();
        }
    }
}
