<?php

use Phinx\Migration\AbstractMigration;

class InsertNewDocTypeToApprovalWorkflowList extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        ini_set('memory_limit', '-1');
        $dataArray[] = [
            'docID' => 48,
            'isActive' => 0,
            'isShow' => 1 
        ];

        $pdo = $this->getAdapter()->getConnection();

        foreach($dataArray as $method){

            $approvalDocInserted = <<<SQL
                INSERT INTO `approvalDocumentDetails` 
                ( 
                    `docID`, 
                    `isActive`, 
                    `isShow`
                ) VALUES (
                    :docID, 
                    :isActive,  
                    :isShow
                );
    
SQL;
    
            $pdo->prepare($approvalDocInserted)->execute(array( 
                'docID' => $method['docID'], 
                'isActive' => $method['isActive'], 
                'isShow' => $method['isShow']
            ));

        }
    }
}
