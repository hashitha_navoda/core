<?php

use Phinx\Migration\AbstractMigration;

class ChangePropertiesOfPurchesOrderTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {

        $table = $this->table('purchaseRequisition');
        $column = $table->hasColumn('purchaseRequisitionSupplierReference');
        if ($column) {
            $table->changeColumn('purchaseRequisitionSupplierReference', 'text', ['limit' => 4294967295,'default' => null, 'null' => true])
                ->save();
        }

        $table1 = $this->table('purchaseOrder');
        $column1 = $table1->hasColumn('purchaseOrderSupplierReference');
        if ($column1) {
            $table1->changeColumn('purchaseOrderSupplierReference', 'text', ['limit' => 4294967295,'default' => null, 'null' => true])
                ->save();
        }

        $table2 = $this->table('purchaseInvoice');
        $column2 = $table2->hasColumn('purchaseInvoiceSupplierReference');
        if ($column2) {
            $table2->changeColumn('purchaseInvoiceSupplierReference', 'text', ['limit' => 4294967295,'default' => null, 'null' => true])
                ->save();
        }

        $table3 = $this->table('grn');
        $column3 = $table3->hasColumn('grnSupplierReference');
        if ($column3) {
            $table3->changeColumn('grnSupplierReference', 'text', ['limit' => 4294967295,'default' => null, 'null' => true])
                ->save();
        }

        $table4 = $this->table('paymentVoucher');
        $column4 = $table4->hasColumn('paymentVoucherSupplierReference');
        if ($column4) {
            $table4->changeColumn('paymentVoucherSupplierReference', 'text', ['limit' => 4294967295,'default' => null, 'null' => true])
                ->save();
        }
    }
}
