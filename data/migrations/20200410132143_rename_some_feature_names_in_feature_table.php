<?php

use Phinx\Migration\AbstractMigration;

class RenameSomeFeatureNamesInFeatureTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        ini_set('memory_limit', '-1');
        $features = $this->fetchAll("SELECT featureID,featureName,featureAction,featureCategory,featureType FROM `feature` WHERE moduleID = 11 AND featureCategory = 'Resources'");

        foreach ($features as $feature) {
            
            $featureName = "";
            $featureID = $feature['featureID'];

            if ($feature['featureAction'] == 'vehicles' ) {
                $feature['featureName'] = 'Create Vehicles';
            } else {
                $feature['featureName'] = $feature['featureAction'];
            }
           
            $featureName = "'".$feature['featureName']."'";

            if (!empty($feature['featureName'])) {
                 $this->execute("UPDATE `feature` SET `featureName` = $featureName WHERE `feature`.`featureID` = $featureID");       
            }
        }
    }
}
