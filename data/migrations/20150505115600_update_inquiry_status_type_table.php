<?php

use Phinx\Migration\AbstractMigration;

class UpdateInquiryStatusTypeTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
      public function change()
      {
      }
     */

    /**
     * Migrate Up.
     */
    public function up()
    {
        $this->execute('TRUNCATE `inquiryStatusType` ;');

        $this->execute("INSERT INTO `inquiryStatusType` (`inquiryStatusTypeID` ,`inquiryStatusTypeName`)VALUES ('1', 'At Creation');");
        $this->execute("INSERT INTO `inquiryStatusType` (`inquiryStatusTypeID` ,`inquiryStatusTypeName`)VALUES ('2', 'After One Minute');");
        $this->execute("INSERT INTO `inquiryStatusType` (`inquiryStatusTypeID` ,`inquiryStatusTypeName`)VALUES ('3', 'After One hour');");
        $this->execute("INSERT INTO `inquiryStatusType` (`inquiryStatusTypeID` ,`inquiryStatusTypeName`)VALUES ('4', 'After 24 Hours');");
    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }

}
