<?php

use Phinx\Migration\AbstractMigration;

class CreateTableCalledCreditNoteSubProduct extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
      public function change()
      {
      }
     */

    /**
     * Migrate Up.
     */
    public function up()
    {
        $this->execute("CREATE TABLE IF NOT EXISTS `creditNoteSubProduct` (
  `creditNoteSubProductID` int(11) NOT NULL AUTO_INCREMENT,
  `creditNoteProductID` int(11) NOT NULL,
  `productBatchID` int(11) DEFAULT NULL,
  `productSerialID` int(11) DEFAULT NULL,
  `creditNoteSubProductQuantity` int(11) NOT NULL,
  PRIMARY KEY (`creditNoteSubProductID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;");
    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }

}
