<?php

use Phinx\Migration\AbstractMigration;

class ChangeIncomingPaymentTypeOfIncomeBasePayments extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        ini_set('memory_limit', '-1');
        $rows = $this->fetchAll("SELECT incomingPaymentID FROM `incomingInvoicePayment` WHERE `incomeID` > 0");

        if (sizeof($rows) > 0) {
            foreach ($rows as $key => $value) {
                $type = "income";
                $id = $value['incomingPaymentID'];
                $this->execute("UPDATE  `incomingPayment` SET  `incomingPaymentType` = 'income' WHERE `incomingPaymentID` = '{$id}'");
            }
        }
    }
}
