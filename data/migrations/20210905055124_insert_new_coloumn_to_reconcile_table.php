<?php

use Phinx\Migration\AbstractMigration;

class InsertNewColoumnToReconcileTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->table('reconciliation');
        $column1 = $table->hasColumn('reconciliationStatus');
        $column2 = $table->hasColumn('reconciliationStartDate');
        $column3 = $table->hasColumn('reconciliationEndDate');
        
        if (!$column1) {
            $table->addColumn('reconciliationStatus', 'integer', ['limit' => 20, 'default' => null, 'null' => true])
                ->update();
        }
        if (!$column2) {
            $table->addColumn('reconciliationStartDate', 'date', ['default' => null, 'null' => true])
                ->update();
        }
        if (!$column3) {
            $table->addColumn('reconciliationEndDate', 'date', ['default' => null, 'null' => true])
                ->update();
        }
    }
}
