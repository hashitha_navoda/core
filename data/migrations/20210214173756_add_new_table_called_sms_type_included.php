<?php

use Phinx\Migration\AbstractMigration;

class AddNewTableCalledSmsTypeIncluded extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $smsIncluded = $this->hasTable('smsIncluded');
        if (!$smsIncluded) {
            $newTable = $this->table('smsIncluded', ['id' => 'smsIncludedId']);
            $newTable->addColumn('smsIncludedName', 'string', ['limit' => 200,'default' => null, 'null' => true])
                ->save();
        }
    }
}
