<?php

use Phinx\Migration\AbstractMigration;

class AddChequeDepositFeatures extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
    public function change()
    {
    }
    */
    
    /**
     * Migrate Up.
     */
    public function up()
    {
        //role list
        $roles = $this->fetchAll("SELECT roleID FROM `role`");
        
        //feature list
        $featureList = array(
            array('name' =>'Undeposited Cheques','controller'=>'Expenses\\\Controller\\\ChequeDeposit','action'=>'index','category'=>'Cheque Deposit','type'=>'0','moduleId'=>'9'),
            array('name' =>'Deposits','controller'=>'Expenses\\\Controller\\\ChequeDeposit','action'=>'deposits','category'=>'Cheque Deposit','type'=>'0','moduleId'=>'9'),
            array('name' =>'Default','controller'=>'Expenses\\\Controller\\\API\\\ChequeDepositAPI','action'=>'save','category'=>'Cheque Deposit','type'=>'1','moduleId'=>'9'),
            array('name' =>'Default','controller'=>'Expenses\\\Controller\\\API\\\ChequeDepositAPI','action'=>'cancel','category'=>'Cheque Deposit','type'=>'1','moduleId'=>'9'),
            array('name' =>'Default','controller'=>'Expenses\\\Controller\\\API\\\ChequeDepositAPI','action'=>'update','category'=>'Cheque Deposit','type'=>'1','moduleId'=>'9'),
            array('name' =>'Default','controller'=>'Expenses\\\Controller\\\API\\\ChequeDepositAPI','action'=>'getUndepositedCheques','category'=>'Cheque Deposit','type'=>'1','moduleId'=>'9'),
            array('name' =>'Default','controller'=>'Expenses\\\Controller\\\API\\\ChequeDepositAPI','action'=>'getDeposits','category'=>'Cheque Deposit','type'=>'1','moduleId'=>'9'),
            array('name' =>'Default','controller'=>'Expenses\\\Controller\\\API\\\ChequeDepositAPI','action'=>'getDeposit','category'=>'Cheque Deposit','type'=>'1','moduleId'=>'9'),
            array('name' =>'Default','controller'=>'Expenses\\\Controller\\\API\\\AccountAPI','action'=>'bankAccountListForDropdown','category'=>'Account','type'=>'1','moduleId'=>'9')
        );
        
        foreach ($featureList as $feature){            
            //insert feature to feature table
            $this->execute("INSERT INTO `feature` (`featureID`, `featureName`, `featureController`, `featureAction`, `featureCategory`, `featureType`, `moduleID`) VALUES (NULL,'".$feature['name']."', '".$feature['controller']."', '".$feature['action']."', '".$feature['category']."', '".$feature['type']."', '".$feature['moduleId']."');");
            $id = $this->fetchRow("select LAST_INSERT_ID()")[0];
            
            foreach ($roles as $role) {
                $roleID = $role['roleID'];
                if ($roleID == 1) {
                    $enabled = 1;
                } else {
                    //for enable api calls for every role
                    if($feature['type'] === '1'){
                        $enabled = 1;
                    } else {
                        $enabled = 0;
                    }
                }
                $this->execute("INSERT INTO `roleFeature` (`roleFeatureID`, `roleID`, `featureID`, `roleFeatureEnabled`) VALUES (NULL, $roleID, $id,$enabled);");
            }
        }
        
        //for add chequeDepositId for paymentMethodsNumbers table
        $this->execute("ALTER TABLE `paymentMethodsNumbers` ADD `chequeDepositId` int(11) DEFAULT NULL");
        
        //for create chequeDeposit table
        $this->execute("CREATE TABLE IF NOT EXISTS `chequeDeposit` (
            `chequeDepositId` int(11) NOT NULL AUTO_INCREMENT,
            `chequeDepositAmount` decimal(12,2) DEFAULT NULL,
            `chequeDepositDate` datetime DEFAULT NULL,
            `chequeDepositDescription` text,
            `bankId` int(11) DEFAULT NULL,
            `accountId` int(11) DEFAULT NULL,
            `entityId` int(11) DEFAULT NULL,
            PRIMARY KEY (`chequeDepositId`)
          ) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;");
        
        //for create chequeDepositCancellation table
        $this->execute("CREATE TABLE IF NOT EXISTS `chequeDepositCancellation` (
            `chequeDepositCancelationId` int(11) NOT NULL AUTO_INCREMENT,
            `chequeDepositCancelationCharge` decimal(10,2) NOT NULL DEFAULT '0.00',
            `chequeDepositCancelationDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
            `chequeDepositCancellationComment` text,
            `chequeDepositId` int(11) DEFAULT NULL,
            PRIMARY KEY (`chequeDepositCancelationId`)
          ) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;");
        
        //for create chequeDepositCancellationCheque table
        $this->execute("CREATE TABLE IF NOT EXISTS `chequeDepositCancellationCheque` (
            `chequeDepositCancelationChequeId` int(11) NOT NULL AUTO_INCREMENT,
            `chequeDepositCancelationId` int(11) DEFAULT NULL,
            `paymentMethodsNumbersId` int(11) DEFAULT NULL,
            PRIMARY KEY (`chequeDepositCancelationChequeId`)
          ) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;");
    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }
}