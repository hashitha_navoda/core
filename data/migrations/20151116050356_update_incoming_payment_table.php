<?php

use Phinx\Migration\AbstractMigration;

class UpdateIncomingPaymentTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     */
    public function change()
    {
        //get incoming payment list
        $paymentList = $this->fetchAll("SELECT * FROM `incomingPayment`");
        foreach ($paymentList as $value) {
            //check incoming payment payed amount is null or 0
            if($value['incomingPaymentPaidAmount']==''||$value['incomingPaymentPaidAmount']==0){
                //update incoming payment table        
                $this->execute("UPDATE `incomingPayment` SET `incomingPaymentPaidAmount` = {$value['incomingPaymentAmount']} WHERE `incomingPayment`.`incomingPaymentID` = {$value['incomingPaymentID']};");
            }
        }
    }
}
