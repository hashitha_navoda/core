<?php

use Phinx\Migration\AbstractMigration;

class AddDefaultReferenceMigration extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $addRow = $this->execute("INSERT IGNORE INTO `reference` (`referenceNameID`, `referenceName`, `referenceTypeID`) VALUES ('30', 'JE', '0');");

        $addRowToReferencePrefix = $this->execute("INSERT IGNORE INTO `referencePrefix` (`referencePrefixID`,`referenceNameID`, `referencePrefixCHAR`, `referencePrefixNumberOfDigits`, `referencePrefixCurrentReference`) VALUES ('NULL','30', 'JE', '9', '1')");
    }
}
