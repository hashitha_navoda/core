<?php

use Phinx\Migration\AbstractMigration;

class RemoveWrongCardTypeMigration extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     */
    // public function change()
    // {

    // }

    public function up()
    {

        $sqlDataArray = [
            [
                'featureName' => 'View Card Type',
                'featureController' => 'Core\\\Controller\\\CoreController\\\CardType',
                'featureAction' => 'index',
                'featureCategory' => 'Card Type',
                'featureType' => '0',
                'moduleID' => '9'
            ],
            [
                'featureName' => 'Default',
                'featureController' => 'Core\\\Controller\\\CoreController\\\API\\\CardType',
                'featureAction' => 'save',
                'featureCategory' => 'Card Type',
                'featureType' => '1',
                'moduleID' => '9'
            ],
            [
                'featureName' => 'Default',
                'featureController' => 'Core\\\Controller\\\CoreController\\\API\\\CardType',
                'featureAction' => 'update',
                'featureCategory' => 'Card Type',
                'featureType' => '1',
                'moduleID' => '9'
            ],
            [
                'featureName' => 'Default',
                'featureController' => 'Core\\\Controller\\\CoreController\\\API\\\CardType',
                'featureAction' => 'delete',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '9'
            ],
            [
                'featureName' => 'Default',
                'featureController' => 'Core\\\Controller\\\CoreController\\\API\\\CardType',
                'featureAction' => 'search',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '9'
            ],
            [
                'featureName' => 'Default',
                'featureController' => 'Core\\\Controller\\\CoreController\\\API\\\CardType',
                'featureAction' => 'getUnAssignedCardList',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '9'
            ]
        ];

        foreach ($sqlDataArray as $sqlData) {
            $result = $this->fetchRow("SELECT `feature`.`featureID`, `roleFeatureID` FROM `feature` JOIN `roleFeature` ON `feature`.`featureID` = `roleFeature`.`featureID`
                    WHERE `featureName` = '{$sqlData['featureName']}' 
                    AND `featureController` = '{$sqlData['featureController']}' 
                    AND `featureAction` = '{$sqlData['featureAction']}'
                    AND `featureCategory` = '{$sqlData['featureCategory']}'
                    AND `featureType` = '{$sqlData['featureType']}'
                    AND `moduleID` = '{$sqlData['moduleID']}' ;"
                    );
            $this->execute("DELETE FROM `roleFeature` WHERE `roleFeatureID` = '{$result['roleFeatureID']}'");
            $this->execute("DELETE FROM `feature` WHERE `featureID` = '{$result['featureID']}'");
        }
    }
}
