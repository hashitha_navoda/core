<?php

use Phinx\Migration\AbstractMigration;

class CreateVehicleTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
    public function change()
    {
    }
    */
    
    /**
     * Migrate Up.
     */
    public function up()
    {
        //create vehicle table
        $this->execute("CREATE TABLE IF NOT EXISTS `vehicle` (
            `vehicleId` int(11) NOT NULL AUTO_INCREMENT,
            `vehicleMake` varchar(255) DEFAULT NULL,
            `vehicleModel` varchar(255) DEFAULT NULL,
            `vehicleRegistrationNumber` varchar(255) DEFAULT NULL,
            `vehicleCost` decimal(12,2) DEFAULT NULL,
            `entityId` int(11) DEFAULT NULL,
            PRIMARY KEY (`vehicleId`)
          ) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;"
        );
        
        //role list
        $roles = $this->fetchAll("SELECT roleID FROM `role`");
        
        //feature list
        $featureList = array(
            array('name' =>'Manage','controller'=>'JobCard\\\Controller\\\Vehicle','action'=>'index','category'=>'Vehicle','type'=>'0','moduleId'=>'8'),
            array('name' =>'Default','controller'=>'JobCard\\\Controller\\\API\\\Vehicle','action'=>'create','category'=>'Application','type'=>'1','moduleId'=>'8'),
            array('name' =>'Default','controller'=>'JobCard\\\Controller\\\API\\\Vehicle','action'=>'update','category'=>'Application','type'=>'1','moduleId'=>'8'),
            array('name' =>'Default','controller'=>'JobCard\\\Controller\\\API\\\Vehicle','action'=>'delete','category'=>'Application','type'=>'1','moduleId'=>'8'),
            array('name' =>'Default','controller'=>'JobCard\\\Controller\\\API\\\Vehicle','action'=>'getVehicle','category'=>'Application','type'=>'1','moduleId'=>'8'),
            array('name' =>'Default','controller'=>'JobCard\\\Controller\\\API\\\Vehicle','action'=>'getVehicles','category'=>'Application','type'=>'1','moduleId'=>'8'),
            array('name' =>'Default','controller'=>'JobCard\\\Controller\\\API\\\Vehicle','action'=>'search','category'=>'Application','type'=>'1','moduleId'=>'8')
        );
        
        foreach ($featureList as $feature){            
            //insert feature to feature table
            $this->execute("INSERT INTO `feature` (`featureID`, `featureName`, `featureController`, `featureAction`, `featureCategory`, `featureType`, `moduleID`) VALUES (NULL,'".$feature['name']."', '".$feature['controller']."', '".$feature['action']."', '".$feature['category']."', '".$feature['type']."', '".$feature['moduleId']."');");
            $id = $this->fetchRow("select LAST_INSERT_ID()")[0];
            
            foreach ($roles as $role) {
                $roleID = $role['roleID'];
                if ($roleID == 1) {
                    $enabled = 1;
                } else {
                    //for enable api calls for every role
                    if($feature['type'] === '1'){
                        $enabled = 1;
                    } else {
                        $enabled = 0;
                    }
                }
                $this->execute("INSERT INTO `roleFeature` (`roleFeatureID`, `roleID`, `featureID`, `roleFeatureEnabled`) VALUES (NULL, $roleID, $id,$enabled);");
            }
        }
    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }
}