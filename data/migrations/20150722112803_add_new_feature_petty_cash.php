<?php

use Phinx\Migration\AbstractMigration;

class AddNewFeaturePettyCash extends AbstractMigration {

    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     */
//    public function change()
//    {
//
//    }

    public function up() {

        //role list
        $roles = $this->fetchAll("SELECT roleID FROM `role`");

        //feature list
        $featureList = [
            [
                'name' => 'Petty Cash Expense Type',
                'controller' => 'Expenses\\\Controller\\\PettyCashExpenseType', 'action' => 'index',
                'category' => 'Petty Cash', 'type' => '0', 'moduleId' => '9'
            ],
            [
                'name' => 'Default',
                'controller' => 'Expenses\\\Controller\\\API\\\PettyCashExpenseType', 'action' => 'saveExpenseType',
                'category' => 'Application', 'type' => '1', 'moduleId' => '9'
            ],
            [
                'name' => 'Default',
                'controller' => 'Expenses\\\Controller\\\API\\\PettyCashExpenseType', 'action' => 'updateExpenseType',
                'category' => 'Application', 'type' => '1', 'moduleId' => '9'
            ],
            [
                'name' => 'Default',
                'controller' => 'Expenses\\\Controller\\\API\\\PettyCashExpenseType', 'action' => 'getPettyCashExpenseType',
                'category' => 'Application', 'type' => '1', 'moduleId' => '9'
            ],
            [
                'name' => 'Default',
                'controller' => 'Expenses\\\Controller\\\API\\\PettyCashExpenseType', 'action' => 'deletePettyCashExpenseType',
                'category' => 'Application', 'type' => '1', 'moduleId' => '9'
            ],
            [
                'name' => 'Default',
                'controller' => 'Expenses\\\Controller\\\API\\\PettyCashExpenseType', 'action' => 'updatePettyCashExpenseTypeStatus',
                'category' => 'Application', 'type' => '1', 'moduleId' => '9'
            ],
            [
                'name' => 'Default',
                'controller' => 'Expenses\\\Controller\\\API\\\PettyCashExpenseType', 'action' => 'searchExpenseType',
                'category' => 'Application', 'type' => '1', 'moduleId' => '9'
            ],
            [
                'name' => 'Petty Cash Float',
                'controller' => 'Expenses\\\Controller\\\PettyCashFloat', 'action' => 'index',
                'category' => 'Petty Cash', 'type' => '0', 'moduleId' => '9'
            ],
            [
                'name' => 'Petty Cash Float View',
                'controller' => 'Expenses\\\Controller\\\PettyCashFloat', 'action' => 'view',
                'category' => 'Petty Cash', 'type' => '0', 'moduleId' => '9'
            ],
            [
                'name' => 'Default',
                'controller' => 'Expenses\\\Controller\\\API\\\PettyCashFloatAPI', 'action' => 'save',
                'category' => 'Application', 'type' => '1', 'moduleId' => '9'
            ],
            [
                'name' => 'Default',
                'controller' => 'Expenses\\\Controller\\\API\\\PettyCashFloatAPI', 'action' => 'updateStatus',
                'category' => 'Application', 'type' => '1', 'moduleId' => '9'
            ],
            [
                'name' => 'Petty Cash Voucher',
                'controller' => 'Expenses\\\Controller\\\PettyCashVoucher', 'action' => 'index',
                'category' => 'Petty Cash', 'type' => '0', 'moduleId' => '9'
            ],
            [
                'name' => 'Petty Cash Voucher View',
                'controller' => 'Expenses\\\Controller\\\PettyCashVoucher', 'action' => 'view',
                'category' => 'Petty Cash', 'type' => '0', 'moduleId' => '9'
            ],
            [
                'name' => 'Default',
                'controller' => 'Expenses\\\Controller\\\API\\\PettyCashVoucher', 'action' => 'save',
                'category' => 'Application', 'type' => '1', 'moduleId' => '9'
            ],
            [
                'name' => 'Default',
                'controller' => 'Expenses\\\Controller\\\API\\\PettyCashVoucher', 'action' => 'delete',
                'category' => 'Application', 'type' => '1', 'moduleId' => '9'
            ],
            [
                'name' => 'Default',
                'controller' => 'Expenses\\\Controller\\\API\\\PettyCashVoucher', 'action' => 'delete',
                'category' => 'Application', 'type' => '1', 'moduleId' => '9'
            ],
        ];

        foreach ($featureList as $feature) {
            //insert feature to feature table
            $this->execute("INSERT INTO `feature` (`featureID`, `featureName`, `featureController`, `featureAction`, `featureCategory`, `featureType`, `moduleID`) "
                    . "VALUES (NULL,'" . $feature['name'] . "', '" . $feature['controller'] . "', '" . $feature['action'] . "', '" . $feature['category'] . "', '" . $feature['type'] . "', '" . $feature['moduleId'] . "') ; ");
            $id = $this->fetchRow("select LAST_INSERT_ID()")[0];

            foreach ($roles as $role) {
                $roleID = $role['roleID'];
                if ($roleID == 1) {
                    $enabled = 1;
                } else {
                    //for enable api calls for every role
                    if ($feature['type'] === '1') {
                        $enabled = 1;
                    } else {
                        $enabled = 0;
                    }
                }
                $this->execute("INSERT INTO `roleFeature` (`roleFeatureID`, `roleID`, `featureID`, `roleFeatureEnabled`) VALUES (NULL, '$roleID', '$id','$enabled');");
            }
        }
//      create table petty Cash Expense type
        $this->execute("SET SESSION old_alter_table=1; CREATE TABLE IF NOT EXISTS `pettyCashExpenseType` (
            `pettyCashExpenseTypeID` int(11) NOT NULL AUTO_INCREMENT,
            `pettyCashExpenseTypeName` varchar(100) NOT NULL,
            `pettyCashExpenseTypeDescription` text,
            `pettyCashExpenseTypeMinAmount` double(20,5) DEFAULT NULL,
            `pettyCashExpenseTypeMaxAmount` decimal(20,5) DEFAULT NULL,
            `pettyCashExpenseTypeStatus` tinyint(4) NOT NULL,
            PRIMARY KEY (`pettyCashExpenseTypeID`)
          ) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ; ");

//        Create petty Cash Float Table
        $this->execute("SET SESSION old_alter_table=1; CREATE TABLE IF NOT EXISTS `pettyCashFloat` (
            `pettyCashFloatID` int(11) NOT NULL AUTO_INCREMENT,
            `pettyCashFloatAmount` decimal(20,5) NOT NULL,
            `pettyCashFloatDate` date NOT NULL,
            `accountID` int(11) NOT NULL,
            `pettyCashFloatBalance` decimal(20,5) NOT NULL,
            `locationID` int(11) NOT NULL,
            `pettyCashFloatStatus` tinyint(2) NOT NULL,
            `entityID` int(11) NOT NULL,
            PRIMARY KEY (`pettyCashFloatID`)
          ) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;");

//        Create Petty Cash Voucher Table
        $this->execute("SET SESSION old_alter_table=1; CREATE TABLE IF NOT EXISTS `pettyCashVoucher` (
            `pettyCashVoucherID` int(11) NOT NULL AUTO_INCREMENT,
            `pettyCashVoucherNo` varchar(20) NOT NULL,
            `locationID` int(11) NOT NULL,
            `pettyCashVoucherAmount` decimal(20,5) NOT NULL,
            `pettyCashVoucherDate` date NOT NULL,
            `pettyCashVoucherType` tinyint(4) NOT NULL,
            `pettyCashExpenseTypeID` int(11) DEFAULT NULL,
            `outgoingPettyCashVoucherID` int(11) DEFAULT NULL,
            `userID` int(11) DEFAULT NULL,
            `entityID` int(11) NOT NULL,
            PRIMARY KEY (`pettyCashVoucherID`)
          ) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;");

//      Add Reference to the Petty Cash Voucher
        $this->execute("INSERT INTO  `reference` (
            `referenceNameID` ,
            `referenceName` ,
            `referenceTypeID`
            )
            VALUES (
            '27',  'Petty Cash Voucher',  '0'
           ); ");

        $this->execute("INSERT INTO  `referencePrefix` (
            `referencePrefixID` ,
            `referenceNameID` ,
            `locationID` ,
            `referencePrefixCHAR` ,
            `referencePrefixNumberOfDigits` ,
            `referencePrefixCurrentReference`
            )
            VALUES (
            NULL ,  '27', NULL ,  'PVOU',  '3',  '1'
          )  ; ");
    }

}
