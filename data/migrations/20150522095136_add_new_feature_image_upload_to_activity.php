<?php

use Phinx\Migration\AbstractMigration;

class AddNewFeatureImageUploadToActivity extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
      public function change()
      {
      }
     */

    /**
     * Migrate Up.
     */
    public function up()
    {
        $this->execute("INSERT INTO `feature` (`featureID` ,`featureName` ,`featureController` ,`featureAction` ,`featureCategory` ,`featureType` ,`moduleID`)
                        VALUES (NULL, 'Default', 'JobCard\\\Controller\\\Activity', 'saveImage', 'Application', '1', '8');");
        $id2 = $this->fetchRow("select LAST_INSERT_ID()")[0];
        $this->execute("INSERT INTO `roleFeature` (`roleFeatureID`, `roleID`, `featureID`, `roleFeatureEnabled`) VALUES (NULL, '1', $id2, '1');");

        $this->execute("INSERT INTO `feature` (`featureID` ,`featureName` ,`featureController` ,`featureAction` ,`featureCategory` ,`featureType` ,`moduleID`)
                        VALUES (NULL, 'Default', 'JobCard\\\Controller\\\Activity', 'deleteImage', 'Application', '1', '8');");
        $id2 = $this->fetchRow("select LAST_INSERT_ID()")[0];
        $this->execute("INSERT INTO `roleFeature` (`roleFeatureID`, `roleID`, `featureID`, `roleFeatureEnabled`) VALUES (NULL, '1', $id2, '1');");

        $this->execute("CREATE TABLE IF NOT EXISTS `attachment` (
            `attachmentID` int(11) NOT NULL AUTO_INCREMENT,
            `attachmentName` varchar(255) NOT NULL,
            `attachmentPath` varchar(255) NOT NULL,
            `attachmentSize` int(11) NOT NULL,
            `entityID` int(11) NOT NULL,
            `entitySubTypeID` int(11) NOT NULL,
            `userID` int(11) NOT NULL,
            PRIMARY KEY (`attachmentID`)
          ) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;");
    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }

}
