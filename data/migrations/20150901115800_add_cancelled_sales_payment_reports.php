<?php

use Phinx\Migration\AbstractMigration;

class AddCancelledSalesPaymentReports extends AbstractMigration
{

    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     */
//    public function change()
//    {
//
//    }

    public function up()
    {
        $rows = $this->fetchAll("SELECT roleID FROM `role`");

//        sales payments
        $this->execute("INSERT INTO `feature` (`featureID`, `featureName`, `featureController`, `featureAction`, `featureCategory`, `featureType`, `moduleID`) VALUES (NULL, 'View Report', 'Reporting\\\Controller\\\Generate\\\SalesPaymentReport', 'viewPaymentsSummery', 'Sales Payments', '0', '5');");
        $id1 = $this->fetchRow("select LAST_INSERT_ID()")[0];

        $this->execute("INSERT INTO `feature` (`featureID`, `featureName`, `featureController`, `featureAction`, `featureCategory`, `featureType`, `moduleID`) VALUES (NULL, 'Generate PDF', 'Reporting\\\Controller\\\Generate\\\SalesPaymentReport', 'generatePaymentsSummeryPdf', 'Sales Payments', '0', '5');");
        $id2 = $this->fetchRow("select LAST_INSERT_ID()")[0];

        $this->execute("INSERT INTO `feature` (`featureID`, `featureName`, `featureController`, `featureAction`, `featureCategory`, `featureType`, `moduleID`) VALUES (NULL, 'Generate Sheet', 'Reporting\\\Controller\\\Generate\\\SalesPaymentReport', 'generatePaymentsSummerySheet', 'Sales Payments', '0', '5');");
        $id3 = $this->fetchRow("select LAST_INSERT_ID()")[0];

//       cancelled sales payments
        $this->execute("INSERT INTO `feature` (`featureID`, `featureName`, `featureController`, `featureAction`, `featureCategory`, `featureType`, `moduleID`) VALUES (NULL, 'View Report', 'Reporting\\\Controller\\\Generate\\\SalesPaymentReport', 'viewCancelledPaymentsSummery', 'Cancelled Sales Payments', '0', '5');");
        $id4 = $this->fetchRow("select LAST_INSERT_ID()")[0];

        $this->execute("INSERT INTO `feature` (`featureID`, `featureName`, `featureController`, `featureAction`, `featureCategory`, `featureType`, `moduleID`) VALUES (NULL, 'Generate PDF', 'Reporting\\\Controller\\\Generate\\\SalesPaymentReport', 'generateCanclledPaymentsSummeryPdf', 'Cancelled Sales Payments', '0', '5');");
        $id5 = $this->fetchRow("select LAST_INSERT_ID()")[0];

        $this->execute("INSERT INTO `feature` (`featureID`, `featureName`, `featureController`, `featureAction`, `featureCategory`, `featureType`, `moduleID`) VALUES (NULL, 'Generate Sheet', 'Reporting\\\Controller\\\Generate\\\SalesPaymentReport', 'generateCancelledPaymentsSummerySheet', 'Cancelled Sales Payments', '0', '5');");
        $id6 = $this->fetchRow("select LAST_INSERT_ID()")[0];

        $this->execute("INSERT INTO `feature` (`featureID`, `featureName`, `featureController`, `featureAction`, `featureCategory`, `featureType`, `moduleID`) VALUES (NULL, 'Default', 'Reporting\\\Controller\\\SalesPaymentReport', 'index', 'Application', '1', '5');");
        $id7 = $this->fetchRow("select LAST_INSERT_ID()")[0];

        foreach ($rows as $row) {
            $roleID = $row['roleID'];
            $enabled = 1;
            $this->execute("INSERT INTO `roleFeature` (`roleFeatureID`, `roleID`, `featureID`, `roleFeatureEnabled`) VALUES (NULL, $roleID, $id1,$enabled);");
            $this->execute("INSERT INTO `roleFeature` (`roleFeatureID`, `roleID`, `featureID`, `roleFeatureEnabled`) VALUES (NULL, $roleID, $id2,$enabled);");
            $this->execute("INSERT INTO `roleFeature` (`roleFeatureID`, `roleID`, `featureID`, `roleFeatureEnabled`) VALUES (NULL, $roleID, $id3,$enabled);");
            $this->execute("INSERT INTO `roleFeature` (`roleFeatureID`, `roleID`, `featureID`, `roleFeatureEnabled`) VALUES (NULL, $roleID, $id4,$enabled);");
            $this->execute("INSERT INTO `roleFeature` (`roleFeatureID`, `roleID`, `featureID`, `roleFeatureEnabled`) VALUES (NULL, $roleID, $id5,$enabled);");
            $this->execute("INSERT INTO `roleFeature` (`roleFeatureID`, `roleID`, `featureID`, `roleFeatureEnabled`) VALUES (NULL, $roleID, $id6,$enabled);");
            $this->execute("INSERT INTO `roleFeature` (`roleFeatureID`, `roleID`, `featureID`, `roleFeatureEnabled`) VALUES (NULL, $roleID, $id7,$enabled);");
        }
    }

}
