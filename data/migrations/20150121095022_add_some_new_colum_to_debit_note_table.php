<?php

use Phinx\Migration\AbstractMigration;

class AddSomeNewColumToDebitNoteTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
      public function change()
      {
      }
     */

    /**
     * Migrate Up.
     */
    public function up()
    {
        $this->execute("SET SESSION old_alter_table=1;ALTER TABLE `debitNote` ADD `debitNoteCode` VARCHAR( 45 ) NOT NULL AFTER `debitNoteID`;
SET SESSION old_alter_table=1; ALTER TABLE `debitNote` CHANGE `debitNotePrice` `debitNoteTotal` DECIMAL( 12, 2 ) NULL DEFAULT '0.00';
SET SESSION old_alter_table=1; ALTER TABLE `debitNote` ADD `locationID` INT( 11 ) NOT NULL AFTER `debitNoteTotal`;
SET SESSION old_alter_table=1; ALTER TABLE `debitNote` ADD `paymentTermID` INT( 11 ) NOT NULL AFTER `locationID`;
SET SESSION old_alter_table=1; ALTER TABLE `debitNote` DROP FOREIGN KEY `fk_debitNote_purchaseInvoiceProductID0`;
SET SESSION old_alter_table=1; ALTER TABLE `debitNote` CHANGE `purchaseInvoiceProductID` `purchaseInvoiceID` INT( 11 ) NOT NULL;
SET SESSION old_alter_table=1; ALTER TABLE `debitNote` CHANGE `debitNoteQuantity` `debitNoteComment` VARCHAR( 1000 ) NULL DEFAULT NULL;
SET SESSION old_alter_table=1; ALTER TABLE `debitNote` ADD `debitNoteDate` DATE NOT NULL AFTER `debitNoteComment`;
SET SESSION old_alter_table=1; ALTER TABLE `debitNote` ADD `supplierID` INT( 11 ) NOT NULL AFTER `debitNoteDate`;
SET SESSION old_alter_table=1; ALTER TABLE `debitNote` ADD `statusID` INT( 11 ) NOT NULL AFTER `supplierID`;
SET SESSION old_alter_table=1; ALTER TABLE `debitNote` ADD `debitNotePaymentEligible` INT( 1 ) NULL DEFAULT '0' AFTER `paymentTermID`;
SET SESSION old_alter_table=1; ALTER TABLE `debitNote` ADD `debitNotePaymentAmount` DECIMAL( 12, 2 ) NULL DEFAULT '0.00' AFTER `debitNotePaymentEligible`;
SET SESSION old_alter_table=1; ALTER TABLE `debitNote` ADD `debitNoteSettledAmount` DECIMAL( 12, 2 ) NULL DEFAULT '0.00' AFTER `debitNotePaymentAmount`;
SET SESSION old_alter_table=1; ALTER TABLE `debitNote` CHANGE `debitNoteID` `debitNoteID` INT( 11 ) NOT NULL AUTO_INCREMENT ");
    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }

}
