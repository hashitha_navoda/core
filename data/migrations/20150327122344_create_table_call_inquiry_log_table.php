<?php

use Phinx\Migration\AbstractMigration;

class CreateTableCallInquiryLogTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
      public function change()
      {
      }
     */

    /**
     * Migrate Up.
     */
    public function up()
    {
        $this->execute("CREATE TABLE IF NOT EXISTS `inquiryLog` (
  `inquiryLogID` int(11) NOT NULL AUTO_INCREMENT,
  `inquiryLogReference` VARCHAR(100) NOT NULL,
  `inquiryLogDescription` VARCHAR(100) DEFAULT NULL,
  `customerID` int(11) NOT NULL,
  `customerProfileID` int(11) NOT NULL,
  `inquiryTypeID` int(11) NOT NULL,
  `inquiryLogDateAndTime` DATETIME NOT NULL,
  `inquiryLogCurrentUserID` int(11) NOT NULL,
  `inquiryStatusID` int(11) NOT NULL ,
  `projectID` VARCHAR(100) DEFAULT NULL,
  `jobID` VARCHAR(100) DEFAULT NULL,
  `activityID` VARCHAR(100) DEFAULT NULL,
  `locationID` int(11) NOT NULL,
  `statusID` int(11) NOT NULL,
  `inquiryLogTP1` VARCHAR(100) DEFAULT NULL,
  `inquiryLogTP2` VARCHAR(100) DEFAULT NULL,
  `inquiryLogAddress` VARCHAR(100) DEFAULT NULL,
   PRIMARY KEY (`inquiryLogID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;");
    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }

}
