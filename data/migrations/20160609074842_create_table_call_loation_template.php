<?php

use Phinx\Migration\AbstractMigration;

class CreateTableCallLoationTemplate extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function up()
    {
        $locationTpl = $this->table('locationTemplate', array('id' => 'locationTemplateID'));

        $locationTpl->addColumn('locationID', 'integer', array('limit' => 11))
                    ->addColumn('documentTypeID', 'integer', array('limit' => 11))
                    ->addColumn('templateID', 'integer', array('limit' => 11))
                    ->save();
    }
}
