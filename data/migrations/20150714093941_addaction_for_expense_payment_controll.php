<?php

use Phinx\Migration\AbstractMigration;

class AddactionForExpensePaymentControll extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
      public function change()
      {
      }
     */

    /**
     * Migrate Up.
     */
    public function up()
    {
        $rows = $this->fetchAll("SELECT roleID FROM `role`");

        $this->execute("INSERT INTO `feature` (`featureID`, `featureName`, `featureController`, `featureAction`, `featureCategory`, `featureType`, `moduleID`) VALUES (NULL, 'payment create', 'Expenses\\\Controller\\\ExpensePurchaseInvoice', 'create', 'payment voucher', '0', '9')");
        $id1 = $this->fetchRow("select LAST_INSERT_ID()")[0];

        $this->execute("INSERT INTO `feature` (`featureID`, `featureName`, `featureController`, `featureAction`, `featureCategory`, `featureType`, `moduleID`) VALUES (NULL, 'payment list', 'Expenses\\\Controller\\\ExpensePurchaseInvoice', 'list', 'payment voucher', '0', '9')");
        $id6 = $this->fetchRow("select LAST_INSERT_ID()")[0];

        $this->execute("INSERT INTO `feature` (`featureID`, `featureName`, `featureController`, `featureAction`, `featureCategory`, `featureType`, `moduleID`) VALUES (NULL, 'payment view', 'Expenses\\\Controller\\\ExpensePurchaseInvoice', 'view', 'payment voucher', '0', '9')");
        $id7 = $this->fetchRow("select LAST_INSERT_ID()")[0];

        $this->execute("INSERT INTO `feature` (`featureID`, `featureName`, `featureController`, `featureAction`, `featureCategory`, `featureType`, `moduleID`) VALUES (NULL, 'payment preview', 'Expenses\\\Controller\\\ExpensePurchaseInvoice', 'preview', 'payment voucher', '0', '9')");
        $id8 = $this->fetchRow("select LAST_INSERT_ID()")[0];

        $this->execute("INSERT INTO `feature` (`featureID`, `featureName`, `featureController`, `featureAction`, `featureCategory`, `featureType`, `moduleID`) VALUES (NULL, 'Default', 'Expenses\\\Controller\\\ExpensePurchaseInvoice', 'document', 'payment voucher', '0', '9')");
        $id9 = $this->fetchRow("select LAST_INSERT_ID()")[0];

        foreach ($rows as $row) {
            $roleID = $row['roleID'];
            if ($roleID == 1) {
                $enabled = 1;
            } else {
                $enabled = 0;
            }
            $this->execute("INSERT INTO `roleFeature` (`roleFeatureID`, `roleID`, `featureID`, `roleFeatureEnabled`) VALUES (NULL, $roleID, $id1,$enabled);");
            $this->execute("INSERT INTO `roleFeature` (`roleFeatureID`, `roleID`, `featureID`, `roleFeatureEnabled`) VALUES (NULL, $roleID, $id6,$enabled);");
            $this->execute("INSERT INTO `roleFeature` (`roleFeatureID`, `roleID`, `featureID`, `roleFeatureEnabled`) VALUES (NULL, $roleID, $id7,$enabled);");
            $this->execute("INSERT INTO `roleFeature` (`roleFeatureID`, `roleID`, `featureID`, `roleFeatureEnabled`) VALUES (NULL, $roleID, $id8,$enabled);");
            $this->execute("INSERT INTO `roleFeature` (`roleFeatureID`, `roleID`, `featureID`, `roleFeatureEnabled`) VALUES (NULL, $roleID, $id9,$enabled);");
        }
        //for API actions
        $this->execute("INSERT INTO `feature` (`featureID`, `featureName`, `featureController`, `featureAction`, `featureCategory`, `featureType`, `moduleID`) VALUES (NULL, 'Default', 'Expenses\\\Controller\\\API\\\ExpensePurchaseInvoiceAPI', 'getExpensePurchaseInvoiceReferenceForLocation', 'application', '1', '9')");
        $id2 = $this->fetchRow("select LAST_INSERT_ID()")[0];

        $this->execute("INSERT INTO `feature` (`featureID`, `featureName`, `featureController`, `featureAction`, `featureCategory`, `featureType`, `moduleID`) VALUES (NULL, 'Default', 'Expenses\\\Controller\\\API\\\ExpensePurchaseInvoiceAPI', 'savePi', 'application', '1', '9')");
        $id3 = $this->fetchRow("select LAST_INSERT_ID()")[0];

        $this->execute("INSERT INTO `feature` (`featureID`, `featureName`, `featureController`, `featureAction`, `featureCategory`, `featureType`, `moduleID`) VALUES (NULL, 'Default', 'Expenses\\\Controller\\\API\\\ExpensePurchaseInvoiceAPI', 'getPVsForSearch', 'application', '1', '9')");
        $id4 = $this->fetchRow("select LAST_INSERT_ID()")[0];

        $this->execute("INSERT INTO `feature` (`featureID`, `featureName`, `featureController`, `featureAction`, `featureCategory`, `featureType`, `moduleID`) VALUES (NULL, 'Default', 'Expenses\\\Controller\\\API\\\ExpensePurchaseInvoiceAPI', 'sendPVEmail', 'application', '1', '9')");
        $id5 = $this->fetchRow("select LAST_INSERT_ID()")[0];

        foreach ($rows as $row) {
            $roleID = $row['roleID'];
            $enabled = 1;
            $this->execute("INSERT INTO `roleFeature` (`roleFeatureID`, `roleID`, `featureID`, `roleFeatureEnabled`) VALUES (NULL, $roleID, $id2,$enabled);");
            $this->execute("INSERT INTO `roleFeature` (`roleFeatureID`, `roleID`, `featureID`, `roleFeatureEnabled`) VALUES (NULL, $roleID, $id3,$enabled);");
            $this->execute("INSERT INTO `roleFeature` (`roleFeatureID`, `roleID`, `featureID`, `roleFeatureEnabled`) VALUES (NULL, $roleID, $id4,$enabled);");
            $this->execute("INSERT INTO `roleFeature` (`roleFeatureID`, `roleID`, `featureID`, `roleFeatureEnabled`) VALUES (NULL, $roleID, $id5,$enabled);");
        }
    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }

}
