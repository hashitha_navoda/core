<?php

use Phinx\Migration\AbstractMigration;

class AddValuesToTheReferenceTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        // add values to the Reference Table
        $this->execute("INSERT INTO reference (referenceNameID, referenceName, referenceTypeID) VALUES ('35','Landing Cost','0')");

        // add values to the Reference prifix table
        $this->execute("INSERT INTO referencePrefix (referenceNameID, locationID, referencePrefixCHAR,referencePrefixNumberOfDigits,referencePrefixCurrentReference) VALUES ('35',null,'LDCT','6','1')");

        // add column to the compoundCostTemplate table
        $compoundCostTemplateTable = $this->table('compoundCostTemplate');
        $compoundCostTemplateColumn = $compoundCostTemplateTable->hasColumn('compoundCostTemplateCode');

        if (!$compoundCostTemplateColumn) {
            $compoundCostTemplateTable->addColumn('compoundCostTemplateCode', 'string', ['default' => null, 'null' => true,'limit' => 100])
              ->update();
        }

    }
}
