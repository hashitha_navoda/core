<?php

use Phinx\Migration\AbstractMigration;

class AddNewTablesForDraftInvoiceReturn extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $draftInvoiceReturns = $this->hasTable('draftInvoiceReturns');
        if (!$draftInvoiceReturns) {
            $newTable = $this->table('draftInvoiceReturns', ['id' => 'draftInvoiceReturnID']);
            $newTable->addColumn('invoiceReturnCode', 'string', ['limit' => 200])
                ->addColumn('invoiceReturnTotal', 'decimal', ['precision' => 20, 'scale' => 5, 'default' => null, 'null' => true])
                ->addColumn('locationID', 'integer', ['limit' => 20])
                ->addColumn('invoiceID', 'integer', ['limit' => 20])
                ->addColumn('invoiceReturnComment', 'text', ['default' => null, 'null' => true])
                ->addColumn('invoiceReturnDate', 'date', ['default' => null, 'null' => true])
                ->addColumn('customerID', 'integer', ['limit' => 20])
                ->addColumn('statusID', 'integer', ['limit' => 20, 'default' => null, 'null' => true])
                ->addColumn('customCurrencyId', 'integer', ['limit' => 20, 'default' => null, 'null' => true])
                ->addColumn('invoiceReturnCustomCurrencyRate', 'decimal', ['precision' => 20, 'scale' => 5, 'default' => null, 'null' => true])
                ->addColumn('invoiceReturnDiscountType', 'string', ['limit' => 255, 'default' => null, 'null' => true])
                ->addColumn('invoiceReturnTotalDiscount', 'float', ['default' => null, 'null' => true])
                ->addColumn('promotionID', 'integer', ['limit' => 20, 'default' => null, 'null' => true])
                ->addColumn('invoiceReturnPromotionDiscount', 'decimal', ['precision' => 12, 'scale' => 2, 'default' => null, 'null' => true])
                ->addColumn('entityID', 'integer', ['limit' => 20])
                ->addColumn('invHashValue', 'text', ['default' => null, 'null' => true])
                ->addColumn('actInvData', 'text', ['default' => null, 'null' => true])
                ->addColumn('isInvApproved', 'boolean', ['default' => 0, 'null' => true])
                ->addColumn('selectedWfID', 'integer', ['limit' => 20, 'default' => null, 'null' => true])
                ->addColumn('approvedBy', 'integer', ['limit' => 20, 'default' => null, 'null' => true])
                ->addColumn('rejectedBy', 'integer', ['limit' => 20, 'default' => null, 'null' => true])
                ->addColumn('approvedAt', 'datetime', ['default' => null, 'null' => true])
                ->addColumn('rejectedAt', 'datetime', ['default' => null, 'null' => true])
                ->save();
        }


        $draftInvoiceReturnProduct = $this->hasTable('draftInvoiceReturnProduct');
        if (!$draftInvoiceReturnProduct) {
            $newTable = $this->table('draftInvoiceReturnProduct', ['id' => 'draftInvoiceReturnProductID']);
            $newTable->addColumn('draftInvoiceReturnID', 'integer', ['limit' => 20])
                ->addColumn('productID', 'integer', ['limit' => 20])
                ->addColumn('invoiceProductID', 'integer', ['limit' => 20])
                ->addColumn('invoiceReturnProductPrice', 'decimal', ['precision' => 25, 'scale' => 10, 'default' => null, 'null' => true])
                ->addColumn('invoiceReturnProductDiscount', 'float', ['default' => 0, 'null' => true])
                ->addColumn('invoiceReturnProductDiscountType', 'string', ['default' => 'percentage', 'limit' => 255, 'null' => true])
                ->addColumn('invoiceReturnProductQuantity', 'decimal', ['precision' => 25, 'scale' => 10, 'default' => null, 'null' => true])
                ->addColumn('invoiceReturnProductTotal', 'decimal', ['precision' => 25, 'scale' => 10, 'default' => null, 'null' => true])
                ->save();
        }


        $draftInvoiceReturnProductTax = $this->hasTable('draftInvoiceReturnProductTax');
        if (!$draftInvoiceReturnProductTax) {
            $newTable = $this->table('draftInvoiceReturnProductTax', ['id' => 'draftInvoiceReturnProductTaxID']);
            $newTable->addColumn('draftInvoiceReturnProductID', 'integer', ['limit' => 20])
                ->addColumn('productID', 'integer', ['limit' => 20, 'default' => null, 'null' => true])
                ->addColumn('taxID', 'integer', ['limit' => 20, 'default' => null, 'null' => true])
                ->addColumn('invoiceReturnProductTaxPrecentage', 'decimal', ['precision' => 12, 'scale' => 5, 'default' => null, 'null' => true])
                ->addColumn('invoiceReturnProductTaxAmount', 'decimal', ['precision' => 20, 'scale' => 5, 'default' => null, 'null' => true])
                ->save();
        }


        $draftInvoiceReturnSubProduct = $this->hasTable('draftInvoiceReturnSubProduct');
        if (!$draftInvoiceReturnSubProduct) {
            $newTable = $this->table('draftInvoiceReturnSubProduct', ['id' => 'draftInvoiceReturnSubProductID']);
            $newTable->addColumn('draftInvoiceReturnProductID', 'integer', ['limit' => 20])
                ->addColumn('productBatchID', 'integer', ['limit' => 20, 'default' => null, 'null' => true])
                ->addColumn('productSerialID', 'integer', ['limit' => 20, 'default' => null, 'null' => true])
                ->addColumn('invoiceReturnSubProductQuantity', 'decimal', ['precision' => 20, 'scale' => 5, 'default' => null, 'null' => true])
                ->save();
        }
    }
}
