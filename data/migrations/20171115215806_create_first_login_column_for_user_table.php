<?php


use Phinx\Migration\AbstractMigration;

class CreateFirstLoginColumnForUserTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        //add column to the user table
        $creditNoteTable = $this->table('user');
        $inColumn = $creditNoteTable->hasColumn('userFirstLogin');

        if (!$inColumn) {
            $creditNoteTable->addColumn('userFirstLogin', 'boolean', array(
                                    'after' => 'userShowWelcome', 
                                    'default' => 1
                                )
                            )
                ->update();
            $this->execute("UPDATE user SET userFirstLogin = 0 where userShowWelcome = 1");
        }
    }
}
