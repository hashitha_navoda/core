<?php

use Phinx\Migration\AbstractMigration;

class CreateSampleTemplatesForPosTemplates extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function up()
    {
        $pdo = $this->getAdapter()->getConnection();
        $doc_ids = array(8, 24, 25, 26); // document type ids of templates

        // check if each document has a sample template
        foreach ($doc_ids as $id) {
            $doc = $this->fetchRow('SELECT count(*) as hasSample FROM template where documentTypeID = ' . $id . ' AND templateSample = 1');
            
            if(!$doc['hasSample']){
                $tpl = $this->fetchRow('SELECT * FROM template where documentTypeID = ' . $id . ' AND templateDefault = 1');

                $tpl_insert = <<<SQL
                    INSERT INTO `template` (
                        `templateName`,
                        `documentTypeID`,
                        `documentSizeID`,
                        `templateContent`,
                        `templateFooterDetails`,
                        `templateFooterShow`,
                        `templateDefault`,
                        `templateSample`,
                        `templateOptions`
                    ) VALUES (
                        :templateName,
                        :documentTypeID,
                        :documentSizeID,
                        :templateContent,
                        :templateFooterDetails,
                        :templateFooterShow,
                        :templateDefault,
                        :templateSample,
                        :templateOptions
                    )
SQL;

                $pdo->prepare($tpl_insert)->execute(array(
                    "templateName" => $tpl["templateName"],
                    "documentTypeID" => $tpl["documentTypeID"],
                    "documentSizeID" => $tpl["documentSizeID"],
                    "templateContent" => $tpl["templateContent"],
                    "templateFooterDetails" => $tpl["templateFooterDetails"],
                    "templateFooterShow" => $tpl["templateFooterShow"],
                    "templateDefault" => "0",
                    "templateSample" => "1",
                    "templateOptions" => $tpl["templateOptions"]
                ));
            }
        }
    }
}
