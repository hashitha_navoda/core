<?php

use Phinx\Migration\AbstractMigration;

class AddDefaultCustomerProfileIdForSalesInvoice extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $pdo = $this->getAdapter()->getConnection();
        
        $invoices = $this->fetchAll("SELECT * FROM `salesInvoice` WHERE `customerProfileID` IS NULL OR `customerProfileID` = 0");        
        
        foreach ($invoices as $invoice) 
        {
            if ($invoice['customerID'] != 0) {
                $defaultProfile = $this->fetchRow("SELECT * FROM `customerProfile` WHERE `customerID` = {$invoice['customerID']} AND isPrimary = 1");
                if ($defaultProfile)
                {                
                    $customerProfileUpdate = <<<SQL
                    UPDATE `salesInvoice` SET customerProfileID = :customerProfileID WHERE salesInvoiceID = :invoiceId;
SQL;
                    $pdo->prepare($customerProfileUpdate)->execute(array(
                        'customerProfileID' => $defaultProfile['customerProfileID'],
                        'invoiceId' => $invoice['salesInvoiceID']
                    ));
                }
            }
        }
    }
}
