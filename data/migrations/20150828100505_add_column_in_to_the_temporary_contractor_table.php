<?php

use Phinx\Migration\AbstractMigration;

class AddColumnInToTheTemporaryContractorTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
      public function change()
      {
      }
     */

    /**
     * Migrate Up.
     */
    public function up()
    {
        $table = $this->table('temporaryContractor');
        $columnExists = $table->hasColumn('status');
        if (!$columnExists) {
            $table->addColumn('status', 'boolean', array('default' => '1', 'after' => 'temporaryContractorTP'))
                    ->update();
        }
    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }

}
