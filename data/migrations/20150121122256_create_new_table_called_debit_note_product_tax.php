<?php

use Phinx\Migration\AbstractMigration;

class CreateNewTableCalledDebitNoteProductTax extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
      public function change()
      {
      }
     */

    /**
     * Migrate Up.
     */
    public function up()
    {
        $this->execute("CREATE TABLE IF NOT EXISTS `debitNoteProductTax` (
  `debitNoteProductTaxID` int(11) NOT NULL AUTO_INCREMENT,
  `debitNoteProductID` int(11) NOT NULL,
  `taxID` int(11) NOT NULL,
  `debitNoteProductTaxPercentage` decimal(12,2) DEFAULT '0.00',
  `debitNoteProductTaxAmount` decimal(12,2) DEFAULT '0.00',
  PRIMARY KEY (`debitNoteProductTaxID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;");
    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }

}
