<?php

use Phinx\Migration\AbstractMigration;

class AddNewColumsToGlAccountSetupTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $this->execute('SET SESSION old_alter_table=1; ALTER TABLE `glAccountSetup` ADD `glAccountSetupSalesAndCustomerReceivableAccountID` Int(11) NULL AFTER `glAccountSetupItemDefaultAdjusmentAccountID`; ');
        $this->execute('SET SESSION old_alter_table=1; ALTER TABLE `glAccountSetup` ADD `glAccountSetupSalesAndCustomerSalesAccountID` Int(11) NULL AFTER `glAccountSetupSalesAndCustomerReceivableAccountID`; ');
        $this->execute('SET SESSION old_alter_table=1; ALTER TABLE `glAccountSetup` ADD `glAccountSetupSalesAndCustomerSalesDiscountAccountID` Int(11) NULL AFTER `glAccountSetupSalesAndCustomerSalesAccountID`; ');
        $this->execute('SET SESSION old_alter_table=1; ALTER TABLE `glAccountSetup` ADD `glAccountSetupSalesAndCustomerAdvancePaymentAccountID` Int(11) NULL AFTER `glAccountSetupSalesAndCustomerSalesDiscountAccountID`; ');
        $this->execute('SET SESSION old_alter_table=1; ALTER TABLE `glAccountSetup` ADD `glAccountSetupPurchasingAndSupplierPayableAccountID` Int(11) NULL AFTER `glAccountSetupSalesAndCustomerAdvancePaymentAccountID`; ');
        $this->execute('SET SESSION old_alter_table=1; ALTER TABLE `glAccountSetup` ADD `glAccountSetupPurchasingAndSupplierPurchaseDiscountAccountID` Int(11) NULL AFTER `glAccountSetupPurchasingAndSupplierPayableAccountID`; ');
        $this->execute('SET SESSION old_alter_table=1; ALTER TABLE `glAccountSetup` ADD `glAccountSetupPurchasingAndSupplierGrnClearingAccountID` Int(11) NULL AFTER `glAccountSetupPurchasingAndSupplierPurchaseDiscountAccountID`; ');
        $this->execute('SET SESSION old_alter_table=1; ALTER TABLE `glAccountSetup` ADD `glAccountSetupPurchasingAndSupplierAdvancePaymentAccountID` Int(11) NULL AFTER `glAccountSetupPurchasingAndSupplierGrnClearingAccountID`; ');
        $this->execute('SET SESSION old_alter_table=1; ALTER TABLE `glAccountSetup` ADD `glAccountSetupGeneralExchangeVarianceAccountID` Int(11) NULL AFTER `glAccountSetupPurchasingAndSupplierAdvancePaymentAccountID`; ');
        $this->execute('SET SESSION old_alter_table=1; ALTER TABLE `glAccountSetup` ADD `glAccountSetupGeneralProfitAndLostYearAccountID` Int(11) NULL AFTER `glAccountSetupGeneralExchangeVarianceAccountID`; ');
    }
}
