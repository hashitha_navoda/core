<?php

use Phinx\Migration\AbstractMigration;

class AddNewRefernceNumberToTournament extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $this->execute("INSERT INTO `documentType` (`documentTypeID`, `documentTypeName`) VALUES ('39', 'Tournament');");
        $this->execute("INSERT INTO `documentType` (`documentTypeID`, `documentTypeName`) VALUES ('40', 'Event');");

        $singleRow1 = [
            'referenceNameID'    => 40,
            'referenceName'  => 'Tournament',
            'referenceTypeID' => 0,
        ];

        $singleRow2 = [
            'referenceNameID'    => 41,
            'referenceName'  => 'Event',
            'referenceTypeID' => 0,
        ];
        $abc = $this->table('reference');
        $abc->insert($singleRow1);
        $abc->insert($singleRow2);
        $abc->saveData();

        $singleRow1 = [
            'referenceNameID'    => 40,
            'referencePrefixCHAR'  => 'TOUR',
            'referencePrefixNumberOfDigits' => 3,
            'referencePrefixCurrentReference' => 1
        ];

        $singleRow2 = [
            'referenceNameID'    => 41,
            'referencePrefixCHAR'  => 'EVNT',
            'referencePrefixNumberOfDigits' => 3,
            'referencePrefixCurrentReference' => 1
        ];

        $table1 = $this->table('referencePrefix');
        $table1->insert($singleRow1);
        $table1->insert($singleRow2);
        $table1->saveData();
    }
}
