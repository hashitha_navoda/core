<?php

use Phinx\Migration\AbstractMigration;

class AddNewFeatureForSmsTypeDisable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $pdo = $this->getAdapter()->getConnection();
        $rows = $this->fetchAll("SELECT roleID FROM `role`");

        $dataArray = array(
            array(
                'featureID' => 'NULL',
                'featureName' => 'Sms Type Disable',
                'featureController' => 'Settings\Controller\API\SmsNotification',
                'featureAction' => 'disableSmsType',
                'featureCategory' => 'Sms Notification',
                'featureType' => '1',
                'moduleID' => '10'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'Save sms Type message',
                'featureController' => 'Settings\Controller\API\SmsNotification',
                'featureAction' => 'saveSmsTypeMessage',
                'featureCategory' => 'Sms Notification',
                'featureType' => '1',
                'moduleID' => '10'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'Update sms notification',
                'featureController' => 'Settings\Controller\API\SmsNotification',
                'featureAction' => 'updateSmsType',
                'featureCategory' => 'Sms Notification',
                'featureType' => '1',
                'moduleID' => '10'
            ),

         );

        foreach($dataArray as $feature){

            $featureInsert = <<<SQL
                INSERT INTO `feature`
                (
                    `featureName`,
                    `featureController`,
                    `featureAction`,
                    `featureCategory`,
                    `featureType`,
                    `moduleID`
                ) VALUES (
                    :featureName,
                    :featureController,
                    :featureAction,
                    :featureCategory,
                    :featureType,
                    :featureModuleId
                );

SQL;

            $pdo->prepare($featureInsert)->execute(array(
                'featureName' => $feature['featureName'],
                'featureController' => $feature['featureController'],
                'featureAction' => $feature['featureAction'],
                'featureCategory' => $feature['featureCategory'],
                'featureType' => $feature['featureType'],
                'featureModuleId' => $feature['moduleID']
            ));

            $featureID = $this->fetchRow("select LAST_INSERT_ID()")[0];

            foreach ($rows as $row) {
                $roleID = $row['roleID'];

                $enabled = 0;
                if ($roleID == 1 || $feature['featureType'] == 1) {
                    $enabled = 1;
                }

                $roleFeatureInsert =<<<SQL
                    INSERT INTO `roleFeature`
                    (
                        `roleID`,
                        `featureID`,
                        `roleFeatureEnabled`
                    ) VALUES(
                        :roleID,
                        :featureID,
                        :roleFeatureEnabled
                    )
SQL;
                $pdo->prepare($roleFeatureInsert)->execute(array(
                    'roleID' => $roleID,
                    'featureID' => $featureID,
                    'roleFeatureEnabled' => $enabled
                ));
            }
        }

    }
}
