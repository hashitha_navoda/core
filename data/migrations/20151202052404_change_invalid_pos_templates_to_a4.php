<?php

use Phinx\Migration\AbstractMigration;

class ChangeInvalidPosTemplatesToA4 extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
    public function change()
    {
    }
    */
    
    /**
     * Migrate Up.
     */
    public function up()
    {
        
        // Document types - 1 => Sales Invoice, 8 => POS Printout
        // Documents except the above 2, should not contain the POS size templates
        $this->execute("UPDATE `template`  SET `documentSizeID` = 1 WHERE `documentTypeID` NOT IN (1, 8) AND `documentSizeID` = 3;");
    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }
}