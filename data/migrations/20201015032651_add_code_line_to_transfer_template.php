<?php

use Phinx\Migration\AbstractMigration;

class AddCodeLineToTransferTemplate extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        ini_set('memory_limit', '-1');
        $templateContent = $this->fetchAll("SELECT templateContent  FROM `template` WHERE documentTypeID = 15");


        $newContent = $templateContent[0]['templateContent'];
        $newContent .= "\n";

        $appendContent = <<<HTML
        
<style type="text/css">
    #tbl_with_cost {
        display: none;
    }
</style>
HTML;
    
        $newContent .= $appendContent;
        $this->execute("UPDATE  `template` SET  `templateContent` ='{$newContent}' WHERE `templateID` =69");
    }
}
