<?php

use Phinx\Migration\AbstractMigration;

class AddLocationTaxColumnToLocationTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
    public function change()
    {
    }
    */

    /**
     * Migrate Up.
     */
    public function up()
    {
        $this->execute("ALTER TABLE `location`  ADD `locationTax` INT(1) NOT NULL AFTER `locationFax`;");
        $this->execute("UPDATE `location`  SET `locationTax` = 1;");
    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }
}
