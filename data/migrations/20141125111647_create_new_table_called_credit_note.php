<?php

use Phinx\Migration\AbstractMigration;

class CreateNewTableCalledCreditNote extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
      public function change()
      {
      }
     */

    /**
     * Migrate Up.
     */
    public function up()
    {
        $this->execute("ALTER TABLE `creditNote` ADD `creditNoteCode` VARCHAR( 45 ) NOT NULL AFTER `creditNoteID` ;
ALTER TABLE `creditNote` ADD `locationID` INT( 11 ) NOT NULL AFTER `creditNoteTotal` ;
ALTER TABLE `creditNote` ADD `creditNoteComment` VARCHAR( 1000 ) NULL AFTER `locationID` ;
ALTER TABLE `creditNote` ADD `statusID` INT( 11 ) NOT NULL AFTER `customerID` ;
ALTER TABLE `creditNote` DROP `creditNoteSettledAmount` ;
ALTER TABLE `creditNote` CHANGE `creditNoteID` `creditNoteID` INT( 11 ) NOT NULL AUTO_INCREMENT;
");
    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }

}
