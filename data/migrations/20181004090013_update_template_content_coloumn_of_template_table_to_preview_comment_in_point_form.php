<?php

use Phinx\Migration\AbstractMigration;

class UpdateTemplateContentColoumnOfTemplateTableToPreviewCommentInPointForm extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $this->execute('UPDATE `template` SET `templateContent` = REPLACE(`templateContent`,"\<abbr contenteditable\=\"false\" spellcheck\=\"false\"\>\[Comment\]\<\/abbr\>","\<pre style\=\"font\-family\:Helvetica\,Arial\,sans\-serif\" contenteditable\=\"false\" spellcheck\=\"false\"\>\[Comment\]\<\/pre\>") WHERE `documentTypeID` = 9');

        $this->execute('UPDATE `template` SET `templateContent` = REPLACE(`templateContent`,"\<abbr contenteditable\=\"false\" spellcheck\=\"false\"\>\[Comment\]\<\/abbr\>","\<pre style\=\"font\-family\:Helvetica\,Arial\,sans\-serif\" contenteditable\=\"false\" spellcheck\=\"false\"\>\[Comment\]\<\/pre\>") WHERE `documentTypeID` = 12');
        
        $this->execute('UPDATE `template` SET `templateContent` = REPLACE(`templateContent`,"\<abbr contenteditable\=\"false\" spellcheck\=\"false\"\>\[Comment\]\<\/abbr\>","\<pre style\=\"font\-family\:Helvetica\,Arial\,sans\-serif\" contenteditable\=\"false\" spellcheck\=\"false\"\>\[Comment\]\<\/pre\>") WHERE `documentTypeID` = 11');

        $this->execute('UPDATE `template` SET `templateContent` = REPLACE(`templateContent`,"\<abbr contenteditable\=\"false\" spellcheck\=\"false\"\>\[Comment\]\<\/abbr\>","\<pre style\=\"font\-family\:Helvetica\,Arial\,sans\-serif\" contenteditable\=\"false\" spellcheck\=\"false\"\>\[Comment\]\<\/pre\>") WHERE `documentTypeID` = 10');

        $this->execute('UPDATE `template` SET `templateContent` = REPLACE(`templateContent`,"\<abbr contenteditable\=\"false\" spellcheck\=\"false\"\>\[Payment Voucher Comment\]\<\/abbr\>","\<pre style\=\"font\-family\:Helvetica\,Arial\,sans\-serif\" contenteditable\=\"false\" spellcheck\=\"false\"\>\[Payment Voucher Comment\]\<\/pre\>") WHERE `documentTypeID` = 20');
    }
}
