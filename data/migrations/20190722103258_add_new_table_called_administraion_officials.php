<?php

use Phinx\Migration\AbstractMigration;

class AddNewTableCalledAdministraionOfficials extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->hasTable('administrationOfficials');

        if (!$table) {
            $table = $this->table('administrationOfficials', ['id' => 'administrationOfficialID']);
            $table->addColumn('firstName', 'string', ['limit' => 200]);
            $table->addColumn('lastName', 'string', ['limit' => 200]);
            $table->addColumn('telephoneNo', 'string', ['limit' => 20]);
            $table->addColumn('nic', 'string', ['limit' => 20]);
            $table->addColumn('address', 'string', ['limit' => 200]);
            $table->addColumn('email', 'string', ['limit' => 200]);
            $table->addColumn('dateOfBirth', 'date', ['default' => null, 'null' => true]);
            $table->addColumn('gender', 'integer', ['default' => null, 'null' => true]);
            $table->addColumn('entityID', 'integer', ['limit' => 20]);
            $table->addColumn('status', 'integer', ['limit' => 20]);
            $table->save();
        }
    }
}
