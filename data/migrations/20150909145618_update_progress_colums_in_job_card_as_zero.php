<?php

use Phinx\Migration\AbstractMigration;

class UpdateProgressColumsInJobCardAsZero extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
      public function change()
      {
      }
     */

    /**
     * Migrate Up.
     */
    public function up()
    {
        $this->execute("SET SESSION old_alter_table=1; ALTER TABLE `project` CHANGE `projectProgress` `projectProgress` DECIMAL( 5, 2 ) NULL DEFAULT '0.00';");
        $this->execute("SET SESSION old_alter_table=1; ALTER TABLE `job` CHANGE `jobProgress` `jobProgress` DECIMAL( 5, 2 ) NULL DEFAULT '0.00';");
        $this->execute("SET SESSION old_alter_table=1; ALTER TABLE `activity` CHANGE `activityProgress` `activityProgress` DECIMAL( 5, 2 ) NULL DEFAULT '0.00';");

        $this->execute("UPDATE `project` SET `projectProgress` = 0.00 WHERE `projectProgress` IS NULL ;");
        $this->execute("UPDATE `job` SET `jobProgress` = 0.00 WHERE `jobProgress` IS NULL ;");
        $this->execute("UPDATE `activity` SET `activityProgress` = 0.00 WHERE `activityProgress` IS NULL ;");
    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }

}
