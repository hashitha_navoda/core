<?php

use Phinx\Migration\AbstractMigration;

class AddNewTableCalledTemplateAttributeMap extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->hasTable('templateItemAttributeMap');

        if (!$table) {
            $table = $this->table('templateItemAttributeMap', ['id' => 'templateItemAttributeMapID']);
            $table->addColumn('templateID', 'integer', ['limit' => 11]);
            $table->addColumn('attributeOneID', 'integer',
                [
                    'null' => true,
                    'default' => null,
                ]);
            $table->addColumn('attributeTwoID', 'integer',
                [
                    'null' => true,
                    'default' => null,
                ]);
            $table->addColumn('attributeThreeID', 'integer',
                [
                    'null' => true,
                    'default' => null,
                ]);
            $table->save();
        }
    }
}
