<?php

use Phinx\Migration\AbstractMigration;

class AddNewPermissionsForNewlyAddedFunctions extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $pdo = $this->getAdapter()->getConnection();
        $rows = $this->fetchAll("SELECT roleID FROM `role`");

        $dataArray = array(
            
            array(
                'featureName' => 'Default',
                'featureController' => 'Invoice\Controller\API\Invoice',
                'featureAction' => 'sendApproverEmail',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '3'
            ),
            array(
                'featureName' => 'Default',
                'featureController' => 'Invoice\Controller\API\Invoice',
                'featureAction' => 'approve',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '3'
            ),
            array(
                'featureName' => 'Default',
                'featureController' => 'Invoice\Controller\API\Invoice',
                'featureAction' => 'getDraftInvoiceEditDetails',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '3'
            ),
            array(
                'featureName' => 'Default',
                'featureController' => 'Invoice\Controller\API\Invoice',
                'featureAction' => 'updateDraftInvoice',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '3'
            ),
            array(
                'featureName' => 'Default',
                'featureController' => 'Invoice\Controller\API\Invoice',
                'featureAction' => 'deleteDraftInvoice',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '3'
            ),
            array(
                'featureName' => 'Edit Draft Invoice',
                'featureController' => 'Invoice\Controller\Invoice',
                'featureAction' => 'draftEdit',
                'featureCategory' => 'Invoice',
                'featureType' => '0',
                'moduleID' => '3'
            ),
            array(
                'featureName' => 'View Draft Invoice',
                'featureController' => 'Invoice\Controller\Invoice',
                'featureAction' => 'viewDraftInvoice',
                'featureCategory' => 'Invoice',
                'featureType' => '0',
                'moduleID' => '3'
            ),
            array(
                'featureName' => 'View Draft Invoice List',
                'featureController' => 'Invoice\Controller\Invoice',
                'featureAction' => 'draftView',
                'featureCategory' => 'Invoice',
                'featureType' => '0',
                'moduleID' => '3'
            ),
            array(
                'featureName' => 'Default',
                'featureController' => 'Invoice\Controller\Invoice',
                'featureAction' => 'draftPreview',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '3'
            ),
            array(
                'featureName' => 'Default',
                'featureController' => 'Invoice\Controller\Invoice',
                'featureAction' => 'draftDocument',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '3'
            )
         );

        foreach($dataArray as $feature){

            $featureInsert = <<<SQL
                INSERT INTO `feature`
                (
                    `featureName`,
                    `featureController`,
                    `featureAction`,
                    `featureCategory`,
                    `featureType`,
                    `moduleID`
                ) VALUES (
                    :featureName,
                    :featureController,
                    :featureAction,
                    :featureCategory,
                    :featureType,
                    :moduleID
                );

SQL;

            $pdo->prepare($featureInsert)->execute($feature);

            $featureID = $this->fetchRow("select LAST_INSERT_ID()")[0];

            foreach ($rows as $row) {
                $roleID = $row['roleID'];
                $enabled = 0;
                if ($roleID == 1 || $feature['featureType'] == 1) {
                    $enabled = 1;
                } 

                $roleFeatureInsert =<<<SQL
                    INSERT INTO `roleFeature`
                    (
                        `roleID`,
                        `featureID`,
                        `roleFeatureEnabled`
                    ) VALUES(
                        :roleID,
                        :featureID,
                        :roleFeatureEnabled
                    )
SQL;
                $pdo->prepare($roleFeatureInsert)->execute(array(
                    'roleID' => $roleID,
                    'featureID' => $featureID,
                    'roleFeatureEnabled' => $enabled
                ));
            }
        }
    }
}
