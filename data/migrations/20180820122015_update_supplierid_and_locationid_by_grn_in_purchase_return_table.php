<?php

use Phinx\Migration\AbstractMigration;

class UpdateSupplieridAndLocationidByGrnInPurchaseReturnTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        //get normal return data set
        $normalPurchaseReturnSet = $this->query("SELECT * from purchaseReturn where directReturnFlag = 0")->fetchAll();

        foreach ($normalPurchaseReturnSet as $key => $value) {
            //get related grn details
            $grnData = $this->query("SELECT * from grn where grnID = ".$value['purchaseReturnGrnID'])->fetchAll();
            foreach ($grnData as $ke => $val) {
                //update purchase return table
                $updateTable = $this->query("UPDATE `purchaseReturn` SET `prSupplierID` =".$val['grnSupplierID']." ,`prLocationID` =".$val['grnRetrieveLocation']." WHERE `purchaseReturnID` =".$value['purchaseReturnID']);
            }
        }
    }
}
