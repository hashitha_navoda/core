<?php

use Phinx\Migration\AbstractMigration;

class AddNewColumnsToQuotationTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {

        $table = $this->table('quotation');
        $column1 = $table->hasColumn('quotationTotalDiscountType');

        if (!$column1) {
            $table->addColumn('quotationTotalDiscountType', 'text', ['default' => null, "null" => true])
              ->update();
        }

        $column2 = $table->hasColumn('quotationWiseTotalDiscount');

        if (!$column2) {
            $table->addColumn('quotationWiseTotalDiscount', 'float', ['default' => 0])
              ->update();
        }

        $column3 = $table->hasColumn('quotationDiscountRate');

        if (!$column3) {
           $table->addColumn('quotationDiscountRate', 'decimal', ['default' => 0, 'precision' => 20, 'scale' => 5])->update();
        }


    }
}
