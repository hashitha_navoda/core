<?php

use Phinx\Migration\AbstractMigration;

class AddNewTableCalledJobCardServiceIncentiveRateTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->hasTable('jobCardServiceIncentiveRate');

        if (!$table) {
            $table = $this->table('jobCardServiceIncentiveRate', ['id' => 'jobCardServiceIncentiveRateID']);
            $table->addColumn('jobCardIncentiveRateID', 'integer', ['limit' => 20,'default' => null, 'null' => true]);
            $table->addColumn('serviceID', 'integer', ['limit' => 20,'default' => null, 'null' => true]);
            $table->addColumn('serviceRate', 'decimal', ['default' => null, 'null' => true, 'precision' => 20, 'scale' => 5]);
            $table->addColumn('incentive', 'decimal', ['default' => null, 'null' => true, 'precision' => 20, 'scale' => 5]);
            $table->addColumn('incentiveType', 'integer', ['limit' => 20,'default' => null, 'null' => true]);
            $table->addColumn('deleted', 'boolean', ['default' => 0, 'null' => true]);
            $table->save();
        }
    }
}
