<?php

use Phinx\Migration\AbstractMigration;

class AddDesignationIdInTemporaryContractor extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
      public function change()
      {
      }
     */

    /**
     * Migrate Up.
     */
    public function up()
    {
//      add new column designationID into temporary contractor
        $this->execute("SET SESSION old_alter_table=1; "
                . "ALTER TABLE  `temporaryContractor` ADD  `designationID` INT NULL "
                . "AFTER  `temporaryContractorSecondName` ;");

        $temporaryContractors = $this->fetchAll("SELECT * FROM `temporaryContractor` WHERE `temporaryContractorDesignation` IS NOT NULL ;");

        foreach ($temporaryContractors as $temporaryContractor) {

//          check designation name is already exists in designation table
            $designationName = $temporaryContractor['temporaryContractorDesignation'];
            $fetchDesignationSql = <<<SQL
                    SELECT * FROM `designation`
                    WHERE `designation`.`designationName`
                    =
                    '{$designationName}' ;
SQL;
            $isExists = $this->fetchAll($fetchDesignationSql);

            if (!isset($isExists[0])) {
//              Create New Designation and entity related to that designation
                $getUserIDSql = <<<SQL
                        SELECT `user`.`userID`,`entity`.`deleted`
                        FROM `user`
                        LEFT JOIN `entity`
                        ON
                        `entity`.`deleted` = '0'
                        WHERE `user`.`roleID` = '1'
                        LIMIT 1 ;
SQL;
                $users = $this->fetchAll($getUserIDSql);
                $userID = $users[0]['userID'];

                $currentGMTDateTime = gmdate('Y-m-d H:i:s');

//              create entity
                $insertEntitySql = <<<SQL
                        INSERT INTO `entity` (
                        `createdBy`,
                        `createdTimeStamp`,
                        `updatedBy`,
                        `updatedTimeStamp`,
                        `deleted`
                        )
                            VALUES (
                        '{$userID}',
                        '{$currentGMTDateTime}',
                        '{$userID}',
                        '{$currentGMTDateTime}',
                        '0'
   ) ;
SQL;
                $this->execute($insertEntitySql);
                $entityID = $this->fetchRow("select LAST_INSERT_ID()")[0];

//              insert into new Designation
                $insertDesignationSql = <<<SQL
                        INSERT INTO `designation` (
                        `designationName`,
                        `entityID`,
                        `designationStatus`
                        )
                        VALUES (
                        '{$designationName}',
                        '{$entityID}',
                        '1'
   ) ;
SQL;
                $this->execute($insertDesignationSql);
            }

//          update the designatioID into temporary Contractor table
            $updateSql = <<<SQL
                        UPDATE `designation`, `temporaryContractor`
                        SET `temporaryContractor`.`designationID` =  `designation`.`designationID`
                        WHERE
                        `temporaryContractor`.`temporaryContractorDesignation`
                            =
                        `designation`.`designationName` AND
                        `temporaryContractor`.`temporaryContractorDesignation`
                        =
                        '{$designationName}' ;
SQL;
            $this->execute($updateSql);

//            add status column to temporary table
            $this->execute("ALTER TABLE  `temporaryContractor` ADD  `status` TINYINT NOT NULL DEFAULT  '1' AFTER  `temporaryContractorTP` ;");
        }
    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }

}
