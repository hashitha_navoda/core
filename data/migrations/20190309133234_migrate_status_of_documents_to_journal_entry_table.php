<?php

use Phinx\Migration\AbstractMigration;

class MigrateStatusOfDocumentsToJournalEntryTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        ini_set('memory_limit', '-1');
        $journalEntries = $this->fetchAll("SELECT journalEntryDocumentID,journalEntryID,documentTypeID FROM `journalEntry`");

        foreach ($journalEntries as $journalEntry) {
            $documentID = $journalEntry['journalEntryDocumentID'];
            $journalEntryID = $journalEntry['journalEntryID'];
            $status = null;
            if (!is_null($documentID)) {
                switch ($journalEntry['documentTypeID']) {
                    case '1':
                        $invoiceDetails = $this->fetchRow("SELECT statusID FROM `salesInvoice` WHERE `salesInvoiceID` = $documentID");
                        $status = $invoiceDetails['statusID'];
                        break;
                    
                    case '4':
                        $deliveryNoteDetails = $this->fetchRow("SELECT deliveryNoteStatus FROM `deliveryNote` WHERE `deliveryNoteID` = $documentID");
                        $status = $deliveryNoteDetails['deliveryNoteStatus'];
                        break;
                    
                    case '7':
                        $incomingPaymentDetails = $this->fetchRow("SELECT incomingPaymentStatus FROM `incomingPayment` WHERE `incomingPaymentID` = $documentID");
                        $status = $incomingPaymentDetails['incomingPaymentStatus'];
                        break;
                    
                    case '5':
                        $salesReturnDetails = $this->fetchRow("SELECT statusID FROM `salesReturn` WHERE `salesReturnID` = $documentID");
                        $status = $salesReturnDetails['statusID'];
                        break;
                    
                    case '6':
                        $creditNoteDetails = $this->fetchRow("SELECT statusID FROM `creditNote` WHERE `creditNoteID` = $documentID");
                        $status = $creditNoteDetails['statusID'];
                        break;
                    
                    case '17':
                        $creditNotePaymentDetails = $this->fetchRow("SELECT statusID FROM `creditNotePayment` WHERE `creditNotePaymentID` = $documentID");
                        $status = $creditNotePaymentDetails['statusID'];
                        break;
                    
                    case '12':
                        $purchaseInvocieDetails = $this->fetchRow("SELECT status FROM `purchaseInvoice` WHERE `purchaseInvoiceID` = $documentID");
                        $status = $purchaseInvocieDetails['status'];
                        break;
                    
                    case '14':
                        $supplierpaymentDetails = $this->fetchRow("SELECT outgoingPaymentStatus FROM `outgoingPayment` WHERE `outgoingPaymentID` = $documentID");
                        $status = $supplierpaymentDetails['outgoingPaymentStatus'];
                        break;
                    
                    case '11':
                        $purchaseReturnDetails = $this->fetchRow("SELECT purchaseReturnStatus FROM `purchaseReturn` WHERE `purchaseReturnID` = $documentID");
                        $status = $purchaseReturnDetails['purchaseReturnStatus'];
                        break;
                    
                    case '13':
                        $debitNoteDetails = $this->fetchRow("SELECT statusID FROM `debitNote` WHERE `debitNoteID` = $documentID");
                        $status = $debitNoteDetails['statusID'];
                        break;
                    
                    case '18':
                        $debitNotePaymentDetails = $this->fetchRow("SELECT statusID FROM `debitNotePayment` WHERE `debitNotePaymentID` = $documentID");
                        $status = $debitNotePaymentDetails['statusID'];
                        break;
                    
                    case '10':
                        $grnDetails = $this->fetchRow("SELECT status FROM `grn` WHERE `grnID` = $documentID");
                        $status = $grnDetails['status'];
                        break;
                    
                    case '16':
                        $goodIssueDetails = $this->fetchRow("SELECT goodsIssueStatus FROM `goodsIssue` WHERE `goodsIssueID` = $documentID");
                        $status = $goodIssueDetails['goodsIssueStatus'];
                        break;
                    
                    case '20':
                        $paymentVoucherDetails = $this->fetchRow("SELECT paymentVoucherStatus FROM `paymentVoucher` WHERE `paymentVoucherID` = $documentID");
                        $status = $paymentVoucherDetails['paymentVoucherStatus'];
                        break;
                    
                    case '32':
                        $status = 5;
                        break;
                    
                    default:
                        $status = null;
                        break;
                }
            }
            if (!is_null($status)) {
                 $this->execute("UPDATE `journalEntry` SET `documentStatus` = $status WHERE `journalEntry`.`journalEntryID` = $journalEntryID");       
            }
        }
    }
}
