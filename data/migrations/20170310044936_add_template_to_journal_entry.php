<?php

use Phinx\Migration\AbstractMigration;

class AddTemplateToJournalEntry extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $sql = <<<EOF
                INSERT INTO `template` (`templateName`, `documentTypeID`, `documentSizeID`, `templateContent`, `templateFooterDetails`, `templateFooterShow`, `templateDefault`, `templateSample`) VALUES
('Template', 33, 1, '<table class="table table-bordered" width="100%">
<tbody>
<tr>
<td><img class="logo" style="background: #e0e0e0; max-height: 75px; display: block; text-align: center;" src="[Company Logo]" alt="Company Logo" /><span style="font-weight: bold;"><abbr contenteditable="false" spellcheck="false">[Company Name]</abbr> </span><br /><abbr contenteditable="false" spellcheck="false">[Company Address]</abbr> <br />Tel: <abbr contenteditable="false" spellcheck="false">[Company Tel. No]</abbr> <br />Email: <abbr contenteditable="false" spellcheck="false">[Company Email]</abbr> <br />Tax No: <abbr contenteditable="false" spellcheck="false">[Company Tax Reg. No]</abbr>&nbsp;</td>
<td>
<h2 align="right">Journal Entry</h2>
</td>
</tr>
<tr>
<td>&nbsp;</td>
<td class="relative_data" rowspan="2" align="right">
<table class="table table-bordered table-condensed" width="100%">
<tbody>
<tr>
<td>JE Code:</td>
<td style="text-align: right;"><abbr contenteditable="false" spellcheck="false">[JE. Code]</abbr>&nbsp;</td>
</tr>
<tr>
<td>JE Date:</td>
<td style="text-align: right;"><abbr contenteditable="false" spellcheck="false">[JE. Date]</abbr>&nbsp;</td>
</tr>
<tr>
<td style="vertical-align: top;">JE Location:</td>
<td style="vertical-align: top; text-align: right;"><abbr contenteditable="false" spellcheck="false">[JE. Location]</abbr>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;</td>
<td style="text-align: right;">&nbsp;</td>
</tr>
</tbody>
</table>
</td>
</tr>
<tr>
<td>&nbsp;</td>
</tr>
</tbody>
</table>
<noneditabletable>
<table class="noneditable table table-bordered" style="width: 100%; margin-bottom: 100px;" title="" contenteditable="false" data-original-title="">
<tbody>
<tr>
<td colspan="2">
<div class="qout_body"><hr />
<table id="item_tbl" class="table table-bordered table-striped" width="100%"><colgroup><col width="10px" /><col width="90px" /><col width="290px" /><col width="50px" /><col width="100px" /><col width="100px" /> </colgroup>
<thead>
<tr><th colspan="2">Product code</th><th colspan="1">Product</th><th colspan="1">Qty</th><th colspan="1">Price</th><th colspan="1">Total</th></tr>
</thead>
<tbody>
<tr>
<td>1</td>
<td align="left">12</td>
<td align="left">dell_lap<br /><small>(Disc. 1.00)</small></td>
<td align="left">1</td>
<td align="right">100.00</td>
<td align="right">99.00</td>
</tr>
</tbody>
</table>
<div class="summary"><br /> <br /> <br /> <br />
<table id="" class="table" width="100%">
<tbody>
<tr>
<td colspan="2" rowspan="3">&nbsp;</td>
<td colspan="3">Sub Total</td>
<td class="text-right">99.00</td>
</tr>
<tr>
<td colspan="3">
<h3>Total</h3>
</td>
<td class="text-right">
<h3>99.00</h3>
</td>
</tr>
<!--                                <tr>
                                                                <td colspan="6"><div></div></td>
                                                            </tr>--></tbody>
</table>
</div>
</div>
</td>
</tr>
</tbody>
</table>
</noneditabletable><footer class="doc">
<table class="table table-bordered" width="100%">
<tbody>
<tr>
<td><abbr contenteditable="false" spellcheck="false">[Company Name]</abbr>&nbsp;</td>
<td>&nbsp;</td>
<td style="text-align: right;"><abbr contenteditable="false" spellcheck="false">[Sales Inv. Issue Date]</abbr>&nbsp;</td>
</tr>
</tbody>
</table>
</footer>', '', 1, 1, 0);
EOF;
        $this->execute($sql);
    }
}
