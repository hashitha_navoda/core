<?php

use Phinx\Migration\AbstractMigration;

class AddNewTableCalledPurchaseRequisitionProductTaxTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->hasTable('purchaseRequisitionProductTax');

        if (!$table) {
            $table = $this->table('purchaseRequisitionProductTax', ['id' => 'purchaseRequisitionProductTaxID']);
            $table->addColumn('purchaseRequisitionID', 'integer', ['limit' => 20]);
            $table->addColumn('purchaseRequisitionProductID', 'integer', ['limit' => 20]);
            $table->addColumn('purchaseRequisitionTaxID', 'integer', ['limit' => 20,'default' => null, 'null' => true]);
            $table->addColumn('purchaseRequisitionTaxPrecentage', 'float', ['default' => null, 'null' => true]);
            $table->addColumn('purchaseRequisitionTaxAmount', 'decimal', ['precision' => 20, 'scale' => 10,'default' => null, 'null' => true]);
            $table->save();
        }
    }
}
