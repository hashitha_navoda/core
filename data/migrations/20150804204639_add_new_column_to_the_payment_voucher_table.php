<?php

use Phinx\Migration\AbstractMigration;

class AddNewColumnToThePaymentVoucherTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
      public function change()
      {
      }
     */

    /**
     * Migrate Up.
     */
    public function up()
    {
        $table = $this->table('paymentVoucher');
        $table->addColumn('paymentVoucherApproved', 'boolean', array('after' => 'paymentVoucherExpenseType', 'null' => 'allow'))
                ->addColumn('paymentVoucherHashValue', 'string', array('after' => 'paymentVoucherApproved', 'null' => 'allow', 'limit' => 50))
                ->update();
    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }

}
