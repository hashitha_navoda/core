<?php

use Phinx\Migration\AbstractMigration;

class AddNewColumToPriceListsTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
      public function change()
      {
      }
     */

    /**
     * Migrate Up.
     */
    public function up()
    {
        $this->execute("SET SESSION old_alter_table=1; ALTER TABLE `priceList` ADD `priceListState` TINYINT( 4 ) NOT NULL DEFAULT '0' AFTER `entityID` ;");
    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }

}
