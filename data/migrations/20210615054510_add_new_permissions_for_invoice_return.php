<?php

use Phinx\Migration\AbstractMigration;

class AddNewPermissionsForInvoiceReturn extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $pdo = $this->getAdapter()->getConnection();
        $rows = $this->fetchAll("SELECT roleID FROM `role`");

        $dataArray = array(
            
            array(
                'featureName' => 'Allow Change Display Setting',
                'featureController' => 'Settings\Controller\DisplaySettings',
                'featureAction' => 'index',
                'featureCategory' => 'General Settings',
                'featureType' => '0',
                'moduleID' => '7'
            ),
            array(
                'featureName' => 'Default',
                'featureController' => 'Settings\Controller\API\DisplaySettings',
                'featureAction' => 'update',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '7'
            ),
            array(
                'featureName' => 'Create Invoice Return',
                'featureController' => 'Invoice\Controller\InvoiceReturn',
                'featureAction' => 'create',
                'featureCategory' => 'Sales Invoice Return',
                'featureType' => '0',
                'moduleID' => '3'
            ),
            array(
                'featureName' => 'Create Invoice Return',
                'featureController' => 'Invoice\Controller\InvoiceReturn',
                'featureAction' => 'view',
                'featureCategory' => 'Sales Invoice Return',
                'featureType' => '0',
                'moduleID' => '3'
            ),
            array(
                'featureName' => 'Default',
                'featureController' => 'Invoice\Controller\InvoiceReturn',
                'featureAction' => 'viewInvoiceReturnReceipt',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '3'
            ),
            array(
                'featureName' => 'Default',
                'featureController' => 'Invoice\Controller\InvoiceReturn',
                'featureAction' => 'documentPreview',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '3'
            ),
            array(
                'featureName' => 'Default',
                'featureController' => 'Invoice\Controller\InvoiceReturn',
                'featureAction' => 'document',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '3'
            ),
            array(
                'featureName' => 'Default',
                'featureController' => 'Invoice\Controller\InvoiceReturn',
                'featureAction' => 'documentPdf',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '3'
            ),
            array(
                'featureName' => 'Default',
                'featureController' => 'Invoice\Controller\API\InvoiceReturn',
                'featureAction' => 'saveInvoiceReturnDetails',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '3'
            ),
            array(
                'featureName' => 'Default',
                'featureController' => 'Invoice\Controller\API\InvoiceReturn',
                'featureAction' => 'retriveCustomerInvoiceReturn',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '3'
            ),
            array(
                'featureName' => 'Default',
                'featureController' => 'Invoice\Controller\API\InvoiceReturn',
                'featureAction' => 'retriveInvoiceReturnFromInvoiceReturnID',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '3'
            ),
            array(
                'featureName' => 'Default',
                'featureController' => 'Invoice\Controller\API\InvoiceReturn',
                'featureAction' => 'retriveInvoiceReturnByDatefilter',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '3'
            ),
            array(
                'featureName' => 'Default',
                'featureController' => 'Invoice\Controller\API\InvoiceReturn',
                'featureAction' => 'sendEmail',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '3'
            ),
            array(
                'featureName' => 'Default',
                'featureController' => 'Invoice\Controller\API\InvoiceReturn',
                'featureAction' => 'getInvoiceRelatedInvoiceRetunrs',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '3'
            ),
            array(
                'featureName' => 'Default',
                'featureController' => 'Invoice\Controller\API\InvoiceReturn',
                'featureAction' => 'searchAllInvoiceReturnForDropdown',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '3'
            ),
            array(
                'featureName' => 'Default',
                'featureController' => 'Invoice\Controller\API\InvoiceReturn',
                'featureAction' => 'deleteInvoiceReturn',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '3'
            ),
            array(
                'featureName' => 'Default',
                'featureController' => 'Invoice\Controller\API\Invoice',
                'featureAction' => 'getInvoiceDetailsAccordingToInvoiceReturns',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '3'
            ),
            array(
                'featureName' => 'Default',
                'featureController' => 'Invoice\Controller\API\Invoice',
                'featureAction' => 'getInvoiceDetailsForReturnNote',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '3'
            )
         );

        foreach($dataArray as $feature){

            $featureInsert = <<<SQL
                INSERT INTO `feature`
                (
                    `featureName`,
                    `featureController`,
                    `featureAction`,
                    `featureCategory`,
                    `featureType`,
                    `moduleID`
                ) VALUES (
                    :featureName,
                    :featureController,
                    :featureAction,
                    :featureCategory,
                    :featureType,
                    :moduleID
                );

SQL;

            $pdo->prepare($featureInsert)->execute($feature);

            $featureID = $this->fetchRow("select LAST_INSERT_ID()")[0];

            foreach ($rows as $row) {
                $roleID = $row['roleID'];
                $enabled = 0;
                if ($roleID == 1 || $feature['featureType'] == 1) {
                    $enabled = 1;
                } 

                $roleFeatureInsert =<<<SQL
                    INSERT INTO `roleFeature`
                    (
                        `roleID`,
                        `featureID`,
                        `roleFeatureEnabled`
                    ) VALUES(
                        :roleID,
                        :featureID,
                        :roleFeatureEnabled
                    )
SQL;
                $pdo->prepare($roleFeatureInsert)->execute(array(
                    'roleID' => $roleID,
                    'featureID' => $featureID,
                    'roleFeatureEnabled' => $enabled
                ));
            }
        }
    }
}
