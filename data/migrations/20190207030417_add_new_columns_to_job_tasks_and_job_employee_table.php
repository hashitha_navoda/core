<?php

use Phinx\Migration\AbstractMigration;

class AddNewColumnsToJobTasksAndJobEmployeeTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table1 = $this->table('jobTaskEmployee');
        $table2 = $this->table('jobTask');
        $column1 = $table1->hasColumn('isChanged');
        $column2 = $table2->hasColumn('departmentStationId');
       
        if (!$column1) {
            $table1->addColumn('isChanged', 'boolean', ['default' => false, 'null' => true])
                ->update();
        }
        if (!$column2) {
            $table2->addColumn('departmentStationId', 'integer', ['null' => true])
                ->update();
        }
    }
}
