<?php

use Phinx\Migration\AbstractMigration;

class AddNewColumCalledProductDefaultPurchasePriceInProductAndLocationProductTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
      public function change()
      {
      }
     */

    /**
     * Migrate Up.
     */
    public function up()
    {
        $this->execute("SET SESSION old_alter_table=1; ALTER TABLE `product` ADD `productDefaultPurchasePrice` DECIMAL( 12, 2 ) NULL DEFAULT '0.00' AFTER `productDefaultSellingPrice` ;");
        $this->execute("SET SESSION old_alter_table=1; ALTER TABLE `locationProduct` ADD `defaultPurchasePrice` DECIMAL( 20, 2 ) NULL DEFAULT '0.00' AFTER `defaultSellingPrice` ;");
    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }

}
