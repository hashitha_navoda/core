<?php

use Phinx\Migration\AbstractMigration;

class AddNewColoumCalledYearendclosedInJournalEntryTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    
    /**
     * Migrate Up.
     */
    public function up()
    {
        $this->execute("SET SESSION old_alter_table=1; ALTER TABLE `journalEntry` ADD `yearEndClosed` tinyint(1) NOT NULL DEFAULT '0' after journalEntryTemplateStatus;");
    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }
}
