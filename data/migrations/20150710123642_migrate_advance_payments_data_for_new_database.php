<?php

use Phinx\Migration\AbstractMigration;

class MigrateAdvancePaymentsDataForNewDatabase extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
      public function change()
      {
      }
     */

    /**
     * Migrate Up.
     */
    public function up()
    {
        $payments = $this->fetchAll('SELECT * FROM incomingPayment');
        foreach ($payments as $payment) {
            if ($payment['incomingPaymentType'] == 'advance') {
                $paymentID = $payment['incomingPaymentID'];
                $paymentAmount = $payment['incomingPaymentAmount'];
                $paymentMethodID = $payment['advancePaymentMethodID'];

                if ($paymentMethodID == 1) {
                    $this->execute("INSERT INTO `incomingPaymentMethodCash` (`incomingPaymentMethodCashId`) VALUES (NULL)");
                    $cashID = $this->fetchRow("select LAST_INSERT_ID()")[0];
                    $this->execute("INSERT INTO `incomingPaymentMethod` (`incomingPaymentMethodId`, `incomingPaymentId`, `incomingPaymentMethodAmount`, `incomingPaymentMethodCashId`) VALUES (NULL, $paymentID, $paymentAmount,$cashID)");
                }

                $paymentMethodNumbers = $this->fetchAll("SELECT * FROM `paymentMethodsNumbers` WHERE `paymentID` = $paymentID");
                foreach ($paymentMethodNumbers as $paymentMethodNumber) {
                    if ($paymentMethodID == 2) {
                        $checqueNumber = $paymentMethodNumber['paymentMethodReferenceNumber'];
                        $bankName = $paymentMethodNumber['paymentMethodBank'];
                        $this->execute("INSERT INTO `incomingPaymentMethodCheque` (`incomingPaymentMethodChequeId`, `incomingPaymentMethodChequeNumber`, `incomingPaymentMethodChequeBankName`) VALUES (NULL, '$checqueNumber', '$bankName');");
                        $checqueID = $this->fetchRow("select LAST_INSERT_ID()")[0];
                        $this->execute("INSERT INTO `incomingPaymentMethod` (`incomingPaymentMethodId`, `incomingPaymentId`, `incomingPaymentMethodAmount`, `incomingPaymentMethodChequeId`) VALUES (NULL, $paymentID, $paymentAmount,$checqueID);");
                    } else if ($paymentMethodID == 3) {
                        $creditCardReferenceNumber = $paymentMethodNumber['paymentMethodReferenceNumber'];
                        $creditCardNumber = $paymentMethodNumber['paymentMethodCardID'];
                        $this->execute("INSERT INTO `incomingPaymentMethodCreditCard` (`incomingPaymentMethodCreditCardId`, `incomingPaymentMethodCreditCardNumber`, `incomingPaymentMethodCreditReceiptNumber`) VALUES (NULL, '$creditCardNumber', '$creditCardReferenceNumber');");
                        $creditCardID = $this->fetchRow("select LAST_INSERT_ID()")[0];
                        $this->execute("INSERT INTO `incomingPaymentMethod` (`incomingPaymentMethodId`, `incomingPaymentId`, `incomingPaymentMethodAmount`, `incomingPaymentMethodCreditCardId`) VALUES (NULL, $paymentID, $paymentAmount,$creditCardID);");
                    }
                }
            }
        }
    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }

}
