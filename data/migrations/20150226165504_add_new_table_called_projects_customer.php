<?php

use Phinx\Migration\AbstractMigration;

class AddNewTableCalledProjectsCustomer extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
      public function change()
      {
      }
     */

    /**
     * Migrate Up.
     */
    public function up()
    {
        $this->execute("CREATE TABLE IF NOT EXISTS `projectsCustomer` (
  `projectsCustomerId` INT NOT NULL,
  `projectId` INT NULL,
  `customerId` INT NULL,
  PRIMARY KEY (`projectsCustomerId`));");
    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }

}
