<?php

use Phinx\Migration\AbstractMigration;

class AddsDefaultQuantityInProductTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
      public function change()
      {
      }
     */

    /**
     * Migrate Up.
     */
    public function up()
    {
        $this->execute("set session old_alter_table=1; ALTER TABLE `product` ADD `productDefaultOpeningQuantity` INT NULL DEFAULT '0' AFTER `productDiscountValue` ;");
    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }

}
