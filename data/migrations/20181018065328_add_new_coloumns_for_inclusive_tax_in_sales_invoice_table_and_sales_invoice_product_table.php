<?php

use Phinx\Migration\AbstractMigration;

class AddNewColoumnsForInclusiveTaxInSalesInvoiceTableAndSalesInvoiceProductTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table1 = $this->table('salesInvoice');
        $column1 = $table1->hasColumn('inclusiveTax');
        if (!$column1) {
            $table1->addColumn('inclusiveTax', 'boolean', ['default' => false])
                  ->update();
        }

        $table2 = $this->table('salesInvoiceProduct');;
        $column2 = $table2->hasColumn('inclusiveTaxSalesInvoiceProductPrice');
        if (!$column2) {
            $table2->addColumn('inclusiveTaxSalesInvoiceProductPrice', 'decimal', ['default' => null, 'null' => true, 'precision' => 20, 'scale' => 5])
                  ->update();
        }
    }
}
