<?php

use Phinx\Migration\AbstractMigration;

class AddNewTableCalledBudget extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->hasTable('budget');

        if (!$table) {
            $table = $this->table('budget', ['id' => 'budgetId']);
            $table->addColumn('budgetName', 'string', ['limit' => 200]);
            $table->addColumn('fiscalPeriodID', 'integer', ['limit' => 11]);
            $table->addColumn('dimension', 'string', ['default' => null, 'null' => true,'limit' => 200]);
            $table->addColumn('dimensionValue', 'integer', ['default' => null, 'null' => true,'limit' => 11]);
            $table->addColumn('interval', 'string', ['limit' => 200]);
            $table->addColumn('deleted', 'boolean', ['default' => 0]);
            $table->save();
        }
    }
}
