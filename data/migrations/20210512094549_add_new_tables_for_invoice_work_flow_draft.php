<?php

use Phinx\Migration\AbstractMigration;

class AddNewTablesForInvoiceWorkFlowDraft extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $draftInvWf = $this->hasTable('draftInvWf');
        if (!$draftInvWf) {
            $newTable = $this->table('draftInvWf', ['id' => 'draftInvWfID']);
            $newTable->addColumn('salesInvoiceCode', 'string', ['limit' => 200])
                ->addColumn('customerID', 'integer', ['limit' => 20])
                ->addColumn('locationID', 'integer', ['limit' => 20])
                ->addColumn('salesInvoiceIssuedDate', 'date', ['default' => null, 'null' => true])
                ->addColumn('salesInvoiceOverDueDate', 'date', ['default' => null, 'null' => true])
                ->addColumn('salesInvoiceTaxAmount', 'float', ['default' => null, 'null' => true])
                ->addColumn('salesInvoiceTotalDiscount', 'float', ['default' => null, 'null' => true])
                ->addColumn('salesInvoiceWiseTotalDiscount', 'float', ['default' => null, 'null' => true])
                ->addColumn('salesinvoiceTotalAmount', 'decimal', ['precision' => 20, 'scale' => 5, 'default' => null, 'null' => true])
                ->addColumn('salesInvoicePayedAmount', 'decimal', ['precision' => 20, 'scale' => 5, 'default' => null, 'null' => true])
                ->addColumn('salesInvoiceComment', 'text', ['default' => null, 'null' => true])
                ->addColumn('salesInvoiceDeliveryCharge', 'decimal', ['precision' => 20, 'scale' => 5, 'default' => null, 'null' => true])
                ->addColumn('salesInvoiceTotalDiscountType', 'string', ['limit' => 255, 'default' => null, 'null' => true])
                ->addColumn('salesInvoiceDiscountRate', 'decimal', ['precision' => 20, 'scale' => 2, 'default' => null, 'null' => true])
                ->addColumn('salesInvoiceRemainingDiscValue', 'decimal', ['precision' => 20, 'scale' => 2, 'default' => null, 'null' => true])
                ->addColumn('paymentTermID', 'integer', ['limit' => 20,'default' => null, 'null' => true])
                ->addColumn('statusID', 'integer', ['limit' => 20, 'default' => null, 'null' => true])
                ->addColumn('quotationID', 'integer', ['limit' => 20, 'default' => null, 'null' => true])
                ->addColumn('deliveryNoteID', 'integer', ['limit' => 20, 'default' => null, 'null' => true])
                ->addColumn('salesOrderID', 'integer', ['limit' => 20, 'default' => null, 'null' => true])
                ->addColumn('activityID', 'integer', ['limit' => 20, 'default' => null, 'null' => true])
                ->addColumn('jobID', 'integer', ['limit' => 20, 'default' => null, 'null' => true])
                ->addColumn('projectID', 'integer', ['limit' => 20, 'default' => null, 'null' => true])
                ->addColumn('salesInvoiceShowTax', 'boolean', ['default' => 0, 'null' => true])
                ->addColumn('entityID', 'integer', ['limit' => 20])
                ->addColumn('pos', 'boolean', ['default' => 0, 'null' => true])
                ->addColumn('salesPersonID', 'integer', ['limit' => 20, 'default' => null, 'null' => true])
                ->addColumn('salesInvoiceSuspendedTax', 'boolean', ['default' => 0, 'null' => true])
                ->addColumn('salesInvoiceDeliveryAddress', 'text', ['default' => null, 'null' => true])
                ->addColumn('promotionID', 'integer', ['limit' => 20, 'default' => null, 'null' => true])
                ->addColumn('salesInvoicePromotionDiscount', 'decimal', ['precision' => 12, 'scale' => 2, 'default' => null, 'null' => true])
                ->addColumn('salesInvoiceRemainingPromotionDiscValue', 'decimal', ['precision' => 20, 'scale' => 2, 'default' => null, 'null' => true])
                ->addColumn('customCurrencyId', 'integer', ['limit' => 20, 'default' => null, 'null' => true])
                ->addColumn('salesInvoiceCustomCurrencyRate', 'decimal', ['precision' => 20, 'scale' => 5, 'default' => null, 'null' => true])
                ->addColumn('priceListId', 'integer', ['limit' => 20, 'default' => null, 'null' => true])
                ->addColumn('customerProfileID', 'integer', ['limit' => 20, 'default' => null, 'null' => true])
                ->addColumn('salesInvoiceDeliveryChargeEnable', 'boolean', ['default' => 0, 'null' => true])
                ->addColumn('salesInvoiceTemporaryCode', 'string', ['limit' => 255, 'default' => null, 'null' => true])
                ->addColumn('offlineSavedTime', 'datetime', ['default' => null, 'null' => true])
                ->addColumn('offlineUser', 'integer', ['limit' => 20, 'default' => null, 'null' => true])
                ->addColumn('discountOnlyOnEligible', 'boolean', ['default' => 0, 'null' => true])
                ->addColumn('customerCurrentOutstanding', 'decimal', ['precision' => 20, 'scale' => 5, 'default' => null, 'null' => true])
                ->addColumn('inclusiveTax', 'boolean', ['default' => 0, 'null' => true])
                ->addColumn('templateInvoiceTotal', 'decimal', ['precision' => 20, 'scale' => 5, 'default' => null, 'null' => true])
                ->addColumn('salesInvoiceServiceChargeAmount', 'decimal', ['precision' => 20, 'scale' => 5, 'default' => null, 'null' => true])
                ->addColumn('invHashValue', 'text', ['default' => null, 'null' => true])
                ->addColumn('actInvData', 'text', ['default' => null, 'null' => true])
                ->addColumn('isInvApproved', 'boolean', ['default' => 0, 'null' => true])
                ->addColumn('selectedWfID', 'integer', ['limit' => 20, 'default' => null, 'null' => true])
                ->addColumn('approvedBy', 'integer', ['limit' => 20, 'default' => null, 'null' => true])
                ->addColumn('rejectedBy', 'integer', ['limit' => 20, 'default' => null, 'null' => true])
                ->addColumn('approvedAt', 'datetime', ['default' => null, 'null' => true])
                ->addColumn('rejectedAt', 'datetime', ['default' => null, 'null' => true])
                ->save();
        }


        $draftInvProduct = $this->hasTable('draftInvProduct');
        if (!$draftInvProduct) {
            $newTable = $this->table('draftInvProduct', ['id' => 'draftInvProductID']);
            $newTable->addColumn('salesInvoiceID', 'integer', ['limit' => 20])
                ->addColumn('productID', 'integer', ['limit' => 20])
                ->addColumn('locationProductID', 'integer', ['limit' => 20])
                ->addColumn('salesInvoiceProductDescription', 'text', ['default' => null, 'null' => true])
                ->addColumn('salesInvoiceProductPrice', 'decimal', ['precision' => 25, 'scale' => 10, 'default' => null, 'null' => true])
                ->addColumn('salesInvoiceProductQuantity', 'decimal', ['precision' => 25, 'scale' => 10, 'default' => null, 'null' => true])
                ->addColumn('salesInvoiceProductTotal', 'decimal', ['precision' => 25, 'scale' => 10, 'default' => null, 'null' => true])
                ->addColumn('salesInvoiceProductDiscount', 'float', ['default' => 0, 'null' => true])
                ->addColumn('salesInvoiceProductDiscountType', 'string', ['default' => 'percentage', 'limit' => 255, 'null' => true])
                ->addColumn('salesInvoiceProductTax', 'string', ['default' => null, 'limit' => 255, 'null' => true])
                ->addColumn('copied', 'boolean', ['default' => 0, 'null' => true])
                ->addColumn('documentTypeID', 'integer', ['limit' => 20, 'default' => null, 'null' => true])
                ->addColumn('salesInvoiceProductDocumentID', 'integer', ['limit' => 20, 'default' => null, 'null' => true])
                ->addColumn('salesInvoiceProductSelectedUomId', 'integer', ['limit' => 20, 'default' => null, 'null' => true])
                ->addColumn('inclusiveTaxSalesInvoiceProductPrice', 'decimal', ['precision' => 20, 'scale' => 5, 'default' => null, 'null' => true])
                ->addColumn('itemSalesPersonID', 'integer', ['limit' => 20, 'default' => null, 'null' => true])
                ->addColumn('salesInvoiceProductMrpType', 'integer', ['limit' => 20, 'default' => null, 'null' => true])
                ->addColumn('salesInvoiceProductMrpValue', 'decimal', ['precision' => 20, 'scale' => 5, 'default' => null, 'null' => true])
                ->addColumn('salesInvoiceProductMrpPercentage', 'decimal', ['precision' => 20, 'scale' => 5, 'default' => null, 'null' => true])
                ->addColumn('salesInvoiceProductMrpAmount', 'decimal', ['precision' => 20, 'scale' => 5, 'default' => null, 'null' => true])
                ->addColumn('isFreeItem', 'boolean', ['default' => 0, 'null' => true])
                ->addColumn('hasFreeItem', 'boolean', ['default' => 0, 'null' => true])
                ->save();
        }


        $draftInvSubProduct = $this->hasTable('draftInvSubProduct');
        if (!$draftInvSubProduct) {
            $newTable = $this->table('draftInvSubProduct', ['id' => 'draftInvSubProductID']);
            $newTable->addColumn('salesInvocieProductID', 'integer', ['limit' => 20])
                ->addColumn('productBatchID', 'integer', ['limit' => 20, 'default' => null, 'null' => true])
                ->addColumn('productSerialID', 'integer', ['limit' => 20, 'default' => null, 'null' => true])
                ->addColumn('salesInvoiceSubProductQuantity', 'decimal', ['precision' => 20, 'scale' => 5, 'default' => null, 'null' => true])
                ->addColumn('salesInvoiceSubProductWarranty', 'integer', ['limit' => 20, 'default' => null, 'null' => true])
                ->addColumn('salesInvoiceSubProductWarrantyType', 'integer', ['limit' => 20, 'default' => 1])
                ->save();
        }

        
        $draftInvProductTax = $this->hasTable('draftInvProductTax');
        if (!$draftInvProductTax) {
            $newTable = $this->table('draftInvProductTax', ['id' => 'draftInvProductTaxID']);
            $newTable->addColumn('salesInvoiceProductID', 'integer', ['limit' => 20])
                ->addColumn('productID', 'integer', ['limit' => 20, 'default' => null, 'null' => true])
                ->addColumn('taxID', 'integer', ['limit' => 20, 'default' => null, 'null' => true])
                ->addColumn('salesInvoiceProductTaxPrecentage', 'decimal', ['precision' => 12, 'scale' => 5, 'default' => null, 'null' => true])
                ->addColumn('salesInvoiceProductTaxAmount', 'decimal', ['precision' => 20, 'scale' => 5, 'default' => null, 'null' => true])
                ->save();
        }

        $draftInvSalesPersons = $this->hasTable('draftInvSalesPersons');
        if (!$draftInvSalesPersons) {
            $newTable = $this->table('draftInvSalesPersons', ['id' => 'draftInvSalesPersonsID']);
            $newTable->addColumn('invoiceID', 'integer', ['limit' => 20, 'default' => null, 'null' => true])
                ->addColumn('salesPersonID', 'integer', ['limit' => 20, 'default' => null, 'null' => true])
                ->addColumn('relatedSalesAmount', 'decimal', ['precision' => 20, 'scale' => 5, 'default' => null, 'null' => true])
                ->save();
        }

        $draftInvFreeIssueItems = $this->hasTable('draftInvFreeIssueItems');
        if (!$draftInvFreeIssueItems) {
            $newTable = $this->table('draftInvFreeIssueItems', ['id' => 'draftInvFreeIssueItemsID']);
            $newTable->addColumn('invoiceID', 'integer', ['limit' => 20, 'default' => null, 'null' => true])
                ->addColumn('mainProductID', 'integer', ['limit' => 20, 'default' => null, 'null' => true])
                ->addColumn('mainSalesInvoiceProductID', 'integer', ['limit' => 20, 'default' => null, 'null' => true])
                ->addColumn('freeProductID', 'integer', ['limit' => 20, 'default' => null, 'null' => true])
                ->addColumn('freeQuantity', 'decimal', ['precision' => 20, 'scale' => 5, 'default' => null, 'null' => true])
                ->addColumn('salesInvoiceFreeItemSelectedUom', 'integer', ['limit' => 20, 'default' => null, 'null' => true])
                ->addColumn('locationID', 'integer', ['limit' => 20, 'default' => null, 'null' => true])
                ->save();
        }

        $draftInvServiceCharge = $this->hasTable('draftInvServiceCharge');
        if (!$draftInvServiceCharge) {
            $newTable = $this->table('draftInvServiceCharge', ['id' => 'draftInvServiceChargeID']);
            $newTable->addColumn('invoiceID', 'integer', ['limit' => 20, 'default' => null, 'null' => true])
                ->addColumn('serviceChargeTypeID', 'integer', ['limit' => 20, 'default' => null, 'null' => true])
                ->addColumn('serviceChargeAmount', 'decimal', ['precision' => 20, 'scale' => 5, 'default' => null, 'null' => true])
                ->save();
        }
    }
}
