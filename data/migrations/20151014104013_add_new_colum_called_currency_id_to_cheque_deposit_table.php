<?php

use Phinx\Migration\AbstractMigration;

class AddNewColumCalledCurrencyIdToChequeDepositTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
    public function change()
    {
    }
    */
    
    /**
     * Migrate Up.
     */
    public function up()
    {
        //add currency Id coloumn
        $this->execute("SET SESSION old_alter_table=1; ALTER TABLE `chequeDeposit`  ADD `chequeDepositCurrencyId` INT NULL DEFAULT NULL AFTER `chequeDepositDescription`;");
        //add cheque deposit currency rate coloumn
        $this->execute("SET SESSION old_alter_table=1; ALTER TABLE `chequeDeposit`  ADD `chequeDepositCurrencyRate` DECIMAL(20,5) NULL DEFAULT NULL AFTER `chequeDepositCurrencyId`;");
        //add cheque deposited account's currency currency rate coloumn
        $this->execute("SET SESSION old_alter_table=1; ALTER TABLE `chequeDeposit`  ADD `chequeDepositAccountCurrencyRate` DECIMAL(20,5) NULL DEFAULT NULL AFTER `chequeDepositCurrencyRate`;");
        //change chequeDepositAmount type length
        $this->execute("SET SESSION old_alter_table=1; ALTER TABLE `chequeDeposit` CHANGE `chequeDepositAmount` `chequeDepositAmount` DECIMAL(20,5) NULL DEFAULT NULL;");
        //set company currency id for existing deposits
        $displaySetup = $this->fetchRow('SELECT * FROM displaySetup');
        //get company currency rate
        $companyCurrency = $this->fetchRow("SELECT * FROM currency WHERE `currencyID` = {$displaySetup['currencyID']}");
        //get existing cheque deposits
        $deposits = $this->fetchAll("SELECT * FROM chequeDeposit");        
        foreach ($deposits as $deposit){
            $this->execute("UPDATE `chequeDeposit` SET `chequeDepositCurrencyId` = {$displaySetup['currencyID']}, `chequeDepositCurrencyRate` = {$companyCurrency['currencyRate']}, `chequeDepositAccountCurrencyRate` = {$companyCurrency['currencyRate']} WHERE `chequeDeposit`.`chequeDepositId` = {$deposit['chequeDepositId']};");
        }
    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }
}