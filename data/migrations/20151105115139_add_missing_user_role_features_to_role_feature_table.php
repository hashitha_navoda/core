<?php

use Phinx\Migration\AbstractMigration;

class AddMissingUserRoleFeaturesToRoleFeatureTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
    public function change()
    {
    }
    */
    
    /**
     * Migrate Up.
     */
    public function up()
    {
        //get existing role list
        $roles = $this->fetchAll("SELECT roleID FROM `role`");
        
        //get existing features
        $featureList = $this->fetchAll("SELECT * FROM `feature`");
        foreach ($featureList as $feature){
            //check feature type
            if($feature['featureType'] == '1'){                
                //check feature for role
                foreach ($roles as $role){
                    //get role feature
                    $roleFeature = $this->fetchRow("SELECT * FROM `roleFeature` WHERE `featureID`={$feature['featureID']} AND `roleID`={$role['roleID']};");
                    if($roleFeature){
                        if($roleFeature['roleFeatureEnabled'] == '0'){
                            //update roleFeatureEnabled
                            $this->execute("UPDATE `roleFeature` SET `roleFeatureEnabled` = '1' WHERE `roleFeature`.`roleFeatureID` = {$roleFeature['roleFeatureID']};");
                        }
                    } else {
                        //add role feature
                        $this->execute("INSERT INTO `roleFeature` (`roleFeatureID`, `roleID`, `featureID`, `roleFeatureEnabled`) VALUES (NULL, {$role['roleID']}, {$feature['featureID']}, '1');");
                    }
                }
            } else {
                //check feature for role
                foreach ($roles as $role){
                    //get role feature
                    $roleFeature = $this->fetchRow("SELECT * FROM `roleFeature` WHERE `featureID`={$feature['featureID']} AND `roleID`={$role['roleID']};");
                    if(!$roleFeature){
                        //add role feature
                        $this->execute("INSERT INTO `roleFeature` (`roleFeatureID`, `roleID`, `featureID`, `roleFeatureEnabled`) VALUES (NULL, {$role['roleID']}, {$feature['featureID']}, '1');");
                    }
                }
            }          
        }
    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }
}