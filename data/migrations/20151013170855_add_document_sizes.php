<?php

use Phinx\Migration\AbstractMigration;

class AddDocumentSizes extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
    public function change()
    {
    }
    */
    
    /**
     * Migrate Up.
     */
    public function up()
    {
        $this->execute("UPDATE documentSize SET documentSizeName = 'A4 (Portrait)' WHERE documentSizeName = 'A4';");
        $this->execute("UPDATE documentSize SET documentSizeName = 'A5 (Portrait)' WHERE documentSizeName = 'A5';");
        
        $this->execute("INSERT INTO documentSize VALUES (null, 'A4 (Landscape)', '297mm x 210mm');");
        $this->execute("INSERT INTO documentSize VALUES (null, 'A5 (Landscape)', '210mm x 148mm');");
    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }
}