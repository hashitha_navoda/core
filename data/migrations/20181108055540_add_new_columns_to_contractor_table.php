<?php

use Phinx\Migration\AbstractMigration;

class AddNewColumnsToContractorTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        // add column to the compoundCostTemplate table
        $contractorTable = $this->table('contractor');
        $contractorCodeColumn = $contractorTable->hasColumn('contractorCode');

        if (!$contractorCodeColumn) {
            $contractorTable->addColumn('contractorCode', 'string', ['limit' => 20, 'null' => false])
              ->update();
        }

        $IsPermanentFlagColumn = $contractorTable->hasColumn('IsPermanentFlag');

        if (!$IsPermanentFlagColumn) {
            $contractorTable->addColumn('IsPermanentFlag', 'boolean', ['default' => false])
              ->update();
        }

        $assignForJobColumn = $contractorTable->hasColumn('assignForJob');

        if (!$assignForJobColumn) {
            $contractorTable->addColumn('assignForJob', 'boolean', ['default' => false])
              ->update();
        }

        $assignForTaskColumn = $contractorTable->hasColumn('assignForTask');

        if (!$assignForTaskColumn) {
            $contractorTable->addColumn('assignForTask', 'boolean', ['default' => false])
              ->update();
        }
    }
}
