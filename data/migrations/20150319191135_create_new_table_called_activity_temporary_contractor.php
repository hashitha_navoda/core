<?php

use Phinx\Migration\AbstractMigration;

class CreateNewTableCalledActivityTemporaryContractor extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
      public function change()
      {
      }
     */

    /**
     * Migrate Up.
     */
    public function up()
    {
        $this->execute("CREATE TABLE IF NOT EXISTS `activityTemporaryContractor` (
                            `activityTemporaryContractorID` int(11) NOT NULL AUTO_INCREMENT,
                            `activityID` int(11) NOT NULL,
                            `temporaryContractorID` int(11) NOT NULL,
                             PRIMARY KEY (`activityTemporaryContractorID`)
                    ) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;");

        $this->execute("CREATE TABLE IF NOT EXISTS `activityOwner` (
                            `activityOwnerID` int(11) NOT NULL AUTO_INCREMENT,
                            `activityID` int(11) NOT NULL,
                            `employeeID` int(11) NOT NULL,
                             PRIMARY KEY (`activityOwnerID`)
                    ) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;");

        $this->execute("CREATE TABLE IF NOT EXISTS `activitySupervisor` (
                            `activitySupervisorID` int(11) NOT NULL AUTO_INCREMENT,
                            `activityID` int(11) NOT NULL,
                            `employeeID` int(11) NOT NULL,
                             PRIMARY KEY (`activitySupervisorID`)
                    ) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;");

        $this->execute("CREATE TABLE IF NOT EXISTS `activityBatch` (
                            `activityBatchID` int(11) NOT NULL AUTO_INCREMENT,
                            `activityID` int(11) NOT NULL,
                            `activityBatchCode` VARCHAR(150) NOT NULL,
                            `activityBatchQuantity` decimal(10,2) NULL,
                             PRIMARY KEY (`activityBatchID`)
                    ) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;");

        $this->execute("CREATE TABLE IF NOT EXISTS `activitySerial` (
                            `activitySerialID` int(11) NOT NULL AUTO_INCREMENT,
                            `activityID` int(11) NOT NULL,
                            `activityBatchID` int(11) NOT NULL,
                            `activitySerialCode` VARCHAR(150) NOT NULL,
                            `activitySerialSold` tinyint(1) NOT NULL DEFAULT '0',
                             PRIMARY KEY (`activitySerialID`)
                    ) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;");
    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }

}
