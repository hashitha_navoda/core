<?php

use Phinx\Migration\AbstractMigration;

class AddNewTableCalledServiceChargeTypeTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $serviceCharge = $this->hasTable('serviceChargeType');
        if (!$serviceCharge) {
            $newTable = $this->table('serviceChargeType', ['id' => 'serviceChargeTypeID']);
            $newTable->addColumn('serviceChargeTypeCode', 'string', ['limit' => 200])
                    ->addColumn('serviceChargeTypeName', 'string', ['limit' => 200])
                    ->addColumn('serviceChargeTypeGlAccountID', 'integer', ['limit' => 20])
                    ->addColumn('locationId', 'integer', ['limit' => 20])
                    ->addColumn('serviceChargeTypeIsDelete' , 'boolean', ['default' => false])
                    ->save();
        }

        $invoiceServiceCharge = $this->hasTable('invoiceServiceCharge');
        if (!$invoiceServiceCharge) {
            $newTable = $this->table('invoiceServiceCharge', ['id' => 'invoiceServiceChargeID']);
            $newTable->addColumn('invoiceID', 'integer', ['limit' => 20])
                    ->addColumn('serviceChargeTypeID', 'integer', ['limit' => 20])
                    ->addColumn('serviceChargeAmount', 'decimal', ['precision' => 20, 'scale' => 5, 'default' => null, 'null' => true])
                    ->save();
        }



    }
}
