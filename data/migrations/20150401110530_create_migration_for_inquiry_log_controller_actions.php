<?php

use Phinx\Migration\AbstractMigration;

class CreateMigrationForInquiryLogControllerActions extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
      public function change()
      {
      }
     */

    /**
     * Migrate Up.
     */
    public function up()
    {
        $this->execute("INSERT INTO `feature` (`featureID`, `featureName`, `featureController`, `featureAction`, `featureCategory`, `featureType`, `moduleID`) VALUES (NULL, 'Create', 'JobCard\\\Controller\\\InquiryLog', 'create', 'Inquiry Log', '0', '8');");
        $id = $this->fetchRow("select LAST_INSERT_ID()")[0];
        $this->execute("INSERT INTO `roleFeature` (`roleFeatureID` ,`roleID` ,`featureID` ,`roleFeatureEnabled`)VALUES (NULL , '1', $id, '1');");
        $this->execute("INSERT INTO `feature` (`featureID`, `featureName`, `featureController`, `featureAction`, `featureCategory`, `featureType`, `moduleID`) VALUES (NULL, 'Default', 'Invoice\\\Controller\\\CustomerAPI', 'getCustomerProfileDataByCustomerProfileID', 'Application', '1', '3');");
        $id = $this->fetchRow("select LAST_INSERT_ID()")[0];
        $this->execute("INSERT INTO `roleFeature` (`roleFeatureID` ,`roleID` ,`featureID` ,`roleFeatureEnabled`)VALUES (NULL , '1', $id, '1');");
        $this->execute("INSERT INTO `feature` (`featureID`, `featureName`, `featureController`, `featureAction`, `featureCategory`, `featureType`, `moduleID`) VALUES (NULL, 'Default', 'Core\\\Controller\\\JavascriptVariable', 'getAllInquiryStatusNames', 'Application', '1', '1');");
        $id = $this->fetchRow("select LAST_INSERT_ID()")[0];
        $this->execute("INSERT INTO `roleFeature` (`roleFeatureID` ,`roleID` ,`featureID` ,`roleFeatureEnabled`)VALUES (NULL , '1', $id, '1');");
        $this->execute("INSERT INTO `feature` (`featureID`, `featureName`, `featureController`, `featureAction`, `featureCategory`, `featureType`, `moduleID`) VALUES (NULL, 'Default', 'Core\\\Controller\\\JavascriptVariable', 'getAllActivityReferenceNumbers', 'Application', '1', '1');");
        $id = $this->fetchRow("select LAST_INSERT_ID()")[0];
        $this->execute("INSERT INTO `roleFeature` (`roleFeatureID` ,`roleID` ,`featureID` ,`roleFeatureEnabled`)VALUES (NULL , '1', $id, '1');");
        $this->execute("INSERT INTO `feature` (`featureID`, `featureName`, `featureController`, `featureAction`, `featureCategory`, `featureType`, `moduleID`) VALUES (NULL, 'Default', 'Core\\\Controller\\\JavascriptVariable', 'getAllInquiryTypes', 'Application', '1', '1');");
        $id = $this->fetchRow("select LAST_INSERT_ID()")[0];
        $this->execute("INSERT INTO `roleFeature` (`roleFeatureID` ,`roleID` ,`featureID` ,`roleFeatureEnabled`)VALUES (NULL , '1', $id, '1');");
        $this->execute("INSERT INTO `feature` (`featureID`, `featureName`, `featureController`, `featureAction`, `featureCategory`, `featureType`, `moduleID`) VALUES (NULL, 'Default', 'JobCard\\\Controller\\\API\\\InquiryLog', 'add', 'Application', '1', '8');");
        $id = $this->fetchRow("select LAST_INSERT_ID()")[0];
        $this->execute("INSERT INTO `roleFeature` (`roleFeatureID` ,`roleID` ,`featureID` ,`roleFeatureEnabled`)VALUES (NULL , '1', $id, '1');");
        $this->execute("INSERT INTO `feature` (`featureID`, `featureName`, `featureController`, `featureAction`, `featureCategory`, `featureType`, `moduleID`) VALUES (NULL, 'List', 'JobCard\\\Controller\\\InquiryLog', 'list', 'Inquiry Log', '0', '8');");
        $id = $this->fetchRow("select LAST_INSERT_ID()")[0];
        $this->execute("INSERT INTO `roleFeature` (`roleFeatureID` ,`roleID` ,`featureID` ,`roleFeatureEnabled`)VALUES (NULL , '1', $id, '1');");
        $this->execute("INSERT INTO `feature` (`featureID`, `featureName`, `featureController`, `featureAction`, `featureCategory`, `featureType`, `moduleID`) VALUES (NULL, 'View', 'JobCard\\\Controller\\\InquiryLog', 'view', 'Inquiry Log', '0', '8');");
        $id = $this->fetchRow("select LAST_INSERT_ID()")[0];
        $this->execute("INSERT INTO `roleFeature` (`roleFeatureID` ,`roleID` ,`featureID` ,`roleFeatureEnabled`)VALUES (NULL , '1', $id, '1');");
        $this->execute("INSERT INTO `feature` (`featureID`, `featureName`, `featureController`, `featureAction`, `featureCategory`, `featureType`, `moduleID`) VALUES (NULL, 'Edit', 'JobCard\\\Controller\\\InquiryLog', 'edit', 'Inquiry Log', '0', '8');");
        $id = $this->fetchRow("select LAST_INSERT_ID()")[0];
        $this->execute("INSERT INTO `roleFeature` (`roleFeatureID` ,`roleID` ,`featureID` ,`roleFeatureEnabled`)VALUES (NULL , '1', $id, '1');");
        $this->execute("INSERT INTO `feature` (`featureID`, `featureName`, `featureController`, `featureAction`, `featureCategory`, `featureType`, `moduleID`) VALUES (NULL, 'Default', 'JobCard\\\Controller\\\API\\\InquiryLog', 'update', 'Application', '1', '8');");
        $id = $this->fetchRow("select LAST_INSERT_ID()")[0];
        $this->execute("INSERT INTO `roleFeature` (`roleFeatureID` ,`roleID` ,`featureID` ,`roleFeatureEnabled`)VALUES (NULL , '1', $id, '1');");
        $this->execute("INSERT INTO `feature` (`featureID`, `featureName`, `featureController`, `featureAction`, `featureCategory`, `featureType`, `moduleID`) VALUES (NULL, 'Default', 'JobCard\\\Controller\\\API\\\InquiryLog', 'delete', 'Application', '1', '8');");
        $id = $this->fetchRow("select LAST_INSERT_ID()")[0];
        $this->execute("INSERT INTO `roleFeature` (`roleFeatureID` ,`roleID` ,`featureID` ,`roleFeatureEnabled`)VALUES (NULL , '1', $id, '1');");
        $this->execute("INSERT INTO `feature` (`featureID`, `featureName`, `featureController`, `featureAction`, `featureCategory`, `featureType`, `moduleID`) VALUES (NULL, 'Default', 'JobCard\\\Controller\\\API\\\InquiryLog', 'getInquiryLogBySearchKey', 'Application', '1', '8');");
        $id = $this->fetchRow("select LAST_INSERT_ID()")[0];
        $this->execute("INSERT INTO `roleFeature` (`roleFeatureID` ,`roleID` ,`featureID` ,`roleFeatureEnabled`)VALUES (NULL , '1', $id, '1');");
    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }

}
