<?php

use Phinx\Migration\AbstractMigration;

class AddNewColoumnCalledCashAccountIdInIncomingPaymentMethodCashTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        // add column to the incomingPaymentMethodCash table
        $table = $this->table('incomingPaymentMethodCash');
        $coloumn = $table->hasColumn('cashAccountID');

        if (!$coloumn) {
            $table->addColumn('cashAccountID', 'integer', ['default' => null, 'null' => true])
              ->update();
        }
    }
}
