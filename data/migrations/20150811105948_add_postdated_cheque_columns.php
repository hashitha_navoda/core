<?php

use Phinx\Migration\AbstractMigration;

class AddPostdatedChequeColumns extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
    public function change()
    {
    }
    */
    
    /**
     * Migrate Up.
     */
    public function up()
    {
        //add postdated cheque columns to incomingPaymentMethodCheque
        $this->execute("SET SESSION old_alter_table=1; ALTER TABLE  `incomingPaymentMethodCheque` ADD  `postdatedChequeStatus` TINYINT( 1 ) NOT NULL DEFAULT  '0',
ADD  `postdatedChequeDate` DATE NULL DEFAULT NULL ;");
        
        //add postdated cheque columns to outGoingPaymentMethodsNumbers
        $this->execute("SET SESSION old_alter_table=1; ALTER TABLE  `outGoingPaymentMethodsNumbers` ADD  `postdatedChequeStatus` TINYINT( 1 ) NOT NULL DEFAULT  '0',
ADD  `postdatedChequeDate` DATE NULL DEFAULT NULL ;");
    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }
}