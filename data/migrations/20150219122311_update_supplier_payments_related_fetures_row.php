<?php

use Phinx\Migration\AbstractMigration;

class UpdateSupplierPaymentsRelatedFeturesRow extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
      public function change()
      {
      }
     */

    /**
     * Migrate Up.
     */
    public function up()
    {
        $this->execute("UPDATE `feature` SET `featureType` = '0' WHERE `featureController` ='Inventory\\\Controller\\\OutGoingPayment';");
        $this->execute("UPDATE `feature` SET `featureCategory` = 'Supplier Advance Payment' ,`featureName` = 'Create' WHERE `featureController` ='Inventory\\\Controller\\\OutGoingPayment' AND `featureAction`='advancePayment';");
    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }

}
