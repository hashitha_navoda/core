<?php

use Phinx\Migration\AbstractMigration;

class AddNewTableCallInquiryLogAndComplainRelation extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
      public function change()
      {
      }
     */

    /**
     * Migrate Up.
     */
    public function up()
    {
        $tableExists = $this->hasTable('inquiryComplainRelation');
        if (!$tableExists) {
            $table = $this->table('inquiryComplainRelation', array('id' => 'inquiryComplainRelationID'));
            $table->addColumn('inquiryLogID', 'integer')
                    ->addColumn('documentTypeID', 'integer')
                    ->addColumn('inquiryComplainRelationDocumentID', 'integer')
                    ->save();
        }
        $inquiryLogTable = $this->table('inquiryLog');
        $columnExists = $inquiryLogTable->hasColumn('inquiryComplainType');
        if (!$columnExists) {
            $inquiryLogTable->addColumn('inquiryComplainType', 'string', array('limit' => 30, 'default' => 'Inquiry'))
                    ->update();
        }

        $data = $this->fetchAll("SELECT * FROM inquiryLog");
        foreach ($data as $details) {
            if ($details['projectID']) {
                $inqID = $details['inquiryLogID'];
                $documentTypeID = '21';
                $documentID = $details['projectID'];
                $existsRecord = $this->fetchAll("SELECT * FROM inquiryComplainRelation WHERE `inquiryLogID`='$inqID' and `documentTypeID`='$documentTypeID' and`inquiryComplainRelationDocumentID`='$documentID'");
                if (sizeof($existsRecord) == 0) {
                    $this->execute("INSERT INTO inquiryComplainRelation (`inquiryLogID`,`documentTypeID`,`inquiryComplainRelationDocumentID`) VALUE('$inqID','$documentTypeID','$documentID');");
                }
            }
            if ($details['jobID']) {
                $inqID = $details['inquiryLogID'];
                $documentTypeID = '22';
                $documentID = ($details['jobID']);
                $existsJobRecord = $this->fetchAll("SELECT * FROM inquiryComplainRelation WHERE `inquiryLogID`='$inqID' and `documentTypeID`='$documentTypeID' and `inquiryComplainRelationDocumentID`='$documentID';");
                if (sizeof($existsJobRecord) == 0) {
                    $this->execute("INSERT INTO inquiryComplainRelation (`inquiryLogID`,`documentTypeID`,`inquiryComplainRelationDocumentID`) VALUE('$inqID','$documentTypeID','$documentID');");
                }
            }
            if ($details['activityID']) {
                $inqID = $details['inquiryLogID'];
                $documentTypeID = '23';
                $documentID = $details['activityID'];
                $existsActivityRecord = $this->fetchAll("SELECT * FROM inquiryComplainRelation WHERE `inquiryLogID`='$inqID' and `documentTypeID`='$documentTypeID' and `inquiryComplainRelationDocumentID`='$documentID';");
                if (sizeof($existsActivityRecord) == 0) {
                    $this->execute("INSERT INTO inquiryComplainRelation (`inquiryLogID`,`documentTypeID`,`inquiryComplainRelationDocumentID`) VALUE('$inqID','$documentTypeID','$documentID');");
                }
            }
        }
    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }

}
