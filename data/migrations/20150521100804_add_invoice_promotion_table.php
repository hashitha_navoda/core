<?php

use Phinx\Migration\AbstractMigration;

class AddInvoicePromotionTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
      public function change()
      {
      }
     */

    /**
     * Migrate Up.
     */
    public function up()
    {
        $this->execute("CREATE TABLE IF NOT EXISTS `invoicePromotion` (
  `invoicePromotionID` int(11) NOT NULL AUTO_INCREMENT,
  `invoicePromotionPromotionID` int(11) NOT NULL,
  `invoicePromotionMinValue` decimal(12,2) NOT NULL,
  `invoicePromotionMaxValue` decimal(12,2) NOT NULL,
  `invoicePromotionDiscountType` int(11) NOT NULL,
  `invoicePromotionDiscountAmount` decimal(12,2) NOT NULL,
  PRIMARY KEY (`invoicePromotionID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;");
    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }

}
