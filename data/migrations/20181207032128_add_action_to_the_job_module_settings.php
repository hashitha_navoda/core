<?php

use Phinx\Migration\AbstractMigration;

class AddActionToTheJobModuleSettings extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $pdo = $this->getAdapter()->getConnection();
        $rows = $this->fetchAll("SELECT roleID FROM `role`");

        $dataArray = array(
            array(
                'featureID' => 'NULL',
                'featureName' => 'create',
                'featureController' => 'Jobs\Controller\Contractor',
                'featureAction' => 'create',
                'featureCategory' => 'Contrcator',
                'featureType' => '0',
                'moduleID' => '11'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'default',
                'featureController' => 'Jobs\Controller\API\Contractor',
                'featureAction' => 'create',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '11'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'default',
                'featureController' => 'Jobs\Controller\API\Contractor',
                'featureAction' => 'editContractor',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '11'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'default',
                'featureController' => 'Jobs\Controller\API\Contractor',
                'featureAction' => 'deleteContractor',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '11'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'default',
                'featureController' => 'Jobs\Controller\API\Contractor',
                'featureAction' => 'search',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '11'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'default',
                'featureController' => 'Jobs\Controller\API\Contractor',
                'featureAction' => 'changeStatusContractor',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '11'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'create',
                'featureController' => 'Jobs\Controller\Cost',
                'featureAction' => 'index',
                'featureCategory' => 'Cost',
                'featureType' => '0',
                'moduleID' => '11'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'default',
                'featureController' => 'Jobs\Controller\API\Cost',
                'featureAction' => 'searchCostTypeForDropdown',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '11'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'default',
                'featureController' => 'Jobs\Controller\API\Cost',
                'featureAction' => 'save',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '11'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'create',
                'featureController' => 'Jobs\Controller\CostType',
                'featureAction' => 'index',
                'featureCategory' => 'Cost Type',
                'featureType' => '0',
                'moduleID' => '11'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'default',
                'featureController' => 'Jobs\Controller\API\CostType',
                'featureAction' => 'add',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '11'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'default',
                'featureController' => 'Jobs\Controller\API\CostType',
                'featureAction' => 'getCostTypesBySearchKey',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '11'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'default',
                'featureController' => 'Jobs\Controller\API\CostType',
                'featureAction' => 'deleteCostTypeByCostTypeID',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '11'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'default',
                'featureController' => 'Jobs\Controller\API\CostType',
                'featureAction' => 'changeStatusID',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '11'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'create',
                'featureController' => 'Jobs\Controller\Department',
                'featureAction' => 'create',
                'featureCategory' => 'Department',
                'featureType' => '0',
                'moduleID' => '11'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'default',
                'featureController' => 'Jobs\Controller\API\Department',
                'featureAction' => 'createDepartment',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '11'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'default',
                'featureController' => 'User\Controller\UserApi',
                'featureAction' => 'getAllUserData',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '2'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'default',
                'featureController' => 'Jobs\Controller\API\Department',
                'featureAction' => 'changeStatusID',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '11'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'default',
                'featureController' => 'Jobs\Controller\API\Department',
                'featureAction' => 'update',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '11'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'default',
                'featureController' => 'Jobs\Controller\API\Department',
                'featureAction' => 'search',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '11'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'default',
                'featureController' => 'Jobs\Controller\API\Department',
                'featureAction' => 'searchDepartmentsForDropdown',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '11'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'default',
                'featureController' => 'Jobs\Controller\API\Department',
                'featureAction' => 'deleteDepartment',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '11'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'create',
                'featureController' => 'Jobs\Controller\Designation',
                'featureAction' => 'create',
                'featureCategory' => 'Designation',
                'featureType' => '0',
                'moduleID' => '11'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'default',
                'featureController' => 'Jobs\Controller\API\Designation',
                'featureAction' => 'createDesignation',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '11'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'default',
                'featureController' => 'Jobs\Controller\API\Designation',
                'featureAction' => 'changeStatusID',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '11'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'default',
                'featureController' => 'Jobs\Controller\API\Designation',
                'featureAction' => 'update',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '11'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'default',
                'featureController' => 'Jobs\Controller\API\Designation',
                'featureAction' => 'search',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '11'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'default',
                'featureController' => 'Jobs\Controller\API\Designation',
                'featureAction' => 'deleteDesignation',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '11'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'create',
                'featureController' => 'Jobs\Controller\Employee',
                'featureAction' => 'create',
                'featureCategory' => 'Employee',
                'featureType' => '0',
                'moduleID' => '11'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'default',
                'featureController' => 'Jobs\Controller\API\Employee',
                'featureAction' => 'createEmployee',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '11'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'default',
                'featureController' => 'Jobs\Controller\API\Employee',
                'featureAction' => 'changeStatusID',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '11'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'default',
                'featureController' => 'Jobs\Controller\API\Employee',
                'featureAction' => 'update',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '11'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'default',
                'featureController' => 'Jobs\Controller\API\Employee',
                'featureAction' => 'search',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '11'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'default',
                'featureController' => 'Jobs\Controller\API\Employee',
                'featureAction' => 'getEmployeeDesignation',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '11'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'default',
                'featureController' => 'Jobs\Controller\API\Employee',
                'featureAction' => 'deleteEmployee',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '11'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'default',
                'featureController' => 'Jobs\Controller\API\Employee',
                'featureAction' => 'searchJobSupervisorsForDropdown',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '11'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'default',
                'featureController' => 'Jobs\Controller\API\Employee',
                'featureAction' => 'searchJobManagersForDropdown',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '11'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'default',
                'featureController' => 'Jobs\Controller\API\Employee',
                'featureAction' => 'getEmployeeDetails',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '11'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'default',
                'featureController' => 'Jobs\Controller\API\Employee',
                'featureAction' => 'searchDesignationWiseEmployeesForDropdown',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '11'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'create',
                'featureController' => 'Jobs\Controller\GeneralSetup',
                'featureAction' => 'index',
                'featureCategory' => 'General Setup',
                'featureType' => '0',
                'moduleID' => '11'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'default',
                'featureController' => 'Jobs\Controller\API\GeneralSetup',
                'featureAction' => 'getGeneralSetting',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '11'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'default',
                'featureController' => 'Jobs\Controller\API\GeneralSetup',
                'featureAction' => 'getGeneralSetting',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '11'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'default',
                'featureController' => 'Jobs\Controller\API\GeneralSetup',
                'featureAction' => 'saveGeneralSetting',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '11'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'create',
                'featureController' => 'Jobs\Controller\Job',
                'featureAction' => 'create',
                'featureCategory' => 'Job',
                'featureType' => '0',
                'moduleID' => '11'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'List',
                'featureController' => 'Jobs\Controller\Job',
                'featureAction' => 'list',
                'featureCategory' => 'Job',
                'featureType' => '0',
                'moduleID' => '11'
            ), 
            array(
                'featureID' => 'NULL',
                'featureName' => 'View',
                'featureController' => 'Jobs\Controller\Job',
                'featureAction' => 'view',
                'featureCategory' => 'Job',
                'featureType' => '0',
                'moduleID' => '11'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'Edit',
                'featureController' => 'Jobs\Controller\Job',
                'featureAction' => 'edit',
                'featureCategory' => 'Job',
                'featureType' => '0',
                'moduleID' => '11'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'default',
                'featureController' => 'Jobs\Controller\API\Job',
                'featureAction' => 'saveJob',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '11'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'default',
                'featureController' => 'Jobs\Controller\API\Job',
                'featureAction' => 'searchJobsForDropdown',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '11'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'default',
                'featureController' => 'Jobs\Controller\API\Job',
                'featureAction' => 'loadJobsForDropdown',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '11'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'default',
                'featureController' => 'Jobs\Controller\API\Job',
                'featureAction' => 'getJobBySearchKey',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '11'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'default',
                'featureController' => 'Jobs\Controller\API\Job',
                'featureAction' => 'searchByProjectAndJob',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '11'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'default',
                'featureController' => 'Jobs\Controller\API\Job',
                'featureAction' => 'getJobSupervisorsByJobID',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '11'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'default',
                'featureController' => 'Jobs\Controller\API\Job',
                'featureAction' => 'delete',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '11'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'default',
                'featureController' => 'Jobs\Controller\API\Job',
                'featureAction' => 'getDataForUpdate',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '11'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'default',
                'featureController' => 'Jobs\Controller\API\Job',
                'featureAction' => 'updateJob',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '11'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'default',
                'featureController' => 'Jobs\Controller\API\Job',
                'featureAction' => 'getJobContractorsByJobID',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '11'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'default',
                'featureController' => 'Jobs\Controller\API\Job',
                'featureAction' => 'searchJobProjectForDropdown',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '11'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'default',
                'featureController' => 'Jobs\Controller\API\Job',
                'featureAction' => 'getJobTaskWithProductDetails',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '11'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'create',
                'featureController' => 'Jobs\Controller\JobSetup',
                'featureAction' => 'index',
                'featureCategory' => 'Job Setup',
                'featureType' => '0',
                'moduleID' => '11'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'default',
                'featureController' => 'Jobs\Controller\API\JobSetup',
                'featureAction' => 'add',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '11'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'default',
                'featureController' => 'Jobs\Controller\API\JobSetup',
                'featureAction' => 'getJobTypesBySearchKey',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '11'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'default',
                'featureController' => 'Jobs\Controller\API\JobSetup',
                'featureAction' => 'deleteJobTypeByJobTypeID',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '11'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'default',
                'featureController' => 'Jobs\Controller\API\JobSetup',
                'featureAction' => 'changeStatusID',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '11'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'create',
                'featureController' => 'Jobs\Controller\MaterialRequisition',
                'featureAction' => 'index',
                'featureCategory' => 'Material Requisition',
                'featureType' => '0',
                'moduleID' => '11'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'create',
                'featureController' => 'Jobs\Controller\MaterialRequisition',
                'featureAction' => 'search',
                'featureCategory' => 'Material Requisition',
                'featureType' => '0',
                'moduleID' => '11'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'default',
                'featureController' => 'Jobs\Controller\API\MaterialRequisition',
                'featureAction' => 'updateRequest',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '11'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'default',
                'featureController' => 'Jobs\Controller\API\MaterialRequisition',
                'featureAction' => 'save',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '11'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'default',
                'featureController' => 'Jobs\Controller\API\MaterialRequisition',
                'featureAction' => 'getRequestedProducts',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '11'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'List',
                'featureController' => 'Jobs\Controller\Progress',
                'featureAction' => 'list',
                'featureCategory' => 'Progress Update',
                'featureType' => '0',
                'moduleID' => '11'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'View Task',
                'featureController' => 'Jobs\Controller\Progress',
                'featureAction' => 'viewTask',
                'featureCategory' => 'Progress Update',
                'featureType' => '0',
                'moduleID' => '11'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'View Resource',
                'featureController' => 'Jobs\Controller\Progress',
                'featureAction' => 'viewResources',
                'featureCategory' => 'Progress Update',
                'featureType' => '0',
                'moduleID' => '11'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'View Request',
                'featureController' => 'Jobs\Controller\Progress',
                'featureAction' => 'viewRequests',
                'featureCategory' => 'Progress Update',
                'featureType' => '0',
                'moduleID' => '11'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'default',
                'featureController' => 'Jobs\Controller\API\Progress',
                'featureAction' => 'searchByProjectAndJob',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '11'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'default',
                'featureController' => 'Jobs\Controller\API\Progress',
                'featureAction' => 'startTask',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '11'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'default',
                'featureController' => 'Jobs\Controller\API\Progress',
                'featureAction' => 'getTaskDetailsById',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '11'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'default',
                'featureController' => 'Jobs\Controller\API\Progress',
                'featureAction' => 'updateJobTaskProgress',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '11'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'default',
                'featureController' => 'Jobs\Controller\API\Progress',
                'featureAction' => 'holdTask',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '11'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'default',
                'featureController' => 'Jobs\Controller\API\Progress',
                'featureAction' => 'endTask',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '11'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'default',
                'featureController' => 'Jobs\Controller\API\Progress',
                'featureAction' => 'getJobRelatedProducts',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '11'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'default',
                'featureController' => 'Jobs\Controller\API\Progress',
                'featureAction' => 'sendMaterialRequest',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '11'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'create',
                'featureController' => 'Jobs\Controller\Project',
                'featureAction' => 'index',
                'featureCategory' => 'Project',
                'featureType' => '0',
                'moduleID' => '11'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'List',
                'featureController' => 'Jobs\Controller\Project',
                'featureAction' => 'list',
                'featureCategory' => 'Project',
                'featureType' => '0',
                'moduleID' => '11'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'default',
                'featureController' => 'Jobs\Controller\API\Project',
                'featureAction' => 'initiateBusiness',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '11'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'default',
                'featureController' => 'Jobs\Controller\API\Project',
                'featureAction' => 'create',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '11'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'default',
                'featureController' => 'Jobs\Controller\API\Project',
                'featureAction' => 'getEditDetails',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '11'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'default',
                'featureController' => 'Jobs\Controller\API\Project',
                'featureAction' => 'update',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '11'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'default',
                'featureController' => 'Jobs\Controller\API\Project',
                'featureAction' => 'getProjectManagers',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '11'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'default',
                'featureController' => 'Jobs\Controller\API\Project',
                'featureAction' => 'search',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '11'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'default',
                'featureController' => 'Jobs\Controller\API\Project',
                'featureAction' => 'delete',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '11'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'default',
                'featureController' => 'Jobs\Controller\API\Project',
                'featureAction' => 'searchProjectsForDropdown',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '11'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'default',
                'featureController' => 'Jobs\Controller\API\Project',
                'featureAction' => 'getProjectDetails',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '11'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'create',
                'featureController' => 'Jobs\Controller\ProjectSetup',
                'featureAction' => 'index',
                'featureCategory' => 'Project Setup',
                'featureType' => '0',
                'moduleID' => '11'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'default',
                'featureController' => 'Jobs\Controller\API\ProjectSetup',
                'featureAction' => 'add',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '11'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'default',
                'featureController' => 'Jobs\Controller\API\ProjectSetup',
                'featureAction' => 'getProjectTypesBySearchKey',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '11'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'default',
                'featureController' => 'Jobs\Controller\API\ProjectSetup',
                'featureAction' => 'changeStatusID',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '11'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'default',
                'featureController' => 'Jobs\Controller\API\ProjectSetup',
                'featureAction' => 'deleteProjectTypeByProjectTypeID',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '11'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'default',
                'featureController' => 'Jobs\Controller\API\ProjectSetup',
                'featureAction' => 'searchProjectTypesForDropdown',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '11'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'create',
                'featureController' => 'Jobs\Controller\ResourceManagement',
                'featureAction' => 'index',
                'featureCategory' => 'Resource Management',
                'featureType' => '0',
                'moduleID' => '11'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'default',
                'featureController' => 'Jobs\Controller\API\ResourceManagement',
                'featureAction' => 'loadJobProducts',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '11'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'default',
                'featureController' => 'Jobs\Controller\API\ResourceManagement',
                'featureAction' => 'update',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '11'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'create',
                'featureController' => 'Jobs\Controller\TaskCard',
                'featureAction' => 'create',
                'featureCategory' => 'Task Card',
                'featureType' => '0',
                'moduleID' => '11'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'List',
                'featureController' => 'Jobs\Controller\TaskCard',
                'featureAction' => 'list',
                'featureCategory' => 'Task Card',
                'featureType' => '0',
                'moduleID' => '11'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'View Task Card',
                'featureController' => 'Jobs\Controller\TaskCard',
                'featureAction' => 'viewTaskCard',
                'featureCategory' => 'Task Card',
                'featureType' => '0',
                'moduleID' => '11'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'Edit Task Card',
                'featureController' => 'Jobs\Controller\TaskCard',
                'featureAction' => 'edit',
                'featureCategory' => 'Task Card',
                'featureType' => '0',
                'moduleID' => '11'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'default',
                'featureController' => 'Jobs\Controller\API\TaskCard',
                'featureAction' => 'updateTaskCard',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '11'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'default',
                'featureController' => 'Jobs\Controller\API\TaskCard',
                'featureAction' => 'saveTaskCard',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '11'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'default',
                'featureController' => 'Jobs\Controller\API\TaskCard',
                'featureAction' => 'getTaskCardBySearchKey',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '11'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'default',
                'featureController' => 'Jobs\Controller\API\TaskCard',
                'featureAction' => 'deleteTaskCardByTaskCardID',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '11'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'default',
                'featureController' => 'Jobs\Controller\API\TaskCard',
                'featureAction' => 'getTaskCardTaskProduct',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '11'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'default',
                'featureController' => 'Jobs\Controller\API\TaskCard',
                'featureAction' => 'getTaskCardDetails',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '11'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'default',
                'featureController' => 'Jobs\Controller\API\TaskCard',
                'featureAction' => 'searchTaskCardsForDropdown',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '11'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'create',
                'featureController' => 'Jobs\Controller\TaskSetup',
                'featureAction' => 'index',
                'featureCategory' => 'Task Setup',
                'featureType' => '0',
                'moduleID' => '11'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'default',
                'featureController' => 'Jobs\Controller\API\TaskSetup',
                'featureAction' => 'add',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '11'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'default',
                'featureController' => 'Jobs\Controller\API\TaskSetup',
                'featureAction' => 'getTaskBySearchKey',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '11'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'default',
                'featureController' => 'Jobs\Controller\API\TaskSetup',
                'featureAction' => 'deleteTaskByTaskID',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '11'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'default',
                'featureController' => 'Jobs\Controller\API\TaskSetup',
                'featureAction' => 'searchTasksForDropdown',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '11'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'create',
                'featureController' => 'Jobs\Controller\Team',
                'featureAction' => 'create',
                'featureCategory' => 'Team',
                'featureType' => '0',
                'moduleID' => '11'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'default',
                'featureController' => 'Jobs\Controller\API\Team',
                'featureAction' => 'createTeam',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '11'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'default',
                'featureController' => 'Jobs\Controller\API\Team',
                'featureAction' => 'changeStatusID',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '11'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'default',
                'featureController' => 'Jobs\Controller\API\Team',
                'featureAction' => 'update',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '11'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'default',
                'featureController' => 'Jobs\Controller\API\Team',
                'featureAction' => 'search',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '11'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'default',
                'featureController' => 'Jobs\Controller\API\Team',
                'featureAction' => 'getTeamEmployees',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '11'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'default',
                'featureController' => 'Jobs\Controller\API\Team',
                'featureAction' => 'deleteTeamByTeamID',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '11'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'create',
                'featureController' => 'Jobs\Controller\Vehicle',
                'featureAction' => 'create',
                'featureCategory' => 'Vehicle',
                'featureType' => '0',
                'moduleID' => '11'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'default',
                'featureController' => 'Jobs\Controller\API\Vehicle',
                'featureAction' => 'searchVehicles',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '11'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'default',
                'featureController' => 'Jobs\Controller\API\Vehicle',
                'featureAction' => 'addVehicle',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '11'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'default',
                'featureController' => 'Jobs\Controller\API\Vehicle',
                'featureAction' => 'editVehicle',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '11'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'default',
                'featureController' => 'Jobs\Controller\API\Vehicle',
                'featureAction' => 'deleteVehicle',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '11'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'default',
                'featureController' => 'Jobs\Controller\API\Vehicle',
                'featureAction' => 'changeStatusVehicle',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '11'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'default',
                'featureController' => 'Settings\Controller\API\Uom',
                'featureAction' => 'searchUomForDropdown',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '7'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'default',
                'featureController' => 'Core\Controller\JavascriptVariable',
                'featureAction' => 'jobSettings',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '1'
            ),
        );

        foreach($dataArray as $feature){

            $featureInsert = <<<SQL
                INSERT INTO `feature`
                (
                    `featureName`,
                    `featureController`,
                    `featureAction`,
                    `featureCategory`,
                    `featureType`,
                    `moduleID`
                ) VALUES (
                    :featureName,
                    :featureController,
                    :featureAction,
                    :featureCategory,
                    :featureType,
                    :featureModuleId
                );

SQL;

            $pdo->prepare($featureInsert)->execute(array(
                'featureName' => $feature['featureName'],
                'featureController' => $feature['featureController'],
                'featureAction' => $feature['featureAction'],
                'featureCategory' => $feature['featureCategory'],
                'featureType' => $feature['featureType'],
                'featureModuleId' => $feature['moduleID']
            ));

            $featureID = $this->fetchRow("select LAST_INSERT_ID()")[0];

            foreach ($rows as $row) {
                $roleID = $row['roleID'];

                if ($roleID == 1) {
                    $enabled = 1;
                } else {

                   //for enable api calls for every role
                    if($feature['featureType'] == 1){
                        $enabled = 1;
                    } else {
                        $enabled = 0;
                    }

                }

                $roleFeatureInsert =<<<SQL
                    INSERT INTO `roleFeature`
                    (
                        `roleID`,
                        `featureID`,
                        `roleFeatureEnabled`
                    ) VALUES(
                        :roleID,
                        :featureID,
                        :roleFeatureEnabled
                    )
SQL;
                $pdo->prepare($roleFeatureInsert)->execute(array(
                    'roleID' => $roleID,
                    'featureID' => $featureID,
                    'roleFeatureEnabled' => $enabled
                ));
            }
        }
    }
}
