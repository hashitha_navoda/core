<?php

use Phinx\Migration\AbstractMigration;

class InsertDataIntoVehicleStatusCheckTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $rows = [
            [
              'vehicleStatusCheckID'    => 1,
              'status'  => 'Checked OK'
            ],
            [
              'vehicleStatusCheckID'    => 2,
              'status'  => 'Topped Up'
            ],
            [
              'vehicleStatusCheckID'    => 3,
              'status'  => 'Replaced'
            ],
            [
              'vehicleStatusCheckID'    => 4,
              'status'  => 'Cleaned'
            ],
            [
              'vehicleStatusCheckID'    => 5,
              'status'  => 'Not Working'
            ],
            [
              'vehicleStatusCheckID'    => 6,
              'status'  => 'Need Attention'
            ],
            [
              'vehicleStatusCheckID'    => 7,
              'status'  => 'Suggest Replacement'
            ],
            [
              'vehicleStatusCheckID'    => 8,
              'status'  => 'Check Wheel Alignment'
            ],
            [
              'vehicleStatusCheckID'    => 9,
              'status'  => 'Not Working Left Side'
            ],
            [
              'vehicleStatusCheckID'    => 10,
              'status'  => 'Not Working Right Side'
            ],
            
        ];

        $stmt = $this->query('SELECT * FROM vehicleStatusCheck');
        $data = $stmt->fetchAll();

        if (sizeof($data) != 0) {
          $count = $this->execute('DELETE FROM vehicleStatusCheck');
        }

        $this->table('vehicleStatusCheck')->insert($rows)->save();
    }
}
