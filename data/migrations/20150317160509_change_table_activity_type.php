<?php

use Phinx\Migration\AbstractMigration;

class ChangeTableActivityType extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
      public function change()
      {
      }
     */

    /**
     * Migrate Up.
     */
    public function up()
    {
        $this->execute("ALTER TABLE `activityType` DROP `activityTypeMaxValue` ;");
        $this->execute("ALTER TABLE `activityType` DROP `activityTypeMinValue` ;");
        $this->execute("ALTER TABLE `activityType` ADD `activityTypeMaxValue` DOUBLE DEFAULT NULL;");
        $this->execute("ALTER TABLE `activityType` ADD `activityTypeMinValue` DOUBLE DEFAULT NULL;");
    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }

}
