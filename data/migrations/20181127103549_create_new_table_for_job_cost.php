<?php

use Phinx\Migration\AbstractMigration;

class CreateNewTableForJobCost extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        // create new table call jobTaskCost
        $jobTaskCostTable = $this->hasTable('jobTaskCost');
        if (!$jobTaskCostTable) {
            $newTable = $this->table('jobTaskCost', ['id' => 'jobTaskCostID']);
            $newTable->addColumn('jobTaskID', 'integer', ['default' => null, 'null' => true])
                ->addColumn('unitCost', 'decimal',['default' => null, 'null' => true, 'precision' => 20, 'scale' => 5])
                ->addColumn('Qty', 'decimal',['default' => null, 'null' => true, 'precision' => 20, 'scale' => 5])
                ->save();
        }

        // create new table call jobProductCost
        $jobProductCostTable = $this->hasTable('jobProductCost');
        if (!$jobProductCostTable) {
            $newProductTable = $this->table('jobProductCost', ['id' => 'jobProductCostID']);
            $newProductTable->addColumn('jobProductID', 'integer', ['default' => null, 'null' => true])
                ->addColumn('productCost', 'decimal',['default' => null, 'null' => true, 'precision' => 20, 'scale' => 5])
                ->addColumn('Qty', 'decimal',['default' => null, 'null' => true, 'precision' => 20, 'scale' => 5])
                ->save();
        }

        // add new column in to the jobProduct table
        $jobProductTable = $this->table('jobProduct');
        $jobProductColumn = $jobProductTable->hasColumn('jobProductInvoicedQty');

        if (!$jobProductColumn) {
            $jobProductTable->addColumn('jobProductInvoicedQty', 'decimal',['default' => 0, 'null' => true, 'precision' => 20, 'scale' => 5])
                ->update();
        }

        // add new column to the jobTask
        $jobTaskTable = $this->table('jobTask');
        $jobTaskColumn = $jobTaskTable->hasColumn('jobTaskInvoicedQty');

        if (!$jobTaskColumn) {
            $jobTaskTable->addColumn('jobTaskInvoicedQty', 'decimal',['default' => 0, 'null' => true, 'precision' => 20, 'scale' => 5])
                ->update();
        }
    }
}
