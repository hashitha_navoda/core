<?php

use Phinx\Migration\AbstractMigration;

class ChangeInvalidInvoiceCancellationItemInDocumentType extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
    public function change()
    {
    }
    */
    
    /**
     * Migrate Up.
     */
    public function up()
    {
        //get invalid rows
        $rows = $this->fetchAll("SELECT *  FROM itemIn WHERE itemInDocumentType LIKE ' cancel'");
        
        foreach ($rows as $row) {
            $this->execute("UPDATE itemIn SET itemInDocumentType = 'Invoice Cancel' where itemInID = {$row['itemInID']}");
        }
    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }
}