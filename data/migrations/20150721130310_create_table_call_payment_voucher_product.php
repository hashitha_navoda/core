<?php

use Phinx\Migration\AbstractMigration;

class CreateTableCallPaymentVoucherProduct extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
      public function change()
      {
      }
     */

    /**
     * Migrate Up.
     */
    public function up()
    {
        $tableExists = $this->hasTable('paymentVoucherProduct');
        if (!$tableExists) {
            $paymentVoucherProduct = $this->table('paymentVoucherProduct', array('id' => 'paymentVoucherProductID'));
            $paymentVoucherProduct->addColumn('paymentVoucherID', 'integer', array('limit' => 20))
                    ->addColumn('locationProductID', 'integer', array('limit' => 20))
                    ->addColumn('paymentVoucherProductQuantity', 'decimal', array('precision' => 20, 'scale' => '5'))
                    ->addColumn('paymentVoucherProductUnitPrice', 'decimal', array('precision' => 12, 'scale' => '2'))
                    ->addColumn('paymentVoucherProductTotalPrice', 'decimal', array('precision' => 12, 'scale' => '2'))
                    ->addColumn('paymentVoucherProductQuantityByBatch', 'decimal', array('precision' => 20, 'scale' => '5', 'null' => 'allow'))
                    ->addColumn('productBatch', 'integer', array('null' => 'allow'))
                    ->addColumn('productSerial', 'integer', array('null' => 'allow'))
                    ->addColumn('batchSerialData', 'string', array('limit' => 500, 'null' => 'allow'))
                    ->addColumn('roockID', 'integer', array('null' => 'allow'))
                    ->save();
        }
    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }

}
