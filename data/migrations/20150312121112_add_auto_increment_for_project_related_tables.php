<?php

use Phinx\Migration\AbstractMigration;

class AddAutoIncrementForProjectRelatedTables extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
      public function change()
      {
      }
     */

    /**
     * Migrate Up.
     */
    public function up()
    {
        $this->execute("SET SESSION old_alter_table=1;ALTER TABLE `project` CHANGE `projectId` `projectId` INT( 11 ) NOT NULL AUTO_INCREMENT ;"
                . "ALTER TABLE `projectDivision` CHANGE `projectDivisionId` `projectDivisionId` INT( 11 ) NOT NULL AUTO_INCREMENT ;"
                . "ALTER TABLE `projectOwners` CHANGE `projectOwnersId` `projectOwnersId` INT( 11 ) NOT NULL AUTO_INCREMENT ;"
                . "ALTER TABLE `projectSupervisors` CHANGE `projectSupervisorsId` `projectSupervisorsId` INT( 11 ) NOT NULL AUTO_INCREMENT ;");
    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }

}
