<?php

use Phinx\Migration\AbstractMigration;

class InsertedAlreadyAddedInvoiceSalesPersonsToNewTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $pdo = $this->getAdapter()->getConnection();
        $rows = $this->fetchAll("SELECT salesPersonID,salesInvoiceID,salesinvoiceTotalAmount,statusID  FROM `salesInvoice`");

        $dataSet = [];
        if (sizeof($rows) > 0) {
            foreach ($rows as $key => $value) {
                if ($value['statusID'] != 10 && !empty($value['salesPersonID'])) {
                    $dataSet[] = [
                        'invoiceID' => $value['salesInvoiceID'],
                        'salesPersonID' => $value['salesPersonID'],
                        'relatedSalesAmount' => $value['salesinvoiceTotalAmount']
                    ];
                }
            }
            $this->table('invoiceSalesPersons')->insert($dataSet)->save();
        }

    }
}
