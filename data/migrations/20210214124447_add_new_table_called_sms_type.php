<?php

use Phinx\Migration\AbstractMigration;

class AddNewTableCalledSmsType extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $smsType = $this->hasTable('smsType');
        if (!$smsType) {
            $newTable = $this->table('smsType', ['id' => 'smsTypeID']);
            $newTable->addColumn('smsTypeName', 'string', ['limit' => 200,'default' => null, 'null' => true])
                ->addColumn('smsTypeMessage', 'text', ['null' => true])
                ->addColumn('isIncludeInvoiceDetails', 'boolean', ['default' => false, 'null' => true])
                ->addColumn('smsSendType', 'integer', ['limit' => 20])
                ->addColumn('isEnable', 'boolean', ['default' => false, 'null' => true])
                ->save();
        }
    }
}
