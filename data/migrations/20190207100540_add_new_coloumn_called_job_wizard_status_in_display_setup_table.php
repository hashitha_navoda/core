<?php

use Phinx\Migration\AbstractMigration;

class AddNewColoumnCalledJobWizardStatusInDisplaySetupTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $this->execute('UPDATE displaySetup SET jobBusinessType = NULL WHERE displaySetupID = 1');

        $table = $this->table('displaySetup');
        $column = $table->hasColumn('jobWizardStatus');

        if (!$column) {
            $table->addColumn('jobWizardStatus', 'boolean', ['after' => 'jobBusinessType', 'default' => 0])
              ->update();
        }
    }
}
