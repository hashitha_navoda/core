<?php

use Phinx\Migration\AbstractMigration;

class AddActionsOfInquiryLogToFeatureTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $pdo = $this->getAdapter()->getConnection();
        $rows = $this->fetchAll("SELECT roleID FROM `role`");

        $dataArray = array(
            array(
                'featureID' => 'NULL',
                'featureName' => 'Create Complain',
                'featureController' => 'JobCard\Controller\InquiryLog',
                'featureAction' => 'create',
                'featureCategory' => 'Inquiry Log',
                'featureType' => '0',
                'moduleID' => '13'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'Create',
                'featureController' => 'JobCard\Controller\InquiryType',
                'featureAction' => 'create',
                'featureCategory' => 'Inquiry Type',
                'featureType' => '0',
                'moduleID' => '13'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'Default',
                'featureController' => 'JobCard\Controller\API\InquiryType',
                'featureAction' => 'add',
                'featureCategory' => 'Inquiry Type',
                'featureType' => '1',
                'moduleID' => '13'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'Default',
                'featureController' => 'JobCard\Controller\API\InquiryType',
                'featureAction' => 'getInquiryTypesBySearchKey',
                'featureCategory' => 'Inquiry Type',
                'featureType' => '1',
                'moduleID' => '13'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'Delete',
                'featureController' => 'JobCard\Controller\API\InquiryType',
                'featureAction' => 'deleteInquiryTypeByInquiryTypeID',
                'featureCategory' => 'Inquiry Type',
                'featureType' => '0',
                'moduleID' => '13'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'Create',
                'featureController' => 'JobCard\Controller\InquiryStatus',
                'featureAction' => 'create',
                'featureCategory' => 'Inquiry Status',
                'featureType' => '0',
                'moduleID' => '13'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'Default',
                'featureController' => 'JobCard\Controller\API\InquiryStatus',
                'featureAction' => 'add',
                'featureCategory' => 'Inquiry Status',
                'featureType' => '1',
                'moduleID' => '13'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'Default',
                'featureController' => 'JobCard\Controller\API\InquiryStatus',
                'featureAction' => 'getInquiryStatusBySearchKey',
                'featureCategory' => 'Inquiry Status',
                'featureType' => '1',
                'moduleID' => '13'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'Default',
                'featureController' => 'JobCard\Controller\API\InquiryStatus',
                'featureAction' => 'deleteInquiryStatusByInquiryStatusID',
                'featureCategory' => 'Inquiry Status',
                'featureType' => '1',
                'moduleID' => '13'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'Create Complain',
                'featureController' => 'JobCard\Controller\InquiryLog',
                'featureAction' => 'create',
                'featureCategory' => 'Inquiry Log',
                'featureType' => '0',
                'moduleID' => '13'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'Default',
                'featureController' => 'JobCard\Controller\API\InquiryLog',
                'featureAction' => 'add',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '13'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'List',
                'featureController' => 'JobCard\Controller\InquiryLog',
                'featureAction' => 'list',
                'featureCategory' => 'Inquiry Log',
                'featureType' => '0',
                'moduleID' => '13'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'View',
                'featureController' => 'JobCard\Controller\InquiryLog',
                'featureAction' => 'view',
                'featureCategory' => 'Inquiry Log',
                'featureType' => '0',
                'moduleID' => '13'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'Edit',
                'featureController' => 'JobCard\Controller\InquiryLog',
                'featureAction' => 'edit',
                'featureCategory' => 'Inquiry Log',
                'featureType' => '0',
                'moduleID' => '13'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'Default',
                'featureController' => 'JobCard\Controller\API\InquiryLog',
                'featureAction' => 'update',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '13'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'Default',
                'featureController' => 'JobCard\Controller\API\InquiryLog',
                'featureAction' => 'delete',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '13'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'Default',
                'featureController' => 'JobCard\Controller\API\InquiryLog',
                'featureAction' => 'getInquiryLogBySearchKey',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '13'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'Default',
                'featureController' => 'JobCard\Controller\API\InquiryLog',
                'featureAction' => 'getJobReferenceByProjectID',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '13'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'Default',
                'featureController' => 'JobCard\Controller\API\InquiryLog',
                'featureAction' => 'getActivityReferenceByJobID',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '13'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'Default',
                'featureController' => 'JobCard\Controller\API\InquiryType',
                'featureAction' => 'searchInquiryTypesForDropdown',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '13'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'Default',
                'featureController' => 'JobCard\Controller\API\InquiryStatus',
                'featureAction' => 'searchInquiryStatusForDropdown',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '13'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'View',
                'featureController' => 'JobCard\Controller\InquirySetup',
                'featureAction' => 'index',
                'featureCategory' => 'Inquiry Setup',
                'featureType' => '0',
                'moduleID' => '13'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'Update',
                'featureController' => 'JobCard\Controller\InquirySetup',
                'featureAction' => 'update',
                'featureCategory' => 'Inquiry Setup',
                'featureType' => '0',
                'moduleID' => '13'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'Default',
                'featureController' => 'JobCard\Controller\API\InquiryType',
                'featureAction' => 'changeStatusID',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '13'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'Default',
                'featureController' => 'JobCard\Controller\API\InquiryStatus',
                'featureAction' => 'changeStatusID',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '13'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'Default',
                'featureController' => 'JobCard\Controller\API\InquiryLog',
                'featureAction' => 'searchDocumentsForDropdown',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '13'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'Default',
                'featureController' => 'JobCard\Controller\API\InquiryLog',
                'featureAction' => 'getAllDocumentDetailsByCustomerID',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '13'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'Create Inquiry',
                'featureController' => 'JobCard\Controller\InquiryLog',
                'featureAction' => 'createInquiry',
                'featureCategory' => 'Inquiry Log',
                'featureType' => '0',
                'moduleID' => '13'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'Create Inquiry',
                'featureController' => 'JobCard\Controller\InquiryLog',
                'featureAction' => 'createInquiry',
                'featureCategory' => 'Inquiry Log',
                'featureType' => '0',
                'moduleID' => '13'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'CRM Inquiry List',
                'featureController' => 'JobCard\Controller\InquiryLog',
                'featureAction' => 'listInquiry',
                'featureCategory' => 'Inquiry Log',
                'featureType' => '0',
                'moduleID' => '13'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'Default',
                'featureController' => 'JobCard\Controller\API\InquiryLog',
                'featureAction' => 'searchInquiryLogForDropdown',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '13'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'Default',
                'featureController' => 'JobCard\Controller\API\InquiryLog',
                'featureAction' => 'getInquiryLogBySearch',
                'featureCategory' => 'getInquiryLogBySearch',
                'featureType' => '1',
                'moduleID' => '13'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'Default',
                'featureController' => 'JobCard\Controller\API\InquiryLog',
                'featureAction' => 'getInquiryLogByDateRange',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '13'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'View From CRM',
                'featureController' => 'JobCard\Controller\InquiryLog',
                'featureAction' => 'inquiryLogView',
                'featureCategory' => 'Inquiry Log',
                'featureType' => '0',
                'moduleID' => '13'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'Edit From CRM',
                'featureController' => 'JobCard\Controller\InquiryLog',
                'featureAction' => 'inquiryLogEdit',
                'featureCategory' => 'Inquiry Log',
                'featureType' => '0',
                'moduleID' => '13'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'Default',
                'featureController' => 'JobCard\Controller\API\InquiryLog',
                'featureAction' => 'getAllDocumentDetailsByInqID',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '13'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'Default',
                'featureController' => 'JobCard\Controller\API\InquiryLog',
                'featureAction' => 'changeInquiryLogStatus',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '13'
            ),
         );

        foreach($dataArray as $feature){

            $featureInsert = <<<SQL
                INSERT INTO `feature`
                (
                    `featureName`,
                    `featureController`,
                    `featureAction`,
                    `featureCategory`,
                    `featureType`,
                    `moduleID`
                ) VALUES (
                    :featureName,
                    :featureController,
                    :featureAction,
                    :featureCategory,
                    :featureType,
                    :featureModuleId
                );

SQL;

            $pdo->prepare($featureInsert)->execute(array(
                'featureName' => $feature['featureName'],
                'featureController' => $feature['featureController'],
                'featureAction' => $feature['featureAction'],
                'featureCategory' => $feature['featureCategory'],
                'featureType' => $feature['featureType'],
                'featureModuleId' => $feature['moduleID']
            ));

            $featureID = $this->fetchRow("select LAST_INSERT_ID()")[0];

            foreach ($rows as $row) {
                $roleID = $row['roleID'];

                if ($roleID == 1) {
                    $enabled = 1;
                } else {

                   //for enable api calls for every role
                    if($feature['featureType'] == 1){
                        $enabled = 1;
                    } else {
                        $enabled = 0;
                    }

                }

                $roleFeatureInsert =<<<SQL
                    INSERT INTO `roleFeature`
                    (
                        `roleID`,
                        `featureID`,
                        `roleFeatureEnabled`
                    ) VALUES(
                        :roleID,
                        :featureID,
                        :roleFeatureEnabled
                    )
SQL;
                $pdo->prepare($roleFeatureInsert)->execute(array(
                    'roleID' => $roleID,
                    'featureID' => $featureID,
                    'roleFeatureEnabled' => $enabled
                ));
            }
        }
    }
}
