<?php

use Phinx\Migration\AbstractMigration;

class CreateTablePaymentVoucherTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
      public function change()
      {
      }
     */

    /**
     * Migrate Up.
     */
    public function up()
    {
        $paymentVoucher = $this->table('paymentVoucher', array('id' => 'paymentVoucherID'));
        $paymentVoucher->addColumn('paymentVoucherCode', 'string', array('limit' => 40))
                ->addColumn('paymentVoucherSupplierID', 'integer', array('limit' => 20, 'null' => 'allow'))
                ->addColumn('paymentVoucherLocationID', 'integer', array('limit' => 20))
                ->addColumn('paymentVoucherIssuedDate', 'date')
                ->addColumn('paymentVoucherDueDate', 'date')
                ->addColumn('paymentVoucherComment', 'string', array('limit' => 50, 'null' => 'allow'))
                ->addColumn('paymentVoucherDeliveryCharge', 'integer', array('limit' => 20, 'null' => 'allow'))
                ->addColumn('paymentVoucherShowTax', 'boolean', array('default' => 0))
                ->addColumn('paymentVoucherStatus', 'integer')
                ->addColumn('paymentVoucherTotal', 'integer')
                ->addColumn('paymentVoucherPaidAmount', 'integer')
                ->addColumn('entityID', 'integer')
                ->addColumn('paymentVoucherEmployeeID', 'integer', array('null' => 'allow'))
                ->addColumn('paymentVoucherItemNotItemFlag', 'integer')
                ->addColumn('paymentVoucherExpenseType', 'integer', array('null' => 'allow'))
                ->save();
    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }

}
