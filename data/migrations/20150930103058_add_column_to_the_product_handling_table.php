<?php

use Phinx\Migration\AbstractMigration;

class AddColumnToTheProductHandlingTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
      public function change()
      {
      }
     */

    /**
     * Migrate Up.
     */
    public function up()
    {
        $table = $this->table('productHandeling');
        $columnExits = $table->hasColumn('productHandelingBomItem');
        if (!$columnExits) {
            $table->addColumn('productHandelingBomItem', 'boolean', array('after' => 'productHandelingGiftCard'))
                    ->update();
        }
    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }

}
