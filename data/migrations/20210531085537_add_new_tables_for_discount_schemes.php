<?php

use Phinx\Migration\AbstractMigration;

class AddNewTablesForDiscountSchemes extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $discountSchemes = $this->hasTable('discountSchemes');
        if (!$discountSchemes) {
            $newTable = $this->table('discountSchemes', ['id' => 'discountSchemesID']);
            $newTable->addColumn('discountSchemeCode',  'string', ['limit' => 200])
                    ->addColumn('discountSchemeName' , 'string', ['limit' => 800])
                    ->addColumn('discountType' , 'string', ['default' => 'percentage', 'limit' => 255, 'null' => true])
                    ->addColumn('locationID' , 'integer', ['limit' => 20])
                    ->addColumn('entityID' , 'integer', ['limit' => 20])
                    ->save();
        }

        $discountSchemeConditions = $this->hasTable('discountSchemeConditions');
        if (!$discountSchemeConditions) {
            $newTable = $this->table('discountSchemeConditions', ['id' => 'discountSchemeConditionsID']);
            $newTable->addColumn('discountSchemesID' , 'integer', ['limit' => 20])
                    ->addColumn('minQty' , 'decimal', ['precision' => 20, 'scale' => 5, 'default' => null, 'null' => true])
                    ->addColumn('maxQty' , 'decimal', ['precision' => 20, 'scale' => 5, 'default' => null, 'null' => true])
                    ->addColumn('discountType' , 'string', ['default' => 'percentage', 'limit' => 255, 'null' => true])
                    ->addColumn('discountPercentage' , 'decimal', ['precision' => 20, 'scale' => 5, 'default' => null, 'null' => true])
                    ->addColumn('discountValue' , 'decimal', ['precision' => 5, 'scale' => 2, 'default' => null, 'null' => true])
                    ->save();
        }
    }
}
