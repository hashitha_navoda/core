<?php

use Phinx\Migration\AbstractMigration;

class AddNewJobModuleTables extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $comTempTable = $this->hasTable('projectMaterial');
        if (!$comTempTable) {
            $table = $this->table('projectMaterial', ['id' => 'projectMaterialID']);
            $table->addColumn('projectID', 'integer')
                ->addColumn('productID', 'integer')
              ->addColumn('locationProductID', 'integer')
              ->save();
        }

        $employeeDesignationTable = $this->hasTable('employeeDesignation');
        if (!$employeeDesignationTable) {
            $employeeDesignationtbl = $this->table('employeeDesignation', ['id' => 'employeeDesignationID']);
            $employeeDesignationtbl->addColumn('employeeID', 'integer')
                ->addColumn('designationID', 'integer')
              ->save();
        }

        $relatedFileTable = $this->hasTable('relatedFile');
        if (!$relatedFileTable) {
            $relatedFiletbl = $this->table('relatedFile', ['id' => 'relatedFileID']);
            $relatedFiletbl->addColumn('relatedFilePath', 'string', ['limit' => 200])
                ->addColumn('documentTypeID', 'integer')
                ->addColumn('documentID', 'integer')
              ->save();
        }

        $jobContractorTable = $this->hasTable('jobContractor');
        if (!$jobContractorTable) {
            $jobContractortbl = $this->table('jobContractor', ['id' => 'jobContractorID']);
            $jobContractortbl->addColumn('jobID', 'integer')
                ->addColumn('contractorID', 'integer')
              ->save();
        }

        $jobEmployeeTable = $this->hasTable('jobEmployee');
        if (!$jobEmployeeTable) {
            $jobEmployeetbl = $this->table('jobEmployee', ['id' => 'jobEmployeeID']);
            $jobEmployeetbl->addColumn('jobID', 'integer')
                ->addColumn('employeeDesignationID', 'integer')
                ->addColumn('jobTaskID', 'integer',['default' => null, 'null' => true])
              ->save();
        }

        $taskTable = $this->hasTable('task');
        if (!$taskTable) {
            $tasktbl = $this->table('task', ['id' => 'taskID']);
            $tasktbl->addColumn('taskCode', 'string',['limit' => 200])
                ->addColumn('taskName', 'string', ['limit' => 200])
                ->addColumn('departmentID', 'integer',['default' => null, 'null' => true])
              ->save();
        }

        $taskCardTable = $this->hasTable('taskCard');
        if (!$taskCardTable) {
            $taskCardtbl = $this->table('taskCard', ['id' => 'taskCardID']);
            $taskCardtbl->addColumn('taskCardName', 'string',['limit' => 200])
                ->addColumn('taskCardCode', 'string', ['limit' => 200, 'default' => null, 'null' => true])
              ->save();
        }

        $taskCardTaskTable = $this->hasTable('taskCardTask');
        if (!$taskCardTaskTable) {
            $taskCardTasktbl = $this->table('taskCardTask', ['id' => 'taskCardTaskID']);
            $taskCardTasktbl->addColumn('taskCardID', 'integer')
                ->addColumn('taskID', 'integer')
                ->addColumn('taskCardTaskRate1', 'decimal',['default' => null, 'null' => true, 'precision' => 20, 'scale' => 5])
                ->addColumn('taskCardTaskRate2', 'decimal',['default' => null, 'null' => true, 'precision' => 20, 'scale' => 5])
                ->addColumn('taskCardTaskRate3', 'decimal',['default' => null, 'null' => true, 'precision' => 20, 'scale' => 5])
                ->addColumn('taskCardTaskIncentiveValue', 'decimal',['default' => null, 'null' => true, 'precision' => 20, 'scale' => 5])
                ->addColumn('uomID', 'integer',['default' => null, 'null' => true])
              ->save();
        }

        $taskCardTaskProductTable = $this->hasTable('taskCardTaskProduct');
        if (!$taskCardTaskProductTable) {
            $taskCardTaskProducttbl = $this->table('taskCardTaskProduct', ['id' => 'taskCardTaskProductID']);
            $taskCardTaskProducttbl->addColumn('taskCardTaskID', 'integer')
                ->addColumn('locationProductID', 'integer')
                ->addColumn('taskCardTaskProductName', 'string',['default' => null, 'null' => true])
                ->addColumn('taskCardTaskProductQty', 'decimal',['default' => null, 'null' => true, 'precision' => 20, 'scale' => 5])
              ->save();
        }

        $rateCardTable = $this->hasTable('rateCard');
        if (!$rateCardTable) {
            $rateCardtbl = $this->table('rateCard', ['id' => 'rateCardID']);
            $rateCardtbl->addColumn('rateCardCode', 'string')
                ->addColumn('rateCardName', 'string')
              ->save();
        }

        $rateCardTaskTable = $this->hasTable('rateCardTask');
        if (!$rateCardTaskTable) {
            $rateCardTasktbl = $this->table('rateCardTask', ['id' => 'rateCardTaskID']);
            $rateCardTasktbl->addColumn('rateCardID', 'integer')
                ->addColumn('taskID', 'integer')
                ->addColumn('rateCardTaskRatePerUnit', 'decimal',['default' => null, 'null' => true, 'precision' => 20, 'scale' => 5])
                ->addColumn('uomID', 'integer',['default' => null, 'null' => true])
              ->save();
        }

        $jobTaskTable = $this->hasTable('jobTask');
        if (!$jobTaskTable) {
            $jobTasktbl = $this->table('jobTask', ['id' => 'jobTaskID']);
            $jobTasktbl->addColumn('jobID', 'integer')
                ->addColumn('taskID', 'integer')
                ->addColumn('uomID', 'integer',['default' => null, 'null' => true])
                ->addColumn('jobTaskUnitCost', 'decimal',['default' => null, 'null' => true, 'precision' => 20, 'scale' => 5])
                ->addColumn('jobTaskEstQty', 'decimal',['default' => null, 'null' => true, 'precision' => 20, 'scale' => 5])
                ->addColumn('jobTaskStatus', 'integer')
                ->addColumn('jobTaskProgress', 'decimal',['default' => null, 'null' => true, 'precision' => 20, 'scale' => 5])
                ->addColumn('jobTaskStartDate', 'date',['default' => null, 'null' => true])
                ->addColumn('jobTaskEndDate', 'date',['default' => null, 'null' => true])
                ->addColumn('stationID', 'integer')
              ->save();
        }

        $jobTaskEmpoyeeTable = $this->hasTable('jobTaskEmployee');
        if (!$jobTaskEmpoyeeTable) {
            $jobTaskEmpoyeetbl = $this->table('jobTaskEmployee', ['id' => 'jobTaskEmployeeID']);
            $jobTaskEmpoyeetbl->addColumn('employeeID', 'integer')
                ->addColumn('taskID', 'integer')
                ->addColumn('jobTaskID', 'integer')
              ->save();
        }

        $jobTaskContractorTable = $this->hasTable('jobTaskContractor');
        if (!$jobTaskContractorTable) {
            $jobTaskContractortbl = $this->table('jobTaskContractor', ['id' => 'jobTaskContractorID']);
            $jobTaskContractortbl->addColumn('jobTaskID', 'integer')
                ->addColumn('contractorID', 'integer')
              ->save();
        }

        $jobTaskProductTable = $this->hasTable('jobTaskProduct');
        if (!$jobTaskProductTable) {
            $jobTaskProducttbl = $this->table('jobTaskProduct', ['id' => 'jobTaskProductID']);
            $jobTaskProducttbl->addColumn('jobTaskID', 'integer')
                ->addColumn('locationProductID', 'integer')
                ->addColumn('jobTaskProductQty', 'decimal',['default' => null, 'null' => true, 'precision' => 20, 'scale' => 5])
              ->save();
        }

        $jobProductTable = $this->hasTable('jobProduct');
        if (!$jobProductTable) {
            $jobProducttbl = $this->table('jobProduct', ['id' => 'jobProductID']);
            $jobProducttbl->addColumn('jobID', 'integer')
                ->addColumn('locationProductID', 'integer')
                ->addColumn('jobProductUnitPrice', 'decimal',['default' => null, 'null' => true, 'precision' => 20, 'scale' => 5])
                ->addColumn('uomID', 'integer',['default' => null, 'null' => true])
                ->addColumn('jobProductAllocatedQty', 'decimal',['default' => null, 'null' => true, 'precision' => 20, 'scale' => 5])
                ->addColumn('jobProductRequestedQty', 'decimal',['default' => null, 'null' => true, 'precision' => 20, 'scale' => 5])
                ->addColumn('jobProductUsedQty', 'decimal',['default' => null, 'null' => true, 'precision' => 20, 'scale' => 5])
                ->addColumn('jobProductIssuedQty', 'decimal',['default' => null, 'null' => true, 'precision' => 20, 'scale' => 5])
                ->addColumn('jobProductReOrderLevel', 'decimal',['default' => null, 'null' => true, 'precision' => 20, 'scale' => 5])
                ->addColumn('jobProductMaterialTypeID', 'integer',['default' => null, 'null' => true])
                ->addColumn('jobProductStatus', 'integer',['default' => null, 'null' => true])
              ->save();
        }

        $jobApprovalTable = $this->hasTable('jobApproval');
        if (!$jobApprovalTable) {
            $jobApprovaltbl = $this->table('jobApproval', ['id' => 'jobApprovalID']);
            $jobApprovaltbl->addColumn('jobID', 'integer')
                ->addColumn('userID', 'integer')
                ->addColumn('jobApprovalDate', 'date',['default' => null, 'null' => true])
              ->save();
        }

        $productJobApprovalTable = $this->hasTable('productJobApproval');
        if (!$productJobApprovalTable) {
            $productJobApprovaltbl = $this->table('productJobApproval', ['id' => 'productJobApprovalID']);
            $productJobApprovaltbl->addColumn('jobApprovalID', 'integer')
                ->addColumn('jobProductID', 'integer')
                ->addColumn('productJobApprovalRequestedQty', 'decimal',['default' => null, 'null' => true, 'precision' => 20, 'scale' => 5])
              ->save();
        }

        $resourceSetupTable = $this->hasTable('resourceSetup');
        if (!$resourceSetupTable) {
            $resourceSetuptbl = $this->table('resourceSetup', ['id' => 'resourceSetupID']);
            $resourceSetuptbl->addColumn('resourceSetupName', 'string',['limit' => 200, 'default' => null, 'null' => true])
                ->addColumn('resourceSetupIsUse', 'boolean', ['default' => false])
              ->save();
        }

        $serviceVehicleTable = $this->hasTable('serviceVehicle');
        if (!$serviceVehicleTable) {
            $serviceVehicletbl = $this->table('serviceVehicle', ['id' => 'serviceVehicleID']);
            $serviceVehicletbl->addColumn('customerID', 'integer')
                ->addColumn('vehicleType', 'string',['default' => null, 'null' => true])
                ->addColumn('vehicleModel', 'string',['default' => null, 'null' => true])
                ->addColumn('serviceVehicleRegNo', 'string',['default' => null, 'null' => true])
                ->addColumn('serviceVehicleEngineNo', 'string',['default' => null, 'null' => true])
                ->addColumn('serviceVehicleOdoMeter', 'decimal',['default' => null, 'null' => true, 'precision' => 10, 'scale' => 3])
              ->save();
        }

        $vehicleTypesTable = $this->hasTable('vehicleType');
        if (!$vehicleTypesTable) {
            $vehicleTypestbl = $this->table('vehicleType', ['id' => 'vehicleTypeID']);
            $vehicleTypestbl->addColumn('vehicleTypeName', 'string')
              ->save();
        }

        $vehicleModelTable = $this->hasTable('vehicleModel');
        if (!$vehicleModelTable) {
            $vehicleModeltbl = $this->table('vehicleModel', ['id' => 'vehicleModelID']);
            $vehicleModeltbl->addColumn('vehicleModelName', 'string', ['default' => null, 'null' => true])
              ->save();
        }

        $jobVehicleTable = $this->hasTable('jobVehicle');
        if (!$jobVehicleTable) {
            $jobVehicletbl = $this->table('jobVehicle', ['id' => 'jobVehicleID']);
            $jobVehicletbl->addColumn('vehicleID', 'integer')
                ->addColumn('jobVehicleCostPerKM', 'decimal',['default' => null, 'null' => true, 'precision' => 20, 'scale' => 5])
                ->addColumn('jobVehicleKMs', 'decimal',['default' => null, 'null' => true, 'precision' => 20, 'scale' => 5])
              ->save();
        }

        $otherJobCostTable = $this->hasTable('otherJobCost');
        if (!$otherJobCostTable) {
            $otherJobCosttbl = $this->table('otherJobCost', ['id' => 'otherJobCostID']);
            $otherJobCosttbl->addColumn('jobID', 'integer')
                ->addColumn('otherJobCostDescription', 'string',['default' => null, 'null' => true, 'limit' => 500])
                ->addColumn('otherJobCostUnitCost', 'decimal',['default' => null, 'null' => true, 'precision' => 20, 'scale' => 5])
                ->addColumn('otherJobCostNoOfUnits', 'decimal',['default' => null, 'null' => true, 'precision' => 20, 'scale' => 5])
              ->save();
        }

        $departmentTable = $this->hasTable('department');
        if (!$departmentTable) {
            $departmenttbl = $this->table('department', ['id' => 'departmentID']);
            $departmenttbl->addColumn('departmentCode', 'string',['default' => null, 'null' => true, 'limit' => 200])
                ->addColumn('departmentName', 'string',['default' => null, 'null' => true, 'limit' => 200])
              ->save();
        }

        $teamTable = $this->hasTable('team');
        if (!$teamTable) {
            $teamtbl = $this->table('team', ['id' => 'teamID']);
            $teamtbl->addColumn('teamCode', 'string',['default' => null, 'null' => true, 'limit' => 200])
                ->addColumn('teamName', 'string',['default' => null, 'null' => true, 'limit' => 200])
              ->save();
        }

        $employeeTeamTable = $this->hasTable('employeeTeam');
        if (!$employeeTeamTable) {
            $employeeTeamtbl = $this->table('employeeTeam', ['id' => 'employeeTeamID']);
            $employeeTeamtbl->addColumn('employeeID', 'integer')
                ->addColumn('teamID', 'integer')
              ->save();
        }

        $stationTable = $this->hasTable('station');
        if (!$stationTable) {
            $stationtbl = $this->table('station', ['id' => 'stationID']);
            $stationtbl->addColumn('stationName', 'string',['default' => null, 'null' => true])
                ->addColumn('stationCode', 'string',['default' => null, 'null' => true])
                ->addColumn('departmentID', 'integer',['default' => null, 'null' => true])
                ->addColumn('stationStatus', 'integer',['default' => null, 'null' => true])
              ->save();
        }


        
    }
}
