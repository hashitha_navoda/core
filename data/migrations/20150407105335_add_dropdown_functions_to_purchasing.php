<?php

use Phinx\Migration\AbstractMigration;

class AddDropdownFunctionsToPurchasing extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
      public function change()
      {
      }
     */

    /**
     * Migrate Up.
     */
    public function up()
    {
        $this->execute("INSERT INTO `feature` (`featureID` ,`featureName` ,`featureController` ,`featureAction` ,`featureCategory` ,`featureType` ,`moduleID`)
                        VALUES (NULL , 'Default', 'Inventory\\\Controller\\\API\\\DebitNote', 'searchDebitNoteForDropdown', 'Application', '1', '2');");
        $id = $this->fetchRow("select LAST_INSERT_ID()")[0];
        $this->execute("INSERT INTO `roleFeature` (`roleFeatureID` ,`roleID` ,`featureID` ,`roleFeatureEnabled`)
                                VALUES (NULL , '1', $id, '1');");

        $this->execute("INSERT INTO `feature` (`featureID` ,`featureName` ,`featureController` ,`featureAction` ,`featureCategory` ,`featureType` ,`moduleID`)
                        VALUES (NULL , 'Default', 'Inventory\\\Controller\\\API\\\DebitNote', 'searchActiveDebitNoteForDropdown', 'Application', '1', '2');");
        $id = $this->fetchRow("select LAST_INSERT_ID()")[0];
        $this->execute("INSERT INTO `roleFeature` (`roleFeatureID` ,`roleID` ,`featureID` ,`roleFeatureEnabled`)
                                VALUES (NULL , '1', $id, '1');");

        $this->execute("INSERT INTO `feature` (`featureID` ,`featureName` ,`featureController` ,`featureAction` ,`featureCategory` ,`featureType` ,`moduleID`)
                        VALUES (NULL , 'Default', 'Inventory\\\Controller\\\API\\\DebitNotePayments', 'searchDebitNotePaymentForDropdown', 'Application', '1', '2');");
        $id = $this->fetchRow("select LAST_INSERT_ID()")[0];
        $this->execute("INSERT INTO `roleFeature` (`roleFeatureID` ,`roleID` ,`featureID` ,`roleFeatureEnabled`)
                                VALUES (NULL , '1', $id, '1');");

        $this->execute("INSERT INTO `feature` (`featureID` ,`featureName` ,`featureController` ,`featureAction` ,`featureCategory` ,`featureType` ,`moduleID`)
                        VALUES (NULL , 'Default', 'Inventory\\\Controller\\\API\\\Grn', 'searchOpenGrnForDropdown', 'Application', '1', '2');");
        $id = $this->fetchRow("select LAST_INSERT_ID()")[0];
        $this->execute("INSERT INTO `roleFeature` (`roleFeatureID` ,`roleID` ,`featureID` ,`roleFeatureEnabled`)
                                VALUES (NULL , '1', $id, '1');");

        $this->execute("INSERT INTO `feature` (`featureID` ,`featureName` ,`featureController` ,`featureAction` ,`featureCategory` ,`featureType` ,`moduleID`)
                        VALUES (NULL , 'Default', 'Inventory\\\Controller\\\API\\\OutGoingPayment', 'searchPVForSupplierPaymentsDropdown', 'Application', '1', '2');");
        $id = $this->fetchRow("select LAST_INSERT_ID()")[0];
        $this->execute("INSERT INTO `roleFeature` (`roleFeatureID` ,`roleID` ,`featureID` ,`roleFeatureEnabled`)
                                VALUES (NULL , '1', $id, '1');");

        $this->execute("INSERT INTO `feature` (`featureID` ,`featureName` ,`featureController` ,`featureAction` ,`featureCategory` ,`featureType` ,`moduleID`)
                        VALUES (NULL , 'Default', 'Inventory\\\Controller\\\API\\\OutGoingPayment', 'searchSupplierPaymentsDropdown', 'Application', '1', '2');");
        $id = $this->fetchRow("select LAST_INSERT_ID()")[0];
        $this->execute("INSERT INTO `roleFeature` (`roleFeatureID` ,`roleID` ,`featureID` ,`roleFeatureEnabled`)
                                VALUES (NULL , '1', $id, '1');");

        $this->execute("INSERT INTO `feature` (`featureID` ,`featureName` ,`featureController` ,`featureAction` ,`featureCategory` ,`featureType` ,`moduleID`)
                        VALUES (NULL , 'Default', 'Inventory\\\Controller\\\API\\\PurchaseInvoiceController', 'searchAllPVForDropdown', 'Application', '1', '2');");
        $id = $this->fetchRow("select LAST_INSERT_ID()")[0];
        $this->execute("INSERT INTO `roleFeature` (`roleFeatureID` ,`roleID` ,`featureID` ,`roleFeatureEnabled`)
                                VALUES (NULL , '1', $id, '1');");
        $this->execute("INSERT INTO `feature` (`featureID` ,`featureName` ,`featureController` ,`featureAction` ,`featureCategory` ,`featureType` ,`moduleID`)
                        VALUES (NULL , 'Default', 'Inventory\\\Controller\\\API\\\PurchaseInvoiceController', 'searchLocationWisePVForDropdown', 'Application', '1', '2');");
        $id = $this->fetchRow("select LAST_INSERT_ID()")[0];
        $this->execute("INSERT INTO `roleFeature` (`roleFeatureID` ,`roleID` ,`featureID` ,`roleFeatureEnabled`)
                                VALUES (NULL , '1', $id, '1');");

        $this->execute("INSERT INTO `feature` (`featureID` ,`featureName` ,`featureController` ,`featureAction` ,`featureCategory` ,`featureType` ,`moduleID`)
                        VALUES (NULL , 'Default', 'Inventory\\\Controller\\\API\\\PurchaseInvoiceController', 'searchPVByPVID', 'Application', '1', '2');");
        $id = $this->fetchRow("select LAST_INSERT_ID()")[0];
        $this->execute("INSERT INTO `roleFeature` (`roleFeatureID` ,`roleID` ,`featureID` ,`roleFeatureEnabled`)
                                VALUES (NULL , '1', $id, '1');");

        $this->execute("INSERT INTO `feature` (`featureID` ,`featureName` ,`featureController` ,`featureAction` ,`featureCategory` ,`featureType` ,`moduleID`)
                        VALUES (NULL , 'Default', 'Inventory\\\Controller\\\API\\\PurchaseOrderController', 'searchPoForDropdown', 'Application', '1', '2');");
        $id = $this->fetchRow("select LAST_INSERT_ID()")[0];
        $this->execute("INSERT INTO `roleFeature` (`roleFeatureID` ,`roleID` ,`featureID` ,`roleFeatureEnabled`)
                                VALUES (NULL , '1', $id, '1');");
        $this->execute("INSERT INTO `feature` (`featureID` ,`featureName` ,`featureController` ,`featureAction` ,`featureCategory` ,`featureType` ,`moduleID`)
                        VALUES (NULL , 'Default', 'Inventory\\\Controller\\\API\\\PurchaseOrderController', 'searchAllPoForDropdown', 'Application', '1', '2');");
        $id = $this->fetchRow("select LAST_INSERT_ID()")[0];
        $this->execute("INSERT INTO `roleFeature` (`roleFeatureID` ,`roleID` ,`featureID` ,`roleFeatureEnabled`)
                                VALUES (NULL , '1', $id, '1');");
        $this->execute("INSERT INTO `feature` (`featureID` ,`featureName` ,`featureController` ,`featureAction` ,`featureCategory` ,`featureType` ,`moduleID`)
                        VALUES (NULL , 'Default', 'Inventory\\\Controller\\\API\\\PurchaseOrderController', 'getPOListForSearch', 'Application', '1', '2');");
        $id = $this->fetchRow("select LAST_INSERT_ID()")[0];
        $this->execute("INSERT INTO `roleFeature` (`roleFeatureID` ,`roleID` ,`featureID` ,`roleFeatureEnabled`)
                                VALUES (NULL , '1', $id, '1');");

        $this->execute("INSERT INTO `feature` (`featureID` ,`featureName` ,`featureController` ,`featureAction` ,`featureCategory` ,`featureType` ,`moduleID`)
                        VALUES (NULL , 'Default', 'Inventory\\\Controller\\\API\\\PurchaseReturnsController', 'searchPurchaseReturnsForDropdownByPRCode', 'Application', '1', '2');");
        $id = $this->fetchRow("select LAST_INSERT_ID()")[0];
        $this->execute("INSERT INTO `roleFeature` (`roleFeatureID` ,`roleID` ,`featureID` ,`roleFeatureEnabled`)
                                VALUES (NULL , '1', $id, '1');");

        $this->execute("INSERT INTO `feature` (`featureID` ,`featureName` ,`featureController` ,`featureAction` ,`featureCategory` ,`featureType` ,`moduleID`)
                        VALUES (NULL , 'Default', 'Inventory\\\Controller\\\API\\\PurchaseReturnsController', 'getPurchaseReturnsForView', 'Application', '1', '2');");
        $id = $this->fetchRow("select LAST_INSERT_ID()")[0];
        $this->execute("INSERT INTO `roleFeature` (`roleFeatureID` ,`roleID` ,`featureID` ,`roleFeatureEnabled`)
                                VALUES (NULL , '1', $id, '1');");

        $this->execute("INSERT INTO `feature` (`featureID` ,`featureName` ,`featureController` ,`featureAction` ,`featureCategory` ,`featureType` ,`moduleID`)
                        VALUES (NULL , 'Default', 'Inventory\\\Controller\\\DebitNote', 'getDebitNoteList', 'Application', '1', '2');");
        $id = $this->fetchRow("select LAST_INSERT_ID()")[0];
        $this->execute("INSERT INTO `roleFeature` (`roleFeatureID` ,`roleID` ,`featureID` ,`roleFeatureEnabled`)
                                VALUES (NULL , '1', $id, '1');");

        $this->execute("INSERT INTO `feature` (`featureID` ,`featureName` ,`featureController` ,`featureAction` ,`featureCategory` ,`featureType` ,`moduleID`)
                        VALUES (NULL , 'Default', 'Inventory\\\Controller\\\DebitNotePayments', 'getDebitNotePaymentList', 'Application', '1', '2');");
        $id = $this->fetchRow("select LAST_INSERT_ID()")[0];
        $this->execute("INSERT INTO `roleFeature` (`roleFeatureID` ,`roleID` ,`featureID` ,`roleFeatureEnabled`)
                                VALUES (NULL , '1', $id, '1');");

        $this->execute("INSERT INTO `feature` (`featureID` ,`featureName` ,`featureController` ,`featureAction` ,`featureCategory` ,`featureType` ,`moduleID`)
                        VALUES (NULL , 'Default', 'Inventory\\\Controller\\\OutGoingPayment', 'getSupplierPaymentsList', 'Application', '1', '2');");
        $id = $this->fetchRow("select LAST_INSERT_ID()")[0];
        $this->execute("INSERT INTO `roleFeature` (`roleFeatureID` ,`roleID` ,`featureID` ,`roleFeatureEnabled`)
                                VALUES (NULL , '1', $id, '1');");
    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }

}
