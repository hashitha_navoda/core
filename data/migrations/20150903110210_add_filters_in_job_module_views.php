<?php

use Phinx\Migration\AbstractMigration;

class AddFiltersInJobModuleViews extends AbstractMigration
{

    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     */
//    public function change()
//    {
//
//    }

    public function up()
    {
        $rows = $this->fetchAll("SELECT roleID FROM `role`");

        $this->execute("INSERT INTO `feature` (`featureID`, `featureName`, `featureController`, `featureAction`, `featureCategory`, `featureType`, `moduleID`) VALUES (NULL, 'Default', 'JobCard\\\Controller\\\API\\\InquiryLog', 'searchInquiryLogForDropdown', 'Application', '1', '8');");
        $id1 = $this->fetchRow("select LAST_INSERT_ID()")[0];
        $this->execute("INSERT INTO `feature` (`featureID`, `featureName`, `featureController`, `featureAction`, `featureCategory`, `featureType`, `moduleID`) VALUES (NULL, 'Default', 'JobCard\\\Controller\\\API\\\InquiryLog', 'getInquiryLogBySearch', 'Application', '1', '8');");
        $id2 = $this->fetchRow("select LAST_INSERT_ID()")[0];
        $this->execute("INSERT INTO `feature` (`featureID`, `featureName`, `featureController`, `featureAction`, `featureCategory`, `featureType`, `moduleID`) VALUES (NULL, 'Default', 'JobCard\\\Controller\\\API\\\InquiryLog', 'getInquiryLogByDateRange', 'Application', '1', '8');");
        $id3 = $this->fetchRow("select LAST_INSERT_ID()")[0];

        $this->execute("INSERT INTO `feature` (`featureID`, `featureName`, `featureController`, `featureAction`, `featureCategory`, `featureType`, `moduleID`) VALUES (NULL, 'Default', 'JobCard\\\Controller\\\API\\\Activity', 'getAllActivitiesForDropdown', 'Application', '1', '8');");
        $id4 = $this->fetchRow("select LAST_INSERT_ID()")[0];
        $this->execute("INSERT INTO `feature` (`featureID`, `featureName`, `featureController`, `featureAction`, `featureCategory`, `featureType`, `moduleID`) VALUES (NULL, 'Default', 'JobCard\\\Controller\\\API\\\Activity', 'getActivityListViewBySearch', 'Application', '1', '8');");
        $id5 = $this->fetchRow("select LAST_INSERT_ID()")[0];
        $this->execute("INSERT INTO `feature` (`featureID`, `featureName`, `featureController`, `featureAction`, `featureCategory`, `featureType`, `moduleID`) VALUES (NULL, 'Default', 'JobCard\\\Controller\\\API\\\Activity', 'getActivityListViewDateRange', 'Application', '1', '8');");
        $id6 = $this->fetchRow("select LAST_INSERT_ID()")[0];

        $this->execute("INSERT INTO `feature` (`featureID`, `featureName`, `featureController`, `featureAction`, `featureCategory`, `featureType`, `moduleID`) VALUES (NULL, 'Default', 'JobCard\\\Controller\\\API\\\Job', 'searchAllJobsForDropdown', 'Application', '1', '8');");
        $id7 = $this->fetchRow("select LAST_INSERT_ID()")[0];
        $this->execute("INSERT INTO `feature` (`featureID`, `featureName`, `featureController`, `featureAction`, `featureCategory`, `featureType`, `moduleID`) VALUES (NULL, 'Default', 'JobCard\\\Controller\\\API\\\Job', 'getJobListViewBySearch', 'Application', '1', '8');");
        $id8 = $this->fetchRow("select LAST_INSERT_ID()")[0];
        $this->execute("INSERT INTO `feature` (`featureID`, `featureName`, `featureController`, `featureAction`, `featureCategory`, `featureType`, `moduleID`) VALUES (NULL, 'Default', 'JobCard\\\Controller\\\API\\\Job', 'getJobListViewByDateRange', 'Application', '1', '8');");
        $id9 = $this->fetchRow("select LAST_INSERT_ID()")[0];

        $this->execute("INSERT INTO `feature` (`featureID`, `featureName`, `featureController`, `featureAction`, `featureCategory`, `featureType`, `moduleID`) VALUES (NULL, 'Default', 'JobCard\\\Controller\\\API\\\Project', 'getProjectListViewByID', 'Application', '1', '8');");
        $id10 = $this->fetchRow("select LAST_INSERT_ID()")[0];
        $this->execute("INSERT INTO `feature` (`featureID`, `featureName`, `featureController`, `featureAction`, `featureCategory`, `featureType`, `moduleID`) VALUES (NULL, 'Default', 'JobCard\\\Controller\\\API\\\Project', 'getProjectListViewByDateRange', 'Application', '1', '8');");
        $id11 = $this->fetchRow("select LAST_INSERT_ID()")[0];


        foreach ($rows as $row) {
            $roleID = $row['roleID'];
            $enabled = 1;
            $this->execute("INSERT INTO `roleFeature` (`roleFeatureID`, `roleID`, `featureID`, `roleFeatureEnabled`) VALUES (NULL, $roleID, $id1,$enabled);");
            $this->execute("INSERT INTO `roleFeature` (`roleFeatureID`, `roleID`, `featureID`, `roleFeatureEnabled`) VALUES (NULL, $roleID, $id2,$enabled);");
            $this->execute("INSERT INTO `roleFeature` (`roleFeatureID`, `roleID`, `featureID`, `roleFeatureEnabled`) VALUES (NULL, $roleID, $id3,$enabled);");
            $this->execute("INSERT INTO `roleFeature` (`roleFeatureID`, `roleID`, `featureID`, `roleFeatureEnabled`) VALUES (NULL, $roleID, $id4,$enabled);");
            $this->execute("INSERT INTO `roleFeature` (`roleFeatureID`, `roleID`, `featureID`, `roleFeatureEnabled`) VALUES (NULL, $roleID, $id5,$enabled);");
            $this->execute("INSERT INTO `roleFeature` (`roleFeatureID`, `roleID`, `featureID`, `roleFeatureEnabled`) VALUES (NULL, $roleID, $id6,$enabled);");
            $this->execute("INSERT INTO `roleFeature` (`roleFeatureID`, `roleID`, `featureID`, `roleFeatureEnabled`) VALUES (NULL, $roleID, $id7,$enabled);");
            $this->execute("INSERT INTO `roleFeature` (`roleFeatureID`, `roleID`, `featureID`, `roleFeatureEnabled`) VALUES (NULL, $roleID, $id8,$enabled);");
            $this->execute("INSERT INTO `roleFeature` (`roleFeatureID`, `roleID`, `featureID`, `roleFeatureEnabled`) VALUES (NULL, $roleID, $id9,$enabled);");
            $this->execute("INSERT INTO `roleFeature` (`roleFeatureID`, `roleID`, `featureID`, `roleFeatureEnabled`) VALUES (NULL, $roleID, $id10,$enabled);");
            $this->execute("INSERT INTO `roleFeature` (`roleFeatureID`, `roleID`, `featureID`, `roleFeatureEnabled`) VALUES (NULL, $roleID, $id11,$enabled);");
        }
    }

}
