<?php

use Phinx\Migration\AbstractMigration;

class AddDispatchNoteTablesAndFeatures extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
    public function change()
    {
    }
    */
    
    /**
     * Migrate Up.
     */
    public function up()
    {
        // Reset 'Expense Payment Voucher' no's to 20
        $id = $this->fetchRow("SELECT documentTypeID
FROM `documentType`
WHERE `documentTypeName` LIKE 'Expense Payment Voucher'
LIMIT 1")[0];
        $this->execute("SET FOREIGN_KEY_CHECKS=0;");
        $this->execute("UPDATE `template` SET `documentTypeID` = 20 WHERE `documentTypeID` = '{$id}'");
        $this->execute("UPDATE `documentType` SET `documentTypeID` = 20 WHERE `documentTypeID` = '{$id}'");
        $this->execute("SET FOREIGN_KEY_CHECKS=1;");


        //add dispatch note referance
        $this->execute("INSERT INTO `reference` (`referenceNameID`, `referenceName`, `referenceTypeID`) VALUES ('29', 'Dispatch Note', '0');");
        //add documentType
        $this->execute("INSERT INTO `documentType` (`documentTypeID`, `documentTypeName`) VALUES ( '21', 'Dispatch Note');");
        //add dispatch note template
        $this->execute("INSERT INTO `template` (`templateID`, `templateName`, `documentTypeID`, `documentSizeID`, `templateContent`, `templateFooterDetails`, `templateFooterShow`, `templateDefault`, `templateSample`, `templateOptions`) VALUES (NULL, 'Template', '21', '1', '''<table class=''table table-bordered'' width=''100%''>
<tbody>
<tr>
<td><img class=''logo'' style=''background: #e0e0e0; width: 75px; height: 75px; display: block; text-align: center;'' src=''[Company Logo]'' alt=''Company Logo'' /><span style=''font-weight: bold;''><abbr contenteditable=''false'' spellcheck=''false''>[Company Name]</abbr> </span><br /><abbr contenteditable=''false'' spellcheck=''false''>[Company Address]</abbr> <br />Tel: <abbr contenteditable=''false'' spellcheck=''false''>[Company Tel. No]</abbr> <br />Email: <abbr contenteditable=''false'' spellcheck=''false''>[Company Email]</abbr> <br />Tax No: <abbr contenteditable=''false'' spellcheck=''false''>[Company Tax Reg. No]</abbr>&nbsp;</td>
<td>
<h2 align=''right''>Dispatch Note</h2>
</td>
</tr>
<tr>
<td>
<div class=''add_block''><hr /></div>
</td>
<td class=''relative_data'' rowspan=''2'' align=''right''>
<table class=''table table-bordered table-condensed'' width=''100%''>
<tbody>
<tr>
<td>Dispatch Note Date:</td>
<td style=''text-align: right;''><abbr contenteditable=''false'' spellcheck=''false''></abbr><abbr contenteditable=''false'' spellcheck=''false''>[Dispatch Note Date]</abbr>&nbsp;</td>
</tr>
<tr>
<td>Dispatch Note Code:</td>
<td style=''text-align: right;''><abbr contenteditable=''false'' spellcheck=''false''>[Dispatch Note Code]</abbr>&nbsp;</td>
</tr>
<tr>
<td>Sales Invoice No:</td>
<td style=''text-align: right;''><abbr contenteditable=''false'' spellcheck=''false''>[Sales Invoice Code]</abbr>&nbsp;</td>
</tr>
<tr>
<td style=''vertical-align: top;''>&nbsp;</td>
<td style=''vertical-align: top; text-align: right;''><br />&nbsp;</td>
</tr>
<tr>
<td>&nbsp;</td>
<td style=''text-align: right;''>&nbsp;</td>
</tr>
</tbody>
</table>
</td>
</tr>
<tr>
<td>
<div class=''add_block''>
<div id=''cust_name''>
<strong><abbr spellcheck=''false'' contenteditable=''false''>[Customer Name]</abbr> <br></strong>
</div><br>
<div id=''delivery_address''>
<p>Delivery Address :</p>
[Dispatch Address]
</div>
</div>
</td>
</tr>
</tbody>
</table>
<noneditabletable>
<table class=''noneditable table table-bordered'' style=''width: 100%; margin-bottom: 100px;'' title='''' contenteditable=''false'' data-original-title=''''>
<tbody>
<tr>
<td colspan=''2''>
<div class=''qout_body''><hr />
<table id=''item_tbl'' class=''table table-bordered table-striped'' style=''height: 92px;'' width=''679''><colgroup><col width=''10px'' /><col width=''90px'' /><col width=''290px'' /><col width=''50px'' /><col width=''100px'' /><col width=''100px'' /> </colgroup>
<thead>
<tr><th colspan=''2''>Item code</th><th colspan=''1''>Item Name</th><th colspan=''1''>Quantity</th><th colspan=''1''></th></tr>
</thead>
<tbody>
<tr>
<td>1</td>
<td align=''right''>Item-xxx</td>
<td align=''right''>Item xxx<br /></td>
<td align=''right''>5</td>
<td align=''right''>Unit</td>
</tr>
</tbody>
</table>
</div>
</td>
</tr>
</tbody>
</table>
</noneditabletable>
<font style=''font-size: 12px;''><footer class=''doc''><table class=''table table-bordered'' width=''100%''><tbody><tr><td><abbr spellcheck=''false'' contenteditable=''false''>[Company Name]</abbr>&nbsp;<br></td><td><br></td><td style=''text-align: right;''><abbr spellcheck=''false'' contenteditable=''false''>[Dispatch Note Date]</abbr>&nbsp;<br></td></tr></tbody></table></footer></font>', '', '1', '1', '0', NULL);");
        
        //create dispatchNote table
        $this->execute("CREATE TABLE IF NOT EXISTS `dispatchNote` (
            `dispatchNoteId` int(11) NOT NULL AUTO_INCREMENT,
            `dispatchNoteCode` varchar(45) DEFAULT NULL,
            `dispatchNoteDate` date DEFAULT NULL,
            `dispatchNoteAddress` varchar(255) DEFAULT NULL,
            `dispatchNoteComment` varchar(255) DEFAULT NULL,
            `invoiceId` int(11) DEFAULT NULL,
            `salesPersonId` int(11) DEFAULT NULL,
            `entityId` int(11) DEFAULT NULL,
            PRIMARY KEY (`dispatchNoteId`)
          ) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;");
        
        //create dispatchNoteProduct table
        $this->execute("CREATE TABLE IF NOT EXISTS `dispatchNoteProduct` (
            `dispatchNoteProductId` int(11) NOT NULL AUTO_INCREMENT,
            `dispatchNoteId` int(11) DEFAULT NULL,
            `salesInvoiceProductId` int(11) DEFAULT NULL,
            `dispatchNoteProductQuantity` decimal(20,5) DEFAULT NULL,
            PRIMARY KEY (`dispatchNoteProductId`)
          ) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;");
        
        //create dispatchNoteSubProduct table
        $this->execute("CREATE TABLE IF NOT EXISTS `dispatchNoteSubProduct` (
            `dispatchNoteSubProductId` int(11) NOT NULL AUTO_INCREMENT,
            `dispatchNoteProductId` int(11) DEFAULT NULL,
            `productBatchId` int(11) DEFAULT NULL,
            `productSerialId` int(11) DEFAULT NULL,
            `dispatchNoteSubProductQuantity` decimal(20,5) DEFAULT NULL,
            `dispatchNoteSubProductsWarranty` int(11) DEFAULT NULL,
            PRIMARY KEY (`dispatchNoteSubProductId`)
          ) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;");
        
        //role list
        $roles = $this->fetchAll("SELECT roleID FROM `role`");

        //feature list
        $featureList = array(
            array('name' =>'Create','controller'=>'Invoice\\\Controller\\\DispatchNote','action'=>'create','category'=>'Dispatch Note','type'=>'0','moduleId'=>'3'),
            array('name' =>'View','controller'=>'Invoice\\\Controller\\\DispatchNote','action'=>'view','category'=>'Dispatch Note','type'=>'0','moduleId'=>'3'),
            array('name' =>'Print View','controller'=>'Invoice\\\Controller\\\DispatchNote','action'=>'documentPreview','category'=>'Dispatch Note','type'=>'0','moduleId'=>'3'),
            array('name' =>'Document','controller'=>'Invoice\\\Controller\\\DispatchNote','action'=>'document','category'=>'Dispatch Note','type'=>'0','moduleId'=>'3'),
            array('name' =>'Default','controller'=>'Invoice\\\Controller\\\API\\\DispatchNote','action'=>'create','category'=>'Dispatch Note','type'=>'1','moduleId'=>'3'),
            array('name' =>'Default','controller'=>'Invoice\\\Controller\\\API\\\DispatchNote','action'=>'getInvoiceDetailsForDispatchNote','category'=>'Dispatch Note','type'=>'1','moduleId'=>'3'),
            array('name' =>'Default','controller'=>'Invoice\\\Controller\\\API\\\DispatchNote','action'=>'searchDispatchCodeForDropdown','category'=>'Dispatch Note','type'=>'1','moduleId'=>'3'),
            array('name' =>'Default','controller'=>'Invoice\\\Controller\\\API\\\DispatchNote','action'=>'search','category'=>'Dispatch Note','type'=>'1','moduleId'=>'3'),
            array('name' =>'Default','controller'=>'Invoice\\\Controller\\\API\\\DispatchNote','action'=>'delete','category'=>'Dispatch Note','type'=>'1','moduleId'=>'3'),
            array('name' =>'Email','controller'=>'Invoice\\\Controller\\\API\\\DispatchNote','action'=>'sendEmail','category'=>'Dispatch Note','type'=>'0','moduleId'=>'3'),
        );

        foreach ($featureList as $feature){
            //insert feature to feature table
            $this->execute("INSERT INTO `feature` (`featureID`, `featureName`, `featureController`, `featureAction`, `featureCategory`, `featureType`, `moduleID`) VALUES (NULL,'".$feature['name']."', '".$feature['controller']."', '".$feature['action']."', '".$feature['category']."', '".$feature['type']."', '".$feature['moduleId']."');");
            $id = $this->fetchRow("select LAST_INSERT_ID()")[0];

            foreach ($roles as $role) {
                $roleID = $role['roleID'];
                if ($roleID == 1) {
                    $enabled = 1;
                } else {
                    //for enable api calls for every role
                    if($feature['type'] === '1'){
                        $enabled = 1;
                    } else {
                        $enabled = 0;
                    }
                }
                $this->execute("INSERT INTO `roleFeature` (`roleFeatureID`, `roleID`, `featureID`, `roleFeatureEnabled`) VALUES (NULL, $roleID, $id,$enabled);");
            }
        }
        
    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }
}