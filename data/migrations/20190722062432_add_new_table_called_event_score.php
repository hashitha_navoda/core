<?php

use Phinx\Migration\AbstractMigration;

class AddNewTableCalledEventScore extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->hasTable('eventScore');

        if (!$table) {
            $table = $this->table('eventScore', ['id' => 'eventScoreId']);
            $table->addColumn('score', 'string', ['limit' => 200]);
            $table->addColumn('remarks', 'text', ['default' => null, 'null' => true]);
            $table->addColumn('eventID', 'integer', ['limit' => 11]);
            $table->addColumn('eventParticipantID', 'integer', ['limit' => 11,'default' => null, 'null' => true]);
            $table->addColumn('deleted', 'boolean', ['default' => 0]);
            $table->save();
        }
    }
}
