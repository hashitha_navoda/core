<?php

use Phinx\Migration\AbstractMigration;

class EditQuantityColumsOfEachTableAsDecimal extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
      public function change()
      {
      }
     */

    /**
     * Migrate Up.
     */
    public function up()
    {
        $this->execute(" ALTER TABLE `activityBatch` CHANGE `activityBatchQuantity` `activityBatchQuantity` DECIMAL( 12, 4 ) NULL DEFAULT NULL ;");
        $this->execute(" ALTER TABLE `activityFixedAsset` CHANGE `activityFixedAssetQuantity` `activityFixedAssetQuantity` DECIMAL( 12, 4 ) NULL DEFAULT NULL ;");
        $this->execute(" ALTER TABLE `activityFixedAssetSubProduct` CHANGE `activityFixedAssetSubProductQuantity` `activityFixedAssetSubProductQuantity` DECIMAL( 12, 4 ) NULL DEFAULT NULL ;");
        $this->execute(" ALTER TABLE `activityRawMaterial` CHANGE `activityRawMaterialQuantity` `activityRawMaterialQuantity` DECIMAL( 12, 4 ) NULL DEFAULT NULL ;");
        $this->execute(" ALTER TABLE `activityRawMaterialSubProduct` CHANGE `activityRawMaterialSubProductQuantity` `activityRawMaterialSubProductQuantity` DECIMAL( 12, 4 ) NULL DEFAULT NULL ;");
        $this->execute(" ALTER TABLE `assemble` CHANGE `assembleQuantity` `assembleQuantity` DECIMAL( 12, 4 ) NULL DEFAULT NULL ;");
        $this->execute(" ALTER TABLE `assembleProduct` CHANGE `assembleProductQuantity` `assembleProductQuantity` DECIMAL( 12, 4 ) NULL DEFAULT NULL ;");
        $this->execute(" ALTER TABLE `creditNoteProduct` CHANGE `creditNoteProductQuantity` `creditNoteProductQuantity` DECIMAL( 12, 4 ) NULL DEFAULT NULL ;");
        $this->execute(" ALTER TABLE `creditNoteSubProduct` CHANGE `creditNoteSubProductQuantity` `creditNoteSubProductQuantity` DECIMAL( 12, 4 ) NOT NULL ;");
        $this->execute(" ALTER TABLE `debitNoteProduct` CHANGE `debitNoteProductQuantity` `debitNoteProductQuantity` DECIMAL( 12, 4 ) NULL DEFAULT '0';");
        $this->execute(" ALTER TABLE `debitNoteSubProduct` CHANGE `debitNoteSubProductQuantity` `debitNoteSubProductQuantity` DECIMAL( 12, 4 ) NOT NULL ;");
        $this->execute(" ALTER TABLE `deliveryNoteProduct` CHANGE `deliveryNoteProductQuantity` `deliveryNoteProductQuantity` DECIMAL( 12, 4 ) NULL DEFAULT NULL ;");
        $this->execute(" ALTER TABLE `deliveryNoteSubProducts` CHANGE `deliveryProductSubQuantity` `deliveryProductSubQuantity` DECIMAL( 12, 4 ) NOT NULL ;");
        $this->execute(" ALTER TABLE `disassemble` CHANGE `disassembleQuantity` `disassembleQuantity` DECIMAL( 12, 4 ) NULL DEFAULT NULL ;");
        $this->execute(" ALTER TABLE `disassembleProduct` CHANGE `disassembleProductQuantity` `disassembleProductQuantity` DECIMAL( 12, 4 ) NULL DEFAULT NULL ;");
        $this->execute(" ALTER TABLE `goodsIssueProduct` CHANGE `goodsIssueProductQuantity` `goodsIssueProductQuantity` DECIMAL( 12, 4 ) NULL DEFAULT NULL ;");
        $this->execute(" ALTER TABLE `goodsIssueProductionProduct` CHANGE `goodsIssueProductionProductQuantity` `goodsIssueProductionProductQuantity` DECIMAL( 12, 4 ) NULL DEFAULT NULL ;");
        $this->execute(" ALTER TABLE `goodsIssueProductionProductReturn` CHANGE `goodsIssueProductionProductReturnQuantity` `goodsIssueProductionProductReturnQuantity` DECIMAL( 12, 4 ) NULL DEFAULT NULL ;");
        $this->execute(" ALTER TABLE `goodsReceiptProduct` CHANGE `goodsReceiptProductQuantity` `goodsReceiptProductQuantity` DECIMAL( 12, 4 ) NULL DEFAULT NULL ;");
        $this->execute(" ALTER TABLE `goodsReceiptProductionProduct` CHANGE `goodsReceiptProductionProductQuantity` `goodsReceiptProductionProductQuantity` DECIMAL( 12, 4 ) NULL DEFAULT NULL ;");
        $this->execute(" ALTER TABLE `goodsReceiptProductionProductReturn` CHANGE `goodsReceiptProductionProductReturnQuantity` `goodsReceiptProductionProductReturnQuantity` DECIMAL( 12, 4 ) NULL DEFAULT NULL ;");
        $this->execute(" ALTER TABLE `grnProduct` CHANGE `grnProductTotalQty` `grnProductTotalQty` DECIMAL( 12, 4 ) NULL DEFAULT '0';");
        $this->execute(" ALTER TABLE `grnProduct` CHANGE `grnProductQuantity` `grnProductQuantity` DECIMAL( 12, 4 ) NULL DEFAULT NULL ;");
        $this->execute(" ALTER TABLE `itemIn` CHANGE `itemInQty` `itemInQty` DECIMAL( 12, 4 ) NOT NULL ;");
        $this->execute(" ALTER TABLE `itemIn` CHANGE `itemInSoldQty` `itemInSoldQty` DECIMAL( 12, 4 ) NOT NULL DEFAULT '0';");
        $this->execute(" ALTER TABLE `itemOut` CHANGE `itemOutQty` `itemOutQty` DECIMAL( 12, 4 ) NOT NULL ;");
        $this->execute(" ALTER TABLE `itemOut` CHANGE `itemOutReturnQty` `itemOutReturnQty` DECIMAL( 12, 4 ) NOT NULL DEFAULT '0';");
        $this->execute(" ALTER TABLE `locationProduct` CHANGE `locationProductQuantity` `locationProductQuantity` DECIMAL( 12, 4 ) NULL DEFAULT NULL ;");
        $this->execute(" ALTER TABLE `locationProduct` CHANGE `locationProductMinInventoryLevel` `locationProductMinInventoryLevel` DECIMAL( 12, 4 ) NOT NULL ;");
        $this->execute(" ALTER TABLE `locationProduct` CHANGE `locationProductReOrderLevel` `locationProductReOrderLevel` DECIMAL( 12, 4 ) NOT NULL ;");
        $this->execute(" ALTER TABLE `product` CHANGE `productDefaultOpeningQuantity` `productDefaultOpeningQuantity` DECIMAL( 12, 4 ) NULL DEFAULT '0';");
        $this->execute(" ALTER TABLE `product` CHANGE `productDefaultMinimumInventoryLevel` `productDefaultMinimumInventoryLevel` DECIMAL( 12, 4 ) NULL DEFAULT '0';");
        $this->execute(" ALTER TABLE `product` CHANGE `productDefaultReorderLevel` `productDefaultReorderLevel` DECIMAL( 12, 4 ) NULL DEFAULT '0';");
        $this->execute(" ALTER TABLE `productBatch` CHANGE `productBatchQuantity` `productBatchQuantity` DECIMAL( 12, 4 ) NULL DEFAULT NULL ;");
//        $this->execute(" ALTER TABLE `promotionProduct` CHANGE `promotionProductMinQty` `promotionProductMinQty` DECIMAL( 12, 4 ) NOT NULL ;");
//        $this->execute(" ALTER TABLE `promotionProduct` CHANGE `promotionProductMaxQty` `promotionProductMaxQty` DECIMAL( 12, 4 ) NOT NULL ;");
        $this->execute(" ALTER TABLE `purchaseInvoiceProduct` CHANGE `purchaseInvoiceProductQuantity` `purchaseInvoiceProductQuantity` DECIMAL( 12, 4 ) NULL DEFAULT NULL COMMENT '	';");
        $this->execute(" ALTER TABLE `purchaseInvoiceProduct` CHANGE `purchaseInvoiceProductTotalQty` `purchaseInvoiceProductTotalQty` DECIMAL( 12, 4 ) NULL DEFAULT '0';");
        $this->execute(" ALTER TABLE `purchaseOrderProduct` CHANGE `purchaseOrderProductQuantity` `purchaseOrderProductQuantity` DECIMAL( 12, 4 ) NULL DEFAULT NULL ;");
        $this->execute(" ALTER TABLE `purchaseReturnProduct` CHANGE `purchaseReturnProductPurchasedQty` `purchaseReturnProductPurchasedQty` DECIMAL( 12, 4 ) NULL DEFAULT NULL ;");
        $this->execute(" ALTER TABLE `purchaseReturnProduct` CHANGE `purchaseReturnProductReturnedQty` `purchaseReturnProductReturnedQty` DECIMAL( 12, 4 ) NULL DEFAULT NULL ;");
        $this->execute(" ALTER TABLE `purchaseReturnProduct` CHANGE `purchaseReturnSubProductReturnedQty` `purchaseReturnSubProductReturnedQty` DECIMAL( 12, 4 ) NULL DEFAULT NULL ;");
        $this->execute(" ALTER TABLE `quotationProduct` CHANGE `quotationProductQuantity` `quotationProductQuantity` DECIMAL( 12, 4 ) NULL DEFAULT NULL ;");
        $this->execute(" ALTER TABLE `salesInvoiceProduct` CHANGE `salesInvoiceProductQuantity` `salesInvoiceProductQuantity` DECIMAL( 12, 4 ) NULL DEFAULT NULL ;");
        $this->execute(" ALTER TABLE `salesInvoiceSubProduct` CHANGE `salesInvoiceSubProductQuantity` `salesInvoiceSubProductQuantity` DECIMAL( 12, 4 ) NOT NULL ;");
        $this->execute(" ALTER TABLE `salesOrdersProduct` CHANGE `quantity` `quantity` DECIMAL( 12, 4 ) NULL DEFAULT NULL ;");
        $this->execute(" ALTER TABLE `salesReturnProduct` CHANGE `salesReturnProductQuantity` `salesReturnProductQuantity` DECIMAL( 12, 4 ) NULL DEFAULT NULL ;");
        $this->execute(" ALTER TABLE `salesReturnSubProduct` CHANGE `salesReturnSubProductQuantity` `salesReturnSubProductQuantity` DECIMAL( 12, 4 ) NOT NULL ;");
        $this->execute(" ALTER TABLE `stockRevaluationProduct` CHANGE `stockRevaluationProductQuantity` `stockRevaluationProductQuantity` DECIMAL( 12, 4 ) NULL DEFAULT NULL ;");
        $this->execute(" ALTER TABLE `temporaryProduct` CHANGE `temporaryProductQuantity` `temporaryProductQuantity` DECIMAL( 12, 4 ) NOT NULL DEFAULT '0.00';");
        $this->execute(" ALTER TABLE `transferProduct` CHANGE `transferProductQuantity` `transferProductQuantity` DECIMAL( 12, 4 ) NULL DEFAULT NULL ;");
    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }

}
