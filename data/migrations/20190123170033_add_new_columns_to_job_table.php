<?php

use Phinx\Migration\AbstractMigration;

class AddNewColumnsToJobTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->table('job');
        $column1 = $table->hasColumn('customerNotes');
        $column2 = $table->hasColumn('supervisorId');

        if (!$column1) {
            $table->addColumn('customerNotes', 'text', ['after' => 'jobEstimatedEndDate', 'default' => null, 'null' => true])
              ->update();
        }

        if (!$column2) {
            $table->addColumn('supervisorId', 'integer', ['null' => true])
                ->update();
        }

    }
}
