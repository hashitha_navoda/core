<?php

use Phinx\Migration\AbstractMigration;

class AddNewColumnsCalledTelephonenumber2AndTelephonenumber3AndFaxnumberInCompanyTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    
     /**
     * Migrate Up.
     */
    public function up()
    {
        $this->execute("ALTER TABLE `company` ADD `telephoneNumber2` VARCHAR(20) NULL DEFAULT NULL;");
        $this->execute("ALTER TABLE `company` ADD `telephoneNumber3` VARCHAR(20) NULL DEFAULT NULL;");
        $this->execute("ALTER TABLE `company` ADD `faxNumber` VARCHAR(20) NULL DEFAULT NULL;");
    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }
}
