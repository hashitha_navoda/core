<?php

use Phinx\Migration\AbstractMigration;

class AddNewTableCalledGiftCard extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
      public function change()
      {
      }
     */

    /**
     * Migrate Up.
     */
    public function up()
    {
        $this->execute("CREATE TABLE IF NOT EXISTS `giftCard` (
  `giftCardId` int(11) NOT NULL AUTO_INCREMENT,
  `giftCardCode` varchar(200) NOT NULL,
  `giftCardDuration` int(11) DEFAULT '0',
  `giftCardIssueDate` date DEFAULT NULL,
  `giftCardExpireDate` date DEFAULT NULL,
  `giftCardValue` decimal(20,5) NOT NULL,
  `giftCardStatus` int(11) NOT NULL,
  PRIMARY KEY (`giftCardId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;");
    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }

}
