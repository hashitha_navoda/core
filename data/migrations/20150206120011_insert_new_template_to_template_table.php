<?php

use Phinx\Migration\AbstractMigration;

class InsertNewTemplateToTemplateTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
      public function change()
      {
      }
     */

    /**
     * Migrate Up.
     */
    public function up()
    {
        $sql = <<<EOF
                INSERT IGNORE INTO `template` (`templateID`, `templateName`, `documentTypeID`, `documentSizeID`, `templateContent`, `templateFooterDetails`, `templateFooterShow`, `templateDefault`, `templateSample`) VALUES
(63, 'Template', 18, 1, '<table class="table table-bordered" width="100%">\n<tbody>\n<tr>\n<td><img class="logo" style="background: #e0e0e0; width: 75px; height: 75px; display: block; text-align: center;" src="[Company Logo]" alt="Company Logo" /><span style="font-weight: bold;"><abbr contenteditable="false" spellcheck="false">[Company Name]</abbr> </span><br /><abbr contenteditable="false" spellcheck="false">[Company Address]</abbr> <br />Tel: <abbr contenteditable="false" spellcheck="false">[Company Tel. No]</abbr> <br />Email: <abbr contenteditable="false" spellcheck="false">[Company Email]</abbr> <br />Tax No: <abbr contenteditable="false" spellcheck="false">[Company Tax Reg. No]</abbr>&nbsp;</td>\n<td>\n<h2 align="right">Debit Note Payment</h2>\n</td>\n</tr>\n<tr>\n<td>\n<div class="add_block"><hr /></div>\n</td>\n<td class="relative_data" rowspan="2" align="right">\n<table class="table table-bordered table-condensed" width="100%">\n<tbody>\n<tr>\n<td>Payment Date:</td>\n<td style="text-align: right;"><abbr contenteditable="false" spellcheck="false">[Debit Note Payment Date]</abbr>&nbsp;</td>\n</tr>\n<tr>\n<td style="vertical-align: top;">Payment Term:</td>\n<td style="vertical-align: top; text-align: right;"><abbr contenteditable="false" spellcheck="false">[Debit Note Payment Term]</abbr>&nbsp;</td>\n</tr>\n<tr>\n<td>Payment No:</td>\n<td style="text-align: right;"><abbr contenteditable="false" spellcheck="false">[Debit Note Payment Code]</abbr>&nbsp;</td>\n</tr>\n</tbody>\n</table>\n</td>\n</tr>\n<tr>\n<td>\n<div class="add_block">\n<div id="cust_name"><abbr contenteditable="false" spellcheck="false">[Supplier Name]</abbr>&nbsp;</div>\n</div>\n</td>\n</tr>\n</tbody>\n</table>\n<noneditabletable>\n<table class="noneditable table table-bordered" style="width: 100%; margin-bottom: 100px;" title="" contenteditable="false" data-original-title="">\n<tbody>\n<tr>\n<td colspan="2"><hr />\n<div class="qout_body">\n<table id="item_tbl" class="table bigtable table-bordered table-striped" width="100%"><colgroup><col width="5px" /><col width="20px" /><col width="20px" /><col width="10px" /><col width="10px" /><col width="20px" /></colgroup>\n<thead>\n<tr><th>&nbsp;</th><th>Invoice no.</th><th>Payment method</th><th>Total amount</th><th>Left to pay</th><th>Paid amount</th></tr>\n</thead>\n<tbody>\n<tr>\n<td>1</td>\n<td align="">INV-K0010</td>\n<td align="">Cash</td>\n<td align="">1,230.00</td>\n<td align="">0.00</td>\n<td align="">1,230.00</td>\n</tr>\n</tbody>\n</table>\n<br /><br /><br />\n<div class="summary">\n<table id="" class="table" width="100%"><colgroup><col width="90px" /><col width="290px" /><col width="75px" /><col width="100px" /><col width="50px" /><col width="100px" /></colgroup>\n<tbody>\n<tr>\n<td colspan="2" rowspan="4">\n<div id="comment"><strong>Memo&nbsp;:</strong>\n<p><em>Sample memo text for payment</em></p>\n</div>\n</td>\n<td colspan="3">Sub Total</td>\n<td class="text-right">1,230.00</td>\n</tr>\n<tr>\n<td colspan="3">\n<div>Credit Payments</div>\n</td>\n<td class="text-right">0.00</td>\n</tr>\n<tr>\n<td colspan="3">\n<div>Discount</div>\n</td>\n<td class="text-right">( 0.00 )</td>\n</tr>\n<tr>\n<td colspan="3">\n<h3>Total</h3>\n</td>\n<td class="text-right">\n<h3>1,230.00</h3>\n</td>\n</tr>\n</tbody>\n</table>\n</div>\n</div>\n</td>\n</tr>\n</tbody>\n</table>\n</noneditabletable><footer class="doc">\n<table class="table table-bordered" width="100%">\n<tbody>\n<tr>\n<td><abbr contenteditable="false" spellcheck="false">[Company Name]</abbr>&nbsp;</td>\n<td>&nbsp;</td>\n<td style="text-align: right;"><abbr contenteditable="false" spellcheck="false">[Today Date]</abbr>&nbsp;</td>\n</tr>\n</tbody>\n</table>\n</footer>', '', 1, 1, 0);
EOF;
        $this->execute($sql);
    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }

}
