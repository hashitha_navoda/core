<?php

use Phinx\Migration\AbstractMigration;

class AddNewTableCalledJournalEntryType extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
                 $Sql = <<<SQL
        CREATE TABLE IF NOT EXISTS `journalEntryType` (
`journalEntryTypeID` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `journalEntryTypeName` varchar(100) NOT NULL,
  `journalEntryTypeApprovedStatus` tinyint(1) NOT NULL,
  `entityID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `journalEntryTypeApprovers` (
`journalEntryTypeApproversID` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `journalEntryTypeID` int(11) NOT NULL,
  `employeeID` int(11) NOT NULL,
  `journalEntryTypeApproversMinAmount` double(15,2) NULL,
  `journalEntryTypeApproversMaxAmount` double(15,2) NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

SQL;

    $this->execute($Sql);
    }
}
