<?php

use Phinx\Migration\AbstractMigration;

class AddNewColoumnToPurchesInvoiceTotalDiscount extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->table('purchaseInvoice');
        $column1 = $table->hasColumn('purchaseInvoiceWiseTotalDiscount');
        $column2 = $table->hasColumn('purchaseInvoiceWiseTotalDiscountType');
        $column3 = $table->hasColumn('purchaseInvoiceWiseTotalDiscountRate');
        
        if (!$column1) {
            $table->addColumn('purchaseInvoiceWiseTotalDiscount', 'decimal', ['precision' => 20, 'scale' => 5, 'default' => null, 'null' => true])
                ->update();
        }
        if (!$column2) {
            $table->addColumn('purchaseInvoiceWiseTotalDiscountType', 'string', ['limit' => 30, 'default' => null, 'null' => true])
                ->update();
        }
        if (!$column3) {
            $table->addColumn('purchaseInvoiceWiseTotalDiscountRate', 'decimal', ['precision' => 20, 'scale' => 5, 'default' => null, 'null' => true])
                ->update();
        }
    }
}
