<?php

use Phinx\Migration\AbstractMigration;

class AddNewColumnsToProjectTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {

        //projectAddress
        $table = $this->table('project');
        $column = $table->hasColumn('projectAddress');
        if (!$column) {
            $table->addColumn('projectAddress', 'text', ['after' => 'projectName', 'default' => null, 'null' => true])
              ->update();
        }

        //projectNoOfJobs
        $column = $table->hasColumn('noOfJobs');
        if (!$column) {
            $table->addColumn('noOfJobs', 'integer', ['after' => 'projectAddress', 'default' => null, 'null' => true])
              ->update();
        }

        //estimatedEndDate
        $column = $table->hasColumn('estimatedEndDate');
        if (!$column) {
            $table->addColumn('estimatedEndDate', 'datetime', ['after' => 'noOfJobs', 'default' => null, 'null' => true])
              ->update();
        }


        //projectValue
        $column = $table->hasColumn('projectValue');
        if (!$column) {
            $table->addColumn('projectValue', 'decimal', ['after' => 'projectDescription','default' => null, 'null' => true, 'precision' => 20, 'scale' => 5])
              ->update();
        }

        //add new column to project Managers
        $table1 = $this->table('projectManagers');
        $column1 = $table1->hasColumn('isDeleted');

        if (!$column1) {
            $table1->addColumn('isDeleted', 'boolean', ['after' => 'employeeID', 'default' => false])
              ->update();
        }

        //add new column to project supervisors
        $table2 = $this->table('projectSupervisors');
        $column2 = $table2->hasColumn('isDeleted');

        if (!$column2) {
            $table2->addColumn('isDeleted', 'boolean', ['after' => 'employeeID', 'default' => false])
              ->update();
        }

        //add new column to project materials
        $table3 = $this->table('projectMaterial');
        $column3 = $table3->hasColumn('isDeleted');

        if (!$column3) {
            $table3->addColumn('isDeleted', 'boolean', ['after' => 'locationProductID', 'default' => false])
              ->update();
        }

    }

}
