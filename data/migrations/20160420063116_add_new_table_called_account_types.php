<?php

use Phinx\Migration\AbstractMigration;

class AddNewTableCalledAccountTypes extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $Sql = <<<SQL
                CREATE TABLE IF NOT EXISTS `financeAccountTypes` (
`financeAccountTypesID` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `financeAccountTypesName` varchar(100) NOT NULL,
  `entityID` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `financeAccountTypes` (`financeAccountTypesID`, `financeAccountTypesName`, `entityID`) VALUES
(1, 'Asset', NULL),
(2, 'Liability', NULL),
(3, 'Expense', NULL),
(4, 'Revenue', NULL),
(5, 'Equity', NULL),
(6, 'Cost of Sales', NULL);

ALTER TABLE `financeAccountTypes`
MODIFY `financeAccountTypesID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;

SQL;

        $this->execute($Sql);
    }
}
