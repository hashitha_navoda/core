<?php

use Phinx\Migration\AbstractMigration;

class AddAmountForPamentMethodsNumbersTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
      public function change()
      {
      }
     */

    /**
     * Migrate Up.
     */
    public function up()
    {
        $rows = $this->fetchAll("SELECT * FROM `incomingInvoicePayment`");

        foreach ($rows as $row) {
            $payments[$row['incomingPaymentID']][$row['paymentMethodID']] = isset($payments[$row['incomingPaymentID']][$row['paymentMethodID']]) ? $payments[$row['incomingPaymentID']][$row['paymentMethodID']] + $row['incomingInvoiceCashAmount'] : $row['incomingInvoiceCashAmount'];
        }


        foreach ($payments as $pid => $val) {
            foreach ($val as $method => $amount) {
                if ($method == 1 || !$method) {
                    $amount = ($amount)? : 0;
                    $sql = "INSERT INTO paymentMethodsNumbers VALUES (null, $pid, 1, null, null, null, null, $amount)";
                } else {

                    $sql = "UPDATE paymentMethodsNumbers SET paymentMethodAmount = $amount where paymentID = $pid AND paymentMethodID = $method";
                }
                $this->execute($sql);
            }
        }
    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }

}
