<?php

use Phinx\Migration\AbstractMigration;

class UpdateReportFeatures extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
      public function change()
      {
      }
     */

    /**
     * Migrate Up.
     */
    public function up()
    {
        $this->execute("UPDATE feature SET featureName =  'Generate CSV' WHERE featureName = 'generateGlobalItemDetailsSheet' AND moduleID = 5;");
        $this->execute("UPDATE feature SET featureName =  'View Report' WHERE featureAction =  'viewPaymentVoucherDetails' AND featureCategory = 'Payment Voucher Report' AND moduleID = 5;");
        $this->execute("UPDATE feature SET featureName =  'Generate PDF' WHERE featureAction =  'generatePaymentVoucherDetailsPdf' AND featureCategory = 'Payment Voucher Report' AND moduleID = 5;");
        $this->execute("UPDATE feature SET featureName =  'Generate CSV' WHERE featureAction =  'generatePaymentVoucherDetailsSheet' AND featureCategory = 'Payment Voucher Report' AND moduleID = 5;");
    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }

}
