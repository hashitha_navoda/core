<?php

use Phinx\Migration\AbstractMigration;

class AddNewColumCalledCreditNoteSettleAmountToCreditNoteTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
      public function change()
      {
      }
     */

    /**
     * Migrate Up.
     */
    public function up()
    {
        $this->execute("ALTER TABLE `creditNote` ADD `creditNoteSettledAmount` DECIMAL( 12, 2 ) NULL DEFAULT '0.00' AFTER `creditNotePaymentAmount`;");
    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }

}
