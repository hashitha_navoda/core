<?php

use Phinx\Migration\AbstractMigration;

class AddColumnToTheProductTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->table('product');
        $hasPurchaseDiscTypeColumn = $table->hasColumn('productPurchaseDiscountPercentage');
        $hasPurchaseDiscValueColumn = $table->hasColumn('productPurchaseDiscountValue');
        
        if(!$hasPurchaseDiscTypeColumn && !$hasPurchaseDiscValueColumn){
            $table->addColumn('productPurchaseDiscountEligible', 'boolean', array('after' => 'productDiscountValue'))
                ->addColumn('productPurchaseDiscountPercentage', 'decimal', array('after' => 'productPurchaseDiscountEligible', 'precision' => 12,'scale' => 2, 'null' => 'allow' ))
                ->addColumn('productPurchaseDiscountValue', 'decimal', array('after' => 'productPurchaseDiscountPercentage', 'precision' => 25,'scale' => 10, 'null' => 'allow' ))
            ->update();
            
        }
    }
}
