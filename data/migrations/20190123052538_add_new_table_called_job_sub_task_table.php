<?php

use Phinx\Migration\AbstractMigration;

class AddNewTableCalledJobSubTaskTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $jobSubTask = $this->hasTable('jobSubTask');

        if (!$jobSubTask) {
            $table = $this->table('jobSubTask', ['id' => 'jobSubTaskID']);
            $table->addColumn('jobTaskId', 'integer', ['limit' => 11]);
            $table->addColumn('subTaskId', 'integer');
            $table->addColumn('isDeleted', 'boolean', ['default' => false, 'null' => true]);
            $table->save();
        }

    }
}
