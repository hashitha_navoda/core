<?php

use Phinx\Migration\AbstractMigration;

class AddLoyaltyCardAsaPaymentMethod extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
      public function change()
      {
      }
     */

    /**
     * Migrate Up.
     */
    public function up()
    {
        $this->execute("INSERT INTO paymentMethod VALUES(4,'Loyalty Card','Loyalty Card')");
    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }

}
