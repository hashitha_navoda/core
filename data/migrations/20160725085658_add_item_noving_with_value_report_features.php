<?php

use Phinx\Migration\AbstractMigration;

class AddItemNovingWithValueReportFeatures extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change() {
        $pdo = $this->getAdapter()->getConnection();
        $roles = $this->fetchAll("SELECT roleID FROM `role`"); //role list
        //feature list
        $featureList = array(
            array(
                'name' => 'View Report',
                'controller' => 'Reporting\Controller\API\StockInHandReport',
                'action' => 'itemMovingWithValue',
                'category' => 'Item Moving With Value Report',
                'type' => '0',
                'moduleId' => '2'
            ),
            array(
                'name' => 'Generate PDF',
                'controller' => 'Reporting\Controller\API\StockInHandReport',
                'action' => 'itemMovingWithValuePdf',
                'category' => 'Item Moving With Value Report',
                'type' => '0',
                'moduleId' => '2'
            ),
            array(
                'name' => 'Generate CSV',
                'controller' => 'Reporting\Controller\API\StockInHandReport',
                'action' => 'itemMovingWithValueCsv',
                'category' => 'Item Moving With Value Report',
                'type' => '0',
                'moduleId' => '2'
            )
        );

        foreach ($featureList as $feature) {
            //insert feature to feature table
            $featureInsert = <<<SQL
                INSERT INTO `feature`
                    (
                        `featureName`,
                        `featureController`,
                        `featureAction`,
                        `featureCategory`,
                        `featureType`,
                        `moduleID`
                    ) VALUES (
                        :featureName,
                        :featureController,
                        :featureAction,
                        :featureCategory,
                        :featureType,
                        :featureModuleId
                    );
SQL;
            $pdo->prepare($featureInsert)->execute(array(
                'featureName' => $feature['name'],
                'featureController' => $feature['controller'],
                'featureAction' => $feature['action'],
                'featureCategory' => $feature['category'],
                'featureType' => $feature['type'],
                'featureModuleId' => $feature['moduleId']
            ));

            $featureID = $this->fetchRow("select LAST_INSERT_ID()")[0];

            foreach ($roles as $role) {
                $roleID = $role['roleID'];
                if ($roleID == 1) {
                    $enabled = 1;
                } else {
                    //for enable api calls for every role
                    if ($feature['type'] == 1) {
                        $enabled = 1;
                    } else {
                        $enabled = 0;
                    }
                }
                //insert role feature to role feature table
                $roleFeatureInsert = <<<SQL
                INSERT INTO `roleFeature`
                    (
                        `roleID`,
                        `featureID`,
                        `roleFeatureEnabled`
                    ) VALUES (
                        :roleID,
                        :featureID,
                        :roleFeatureEnabled
                    );
SQL;
                $pdo->prepare($roleFeatureInsert)->execute(array(
                    'roleID' => $roleID,
                    'featureID' => $featureID,
                    'roleFeatureEnabled' => $enabled
                ));
            }
        }
    }
}
