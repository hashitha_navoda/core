<?php

use Phinx\Migration\AbstractMigration;

class InsertNewAccountCalledDraftAccountInFinancialAccountTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $pdo = $this->getAdapter()->getConnection();

        $rows = $this->fetchAll("SELECT userID FROM `user` WHERE roleID = 1");
        $userID = $rows[0]['userID'];         
        $timeStamp = date( "Y-m-d H:i:s"); 
        
        $dataArray = array(
            array(
                'financeAccountsID' => 3000,
                'financeAccountsCode' => '0000',
                'financeAccountsName' => 'Draft Account',
                'financeAccountsParentID' => 0,
                'financeAccountClassID' => 300,
                'financeAccountStatusID' => 20,
            ),
        );

        foreach($dataArray as $accounts){


            $entityInsert = <<<SQL
                INSERT INTO `entity` 
                (
                    `createdBy`, 
                    `createdTimeStamp`, 
                    `updatedBy`, 
                    `updatedTimeStamp`, 
                    `deleted`, 
                    `deletedBy`,
                    `deletedTimeStamp`
                ) VALUES (
                    :createdBy, 
                    :createdTimeStamp, 
                    :updatedBy, 
                    :updatedTimeStamp, 
                    :deleted, 
                    :deletedBy,
                    :deletedTimeStamp
                );
    
SQL;

            $pdo->prepare($entityInsert)->execute(array(
                'createdBy' => $userID, 
                'createdTimeStamp' => $timeStamp, 
                'updatedBy' => $userID, 
                'updatedTimeStamp' =>  $timeStamp, 
                'deleted' => 0, 
                'deletedBy' => NULL,
                'deletedTimeStamp' => NULL
            ));

            $entityID = $this->fetchRow("select LAST_INSERT_ID()")[0];
            
            $accountsInsert = <<<SQL
                INSERT INTO `financeAccounts` 
                ( 
                    `financeAccountsID`,
                    `financeAccountsCode`, 
                    `financeAccountsName`, 
                    `financeAccountsParentID`, 
                    `financeAccountClassID`, 
                    `financeAccountStatusID`, 
                    `entityID` 
                ) VALUES (
                    :financeAccountsID,
                    :financeAccountsCode, 
                    :financeAccountsName, 
                    :financeAccountsParentID, 
                    :financeAccountClassID, 
                    :financeAccountStatusID, 
                    :entityID
                );
    
SQL;
    
            $pdo->prepare($accountsInsert)->execute(array( 
                'financeAccountsID' => $accounts['financeAccountsID'], 
                'financeAccountsCode' => $accounts['financeAccountsCode'], 
                'financeAccountsName' => $accounts['financeAccountsName'], 
                'financeAccountsParentID' => $accounts['financeAccountsParentID'], 
                'financeAccountClassID' => $accounts['financeAccountClassID'], 
                'financeAccountStatusID' => $accounts['financeAccountStatusID'], 
                'entityID' => $entityID,
            ));

        }
    }
}
