<?php

use Phinx\Migration\AbstractMigration;

class ChangeTemplateFooterProperties extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->table('template');
        $column = $table->hasColumn('templateFooterShow');
        $column1 = $table->hasColumn('templateDefault');
        $column2 = $table->hasColumn('templateSample');
        if ($column) {
            $table->changeColumn('templateFooterShow', 'boolean', ['default' => 0, 'null' => true])
                ->save();
        }

        if ($column1) {
            $table->changeColumn('templateDefault', 'boolean', ['default' => 0, 'null' => true])
                ->save();
        }

        if ($column2) {
            $table->changeColumn('templateSample', 'boolean', ['default' => 0, 'null' => true])
                ->save();
        }
    }
}
