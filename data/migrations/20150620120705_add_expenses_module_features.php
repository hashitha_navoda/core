<?php

use Phinx\Migration\AbstractMigration;

class AddExpensesModuleFeatures extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
    public function change()
    {
    }
    */

    /**
     * Migrate Up.
     */
    public function up()
    {
        //for insert expenses module to module table
        $this->execute("INSERT INTO `module`(`moduleID`, `moduleName`) VALUES ('9','Expenses')");

        //role list
        $roles = $this->fetchAll("SELECT roleID FROM `role`");

        //feature list
        $featureList = array(
            array('name' =>'view','controller'=>'Expenses\\\Controller\\\Bank','action'=>'index','category'=>'Banks','type'=>'0','moduleId'=>'9'),
            array('name' =>'Default','controller'=>'Expenses\\\Controller\\\API\\\BankAPI','action'=>'register','category'=>'Bank','type'=>'1','moduleId'=>'9'),
            array('name' =>'Default','controller'=>'Expenses\\\Controller\\\API\\\BankAPI','action'=>'update','category'=>'Bank','type'=>'1','moduleId'=>'9'),
            array('name' =>'Default','controller'=>'Expenses\\\Controller\\\API\\\BankAPI','action'=>'getBank','category'=>'Bank','type'=>'1','moduleId'=>'9'),
            array('name' =>'Default','controller'=>'Expenses\\\Controller\\\API\\\BankAPI','action'=>'search','category'=>'Bank','type'=>'1','moduleId'=>'9'),
            array('name' =>'Default','controller'=>'Expenses\\\Controller\\\API\\\BankAPI','action'=>'getBanks','category'=>'Bank','type'=>'1','moduleId'=>'9'),
            array('name' =>'Default','controller'=>'Expenses\\\Controller\\\API\\\BankAPI','action'=>'deleteBank','category'=>'Bank','type'=>'1','moduleId'=>'9'),
            array('name' =>'Default','controller'=>'Expenses\\\Controller\\\API\\\BankAPI','action'=>'bankNameListForDropdown','category'=>'Bank','type'=>'1','moduleId'=>'9'),
            array('name' =>'view','controller'=>'Expenses\\\Controller\\\Bank','action'=>'branch','category'=>'Bank Branches','type'=>'0','moduleId'=>'9'),
            array('name' =>'Default','controller'=>'Expenses\\\Controller\\\API\\\BankAPI','action'=>'addBankBranch','category'=>'Bank Branch','type'=>'1','moduleId'=>'9'),
            array('name' =>'Default','controller'=>'Expenses\\\Controller\\\API\\\BankAPI','action'=>'getBankBranch','category'=>'Bank Branch','type'=>'1','moduleId'=>'9'),
            array('name' =>'Default','controller'=>'Expenses\\\Controller\\\API\\\BankAPI','action'=>'updateBankBranch','category'=>'Bank Branch','type'=>'1','moduleId'=>'9'),
            array('name' =>'Default','controller'=>'Expenses\\\Controller\\\API\\\BankAPI','action'=>'deleteBankBranch','category'=>'Bank Branch','type'=>'1','moduleId'=>'9'),
            array('name' =>'Default','controller'=>'Expenses\\\Controller\\\API\\\BankAPI','action'=>'searchBankBranches','category'=>'Bank Branch','type'=>'1','moduleId'=>'9'),
            array('name' =>'Default','controller'=>'Expenses\\\Controller\\\API\\\BankAPI','action'=>'getBankBranches','category'=>'Bank Branch','type'=>'1','moduleId'=>'9'),
            array('name' =>'Default','controller'=>'Expenses\\\Controller\\\API\\\BankAPI','action'=>'bankBranchListForDropdown','category'=>'Bank Branch','type'=>'1','moduleId'=>'9'),
            array('name' =>'view','controller'=>'Expenses\\\Controller\\\AccountType','action'=>'index','category'=>'Account Types','type'=>'0','moduleId'=>'9'),
            array('name' =>'Default','controller'=>'Expenses\\\Controller\\\API\\\AccountTypeAPI','action'=>'add','category'=>'Account Types','type'=>'1','moduleId'=>'9'),
            array('name' =>'Default','controller'=>'Expenses\\\Controller\\\API\\\AccountTypeAPI','action'=>'updateAccountType','category'=>'Account Type','type'=>'1','moduleId'=>'9'),
            array('name' =>'Default','controller'=>'Expenses\\\Controller\\\API\\\AccountTypeAPI','action'=>'search','category'=>'Account Type','type'=>'1','moduleId'=>'9'),
            array('name' =>'Default','controller'=>'Expenses\\\Controller\\\API\\\AccountTypeAPI','action'=>'getAccountTypes','category'=>'Account Type','type'=>'1','moduleId'=>'9'),
            array('name' =>'Default','controller'=>'Expenses\\\Controller\\\API\\\AccountTypeAPI','action'=>'deleteAccountType','category'=>'Account Type','type'=>'1','moduleId'=>'9'),
            array('name' =>'view','controller'=>'Expenses\\\Controller\\\Account','action'=>'index','category'=>'Accounts','type'=>'0','moduleId'=>'9'),
            array('name' =>'Default','controller'=>'Expenses\\\Controller\\\API\\\AccountAPI','action'=>'createAccount','category'=>'Account','type'=>'1','moduleId'=>'9'),
            array('name' =>'Default','controller'=>'Expenses\\\Controller\\\API\\\AccountAPI','action'=>'updateAccount','category'=>'Account','type'=>'1','moduleId'=>'9'),
            array('name' =>'Default','controller'=>'Expenses\\\Controller\\\API\\\AccountAPI','action'=>'deleteAccount','category'=>'Account','type'=>'1','moduleId'=>'9'),
            array('name' =>'Default','controller'=>'Expenses\\\Controller\\\API\\\AccountAPI','action'=>'getAccount','category'=>'Account','type'=>'1','moduleId'=>'9'),
            array('name' =>'Default','controller'=>'Expenses\\\Controller\\\API\\\AccountAPI','action'=>'getAccounts','category'=>'Account','type'=>'1','moduleId'=>'9'),
            array('name' =>'Default','controller'=>'Expenses\\\Controller\\\API\\\AccountAPI','action'=>'search','category'=>'Account','type'=>'1','moduleId'=>'9'),
            array('name' =>'Default','controller'=>'Expenses\\\Controller\\\API\\\AccountAPI','action'=>'accountNameListForDropdown','category'=>'Account','type'=>'1','moduleId'=>'9'),
            array('name' =>'Default','controller'=>'Expenses\\\Controller\\\API\\\AccountAPI','action'=>'accountNumberListForDropdown','category'=>'Account','type'=>'1','moduleId'=>'9'),
        );

        foreach ($featureList as $feature){
            //insert feature to feature table
            $this->execute("INSERT INTO `feature` (`featureID`, `featureName`, `featureController`, `featureAction`, `featureCategory`, `featureType`, `moduleID`) VALUES (NULL,'".$feature['name']."', '".$feature['controller']."', '".$feature['action']."', '".$feature['category']."', '".$feature['type']."', '".$feature['moduleId']."');");
            $id = $this->fetchRow("select LAST_INSERT_ID()")[0];

            foreach ($roles as $role) {
                $roleID = $role['roleID'];
                if ($roleID == 1) {
                    $enabled = 1;
                } else {
                    //for enable api calls for every role
                    if($feature['type'] === '1'){
                        $enabled = 1;
                    } else {
                        $enabled = 0;
                    }
                }
                $this->execute("INSERT INTO `roleFeature` (`roleFeatureID`, `roleID`, `featureID`, `roleFeatureEnabled`) VALUES (NULL, $roleID, $id,$enabled);");
            }
        }

        //for bank table
        $this->execute("alter table supplierBank drop foreign key `fk_supplierBank_bankBranchID0`; alter table bankBranch drop foreign key `fk_bankBranch_bankID0`; ALTER TABLE `bank` CHANGE bankID bankId INT NOT NULL AUTO_INCREMENT");
        $this->execute("ALTER TABLE `bank` ADD `entityId` INT");

        //bank branch table
        $this->execute("ALTER TABLE `bankBranch` CHANGE bankBranchID bankBranchId INT NOT NULL AUTO_INCREMENT");
        $this->execute("ALTER TABLE `bankBranch` CHANGE bankID bankId INT");
        $this->execute("ALTER TABLE `bankBranch` ADD `entityId` INT");

        //for accountType
        $this->execute("CREATE TABLE IF NOT EXISTS `accountType` (
            `accountTypeId` int(11) NOT NULL AUTO_INCREMENT,
            `accountTypeName` varchar(255) DEFAULT NULL,
            `entityId` int(11) DEFAULT NULL,
            PRIMARY KEY (`accountTypeId`)
          ) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;"
        );

        //for account table
        $this->execute("CREATE TABLE IF NOT EXISTS `account` (
            `accountId` int(11) NOT NULL AUTO_INCREMENT,
            `accountName` varchar(255) DEFAULT NULL,
            `accountNumber` varchar(30) DEFAULT NULL,
            `accountBalance` decimal(12,2) NOT NULL DEFAULT '0.00',
            `accountStatus` tinyint(1) NOT NULL DEFAULT '1',
            `accountTypeId` int(11) DEFAULT NULL,
            `bankId` int(11) DEFAULT NULL,
            `bankBranchId` int(11) DEFAULT NULL,
            `entityId` int(11) DEFAULT NULL,
            PRIMARY KEY (`accountId`)
          ) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;"
        );

    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }
}