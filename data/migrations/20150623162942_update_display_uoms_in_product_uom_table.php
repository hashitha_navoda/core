<?php

use Phinx\Migration\AbstractMigration;

class UpdateDisplayUomsInProductUomTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
      public function change()
      {
      }
     */

    /**
     * Migrate Up.
     */
    public function up()
    {
        $products = $this->fetchAll("SELECT `productID` FROM product");

        foreach ($products as $product) {
            $id = $product['productID'];
            $productUomData = $this->fetchAll("SELECT `productUoMID` FROM `productUom` where `productID`='$id' order by `productUomConversion` desc limit 1");
            $key = $productUomData['0']['productUoMID'];
            $this->execute("UPDATE productUom SET `productUomDisplay`='1' WHERE `productUoMID`='$key';");
        }
    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }

}
