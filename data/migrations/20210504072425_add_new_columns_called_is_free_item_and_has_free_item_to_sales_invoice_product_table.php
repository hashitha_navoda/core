<?php

use Phinx\Migration\AbstractMigration;

class AddNewColumnsCalledIsFreeItemAndHasFreeItemToSalesInvoiceProductTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->table('salesInvoiceProduct');
        $column = $table->hasColumn('isFreeItem');
        $column1 = $table->hasColumn('hasFreeItem');
        

        if (!$column) {
            $table->addColumn('isFreeItem', 'boolean', ['default' => false])
                ->update();
        }

        if (!$column1) {
            $table->addColumn('hasFreeItem', 'boolean', ['default' => false])
                ->update();
        }

    }
}
