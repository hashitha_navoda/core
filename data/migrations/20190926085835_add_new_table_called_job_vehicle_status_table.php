<?php

use Phinx\Migration\AbstractMigration;

class AddNewTableCalledJobVehicleStatusTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->hasTable('jobVehicleStatus');

        if (!$table) {
            $table = $this->table('jobVehicleStatus', ['id' => 'jobVehicleStatusId']);
            $table->addColumn('jobVehicleID', 'integer', ['limit' => 20,'default' => null, 'null' => true]);
            $table->addColumn('airFilter', 'boolean', ['default' => null, 'null' => true]);
            $table->addColumn('engineOil', 'boolean', ['default' => null, 'null' => true]);
            $table->addColumn('wiperFluid', 'boolean', ['default' => null, 'null' => true]);
            $table->addColumn('wiperBlades', 'boolean', ['default' => null, 'null' => true]);
            $table->addColumn('tyres', 'boolean', ['default' => null, 'null' => true]);
            $table->addColumn('checkBelts', 'boolean', ['default' => null, 'null' => true]);
            $table->addColumn('coolant', 'boolean', ['default' => null, 'null' => true]);
            $table->addColumn('brakeFluid', 'boolean', ['default' => null, 'null' => true]);
            $table->addColumn('steeringFluid', 'boolean', ['default' => null, 'null' => true]);
            $table->addColumn('transmissionFluid', 'boolean', ['default' => null, 'null' => true]);
            $table->addColumn('battery', 'boolean', ['default' => null, 'null' => true]);
            $table->addColumn('frontLeftTyrePressure', 'decimal', ['default' => null, 'null' => true, 'precision' => 20, 'scale' => 5]);
            $table->addColumn('frontRightTyrePressure', 'decimal', ['default' => null, 'null' => true, 'precision' => 20, 'scale' => 5]);
            $table->addColumn('rearLeftTyrePressure', 'decimal', ['default' => null, 'null' => true, 'precision' => 20, 'scale' => 5]);
            $table->addColumn('rearRightTyrePressure', 'decimal', ['default' => null, 'null' => true, 'precision' => 20, 'scale' => 5]);
            $table->addColumn('odoMeter', 'decimal', ['default' => null, 'null' => true, 'precision' => 20, 'scale' => 5]);
            $table->addColumn('brakePadsFrontfactor', 'decimal', ['default' => null, 'null' => true, 'precision' => 20, 'scale' => 5]);
            $table->addColumn('brakePadsRearfactor', 'decimal', ['default' => null, 'null' => true, 'precision' => 20, 'scale' => 5]);
            $table->addColumn('brakePadsChangeFluid', 'decimal', ['default' => null, 'null' => true, 'precision' => 20, 'scale' => 5]);
            $table->addColumn('entityID', 'integer', ['limit' => 200]);
            $table->save();
        }
    }
}
