<?php

use Phinx\Migration\AbstractMigration;

class CreateMaterialRequestsJobProductTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $materialRequestJobProduct = $this->hasTable('materialRequestJobProduct');

        if (!$materialRequestJobProduct) {
            $table = $this->table('materialRequestJobProduct', ['id' => 'materialRequestJobProductId']);
            $table->addColumn('materialRequestId', 'integer', ['limit' => 11]);
            $table->addColumn('jobProductId', 'integer');
            $table->addColumn('requestedQuantity', 'decimal', ['default' => null, 'null' => true, 'precision' => 20, 'scale' => 5]);
            $table->save();
        }
    }
}
