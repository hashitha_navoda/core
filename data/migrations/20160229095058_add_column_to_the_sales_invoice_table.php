<?php

use Phinx\Migration\AbstractMigration;

class AddColumnToTheSalesInvoiceTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
    public function change()
    {
    }
    */
    
    /**
     * Migrate Up.
     */
    public function up()
    {
        $table = $this->table('salesInvoice');
        
        if (!$table->hasColumn('salesInvoiceRemainingPromotionDiscValue')) {
            $table->addColumn('salesInvoiceRemainingPromotionDiscValue', 'decimal', [
                'after' => 'salesInvoicePromotionDiscount',
                'null'=>'allow',
                'precision'=>20,
                'scale'=>2
            ])->update();
        }
    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }
}