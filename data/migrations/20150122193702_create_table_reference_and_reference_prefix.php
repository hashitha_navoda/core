<?php

use Phinx\Migration\AbstractMigration;

class CreateTableReferenceAndReferencePrefix extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
      public function change()
      {
      }
     */

    /**
     * Migrate Up.
     */
    public function up()
    {
        $check = $this->fetchAll("SHOW COLUMNS FROM `reference` LIKE 'referenceType'");
        if ($check) {
            $abc = $this->fetchAll("SELECT * FROM reference");
            $this->execute("DROP TABLE reference");
            $this->execute("CREATE TABLE reference (referenceNameID INT(11) NOT NULL,referenceName VARCHAR(50),referenceTypeID int(10),PRIMARY KEY(referenceNameID))");
            $this->execute("CREATE TABLE referencePrefix (referencePrefixID INT AUTO_INCREMENT,referenceNameID INT(11) NOT NULL,locationID INT(10),referencePrefixCHAR VARCHAR(50) NOT NULL,referencePrefixNumberOfDigits INT(5),referencePrefixCurrentReference INT(10),PRIMARY KEY (referencePrefixID),FOREIGN KEY(referenceNameID) REFERENCES reference(referenceNameID),FOREIGN KEY(locationID) REFERENCES location(locationID))");

            foreach ($abc as $a) {

                $value = $a['locationID'];
                $value2 = $a['referenceID'];
                $value3 = $a['referenceType'];
                $value4 = $a['numberOfDigits'];
                $value5 = $a['currentReference'];
                $value6 = $a['referenceName'];

                if ($value) {
                    $test = $this->fetchRow("SELECT(`referencePrefixCHAR`) FROM referencePrefix WHERE `referenceNameID`= '{$value2}'");

                    if ($test) {
                        $this->execute("INSERT INTO `referencePrefix`(`referencePrefixID`,`referenceNameID`,`locationID`,`referencePrefixCHAR`,`referencePrefixNumberOfDigits`,`referencePrefixCurrentReference`) VALUES (NULL,'{$value2}','{$value}','{$value3}','{$value4}' ,'{$value5}');");
                    } else {
                        $this->execute("INSERT INTO `reference`(`referenceNameID`,`referenceName`,`referenceTypeID`) VALUES ('{$value2}','{$value6}','1');");
                        $this->execute("INSERT INTO `referencePrefix`(`referencePrefixID`,`referenceNameID`,`locationID`,`referencePrefixCHAR`,`referencePrefixNumberOfDigits`,`referencePrefixCurrentReference`) VALUES (NULL,'{$value2}','{$value}','{$value3}','{$value4}' ,'{$value5}');");
                    }
                } else {
                    $this->execute("INSERT INTO `reference`(`referenceNameID`,`referenceName`,`referenceTypeID`) VALUES ('{$value2}','{$value6}','0');");
                    $this->execute("INSERT INTO `referencePrefix`(`referencePrefixID`,`referenceNameID`,`locationID`,`referencePrefixCHAR`,`referencePrefixNumberOfDigits`,`referencePrefixCurrentReference`) VALUES (NULL,'{$value2}',null,'{$value3}','{$value4}' ,'{$value5}');");
                }
            }
        }
    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }

}
