<?php

use Phinx\Migration\AbstractMigration;

class CreateCustomerLoyaltyIncomingPaymentHistoryTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
      public function change()
      {
      }
     */

    /**
     * Migrate Up.
     */
    public function up()
    {
        $this->execute("CREATE TABLE `customerLoyaltyHistory` (
          `customerLoyaltyHistoryID` int(11) NOT NULL AUTO_INCREMENT,
          `customerLoyaltyID` int(11) NOT NULL,
          `incomingPaymentID` int(11) NOT NULL,
          `collectedPoints` float(10),
          `redeemedPoints` float(10),
          `expired` int(5) DEFAULT 0,
            PRIMARY KEY (`customerLoyaltyHistoryID`),
            FOREIGN KEY (`customerLoyaltyID`) REFERENCES customerLoyalty(`customerLoyaltyID`),
            FOREIGN KEY (`incomingPaymentID`) REFERENCES incomingPayment(`incomingPaymentID`)
        ) ENGINE=InnoDB DEFAULT CHARSET=latin1");
    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }

}
