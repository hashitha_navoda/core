<?php

use Phinx\Migration\AbstractMigration;

class ReverseItemInUnitPriceDueToInvoiceCancel extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
      public function change()
      {
      }
     */

    /**
     * Migrate Up.
     */
    public function up()
    {
        $itemInInvoiceCancelDetails = $this->fetchAll("SELECT * FROM itemIn WHERE itemInDocumentType = 'Invoice Cancel'");

        foreach ($itemInInvoiceCancelDetails as $v) {
            if ($v['itemInDocumentID'] != NULL) {
                $itemInLocationProductID = $v['itemInLocationProductID'];
                $itemInBatchID = ($v['itemInBatchID'] != NULL) ? "=" . $v['itemInBatchID'] : 'IS NULL';
                $itemInSerialID = ($v['itemInSerialID'] != NULL) ? "=" . $v['itemInSerialID'] : 'IS NULL';

                if ($v['itemInBatchID'] == NULL && $v['itemInSerialID'] == NULL) {
                    continue;
                } else {
                    $grnDetails = $this->fetchRow("SELECT * FROM itemIn WHERE itemInDocumentType = 'Goods Received Note' AND itemInLocationProductID = '{$itemInLocationProductID}' AND itemInBatchID ='{$itemInBatchID}' AND itemInSerialID='{$itemInSerialID}'");
                    $this->execute("UPDATE itemIn SET itemInPrice = '{$grnDetails['itemInPrice']}' WHERE itemInID = '{$v['itemInID']}' AND itemInDocumentType = 'Invoice Cancel'");
                }
            }
        }
    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }

}
