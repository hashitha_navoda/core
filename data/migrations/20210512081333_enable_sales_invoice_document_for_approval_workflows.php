<?php

use Phinx\Migration\AbstractMigration;

class EnableSalesInvoiceDocumentForApprovalWorkflows extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $pdo = $this->getAdapter()->getConnection();
        $apprDetails = $this->fetchAll("SELECT * FROM `approvalDocumentDetails` WHERE `docID` = 1");
        $approvalDocumentDetailID = $apprDetails[0]['approvalDocumentDetailID'];

        if (!empty($apprDetails)) {      
            $this->execute("UPDATE `approvalDocumentDetails` SET `isShow` = true WHERE `approvalDocumentDetailID` = $approvalDocumentDetailID "); 
        }
    }
}
