<?php

use Phinx\Migration\AbstractMigration;

class AddNewColumnsToJobTableCalledOldStatusAndCreatedat extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        // add new columns to the jobTable
        $table = $this->table('job');
        $column = $table->hasColumn('completed_at');
        $column2 = $table->hasColumn('old_status');

        if (!$column) {
            $table->addColumn('completed_at', 'datetime', ['null' => true])
                ->update();
        }

        if (!$column2) {
            $table->addColumn('old_status', 'integer', ['null' => true])
                ->update();
        }

    }
}
