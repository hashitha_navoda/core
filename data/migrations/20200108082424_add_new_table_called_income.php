<?php

use Phinx\Migration\AbstractMigration;

class AddNewTableCalledIncome extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->hasTable('income');

        if (!$table) {
            $table = $this->table('income', ['id' => 'incomeID']);
            $table->addColumn('incomeCode', 'string', ['limit' => 200]);
            $table->addColumn('customerID', 'integer', ['limit' => 20,'default' => null, 'null' => true]);
            $table->addColumn('incomeAccountID', 'integer', ['limit' => 20,'default' => null, 'null' => true]);
            $table->addColumn('incomeLocationID', 'integer', ['limit' => 20,'default' => null, 'null' => true]);
            $table->addColumn('incomeDate', 'date', ['default' => null, 'null' => true]);
            $table->addColumn('incomeTotal', 'decimal', ['precision' => 20, 'scale' => 5]);
            $table->addColumn('incomeComment', 'text', ['default' => null, 'null' => true]);
            $table->addColumn('incomeItemFlag', 'integer', ['limit' => 20,'default' => null, 'null' => true]);
            $table->addColumn('status', 'integer', ['limit' => 20]);
            $table->addColumn('entityID', 'integer', ['limit' => 20,'default' => null, 'null' => true]);
            $table->save();
        }
    }
}
