<?php

use Phinx\Migration\AbstractMigration;

class AddNewColumnsToSalesInvoiceProductTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->table('salesInvoiceProduct');
        $column = $table->hasColumn('salesInvoiceProductMrpType');
        $column1 = $table->hasColumn('salesInvoiceProductMrpValue');
        $column2 = $table->hasColumn('salesInvoiceProductMrpPercentage');
        $column3 = $table->hasColumn('salesInvoiceProductMrpAmount');

        if (!$column) {
            $table->addColumn('salesInvoiceProductMrpType', 'integer', ['default' => null, 'null' => true])
                ->update();
        }

        if (!$column1) {
            $table->addColumn('salesInvoiceProductMrpValue', 'decimal', ['precision' => 20, 'scale' => 5, 'default' => null, 'null' => true])
                ->update();
        }

        if (!$column2) {
            $table->addColumn('salesInvoiceProductMrpPercentage', 'decimal', ['precision' => 20, 'scale' => 5, 'default' => null, 'null' => true])
                ->update();
        }

        if (!$column3) {
            $table->addColumn('salesInvoiceProductMrpAmount', 'decimal', ['precision' => 20, 'scale' => 5, 'default' => null, 'null' => true])
                ->update();
        }
    }
}
