<?php

use Phinx\Migration\AbstractMigration;

class CreateMigrationForAddActionToBomController extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
      public function change()
      {
      }
     */

    /**
     * Migrate Up.
     */
    public function up()
    {
        $rows = $this->fetchAll("SELECT roleID FROM `role`");
//this migration for activate bom Controller Actions.
        $dataArray[1] = array('featureID' => 'NULL', 'featureName' => 'Create', 'featureController' => 'Inventory\\\Controller\\\Bom', 'featureAction' => 'create', 'featureCategory' => 'Bom', 'featureType' => '0', 'moduleID' => '2');
        $dataArray[2] = array('featureID' => 'NULL', 'featureName' => 'Update', 'featureController' => 'Inventory\\\Controller\\\Bom', 'featureAction' => 'edit', 'featureCategory' => 'Bom', 'featureType' => '0', 'moduleID' => '2');
        $dataArray[3] = array('featureID' => 'NULL', 'featureName' => 'List', 'featureController' => 'Inventory\\\Controller\\\Bom', 'featureAction' => 'list', 'featureCategory' => 'Bom', 'featureType' => '0', 'moduleID' => '2');
        $dataArray[4] = array('featureID' => 'NULL', 'featureName' => 'View', 'featureController' => 'Inventory\\\Controller\\\Bom', 'featureAction' => 'view', 'featureCategory' => 'Bom', 'featureType' => '0', 'moduleID' => '2');
        $dataArray[5] = array('featureID' => 'NULL', 'featureName' => 'A/DCreate', 'featureController' => 'Inventory\\\Controller\\\Bom', 'featureAction' => 'ad', 'featureCategory' => 'Bom', 'featureType' => '0', 'moduleID' => '2');
        $dataArray[6] = array('featureID' => 'NULL', 'featureName' => 'A/DList', 'featureController' => 'Inventory\\\Controller\\\Bom', 'featureAction' => 'adList', 'featureCategory' => 'Bom', 'featureType' => '0', 'moduleID' => '2');

        $dataArray[7] = array('featureID' => 'NULL', 'featureName' => 'Default', 'featureController' => 'Inventory\\\Controller\\\API\\\Bom', 'featureAction' => 'addBom', 'featureCategory' => 'Application', 'featureType' => '1', 'moduleID' => '2');
        $dataArray[8] = array('featureID' => 'NULL', 'featureName' => 'Default', 'featureController' => 'Inventory\\\Controller\\\API\\\Bom', 'featureAction' => 'getBomSubItemDetails', 'featureCategory' => 'Application', 'featureType' => '1', 'moduleID' => '2');
        $dataArray[9] = array('featureID' => 'NULL', 'featureName' => 'Default', 'featureController' => 'Inventory\\\Controller\\\API\\\Bom', 'featureAction' => 'updateBom', 'featureCategory' => 'Application', 'featureType' => '1', 'moduleID' => '2');
        $dataArray[10] = array('featureID' => 'NULL', 'featureName' => 'Default', 'featureController' => 'Inventory\\\Controller\\\API\\\Bom', 'featureAction' => 'searchBomItemsForDropdown', 'featureCategory' => 'Application', 'featureType' => '1', 'moduleID' => '2');
        $dataArray[11] = array('featureID' => 'NULL', 'featureName' => 'Default', 'featureController' => 'Inventory\\\Controller\\\API\\\Bom', 'featureAction' => 'getBomSubItemDetailsForAssemble', 'featureCategory' => 'Application', 'featureType' => '1', 'moduleID' => '2');
        $dataArray[12] = array('featureID' => 'NULL', 'featureName' => 'Default', 'featureController' => 'Inventory\\\Controller\\\API\\\Bom', 'featureAction' => 'saveBomAD', 'featureCategory' => 'Application', 'featureType' => '1', 'moduleID' => '2');
        $dataArray[13] = array('featureID' => 'NULL', 'featureName' => 'Default', 'featureController' => 'Inventory\\\Controller\\\API\\\Bom', 'featureAction' => 'getUomConversionValue', 'featureCategory' => 'Application', 'featureType' => '1', 'moduleID' => '2');

        for ($i = 1; $i < 14; $i++) {
            $featureName = $dataArray[$i]['featureName'];
            $featureController = $dataArray[$i]['featureController'];
            $featureAction = $dataArray[$i]['featureAction'];
            $featureCategory = $dataArray[$i]['featureCategory'];
            $featureType = $dataArray[$i]['featureType'];
            $moduleID = $dataArray[$i]['moduleID'];

            $this->execute("INSERT INTO `feature` (`featureID`, `featureName`, `featureController`, `featureAction`, `featureCategory`, `featureType`, `moduleID`) VALUES (NULL, '$featureName', '$featureController', '$featureAction', '$featureCategory', '$featureType', '$moduleID');");
            $id = $this->fetchRow("select LAST_INSERT_ID()")[0];

            foreach ($rows as $row) {
                //for bom controller actions
                $roleID = $row['roleID'];
                if ($featureType == '0') {
                    if ($roleID == 1) {
                        $enabled = 1;
                    } else {
                        $enabled = 0;
                    }
                } // for bom api controller actions
                else {
                    $enabled = 1;
                }
                $this->execute("INSERT INTO `roleFeature` (`roleFeatureID`, `roleID`, `featureID`, `roleFeatureEnabled`) VALUES (NULL, $roleID, $id,$enabled);");
            }
        }
    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }

}
