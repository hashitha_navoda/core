<?php

use Phinx\Migration\AbstractMigration;

class ReCreatePosCreditNoteTemplate extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $docTypeId = $this->fetchRow("SELECT documentTypeID from documentType WHERE documentTypeName = 'POS Credit Note'")[0];

        $this->execute("SET FOREIGN_KEY_CHECKS=0;");

        $this->execute("UPDATE documentType SET documentTypeID = 24 WHERE documentTypeID = " . $docTypeId);

        $this->execute("UPDATE template SET documentTypeID = 24 WHERE documentTypeID = " . $docTypeId);

        $this->execute("SET FOREIGN_KEY_CHECKS=1;");
    }
}
