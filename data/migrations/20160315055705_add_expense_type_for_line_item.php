<?php

use Phinx\Migration\AbstractMigration;

class AddExpenseTypeForLineItem extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
    public function change()
    {
    }
    */
    
    /**
     * Migrate Up.
     */
    public function up()
    {        
        $paymentVoucherProductTable = $this->table('paymentVoucherProduct');
        $hasColumn = $paymentVoucherProductTable->hasColumn('paymentVoucherProductExpenseTypeId');
        if (!$hasColumn) {
            $paymentVoucherProductTable->addColumn('paymentVoucherProductExpenseTypeId', 'integer', array(
                'after' => 'roockID',
                'null' => 'allow',
                'default' => null
            ))
            ->update();
        }
        
        $paymentVoucherNonItemProductTable = $this->table('paymentVoucherNonItemProduct');
        $hasColumn = $paymentVoucherNonItemProductTable->hasColumn('paymentVoucherNonItemProductExpenseTypeId');
        if (!$hasColumn) {
            $paymentVoucherNonItemProductTable->addColumn('paymentVoucherNonItemProductExpenseTypeId', 'integer', array(
                'after' => 'roockID',
                'null' => 'allow',
                'default' => null
            ))
            ->update();
        }        
    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }
}