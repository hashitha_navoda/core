<?php

use Phinx\Migration\AbstractMigration;

class AddNewCoulmnCalledIsDeletedInDepartmentTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table1 = $this->table('department');
        $column1 = $table1->hasColumn('isDeleted');

        if (!$column1) {
            $table1->addColumn('isDeleted', 'boolean', ['after' => 'departmentStatus', 'default' => false])
              ->update();
        }

        $table2 = $this->table('employeeDesignation');
        $column2 = $table2->hasColumn('isDeleted');

        if (!$column2) {
            $table2->addColumn('isDeleted', 'boolean', ['after' => 'designationID', 'default' => false])
              ->update();
        }

        $table3 = $this->table('employeeTeam');
        $column3 = $table3->hasColumn('isDeleted');

        if (!$column3) {
            $table3->addColumn('isDeleted', 'boolean', ['after' => 'teamID', 'default' => false])
              ->update();
        }
    }
}
