<?php

use Phinx\Migration\AbstractMigration;

class CreateTableCalledCreditNoteProductTax extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
      public function change()
      {
      }
     */

    /**
     * Migrate Up.
     */
    public function up()
    {
        $this->execute(" CREATE TABLE IF NOT EXISTS `creditNoteProductTax` (
  `creditNoteProductTaxID` int(11) NOT NULL AUTO_INCREMENT,
  `creditNoteProductID` int(11) NOT NULL,
  `taxID` int(11) NOT NULL,
  `creditNoteProductTaxPercentage` decimal(12,2) DEFAULT NULL,
  `creditNoteTaxAmount` decimal(12,2) DEFAULT NULL,
  PRIMARY KEY (`creditNoteProductTaxID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;");
    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }

}
