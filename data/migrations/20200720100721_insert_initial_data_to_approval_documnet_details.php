<?php

use Phinx\Migration\AbstractMigration;

class InsertInitialDataToApprovalDocumnetDetails extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        ini_set('memory_limit', '-1');
        $docTypes = $this->fetchAll("SELECT documentTypeID,documentTypeName FROM `documentType`");

        $dataArray = [];
        foreach ($docTypes as $key => $value) {
            $dataArray[] = [
                'docID' => $value['documentTypeID'],
                'isActive' => ($value['documentTypeID'] == 9 || $value['documentTypeID'] == 10) ? 1 : 0,
                'isShow' => ($value['documentTypeID'] == 9 || $value['documentTypeID'] == 10) ? 1 : 0
            ];
        }

        $pdo = $this->getAdapter()->getConnection();

        foreach($dataArray as $method){

            $approvalDocInserted = <<<SQL
                INSERT INTO `approvalDocumentDetails` 
                ( 
                    `docID`, 
                    `isActive`, 
                    `isShow`
                ) VALUES (
                    :docID, 
                    :isActive,  
                    :isShow
                );
    
SQL;
    
            $pdo->prepare($approvalDocInserted)->execute(array( 
                'docID' => $method['docID'], 
                'isActive' => $method['isActive'], 
                'isShow' => $method['isShow']
            ));

        }
    }
}
