<?php

use Phinx\Migration\AbstractMigration;

class CreateTableInvoiceEditLog extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
      public function change()
      {
      }
     */

    /**
     * Migrate Up.
     */
    public function up()
    {
        $this->execute("CREATE TABLE IF NOT EXISTS `invoiceEditLog` (
  `invoiceEditLogID` int(11) NOT NULL AUTO_INCREMENT,
  `userID` int(11) NOT NULL,
  `salesInvoiceID` varchar(55) NOT NULL,
  `dateAndTime` datetime NOT NULL,
  PRIMARY KEY (`invoiceEditLogID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;");

        $this->execute("CREATE TABLE IF NOT EXISTS `invoiceEditLogDetails` (
  `invoiceEditLogDetailsID` int(11) NOT NULL AUTO_INCREMENT,
  `invoiceEditLogDetailsOldState` varchar(155) DEFAULT NULL,
  `invoiceEditLogDetailsNewState` varchar(155) NOT NULL,
  `invoiceEditLogDetailsCategory` varchar(55) NOT NULL,
  `invoiceEditLogID` int(11) NOT NULL,
  PRIMARY KEY (`invoiceEditLogDetailsID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;");
    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }

}
