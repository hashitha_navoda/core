<?php

use Phinx\Migration\AbstractMigration;

class AddTablesCallItemAttributeAndItemAttributeValues extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $exists = $this->hasTable('itemAttribute');
        if (!$exists) {
                $table = $this->table('itemAttribute',array('id' => 'itemAttributeID'));
                $table->addColumn('itemAttributeCode', 'string', array('limit' => 200))
                    ->addColumn('itemAttributeName', 'string', array('limit' => 200))
                    ->addColumn('itemAttributeState', 'boolean', array('default'=> true))
                    ->addColumn('entityID', 'integer')
                    ->save();
        }

        $existsItemAttrValue = $this->hasTable('itemAttributeValue');
        if (!$existsItemAttrValue) {
                $attrValueTable = $this->table('itemAttributeValue',array('id' => 'itemAttributeValueID'));
                $attrValueTable->addColumn('itemAttributeID', 'integer')
                    ->addColumn('itemAttributeValueDescription', 'string', array('limit' => 200))
                    ->addColumn('itemAttributeValueUomID', 'integer')
                    ->addColumn('entityID', 'integer')
                    ->save();
        }


    }
}
