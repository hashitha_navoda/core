<?php

use Phinx\Migration\AbstractMigration;

class AddNewThreeColumsToCostTypeTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
      public function change()
      {
      }
     */

    /**
     * Migrate Up.
     */
    public function up()
    {
        $this->execute("SET SESSION old_alter_table=1;ALTER TABLE `costType` ADD `productID` INT( 11 ) NOT NULL AFTER `costTypeDescription` ,
ADD `costTypeMinimumValue` DECIMAL( 12, 2 ) NULL DEFAULT '0.00' AFTER `productID` ,
ADD `costTypeMaximumValue` DECIMAL( 12, 2 ) NULL DEFAULT '0.00' AFTER `costTypeMinimumValue` ;");
    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }

}
