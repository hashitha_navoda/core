<?php

use Phinx\Migration\AbstractMigration;

class AddNewTableCalledDepartmentStation extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $departmentStation = $this->hasTable('departmentStation');

        if (!$departmentStation) {
            $table = $this->table('departmentStation', ['id' => 'departmentStationId']);
            $table->addColumn('departmentStationCode', 'string', ['limit' => 30, 'default' => null]);
            $table->addColumn('departmentStationName', 'string', ['limit' => 30, 'default' => null]);
            $table->addColumn('departmentId', 'integer', ['limit' => 11]);
            $table->addColumn('departmentStationStatus', 'integer', ['limit' => 11]);
            $table->addColumn('deleted', 'boolean', ['default' => false]);
            $table->save();
        }
    }
}
