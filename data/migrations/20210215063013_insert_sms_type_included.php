<?php

use Phinx\Migration\AbstractMigration;

class InsertSmsTypeIncluded extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $pdo = $this->getAdapter()->getConnection();

        $dataArray = array(
            array(
                'smsTypeIncludedId' => 1,
                'smsTypeID' => 1,
                'smsIncludedId' => 1,
                'isSelected' => 0
            ),
            array(
                'smsTypeIncludedId' => 2,
                'smsTypeID' => 1,
                'smsIncludedId' => 2,
                'isSelected' => 0
            ),
            array(
                'smsTypeIncludedId' => 3,
                'smsTypeID' => 1,
                'smsIncludedId' => 3,
                'isSelected' => 0
            ),
            array(
                'smsTypeIncludedId' => 4,
                'smsTypeID' => 1,
                'smsIncludedId' => 4,
                'isSelected' => 0
            ),
            array(
                'smsTypeIncludedId' => 5,
                'smsTypeID' => 1,
                'smsIncludedId' => 5,
                'isSelected' => 0
            ),
            array(
                'smsTypeIncludedId' => 6,
                'smsTypeID' => 2,
                'smsIncludedId' => 6,
                'isSelected' => 0
            ),
            array(
                'smsTypeIncludedId' => 7,
                'smsTypeID' => 2,
                'smsIncludedId' => 7,
                'isSelected' => 0
            ),
            array(
                'smsTypeIncludedId' => 8,
                'smsTypeID' => 3,
                'smsIncludedId' => 8,
                'isSelected' => 0
            ),
            array(
                'smsTypeIncludedId' => 9,
                'smsTypeID' => 3,
                'smsIncludedId' => 9,
                'isSelected' => 0
            ),
            array(
                'smsTypeIncludedId' => 10,
                'smsTypeID' => 4,
                'smsIncludedId' => 9,
                'isSelected' => 0
            ),
            array(
                'smsTypeIncludedId' => 11,
                'smsTypeID' => 4,
                'smsIncludedId' => 10,
                'isSelected' => 0
            ),
            array(
                'smsTypeIncludedId' => 12,
                'smsTypeID' => 5,
                'smsIncludedId' => 11,
                'isSelected' => 0
            ),
            array(
                'smsTypeIncludedId' => 13,
                'smsTypeID' => 5,
                'smsIncludedId' => 12,
                'isSelected' => 0
            ),
            array(
                'smsTypeIncludedId' => 14,
                'smsTypeID' => 5,
                'smsIncludedId' => 13,
                'isSelected' => 0
            ),
            array(
                'smsTypeIncludedId' => 15,
                'smsTypeID' => 6,
                'smsIncludedId' => 14,
                'isSelected' => 0
            ),
            array(
                'smsTypeIncludedId' => 16,
                'smsTypeID' => 6,
                'smsIncludedId' => 15,
                'isSelected' => 0
            ),
            array(
                'smsTypeIncludedId' => 17,
                'smsTypeID' => 6,
                'smsIncludedId' => 16,
                'isSelected' => 0
            ),

        );
        
        foreach($dataArray as $type){
            $smsTypeIncludedInsert = <<<SQL
                INSERT INTO `smsTypeIncluded` 
                ( 
                    `smsTypeIncludedId`, 
                    `smsTypeID`,
                    `smsIncludedId`,
                    `isSelected`
                ) VALUES (
                    :smsTypeIncludedId, 
                    :smsTypeID,
                    :smsIncludedId,
                    :isSelected  
                );
    
SQL;
    
            $pdo->prepare($smsTypeIncludedInsert)->execute(array( 
                'smsTypeIncludedId' => $type['smsTypeIncludedId'], 
                'smsTypeID' => $type['smsTypeID'],
                'smsIncludedId' => $type['smsIncludedId'],
                'isSelected' => $type['isSelected']
            ));

        }
    }
}
