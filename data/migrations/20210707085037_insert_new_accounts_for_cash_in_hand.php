<?php

use Phinx\Migration\AbstractMigration;

class InsertNewAccountsForCashInHand extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $pdo = $this->getAdapter()->getConnection();

        $rows = $this->fetchAll("SELECT userID FROM `user` WHERE roleID = 1");
        $userID = $rows[0]['userID'];         
        $timeStamp = date( "Y-m-d H:i:s"); 
        
        $dataArray = array(
            array(
                'financeAccountsCode' => '1025',
                'financeAccountsName' => 'Cash in hand',
                'financeAccountsParentID' => '0',
                'financeAccountClassID' => '1',
                'financeAccountStatusID' => '1',
                'financeAccountsCreditAmount' => '0.00',
                'financeAccountsDebitAmount' => '0.00'
            ),
            array(
                'financeAccountsCode' => '1045',
                'financeAccountsName' => 'Cheque in hand',
                'financeAccountsParentID' => '0',
                'financeAccountClassID' => '1',
                'financeAccountStatusID' => '1',
                'financeAccountsCreditAmount' => '0.00',
                'financeAccountsDebitAmount' => '0.00'
            ),

        );

        foreach($dataArray as $financeAccount){
            $row = $this->fetchRow('SELECT * FROM financeAccounts WHERE financeAccountsCode = '.$financeAccount['financeAccountsCode']);

            if(!$row){
                $entityInsert = <<<SQL
                    INSERT INTO `entity` 
                    (
                        `createdBy`, 
                        `createdTimeStamp`, 
                        `updatedBy`, 
                        `updatedTimeStamp`, 
                        `deleted`, 
                        `deletedBy`,
                        `deletedTimeStamp`
                    ) VALUES (
                        :createdBy, 
                        :createdTimeStamp, 
                        :updatedBy, 
                        :updatedTimeStamp, 
                        :deleted, 
                        :deletedBy,
                        :deletedTimeStamp
                    );
        
SQL;

                $pdo->prepare($entityInsert)->execute(array(
                    'createdBy' => $userID, 
                    'createdTimeStamp' => $timeStamp, 
                    'updatedBy' => $userID, 
                    'updatedTimeStamp' =>  $timeStamp, 
                    'deleted' => 0, 
                    'deletedBy' => NULL,
                    'deletedTimeStamp' => NULL
                ));

                $entityID = $this->fetchRow("select LAST_INSERT_ID()")[0];
                
                $financeAccountInsert = <<<SQL
                    INSERT INTO `financeAccounts` 
                    ( 
                        `financeAccountsCode`, 
                        `financeAccountsName`,
                        `financeAccountsParentID`,
                        `financeAccountClassID`,
                        `financeAccountStatusID`,
                        `financeAccountsCreditAmount`,
                        `financeAccountsDebitAmount`,
                        `entityID`
                    ) VALUES (
                        :financeAccountsCode, 
                        :financeAccountsName,
                        :financeAccountsParentID,
                        :financeAccountClassID,
                        :financeAccountStatusID,
                        :financeAccountsCreditAmount,
                        :financeAccountsDebitAmount,
                        :entityID
                    );
        
SQL;
                $pdo->prepare($financeAccountInsert)->execute(array( 
                    'financeAccountsCode' => $financeAccount['financeAccountsCode'], 
                    'financeAccountsName' => $financeAccount['financeAccountsName'],
                    'financeAccountsParentID' => $financeAccount['financeAccountsParentID'],
                    'financeAccountClassID' => $financeAccount['financeAccountClassID'], 
                    'financeAccountStatusID' => $financeAccount['financeAccountStatusID'],
                    'financeAccountsCreditAmount' => $financeAccount['financeAccountsCreditAmount'],
                    'financeAccountsDebitAmount' => $financeAccount['financeAccountsDebitAmount'],
                    'entityID' => $entityID
                ));
            }
        }
    }
}
