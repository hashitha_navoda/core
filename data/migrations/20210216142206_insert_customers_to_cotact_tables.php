<?php

use Phinx\Migration\AbstractMigration;

class InsertCustomersToCotactTables extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $pdo = $this->getAdapter()->getConnection();
        $paymentData = $this->fetchAll("SELECT * FROM `customer` JOIN entity ON `customer`.entityID = `entity`.entityID WHERE `entity`.deleted = 0");


        if (sizeof($paymentData) > 0) {
            $cusData = [];
            foreach ($paymentData as $key => $value) {
                $customerID = $value['customerID'];

                $cusProf = $this->fetchRow("SELECT * FROM `customerProfile` WHERE `customerProfile`.customerID = $customerID  AND `customerProfile`.isPrimary = '1'");


                $temp = [
                    'contactID' => 'NULL',
                    'customerID' => $customerID,
                    'firstName' => $value['customerName'],
                    'lastName' => NULL,
                    'title' => $value['customerTitle'],
                    'email' => (!empty($cusProf) && !empty($cusProf['customerProfileEmail'])) ? $cusProf['customerProfileEmail'] : "",
                    'tel' =>  $value['customerTelephoneNumber'],
                    'designation' => NULL,
                    'isConvertToCustomer' => 1
                ];

                array_push($cusData, $temp);

            }

            if (sizeof($cusData) > 0) {
                foreach($cusData as $key1 => $feature){
                    if (!empty($feature['tel']) && $feature['customerID'] != 0) {

                        $getUserIDSql = <<<SQL
                            SELECT `user`.`userID`,`entity`.`deleted`
                            FROM `user`
                            LEFT JOIN `entity`
                            ON
                            `entity`.`deleted` = '0'
                            WHERE `user`.`roleID` = '1'
                            LIMIT 1 ;
SQL;
                        $users = $this->fetchAll($getUserIDSql);
                        $userID = $users[0]['userID'];

                        $currentGMTDateTime = gmdate('Y-m-d H:i:s');

        //              create entity
                        $insertEntitySql = <<<SQL
                                INSERT INTO `entity` (
                                `createdBy`,
                                `createdTimeStamp`,
                                `updatedBy`,
                                `updatedTimeStamp`,
                                `deleted`
                                )
                                    VALUES (
                                '{$userID}',
                                '{$currentGMTDateTime}',
                                '{$userID}',
                                '{$currentGMTDateTime}',
                                '0'
           ) ;
SQL;
                        $this->execute($insertEntitySql);
                        $entityID = $this->fetchRow("select LAST_INSERT_ID()")[0];


                        $contactInsert = <<<SQL
                            INSERT INTO `contacts`
                            (
                                `firstName`,
                                `lastName`,
                                `title`,
                                `designation`,
                                `entityID`,
                                `isConvertToCustomer`,
                                `relatedCusID`
                            ) VALUES (
                                :firstName,
                                :lastName,
                                :title,
                                :designation,
                                :entityID,
                                :isConvertToCustomer,
                                :relatedCusID
                            );
SQL;
                        $pdo->prepare($contactInsert)->execute(array(
                            'firstName' => $feature['firstName'],
                            'lastName' => $feature['lastName'],
                            'title' => $feature['title'],
                            'designation' => $feature['designation'],
                            'entityID' => $entityID,
                            'isConvertToCustomer' => $feature['isConvertToCustomer'],
                            'relatedCusID' => $feature['customerID']
                        ));

                        $contactID = $this->fetchRow("select LAST_INSERT_ID()")[0];


                        $conMobNumInsert = <<<SQL
                            INSERT INTO `contactMobileNumbers`
                            (
                                `contactID`,
                                `mobileNumber`,
                                `mobileNumberType`
                            ) VALUES (
                                :contactID,
                                :mobileNumber,
                                :mobileNumberType
                            );
SQL;
                        $pdo->prepare($conMobNumInsert)->execute(array(
                            'contactID' => $contactID,
                            'mobileNumber' => $feature['tel'],
                            'mobileNumberType' => 1
                        ));

                        if (!empty($feature['email'])) {

                            $conEmailInsert = <<<SQL
                                INSERT INTO `contactEmails`
                                (
                                    `contactID`,
                                    `emailAddress`,
                                    `emailType`
                                ) VALUES (
                                    :contactID,
                                    :emailAddress,
                                    :emailType
                                );
SQL;
                            $pdo->prepare($conEmailInsert)->execute(array(
                                'contactID' => $contactID,
                                'emailAddress' => $feature['email'],
                                'emailType' => 1
                            ));

                        }
                    }
                }
            }
        }
    }
}
