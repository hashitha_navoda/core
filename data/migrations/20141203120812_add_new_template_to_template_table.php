<?php

use Phinx\Migration\AbstractMigration;

class AddNewTemplateToTemplateTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
      public function change()
      {
      }
     */

    /**
     * Migrate Up.
     */
    public function up()
    {
        $sql = <<<EOF
                INSERT INTO `template` (`templateID`, `templateName`, `documentTypeID`, `documentSizeID`, `templateContent`, `templateFooterDetails`, `templateFooterShow`, `templateDefault`, `templateSample`) VALUES
(56, 'Template', 6, 1, '<table class="table table-bordered" width="100%">\n<tbody>\n<tr>\n<td><img class="logo" style="background: #e0e0e0; width: 75px; height: 75px; display: block; text-align: center;" src="[Company Logo]" alt="Company Logo" /><span style="font-weight: bold;"><abbr contenteditable="false" spellcheck="false">[Company Name]</abbr> </span><br /><abbr contenteditable="false" spellcheck="false">[Company Address]</abbr> <br />Tel: <abbr contenteditable="false" spellcheck="false">[Company Tel. No]</abbr> <br />Email: <abbr contenteditable="false" spellcheck="false">[Company Email]</abbr> <br />Tax No: <abbr contenteditable="false" spellcheck="false">[Company Tax Reg. No]</abbr>&nbsp;</td>\n<td>\n<h2 align="right">Credit Note</h2>\n</td>\n</tr>\n<tr>\n<td>\n<div class="add_block"><hr /></div>\n</td>\n<td class="relative_data" rowspan="2" align="right">\n<table class="table table-bordered table-condensed" width="100%">\n<tbody>\n<tr>\n<td>Date:</td>\n<td style="text-align: right;">[Credit Note Date]</td>\n</tr>\n<tr>\n<td>Credit Note no:</td>\n<td style="text-align: right;">[Credit Note Code]</td>\n</tr>\n<tr>\n<td style="vertical-align: top;">&nbsp;</td>\n<td style="vertical-align: top; text-align: right;"><br />&nbsp;</td>\n</tr>\n<tr>\n<td>&nbsp;</td>\n<td style="text-align: right;">&nbsp;</td>\n</tr>\n</tbody>\n</table>\n</td>\n</tr>\n<tr>\n<td>\n<div class="add_block">\n<div id="cust_name"><strong><abbr contenteditable="false" spellcheck="false">[Customer Name]</abbr> <br /></strong>[Customer Address]<strong><br /></strong></div>\n</div>\n</td>\n</tr>\n</tbody>\n</table>\n<noneditabletable>\n<table class="noneditable table table-bordered" style="width: 100%; margin-bottom: 100px;" title="" contenteditable="false" data-original-title="">\n<tbody>\n<tr>\n<td colspan="2">\n<div class="qout_body"><hr />\n<table id="item_tbl" class="table table-bordered table-striped" style="height: 92px;" width="679"><colgroup><col width="10px" /><col width="90px" /><col width="290px" /><col width="50px" /><col width="100px" /><col width="100px" /> </colgroup>\n<thead>\n<tr><th colspan="2">Product code</th><th colspan="1">Product</th><th colspan="1">Qty</th><th colspan="1">Price</th><th colspan="1">Total</th></tr>\n</thead>\n<tbody>\n<tr>\n<td>1</td>\n<td align="left">12</td>\n<td align="left">dell_lap<br /><small>(Disc. 1.00)</small></td>\n<td align="left">1</td>\n<td align="right">100.00</td>\n<td align="right">99.00</td>\n</tr>\n</tbody>\n</table>\n<div class="summary"><br /> <br /> <br /> <br />\n<table id="" class="table" width="100%">\n<tbody>\n<tr>\n<td colspan="2" rowspan="3">&nbsp;</td>\n<td colspan="3">Sub Total</td>\n<td class="text-right">99.00</td>\n</tr>\n<tr>\n<td colspan="3">\n<h3>Total</h3>\n</td>\n<td class="text-right">\n<h3>99.00</h3>\n</td>\n</tr>\n<!--                                <tr>\n                                                                <td colspan="6"><div></div></td>\n                                                            </tr>--></tbody>\n</table>\n</div>\n</div>\n</td>\n</tr>\n</tbody>\n</table>\n</noneditabletable><footer class="doc">\n<table class="table table-bordered" width="100%">\n<tbody>\n<tr>\n<td><abbr contenteditable="false" spellcheck="false">[Company Name]</abbr>&nbsp;</td>\n<td>&nbsp;</td>\n<td style="text-align: right;">[Credit Note Date]</td>\n</tr>\n</tbody>\n</table>\n</footer>', '', 1, 1, 0);
EOF;
        $this->execute($sql);
    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }

}
