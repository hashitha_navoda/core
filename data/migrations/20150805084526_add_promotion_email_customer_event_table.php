<?php

use Phinx\Migration\AbstractMigration;

class AddPromotionEmailCustomerEventTable extends AbstractMigration
{

    public function up()
    {
        $this->execute("CREATE TABLE IF NOT EXISTS `promotionEmailCustomerEvent` (
  `promotionEmailCustomerEventId` int(11) NOT NULL AUTO_INCREMENT,
  `promotionEmail` int(11) NOT NULL,
  `promotionEmailCustomerEvent` int(11) NOT NULL,
  PRIMARY KEY (`promotionEmailCustomerEventId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;");
    }

}
