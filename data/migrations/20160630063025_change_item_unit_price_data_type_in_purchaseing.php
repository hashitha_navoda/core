<?php

use Phinx\Migration\AbstractMigration;

class ChangeItemUnitPriceDataTypeInPurchaseing extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        //for purchaseOrderProduct table
        $this->execute("ALTER TABLE `purchaseOrderProduct` CHANGE `purchaseOrderProductPrice` `purchaseOrderProductPrice` DECIMAL(25,10) NULL DEFAULT NULL;");
        $this->execute("ALTER TABLE `purchaseOrderProduct` CHANGE `purchaseOrderProductTotal` `purchaseOrderProductTotal` DECIMAL(25,10) NULL DEFAULT NULL;");
        //for purchaseOrderProductTax table
        $this->execute("ALTER TABLE `purchaseOrderProductTax` CHANGE `purchaseOrderTaxAmount` `purchaseOrderTaxAmount` DECIMAL(25,10) NULL DEFAULT NULL;");
        //for purchaseInvoiceProduct table
        $this->execute("ALTER TABLE `purchaseInvoiceProduct` CHANGE `purchaseInvoiceProductPrice` `purchaseInvoiceProductPrice` DECIMAL(25,10) NULL DEFAULT NULL;");
        $this->execute("ALTER TABLE `purchaseInvoiceProduct` CHANGE `purchaseInvoiceProductTotal` `purchaseInvoiceProductTotal` DECIMAL(25,10) NULL DEFAULT NULL;");
        //for purchaseInvoiceProductTax table
        $this->execute("ALTER TABLE `purchaseInvoiceProductTax` CHANGE `purchaseInvoiceTaxAmount` `purchaseInvoiceTaxAmount` DECIMAL(25,10) NULL DEFAULT NULL;");
        //for purchaseReturnProduct table
        $this->execute("ALTER TABLE `purchaseReturnProduct` CHANGE `purchaseReturnProductPrice` `purchaseReturnProductPrice` DECIMAL(25,10) NULL DEFAULT NULL;");
        $this->execute("ALTER TABLE `purchaseReturnProduct` CHANGE `purchaseReturnProductTotal` `purchaseReturnProductTotal` DECIMAL(25,10) NULL DEFAULT NULL;");
        //for purchaseReturnProductTax table
        $this->execute("ALTER TABLE `purchaseReturnProductTax` CHANGE `purchaseReturnTaxAmount` `purchaseReturnTaxAmount` DECIMAL(25,10) NULL DEFAULT NULL;");
        //for debitNoteProduct table
        $this->execute("ALTER TABLE `debitNoteProduct` CHANGE `debitNoteProductPrice` `debitNoteProductPrice` DECIMAL(25,10) NULL DEFAULT NULL;");
        $this->execute("ALTER TABLE `debitNoteProduct` CHANGE `debitNoteProductDiscount` `debitNoteProductDiscount` DECIMAL(25,10) NULL DEFAULT NULL;");
        $this->execute("ALTER TABLE `debitNoteProduct` CHANGE `debitNoteProductTotal` `debitNoteProductTotal` DECIMAL(25,10) NULL DEFAULT NULL;");
        //for debitNoteProductTax table
        $this->execute("ALTER TABLE `debitNoteProductTax` CHANGE `debitNoteProductTaxAmount` `debitNoteProductTaxAmount` DECIMAL(25,10) NULL DEFAULT NULL;");
        //for grnProduct table
        $this->execute("ALTER TABLE `grnProduct` CHANGE `grnProductPrice` `grnProductPrice` DECIMAL(25,10) NULL DEFAULT NULL;");
        $this->execute("ALTER TABLE `grnProduct` CHANGE `grnProductTotal` `grnProductTotal` DECIMAL(25,10) NULL DEFAULT NULL;");
        //for grnProductTax table
        $this->execute("ALTER TABLE `grnProductTax` CHANGE `grnTaxAmount` `grnTaxAmount` DECIMAL(25,10) NULL DEFAULT NULL;");
        //for goodsIssueProduct table
        $this->execute("ALTER TABLE `goodsIssueProduct` CHANGE `unitOfPrice` `unitOfPrice` DECIMAL(25,10) NULL DEFAULT NULL;");
        //for product table
        $this->execute("ALTER TABLE `product` CHANGE `productDiscountValue` `productDiscountValue` DECIMAL(25,10) NULL DEFAULT NULL;");
        $this->execute("ALTER TABLE `product` CHANGE `productDefaultPurchasePrice` `productDefaultPurchasePrice` DECIMAL(25,10) NULL DEFAULT NULL;");
        $this->execute("ALTER TABLE `product` CHANGE `customDutyValue` `customDutyValue` DECIMAL(25,10) NULL DEFAULT NULL;");
        //for locationProduct table
        $this->execute("ALTER TABLE `locationProduct` CHANGE `defaultPurchasePrice` `defaultPurchasePrice` DECIMAL(25,10) NULL DEFAULT NULL;");
        $this->execute("ALTER TABLE `locationProduct` CHANGE `lastPurchasePrice` `lastPurchasePrice` DECIMAL(25,10) NULL DEFAULT NULL;");
    }
}
