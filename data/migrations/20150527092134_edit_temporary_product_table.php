<?php

use Phinx\Migration\AbstractMigration;

class EditTemporaryProductTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
      public function change()
      {
      }
     */

    /**
     * Migrate Up.
     */
    public function up()
    {
        $this->execute("SET SESSION old_alter_table=1; ALTER TABLE `temporaryProduct` DROP `activityID` ;");
        $this->execute("ALTER TABLE `temporaryProduct` CHANGE `temporaryProductSerial` `temporaryProductSerial` VARCHAR( 100 ) NULL DEFAULT '0';");
        $this->execute("ALTER TABLE `temporaryProduct` CHANGE `temporaryProductSerial` `temporaryProductSerial` VARCHAR( 100 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL ;");
        $this->execute("ALTER TABLE `temporaryProduct` CHANGE `temporaryProductBatch` `temporaryProductBatch` VARCHAR( 100 ) NULL DEFAULT NULL ;");
    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }

}
