<?php

use Phinx\Migration\AbstractMigration;

class UpdateCutomCurrecyDataToCurrencyDataToAllTemplate extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
      public function change()
      {
      }
     */

    /**
     * Migrate Up.
     */
    public function up()
    {
        $customCurrency = $this->fetchAll("SELECT * FROM `customCurrency`");
        foreach ($customCurrency as $cc) {
            if ($cc['customCurrencyName'] == 'USD') {
                $currencyID = 13;
            } else if ($cc['customCurrencyName'] == 'AUD') {
                $currencyID = 1;
            } else if ($cc['customCurrencyName'] == 'SGD') {
                $currencyID = 12;
            } else if ($cc['customCurrencyName'] == 'EURO' || $cc['customCurrencyName'] == 'EUR') {
                $currencyID = 4;
            } else if ($cc['customCurrencyName'] == 'LKR') {
                $currencyID = 8;
            } else if ($cc['customCurrencyName'] == 'GBP') {
                $currencyID = 5;
            } else if ($cc['customCurrencyName'] == 'MYR') {
                $currencyID = 9;
            } else if ($cc['customCurrencyName'] == 'HKD') {
                $currencyID = 6;
            } else if ($cc['customCurrencyName'] == 'JPY') {
                $currencyID = 7;
            } else if ($cc['customCurrencyName'] == 'Peso' || $cc['customCurrencyName'] == 'PESO') {
                $currencyID = 11;
            } else if ($cc['customCurrencyName'] == 'NZD') {
                $currencyID = 10;
            } else if ($cc['customCurrencyName'] == 'CHF') {
                $currencyID = 3;
            } else if ($cc['customCurrencyName'] == 'CAD') {
                $currencyID = 2;
            } else {
                $currencyID = 13;
            }
            $currencyRate = $cc['customCurrencyRate'];
            $oldCurrencyID = $cc['customCurrencyId'];

            $this->execute("UPDATE `quotation` SET `customCurrencyId`=$currencyID WHERE `customCurrencyId`=$oldCurrencyID;");
            $this->execute("UPDATE `salesOrders` SET `customCurrencyId`=$currencyID WHERE `customCurrencyId`=$oldCurrencyID;");
            $this->execute("UPDATE `deliveryNote` SET `customCurrencyId`=$currencyID WHERE `customCurrencyId`=$oldCurrencyID;");
            $this->execute("UPDATE `salesInvoice` SET `customCurrencyId`=$currencyID WHERE `customCurrencyId`=$oldCurrencyID;");
            $this->execute("UPDATE `incomingPayment` SET `customCurrencyId`=$currencyID WHERE `customCurrencyId`=$oldCurrencyID;");
            $this->execute("UPDATE `salesReturn` SET `customCurrencyId`=$currencyID WHERE `customCurrencyId`=$oldCurrencyID;");
            $this->execute("UPDATE `creditNote` SET `customCurrencyId`=$currencyID WHERE `customCurrencyId`=$oldCurrencyID;");
            $this->execute("UPDATE `creditNotePayment` SET `customCurrencyId`=$currencyID WHERE `customCurrencyId`=$oldCurrencyID;");

            $this->execute("UPDATE `currency` SET `currencyRate`=$currencyRate WHERE `currencyID`=$currencyID;");
        }
        $row = $this->fetchRow("SELECT currencyID FROM `displaySetup` WHERE `displaySetupID`=1;");
        $this->execute("UPDATE `currency` SET `currencyRate`=1 WHERE `currencyID`={$row['currencyID']};");
    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }

}
