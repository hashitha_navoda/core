<?php

use Phinx\Migration\AbstractMigration;

class AddNewTablesForItemPriceList extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
      public function change()
      {
      }
     */

    /**
     * Migrate Up.
     */
    public function up()
    {
        $this->execute("CREATE TABLE IF NOT EXISTS `priceList` (
  `priceListId` int(11) NOT NULL AUTO_INCREMENT,
  `priceListName` varchar(255) NOT NULL,
  PRIMARY KEY (`priceListId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;");

        $this->execute("CREATE TABLE IF NOT EXISTS `priceListLocations` (
  `priceListLocationsId` int(11) NOT NULL AUTO_INCREMENT,
  `priceListId` int(11) NOT NULL,
  `locationId` int(11) NOT NULL,
  PRIMARY KEY (`priceListLocationsId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;");

        $this->execute("CREATE TABLE IF NOT EXISTS `priceListItems` (
  `priceListItemsId` int(11) NOT NULL AUTO_INCREMENT,
  `priceListId` int(11) NOT NULL,
  `productId` int(11) NOT NULL,
  `priceListItemsPrice` decimal(20,5) DEFAULT '0.00000',
  PRIMARY KEY (`priceListItemsId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;");
    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }

}
