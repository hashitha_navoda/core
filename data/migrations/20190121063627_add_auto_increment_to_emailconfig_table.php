<?php

use Phinx\Migration\AbstractMigration;

class AddAutoIncrementToEmailconfigTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $this->execute("SET SESSION old_alter_table=1; ALTER TABLE `emailConfig` MODIFY COLUMN id INT NOT NULL AUTO_INCREMENT;");
        $this->execute("SET SESSION old_alter_table=1; ALTER TABLE `emailConfig` CHANGE `port` `port` INT(5) NULL DEFAULT NULL;;");
    }
}
