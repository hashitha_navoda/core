<?php

use Phinx\Migration\AbstractMigration;

class InsertNewDataForFetureAndFeturePrefixTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
      public function change()
      {
      }
     */

    /**
     * Migrate Up.
     */
    public function up()
    {
        $this->execute("INSERT IGNORE INTO `reference` (`referenceNameID` ,`referenceName` ,`referenceTypeID`)VALUES ('28', 'Auto Ajusment', '0');");
        $this->execute("INSERT IGNORE INTO `referencePrefix` (
`referencePrefixID` ,
`referenceNameID` ,
`locationID` ,
`referencePrefixCHAR` ,
`referencePrefixNumberOfDigits` ,
`referencePrefixCurrentReference`
)
VALUES (
NULL , '28', NULL , 'AADJ', '6', '1'
);");
    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }

}
