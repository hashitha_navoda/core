<?php

use Phinx\Migration\AbstractMigration;

class AddNewColumnCallTeamStatusToTeamTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->table('team');
        $column1 = $table->hasColumn('teamStatus');

        if (!$column1) {
            $table->addColumn('teamStatus', 'boolean', ['after' => 'teamName', 'default' => true])
              ->update();
        }

        $column2 = $table->hasColumn('isDeleted');

        if (!$column2) {
            $table->addColumn('isDeleted', 'boolean', ['after' => 'teamStatus', 'default' => false])
              ->update();
        }

    }
}
