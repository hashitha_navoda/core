<?php

use Phinx\Migration\AbstractMigration;

class ChangeSomeColumnPropertiesInEmployeeTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function up()
    {
        //designationID
        $table = $this->table('employee');
        $column = $table->hasColumn('designationID');
        if ($column) {
            $table->changeColumn('designationID', 'integer', ['default'=> null, 'null'=>true])
              ->save();
        }
        
        //divitionID
        $table = $this->table('employee');
        $column = $table->hasColumn('divisionID');
        if ($column) { 
            $table->changeColumn('divisionID', 'integer', ['default'=> null, 'null'=>true])
              ->save();
        }

        //employeeHourlyCost
        $table = $this->table('employee');
        $column = $table->hasColumn('employeeHourlyCost');
        if ($column) {
            $table->changeColumn('employeeHourlyCost', 'decimal', ['default'=> null, 'null'=>true])
                  ->save();
        }

        //userID
        $table = $this->table('employee');
        $column = $table->hasColumn('userID');
        if ($column) {
            $table->changeColumn('userID', 'integer', ['default'=> null, 'null'=>true])
                  ->save();
        }

        //employeeEmail
        $table = $this->table('employee');
        $column = $table->hasColumn('employeeEmail');
        if ($column) {
            $table->changeColumn('employeeEmail', 'string', ['null'=>true])
                  ->save();
        }
    }
}
