<?php


use Phinx\Migration\AbstractMigration;

class CreateNewTableCalledReportQueue extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */

    /**
     * Migrate Up.
     */
    public function up()
    {
        $this->execute(" CREATE TABLE IF NOT EXISTS `reportQueue` (
          `reportQueueID` int(11) NOT NULL AUTO_INCREMENT,
          `reportQueueUrl` varchar(250) NOT NULL,
          `reportQueueReportName` varchar(250) NOT NULL,
          `reportQueueCategory` varchar(60) NOT NULL,
          `reportQueueStatus` tinyint(1) NOT NULL,
          `reportQueueCreatedDateTime` datetime NOT NULL,
          PRIMARY KEY (`reportQueueID`)
        ) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;");
    }

    /**
     * Migrate Down.
     */
    public function down()
    {
        $this->execute("DROP TABLE IF EXISTS `reportQueue`");
    }

}
