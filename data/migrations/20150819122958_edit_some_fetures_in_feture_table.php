<?php

use Phinx\Migration\AbstractMigration;

class EditSomeFeturesInFetureTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
      public function change()
      {
      }
     */

    /**
     * Migrate Up.
     */
    public function up()
    {
        $rows = $this->fetchAll("SELECT roleID FROM `role`");

        $this->execute("INSERT INTO `feature` (`featureID`, `featureName`, `featureController`, `featureAction`, `featureCategory`, `featureType`, `moduleID`) VALUES (NULL, 'Print View', 'Invoice\\\Controller\\\CreditNote', 'documentPreview', 'Credit Note', '0', '3');");
        $id = $this->fetchRow("select LAST_INSERT_ID()")[0];

        $this->execute("INSERT INTO `feature` (`featureID`, `featureName`, `featureController`, `featureAction`, `featureCategory`, `featureType`, `moduleID`) VALUES (NULL, 'Print View', 'Invoice\\\Controller\\\DeliveryNote', 'documentPreview', 'Delivery Note', '0', '3');");
        $id1 = $this->fetchRow("select LAST_INSERT_ID()")[0];

        $this->execute("INSERT INTO `feature` (`featureID`, `featureName`, `featureController`, `featureAction`, `featureCategory`, `featureType`, `moduleID`) VALUES (NULL, 'Print View', 'Invoice\\\Controller\\\Invoice', 'documentPreview', 'Invoice', '0', '3');");
        $id2 = $this->fetchRow("select LAST_INSERT_ID()")[0];

        $this->execute("INSERT INTO `feature` (`featureID`, `featureName`, `featureController`, `featureAction`, `featureCategory`, `featureType`, `moduleID`) VALUES (NULL, 'Print View', 'Invoice\\\Controller\\\QuotationController', 'documentPreview', 'Quotation', '0', '3');");
        $id3 = $this->fetchRow("select LAST_INSERT_ID()")[0];

        $this->execute("INSERT INTO `feature` (`featureID`, `featureName`, `featureController`, `featureAction`, `featureCategory`, `featureType`, `moduleID`) VALUES (NULL, 'Print View', 'Invoice\\\Controller\\\Return', 'documentPreview', 'Return', '0', '3');");
        $id4 = $this->fetchRow("select LAST_INSERT_ID()")[0];

        $this->execute("INSERT INTO `feature` (`featureID`, `featureName`, `featureController`, `featureAction`, `featureCategory`, `featureType`, `moduleID`) VALUES (NULL, 'Print View', 'Invoice\\\Controller\\\SalesOrders', 'documentPreview', 'Sales Order', '0', '3');");
        $id5 = $this->fetchRow("select LAST_INSERT_ID()")[0];


        foreach ($rows as $row) {
            $roleID = $row['roleID'];
            if ($roleID == 1) {
                $enabled = 1;
            } else {
                $enabled = 0;
            }
            $this->execute("INSERT INTO `roleFeature` (`roleFeatureID`, `roleID`, `featureID`, `roleFeatureEnabled`) VALUES (NULL, $roleID, $id,$enabled);");
            $this->execute("INSERT INTO `roleFeature` (`roleFeatureID`, `roleID`, `featureID`, `roleFeatureEnabled`) VALUES (NULL, $roleID, $id1,$enabled);");
            $this->execute("INSERT INTO `roleFeature` (`roleFeatureID`, `roleID`, `featureID`, `roleFeatureEnabled`) VALUES (NULL, $roleID, $id2,$enabled);");
            $this->execute("INSERT INTO `roleFeature` (`roleFeatureID`, `roleID`, `featureID`, `roleFeatureEnabled`) VALUES (NULL, $roleID, $id3,$enabled);");
            $this->execute("INSERT INTO `roleFeature` (`roleFeatureID`, `roleID`, `featureID`, `roleFeatureEnabled`) VALUES (NULL, $roleID, $id4,$enabled);");
            $this->execute("INSERT INTO `roleFeature` (`roleFeatureID`, `roleID`, `featureID`, `roleFeatureEnabled`) VALUES (NULL, $roleID, $id5,$enabled);");
        }

        $this->execute("UPDATE `feature` SET `featureName` = 'View' WHERE `featureController` = 'Expenses\\\Controller\\\AccountType' AND `featureAction` = 'index'");
        $this->execute("UPDATE `feature` SET `featureName` = 'View' WHERE `featureController` = 'Expenses\\\Controller\\\Account' AND `featureAction` = 'index'");
        $this->execute("UPDATE `feature` SET `featureName` = 'View' WHERE `featureController` = 'Expenses\\\Controller\\\Bank' AND `featureAction` = 'branch'");
        $this->execute("UPDATE `feature` SET `featureName` = 'View' WHERE `featureController` = 'Expenses\\\Controller\\\Bank' AND `featureAction` = 'index'");
        $this->execute("UPDATE `feature` SET `featureName` = 'Document' WHERE `featureController` = 'Expenses\\\Controller\\\ExpensePurchaseInvoice' AND `featureAction` = 'document'");
        $this->execute("UPDATE `feature` SET `featureName` = 'Payment View' WHERE `featureController` = 'Expenses\\\Controller\\\ExpensePurchaseInvoice' AND `featureAction` = 'view'");
        $this->execute("UPDATE `feature` SET `featureName` = 'Payment Create' WHERE `featureController` = 'Expenses\\\Controller\\\ExpensePurchaseInvoice' AND `featureAction` = 'create'");
        $this->execute("UPDATE `feature` SET `featureName` = 'Payment Preview' WHERE `featureController` = 'Expenses\\\Controller\\\ExpensePurchaseInvoice' AND `featureAction` = 'preview'");
        $this->execute("UPDATE `feature` SET `featureName` = 'Payment List' WHERE `featureController` = 'Expenses\\\Controller\\\ExpensePurchaseInvoice' AND `featureAction` = 'list'");
        $this->execute("UPDATE `feature` SET `featureName` = 'Document' WHERE `featureController` = 'Inventory\\\Controller\\\InventoryAdjustment' AND `featureAction` = 'document'");
        $this->execute("UPDATE `feature` SET `featureName` = 'View' WHERE `featureController` = 'Inventory\\\Controller\\\InventoryAdjustment' AND `featureAction` = 'adjustmentView'");
        $this->execute("UPDATE `feature` SET `featureName` = 'Default',`featureType`='1' WHERE `featureController` = 'Inventory\\\Controller\\\API\\\InventoryAdjustment' AND `featureAction` = 'getGoodsIssueDetails'");
        $this->execute("UPDATE `feature` SET `featureName` = 'Default',`featureType`='1' WHERE `featureController` = 'Inventory\\\Controller\\\API\\\InventoryAdjustment' AND `featureAction` = 'updateGoodsIssueById'");
        $this->execute("UPDATE `feature` SET `featureName` = 'Print View'  WHERE `featureController` = 'Inventory\\\Controller\\\InventoryAdjustment' AND `featureAction` = 'preview'");
        $this->execute("UPDATE `feature` SET `featureName` = 'List',`featureCategory`='Adjustment'  WHERE `featureController` = 'Inventory\\\Controller\\\InventoryAdjustment' AND `featureAction` = 'view'");
        $this->execute("UPDATE `feature` SET `featureName` = 'Document',`featureCategory`='Customer Payment'  WHERE `featureController` = 'Invoice\\\Controller\\\CustomerPayments' AND `featureAction` = 'document'");
        $this->execute("UPDATE `feature` SET `featureName` = 'Print View',`featureCategory`='Customer Payment'  WHERE `featureController` = 'Invoice\\\Controller\\\CustomerPayments' AND `featureAction` = 'viewReceipt'");
        $this->execute("UPDATE `feature` SET `featureName` = 'List' WHERE `featureController` = 'Inventory\\\Controller\\\DebitNote' AND `featureAction` = 'view'");
        $this->execute("UPDATE `feature` SET `featureName` = 'Default',`featureType`='1' WHERE `featureController` = 'Inventory\\\Controller\\\DebitNote' AND `featureAction` = 'viewDebitNoteReceipt'");
        $this->execute("UPDATE `feature` SET `featureName` = 'Print View'  WHERE `featureController` = 'Inventory\\\Controller\\\DebitNote' AND `featureAction` = 'documentPreview'");
        $this->execute("UPDATE `feature` SET `featureName` = 'Document'  WHERE `featureController` = 'Inventory\\\Controller\\\DebitNote' AND `featureAction` = 'document'");
        $this->execute("UPDATE `feature` SET `featureName` = 'List'  WHERE `featureController` = 'Inventory\\\Controller\\\DebitNotePayments' AND `featureAction` = 'view'");
        $this->execute("UPDATE `feature` SET `featureName` = 'Default',`featureType`='1' WHERE `featureController` = 'Inventory\\\Controller\\\DebitNotePayments' AND `featureAction` = 'viewDebitNotePaymentReceipt'");
        $this->execute("UPDATE `feature` SET `featureName` = 'Print View' WHERE `featureController` = 'Inventory\\\Controller\\\DebitNotePayments' AND `featureAction` = 'documentPreview'");
        $this->execute("UPDATE `feature` SET `featureName` = 'Document'  WHERE `featureController` = 'Inventory\\\Controller\\\DebitNotePayments' AND `featureAction` = 'document'");
        $this->execute("UPDATE `feature` SET `featureName` = 'Print View'  WHERE `featureController` = 'Inventory\\\Controller\\\GrnController' AND `featureAction` = 'preview'");
        $this->execute("UPDATE `feature` SET `featureName` = 'Document'  WHERE `featureController` = 'Inventory\\\Controller\\\GrnController' AND `featureAction` = 'document'");
        $this->execute("UPDATE `feature` SET `featureName` = 'List',`featureCategory`='GRN'  WHERE `featureController` = 'Inventory\\\Controller\\\GrnController' AND `featureAction` = 'list'");
        $this->execute("UPDATE `feature` SET `featureName` = 'Create'   WHERE `featureController` = 'Inventory\\\Controller\\\Product' AND `featureAction` = 'index'");
        $this->execute("UPDATE `feature` SET `featureCategory`='Item' WHERE `featureController` = 'Inventory\\\Controller\\\Product' AND `featureAction` = 'edit'");
        $this->execute("UPDATE `feature` SET `featureName`='Location Product Edit',`featureCategory`='Item' WHERE `featureController` = 'Inventory\\\Controller\\\Product' AND `featureAction` = 'locationProductDetails'");
        $this->execute("UPDATE `feature` SET `featureName`='Edit Products By Location',`featureCategory`='Item' WHERE `featureController` = 'Inventory\\\Controller\\\Product' AND `featureAction` = 'editProductsByLocation'");
        $this->execute("UPDATE `feature` SET `featureName`='List',`featureCategory`='Item' WHERE `featureController` = 'Inventory\\\Controller\\\Product' AND `featureAction` = 'list'");
        $this->execute("UPDATE `feature` SET `featureName`='Import',`featureCategory`='Item' WHERE `featureController` = 'Inventory\\\Controller\\\Product' AND `featureAction` = 'import'");
        $this->execute("UPDATE `feature` SET `featureName`='Create',`featureCategory`='Purchase Invoice' WHERE `featureController` = 'Inventory\\\Controller\\\PurchaseInvoiceController' AND `featureAction` = 'create'");
        $this->execute("UPDATE `feature` SET `featureName`='List',`featureCategory`='Purchase Invoice' WHERE `featureController` = 'Inventory\\\Controller\\\PurchaseInvoiceController' AND `featureAction` = 'list'");
        $this->execute("UPDATE `feature` SET `featureName`='View',`featureCategory`='Purchase Invoice' WHERE `featureController` = 'Inventory\\\Controller\\\PurchaseInvoiceController' AND `featureAction` = 'view'");
        $this->execute("UPDATE `feature` SET `featureName`='Print View',`featureCategory`='Purchase Invoice' WHERE `featureController` = 'Inventory\\\Controller\\\PurchaseInvoiceController' AND `featureAction` = 'preview'");
        $this->execute("UPDATE `feature` SET `featureName`='Document',`featureCategory`='Purchase Invoice' WHERE `featureController` = 'Inventory\\\Controller\\\PurchaseInvoiceController' AND `featureAction` = 'document'");
        $this->execute("UPDATE `feature` SET `featureName`='List',`featureCategory`='Purchase Order' WHERE `featureController` = 'Inventory\\\Controller\\\PurchaseOrderController' AND `featureAction` = 'list'");
        $this->execute("UPDATE `feature` SET `featureName`='Print View'  WHERE `featureController` = 'Inventory\\\Controller\\\PurchaseOrderController' AND `featureAction` = 'preview'");
        $this->execute("UPDATE `feature` SET `featureName`='Document'  WHERE `featureController` = 'Inventory\\\Controller\\\PurchaseOrderController' AND `featureAction` = 'document'");
        $this->execute("UPDATE `feature` SET `featureName`='List'  WHERE `featureController` = 'Inventory\\\Controller\\\PurchaseReturnsController' AND `featureAction` = 'list'");
        $this->execute("UPDATE `feature` SET `featureName`='Document'  WHERE `featureController` = 'Inventory\\\Controller\\\PurchaseReturnsController' AND `featureAction` = 'document'");
        $this->execute("UPDATE `feature` SET `featureName`='Create'  WHERE `featureController` = 'Inventory\\\Controller\\\Supplier' AND `featureAction` = 'add'");
        $this->execute("UPDATE `feature` SET `featureName`='List'  WHERE `featureController` = 'Inventory\\\Controller\\\OutGoingPayment' AND `featureAction` = 'view'");
        $this->execute("UPDATE `feature` SET `featureName`='Print View',`featureCategory`='Supplier Payment' WHERE `featureController` = 'Inventory\\\Controller\\\OutGoingPayment' AND `featureAction` = 'viewReceipt'");
        $this->execute("UPDATE `feature` SET `featureName`='Document',`featureCategory`='Supplier Payment' WHERE `featureController` = 'Inventory\\\Controller\\\OutGoingPayment' AND `featureAction` = 'document'");
        $this->execute("UPDATE `feature` SET `featureName`='Default',`featureType`='1',`featureCategory`='Transfer' WHERE `featureController` = 'Inventory\\\Controller\\\Transfer' AND `featureAction` = 'transferView'");
        $this->execute("UPDATE `feature` SET `featureName`='Print View'  WHERE `featureController` = 'Inventory\\\Controller\\\Transfer' AND `featureAction` = 'documentPreview'");
        $this->execute("UPDATE `feature` SET `featureName`='List',`featureCategory`='Transfer'  WHERE `featureController` = 'Inventory\\\Controller\\\Transfer' AND `featureAction` = 'list'");
        $this->execute("UPDATE `feature` SET `featureName`='Email',`featureCategory`='Transfer'  WHERE `featureController` = 'Inventory\\\Controller\\\API\\\Transfer' AND `featureAction` = 'sendEmail'");
        $this->execute("UPDATE `feature` SET `featureName`='Create' WHERE `featureController` = 'JobCard\\\Controller\\\Activity' AND `featureAction` = 'create'");
        $this->execute("DELETE FROM `feature`  WHERE `featureController` = 'JobCard\\\Controller\\\Activity' AND `featureAction` = 'viewActivity'");
        $this->execute("UPDATE `feature` SET `featureName`='Edit' WHERE `featureController` = 'JobCard\\\Controller\\\Activity' AND `featureAction` = 'edit'");
        $this->execute("UPDATE `feature` SET `featureCategory`='Activity Type' WHERE `featureController` = 'JobCard\\\Controller\\\ActivityType' AND `featureAction` = 'create'");
        $this->execute("UPDATE `feature` SET `featureType`='1' WHERE `featureController` = 'JobCard\\\Controller\\\API\\\TemporaryProduct' AND `featureAction` = 'getTemporaryProducts'");
        $this->execute("UPDATE `feature` SET `featureType`='1' WHERE `featureController` = 'JobCard\\\Controller\\\API\\\Activity' AND `featureAction` = 'getActivityByProjectID'");
        $this->execute("UPDATE `feature` SET `featureName`='Create',`featureCategory`='Temporary Contractor' WHERE `featureController` = 'JobCard\\\Controller\\\TemporaryContractor' AND `featureAction` = 'index'");
        $this->execute("UPDATE `feature` SET `featureName`='Create Complain'  WHERE `featureController` = 'JobCard\\\Controller\\\InquiryLog' AND `featureAction` = 'create'");
        $this->execute("UPDATE `feature` SET `featureName`='Create Inquiry'  WHERE `featureController` = 'JobCard\\\Controller\\\InquiryLog' AND `featureAction` = 'createInquiry'");
        $this->execute("UPDATE `feature` SET `featureName`='CRM Inquiry List'  WHERE `featureController` = 'JobCard\\\Controller\\\InquiryLog' AND `featureAction` = 'listInquiry'");
        $this->execute("UPDATE `feature` SET `featureType`='1'  WHERE `featureController` = 'JobCard\\\Controller\\\API\\\InquiryType' AND `featureAction` = 'add'");
        $this->execute("UPDATE `feature` SET `featureType`='1'  WHERE `featureController` = 'JobCard\\\Controller\\\API\\\InquiryType' AND `featureAction` = 'getInquiryTypesBySearchKey'");
        $this->execute("UPDATE `feature` SET `featureName`='Delete'  WHERE `featureController` = 'JobCard\\\Controller\\\API\\\InquiryType' AND `featureAction` = 'deleteInquiryTypeByInquiryTypeID'");
        $this->execute("UPDATE `feature` SET `featureCategory`='Inquiry Status'  WHERE `featureController` = 'JobCard\\\Controller\\\InquiryStatus' AND `featureAction` = 'create'");
        $this->execute("UPDATE `feature` SET `featureName`='Print View'  WHERE `featureController` = 'JobCard\\\Controller\\\Job' AND `featureAction` = 'documentPreview'");
        $this->execute("DELETE FROM `feature`  WHERE `featureController` = 'JobCard\\\Controller\\\Project' AND `featureAction` = 'projectProgress'");
        $this->execute("UPDATE `feature` SET `featureName`='Delete',`featureCategory`='Invoice'   WHERE `featureController` = 'Invoice\\\Controller\\\API\\\Invoice' AND `featureAction` = 'deleteInvoice'");
        $this->execute("UPDATE `feature` SET `featureName`='Document',`featureCategory`='Payment'   WHERE `featureController` = 'Invoice\\\Controller\\\CustomerPayments' AND `featureAction` = 'document'");
        $this->execute("UPDATE `feature` SET `featureName`='Print View',`featureCategory`='Payment'   WHERE `featureController` = 'Invoice\\\Controller\\\CustomerPayments' AND `featureAction` = 'viewReceipt'");
        $this->execute("UPDATE `feature` SET `featureName`='List' WHERE `featureController` = 'Invoice\\\Controller\\\CreditNote' AND `featureAction` = 'view'");
        $this->execute("UPDATE `feature` SET `featureName`='View Credit Note Receipt',`featureType`='1',`featureCategory`='Credit Note' WHERE `featureController` = 'Invoice\\\Controller\\\CreditNote' AND `featureAction` = 'viewCreditNoteReceipt'");
        $this->execute("UPDATE `feature` SET `featureName`='Document',`featureCategory`='Credit Note' WHERE `featureController` = 'Invoice\\\Controller\\\CreditNote' AND `featureAction` = 'document'");
        $this->execute("UPDATE `feature` SET `featureName`='List',`featureCategory`='Credit Note Payments' WHERE `featureController` = 'Invoice\\\Controller\\\CreditNotePayments' AND `featureAction` = 'view'");
        $this->execute("UPDATE `feature` SET `featureName`='View Credit Note Payment Receipt',`featureType`='1',`featureCategory`='Credit Note Payments' WHERE `featureController` = 'Invoice\\\Controller\\\CreditNotePayments' AND `featureAction` = 'viewCreditNotePaymentReceipt'");
        $this->execute("UPDATE `feature` SET `featureName`='Print View',`featureCategory`='Credit Note Payments' WHERE `featureController` = 'Invoice\\\Controller\\\CreditNotePayments' AND `featureAction` = 'documentPreview'");
        $this->execute("UPDATE `feature` SET `featureCategory`='Credit Note Payments' WHERE `featureController` = 'Invoice\\\Controller\\\CreditNotePayments' AND `featureAction` = 'document'");
        $this->execute("UPDATE `feature` SET `featureType`='1' WHERE `featureController` = 'Invoice\\\Controller\\\CustomerAPI' AND `featureAction` = 'updatecustomer'");
        $this->execute("UPDATE `feature` SET `featureName`='Create' WHERE `featureController` = 'Invoice\\\Controller\\\Customer' AND `featureAction` = 'add'");
        $this->execute("UPDATE `feature` SET `featureName`='Edit',`featureType`='0',`featureCategory`='Customer'  WHERE `featureController` = 'Invoice\\\Controller\\\Customer' AND `featureAction` = 'edit'");
        $this->execute("UPDATE `feature` SET `featureType`='1' WHERE `featureController` = 'Invoice\\\Controller\\\CustomerPaymentsAPI' AND `featureAction` = 'getCustomersChequesByFilter'");
        $this->execute("UPDATE `feature` SET `featureName`='List',`featureCategory`='Customer Payment' WHERE `featureController` = 'Invoice\\\Controller\\\CustomerPayments' AND `featureAction` = 'view'");
        $this->execute("UPDATE `feature` SET `featureName`='List',`featureCategory`='Delivery Note' WHERE `featureController` = 'Invoice\\\Controller\\\DeliveryNote' AND `featureAction` = 'view'");
        $this->execute("UPDATE `feature` SET `featureType`='1' WHERE `featureController` = 'Invoice\\\Controller\\\DeliveryNote' AND `featureAction` = 'preview'");
        $this->execute("UPDATE `feature` SET `featureName`='List',`featureCategory`='Invoice' WHERE `featureController` = 'Invoice\\\Controller\\\Invoice' AND `featureAction` = 'view'");
        $this->execute("UPDATE `feature` SET `featureType`='1' WHERE `featureController` = 'Invoice\\\Controller\\\Invoice' AND `featureAction` = 'invoiceView'");
        $this->execute("UPDATE `feature` SET `featureName`='Delete',`featureCategory`='Customer Payment' WHERE `featureController` = 'Invoice\\\Controller\\\CustomerPaymentsAPI' AND `featureAction` = 'deletePayment'");
        $this->execute("UPDATE `feature` SET `featureCategory`='Customer Payment' WHERE `featureController` = 'Invoice\\\Controller\\\CustomerPaymentsAPI' AND `featureAction` = 'sendEmail'");
        $this->execute("UPDATE `feature` SET `featureType`='1' WHERE `featureController` = 'Invoice\\\Controller\\\QuotationController' AND `featureAction` = 'quotationView'");
        $this->execute("UPDATE `feature` SET `featureName`='List',`featureCategory`='Quotation' WHERE `featureController` = 'Invoice\\\Controller\\\QuotationController' AND `featureAction` = 'list'");
        $this->execute("UPDATE `feature` SET `featureName`='List',`featureCategory`='Return' WHERE `featureController` = 'Invoice\\\Controller\\\Return' AND `featureAction` = 'view'");
        $this->execute("DELETE FROM `feature`  WHERE `featureController` = 'Invoice\\\Controller\\\Return' AND `featureAction` = 'index'");
        $this->execute("UPDATE `feature` SET `featureName`='Document',`featureType`='0',`featureCategory`='Return' WHERE `featureController` = 'Invoice\\\Controller\\\Return' AND `featureAction` = 'document'");
        $this->execute("UPDATE `feature` SET `featureType`='1' WHERE `featureController` = 'Invoice\\\Controller\\\SalesOrders' AND `featureAction` = 'confirmDeleteSalesorder'");
        $this->execute("UPDATE `feature` SET `featureType`='1' WHERE `featureController` = 'Invoice\\\Controller\\\SalesOrders' AND `featureAction` = 'salesOrdersView'");
        $this->execute("UPDATE `feature` SET `featureName`='List',`featureCategory`='Sales Order'  WHERE `featureController` = 'Invoice\\\Controller\\\SalesOrders' AND `featureAction` = 'list'");
        $this->execute("UPDATE `feature` SET `featureType`='0' WHERE `featureController` = 'Settings\\\Controller\\\Location' AND `featureAction` = 'create'");
        $this->execute("UPDATE `feature` SET `featureType`='0',`featureName`='Location List' WHERE `featureController` = 'Settings\\\Controller\\\Location' AND `featureAction` = 'viewLocationList'");
        $this->execute("DELETE FROM `feature`  WHERE `featureController` = 'Settings\\\Controller\\\Location' AND `featureAction` = 'getLocationView'");
        $this->execute("UPDATE `feature` SET `featureType`='0' WHERE `featureController` = 'Settings\\\Controller\\\Location' AND `featureAction` = 'edit'");
        $this->execute("DELETE FROM `feature`  WHERE `featureController` = 'User\\\Controller\\\User' AND `featureAction` = 'createUser'");
        $this->execute("UPDATE `feature` SET `featureName`='List',`featureCategory`='User' WHERE `featureController` = 'User\\\Controller\\\User' AND `featureAction` = 'index'");
    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }

}
