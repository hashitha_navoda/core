<?php

use Phinx\Migration\AbstractMigration;

class InsertNewUomToUomTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
      public function change()
      {
      }
     */

    /**
     * Migrate Up.
     */
    public function up()
    {
        $this->execute("INSERT INTO `uom` (
`uomID` ,
`uomName` ,
`uomAbbr` ,
`uomDecimalPlace` ,
`uomState`
)
VALUES (
NULL , 'Unit', 'Unit', '1', '1'
);");
    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }

}
