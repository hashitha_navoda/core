<?php

use Phinx\Migration\AbstractMigration;

class CreateMaterialRequestsTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $materialRequest = $this->hasTable('materialRequest');

        if (!$materialRequest) {
            $table = $this->table('materialRequest', ['id' => 'materialRequestId']);
            $table->addColumn('jobId', 'integer');
            $table->addColumn('materialRequestCode', 'string', ['limit' => 30, 'default' => null]);
            $table->addColumn('materialRequestDate', 'date', ['default' => null]);
            $table->addColumn('materialRequestStatus', 'integer');
            $table->addColumn('entityId', 'integer');
            $table->save();
        }
    }
}
