<?php

use Phinx\Migration\AbstractMigration;

class UpdatedDataInPaymentMethodTableCreditCardAsCard extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     */
    public function change()
    {
        $this->execute(" UPDATE `paymentMethod` SET `paymentMethodName` = 'Card', `paymentMethodDescription` = 'Card' WHERE `paymentMethod`.`paymentMethodID` = 3;");
    }
}
