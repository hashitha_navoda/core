<?php

use Phinx\Migration\AbstractMigration;

class UpdateCustomerCreditAndCustomerOutstandingAsZeroWhereValuesAsNull extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
      public function change()
      {
      }
     */

    /**
     * Migrate Up.
     */
    public function up()
    {
        $this->execute("UPDATE `customer` SET `customerCurrentCredit` = 0.00000 WHERE `customerCurrentCredit` IS NULL ");
        $this->execute("UPDATE `customer` SET `customerCurrentBalance` = 0.00000 WHERE `customerCurrentBalance` IS NULL ");
    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }

}
