<?php

use Phinx\Migration\AbstractMigration;

class AddNewTableCalledCreditNotePaymentMethodNumbers extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
      public function change()
      {
      }
     */

    /**
     * Migrate Up.
     */
    public function up()
    {
        $this->execute("CREATE TABLE IF NOT EXISTS `creditNotePaymentMethodNumbers` (
  `creditNotePaymentMethodNumbersID` int(11) NOT NULL AUTO_INCREMENT,
  `creditNotePaymentID` int(11) NOT NULL,
  `paymentMethodID` int(11) NOT NULL,
  `creditNotePaymentMethodReferenceNumber` varchar(20) NOT NULL,
  `creditNotePaymentMethodBank` varchar(50) DEFAULT NULL,
  `creditNotePaymentMethodCardID` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`creditNotePaymentMethodNumbersID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;");
    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }

}
