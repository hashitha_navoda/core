<?php

use Phinx\Migration\AbstractMigration;

class InsertNewPaymentMethodCalledGiftCardIntoPaymentMethodTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
      public function change()
      {
      }
     */

    /**
     * Migrate Up.
     */
    public function up()
    {
        $this->execute("INSERT IGNORE INTO `paymentMethod` (
`paymentMethodID` ,
`paymentMethodName` ,
`paymentMethodDescription`
)
VALUES (
'6', 'Gift Card', 'Gift Card'
);");
    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }

}
