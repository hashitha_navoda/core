<?php

use Phinx\Migration\AbstractMigration;

class ChangeSomeColumsInIncomingPaymentMethodChecque extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
      public function change()
      {
      }
     */

    /**
     * Migrate Up.
     */
    public function up()
    {
        $this->execute('SET SESSION old_alter_table=1; ALTER TABLE `incomingPaymentMethodCheque` CHANGE `incomingPaymentMethodChequeNumber` `incomingPaymentMethodChequeNumber` VARCHAR( 20 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL ;');
        $this->execute('SET SESSION old_alter_table=1; ALTER TABLE `incomingPaymentMethodCheque` CHANGE `incomingPaymentMethodChequeBankName` `incomingPaymentMethodChequeBankName` VARCHAR( 20 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL ;');
    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }

}
