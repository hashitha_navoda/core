<?php

use Phinx\Migration\AbstractMigration;

class RenameActivityTemporaryItemTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
      public function change()
      {
      }
     */

    /**
     * Migrate Up.
     */
    public function up()
    {
        $this->execute("SET SESSION old_alter_table=1; RENAME TABLE `activityTemporaryItem` TO `activityTemporaryProduct` ;");
        $this->execute('SET SESSION old_alter_table=1; ALTER TABLE `activityTemporaryProduct` CHANGE `activityTemporaryItemID` `activityTemporaryProductID` INT( 11 ) NOT NULL ;');
        $this->execute("SET SESSION old_alter_table=1; ALTER TABLE `activityTemporaryProduct` CHANGE `temporaryItemId` `temporaryProductID` INT( 11 ) NOT NULL ;");
        $this->execute("SET SESSION old_alter_table=1; ALTER TABLE `activityTemporaryProduct` CHANGE `activityId` `activityId` INT( 11 ) NOT NULL ;");
        $this->execute("SET SESSION old_alter_table=1; ALTER TABLE `activityTemporaryProduct` DROP `activityTemporaryItemBatchCode` ,
DROP `activityTemporaryItemSerialCode` ,
DROP `activityTemporaryItemQuantity` ,
DROP `activityTemporaryItemDiscarded` ;");
        $this->execute("ALTER TABLE `activityTemporaryProduct` CHANGE `activityTemporaryProductID` `activityTemporaryProductID` INT( 11 ) NOT NULL AUTO_INCREMENT ;");
    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }

}
