<?php

use Phinx\Migration\AbstractMigration;

class AddNewTableCalledPurchaseRequisitionCategoryTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->hasTable('purchaseRequisitionCategory');

        if (!$table) {
            $table = $this->table('purchaseRequisitionCategory', ['id' => 'purchaseRequisitionCategoryId']);
            $table->addColumn('purchaseRequisitionCategoryName', 'string', ['limit' => 200]);
            $table->addColumn('purchaseRequisitionCategoryParentId', 'integer', ['default' => null, 'null' => true]);
            $table->addColumn('purchaseRequisitionCategoryStatus', 'integer', ['limit' => 20]);
            $table->addColumn('entityId', 'integer', ['limit' => 20]);
            $table->save();
        }
    }
}
