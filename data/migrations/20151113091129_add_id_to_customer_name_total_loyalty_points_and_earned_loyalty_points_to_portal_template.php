<?php

use Phinx\Migration\AbstractMigration;

class AddIdToCustomerNameTotalLoyaltyPointsAndEarnedLoyaltyPointsToPortalTemplate extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
    public function change()
    {
    }
    */

    /**
     * Migrate Up.
     */
    public function up()
    {
        $sql1 = 'update template SET templateContent = REPLACE(templateContent,
                    "<abbr contenteditable=\"false\" spellcheck=\"false\">[Customer Name]</abbr>",
                    "<abbr contenteditable=\"false\" spellcheck=\"false\" id=\"customer_name\">[Customer Name]</abbr>")
                    WHERE documentSizeID=3;';
        $this->execute($sql1);

        $sql2 = 'update template SET templateContent = REPLACE(templateContent,
                    "<abbr contenteditable=\"false\" spellcheck=\"false\">[Earned Loyalty Points]</abbr>",
                    "<abbr contenteditable=\"false\" spellcheck=\"false\" id=\"loyalty_points_earned\">[Earned Loyalty Points]</abbr>")
                    WHERE documentSizeID=3;';
        $this->execute($sql2);

        $sql3 = 'update template SET templateContent = REPLACE(templateContent,
                    "<abbr contenteditable=\"false\" spellcheck=\"false\">[Total Loyalty Points]</abbr>",
                    "<abbr contenteditable=\"false\" spellcheck=\"false\" id=\"loyalty_points_totoal\">[Earned Loyalty Points]</abbr>")
                    WHERE documentSizeID=3;';
                    $this->execute($sql3);
}

    /**
     * Migrate Down.
     */
    public function down()
    {

    }
}
