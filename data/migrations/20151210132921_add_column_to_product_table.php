<?php

use Phinx\Migration\AbstractMigration;

class AddColumnToProductTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
      public function change()
      {
      }
     */

    /**
     * Migrate Up.
     */
    public function up()
    {
        $table = $this->table('product');
        $hasColumn = $table->hasColumn('itemDetail');
        if (!$hasColumn) {
            $table->addColumn('itemDetail', 'text', array('after' => 'rackID', 'null' => 'allow'))
                    ->update();
        }
    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }

}
