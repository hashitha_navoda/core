<?php

use Phinx\Migration\AbstractMigration;

class AddAdvancedTemplateSettingsToTemplateTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */

    /**
     * Migrate Up.
     */
    public function up()
    {
        $this->execute("ALTER TABLE `template` ADD `templateHeader` LONGTEXT NULL DEFAULT NULL AFTER `templateOptions`, ADD `templateBody` LONGTEXT NULL DEFAULT NULL AFTER `templateHeader`, ADD `templateFooter` LONGTEXT NULL DEFAULT NULL AFTER `templateBody`, ADD `templateAdvancedOptions` LONGTEXT NULL DEFAULT NULL AFTER `templateFooter`, ADD `isAdvanceTemplateEnabled` TINYINT NOT NULL DEFAULT '0' AFTER `templateAdvancedOptions`;");
    }

    //isAdvanceTemplateEnabled

    /**
     * Migrate Down.
     */
    public function down()
    {
        $this->execute("ALTER TABLE `template` DROP `templateHeader`, DROP `templateBody`, DROP `templateFooter`, DROP `templateAdvancedOptions`, DROP `isAdvanceTemplateEnabled`;");
    }
}
