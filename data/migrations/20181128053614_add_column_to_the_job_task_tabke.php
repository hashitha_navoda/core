<?php

use Phinx\Migration\AbstractMigration;

class AddColumnToTheJobTaskTabke extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        // add new column to the jobTask
        $jobTaskTable = $this->table('jobTask');
        $jobTaskColumn = $jobTaskTable->hasColumn('jobTaskUsedQty');

        if (!$jobTaskColumn) {
            $jobTaskTable->addColumn('jobTaskUsedQty', 'decimal',['default' => 0, 'null' => true, 'precision' => 20, 'scale' => 5])
                ->update();
        }

        // add new column to the jobProductTable
        $jobProductTable = $this->table('jobProduct');
        $jobProductColumn = $jobProductTable->hasColumn('jobTaskID');

        if (!$jobProductColumn) {
            $jobProductTable->addColumn('jobTaskID', 'integer',['default' => null, 'null' => true])
                ->update();
        }
    }
}
