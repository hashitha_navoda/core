<?php

use Phinx\Migration\AbstractMigration;

class AddAutoIncreamentForCostTypeTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
      public function change()
      {
      }
     */

    /**
     * Migrate Up.
     */
    public function up()
    {
        $this->execute("SET SESSION old_alter_table=1;ALTER TABLE `activityCostType` CHANGE `activityCostType` `activityCostTypeID` INT( 11 ) NOT NULL AUTO_INCREMENT ;");
        $this->execute("SET SESSION old_alter_table=1;ALTER TABLE `activityTemporaryContractor` CHANGE `temporaryContractorID` `contractorID` INT( 11 ) NOT NULL ;");
    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }

}
