<?php

use Phinx\Migration\AbstractMigration;

class UpdateGetTemporaryProductsActionFeatureTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
      public function change()
      {
      }
     */

    /**
     * Migrate Up.
     */
    public function up()
    {
        $this->execute("UPDATE feature SET `featureController` = 'JobCard\\\Controller\\\API\\\TemporaryProduct' WHERE feature.`featureAction` = 'getTemporaryProducts' AND feature.`moduleID`=8;");
    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }

}
