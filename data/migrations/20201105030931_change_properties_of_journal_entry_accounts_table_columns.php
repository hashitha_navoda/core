<?php

use Phinx\Migration\AbstractMigration;

class ChangePropertiesOfJournalEntryAccountsTableColumns extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->table('journalEntryAccounts');
        $column1 = $table->hasColumn('journalEntryAccountsDebitAmount');
        $column2 = $table->hasColumn('journalEntryAccountsCreditAmount');
        if ($column1) {
            $table->changeColumn('journalEntryAccountsDebitAmount', 'decimal', ['precision' => 20, 'scale' => 10, 'default' => null, 'null' => true])
              ->save();
        }

        if ($column2) {
            $table->changeColumn('journalEntryAccountsCreditAmount', 'decimal', ['precision' => 20, 'scale' => 10, 'default' => null, 'null' => true])
              ->save();
        }
    }
}
