<?php

use Phinx\Migration\AbstractMigration;

class CreateNewTableCalledRateCardServicesSubTasks extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $rateCardTaskSubTasks = $this->hasTable('rateCardTaskSubTasks');

        if (!$rateCardTaskSubTasks) {
            $table = $this->table('rateCardTaskSubTasks', ['id' => 'rateCardTaskSubTaskId']);
            $table->addColumn('rateCardTaskId', 'integer', ['limit' => 11]);
            $table->addColumn('subTaskId', 'integer', ['limit' => 11]);
            $table->addColumn('uomId', 'integer', ['limit' => 11]);
            $table->addColumn('rateCardTaskSubTaskRate', 'decimal', ['default' => null, 'null' => true, 'precision' => 20, 'scale' => 5]);
            $table->addColumn('isDeleted', 'boolean', ['default' => false, 'null' => true]);
            $table->save();
        }
    }
}
