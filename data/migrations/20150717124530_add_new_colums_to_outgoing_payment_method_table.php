<?php

use Phinx\Migration\AbstractMigration;

class AddNewColumsToOutgoingPaymentMethodTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
      public function change()
      {
      }
     */

    /**
     * Migrate Up.
     */
    public function up()
    {
        $this->execute("ALTER TABLE `outGoingPaymentMethodsNumbers` ADD `outGoingPaymentMethodBankTransferBankId` INT( 11 ) NULL AFTER `outGoingPaymentMethodCardID` ,
ADD `outGoingPaymentMethodBankTransferAccountId` INT( 11 ) NULL AFTER `outGoingPaymentMethodBankTransferBankId` ,
ADD `outGoingPaymentMethodBankTransferSupplierBankName` varchar( 20 ) NULL AFTER `outGoingPaymentMethodBankTransferAccountId` ,
ADD `outGoingPaymentMethodBankTransferSupplierAccountNumber` INT( 11 ) NULL AFTER `outGoingPaymentMethodBankTransferSupplierBankName` ;");
    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }

}
