<?php

use Phinx\Migration\AbstractMigration;

class AddNewTablesForManageDraftGrn extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {

        $draftGrn = $this->hasTable('draftGrn');
        if (!$draftGrn) {
            $newTable = $this->table('draftGrn', ['id' => 'grnID']);
            $newTable->addColumn('grnCode', 'string', ['limit' => 200])
                ->addColumn('grnSupplierID', 'integer', ['limit' => 20])
                ->addColumn('grnSupplierReference', 'string', ['limit' => 200, 'default' => null, 'null' => true])
                ->addColumn('grnRetrieveLocation', 'integer', ['limit' => 20, 'default' => null, 'null' => true])
                ->addColumn('grnDate', 'date', ['default' => null, 'null' => true])
                ->addColumn('grnDeliveryCharge', 'decimal', ['precision' => 20, 'scale' => 5, 'default' => null, 'null' => true])
                ->addColumn('grnTotal', 'decimal', ['precision' => 20, 'scale' => 5, 'default' => null, 'null' => true])
                ->addColumn('grnComment', 'text', ['default' => null, 'null' => true])
                ->addColumn('grnUploadFlag', 'boolean', ['default' => 0, 'null' => true])
                ->addColumn('grnShowTax', 'boolean', ['default' => 0, 'null' => true])
                ->addColumn('grnActGrnData', 'text', ['default' => null, 'null' => true])
                ->addColumn('status', 'integer', ['limit' => 20])
                ->addColumn('entityID', 'integer', ['limit' => 20])
                ->addColumn('grnHashValue', 'string', ['limit' => 200,'default' => null, 'null' => true])
                ->addColumn('isGrnApproved', 'boolean', ['default' => 0, 'null' => true])
                ->save();
        }


        $draftGrnProduct = $this->hasTable('draftGrnProduct');
        if (!$draftGrnProduct) {
            $newTable = $this->table('draftGrnProduct', ['id' => 'grnProductID']);
            $newTable->addColumn('grnID', 'integer', ['limit' => 20])
                ->addColumn('locationProductID', 'integer', ['limit' => 20])
                ->addColumn('productBatchID', 'integer', ['limit' => 20, 'default' => null, 'null' => true])
                ->addColumn('productSerialID', 'integer', ['limit' => 20, 'default' => null, 'null' => true])
                ->addColumn('grnProductPrice', 'decimal', ['precision' => 25, 'scale' => 10])
                ->addColumn('grnProductDiscountType', 'string', ['default' => 'percentage', 'limit' => 255])
                ->addColumn('grnProductDiscount', 'float', ['default' => 0, 'null' => true])
                ->addColumn('grnProductTotalQty', 'decimal', ['precision' => 25, 'scale' => 10])
                ->addColumn('grnProductQuantity', 'decimal', ['precision' => 25, 'scale' => 10])
                ->addColumn('grnProductUom', 'integer', ['limit' => 20])
                ->addColumn('grnProductTotal', 'decimal', ['precision' => 25, 'scale' => 10])
                ->addColumn('rackID', 'integer', ['limit' => 20, 'default' => null, 'null' => true])
                ->addColumn('copied', 'boolean', ['default' => 0, 'null' => true])
                ->addColumn('grnProductCopiedQuantity', 'integer', ['limit' => 20, 'default' => null, 'null' => true])
                ->addColumn('grnProductTotalCopiedQuantity', 'integer', ['limit' => 20, 'default' => null, 'null' => true])
                ->addColumn('grnProductDocumentId', 'integer', ['limit' => 20, 'default' => null, 'null' => true])
                ->addColumn('grnProductDocumentTypeId', 'integer', ['limit' => 20, 'default' => null, 'null' => true])
                ->addColumn('grnProductDoumentTypeCopiedQuantity', 'decimal', ['precision' => 25, 'scale' => 10])
                ->save();
        }


        $draftProductBatch = $this->hasTable('draftProductBatch');
        if (!$draftProductBatch) {
            $newTable = $this->table('draftProductBatch', ['id' => 'productBatchID']);
            $newTable->addColumn('locationProductID', 'integer', ['limit' => 20])
                ->addColumn('productBatchCode', 'string', ['limit' => 200])
                ->addColumn('productBatchExpiryDate', 'date', ['default' => null, 'null' => true])
                ->addColumn('productBatchWarrantyPeriod', 'integer', ['limit' => 20, 'default' => null, 'null' => true])
                ->addColumn('productBatchManufactureDate', 'date', ['default' => null, 'null' => true])
                ->addColumn('productBatchQuantity', 'decimal', ['precision' => 25, 'scale' => 10])
                ->addColumn('productBatchPrice', 'decimal', ['precision' => 25, 'scale' => 10])
                ->save();
        }

        $draftProductSerial = $this->hasTable('draftProductSerial');
        if (!$draftProductSerial) {
            $newTable = $this->table('draftProductSerial', ['id' => 'productSerialID']);
            $newTable->addColumn('locationProductID', 'integer', ['limit' => 20])
                ->addColumn('productBatchID', 'integer', ['limit' => 20])
                ->addColumn('productSerialCode', 'string', ['limit' => 200])
                ->addColumn('productSerialWarrantyPeriod', 'integer', ['limit' => 20, 'default' => null, 'null' => true])
                ->addColumn('productSerialExpireDate', 'date', ['default' => null, 'null' => true])
                ->addColumn('productSerialSold', 'boolean', ['default' => 0, 'null' => true])
                ->addColumn('productSerialReturned', 'boolean', ['default' => 0, 'null' => true])
                ->save();
        }


        $draftGrnProductTax = $this->hasTable('draftGrnProductTax');
        if (!$draftGrnProductTax) {
            $newTable = $this->table('draftGrnProductTax', ['id' => 'grnProductTaxID']);
            $newTable->addColumn('grnID', 'integer', ['limit' => 20])
                ->addColumn('grnProductID', 'integer', ['limit' => 20])
                ->addColumn('grnTaxID', 'integer', ['limit' => 20])
                ->addColumn('grnTaxPrecentage', 'float', ['default' => 0, 'null' => true])
                ->addColumn('grnTaxAmount', 'decimal', ['precision' => 25, 'scale' => 10])
                ->save();
        }

    }
}
