<?php

use Phinx\Migration\AbstractMigration;

class CreateNewTableCalledContractor extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
      public function change()
      {
      }
     */

    /**
     * Migrate Up.
     */
    public function up()
    {
        $this->dropTable('contractor');
        $this->execute("CREATE TABLE IF NOT EXISTS `contractor` (
                            `contractorID` int(11) NOT NULL AUTO_INCREMENT,
                            `contractorFirstName` VARCHAR(100) NOT NULL,
                            `contractorSecondName` VARCHAR(100) NOT NULL,
                            `contractorDesignation` VARCHAR(100) NULL,
                            `divisionID` INT(11) NULL,
                            `contractorTP` VARCHAR(100) NULL,
                             PRIMARY KEY (`contractorID`)
                    ) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;");
    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }

}
