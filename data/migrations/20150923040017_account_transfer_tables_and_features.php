<?php

use Phinx\Migration\AbstractMigration;

class AccountTransferTablesAndFeatures extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
    public function change()
    {
    }
    */
    
    /**
     * Migrate Up.
     */
    public function up()
    {
        //add currencyId column to account table
        $this->execute("ALTER TABLE `account`  ADD `currencyId` INT NULL AFTER `accountTypeId`;");
        
        //get company currencyId
        $displaySetup = $this->fetchRow('SELECT * FROM displaySetup');
        
        //add currencyId for existing accounts
        $accounts = $this->fetchAll('SELECT accountId FROM account');
        foreach ($accounts as $account) {
            $this->execute("UPDATE `account` SET `currencyId` = '".$displaySetup['currencyID']."' WHERE `account`.`accountId` = ".$account[0].";");
        }
        
        //change account amount type length
        $this->execute("ALTER TABLE `account` CHANGE `accountBalance` `accountBalance` DECIMAL(20,5) NOT NULL DEFAULT '0.00';");
        
        //create accountWithdrawal table
        $this->execute("CREATE TABLE IF NOT EXISTS `accountWithdrawal` (
            `accountWithdrawalId` int(11) NOT NULL AUTO_INCREMENT,
            `accountWithdrawalAccountId` int(11) DEFAULT NULL,
            `accountWithdrawalAmount` decimal(20,5) NOT NULL DEFAULT '0.00000',
            `accountWithdrawalDate` date DEFAULT NULL,
            `accountWithdrawalComment` text,
            `entityId` int(11) DEFAULT NULL,
            PRIMARY KEY (`accountWithdrawalId`)
          ) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;");
        
        //create accountDeposit table
        $this->execute("CREATE TABLE IF NOT EXISTS `accountDeposit` (
            `accountDepositId` int(11) NOT NULL AUTO_INCREMENT,
            `accountDepositAccountId` int(11) DEFAULT NULL,
            `accountDepositAmount` decimal(20,5) NOT NULL DEFAULT '0.00000',
            `accountDepositDate` date DEFAULT NULL,
            `accountDepositComment` text,
            `entityId` int(11) DEFAULT NULL,
            PRIMARY KEY (`accountDepositId`)
          ) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;");
        
        //create accountTransfer table
        $this->execute("CREATE TABLE IF NOT EXISTS `accountTransfer` (
            `accountTransferId` int(11) NOT NULL AUTO_INCREMENT,
            `accountTransferIssuedAccountId` int(11) DEFAULT NULL,
            `accountTransferReceivedAccountId` int(11) DEFAULT NULL,
            `accountTransferAmount` decimal(20,5) NOT NULL DEFAULT '0.00000',
            `accountTransferIssuedAccountCurrencyRate` decimal(20,5) DEFAULT NULL,
            `accountTransferReceivedAccountCurrencyRate` decimal(20,5) DEFAULT NULL,
            `accountTransferDate` date DEFAULT NULL,
            `accountTransferComment` text,
            `entityId` int(11) DEFAULT NULL,
            PRIMARY KEY (`accountTransferId`)
          ) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;");
        
        //create cashInHandWithdrawal table
        $this->execute("CREATE TABLE IF NOT EXISTS `cashInHandWithdrawal` (
            `cashInHandWithdrawalId` int(11) NOT NULL AUTO_INCREMENT,
            `cashInHandWithdrawalAmount` decimal(20,5) NOT NULL DEFAULT '0.00000',
            `cashInHandWithdrawalDate` date DEFAULT NULL,
            `cashInHandWithdrawalComment` text,
            `locationId` int(11) DEFAULT NULL,
            `entityId` int(11) DEFAULT NULL,
            PRIMARY KEY (`cashInHandWithdrawalId`)
          ) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;");
        
        //create cashInHandDeposit table
        $this->execute("CREATE TABLE IF NOT EXISTS `cashInHandDeposit` (
            `cashInHandDepositId` int(11) NOT NULL AUTO_INCREMENT,
            `cashInHandDepositAmount` decimal(20,5) NOT NULL DEFAULT '0.00000',
            `cashInHandDepositDate` date DEFAULT NULL,
            `cashInHandDepositComment` text,
            `locationId` int(11) DEFAULT NULL,
            `entityId` int(11) DEFAULT NULL,
            PRIMARY KEY (`cashInHandDepositId`)
          ) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;");
        
        //role list
        $roles = $this->fetchAll("SELECT roleID FROM `role`");
        
        //feature list
        $featureList = array(
            array('name' =>'Create','controller'=>'Expenses\\\Controller\\\Transaction','action'=>'index','category'=>'Transaction','type'=>'0','moduleId'=>'9'),
            array('name' =>'Default','controller'=>'Expenses\\\Controller\\\API\\\Transaction','action'=>'create','category'=>'Transaction','type'=>'1','moduleId'=>'9'),
            array('name' =>'Default','controller'=>'Expenses\\\Controller\\\API\\\AccountAPI','action'=>'getAccountCurrency','category'=>'Account','type'=>'1','moduleId'=>'9')
        );
        
        foreach ($featureList as $feature){
            //insert feature to feature table
            $this->execute("INSERT INTO `feature` (`featureID`, `featureName`, `featureController`, `featureAction`, `featureCategory`, `featureType`, `moduleID`) VALUES (NULL,'".$feature['name']."', '".$feature['controller']."', '".$feature['action']."', '".$feature['category']."', '".$feature['type']."', '".$feature['moduleId']."');");
            $id = $this->fetchRow("select LAST_INSERT_ID()")[0];

            foreach ($roles as $role) {
                $roleID = $role['roleID'];
                if ($roleID == 1) {
                    $enabled = 1;
                } else {
                    //for enable api calls for every role
                    if($feature['type'] === '1'){
                        $enabled = 1;
                    } else {
                        $enabled = 0;
                    }
                }
                $this->execute("INSERT INTO `roleFeature` (`roleFeatureID`, `roleID`, `featureID`, `roleFeatureEnabled`) VALUES (NULL, $roleID, $id,$enabled);");
            }
        }
        
    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }
}