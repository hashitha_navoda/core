<?php

use Phinx\Migration\AbstractMigration;

class CreateTableCallJobCost extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        // create new table call jobCost
        $jobTaskCostTable = $this->hasTable('jobCost');
        if (!$jobTaskCostTable) {
            $newTable = $this->table('jobCost', ['id' => 'jobCostID']);
            $newTable->addColumn('jobID', 'integer', ['default' => null, 'null' => true])
                ->save();
        }

        // add new column to the jobTaskCostTable
        $jobTaskTable = $this->table('jobTaskCost');
        $jobTaskColumn = $jobTaskTable->hasColumn('jobCostID');

        if (!$jobTaskColumn) {
            $jobTaskTable->addColumn('jobCostID', 'integer',['default' => null, 'null' => true])
                ->update();
        }

        // add new column to the jobProductCostTable
        $jobProductCostTable = $this->table('jobProductCost');
        $jobProductCostColumn = $jobProductCostTable->hasColumn('jobCostID');

        if (!$jobProductCostColumn) {
            $jobProductCostTable->addColumn('jobCostID', 'integer',['default' => null, 'null' => true])
                ->update();
        }

        // add new column to the otherJobCostTable
        $otherJobCostTable = $this->table('otherJobCost');
        $otherJobCostColumn = $otherJobCostTable->hasColumn('jobCostID');

        if (!$otherJobCostColumn) {
            $otherJobCostTable->addColumn('jobCostID', 'integer',['default' => null, 'null' => true])
                ->update();
        }
    }
}
