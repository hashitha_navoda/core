<?php

use Phinx\Migration\AbstractMigration;

class AddJobCardQuotationReportFeatures extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
      public function change()
      {
      }
     */

    /**
     * Migrate Up.
     */
    public function up()
    {
        $this->execute("INSERT INTO `feature` (`featureID`, `featureName`, `featureController`, `featureAction`, `featureCategory`, `featureType`, `moduleID`) "
                . "VALUES (NULL, 'View Report', 'Reporting\\\Controller\\\Generate\\\JobCardReport', 'viewJobCardQuotation', 'Job Card Quotation Report', '0', '5');");
        $id3 = $this->fetchRow("select LAST_INSERT_ID()")[0];
        $this->execute("INSERT INTO `roleFeature` (`roleFeatureID`, `roleID`, `featureID`, `roleFeatureEnabled`) VALUES (NULL, '1', $id3, '1');");

        $this->execute("INSERT INTO `feature` (`featureID`, `featureName`, `featureController`, `featureAction`, `featureCategory`, `featureType`, `moduleID`) "
                . "VALUES (NULL, 'Generate PDF', 'Reporting\\\Controller\\\Generate\\\JobCardReport', 'generateJobCardQuotationPdf', 'Job Card Quotation Report', '0', '5');");
        $id3 = $this->fetchRow("select LAST_INSERT_ID()")[0];
        $this->execute("INSERT INTO `roleFeature` (`roleFeatureID`, `roleID`, `featureID`, `roleFeatureEnabled`) VALUES (NULL, '1', $id3, '1');");

        $this->execute("INSERT INTO `feature` (`featureID`, `featureName`, `featureController`, `featureAction`, `featureCategory`, `featureType`, `moduleID`) "
                . "VALUES (NULL, 'Generate CSV', 'Reporting\\\Controller\\\Generate\\\JobCardReport', 'generateJobCardQuotationCsv', 'Job Card Quotation Report', '0', '5');");
        $id3 = $this->fetchRow("select LAST_INSERT_ID()")[0];
        $this->execute("INSERT INTO `roleFeature` (`roleFeatureID`, `roleID`, `featureID`, `roleFeatureEnabled`) VALUES (NULL, '1', $id3, '1');");
    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }

}
