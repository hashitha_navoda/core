<?php

use Phinx\Migration\AbstractMigration;

class AddTablesCallItemAttributeMap extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $existsAttrMapTable = $this->hasTable('itemAttributeMap');
        if (!$existsAttrMapTable) {
            $table = $this->table('itemAttributeMap',array('id' => 'itemAttributeMapID'));
            $table->addColumn('itemAttributeID', 'integer')
                ->addColumn('productID', 'integer')
                ->save();
        }

        $existsAttrValueMapTable = $this->hasTable('itemAttributeValueMap');
        if (!$existsAttrValueMapTable) {
            $valueTable = $this->table('itemAttributeValueMap',array('id' => 'itemAttributeValueMapID'));
            $valueTable->addColumn('itemAttributeMapID', 'integer')
                ->addColumn('itemAttributeValueID', 'integer')
                ->save();
        }
    }
}
