<?php

use Phinx\Migration\AbstractMigration;

class AddNewColumnToTheSalesOrderTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
      public function change()
      {
      }
     */

    /**
     * Migrate Up.
     */
    public function change()
    {
        $table = $this->table('salesInvoice');
        $existDiscountColumn = $table->hasColumn('salesInvoiceWiseTotalDiscount');
        $existDiscountTypeColumn = $table->hasColumn('salesInvoiceTotalDiscountType');
        if (!$existDiscountColumn && !$existDiscountTypeColumn) {
            $table->addColumn('salesInvoiceWiseTotalDiscount', 'float', array('after' => 'salesInvoiceTotalDiscount', 'null' => 'allow'))
                    ->addColumn('salesInvoiceTotalDiscountType', 'string', array('after' => 'salesInvoiceDeliveryCharge', 'null' => 'allow'))
                    ->update();
        }
    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }

}
