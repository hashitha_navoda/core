<?php

use Phinx\Migration\AbstractMigration;

class AddNewColoumnCalledPackageIdInComapnyTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->table('displaySetup');
        $column1 = $table->hasColumn('packageID');

        if (!$column1) {
            $table->addColumn('packageID', 'integer', ['default' => NULL, 'null' => true])
              ->update();
        }

        $column2 = $table->hasColumn('jobModuleEnabled');

        if (!$column2) {
            $table->addColumn('jobModuleEnabled', 'boolean', ['default' => NULL, 'null' => true])
              ->update();
        }

        $column3 = $table->hasColumn('crmModuleEnabled');

        if (!$column3) {
            $table->addColumn('crmModuleEnabled', 'boolean', ['default' => NULL, 'null' => true])
              ->update();
        }
    }
}
