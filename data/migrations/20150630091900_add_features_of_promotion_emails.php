<?php

use Phinx\Migration\AbstractMigration;

class AddFeaturesOfPromotionEmails extends AbstractMigration
{

    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     */
    public function up()
    {
        $queries = array(
            array('query' => "INSERT INTO `feature` (`featureID`, `featureName`, `featureController`, `featureAction`, `featureCategory`, `featureType`, `moduleID`) VALUES (NULL, 'Send Emails', 'Settings\\\Controller\\\Promotion', 'emails', 'Promotions', '0', '7');", 'type' => '0'),
            array('query' => "INSERT INTO `feature` (`featureID`, `featureName`, `featureController`, `featureAction`, `featureCategory`, `featureType`, `moduleID`) VALUES (NULL, 'View Emails', 'Settings\\\Controller\\\Promotion', 'viewEmails', 'Promotions', '0', '7');", 'type' => '0'),
            array('query' => "INSERT INTO `feature` (`featureID`, `featureName`, `featureController`, `featureAction`, `featureCategory`, `featureType`, `moduleID`) VALUES (NULL, 'Default', 'Settings\\\Controller\\\API\\\Promotion', 'savePromotionEmail', 'Application', '1', '7');", 'type' => '1'),
            array('query' => "INSERT INTO `feature` (`featureID`, `featureName`, `featureController`, `featureAction`, `featureCategory`, `featureType`, `moduleID`) VALUES (NULL, 'Default', 'Settings\\\Controller\\\API\\\Promotion', 'getEmailPromotion', 'Application', '1', '7');", 'type' => '1'),
            array('query' => "INSERT INTO `feature` (`featureID`, `featureName`, `featureController`, `featureAction`, `featureCategory`, `featureType`, `moduleID`) VALUES (NULL, 'Default', 'Inventory\\\Controller\\\ProductAPI', 'getProductsForLocations', 'Application', '1', '2');", 'type' => '1'),
            array('query' => "INSERT INTO `feature` (`featureID`, `featureName`, `featureController`, `featureAction`, `featureCategory`, `featureType`, `moduleID`) VALUES (NULL, 'Default', 'Invoice\\\Controller\\\API\\\Invoice', 'getCustomersForPurchasedProducts', 'Application', '1', '3');", 'type' => '1'),
        );
        $rows = $this->fetchAll("SELECT roleID FROM `role`");

        foreach ($queries as $query) {
            $this->execute($query['query']);
            $id = $this->fetchRow("select LAST_INSERT_ID()")[0];
            foreach ($rows as $row) {
                $roleID = $row['roleID'];
                if ($roleID == 1) {
                    $enabled = 1;
                } else {
                    if ($query['type'] == '0') {
                        $enabled = 0;
                    } else {
                        $enabled = 1;
                    }
                }
                $this->execute("INSERT INTO `roleFeature` (`roleFeatureID`, `roleID`, `featureID`, `roleFeatureEnabled`) VALUES (NULL, $roleID, $id,$enabled);");
            }
        }
    }

}
