<?php

use Phinx\Migration\AbstractMigration;

class CreateTableExpiryAlertNotification extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
      public function change()
      {
      }
     */

    /**
     * Migrate Up.
     */
    public function up()
    {
        $tableExist = $this->hasTable('expiryAlertNotification');
        if (!$tableExist) {
            $table = $this->table('expiryAlertNotification', array('id' => 'expiryAlertNotificationID'));
            $table->addColumn('productID', 'integer', array('null' => 'allow'))
                    ->addColumn('NumberOfDays', 'integer', array('null' => 'allow'))
                    ->save();
        }
    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }

}
