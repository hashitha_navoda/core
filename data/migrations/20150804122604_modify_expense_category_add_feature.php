<?php

use Phinx\Migration\AbstractMigration;

class ModifyExpenseCategoryAddFeature extends AbstractMigration
{

    public function up()
    {
        $this->execute("UPDATE `feature` SET `featureAction`= 'add' WHERE `featureController`='Expenses\\\Controller\\\API\\\ExpenseCategory' AND `featureAction`='save';");
    }

}
