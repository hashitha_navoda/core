<?php

use Phinx\Migration\AbstractMigration;

class AddNewColumnsToEmployeeTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        //EmployeeCode
        $table = $this->table('employee');
        $column = $table->hasColumn('employeeCode');
        if (!$column) {
            $table->addColumn('employeeCode', 'string', ['limit' => 20, 'after' => 'employeeID', 'default' => null, 'null' => false])
              ->update();
        }

        //EmployeeAddress
        $table = $this->table('employee');
        $column = $table->hasColumn('employeeAddress');
        if (!$column) {
            $table->addColumn('employeeAddress', 'text', ['after' => 'employeeSecondName', 'default' => null, 'null' => false])
              ->update();
        }

        //EmployeeIDNo
        $table = $this->table('employee');
        $column = $table->hasColumn('EmployeeIDNo');
        if (!$column) {
            $table->addColumn('employeeIDNo', 'string', ['limit' => 100,'after' => 'employeeTP', 'default' => null, 'null' => false])
              ->update();
        }

        //EmployeeIDNo
        $table = $this->table('employee');
        $column = $table->hasColumn('departmentID');
        if (!$column) {
            $table->addColumn('departmentID', 'integer', ['after' => 'designationID', 'default' => null, 'null' => false])
              ->update();
        }
    }
}
