<?php

use Phinx\Migration\AbstractMigration;

class AddNewTableCalledProject extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
      public function change()
      {
      }
     */

    /**
     * Migrate Up.
     */
    public function up()
    {
        $this->execute("CREATE TABLE IF NOT EXISTS `project` (
  `projectId` INT NOT NULL,
  `projectName` VARCHAR(200) NULL,
  `projectType` TINYINT NULL,
  `projectDescription` LONGTEXT NULL,
  `projectEstimatedTimeDuration` DECIMAL(10,2) NULL,
  `projectStartingAt` DATETIME NULL,
  `projectEndAt` DATETIME NULL,
  `projectEstimatedCost` DECIMAL(10,2) NULL,
  `locationId` INT NULL,
  `projectProgress` DECIMAL(3,2) NULL,
  PRIMARY KEY (`projectId`));");

        $this->execute("INSERT INTO `feature` (`featureID`, `featureName`, `featureController`, `featureAction`, `featureCategory`, `featureType`, `moduleID`) VALUES (NULL, 'Create', 'JobCard\\\Controller\\\JobType', 'create', 'Job Type', '0', '8');");
        $id = $this->fetchRow("select LAST_INSERT_ID()")[0];
        $this->execute("INSERT INTO `roleFeature` (`roleFeatureID` ,`roleID` ,`featureID` ,`roleFeatureEnabled`)
                                VALUES (NULL , '1', $id, '1');");
    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }

}
