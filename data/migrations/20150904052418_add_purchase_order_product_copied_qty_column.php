<?php

use Phinx\Migration\AbstractMigration;

class AddPurchaseOrderProductCopiedQtyColumn extends AbstractMigration
{

    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     */
    public function up()
    {
        $this->execute("SET SESSION old_alter_table=1; ALTER TABLE `purchaseOrderProduct` ADD `purchaseOrderProductCopiedQuantity` DECIMAL( 20, 5 ) NULL ;");
    }

}
