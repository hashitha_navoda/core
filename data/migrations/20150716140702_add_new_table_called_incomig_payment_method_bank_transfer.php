<?php

use Phinx\Migration\AbstractMigration;

class AddNewTableCalledIncomigPaymentMethodBankTransfer extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
      public function change()
      {
      }
     */

    /**
     * Migrate Up.
     */
    public function up()
    {
        $this->execute("CREATE TABLE IF NOT EXISTS `incomingPaymentMethodBankTransfer` (
  `incomingPaymentMethodBankTransferId` int(11) NOT NULL AUTO_INCREMENT,
  `incomingPaymentMethodBankTransferBankId` int(11) NOT NULL,
  `incomingPaymentMethodBankTransferAccountId` int(11) NOT NULL,
  `incomingPaymentMethodBankTransferCustomerBankName` varchar(20) DEFAULT NULL,
  `incomingPaymentMethodBankTransferCustomerAccountNumber` int(15) DEFAULT NULL,
  PRIMARY KEY (`incomingPaymentMethodBankTransferId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;
");
    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }

}
