<?php

use Phinx\Migration\AbstractMigration;

class SetDefaultValuesForSomeOptionalFieldsInCompanyTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->table('company');

        $columnTaxRegNumber = $table->hasColumn('taxRegNumber');
        if ($columnTaxRegNumber) {
            $table->changeColumn('taxRegNumber', 'string', ['limit' => 100, 'default' => null, 'null' => true]);
        }

	$columnOldTaxRegNumber = $table->hasColumn('oldTaxRegNumber');
        if ($columnOldTaxRegNumber) {
            $table->changeColumn('oldTaxRegNumber', 'string', ['limit' => 100, 'default' => null, 'null' => true]);
        }

	$columnUserGracePeriod = $table->hasColumn('userGracePeriod');
        if ($columnUserGracePeriod) {
            $table->changeColumn('userGracePeriod', 'integer', ['limit' => 5, 'default' => 0]);
        }

	$columnCompanyGracePeriod = $table->hasColumn('companyGracePeriod');
        if ($columnCompanyGracePeriod) {
            $table->changeColumn('companyGracePeriod', 'integer', ['limit' => 5, 'default' => 0]);
        }

        $columnCompanyAccountType = $table->hasColumn('companyAccountType');
        if ($columnCompanyAccountType) {
            $table->changeColumn('companyAccountType', 'boolean', ['default' => true]);
        }

        $table->save();
    }
}
