<?php

use Phinx\Migration\AbstractMigration;

class AddNewTemplateToIncome extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $sql = <<<EOF
                INSERT INTO `template` (`templateName`, `documentTypeID`, `documentSizeID`, `templateContent`, `templateFooterDetails`, `templateFooterShow`, `templateDefault`, `templateSample`) VALUES
('Template', 43, 1, '        <table class="table table-bordered" width="100%">
<tbody>
<tr>
<td><img class="logo" style="background: #e0e0e0; width: 75px; height: 75px; display: block; text-align: center;" src="[Company Logo]" alt="Company Logo" /><span style="font-weight: bold;"><abbr contenteditable="false" spellcheck="false">[Company Name]</abbr> </span><br /><abbr contenteditable="false" spellcheck="false">[Company Address]</abbr> <br />Tel: <abbr contenteditable="false" spellcheck="false">[Company Tel. No]</abbr> <br />Email: <abbr contenteditable="false" spellcheck="false">[Company Email]</abbr> <br />Tax No: <abbr contenteditable="false" spellcheck="false">[Company Tax Reg. No]</abbr>&nbsp;</td>
<td>
<h2 align="right">Income</h2>
</td>
</tr>
<tr>
<td>
<div class="add_block"><hr /></div>
</td>
<td class="relative_data" rowspan="2" align="right">
<table class="table table-bordered table-condensed" width="100%">
<tbody>
<tr>
<td>Income Code:</td>
<td style="text-align: right;"><abbr contenteditable="false" spellcheck="false">[Income Code]</abbr>&nbsp;&nbsp;</td>
</tr>
<tr>
<td style="vertical-align: top;">Issue Date:</td>
<td style="vertical-align: top; text-align: right;"><abbr contenteditable="false" spellcheck="false">[Income Date]</abbr>&nbsp;&nbsp;</td>
</tr>
</tbody>
</table>
</td>
</tr>
<tr>
<td>
<div class="add_block">
<div id="cust_name">&nbsp;</div>
<div><abbr contenteditable="false" spellcheck="false">[Customer Name]</abbr>&nbsp;</div>
<div><abbr contenteditable="false" spellcheck="false">[Customer Address]</abbr>&nbsp;&nbsp;<br />&nbsp;</div>
</div>
</td>
</tr>
</tbody>
</table>
<noneditabletable>
<table class="noneditable table table-bordered" style="width: 100%; margin-bottom: 100px;" title="" contenteditable="false" data-original-title="">
<tbody>
<tr>
<td colspan="2"><hr />
<div class="qout_body">
<table id="item_tbl" class="table bigtable table-bordered table-striped" width="100%"><colgroup><col width="5px" /><col width="20px" /><col width="20px" /><col width="10px" /><col width="10px" /><col width="20px" /></colgroup>
<thead>
<tr><th>&nbsp;</th><th>Item Code.</th><th>ItemName</th><th>Qty</th><th>Sub Product</th><th>Price</th><th>Total</th></tr>
</thead>
<tbody>
<tr>
<td>1</td>
<td align="">PR_10</td>
<td align="">Item1</td>
<td align="">1</td>
<td align="">
<ul>
<li>Batch
<ul>
<li>1</li>
</ul>
</li>
</ul>
</td>
<td align="">1,000.00</td>
<td align="">1,000.00</td>
</tr>
</tbody>
</table>
<br /><br /><br />
<div class="summary">
<table id="" class="table" width="100%"><colgroup><col width="90px" /><col width="290px" /><col width="75px" /><col width="100px" /><col width="50px" /><col width="100px" /></colgroup>
<tbody>
<tr>
<td colspan="2" rowspan="4">
<div id="comment"><strong>Memo&nbsp;:</strong>
<p><em>Sample memo text for payment</em></p>
</div>
</td>
<td colspan="3">Sub Total</td>
<td class="text-right">1,230.00</td>
</tr>
<tr>
<td colspan="3">
<div>Credit Payments</div>
</td>
<td class="text-right">0.00</td>
</tr>
<tr>
<td colspan="3">
<div>Discount</div>
</td>
<td class="text-right">( 0.00 )</td>
</tr>
<tr>
<td colspan="3">
<h3>Total</h3>
</td>
<td class="text-right">
<h3>1,230.00</h3>
</td>
</tr>
</tbody>
</table>
</div>
</div>
</td>
</tr>
</tbody>
</table>
</noneditabletable><footer class="doc">
<table class="table table-bordered" width="100%">
<tbody>
<tr>
<td><abbr contenteditable="false" spellcheck="false">[Company Name]</abbr>&nbsp;</td>
<td>&nbsp;</td>
<td style="text-align: right;"><abbr contenteditable="false" spellcheck="false">[Today Date]</abbr>&nbsp;</td>
</tr>
</tbody>
</table>
</footer>', '', 1, 1, 0);
EOF;
        $this->execute($sql);

    }
}
