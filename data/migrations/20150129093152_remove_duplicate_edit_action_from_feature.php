<?php

use Phinx\Migration\AbstractMigration;

class RemoveDuplicateEditActionFromFeature extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
      public function change()
      {
      }
     */

    /**
     * Migrate Up.
     */
    public function up()
    {
        $this->execute("DELETE FROM `log` WHERE `featureID` = 84");
        $this->execute("DELETE FROM `feature` WHERE `featureName` = 'Default' AND `featureController` = 'User\\\Controller\\\User' AND `featureAction` = 'edit' AND `featureCategory` = 'Application' AND `featureType` = 1  AND `moduleID` = 4");
    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }

}
