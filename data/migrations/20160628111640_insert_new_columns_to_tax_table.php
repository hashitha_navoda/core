<?php

use Phinx\Migration\AbstractMigration;

class InsertNewColumnsToTaxTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $this->execute("SET SESSION old_alter_table=1; ALTER TABLE `tax` ADD `taxSalesAccountID` INT(11) NULL AFTER `taxSuspendable`; ");
        $this->execute("SET SESSION old_alter_table=1; ALTER TABLE `tax` ADD `taxPurchaseAccountID` INT(11) NULL AFTER `taxSalesAccountID`; ");
    }
}
