<?php

use Phinx\Migration\AbstractMigration;

class InsertSmsType extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $pdo = $this->getAdapter()->getConnection();

        $dataArray = array(
            array(
                'smsTypeID' => 1,
                'smsTypeName' => 'After Invoice',
                'smsTypeMessage' => null,
                'isIncludeInvoiceDetails' => 0,
                'smsSendType' => 1,
                'isEnable' => 0,
            ),
            array(
                'smsTypeID' => 2,
                'smsTypeName' => 'After Receipt',
                'smsTypeMessage' => null,
                'isIncludeInvoiceDetails' => 0,
                'smsSendType' => 1,
                'isEnable' => 0, 
            ),
            array(
                'smsTypeID' => 3,
                'smsTypeName' => 'Due Balance',
                'smsTypeMessage' => null,
                'isIncludeInvoiceDetails' => 0,
                'smsSendType' => 1,
                'isEnable' => 0, 
            ),
            array(
                'smsTypeID' => 4,
                'smsTypeName' => 'Due Invoice',
                'smsTypeMessage' => null,
                'isIncludeInvoiceDetails' => 0,
                'smsSendType' => 1,
                'isEnable' => 0, 
            ),
            array(
                'smsTypeID' => 5,
                'smsTypeName' => 'Service Reminder',
                'smsTypeMessage' => null,
                'isIncludeInvoiceDetails' => 0,
                'smsSendType' => 1,
                'isEnable' => 0, 
            ),
            array(
                'smsTypeID' => 6,
                'smsTypeName' => 'Post Dated Cheque',
                'smsTypeMessage' => null,
                'isIncludeInvoiceDetails' => 0,
                'smsSendType' => 1,
                'isEnable' => 0, 
            ),

        );

        foreach($dataArray as $type){

            $widgetTypeInsert = <<<SQL
                INSERT INTO `smsType` 
                ( 
                    `smsTypeID`, 
                    `smsTypeName`,
                    `smsTypeMessage`,
                    `isIncludeInvoiceDetails`, 
                    `smsSendType`,
                    `isEnable`
                ) VALUES (
                    :smsTypeID, 
                    :smsTypeName,
                    :smsTypeMessage,
                    :isIncludeInvoiceDetails, 
                    :smsSendType,
                    :isEnable
                );
    
SQL;
    
            $pdo->prepare($widgetTypeInsert)->execute(array( 
                'smsTypeID' => $type['smsTypeID'], 
                'smsTypeName' => $type['smsTypeName'],
                'smsTypeMessage' => $type['smsTypeMessage'], 
                'isIncludeInvoiceDetails' => $type['isIncludeInvoiceDetails'],
                'smsSendType' => $type['smsSendType'], 
                'isEnable' => $type['isEnable']
            ));

        }
    }
}
