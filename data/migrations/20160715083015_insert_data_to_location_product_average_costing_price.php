<?php

use Phinx\Migration\AbstractMigration;

class InsertDataToLocationProductAverageCostingPrice extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $locationProduct = $this->fetchAll("SELECT * FROM `locationProduct`");

        foreach ($locationProduct as $key => $value) {
            $locationPrTotalQty = 0;
            $locationPrTotalPrice = 0;
            $lastItemInID = 0;
            $locationProductID = $value['locationProductID'];            
            $itemInData = $this->fetchAll("SELECT * FROM `itemIn` WHERE itemInLocationProductID = {$locationProductID};");
            foreach ($itemInData as $inkey => $inData) {
                $remainQty = $inData['itemInQty'] - $inData['itemInSoldQty'];
                if($remainQty > 0){
                    $itemInPrice = $remainQty*$inData['itemInPrice'];
                    $itemInDiscount = $remainQty*$inData['itemInDiscount'];
                    $locationPrTotalQty += $remainQty;
                    $locationPrTotalPrice += $itemInPrice - $itemInDiscount; 
                }
                if($inData['itemInID'] > $lastItemInID ){
                    $lastItemInID = $inData['itemInID'];
                }
            }
            if($locationPrTotalQty > 0){
                $averageCost = $locationPrTotalPrice/$locationPrTotalQty;
                $this->execute("UPDATE `locationProduct` SET `locationProductAverageCostingPrice` = {$averageCost}, `locationProductLastItemInID` = {$lastItemInID} WHERE `locationProductID` = {$locationProductID};");
            }
        }
    }
}
