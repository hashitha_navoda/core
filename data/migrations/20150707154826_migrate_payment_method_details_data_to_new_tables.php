<?php

use Phinx\Migration\AbstractMigration;

class MigratePaymentMethodDetailsDataToNewTables extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
      public function change()
      {
      }
     */

    /**
     * Migrate Up.
     */
    public function up()
    {
        $payments = $this->fetchAll('SELECT * FROM incomingPayment');
        foreach ($payments as $payment) {
            if ($payment['incomingPaymentType'] == 'invoice') {
                $paymentID = $payment['incomingPaymentID'];
                $totalCreditAmount = 0.00;
                $cashAmount = 0.00;
                $creditCardAmount = 0.00;
                $checqueAmount = 0.00;

                $invoicePayments = $this->fetchAll("SELECT * FROM `incomingInvoicePayment` WHERE `incomingPaymentID` = $paymentID");
                foreach ($invoicePayments as $invoicePayment) {
                    $invoiceCashAmount = $invoicePayment['incomingInvoiceCashAmount'];
                    $invoiceCreditAmount = $invoicePayment['incomingInvoiceCreditAmount'];
                    $invoicePaymentAmount = $invoiceCashAmount + $invoiceCreditAmount;
                    $incomingInvoicePaymentID = $invoicePayment['incomingInvoicePaymentID'];
                    $totalCreditAmount += $invoiceCreditAmount;

                    $this->execute("UPDATE `incomingInvoicePayment` SET `incomingInvoicePaymentAmount` = $invoicePaymentAmount WHERE `incomingInvoicePayment`.`incomingInvoicePaymentID` =$incomingInvoicePaymentID;");


                    if ($invoicePayment['paymentMethodID'] == 1) {
                        $cashAmount+=$invoicePayment['incomingInvoiceCashAmount'];
                    } else if ($invoicePayment['paymentMethodID'] == 3) {
                        $creditCardAmount+=$invoicePayment['incomingInvoiceCashAmount'];
                    } else if ($invoicePayment['paymentMethodID'] == 2) {
                        $checqueAmount+=$invoicePayment['incomingInvoiceCashAmount'];
                    }
                }

                $paymentMethodNumbers = $this->fetchAll("SELECT * FROM `paymentMethodsNumbers` WHERE `paymentID` = $paymentID");
                foreach ($paymentMethodNumbers as $paymentMethodNumber) {
                    if ($paymentMethodNumber['paymentMethodID'] == 2) {
                        $checqueNumber = $paymentMethodNumber['paymentMethodReferenceNumber'];
                        $bankName = $paymentMethodNumber['paymentMethodBank'];
                        $this->execute("INSERT INTO `incomingPaymentMethodCheque` (`incomingPaymentMethodChequeId`, `incomingPaymentMethodChequeNumber`, `incomingPaymentMethodChequeBankName`) VALUES (NULL, '$checqueNumber', '$bankName');");
                        $checqueID = $this->fetchRow("select LAST_INSERT_ID()")[0];
                        $this->execute("INSERT INTO `incomingPaymentMethod` (`incomingPaymentMethodId`, `incomingPaymentId`, `incomingPaymentMethodAmount`, `incomingPaymentMethodChequeId`) VALUES (NULL, $paymentID, $checqueAmount,$checqueID);");
                    } else if ($paymentMethodNumber['paymentMethodID'] == 3) {
                        $creditCardReferenceNumber = $paymentMethodNumber['paymentMethodReferenceNumber'];
                        $creditCardNumber = $paymentMethodNumber['paymentMethodCardID'];
                        $this->execute("INSERT INTO `incomingPaymentMethodCreditCard` (`incomingPaymentMethodCreditCardId`, `incomingPaymentMethodCreditCardNumber`, `incomingPaymentMethodCreditReceiptNumber`) VALUES (NULL, '$creditCardNumber', '" . addslashes($creditCardReferenceNumber) . "');");
                        $creditCardID = $this->fetchRow("select LAST_INSERT_ID()")[0];
                        $this->execute("INSERT INTO `incomingPaymentMethod` (`incomingPaymentMethodId`, `incomingPaymentId`, `incomingPaymentMethodAmount`, `incomingPaymentMethodCreditCardId`) VALUES (NULL, $paymentID, $creditCardAmount,$creditCardID);");
                    }
                }

                if ($cashAmount != 0.00) {
                    $this->execute("INSERT INTO `incomingPaymentMethodCash` (`incomingPaymentMethodCashId`) VALUES (NULL)");
                    $cashID = $this->fetchRow("select LAST_INSERT_ID()")[0];
                    $this->execute("INSERT INTO `incomingPaymentMethod` (`incomingPaymentMethodId`, `incomingPaymentId`, `incomingPaymentMethodAmount`, `incomingPaymentMethodCashId`) VALUES (NULL, $paymentID, $cashAmount,$cashID)");
                }
                $this->execute("UPDATE `incomingPayment` SET `incomingPaymentCreditAmount` = $totalCreditAmount WHERE `incomingPayment`.`incomingPaymentID` = $paymentID;");
            }
        }
    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }

}
