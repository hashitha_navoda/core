<?php

use Phinx\Migration\AbstractMigration;

class CreateNewTableCalledTemporaryContractorAndAddedFeatures extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
      public function change()
      {
      }
     */

    /**
     * Migrate Up.
     */
    public function up()
    {
        $this->execute("CREATE TABLE IF NOT EXISTS  `temporaryContractor` (
                            `temporaryContractorID` INT( 11 ) NOT NULL AUTO_INCREMENT ,
                            `temporaryContractorFirstName` VARCHAR( 100 ) NOT NULL ,
                            `temporaryContractorSecondName` VARCHAR( 100 ) NOT NULL ,
                            `temporaryContractorDesignation` VARCHAR( 100 ) NULL ,
                            `divisionID` INT( 11 ) NULL ,
                            `temporaryContractorTP` VARCHAR( 100 ) NULL ,
                           PRIMARY KEY (  `temporaryContractorID` )
                           ) ENGINE = INNODB DEFAULT CHARSET = latin1 AUTO_INCREMENT =1");

        $this->execute("INSERT INTO `feature` (`featureID` ,`featureName` ,`featureController` ,`featureAction` ,`featureCategory` ,`featureType` ,`moduleID`)
                        VALUES (NULL, 'Default', 'JobCard\\\Controller\\\API\\\Contractor', 'saveTemporaryContractor', 'Contractor', '1', '8');");
        $id1 = $this->fetchRow("select LAST_INSERT_ID()")[0];
        $this->execute("INSERT INTO `roleFeature` (`roleFeatureID` ,`roleID` ,`featureID` ,`roleFeatureEnabled`)
                                VALUES (NULL , '1', $id1, '1');");

        $this->execute("INSERT INTO `feature` (`featureID` ,`featureName` ,`featureController` ,`featureAction` ,`featureCategory` ,`featureType` ,`moduleID`)
                        VALUES (NULL, 'Default', 'Core\\\Controller\\\JavascriptVariable', 'temporaryContractors', 'Application', '1', '1');");
        $id2 = $this->fetchRow("select LAST_INSERT_ID()")[0];
        $this->execute("INSERT INTO `roleFeature` (`roleFeatureID` ,`roleID` ,`featureID` ,`roleFeatureEnabled`)
                                VALUES (NULL , '1', $id2, '1');");

        $this->execute("INSERT INTO `feature` (`featureID` ,`featureName` ,`featureController` ,`featureAction` ,`featureCategory` ,`featureType` ,`moduleID`)
                        VALUES (NULL, 'Default', 'Core\\\Controller\\\JavascriptVariable', 'rawMaterialProducts', 'Application', '1', '1');");
        $id3 = $this->fetchRow("select LAST_INSERT_ID()")[0];
        $this->execute("INSERT INTO `roleFeature` (`roleFeatureID` ,`roleID` ,`featureID` ,`roleFeatureEnabled`)
                                VALUES (NULL , '1', $id3, '1');");

        $this->execute("INSERT INTO `feature` (`featureID` ,`featureName` ,`featureController` ,`featureAction` ,`featureCategory` ,`featureType` ,`moduleID`)
                        VALUES (NULL, 'Default', 'Core\\\Controller\\\JavascriptVariable', 'fixedAssetsProducts', 'Application', '1', '1');");
        $id4 = $this->fetchRow("select LAST_INSERT_ID()")[0];
        $this->execute("INSERT INTO `roleFeature` (`roleFeatureID` ,`roleID` ,`featureID` ,`roleFeatureEnabled`)
                                VALUES (NULL , '1', $id4, '1');");
    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }

}
