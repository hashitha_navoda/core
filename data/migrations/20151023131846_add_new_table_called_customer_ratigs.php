<?php

use Phinx\Migration\AbstractMigration;

class AddNewTableCalledCustomerRatigs extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
      public function change()
      {
      }
     */

    /**
     * Migrate Up.
     */
    public function up()
    {
        $sql = <<<SQL
            CREATE TABLE IF NOT EXISTS `customerRatings` (
  `customerRatingsId` int(11) NOT NULL AUTO_INCREMENT,
  `customerId` int(11) NOT NULL,
  `customerProfileId` int(11) DEFAULT '0',
  `ratingTypesId` int(11) NOT NULL,
  `customerRatingsRatingValue` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`customerRatingsId`),
  KEY `customerId` (`customerId`,`customerProfileId`,`ratingTypesId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;
SQL;
        $this->execute($sql);
    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }

}
