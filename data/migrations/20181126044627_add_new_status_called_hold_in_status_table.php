<?php

use Phinx\Migration\AbstractMigration;

class AddNewStatusCalledHoldInStatusTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $pdo = $this->getAdapter()->getConnection();
        $dataArray = array(
            array(
                'statusID' => '17',
                'statusName' => 'hold',
            ),
            array(
                'statusID' => '18',
                'statusName' => 'ended',
            ),
            array(
                'statusID' => '19',
                'statusName' => 'requested',
            ),

        );

        foreach($dataArray as $status){

            $statusInsert = <<<SQL
                INSERT IGNORE INTO `status` 
                ( 
                    `statusID`, 
                    `statusName`
                ) VALUES (
                    :statusID, 
                    :statusName
                );
    
SQL;
    
            $pdo->prepare($statusInsert)->execute(array( 
                'statusID' => $status['statusID'], 
                'statusName' => $status['statusName'] 
            ));

        }

    }
}
