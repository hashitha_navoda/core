<?php

use Phinx\Migration\AbstractMigration;

class InquiryStatusTypeContentChanges extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
      public function change()
      {
      }
     */

    /**
     * Migrate Up.
     */
    public function up()
    {
        $this->execute("SET SESSION old_alter_table=1; UPDATE `inquiryStatusType` SET `inquiryStatusTypeName` = 'At the Creation' WHERE `inquiryStatusType`.`inquiryStatusTypeID` = 1 ;");
        $this->execute("SET SESSION old_alter_table=1; UPDATE `inquiryStatusType` SET `inquiryStatusTypeName` = 'After One Minute' WHERE `inquiryStatusType`.`inquiryStatusTypeID` = 2 ;");
        $this->execute("SET SESSION old_alter_table=1; UPDATE `inquiryStatusType` SET `inquiryStatusTypeName` = 'After One hour' WHERE `inquiryStatusType`.`inquiryStatusTypeID` = 3 ;");
        $this->execute("SET SESSION old_alter_table=1; UPDATE `inquiryStatusType` SET `inquiryStatusTypeName` = 'After 24 Hours' WHERE `inquiryStatusType`.`inquiryStatusTypeID` =4 ;");
    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }

}
