<?php

use Phinx\Migration\AbstractMigration;

class AddNewThreeColumsCalledProductDefaultSellingPriceProductDefaultMinimumInventoryLevelProdutDefaultReorderLevelToProductTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
      public function change()
      {
      }
     */

    /**
     * Migrate Up.
     */
    public function up()
    {
        $this->execute("SET SESSION old_alter_table=1; ALTER TABLE `product` ADD `productDefaultSellingPrice` DECIMAL( 12, 2 ) NULL DEFAULT '0.00' AFTER `productDiscountValue` ,
ADD `productDefaultMinimumInventoryLevel` INT( 11 ) NULL DEFAULT '0' AFTER `productDefaultSellingPrice` ,
ADD `productDefaultReorderLevel` INT( 11 ) NULL DEFAULT '0' AFTER `productDefaultMinimumInventoryLevel`; ");
    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }

}
