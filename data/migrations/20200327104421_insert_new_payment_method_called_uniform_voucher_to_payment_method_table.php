<?php

use Phinx\Migration\AbstractMigration;

class InsertNewPaymentMethodCalledUniformVoucherToPaymentMethodTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $pdo = $this->getAdapter()->getConnection();

        $dataArray = array(
            array(
                'paymentMethodID' => 9,
                'paymentMethodName' => 'Uniform Voucher',
                'paymentMethodDescription' => 'Uniform Voucher',
                'paymentMethodSalesFinanceAccountID' => 0,
                'paymentMethodPurchaseFinanceAccountID' => 0,
                'paymentMethodInSales' => 0,
                'paymentMethodInPurchase' => 0

            ),

        );

        foreach($dataArray as $method){

            $paymentMethodInsert = <<<SQL
                INSERT INTO `paymentMethod` 
                ( 
                    `paymentMethodID`, 
                    `paymentMethodName`, 
                    `paymentMethodDescription`,
                    `paymentMethodSalesFinanceAccountID`,
                    `paymentMethodPurchaseFinanceAccountID`,
                    `paymentMethodInSales`,
                    `paymentMethodInPurchase`
                ) VALUES (
                    :paymentMethodID, 
                    :paymentMethodName,  
                    :paymentMethodDescription,
                    :paymentMethodSalesFinanceAccountID,
                    :paymentMethodPurchaseFinanceAccountID,
                    :paymentMethodInSales,
                    :paymentMethodInPurchase
                );
    
SQL;
    
            $pdo->prepare($paymentMethodInsert)->execute(array( 
                'paymentMethodID' => $method['paymentMethodID'], 
                'paymentMethodName' => $method['paymentMethodName'], 
                'paymentMethodDescription' => $method['paymentMethodDescription'], 
                'paymentMethodSalesFinanceAccountID' => $method['paymentMethodSalesFinanceAccountID'], 
                'paymentMethodPurchaseFinanceAccountID' => $method['paymentMethodPurchaseFinanceAccountID'], 
                'paymentMethodInSales' => $method['paymentMethodInSales'], 
                'paymentMethodInPurchase' => $method['paymentMethodInPurchase'] 
            ));

        }

    }
}
