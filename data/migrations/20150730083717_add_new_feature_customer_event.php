<?php

use Phinx\Migration\AbstractMigration;

class AddNewFeatureCustomerEvent extends AbstractMigration
{

    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     */
//    public function change()
//    {
//
//    }

    public function up()
    {
        //role list
        $roles = $this->fetchAll("SELECT roleID FROM `role`");

        //feature list
        $featureList = [
            [
                'name' => 'Create',
                'controller' => 'Settings\\\Controller\\\CustomerEvent', 'action' => 'index',
                'category' => 'Customer Event', 'type' => '0', 'moduleId' => '7'
            ],
            [
                'name' => 'Default',
                'controller' => 'Settings\\\Controller\\\API\\\CustomerEvent', 'action' => 'save',
                'category' => 'Application', 'type' => '1', 'moduleId' => '7'
            ],
            [
                'name' => 'Default',
                'controller' => 'Settings\\\Controller\\\API\\\CustomerEvent', 'action' => 'updateStatus',
                'category' => 'Application', 'type' => '1', 'moduleId' => '7'
            ],
            [
                'name' => 'Default',
                'controller' => 'Settings\\\Controller\\\API\\\CustomerEvent', 'action' => 'delete',
                'category' => 'Application', 'type' => '1', 'moduleId' => '7'
            ],
            [
                'name' => 'Default',
                'controller' => 'Settings\\\Controller\\\API\\\CustomerEvent', 'action' => 'edit',
                'category' => 'Application', 'type' => '1', 'moduleId' => '7'
            ],
            [
                'name' => 'Default',
                'controller' => 'Settings\\\Controller\\\API\\\CustomerEvent', 'action' => 'update',
                'category' => 'Application', 'type' => '1', 'moduleId' => '7'
            ],
            [
                'name' => 'Default',
                'controller' => 'Settings\\\Controller\\\API\\\CustomerEvent', 'action' => 'search',
                'category' => 'Application', 'type' => '1', 'moduleId' => '7'
            ],
            [
                'name' => 'Sales Customer View',
                'controller' => 'Invoice\\\Controller\\\Customer', 'action' => 'salesCustomerIndex',
                'category' => 'Customer', 'type' => '0', 'moduleId' => '3'
            ],
            [
                'name' => 'Sales Customer Create',
                'controller' => 'Invoice\\\Controller\\\Customer', 'action' => 'salesCustomerAdd',
                'category' => 'Customer', 'type' => '0', 'moduleId' => '3'
            ],
        ];

        foreach ($featureList as $feature) {
            //insert feature to feature table
            $this->execute("INSERT INTO `feature` (`featureID`, `featureName`, `featureController`, `featureAction`, `featureCategory`, `featureType`, `moduleID`) "
                    . "VALUES (NULL,'" . $feature['name'] . "', '" . $feature['controller'] . "', '" . $feature['action'] . "', '" . $feature['category'] . "', '" . $feature['type'] . "', '" . $feature['moduleId'] . "') ; ");
            $id = $this->fetchRow("select LAST_INSERT_ID()")[0];

            foreach ($roles as $role) {
                $roleID = $role['roleID'];
                if ($roleID == 1) {
                    $enabled = 1;
                } else {
                    //for enable api calls for every role
                    if ($feature['type'] === '1') {
                        $enabled = 1;
                    } else {
                        $enabled = 0;
                    }
                }
                $this->execute("INSERT INTO `roleFeature` (`roleFeatureID`, `roleID`, `featureID`, `roleFeatureEnabled`) VALUES (NULL, '$roleID', '$id','$enabled');");
            }
        }

//        Create customer event table.
        $this->execute("SET SESSION old_alter_table=1; CREATE TABLE IF NOT EXISTS `customerEvent` (
            `customerEventID` int(11) NOT NULL AUTO_INCREMENT,
            `customerEventName` varchar(100) NOT NULL,
            `customerEventDuration` int(11) NOT NULL,
            `customerEventStartDate` date NOT NULL,
            `customerEventEndDate` date NOT NULL,
            `customerEventStatus` tinyint(1) NOT NULL,
            `entityID` int(11) NOT NULL,
            PRIMARY KEY (`customerEventID`)
        ) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;");

//        add new column customerEvent in customer table
        $this->execute("SET SESSION old_alter_table=1; ALTER TABLE  `customer` ADD  `customerEvent` INT NULL AFTER  `customerCurrency` ; ");
    }

}
