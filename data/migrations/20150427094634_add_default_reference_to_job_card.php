<?php

use Phinx\Migration\AbstractMigration;

class AddDefaultReferenceToJobCard extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
      public function change()
      {
      }
     */

    /**
     * Migrate Up.
     */
    public function up()
    {
        $result = $this->fetchAll('SELECT * FROM `reference`');
        if ($result) {
            $jobCardProjectReference = $this->fetchRow("SELECT * FROM `reference` where `referenceNameID`='21'");
            if (!$jobCardProjectReference) {
                $this->execute("INSERT INTO `reference` (`referenceNameID`,`referenceName`, `referenceTypeID`) VALUES ('21','PRO','0');");
                $this->execute("INSERT INTO `referencePrefix` (`referencePrefixID`, `referenceNameID`, `locationID`, `referencePrefixCHAR`, `referencePrefixNumberOfDigits`, `referencePrefixCurrentReference`) VALUES (NULL, '21', NULL, 'PRO', '3', '1');");
            }
            $jobCardJobReference = $this->fetchRow("SELECT * FROM `reference` where `referenceNameID`='22'");
            if (!$jobCardJobReference) {
                $this->execute("INSERT INTO `reference`(`referenceNameID`,`referenceName`, `referenceTypeID`) VALUES ('22','JOB','0');");
                $this->execute("INSERT INTO `referencePrefix` (`referencePrefixID`, `referenceNameID`, `locationID`, `referencePrefixCHAR`, `referencePrefixNumberOfDigits`, `referencePrefixCurrentReference`) VALUES (NULL, '22', NULL, 'JOB', '3', '1');");
            }
            $jobCardActivityReference = $this->fetchRow("SELECT * FROM `reference` where `referenceNameID`='23'");
            if (!$jobCardActivityReference) {
                $this->execute("INSERT INTO `reference`(`referenceNameID`,`referenceName`, `referenceTypeID`) VALUES ('23','ACTY','0');");
                $this->execute("INSERT INTO `referencePrefix` (`referencePrefixID`, `referenceNameID`, `locationID`, `referencePrefixCHAR`, `referencePrefixNumberOfDigits`, `referencePrefixCurrentReference`) VALUES (NULL, '23', NULL, 'ACTY', '3', '1');");
            }
            $jobCardInquiryLogReference = $this->fetchRow("SELECT * FROM `reference` where `referenceNameID`='24'");
            if (!$jobCardInquiryLogReference) {
                $this->execute("INSERT INTO `reference`(`referenceNameID`,`referenceName`, `referenceTypeID`) VALUES ('24','INQLG','0');");
                $this->execute("INSERT INTO `referencePrefix` (`referencePrefixID`, `referenceNameID`, `locationID`, `referencePrefixCHAR`, `referencePrefixNumberOfDigits`, `referencePrefixCurrentReference`) VALUES (NULL, '24', NULL, 'INQLG', '3', '1');");
            }
        }
    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }

}
