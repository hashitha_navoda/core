<?php

use Phinx\Migration\AbstractMigration;

class UpdateFeatureTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
      public function change()
      {
      }
     */

    /**
     * Migrate Up.
     */
    public function up()
    {
        $this->execute("UPDATE `feature` SET `featureName`='View' WHERE `featureController`='Inventory\\\Controller\\\Product' AND `featureAction`='list';");
        $this->execute("UPDATE `feature` SET `featureCategory`='Items' WHERE `featureController`='Inventory\\\Controller\\\Product' AND `featureAction`='list';");
        $this->execute("UPDATE `feature` SET `featureCategory`='Users' WHERE `featureController`='User\\\Controller\\\User' AND `featureAction`='index';");
        $this->execute("UPDATE `feature` SET `featureName`='View' WHERE `featureController`='User\\\Controller\\\User' AND `featureAction`='index';");

        $this->execute("UPDATE `feature` SET `featureName`='View' WHERE `featureController`='Invoice\\\Controller\\\QuotationController' AND `featureAction`='list';");
        $this->execute("UPDATE `feature` SET `featureCategory`='Quotations' WHERE `featureController`='Invoice\\\Controller\\\QuotationController' AND `featureAction`='list';");

        $this->execute("UPDATE `feature` SET `featureName`='View' WHERE `featureController`='Invoice\\\Controller\\\SalesOrders' AND `featureAction`='list';");
        $this->execute("UPDATE `feature` SET `featureCategory`='Sales Orders' WHERE `featureController`='Invoice\\\Controller\\\SalesOrders' AND `featureAction`='list';");

        $this->execute("UPDATE `feature` SET `featureCategory`='Delivery Notes' WHERE `featureController`='Invoice\\\Controller\\\DeliveryNote' AND `featureAction`='view';");

        $this->execute("UPDATE `feature` SET `featureCategory`='Returns' WHERE `featureController`='Invoice\\\Controller\\\Return' AND `featureAction`='view';");

        $this->execute("UPDATE `feature` SET `featureCategory`='Customer Payments' WHERE `featureController`='Invoice\\\Controller\\\CustomerPayments' AND `featureAction`='view';");

        $this->execute("UPDATE `feature` SET `featureCategory`='Invoices' WHERE `featureController`='Invoice\\\Controller\\\Invoice' AND `featureAction`='view';");
        $this->execute("UPDATE `feature` SET `featureCategory`='Adjustments' WHERE `featureController`='Inventory\\\Controller\\\InventoryAdjustment' AND `featureAction`='view';");
        $this->execute("UPDATE `feature` SET `featureCategory`='Transfers' WHERE `featureController`='Inventory\\\Controller\\\Transfer' AND `featureAction`='list';");
        $this->execute("UPDATE `feature` SET `featureName`='View' WHERE `featureController`='Inventory\\\Controller\\\Transfer' AND `featureAction`='list';");
        $this->execute("UPDATE `feature` SET `featureName`='View' WHERE `featureController`='Inventory\\\Controller\\\PurchaseReturnsController' AND `featureAction`='list';");
        $this->execute("UPDATE `feature` SET `featureCategory`='PRs' WHERE `featureController`='Inventory\\\Controller\\\PurchaseReturnsController' AND `featureAction`='list';");
        $this->execute("UPDATE `feature` SET `featureCategory`='GRNs' WHERE `featureController`='Inventory\\\Controller\\\GrnController' AND `featureAction`='list';");
        $this->execute("UPDATE `feature` SET `featureName`='View' WHERE `featureController`='Inventory\\\Controller\\\GrnController' AND `featureAction`='list';");
        $this->execute("UPDATE `feature` SET `featureName`='View' WHERE `featureController`='Inventory\\\Controller\\\PurchaseInvoiceController' AND `featureAction`='list';");
        $this->execute("UPDATE `feature` SET `featureCategory`='Purchase Vouchers' WHERE `featureController`='Inventory\\\Controller\\\PurchaseInvoiceController' AND `featureAction`='list';");
        $this->execute("UPDATE `feature` SET `featureCategory`='Purchase Orders' WHERE `featureController`='Inventory\\\Controller\\\PurchaseOrderController' AND `featureAction`='list';");
        $this->execute("UPDATE `feature` SET `featureName`='View' WHERE `featureController`='Inventory\\\Controller\\\PurchaseOrderController' AND `featureAction`='list';");
        $this->execute("UPDATE `feature` SET `featureCategory`='Items' WHERE `featureController`='Inventory\\\Controller\\\Product' AND `featureAction`='editProductsByLocation';");
        $this->execute("UPDATE `feature` SET `featureName`='Edit By Location' WHERE `featureController`='Inventory\\\Controller\\\Product' AND `featureAction`='editProductsByLocation';");
    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }

}
