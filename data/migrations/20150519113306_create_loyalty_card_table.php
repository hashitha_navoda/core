<?php

use Phinx\Migration\AbstractMigration;

class CreateLoyaltyCardTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
      public function change()
      {
      }
     */

    /**
     * Migrate Up.
     */
    public function up()
    {
        $this->execute("CREATE TABLE `loyalty` (
  `loyaltyID` int(11) NOT NULL AUTO_INCREMENT,
  `loyaltyName` varchar(150) NOT NULL,
  `loyaltyPrefix` varchar(30) DEFAULT NULL,
  `loyaltyCodeDegits` int(10) DEFAULT NULL,
  `loyaltyCodeCurrentNo` int(10) NOT NULL,
  `loyaltyActiveMinVal` int(5) DEFAULT NULL,
  `loyaltyActiveMaxVal` int(5) DEFAULT NULL,
  `loyaltyEarningPerPoint` int(5) DEFAULT NULL,
  `loyaltyRedeemPerPoint` int(5) DEFAULT NULL,
  `loyaltyStatus` int(5) DEFAULT NULL,
    PRIMARY KEY (`loyaltyID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1");
    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }

}
