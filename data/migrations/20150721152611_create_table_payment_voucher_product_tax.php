<?php

use Phinx\Migration\AbstractMigration;

class CreateTablePaymentVoucherProductTax extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
      public function change()
      {
      }
     */

    /**
     * Migrate Up.
     */
    public function up()
    {
        $tableExists = $this->hasTable('paymentVoucherProductTax');
        if (!$tableExists) {
            $paymentVoucherProduct = $this->table('paymentVoucherProductTax', array('id' => 'paymentVoucherProductTaxID'));
            $paymentVoucherProduct->addColumn('paymentVoucherID', 'integer', array('limit' => 20))
                    ->addColumn('paymentVoucherProductIdOrNonItemProductID', 'integer', array('limit' => 20))
                    ->addColumn('paymentVoucherTaxID', 'integer', array('limit' => 20))
                    ->addColumn('paymentVoucherTaxPrecentage', 'float')
                    ->addColumn('paymentVoucherTaxAmount', 'decimal', array('precision' => 12, 'scale' => '2'))
                    ->addColumn('paymentVoucherTaxItemOrNonItemProduct', 'string', array('limit' => 50, 'null' => 'allow'))
                    ->save();
        }
    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }

}
