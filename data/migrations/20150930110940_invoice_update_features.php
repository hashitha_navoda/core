<?php

use Phinx\Migration\AbstractMigration;

class InvoiceUpdateFeatures extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
      public function change()
      {
      }
     */

    /**
     * Migrate Up.
     */
    public function up()
    {
        $this->execute("SET SESSION old_alter_table=1;INSERT INTO `status` (`statusID`, `statusName`) VALUES ('8', 'In Progress');");
        $this->execute("SET SESSION old_alter_table=1;INSERT INTO `status` (`statusID`, `statusName`) VALUES ('9', 'Completed');");
        $this->execute("SET SESSION old_alter_table=1;INSERT INTO `status` (`statusID`, `statusName`) VALUES ('10', 'Replace');");

        $rows = $this->fetchAll("SELECT roleID FROM `role`");
        $this->execute("INSERT INTO `feature` (`featureID`, `featureName`, `featureController`, `featureAction`, `featureCategory`, `featureType`, `moduleID`) VALUES (NULL, 'Default', 'Invoice\\\Controller\\\API\\\Invoice', 'updateInvoice', 'Application', '1', '3');");
        $id1 = $this->fetchRow("select LAST_INSERT_ID()")[0];
        foreach ($rows as $row) {
            $roleID = $row['roleID'];
            if ($roleID == 1) {
                $enabled = 1;
            } else {
                $enabled = 0;
            }
            $this->execute("INSERT INTO `roleFeature` (`roleFeatureID`, `roleID`, `featureID`, `roleFeatureEnabled`) VALUES (NULL, $roleID, $id1,$enabled);");
        }
    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }

}
