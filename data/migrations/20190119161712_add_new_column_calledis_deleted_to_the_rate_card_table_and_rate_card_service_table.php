<?php

use Phinx\Migration\AbstractMigration;

class AddNewColumnCalledisDeletedToTheRateCardTableAndRateCardServiceTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->table('rateCard');
        $table1 = $this->table('rateCardTask');
        $column = $table->hasColumn('isDeleted');
        $column1 = $table1->hasColumn('isDeleted');
        $column2 = $table1->hasColumn('rateCardStatus');

        if (!$column) {
            $table->addColumn('isDeleted', 'boolean', ['default' => false, 'null' => true])
                ->update();
        }

        if (!$column1) {
            $table1->addColumn('isDeleted', 'boolean', ['default' => false, 'null' => true])
                ->update();
        }

        if (!$column2) {
            $table->addColumn('rateCardStatus', 'boolean', ['default' => true, 'null' => true])
                ->update();
        }
    }
}
