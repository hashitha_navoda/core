<?php

use Phinx\Migration\AbstractMigration;

class AddNoneInventoryDailySalesReportFeatures extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
      public function change()
      {
      }
     */

    /**
     * Migrate Up.
     */
    public function up()
    {
        //role list
        $roles = $this->fetchAll("SELECT roleID FROM `role`");

        //feature list
        $featureList = array(
            array('name' => 'View Report', 'controller' => 'Reporting\\\Controller\\\DailySalesReport', 'action' => 'viewInvoicedDailySalesNoneInventoryItem', 'category' => 'Invoiced Daily Sales None Inventory Items', 'type' => '0', 'moduleId' => '3'),
            array('name' => 'Generate PDF', 'controller' => 'Reporting\\\Controller\\\DailySalesReport', 'action' => 'generateInvoicedDailySalesNoneInventoryItemPdf', 'category' => 'Invoiced Daily Sales None Inventory Items', 'type' => '0', 'moduleId' => '3'),
            array('name' => 'Generate CSV', 'controller' => 'Reporting\\\Controller\\\DailySalesReport', 'action' => 'generateInvoicedDailySalesNoneInventoryItemSheet', 'category' => 'Invoiced Daily Sales None Inventory Items', 'type' => '0', 'moduleId' => '3')
        );

        foreach ($featureList as $feature) {
            //insert feature to feature table
            $this->execute("INSERT INTO `feature` (`featureID`, `featureName`, `featureController`, `featureAction`, `featureCategory`, `featureType`, `moduleID`) VALUES (NULL,'" . $feature['name'] . "', '" . $feature['controller'] . "', '" . $feature['action'] . "', '" . $feature['category'] . "', '" . $feature['type'] . "', '" . $feature['moduleId'] . "');");
            $id = $this->fetchRow("select LAST_INSERT_ID()")[0];

            foreach ($roles as $role) {
                $roleID = $role['roleID'];
                if ($roleID == 1) {
                    $enabled = 1;
                } else {
                    //for enable api calls for every role
                    if ($feature['type'] === '1') {
                        $enabled = 1;
                    } else {
                        $enabled = 0;
                    }
                }
                $this->execute("INSERT INTO `roleFeature` (`roleFeatureID`, `roleID`, `featureID`, `roleFeatureEnabled`) VALUES (NULL, $roleID, $id,$enabled);");
            }
        }
    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }

}
