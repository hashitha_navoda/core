<?php

use Phinx\Migration\AbstractMigration;

class AddNewTableCalledIncomeNonItemProductTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->hasTable('incomeNonItemProduct');

        if (!$table) {
            $table = $this->table('incomeNonItemProduct', ['id' => 'incomeNonItemProductID']);
            $table->addColumn('incomeID', 'integer', ['limit' => 20,'default' => null, 'null' => true]);
            $table->addColumn('incomeNonItemProductDiscription', 'text', ['default' => null, 'null' => true]);
            $table->addColumn('incomeNonItemProductQuantity', 'integer', ['limit' => 20,'default' => null, 'null' => true]);
            $table->addColumn('incomeNonItemProductUnitPrice', 'decimal', ['precision' => 20, 'scale' => 5]);
            $table->addColumn('incomeNonItemProductTotalPrice', 'decimal', ['precision' => 20, 'scale' => 5]);
            $table->addColumn('incomeNonItemProductGLAccountID', 'integer', ['limit' => 20,'default' => null, 'null' => true]);
            $table->save();
        }
    }
}
