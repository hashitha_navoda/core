<?php

use Phinx\Migration\AbstractMigration;

class AddColumnsToVehicleTypeAndVehicleModelTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $vehicleTypeTable = $this->table('vehicleType');
        $column = $vehicleTypeTable->hasColumn('entityId');
        if (!$column) {
            $vehicleTypeTable->addColumn('entityId', 'integer',
                [
                    'after' => 'vehicleTypeName',
                    'null' => true,
                    'default' => NULL,
                ])
            ->update();
        }
        $column = $vehicleTypeTable->hasColumn('vehicleTypeStatus');
        if (!$column) {
            $vehicleTypeTable->addColumn('vehicleTypeStatus', 'integer',
                [
                    'length' => 2,
                    'after' => 'vehicleTypeName',
                    'null' => true,
                    'default' => NULL,
                ])
            ->update();
        }
        $vehicleModelTable = $this->table('vehicleModel');
        $column = $vehicleModelTable->hasColumn('vehicleTypeID');
        if (!$column) {
            $vehicleModelTable->addColumn('vehicleTypeID', 'integer',
                [
                    'after' => 'vehicleModelName',
                    'null' => true,
                    'default' => NULL,
                ])
            ->update();
        }
        $column = $vehicleModelTable->hasColumn('vehicleTypeDelete');
        if (!$column) {
            $vehicleModelTable->addColumn('vehicleTypeDelete', 'integer',
                [
                    'length' => 1,
                    'after' => 'vehicleTypeID',
                    'null' => true,
                    'default' => 0,
                ])
            ->update();
        }
    }
}
