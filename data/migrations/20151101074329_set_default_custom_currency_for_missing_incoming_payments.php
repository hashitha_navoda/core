<?php

use Phinx\Migration\AbstractMigration;

class SetDefaultCustomCurrencyForMissingIncomingPayments extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
    public function change()
    {
    }
    */
    
    /**
     * Migrate Up.
     */
    public function up()
    {
        //get company currency
        $displaySetup = $this->fetchRow('SELECT * FROM displaySetup');
        if($displaySetup['currencyID']){
            //get currency rate
            $currency = $this->fetchRow("SELECT * FROM `currency` WHERE `currencyID` = {$displaySetup['currencyID']}");
            //get incoming payments
            $incomingPayments = $this->fetchAll("SELECT * FROM `incomingPayment`;");
            foreach ($incomingPayments as $incomingPayment){
                if(is_null($incomingPayment['customCurrencyId'])){
                    $this->execute("UPDATE `incomingPayment` SET `customCurrencyId` = {$displaySetup['currencyID']}, `incomingPaymentCustomCurrencyRate` = {$currency['currencyRate']} WHERE `incomingPayment`.`incomingPaymentID` = {$incomingPayment['incomingPaymentID']};");
                }
            }
        }
    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }
}