<?php

use Phinx\Migration\AbstractMigration;

class AddNewFunctionsForTypeahead extends AbstractMigration
{

    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     */
//    public function change()
//    {
//
//    }

    public function up()
    {
        //role list
        $roles = $this->fetchAll("SELECT roleID FROM `role`");

        //feature list
        $featureList = [
            [
                'name' => 'Default',
                'controller' => 'JobCard\\\Controller\\\API\\\Job', 'action' => 'getJobsForTypeahead',
                'category' => 'Application', 'type' => '1', 'moduleId' => '8'
            ],
            [
                'name' => 'Default',
                'controller' => 'JobCard\\\Controller\\\API\\\Activity', 'action' => 'getActivitiesForTypeahead',
                'category' => 'Application', 'type' => '1', 'moduleId' => '8'
            ],
        ];

        foreach ($featureList as $feature) {
            //insert feature to feature table
            $this->execute("INSERT INTO `feature` (`featureID`, `featureName`, `featureController`, `featureAction`, `featureCategory`, `featureType`, `moduleID`) "
                    . "VALUES (NULL,'" . $feature['name'] . "', '" . $feature['controller'] . "', '" . $feature['action'] . "', '" . $feature['category'] . "', '" . $feature['type'] . "', '" . $feature['moduleId'] . "') ; ");
            $id = $this->fetchRow("select LAST_INSERT_ID()")[0];

            foreach ($roles as $role) {
                $roleID = $role['roleID'];
                if ($roleID == 1) {
                    $enabled = 1;
                } else {
                    //for enable api calls for every role
                    if ($feature['type'] === '1') {
                        $enabled = 1;
                    } else {
                        $enabled = 0;
                    }
                }
                $this->execute("INSERT INTO `roleFeature` (`roleFeatureID`, `roleID`, `featureID`, `roleFeatureEnabled`) VALUES (NULL, '$roleID', '$id','$enabled');");
            }
        }
    }

}
