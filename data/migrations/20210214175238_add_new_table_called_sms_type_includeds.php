<?php

use Phinx\Migration\AbstractMigration;

class AddNewTableCalledSmsTypeIncludeds extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $smsTypeIncluded = $this->hasTable('smsTypeIncluded');
        if (!$smsTypeIncluded) {
            $newTable = $this->table('smsTypeIncluded', ['id' => 'smsTypeIncludedId']);
            $newTable->addColumn('smsTypeID', 'integer', ['limit' => 20])
                    ->addColumn('smsIncludedId', 'integer', ['limit' => 20])
                    ->addColumn('isSelected', 'boolean', ['default' => false, 'null' => true])
                    ->save();
        }
    }
}
