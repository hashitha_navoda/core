<?php

use Phinx\Migration\AbstractMigration;

class CreateTablejobGeneralSettings extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $jobGeneralSettingsTable = $this->hasTable('jobGeneralSettings');
        if (!$jobGeneralSettingsTable) {
            $table = $this->table('jobGeneralSettings');
            $table = $this->table('jobGeneralSettings', ['id' => 'id']);
            $table->addColumn('maintain_project', 'boolean')
                ->addColumn('maintain_designation', 'boolean')
                ->addColumn('maintain_division', 'boolean')
                ->addColumn('raw_materials', 'boolean')
                ->addColumn('finished_goods', 'boolean')
                ->addColumn('fixed_assets', 'boolean')
                ->addColumn('vehicles', 'boolean')
                ->addColumn('maintain_contractors', 'boolean')
                ->addColumn('assign_for_jobs', 'boolean')
                ->addColumn('assign_for_tasks', 'boolean')
                ->save();
        }

        $jobGeneralSettingsTable = $this->hasTable('jobGeneralSettings');
        if ($jobGeneralSettingsTable) {
            $row = $this->fetchRow('SELECT * FROM jobGeneralSettings WHERE id = 1');
            if (!$row) {
                $pdo = $this->getAdapter()->getConnection();
                $settingsInsert = <<<SQL
                    INSERT INTO `jobGeneralSettings` 
                    ( 
                        `id`, 
                        `maintain_project`, 
                        `maintain_designation`, 
                        `maintain_division`, 
                        `raw_materials`, 
                        `finished_goods`, 
                        `fixed_assets`, 
                        `vehicles`, 
                        `maintain_contractors`, 
                        `assign_for_jobs`, 
                        `assign_for_tasks` 
                    ) VALUES (
                        1,
                        true, true, true, true, true,
                        true, true, true, true, true
                    );
SQL;
                $pdo->prepare($settingsInsert)->execute();
            }
        }
    }
}
