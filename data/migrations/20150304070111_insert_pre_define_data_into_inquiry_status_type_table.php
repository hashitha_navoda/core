<?php

use Phinx\Migration\AbstractMigration;

class InsertPreDefineDataIntoInquiryStatusTypeTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
      public function change()
      {
      }
     */

    /**
     * Migrate Up.
     */
    public function up()
    {
        $this->execute("INSERT INTO `inquiryStatusType` (`inquiryStatusTypeID`, `inquiryStatusTypeName`) VALUES (NULL, 'Every One hour'), (NULL, 'Every day'),(NULL, 'Every week'), (NULL, 'At the Creation'),(NULL, 'At the end');");
    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }

}
