<?php

use Phinx\Migration\AbstractMigration;

class CreateTableCallBom extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
      public function change()
      {
      }
     */

    /**
     * Migrate Up.
     */
    public function up()
    {
        $bomTableExit = $this->hasTable('bom');
        if (!$bomTableExit) {
            $bomTable = $this->table('bom', array('id' => 'bomID'));
            $bomTable->addColumn('bomCode', 'string', array('null' => 'allow'))
                    ->addColumn('productID', 'integer', array('null' => 'allow'))
                    ->addColumn('bomTotalCost', 'float', array('null' => 'allow'))
                    ->addColumn('bomDiscription', 'text', array('null' => 'allow'))
                    ->save();
        }
        $bomSubItemTableExit = $this->hasTable('bomSubItem');
        if (!$bomSubItemTableExit) {
            $bomSubItemTable = $this->table('bomSubItem', array('id' => 'bomSubItemID'));
            $bomSubItemTable->addColumn('bomID', 'integer')
                    ->addColumn('productID', 'integer')
                    ->addColumn('bomSubItemQuantity', 'float', array('null' => 'allow'))
                    ->addColumn('bomSubItemUnitPrice', 'float', array('null' => 'allow'))
                    ->addColumn('bomSubItemCost', 'float', array('null' => 'allow'))
                    ->save();
        }
    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }

}
