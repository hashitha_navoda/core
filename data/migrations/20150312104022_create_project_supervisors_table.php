<?php

use Phinx\Migration\AbstractMigration;

class CreateProjectSupervisorsTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
      public function change()
      {
      }
     */

    /**
     * Migrate Up.
     */
    public function up()
    {
        $this->execute("CREATE TABLE IF NOT EXISTS `projectSupervisors` (
  `projectSupervisorsId` int(11) NOT NULL,
  `projectId` int(11) DEFAULT NULL,
  `employeeId` int(11) DEFAULT NULL,
  PRIMARY KEY (`projectSupervisorsId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;");
    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }

}
