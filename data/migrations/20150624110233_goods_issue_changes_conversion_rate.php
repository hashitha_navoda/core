<?php

use Phinx\Migration\AbstractMigration;

class GoodsIssueChangesConversionRate extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
      public function change()
      {
      }
     */

    /**
     * Migrate Up.
     */
    public function up()
    {
        $this->execute("SET SESSION old_alter_table=1; ALTER TABLE `goodsIssueProduct` CHANGE `uomConversionRate` `uomConversionRate` DECIMAL( 12, 2 ) NOT NULL DEFAULT '1';");
        $this->execute("SET SESSION old_alter_table=1; UPDATE `goodsIssueProduct` SET `uomConversionRate` = 1 WHERE uomConversionRate = 0;");
        $this->execute("SET SESSION old_alter_table=1; ALTER TABLE `goodsIssueProduct` CHANGE `uomConversionRate` `uomConversionRate` DECIMAL( 12, 2 ) NULL DEFAULT '1.00';");
    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }

}
