<?php

use Phinx\Migration\AbstractMigration;

class AddNewTableCalledJobCardCost extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        // create new table call jobTaskCost
        $jobTaskCostTable = $this->hasTable('jobCardCost');
        if (!$jobTaskCostTable) {
            $newTable = $this->table('jobCardCost', ['id' => 'jobCardCostID']);
            $newTable->addColumn('jobCardID', 'integer', ['default' => null, 'null' => true])
                ->addColumn('unitCost', 'decimal',['default' => null, 'null' => true, 'precision' => 20, 'scale' => 5])
                ->addColumn('Qty', 'decimal',['default' => null, 'null' => true, 'precision' => 20, 'scale' => 5])
                ->addColumn('jobCostID', 'integer', ['default' => null, 'null' => true])
                ->addColumn('unitRate', 'decimal',['default' => null, 'null' => true, 'precision' => 20, 'scale' => 5])
                ->save();
        }
    }
}
