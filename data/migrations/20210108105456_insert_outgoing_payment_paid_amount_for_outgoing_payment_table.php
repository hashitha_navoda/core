<?php

use Phinx\Migration\AbstractMigration;

class InsertOutgoingPaymentPaidAmountForOutgoingPaymentTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $pdo = $this->getAdapter()->getConnection();
        $paymentData = $this->fetchAll("SELECT * FROM `outgoingPayment`");

        if (sizeof($paymentData)) {
            foreach ($paymentData as $key => $value) {
                $payID = $value['outgoingPaymentID'];

                $paymentMethodData = $this->fetchAll("SELECT * FROM `outGoingPaymentMethodsNumbers`  WHERE `outGoingPaymentID`= $payID");

                $totPaidAmount = 0;
                $balanceAmount = 0;
                $discount = 0;
                foreach ($paymentMethodData as $key1 => $value1) {
                    $totPaidAmount +=  (!empty($value1['outGoingPaymentMethodPaidAmount'])) ? floatval($value1['outGoingPaymentMethodPaidAmount']) : 0.00;

                }

                if (!empty($value['outgoingPaymentCreditAmount']) && floatval($value['outgoingPaymentCreditAmount']) > 0) {

                    $totPaidAmount += floatval($value['outgoingPaymentCreditAmount']);
                }

                if (!empty($value['outgoingPaymentDiscount']) && floatval($value['outgoingPaymentDiscount']) > 0) {

                    $discount = floatval($value['outgoingPaymentDiscount']);
                }
                
                $totPaidAmount -= $discount;
                $balanceAmount = $totPaidAmount - (floatval($value['outgoingPaymentAmount']) - floatval($discount));

                $this->execute("UPDATE `outgoingPayment` SET `outgoingPaymentPaidAmount` = $totPaidAmount, `outgoingPaymentBalanceAmount` = $balanceAmount  WHERE `outgoingPaymentID` = $payID"); 
            }
        }

    }
}
