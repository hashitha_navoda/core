<?php

use Phinx\Migration\AbstractMigration;

class AddNewTemplateCalledTransfer extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
      public function change()
      {
      }
     */

    /**
     * Migrate Up.
     */
    public function up()
    {
        $sql = <<<EOF
                INSERT INTO `template` (`templateID`, `templateName`, `documentTypeID`, `documentSizeID`, `templateContent`, `templateFooterDetails`, `templateFooterShow`, `templateDefault`, `templateSample`) VALUES
(NULL, 'Template', 15, 1,'<table class="table table-bordered" width="100%">
<tbody>
<tr>
<td><img class="logo" style="background: #e0e0e0; width: 75px; height: 75px; display: block; text-align: center;" src="[Company Logo]" alt="Company Logo" /><span style="font-weight: bold;"><abbr contenteditable="false" spellcheck="false">[Company Name]</abbr> </span><br /><abbr contenteditable="false" spellcheck="false">[Company Address]</abbr> <br />Tel: <abbr contenteditable="false" spellcheck="false">[Company Tel. No]</abbr> <br />Email: <abbr contenteditable="false" spellcheck="false">[Company Email]</abbr> <br />Tax No: <abbr contenteditable="false" spellcheck="false">[Company Tax Reg. No]</abbr>&nbsp;</td>
<td>
<h2 align="right">Transfer</h2>
</td>
</tr>
<tr>
<td>
<div class="add_block"><hr /></div>
</td>
<td class="relative_data" rowspan="2" align="right">
<table class="table table-bordered table-condensed" width="100%">
<tbody>
<tr>
<td>Transfer Date:</td>
<td style="text-align: right;"><abbr contenteditable="false" spellcheck="false"></abbr><abbr contenteditable="false" spellcheck="false">[Transfer Date]</abbr>&nbsp;</td>
</tr>
<tr>
<td>Transfer No:</td>
<td style="text-align: right;"><abbr contenteditable="false" spellcheck="false">[Transfer Code]</abbr>&nbsp;</td>
</tr>
<tr>
<td style="vertical-align: top;">&nbsp;</td>
<td style="vertical-align: top; text-align: right;"><br />&nbsp;</td>
</tr>
<tr>
<td>&nbsp;</td>
<td style="text-align: right;">&nbsp;</td>
</tr>
</tbody>
</table>
</td>
</tr>
<tr>
<td>
<div class="add_block">
<div id="cust_name"><strong>&nbsp;</strong></div>
</div>
</td>
</tr>
</tbody>
</table>
<noneditabletable>
<table class="noneditable table table-bordered" style="width: 100%; margin-bottom: 100px;" title="" contenteditable="false" data-original-title="">
<tbody>
<tr>
<td colspan="2">
<div class="qout_body"><hr />
<table id="item_tbl" class="table table-bordered table-striped" style="height: 92px;" width="679"><colgroup><col width="10px" /><col width="90px" /><col width="290px" /><col width="50px" /><col width="100px" /><col width="100px" /> </colgroup>
<thead>
<tr><th colspan="2">Product code</th><th colspan="1">Product</th><th colspan="1">Qty</th><th colspan="1">Price</th><th colspan="1">Total</th></tr>
</thead>
<tbody>
<tr>
<td>1</td>
<td align="left">12</td>
<td align="left">dell_lap<br /><small>(Disc. 1.00)</small></td>
<td align="left">1</td>
<td align="right">100.00</td>
<td align="right">99.00</td>
</tr>
</tbody>
</table>
<div class="summary"><br /> <br /> <br /> <br />
<table id="" class="table" width="100%">
<tbody>
<tr>
<td colspan="2" rowspan="3">&nbsp;</td>
<td colspan="3">Sub Total</td>
<td class="text-right">99.00</td>
</tr>
<tr>
<td colspan="3">
<h3>Total</h3>
</td>
<td class="text-right">
<h3>99.00</h3>
</td>
</tr>
<!--                                <tr>
                                                                <td colspan="6"><div></div></td>
                                                            </tr>--></tbody>
</table>
</div>
</div>
</td>
</tr>
</tbody>
</table>
</noneditabletable><footer class="doc">
<table class="table table-bordered" width="100%">
<tbody>
<tr>
<td><abbr contenteditable="false" spellcheck="false">[Company Name]</abbr>&nbsp;</td>
<td>&nbsp;</td>
<td style="text-align: right;"><abbr contenteditable="false" spellcheck="false">[Transfer Date]</abbr>&nbsp;</td>
</tr>
</tbody>
</table>
</footer>', '', 1, 1, 0);
EOF;
        $this->execute($sql);
    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }

}
