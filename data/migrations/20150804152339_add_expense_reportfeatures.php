<?php

use Phinx\Migration\AbstractMigration;

class AddExpenseReportfeatures extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
    public function change()
    {
    }
    */
    
    /**
     * Migrate Up.
     */
    public function up()
    {
        //role list
        $roles = $this->fetchAll("SELECT roleID FROM `role`");
        
        //reconciliation feature list
        $featureList = array(
            array('name' =>'Access Report Screen','controller'=>'Reporting\\\Controller\\\ReconciliationReport','action'=>'index','category'=>'Reconciliation Report','type'=>'0','moduleId'=>'5'),
            array('name' =>'View Report','controller'=>'Reporting\\\Controller\\\API\\\ReconciliationReport','action'=>'generateAccountReconciliation','category'=>'Reconciliation Report','type'=>'0','moduleId'=>'5'),
            array('name' =>'Generate PDF','controller'=>'Reporting\\\Controller\\\API\\\ReconciliationReport','action'=>'generateAccountReconciliationPdf','category'=>'Reconciliation Report','type'=>'0','moduleId'=>'5'),
            array('name' =>'Generate CSV','controller'=>'Reporting\\\Controller\\\API\\\ReconciliationReport','action'=>'generateAccountReconciliationCsv','category'=>'Reconciliation Report','type'=>'0','moduleId'=>'5'),
            array('name' =>'Access Report Screen','controller'=>'Reporting\\\Controller\\\ChequeReport','action'=>'index','category'=>'Cheque Report','type'=>'0','moduleId'=>'5'),
            array('name' =>'View Report','controller'=>'Reporting\\\Controller\\\API\\\ChequeReport','action'=>'generateChequeReport','category'=>'Cheque Report','type'=>'0','moduleId'=>'5'),
            array('name' =>'Generate PDF','controller'=>'Reporting\\\Controller\\\API\\\ChequeReport','action'=>'generateChequeReportPdf','category'=>'Cheque Report','type'=>'0','moduleId'=>'5'),
            array('name' =>'Generate CSV','controller'=>'Reporting\\\Controller\\\API\\\ChequeReport','action'=>'generateChequeReportCsv','category'=>'Cheque Report','type'=>'0','moduleId'=>'5'),
        );
        
        foreach ($featureList as $feature){            
            //insert feature to feature table
            $this->execute("INSERT INTO `feature` (`featureID`, `featureName`, `featureController`, `featureAction`, `featureCategory`, `featureType`, `moduleID`) VALUES (NULL,'".$feature['name']."', '".$feature['controller']."', '".$feature['action']."', '".$feature['category']."', '".$feature['type']."', '".$feature['moduleId']."');");
            $id = $this->fetchRow("select LAST_INSERT_ID()")[0];
            
            foreach ($roles as $role) {
                $roleID = $role['roleID'];
                if ($roleID == 1) {
                    $enabled = 1;
                } else {
                    //for enable api calls for every role
                    if($feature['type'] === '1'){
                        $enabled = 1;
                    } else {
                        $enabled = 0;
                    }
                }
                $this->execute("INSERT INTO `roleFeature` (`roleFeatureID`, `roleID`, `featureID`, `roleFeatureEnabled`) VALUES (NULL, $roleID, $id,$enabled);");
            }
        }
    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }
}