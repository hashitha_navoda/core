<?php

use Phinx\Migration\AbstractMigration;

class AddNewFeatureCustomerProfile extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
      public function change()
      {
      }
     */

    /**
     * Migrate Up.
     */
    public function up()
    {
        $createTableSql = "CREATE TABLE IF NOT EXISTS `customerProfile` (
                            `customerProfileID` int(11) NOT NULL AUTO_INCREMENT,
                            `customerProfileName` varchar(100) NOT NULL,
                            `customerID` int(11) NOT NULL,
                            `customerProfileMobileTP1` varchar(20) DEFAULT NULL,
                            `customerProfileMobileTP2` varchar(20) DEFAULT NULL,
                            `customerProfileLandTP1` varchar(20) DEFAULT NULL,
                            `customerProfileLandTP2` varchar(20) DEFAULT NULL,
                            `customerProfileFaxNo` varchar(20) DEFAULT NULL,
                            `customerProfileEmail` varchar(100) DEFAULT NULL,
                            `customerProfileContactPersonName` varchar(255) DEFAULT NULL,
                            `customerProfileContactPersonNumber` varchar(20) DEFAULT NULL,
                            `customerProfileLocationNo` varchar(10) DEFAULT NULL,
                            `customerProfileLocationRoadName1` varchar(255) DEFAULT NULL,
                            `customerProfileLocationRoadName2` varchar(255) DEFAULT NULL,
                            `customerProfileLocationRoadName3` varchar(255) DEFAULT NULL,
                            `customerProfileLocationSubTown` varchar(100) DEFAULT NULL,
                            `customerProfileLocationTown` varchar(100) DEFAULT NULL,
                            `customerProfileLocationPostalCode` varchar(10) DEFAULT NULL,
                            `customerProfileLocationCountry` varchar(100) DEFAULT NULL,
                            `isPrimary` tinyint(1) DEFAULT NULL,
                            PRIMARY KEY (`customerProfileID`),
                            KEY `customerID` (`customerID`)
                            ) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;";
        $this->execute($createTableSql);

        $addColumnsToCustomerSql = "SET SESSION old_alter_table=1;
                 ALTER TABLE `customer` ADD `customerCode` VARCHAR( 50 ) NOT NULL AFTER `customerName` ,
                 ADD `customerIdentityNumber` VARCHAR( 50 ) NULL AFTER `customerShortName` ,
                 ADD `customerCategory` VARCHAR( 100 ) NULL AFTER `customerIdentityNumber` ,
                 ADD `customerDateOfBirth` VARCHAR( 50 ) NULL AFTER `customerVatNumber` ,
                 ADD `customerDesignation` VARCHAR( 11 ) NULL AFTER `customerDateOfBirth` ,
                 ADD `customerDepartment` VARCHAR( 11 ) NULL AFTER `customerDesignation` ; ";

        $this->execute($addColumnsToCustomerSql);

        $insertCusCodeSql = "UPDATE `customer` SET `customerCode` = concat('CUS', lpad(`customerID`, 5, 0)) ;";
        $this->execute($insertCusCodeSql);

        $customersList = $this->fetchAll("SELECT * FROM `customer` ;");
        foreach ($customersList as $singleCustomer) {

            $addressArray = split(',', $singleCustomer['customerAddress']);
            $locNo = isset($addressArray[0]) ? addslashes($addressArray[0]) : '';
            $locRoadName1 = isset($addressArray[1]) ? addslashes($addressArray[1]) : '';
            $locRoadName2 = isset($addressArray[2]) ? addslashes($addressArray[2]) : '';
            $locRoadName3 = isset($addressArray[3]) ? addslashes($addressArray[3]) : '';
            $subTown = isset($addressArray[4]) ? addslashes($addressArray[4]) : '';
            $town = isset($addressArray[5]) ? addslashes($addressArray[5]) : '';
            $country = isset($addressArray[6]) ? addslashes($addressArray[6]) : '';
            $sql = <<<SQL
        INSERT INTO `customerProfile` (
                    `customerID` ,
                    `customerProfileMobileTP1` ,
                    `customerProfileEmail` ,
                    `customerProfileLocationNo` ,
                    `customerProfileLocationRoadName1` ,
                    `customerProfileLocationRoadName2` ,
                    `customerProfileLocationRoadName3` ,
                    `customerProfileLocationSubTown` ,
                    `customerProfileLocationTown` ,
                    `customerProfileLocationCountry` ,
                    `customerProfileName` ,
                    `isPrimary`
   )
                    VALUES (
            '{$singleCustomer['customerID']}' ,
            '{$singleCustomer['customerAdditionalTelephoneNumber']}' ,
            '{$singleCustomer['customerEmail']}' ,
            '{$locNo}' ,
            '{$locRoadName1}' ,
            '{$locRoadName2}' ,
            '{$locRoadName3}' ,
            '{$subTown}' ,
            '{$town}' ,
            '{$country}',
            'Default' ,
            '1'
             ) ;
SQL;
            $this->execute($sql);
        }

        $deleteColumnsFromCustomerSql = "SET SESSION old_alter_table=1;
                 ALTER TABLE `customer` DROP `customerAdditionalTelephoneNumber` ,
                 DROP `customerEmail` ,
                 DROP `customerAddress` ;";

        $this->execute($deleteColumnsFromCustomerSql);

        $customers = $this->fetchAll("SELECT * FROM `customer` ORDER BY customerID DESC;");

        $lastCustID = $customers[0]['customerID'] + 1;

        $existingCustomerReference = $this->fetchAll("SELECT * FROM `reference` WHERE `referenceNameID` = '20';");

        if (!isset($existingCustomerReference[0]['referenceNameID'])) {

            $referenceSql = <<<SQL
    INSERT INTO `reference` (
        `referenceNameID`,
        `referenceName`,
        `referenceTypeID`)
            VALUES (
                '20',
                'Customer',
                '0'
   ) ;
SQL;
            $this->execute($referenceSql);

            $referencePrefixSql = <<<SQL
                    INSERT INTO `referencePrefix` (
                    `referenceNameID` ,
                    `referencePrefixCHAR` ,
                    `referencePrefixNumberOfDigits` ,
                    `referencePrefixCurrentReference` )
                    VALUES (
                        '20' ,
                        'CUS' ,
                        '5' ,
                    '{$lastCustID}' ) ;

SQL;
            $this->execute($referencePrefixSql);
        }

        $this->execute("SET SESSION old_alter_table=1; ALTER TABLE `customer` CHANGE `customerShortName` `customerShortName` VARCHAR( 255 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL ;");
    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }

}
