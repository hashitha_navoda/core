<?php

use Phinx\Migration\AbstractMigration;

class AddColumnToTheTablePurchaseInvoice extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->table('purchaseInvoiceProduct');
        $hasColumn = $table->hasColumn('purchaseInvoiceProductDocumentTypeID');
        if(!$hasColumn){
            $table->addColumn('purchaseInvoiceProductDocumentTypeID', 'integer', array('null' => 'allow', 'after' => 'purchaseInvoiceProductTotal'))
                ->addColumn('purchaseInvoiceProductDocumentID', 'integer', array('null' => 'allow', 'after' => 'purchaseInvoiceProductDocumentTypeID'))
                ->update();
        }     
     }
}
