<?php

use Phinx\Migration\AbstractMigration;

class AddNewcolumnsToCreditNotePaymentTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $this->execute("SET SESSION old_alter_table=1;
            ALTER TABLE `creditNotePayment` ADD `creditNotePaymentLoyaltyAmount` decimal(20,5) NULL DEFAULT '0.00' AFTER `creditNotePaymentCustomCurrencyRate`;
        ");

        $this->execute("ALTER TABLE `creditNotePaymentMethodNumbers` CHANGE `creditNotePaymentMethodReferenceNumber` `creditNotePaymentMethodReferenceNumber` VARCHAR(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL;");
    }
}
