<?php

use Phinx\Migration\AbstractMigration;

class CreateTablePaymentVoucherNonItemProduct extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
      public function change()
      {
      }
     */

    /**
     * Migrate Up.
     */
    public function up()
    {
        $tableExists = $this->hasTable('paymentVoucherNonItemProduct');
        if (!$tableExists) {
            $paymentVoucherProduct = $this->table('paymentVoucherNonItemProduct', array('id' => 'paymentVoucherNonItemProductID'));
            $paymentVoucherProduct->addColumn('paymentVoucherID', 'integer', array('limit' => 20))
                    ->addColumn('paymentVoucherNonItemProductDiscription', 'string', array('limit' => 100))
                    ->addColumn('paymentVoucherNonItemProductQuantity', 'decimal', array('precision' => 20, 'scale' => '5'))
                    ->addColumn('paymentVoucherNonItemProductUnitPrice', 'decimal', array('precision' => 12, 'scale' => '2'))
                    ->addColumn('paymentVoucherNonItemProductTotalPrice', 'decimal', array('precision' => 12, 'scale' => '2'))
                    ->addColumn('roockID', 'integer', array('null' => 'allow'))
                    ->save();
        }
    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }

}
