<?php

use Phinx\Migration\AbstractMigration;

class AddDefaultCustomerToDb extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
      public function change()
      {
      }
     */

    /**
     * Migrate Up.
     */
    public function up()
    {

        $this->execute("INSERT INTO `entity` (
`entityID` ,
`createdBy` ,
`createdTimeStamp` ,
`updatedBy` ,
`updatedTimeStamp` ,
`deleted` ,
`deletedBy` ,
`deletedTimeStamp`
)
VALUES (
NULL , NULL , NULL , NULL , NULL , '0', NULL , NULL
);");
        $id = $this->fetchRow("select LAST_INSERT_ID()")[0];
        $this->execute("SET sql_mode='NO_AUTO_VALUE_ON_ZERO';INSERT INTO `customer` (
`customerID` ,
`customerTitle` ,
`customerName` ,
`customerShortName` ,
`customerTelephoneNumber` ,
`customerAdditionalTelephoneNumber` ,
`customerEmail` ,
`customerAddress` ,
`customerVatNumber` ,
`customerPaymentTerm` ,
`customerCreditLimit` ,
`customerCurrentBalance` ,
`customerCurrentCredit` ,
`customerDiscount` ,
`customerOther` ,
`customerCurrency` ,
`entityID`
)
VALUES (
'0', 'Mr.', 'Default Customer', 'DefaultCustomer', NULL , NULL , NULL , NULL , NULL , '1', '0.00', '0.00', '0.00', '0', NULL , '13', $id
);");
    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }

}
