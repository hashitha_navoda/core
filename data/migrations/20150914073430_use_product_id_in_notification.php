<?php

use Phinx\Migration\AbstractMigration;

class UseProductIdInNotification extends AbstractMigration
{

    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     */
//    public function change()
//    {
//
//    }

    public function up()
    {

        $sql = <<<SQL
                UPDATE `notification` JOIN `locationProduct` ON `notification`.`notificationReferID` = `locationProduct`.`locationProductID`
                    SET `notification`.`notificationReferID` = `locationProduct`.`productID`
                        WHERE `notificationType` IN ('3', '4')
SQL;
        $this->execute($sql);
    }

}
