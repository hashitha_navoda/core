<?php

use Phinx\Migration\AbstractMigration;

class AddColumnToNotificationTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
      public function change()
      {
      }
     */

    /**
     * Migrate Up.
     */
    public function up()
    {
        $table = $this->table('notification');
        $hasColumn = $table->hasColumn('notificationShowID');
        if (!$hasColumn) {
            $table->addColumn('notificationShowID', 'integer', array('after' => 'userID', 'default' => '0'))
                    ->update();
        }
    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }

}
