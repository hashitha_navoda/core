<?php

use Phinx\Migration\AbstractMigration;

class ChangeQuotCodeColumnToQuotationIdInSalesOrder extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
      public function change()
      {
      }
     */

    /**
     * Migrate Up.
     */
    public function up()
    {
        $this->execute("SET SESSION old_alter_table=1; "
                . "ALTER TABLE  `salesOrders` ADD  `quotationID` INT NOT NULL AFTER  `soCode` ;");

        $this->execute("UPDATE `salesOrders`, `quotation` "
                . "SET `salesOrders`.`quotationID` = `quotation`.`quotationID` "
                . "WHERE `salesOrders`.`quotCode` = `quotation`.`quotationCode` "
                . "AND `salesOrders`.`quotCode` != '' ;");

        $this->execute("SET SESSION old_alter_table=1; "
                . "ALTER TABLE  `salesOrders` DROP  `quotCode` ;");
    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }

}
