<?php

use Phinx\Migration\AbstractMigration;

class ChangeSomePropertiesInContractorTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function up()
    {
        $table = $this->table('contractor');
        $column = $table->hasColumn('contractorCode');
        if ($column) {
            $table->changeColumn('contractorCode', 'string', ['default'=> null, 'null'=>true])
              ->save();
        }

        $table = $this->table('contractor');
        $column = $table->hasColumn('assignForTask');
        if ($column) {
            $table->changeColumn('assignForTask', 'boolean', ['default'=> null, 'null'=>true])
              ->save();
        }

        $table = $this->table('contractor');
        $column = $table->hasColumn('assignForJob');
        if ($column) {
            $table->changeColumn('assignForJob', 'boolean', ['default'=> null, 'null'=>true])
              ->save();
        }
    }
}
