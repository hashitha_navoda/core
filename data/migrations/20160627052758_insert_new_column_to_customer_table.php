<?php

use Phinx\Migration\AbstractMigration;

class InsertNewColumnToCustomerTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $this->execute("SET SESSION old_alter_table=1; ALTER TABLE `customer` ADD `customerReceviableAccountID` INT(11) NULL AFTER `customerEvent`; ");
        $this->execute("SET SESSION old_alter_table=1; ALTER TABLE `customer` ADD `customerSalesAccountID` INT(11) NULL AFTER `customerReceviableAccountID`; ");
        $this->execute("SET SESSION old_alter_table=1; ALTER TABLE `customer` ADD `customerSalesDiscountAccountID` INT(11) NULL AFTER `customerSalesAccountID`; ");
        $this->execute("SET SESSION old_alter_table=1; ALTER TABLE `customer` ADD `customerAdvancePaymentAccountID` INT(11) NULL AFTER `customerSalesDiscountAccountID`; ");
    }
}
