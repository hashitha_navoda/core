<?php

use Phinx\Migration\AbstractMigration;

class JobCardReportFeatures extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
      public function change()
      {
      }
     */

    /**
     * Migrate Up.
     */
    public function up()
    {
        $this->execute("INSERT INTO `feature` (`featureID` ,`featureName` ,`featureController` ,`featureAction` ,`featureCategory` ,`featureType` ,`moduleID`)
                        VALUES (NULL, 'Generate CSV', 'Reporting\\\Controller\\\Generate\\\JobCardReport', 'generateJobCardProgressSheet', 'Job Card Progress Report', '0', '5');");
        $id1 = $this->fetchRow("select LAST_INSERT_ID()")[0];
        $this->execute("INSERT INTO `roleFeature` (`roleFeatureID`, `roleID`, `featureID`, `roleFeatureEnabled`) VALUES (NULL, '1', $id1, '1');");

        $this->execute("INSERT INTO `feature` (`featureID` ,`featureName` ,`featureController` ,`featureAction` ,`featureCategory` ,`featureType` ,`moduleID`)
                        VALUES (NULL, 'Generate PDF', 'Reporting\\\Controller\\\Generate\\\JobCardReport', 'generateJobCardProgressPdf', 'Job Card Progress Report', '0', '5');");
        $id2 = $this->fetchRow("select LAST_INSERT_ID()")[0];
        $this->execute("INSERT INTO `roleFeature` (`roleFeatureID`, `roleID`, `featureID`, `roleFeatureEnabled`) VALUES (NULL, '1', $id2, '1');");

        $this->execute("INSERT INTO `feature` (`featureID` ,`featureName` ,`featureController` ,`featureAction` ,`featureCategory` ,`featureType` ,`moduleID`)
                        VALUES (NULL, 'View Report', 'Reporting\\\Controller\\\Generate\\\JobCardReport', 'viewJobCardProgress', 'Job Card Progress Report', '0', '5');");
        $id3 = $this->fetchRow("select LAST_INSERT_ID()")[0];
        $this->execute("INSERT INTO `roleFeature` (`roleFeatureID`, `roleID`, `featureID`, `roleFeatureEnabled`) VALUES (NULL, '1', $id3, '1');");

        $this->execute("INSERT INTO `feature` (`featureID` ,`featureName` ,`featureController` ,`featureAction` ,`featureCategory` ,`featureType` ,`moduleID`)
                        VALUES (NULL, 'Generate CSV', 'Reporting\\\Controller\\\Generate\\\JobCardReport', 'generateWeightageSheet', 'Weightage Report', '0', '5');");
        $id4 = $this->fetchRow("select LAST_INSERT_ID()")[0];
        $this->execute("INSERT INTO `roleFeature` (`roleFeatureID`, `roleID`, `featureID`, `roleFeatureEnabled`) VALUES (NULL, '1', $id4, '1');");

        $this->execute("INSERT INTO `feature` (`featureID` ,`featureName` ,`featureController` ,`featureAction` ,`featureCategory` ,`featureType` ,`moduleID`)
                        VALUES (NULL, 'Generate PDF', 'Reporting\\\Controller\\\Generate\\\JobCardReport', 'generateWeightagePdf', 'Weightage Report', '0', '5');");
        $id5 = $this->fetchRow("select LAST_INSERT_ID()")[0];
        $this->execute("INSERT INTO `roleFeature` (`roleFeatureID`, `roleID`, `featureID`, `roleFeatureEnabled`) VALUES (NULL, '1', $id5, '1');");

        $this->execute("INSERT INTO `feature` (`featureID` ,`featureName` ,`featureController` ,`featureAction` ,`featureCategory` ,`featureType` ,`moduleID`)
                        VALUES (NULL, 'View Report', 'Reporting\\\Controller\\\Generate\\\JobCardReport', 'viewWeightage', 'Weightage Report', '0', '5');");
        $id6 = $this->fetchRow("select LAST_INSERT_ID()")[0];
        $this->execute("INSERT INTO `roleFeature` (`roleFeatureID`, `roleID`, `featureID`, `roleFeatureEnabled`) VALUES (NULL, '1', $id6, '1');");

        $this->execute("INSERT INTO `feature` (`featureID` ,`featureName` ,`featureController` ,`featureAction` ,`featureCategory` ,`featureType` ,`moduleID`)
                        VALUES (NULL, 'Generate CSV', 'Reporting\\\Controller\\\Generate\\\JobCardReport', 'generateInquiryLogSheet', 'Inquiry Log Report', '0', '5');");
        $id7 = $this->fetchRow("select LAST_INSERT_ID()")[0];
        $this->execute("INSERT INTO `roleFeature` (`roleFeatureID`, `roleID`, `featureID`, `roleFeatureEnabled`) VALUES (NULL, '1', $id7, '1');");

        $this->execute("INSERT INTO `feature` (`featureID` ,`featureName` ,`featureController` ,`featureAction` ,`featureCategory` ,`featureType` ,`moduleID`)
                        VALUES (NULL, 'Generate PDF', 'Reporting\\\Controller\\\Generate\\\JobCardReport', 'generateInquiryLogPdf', 'Inquiry Log Report', '0', '5');");
        $id8 = $this->fetchRow("select LAST_INSERT_ID()")[0];
        $this->execute("INSERT INTO `roleFeature` (`roleFeatureID`, `roleID`, `featureID`, `roleFeatureEnabled`) VALUES (NULL, '1', $id8, '1');");

        $this->execute("INSERT INTO `feature` (`featureID` ,`featureName` ,`featureController` ,`featureAction` ,`featureCategory` ,`featureType` ,`moduleID`)
                        VALUES (NULL, 'View Report', 'Reporting\\\Controller\\\Generate\\\JobCardReport', 'viewInquiryLog', 'Inquiry Log Report', '0', '5');");
        $id9 = $this->fetchRow("select LAST_INSERT_ID()")[0];
        $this->execute("INSERT INTO `roleFeature` (`roleFeatureID`, `roleID`, `featureID`, `roleFeatureEnabled`) VALUES (NULL, '1', $id9, '1');");

        $this->execute("INSERT INTO `feature` (`featureID` ,`featureName` ,`featureController` ,`featureAction` ,`featureCategory` ,`featureType` ,`moduleID`)
                        VALUES (NULL, 'Generate CSV', 'Reporting\\\Controller\\\Generate\\\JobCardReport', 'generateJobCardInvoiceValueSheet', 'Job Card Invoice Value Report', '0', '5');");
        $id10 = $this->fetchRow("select LAST_INSERT_ID()")[0];
        $this->execute("INSERT INTO `roleFeature` (`roleFeatureID`, `roleID`, `featureID`, `roleFeatureEnabled`) VALUES (NULL, '1', $id10, '1');");

        $this->execute("INSERT INTO `feature` (`featureID` ,`featureName` ,`featureController` ,`featureAction` ,`featureCategory` ,`featureType` ,`moduleID`)
                        VALUES (NULL, 'Generate PDF', 'Reporting\\\Controller\\\Generate\\\JobCardReport', 'generateJobCardInvoiceValuePdf', 'Job Card Invoice Value Report', '0', '5');");
        $id11 = $this->fetchRow("select LAST_INSERT_ID()")[0];
        $this->execute("INSERT INTO `roleFeature` (`roleFeatureID`, `roleID`, `featureID`, `roleFeatureEnabled`) VALUES (NULL, '1', $id11, '1');");

        $this->execute("INSERT INTO `feature` (`featureID` ,`featureName` ,`featureController` ,`featureAction` ,`featureCategory` ,`featureType` ,`moduleID`)
                        VALUES (NULL, 'View Report', 'Reporting\\\Controller\\\Generate\\\JobCardReport', 'viewJobCardInvoiceValue', 'Job Card Invoice Value Report', '0', '5');");
        $id12 = $this->fetchRow("select LAST_INSERT_ID()")[0];
        $this->execute("INSERT INTO `roleFeature` (`roleFeatureID`, `roleID`, `featureID`, `roleFeatureEnabled`) VALUES (NULL, '1', $id12, '1');");

        $this->execute("INSERT INTO `feature` (`featureID` ,`featureName` ,`featureController` ,`featureAction` ,`featureCategory` ,`featureType` ,`moduleID`)
                        VALUES (NULL, 'Access Report Screen', 'Reporting\\\Controller\\\JobCardReport', 'index', 'Job Card Report', '0', '5');");
        $id13 = $this->fetchRow("select LAST_INSERT_ID()")[0];
        $this->execute("INSERT INTO `roleFeature` (`roleFeatureID`, `roleID`, `featureID`, `roleFeatureEnabled`) VALUES (NULL, '1', $id13, '1');");

        $this->execute("INSERT INTO `feature` (`featureID` ,`featureName` ,`featureController` ,`featureAction` ,`featureCategory` ,`featureType` ,`moduleID`)
                        VALUES (NULL, 'Default', 'JobCard\\\Controller\\\API\\\Activity', 'getActivityByProjectID', 'Application', '0', '8');");
        $id14 = $this->fetchRow("select LAST_INSERT_ID()")[0];
        $this->execute("INSERT INTO `roleFeature` (`roleFeatureID`, `roleID`, `featureID`, `roleFeatureEnabled`) VALUES (NULL, '1', $id14, '1');");
    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }

}
