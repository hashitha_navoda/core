<?php

use Phinx\Migration\AbstractMigration;

class UpdateUserControllerEditFeature extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
      public function change()
      {
      }
     */

    /**
     * Migrate Up.
     */
    public function up()
    {
        $this->execute("UPDATE `feature` SET `featureName`='Edit Location' WHERE `featureController`='User\\\Controller\\\User' AND `featureAction`='editUserLocation' ");
        $this->execute("UPDATE `feature` SET `featureType`= 1 WHERE `featureController`='User\\\Controller\\\UserApi' AND `featureAction`='userUpdate' ");
    }

    /**
     * Migrate Down.
     */
    public function down()
    {
        
    }

}
