<?php

use Phinx\Migration\AbstractMigration;

class AddNewFeatureInquiryLogEmailSchedule extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
      public function change()
      {
      }
     */

    /**
     * Migrate Up.
     */
    public function up()
    {

        $this->execute("INSERT INTO `feature` (`featureID` ,`featureName` ,`featureController` ,`featureAction` ,`featureCategory` ,`featureType` ,`moduleID`)
                        VALUES (NULL, 'View', 'JobCard\\\Controller\\\InquirySetup', 'index', 'Inquiry Setup', '0', '8');");
        $id2 = $this->fetchRow("select LAST_INSERT_ID()")[0];
        $this->execute("INSERT INTO `roleFeature` (`roleFeatureID`, `roleID`, `featureID`, `roleFeatureEnabled`) VALUES (NULL, '1', $id2, '1');");
        $this->execute("INSERT INTO `feature` (`featureID` ,`featureName` ,`featureController` ,`featureAction` ,`featureCategory` ,`featureType` ,`moduleID`)
                        VALUES (NULL, 'Update', 'JobCard\\\Controller\\\InquirySetup', 'update', 'Inquiry Setup', '0', '8');");
        $id2 = $this->fetchRow("select LAST_INSERT_ID()")[0];
        $this->execute("INSERT INTO `roleFeature` (`roleFeatureID`, `roleID`, `featureID`, `roleFeatureEnabled`) VALUES (NULL, '1', $id2, '1');");

        $this->execute("SET SESSION old_alter_table=1;ALTER TABLE `employee` ADD `employeeEmail` VARCHAR( 150 ) NOT NULL AFTER `divisionID` ;");
        $this->execute("SET SESSION old_alter_table=1;ALTER TABLE `employee` ADD `userID` INT NULL AFTER `employeeHourlyCost` ;");
        $this->execute("SET SESSION old_alter_table=1;ALTER TABLE `notification` ADD `userID` INT NULL AFTER `notificationUpdatedDateTime` ;");

        $this->execute("SET SESSION old_alter_table=1;
            CREATE TABLE IF NOT EXISTS `inquiryLogEmailSchedule` (
            `inquiryLogEmailScheduleID` int(11) NOT NULL AUTO_INCREMENT,
            `inquiryLogID` int(11) NOT NULL,
            `inquirySetupID` int(11) NOT NULL,
            `createdAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
            `inquiryLogEmailScheduleDateTime` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
            `isEmailSend` tinyint(1) NOT NULL,
            PRIMARY KEY (`inquiryLogEmailScheduleID`),
            KEY `isEmailSend` (`isEmailSend`)
          ) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;");

        $this->execute("SET SESSION old_alter_table=1;
            CREATE TABLE IF NOT EXISTS `inquirySetup` (
            `inquirySetupID` int(11) NOT NULL AUTO_INCREMENT,
            `inquirySetupName` varchar(100) NOT NULL,
            `inquirySetupStatus` tinyint(1) NOT NULL,
            PRIMARY KEY (`inquirySetupID`)
          ) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

          INSERT INTO `inquirySetup` (`inquirySetupID`, `inquirySetupName`, `inquirySetupStatus`) VALUES
          (1, 'create', 1),
          (2, 'update', 1),
          (3, 'delete', 1); ");

        $this->execute("SET SESSION old_alter_table=1; UPDATE `inquiryStatusType` SET `inquiryStatusTypeName` = 'At the Creation' WHERE `inquiryStatusType`.`inquiryStatusTypeID` = 1 ;");
        $this->execute("SET SESSION old_alter_table=1; UPDATE `inquiryStatusType` SET `inquiryStatusTypeName` = 'After the one Minutes' WHERE `inquiryStatusType`.`inquiryStatusTypeID` = 2 ;");
        $this->execute("SET SESSION old_alter_table=1; UPDATE `inquiryStatusType` SET `inquiryStatusTypeName` = 'After the one hour' WHERE `inquiryStatusType`.`inquiryStatusTypeID` = 3 ;");
        $this->execute("SET SESSION old_alter_table=1; UPDATE `inquiryStatusType` SET `inquiryStatusTypeName` = 'After the one Day' WHERE `inquiryStatusType`.`inquiryStatusTypeID` =4 ;");
        $this->execute("SET SESSION old_alter_table=1; DELETE FROM `inquiryStatusType` WHERE `inquiryStatusType`.`inquiryStatusTypeID` =5 ;");
    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }

}
