<?php

use Phinx\Migration\AbstractMigration;

class CreateTableCompoundCostGrn extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $comCostGrnTable = $this->hasTable('compoundCostGrn');
        if (!$comCostGrnTable) {
            $table = $this->table('compoundCostGrn', ['id' => 'compoundCostGrnID']);
            $table->addColumn('compoundCostTemplateID', 'integer')
                ->addColumn('grnID', 'integer')
                ->save();
        }

        // add values to the Compound Cost Type table
        $this->execute("INSERT INTO compoundCostType (compoundCostTypeName, compoundCostTypeCode) VALUES ('TRANSPORT','CCT001'),('FORWARDING','CCT002'),('CLEARANCE','CCT003'),('INSURANCE','CCT004'),('TT PAYMENT BANK CHARGES','CCT005'),('FUMIGATION','CCT006'),('CUSTOM DUTY', 'CCT007'),('DUTY-BANK CHARGES', 'CCT008')");
        
    }
}
