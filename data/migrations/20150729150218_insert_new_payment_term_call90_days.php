<?php

use Phinx\Migration\AbstractMigration;

class InsertNewPaymentTermCall90Days extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
      public function change()
      {
      }
     */

    /**
     * Migrate Up.
     */
    public function up()
    {
        $this->execute("INSERT IGNORE INTO `paymentTerm` (
`paymentTermID` ,
`paymentTermName` ,
`paymentTermDescription`
)
VALUES (
'6', 'Within 90 Days', NULL
);");
    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }

}
