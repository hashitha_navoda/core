<?php

use Phinx\Migration\AbstractMigration;

class AddLocationProductIdToSalesInvoiceProductTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $pdo = $this->getAdapter()->getConnection();
        $results = $this->fetchAll("SELECT * FROM `salesInvoiceProduct` INNER JOIN `salesInvoice` ON `salesInvoiceProduct`.`salesInvoiceID` = `salesInvoice`.`salesInvoiceID` WHERE `locationProductID` = 0");
        foreach ($results as $key => $value) {
            $salesInvoiceProductID = $value['salesInvoiceProductID'];
            $productID = $value['productID'];
            $locationID = $value['locationID'];
            if($productID != null && $locationID != null && $salesInvoiceProductID != null){
                $locationProducts = $this->fetchRow("SELECT * FROM `locationProduct`  WHERE productID={$productID} AND locationID={$locationID}");
                $locationProductID = $locationProducts['locationProductID'];
                $updateQuery = <<<SQL
                            UPDATE `salesInvoiceProduct` SET 
                            `locationProductID` = :locationProductID
                             WHERE `salesInvoiceProduct`.`salesInvoiceProductID` = :salesInvoiceProductID;
SQL;
                    $pdo->prepare($updateQuery)->execute(array(
                        'locationProductID' => $locationProductID, 
                        'salesInvoiceProductID' => $salesInvoiceProductID, 
                    ));
            }
        }
        
    }
}
