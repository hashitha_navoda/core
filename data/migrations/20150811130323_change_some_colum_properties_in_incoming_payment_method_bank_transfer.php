<?php

use Phinx\Migration\AbstractMigration;

class ChangeSomeColumPropertiesInIncomingPaymentMethodBankTransfer extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
      public function change()
      {
      }
     */

    /**
     * Migrate Up.
     */
    public function up()
    {
        $this->execute('SET SESSION old_alter_table=1; ALTER TABLE `incomingPaymentMethodBankTransfer` CHANGE `incomingPaymentMethodBankTransferBankId` `incomingPaymentMethodBankTransferBankId` INT( 11 ) NULL ;');
        $this->execute('SET SESSION old_alter_table=1; ALTER TABLE `incomingPaymentMethodBankTransfer` CHANGE `incomingPaymentMethodBankTransferAccountId` `incomingPaymentMethodBankTransferAccountId` INT( 11 ) NULL ;');
        $this->execute('SET SESSION old_alter_table=1; ALTER TABLE `incomingPaymentMethodCreditCard` CHANGE `incomingPaymentMethodCreditReceiptNumber` `incomingPaymentMethodCreditReceiptNumber` VARCHAR( 20 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL ;');
    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }

}
