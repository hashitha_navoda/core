<?php

use Phinx\Migration\AbstractMigration;

class CreateNewTableCalledDebitNotePaymetDetails extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
      public function change()
      {
      }
     */

    /**
     * Migrate Up.
     */
    public function up()
    {
        $this->execute("CREATE TABLE IF NOT EXISTS `debitNotePaymentDetails` (
  `debitNotePaymentDetailsID` int(11) NOT NULL AUTO_INCREMENT,
  `paymentMethodID` int(11) NOT NULL,
  `debitNotePaymentID` int(11) NOT NULL,
  `debitNoteID` int(11) NOT NULL,
  `debitNotePaymentDetailsAmount` decimal(12,2) NOT NULL DEFAULT '0.00',
  PRIMARY KEY (`debitNotePaymentDetailsID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;");
    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }

}
