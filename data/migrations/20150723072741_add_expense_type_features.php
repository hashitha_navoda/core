<?php

use Phinx\Migration\AbstractMigration;

class AddExpenseTypeFeatures extends AbstractMigration
{

    public function up()
    {
        $queries = array(
            array('query' => "INSERT INTO `feature` (`featureID`, `featureName`, `featureController`, `featureAction`, `featureCategory`, `featureType`, `moduleID`) VALUES (NULL, 'Create', 'Expenses\\\Controller\\\ExpenseType', 'create', 'Expense Type', '0', '9');", 'type' => '0'),
            array('query' => "INSERT INTO `feature` (`featureID`, `featureName`, `featureController`, `featureAction`, `featureCategory`, `featureType`, `moduleID`) VALUES (NULL, 'Edit', 'Expenses\\\Controller\\\ExpenseType', 'edit', 'Expense Type', '0', '9');", 'type' => '0'),
            array('query' => "INSERT INTO `feature` (`featureID`, `featureName`, `featureController`, `featureAction`, `featureCategory`, `featureType`, `moduleID`) VALUES (NULL, 'List', 'Expenses\\\Controller\\\ExpenseType', 'list', 'Expense Type', '0', '9');", 'type' => '0'),
            array('query' => "INSERT INTO `feature` (`featureID`, `featureName`, `featureController`, `featureAction`, `featureCategory`, `featureType`, `moduleID`) VALUES (NULL, 'Default', 'Expenses\\\Controller\\\API\\\ExpenseType', 'add', 'Application', '1', '9');", 'type' => '1'),
            array('query' => "INSERT INTO `feature` (`featureID`, `featureName`, `featureController`, `featureAction`, `featureCategory`, `featureType`, `moduleID`) VALUES (NULL, 'Default', 'Expenses\\\Controller\\\API\\\ExpenseType', 'edit', 'Application', '1', '9');", 'type' => '1'),
            array('query' => "INSERT INTO `feature` (`featureID`, `featureName`, `featureController`, `featureAction`, `featureCategory`, `featureType`, `moduleID`) VALUES (NULL, 'Default', 'Expenses\\\Controller\\\API\\\ExpenseType', 'delete', 'Application', '1', '9');", 'type' => '1'),
            array('query' => "INSERT INTO `feature` (`featureID`, `featureName`, `featureController`, `featureAction`, `featureCategory`, `featureType`, `moduleID`) VALUES (NULL, 'Default', 'Expenses\\\Controller\\\API\\\ExpenseType', 'changeExpenseTypeStatus', 'Application', '1', '9');", 'type' => '1'),
        );
        $rows = $this->fetchAll("SELECT roleID FROM `role`");

        foreach ($queries as $query) {
            $this->execute($query['query']);
            $id = $this->fetchRow("select LAST_INSERT_ID()")[0];
            foreach ($rows as $row) {
                $roleID = $row['roleID'];
                if ($roleID == 1) {
                    $enabled = 1;
                } else {
                    if ($query['type'] == '0') {
                        $enabled = 0;
                    } else {
                        $enabled = 1;
                    }
                }
                $this->execute("INSERT INTO `roleFeature` (`roleFeatureID`, `roleID`, `featureID`, `roleFeatureEnabled`) VALUES (NULL, $roleID, $id,$enabled);");
            }
        }
    }

}
