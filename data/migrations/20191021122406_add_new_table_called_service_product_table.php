<?php

use Phinx\Migration\AbstractMigration;

class AddNewTableCalledServiceProductTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->hasTable('serviceProduct');

        if (!$table) {
            $table = $this->table('serviceProduct', ['id' => 'serviceProductID']);
            $table->addColumn('serviceID', 'integer', ['limit' => 20,'default' => null, 'null' => true]);
            $table->addColumn('vehicleTypeID', 'integer', ['limit' => 20,'default' => null, 'null' => true]);
            $table->addColumn('locationProductID', 'integer', ['limit' => 20,'default' => null, 'null' => true]);
            $table->addColumn('serviceProductType', 'integer', ['limit' => 20,'default' => null, 'null' => true]);
            $table->addColumn('serviceProductQty', 'decimal', ['default' => null, 'null' => true, 'precision' => 20, 'scale' => 5]);
            $table->addColumn('serviceProductUomID', 'integer', ['limit' => 20,'default' => null, 'null' => true]);
            $table->save();
        }
    }
}
