<?php

use Phinx\Migration\AbstractMigration;

class CreateTemporaryProductTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
      public function change()
      {
      }
     */

    /**
     * Migrate Up.
     */
    public function up()
    {
        $this->dropTable('temporaryItem');
        $this->execute("CREATE TABLE IF NOT EXISTS `temporaryProduct` (
                            `temporaryProductID` int(11) NOT NULL AUTO_INCREMENT,
                            `temporaryProductCode` VARCHAR(100) NOT NULL,
                            `temporaryProductName` VARCHAR(100) NOT NULL,
                            `temporaryProductDescription` longtext NULL,
                            `temporaryProductQuantity` DECIMAL( 12, 2 ) NOT NULL DEFAULT '0',
                            `uomID` INT( 11 ) NOT NULL,
                            `temporaryProductImageURL` VARCHAR(100) NULL,
                            `temporaryProductSerial` TINYINT( 1 ) NULL DEFAULT NULL ,
                            `temporaryProductBatch` TINYINT( 1 ) NULL DEFAULT NULL ,
                            `entityID` INT( 1 ) NOT NULL ,
                             PRIMARY KEY (`temporaryProductID`)
                    ) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;");
    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }

}
