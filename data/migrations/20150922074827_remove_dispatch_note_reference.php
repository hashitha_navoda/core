<?php

use Phinx\Migration\AbstractMigration;

class RemoveDispatchNoteReference extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
    public function change()
    {
    }
    */
    
    /**
     * Migrate Up.
     */
    public function up()
    {
        $referencePrefixID = $this->fetchRow("SELECT referencePrefixID FROM `referencePrefix` WHERE `referenceNameID` = '29';")[0];
        if(!$referencePrefixID){
            $this->execute("INSERT INTO `referencePrefix` (`referencePrefixID`, `referenceNameID`, `locationID`, `referencePrefixCHAR`, `referencePrefixNumberOfDigits`, `referencePrefixCurrentReference`) VALUES (NULL, '29', NULL, 'DISPNOTE', '3', '1');");
        }
        
    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }
}