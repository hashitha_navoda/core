<?php

use Phinx\Migration\AbstractMigration;

class CreateNewTableCalledDebitNotePaymet extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
      public function change()
      {
      }
     */

    /**
     * Migrate Up.
     */
    public function up()
    {
        $this->execute("CREATE TABLE IF NOT EXISTS `debitNotePayment` (
  `debitNotePaymentID` int(11) NOT NULL AUTO_INCREMENT,
  `debitNotePaymentCode` varchar(30) NOT NULL,
  `debitNotePaymentDate` date NOT NULL,
  `paymentTermID` int(11) NOT NULL,
  `debitNotePaymentAmount` decimal(12,2) NOT NULL DEFAULT '0.00',
  `supplierID` int(11) NOT NULL,
  `debitNotePaymentDiscount` decimal(12,2) DEFAULT '0.00',
  `debitNotePaymentMemo` text,
  `locationID` int(11) NOT NULL,
  `statusID` int(11) NOT NULL,
  `entityID` int(11) NOT NULL,
  PRIMARY KEY (`debitNotePaymentID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;");
    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }

}
