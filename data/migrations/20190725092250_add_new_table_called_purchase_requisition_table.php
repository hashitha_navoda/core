<?php

use Phinx\Migration\AbstractMigration;

class AddNewTableCalledPurchaseRequisitionTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->hasTable('purchaseRequisition');

        if (!$table) {
            $table = $this->table('purchaseRequisition', ['id' => 'purchaseRequisitionID']);
            $table->addColumn('purchaseRequisitionCode', 'string', ['limit' => 200]);
            $table->addColumn('purchaseRequisitionSupplierID', 'integer', ['limit' => 20,'default' => null, 'null' => true]);
            $table->addColumn('purchaseRequisitionSupplierReference', 'string', ['limit' => 200,'default' => null, 'null' => true]);
            $table->addColumn('purchaseRequisitionRetrieveLocation', 'integer', ['limit' => 20]);
            $table->addColumn('purchaseRequisitionEstDelDate', 'date', ['default' => null, 'null' => true]);
            $table->addColumn('purchaseRequisitionDate', 'date', ['default' => null, 'null' => true]);
            $table->addColumn('purchaseRequisitionDescription', 'text', ['default' => null, 'null' => true]);
            $table->addColumn('purchaseRequisitionDeliveryCharge', 'decimal', ['precision' => 20, 'scale' => 5]);
            $table->addColumn('purchaseRequisitionTotal', 'decimal', ['precision' => 20, 'scale' => 5]);
            $table->addColumn('purchaseRequisitionShowTax', 'boolean', ['default' => 0, 'null' => true]);
            $table->addColumn('purchaseRequisitionDraftFlag', 'boolean', ['default' => 0]);
            $table->addColumn('purchaseRequisitionType', 'integer', ['limit' => 20,'default' => null, 'null' => true]);
            $table->addColumn('purchaseRequisitionApproved', 'boolean', ['default' => null, 'null' => true]);
            $table->addColumn('purchaseRequisitionHashValue', 'string', ['limit' => 200,'default' => null, 'null' => true]);
            $table->addColumn('status', 'integer', ['limit' => 20]);
            $table->addColumn('entityID', 'integer', ['limit' => 20,'default' => null, 'null' => true]);
            $table->save();
        }
    }
}
