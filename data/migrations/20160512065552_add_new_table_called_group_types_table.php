<?php

use Phinx\Migration\AbstractMigration;

class AddNewTableCalledGroupTypesTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
         $Sql = <<<SQL
                CREATE TABLE IF NOT EXISTS `financeGroupTypes` (
`financeGroupTypesID` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `financeGroupTypesName` varchar(100) NOT NULL,
  `entityID` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `financeGroupTypes` (`financeGroupTypesID`, `financeGroupTypesName`, `entityID`) VALUES
(1, 'Branch', NULL),
(2, 'Division', NULL),
(3, 'Project', NULL);

ALTER TABLE `financeGroupTypes`
MODIFY `financeGroupTypesID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;

SQL;

        $this->execute($Sql);
    }
}
