<?php

use Phinx\Migration\AbstractMigration;

class UpdateNewTemplateOptionsToInvoiceTemplate extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $conn = $this->getAdapter()->getConnection();
        $templateRes = $this->fetchAll("SELECT * FROM `template` WHERE `documentTypeID` = 1");

        foreach ($templateRes as $value) {
            if (!is_null($value['templateOptions'])) {
                $templateOptions = json_decode($value['templateOptions']);

                $attribute_01 = array(
                    'name' => "attribute_01",
                    'show' => false,
                    'width' => '',
                    'order' => "11"
                );
                $attribute_02 = array(
                    'name' => "attribute_02",
                    'show' => false,
                    'width' => '',
                    'order' => "12"
                );
                $attribute_03 = array(
                    'name' => "attribute_03",
                    'show' => false,
                    'width' => '',
                    'order' => "13"
                );
                
                array_push($templateOptions->product_table->columns, (object)$attribute_01, (object)$attribute_02, (object)$attribute_03);
                $templateOptionData = json_encode($templateOptions);
                $quotedString = $conn->quote($templateOptionData);
                $this->execute("UPDATE template SET templateOptions = $quotedString WHERE templateID =".$value['templateID']); 
            }
        }
    }
}
