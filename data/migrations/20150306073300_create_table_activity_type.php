<?php

use Phinx\Migration\AbstractMigration;

class CreateTableActivityType extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
      public function change()
      {
      }
     */

    /**
     * Migrate Up.
     */
    public function up()
    {

        $this->execute("CREATE TABLE IF NOT EXISTS `activityType` (
  `activityTypeID` int(11) NOT NULL AUTO_INCREMENT,
  `activityTypeName` VARCHAR(100) NOT NULL,
  `activityTypeMaxValue` DOUBLE NOT NULL,
  `activityTypeMinValue` DOUBLE NOT NULL,
   PRIMARY KEY (`activityTypeID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;");
    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }

}
