<?php

use Phinx\Migration\AbstractMigration;

class CreateTableForCompoundCosting extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $comTempTable = $this->hasTable('compoundCostTemplate');
        if (!$comTempTable) {
            $table = $this->table('compoundCostTemplate', ['id' => 'compoundCostTemplateID']);
            $table->addColumn('compoundCostTemplateDescription', 'string', ['limit' => 200, 'default' => null])
                ->addColumn('compoundCostTemplateName', 'string', ['limit' => 100, 'default' => null])
              ->addColumn('locationID', 'integer')
              ->addColumn('entityID', 'integer')
              ->addColumn('status', 'integer')
              ->save();
        }
        
        $compCostItem = $this->hasTable('compoundCostItem');
        if(!$compCostItem) {
            $compCostItemTable = $this->table('compoundCostItem', ['id'=>'compoundCostItemID']);
            $compCostItemTable->addColumn('productID','integer')
                ->addColumn('locationProductID', 'integer')
                ->addColumn('compoundCostItemQty', 'decimal',['default' => null, 'null' => true, 'precision' => 20, 'scale' => 5])
                ->addColumn('compoundCostItemPurchasePrice', 'decimal',['default' => null, 'null' => true, 'precision' => 20, 'scale' => 5])
                ->addColumn('compoundCostItemCostAmount', 'decimal',['default' => null, 'null' => true, 'precision' => 20, 'scale' => 5])
                ->addColumn('compoundCostItemTotalUnitCost', 'decimal',['default' => null, 'null' => true, 'precision' => 20, 'scale' => 5])
                ->addColumn('compoundCostTemplateID', 'integer')
                ->save();
        }

        $compCostValue = $this->hasTable('compoundCostValue');
        if(!$compCostValue) {
            $compCostValueTable = $this->table('compoundCostValue',['id' => 'compoundCostValueID']);
            $compCostValueTable->addColumn('compoundCostTypeID', 'integer')
            ->addColumn('amount', 'integer')
            ->addColumn('compoundCostTemplateID', 'integer')
            ->save();
        }

        $compCostItemMap = $this->hasTable('compoundCostItemMap');
        if(!$compCostItemMap) {
            $compCostItemMapTable = $this->table('compoundCostItemMap',['id'=>'compoundCostItemMapID']);
            $compCostItemMapTable->addColumn('compoundCostItemID', 'integer')
            ->addColumn('compoundCostValueID', 'integer')
            ->addColumn('ratio', 'decimal',['default' => null, 'null' => true, 'precision' => 20, 'scale' => 5])
            ->save();
        }
    }
}
