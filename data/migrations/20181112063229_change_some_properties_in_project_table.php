<?php

use Phinx\Migration\AbstractMigration;

class ChangeSomePropertiesInProjectTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    
    public function up()
    {
        //change some column properties in project table
        //designationID
        $table = $this->table('project');
        $column = $table->hasColumn('customerProfileID');
        if ($column) {
            $table->changeColumn('customerProfileID', 'integer', ['default'=> null, 'null'=>true])
              ->save();
        }
        
    }
}
