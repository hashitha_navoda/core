<?php

use Phinx\Migration\AbstractMigration;

class AddNewTablesToDatabaseRelatedWithPromotion extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change() {
        $table = $this->hasTable('promotionCombination');

        if (!$table) {
            $table = $this->table('promotionCombination', ['id' => 'promotionCombinationID']);
            $table->addColumn('promotionID', 'integer', ['limit' => 20,'default' => null, 'null' => true]);
            $table->addColumn('combinationName', 'string', ['limit' => 200]);
            $table->addColumn('conditionType', 'string', ['limit' => 200]);
            $table->addColumn('discountType', 'integer', ['limit' => 20]);
            $table->addColumn('discountAmount', 'decimal', ['default' => 0, 'precision' => 20, 'scale' => 5]);
            $table->save();
        }   

        $table1 = $this->hasTable('itemBaseCombinationRule');

        if (!$table1) {
            $table1 = $this->table('itemBaseCombinationRule', ['id' => 'itemBaseCombinationRuleID']);
            $table1->addColumn('combinationID', 'integer', ['limit' => 20,'default' => null, 'null' => true]);
            $table1->addColumn('productID', 'integer', ['limit' => 20,'default' => null, 'null' => true]);
            $table1->addColumn('rule', 'string', ['limit' => 200]);
            $table1->addColumn('quantity', 'integer', ['limit' => 20]);
            $table1->save();
        }      

        $table2 = $this->hasTable('attributeBaseCombinationRule');

        if (!$table2) {
            $table2 = $this->table('attributeBaseCombinationRule', ['id' => 'attributeBaseCombinationRuleID']);
            $table2->addColumn('combinationID', 'integer', ['limit' => 20,'default' => null, 'null' => true]);
            $table2->addColumn('attributeID', 'integer', ['limit' => 20,'default' => null, 'null' => true]);
            $table2->addColumn('rule', 'string', ['limit' => 200]);
            $table2->addColumn('attributeValueID', 'integer', ['limit' => 20,'default' => null, 'null' => true]);
            $table2->addColumn('quantity', 'integer', ['limit' => 20, 'default' => null, 'null' => true]);
            $table2->save();
        }        

    }
}
