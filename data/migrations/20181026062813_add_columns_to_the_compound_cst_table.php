<?php

use Phinx\Migration\AbstractMigration;

class AddColumnsToTheCompoundCstTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        // add column to the compoundCostTemplate table
        $compoundCostTable = $this->table('compoundCostTemplate');
        $compoundCostColumn = $compoundCostTable->hasColumn('compoundCostTemplateCostCalType');

        if (!$compoundCostColumn) {
            $compoundCostTable->addColumn('compoundCostTemplateCostCalType', 'integer', ['default' => 1,])
              ->update();
        }

        // add column to the compoundCostValue table
        $compoundCostValueTable = $this->table('compoundCostValue');
        $compoundCostValueColumn = $compoundCostValueTable->hasColumn('supplierID');

        if (!$compoundCostValueColumn) {
            $compoundCostValueTable->addColumn('supplierID', 'integer', ['default' => null, 'null' => true])
              ->update();
        }

        // add column to the compoundCostItem table
        $compoundCostItemTable = $this->table('compoundCostItem');
        $compoundCostItemColumn = $compoundCostItemTable->hasColumn('supplierID');

        if (!$compoundCostItemColumn) {
            $compoundCostItemTable->addColumn('compoundCostItemWiseTotalCost', 'decimal',['default' => null, 'null' => true, 'precision' => 20, 'scale' => 5])
                ->addColumn('supplierID', 'integer', ['default' => null, 'null' => true])
              ->update();
        }
    }
}
