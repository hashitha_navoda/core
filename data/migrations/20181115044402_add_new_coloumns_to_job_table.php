<?php

use Phinx\Migration\AbstractMigration;

class AddNewColoumnsToJobTable extends AbstractMigration {
	/**
	 * Change Method.
	 *
	 * Write your reversible migrations using this method.
	 *
	 * More information on writing migrations is available here:
	 * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
	 *
	 * The following commands can be used in this method and Phinx will
	 * automatically reverse them when rolling back:
	 *
	 *    createTable
	 *    renameTable
	 *    addColumn
	 *    renameColumn
	 *    addIndex
	 *    addForeignKey
	 *
	 * Remember to call "create()" or "update()" and NOT "save()" when working
	 * with the Table class.
	 */
	public function change() {
		$table = $this->table('job');
		$column1 = $table->hasColumn('contactorID');

		if (!$column1) {
			$table->addColumn('contactorID', 'integer', ['default' => null, 'null' => true])
				->update();
		}

		$column2 = $table->hasColumn('jobDueDate');

		if (!$column2) {
			$table->addColumn('jobDueDate', 'date', ['default' => null, 'null' => true])
				->update();
		}

		$column3 = $table->hasColumn('jobEstimatedEndDate');

		if (!$column3) {
			$table->addColumn('jobEstimatedEndDate', 'date', ['default' => null, 'null' => true])
				->update();
		}

		$column4 = $table->hasColumn('customerProfileID');
		if ($column4) {
			$table->changeColumn('customerProfileID', 'integer', ['default' => null, 'null' => true])
				->save();
		}
	}
}
