<?php

use Phinx\Migration\AbstractMigration;

class AddDataToNewlyCreatedColumsInInvoiceProductTable extends AbstractMigration
{
    
    public function change()
    {
        /*$result = $this->fetchAll('SELECT * FROM `salesInvoice` INNER JOIN `salesInvoiceProduct` ON `salesInvoice`.salesInvoiceID = `salesInvoiceProduct`.salesInvoiceID WHERE `deliveryNoteID` IS NOT NULL');
        foreach ($result as $key => $value) {
            $deliveryNoteID = $value['deliveryNoteID'];
            $salesInvoiceProductID = $value['salesInvoiceProductID'];
            $deliveryNoteProducts = $this->fetchAll('SELECT * FROM `deliveryNote` INNER JOIN `deliveryNoteProduct` ON `deliveryNote`.deliveryNoteID = `deliveryNoteProduct`.deliveryNoteID WHERE `deliveryNote`.`deliveryNoteID`='.$deliveryNoteID);
            foreach ($deliveryNoteProducts as $key1 => $value1) {
                if($value['productID'] == $value1['productID']){
                    $locationProducts = $this->fetchAll('SELECT * FROM `locationProduct`  WHERE productID='.$value['productID'].' AND locationID='.$value['locationID'])[0];
                    $locationProductID = $locationProducts['locationProductID'];
                    $this->execute("UPDATE `salesInvoiceProduct` SET `documentTypeID` = '4',`salesInvoiceProductDocumentID` = ".$deliveryNoteID.",`locationProductID` = ".$locationProductID." WHERE `salesInvoiceProduct`.`salesInvoiceProductID` = ".$salesInvoiceProductID.";");
                }
            }
        }

        $results = $this->fetchAll('SELECT * FROM `salesInvoice` INNER JOIN `salesInvoiceProduct` ON `salesInvoice`.salesInvoiceID = `salesInvoiceProduct`.salesInvoiceID WHERE `deliveryNoteID` IS NULL');
        foreach ($results as $key => $value) {
            $salesInvoiceProductID = $value['salesInvoiceProductID'];
            if($value['productID'] != null && $value['locationID']!= null && $salesInvoiceProductID != null){
                $locationProducts = $this->fetchAll('SELECT * FROM `locationProduct`  WHERE productID='.$value['productID'].' AND locationID='.$value['locationID'])[0];
                $locationProductID = $locationProducts['locationProductID'];
                $this->execute("UPDATE `salesInvoiceProduct` SET `locationProductID` = ".$locationProductID." WHERE `salesInvoiceProduct`.`salesInvoiceProductID` = ".$salesInvoiceProductID.";");
            }
        }*/

        $pdo = $this->getAdapter()->getConnection();
        $result = $this->fetchAll("SELECT * FROM `salesInvoice` INNER JOIN `salesInvoiceProduct` ON `salesInvoice`.salesInvoiceID = `salesInvoiceProduct`.salesInvoiceID WHERE `deliveryNoteID` IS NOT NULL");
        foreach ($result as $key => $value) {
            $deliveryNoteID = $value['deliveryNoteID'];
            $salesInvoiceProductID = $value['salesInvoiceProductID'];
            $deliveryNoteProducts = $this->fetchAll("SELECT * FROM `deliveryNote` INNER JOIN `deliveryNoteProduct` ON `deliveryNote`.deliveryNoteID = `deliveryNoteProduct`.deliveryNoteID WHERE `deliveryNote`.`deliveryNoteID`= {$deliveryNoteID}");
            foreach ($deliveryNoteProducts as $key1 => $value1) {
                $productID = $value['productID'];
                $locationID = $value['locationID'];
                if($productID == $value1['productID']){
                    $locationProducts = $this->fetchAll("SELECT * FROM `locationProduct`  WHERE productID={$productID} AND locationID={$locationID}")[0];
                    $locationProductID = $locationProducts['locationProductID'];
                    $updateQuery = <<<SQL
                            UPDATE `salesInvoiceProduct` SET 
                            `documentTypeID` = :documentTypeID,
                            `salesInvoiceProductDocumentID` = :deliveryNoteID,
                            `locationProductID` = :locationProductID
                             WHERE `salesInvoiceProduct`.`salesInvoiceProductID` = :salesInvoiceProductID;
SQL;
                    $pdo->prepare($updateQuery)->execute(array(
                        'documentTypeID' => '4', 
                        'deliveryNoteID' => $deliveryNoteID, 
                        'locationProductID' => $locationProductID, 
                        'salesInvoiceProductID' => $salesInvoiceProductID, 
                    ));
                }
            }
        }

        $results = $this->fetchAll("SELECT * FROM `salesInvoice` INNER JOIN `salesInvoiceProduct` ON `salesInvoice`.salesInvoiceID = `salesInvoiceProduct`.salesInvoiceID WHERE `deliveryNoteID` IS NULL");
        foreach ($results as $key => $value) {
            $salesInvoiceProductID = $value['salesInvoiceProductID'];
            $productID = $value['productID'];
            $locationID = $value['locationID'];
            if($productID != null && $locationID != null && $salesInvoiceProductID != null){
                $locationProducts = $this->fetchAll("SELECT * FROM `locationProduct`  WHERE productID={$productID} AND locationID={$locationID}")[0];
                $locationProductID = $locationProducts['locationProductID'];
                 $updateQuery = <<<SQL
                            UPDATE `salesInvoiceProduct` SET 
                            `locationProductID` = :locationProductID
                             WHERE `salesInvoiceProduct`.`salesInvoiceProductID` = :salesInvoiceProductID;
SQL;
                    $pdo->prepare($updateQuery)->execute(array(
                        'locationProductID' => $locationProductID, 
                        'salesInvoiceProductID' => $salesInvoiceProductID, 
                    ));
            }
        }


    }
}
