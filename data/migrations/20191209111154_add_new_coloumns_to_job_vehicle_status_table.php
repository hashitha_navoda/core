<?php

use Phinx\Migration\AbstractMigration;

class AddNewColoumnsToJobVehicleStatusTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table1 = $this->table('jobVehicleStatus');
        $column1 = $table1->hasColumn('oilFilter');
        if (!$column1) {
            $table1->addColumn('oilFilter', 'integer', ['limit' => 20, 'default' => null, 'null' => true])
              ->update();
        }

        $column2 = $table1->hasColumn('gearBoxOil');
        if (!$column2) {
            $table1->addColumn('gearBoxOil', 'integer', ['limit' => 20, 'default' => null, 'null' => true])
              ->update();
        }

        $column3 = $table1->hasColumn('differentialOil');
        if (!$column3) {
            $table1->addColumn('differentialOil', 'integer', ['limit' => 20, 'default' => null, 'null' => true])
              ->update();
        }

        $column4 = $table1->hasColumn('acCabinFilter');
        if (!$column4) {
            $table1->addColumn('acCabinFilter', 'integer', ['limit' => 20, 'default' => null, 'null' => true])
              ->update();
        }

        $column5 = $table1->hasColumn('fuelFilter');
        if (!$column5) {
            $table1->addColumn('fuelFilter', 'integer', ['limit' => 20, 'default' => null, 'null' => true])
              ->update();
        }

        $column6 = $table1->hasColumn('radiatorCoolantLevel');
        if (!$column6) {
            $table1->addColumn('radiatorCoolantLevel', 'integer', ['limit' => 20, 'default' => null, 'null' => true])
              ->update();
        }

        $column7 = $table1->hasColumn('powerSteeringOil');
        if (!$column7) {
            $table1->addColumn('powerSteeringOil', 'integer', ['limit' => 20, 'default' => null, 'null' => true])
              ->update();
        }

        $column8 = $table1->hasColumn('radiatorHosesdriveBelts');
        if (!$column8) {
            $table1->addColumn('radiatorHosesdriveBelts', 'integer', ['limit' => 20, 'default' => null, 'null' => true])
              ->update();
        }

        $column9 = $table1->hasColumn('driveBelts');
        if (!$column9) {
            $table1->addColumn('driveBelts', 'integer', ['limit' => 20, 'default' => null, 'null' => true])
              ->update();
        }

        $column10 = $table1->hasColumn('engineMount');
        if (!$column10) {
            $table1->addColumn('engineMount', 'integer', ['limit' => 20, 'default' => null, 'null' => true])
              ->update();
        }

        $column11 = $table1->hasColumn('shockMounts');
        if (!$column11) {
            $table1->addColumn('shockMounts', 'integer', ['limit' => 20, 'default' => null, 'null' => true])
              ->update();
        }

        $column12 = $table1->hasColumn('silencerMounts');
        if (!$column12) {
            $table1->addColumn('silencerMounts', 'integer', ['limit' => 20, 'default' => null, 'null' => true])
              ->update();
        }

        $column13 = $table1->hasColumn('axleBoots');
        if (!$column13) {
            $table1->addColumn('axleBoots', 'integer', ['limit' => 20, 'default' => null, 'null' => true])
              ->update();
        }

        $column14 = $table1->hasColumn('headLamps');
        if (!$column14) {
            $table1->addColumn('headLamps', 'integer', ['limit' => 20, 'default' => null, 'null' => true])
              ->update();
        }

        $column15 = $table1->hasColumn('signalLamps');
        if (!$column15) {
            $table1->addColumn('signalLamps', 'integer', ['limit' => 20, 'default' => null, 'null' => true])
              ->update();
        }

        $column16 = $table1->hasColumn('parkingLamps');
        if (!$column16) {
            $table1->addColumn('parkingLamps', 'integer', ['limit' => 20, 'default' => null, 'null' => true])
              ->update();
        }

        $column17 = $table1->hasColumn('fogLamps');
        if (!$column17) {
            $table1->addColumn('fogLamps', 'integer', ['limit' => 20, 'default' => null, 'null' => true])
              ->update();
        }

        $column18 = $table1->hasColumn('tailLamp');
        if (!$column18) {
            $table1->addColumn('tailLamp', 'integer', ['limit' => 20, 'default' => null, 'null' => true])
              ->update();
        }

        $column19 = $table1->hasColumn('roomLamp');
        if (!$column19) {
            $table1->addColumn('roomLamp', 'integer', ['limit' => 20, 'default' => null, 'null' => true])
              ->update();
        }

        $column20 = $table1->hasColumn('wScreenWasherFluid');
        if (!$column20) {
            $table1->addColumn('wScreenWasherFluid', 'integer', ['limit' => 20, 'default' => null, 'null' => true])
              ->update();
        }

        $column21 = $table1->hasColumn('rackBoots');
        if (!$column21) {
            $table1->addColumn('rackBoots', 'integer', ['limit' => 20, 'default' => null, 'null' => true])
              ->update();
        }

        $column22 = $table1->hasColumn('brakePadsLiners');
        if (!$column22) {
            $table1->addColumn('brakePadsLiners', 'integer', ['limit' => 20, 'default' => null, 'null' => true])
              ->update();
        }

        $column23 = $table1->hasColumn('tyreWareUneven');
        if (!$column23) {
            $table1->addColumn('tyreWareUneven', 'integer', ['limit' => 20, 'default' => null, 'null' => true])
              ->update();
        }

        $column24 = $table1->hasColumn('wheelNutsStuds');
        if (!$column24) {
            $table1->addColumn('wheelNutsStuds', 'integer', ['limit' => 20, 'default' => null, 'null' => true])
              ->update();
        }

        $column25 = $table1->hasColumn('airFilter');
        if ($column25) {
            $table1->changeColumn('airFilter', 'integer', ['limit' => 20, 'default' => null, 'null' => true])
              ->save();
        }

        $column26 = $table1->hasColumn('engineOil');
        if ($column26) {
            $table1->changeColumn('engineOil', 'integer', ['limit' => 20, 'default' => null, 'null' => true])
              ->save();
        }

        $column27 = $table1->hasColumn('wiperFluid');
        if ($column27) {
            $table1->changeColumn('wiperFluid', 'integer', ['limit' => 20, 'default' => null, 'null' => true])
              ->save();
        }

        $column28 = $table1->hasColumn('wiperBlades');
        if ($column28) {
            $table1->changeColumn('wiperBlades', 'integer', ['limit' => 20, 'default' => null, 'null' => true])
              ->save();
        }

        $column29 = $table1->hasColumn('tyres');
        if ($column29) {
            $table1->changeColumn('tyres', 'integer', ['limit' => 20, 'default' => null, 'null' => true])
              ->save();
        }

        $column30 = $table1->hasColumn('checkBelts');
        if ($column30) {
            $table1->changeColumn('checkBelts', 'integer', ['limit' => 20, 'default' => null, 'null' => true])
              ->save();
        }

        $column31 = $table1->hasColumn('coolant');
        if ($column31) {
            $table1->changeColumn('coolant', 'integer', ['limit' => 20, 'default' => null, 'null' => true])
              ->save();
        }

        $column32 = $table1->hasColumn('brakeFluid');
        if ($column32) {
            $table1->changeColumn('brakeFluid', 'integer', ['limit' => 20, 'default' => null, 'null' => true])
              ->save();
        }

        $column33 = $table1->hasColumn('steeringFluid');
        if ($column33) {
            $table1->changeColumn('steeringFluid', 'integer', ['limit' => 20, 'default' => null, 'null' => true])
              ->save();
        }

        $column34 = $table1->hasColumn('transmissionFluid');
        if ($column34) {
            $table1->changeColumn('transmissionFluid', 'integer', ['limit' => 20, 'default' => null, 'null' => true])
              ->save();
        }

        $column35 = $table1->hasColumn('battery');
        if ($column35) {
            $table1->changeColumn('battery', 'integer', ['limit' => 20, 'default' => null, 'null' => true])
              ->save();
        }

        
    }
}
