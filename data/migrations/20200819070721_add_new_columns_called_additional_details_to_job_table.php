<?php

use Phinx\Migration\AbstractMigration;

class AddNewColumnsCalledAdditionalDetailsToJobTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->table('job');
        $column = $table->hasColumn('jobAdditionalDetail1');

        if (!$column) {
            $table->addColumn('jobAdditionalDetail1', 'text', ['default' => null, 'null' => true])
              ->update();
        }

        $column1 = $table->hasColumn('jobAdditionalDetail2');

        if (!$column1) {
            $table->addColumn('jobAdditionalDetail2', 'text', ['default' => null, 'null' => true])
              ->update();
        }
    }
}
