<?php

use Phinx\Migration\AbstractMigration;

class AddNewTableCalledInquiry extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
      public function change()
      {
      }
     */

    /**
     * Migrate Up.
     */
    public function up()
    {
        $this->execute("CREATE TABLE IF NOT EXISTS `inquiry` (
  `inquiryId` INT NOT NULL,
  `customerId` INT NULL,
  `inquiryType` INT NULL,
  `inquiryCode` VARCHAR(100) NULL,
  `inquiryCreatedAt` DATETIME NULL,
  `inquiryCreatedBy` INT NULL,
  `locationId` INT NULL,
  PRIMARY KEY (`inquiryId`));");
    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }

}
