<?php

use Phinx\Migration\AbstractMigration;

class SetSelectedUomForInvoices extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    crea = :salesInvoiceProductIDteTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $pdo = $this->getAdapter()->getConnection();
        $select_invoices_qury = <<<SQL
            SELECT sp.salesInvoiceProductID, pu.uomID 
            FROM salesInvoiceProduct sp 
            JOIN productUom pu on sp.productID = pu.productID
            WHERE pu.productUomDisplay = 1
            AND sp.salesInvoiceProductSelectedUomId is null;
SQL;
        
        $invoices = $this->fetchAll($select_invoices_qury);

       foreach ($invoices as $inv) {
            $update_inv = <<<SQL
                UPDATE salesInvoiceProduct
                SET salesInvoiceProductSelectedUomId = :uomID
                WHERE salesInvoiceProductID = :salesInvoiceProductID;
SQL;
            $pdo->prepare($update_inv)->execute(array(
                'uomID' => $inv['uomID'],
                'salesInvoiceProductID' => $inv['salesInvoiceProductID']
            ));
        } 
    }
}
