<?php

use Phinx\Migration\AbstractMigration;

class AddNewColumnsToTheProjectAndJobTables extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
      public function change()
      {
      }
     */

    /**
     * Migrate Up.
     */
    public function up()
    {
        $this->execute("SET SESSION old_alter_table=1;ALTER TABLE `job` CHANGE `jobEstimatedTime` `jobEstimatedTime` INT( 20 ) NULL ;");
        $this->execute("SET SESSION old_alter_table=1;ALTER TABLE `job` ADD `entityID` INT NOT NULL ;");
        $this->execute("SET SESSION old_alter_table=1;ALTER TABLE `project` ADD `projectStatus` INT NOT NULL ;");
    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }

}
