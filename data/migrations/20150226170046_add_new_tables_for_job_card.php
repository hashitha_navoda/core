<?php

use Phinx\Migration\AbstractMigration;

class AddNewTablesForJobCard extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
      public function change()
      {
      }
     */

    /**
     * Migrate Up.
     */
    public function up()
    {
        $this->execute("CREATE TABLE IF NOT EXISTS `inquiryType` (
  `inquiryTypeId` INT NOT NULL,
  `inquiryTypeName` VARCHAR(200) NULL,
  `inquiryTypeCode` VARCHAR(100) NULL,
  PRIMARY KEY (`inquiryTypeId`));");

        $this->execute("CREATE TABLE IF NOT EXISTS `job` (
  `jobId` INT NOT NULL,
  `jobName` VARCHAR(200) NULL,
  `jobTypeId` INT NULL,
  `jobDescription` LONGTEXT NULL,
  `jobCustomerJobDetails` LONGTEXT NULL,
  `jobStartingTime` DATETIME NULL,
  `jobEndTime` DATETIME NULL,
  `jobEstimatedCost` DECIMAL(10,2) NULL,
  `projectId` INT NULL,
  `jobWeight` DECIMAL(3,2) NULL,
  `jobSortOrder` INT NULL,
  PRIMARY KEY (`jobId`));");

        $this->execute("CREATE TABLE IF NOT EXISTS `jobType` (
  `jobTypeId` INT NOT NULL,
  `jobTypeName` VARCHAR(200) NULL,
  `jobTypeCode` VARCHAR(200) NULL,
  PRIMARY KEY (`jobTypeId`));");

        $this->execute("CREATE TABLE IF NOT EXISTS `activity` (
  `activityId` INT NOT NULL,
  `activityType` INT NULL,
  `activityDescription` LONGTEXT NULL,
  `activityStartingTime` DATETIME NULL,
  `activityEndTime` DATETIME NULL,
  `activityEstimatedCost` DECIMAL(10,2) NULL,
  `jobId` INT NULL,
  `activityWeight` DECIMAL(3,2) NULL,
  `activitySortOrder` INT NULL,
  PRIMARY KEY (`activityId`));");

        $this->execute("CREATE TABLE IF NOT EXISTS `activityRawMaterial` (
  `activityRawMaterialId` INT NOT NULL,
  `activityId` INT NULL,
  `locationProductId` INT NULL,
  `activityRawMaterialQuantity` INT NULL,
  `activityRawMaterialCost` DECIMAL(10,2) NULL,
  PRIMARY KEY (`activityRawMaterialId`));");

        $this->execute("CREATE TABLE IF NOT EXISTS `activityFixedAsset` (
  `activityFixedAssetID` INT NOT NULL,
  `activityId` INT NULL,
  `locationProductId` INT NULL,
  `activityFixedAssetQuantity` INT NULL,
  `activityFixedAssetComment` LONGTEXT NULL,
  PRIMARY KEY (`activityFixedAssetID`));");

        $this->execute("CREATE TABLE IF NOT EXISTS `activityTemporaryItem` (
  `activityTemporaryItemID` INT NOT NULL,
  `temporaryItemId` INT NULL,
  `activityId` INT NULL,
  `activityTemporaryItemBatchCode` VARCHAR(50) NULL,
  `activityTemporaryItemSerialCode` VARCHAR(50) NULL,
  `activityTemporaryItemQuantity` INT NULL,
  `activityTemporaryItemDiscarded` TINYINT(1) NULL,
  PRIMARY KEY (`activityTemporaryItemID`));");

        $this->execute("CREATE TABLE IF NOT EXISTS `temporaryItem` (
  `temporaryItemId` INT NOT NULL,
  `temporaryItemName` VARCHAR(200) NULL,
  `temporaryItemCode` VARCHAR(100) NULL,
  `temporaryItemDescription` LONGTEXT NULL,
  `temporaryItemBarcode` VARCHAR(50) NULL,
  `temporaryItemSerial` TINYINT(1) NULL,
  `temporaryItemBatch` TINYINT(1) NULL,
  PRIMARY KEY (`temporaryItemId`));");

        $this->execute("CREATE TABLE IF NOT EXISTS `contractor` (
  `contractorId` INT NOT NULL,
  `contractorName` VARCHAR(200) NULL,
  `contractorTemp` TINYINT(1) NULL,
  PRIMARY KEY (`contractorId`));");

        $this->execute("CREATE TABLE IF NOT EXISTS `statusUpdate` (
  `statusUpdateId` INT NOT NULL,
  `jobId` INT NULL,
  `activityId` INT NULL,
  `status` DECIMAL(10) NULL,
  `comment` LONGTEXT NULL,
  PRIMARY KEY (`statusUpdateId`));");

        $this->execute("CREATE TABLE IF NOT EXISTS `activityContractor` (
  `activityContractorId` INT NOT NULL,
  `activityId` INT NULL,
  `contractorId` INT NULL,
  `activityContractorCost` DECIMAL(10,2) NULL,
  PRIMARY KEY (`activityContractorId`));");

        $this->execute("CREATE TABLE IF NOT EXISTS `costType` (
  `costTypeId` INT NOT NULL,
  `costTypeName` VARCHAR(200) NULL,
  `locationProductId` INT NULL,
  `costTypeMinimumCost` DECIMAL(10,2) NULL,
  `costTypeMaximumCost` DECIMAL(10,2) NULL,
  PRIMARY KEY (`costTypeId`));");

        $this->execute("CREATE TABLE IF NOT EXISTS `activityCostType` (
  `activityCostType` INT NOT NULL,
  `activityId` INT NULL,
  `costTypeId` INT NULL,
  `activityCostTypeEstimatedCost` DECIMAL(10,2) NULL,
  `activityCostTypeActualCost` DECIMAL(10,2) NULL,
  PRIMARY KEY (`activityCostType`));");

        $this->execute("CREATE TABLE IF NOT EXISTS `activityTemporaryItem` (
  `activityTemporaryItemID` INT NOT NULL,
  `temporaryItemId` INT NULL,
  `activityId` INT NULL,
  `activityTemporaryItemBatchCode` VARCHAR(50) NULL,
  `activityTemporaryItemSerialCode` VARCHAR(50) NULL,
  `activityTemporaryItemQuantity` INT NULL,
  `activityTemporaryItemDiscarded` TINYINT(1) NULL,
  PRIMARY KEY (`activityTemporaryItemID`));");
    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }

}
