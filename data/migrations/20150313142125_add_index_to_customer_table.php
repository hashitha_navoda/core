<?php

use Phinx\Migration\AbstractMigration;

class AddIndexToCustomerTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
      public function change()
      {
      }
     */

    /**
     * Migrate Up.
     */
    public function up()
    {
        $query = "SET SESSION old_alter_table=1;
        ALTER TABLE `customer` ADD INDEX ( `customerCode` ) ,
        ADD INDEX ( `customerName` ) ;";
        $this->execute($query);
    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }

}
