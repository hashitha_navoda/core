<?php

use Phinx\Migration\AbstractMigration;

class AddNewTableCalledContactDetail extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
      public function change()
      {
      }
     */

    /**
     * Migrate Up.
     */
    public function up()
    {
        $this->execute("CREATE TABLE IF NOT EXISTS `contactDetail` (
  `contactDetailId` INT NOT NULL,
  `address` LONGTEXT NULL,
  PRIMARY KEY (`contactDetailId`));
");
    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }

}
