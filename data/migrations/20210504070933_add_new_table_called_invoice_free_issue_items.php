<?php

use Phinx\Migration\AbstractMigration;

class AddNewTableCalledInvoiceFreeIssueItems extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $salesInvoiceFreeIssueItems = $this->hasTable('salesInvoiceFreeIssueItems');
        if (!$salesInvoiceFreeIssueItems) {
            $newTable = $this->table('salesInvoiceFreeIssueItems', ['id' => 'salesInvoiceFreeIssueItemsID']);
            $newTable->addColumn('invoiceID', 'integer', ['limit' => 20])
                    ->addColumn('mainProductID' , 'integer', ['limit' => 20])
                    ->addColumn('mainSalesInvoiceProductID' , 'integer', ['limit' => 20])
                    ->addColumn('freeProductID' , 'integer', ['limit' => 20])
                    ->addColumn('freeQuantity', 'decimal', ['precision' => 20, 'scale' => 5, 'default' => null, 'null' => true])
                    ->addColumn('salesInvoiceFreeItemSelectedUom', 'integer', ['limit' => 20])
                    ->addColumn('locationID', 'integer', ['limit' => 20])
                    ->save();
        }
    }
}
