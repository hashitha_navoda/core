<?php

use Phinx\Migration\AbstractMigration;

class CreateNewTableCalledDimension extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $deimensionTable = $this->hasTable('dimension');

        if (!$deimensionTable) {
            $table = $this->table('dimension', ['id' => 'dimensionId']);
            $table->addColumn('dimensionName', 'string', ['limit' => 200]);
            $table->addColumn('deleted', 'boolean', ['default' => 0]);
            $table->save();
        }
    }
}
