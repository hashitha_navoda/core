<?php

use Phinx\Migration\AbstractMigration;

class CreateNewTableCalledDebitNoteSubProduct extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
      public function change()
      {
      }
     */

    /**
     * Migrate Up.
     */
    public function up()
    {
        $this->execute("CREATE TABLE IF NOT EXISTS `debitNoteSubProduct` (
  `debitNoteSubProductID` int(11) NOT NULL AUTO_INCREMENT,
  `debitNoteProductID` int(11) NOT NULL,
  `productBatchID` int(11) DEFAULT NULL,
  `productSerialID` int(11) DEFAULT NULL,
  `debitNoteSubProductQuantity` int(11) NOT NULL,
  PRIMARY KEY (`debitNoteSubProductID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;");
    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }

}
