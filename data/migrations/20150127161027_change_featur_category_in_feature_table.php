<?php

use Phinx\Migration\AbstractMigration;

class ChangeFeaturCategoryInFeatureTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
      public function change()
      {
      }
     */

    /**
     * Migrate Up.
     */
    public function up()
    {
        $this->execute("UPDATE `feature` SET `featureCategory`='GRN' WHERE `featureController`='Inventory\\\Controller\\\GrnController' AND `featureAction`='preview';");
        $this->execute("UPDATE `feature` SET `featureCategory`='GRN' WHERE `featureController`='Inventory\\\Controller\\\API\\\Grn' AND `featureAction`='changeGrnStatus';");
        $this->execute("UPDATE `feature` SET `featureCategory`='GRN' WHERE `featureController`='Inventory\\\Controller\\\GrnController' AND `featureAction`='document';");
        $this->execute("UPDATE `feature` SET `featureCategory`='GRN' WHERE `featureController`='Inventory\\\Controller\\\API\\\Grn' AND `featureAction`='sendGrnEmail';");
        $this->execute("UPDATE `feature` SET `featureName`='List' WHERE `featureController`='Inventory\\\Controller\\\GrnController' AND `featureAction`='list';");
    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }

}
