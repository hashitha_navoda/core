<?php

use Phinx\Migration\AbstractMigration;

class EditCreditNoteProductTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
      public function change()
      {
      }
     */

    /**
     * Migrate Up.
     */
    public function up()
    {
        $this->execute("ALTER TABLE `creditNoteProduct` ADD `invoiceProductID` INT( 11 ) NOT NULL AFTER `creditNoteID` ;
ALTER TABLE `creditNoteProduct` ADD `creditNoteProductDiscount` DECIMAL( 12, 2 ) NULL AFTER `creditNoteProductPrice` ;
ALTER TABLE `creditNoteProduct` ADD `creditNoteProductDiscountType` VARCHAR( 100 ) NULL AFTER `creditNoteProductDiscount` ;
ALTER TABLE `creditNoteProduct` ADD `creditNoteProductTotal` DECIMAL( 12, 2 ) NOT NULL AFTER `creditNoteProductQuantity` ;
ALTER TABLE `creditNoteProduct` ADD `rackID` INT( 11 ) NULL AFTER `creditNoteProductTotal` ;
ALTER TABLE `creditNoteProduct` CHANGE `creditNoteProductID` `creditNoteProductID` INT( 11 ) NOT NULL AUTO_INCREMENT 
");
    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }

}
