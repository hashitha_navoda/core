<?php

use Phinx\Migration\AbstractMigration;

class AddColumnToTheSalesPersonTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
      public function change()
      {
      }
     */

    /**
     * Migrate Up.
     */
    public function up()
    {
        $table = $this->table('salesPerson');
        $rowExist = $table->hasColumn('userID');
        if (!$rowExist) {
            $table->addColumn('userID', 'integer', array('after' => 'salesPersonAddress', 'null' => 'allow'))
                    ->update();
        }
    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }

}
