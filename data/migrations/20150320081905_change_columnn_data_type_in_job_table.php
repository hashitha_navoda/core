<?php

use Phinx\Migration\AbstractMigration;

class ChangeColumnnDataTypeInJobTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
      public function change()
      {
      }
     */

    /**
     * Migrate Up.
     */
    public function up()
    {
        $this->execute("ALTER TABLE `job` CHANGE `jobWeight` `jobWeight` DECIMAL( 5, 2 ) NULL DEFAULT NULL ;");
        $this->execute("ALTER TABLE `job` CHANGE `jobProgress` `jobProgress` DECIMAL( 5, 2 ) NULL DEFAULT NULL ;");
    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }

}
