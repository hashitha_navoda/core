<?php

use Phinx\Migration\AbstractMigration;

class AddGetUomForProductFeature extends AbstractMigration
{

    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     */
    public function up()
    {
        $rows = $this->fetchAll("SELECT roleID FROM `role`");
        $this->execute("INSERT INTO `feature` (`featureID`, `featureName`, `featureController`, `featureAction`, `featureCategory`, `featureType`, `moduleID`) VALUES (NULL, 'Default', 'Inventory\\\Controller\\\ProductAPI', 'getProductUomList', 'Application', '1', '2');");
        $id = $this->fetchRow("select LAST_INSERT_ID()")[0];
        foreach ($rows as $row) {
            $roleID = $row['roleID'];
            $this->execute("INSERT INTO `roleFeature` (`roleFeatureID`, `roleID`, `featureID`, `roleFeatureEnabled`) VALUES (NULL, $roleID, $id,1);");
        }
    }

}
