<?php

use Phinx\Migration\AbstractMigration;

class UpdateIncomingPaymentTableCustomerIdDefaultAsZero extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
      public function change()
      {
      }
     */

    /**
     * Migrate Up.
     */
    public function up()
    {
        $this->execute("SET SESSION old_alter_table=1; ALTER TABLE `incomingPayment` CHANGE `customerID` `customerID` INT(11) NULL DEFAULT '0';");
        $this->execute("UPDATE `incomingPayment` SET `customerID` = 0 WHERE `customerID` IS NULL ;");
    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }

}
