<?php

use Phinx\Migration\AbstractMigration;

class CreateTableJobSupervisorAndJobOwner extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
      public function change()
      {
      }
     */

    /**
     * Migrate Up.
     */
    public function up()
    {
        $this->execute("DROP TABLE IF EXISTS `job`");

        $this->execute("CREATE TABLE IF NOT EXISTS `job` (
  `jobId` int(11) NOT NULL AUTO_INCREMENT,
  `jobName` VARCHAR(100) ,
  `customerID` int(11) NOT NULL,
  `jobTypeId` int(11),
  `jobDescription` LONGTEXT ,
  `jobCustomerJobDetails` LONGTEXT,
  `jobStartingTime` DATETIME,
  `jobEndTime` DATETIME,
  `jobEstimatedTime` int(20) NOT NULL,
  `jobEstimatedCost` DECIMAL(10,2),
  `projectId` int(11),
  `jobWeight` DECIMAL(3,2),
  `jobStation` VARCHAR(50),
  `jobReferenceNumber` VARCHAR(50),
     PRIMARY KEY (`jobId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;");


        $this->execute("CREATE TABLE IF NOT EXISTS `jobSupervisor` (
  `jobSupervisorTableID` int(11) NOT NULL AUTO_INCREMENT,
  `jobSupervisorID` int(11) NOT NULL,
  `referenceCode` VARCHAR(100) NOT NULL,
     PRIMARY KEY (`jobSupervisorTableID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;");

        $this->execute("CREATE TABLE IF NOT EXISTS `jobOwner` (
  `jobOwnerTableID` int(11) NOT NULL AUTO_INCREMENT,
  `jobOwnerID` int(11) NOT NULL,
  `referenceCode` VARCHAR(100) NOT NULL,
     PRIMARY KEY (`jobOwnerTableID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;");
    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }

}
