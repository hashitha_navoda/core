<?php

use Phinx\Migration\AbstractMigration;

class AddNewColumnsToDisplaySetupAndLocationProductTables extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table1 = $this->table('displaySetup');
        $column1 = $table1->hasColumn('useMrpSettings');

        $table2 = $this->table('locationProduct');
        $column2 = $table2->hasColumn('mrpType');
        $column3 = $table2->hasColumn('mrpPercentageValue');
        $column4 = $table2->hasColumn('mrpValue');

        if (!$column1) {
            $table1->addColumn('useMrpSettings', 'boolean', ['default' => false])
                ->update();
        }

        if (!$column2) {
            $table2->addColumn('mrpType', 'integer', ['default' => null, 'null' => true])
                ->update();
        }

        if (!$column3) {
            $table2->addColumn('mrpPercentageValue',  'decimal', ['precision' => 20, 'scale' => 5, 'default' => null, 'null' => true])
                ->update();
        }

        if (!$column4) {
            $table2->addColumn('mrpValue', 'decimal', ['precision' => 20, 'scale' => 5, 'default' => null, 'null' => true])
                ->update();
        }
    }
}
