<?php

use Phinx\Migration\AbstractMigration;

class UpdateSalesInvoiceTableStatusAccordingToInvoiceCancelEntityId extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
      public function change()
      {
      }
     */

    /**
     * Migrate Up.
     */
    public function up()
    {
        //fetch which deleted calumn 1
        $invoiceData = $this->fetchAll("SELECT * FROM `salesInvoice` INNER JOIN entity ON salesInvoice.entityID = entity.entityID WHERE `deleted`=1");

        //then updates salesInvoice table status to cancel(5)
        foreach ($invoiceData as $value) {
            $invoiceID = $value['salesInvoiceID'];
            if ($invoiceID != NULL) {
                $this->execute("UPDATE `salesInvoice` SET `statusID`= 5 WHERE `salesInvoiceID`=$invoiceID");
            }
        }
    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }

}
