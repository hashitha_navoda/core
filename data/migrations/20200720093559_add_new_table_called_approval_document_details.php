<?php

use Phinx\Migration\AbstractMigration;

class AddNewTableCalledApprovalDocumentDetails extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $approvalDocType = $this->hasTable('approvalDocumentDetails');
        if (!$approvalDocType) {
            $newTable = $this->table('approvalDocumentDetails', ['id' => 'approvalDocumentDetailID']);
            $newTable->addColumn('docID', 'integer', ['default' => null, 'null' => true])
                ->addColumn('isActive', 'boolean', ['default' => 0, 'null' => true])
                ->addColumn('isShow', 'boolean', ['default' => 0, 'null' => true])
                ->save();
        }
    }
}
