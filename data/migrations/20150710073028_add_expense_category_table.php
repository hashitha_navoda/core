<?php

use Phinx\Migration\AbstractMigration;

class AddExpenseCategoryTable extends AbstractMigration
{

    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     */
    public function up()
    {
        $this->execute("CREATE TABLE IF NOT EXISTS `expenseCategory` (
  `expenseCategoryId` int(11) NOT NULL AUTO_INCREMENT,
  `expenseCategoryName` varchar(255) NOT NULL,
  `expenseCategoryParentId` int(11) DEFAULT NULL,
  `expenseCategoryStatus` int(11) NOT NULL,
  `entityId` int(11) NOT NULL,
  PRIMARY KEY (`expenseCategoryId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;");
    }

}
