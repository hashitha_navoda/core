<?php

use Phinx\Migration\AbstractMigration;

class AddDeliveryChargeEnableToInvoice extends AbstractMigration
{

    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     */
//    public function change()
//    {
//
//    }

    public function up()
    {
        $this->execute("SET SESSION old_alter_table=1; ALTER TABLE  `salesInvoice` ADD  `salesInvoiceDeliveryChargeEnable` TINYINT NULL ;");
        $this->execute("UPDATE  `salesInvoice` SET  `salesInvoiceDeliveryChargeEnable` =  '1';");
    }

}
