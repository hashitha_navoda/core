<?php

use Phinx\Migration\AbstractMigration;

class AddNewColumCalledPaymentTermIdToPurchaseInvoiceTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
      public function change()
      {
      }
     */

    /**
     * Migrate Up.
     */
    public function up()
    {
        $this->execute("set session old_alter_table=1; ALTER TABLE `purchaseInvoice` ADD `paymentTermID` INT( 11 ) NULL AFTER `purchaseInvoicePayedAmount`;");
    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }

}
