<?php

use Phinx\Migration\AbstractMigration;

class CreateNewTableCalledDeliveryNoteEditLogDetails extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
    $Sql = <<<SQL
                CREATE TABLE IF NOT EXISTS `deliveryNoteEditLogDetails` (
                    `deliveryNoteEditLogDetailsID` int(11) NOT NULL,
                    `deliveryNoteEditLogDetailsOldState` varchar(155) DEFAULT NULL,
                    `deliveryNoteEditLogDetailsNewState` varchar(155) NOT NULL,
                    `deliveryNoteEditLogDetailsCategory` varchar(155) NOT NULL,
                    `deliveryNoteEditLogID` int(11) NOT NULL
                ) ENGINE=InnoDB DEFAULT CHARSET=latin1;

                ALTER TABLE `deliveryNoteEditLogDetails`
                ADD PRIMARY KEY (`deliveryNoteEditLogDetailsID`);
                ALTER TABLE `deliveryNoteEditLogDetails`
                MODIFY `deliveryNoteEditLogDetailsID` int(11) NOT NULL AUTO_INCREMENT;
SQL;

        $this->execute($Sql);

    }
}
