<?php

use Phinx\Migration\AbstractMigration;

class InsertSmsIncluded extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $pdo = $this->getAdapter()->getConnection();

        $dataArray = array(
            array(
                'smsIncludedId' => 1,
                'smsIncludedName' => 'Invoice Date',
            ),
            array(
                'smsIncludedId' => 2,
                'smsIncludedName' => 'Invoice Number',
            ),
            array(
                'smsIncludedId' => 3,
                'smsIncludedName' => 'Invoice Amount',
            ),
            array(
                'smsIncludedId' => 4,
                'smsIncludedName' => 'Due Date',
            ),
            array(
                'smsIncludedId' => 5,
                'smsIncludedName' => 'Sales Person',
            ),
            array(
                'smsIncludedId' => 6,
                'smsIncludedName' => 'Payment Date',
            ),
            array(
                'smsIncludedId' => 7,
                'smsIncludedName' => 'Paid Amount',
            ),
            array(
                'smsIncludedId' => 8,
                'smsIncludedName' => 'Total due amount',
            ),
            array(
                'smsIncludedId' => 9,
                'smsIncludedName' => 'Number of due invoices',
            ),
            array(
                'smsIncludedId' => 10,
                'smsIncludedName' => 'Invoice with due date',
            ),
            array(
                'smsIncludedId' => 11,
                'smsIncludedName' => 'Last invoice date',
            ),
            array(
                'smsIncludedId' => 12,
                'smsIncludedName' => 'Last invoice amount',
            ),
            array(
                'smsIncludedId' => 13,
                'smsIncludedName' => 'Salesperson of last invoice',
            ),
            array(
                'smsIncludedId' => 14,
                'smsIncludedName' => 'Cheque Date',
            ),
            array(
                'smsIncludedId' => 15,
                'smsIncludedName' => 'Cheque amount',
            ),
            array(
                'smsIncludedId' => 16,
                'smsIncludedName' => 'Cheque number',
            ),

        );

        foreach($dataArray as $type){

            $smsIncludedInsert = <<<SQL
                INSERT INTO `smsIncluded` 
                ( 
                    `smsIncludedId`, 
                    `smsIncludedName`
                ) VALUES (
                    :smsIncludedId, 
                    :smsIncludedName
                );
    
SQL;
    
            $pdo->prepare($smsIncludedInsert)->execute(array( 
                'smsIncludedId' => $type['smsIncludedId'], 
                'smsIncludedName' => $type['smsIncludedName']
            ));

        }
    }
}
