<?php

use Phinx\Migration\AbstractMigration;

class AddNewColoumnsToProjectTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->table('project');
        $column = $table->hasColumn('eventEstimatedCost');
        if (!$column) {
            $table->addColumn('eventEstimatedCost', 'decimal', ['default' => null, 'null' => true, 'precision' => 20, 'scale' => 5])
              ->update();
        }

        $table = $this->table('project');
        $column = $table->hasColumn('teamID');
        if (!$column) {
            $table->addColumn('teamID', 'integer', ['default' => null, 'null' => true])
              ->update();
        }
    }
}
