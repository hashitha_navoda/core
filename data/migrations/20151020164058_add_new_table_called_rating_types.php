<?php

use Phinx\Migration\AbstractMigration;

class AddNewTableCalledRatingTypes extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
      public function change()
      {
      }
     */

    /**
     * Migrate Up.
     */
    public function up()
    {
        $sql = <<<SQL
                CREATE TABLE IF NOT EXISTS `ratingTypes` (
  `ratingTypesId` int(11) NOT NULL AUTO_INCREMENT,
  `ratingTypesCode` varchar(30) NOT NULL,
  `ratingTypesName` varchar(100) NOT NULL,
  `ratingTypesDescription` text,
  `ratingTypesMethod` int(11) NOT NULL COMMENT '1 => ''Star Ratings'', 2 => ''Number Rating''',
  `ratingTypesMethodDefinition` text NOT NULL,
  `ratingTypesState` tinyint(1) NOT NULL COMMENT '1=>''active'',0=>''inactive''',
  `ratingTypesSize` int(11) NOT NULL,
  `entityID` int(11) NOT NULL,
  PRIMARY KEY (`ratingTypesId`),
  KEY `ratingTypesMethod` (`ratingTypesMethod`,`ratingTypesState`,`entityID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;
SQL;
        $this->execute($sql);
    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }

}
