<?php

use Phinx\Migration\AbstractMigration;

class InsertPosTemplateDetails extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $pdo = $this->getAdapter()->getConnection();

        $dataArray = array(
            array(
                'posTemplateDetailsID' => 1,
                'posTemplateDetailsAttribute' => 'Company logo',
                'posTemplateDetailsIsSelected' => 0,
                'posTemplateDetailsByDefaultSelected' => 0
            ),
            array(
                'posTemplateDetailsID' => 2,
                'posTemplateDetailsAttribute' => 'Company name',
                'posTemplateDetailsIsSelected' => 1,
                'posTemplateDetailsByDefaultSelected' => 1
            ),
            array(
                'posTemplateDetailsID' => 3,
                'posTemplateDetailsAttribute' => 'Company Address',
                'posTemplateDetailsIsSelected' => 0,
                'posTemplateDetailsByDefaultSelected' => 0
            ),
            array(
                'posTemplateDetailsID' => 4,
                'posTemplateDetailsAttribute' => 'Telephone number 1',
                'posTemplateDetailsIsSelected' => 0,
                'posTemplateDetailsByDefaultSelected' => 0
            ),
            array(
                'posTemplateDetailsID' => 5,
                'posTemplateDetailsAttribute' => 'Telephone number 2',
                'posTemplateDetailsIsSelected' => 0,
                'posTemplateDetailsByDefaultSelected' => 0
            ),
            array(
                'posTemplateDetailsID' => 6,
                'posTemplateDetailsAttribute' => 'Email',
                'posTemplateDetailsIsSelected' => 0,
                'posTemplateDetailsByDefaultSelected' => 0
            ),
            array(
                'posTemplateDetailsID' => 7,
                'posTemplateDetailsAttribute' => 'Fax Number',
                'posTemplateDetailsIsSelected' => 0,
                'posTemplateDetailsByDefaultSelected' => 0
            ),
            array(
                'posTemplateDetailsID' => 8,
                'posTemplateDetailsAttribute' => 'Company website',
                'posTemplateDetailsIsSelected' => 0,
                'posTemplateDetailsByDefaultSelected' => 0
            ),
            array(
                'posTemplateDetailsID' => 9,
                'posTemplateDetailsAttribute' => 'Company Postal Code',
                'posTemplateDetailsIsSelected' => 0,
                'posTemplateDetailsByDefaultSelected' => 0
            ),
            array(
                'posTemplateDetailsID' => 10,
                'posTemplateDetailsAttribute' => 'Company Tax Registration Number',
                'posTemplateDetailsIsSelected' => 0,
                'posTemplateDetailsByDefaultSelected' => 0
            ),
            array(
                'posTemplateDetailsID' => 11,
                'posTemplateDetailsAttribute' => 'Date and time',
                'posTemplateDetailsIsSelected' => 0,
                'posTemplateDetailsByDefaultSelected' => 0
            ),
            array(
                'posTemplateDetailsID' => 12,
                'posTemplateDetailsAttribute' => 'username',
                'posTemplateDetailsIsSelected' => 0,
                'posTemplateDetailsByDefaultSelected' => 0
            ),
            array(
                'posTemplateDetailsID' => 13,
                'posTemplateDetailsAttribute' => 'Invoice Code',
                'posTemplateDetailsIsSelected' => 1,
                'posTemplateDetailsByDefaultSelected' => 1
            ),
            array(
                'posTemplateDetailsID' => 14,
                'posTemplateDetailsAttribute' => 'Customer name',
                'posTemplateDetailsIsSelected' => 0,
                'posTemplateDetailsByDefaultSelected' => 0
            ),
            array(
                'posTemplateDetailsID' => 15,
                'posTemplateDetailsAttribute' => 'Customer Mobile Number',
                'posTemplateDetailsIsSelected' => 0,
                'posTemplateDetailsByDefaultSelected' => 0
            ),
            array(
                'posTemplateDetailsID' => 16,
                'posTemplateDetailsAttribute' => 'Line number',
                'posTemplateDetailsIsSelected' => 0,
                'posTemplateDetailsByDefaultSelected' => 0
            ),
            array(
                'posTemplateDetailsID' => 17,
                'posTemplateDetailsAttribute' => 'Item Code',
                'posTemplateDetailsIsSelected' => 0,
                'posTemplateDetailsByDefaultSelected' => 0
            ),
            array(
                'posTemplateDetailsID' => 18,
                'posTemplateDetailsAttribute' => 'Item Name',
                'posTemplateDetailsIsSelected' => 1,
                'posTemplateDetailsByDefaultSelected' => 1
            ),
            array(
                'posTemplateDetailsID' => 19,
                'posTemplateDetailsAttribute' => 'Unit Price',
                'posTemplateDetailsIsSelected' => 1,
                'posTemplateDetailsByDefaultSelected' => 1
            ),
            array(
                'posTemplateDetailsID' => 20,
                'posTemplateDetailsAttribute' => 'Qty',
                'posTemplateDetailsIsSelected' => 1,
                'posTemplateDetailsByDefaultSelected' => 1
            ),
            array(
                'posTemplateDetailsID' => 21,
                'posTemplateDetailsAttribute' => 'Unit Discount',
                'posTemplateDetailsIsSelected' => 0,
                'posTemplateDetailsByDefaultSelected' => 0
            ),
            array(
                'posTemplateDetailsID' => 22,
                'posTemplateDetailsAttribute' => 'Gross amount',
                'posTemplateDetailsIsSelected' => 1,
                'posTemplateDetailsByDefaultSelected' => 1
            ),
            array(
                'posTemplateDetailsID' => 23,
                'posTemplateDetailsAttribute' => 'Discount',
                'posTemplateDetailsIsSelected' => 1,
                'posTemplateDetailsByDefaultSelected' => 1
            ),
            array(
                'posTemplateDetailsID' => 24,
                'posTemplateDetailsAttribute' => 'Net amount',
                'posTemplateDetailsIsSelected' => 1,
                'posTemplateDetailsByDefaultSelected' => 1
            ),
            array(
                'posTemplateDetailsID' => 25,
                'posTemplateDetailsAttribute' => 'Payment method breakdown',
                'posTemplateDetailsIsSelected' => 0,
                'posTemplateDetailsByDefaultSelected' => 0
            ),
            array(
                'posTemplateDetailsID' => 26,
                'posTemplateDetailsAttribute' => 'Balance',
                'posTemplateDetailsIsSelected' => 0,
                'posTemplateDetailsByDefaultSelected' => 0
            ),
            array(
                'posTemplateDetailsID' => 27,
                'posTemplateDetailsAttribute' => 'Customer Total  Loyalty Points',
                'posTemplateDetailsIsSelected' => 0,
                'posTemplateDetailsByDefaultSelected' => 0
            ),
            array(
                'posTemplateDetailsID' => 28,
                'posTemplateDetailsAttribute' => 'Customer earned Loyalty points',
                'posTemplateDetailsIsSelected' => 0,
                'posTemplateDetailsByDefaultSelected' => 0
            ),
            array(
                'posTemplateDetailsID' => 29,
                'posTemplateDetailsAttribute' => 'Additional text',
                'posTemplateDetailsIsSelected' => 0,
                'posTemplateDetailsByDefaultSelected' => 0
            ),
            array(
                'posTemplateDetailsID' => 30,
                'posTemplateDetailsAttribute' => 'Powered by text',
                'posTemplateDetailsIsSelected' => 0,
                'posTemplateDetailsByDefaultSelected' => 0
            ),
        );

        foreach($dataArray as $type){

            $widgetTypeInsert = <<<SQL
                INSERT INTO `posTemplateDetails` 
                ( 
                    `posTemplateDetailsID`, 
                    `posTemplateDetailsAttribute`,
                    `posTemplateDetailsIsSelected`,
                    `posTemplateDetailsByDefaultSelected`
                ) VALUES (
                    :posTemplateDetailsID, 
                    :posTemplateDetailsAttribute,
                    :posTemplateDetailsIsSelected,
                    :posTemplateDetailsByDefaultSelected
                );
    
SQL;
    
            $pdo->prepare($widgetTypeInsert)->execute(array( 
                'posTemplateDetailsID' => $type['posTemplateDetailsID'], 
                'posTemplateDetailsAttribute' => $type['posTemplateDetailsAttribute'],
                'posTemplateDetailsIsSelected' => $type['posTemplateDetailsIsSelected'], 
                'posTemplateDetailsByDefaultSelected' => $type['posTemplateDetailsByDefaultSelected']
            ));

        }
    }
}
