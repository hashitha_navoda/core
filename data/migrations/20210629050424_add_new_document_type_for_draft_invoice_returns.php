<?php

use Phinx\Migration\AbstractMigration;

class AddNewDocumentTypeForDraftInvoiceReturns extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $this->execute("INSERT INTO `documentType` (`documentTypeID`, `documentTypeName`) VALUES ('49', 'Draft Invoice Return');");

        $singleRow1 = [
            'referenceNameID'    => 50,
            'referenceName'  => 'Draft Invoice Return',
            'referenceTypeID' => 0,
        ];

        $abc = $this->table('reference');
        $abc->insert($singleRow1);
        $abc->saveData();

        $singleRow1 = [
            'referenceNameID'    => 50,
            'referencePrefixCHAR'  => 'DFTINVRTN',
            'referencePrefixNumberOfDigits' => 4,
            'referencePrefixCurrentReference' => 1
        ];

        $table1 = $this->table('referencePrefix');
        $table1->insert($singleRow1);
        $table1->saveData();
    }
}
