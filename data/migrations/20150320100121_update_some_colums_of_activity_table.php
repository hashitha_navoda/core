<?php

use Phinx\Migration\AbstractMigration;

class UpdateSomeColumsOfActivityTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
      public function change()
      {
      }
     */

    /**
     * Migrate Up.
     */
    public function up()
    {
        $this->execute('SET SESSION old_alter_table=1;  ALTER TABLE `activity` CHANGE `activityWeight` `activityWeight` DECIMAL( 5, 2 ) NULL DEFAULT NULL ;');
        $this->execute("SET SESSION old_alter_table=1;  ALTER TABLE `activity` CHANGE `activityProgress` `activityProgress` DECIMAL( 5, 2 ) NOT NULL DEFAULT '0.00';");
    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }

}
