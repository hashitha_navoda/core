<?php

use Phinx\Migration\AbstractMigration;

class AddApiKeyColumnToCompanyTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
      public function change()
      {
      }
     */

    /**
     * Migrate Up.
     */
    public function up()
    {
        $this->execute("set session old_alter_table=1; ALTER TABLE `company` ADD `apiKey` VARCHAR( 255 ) NULL AFTER `companyAccountType` ");
    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }

}
