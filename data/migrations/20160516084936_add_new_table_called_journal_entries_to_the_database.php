<?php

use Phinx\Migration\AbstractMigration;

class AddNewTableCalledJournalEntriesToTheDatabase extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
         $Sql = <<<SQL
        CREATE TABLE IF NOT EXISTS `journalEntry` (
`journalEntryID` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `journalEntryCode` varchar(100) NOT NULL,
  `journalEntryDate` date NOT NULL,
  `journalEntryIsReverse` tinyint(1) NOT NULL,
  `journalEntryComment` varchar(200) NULL,
  `locationID` int(11) NOT NULL,
  `entityID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `journalEntryAccounts` (
`journalEntryAccountsID` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `journalEntryID` int(11) NOT NULL,
  `financeAccountsID` int(11) NOT NULL,
  `financeGroupsID` int(11) NULL,
  `journalEntryAccountsDebitAmount` double(15,2) NULL,
  `journalEntryAccountsCreditAmount` double(15,2) NULL,
  `journalEntryAccountsMemo` varchar(100) NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

SQL;

    $this->execute($Sql);
    }
}
