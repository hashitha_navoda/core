<?php

use Phinx\Migration\AbstractMigration;

class ChangeColumnsOnTemporaryProductTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
      public function change()
      {
      }
     */

    /**
     * Migrate Up.
     */
    public function up()
    {
        $this->execute("SET SESSION old_alter_table=1; ALTER TABLE `temporaryProduct` ADD `activityID` INT( 11 ) NOT NULL AFTER `temporaryProductID`;");
        $this->execute("SET SESSION old_alter_table=1; ALTER TABLE `activityBatch` CHANGE `activityID` `temporaryProductID` INT( 11 ) NOT NULL;");
        $this->execute("SET SESSION old_alter_table=1; ALTER TABLE `activitySerial` CHANGE `activityID` `temporaryProductID` INT( 11 ) NOT NULL;");
    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }

}
