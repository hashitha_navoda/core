<?php

use Phinx\Migration\AbstractMigration;

class AddNewColumCallPaymentMethodIdToCreditNotesPaymentsDetailsTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
      public function change()
      {
      }
     */

    /**
     * Migrate Up.
     */
    public function up()
    {
        $this->execute("CREATE TABLE IF NOT EXISTS `creditNotePaymentDetails` (
  `creditNotePaymentDetailsID` int(11) NOT NULL AUTO_INCREMENT,
  `paymentMethodID` int(11) NOT NULL,
  `creditNotePaymentID` int(11) NOT NULL,
  `creditNoteID` int(11) NOT NULL,
  `creditNotePaymentDetailsAmount` int(11) NOT NULL,
  PRIMARY KEY (`creditNotePaymentDetailsID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=34 ;");
    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }

}
