<?php

use Phinx\Migration\AbstractMigration;

class AddNewColumCalledQuotationProductCopiedQuantityInQuotationTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
      public function change()
      {
      }
     */

    /**
     * Migrate Up.
     */
    public function up()
    {
        $this->execute("SET SESSION old_alter_table=1; ALTER TABLE `quotationProduct` ADD `quotationProductCopiedQuantity` DECIMAL( 20, 5 ) NULL DEFAULT '0' AFTER `quotationProductCopied` ;");
    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }

}
