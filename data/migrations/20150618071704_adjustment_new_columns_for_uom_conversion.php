<?php

use Phinx\Migration\AbstractMigration;

class AdjustmentNewColumnsForUomConversion extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
      public function change()
      {
      }
     */

    /**
     * Migrate Up.
     */
    public function up()
    {
        $this->execute("SET SESSION old_alter_table=1; ALTER TABLE  `goodsIssueProduct` ADD  `goodsIssueUomID` INT NULL AFTER  `uomID` ;");
        $this->execute("SET SESSION old_alter_table=1; ALTER TABLE  `goodsIssueProduct` ADD  `uomConversionRate` DECIMAL( 12, 2 ) NOT NULL AFTER  `goodsIssueUomID` ;");
    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }

}
