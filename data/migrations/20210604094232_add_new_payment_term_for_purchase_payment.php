<?php

use Phinx\Migration\AbstractMigration;

class AddNewPaymentTermForPurchasePayment extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {

        $pdo = $this->getAdapter()->getConnection();
        $paymentTermLastRecord = $this->fetchAll("SELECT paymentTermID FROM `paymentTerm` ORDER BY paymentTermID DESC LIMIT 1");


        $dataSet = [];
        $id = floatval($paymentTermLastRecord[0]['paymentTermID']) +  1;
        $dataSet[] = [
            'paymentTermID' => $id,
            'paymentTermName' => '90Days LC',
            'paymentTermDescription' => NULL,
        ];

        foreach ($dataSet as $key1 => $value1) {

            $featureInsert = <<<SQL
                INSERT INTO `paymentTerm`
                (
                    `paymentTermID`,
                    `paymentTermName`,
                    `paymentTermDescription`
                ) VALUES (
                    :paymentTermID,
                    :paymentTermName,
                    :paymentTermDescription
                );

SQL;

            $pdo->prepare($featureInsert)->execute(array(
                'paymentTermID' => $value1['paymentTermID'],
                'paymentTermName' => $value1['paymentTermName'],
                'paymentTermDescription' => $value1['paymentTermDescription']
            ));
        }

    }
}
