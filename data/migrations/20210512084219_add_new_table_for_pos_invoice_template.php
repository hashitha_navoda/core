<?php

use Phinx\Migration\AbstractMigration;

class AddNewTableForPosInvoiceTemplate extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $posTemplateDetailsExist = $this->hasTable('posTemplateDetails');
        if (!$posTemplateDetailsExist) {
            $newTable = $this->table('posTemplateDetails', ['id' => 'posTemplateDetailsID']);
            $newTable->addColumn('posTemplateDetailsAttribute', 'string', ['limit' => 200, 'default' => null, 'null' => true])
                    ->addColumn('posTemplateDetailsIsSelected', 'boolean', ['default' => true, 'null' => true])
                    ->addColumn('posTemplateDetailsByDefaultSelected', 'boolean', ['default' => true, 'null' => true])
                    ->save();
        }
    }
}
