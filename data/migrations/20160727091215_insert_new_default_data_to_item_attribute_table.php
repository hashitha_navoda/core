<?php

use Phinx\Migration\AbstractMigration;

class InsertNewDefaultDataToItemAttributeTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $pdo = $this->getAdapter()->getConnection();

        // create new entity ID
        $rows = $this->fetchAll("SELECT userID FROM `user` WHERE roleID = 1");
            $userID = $rows[0]['userID'];         
            $timeStamp = date( "Y-m-d H:i:s");
                  

            $entityInsert = <<<SQL
                INSERT INTO `entity` 
                (
                    `createdBy`, 
                    `createdTimeStamp`, 
                    `updatedBy`, 
                    `updatedTimeStamp`, 
                    `deleted`, 
                    `deletedBy`,
                    `deletedTimeStamp`
                ) VALUES (
                    :createdBy, 
                    :createdTimeStamp, 
                    :updatedBy, 
                    :updatedTimeStamp, 
                    :deleted, 
                    :deletedBy,
                    :deletedTimeStamp
                );
SQL;

            $pdo->prepare($entityInsert)->execute(array(
                'createdBy' => $userID, 
                'createdTimeStamp' => $timeStamp, 
                'updatedBy' => $userID, 
                'updatedTimeStamp' =>  $timeStamp, 
                'deleted' => 0, 
                'deletedBy' => NULL,
                'deletedTimeStamp' => NULL
            ));

        $entityID = $this->fetchRow("select LAST_INSERT_ID()")[0];
        
        $existingAttrDetails = null;
        // check ItemAttr exists or not 
        $existingAttrDetails = $this->fetchAll('SELECT * from `itemAttribute` WHERE `itemAttributeID` = 1');
        if($existingAttrDetails){
            //add new records to itemAttribute table
            $itemAttrInsert = <<<SQL
                INSERT INTO `itemAttribute` 
                (
                    `itemAttributeCode`, 
                    `itemAttributeName`, 
                    `itemAttributeState`, 
                    `entityID` 
                ) VALUES (
                    :itemAttributeCode, 
                    :itemAttributeName, 
                    :itemAttributeState, 
                    :entityID 
                );
SQL;

            $pdo->prepare($itemAttrInsert)->execute(array(
                'itemAttributeCode' => $existingAttrDetails['0']['itemAttributeCode'], 
                'itemAttributeName' => $existingAttrDetails['0']['itemAttributeName'], 
                'itemAttributeState' => $existingAttrDetails['0']['itemAttributeState'], 
                'entityID' =>  $existingAttrDetails['0']['entityID']
                
            ));
            $itemAttributeID = $this->fetchRow("select LAST_INSERT_ID()")[0];

            $existingAttrValueDetails = null;
            $existingAttrValueDetails = $this->fetchAll(
                'SELECT * from `itemAttributeValue` WHERE `itemAttributeID` = 1');
            if($existingAttrValueDetails){
                //update itemAttribute value table
                foreach ($existingAttrValueDetails as $key => $value) {
                    $valueData = $value['itemAttributeValueID'];
                    $updateAttrValue = $this->execute("UPDATE `itemAttributeValue` SET `itemAttributeID` = '".$itemAttributeID."'
                        WHERE `itemAttributeValueID`= '".$valueData."'");
                }
            }
            $existingAttrmapDetails = null;
            $existingAttrmapDetails = $this->fetchAll(
                'SELECT * from `itemAttributeMap` WHERE `itemAttributeID` =  1');
            if($existingAttrmapDetails){
                //update itemAttributeMap table
                foreach ($existingAttrmapDetails as $key => $mapValue) {
                    $valueData = $mapValue['itemAttributeMapID'];
                    $updateAttrMap = $this->execute("UPDATE `itemAttributeMap` SET `itemAttributeID` = '".$itemAttributeID."'
                        WHERE `itemAttributeMapID`= '".$valueData."'");
                }
                
            }
            //update itemAttribute table
            $attributeUpdate = $this->execute("UPDATE `itemAttribute` SET 
                `itemAttributeCode` = 'CP',
                `itemAttributeName` = 'Credit Period',
                `itemAttributeState` = '1',
                `entityID` = '".$entityID."'
                 WHERE `itemAttributeID`= '1'");


        } else {
            //insert new record to the ItemAttribute table
            $attributeInsert = <<<SQL
                    INSERT INTO `itemAttribute` 
                    (
                        `itemAttributeID`, 
                        `itemAttributeCode`, 
                        `itemAttributeName`, 
                        `itemAttributeState`, 
                        `entityID` 
                    ) VALUES (
                        :itemAttributeID, 
                        :itemAttributeCode, 
                        :itemAttributeName, 
                        :itemAttributeState, 
                        :entityID
                    );
        
SQL;
    
            $pdo->prepare($attributeInsert)->execute(array(
                'itemAttributeID' => 1, 
                'itemAttributeCode' => 'CP', 
                'itemAttributeName' => "Credit Period", 
                'itemAttributeState' => 1, 
                'entityID' => $entityID,
            ));

        }

        
    }
}
