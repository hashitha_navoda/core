<?php

use Phinx\Migration\AbstractMigration;

class RenameCustomerLoyaltyHistoryTableToIncomingPaymentMethodLoyaltyCard extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
      public function change()
      {
      }
     */

    /**
     * Migrate Up.
     */
    public function up()
    {
        $this->execute("RENAME TABLE customerLoyaltyHistory TO incomingPaymentMethodLoyaltyCard;");
        $this->execute("SET SESSION old_alter_table=1;ALTER TABLE incomingPaymentMethodLoyaltyCard CHANGE customerLoyaltyHistoryID incomingPaymentMethodLoyaltyCardId INT auto_increment");
    }

    /**
     * Migrate Down.
     */
    public function down()
    {
        $this->execute("RENAME TABLE incomingPaymentMethodLoyaltyCard TO customerLoyaltyHistory ;");
        $this->execute("SET SESSION old_alter_table=1;ALTER TABLE customerLoyaltyHistory CHANGE incomingPaymentMethodLoyaltyCardId customerLoyaltyHistoryID  INT auto_increment");
    }

}
