<?php

use Phinx\Migration\AbstractMigration;

class AddNewColumnCalledStatementBalanceToReconciliationTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
    public function change()
    {
    }
    */
    
    /**
     * Migrate Up.
     */
    public function up()
    {
        //add reconciliationStatementBalance column
        $this->execute("SET SESSION old_alter_table=1; "
                . " ALTER TABLE  `reconciliation` ADD  `reconciliationStatementBalance` DECIMAL( 10, 2 ) NOT NULL DEFAULT  '0.00' AFTER  `reconciliationDate` ;");
        
        //role list
        $roles = $this->fetchAll("SELECT roleID FROM `role`");
        
        //reconciliation feature list
        $featureList = array(
            array('name' =>'Default','controller'=>'Expenses\\\Controller\\\API\\\ReconciliationAPI','action'=>'calculateTransactionAmount','category'=>'Reconciliation','type'=>'1','moduleId'=>'9')
        );
        
        foreach ($featureList as $feature){            
            //insert feature to feature table
            $this->execute("INSERT INTO `feature` (`featureID`, `featureName`, `featureController`, `featureAction`, `featureCategory`, `featureType`, `moduleID`) VALUES (NULL,'".$feature['name']."', '".$feature['controller']."', '".$feature['action']."', '".$feature['category']."', '".$feature['type']."', '".$feature['moduleId']."');");
            $id = $this->fetchRow("select LAST_INSERT_ID()")[0];
            
            foreach ($roles as $role) {
                $roleID = $role['roleID'];
                if ($roleID == 1) {
                    $enabled = 1;
                } else {
                    //for enable api calls for every role
                    if($feature['type'] === '1'){
                        $enabled = 1;
                    } else {
                        $enabled = 0;
                    }
                }
                $this->execute("INSERT INTO `roleFeature` (`roleFeatureID`, `roleID`, `featureID`, `roleFeatureEnabled`) VALUES (NULL, $roleID, $id, $enabled);");
            }
        }
    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }
}