<?php

use Phinx\Migration\AbstractMigration;

class ChangeSomeTablePropertiesOfEmployeeTableColumns extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function up()
    {
        $table = $this->table('employee');
        $column = $table->hasColumn('employeeFirstName');
        $column1 = $table->hasColumn('employeeSecondName');
        $column2 = $table->hasColumn('employeeAddress');
        if ($column) {
            $table->changeColumn('employeeFirstName', 'string', ['limit' => 100, 'null' => true])
              ->save();
        }

        if ($column1) {
            $table->changeColumn('employeeSecondName', 'string', ['limit' => 100, 'null' => true])
              ->save();
        }
        if ($column2) {
            $table->changeColumn('employeeAddress', 'text', ['null' => true])
              ->save();
        }

    }
}
