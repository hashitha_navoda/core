<?php

use Phinx\Migration\AbstractMigration;

class CreateNewTableCalledDebitNoteProduct extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
      public function change()
      {
      }
     */

    /**
     * Migrate Up.
     */
    public function up()
    {
        $this->execute("CREATE TABLE IF NOT EXISTS `debitNoteProduct` (
  `debitNoteProductID` int(11) NOT NULL AUTO_INCREMENT,
  `debitNoteID` int(11) NOT NULL,
  `purchaseInvoiceProductID` int(11) NOT NULL,
  `productID` int(11) NOT NULL,
  `debitNoteProductPrice` decimal(12,2) DEFAULT '0.00',
  `debitNoteProductDiscount` decimal(12,2) DEFAULT '0.00',
  `debitNoteProductDiscountType` varchar(100) DEFAULT NULL,
  `debitNoteProductQuantity` int(11) DEFAULT '0',
  `debitNoteProductTotal` decimal(12,2) DEFAULT '0.00',
  `rackID` int(11) DEFAULT NULL,
  PRIMARY KEY (`debitNoteProductID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;");
    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }

}
