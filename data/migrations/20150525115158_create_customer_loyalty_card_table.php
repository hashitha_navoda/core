<?php

use Phinx\Migration\AbstractMigration;

class CreateCustomerLoyaltyCardTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
      public function change()
      {
      }
     */

    /**
     * Migrate Up.
     */
    public function up()
    {
        $this->execute("CREATE TABLE `customerLoyalty` (
          `customerLoyaltyID` int(11) NOT NULL AUTO_INCREMENT,
          `customerID` int(11) NOT NULL,
          `loyaltyID` int(11) NOT NULL,
          `customerLoyaltyCode` varchar(50) DEFAULT NULL,
          `customerLoyaltyPoints` int(30) DEFAULT NULL,
          `customerLoyaltyCreateDate` datetime,
          `customerLoyaltyStatus` int(5) DEFAULT NULL,
            PRIMARY KEY (`customerLoyaltyID`),
            FOREIGN KEY (`customerID`) REFERENCES customer(`customerID`),
            FOREIGN KEY (`loyaltyID`) REFERENCES loyalty(`loyaltyID`)
        ) ENGINE=InnoDB DEFAULT CHARSET=latin1");
    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }

}
