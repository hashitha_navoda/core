<?php

use Phinx\Migration\AbstractMigration;

class DeletePreviousInquiryTypeTableAndCreateNewInquiryTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
      public function change()
      {
      }
     */

    /**
     * Migrate Up.
     */
    public function up()
    {
        $this->dropTable('inquiryType');
        $this->execute("CREATE TABLE IF NOT EXISTS `inquiryType` (
                            `inquiryTypeID` int(11) NOT NULL AUTO_INCREMENT,
                            `inquiryTypeCode` varchar(200) NOT NULL,
                            `inquiryTypeName` varchar(200) DEFAULT NULL,
                            `entityID` int(11) NOT NULL,
                            PRIMARY KEY (`inquiryTypeID`)
                        ) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;");
    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }

}
