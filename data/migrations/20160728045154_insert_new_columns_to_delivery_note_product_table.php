<?php

use Phinx\Migration\AbstractMigration;

class InsertNewColumnsToDeliveryNoteProductTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->table('deliveryNoteProduct');
        $dtcolumn = $table->hasColumn('documentTypeID');
        $dpdcolumn = $table->hasColumn('deliveryNoteProductDocumentID');

        if (!$dtcolumn) {
            $table->addColumn('documentTypeID', 'integer', 
                array(
                    'precision' => 11,
                    'after' => 'deliveryNoteProductCopiedQuantity',
                    'default' => 0,
                    'null' => 'allow',
                    ))
                ->update();
        }

        if (!$dpdcolumn) {
            $table->addColumn('deliveryNoteProductDocumentID', 'integer', 
                array(
                    'precision' => 11,
                    'after' => 'documentTypeID',
                    'default' => 0,
                    'null' => 'allow',
                    ))
                ->update();
        }
    }
}
