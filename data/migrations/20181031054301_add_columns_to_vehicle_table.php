<?php

use Phinx\Migration\AbstractMigration;

class AddColumnsToVehicleTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $vehicleTable = $this->table('vehicle');

        //add vehicleName column
        $vehicleNameColumn = $vehicleTable->hasColumn('vehicleName');
        if (!$vehicleNameColumn) {
            $vehicleTable->addColumn('vehicleName', 'string', 
                [
                    'after' => 'vehicleId', 
                    'null' => true, 
                    'default' => 'NULL',
                ])
            ->update();
        }

        //add vehicleCode column
        $vehicleCodeColumn = $vehicleTable->hasColumn('vehicleCode');
        if (!$vehicleCodeColumn) {
            $vehicleTable->addColumn('vehicleCode', 'string', 
                [
                    'after' => 'vehicleId', 
                    'null' => true, 
                    'default' => 'NULL',
                    'limit' => 25
                ])
            ->update();
        }

        //add vehicleStatus column
        $vehicleStatusColumn = $vehicleTable->hasColumn('vehicleStatus');
        if (!$vehicleStatusColumn) {
            $vehicleTable->addColumn('vehicleStatus', 'integer', 
                [
                    'limit' => 2,
                    'default' => 1
                ])
            ->update();
        }
    }
}
