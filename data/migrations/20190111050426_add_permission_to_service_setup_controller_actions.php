<?php

use Phinx\Migration\AbstractMigration;

class AddPermissionToServiceSetupControllerActions extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $pdo = $this->getAdapter()->getConnection();
        $rows = $this->fetchAll("SELECT roleID FROM `role`");

        $dataArray = array(
            array(
                'featureID' => 'NULL',
                'featureName' => 'default',
                'featureController' => 'Jobs\Controller\ServiceSetup',
                'featureAction' => 'index',
                'featureCategory' => 'ServiceSetup',
                'featureType' => '0',
                'moduleID' => '11'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'default',
                'featureController' => 'Jobs\Controller\API\ServiceSetup',
                'featureAction' => 'add',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '11'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'default',
                'featureController' => 'Jobs\Controller\API\ServiceSetup',
                'featureAction' => 'getServicesBySearchKey',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '11'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'default',
                'featureController' => 'Jobs\Controller\API\ServiceSetup',
                'featureAction' => 'deleteServiceByServiceID',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '11'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'default',
                'featureController' => 'Jobs\Controller\API\ServiceSetup',
                'featureAction' => 'getRelatedSubTasks',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '11'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'default',
                'featureController' => 'Jobs\Controller\API\ServiceSetup',
                'featureAction' => 'getSubTaskByTaskId',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '11'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'default',
                'featureController' => 'Jobs\Controller\API\ServiceSetup',
                'featureAction' => 'changeSubTaskStatus',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '11'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'default',
                'featureController' => 'Jobs\Controller\API\ServiceSetup',
                'featureAction' => 'changeStatusID',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '11'
            ),
            array(
                'featureID' => 'NULL',
                'featureName' => 'default',
                'featureController' => 'Jobs\Controller\API\ServiceSetup',
                'featureAction' => 'deleteSubTask',
                'featureCategory' => 'Application',
                'featureType' => '1',
                'moduleID' => '11'
            ),
         );

        foreach($dataArray as $feature){

            $featureInsert = <<<SQL
                INSERT INTO `feature`
                (
                    `featureName`,
                    `featureController`,
                    `featureAction`,
                    `featureCategory`,
                    `featureType`,
                    `moduleID`
                ) VALUES (
                    :featureName,
                    :featureController,
                    :featureAction,
                    :featureCategory,
                    :featureType,
                    :featureModuleId
                );

SQL;

            $pdo->prepare($featureInsert)->execute(array(
                'featureName' => $feature['featureName'],
                'featureController' => $feature['featureController'],
                'featureAction' => $feature['featureAction'],
                'featureCategory' => $feature['featureCategory'],
                'featureType' => $feature['featureType'],
                'featureModuleId' => $feature['moduleID']
            ));

            $featureID = $this->fetchRow("select LAST_INSERT_ID()")[0];

            foreach ($rows as $row) {
                $roleID = $row['roleID'];

                if ($roleID == 1) {
                    $enabled = 1;
                } else {

                   //for enable api calls for every role
                    if($feature['featureType'] == 1){
                        $enabled = 1;
                    } else {
                        $enabled = 0;
                    }

                }

                $roleFeatureInsert =<<<SQL
                    INSERT INTO `roleFeature`
                    (
                        `roleID`,
                        `featureID`,
                        `roleFeatureEnabled`
                    ) VALUES(
                        :roleID,
                        :featureID,
                        :roleFeatureEnabled
                    )
SQL;
                $pdo->prepare($roleFeatureInsert)->execute(array(
                    'roleID' => $roleID,
                    'featureID' => $featureID,
                    'roleFeatureEnabled' => $enabled
                ));
            }
        }
    
    }
}
