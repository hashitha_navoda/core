<?php

use Phinx\Migration\AbstractMigration;

class AddNewTableCalledCreditNotePayment extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
      public function change()
      {
      }
     */

    /**
     * Migrate Up.
     */
    public function up()
    {
        $this->execute("CREATE TABLE IF NOT EXISTS `creditNotePayment` (
  `creditNotePaymentID` int(11) NOT NULL AUTO_INCREMENT,
  `creditNotePaymentCode` varchar(30) NOT NULL,
  `creditNotePaymentDate` date NOT NULL,
  `paymentTermID` int(11) NOT NULL,
  `creditNotePaymentAmount` decimal(12,2) NOT NULL,
  `customerID` int(11) NOT NULL,
  `creditNotePaymentDiscount` int(11) DEFAULT NULL,
  `creditNotePaymentMemo` text,
  `locationID` int(11) NOT NULL,
  `statusID` int(11) NOT NULL,
  `entityID` int(11) NOT NULL,
  PRIMARY KEY (`creditNotePaymentID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=43 ;");
    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }

}
