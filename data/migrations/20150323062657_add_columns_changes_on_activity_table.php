<?php

use Phinx\Migration\AbstractMigration;

class AddColumnsChangesOnActivityTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
      public function change()
      {
      }
     */

    /**
     * Migrate Up.
     */
    public function up()
    {
        $this->execute("SET SESSION old_alter_table=1; ALTER TABLE `activity` ADD `locationID` INT NOT NULL AFTER `activityId` ;");
        $this->execute("SET SESSION old_alter_table=1; ALTER TABLE `activity` ADD `activityStatus` INT NOT NULL DEFAULT '3' AFTER `activitySortOrder` ;");


        $this->execute("INSERT INTO `feature` (`featureID`, `featureName`, `featureController`, `featureAction`, `featureCategory`, `featureType`, `moduleID`) VALUES (NULL, 'Default', 'Core\\\Controller\\\JavascriptVariable', 'costTypes', 'Application', '1', '1');");
        $id1 = $this->fetchRow("select LAST_INSERT_ID()")[0];
        $this->execute("INSERT INTO `roleFeature` (`roleFeatureID` ,`roleID` ,`featureID` ,`roleFeatureEnabled`)
                                VALUES (NULL , '1', $id1, '1');");
    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }

}
