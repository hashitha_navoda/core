<?php

use Phinx\Migration\AbstractMigration;

class InsertDataForNewColumnsThatAddToFiscalPeriodTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $pdo = $this->getAdapter()->getConnection();
        $fiscalPeriods = $this->fetchAll("SELECT * FROM `fiscalPeriod`");


        if (sizeof($fiscalPeriods) > 0) {
            foreach ($fiscalPeriods as $key => $value) {
                $fiscalPeriodID = $value['fiscalPeriodID'];                
                if ($value['fiscalPeriodStatusID'] == 11) {
                    $this->execute("UPDATE `fiscalPeriod` SET `isCompleteStepOne` = true , `isCompleteStepTwo` = true WHERE `fiscalPeriodID` = $fiscalPeriodID "); 
                }
        

            }
        }
    }
}
