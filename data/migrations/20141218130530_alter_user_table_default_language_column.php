<?php

use Phinx\Migration\AbstractMigration;

class AlterUserTableDefaultLanguageColumn extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     *
     * Uncomment this method if you would like to use it.
     *
    public function change()
    {
    }
    */
    
    /**
     * Migrate Up.
     */
    public function up()
    {
    $this->execute("ALTER TABLE `user` CHANGE `defaultLanguage` `defaultLanguage` VARCHAR( 15 ) NOT NULL DEFAULT 'en_US';");
    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }
}