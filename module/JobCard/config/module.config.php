<?php

return array(
    'controllers' => array(
        'invokables' => array(
            'JobCard\Controller\JobType' => 'JobCard\Controller\JobTypeController',
            'JobCard\Controller\API\JobType' => 'JobCard\Controller\API\JobTypeController',
            'JobCard\Controller\Designations' => 'JobCard\Controller\DesignationsController',
            'JobCard\Controller\API\Designation' => 'JobCard\Controller\API\DesignationController',
            'JobCard\Controller\Divisions' => 'JobCard\Controller\DivisionsController',
            'JobCard\Controller\API\Division' => 'JobCard\Controller\API\DivisionController',
            'JobCard\Controller\Employees' => 'JobCard\Controller\EmployeesController',
            'JobCard\Controller\JobStation' => 'JobCard\Controller\JobStationController',
            'JobCard\Controller\API\JobStation' => 'JobCard\Controller\API\JobStationController',
            'JobCard\Controller\ProjectType' => 'JobCard\Controller\ProjectTypeController',
            'JobCard\Controller\API\ProjectType' => 'JobCard\Controller\API\ProjectTypeController',
            'JobCard\Controller\InquiryType' => 'JobCard\Controller\InquiryTypeController',
            'JobCard\Controller\API\InquiryType' => 'JobCard\Controller\API\InquiryTypeController',
            'JobCard\Controller\Employees' => 'JobCard\Controller\EmployeesController',
            'JobCard\Controller\API\Employee' => 'JobCard\Controller\API\EmployeeController',
            // 'JobCard\Controller\CostType' => 'JobCard\Controller\CostTypeController',
            // 'JobCard\Controller\API\CostType' => 'JobCard\Controller\API\CostTypeController',
            'JobCard\Controller\ActivityType' => 'JobCard\Controller\ActivityTypeController',
            'JobCard\Controller\API\ActivityType' => 'JobCard\Controller\API\ActivityTypeController',
            'JobCard\Controller\InquiryStatus' => 'JobCard\Controller\InquiryStatusController',
            'JobCard\Controller\API\InquiryStatus' => 'JobCard\Controller\API\InquiryStatusController',
            'JobCard\Controller\Job' => 'JobCard\Controller\JobController',
            'JobCard\Controller\Project' => 'JobCard\Controller\ProjectController',
            'JobCard\Controller\API\Job' => 'JobCard\Controller\API\JobController',
            'JobCard\Controller\Activity' => 'JobCard\Controller\ActivityController',
            'JobCard\Controller\API\Activity' => 'JobCard\Controller\API\ActivityController',
            'JobCard\Controller\API\Contractor' => 'JobCard\Controller\API\ContractorController',
            'JobCard\Controller\API\Project' => 'JobCard\Controller\API\ProjectController',
            'JobCard\Controller\API\TemporaryProduct' => 'JobCard\Controller\API\TemporaryProductController',
            'JobCard\Controller\Progress' => 'JobCard\Controller\ProgressController',
            'JobCard\Controller\API\Progress' => 'JobCard\Controller\API\ProgressController',
            'JobCard\Controller\InquiryLog' => 'JobCard\Controller\InquiryLogController',
            'JobCard\Controller\API\InquiryLog' => 'JobCard\Controller\API\InquiryLogController',
            'JobCard\Controller\UniversalSearch' => 'JobCard\Controller\UniversalSearchController',
            'JobCard\Controller\API\JobDispatch' => 'JobCard\Controller\API\JobDispatchController',
            'JobCard\Controller\JobDispatch' => 'JobCard\Controller\JobDispatchController',
            'JobCard\Controller\Contractor' => 'JobCard\Controller\ContractorController',
            'JobCard\Controller\InquirySetup' => 'JobCard\Controller\InquirySetupController',
            'JobCard\Controller\TemporaryContractor' => 'JobCard\Controller\TemporaryContractorController',
            'JobCard\Controller\API\TemporaryContractor' => 'JobCard\Controller\API\TemporaryContractorController',
            'JobCard\Controller\Vehicle' => 'JobCard\Controller\VehicleController',
            'JobCard\Controller\API\Vehicle' => 'JobCard\Controller\API\VehicleController',
        ),
    ),
    'router' => array(
        'routes' => array(
            'jobType' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/job-type[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'JobCard\Controller\JobType',
                        'action' => 'create',
                    ),
                ),
            ),
            'jobTypeApi' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/job-type-api[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param1' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param2' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'JobCard\Controller\API\JobType',
                    ),
                ),
            ),
            // 'designations' => array(
            //     'type' => 'segment',
            //     'options' => array(
            //         'route' => '/designations[/:action][/:param1][/:param2]',
            //         'constraints' => array(
            //             'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
            //         ),
            //         'defaults' => array(
            //             'controller' => 'JobCard\Controller\Designations',
            //             'action' => 'create',
            //         ),
            //     ),
            // ),
            'projectType' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/project-type[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'JobCard\Controller\ProjectType',
                        'action' => 'create',
                    ),
                ),
            ),
            'designationApi' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/designation_api[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param1' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param2' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'JobCard\Controller\API\Designation',
                    ),
                ),
            ),
            'projectTypeApi' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/project-type-api[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param1' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param2' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'JobCard\Controller\API\ProjectType',
                    ),
                ),
            ),
            'divisions' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/divisions[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'JobCard\Controller\Divisions',
                        'action' => 'create',
                    ),
                ),
            ),
            'divisionApi' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/division_api[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param1' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param2' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'JobCard\Controller\API\Division',
                    ),
                ),
            ),
            'employees' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/employees[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'JobCard\Controller\Employees',
                        'action' => 'create',
                    ),
                ),
            ),
            'employeeApi' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/employee_api[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param1' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param2' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'JobCard\Controller\API\Employee',
                    ),
                ),
            ),
            'activityType' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/activity-type[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'JobCard\Controller\ActivityType',
                        'action' => 'create',
                    ),
                ),
            ),
            'activityTypeApi' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/activity_type_api[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param1' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param2' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'JobCard\Controller\API\ActivityType',
                    ),
                ),
            ),
            'inquiryType' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/inquiry-type[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'JobCard\Controller\InquiryType',
                        'action' => 'create',
                    )
                ),
            ),
            'inquiryTypeAPI' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/api/inquiry-type[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param1' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param2' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'JobCard\Controller\API\InquiryType',
                    )
                ),
            ),
            // 'costType' => array(
            //     'type' => 'segment',
            //     'options' => array(
            //         'route' => '/cost-type[/:action][/:param1][/:param2]',
            //         'constraints' => array(
            //             'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
            //         ),
            //         'defaults' => array(
            //             'controller' => 'JobCard\Controller\CostType',
            //             'action' => 'create',
            //         ),
            //     ),
            // ),
            // 'costTypeApi' => array(
            //     'type' => 'segment',
            //     'options' => array(
            //         'route' => '/cost-type-api[/:action][/:param1][/:param2]',
            //         'constraints' => array(
            //             'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
            //             'param1' => '[a-zA-Z][a-zA-Z0-9_-]*',
            //             'param2' => '[a-zA-Z][a-zA-Z0-9_-]*',
            //         ),
            //         'defaults' => array(
            //             'controller' => 'JobCard\Controller\API\CostType',
            //         ),
            //     ),
            // ),
            'inquiryStatus' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/inquiry-status[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'JobCard\Controller\InquiryStatus',
                        'action' => 'create',
                    )
                ),
            ),
            'inquiryStatusAPI' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/api/inquiry-status[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param1' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param2' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'JobCard\Controller\API\InquiryStatus',
                    )
                ),
            ),
            'job' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/job[/:action][/:param1][/:param2][/:param3]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'JobCard\Controller\Job',
                        'action' => 'create',
                    ),
                ),),
            'jobApi' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/job-api[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param1' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param2' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'JobCard\Controller\API\Job',
                    ),
                ),
            ),
            'project' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/project[/:action][/:param1][/:param2][/:param3]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'JobCard\Controller\Project',
                        'action' => 'create',
                    )
                ),
            ),
            'activity' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/activity[/:action][/:param1][/:param2][/:param3]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'JobCard\Controller\Activity',
                        'action' => 'create',
                    )
                ),
            ),
            'activityAPI' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/api/activity[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param1' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param2' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'JobCard\Controller\API\Activity',
                    )
                ),
            ),
            'projectAPI' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/project-api[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param1' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param2' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'JobCard\Controller\API\Project',
                    )
                ),
            ),
            'jobStation' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/job-station[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'JobCard\Controller\JobStation',
                        'action' => 'create',
                    ),
                ),
            ),
            'jobStationApi' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/job-station-api[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'JobCard\Controller\API\JobStation',
                    ),
                ),
            ),
            'temporaryProductAPI' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/api/temporary-product[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param1' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param2' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'JobCard\Controller\API\TemporaryProduct',
                    ),
                ),
            ),
            'progress' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/progress[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param1' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param2' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'JobCard\Controller\Progress',
                    ),
                ),
            ),
            'progressApi' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/progress-api[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param1' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param2' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'JobCard\Controller\API\Progress',
                    ),
                ),
            ),
            'inquiryLog' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/inquiry-log[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'JobCard\Controller\InquiryLog',
                        'action' => 'create',
                    ),
                ),
            ),
            'inquiryLogApi' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/inquiry-log-api[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'JobCard\Controller\API\InquiryLog',
                    ),
                ),
            ),
            'universalSearch' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/universal-search[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'JobCard\Controller\UniversalSearch',
                        'action' => 'create',
                    ),
                ),
            ),
            'jobDispatch' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/job-dispatch[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'JobCard\Controller\JobDispatch',
                        'action' => 'index',
                    ),
                ),
            ),
            'jobDispatchAPI' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/api/job-dispatch[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'JobCard\Controller\API\JobDispatch',
                        'action' => 'index',
                    ),
                ),
            ),
            // 'contractor' => array(
            //     'type' => 'segment',
            //     'options' => array(
            //         'route' => '/contractor[/:action][/:param1][/:param2]',
            //         'constraints' => array(
            //             'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
            //         ),
            //         'defaults' => array(
            //             'controller' => 'JobCard\Controller\Contractor',
            //             'action' => 'create',
            //         ),
            //     ),
            // ),
            'contractorAPI' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/api/contractor[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param1' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param2' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'JobCard\Controller\API\Contractor',
                    )
                )
            ),
            'inquirySetup' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/inquiry-setup[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'JobCard\Controller\InquirySetup',
                        'action' => 'index',
                    ),
                ),
            ),
            'temporaryContractor' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/temporary-contractor[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'JobCard\Controller\TemporaryContractor',
                        'action' => 'index',
                    ),
                ),
            ),
            'temporaryContractorAPI' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/api/temporary-contractor[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'JobCard\Controller\API\TemporaryContractor',
                        'action' => 'index',
                    ),
                ),
            ),
            'vehicle' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/vehicle[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'JobCard\Controller\Vehicle',
                        'action' => 'index',
                    ),
                ),
            ),
            'vehicle-api' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/vehicle-api[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'JobCard\Controller\API\Vehicle',
                        'action' => 'index',
                    ),
                ),
            ),
        ),
    ),
    'translator' => array(
        'translation_file_patterns' => array(
            array(
                'type' => 'gettext',
                'base_dir' => __DIR__ . '/../language',
                'pattern' => '%s.mo',
            ),
        ),
    ),
    'view_manager' => array(
        'template_path_stack' => array(
            'JobCard' => __DIR__ . '/../view',
        ),
        'strategies' => array(
            'ViewJsonStrategy',
        ),
    ),
);
