<?php

namespace JobCard\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

/**
 * @author Sharmilan <sharmilan@thinkcube.com>
 * Inquiry Setup
 */
class InquirySetup
{

    public $inquirySetupID;
    public $inquirySetupName;
    public $inquirySetupStatus;

    public function exchangeArray($data)
    {
        $this->inquirySetupID = (!empty($data['inquirySetupID'])) ? $data['inquirySetupID'] : null;
        $this->inquirySetupName = (!empty($data['inquirySetupName'])) ? $data['inquirySetupName'] : null;
        $this->inquirySetupStatus = (!empty($data['inquirySetupStatus'])) ? $data['inquirySetupStatus'] : null;
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory = new InputFactory();

            $inputFilter->add($factory->createInput(array(
                        'name' => 'inquirySetupID',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'Int'),
                        ),
            )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'inquirySetupStatus',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'Int'),
                        ),
            )));

            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

}
