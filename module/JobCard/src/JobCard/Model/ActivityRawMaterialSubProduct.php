<?php

namespace JobCard\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class ActivityRawMaterialSubProduct
{

    public $activityRawMaterialSubProductID;
    public $activityRawMaterialId;
    public $productBatchID;
    public $productSerialID;
    public $activityRawMaterialSubProductQuantity;
    protected $inputFilter;

    public function exchangeArray($data)
    {
        $this->activityRawMaterialSubProductID = (!empty($data['activityRawMaterialSubProductID'])) ? $data['activityRawMaterialSubProductID'] : null;
        $this->activityRawMaterialId = (!empty($data['activityRawMaterialId'])) ? $data['activityRawMaterialId'] : null;
        $this->productBatchID = (!empty($data['productBatchID'])) ? $data['productBatchID'] : null;
        $this->productSerialID = (!empty($data['productSerialID'])) ? $data['productSerialID'] : null;
        $this->activityRawMaterialSubProductQuantity = (!empty($data['activityRawMaterialSubProductQuantity'])) ? $data['activityRawMaterialSubProductQuantity'] : null;
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

}
