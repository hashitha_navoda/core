<?php

/**
 * @author Ashan Madushka   <ashan@thinkcube.com>
 * This file contains Project Divisions Table Functions
 */

namespace JobCard\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Predicate\PredicateSet;
use Zend\Db\Sql\Predicate\Operator;

class ProjectDivisionsTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    //save project Divisions function
    public function saveProjectDivisions(ProjectDivisions $projectDivisions)
    {
        $projectData = array(
            'projectId' => $projectDivisions->projectId,
            'divisionId' => $projectDivisions->divisionId,
        );
        if ($this->tableGateway->insert($projectData)) {
            $result = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
            return $result;
        } else {
            return FALSE;
        }
    }

    //get project Divisions by projectID
    public function getProjectDivisionsByProjectID($projectID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('projectDivision')
                ->columns(array('*'))
                ->join('division', "projectDivision.divisionId = division.divisionID", '*', 'left')
                ->where(array('projectDivision.projectId' => $projectID));
        $query = $sql->prepareStatementForSqlObject($select);
        $rowset = $query->execute();
        if (!$rowset) {
            return null;
        }
        return $rowset;
    }

    //delete project division by project and division id
    public function deleteProjectDivisionsByProjectAndDivisionID($projectID, $divisionID)
    {
        try {
            $this->tableGateway->delete(array('projectId' => $projectID, 'divisionId' => $divisionID));
            return true;
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
            return false;
        }
    }

}
