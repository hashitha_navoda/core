<?php

namespace JobCard\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Predicate\PredicateSet;
use Zend\Db\Sql\Predicate\Operator;

class InquiryLogDutyManagerTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

//this function use to save new duty managers
    public function saveDutyManager($inquiryLogID, $dutyManagerList, $dutyManagerListLength)
    {
        for ($i = 0; $i < $dutyManagerListLength; $i++) {
            $temp = $dutyManagerList[$i];
            $inquiryLogData = array(
                'employeeID' => $temp,
                'inquiryLogID' => $inquiryLogID,
            );
            if ($this->tableGateway->insert($inquiryLogData)) {
                $result = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
            } else {
                return FALSE;
            }
        }
        return $result;
    }

//this function use to update existing duty managers related to the inquiry log
    public function updateDutyManager($id, $dutyManagerList, $dutyManagerListLength)
    {
        $removeResult = $this->tableGateway->delete(array('inquiryLogID' => (int) $id));
        for ($i = 0; $i < $dutyManagerListLength; $i++) {
            $temp = $dutyManagerList[$i];
            $dutyManagerData = array(
                'employeeID' => $temp,
                'inquiryLogID' => (int) $id,
            );
            if ($this->tableGateway->insert($dutyManagerData)) {
                $result = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
            } else {
                return FALSE;
            }
        }
        return $result;
    }

    /**
     *
     * @param type $ID
     * @return All inquiry log duty manager data that related to the Inquiry log ID
     */
    public function getInquiryLogDutyManagerNameByInquiryLogID($ID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('inquiryLogDutyManager')
                ->columns(array('*'))
                ->join('employee', 'inquiryLogDutyManager.employeeID=employee.employeeID', array("employeeFirstName", "employeeSecondName"), "left")
                ->where(array('inquiryLogDutyManager.inquiryLogID' => $ID));
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if (!$rowset) {
            return NULL;
        }
        return $rowset;
    }

//get Inquiry Duty manager Details that related to the given employee ID
    public function getInquiryLogDutyManagerDetailsByEmployeeID($ID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('inquiryLogDutyManager')
                ->columns(array('*'))
                ->where(array('inquiryLogDutyManager.employeeID' => $ID));
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if (!$rowset) {
            return NULL;
        }
        return $rowset;
    }

}
