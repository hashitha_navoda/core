<?php

namespace JobCard\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Predicate\PredicateSet;
use Zend\Db\Sql\Predicate\Operator;

class ContractorTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll($paginated = FALSE)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('contractor');
        $select->order('contractorFirstName ASC');
        $select->join('division', 'contractor.divisionID =  division.divisionID', array("divisionName"), "left");
        $select->join('designation', 'contractor.designationID =  designation.designationID', array("designationName"), "left");
        $select->join('entity', 'contractor.entityID = entity.entityID', array('deleted'));
        $select->where(array('entity.deleted' => '0'));
        if ($paginated) {
            $paginatorAdapter = new DbSelect(
                    $select, $this->tableGateway->getAdapter());
            $paginator = new Paginator($paginatorAdapter);
            return $paginator;
        } else {
            $statement = $sql->prepareStatementForSqlObject($select);
            $result = $statement->execute();
            return $result;
        }
    }

    public function saveContractor($contractor)
    {
        $contractorData = array(
            'contractorFirstName' => $contractor->contractorFirstName,
            'contractorSecondName' => $contractor->contractorSecondName,
            'designationID' => $contractor->designationID,
            'divisionID' => $contractor->divisionID,
            'contractorTP' => $contractor->contractorTP,
            'entityID' => $contractor->entityID
        );
        if ($this->tableGateway->insert($contractorData)) {
            $lastID = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
            $rowset = $this->tableGateway->select(array('contractorID' => $lastID));
            return $rowset->current();
        } else {
            return FALSE;
        }
    }

    public function getContractor($firstName, $secondName)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('contractor')
                ->columns(array('*'))
                ->join('entity', 'contractor.entityID = entity.entityID', array('deleted'))
                ->where(array('entity.deleted' => '0'))
                ->where(array('contractor.contractorFirstName' => $firstName, 'contractor.contractorSecondName' => $secondName));
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if (!$rowset) {
            return NULL;
        }
        return $rowset;
    }

    public function searchContractorForDropDown($searchKey)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('contractor');
        $select->join('entity', 'contractor.entityID=  entity.entityID', array("*"), "left");
        $select->where(new PredicateSet(array(new Operator('contractor.contractorFirstName', 'like', '%' . $searchKey . '%'), new Operator('contractor.contractorSecondName', 'like', '%' . $searchKey . '%')), PredicateSet::OP_OR));
        $select->where(new PredicateSet(array(new Operator('entity.deleted', '=', '0'))));
        $select->where(new PredicateSet(array(new Operator('contractor.contractorStatus', '=', '1'))));
        $select->limit(50);
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

    public function updateContractor(Contractor $contractor)
    {
        $contractorData = array(
            'designationID' => $contractor->designationID,
            'divisionID' => $contractor->divisionID,
            'contractorTP' => $contractor->contractorTP,
        );
        try {
            $this->tableGateway->update($contractorData, array('contractorID' => $contractor->contractorID));
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
        return true;
    }

    public function getContractorByID($contractorID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('contractor')
                ->columns(array('*'))
                ->join('entity', 'contractor.entityID = entity.entityID', array('deleted'))
                ->where(array('entity.deleted' => '0'))
                ->where(array('contractor.contractorID' => $contractorID));
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if (!$rowset) {
            return NULL;
        }
        return $rowset;
    }

    public function getContractorforSearch($contractorSearchKey)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('contractor');
        $select->join('division', 'contractor.divisionID =  division.divisionID', array("divisionName"), "left");
        $select->join('designation', 'contractor.designationID =  designation.designationID', array("designationName"), "left");
        $select->join('entity', 'contractor.entityID = entity.entityID', array('deleted'));
        $select->where(array('entity.deleted' => '0'));
        $select->where(new PredicateSet(array(new Operator('contractor.contractorFirstName', 'like', '%' . $contractorSearchKey . '%'), new Operator('contractor.contractorSecondName', 'like', '%' . $contractorSearchKey . '%')), PredicateSet::OP_OR));
        $select->order('contractorFirstName ASC');
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        return (object) $results;
    }

    public function updateContractorState($contractorID, $status)
    {
        $jobStationData = array(
            'contractorStatus' => $status,
        );
        try {
            $this->tableGateway->update($jobStationData, array('contractorID' => $contractorID));
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
        return true;
    }

}
