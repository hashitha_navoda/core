<?php

namespace JobCard\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Predicate\PredicateSet;
use Zend\Db\Sql\Predicate\Operator;

class TemporaryContractorTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll($paginated = FALSE)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('temporaryContractor');
        $select->order('temporarycontractorFirstName ASC');
        $select->join('division', 'temporaryContractor.divisionID=  division.divisionID', array("divisionName"), "left");
        $select->join('designation', 'temporaryContractor.designationID =  designation.designationID', array("designationName"), "left");
        if ($paginated) {
            $paginatorAdapter = new DbSelect(
                    $select, $this->tableGateway->getAdapter());
            $paginator = new Paginator($paginatorAdapter);
            return $paginator;
        } else {
            $statement = $sql->prepareStatementForSqlObject($select);
            $result = $statement->execute();
            return $result;
        }
    }

    public function saveTempContractor($tempContractor)
    {
        $tempContractorData = array(
            'temporaryContractorFirstName' => $tempContractor->temporaryContractorFirstName,
            'temporaryContractorSecondName' => $tempContractor->temporaryContractorSecondName,
            'designationID' => $tempContractor->designationID,
            'divisionID' => $tempContractor->divisionID,
            'temporaryContractorTP' => $tempContractor->temporaryContractorTP,
            'status' => $tempContractor->status,
        );

        if ($this->tableGateway->insert($tempContractorData)) {
            $lastID = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
            $rowset = $this->tableGateway->select(array('temporaryContractorID' => $lastID));
            return $rowset->current();
        } else {
            return FALSE;
        }
    }

    public function getTempContractor($firstName, $secondName)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('temporaryContractor')
                ->columns(array('*'))
                ->where(array('temporaryContractor.temporaryContractorFirstName' => $firstName, 'temporaryContractor.temporaryContractorSecondName' => $secondName));
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if (!$rowset) {
            return NULL;
        }
        return $rowset;
    }

    public function updateEmployees($employeesData)
    {
        $EmployeesData = array(
            'employeesFirstName' => $employeesData->employeesFirstName,
            'employeesSecondName' => $employeesData->employeesSecondName,
            'designationID' => $employeesData->designationID,
            'divisionID' => $employeesData->divisionID,
            'employeesTP' => $employeesData->employeesTP,
            'employeesHourlyCost' => $employeesData->employeesHourlyCost,
        );
        try {
            $this->tableGateway->update($EmployeesData, array('employeesID' => $employeesData->employeesID));
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
        return true;
    }

    public function deleteEmployeesByEmployeeID($employeesData)
    {
        try {
            $result = $this->tableGateway->delete(array('employeesID' => $employeesData->employeesID));
            return $result;
        } catch (\Exception $e) {
            $error = 0;
            return $error;
        }
    }

    public function EmployeeSearchByKey($keyword)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('employees')
                ->columns(array('*'));
        $select->where(new PredicateSet(array(new Operator('employees.employeesFirstName', 'like', '%' . $keyword . '%'), new Operator('employees.employeesSecondName', 'like', '%' . $keyword . '%')), PredicateSet::OP_OR));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return (object) $results;
    }

    public function searchTempContractorForDropdown($searchKey)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('temporaryContractor');
//        $select->join('entity', 'contractor.entityID=  entity.entityID', array("*"), "left");
//        $select->where(array("entity.deleted" => 0));
        $select->where->like('temporaryContractorFirstName', '%' . $searchKey . '%')
        ->or->like('temporaryContractorSecondName', '%' . $searchKey . '%');
        $select->limit(50);
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

    public function getTemporaryContractorforSearch($contractorSearchKey)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('temporaryContractor');
        $select->join('division', 'temporaryContractor.divisionID =  division.divisionID', array("divisionName"), "left");
        $select->join('designation', 'temporaryContractor.designationID =  designation.designationID', array("designationName"), "left");
        $select->where(new PredicateSet(array(new Operator('temporaryContractor.temporaryContractorFirstName', 'like', '%' . $contractorSearchKey . '%'),
            new Operator('temporaryContractor.temporaryContractorSecondName', 'like', '%' . $contractorSearchKey . '%')), PredicateSet::OP_OR));
        $select->order('temporaryContractorFirstName ASC');
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        return (object) $results;
    }

    public function getTemporaryContractorByID($tcontractorID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('temporaryContractor');
        $select->where(array('temporaryContractor.temporaryContractorID' => $tcontractorID));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        return (object) $results;
    }

    public function updateTemporaryContractor($tempContractor)
    {

        $tempContractorData = array(
            'designationID' => $tempContractor->designationID,
            'divisionID' => $tempContractor->divisionID,
            'temporaryContractorTP' => $tempContractor->temporaryContractorTP,
        );

        if ($this->tableGateway->update($tempContractorData, array('temporaryContractorID' => $tempContractor->temporaryContractorID))) {
            $rowset = $this->tableGateway->select(array('temporaryContractorID' => $tempContractor->temporaryContractorID));
            return $rowset->current();
        } else {
            return FALSE;
        }
    }

    public function deleteTemporaryContractor($temporaryContractorID)
    {
        try {
            $result = $this->tableGateway->delete(array('temporaryContractorID' => $temporaryContractorID));
            return $result;
        } catch (\Exception $e) {
            $error = 0;
            return $error;
        }
    }

    public function updateStatusTemporaryContractor($status, $temporaryContractorID)
    {
        try {
            $result = $this->tableGateway->update(array('status' => $status), array('temporaryContractorID' => $temporaryContractorID));
            return $result;
        } catch (\Exception $e) {
            $error = 0;
            return $error;
        }
    }

}
