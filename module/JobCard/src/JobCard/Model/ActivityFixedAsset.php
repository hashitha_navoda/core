<?php

namespace JobCard\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class ActivityFixedAsset
{

    public $activityFixedAssetId;
    public $activityId;
    public $locationProductId;
    public $activityFixedAssetQuantity;
    public $activityFixedAssetCost;

    public function exchangeArray($data)
    {
        $this->activityFixedAssetId = (!empty($data['activityFixedAssetId'])) ? $data['activityFixedAssetId'] : null;
        $this->activityId = (!empty($data['activityId'])) ? $data['activityId'] : null;
        $this->locationProductId = (!empty($data['locationProductId'])) ? $data['locationProductId'] : null;
        $this->activityFixedAssetQuantity = (!empty($data['activityFixedAssetQuantity'])) ? $data['activityFixedAssetQuantity'] : null;
        $this->activityFixedAssetCost = (!empty($data['activityFixedAssetCost'])) ? $data['activityFixedAssetCost'] : null;
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory = new InputFactory();

            $inputFilter->add($factory->createInput(array(
                        'name' => 'activityFixedAssetId',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'Int'),
                        ),
            )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'activityId',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'Int'),
                        ),
            )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'locationProductId',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'Int'),
                        ),
            )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'activityFixedAssetQuantity',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'StringLength',
                                'options' => array(
                                    'encoding' => 'UTF-8',
                                    'min' => 1,
                                    'max' => 1000,
                                ),
                            ),
                        ),
            )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'activityFixedAssetCost',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'StringLength',
                                'options' => array(
                                    'encoding' => 'UTF-8',
                                    'min' => 1,
                                    'max' => 1000,
                                ),
                            ),
                        ),
            )));
            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

}
