<?php

namespace JobCard\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Predicate\PredicateSet;
use Zend\Db\Sql\Predicate\Operator;

class DesignationTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll($paginated = FALSE)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('designation');
        $select->order('designationName DESC');
        $select->join('entity', 'designation.entityID = entity.entityID', array('deleted'));
        $select->where(array('deleted' => '0'));
        if ($paginated) {
            $paginatorAdapter = new DbSelect(
                    $select, $this->tableGateway->getAdapter());
            $paginator = new Paginator($paginatorAdapter);
            return $paginator;
        } else {
            $statement = $sql->prepareStatementForSqlObject($select);
            $result = $statement->execute();
            return $result;
        }
    }

    public function getAllDesignations()
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('designation')
                ->columns(array('*'))
                ->join('entity', 'designation.entityID=  entity.entityID', array("*"), "left")
                ->order('designationID ASC');
        $select->where(array('entity.deleted' => 0, 'designation.designationStatus' => '1'));
        $query = $sql->prepareStatementForSqlObject($select);
        $rowset = $query->execute();
        if (!$rowset) {
            return NULL;
        } else {
            return $rowset;
        }
    }

    public function getDesignations($designationName)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('designation')
                ->columns(array('designationName'))
                ->join('entity', 'designation.entityID=  entity.entityID', array("*"), "left")
                ->where(array('designation.designationName' => $designationName, "entity.deleted" => 0));
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if (!$rowset) {
            return NULL;
        }
        return $rowset;
    }

    public function getDesignationDetailByDesignationID($designationID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('designation')
                ->columns(array('*'))
                ->where(array('designation.designationID' => $designationID));
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if (!$rowset) {
            return NULL;
        }
        return $rowset;
    }

    public function saveDesignations($designationData)
    {
        $DesignationData = array(
            'designationName' => $designationData->designationName,
            'entityID' => $designationData->entityID,
            'designationStatus' => '1',
        );
        if ($this->tableGateway->insert($DesignationData)) {
            $result = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
            return $result;
        } else {
            return FALSE;
        }
    }

    public function updateDesignation($designationData)
    {
        $DestinationData = array(
            'designationName' => $designationData->designationName,
        );
        try {
            $this->tableGateway->update($DestinationData, array('designationID' => $designationData->designationID));
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
        return true;
    }

    public function deleteDesignationByDesignationID($designationData)
    {
        try {
            $result = $this->tableGateway->delete(array('designationID' => $designationData->designationID));
            return $result;
        } catch (\Exception $e) {
            $error = 0;
            return $error;
        }
    }

    public function DesignationSearchByKey($keyword)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('designation')
                ->columns(array('*'))
                ->join('entity', 'designation.entityID = entity.entityID', array('*'), 'left');
        $select->where(new PredicateSet(array(new Operator('designation.designationName', 'like', '%' . $keyword . '%'))));
        $select->where(new PredicateSet(array(new Operator('entity.deleted', '=', '0'))));

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return (object) $results;
    }

    public function updateDesignationState($designationID, $status)
    {
        $designationData = array(
            'designationStatus' => $status,
        );
        try {
            $this->tableGateway->update($designationData, array('designationID' => $designationID));
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
        return true;
    }

}
