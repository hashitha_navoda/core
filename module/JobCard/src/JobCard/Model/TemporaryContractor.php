<?php

namespace JobCard\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class TemporaryContractor
{

    public $temporaryContractorID;
    public $temporaryContractorFirstName;
    public $temporaryContractorSecondName;
    public $designationID;
    public $divisionID;
    public $temporaryContractorTP;
    public $status;

    public function exchangeArray($data)
    {
        $this->temporaryContractorID = (!empty($data['temporaryContractorID'])) ? $data['temporaryContractorID'] : null;
        $this->temporaryContractorFirstName = (!empty($data['temporaryContractorFirstName'])) ? $data['temporaryContractorFirstName'] : null;
        $this->temporaryContractorSecondName = (!empty($data['temporaryContractorSecondName'])) ? $data['temporaryContractorSecondName'] : null;
        $this->designationID = (!empty($data['designationID'])) ? $data['designationID'] : null;
        $this->divisionID = (!empty($data['divisionID'])) ? $data['divisionID'] : null;
        $this->temporaryContractorTP = (!empty($data['temporaryContractorTP'])) ? $data['temporaryContractorTP'] : null;
        $this->status = (!empty($data['status'])) ? $data['status'] : null;
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory = new InputFactory();

            $inputFilter->add($factory->createInput(array(
                        'name' => 'temporaryContractorID',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'Int'),
                        ),
            )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'temporaryContractorFirstName',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'StringLength',
                                'options' => array(
                                    'encoding' => 'UTF-8',
                                    'min' => 1,
                                    'max' => 100,
                                ),
                            ),
                        ),
            )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'temporaryContractorSecondName',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'StringLength',
                                'options' => array(
                                    'encoding' => 'UTF-8',
                                    'min' => 1,
                                    'max' => 100,
                                ),
                            ),
                        ),
            )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'designationID',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'Int'),
                        ),
            )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'divisionID',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'StringLength',
                                'options' => array(
                                    'encoding' => 'UTF-8',
                                    'min' => 1,
                                    'max' => 100,
                                ),
                            ),
                        ),
            )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'temporaryContractorTP',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'Int'),
                        ),
            )));

            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

}
