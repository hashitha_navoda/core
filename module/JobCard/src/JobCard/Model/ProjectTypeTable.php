<?php

/**
 * @author Ashan Madushka   <ashan@thinkcube.com>
 * This file contains Project Type Table Functions
 */

namespace JobCard\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Predicate\PredicateSet;
use Zend\Db\Sql\Predicate\Operator;

class ProjectTypeTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll($paginated = FALSE)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('projectType');
        $select->order('projectTypeName DESC');
        $select->join('entity', 'projectType.entityID = entity.entityID', array('deleted'));
        $select->where(array('deleted' => '0'));
        if ($paginated) {
            $paginatorAdapter = new DbSelect(
                    $select, $this->tableGateway->getAdapter());
            $paginator = new Paginator($paginatorAdapter);
            return $paginator;
        } else {
            $statement = $sql->prepareStatementForSqlObject($select);
            $result = $statement->execute();
            return $result;
        }
    }

    //save project type function
    public function saveProjectType(ProjectType $projectType)
    {
        $projectTypeData = array(
            'projectTypeName' => $projectType->projectTypeName,
            'projectTypeCode' => $projectType->projectTypeCode,
            'entityID' => $projectType->entityID,
            'projectTypeStatus' => '1',
        );
        if ($this->tableGateway->insert($projectTypeData)) {
            $result = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
            return $result;
        } else {
            return FALSE;
        }
    }

    //get project type by project type code
    public function getProjectTypeByProjectTypeCode($projectTybeCode)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('projectType')
                ->columns(array("*"))
                ->join('entity', 'projectType.entityID=  entity.entityID', array("*"), "left")
                ->where(array('projectType.projectTypeCode' => $projectTybeCode, "entity.deleted" => 0));
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if (!$rowset) {
            return NULL;
        }
        return $rowset;
    }

    //update project type by project type id
    public function updateProjectType(ProjectType $ProjectType)
    {
        $ProjectTypeData = array(
            'projectTypeName' => $ProjectType->projectTypeName,
        );
        try {
            $this->tableGateway->update($ProjectTypeData, array('projectTypeId' => $ProjectType->projectTypeID));
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
        return true;
    }

    //get project Type for search
    public function getProjectTypeforSearch($keyword)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('projectType')
                ->columns(array(
                    'posi' => new Expression('LEAST(IF(POSITION(\'' . $keyword . '\' in projectTypeName )>0,POSITION(\'' . $keyword . '\' in projectTypeName), 9999),'
                            . 'IF(POSITION(\'' . $keyword . '\' in projectTypeCode )>0,POSITION(\'' . $keyword . '\' in projectTypeCode), 9999)) '),
                    'len' => new Expression('LEAST(CHAR_LENGTH(projectTypeName ), CHAR_LENGTH(projectTypeCode )) '),
                    '*'))
                ->join('entity', 'projectType.entityID = entity.entityID', array('*'), 'left');
        $select->where(new PredicateSet(array(new Operator('projectType.projectTypeName', 'like', '%' . $keyword . '%'), new Operator('projectType.projectTypeCode', 'like', '%' . $keyword . '%')), PredicateSet::OP_OR));
        $select->where(new PredicateSet(array(new Operator('entity.deleted', '=', '0'))));
        $select->order(new Expression('IF(posi > 0, posi, 9999) ASC, len ASC'));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return (object) $results;
    }

    //delete project type by project type id
    public function deleteProjectTypeByProjectTypeID($projectTypeID)
    {
        try {
            $result = $this->tableGateway->delete(array('projectTypeId' => $projectTypeID));
            return $result;
        } catch (\Exception $e) {
            $error = 0;
            return $error;
        }
    }

    //get project type by project type id
    public function getProjectTypeByProjectTypeID($projectTybeID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('projectType')
                ->columns(array("*"))
                ->join('entity', 'projectType.entityID=  entity.entityID', array("*"), "left")
                ->where(array('projectType.projectTypeId' => $projectTybeID, "entity.deleted" => 0));
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if (!$rowset) {
            return NULL;
        }
        return $rowset;
    }

    /**
     * @author Ashan     <ashan@thinkcube.com>
     * search project types for dropdown
     */
    public function searchProjectTypesForDropDown($searchKey)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('projectType');
        $select->join('entity', 'projectType.entityID=  entity.entityID', array("*"), "left");
        $select->where(array("entity.deleted" => 0, 'projectType.projectTypeStatus' => 1));
        $select->where->like('projectTypeName', '%' . $searchKey . '%');
        $select->limit(50);

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

    /**
     * @author Lahiru Abeysinghe <lahiru@thinkcube.com>
     * @return boolean
     */
    public function updateProjectTypeState($projectTypeID, $status)
    {
        $ProjectTypeData = array(
            'projectTypeStatus' => $status,
        );
        try {
            $this->tableGateway->update($ProjectTypeData, array('projectTypeId' => $projectTypeID));
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
        return true;
    }

}
