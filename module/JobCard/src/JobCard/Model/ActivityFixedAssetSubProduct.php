<?php

namespace JobCard\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class ActivityFixedAssetSubProduct
{

    public $activityFixedAssetSubProductID;
    public $activityFixedAssetId;
    public $productBatchID;
    public $productSerialID;
    public $activityFixedAssetSubProductQuantity;

    public function exchangeArray($data)
    {
        $this->activityFixedAssetSubProductID = (!empty($data['activityFixedAssetSubProductID'])) ? $data['activityFixedAssetSubProductID'] : null;
        $this->activityFixedAssetId = (!empty($data['activityFixedAssetId'])) ? $data['activityFixedAssetId'] : null;
        $this->productBatchID = (!empty($data['productBatchID'])) ? $data['productBatchID'] : null;
        $this->productSerialID = (!empty($data['productSerialID'])) ? $data['productSerialID'] : null;
        $this->activityFixedAssetSubProductQuantity = (!empty($data['activityFixedAssetSubProductQuantity'])) ? $data['activityFixedAssetSubProductQuantity'] : null;
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

}
