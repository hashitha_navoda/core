<?php

/**
 * @author Sandun <sandun@thinkcube.com>
 */

namespace JobCard\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;

class ActivitySerialTable
{

    protected $tableGateway;
    public $adapter;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll()
    {
        $rowset = $this->tableGateway->select();
        return $rowset;
    }

    public function saveSerialProduct(ActivitySerial $activitySerial)
    {
        $serialData = array(
            'temporaryProductID' => $activitySerial->temporaryProductID,
            'activityBatchID' => $activitySerial->activityBatchID,
            'activitySerialCode' => $activitySerial->activitySerialCode,
            'activitySerialSold' => $activitySerial->activitySerialSold
        );
        $this->tableGateway->insert($serialData);
        $insertedID = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
        return $insertedID;
    }

    public function updateBySerialIDAndLocationProductID($data, $sId, $locProductID)
    {
        $result = $this->tableGateway->update($data, array('activitySerialID' => $sId));
        return $result;
    }

    public function getSerialByTempProductID($temporaryProductID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('activitySerial')
                ->columns(array('*'))
                ->where(array('temporaryProductID' => $temporaryProductID));
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if (!$rowset->current()) {
            return NULL;
        }

        return $rowset;
    }

    public function deleteActivitySerial($tempProductID)
    {
        try {
            $result = $this->tableGateway->delete(array('temporaryProductID' => $tempProductID));
            return $result;
        } catch (\Exception $e) {
            $error = 0;
            return $error;
        }
    }

}
