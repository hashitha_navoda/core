<?php

namespace JobCard\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Predicate\PredicateSet;
use Zend\Db\Sql\Predicate\Operator;

class TemporaryProductTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll($paginated = FALSE)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('temporaryProduct');
        $select->columns(array('*'));
        $select->order('temporaryProductName ASC');
        $select->join('entity', 'temporaryProduct.entityID = entity.entityID', array('deleted'));
        $select->where(array('deleted' => '0'));
        if ($paginated) {
            $paginatorAdapter = new DbSelect(
                    $select, $this->tableGateway->getAdapter());
            $paginator = new Paginator($paginatorAdapter);
            return $paginator;
        } else {
            $statement = $sql->prepareStatementForSqlObject($select);
            $result = $statement->execute();
            return $result;
        }
    }

    public function saveTemporaryProduct(TemporaryProduct $tempProduct)
    {
        $tempProductData = array(
            'temporaryProductCode' => $tempProduct->temporaryProductCode,
            'temporaryProductName' => $tempProduct->temporaryProductName,
            'temporaryProductDescription' => $tempProduct->temporaryProductDescription,
            'temporaryProductQuantity' => $tempProduct->temporaryProductQuantity,
            'temporaryProductPrice' => $tempProduct->temporaryProductPrice,
            'uomID' => $tempProduct->uomID,
            'temporaryProductImageURL' => $tempProduct->temporaryProductImageURL,
            'temporaryProductSerial' => $tempProduct->temporaryProductSerial,
            'temporaryProductBatch' => $tempProduct->temporaryProductBatch,
            'entityID' => $tempProduct->entityID,
        );
        if ($this->tableGateway->insert($tempProductData)) {
            $lastID = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
            return $lastID;
        } else {
            return FALSE;
        }
    }

    public function getTemporaryProductByCode($tempProductCode)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('temporaryProduct')
                ->columns(array('*'))
                ->where(array('temporaryProduct.temporaryProductCode' => $tempProductCode));
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if (!$rowset) {
            return NULL;
        }
        return $rowset;
    }

    /**
     * @author Sandun Dissanayake <sandun@thinkcube.com>
     * get all details of temporary products
     * @return $rowset
     */
    public function getTemporaryProductDetails()
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('temporaryProduct')
                ->columns(array('*'))
                ->join('entity', 'temporaryProduct.entityID = entity.entityID', array('deleted'))
                ->join('activitySerial', 'temporaryProduct.temporaryProductID = activitySerial.temporaryProductID', array(
                    'serialActivityBatchID' => new Expression('activitySerial.activityBatchID'), 'activitySerialCode', 'activitySerialSold', 'activitySerialID'))
                ->join('activityBatch', 'temporaryProduct.temporaryProductID = activityBatch.temporaryProductID', array('activityBatchID', 'activityBatchCode'))
                ->join('uom', 'temporaryProduct.uomID = uom.uomID', array('uomID', 'uomDecimalPlace', 'uomAbbr', 'uomState', 'uomName'))
                ->where(array('entity.deleted' => '0'));
        $query = $sql->prepareStatementForSqlObject($select);
        $rowset = $query->execute();
        return $rowset;
    }

    public function deleteTempProductByID($tempProductID)
    {
        try {
            $result = $this->tableGateway->delete(array('temporaryProductID' => $tempProductID));
            return $result;
        } catch (\Exception $e) {
            $error = 0;
            return $error;
        }
    }

    public function getTempProductByID($tempProductID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('temporaryProduct')
                ->columns(array('*'))
                ->where(array('temporaryProductID' => $tempProductID));
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if (!$rowset) {
            return NULL;
        }
        return $rowset;
    }

    //search temporary poduct by search key
    public function searchTemporaryProductsBySearchKeyWithJobID($tempProductSearchKey, $jobID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);

        $subQuery = $sql->select();
        $subQuery->from('activity')
                ->columns(array('activityId'))
                ->where(array('activity.jobid' => $jobID));

        $select = $sql->select();
        $select->from('temporaryProduct')
                ->columns(array('*'))
                ->join('activityTemporaryProduct', 'temporaryProduct.temporaryProductID=  activityTemporaryProduct.temporaryProductID', array("activityId"), "left")
                ->join('entity', 'temporaryProduct.entityID=  entity.entityID', array("deleted"), "left");

        $select->where->in('activityTemporaryProduct.activityId', $subQuery);
        $select->where(array('entity.deleted' => 0));
        $select->where(new PredicateSet(array(new Operator('temporaryProduct.temporaryProductCode', 'like', '%' . $tempProductSearchKey . '%'), new Operator('temporaryProduct.temporaryProductName', 'like', '%' . $tempProductSearchKey . '%'), new Operator('temporaryProduct.temporaryProductSerial', 'like', '%' . $tempProductSearchKey . '%'), new Operator('temporaryProduct.temporaryProductBatch', 'like', '%' . $tempProductSearchKey . '%')), PredicateSet::OP_OR));

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return (object) $results;
    }

    //search temporary poduct by search key
    public function searchTemporaryProductsBySearchKeyWithProjectID($tempProductSearchKey, $projectID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);

        $subQuery = $sql->select();
        $subQuery->from('job')
                ->columns(array('jobId'));
        $subQuery->where(array('job.projectId' => $projectID));

        $subQuery1 = $sql->select();
        $subQuery1->from('activity')
                ->columns(array('activityId'));
        $subQuery1->where->in('activity.jobid', $subQuery);

        $select = $sql->select();
        $select->from('temporaryProduct')
                ->columns(array('*'))
                ->join('activityTemporaryProduct', 'temporaryProduct.temporaryProductID=  activityTemporaryProduct.temporaryProductID', array("activityId"), "left")
                ->join('entity', 'temporaryProduct.entityID=  entity.entityID', array("deleted"), "left");
        $select->where->in('activityTemporaryProduct.activityId', $subQuery1);
        $select->where(array('entity.deleted' => 0));
        $select->where(new PredicateSet(array(new Operator('temporaryProduct.temporaryProductCode', 'like', '%' . $tempProductSearchKey . '%'), new Operator('temporaryProduct.temporaryProductName', 'like', '%' . $tempProductSearchKey . '%'), new Operator('temporaryProduct.temporaryProductSerial', 'like', '%' . $tempProductSearchKey . '%'), new Operator('temporaryProduct.temporaryProductBatch', 'like', '%' . $tempProductSearchKey . '%')), PredicateSet::OP_OR));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return (object) $results;
    }

    //get temporary product by job id
    public function getTemporaryProductsByJobID($jobID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);

        $subQuery = $sql->select();
        $subQuery->from('activity')
                ->columns(array('activityId'))
                ->where(array('activity.jobid' => $jobID));

        $select = $sql->select();
        $select->from('temporaryProduct')
                ->columns(array('*'))
                ->join('activityTemporaryProduct', 'temporaryProduct.temporaryProductID=  activityTemporaryProduct.temporaryProductID', array("activityId"), "left")
                ->join('entity', 'temporaryProduct.entityID=  entity.entityID', array("deleted"), "left");
        $select->where(array('entity.deleted' => 0));
        $select->where->in('activityTemporaryProduct.activityId', $subQuery);
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return (object) $results;
    }

    //get temporary product by project id
    public function getTemporaryProductsByProjectID($projectID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);

        $subQuery = $sql->select();
        $subQuery->from('job')
                ->columns(array('jobId'));
        $subQuery->where(array('job.projectId' => $projectID));

        $subQuery1 = $sql->select();
        $subQuery1->from('activity')
                ->columns(array('activityId'));
        $subQuery1->where->in('activity.jobid', $subQuery);

        $select = $sql->select();
        $select->from('temporaryProduct')
                ->columns(array('*'))
                ->join('activityTemporaryProduct', 'temporaryProduct.temporaryProductID=  activityTemporaryProduct.temporaryProductID', array("activityId"), "left")
                ->join('entity', 'temporaryProduct.entityID=  entity.entityID', array("deleted"), "left");
        $select->where(array('entity.deleted' => 0));
        $select->where->in('activityTemporaryProduct.activityId', $subQuery1);
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return (object) $results;
    }

    /**
     * get temporary product details with images by temporary product ID
     * @param type $temporaryProductID
     * @return type
     */
    public function getTemporaryProductDetailByID($temporaryProductID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('temporaryProduct')
                ->columns(array('*'))
                ->join('uom', 'temporaryProduct.uomID = uom.uomID', array('uomAbbr'), 'left')
                ->join('attachment', 'temporaryProduct.entityID = attachment.entityID', array('*'), 'left')
                ->where(array('temporaryProduct.temporaryProductID' => $temporaryProductID));

        $statment = $sql->prepareStatementForSqlObject($select);
        return $statment->execute();
    }

    public function getTemporaryProductBySearch($tempProductSearchKey = NULL)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('temporaryProduct')
                ->columns(array('*'))
                ->join('activityTemporaryProduct', 'temporaryProduct.temporaryProductID=  activityTemporaryProduct.temporaryProductID', array("activityId"), "left")
                ->join('entity', 'temporaryProduct.entityID=  entity.entityID', array("deleted"), "left");

        $select->where(array('entity.deleted' => 0));
        $select->where(new PredicateSet(array(
            new Operator('temporaryProduct.temporaryProductCode', 'like', '%' . $tempProductSearchKey . '%'),
            new Operator('temporaryProduct.temporaryProductName', 'like', '%' . $tempProductSearchKey . '%')), PredicateSet::OP_OR));

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return (object) $results;
    }

    public function getTemporaryProductWithActivity($tempProductId = \NULL, $isAllSelect = \FALSE)
    {
        $isAllSelect = filter_var($isAllSelect, FILTER_VALIDATE_BOOLEAN);
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('temporaryProduct')
                ->columns(array('*'))
                ->join('activityTemporaryProduct', 'temporaryProduct.temporaryProductID=  activityTemporaryProduct.temporaryProductID', array("activityId"), "left")
                ->join('entity', 'temporaryProduct.entityID=  entity.entityID', array("deleted"), "left")
                ->join('activity', 'activity.activityId = activityTemporaryProduct.activityId', array('activityCode'), 'left')
                ->join('uom', 'uom.uomID = temporaryProduct.uomID', array('uomAbbr'), 'left')
                ->where(array('entity.deleted' => 0));
        if (!$isAllSelect) {
            $select->where->in('temporaryProduct.temporaryProductID', $tempProductId);
        }
        $select->order("activity.activityId");

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        if (!$results) {
            return NULL;
        }
        return $results;
    }

}
