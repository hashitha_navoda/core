<?php

namespace JobCard\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;

/**
 * @author Sharmilan <sharmilan@thinkcube.com>
 * Inquiry Setup Table operations
 */
class InquirySetupTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll()
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('inquirySetup');
        $statement = $sql->prepareStatementForSqlObject($select);
        return $statement->execute();
    }

    /**
     * Update the value for inquirySetupID
     * @param type $inquirySetupID
     * @param type $inquirySetupStatus
     * @return boolean
     */
    public function updateEmailSendStatus($inquirySetupID = false, $inquirySetupStatus = 0)
    {
        if ($inquirySetupID) {
            $inquirySetupData = array(
                'inquirySetupStatus' => $inquirySetupStatus,
            );
            try {
                $this->tableGateway->update($inquirySetupData, array('inquirySetupID' => $inquirySetupID));
            } catch (Exception $exc) {
                echo $exc->getTraceAsString();
            }
            return true;
        } else {
            return false;
        }
    }

    /**
     * Get The status for an inquirySetupID
     * @param type $inquirySetupID
     * @return type
     */
    public function getStatusByInquirySetupID($inquirySetupID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('inquirySetup');
        $select->where->equalTo('inquirySetupID', $inquirySetupID);
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        $inquirySetupStatus = null;
        foreach ($result as $val) {
            $inquirySetupStatus = $val['inquirySetupStatus'];
        }
        return $inquirySetupStatus;
    }

}
