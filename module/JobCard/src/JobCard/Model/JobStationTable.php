<?php

namespace JobCard\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Predicate\PredicateSet;
use Zend\Db\Sql\Predicate\Operator;

class JobStationTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll($locationID, $paginated = FALSE)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('jobStation');
        $select->order('jobStationName DESC');
        $select->join('entity', 'jobStation.entityID=entity.entityID', array("deleted"), "left");
        $select->where(array('locationID' => $locationID, 'entity.deleted' => 0));
        if ($paginated) {
            $paginatorAdapter = new DbSelect(
                    $select, $this->tableGateway->getAdapter());
            $paginator = new Paginator($paginatorAdapter);
            return $paginator;
        } else {
            $statement = $sql->prepareStatementForSqlObject($select);
            $result = $statement->execute();
            return $result;
        }
    }

    public function getJobStation($jobStationName, $locationID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('jobStation')
                ->columns(array('*'))
                ->where(array('jobStation.jobStationName' => $jobStationName, 'jobStation.locationID' => $locationID));
        $select->join('entity', 'jobStation.entityID=entity.entityID', array("deleted"), "left");
        $select->where(array('locationID' => $locationID, 'entity.deleted' => 0));
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if (!$rowset) {
            return NULL;
        }
        return $rowset;
    }

//get job Station details that related to the given inputs
    public function getJobStationByNameAndID($jobStationName, $locationID, $ID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('jobStation')
                ->columns(array('*'))
                ->where(array('jobStation.jobStationName' => $jobStationName, 'jobStation.locationID' => $locationID));
        $select->where->notEqualTo('jobStation.jobStationID', $ID);
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if (!$rowset) {
            return NULL;
        }
        return $rowset;
    }

//this function gets all job station details that related to the given job station ID
    public function getJobStationByJobStationID($jobStationID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('jobStation')
                ->columns(array('*'))
                ->where(array('jobStation.jobStationID' => $jobStationID));
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if (!$rowset) {
            return NULL;
        }
        return $rowset;
    }

    public function saveJobStation($jobStationData)
    {
        $jStationData = array(
            'jobStationName' => $jobStationData->jobStationName,
            'jobStationMaxValue' => $jobStationData->jobStationMaxValue,
            'jobStationMinValue' => $jobStationData->jobStationMinValue,
            'locationID' => $jobStationData->locationID,
            'entityID' => $jobStationData->entityID,
        );
        if ($this->tableGateway->insert($jStationData)) {
            $result = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
            return $result;
        } else {
            return FALSE;
        }
    }

    public function updateJobStation($jobStationData)
    {
        $jStationData = array(
            'jobStationName' => $jobStationData->jobStationName,
            'jobStationMaxValue' => $jobStationData->jobStationMaxValue,
            'jobStationMinValue' => $jobStationData->jobStationMinValue,
        );
        try {
            $this->tableGateway->update($jStationData, array('jobStationID' => $jobStationData->jobStationID));
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
        return true;
    }

    public function deleteJobStationByJobStationID($jobStationData)
    {
        try {
            $result = $this->tableGateway->delete(array('jobStationID' => $jobStationData->jobStationID));
            return $result;
        } catch (\Exception $e) {
            $error = 0;
            return $error;
        }
    }

    public function jobStationSearchByKey($keyword, $locationID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('jobStation')
                ->columns(array('*'))
                ->join('entity', 'jobStation.entityID=entity.entityID', array("deleted"), "left");
        $select->where(new PredicateSet(array(new Operator('jobStation.locationID', '=', $locationID))));
        $select->where(new PredicateSet(array(new Operator('entity.deleted', '=', 0))));
        $select->where(new PredicateSet(array(new Operator('jobStation.jobStationStatus', '=', 1))));
        $select->where(new PredicateSet(array(new Operator('jobStation.jobStationName', 'like', '%' . $keyword . '%'))));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return (object) $results;
    }

    public function updateJobStationState($jobStationID, $status)
    {
        $jobStationData = array(
            'jobStationStatus' => $status,
        );
        try {
            $this->tableGateway->update($jobStationData, array('jobStationID' => $jobStationID));
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
        return true;
    }

}
