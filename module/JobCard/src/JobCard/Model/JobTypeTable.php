<?php

/**
 * @author Ashan Madushka   <ashan@thinkcube.com>
 * This file contains Job Type Table Functions
 */

namespace JobCard\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Predicate\PredicateSet;
use Zend\Db\Sql\Predicate\Operator;

class JobTypeTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll($paginated = FALSE)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('jobType');
        $select->order('jobTypeName DESC');
        $select->join('entity', 'jobType.entityID = entity.entityID', array('deleted'));
        $select->where(array('deleted' => '0'));
        if ($paginated) {
            $paginatorAdapter = new DbSelect(
                    $select, $this->tableGateway->getAdapter());
            $paginator = new Paginator($paginatorAdapter);
            return $paginator;
        } else {
            $statement = $sql->prepareStatementForSqlObject($select);
            $result = $statement->execute();
            return $result;
        }
    }

    //save job type function
    public function saveJobType(JobType $JobType)
    {
        $JobTypeData = array(
            'jobTypeName' => $JobType->jobTypeName,
            'jobTypeCode' => $JobType->jobTypeCode,
            'entityID' => $JobType->entityID,
            'jobTypeStatus' => '1',
        );
        if ($this->tableGateway->insert($JobTypeData)) {
            $result = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
            return $result;
        } else {
            return FALSE;
        }
    }

    //get job type by job type code
    public function getJobTypeByJobTypeCode($jobTybeCode)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('jobType')
                ->columns(array("*"))
                ->join('entity', 'jobType.entityID=  entity.entityID', array("*"), "left")
                ->where(array('jobType.jobTypeCode' => $jobTybeCode, "entity.deleted" => 0));
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if (!$rowset) {
            return NULL;
        }
        return $rowset;
    }

    //update job type by job type id
    public function updateJobType(JobType $JobType)
    {
        $JobTypeData = array(
            'jobTypeName' => $JobType->jobTypeName,
        );
        try {
            $this->tableGateway->update($JobTypeData, array('jobTypeId' => $JobType->jobTypeID));
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
        return true;
    }

    //get job Type for search
    public function getJobTypeforSearch($keyword)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('jobType')
                ->columns(array(
                    'posi' => new Expression('LEAST(IF(POSITION(\'' . $keyword . '\' in jobTypeName )>0,POSITION(\'' . $keyword . '\' in jobTypeName), 9999),'
                            . 'IF(POSITION(\'' . $keyword . '\' in jobTypeCode )>0,POSITION(\'' . $keyword . '\' in jobTypeCode), 9999)) '),
                    'len' => new Expression('LEAST(CHAR_LENGTH(jobTypeName ), CHAR_LENGTH(jobTypeCode )) '),
                    '*'))
                ->join('entity', 'jobType.entityID = entity.entityID', array('*'), 'left');
        $select->where(new PredicateSet(array(new Operator('jobType.jobTypeName', 'like', '%' . $keyword . '%'), new Operator('jobType.jobTypeCode', 'like', '%' . $keyword . '%')), PredicateSet::OP_OR));
        $select->where(new PredicateSet(array(new Operator('entity.deleted', '=', '0'))));
        $select->where(new PredicateSet(array(new Operator('jobType.jobTypeStatus', '=', '1'))));
        $select->order(new Expression('IF(posi > 0, posi, 9999) ASC, len ASC'));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return (object) $results;
    }

    //delete job type by job type id
    public function deleteJobTypeByJobTypeID($jobTypeID)
    {
        try {
            $result = $this->tableGateway->delete(array('jobTypeId' => $jobTypeID));
            return $result;
        } catch (\Exception $e) {
            $error = 0;
            return $error;
        }
    }

    //get job type by job type id
    public function getJobTypeByJobTypeID($jobTybeID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('jobType')
                ->columns(array("*"))
                ->join('entity', 'jobType.entityID=  entity.entityID', array("*"), "left")
                ->where(array('jobType.jobTypeId' => $jobTybeID, "entity.deleted" => 0));
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if (!$rowset) {
            return NULL;
        }
        return $rowset;
    }

    public function updateJobTypeState($jobTypeID, $status)
    {
        $jobTypeData = array(
            'jobTypeStatus' => $status,
        );
        try {
            $this->tableGateway->update($jobTypeData, array('jobTypeId' => $jobTypeID));
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
        return true;
    }

}
