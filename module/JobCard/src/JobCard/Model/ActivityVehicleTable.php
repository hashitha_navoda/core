<?php

namespace JobCard\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Predicate\PredicateSet;
use Zend\Db\Sql\Predicate\Operator;

class ActivityVehicleTable
{
    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    /**
     * Save activity vehicle
     * @param JobCard\Model\ActivityVehicle $actityVehicle
     * @return mixed
     */
    public function saveActivityVehicle(ActivityVehicle $actityVehicle )
    {
        $data = array(
            'activityVehicleStartMileage' => $actityVehicle->activityVehicleStartMileage,
            'activityVehicleEndMileage' => $actityVehicle->activityVehicleEndMileage,
            'activityVehicleCost' => $actityVehicle->activityVehicleCost,
            'vehicleId' => $actityVehicle->vehicleId,
            'activityId' => $actityVehicle->activityId,
            'entityId' => $actityVehicle->entityId
        );

        if ($this->tableGateway->insert($data)) {
            return $this->tableGateway->lastInsertValue;
        } else {
            return FALSE;
        }
    }
    
    /**
     * Update activity vehicle
     * @param array $data
     * @param string $activityVehicleId
     * @return boolean
     */
    public function updateActivityVehicle($data, $activityVehicleId)
    {
        if ($activityVehicleId) {
            if ($this->tableGateway->update($data, array('activityVehicleId' => $activityVehicleId))) {
                return TRUE;
            } else {
                return FALSE;
            }
        } else {
            return FALSE;
        }
    }
    
    /**
     * Get vehicle list by activityId
     * @param int $activityId
     * @return mixed
     */
    public function getActivityVehiclesByActivityId($activityId)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('activityVehicle')
                ->columns(array('*'))
                ->join('vehicle', 'activityVehicle.vehicleId = vehicle.vehicleId', array('vehicleRegistrationNumber'))
                ->join('entity', 'activityVehicle.entityId = entity.entityID', array('deleted'))
                ->where(array('activityVehicle.activityId' => $activityId,'entity.deleted' => 0));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }
    
    /**
     * Delete vehicle by activity id
     * @param type $activityId
     * @return mixed
     */
    public function deleteVehicleByActivityId($activityId)
    {
        try {
            $result = $this->tableGateway->delete(array('activityId' => $activityId));
            return $result;
        } catch (\Exception $e) {
            $error = 0;
            return $error;
        }
    }
    
    /**
     * Get activity vehicle list by vehicleId
     * @param int $vehicleId
     * @return mixed
     */
    public function getActivityVehiclesByVehicleId($vehicleId, $startDate = null, $endDate = null)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('activityVehicle')
                ->columns(array('*'))
                ->join('vehicle', 'activityVehicle.vehicleId = vehicle.vehicleId', array('vehicleRegistrationNumber','vehicleMake','vehicleModel'))
                ->join('activity', 'activityVehicle.activityId = activity.activityId', array('activityName','activityCode','jobId'))
                ->join('job', 'activity.jobId = job.jobId', array('jobName','jobReferenceNumber'))
                ->join('entity', 'activityVehicle.entityId = entity.entityID', array('deleted','createdTimeStamp'))
                ->where(array('activityVehicle.vehicleId' => $vehicleId,'entity.deleted' => 0));
        if($startDate && $endDate){
            $select->where->between('entity.createdTimeStamp', $startDate, $endDate);
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

}
