<?php

namespace JobCard\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Predicate\PredicateSet;
use Zend\Db\Sql\Predicate\Operator;
use Zend\Db\Sql\Predicate\IsNotNull;

class EmployeeTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll($paginated = FALSE)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('employee');
        $select->order('employeeFirstName DESC');
        $select->join('division', 'employee.divisionID=  division.divisionID', array("divisionName"), "left");
        $select->join('designation', 'employee.designationID=  designation.designationID', array("designationName"), "left");
        $select->join('user', 'employee.userID=  user.userID', array("userUsername", "userFirstName", "userLastName"), "left");
        $select->join('entity', 'employee.entityID =entity.entityID', array("deleted"), "left");
        $select->where(array('entity.deleted' => 0));
        if ($paginated) {
            $paginatorAdapter = new DbSelect(
                    $select, $this->tableGateway->getAdapter());
            $paginator = new Paginator($paginatorAdapter);
            return $paginator;
        } else {
            $statement = $sql->prepareStatementForSqlObject($select);
            $result = $statement->execute();
            return $result;
        }
    }

    public function getEmployees($empFName, $empSName, $designation, $division)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('employee')
                ->columns(array('*'))
                ->join('entity', 'employee.entityID =entity.entityID', array("deleted"), "left")
                ->where(array('employee.employeeFirstName' => $empFName, 'employee.employeeSecondName' => $empSName, 'employee.designationID' => $designation, 'employee.divisionID' => $division, 'entity.deleted' => 0));
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if (!$rowset) {
            return NULL;
        }
        return $rowset;
    }

    public function getUserAssignedEmployees()
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('employee')
                ->columns(array('*'))
                ->join('entity', 'employee.entityID =entity.entityID', array("deleted"), "left")
                ->where(array(new PredicateSet(array(new IsNotNull('employee.userID'))), 'entity.deleted' => 0));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        $resultsArray = [];
        foreach ($results as $result) {
            $resultsArray[] = $result;
        }
        return $resultsArray;
    }

//get employee details that related to given employeeID
    public function getEmployeeDetailsByEmployeeID($empID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('employee')
                ->columns(array('*'))
                ->where(array('employee.employeeID' => $empID));
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if (!$rowset) {
            return NULL;
        }
        return $rowset;
    }

//get employee details that related to given designationID
    public function getEmployeeDetailsByDesignationID($designationID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('employee')
                ->columns(array('*'))
                ->join('entity', 'employee.entityID =entity.entityID', array("deleted"), "left")
                ->where(array('employee.designationID' => $designationID, 'entity.deleted' => 0));
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if (!$rowset) {
            return NULL;
        }
        return $rowset;
    }

//get employee details that related to given divisionID
    public function getEmployeeDetailsByDivisionID($divisionID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('employee')
                ->columns(array('*'))
                ->join('entity', 'employee.entityID =entity.entityID', array("deleted"), "left")
                ->where(array('employee.divisionID' => $divisionID, 'entity.deleted' => 0));
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if (!$rowset) {
            return NULL;
        }
        return $rowset;
    }

    public function saveEmployees($employeesData)
    {
        $EmployeesData = array(
            'employeeFirstName' => $employeesData->employeeFirstName,
            'employeeSecondName' => $employeesData->employeeSecondName,
            'designationID' => $employeesData->designationID,
            'divisionID' => $employeesData->divisionID,
            'employeeEmail' => $employeesData->employeeEmail,
            'employeeTP' => $employeesData->employeeTP,
            'employeeHourlyCost' => $employeesData->employeeHourlyCost,
            'userID' => $employeesData->userID,
            'entityID' => $employeesData->entityID,
            'employeeStatus' => '1',
        );
        if ($this->tableGateway->insert($EmployeesData)) {
            $result = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
//            return $result;
            return true;
        } else {
            return FALSE;
        }
    }

    public function updateEmployees($employeesData)
    {
        $EmployeesData = array(
            'employeeFirstName' => $employeesData->employeeFirstName,
            'employeeSecondName' => $employeesData->employeeSecondName,
            'designationID' => $employeesData->designationID,
            'divisionID' => $employeesData->divisionID,
            'employeeEmail' => $employeesData->employeeEmail,
            'employeeTP' => $employeesData->employeeTP,
            'employeeHourlyCost' => $employeesData->employeeHourlyCost,
            'userID' => $employeesData->userID,
        );
        try {
            $this->tableGateway->update($EmployeesData, array('employeeID' => $employeesData->employeeID));
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
        return true;
    }

    public function deleteEmployeesByEmployeeID($employeesData)
    {
        try {
            $result = $this->tableGateway->delete(array('employeeID' => $employeesData->employeeID));
            return $result;
        } catch (\Exception $e) {
            $error = 0;
            return $error;
        }
    }

    public function EmployeeSearchByKey($keyword)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('employee')
                ->columns(array('*'));
        $select->join('division', 'employee.divisionID=  division.divisionID', array("divisionName"), "left");
        $select->join('designation', 'employee.designationID=  designation.designationID', array("designationName"), "left");
        $select->join('user', 'employee.userID=  user.userID', array("userUsername", "userFirstName", "userLastName"), "left");
        $select->join('entity', 'employee.entityID =entity.entityID', array("deleted"), "left");
        $select->where(new PredicateSet(array(new Operator('employee.employeeFirstName', 'like', '%' . $keyword . '%'), new Operator('employee.employeeSecondName', 'like', '%' . $keyword . '%')), PredicateSet::OP_OR));
        $select->where(new PredicateSet(array(new Operator('entity.deleted', '=', 0))));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return (object) $results;
    }

    public function searchEmployeeForDropdown($searchKey)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('employee');
        $select->join('entity', 'employee.entityID=  entity.entityID', array("*"), "left");
        $select->where(new PredicateSet(array(new Operator('employee.employeeFirstName', 'like', '%' . $searchKey . '%'), new Operator('employee.employeeSecondName', 'like', '%' . $searchKey . '%')), PredicateSet::OP_OR));
        $select->where(new PredicateSet(array(new Operator('entity.deleted', '=', '0'))));
        $select->where(new PredicateSet(array(new Operator('employee.employeeStatus', '=', '1'))));
        $select->limit(50);
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

    public function updateEmployeeState($employeeID, $status)
    {
        $employeeData = array(
            'employeeStatus' => $status,
        );
        try {
            $this->tableGateway->update($employeeData, array('employeeID' => $employeeID));
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
        return true;
    }

}
