<?php

/**
 * @author Sandun <sandun@thinkcube.com>
 * This file contains activity contractor table Functions
 */

namespace JobCard\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Predicate\PredicateSet;
use Zend\Db\Sql\Predicate\Operator;

class ActivityContractorTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function saveActivityContractor(ActivityContractor $activityContractor)
    {
        $constractorData = array(
            'activityId' => $activityContractor->activityId,
            'contractorId' => $activityContractor->contractorId,
            'activityContractorCost' => $activityContractor->activityContractorCost,
        );
        if ($this->tableGateway->insert($constractorData)) {
            $result = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
            return $result;
        } else {
            return FALSE;
        }
    }

    public function deleteActivityContractor($activityId)
    {
        try {
            $result = $this->tableGateway->delete(array('activityId' => $activityId));
            return $result;
        } catch (\Exception $e) {
            $error = 0;
            return $error;
        }
    }

    public function getActivityByActivityTypeID($contractorId)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('activityContractor')
                ->columns(array('*'));
        $select->where(array('activityContractor.contractorId' => $contractorId));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return (object) $results;
    }

}
