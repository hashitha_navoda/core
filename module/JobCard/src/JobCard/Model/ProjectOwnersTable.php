<?php

/**
 * @author Ashan Madushka   <ashan@thinkcube.com>
 * This file contains Project Owners Table Functions
 */

namespace JobCard\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Predicate\PredicateSet;
use Zend\Db\Sql\Predicate\Operator;

class ProjectOwnersTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    //save project owners function
    public function saveProjectOwners(ProjectOwners $projectOwners)
    {
        $projectData = array(
            'projectId' => $projectOwners->projectId,
            'employeeId' => $projectOwners->employeeId,
        );
        if ($this->tableGateway->insert($projectData)) {
            $result = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
            return $result;
        } else {
            return FALSE;
        }
    }

    //get project owners by projectID
    public function getProjectOwnersByProjectID($projectID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('projectOwners')
                ->columns(array('*'))
                ->join('employee', "projectOwners.employeeId = employee.employeeID", '*', 'left')
                ->where(array('projectOwners.projectId' => $projectID));
        $query = $sql->prepareStatementForSqlObject($select);
        $rowset = $query->execute();
        if (!$rowset) {
            return null;
        }
        return $rowset;
    }

    //delete project owners by project and ownersID
    public function deleteProjectOwnerByProjectAndOwnerID($projectID, $ownerID)
    {
        try {
            $this->tableGateway->delete(array('projectId' => $projectID, 'employeeId' => $ownerID));
            return true;
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
            return false;
        }
    }

//this function use to get all Projectowners details that relatedto the given employee ID
    public function getProjectOwnerDetailsByEmployeeID($ID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('projectOwners')
                ->columns(array('*'))
                ->where(array('projectOwners.employeeId' => $ID));
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if (!$rowset) {
            return NULL;
        }
        return $rowset;
    }
    
    /**
     * Get project employee by employee id
     * @param int $projectId
     * @param int $employeeId
     * @return mixed
     */
    public function getProjectOwnerByEmployeeId( $projectId, $employeeId) 
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('projectOwners')
                ->columns(array('*'))
                ->where(array('projectOwners.projectId' => $projectId,'projectOwners.employeeId' => $employeeId));
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if ($rowset->count() == 0) {
            return NULL;
        }
        return $rowset->current();
    }
    
    /**
     * Check wether employee exist in the given project
     * @param int $projectId
     * @param int $employeeId
     * @return boolean
     */
    public function isProjectEmployeeExist( $projectId, $employeeId) 
    {
        if($this->getProjectOwnerByEmployeeId( $projectId, $employeeId)){
            return TRUE;
        } else {
            return FALSE;
        }
    }

}
