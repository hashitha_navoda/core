<?php

namespace JobCard\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class ActivityRawMaterial
{

    public $activityRawMaterialId;
    public $activityId;
    public $locationProductId;
    public $activityRawMaterialQuantity;
    public $activityRawMaterialCost;
    protected $inputFilter;

    public function exchangeArray($data)
    {
        $this->activityRawMaterialId = (!empty($data['activityRawMaterialId'])) ? $data['activityRawMaterialId'] : null;
        $this->activityId = (!empty($data['activityId'])) ? $data['activityId'] : null;
        $this->locationProductId = (!empty($data['locationProductId'])) ? $data['locationProductId'] : null;
        $this->activityRawMaterialQuantity = (!empty($data['activityRawMaterialQuantity'])) ? $data['activityRawMaterialQuantity'] : null;
        $this->activityRawMaterialCost = (!empty($data['activityRawMaterialCost'])) ? $data['activityRawMaterialCost'] : null;
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory = new InputFactory();

            $inputFilter->add($factory->createInput(array(
                        'name' => 'activityRawMaterialId',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'Int'),
                        ),
            )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'activityId',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'Int'),
                        ),
            )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'locationProductId',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'Int'),
                        ),
            )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'activityRawMaterialQuantity',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'StringLength',
                                'options' => array(
                                    'encoding' => 'UTF-8',
                                    'min' => 1,
                                    'max' => 1000,
                                ),
                            ),
                        ),
            )));

            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

}
