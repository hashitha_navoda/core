<?php

namespace JobCard\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class InquiryLog
{

    public $inquiryLogID;
    public $inquiryLogReference;
    public $inquiryLogDescription;
    public $customerID;
    public $customerProfileID;
    public $inquiryTypeID;
    public $inquiryLogDateAndTime;
    public $inquiryLogCurrentUserID;
    public $inquiryStatusID;
    public $projectID;
    public $jobID;
    public $activityID;
    public $inquiryLogTP1;
    public $inquiryLogTP2;
    public $inquiryLogAddress;
    public $entityID;

    public function exchangeArray($data)
    {
        $this->inquiryLogID = (!empty($data['inquiryLogID'])) ? $data['inquiryLogID'] : null;
        $this->inquiryLogReference = (!empty($data['inquiryLogReference'])) ? $data['inquiryLogReference'] : null;
        $this->inquiryLogDescription = (!empty($data['inquiryLogDescription'])) ? $data['inquiryLogDescription'] : null;
        $this->customerID = (!empty($data['customerID'])) ? $data['customerID'] : null;
        $this->customerProfileID = (!empty($data['customerProfileID'])) ? $data['customerProfileID'] : null;
        $this->inquiryTypeID = (!empty($data['inquiryTypeID'])) ? $data['inquiryTypeID'] : null;
        $this->inquiryLogDateAndTime = (!empty($data['inquiryLogDateAndTime'])) ? $data['inquiryLogDateAndTime'] : null;
        $this->inquiryLogCurrentUserID = (!empty($data['inquiryLogCurrentUserID'])) ? $data['inquiryLogCurrentUserID'] : null;
        $this->inquiryStatusID = (!empty($data['inquiryStatusID'])) ? $data['inquiryStatusID'] : null;
        $this->projectID = (!empty($data['projectID'])) ? $data['projectID'] : null;
        $this->jobID = (!empty($data['jobID'])) ? $data['jobID'] : null;
        $this->activityID = (!empty($data['activityID'])) ? $data['activityID'] : null;
        $this->locationID = (!empty($data['locationID'])) ? $data['locationID'] : null;
        $this->inquiryLogTP1 = (!empty($data['inquiryLogTP1'])) ? $data['inquiryLogTP1'] : null;
        $this->inquiryLogTP2 = (!empty($data['inquiryLogTP2'])) ? $data['inquiryLogTP2'] : null;
        $this->inquiryLogAddress = (!empty($data['inquiryLogAddress'])) ? $data['inquiryLogAddress'] : null;
        $this->entityID = (!empty($data['entityID'])) ? $data['entityID'] : null;
        $this->inquiryComplainType = (!empty($data['inquiryComplainType'])) ? $data['inquiryComplainType'] : null;
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory = new InputFactory();

            $inputFilter->add($factory->createInput(array(
                        'name' => 'inquiryLogID',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'Int'),
                        ),
            )));


            $inputFilter->add($factory->createInput(array(
                        'name' => 'customerID',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'Int'),
                        ),
            )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'inquiryTypeID',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'Int'),
                        ),
            )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'inquiryStatusID',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'Int'),
                        ),
            )));

            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

}
