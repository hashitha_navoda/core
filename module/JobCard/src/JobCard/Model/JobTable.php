<?php

namespace JobCard\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Predicate\PredicateSet;
use Zend\Db\Sql\Predicate\Operator;
use Zend\Db\Sql\Where;

class JobTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll($locationID, $paginated = FALSE)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('job');
        $select->order('jobId DESC');
        $select->join('customer', 'job.customerID=  customer.customerID', array("customerName", "customerShortName", "customerStatus"), "left");
        $select->join('jobType', 'job.jobTypeId=  jobType.jobTypeId', array("jobTypeName"), "left");
        $select->join('entity', 'job.entityID=  entity.entityID', array("deleted"), "left");
        if ($locationID) {
            $select->where(array('job.locationID' => $locationID));
        }
        if ($paginated) {
            $paginatorAdapter = new DbSelect(
                    $select, $this->tableGateway->getAdapter());
            $paginator = new Paginator($paginatorAdapter);
            return $paginator;
        } else {
            $statement = $sql->prepareStatementForSqlObject($select);
            $result = $statement->execute();
            return $result;
        }
    }

    //get all Job details that related to Location
    public function getAllJobDetails($locationID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('job');
        $select->order('jobId DESC');
//        $select->join('customer', 'job.customerID=  customer.customerID', array("customerName", "customerShortName"), "left");
//        $select->join('jobType', 'job.jobTypeId=  jobType.jobTypeId', array("jobTypeName"), "left");
        $select->join('entity', 'job.entityID=  entity.entityID', array("deleted"), "left");
        $select->where(array('job.locationID' => $locationID, 'entity.deleted' => 0));
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();

        return $result;
    }

//get details that related to the given Job ID
    public function getJobDetails($ID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('job')
                ->columns(array('*'))
                ->join('customer', 'job.customerID=  customer.customerID', array("customerName", "customerShortName"), "left")
                ->join('jobType', 'job.jobTypeId=  jobType.jobTypeId', array("jobTypeName"), "left")
                ->join('status', "job.jobStatus= status.statusID", array('statusName'), 'left')
                ->join('customerProfile', 'job.customerProfileID=  customerProfile.customerProfileID', array("*"), "left")
                ->join('entity', 'job.entityID=  entity.entityID', array("*"), "left")
                ->join('user', 'entity.createdBy = user.userID', array('userUsername'), 'left')
                ->where(array('job.jobId' => $ID));
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if (!$rowset) {
            return NULL;
        }
        return $rowset;
    }

//get job details that related to the given jobType ID
    public function getJobDetailsByJobTypeID($ID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('job')
                ->columns(array('*'))
                ->join('entity', 'job.entityID=  entity.entityID', array("deleted"), "left")
                ->where(array('job.jobTypeId' => $ID, 'entity.deleted' => 0));
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if (!$rowset) {
            return NULL;
        }
        return $rowset;
    }

    /**
     * @author Lahiru Abeysinghe <lahiru@thinkcube.com>
     * use to get all job details that related to the given customerID
     * @param String $customerID
     *
     */
    public function getJobDetailsByCustomerID($customerID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('job')
                ->columns(array('*'))
                ->join('entity', 'job.entityID=  entity.entityID', array("deleted"), "left")
                ->where(array('job.customerID' => $customerID, 'entity.deleted' => 0));
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if (!$rowset) {
            return NULL;
        }
        return $rowset;
    }

//get job details that related to the given jobStation ID
    public function getJobDetailsByJobStationID($ID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('job')
                ->columns(array('*'))
                ->join('entity', 'job.entityID=  entity.entityID', array("deleted"), "left")
                ->where(array('job.jobStation' => $ID, 'entity.deleted' => 0));
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if (!$rowset) {
            return NULL;
        }
        return $rowset;
    }

//get job details that related to given params

    public function getJob($cusName, $jobname, $jobStation, $locationID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('job')
                ->columns(array('*'))
                ->join('customer', 'job.customerID=  customer.customerID', array("customerName", "customerShortName"), "left")
                ->join('jobType', 'job.jobTypeId=  jobType.jobTypeId', array("jobTypeName"), "left")
                ->where(array('job.customerID' => $cusName, 'job.jobName' => $jobname, 'job.jobStation' => $jobStation, 'job.locationID' => $locationID));
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if (!$rowset) {
            return NULL;
        }
        return $rowset;
    }

// save job details
    public function saveJob($jobData)
    {
        $jobSaveData = array(
            'jobName' => $jobData->jobName,
            'customerID' => $jobData->customerID,
            'jobTypeId' => $jobData->jobTypeId,
            'jobDescription' => $jobData->jobDescription,
            'jobStartingTime' => $jobData->jobStartingTime,
            'jobEstimatedTime' => $jobData->jobEstimatedTime,
            'jobEndTime' => $jobData->jobEndTime,
            'jobEstimatedCost' => $jobData->jobEstimatedCost,
            'projectId' => $jobData->projectId,
            'jobStation' => $jobData->jobStation,
            'jobReferenceNumber' => $jobData->jobReferenceNumber,
            'customerProfileID' => $jobData->customerProfileID,
            'jobWeight' => $jobData->jobWeight,
            'locationID' => $jobData->locationID,
            'jobStatus' => $jobData->jobStatus,
            'entityID' => $jobData->entityID,
            'jobAddress' => $jobData->jobAddress,
            'jobRepeatEnabled' => $jobData->jobRepeatEnabled,
            'jobRepeatComment' => $jobData->jobRepeatComment,
        );
        if ($this->tableGateway->insert($jobSaveData)) {
            $result = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue('jobID');
            return $result;
//           return true;
        } else {
            return FALSE;
        }
    }

//get job details that related to the jobReference Code
    public function getJobByCode($jobRefCode)
    {
        $rowset = $this->tableGateway->select(array('jobReferenceNumber' => $jobRefCode));
        $row = $rowset->current();
        if (!$row) {
            return NULL;
        }
        return $row;
    }

//update existing job records
    public function updateJob($jobData)
    {
        $jobSaveData = array(
            'jobName' => $jobData->jobName,
            'customerID' => $jobData->customerID,
            'jobTypeId' => $jobData->jobTypeId,
            'jobDescription' => $jobData->jobDescription,
            'jobStartingTime' => $jobData->jobStartingTime,
            'jobEstimatedTime' => $jobData->jobEstimatedTime,
            'jobEndTime' => $jobData->jobEndTime,
            'jobEstimatedCost' => $jobData->jobEstimatedCost,
            'projectId' => $jobData->projectId,
            'jobStation' => $jobData->jobStation,
            'jobReferenceNumber' => $jobData->jobReferenceNumber,
            'customerProfileID' => $jobData->customerProfileID,
            'jobAddress' => $jobData->jobAddress,
            'jobRepeatEnabled' => $jobData->jobRepeatEnabled,
            'jobRepeatComment' => $jobData->jobRepeatComment,
        );
        try {
            $this->tableGateway->update($jobSaveData, array('jobId' => $jobData->jobId));
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
        return true;
    }

    public function updateJobstatusID($jobID, $status)
    {
        $jobSaveData = array(
            'jobStatus' => $status
        );
        try {
            $this->tableGateway->update($jobSaveData, array('jobId' => $jobID));
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
        return true;
    }

    public function deleteJobByJobID($jobData)
    {
        try {
            $result = $this->tableGateway->delete(array('jobId' => $jobData->jobId));
            return $result;
        } catch (\Exception $e) {
            $error = 0;
            return $error;
        }
    }

//search job
    public function jobSearchByKey($jobSearchKey, $locationID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('job')
                ->columns(array('*'))
                ->join('customer', 'job.customerID=  customer.customerID', array("customerName", "customerShortName"), "left")
                ->join('entity', 'job.entityID=  entity.entityID', array("deleted"), "left")
                ->join('jobType', 'job.jobTypeId=  jobType.jobTypeId', array("jobTypeName"), "left");
        $select->where(new PredicateSet(array(new Operator('job.jobName', 'like', '%' . $jobSearchKey . '%'), new Operator('job.jobReferenceNumber', 'like', '%' . $jobSearchKey . '%'), new Operator('customer.customerName', 'like', '%' . $jobSearchKey . '%'), new Operator('customer.customerCode', 'like', '%' . $jobSearchKey . '%')), PredicateSet::OP_OR));
        $select->where(new PredicateSet(array(new Operator('job.locationID', '=', $locationID))));
        $select->where(new PredicateSet(array(new Operator('entity.deleted', '=', 0))));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return (object) $results;
    }

//get all jobs that related to the given project id and location id
    public function getJobListByProjectID($projectID, $locationID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('job')
                ->columns(array('*'))
                ->join('customer', 'job.customerID=  customer.customerID', array("customerName", "customerShortName", "customerStatus"), "left")
                ->join('customerProfile', 'job.customerProfileID=  customerProfile.customerProfileID', array("customerProfileName"), "left")
                ->join('status', "job.jobStatus= status.statusID", array('statusName'), 'left')
                ->join('jobType', 'job.jobTypeId=  jobType.jobTypeId', array("jobTypeName"), "left")
                ->join('entity', 'job.entityID=  entity.entityID', array("deleted", "createdTimeStamp"), "left");
        $select->where(array('job.projectId' => $projectID, 'job.locationID' => $locationID, 'entity.deleted' == 0));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return (object) $results;
    }

//use to update job weight
    public function updateJobWeight($jobData)
    {
        $jobWeightUpdateData = array(
            'jobWeight' => $jobData[jobWeight],
        );
        try {
            $this->tableGateway->update($jobWeightUpdateData, array('jobId' => $jobData[jobId], 'projectId' => $jobData[projectId]));
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
        return true;
    }

    //update job progress
    public function updateJobProgress(Job $job)
    {
        $data = array('jobProgress' => $job->jobProgress);
        try {
            $this->tableGateway->update($data, array('jobId' => $job->jobId));
            return true;
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
            return true;
        }
    }

//get job details that related to the activity id
    public function getJobDetilsByActivityID($activity)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('job')
                ->columns(array('*'))
                ->join('activity', 'job.jobId=  activity.jobId', array('*'), "left")
                ->join('customer', 'job.customerID = customer.customerID', array("customerName", "customerShortName", "customerCurrentBalance", "customerCurrentCredit"), "left");
        $select->where(array('activity.activityId' => $activity));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return (object) $results;
    }

    /**
     * @author ashan <ashan@thinkcube.com>
     * @param type $locationID
     * @param type $jobSearchKey
     * @return $results
     */
    public function searchJobsForDropDown($locationID, $jobSearchKey, $projectID = null)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('job');
        $select->columns(array('jobReferenceNumber', 'jobId', 'jobProgress', 'jobName'));
        $select->join('entity', 'job.entityID=  entity.entityID', array("deleted"), "left");
        $select->where(array('job.locationID' => $locationID, 'entity.deleted' => 0));
        $select->where->like('jobReferenceNumber', '%' . $jobSearchKey . '%');
        if ($projectID) {
            $select->where(array('job.projectId' => $projectID));
        }
        $select->limit(50);

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

    //update job status
    public function updateJobStatus(Job $job)
    {
        $data = array('jobStatus' => $job->jobStatus);
        try {
            $this->tableGateway->update($data, array('jobId' => $job->jobId));
            return true;
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
            return false;
        }
    }

    public function searchJobReferenceForDropDown($searchKey)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('job');
        $select->join('entity', 'job.entityID=  entity.entityID', array("*"), "left");
        $select->where(array("entity.deleted" => 0));
        $select->where->like('jobReferenceNumber', '%' . $searchKey . '%');
        $select->limit(50);

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

    /**
     * Seach jobs name for typehead
     * @author sharmilan <sharmilan@thinkcube.com>
     * @param type $locationID
     * @param type $jobSearchKey
     * @return type
     */
    public function searchJobsForTypeahead($locationID, $jobSearchKey)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('job');
        $select->columns(array('jobName', 'jobId'));
        $select->join('entity', 'job.entityID=  entity.entityID', array("deleted"), "left");
        $select->where(array('job.locationID' => $locationID, 'entity.deleted' => 0));
        $select->where->like('jobName', '%' . $jobSearchKey . '%');
        $select->limit(50);

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

    /**
     * Get All Jobs list for drop down by search key
     * @author sharmilan <sharmilan@thinkcube.com>
     * @param type $locationID
     * @param type $jobSearchKey
     * @return type
     */
    public function searchAllJobsForDropDown($locationID, $jobSearchKey)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('job');
        $select->columns(['jobName', 'jobId', 'jobReferenceNumber']);
        $select->where(['job.locationID' => $locationID]);
        $select->where(new PredicateSet([new Operator('jobName', 'like', '%' . $jobSearchKey . '%'),
            new Operator('jobReferenceNumber', 'like', '%' . $jobSearchKey . '%')], PredicateSet::OP_OR));
        $select->limit(50);

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

    /**
     * get job for list view
     * @author sharmilan <sharmilan@thikcube.com>
     * @param type $jobID
     * @param type $locationID
     * @return type
     */
    public function getJobListData($jobID, $locationID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('job')
                ->columns(array('*'))
                ->join('customer', 'job.customerID=  customer.customerID', array("customerName", "customerShortName", "customerStatus"), "left")
                ->join('entity', 'job.entityID=  entity.entityID', array("deleted"), "left")
                ->join('jobType', 'job.jobTypeId=  jobType.jobTypeId', array("jobTypeName"), "left");
        $select->where(['job.locationID' => $locationID, 'jobID' => $jobID]);
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return (object) $results;
    }

    /**
     * Get jobs for list view by date Range
     * @author sharmilan <sharmilan@thinkcube.com>
     * @param type $locationID
     * @param type $fromDate
     * @param type $toDate
     * @return type
     */
    public function getJobListDataByDateRange($locationID, $fromDate, $toDate)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('job')
                ->columns(array('*'))
                ->join('customer', 'job.customerID=  customer.customerID', ['customerName', 'customerShortName', 'customerStatus'], 'left')
                ->join('entity', 'job.entityID=  entity.entityID', ['deleted', 'createdTimeStamp'], "left")
                ->join('jobType', 'job.jobTypeId=  jobType.jobTypeId', ['jobTypeName'], 'left');
        $select->where(['job.locationID' => $locationID]);
        $select->where->greaterThanOrEqualTo('entity.createdTimeStamp', $fromDate)->AND->lessThan('entity.createdTimeStamp', $toDate);
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return (object) $results;
    }

    /**
     * Get Repeat jobs for report
     * @author sharmilan <sharmilan@thinkcube.com>
     * @param type $locations
     * @param type $fromDate
     * @param type $toDate
     * @return type
     */
    public function getRepeatJobs($locations, $fromDate = false, $toDate = false)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('job');
        $select->join('entity', 'job.entityID = entity.entityID', ['createdTimeStamp', 'deleted'], 'left');
        $select->join('project', 'job.projectId = project.projectId', ['projectCode', 'projectId'], 'left');
        $select->where(['entity.deleted' => '0', 'job.jobRepeatEnabled' => '1']);
        $select->where->in('job.locationID', $locations);
        if ($fromDate && $toDate) {
            $select->where->between('createdTimeStamp', $fromDate, $toDate);
        }
        $statment = $sql->prepareStatementForSqlObject($select);
        return $statment->execute();
    }

    /**
     * Update job
     * @param array $jobData
     * @param int $jobId
     * @return boolean
     */
    public function updateJobByArray($jobData,$jobId)
    {
        try {
            $this->tableGateway->update($jobData, array('jobId' => $jobId));
            return true;
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
            return false;
        }
    }

    /**
     * Update multiple jobs
     * @param array $jobData
     * @param array $jobIdArray
     * @return boolean
     */
    public function updateMultipleJobs( $jobData, $jobIdArray)
    {
        $where = new Where();
        $where->in('jobId', $jobIdArray);

        try {
            $this->tableGateway->update($jobData, $where);
            return true;
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
            return false;
        }
    }

    /**
     * Get all the jobs except closed jobs
     * @param type $locationId
     * @return mixed
     */
    public function getActiveJobtList( $locationId)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('job');
        $select->order('jobId DESC');
        $select->join('customer', 'job.customerID=  customer.customerID', array("customerName", "customerShortName","customerStatus"), "left");
        $select->join('jobType', 'job.jobTypeId=  jobType.jobTypeId', array("jobTypeName"), "left");
        $select->join('entity', 'job.entityID=  entity.entityID', array("deleted"), "left");
        $select->where->notEqualTo( 'job.jobStatus', 4);
        if ($locationId) {
            $select->where(array('job.locationID' => $locationId));
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }

    public function getJobsByJobID($jobID)
    {
        $rowset = $this->tableGateway->select(array('jobId' => $jobID));
        $row = $rowset->current();
        if (!$row) {
            return NULL;
        }
        return $row;
    }

}
