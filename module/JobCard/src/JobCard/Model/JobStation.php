<?php

namespace JobCard\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class JobStation
{

    public $jobStationID;
    public $jobStationName;
    public $jobStationMaxValue;
    public $jobStationMinValue;
    public $locationID;
    public $entityID;

    public function exchangeArray($data)
    {
        $this->jobStationID = (!empty($data['jobStationID'])) ? $data['jobStationID'] : null;
        $this->jobStationName = (!empty($data['jobStationName'])) ? $data['jobStationName'] : null;
        $this->jobStationMaxValue = (!empty($data['jobStationMaxValue'])) ? $data['jobStationMaxValue'] : null;
        $this->jobStationMinValue = (!empty($data['jobStationMinValue'])) ? $data['jobStationMinValue'] : null;
        $this->locationID = (!empty($data['locationID'])) ? $data['locationID'] : null;
        $this->entityID = (!empty($data['entityID'])) ? $data['entityID'] : null;
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory = new InputFactory();

            $inputFilter->add($factory->createInput(array(
                        'name' => 'jobStationID',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'Int'),
                        ),
            )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'jobStationName',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'StringLength',
                                'options' => array(
                                    'encoding' => 'UTF-8',
                                    'min' => 1,
                                    'max' => 100,
                                ),
                            ),
                        ),
            )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'jobStationMaxValue',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'double'),
                        ),
            )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'jobStationMinValue',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'double'),
                        ),
            )));

            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

}
