<?php

/**
 * @author Ashan Madushka <ashan@thinkcube.com>
 * This file contains Project  Table Functions
 */

namespace JobCard\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class Project
{

    public $projectId;
    public $projectCode;
    public $projectName;
    public $projectType;
    public $projectDescription;
    public $projectEstimatedTimeDuration;
    public $projectStartingAt;
    public $projectEndAt;
    public $projectEstimatedCost;
    public $locationId;
    public $projectProgress;
    public $projectStatus;
    public $customerID;
    public $customerProfileID;
    public $entityID;
    protected $inputFilter;

    public function exchangeArray($data)
    {
        $this->projectId = (!empty($data['projectId'])) ? $data['projectId'] : null;
        $this->projectCode = (!empty($data['projectCode'])) ? $data['projectCode'] : null;
        $this->projectName = (!empty($data['projectName'])) ? $data['projectName'] : null;
        $this->projectType = (!empty($data['projectType'])) ? $data['projectType'] : null;
        $this->projectDescription = (!empty($data['projectDescription'])) ? $data['projectDescription'] : null;
        $this->projectEstimatedTimeDuration = (!empty($data['projectEstimatedTimeDuration'])) ? $data['projectEstimatedTimeDuration'] : 0;
        $this->projectStartingAt = (!empty($data['projectStartingAt'])) ? $data['projectStartingAt'] : '0000-00-00 00:00:00';
        $this->projectEndAt = (!empty($data['projectEndAt'])) ? $data['projectEndAt'] : '0000-00-00 00:00:00';
        $this->projectEstimatedCost = (!empty($data['projectEstimatedCost'])) ? $data['projectEstimatedCost'] : 0.00;
        $this->locationId = (!empty($data['locationId'])) ? $data['locationId'] : null;
        $this->projectProgress = (!empty($data['projectProgress'])) ? $data['projectProgress'] : 0;
        $this->projectStatus = (!empty($data['projectStatus'])) ? $data['projectStatus'] : 3;
        $this->customerID = (!empty($data['customerID'])) ? $data['customerID'] : null;
        $this->customerProfileID = (!empty($data['customerProfileID'])) ? $data['customerProfileID'] : null;
        $this->entityID = (!empty($data['entityID'])) ? $data['entityID'] : null;
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory = new InputFactory();

            $inputFilter->add($factory->createInput(array(
                        'name' => 'projectId',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'Int'),
                        ),
            )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'projectCode',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'StringLength',
                                'options' => array(
                                    'encoding' => 'UTF-8',
                                    'min' => 1,
                                    'max' => 255,
                                ),
                            ),
                        ),
            )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'projectName',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'StringLength',
                                'options' => array(
                                    'encoding' => 'UTF-8',
                                    'min' => 1,
                                    'max' => 10,
                                ),
                            ),
                        ),
            )));

            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

}
