<?php

/**
 * @author Ashan <ashan@thinkcube.com>
 * This file contains activity temporary product Functions
 */

namespace JobCard\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class ActivityTemporaryProduct
{

    public $activityTemporaryProductID;
    public $temporaryProductID;
    public $activityId;
    protected $inputFilter;

    public function exchangeArray($data)
    {
        $this->activityTemporaryProductID = (!empty($data['activityTemporaryProductID'])) ? $data['activityTemporaryProductID'] : null;
        $this->temporaryProductID = (!empty($data['temporaryProductID'])) ? $data['temporaryProductID'] : null;
        $this->activityId = (!empty($data['activityId'])) ? $data['activityId'] : null;
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory = new InputFactory();

            $inputFilter->add($factory->createInput(array(
                        'name' => 'activityTemporaryProductID',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'Int'),
                        ),
            )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'activityTemporaryProductID',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'Int'),
                        ),
            )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'activityId',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'Int'),
                        ),
            )));


            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

}
