<?php

namespace JobCard\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Predicate\PredicateSet;
use Zend\Db\Sql\Predicate\Operator;

class ActivityOwnerTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function saveActivityOwner(ActivityOwner $activityOwner)
    {
        $activityOwnerData = array(
            'activityID' => $activityOwner->activityID,
            'employeeID' => $activityOwner->employeeID,
        );
        if ($this->tableGateway->insert($activityOwnerData)) {
            $result = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
            return $result;
        } else {
            return FALSE;
        }
    }

    public function deleteActivityOwner($activityID)
    {
        try {
            $result = $this->tableGateway->delete(array('activityID' => $activityID));
            return $result;
        } catch (\Exception $e) {
            $error = 0;
            return $error;
        }
    }
    
    /**
     * Get activity owner details by employeeId
     * @param int $employeeId
     * @return mixed
     */
    public function getActivityOwnerDetailsByEmployeeId( $employeeId, $startDate = null, $endDate = null) 
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('activityOwner')
                ->columns(array('*'))
                ->join('activity', 'activityOwner.activityID = activity.activityId', array('activityName','activityCode','jobId'))
                ->join('job', 'activity.jobId = job.jobId', array('jobName','jobReferenceNumber'))
                ->join('employee', 'employee.employeeID = activityOwner.employeeID', array('employeeFirstName','employeeSecondName'))
                ->where(array('activityOwner.employeeID' => $employeeId));
        if($startDate && $endDate){
            $select->where->between('activity.activityStartingTime', $startDate, $endDate);
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }
    
    /**
     * Get activity owners by activity id
     * @param int $activityId
     * @return mixed
     */
    public function getActivityOwnersByActivityId( $activityId) {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('activityOwner')
                ->columns(array('*'))
                ->join('employee', 'employee.employeeID = activityOwner.employeeID', array('employeeFirstName','employeeSecondName'))
                ->where(array('activityOwner.activityID' => $activityId));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }
    
    /**
     * Get activity employee by employee id
     * @param int $activityId
     * @param int $employeeId
     * @return mixed
     */
    public function getActivityOwnerByEmployeeId( $activityId, $employeeId) 
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('activityOwner')
                ->columns(array('*'))
                ->where(array('activityOwner.activityID' => $activityId,'activityOwner.employeeID' => $employeeId));
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if ($rowset->count() == 0) {
            return NULL;
        }
        return $rowset->current();
    }
    
    /**
     * Check wether employee exist in the given activity
     * @param int $activityId
     * @param int $employeeId
     * @return boolean
     */
    public function isActivityEmployeeExist( $activityId, $employeeId) 
    {
        if($this->getActivityOwnerByEmployeeId( $activityId, $employeeId)){            
            return TRUE;
        } else {            
            return FALSE;
        }
    }
    
    public function deleteActivityOwnerByActivityIdAndEmployeeId( $activityId, $employeeId)
    {
        try {
            $this->tableGateway->delete(array('activityID' => $activityId, 'employeeID' => $employeeId));
            return true;
        } catch (Exception $exc) {
            return false;
        }
    }
}
