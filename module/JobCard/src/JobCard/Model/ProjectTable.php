<?php

/**
 * @author Ashan Madushka   <ashan@thinkcube.com>
 * This file contains Project Table Functions
 */

namespace JobCard\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Predicate\PredicateSet;
use Zend\Db\Sql\Predicate\Operator;

class ProjectTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    //get paginated projet or normal project list
    public function fetchAll($paginated = FALSE, $locationID = false)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('project');
        $select->order('projectId DESC');
        $select->join('customer', 'project.customerID=  customer.customerID', array("customerName", "customerShortName", "customerStatus"), "left");
        $select->join('projectType', 'project.projectType=  projectType.projectTypeID', array("projectTypeName", "projectTypeCode"), "left");
        $select->join('entity', 'project.entityID = entity.entityID', array('deleted'));
        $select->where(array('entity.deleted' => 0));

        if ($locationID) {
            $select->where(['project.locationId' => $locationID]);
        }

        if ($paginated) {
            $paginatorAdapter = new DbSelect(
                    $select, $this->tableGateway->getAdapter());
            $paginator = new Paginator($paginatorAdapter);
            return $paginator;
        } else {
            $statement = $sql->prepareStatementForSqlObject($select);
            $result = $statement->execute();
            return $result;
        }
    }

    //save project function
    public function saveProject(Project $project)
    {
        $projectData = array(
            'projectCode' => $project->projectCode,
            'projectName' => $project->projectName,
            'projectType' => $project->projectType,
            'projectDescription' => $project->projectDescription,
            'projectEstimatedTimeDuration' => $project->projectEstimatedTimeDuration,
            'projectStartingAt' => $project->projectStartingAt,
            'projectEndAt' => $project->projectEndAt,
            'projectEstimatedCost' => $project->projectEstimatedCost,
            'locationId' => $project->locationId,
            'projectProgress' => $project->projectProgress,
            'projectStatus' => $project->projectStatus,
            'customerID' => $project->customerID,
            'customerProfileID' => $project->customerProfileID,
            'entityID' => $project->entityID,
            'projectStatus' => $project->projectStatus,
        );
        if ($this->tableGateway->insert($projectData)) {
            $result = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
            return $result;
        } else {
            return FALSE;
        }
    }

    //get project details by project code
    public function getProjectDetailsByProjectCode($projectCode)
    {
        try {
            $sql = new Sql($this->tableGateway->getAdapter());
            $select = $sql->select();
            $select->from('project')
                    ->columns(array('*'))
                    ->join(array('e' => 'entity'), "project.entityID = e.entityID")
                    ->where(array('e.deleted' => '0'))
                    ->where(array('project.projectCode' => $projectCode));
            $query = $sql->prepareStatementForSqlObject($select);
            $rowset = $query->execute();

            if (!$rowset) {
                return null;
            }
            return $rowset;
        } catch (\Exception $exc) {
            echo $exc;
        }
    }

    //get project list by search key
    public function getProjectListBySearchKey($serchKey)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('project');
        $select->columns(array(
            'posi' => new Expression('LEAST(IF(POSITION(\'' . $serchKey . '\' in project.projectCode )>0,POSITION(\'' . $serchKey . '\' in project.projectCode), 9999),'
                    . 'IF(POSITION(\'' . $serchKey . '\' in project.projectName )>0,POSITION(\'' . $serchKey . '\' in project.projectName), 9999)) '),
            'len' => new Expression('LEAST(CHAR_LENGTH(project.projectCode ), CHAR_LENGTH(project.projectName )) '),
            '*',
        ));
        $select->join('entity', 'project.entityID = entity.entityID', array('deleted'));
        $select->join('customer', 'project.customerID= customer.customerID', array('customerName', 'customerShortName', 'customerStatus'));
        $select->join('projectType', 'project.projectType=  projectType.projectTypeID', array("projectTypeName", "projectTypeCode"), "left");
        $select->where(new PredicateSet(array(new Operator('entity.deleted', '=', '0'))));
        $select->where(new PredicateSet(array(new Operator('project.projectCode', 'like', '%' . $serchKey . '%'), new Operator('project.projectName', 'like', '%' . $serchKey . '%'), new Operator('customer.customerName', 'like', '%' . $serchKey . '%'), new Operator('customer.customerCode', 'like', '%' . $serchKey . '%')), PredicateSet::OP_OR));
        $select->order(new Expression('IF(posi > 0, posi, 9999) ASC, len ASC'));
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }

    //get project details by projectID
    public function getProjectDetailsByProjectID($projectID)
    {
        try {
            $sql = new Sql($this->tableGateway->getAdapter());
            $select = $sql->select();
            $select->from('project')
                    ->columns(array('*'))
                    ->join('entity', "project.entityID = entity.entityID", '*', 'left')
                    ->join('projectType', "project.projectType = projectType.projectTypeID", array('projectTypeID', 'projectTypeCode', 'projectTypeName'), 'left')
                    ->join('customer', "project.customerID= customer.customerID", array('customerName', 'customerShortName', 'customerStatus'), 'left')
                    ->join('customerProfile', "project.customerProfileID= customerProfile.customerProfileID", array('customerProfileName'), 'left')
                    ->join('status', "project.projectStatus= status.statusID", array('statusName'), 'left')
                    ->where(array('entity.deleted' => '0'))
                    ->where(array('project.projectId' => $projectID));
            $query = $sql->prepareStatementForSqlObject($select);
            $rowset = $query->execute();
            if (!$rowset) {
                return null;
            }
            return $rowset;
        } catch (\Exception $exc) {
            echo $exc;
        }
    }

    //this function get project Details that related to given project Type ID
    public function getProjectDetailsByProjectTypeID($projectTypeID)
    {
        try {
            $sql = new Sql($this->tableGateway->getAdapter());
            $select = $sql->select();
            $select->from('project')
                    ->columns(array('*'))
                    ->join('entity', "project.entityID = entity.entityID", '*', 'left')
                    ->where(array('entity.deleted' => '0'))
                    ->where(array('project.projectType' => $projectTypeID));
            $query = $sql->prepareStatementForSqlObject($select);
            $rowset = $query->execute();
            if (!$rowset) {
                return null;
            }
            return $rowset;
        } catch (\Exception $exc) {
            echo $exc;
        }
    }

    /**
     * @author Lahiru Abeysinghe <lahiru@thinkcube.com>
     * Use to get all project details that related to given customerID
     * @param string $customerID
     *
     */
    public function getProjectDetailsByCustomerID($customerID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('project')
                ->columns(array('*'))
                ->join('entity', 'project.entityID=  entity.entityID', array("deleted"), "left")
                ->where(array('project.customerID' => $customerID, 'entity.deleted' => 0));
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if (!$rowset) {
            return NULL;
        }
        return $rowset;
    }

    //update project function
    public function updateProject(Project $project)
    {
        $projectData = array(
            'projectName' => $project->projectName,
            'projectType' => $project->projectType,
            'projectDescription' => $project->projectDescription,
            'projectEstimatedTimeDuration' => $project->projectEstimatedTimeDuration,
            'projectStartingAt' => $project->projectStartingAt,
            'projectEndAt' => $project->projectEndAt,
            'projectEstimatedCost' => $project->projectEstimatedCost,
            'locationId' => $project->locationId,
            'projectProgress' => $project->projectProgress,
            'customerID' => $project->customerID,
            'customerProfileID' => $project->customerProfileID,
        );
        try {
            $this->tableGateway->update($projectData, array('projectId' => $project->projectId));
            return true;
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
            return false;
        }
    }

    //update project progress
    public function updateProjectProgress(Project $project)
    {
        $data = array('projectProgress' => $project->projectProgress);
        try {
            $this->tableGateway->update($data, array('projectId' => $project->projectId));
            return true;
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
            return true;
        }
    }

    /**
     * @author ashan <ashan@thinkcube.com>
     * @param type $locationID
     * @param type $projectSearchKey
     * @return $results
     */
    public function searchProjectsForDropDown($locationID, $projectSearchKey)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('project');
        $select->join('entity', 'project.entityID=entity.entityID', array('*'), 'left');
        $select->columns(array('projectCode', 'projectId', 'projectProgress', 'projectName'));
        $select->where(array('project.locationId' => $locationID, 'entity.deleted' => 0));
        $select->where->like('project.projectCode', '%' . $projectSearchKey . '%');
        $select->limit(50);

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

    //update projet status
    public function updateProjectStatus(Project $project)
    {
        $data = array('projectStatus' => $project->projectStatus);
        try {
            $this->tableGateway->update($data, array('projectId' => $project->projectId));
            return true;
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
            return false;
        }
    }

    /**
     * @author Sandun <sandun@thinkcube.com>
     * get data of weightage from job and activity table according to passing variables
     * @param array $data
     * @return array $results
     */
    public function getWeightData($data)
    {
        $isAllSelect = filter_var($data['isAllSelect'], FILTER_VALIDATE_BOOLEAN);
        if ($data['toDate'] != NULL) {
            $plusToDate = date('Y-m-d H:i:s', strtotime($data['toDate'] . ' + 1 day'));
        }

        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('activity');
        $select->columns(array('activityId', 'activityCode', 'activityWeight', 'activityProgress'));
        $select->join('job', 'job.jobId = activity.jobId', array('jobId', 'jobReferenceNumber', 'jobWeight', 'jobProgress'), 'left');
        $select->join('project', 'project.projectId = activity.projectId', array('projectId', 'projectCode', 'projectProgress'), 'left');
        $select->join('customer', 'customer.customerID =  job.customerID', array("customerTitle", "customerName"), "left");

        if ($data['typeId'] == 'activityNo') {
            $select->join('entity', 'entity.entityID = activity.entityID', array('createdTimeStamp', 'deleted'), 'left');
        }
        if (!$isAllSelect && $data['activityId'] != NULL) {
            $select->where->in('activity.activityId', $data['activityId']);
        }

        if ($data['typeId'] == 'jobNo') {
            $select->join('entity', 'entity.entityID = job.entityID', array('createdTimeStamp', 'deleted'), 'left');
        }
        if (!$isAllSelect && $data['jobId'] != NULL) {
            $select->where->in('job.jobId', $data['jobId']);
        }

        if ($data['typeId'] == 'projectNo') {
            $select->join('entity', 'entity.entityID = project.entityID', array('createdTimeStamp', 'deleted'), 'left');
        }
        if (!$isAllSelect && $data['projectId'] != NULL) {
            $select->where->in('project.projectId', $data['projectId']);
        }

        if ($data['fromDate'] != NULL && $data['toDate'] != NULL) {
            $select->where->between('createdTimeStamp', $data['fromDate'], $plusToDate);
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        if (!$results) {
            return NULL;
        }
        return $results;
    }

    /**
     * @author Sandun <sandun@thinkcube.com>
     * @param array $data
     * @return array $results
     */
    public function getProgressData($data)
    {
        $isAllSelect = filter_var($data['isAllSelect'], FILTER_VALIDATE_BOOLEAN);

        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();

        if ($data['customerId'] != NULL || $isAllSelect) {

            if ($data['projectId'] != NULL) {

                $select->from('project');
                $select->columns(array('projectId', 'projectCode', 'projectProgress'));
                $select->join('customer', 'customer.customerID =  project.customerID', array(
                    "customerID" => new Expression("customer.customerID"),
                    "customerTitle", "customerName"), "left");
                $select->join('entity', 'entity.entityID = project.entityID', array('createdTimeStamp', 'deleted'), 'left');
                $select->where->in('projectId', $data['projectId']);
                $select->where->in('customer.customerID', $data['customerId']);
                $select->order('createdTimeStamp');
            } elseif ($data['jobId'] != NULL) {

                $select->from('job');
                $select->columns(array('jobId', 'jobReferenceNumber', 'jobWeight', 'jobProgress'));
                $select->join('customer', 'customer.customerID =  job.customerID', array(
                    "customerID" => new Expression("customer.customerID"), "customerTitle",
                    "customerName"), "left");
                $select->join('entity', 'entity.entityID = job.entityID', array('createdTimeStamp', 'deleted'), 'left');
                $select->where->in('jobId', $data['jobId']);
                $select->order('createdTimeStamp');
            } else {
                if ($data['typeId'] == 'projectNo') {

                    $select->from('project');
                    $select->columns(array('projectId',
                        'projectCode',
                        'projectProgress',
                        'progress' => new Expression('projectProgress'),
                        'refCode' => new Expression('projectCode')));
                    $select->join('job', 'job.projectId = project.projectId', array('jobId', 'jobReferenceNumber', 'jobWeight', 'jobProgress'), 'left');
                    $select->join('activity', 'activity.jobId = job.jobId', array('activityId', 'activityCode', 'activityWeight', 'activityProgress'), 'left');
                    $select->join('customer', 'customer.customerID =  project.customerID', array(
                        "customerID" => new Expression("customer.customerID"),
                        "customerTitle", "customerName"), "left");
                    $select->join('entity', 'entity.entityID = project.entityID', array('createdTimeStamp', 'deleted'), 'left');
                    if ($data['customerId'] != NULL) {
                        $select->where->in('customer.customerID', $data['customerId']);
                    } else {
                        $select->group('project.projectId');
                    }
                    $select->order('createdTimeStamp');
                } else if ($data['typeId'] == 'jobNo') {

                    $select->from('job');
                    $select->columns(array('jobId',
                        'refCode' => new Expression('jobReferenceNumber'),
                        'progress' => new Expression('jobProgress'),
                        'jobReferenceNumber', 'jobWeight', 'jobProgress'));
                    $select->join('activity', 'activity.jobId = job.jobId', array('activityId', 'activityCode', 'activityWeight', 'activityProgress'), 'left');
                    $select->join('customer', 'customer.customerID =  job.customerID', array(
                        "customerID" => new Expression("customer.customerID"),
                        "customerTitle", "customerName"), "left");
                    $select->join('entity', 'entity.entityID = job.entityID', array('createdTimeStamp', 'deleted'), 'left');
                    if ($data['customerId'] != NULL) {
                        $select->where->in('customer.customerID', $data['customerId']);
                    } else {
                        $select->group("job.jobId");
                    }
                    $select->order('createdTimeStamp');
                } else if ($data['typeId'] == 'activityNo') {

                    $select->from('activity');
                    $select->columns(array('activityId',
                        'refCode' => new Expression('activityCode'),
                        'progress' => new Expression('activityProgress'),
                        'activityCode', 'activityWeight', 'activityProgress'));
//                    $select->join('job', 'job.jobId = activity.jobId', array('jobId', 'jobReferenceNumber', 'jobWeight', 'jobProgress'), 'left');
//                    $select->join('project', 'project.projectId = activity.projectId', array('projectId', 'projectCode', 'projectProgress'), 'left');
//                    $select->join('customer', 'customer.customerID =  job.customerID', array(
//                        "customerID" => new Expression("customer.customerID"),
//                        "customerTitle", "customerName"), "left");
                    $select->join('entity', 'entity.entityID = activity.entityID', array('createdTimeStamp', 'deleted'), 'left');
                    $select->order('createdTimeStamp');
                }
            }
        }

        if ($data['customerId'] == NULL && !$isAllSelect) {
            if ($data['projectId'] != NULL) {

                $select->from('project');
                $select->columns(array('projectId', 'projectCode', 'projectProgress'));
                $select->where->in('projectId', $data['projectId']);
                $select->order('project.projectId');
            }
            if ($data['jobId'] != NULL) {

                $select->from('job');
                $select->columns(array('jobId', 'jobReferenceNumber', 'jobWeight', 'jobProgress'));
                $select->where->in('jobId', $data['jobId']);
                $select->order('job.jobId');
            }
            if ($data['activityId'] != NULL) {

                $select->from('activity');
                $select->columns(array('activityId', 'activityCode', 'activityWeight', 'activityProgress'));
                $select->join('job', 'job.jobId = activity.jobId', array('jobId', 'jobReferenceNumber', 'jobWeight', 'jobProgress'), 'left');
                $select->join('project', 'project.projectId = activity.projectId', array(
                    'projectId', 'projectCode', 'projectProgress'), 'left');
                $select->join('customer', 'customer.customerID =  job.customerID', array(
                    "customerID" => new Expression("customer.customerID"),
                    "customerTitle", "customerName"), "left");
                $select->where->in('activityId', $data['activityId']);
                $select->order('activityId');
            }
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        if (!$results) {
            return NULL;
        }
        return $results;
    }

    public function getJobCardQuotationData($data)
    {
        $isAllSelect = filter_var($data['isAllSelect'], FILTER_VALIDATE_BOOLEAN);
        if ($data['toDate'] != NULL) {
            $plusToDate = date('Y-m-d H:i:s', strtotime($data['toDate'] . ' + 1 day'));
        }

        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('quotation');
        $select->columns(array('quotationID', 'quotationCode', 'quotationExpireDate'));

        if ($data['typeId'] == 'activityNo') {
            $select->join('activity', 'quotation.jobRefTypeID = activity.activityId', array(
                'activityID' => new Expression('activity.activityId'),
                'activityCode', 'activityName'), 'left');
            $select->join('job', 'job.jobId = activity.jobId', array(
                'jobId',
                'jobCode' => new Expression('job.jobReferenceNumber')), 'left');
            $select->join('customer', 'customer.customerID =  job.customerID', array("customerTitle", "customerName"), "left");
            $select->join('entity', 'entity.entityID = activity.entityID', array('createdTimeStamp', 'deleted'), 'left');
        }
        if (!$isAllSelect && $data['activityId'] != NULL) {
            $select->where->in('activity.activityId', $data['activityId']);
            $select->where(array('quotation.jobRefType' => 3));
        }

        if ($data['typeId'] == 'jobNo') {
            $select->join('job', 'job.jobId = quotation.jobRefTypeID', array(
                'jobID' => new Expression('job.jobId'),
                'jobCode' => new Expression('job.jobReferenceNumber'),
                'jobReferenceNumber'), 'left');
            $select->join('customer', 'customer.customerID =  job.customerID', array("customerTitle", "customerName"), "left");
            $select->join('entity', 'entity.entityID = job.entityID', array('createdTimeStamp', 'deleted'), 'left');
        }
        if (!$isAllSelect && $data['jobId'] != NULL) {
            $select->where->in('job.jobId', $data['jobId']);
            $select->where(array('quotation.jobRefType' => 2));
        }

        if ($data['typeId'] == 'projectNo') {
            $select->join('project', 'project.projectId = quotation.jobRefTypeID', array(
                'projectID' => new Expression('project.projectId'),
                'projectCD' => new Expression('project.projectCode'),
                'projectCode'), 'left');
            $select->join('job', 'project.projectId = job.projectId', array(
                'jobCode' => new Expression('job.jobReferenceNumber')
                    ), 'left');
            $select->join('customer', 'customer.customerID =  project.customerID', array("customerTitle", "customerName"), "left");
            $select->join('entity', 'entity.entityID = project.entityID', array('createdTimeStamp', 'deleted'), 'left');
        }
        if (!$isAllSelect && $data['projectId'] != NULL) {
            $select->where->in('project.projectId', $data['projectId']);
            $select->where(array('quotation.jobRefType' => 1));
        }

        if ($data['fromDate'] != NULL && $data['toDate'] != NULL) {
            $select->where->between('createdTimeStamp', $data['fromDate'], $plusToDate);
        }

        $select->order("quotation.quotationID");

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        if (!$results) {
            return NULL;
        }
        return $results;
    }

    /**
     * Get Project list view by project ID
     * @param type $locationID
     * @param type $projectID
     * @return type
     */
    public function getProjectLisViewtByID($locationID, $projectID = false)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('project');
        $select->columns(['*']);
        $select->join('entity', 'project.entityID = entity.entityID', array('deleted'));
        $select->join('customer', 'project.customerID = customer.customerID', array('customerName', 'customerShortName', 'customerStatus'));
        $select->join('projectType', 'project.projectType =  projectType.projectTypeID', array("projectTypeName", "projectTypeCode"), "left");
        $select->where(['project.locationId' => $locationID]);
        if ($projectID) {
            $select->where(['project.projectId' => $projectID]);
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }

    public function getProjectLisViewtByDateRange($locationID, $fromDate, $toDate)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('project');
        $select->columns(['*']);
        $select->join('entity', 'project.entityID = entity.entityID', array('deleted', 'createdTimeStamp'));
        $select->join('customer', 'project.customerID = customer.customerID', array('customerName', 'customerShortName', 'customerStatus'));
        $select->join('projectType', 'project.projectType =  projectType.projectTypeID', array("projectTypeName", "projectTypeCode"), "left");
        $select->where(['project.locationId' => $locationID]);
        $select->where->between('entity.createdTimeStamp', $fromDate, $toDate);
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }

    /**
     * Update project
     * @param array $projectData
     * @param int $projectId
     * @return boolean
     */
    public function updateProjectByArray( $projectData, $projectId)
    {
        try {
            $this->tableGateway->update($projectData, array('projectId' => $projectId));
            return true;
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
            return false;
        }
    }

    /**
     * Get all the projects except closed projects
     * @param int $locationId
     * @return mixed
     */
    public function getActiveProjectList( $locationId = false)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('project');
        $select->order('projectId DESC');
        $select->join('customer', 'project.customerID=  customer.customerID', array("customerName", "customerShortName", "customerStatus"), "left");
        $select->join('projectType', 'project.projectType=  projectType.projectTypeID', array("projectTypeName", "projectTypeCode"), "left");
        $select->join('entity', 'project.entityID = entity.entityID', array('deleted'));
        $select->where(array('entity.deleted' => 0));
        $select->where->notEqualTo( 'project.projectStatus', 4);
        if ($locationId) {
            $select->where(['project.locationId' => $locationId]);
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }

    public function getProjectByProjectId($projectID)
    {
        $rowset = $this->tableGateway->select(array('projectId' => $projectID));
        $row = $rowset->current();
        if (!$row) {
            return NULL;
        }
        return $row;
    }

}
