<?php

namespace JobCard\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Predicate\PredicateSet;
use Zend\Db\Sql\Predicate\Operator;

class ActivityRawMaterialSubProductTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function saveActivityRawMaterialSubProduct(ActivityRawMaterialSubProduct $actRMSubProduct)
    {
        $activityRawMaterialData = array(
            'activityRawMaterialId' => $actRMSubProduct->activityRawMaterialId,
            'productBatchID' => $actRMSubProduct->productBatchID,
            'productSerialID' => $actRMSubProduct->productSerialID,
            'activityRawMaterialSubProductQuantity' => $actRMSubProduct->activityRawMaterialSubProductQuantity,
        );
        if ($this->tableGateway->insert($activityRawMaterialData)) {
            $result = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
            return $result;
        } else {
            return FALSE;
        }
    }

    public function getDataByRawMaterialID($rawMaterialID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('activityRawMaterialSubProduct')
                ->columns(array('*'))
                ->where(array('activityRawMaterialId' => $rawMaterialID));
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if (!$rowset) {
            return NULL;
        }
        return $rowset;
    }

    public function deleteActivitySubRawMaterial($rawMaterialID)
    {
        try {
            $result = $this->tableGateway->delete(array('activityRawMaterialId' => $rawMaterialID));
            return $result;
        } catch (\Exception $e) {
            $error = 0;
            return $error;
        }
    }

    public function getSubRawMeterialProduct($activityID, $locationProductID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('activityRawMaterialSubProduct')
                ->columns(array('*'))
                ->where(array('activityId' => $activityID, 'locationProductId' => $locationProductID));
        $select->join('activityRawMaterial', 'activityRawMaterialSubProduct.activityRawMaterialId = activityRawMaterial.activityRawMaterialId');
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if (!$rowset) {
            return NULL;
        }
        return $rowset;
    }

}
