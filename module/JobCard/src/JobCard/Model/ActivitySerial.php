<?php

/**
 * @author Sandun <sandun@thinkcube.com>
 */

namespace JobCard\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class ActivitySerial
{

    public $activitySerialID;
    public $temporaryProductID;
    public $activityBatchID;
    public $activitySerialCode;
    public $activitySerialSold;
    protected $inputFilter;

    public function exchangeArray($data)
    {
        $this->activitySerialID = (!empty($data['activitySerialID'])) ? $data['activitySerialID'] : null;
        $this->temporaryProductID = (!empty($data['temporaryProductID'])) ? $data['temporaryProductID'] : null;
        $this->activityBatchID = (!empty($data['activityBatchID'])) ? $data['activityBatchID'] : null;
        $this->activitySerialCode = (!empty($data['activitySerialCode'])) ? $data['activitySerialCode'] : null;
        $this->activitySerialSold = (!empty($data['activitySerialSold'])) ? $data['activitySerialSold'] : 0;
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

}
