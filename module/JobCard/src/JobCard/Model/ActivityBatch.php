<?php

/**
 * @author Sandun <sandun@thinkcube.com>
 */

namespace JobCard\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class ActivityBatch
{

    public $activityBatchID;
    public $temporaryProductID;
    public $activityBatchCode;
    public $activityBatchQuantity;
    protected $inputFilter;

    public function exchangeArray($data)
    {
        $this->activityBatchID = (!empty($data['activityBatchID'])) ? $data['activityBatchID'] : null;
        $this->temporaryProductID = (!empty($data['temporaryProductID'])) ? $data['temporaryProductID'] : null;
        $this->activityBatchCode = (!empty($data['activityBatchCode'])) ? $data['activityBatchCode'] : null;
        $this->activityBatchQuantity = (!empty($data['activityBatchQuantity'])) ? $data['activityBatchQuantity'] : null;
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

}
