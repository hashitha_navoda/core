<?php

namespace JobCard\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class Activity
{

    public $activityId;
    public $locationID;
    public $activityCode;
    public $activityName;
    public $activityTypeID;
    public $activityDescription;
    public $activityStartingTime;
    public $activityEndTime;
    public $activityEstimateTime;
    public $activityEstimatedCost;
    public $projectId;
    public $jobId;
    public $activityWeight;
    public $activityProgress;
    public $activitySortOrder;
    public $activityStatus;
    public $entityID;
    public $activityRepeatEnabled;
    public $activityRepeatComment;

    public function exchangeArray($data)
    {
        $this->activityId = (!empty($data['activityId'])) ? $data['activityId'] : null;
        $this->locationID = (!empty($data['locationID'])) ? $data['locationID'] : null;
        $this->activityCode = (!empty($data['activityCode'])) ? $data['activityCode'] : null;
        $this->activityName = (!empty($data['activityName'])) ? $data['activityName'] : null;
        $this->activityTypeID = (!empty($data['activityTypeID'])) ? $data['activityTypeID'] : null;
        $this->activityDescription = (!empty($data['activityDescription'])) ? $data['activityDescription'] : null;
        $this->activityStartingTime = (!empty($data['activityStartingTime'])) ? $data['activityStartingTime'] : null;
        $this->activityEndTime = (!empty($data['activityEndTime'])) ? $data['activityEndTime'] : null;
        $this->activityEstimateTime = (!empty($data['activityEstimateTime'])) ? $data['activityEstimateTime'] : null;
        $this->activityEstimatedCost = (!empty($data['activityEstimatedCost'])) ? $data['activityEstimatedCost'] : 0.00;
        $this->projectId = (!empty($data['projectId'])) ? $data['projectId'] : null;
        $this->jobId = (!empty($data['jobId'])) ? $data['jobId'] : null;
        $this->activityWeight = (!empty($data['activityWeight'])) ? $data['activityWeight'] : 0.00;
        $this->activityProgress = (!empty($data['activityProgress'])) ? $data['activityProgress'] : 0.00;
        $this->activitySortOrder = (!empty($data['activitySortOrder'])) ? $data['activitySortOrder'] : null;
        $this->activityStatus = (!empty($data['activityStatus'])) ? $data['activityStatus'] : 3;
        $this->entityID = (!empty($data['entityID'])) ? $data['entityID'] : null;
        $this->activityRepeatEnabled = (!empty($data['activityRepeatEnabled'])) ? $data['activityRepeatEnabled'] : 0;
        $this->activityRepeatComment = (!empty($data['activityRepeatComment'])) ? $data['activityRepeatComment'] : null;
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory = new InputFactory();

            $inputFilter->add($factory->createInput(array(
                        'name' => 'activityId',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'Int'),
                        ),
            )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'locationID',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'Int'),
                        ),
            )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'activityCode',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'StringLength',
                                'options' => array(
                                    'encoding' => 'UTF-8',
                                    'min' => 1,
                                    'max' => 100,
                                ),
                            ),
                        ),
            )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'activityType',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'Int'),
                        ),
            )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'activityDescription',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'StringLength',
                                'options' => array(
                                    'encoding' => 'UTF-8',
                                    'min' => 1,
                                    'max' => 100,
                                ),
                            ),
                        ),
            )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'activityStartingTime',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'StringLength',
                                'options' => array(
                                    'encoding' => 'UTF-8',
                                    'min' => 1,
                                    'max' => 100,
                                ),
                            ),
                        ),
            )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'activityEndTime',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'StringLength',
                                'options' => array(
                                    'encoding' => 'UTF-8',
                                    'min' => 1,
                                    'max' => 100,
                                ),
                            ),
                        ),
            )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'activityEstimatedCost',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'double'),
                        ),
            )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'projectId',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'Int'),
                        ),
            )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'jobId',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'Int'),
                        ),
            )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'activityWeight',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'double'),
                        ),
            )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'activityProgress',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'double'),
                        ),
            )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'activitySortOrder',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'StringLength',
                                'options' => array(
                                    'encoding' => 'UTF-8',
                                    'min' => 1,
                                    'max' => 100,
                                ),
                            ),
                        ),
            )));


            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

}
