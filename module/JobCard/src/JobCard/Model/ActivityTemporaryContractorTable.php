<?php

/**
 * @author Sandun <sandun@thinkcube.com>
 * This file contains activity contractor table Functions
 */

namespace JobCard\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Predicate\PredicateSet;
use Zend\Db\Sql\Predicate\Operator;

class ActivityTemporaryContractorTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function saveActivityTemporaryContractor(ActivityTemporaryContractor $activityTempContractor)
    {
        $actTempContractorData = array(
            'activityId' => $activityTempContractor->activityId,
            'contractorId' => $activityTempContractor->contractorId,
        );
        if ($this->tableGateway->insert($actTempContractorData)) {
            $result = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
            return $result;
        } else {
            return FALSE;
        }
    }

    public function deleteActivityTemporaryContractor($activityID)
    {
        try {
            $result = $this->tableGateway->delete(array('activityID' => $activityID));
            return $result;
        } catch (\Exception $e) {
            $error = 0;
            return $error;
        }
    }

    public function getDataByTeamporaryContractorID($temporaryContractorID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('activityTemporaryContractor');
        $select->where(array('contractorID' => $temporaryContractorID));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        return (object) $results;
    }

}
