<?php

namespace JobCard\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class TemporaryProduct
{

    public $temporaryProductID;
    public $temporaryProductCode;
    public $temporaryProductName;
    public $temporaryProductDescription;
    public $temporaryProductQuantity;
    public $temporaryProductPrice;
    public $uomID;
    public $temporaryProductImageURL;
    public $temporaryProductSerial;
    public $temporaryProductBatch;
    public $entityID;

    public function exchangeArray($data)
    {
        $this->temporaryProductID = (!empty($data['temporaryProductID'])) ? $data['temporaryProductID'] : null;
        $this->temporaryProductCode = (!empty($data['temporaryProductCode'])) ? $data['temporaryProductCode'] : null;
        $this->temporaryProductName = (!empty($data['temporaryProductName'])) ? $data['temporaryProductName'] : null;
        $this->temporaryProductDescription = (!empty($data['temporaryProductDescription'])) ? $data['temporaryProductDescription'] : null;
        $this->temporaryProductQuantity = (!empty($data['temporaryProductQuantity'])) ? $data['temporaryProductQuantity'] : null;
        $this->temporaryProductPrice = (!empty($data['temporaryProductPrice'])) ? $data['temporaryProductPrice'] : 0.00;
        $this->uomID = (!empty($data['uomID'])) ? $data['uomID'] : null;
        $this->temporaryProductImageURL = (!empty($data['temporaryProductImageURL'])) ? $data['temporaryProductImageURL'] : null;
        $this->temporaryProductSerial = (!empty($data['temporaryProductSerial'])) ? $data['temporaryProductSerial'] : 0;
        $this->temporaryProductBatch = (!empty($data['temporaryProductBatch'])) ? $data['temporaryProductBatch'] : 0;
        $this->entityID = (!empty($data['entityID'])) ? $data['entityID'] : null;
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

}
