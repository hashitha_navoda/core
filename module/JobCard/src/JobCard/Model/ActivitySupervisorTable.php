<?php

namespace JobCard\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Predicate\PredicateSet;
use Zend\Db\Sql\Predicate\Operator;

class ActivitySupervisorTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function saveActivitySupervisor(ActivitySupervisor $activitySupervisor)
    {
        $activitySupervisorData = array(
            'activityID' => $activitySupervisor->activityID,
            'employeeID' => $activitySupervisor->employeeID,
        );
        if ($this->tableGateway->insert($activitySupervisorData)) {
            $result = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
            return $result;
        } else {
            return FALSE;
        }
    }

    public function deleteActivitySupervisor($activityID)
    {
        try {
            $result = $this->tableGateway->delete(array('activityID' => $activityID));
            return $result;
        } catch (\Exception $e) {
            $error = 0;
            return $error;
        }
    }
    
    /**
     * Get activity supervisor details by employeeId
     * @param int $employeeId
     * @return mixed
     */
    public function getActivitySupervisorDetailsByEmployeeId( $employeeId, $startDate = null, $endDate = null) 
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('activitySupervisor')
                ->columns(array('*'))
                ->join('activity', 'activitySupervisor.activityID = activity.activityId', array('activityName','activityCode','jobId'))
                ->join('job', 'activity.jobId = job.jobId', array('jobName','jobReferenceNumber'))
                ->join('employee', 'employee.employeeID = activitySupervisor.employeeID', array('employeeFirstName','employeeSecondName'))
                ->where(array('activitySupervisor.employeeID' => $employeeId));
        if($startDate && $endDate){
            $select->where->between('activity.activityStartingTime', $startDate, $endDate);
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

}
