<?php

/**
 * @author Madawa Chandrarathne <madawa@thinkcube.com>
 */

namespace JobCard\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Predicate\PredicateSet;
use Zend\Db\Sql\Predicate\Operator;

/**
 * Class VehicleTable
 * @package JobCard\Model
 * @author Madawa Chandraratne <madawa@thinkcube.com>
 *
 * This file contains Vehicle Table Functions
 */
class VehicleTable
{
    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }
    
    /**
     * Create new vehicle
     * @param \JobCard\Model\Vehicle $vehicle
     * @return mixed
     */
    public function saveVehicle(Vehicle $vehicle)
    {
        $data = array(
            'vehicleMake' => $vehicle->vehicleMake,
            'vehicleModel' => $vehicle->vehicleModel,
            'vehicleRegistrationNumber' => $vehicle->vehicleRegistrationNumber,
            'vehicleCost' => $vehicle->vehicleCost,
            'entityId' => $vehicle->entityId
        );

        if ($this->tableGateway->insert($data)) {
            return $this->tableGateway->lastInsertValue;
        } else {
            return FALSE;
        }
    }
    
    
    /**
     * Update existing vehicle
     * @param array $data
     * @param string $vehicleId
     * @return boolean
     */
    public function updateVehicle($data, $vehicleId)
    {
        if ($vehicleId) {
            if ($this->tableGateway->update($data, array('vehicleId' => $vehicleId))) {
                return TRUE;
            } else {
                return FALSE;
            }
        } else {
            return FALSE;
        }
    }
    
    
    /**
     * Return all the vehicles in the table.
     * @param boolean $paginated
     * @param array $order
     * @return mixed
     */
    public function fetchAll($paginated = FALSE, $order = array())
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('vehicle')
                ->columns(array('*'))
                ->join('entity', 'vehicle.entityId = entity.entityID', array('deleted'))
                ->order($order)
        ->where->equalTo('entity.deleted', 0);
        if ($paginated) {
            $paginatorAdapter = new DbSelect($select, $this->tableGateway->getAdapter());
            return new Paginator($paginatorAdapter);
        } else {
            $statement = $sql->prepareStatementForSqlObject($select);
            $result = $statement->execute();
            return $result;
        }
    }
    
    
    /**
     * Return vehicle according to the given id.
     * @param string $vehicleId
     * @return mixed
     */
    public function getVehicleByVehicleId($vehicleId)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('vehicle')
                ->columns(array('*'))
                ->join('entity', 'vehicle.entityId = entity.entityID', array('deleted'))
        ->where->equalTo('entity.deleted', 0)
        ->where->equalTo('vehicle.vehicleId', $vehicleId);
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        if ($result->count() == 0) {
            return NULL;
        }
        return $result->current();
    }
    

    /**
     * Return vehicles which having numbers match to the given query string.
     * @param string $query
     * @param array $order
     * @param boolean $paginated
     * @return mixed
     */
    public function searchVehicle($query = null, $order = array(), $paginated = FALSE)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('vehicle')
                ->columns(array('*'))
                ->join('entity', 'vehicle.entityId = entity.entityID', array('deleted'))
                ->order($order)
                ->where->equalTo('entity.deleted', 0);
        if($query != NULL){
            $select->where(new PredicateSet(array(
                new Operator('vehicle.vehicleRegistrationNumber', 'like', '%' . $query . '%'), 
                new Operator('vehicle.vehicleModel', 'like', '%' . $query . '%'), 
                new Operator('vehicle.vehicleMake', 'like', '%' . $query . '%')),PredicateSet::OP_OR)
            );
            $select->limit(10);
        }    
        if ($paginated) {
            $paginatorAdapter = new DbSelect($select, $this->tableGateway->getAdapter());
            return new Paginator($paginatorAdapter);
        } else {
            $statement = $sql->prepareStatementForSqlObject($select);
            $result = $statement->execute();
            return $result;
        }
    }
    
    
    /**
     * Return vehicle according to the given registration number.
     * @param string $regNumber
     * @return mixed
     */
    public function getVehicleByRegistrationNumber($regNumber)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('vehicle')
                ->columns(array('*'))
                ->join('entity', 'vehicle.entityId = entity.entityID', array('deleted'))
        ->where->equalTo('entity.deleted', 0)
        ->where->equalTo('vehicle.vehicleRegistrationNumber', $regNumber);
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        if ($result->count() == 0) {
            return NULL;
        }
        return $result->current();
    }

}
