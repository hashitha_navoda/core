<?php

/**
 * @author Sandun <sandun@thinkcube.com>
 */

namespace JobCard\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;

class ActivityBatchTable
{

    protected $tableGateway;
    public $adapter;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll()
    {
        $rowset = $this->tableGateway->select();
        return $rowset;
    }

    public function saveBatchProduct(ActivityBatch $activityBatch)
    {
        $batchData = array(
            'temporaryProductID' => $activityBatch->temporaryProductID,
            'activityBatchCode' => $activityBatch->activityBatchCode,
            'activityBatchQuantity' => $activityBatch->activityBatchQuantity
        );
        if ($this->tableGateway->insert($batchData)) {
            $insertedID = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
            return $insertedID;
        } else {
            return FALSE;
        }
    }

    public function getBatchByTempProductID($temporaryProductID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('activityBatch')
                ->columns(array('*'))
                ->where(array('temporaryProductID' => $temporaryProductID));
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if (!$rowset) {
            return NULL;
        }

        return $rowset;
    }

    public function deleteActivityBatch($tempProductID)
    {
        try {
            $result = $this->tableGateway->delete(array('temporaryProductID' => $tempProductID));
            return $result;
        } catch (\Exception $e) {
            $error = 0;
            return $error;
        }
    }

}
