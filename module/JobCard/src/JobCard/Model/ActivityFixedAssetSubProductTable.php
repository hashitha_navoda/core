<?php

namespace JobCard\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Predicate\PredicateSet;
use Zend\Db\Sql\Predicate\Operator;

class ActivityFixedAssetSubProductTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function saveActivityFixedAssetSubProduct(ActivityFixedAssetSubProduct $activityFixedAssetSubProducts)
    {
        $activityFixedAssetData = array(
            'activityFixedAssetId' => $activityFixedAssetSubProducts->activityFixedAssetId,
            'productBatchID' => $activityFixedAssetSubProducts->productBatchID,
            'productSerialID' => $activityFixedAssetSubProducts->productSerialID,
            'activityFixedAssetSubProductQuantity' => $activityFixedAssetSubProducts->activityFixedAssetSubProductQuantity,
        );
        if ($this->tableGateway->insert($activityFixedAssetData)) {
            $result = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
            return $result;
        } else {
            return FALSE;
        }
    }

    public function getDataByFixedAssetsID($fixedAssetsID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('activityFixedAssetSubProduct')
                ->columns(array('*'))
                ->where(array('activityFixedAssetID' => $fixedAssetsID));
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if (!$rowset) {
            return NULL;
        }
        return $rowset;
    }

    public function deleteActivitySubFixedAssets($fixedAssetsID)
    {
        try {
            $result = $this->tableGateway->delete(array('activityFixedAssetID' => $fixedAssetsID));
            return $result;
        } catch (\Exception $e) {
            $error = 0;
            return $error;
        }
    }

}
