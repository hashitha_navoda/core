<?php

/**
 * @author Sandun <sandun@thinkcube.com>
 * This file contains activity contractor Functions
 */

namespace JobCard\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class ActivityContractor
{

    public $activityContractorId;
    public $activityId;
    public $contractorId;
    public $activityContractorCost;
    protected $inputFilter;

    public function exchangeArray($data)
    {
        $this->activityContractorId = (!empty($data['activityContractorId'])) ? $data['activityContractorId'] : null;
        $this->activityId = (!empty($data['activityId'])) ? $data['activityId'] : null;
        $this->contractorId = (!empty($data['contractorId'])) ? $data['contractorId'] : null;
        $this->activityContractorCost = (!empty($data['activityContractorCost'])) ? $data['activityContractorCost'] : null;
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory = new InputFactory();

            $inputFilter->add($factory->createInput(array(
                        'name' => 'activityContractorId',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'Int'),
                        ),
            )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'activityId',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'Int'),
                        ),
            )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'contractorId',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'Int'),
                        ),
            )));


            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

}
