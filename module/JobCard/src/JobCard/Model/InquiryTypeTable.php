<?php

/**
 * @author Sandun <sandun@thinkcube.com>
 * This file contains Inquiry Type Table Functions
 */

namespace JobCard\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Predicate\PredicateSet;
use Zend\Db\Sql\Predicate\Operator;

class InquiryTypeTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll($paginated = FALSE)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('inquiryType');
        $select->order('inquiryTypeName DESC');
        $select->join('entity', 'inquiryType.entityID = entity.entityID', array('deleted'));
        $select->where(array('deleted' => '0'));
        if ($paginated) {
            $paginatorAdapter = new DbSelect(
                    $select, $this->tableGateway->getAdapter());
            $paginator = new Paginator($paginatorAdapter);
            return $paginator;
        } else {
            $statement = $sql->prepareStatementForSqlObject($select);
            $result = $statement->execute();
            return $result;
        }
    }

    //save inquiry type function
    public function saveInquiryType(InquiryType $inquiryType)
    {
        $inquiryTypeData = array(
            'inquiryTypeName' => $inquiryType->inquiryTypeName,
            'inquiryTypeCode' => $inquiryType->inquiryTypeCode,
            'entityID' => $inquiryType->entityID,
        );
        if ($this->tableGateway->insert($inquiryTypeData)) {
            $result = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
            return $result;
        } else {
            return FALSE;
        }
    }

    //get inquiry type by inquiry type code
    public function getInquiryTypeByInquiryTypeCode($inquiryTybeCode)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('inquiryType')
                ->columns(array("*"))
                ->join('entity', 'inquiryType.entityID=  entity.entityID', array("*"), "left")
                ->where(array('inquiryType.inquiryTypeCode' => $inquiryTybeCode, "entity.deleted" => 0));
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if (!$rowset) {
            return NULL;
        }
        return $rowset;
    }

    //update inquiry type by inquiry type id
    public function updateInquiryType(InquiryType $inquiryType)
    {
        $inquiryTypeData = array(
            'inquiryTypeName' => $inquiryType->inquiryTypeName,
        );
        try {
            $this->tableGateway->update($inquiryTypeData, array('inquiryTypeId' => $inquiryType->inquiryTypeID));
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
        return true;
    }

    //get inquiry Type for search
    public function getInquiryTypeforSearch($keyword)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('inquiryType')
                ->columns(array(
                    'posi' => new Expression('LEAST(IF(POSITION(\'' . $keyword . '\' in inquiryTypeName )>0,POSITION(\'' . $keyword . '\' in inquiryTypeName), 9999),'
                            . 'IF(POSITION(\'' . $keyword . '\' in inquiryTypeCode )>0,POSITION(\'' . $keyword . '\' in inquiryTypeCode), 9999)) '),
                    'len' => new Expression('LEAST(CHAR_LENGTH(inquiryTypeName ), CHAR_LENGTH(inquiryTypeCode )) '),
                    '*'))
                ->join('entity', 'inquiryType.entityID = entity.entityID', array('*'), 'left');
        $select->where(new PredicateSet(array(new Operator('inquiryType.inquiryTypeName', 'like', '%' . $keyword . '%'), new Operator('inquiryType.inquiryTypeCode', 'like', '%' . $keyword . '%')), PredicateSet::OP_OR));
        $select->where(new PredicateSet(array(new Operator('entity.deleted', '=', '0'))));
        $select->where(new PredicateSet(array(new Operator('inquiryType.inquiryTypeStatus', '=', '1'))));
        $select->order(new Expression('IF(posi > 0, posi, 9999) ASC, len ASC'));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return (object) $results;
    }

    //get inquiry type by inquiry type id
    public function getInquiryTypeByInquiryTypeID($inquiryTybeID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('inquiryType')
                ->columns(array("*"))
                ->join('entity', 'inquiryType.entityID=  entity.entityID', array("*"), "left")
                ->where(array('inquiryType.inquiryTypeId' => $inquiryTybeID, "entity.deleted" => 0));
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if (!$rowset) {
            return NULL;
        }
        return $rowset;
    }

    public function updateInquiryTypeState($inquiryTypeID, $status)
    {
        $inquiryTypeData = array(
            'inquiryTypeStatus' => $status,
        );
        try {
            $this->tableGateway->update($inquiryTypeData, array('inquiryTypeID' => $inquiryTypeID));
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
        return true;
    }

}
