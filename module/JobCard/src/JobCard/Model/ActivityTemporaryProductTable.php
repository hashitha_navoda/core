<?php

/**
 * @author Ashan <ashan@thinkcube.com>
 * This file contains activity temporary product table Functions
 */

namespace JobCard\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Predicate\PredicateSet;
use Zend\Db\Sql\Predicate\Operator;

class ActivityTemporaryProductTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function saveActivityTemporaryProduct(ActivityTemporaryProduct $activityTempProduct)
    {
        $actTempProductData = array(
            'temporaryProductID' => $activityTempProduct->temporaryProductID,
            'activityId' => $activityTempProduct->activityId,
        );
        if ($this->tableGateway->insert($actTempProductData)) {
            $result = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
            return $result;
        } else {
            return FALSE;
        }
    }

    public function getActivityTempProdcutDetailsByActivityID($activityID)
    {

        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('temporaryProduct')
                ->columns(array('*'))
                ->join('activityTemporaryProduct', 'activityTemporaryProduct.temporaryProductID=temporaryProduct.temporaryProductID', array('activityId'), "left")
                ->join('uom', 'uom.uomID = temporaryProduct.uomID', array('uomAbbr', 'uomID' => new Expression('temporaryProduct.uomID')), 'left')
                ->where(array('activityTemporaryProduct.activityId' => $activityID));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        return (object) $results;
    }

    public function deleteActivityTempProdcutDetailsByActivityID($activityID)
    {
        try {
            $this->tableGateway->delete(array('activityId' => $activityID));
            return true;
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
            return false;
        }
    }

}
