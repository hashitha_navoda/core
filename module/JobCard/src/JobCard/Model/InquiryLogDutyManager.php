<?php

namespace JobCard\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class InquiryLogDutyManager
{

    public $inquiryLogDutyManagerID;
    public $inquiryLogID;
    public $employeeID;

    public function exchangeArray($data)
    {
        $this->inquiryLogDutyManagerID = (!empty($data['inquiryLogDutyManagerID'])) ? $data['inquiryLogDutyManagerID'] : null;
        $this->inquiryLogID = (!empty($data['inquiryLogID'])) ? $data['inquiryLogID'] : null;
        $this->employeeID = (!empty($data['employeeID'])) ? $data['employeeID'] : null;
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {

    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

}
