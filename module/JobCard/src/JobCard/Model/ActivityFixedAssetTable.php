<?php

namespace JobCard\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Predicate\PredicateSet;
use Zend\Db\Sql\Predicate\Operator;

class ActivityFixedAssetTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function saveActivityFixedAsset(ActivityFixedAsset $activityFixedAsset)
    {
        $activityFixedAssetData = array(
            'activityId' => $activityFixedAsset->activityId,
            'locationProductId' => $activityFixedAsset->locationProductId,
            'activityFixedAssetQuantity' => $activityFixedAsset->activityFixedAssetQuantity,
            'activityFixedAssetCost' => $activityFixedAsset->activityFixedAssetCost,
        );
        if ($this->tableGateway->insert($activityFixedAssetData)) {
            $result = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
            return $result;
        } else {
            return FALSE;
        }
    }

    public function getFixedAssetsByActivityID($activityId)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('activityFixedAsset')
                ->columns(array('*'))
                ->where(array('activityId' => $activityId));
        $select->join('locationProduct', 'activityFixedAsset.locationProductId = locationProduct.locationProductID', ['productID', 'locationID'], 'inner');
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if (!$rowset) {
            return NULL;
        }
        return $rowset;
    }

    public function deleteActivityFixedAssets($activityId)
    {
        try {
            $result = $this->tableGateway->delete(array('activityId' => $activityId));
            return $result;
        } catch (\Exception $e) {
            $error = 0;
            return $error;
        }
    }

}
