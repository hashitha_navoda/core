<?php

namespace JobCard\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterInterface;

/**
 * Class Vehicle
 * @package JobCard\Model
 * @author Madawa Chandraratne <madawa@thinkcube.com>
 */
class Vehicle
{
    /**
     * @var int Auto incremented vehicle Id. (Primary key)
     */
    public $vehicleId;
    
    /**
     * @var string vehicle made.
     */
    public $vehicleMake;
    
    /**
     * @var string vehicle model.
     */
    public $vehicleModel;
    
    /**
     * @var string vehicle registration number.
     */
    public $vehicleRegistrationNumber;
    
    /**
     * @var string vehicle cost per kilo meter.
     */
    public $vehicleCost;
    
    /**
     * @var int entity id.
     */
    public $entityId;
    
    protected $inputFilter;

    /**
     * Exchange array to map data with the object.
     * @param $data
     */
    public function exchangeArray($data)
    {
        $this->vehicleId    = (!empty($data['vehicleId'])) ? $data['vehicleId'] : null;
        $this->vehicleMake  = (!empty($data['vehicleMake'])) ? $data['vehicleMake'] : null;
        $this->vehicleModel = (!empty($data['vehicleModel'])) ? $data['vehicleModel'] : null;
        $this->vehicleRegistrationNumber = (!empty($data['vehicleRegistrationNumber'])) ? $data['vehicleRegistrationNumber'] : null;
        $this->vehicleCost  = (!empty($data['vehicleCost'])) ? $data['vehicleCost'] : null;
        $this->entityId     = (!empty($data['entityId'])) ? $data['entityId'] : null;
    }
    
    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory = new InputFactory();

            $inputFilter->add(
                $factory->createInput(array(
                    'name' => 'vehicleId',
                    'required' => false,
                    'filters' => array(
                        array('name' => 'Int'),
                    ),
                )
            ));

            $inputFilter->add(
                $factory->createInput(array(
                    'name' => 'vehicleMake',
                    'required' => true,
                    'filters' => array(
                        array('name' => 'StripTags'),
                        array('name' => 'StringTrim'),
                    ),
                    'validators' => array(
                        array(
                            'name' => 'StringLength',
                            'options' => array(
                                'encoding' => 'UTF-8',
                                'min' => 1,
                                'max' => 255,
                            ),
                        ),
                    ),
                )
            ));
            
            $inputFilter->add(
                $factory->createInput(array(
                    'name' => 'vehicleModel',
                    'required' => true,
                    'filters' => array(
                        array('name' => 'StripTags'),
                        array('name' => 'StringTrim'),
                    ),
                    'validators' => array(
                        array(
                            'name' => 'StringLength',
                            'options' => array(
                                'encoding' => 'UTF-8',
                                'min' => 1,
                                'max' => 255,
                            ),
                        ),
                    ),
                )
            ));
            
            $inputFilter->add(
                $factory->createInput(array(
                    'name' => 'vehicleRegistrationNumber',
                    'required' => true,
                    'filters' => array(
                        array('name' => 'StripTags'),
                        array('name' => 'StringTrim'),
                    ),
                    'validators' => array(
                        array(
                            'name' => 'StringLength',
                            'options' => array(
                                'encoding' => 'UTF-8',
                                'min' => 1,
                                'max' => 255,
                            ),
                        ),
                    ),
                )
            ));
            
            $inputFilter->add(
                $factory->createInput(array(
                    'name' => 'vehicleCost',
                    'required' => false,
                    'filters' => array(
                        array('name' => 'StripTags'),
                        array('name' => 'StringTrim'),
                    ),
                    'validators' => array(
                        array(
                            'name' => 'Float',
                        ),
                    ),
                )
            ));
            
            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }
    
}
