<?php

namespace JobCard\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Predicate\PredicateSet;
use Zend\Db\Sql\Predicate\Operator;

class JobSupervisorTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function saveSupervisor($jobReference, $jobSupervisor, $jobSupervisorLength)
    {
        for ($i = 0; $i < $jobSupervisorLength; $i++) {
            $temp = $jobSupervisor[$i];
            $jobSupervisorData = array(
                'jobSupervisorID' => $temp,
                'referenceCode' => $jobReference,
            );
            if ($this->tableGateway->insert($jobSupervisorData)) {
                $result = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
            } else {
                return FALSE;
            }
        }
        return $result;
    }

    public function updateSupervisor($jobReference, $jobSupervisor = null, $jobSupervisorLength = null)
    {
        $result = $this->tableGateway->delete(array('referenceCode' => $jobReference));
        if ($jobSupervisor && $jobSupervisorLength) {
            for ($i = 0; $i < $jobSupervisorLength; $i++) {
                $temp = $jobSupervisor[$i];
                $jobSupervisorData = array(
                    'jobSupervisorID' => $temp,
                    'referenceCode' => $jobReference,
                );
                if ($this->tableGateway->insert($jobSupervisorData)) {
                    $result = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
                } else {
                    return FALSE;
                }
            }
        }
        return true;
    }

    public function getJobSupervisorName($ID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('jobSupervisor')
                ->columns(array('*'))
                ->join('employee', 'jobSupervisor.jobSupervisorID=employee.employeeID', array("employeeFirstName", "employeeSecondName"), "left")
                ->where(array('jobSupervisor.referenceCode' => $ID));
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if (!$rowset) {
            return NULL;
        }
        return $rowset;
    }

//this function get job supervisor details that related to the employee ID
    public function getJobSupervisorDetailsByEmployeeID($ID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('jobSupervisor')
                ->columns(array('*'))
                ->where(array('jobSupervisor.jobSupervisorID' => $ID));
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if (!$rowset) {
            return NULL;
        }
        return $rowset;
    }

    //this function use to get job Supervisors details that related to the job reference
    public function getJobSupervisorsDetailsByJobReference($jobReference)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('jobSupervisor')
                ->columns(array('*'))
                ->join('employee', 'jobSupervisor.jobSupervisorID =  employee.employeeID', array("employeeFirstName", "employeeSecondName"), "left")
                ->where(array('jobSupervisor.referenceCode' => $jobReference));
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if (!$rowset) {
            return NULL;
        }
        return $rowset;
    }

}
