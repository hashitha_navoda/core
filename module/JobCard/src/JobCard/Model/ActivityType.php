<?php

namespace JobCard\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class ActivityType
{

    public $activityTypeID;
    public $activityTypeName;
    public $activityTypeMaxValue;
    public $activityTypeMinValue;
    public $entityID;

    public function exchangeArray($data)
    {
        $this->activityTypeID = (!empty($data['activityTypeID'])) ? $data['activityTypeID'] : null;
        $this->activityTypeName = (!empty($data['activityTypeName'])) ? $data['activityTypeName'] : null;
        $this->activityTypeMaxValue = (!empty($data['activityTypeMaxValue'])) ? $data['activityTypeMaxValue'] : null;
        $this->activityTypeMinValue = (!empty($data['activityTypeMinValue'])) ? $data['activityTypeMinValue'] : null;
        $this->entityID = (!empty($data['entityID'])) ? $data['entityID'] : null;
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory = new InputFactory();

            $inputFilter->add($factory->createInput(array(
                        'name' => 'activityTypeID',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'Int'),
                        ),
            )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'activityTypeName',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'StringLength',
                                'options' => array(
                                    'encoding' => 'UTF-8',
                                    'min' => 1,
                                    'max' => 100,
                                ),
                            ),
                        ),
            )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'activityTypeMaxValue',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'double'),
                        ),
            )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'activityTypeMinValue',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'double'),
                        ),
            )));

            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

}
