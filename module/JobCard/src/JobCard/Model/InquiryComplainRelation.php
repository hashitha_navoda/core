<?php

namespace JobCard\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class InquiryComplainRelation
{

    public $inquiryComplainRelationID;
    public $inquiryLogID;
    public $documentTypeID;
    public $inquiryComplainRelationDocumentID;

    public function exchangeArray($data)
    {
        $this->inquiryComplainRelationID = (!empty($data['inquiryComplainRelationID'])) ? $data['inquiryComplainRelationID'] : null;
        $this->inquiryLogID = (!empty($data['inquiryLogID'])) ? $data['inquiryLogID'] : null;
        $this->documentTypeID = (!empty($data['documentTypeID'])) ? $data['documentTypeID'] : null;
        $this->inquiryComplainRelationDocumentID = (!empty($data['inquiryComplainRelationDocumentID'])) ? $data['inquiryComplainRelationDocumentID'] : null;
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {

    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

}
