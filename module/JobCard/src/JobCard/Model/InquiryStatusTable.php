<?php

/**
 * @author Sandun <sandun@thinkcube.com>
 * This file contains Inquiry status Table Functions
 */

namespace JobCard\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Predicate\PredicateSet;
use Zend\Db\Sql\Predicate\Operator;

class InquiryStatusTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll($paginated = FALSE)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('inquiryStatus');
        $select->join('inquiryStatusType', 'inquiryStatus.inquiryStatusTypeID = inquiryStatusType.inquiryStatusTypeID', array('inquiryStatusTypeName'));
        $select->join('entity', 'inquiryStatus.entityID = entity.entityID', array('deleted'));
        $select->order('inquiryStatusName ASC');
        $select->where(array('deleted' => '0'));
        if ($paginated) {
            $paginatorAdapter = new DbSelect(
                    $select, $this->tableGateway->getAdapter());
            $paginator = new Paginator($paginatorAdapter);
            return $paginator;
        } else {
            $statement = $sql->prepareStatementForSqlObject($select);
            $result = $statement->execute();
            return $result;
        }
    }

    //save inquiry status function
    public function saveInquiryStatus(InquiryStatus $inquiryStatus)
    {
        $inquiryStatusData = array(
            'inquiryStatusName' => $inquiryStatus->inquiryStatusName,
            'inquiryStatusTypeID' => $inquiryStatus->inquiryStatusTypeID,
            'entityID' => $inquiryStatus->entityID,
        );
        if ($this->tableGateway->insert($inquiryStatusData)) {
            $result = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
            return $result;
        } else {
            return FALSE;
        }
    }

    //get inquiry status by inquiry status name
    public function getInquiryStatusByInquiryStatusName($inquiryStatusName)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('inquiryStatus')
                ->columns(array("*"))
                ->join('entity', 'inquiryStatus.entityID=  entity.entityID', array("*"), "left")
                ->where(array('inquiryStatus.inquiryStatusName' => $inquiryStatusName, "entity.deleted" => 0));
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if (!$rowset) {
            return NULL;
        }
        return $rowset;
    }

    //update inquiry status by inquiry status id
    public function updateInquiryStatus(InquiryStatus $inquiryStatus)
    {
        $inquiryStatusData = array(
            'inquiryStatusTypeID' => $inquiryStatus->inquiryStatusTypeID,
        );
        try {
            $this->tableGateway->update($inquiryStatusData, array('inquiryStatusID' => $inquiryStatus->inquiryStatusID));
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
        return true;
    }

    //get inquiry Status or search
    public function getInquiryStatusforSearch($keyword)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('inquiryStatus')
                ->columns(array(
                    'posi' => new Expression('LEAST(IF(POSITION(\'' . $keyword . '\' in inquiryStatusName )>0,POSITION(\'' . $keyword . '\' in inquiryStatusTypeName), 9999),'
                            . 'IF(POSITION(\'' . $keyword . '\' in inquiryStatusName )>0,POSITION(\'' . $keyword . '\' in inquiryStatusTypeName), 9999)) '),
                    'len' => new Expression('LEAST(CHAR_LENGTH(inquiryStatusName ), CHAR_LENGTH(inquiryStatusTypeName )) '),
                    '*'))
                ->join('inquiryStatusType', 'inquiryStatus.inquiryStatusTypeID = inquiryStatusType.inquiryStatusTypeID', array('*'), 'left')
                ->join('entity', 'inquiryStatus.entityID = entity.entityID', array('*'), 'left');
        $select->where(new PredicateSet(array(new Operator('inquiryStatus.inquiryStatusName', 'like', '%' . $keyword . '%'), new Operator('inquiryStatusType.inquiryStatusTypeName', 'like', '%' . $keyword . '%')), PredicateSet::OP_OR));
        $select->where(new PredicateSet(array(new Operator('entity.deleted', '=', '0'))));
        $select->where(new PredicateSet(array(new Operator('inquiryStatus.inquiryStatusStatus', '=', '1'))));
        $select->order(new Expression('IF(posi > 0, posi, 9999) ASC, len ASC'));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return (object) $results;
    }

    //get inquiry status by inquiry status id
    public function getInquiryStatusByInquiryStatusID($inquiryTybeID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('inquiryStatus')
                ->columns(array("*"))
                ->join('entity', 'inquiryStatus.entityID=  entity.entityID', array("*"), "left")
                ->where(array('inquiryStatus.inquiryStatusID' => $inquiryTybeID, "entity.deleted" => 0));
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if (!$rowset) {
            return NULL;
        }
        return $rowset;
    }

    public function updateInquiryStatusState($inquiryStatusID, $status)
    {
        $inquiryStatusData = array(
            'inquiryStatusStatus' => $status,
        );
        try {
            $this->tableGateway->update($inquiryStatusData, array('inquiryStatusID' => $inquiryStatusID));
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
        return true;
    }

}
