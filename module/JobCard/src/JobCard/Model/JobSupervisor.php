<?php

namespace JobCard\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class JobSupervisor
{

    public $jobSupervisorTableID;
    public $jobSupervisorID;
    public $referenceCode;

    public function exchangeArray($data)
    {
        $this->jobSupervisorTableID = (!empty($data['jobSupervisorTableID'])) ? $data['jobSupervisorTableID'] : null;
        $this->jobSupervisorID = (!empty($data['jobSupervisorID'])) ? $data['jobSupervisorID'] : null;
        $this->referenceCode = (!empty($data['referenceCode'])) ? $data['referenceCode'] : null;
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {
//        if (!$this->inputFilter) {
//            $inputFilter = new InputFilter();
//            $factory = new InputFactory();
//
//            $inputFilter->add($factory->createInput(array(
//                        'name' => 'designationID',
//                        'required' => false,
//                        'filters' => array(
//                            array('name' => 'Int'),
//                        ),
//            )));
//            $inputFilter->add($factory->createInput(array(
//                        'name' => 'designationName',
//                        'required' => true,
//                        'filters' => array(
//                            array('name' => 'StripTags'),
//                            array('name' => 'StringTrim'),
//                        ),
//                        'validators' => array(
//                            array(
//                                'name' => 'StringLength',
//                                'options' => array(
//                                    'encoding' => 'UTF-8',
//                                    'min' => 1,
//                                    'max' => 100,
//                                ),
//                            ),
//                        ),
//            )));
//
//            $this->inputFilter = $inputFilter;
//        }
//
//        return $this->inputFilter;
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

}
