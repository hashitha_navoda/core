<?php

namespace JobCard\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Predicate\PredicateSet;
use Zend\Db\Sql\Predicate\Operator;

class DivisionTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll($paginated = FALSE)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('division');
        $select->order('divisionName DESC');
        $select->join('entity', 'division.entityID=  entity.entityID', array("deleted"), "left");
        $select->where(array('entity.deleted' => 0));

        if ($paginated) {
            $paginatorAdapter = new DbSelect(
                    $select, $this->tableGateway->getAdapter());
            $paginator = new Paginator($paginatorAdapter);
            return $paginator;
        } else {
            $statement = $sql->prepareStatementForSqlObject($select);
            $result = $statement->execute();
            return $result;
        }
    }

    public function getAllDivisions()
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('division')
                ->columns(array('*'))
                ->join('entity', 'division.entityID=  entity.entityID', array("*"), "left")
                ->order('divisionID ASC');
        $select->where(array('entity.deleted' => 0, 'division.divisionStatus' => '1'));
        $query = $sql->prepareStatementForSqlObject($select);
        $rowset = $query->execute();
//        $row = (object) $rowset->current();

        if (!$rowset) {
            return NULL;
        } else {
            return $rowset;
        }
    }

    public function getDivisions($divisionName)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('division')
                ->columns(array('divisionName'))
                ->join('entity', 'division.entityID=  entity.entityID', array("*"), "left")
                ->where(array('division.divisionName' => $divisionName, "entity.deleted" => 0));
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if (!$rowset) {
            return NULL;
        }
        return $rowset;
    }

//get devision that related to given division ID
    public function getDivisionByDivisionID($divisionID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('division')
                ->columns(array('*'))
                ->where(array('division.divisionID' => $divisionID));
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if (!$rowset) {
            return NULL;
        }
        return $rowset;
    }

    public function saveDivisions($divisionData)
    {
        $DivisionData = array(
            'divisionName' => $divisionData->divisionName,
            'entityID' => $divisionData->entityID,
            'divisionStatus' => '1',
        );
        if ($this->tableGateway->insert($DivisionData)) {
            $result = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
//            return $result;
            return true;
        } else {
            return FALSE;
        }
    }

    public function updateDivision($divisionData)
    {
        $DivisionData = array(
            'divisionName' => $divisionData->divisionName,
        );
        try {
            $this->tableGateway->update($DivisionData, array('divisionID' => $divisionData->divisionID));
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
        return true;
    }

    public function deleteDivisionByDivisionID($divisionData)
    {
        try {
            $result = $this->tableGateway->delete(array('divisionID' => $divisionData->divisionID));
            return $result;
        } catch (\Exception $e) {
            $error = 0;
            return $error;
        }
    }

    public function DivisionSearchByKey($keyword)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('division')
                ->columns(array('*'))
                ->join('entity', 'division.entityID = entity.entityID', array('*'), 'left');
        $select->where(new PredicateSet(array(new Operator('division.divisionName', 'like', '%' . $keyword . '%'))));
        $select->where(new PredicateSet(array(new Operator('entity.deleted', '=', '0'))));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return (object) $results;
    }

    public function updateDivisionState($divisionID, $status)
    {
        $divisionData = array(
            'divisionStatus' => $status,
        );
        try {
            $this->tableGateway->update($divisionData, array('divisionID' => $divisionID));
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
        return true;
    }

}
