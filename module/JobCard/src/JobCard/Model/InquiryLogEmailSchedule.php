<?php

namespace JobCard\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

/**
 * @author Sharmilan <sharmilan@thinkcube.com>
 * Email send by schedule to Inquiry Log Assigned Employee
 */
class InquiryLogEmailSchedule
{

    public $inquiryLogEmailScheduleID;
    public $inquiryLogID;
    public $inquiryLogEmailScheduleDateTime;
    public $inquirySetupID;
    public $createdAt;
    public $isEmailSend;

    public function exchangeArray($data)
    {
        $this->inquiryLogEmailScheduleID = (!empty($data['inquiryLogEmailScheduleID'])) ? $data['inquiryLogEmailScheduleID'] : null;
        $this->inquiryLogID = (!empty($data['inquiryLogID'])) ? $data['inquiryLogID'] : null;
        $this->inquiryLogEmailScheduleDateTime = (!empty($data['inquiryLogEmailScheduleDateTime'])) ? $data['inquiryLogEmailScheduleDateTime'] : null;
        $this->inquirySetupID = (!empty($data['inquirySetupID'])) ? $data['inquirySetupID'] : null;
        $this->createdAt = (!empty($data['createdAt'])) ? $data['createdAt'] : null;
        $this->isEmailSend = (!empty($data['isEmailSend'])) ? $data['isEmailSend'] : 0;
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory = new InputFactory();

            $inputFilter->add($factory->createInput(array(
                        'name' => 'inquiryLogEmailScheduleID',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'Int'),
                        ),
            )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'inquiryLogID',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'Int'),
                        ),
            )));

            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

}
