<?php

/**
 * @author Madawa Chandrarathne <madawa@thinkcube.com>
 */

namespace JobCard\Model;

class ActivityVehicle
{
    /**
     * @var int Auto incremented activityVehicleId. (Primary key)
     */
    public $activityVehicleId;
    
    /**
     * @var decimal starting milage of the vehicle 
     */
    public $activityVehicleStartMileage;
    
    /**
     * @var decimal ending milage of the vehicle 
     */
    public $activityVehicleEndMileage;
    
    /**
     * @var decimal cost of the vehicle 
     */
    public $activityVehicleCost;
    
    /**
     * @var int vehicle id 
     */
    public $vehicleId;
    
    /**
     * @var int activity id 
     */
    public $activityId;
    
    /**
     * @var int entity id.
     */
    public $entityId;

    public function exchangeArray($data)
    {
        $this->activityVehicleId = (!empty($data['activityVehicleId'])) ? $data['activityVehicleId'] : null;
        $this->activityVehicleStartMileage = (!empty($data['activityVehicleStartMileage'])) ? $data['activityVehicleStartMileage'] : null;
        $this->activityVehicleEndMileage = (!empty($data['activityVehicleEndMileage'])) ? $data['activityVehicleEndMileage'] : null;
        $this->activityVehicleCost = (!empty($data['activityVehicleCost'])) ? $data['activityVehicleCost'] : null;
        $this->vehicleId = (!empty($data['vehicleId'])) ? $data['vehicleId'] : null;
        $this->activityId = (!empty($data['activityId'])) ? $data['activityId'] : null;
        $this->entityId = (!empty($data['entityId'])) ? $data['entityId'] : null;
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

}
