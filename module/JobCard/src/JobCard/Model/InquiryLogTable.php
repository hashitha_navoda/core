<?php

namespace JobCard\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Predicate\PredicateSet;
use Zend\Db\Sql\Predicate\Operator;

class InquiryLogTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll($locationID, $paginated = FALSE)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('inquiryLog');
        $select->join('customer', 'inquiryLog.customerID=  customer.customerID', array("customerName", "customerShortName"), "left");
        $select->join('project', 'inquiryLog.projectID=  project.projectId', array("projectCode"), "left");
        $select->join('job', 'inquiryLog.jobID=  job.jobId', array("jobReferenceNumber"), "left");
//        $select->join('inquiryComplainRelation', 'inquiryLog.inquiryLogID=  inquiryComplainRelation.inquiryLogID', array("documentTypeID", "inquiryComplainRelationDocumentID"), "left");
        $select->join('activity', 'inquiryLog.activityID=  activity.activityId', array("activityCode"), "left");
        $select->join('entity', 'inquiryLog.entityID=  entity.entityID', array("deleted"), "left");
        $select->order('inquiryLog.inquiryLogID DESC');
        $select->where(array('inquiryLog.locationID' => $locationID, 'entity.deleted' => 0));
        if ($paginated) {
            $paginatorAdapter = new DbSelect(
                    $select, $this->tableGateway->getAdapter());
            $paginator = new Paginator($paginatorAdapter);
            return $paginator;
        } else {
            $statement = $sql->prepareStatementForSqlObject($select);
            $result = $statement->execute();
            return $result;
        }
    }

    //get all inqury details
    public function getInquiryLogDetails($inquiryLogID = NULL, $userId = NULL, $customerId = NULL, $dutyManagerId = NULL, $fromDate = NULL, $toDate = NULL)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('inquiryLog');
        $select->join('customer', 'inquiryLog.customerID=  customer.customerID', array("customerName", "customerShortName", "customerCode"), "left");
        $select->join('user', 'inquiryLog.inquiryLogCurrentUserID=  user.userID', array("userUsername"), "left");
        $select->join('project', 'inquiryLog.projectID=  project.projectId', array("projectCode"), "left");
        $select->join('job', 'inquiryLog.jobID=  job.jobId', array("jobReferenceNumber"), "left");
        $select->join('activity', 'inquiryLog.activityID=  activity.activityId', array("activityCode"), "left");
        $select->join('entity', 'inquiryLog.entityID=  entity.entityID', array("deleted"), "left");
        $select->join('inquiryLogDutyManager', 'inquiryLog.inquiryLogID=  inquiryLogDutyManager.inquiryLogID', array("inquiryLogDutyManagerID"), "left");
        $select->join('employee', 'inquiryLogDutyManager.employeeID=  employee.employeeID', array("employeeFirstName", "employeeSecondName", "employeeID"), "left");
        /*
         * @auther Sandun <sandun@thinkcube.com>
         * I have edited this function to get inguiry log according to passing variable
         */
        if ($fromDate != NULL && $toDate != NULL) {
            $plusToDate = date('Y-m-d H:i:s', strtotime($toDate . ' + 1 day'));
            $select->where->between('inquiryLogDateAndTime', $fromDate, $plusToDate);
            $select->where(array('employee.employeeStatus' => 1));
        }
        if ($inquiryLogID != NULL) {

            $select->where(array('inquiryLog.inquiryLogID' => $inquiryLogID));
            $select->where(array('entity.deleted' => 0));
        }
        if ($userId != NULL) {
            $select->where(array('employee.employeeStatus' => 1));
            $select->where->in('inquiryLog.inquiryLogCurrentUserID', $userId);
        }
        if ($customerId != NULL) {
            $select->where(array('employee.employeeStatus' => 1));
            $select->where->in('inquiryLog.customerID', $customerId);
        }
        if ($dutyManagerId != NULL) {
            $select->where(array('employee.employeeStatus' => 1));
            $select->where->in('inquiryLogDutyManager.employeeID', $dutyManagerId);
            $select->order(array('inquiryLogID', 'employeeID'));
        }

        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if (!$rowset) {
            return NULL;
        }

        return $rowset;
    }

    //this function get inquiryLog data that related to the given inquiryType ID
    public function getInquiryLogDetailsByInquiryTypeID($inquiryTypeID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('inquiryLog');
        $select->join('entity', 'inquiryLog.entityID=  entity.entityID', array("deleted"), "left");
        $select->where(array('inquiryLog.inquiryTypeID' => $inquiryTypeID, 'entity.deleted' => 0));
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if (!$rowset) {
            return NULL;
        }
        return $rowset;
    }

//this function use to get inquiry log data that related to given inquiry status ID
    public function getInquiryLogDetailsByInquiryStatusID($inquiryStatusID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('inquiryLog');
        $select->join('entity', 'inquiryLog.entityID=  entity.entityID', array("deleted"), "left");
        $select->where(array('inquiryLog.inquiryStatusID' => $inquiryStatusID, 'entity.deleted' => 0));
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if (!$rowset) {
            return NULL;
        }
        return $rowset;
    }

//this function use to get inquiry log data that related to given projectID,JobID or activityID
    public function getInquirylogs($data)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('inquiryLog')
                ->columns(array('*'))
                ->where(array('inquiryLog.inquiryLogDescription' => $data->inquiryLogDescription));
        if ($data->projectID) {
            $select->where(array('inquiryLog.projectID' => $data->projectID));
        }
        if ($data->jobID) {
            $select->where(array('inquiryLog.jobID' => $data->jobID));
        }
        if ($data->activityID) {
            $select->where(array('inquiryLog.activityID' => $data->activityID));
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if (!$rowset) {
            return NULL;
        }
        return $rowset;
    }

//save inquiry log records.
    public function saveInquiryLog($inquiryLogData)
    {
        $inquiryLogSaveData = array(
            'customerID' => $inquiryLogData->customerID,
            'customerProfileID' => $inquiryLogData->customerProfileID,
            'inquiryTypeID' => $inquiryLogData->inquiryTypeID,
            'inquiryLogDateAndTime' => $inquiryLogData->inquiryLogDateAndTime,
            'inquiryLogDescription' => $inquiryLogData->inquiryLogDescription,
            'inquiryLogCurrentUserID' => $inquiryLogData->inquiryLogCurrentUserID,
            'inquiryLogReference' => $inquiryLogData->inquiryLogReference,
            'inquiryStatusID' => $inquiryLogData->inquiryStatusID,
            'projectID' => $inquiryLogData->projectID,
            'jobID' => $inquiryLogData->jobID,
            'activityID' => $inquiryLogData->activityID,
            'locationID' => $inquiryLogData->locationID,
            'inquiryLogTP1' => $inquiryLogData->inquiryLogTP1,
            'inquiryLogTP2' => $inquiryLogData->inquiryLogTP2,
            'inquiryLogAddress' => $inquiryLogData->inquiryLogAddress,
            'statusID' => 3,
            'entityID' => $inquiryLogData->entityID,
            'inquiryComplainType' => $inquiryLogData->inquiryComplainType,
        );
        if ($this->tableGateway->insert($inquiryLogSaveData)) {
            $result = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue('inquiryLogID');
            return $result;
        } else {
            return FALSE;
        }
    }

    //this function use to update project id to existing inquiry Log reports.
    public function updateInquiryLogProjectID($inquiryLogData)
    {
        $inquiryLogData = (object) $inquiryLogData;
        $inquiryLogSaveData = array(
            'projectID' => $inquiryLogData->projectID,
        );
        try {
            $this->tableGateway->update($inquiryLogSaveData, array('inquiryLogID' => (int) $inquiryLogData->inquiryLogID));
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
        return true;
    }

//this function use to update job Id
    public function updateInquiryLogJobID($inquiryLogData)
    {
        $inquiryLogData = (object) $inquiryLogData;
        $inquiryLogSaveData = array(
            'jobID' => $inquiryLogData->jobID,
        );
        try {
            $this->tableGateway->update($inquiryLogSaveData, array('inquiryLogID' => (int) $inquiryLogData->inquiryLogID));
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
        return true;
    }

//this function use to update activity Id
    public function updateInquiryLogActivityID($inquiryLogData)
    {
        $inquiryLogData = (object) $inquiryLogData;
        $inquiryLogSaveData = array(
            'activityID' => $inquiryLogData->activityID,
        );
        try {
            $this->tableGateway->update($inquiryLogSaveData, array('inquiryLogID' => (int) $inquiryLogData->inquiryLogID));
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
        return true;
    }

//update existing inquiry log records
    public function updateInquiryLog($inquiryLogData)
    {
        $inquiryLogSaveData = array(
            'customerID' => $inquiryLogData->customerID,
            'customerProfileID' => $inquiryLogData->customerProfileID,
            'inquiryTypeID' => $inquiryLogData->inquiryTypeID,
            'inquiryLogDateAndTime' => $inquiryLogData->inquiryLogDateAndTime,
            'inquiryLogDescription' => $inquiryLogData->inquiryLogDescription,
            'inquiryLogCurrentUserID' => $inquiryLogData->inquiryLogCurrentUserID,
            'inquiryLogReference' => $inquiryLogData->inquiryLogReference,
            'inquiryStatusID' => $inquiryLogData->inquiryStatusID,
            'projectID' => $inquiryLogData->projectID,
            'jobID' => $inquiryLogData->jobID,
            'activityID' => $inquiryLogData->activityID,
            'locationID' => $inquiryLogData->locationID,
            'inquiryLogTP1' => $inquiryLogData->inquiryLogTP1,
            'inquiryLogTP2' => $inquiryLogData->inquiryLogTP2,
            'inquiryLogAddress' => $inquiryLogData->inquiryLogAddress,
        );
        try {
            $this->tableGateway->update($inquiryLogSaveData, array('inquiryLogID' => (int) $inquiryLogData->inquiryLogID));
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
        return true;
    }

//this function use to delete existing inquiry log records from the database
    public function deleteInquiryLogByInquiryLogID($inquiryLogData)
    {
        try {
            $result = $this->tableGateway->delete(array('inquiryLogID' => (int) $inquiryLogData->inquiryLogID));
            return $result;
        } catch (\Exception $e) {
            $error = 0;
            return $error;
        }
    }

//use for search
    public function inquiryLogSearchByKey($searchKey, $locationID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('inquiryLog');
        $select->join('customer', 'inquiryLog.customerID=  customer.customerID', array("customerName", "customerShortName"), "left");
        $select->join('project', 'inquiryLog.projectID=  project.projectId', array("projectCode"), "left");
        $select->join('job', 'inquiryLog.jobID=  job.jobId', array("jobReferenceNumber"), "left");
        $select->join('activity', 'inquiryLog.activityID=  activity.activityId', array("activityCode"), "left");
        $select->join('entity', 'inquiryLog.entityID=  entity.entityID', array("deleted"), "left");
        $select->where(new PredicateSet(array(new Operator('inquiryLog.inquiryLogReference', 'like', '%' . $searchKey . '%'), new Operator('customer.customerName', 'like', '%' . $searchKey . '%'), new Operator('customer.customerCode', 'like', '%' . $searchKey . '%')), PredicateSet::OP_OR));
        $select->where(new PredicateSet(array(new Operator('inquiryLog.locationID', '=', $locationID))));
        $select->where(new PredicateSet(array(new Operator('entity.deleted', '=', 0))));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return (object) $results;
    }

//this function use to get inquiry log details that related to the given Inquiry Reference code
    public function getInquiryLogByCode($inqReferenceCode)
    {
        $rowset = $this->tableGateway->select(array('inquiryLogReference' => $inqReferenceCode));
        $row = $rowset->current();
        if (!$row) {
            return NULL;
        }
        return $row;
    }

    /**
     * Get Inquiry Details By inquiryLogID with deleted also, Use For Email to employee
     * @author Sharmilan <sharmilan@thinkcube.com>
     * @param type $inquiryLogID
     * @return null
     */
    public function getInquiryLogDetailsWithEmployeeDetails($inquiryLogID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('inquiryLog');
        $select->join('customer', 'inquiryLog.customerID=  customer.customerID', array("customerName", "customerShortName"), "left");
        $select->join('inquiryLogDutyManager', 'inquiryLog.inquiryLogID =  inquiryLogDutyManager.inquirylogID', array("employeeID"), "left");
        $select->join('employee', 'inquiryLogDutyManager.employeeID=  employee.employeeID', array("*"), "left");
        $select->join('project', 'inquiryLog.projectID=  project.projectId', array("projectCode"), "left");
        $select->join('job', 'inquiryLog.jobID=  job.jobId', array("jobReferenceNumber"), "left");
        $select->join('activity', 'inquiryLog.activityID=  activity.activityId', array("activityCode"), "left");
        $select->join('entity', 'inquiryLog.entityID=  entity.entityID', array("deleted"), "left");
        $select->where(array('inquiryLog.inquiryLogID' => $inquiryLogID));
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if (!$rowset) {
            return NULL;
        }
        return $rowset;
    }

    /**
     * Search Inquiry logs
     * @author sharmilan <sharmilan@thinkcube.com>
     * @param type $locationID
     * @param type $searchKey
     * @return type
     */
    public function searchInquiryLogForDropdown($locationID, $searchKey)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('inquiryLog');
        $select->join('entity', 'inquiryLog.entityID=  entity.entityID', array("deleted"), "left");
        $select->where(['inquiryLog.locationID' => $locationID]);
        $select->where->notEqualTo('entity.deleted',1);
        $select->where->like('inquiryLog.inquiryLogReference', '%' . $searchKey . '%');
        $select->limit(50);

        $statement = $sql->prepareStatementForSqlObject($select);
        return $statement->execute();
    }

    /**
     * Get Inquiry Logs for list view
     * @author sharmilan <sharmilan@thinkcube.com>
     * @param type $locationID
     * @param type $inquiryLogID
     * @return type
     */
    public function getInquiryLogsForView($locationID, $inquiryLogID = false)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('inquiryLog');
        $select->join('customer', 'inquiryLog.customerID=  customer.customerID', array("customerName", "customerShortName", "customerStatus"), "left");
        $select->join('project', 'inquiryLog.projectID=  project.projectId', array("projectCode"), "left");
        $select->join('job', 'inquiryLog.jobID=  job.jobId', array("jobReferenceNumber"), "left");
        $select->join('activity', 'inquiryLog.activityID=  activity.activityId', array("activityCode"), "left");
        $select->join('entity', 'inquiryLog.entityID=  entity.entityID', array("deleted"), "left");

        $select->where(['inquiryLog.locationID' => $locationID]);
        if ($inquiryLogID) {
            $select->where(['inquiryLog.inquiryLogID' => $inquiryLogID]);
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return (object) $results;
    }

    /**
     * @author sharmilan <sharmilan@thinkcube.com>
     * @param type $locationID
     * @param type $fromDate
     * @param type $toDate
     * @return type
     */
    public function getInquiryLogsForViewByDateRange($locationID, $fromDate, $toDate)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('inquiryLog');
        $select->join('customer', 'inquiryLog.customerID=  customer.customerID', array("customerName", "customerShortName", "customerStatus"), "left");
        $select->join('project', 'inquiryLog.projectID=  project.projectId', array("projectCode"), "left");
        $select->join('job', 'inquiryLog.jobID=  job.jobId', array("jobReferenceNumber"), "left");
        $select->join('activity', 'inquiryLog.activityID=  activity.activityId', array("activityCode"), "left");
        $select->join('entity', 'inquiryLog.entityID=  entity.entityID', array("deleted"), "left");
        $select->where(['inquiryLog.locationID' => $locationID]);
        $select->where->between('inquiryLogDateAndTime', $fromDate, $toDate);
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return (object) $results;
    }

    public function searchInquiryLogsForDropDown($locationID, $searchKey)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('inquiryLog');
        $select->columns(array('inquiryLogID', 'inquiryLogReference'));
        $select->join('entity', 'inquiryLog.entityID=  entity.entityID', array("deleted"), "left");
        $select->where(array('inquiryLog.locationID' => $locationID, 'entity.deleted' => 0));
        $select->where->like('inquiryLog.inquiryLogReference', '%' . $searchKey . '%');
        $select->limit(50);
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

    public function getInquiryLogDetailsByInquiryLogID($inquiryLogID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('inquiryLog');
        $select->join('inquiryComplainRelation', 'inquiryLog.inquiryLogID=  inquiryComplainRelation.inquiryLogID', array("*"), "left");
        $select->where(array('inquiryLog.inquiryLogID' => $inquiryLogID));
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if (!$rowset) {
            return NULL;
        }
        return $rowset;
    }

    public function getInquirylogsRelatedToJobID($jobID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('inquiryLog')
                ->columns(array('*'))
                ->join('inquiryComplainRelation', 'inquiryLog.inquiryLogID=inquiryComplainRelation.inquiryLogID', array('*'), 'left')
                ->where(array('inquiryLog.inquiryComplainType' => 'Inquiry', 'inquiryComplainRelation.documentTypeID' => '22', 'inquiryComplainRelation.inquiryComplainRelationDocumentID' => $jobID));
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if (!$rowset) {
            return NULL;
        }
        return $rowset;
    }

    public function getInquirylogsRelatedToActivityID($activityID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('inquiryLog')
                ->columns(array('*'))
                ->join('inquiryComplainRelation', 'inquiryLog.inquiryLogID=inquiryComplainRelation.inquiryLogID', array('*'), 'left')
                ->where(array('inquiryLog.inquiryComplainType' => 'Inquiry', 'inquiryComplainRelation.documentTypeID' => '23', 'inquiryComplainRelation.inquiryComplainRelationDocumentID' => $activityID));
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if (!$rowset) {
            return NULL;
        }
        return $rowset;
    }

    /**
     * Update InquiryLog
     * @param array $inquiryLogData
     * @param int $inquiryLogId
     * @return boolean
     */
    public function updateInquiryLogDetails($inquiryLogData, $inquiryLogId)
    {
        try {
            $this->tableGateway->update($inquiryLogData, array('inquiryLogID' => $inquiryLogId));
            return true;
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
            return false;
        }
    }

}
