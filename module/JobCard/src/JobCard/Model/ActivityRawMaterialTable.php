<?php

namespace JobCard\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Predicate\PredicateSet;
use Zend\Db\Sql\Predicate\Operator;

class ActivityRawMaterialTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function saveActivityRawMaterial(ActivityRawMaterial $activityRawMaterial)
    {

        $activityRawMaterialData = array(
            'activityId' => $activityRawMaterial->activityId,
            'locationProductId' => $activityRawMaterial->locationProductId,
            'activityRawMaterialQuantity' => $activityRawMaterial->activityRawMaterialQuantity,
            'activityRawMaterialCost' => $activityRawMaterial->activityRawMaterialCost,
        );
        if ($this->tableGateway->insert($activityRawMaterialData)) {
            $result = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
            return $result;
        } else {
            return FALSE;
        }
    }

    public function deleteActivityRawMaterial($activityId)
    {
        try {
            $result = $this->tableGateway->delete(array('activityId' => $activityId));
            return $result;
        } catch (\Exception $e) {
            $error = 0;
            return $error;
        }
    }

    public function getRawMaterialByActivityID($activityId)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('activityRawMaterial')
                ->columns(array('*'))
                ->where(array('activityId' => $activityId));
        $select->join('locationProduct', 'activityRawMaterial.locationProductId = locationProduct.locationProductID', ['productID', 'locationID', 'locationProductID'], 'inner');
        $select->join('product', 'product.productID = locationProduct.productID', ['productCode', 'productName']);
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if (!$rowset) {
            return NULL;
        }
        return $rowset;
    }

}
