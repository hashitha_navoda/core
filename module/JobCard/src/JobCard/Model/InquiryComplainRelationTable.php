<?php

namespace JobCard\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Predicate\PredicateSet;
use Zend\Db\Sql\Predicate\Operator;

class InquiryComplainRelationTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    //save inquiry log records.
    public function saveRelatedDocument($inquiryCompalainRelationData)
    {

        $inquiryComplainRelationSaveData = array(
            'inquiryLogID' => $inquiryCompalainRelationData->inquiryLogID,
            'documentTypeID' => $inquiryCompalainRelationData->documentTypeID,
            'inquiryComplainRelationDocumentID' => $inquiryCompalainRelationData->inquiryComplainRelationDocumentID,
        );
        if ($this->tableGateway->insert($inquiryComplainRelationSaveData)) {
            $result = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue('inquiryComplainRelationID');
            return $result;
        } else {
            return FALSE;
        }
    }

//   update existing inquiry log records
    public function updateRelatedDocument($inquiryCompalainRelationData)
    {
        $inquiryComplainRelationSaveData = array(
            'inquiryComplainRelationDocumentID' => $inquiryCompalainRelationData->inquiryComplainRelationDocumentID,
        );
        try {
            $this->tableGateway->update($inquiryComplainRelationSaveData, array('inquiryLogID' => (int) $inquiryCompalainRelationData->inquiryLogID, 'inquiryComplainRelationDocumentID' => $inquiryCompalainRelationData->inquiryComplainRelationDocumentID));
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
        return true;
    }

    public function getDocumentForGivenInqID($inquiryLogID, $documentTypeId = false)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('inquiryComplainRelation');
        $select->where(array('inquiryComplainRelation.inquiryLogID' => $inquiryLogID));
        if ($documentTypeId) {
            $select->where(array('inquiryComplainRelation.documentTypeID' => $documentTypeId));
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if (!$rowset) {
            return NULL;
        }
        return $rowset;
    }

}
