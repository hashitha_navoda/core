<?php

/**
 * @author Ashan Madushka   <ashan@thinkcube.com>
 * This file contains Project Supervisors Table Functions
 */

namespace JobCard\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Predicate\PredicateSet;
use Zend\Db\Sql\Predicate\Operator;

class ProjectSupervisorsTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    //save project owners function
    public function saveProjectSupervisors(ProjectSupervisors $projectSupervisors)
    {
        $projectData = array(
            'projectId' => $projectSupervisors->projectId,
            'employeeId' => $projectSupervisors->employeeId,
        );
        if ($this->tableGateway->insert($projectData)) {
            $result = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
            return $result;
        } else {
            return FALSE;
        }
    }

    //get project Supervisors by projectID
    public function getProjectSupervisorsByProjectID($projectID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('projectSupervisors')
                ->columns(array('*'))
                ->join('employee', "projectSupervisors.employeeId = employee.employeeID", '*', 'left')
                ->where(array('projectSupervisors.projectId' => $projectID));
        $query = $sql->prepareStatementForSqlObject($select);
        $rowset = $query->execute();
        if (!$rowset) {
            return null;
        }
        return $rowset;
    }

    //dete project Supervisors by project and supervisorID
    public function deleteProjectSupervisorsByProjectAndSupervisorID($projectID, $suprvisorID)
    {
        try {
            $this->tableGateway->delete(array('projectId' => $projectID, 'employeeId' => $suprvisorID));
            return true;
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
            return false;
        }
    }

//this function use to get projectSupervisor details that related to given employee ID
    public function getProjectSupervisorDetailsByEmployeeID($ID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('projectSupervisors')
                ->columns(array('*'))
                ->where(array('projectSupervisors.employeeId' => $ID));
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if (!$rowset) {
            return NULL;
        }
        return $rowset;
    }

}
