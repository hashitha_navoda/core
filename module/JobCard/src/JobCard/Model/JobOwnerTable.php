<?php

namespace JobCard\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Predicate\PredicateSet;
use Zend\Db\Sql\Predicate\Operator;

class JobOwnerTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function saveOwner($jobReference, $jobOwner, $jobOwnerLength)
    {
        for ($i = 0; $i < $jobOwnerLength; $i++) {
            $temp = $jobOwner[$i];
            $jobOwnerData = array(
                'jobOwnerID' => $temp,
                'referenceCode' => $jobReference,
            );
            if ($this->tableGateway->insert($jobOwnerData)) {
                $result = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
            } else {
                return FALSE;
            }
        }
        return $result;
    }

    public function updateOwner($jobReference, $jobOwner = null, $jobOwnerLength = null)
    {
        $result = $this->tableGateway->delete(array('referenceCode' => $jobReference));
        if ($jobOwner && $jobOwnerLength) {
            for ($i = 0; $i < $jobOwnerLength; $i++) {
                $temp = $jobOwner[$i];
                $jobOwnerData = array(
                    'jobOwnerID' => $temp,
                    'referenceCode' => $jobReference,
                );
                if ($this->tableGateway->insert($jobOwnerData)) {
                    $result = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
                } else {
                    return FALSE;
                }
            }
        }
        return true;
    }

    public function getJobOwnerName($ID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('jobOwner')
                ->columns(array('*'))
                ->join('employee', 'jobOwner.jobOwnerID=  employee.employeeID', array("employeeFirstName", "employeeSecondName"), "left")
                ->where(array('jobOwner.referenceCode' => $ID));
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if (!$rowset) {
            return NULL;
        }
        return $rowset;
    }

//this function use to get job Owners details that related to the employee ID
    public function getJobOwnerDetailsByEmployeeID($ID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('jobOwner')
                ->columns(array('*'))
                ->where(array('jobOwner.jobOwnerID' => $ID));
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if (!$rowset) {
            return NULL;
        }
        return $rowset;
    }

    //this function use to get job Owners details that related to the job reference
    public function getJobOwnersDetailsByJobReference($jobReference)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('jobOwner')
                ->columns(array('*'))
                ->join('employee', 'jobOwner.jobOwnerID =  employee.employeeID', array("employeeFirstName", "employeeSecondName"), "left")
                ->where(array('jobOwner.referenceCode' => $jobReference));
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if (!$rowset) {
            return NULL;
        }
        return $rowset;
    }
    
    /**
     * Save job owner
     * @param \JobCard\Model\JobOwner $jobOwner
     * @return mixed
     */
    public function saveJobOwner(JobOwner $jobOwner) 
    {
        $jobOwnerData = array(
            'jobOwnerID'  => $jobOwner->jobOwnerID,
            'referenceCode' => $jobOwner->referenceCode,
        );
        if ($this->tableGateway->insert($jobOwnerData)) {
            $result = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
            return $result;
        } else {
            return FALSE;
        }
    }
    
    /**
     * Delete job owner
     * @param string $jobCode
     * @param int $ownerId
     * @return boolean
     */
    public function deleteJobOwnerByJobCodeAndOwnerId( $jobCode, $ownerId)
    {
        try {
            $this->tableGateway->delete(array('referenceCode' => $jobCode, 'jobOwnerID' => $ownerId));
            return true;
        } catch (Exception $exc) {
            return false;
        }
    }
    
    /**
     * Get job employee by employee id
     * @param int $jobCode
     * @param int $employeeId
     * @return mixed
     */
    public function getJobOwnerByEmployeeId( $jobCode, $employeeId) 
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('jobOwner')
                ->columns(array('*'))
                ->where(array('jobOwner.referenceCode' => $jobCode,'jobOwner.jobOwnerID' => $employeeId));
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if ($rowset->count() == 0) {
            return NULL;
        }
        return $rowset->current();
    }
    
    /**
     * Check wether employee exist in the given job
     * @param int $jobCode
     * @param int $employeeId
     * @return boolean
     */
    public function isJobEmployeeExist( $jobCode, $employeeId) 
    {
        if($this->getJobOwnerByEmployeeId( $jobCode, $employeeId)){
            return TRUE;
        } else {
            return FALSE;
        }
    }

}
