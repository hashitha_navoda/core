<?php

namespace JobCard\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Predicate\PredicateSet;
use Zend\Db\Sql\Predicate\Operator;

class ActivityTypeTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll($paginated = FALSE)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('activityType');
        $select->join('entity', 'activityType.entityID=entity.entityID', array("deleted"), "left");
        $select->where(array('entity.deleted' => 0));
        $select->order('activityTypeName DESC');
        if ($paginated) {
            $paginatorAdapter = new DbSelect(
                    $select, $this->tableGateway->getAdapter());
            $paginator = new Paginator($paginatorAdapter);
            return $paginator;
        } else {
            $statement = $sql->prepareStatementForSqlObject($select);
            $result = $statement->execute();
            return $result;
        }
    }

    public function getActivityTypes($activityTypeName)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('activityType')
                ->columns(array('*'))
                ->join('entity', 'activityType.entityID=entity.entityID', array("deleted"), "left")
                ->where(array('activityType.activityTypeName' => $activityTypeName, 'entity.deleted' => 0));
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if (!$rowset) {
            return NULL;
        }
        return $rowset;
    }

//get all Activty types that related to given activityType Name
    public function getActivityTypesByNameAndID($activityTypeName, $ID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('activityType')
                ->columns(array('*'))
                ->join('entity', 'activityType.entityID=entity.entityID', array("deleted"), "left")
                ->where(array('activityType.activityTypeName' => $activityTypeName, 'entity.deleted' => 0));
        $select->where->notEqualTo('activityType.activityTypeID', $ID);
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if (!$rowset) {
            return NULL;
        }
        return $rowset;
    }

    //this function use to get activity type data that related to given activity type id
    public function getActivityTypeDataByActivityTypeID($activityTypeID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('activityType')
                ->columns(array('*'))
                ->where(array('activityType.activityTypeID' => $activityTypeID));
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if (!$rowset) {
            return NULL;
        }
        return $rowset;
    }

    public function saveActivityTypes($activityTypeData)
    {
        $ActivityTypeData = array(
            'activityTypeName' => $activityTypeData->activityTypeName,
            'activityTypeMaxValue' => $activityTypeData->activityTypeMaxValue,
            'activityTypeMinValue' => $activityTypeData->activityTypeMinValue,
            'entityID' => $activityTypeData->entityID,
            'activityTypeStatus' => '1',
        );
        if ($this->tableGateway->insert($ActivityTypeData)) {
            $result = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
            return $result;
        } else {
            return FALSE;
        }
    }

    public function updateActivityType($activityTypeData)
    {
        $ActivityTypeData = array(
            'activityTypeName' => $activityTypeData->activityTypeName,
            'activityTypeMaxValue' => $activityTypeData->activityTypeMaxValue,
            'activityTypeMinValue' => $activityTypeData->activityTypeMinValue,
        );
        try {
            $this->tableGateway->update($ActivityTypeData, array('activityTypeID' => $activityTypeData->activityTypeID));
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
        return true;
    }

    public function deleteActivityTypeByActivityTypeID($activityTypeData)
    {
        try {
            $result = $this->tableGateway->delete(array('activityTypeID' => $activityTypeData->activityTypeID));
            return $result;
        } catch (\Exception $e) {
            $error = 0;
            return $error;
        }
    }

    public function ActivityTypeSearchByKey($keyword)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('activityType')
                ->columns(array('*'))
                ->join('entity', 'activityType.entityID=entity.entityID', array("deleted"), "left");
        $select->where(new PredicateSet(array(new Operator('entity.deleted', '=', '0'))));
        $select->where(new PredicateSet(array(new Operator('activityType.activityTypeName', 'like', '%' . $keyword . '%'))));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return (object) $results;
    }

    public function searchActivityTypesForDropDown($searchKey)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('activityType');
        $select->join('entity', 'activityType.entityID=  entity.entityID', array("*"), "left");
        $select->where(array("entity.deleted" => 0, 'activityType.activityTypeStatus' => 1));
        $select->where->like('activityTypeName', '%' . $searchKey . '%');
        $select->limit(50);

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

    public function updateActivityTypeState($activityTypeID, $status)
    {
        $activityTypeData = array(
            'activityTypeStatus' => $status,
        );
        try {
            $this->tableGateway->update($activityTypeData, array('activityTypeID' => $activityTypeID));
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
        return true;
    }

}
