<?php

/**
 * @author Sandun <sandun@thinkcube.com>
 * This file contains Inquiry Status Table Functions
 */

namespace JobCard\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterInterface;

class InquiryStatusType
{

    public $inquiryStatusTypeID;
    public $inquiryStatusTypeName;
    protected $inputFilter;

    public function exchangeArray($data)
    {
        $this->inquiryStatusTypeID = (!empty($data['inquiryStatusTypeID'])) ? $data['inquiryStatusTypeID'] : null;
        $this->inquiryStatusTypeName = (!empty($data['inquiryStatusTypeName'])) ? $data['inquiryStatusTypeName'] : null;
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory = new InputFactory();

            $inputFilter->add($factory->createInput(array(
                        'name' => 'inquiryStatusTypeID',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'Int'),
                        ),
            )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'inquiryStatusTypeName',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'StringLength',
                                'options' => array(
                                    'encoding' => 'UTF-8',
                                    'min' => 1,
                                    'max' => 255,
                                ),
                            ),
                        ),
            )));

            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

}
