<?php

namespace JobCard\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class Job
{

    public $jobId;
    public $jobName;
    public $customerID;
    public $jobTypeId;
    public $jobDescription;
    public $jobCustomerJobDetails;
    public $jobStartingTime;
    public $jobEndTime;
    public $jobEstimatedTime;
    public $jobEstimatedCost;
    public $projectId;
    public $jobWeight;
    public $jobStation;
    public $jobReferenceNumber;
    public $customerProfileID;
    public $jobProgress;
    public $jobStatus;
    public $locationID;
    public $entityID;
    public $jobAddress;
    public $jobRepeatEnabled;
    public $jobRepeatComment;

    public function exchangeArray($data)
    {
        $this->jobId = (!empty($data['jobId'])) ? $data['jobId'] : null;
        $this->jobName = (!empty($data['jobName'])) ? $data['jobName'] : null;
        $this->customerID = (!empty($data['customerID'])) ? $data['customerID'] : null;
        $this->jobTypeId = (!empty($data['jobTypeId'])) ? $data['jobTypeId'] : null;
        $this->jobDescription = (!empty($data['jobDescription'])) ? $data['jobDescription'] : null;
        $this->jobCustomerJobDetails = (!empty($data['jobCustomerJobDetails'])) ? $data['jobCustomerJobDetails'] : null;
        $this->jobStartingTime = (!empty($data['jobStartingTime'])) ? $data['jobStartingTime'] : null;
        $this->jobEndTime = (!empty($data['jobEndTime'])) ? $data['jobEndTime'] : null;
        $this->jobEstimatedTime = (!empty($data['jobEstimatedTime'])) ? $data['jobEstimatedTime'] : null;
        $this->jobEstimatedCost = (!empty($data['jobEstimatedCost'])) ? $data['jobEstimatedCost'] : null;
        $this->projectId = (!empty($data['projectId'])) ? $data['projectId'] : null;
        $this->jobWeight = (!empty($data['jobWeight'])) ? $data['jobWeight'] : null;
        $this->jobStation = (!empty($data['jobStation'])) ? $data['jobStation'] : null;
        $this->jobReferenceNumber = (!empty($data['jobReferenceNumber'])) ? $data['jobReferenceNumber'] : null;
        $this->customerProfileID = (!empty($data['customerProfileID'])) ? $data['customerProfileID'] : null;
        $this->jobProgress = (!empty($data['jobProgress'])) ? $data['jobProgress'] : null;
        $this->jobStatus = (!empty($data['jobStatus'])) ? $data['jobStatus'] : null;
        $this->locationID = (!empty($data['locationID'])) ? $data['locationID'] : null;
        $this->entityID = (!empty($data['entityID'])) ? $data['entityID'] : null;
        $this->jobAddress = (!empty($data['jobAddress'])) ? $data['jobAddress'] : null;
        $this->jobRepeatEnabled = (!empty($data['jobRepeatEnabled'])) ? $data['jobRepeatEnabled'] : 0;
        $this->jobRepeatComment = (!empty($data['jobRepeatComment'])) ? $data['jobRepeatComment'] : null;
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory = new InputFactory();

            $inputFilter->add($factory->createInput(array(
                        'name' => 'jobId',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'Int'),
                        ),
            )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'jobName',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'StringLength',
                                'options' => array(
                                    'encoding' => 'UTF-8',
                                    'min' => 1,
                                    'max' => 255,
                                ),
                            ),
                        ),
            )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'customerID',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'Int'),
                        ),
            )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'jobTypeId',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'Int'),
                        ),
            )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'jobDescription',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'StringLength',
                                'options' => array(
                                    'encoding' => 'UTF-8',
                                    'min' => 1,
                                    'max' => 800,
                                ),
                            ),
                        ),
            )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'jobCustomerJobDetails',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'StringLength',
                                'options' => array(
                                    'encoding' => 'UTF-8',
                                    'min' => 1,
                                    'max' => 1000,
                                ),
                            ),
                        ),
            )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'projectId',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'Int'),
                        ),
            )));

            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

}
