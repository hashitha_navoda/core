<?php

/**
 * @author Ashan Madushka <ashan@thinkcube.com>
 * This file contains Project Divisions Functions
 */

namespace JobCard\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class ProjectDivisions
{

    public $projectDivisionId;
    public $projectId;
    public $divisionId;
    protected $inputFilter;

    public function exchangeArray($data)
    {
        $this->projectDivisionId = (!empty($data['projectDivisionId'])) ? $data['projectDivisionId'] : null;
        $this->projectId = (!empty($data['projectId'])) ? $data['projectId'] : null;
        $this->divisionId = (!empty($data['divisionId'])) ? $data['divisionId'] : null;
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory = new InputFactory();

            $inputFilter->add($factory->createInput(array(
                        'name' => 'projectDivisionId',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'Int'),
                        ),
            )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'projectId',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'Int'),
                        ),
            )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'divisionId',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'Int'),
                        ),
            )));


            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

}
