<?php

namespace JobCard\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Predicate\PredicateSet;
use Zend\Db\Sql\Predicate\Operator;
use Zend\Db\Sql\Where;

class ActivityTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fechAll($locationID, $paginated = FALSE)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('activity');
        $select->join('job', 'job.jobId = activity.jobId', array('jobName', 'jobReferenceNumber'));
        $select->join('activityType', 'activityType.activityTypeID = activity.activityTypeID', array('activityTypeName'));
        $select->join('customer', 'job.customerID = customer.customerID', array("customerStatus"), "left");
        $select->join('project', 'project.projectId = activity.projectId', array('projectCode'), 'left');
        $select->join('entity', 'activity.entityID = entity.entityID', array('deleted'), 'left');
        $select->order('activity.activityId DESC');
        if (isset($locationID)) {
            $select->where(array('activity.locationID' => $locationID));
        }
        $select->where(array('entity.deleted' => '0'));
        $select->order('activityCode DESC');
        if ($paginated) {
            $paginatorAdapter = new DbSelect(
                    $select, $this->tableGateway->getAdapter());
            $paginator = new Paginator($paginatorAdapter);
            return $paginator;
        } else {
            $statement = $sql->prepareStatementForSqlObject($select);
            $result = $statement->execute();
            return $result;
        }
    }

    public function activitySearchByKey($activitySearchKey, $locationID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('activity');
        $select->columns(array('*'));
        $select->join('job', 'job.jobId = activity.jobId', array('jobName', 'jobReferenceNumber'));
        $select->join('activityType', 'activityType.activityTypeID = activity.activityTypeID', array('activityTypeName'));
        $select->join('entity', 'activity.entityID = entity.entityID', array('deleted'), 'left');
        $select->where(array('entity.deleted' => '0'));
        $select->where(new PredicateSet(array(new Operator('activity.activityCode', 'like', '%' . $activitySearchKey . '%'), new Operator('activity.activityName', 'like', '%' . $activitySearchKey . '%')), PredicateSet::OP_OR));
        $select->where(new PredicateSet(array(new Operator('activity.locationID', '=', $locationID))));
        $select->order('activityCode DESC');
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        return (object) $results;
    }

//get activities by job id
    public function getActivityListByJobID($jobID, $locationID = null)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('activity')
                ->columns(array('*'))
                ->join('job', 'job.jobId = activity.jobId', array('jobName', 'projectId', 'jobReferenceNumber'))
                ->join('status', "activity.activityStatus= status.statusID", array('statusName'), 'left')
                ->join('activityType', 'activity.activityTypeID = activityType.activityTypeID', '*', "left")
                ->join('customer', 'job.customerID=  customer.customerID', array("customerStatus"), "left")
                ->join('entity', 'entity.entityID = activity.entityID', array('deleted', 'createdTimeStamp'), "left")
                ->where(array('deleted' => 0));
        $select->where(array('activity.jobId' => $jobID));
        if ($locationID) {
            $select->where(array('activity.locationID' => $locationID));
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return (object) $results;
    }

//get activity by activity id
    public function getActivityByActivityID($activityID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('activity')
                ->columns(array('*'))
                ->join('activityType', 'activity.activityTypeID=activityType.activityTypeID', '*', "left")
                ->join('entity', 'entity.entityID = activity.entityID', 'deleted', "left")
                ->where(array('deleted' => 0));
        $select->where(array('activity.activityId' => $activityID));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return (object) $results;
    }

//this function use to get Activity details that reated to given activity Type ID
    public function getActivityByActivityTypeID($activityTypeID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('activity')
                ->columns(array('*'))
                ->join('entity', 'activity.entityID=entity.entityID', array("deleted"), "left");
        $select->where(array('activity.activityTypeID' => $activityTypeID, 'entity.deleted' => 0));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return (object) $results;
    }

//update activity progress
    public function updateActivityProgress(Activity $activity)
    {
        $data = array('activityProgress' => $activity->activityProgress);
        try {
            $this->tableGateway->update($data, array('activityId' => $activity->activityId));
            return true;
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
            return false;
        }
    }

    public function getActivityDetailsByActivityCode($activityCode)
    {
        try {
            $sql = new Sql($this->tableGateway->getAdapter());
            $select = $sql->select();
            $select->from('activity')
                    ->columns(array('*'))
//                    ->join(array('e' => 'entity'), "activity.entityID = e.entityID")
//                    ->where(array('e.deleted' => '0'))
                    ->where(array('activity.activityCode' => $activityCode));
            $query = $sql->prepareStatementForSqlObject($select);
            $rowset = $query->execute();

            if (!$rowset) {
                return null;
            }
            return $rowset;
        } catch (\Exception $exc) {
            echo $exc;
        }
    }

    public function saveActivity(Activity $activity)
    {
        $activityData = array(
            'locationID' => $activity->locationID,
            'activityCode' => $activity->activityCode,
            'activityName' => $activity->activityName,
            'activityTypeID' => $activity->activityTypeID,
            'activityDescription' => $activity->activityDescription,
            'activityStartingTime' => $activity->activityStartingTime,
            'activityEndTime' => $activity->activityEndTime,
            'activityEstimateTime' => $activity->activityEstimateTime,
            'activityEstimatedCost' => $activity->activityEstimatedCost,
            'projectId' => $activity->projectId,
            'jobId' => $activity->jobId,
            'activityWeight' => $activity->activityWeight,
            'activityProgress' => $activity->activityProgress,
            'activitySortOrder' => $activity->activitySortOrder,
            'activityStatus' => $activity->activityStatus,
            'entityID' => $activity->entityID,
            'activityRepeatEnabled' => $activity->activityRepeatEnabled,
            'activityRepeatComment' => $activity->activityRepeatComment,
        );
        if ($this->tableGateway->insert($activityData)) {
            $result = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
            return $result;
        } else {
            return FALSE;
        }
    }

    /**
     * @author ashan <ashan@thinkcube.com>
     * @param type $locationID
     * @param type $activitySearchKey
     * @return type
     */
    public function searchActivitiesForDropDown($locationID, $activitySearchKey, $jobID = null, $flag = null)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('activity');
        $select->columns(array('activityCode', 'activityId', 'activityProgress', 'activityName'));
        $select->where(array('activity.locationID' => $locationID));
        $select->where->like('activityCode', '%' . $activitySearchKey . '%');
        if ($flag) {
            $select->where(array('activity.activityStatus' => '3'));
        }
        if ($jobID) {
            $select->where(array('activity.jobId' => $jobID));
        }
        $select->limit(50);

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

//get activity raw materials  by activity id getActivityRawMaterialsByActivityID
    public function getActivityRawMaterialsByActivityID($activityID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('activity')
                ->columns(array('*'))
                ->join('activityRawMaterial', 'activity.activityId = activityRawMaterial.activityId', array('activityRawMaterialId', 'locationProductId', 'activityRawMaterialQuantity', 'activityRawMaterialCost'), "left")
                ->join('locationProduct', 'activityRawMaterial.locationProductId = locationProduct.locationProductID', array('locationDiscountValue', 'locationDiscountPercentage', 'locationProductQuantity'), 'left')
                ->join('product', 'locationProduct.productID = product.productID', '*', "left")
                ->join('activityRawMaterialSubProduct', 'activityRawMaterial.activityRawMaterialId = activityRawMaterialSubProduct.activityRawMaterialId', array('activityRawMaterialSubProductID', 'productBatchID', 'productSerialID', 'activityRawMaterialSubProductQuantity'), 'left')
                ->join('productBatch', 'activityRawMaterialSubProduct.productBatchID = productBatch.productBatchID', array('productBatchCode', 'locationProductID', 'productBatchQuantity'), 'left')
                ->join('productSerial', 'activityRawMaterialSubProduct.productSerialID = productSerial.productSerialID', array('productSerialCode', 'locationProductID', 'locationProductID'), 'left')
                ->join('activityType', 'activity.activityTypeID = activityType.activityTypeID', '*', "left");
        $select->where(array('activity.activityId' => $activityID));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return (object) $results;
    }

//get activity cost types by activity id
    public function getActivityCostTypesByActivityID($activityID, $locationID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('activity')
                ->columns(array('*'))
//                ->join('job', 'activity.jobId = job.jobId', array("customerID", "customerProfileID"), "left")
//                ->join('customer', 'job.customerID = customer.customerID', array("customerName", "customerShortName", "customerCurrentBalance", "customerCurrentCredit"), "left")
                ->join('activityCostType', 'activity.activityId = activityCostType.activityId', '*', "left")
                ->join('costType', 'activityCostType.costTypeId = costType.costTypeID', '*', "left")
                ->join('product', 'costType.productID = product.productID', '*', "left")
                ->join('locationProduct', 'product.productID = locationProduct.productID', array('*'))
                ->join('activityType', 'activity.activityTypeID = activityType.activityTypeID', '*', "left");
        $select->where(array('activity.activityId' => $activityID, 'locationProduct.locationID' => $locationID));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return (object) $results;
    }

    /**
     * @author Sandun <sandun@thinkcube.com>
     * get activity related data
     * @param int $activityID
     * @return array $results
     */
    public function getActivityDetailsByActivityID($activityID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('activity')
                ->columns(array('*'))
                ->join('job', 'job.jobId = activity.jobId', array('jobName', 'jobReferenceNumber'), 'left')
                ->join('project', 'project.projectId = activity.projectId', array('projectId' => new Expression('activity.projectId'), 'projectCode'), 'left')
                ->join('activityType', 'activity.activityTypeID=activityType.activityTypeID', 'activityTypeName', "left")
                ->join('customer', 'job.customerID=  customer.customerID', array("customerName", "customerShortName"), "left")
                ->where(array('activity.activityId' => $activityID));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return (object) $results;
    }

    public function getActivityContractorDetailsByActivityID($activityID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('activity')
                ->columns(array('*'))
                ->join('activityContractor', 'activity.activityId=activityContractor.activityId', array('*'), "left")
                ->join('contractor', 'contractor.contractorID=activityContractor.contractorId', array('*'), "left")
                ->where(array('activity.activityId' => $activityID));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return (object) $results;
    }

    public function getActivityTempContractorDetailsByActivityID($activityID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('activity')
                ->columns(array('*'))
                ->join('activityTemporaryContractor', 'activity.activityId=activityTemporaryContractor.activityID', array('*'), "left")
                ->join('temporaryContractor', 'temporaryContractor.temporaryContractorID=activityTemporaryContractor.contractorId', array('*'), "left")
                ->where(array('activity.activityId' => $activityID));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return (object) $results;
    }

    public function getActivityOwnerDetailsByActivityID($activityID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('activity')
                ->columns(array('*'))
                ->join('activityOwner', 'activity.activityId=activityOwner.activityID', array('*'), "left")
                ->join('employee', 'employee.employeeID=activityOwner.employeeID', array('*'), "left")
                ->where(array('activity.activityId' => $activityID));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return (object) $results;
    }

    public function getActivitySupervisorDetailsByActivityID($activityID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('activity')
                ->columns(array('*'))
                ->join('activitySupervisor', 'activity.activityId=activitySupervisor.activityID', array('*'), "left")
                ->join('employee', 'employee.employeeID=activitySupervisor.employeeID', array('*'), "left")
                ->where(array('activity.activityId' => $activityID));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return (object) $results;
    }

    public function getActivityCostTypeDetailsByActivityID($activityID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('activity')
                ->columns(array('*'))
                ->join('activityCostType', 'activityCostType.activityId=activity.activityId', array('*'), "left")
                ->join('costType', 'costType.costTypeID=activityCostType.costTypeId', array('*'), "left")
                ->where(array('activity.activityId' => $activityID));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return (object) $results;
    }

    public function getActivityRawMDetailsByActivityID($activityID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('activity')
                ->columns(array('*'))
                ->join('activityRawMaterial', 'activityRawMaterial.activityId=activity.activityId', array('*'), "left")
                ->join('locationProduct', 'activityRawMaterial.locationProductId=locationProduct.locationProductID', array('rMLPID' => new Expression('locationProduct.locationProductId')), "left")
                ->join('product', 'product.productID=locationProduct.productID', array(
                    'rMProductID' => new Expression('product.productID'),
                    'rMProductCode' => new Expression('product.productCode'),
                    'rMProductName' => new Expression('product.productName'),
                        ), "left")
                ->join('activityRawMaterialSubProduct', 'activityRawMaterialSubProduct.activityRawMaterialId=activityRawMaterial.activityRawMaterialId', array('*'), "left")
                ->join('productBatch', 'activityRawMaterialSubProduct.productBatchID=productBatch.productBatchID', array(
                    'rMBatchCode' => new Expression('productBatch.productBatchCode'),
                    'rMBatchID' => new Expression('productBatch.productBatchID')), "left")
                ->join('productSerial', 'activityRawMaterialSubProduct.productSerialID=productSerial.productSerialID', array(
                    'rMSerialCode' => new Expression('productSerial.productSerialCode'),
                    'rMSerialID' => new Expression('productSerial.productSerialID')), "left")
                ->join('productUom', 'productUom.productID = product.productID', array('uomID'), 'left')
                ->join('uom', 'uom.uomID = productUom.uomID', array('uomAbbr'), 'left')
                ->where(array('activity.activityId' => $activityID, 'productUom.productUomBase' => 1));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return (object) $results;
    }

    public function getActivityFixedAssetDetailsByActivityID($activityID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('activity')
                ->columns(array('*'))
                ->join('activityFixedAsset', 'activityFixedAsset.activityId=activity.activityId', array('*'), "left")
                ->join('locationProduct', 'activityFixedAsset.locationProductId=locationProduct.locationProductID', array('fALPID' => new Expression('locationProduct.locationProductId')), "left")
                ->join('product', 'product.productID=locationProduct.productID', array(
                    'fAProductID' => new Expression('product.productID'),
                    'fAProductCode' => new Expression('product.productCode'),
                    'fAProductName' => new Expression('product.productName'),
                        ), "left")
                ->join('activityFixedAssetSubProduct', 'activityFixedAssetSubProduct.activityFixedAssetID=activityFixedAsset.activityFixedAssetID', array('*'), "left")
                ->join(array('pB' => 'productBatch'), 'activityFixedAssetSubProduct.productBatchID=pB.productBatchID', array(
                    'fABatchCode' => new Expression('pB.productBatchCode'),
                    'fABatchID' => new Expression('pB.productBatchID')), "left")
                ->join(array('pS' => 'productSerial'), 'activityFixedAssetSubProduct.productSerialID=pS.productSerialID', array(
                    'fASerialCode' => new Expression('pS.productSerialCode'),
                    'fASerialID' => new Expression('pS.productSerialID')), "left")
                ->join('productUom', 'productUom.productID = product.productID', array('uomID'), 'left')
                ->join('uom', 'uom.uomID = productUom.uomID', array('uomAbbr'), 'left')
                ->where(array('activity.activityId' => $activityID, 'productUom.productUomBase' => 1));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return (object) $results;
    }

    public function getActivityTempProdcutDetailsByActivityID($activityID = NULL)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('activity')
                ->columns(array('*'))
                ->join('job', 'job.jobId = activity.jobId', array('jobName', 'jobReferenceNumber'))
                ->join('entity', 'entity.entityID = activity.entityID', array('deleted', 'createdTimeStamp'))
                ->join('customer', 'job.jobId = customer.customerID', array('customerTitle', 'customerName'))
                ->join('activityTemporaryProduct', 'activityTemporaryProduct.activityId = activity.activityId', array(
                    'tempProID' => new Expression('activityTemporaryProduct.temporaryProductID')))
                ->join('temporaryProduct', 'activityTemporaryProduct.temporaryProductID = temporaryProduct.temporaryProductID', array(
                    'temporaryProductName',
                    'temporaryProductCode',
                    'temporaryProductQuantity'), "left")
                ->join('uom', 'uom.uomID = temporaryProduct.uomID', array('uomAbbr'), 'left')
                ->where(array('deleted' => 0));
        if ($activityID != NULL) {
            $select->where->in('activity.activityId', $activityID);
        }

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        return (object) $results;
    }

//update activity status
    public function updateActivityStatus(Activity $activity)
    {
        $data = array('activityStatus' => $activity->activityStatus);
        try {
            $this->tableGateway->update($data, array('activityId' => $activity->activityId));
            return true;
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
            return false;
        }
    }

//this function use to get all Activity reference codes.
    public function getActivityByProjectID($projectId = null)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('activity')
                ->columns(array('*'))
                ->join('entity', 'entity.entityID = activity.entityID', array('deleted'), 'left')
                ->where(array('deleted' => 0));
        if ($projectId) {
            $select->where(array('projectId' => $projectId));
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return (object) $results;
    }

//update activity
    public function updateActivity($activityData, $activityID)
    {

        try {
            $this->tableGateway->update($activityData, array('activityId' => $activityID));
            return true;
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
            return false;
        }
    }

    public function getActivityByID($activityID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('activity')
                ->columns(array('*'))
                ->where(array('activityId' => $activityID));
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if (!$rowset) {
            return NULL;
        }
        return $rowset;
    }

    public function updateActivityWeight($activityData)
    {
        $activityWeightUpdateData = array(
            'activityWeight' => $activityData['activityWeight'],
        );
        try {
            $this->tableGateway->update($activityWeightUpdateData, array('activityId' => $activityData['activityId'], 'jobId' => $activityData['jobId']));
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
        return true;
    }

    public function searchActivitiesForTypeahead($locationID, $activitySearchKey)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('activity');
        $select->columns(array('activityName', 'activityId'));
        $select->where(array('activity.locationID' => $locationID));
        $select->where->like('activityName', '%' . $activitySearchKey . '%');
        $select->limit(50);

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

    /**
     * Get Activity list for dropdown
     * @author sharmilan <sharmilan<sharmilan@thinkcube.com>
     * @param type $locationID
     * @param type $activitySearchKey
     * @return type
     */
    public function searchAllActivitiesForDropdown($locationID, $activitySearchKey)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('activity');
        $select->columns(array('activityName', 'activityId', 'activityCode'));
        $select->where(array('activity.locationID' => $locationID));
        $select->where(new PredicateSet([new Operator('activity.activityCode', 'like', '%' . $activitySearchKey . '%'),
            new Operator('activity.activityName', 'like', '%' . $activitySearchKey . '%')], PredicateSet::OP_OR));
        $select->limit(50);

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

    /**
     * Get Activity list by search
     * @author sharmilan <sharmilan@thinkcube.com>
     * @param type $locationID
     * @param type $activityID
     * @return type
     */
    public function getActivityListView($locationID, $activityID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('activity');
        $select->columns(array('*'));
        $select->join('job', 'job.jobId = activity.jobId', array('jobName', 'jobReferenceNumber'));
        $select->join('activityType', 'activityType.activityTypeID = activity.activityTypeID', array('activityTypeName'));
        $select->join('customer', 'job.customerID = customer.customerID', array("customerStatus"), "left");
        $select->join('entity', 'activity.entityID = entity.entityID', array('deleted'), 'left');
        $select->where([
            'activity.locationID' => $locationID,
            'activity.activityId' => $activityID
        ]);
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        return (object) $results;
    }

    /**
     * Get Activity List By date range
     * @author sharmilan <sharmilan@thincube.com>
     * @param type $locationID
     * @param type $fromDate
     * @param type $toDate
     * @return type
     */
    public function getActivityListViewByDateRange($locationID, $fromDate, $toDate)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('activity');
        $select->columns(array('*'));
        $select->join('job', 'job.jobId = activity.jobId', array('jobName', 'jobReferenceNumber'));
        $select->join('activityType', 'activityType.activityTypeID = activity.activityTypeID', array('activityTypeName'));
        $select->join('entity', 'activity.entityID = entity.entityID', ['deleted', 'createdTimeStamp'], 'left');
        $select->join('customer', 'job.customerID = customer.customerID', array("customerStatus"), "left");
        $select->where([
            'activity.locationID' => $locationID,
        ]);
        $select->where->greaterThanOrEqualTo('entity.createdTimeStamp', $fromDate)->AND->lessThan('entity.createdTimeStamp', $toDate);
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        return (object) $results;
    }

    public function getRepeatActivityList($locations, $fromDate = false, $toDate = false)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('activity');
        $select->join('entity', 'activity.entityID = entity.entityID', ['createdTimeStamp', 'deleted'], 'left');
        $select->join('job', 'job.jobId = activity.jobId', ['jobName', 'jobReferenceNumber']);
        $select->join('project', 'job.projectId = project.projectId', ['projectCode', 'projectId'], 'left');
        $select->where([ 'activity.activityRepeatEnabled' => '1', 'entity.deleted' => '0']);
        $select->where->in('activity.locationID', $locations);
        if ($fromDate && $toDate) {
            $select->where->between('createdTimeStamp', $fromDate, $toDate);
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        return $statement->execute();
    }

    /**
     *
     * @param array $data
     * @return $results
     */
    public function getActivityCostingDetails($data)
    {

        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('activity')
                ->columns(array('activityID' => new Expression('activity.activityId'),
                    'activityName', 'activityCode',
                    'hours' => new Expression(" activityEstimateTime/ 60")))
                ->join('activityRawMaterial', 'activity.activityId = activityRawMaterial.activityId', array('activityRawMaterialId', 'locationProductId', 'activityRawMaterialQuantity', 'activityRawMaterialCost'), "left")
                ->join('locationProduct', 'activityRawMaterial.locationProductId = locationProduct.locationProductID', array('locationDiscountValue', 'locationDiscountPercentage', 'locationProductQuantity'), 'left')
                ->join('product', 'locationProduct.productID = product.productID', array('*',
                    'proID' => new Expression('product.productID')), "left")
                ->join('activityCostType', 'activityCostType.activityId=activity.activityId', array('*'), "left")
                ->join('costType', 'costType.costTypeID=activityCostType.costTypeId', array('*'), "left")
                ->join('activityVehicle', 'activityVehicle.activityId=activity.activityId', array('*'), "left")
                ->join('vehicle', 'vehicle.vehicleId=activityVehicle.vehicleId', array('*'), "left")
                ->join('activityOwner', 'activityOwner.activityId=activity.activityId', array('*'), "left")
                ->join('employee', 'employee.employeeID=activityOwner.employeeID', ['*'], "left");
        if ($data['typeId'] == 'activityNo' && $data['activityId'] != NULL) {
            $select->where->in('activity.activityId', $data['activityId']);
        }
        if ($data['typeId'] == 'jobNo') {
            $select->join('job', 'activity.jobId = job.jobId', array(
                'jobReferenceNumber', 'jobID' => new Expression('job.jobId'),
                'jobName'));
        }
        if ($data['jobId'] != NULL) {
            $select->where->in('activity.jobId', $data['jobId']);
        }
        if ($data['typeId'] == 'projectNo') {
            $select->join('project', 'activity.projectId = project.projectId', array(
                'projectCode', 'projectID' => new Expression('project.projectId'),
                'projectName'));
        }
        if ($data['projectId'] != NULL) {
            $select->where->in('activity.projectId', $data['projectId']);
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        return (object) $results;
    }

    public function getActivityByCode($activityCode)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('activity');
        $select->columns(array('*'));
        $select->join('location', 'location.locationID = activity.locationID', array('locationName', 'locationCode'));
        $select->where(array('activityCode ' => $activityCode));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        if (!$results) {
            return null;
        }

        return $results;
    }

    public function getActivityDataById($activityId)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('activity');
        $select->columns(array('*'));
        $select->join('location', 'location.locationID = activity.locationID', array('locationName', 'locationCode'));
        $select->where(array('activityID ' => $activityId));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        if (!$results) {
            return null;
        }

        return $results;
    }

    public function updateMultipleActivities($activityData, $activityIdArray)
    {
        $where = new Where();
        $where->in('activityId', $activityIdArray);

        try {
            $this->tableGateway->update($activityData, $where);
            return true;
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
            return false;
        }
    }

}
