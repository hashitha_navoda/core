<?php

namespace JobCard\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;

/**
 * @author Sharmilan <sharmilan@thinkcube.com>
 * Email send by schedule to Inquiry Log Assigned Employee Table operations
 */
class InquiryLogEmailScheduleTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function save(InquiryLogEmailSchedule $inquiryLogEmailSchedule)
    {
        $inquiryLogEmailScheduleData = array(
            'inquiryLogID' => $inquiryLogEmailSchedule->inquiryLogID,
            'inquiryLogEmailScheduleDateTime' => $inquiryLogEmailSchedule->inquiryLogEmailScheduleDateTime,
            'inquirySetupID' => $inquiryLogEmailSchedule->inquirySetupID,
            'createdAt' => $inquiryLogEmailSchedule->createdAt,
            'isEmailSend' => $inquiryLogEmailSchedule->isEmailSend,
        );
        if ($this->tableGateway->insert($inquiryLogEmailScheduleData)) {
            $result = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
            return true;
        } else {
            return FALSE;
        }
    }

    public function updateEmailSendStatus($inquiryLogID = false, $isEmailSend = 0)
    {
        if ($inquiryLogID) {
            $inquiryLogEmailScheduleData = array(
                'isEmailSend' => $isEmailSend,
            );
            try {
                $this->tableGateway->update($inquiryLogEmailScheduleData, array('inquiryLogID' => $inquiryLogID));
            } catch (Exception $exc) {
                echo $exc->getTraceAsString();
            }
            return true;
        } else {
            return false;
        }
    }

    public function updateEmailSendStatusByInquirySetupID($inquirySetupID = false, $isEmailSend = 0)
    {
        if ($inquirySetupID) {
            $inquiryLogEmailScheduleData = array(
                'isEmailSend' => $isEmailSend,
            );
            try {
                $this->tableGateway->update($inquiryLogEmailScheduleData, array('inquirySetupID' => $inquirySetupID));
            } catch (Exception $exc) {
                echo $exc->getTraceAsString();
            }
            return true;
        } else {
            return false;
        }
    }

    public function getInquiryLogIDsByScheduleDateTime($currentGmtDatetime)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('inquiryLogEmailSchedule');
        $select->join('inquirySetup', 'inquiryLogEmailSchedule.inquirySetupID = inquirySetup.inquirySetupID', array('inquirySetupName', 'inquirySetupStatus'), 'LEFT');
        $select->where->equalTo('isEmailSend', '0');
        $select->where->equalTo('inquirySetupStatus', '1');
        $select->where->lessThanOrEqualTo('inquiryLogEmailScheduleDateTime', $currentGmtDatetime);
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }

}
