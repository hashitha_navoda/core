<?php

/**
 * @author Ashan Madushka <ashan@thinkcube.com>
 * This file contains Universal search related controller functions
 */

namespace JobCard\Controller;

use Zend\View\Model\ViewModel;
use Core\Controller\CoreController;
use JobCard\Form\ProjectForm;
use Zend\Session\Container;

class UniversalSearchController extends CoreController
{

    protected $sideMenus = 'job_card_side_menu';
    protected $upperMenus = 'universal_search_upper_menu';

    //serch action of Universal search
    public function searchAction()
    {
        $this->getSideAndUpperMenus('Universal Search', 'Search', 'JobCard');

        $view = new ViewModel(array(
        ));
        $this->getViewHelper('HeadScript')->prependFile('/js/job-card/universal-search.js');

        return $view;
    }

    /**
     * @author Ashan <ashan@thinkcube.com>
     * serch projects jobs and activities for dropdown
     * @return type
     */
    public function searchProjectsJobsAndActivitiesBySearchKeyAction()
    {
        $searchrequest = $this->getRequest();
        if ($searchrequest->isPost()) {
            $searchKey = $searchrequest->getPost('searchKey');
            $locationID = $this->user_session->userActiveLocation["locationID"];
            $projects = $this->CommonTable('JobCard/Model/ProjectTable')->getProjectListBySearchKey($searchKey);
            $list = array();
            foreach ($projects as $project) {
                $list['pro_' . $project['projectId']] = $project;
            }

            $jobs = $this->CommonTable('JobCard/Model/JobTable')->jobSearchByKey($searchKey, $locationID);
            foreach ($jobs as $job) {
                $list['job_' . $job['jobId']] = $job;
            }

            $activities = $this->CommonTable('JobCard\Model\ActivityTable')->activitySearchByKey($searchKey, $locationID);
            foreach ($activities as $activity) {
                $list['act_' . $activity['activityId']] = $activity;
            }

            $inquiryLogs = $this->CommonTable('JobCard\Model\InquiryLogTable')->inquiryLogSearchByKey($searchKey, $locationID);
            foreach ($inquiryLogs as $inquiryLog) {
                $list['inq_' . $inquiryLog['inquiryLogID']] = $inquiryLog;
            }
            $this->status = true;
            $this->data = array('list' => $list);
            return $this->JSONRespond();
        }
    }

    /**
     * @author Ashan <ashan@thinkcube.com>
     * view space tree
     *
     */
    public function viewSpaceTreeAction()
    {
        $this->getSideAndUpperMenus('Universal Search', 'Search', 'JobCard');

        $prefix = $this->params()->fromRoute('param1');
        $array = split("_", $prefix);
        $type = $array[0];
        $id = $array[1];
        $locationID = $this->user_session->userActiveLocation["locationID"];
        $projectArray = array();
        $jobsArray = array();
        $activitiesArray = array();
        $projectID = '';
        $jobID = '';
        $inquiryCode = '';
        $customerName = '';

        if ($type == 'pro') {
            $arrayvalue = $this->getProject($id, $locationID);
            $projectArray = $arrayvalue[0];
            $jobsArray = $arrayvalue[1];
            $activitiesArray = $arrayvalue[2];
            $projectID = $arrayvalue[3];
            $customerName = $arrayvalue[4];
        } elseif ($type == 'job') {
            $arrayvalue = $this->getJobs($id, $locationID);
            $projectArray = $arrayvalue[0];
            $jobsArray = $arrayvalue[1];
            $activitiesArray = $arrayvalue[2];
            $projectID = $arrayvalue[3];
            $jobID = $arrayvalue[4];
            $customerName = $arrayvalue[5];
        } elseif ($type == 'act') {
            $arrayvalue = $this->getActivities($id, $locationID);
            $projectArray = $arrayvalue[0];
            $jobsArray = $arrayvalue[1];
            $activitiesArray = $arrayvalue[2];
            $projectID = $arrayvalue[3];
            $jobID = $arrayvalue[4];
            $customerName = $arrayvalue[5];
        } elseif ($type == 'inq') {
            $inquiryLogs = (object) $this->CommonTable('JobCard\Model\InquiryLogTable')->getInquiryLogDetailsByInquiryLogID($id)->current();
            $inquiryCode = $inquiryLogs->inquiryLogReference;
            $pjaID = $inquiryLogs->inquiryComplainRelationDocumentID;
            if ($inquiryLogs->documentTypeID == 21) {
                $arrayvalue = $this->getProject($pjaID, $locationID);
                $projectArray = $arrayvalue[0];
                $jobsArray = $arrayvalue[1];
                $activitiesArray = $arrayvalue[2];
                $projectID = $arrayvalue[3];
                $customerName = $arrayvalue[4];
            } else if ($inquiryLogs->documentTypeID == 22) {
                $arrayvalue = $this->getJobs($pjaID, $locationID);
                $projectArray = $arrayvalue[0];
                $jobsArray = $arrayvalue[1];
                $activitiesArray = $arrayvalue[2];
                $projectID = $arrayvalue[3];
                $jobID = $arrayvalue[4];
                $customerName = $arrayvalue[5];
            } else if ($inquiryLogs->documentTypeID == 23) {
                $arrayvalue = $this->getActivities($pjaID, $locationID);
                $projectArray = $arrayvalue[0];
                $jobsArray = $arrayvalue[1];
                $activitiesArray = $arrayvalue[2];
                $projectID = $arrayvalue[3];
                $jobID = $arrayvalue[4];
                $customerName = $arrayvalue[5];
            }
        }



        $view = new ViewModel(array(
            'project' => $projectArray,
            'jobs' => $jobsArray,
            'activities' => $activitiesArray,
            'projectId' => $projectID,
            'jobId' => $jobID,
            'customerName' => $customerName,
            'inquieryLog' => $inquiryCode,
        ));

        $this->getViewHelper('HeadScript')->prependFile('/assets/space-tree/excanvas.js');
        $this->getViewHelper('HeadScript')->prependFile('/assets/space-tree/jit.js');
        $this->getViewHelper('HeadScript')->prependFile('/assets/space-tree/jquery.spacetree.js');
        $this->getViewHelper('HeadLink')->prependStylesheet('/assets/space-tree/jquery.spacetree.css');
        return $view;
    }

    public function getProject($id, $locationID)
    {
        $projectArray = array();
        $jobsArray = array();
        $activitiesArray = array();
        $projectID = $id;
        $projects = (object) $this->CommonTable('JobCard/Model/ProjectTable')->getProjectDetailsByProjectID($id)->current();
        $projectArray[$id] = $projects->projectCode . ' - ' . $projects->projectName;
        $customerName = $projects->customerName . ' - ' . $projects->customerShortName;
        $jobs = $this->CommonTable('JobCard/Model/JobTable')->getJobListByProjectID($id, $locationID);
        foreach ($jobs as $jobsValues) {
            $jobsValues = (object) $jobsValues;
            $jobsArray[$jobsValues->jobId] = $jobsValues->jobReferenceNumber . ' - ' . $jobsValues->jobName;
            $activities = $this->CommonTable('JobCard\Model\ActivityTable')->getActivityListByJobID($jobsValues->jobId, $locationID);
            foreach ($activities as $activityValues) {
                $activityValues = (object) $activityValues;
                $activitiesArray[$jobsValues->jobId][$activityValues->activityId] = $activityValues->activityCode . ' - ' . $activityValues->activityName;
            }
        }

        return array($projectArray, $jobsArray, $activitiesArray, $projectID, $customerName);
    }

    public function getJobs($id, $locationID)
    {
        $projectArray = array();
        $jobsArray = array();
        $activitiesArray = array();
        $job = (object) $this->CommonTable('JobCard/Model/JobTable')->getJobDetails($id)->current();
        $customerName = $job->customerName . ' - ' . $job->customerShortName;
        $jobsArray[$id] = $job->jobReferenceNumber . ' - ' . $job->jobName;
        if ($job->projectId) {
            $projectID = $job->projectId;
            $projects = (object) $this->CommonTable('JobCard/Model/ProjectTable')->getProjectDetailsByProjectID($job->projectId)->current();
            $projectArray[$job->projectId] = $projects->projectCode . ' - ' . $projects->projectName;

            $jobs = $this->CommonTable('JobCard/Model/JobTable')->getJobListByProjectID($job->projectId, $locationID);
            foreach ($jobs as $jobsValues) {
                $jobsValues = (object) $jobsValues;
                $jobsArray[$jobsValues->jobId] = $jobsValues->jobReferenceNumber . ' - ' . $jobsValues->jobName;
                $activities = $this->CommonTable('JobCard\Model\ActivityTable')->getActivityListByJobID($jobsValues->jobId, $locationID);
                foreach ($activities as $activityValues) {
                    $activityValues = (object) $activityValues;
                    $activitiesArray[$jobsValues->jobId][$activityValues->activityId] = $activityValues->activityCode . ' - ' . $activityValues->activityName;
                }
            }
        } else {
            $jobID = $id;
            $activities = $this->CommonTable('JobCard\Model\ActivityTable')->getActivityListByJobID($jobID, $locationID);
            foreach ($activities as $activityValues) {
                $activityValues = (object) $activityValues;
                $activitiesArray[$id][$activityValues->activityId] = $activityValues->activityCode . ' - ' . $activityValues->activityName;
            }
        }
        return array($projectArray, $jobsArray, $activitiesArray, $projectID, $jobID, $customerName);
    }

    public function getActivities($id, $locationID)
    {
        $projectArray = array();
        $jobsArray = array();
        $activitiesArray = array();

        $activitie = (object) $this->CommonTable('JobCard\Model\ActivityTable')->getActivityByActivityID($id)->current();

        $job = (object) $this->CommonTable('JobCard/Model/JobTable')->getJobDetails($activitie->jobId)->current();
        $customerName = $job->customerName . ' - ' . $job->customerShortName;
        $jobsArray[$activitie->jobId] = $job->jobReferenceNumber . ' - ' . $job->jobName;
        if ($job->projectId) {
            $projectID = $job->projectId;
            $projects = (object) $this->CommonTable('JobCard/Model/ProjectTable')->getProjectDetailsByProjectID($job->projectId)->current();
            $projectArray[$job->projectId] = $projects->projectCode . ' - ' . $projects->projectName;

            $jobs = $this->CommonTable('JobCard/Model/JobTable')->getJobListByProjectID($job->projectId, $locationID);
            foreach ($jobs as $jobsValues) {
                $jobsValues = (object) $jobsValues;
                $jobsArray[$jobsValues->jobId] = $jobsValues->jobReferenceNumber . ' - ' . $jobsValues->jobName;
                $activities = $this->CommonTable('JobCard\Model\ActivityTable')->getActivityListByJobID($jobsValues->jobId, $locationID);
                foreach ($activities as $activityValues) {
                    $activityValues = (object) $activityValues;
                    $activitiesArray[$jobsValues->jobId][$activityValues->activityId] = $activityValues->activityCode . ' - ' . $activityValues->activityName;
                }
            }
        } else {
            $jobID = $activitie->jobId;
            $activities = $this->CommonTable('JobCard\Model\ActivityTable')->getActivityListByJobID($job->jobId, $locationID);
            foreach ($activities as $activityValues) {
                $activityValues = (object) $activityValues;
                $activitiesArray[$activitie->jobId][$activityValues->activityId] = $activityValues->activityCode . ' - ' . $activityValues->activityName;
            }
        }
        return array($projectArray, $jobsArray, $activitiesArray, $projectID, $jobID, $customerName);
    }

}
