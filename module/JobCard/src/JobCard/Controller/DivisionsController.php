<?php

namespace JobCard\Controller;

use Core\Controller\CoreController;
use Zend\Session\Container;
use Zend\View\Model\ViewModel;
use JobCard\Form\DivisionForm;

class DivisionsController extends CoreController
{

    protected $sideMenus = 'job_card_side_menu';
    protected $upperMenus = 'job_card_employee_set_up_upper_menu';
    protected $downMenus = 'job_card_set_up_down_menu';

    public function createAction()
    {
        $this->getSideAndUpperMenus('Job Card Setup', 'Divisions', 'JobCard', 'Employee Setup');
        $division = $this->getPaginatedDivisions();
        $addDivisionForm = new DivisionForm();
        $view = new ViewModel(array(
            'addDivisionForm' => $addDivisionForm,
            'division' => $division,
            'paginated' => true
        ));
        $this->getViewHelper('HeadScript')->prependFile('/js/job-card/division.js');
        return $view;
    }

    public function getPaginatedDivisions()
    {
        $this->division = $this->CommonTable('JobCard/Model/DivisionTable')->fetchAll(true);
        $this->division->setCurrentPageNumber((int) $this->params()->fromRoute('param1', 1));
        $this->division->setItemCountPerPage(5);
        return $this->division;
    }

}
