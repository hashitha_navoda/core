<?php
/**
 * @author Madawa Chandrarathne <madawa@thinkcube.com>
 * This file contains job dispatch related controller functions
 */
namespace JobCard\Controller;

use Zend\View\Model\ViewModel;
use Core\Controller\CoreController;

class JobDispatchController extends CoreController
{
    protected $sideMenus = 'job_card_side_menu';

    public function indexAction()
    {
        $this->getSideAndUpperMenus('Job Dispatch', null, null);
        //get location
        $locationId = $this->user_session->userActiveLocation['locationID'];
        //get project list
        $projects = $this->CommonTable('JobCard\Model\ProjectTable')->getActiveProjectList( $locationId);
        //get job list
        $jobs = $this->CommonTable('JobCard\Model\JobTable')->getActiveJobtList( $locationId);
        //get employees
        $employees = $this->CommonTable('JobCard/Model/EmployeeTable')->fetchAll(false);
                
        return new ViewModel( array('projects'=>$projects, 'jobs'=>$jobs, 'employees' => $employees));
    }

}
