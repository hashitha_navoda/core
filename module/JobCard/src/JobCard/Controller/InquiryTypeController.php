<?php

/**
 * @author Sandun Dissanayake <sandun@thinkcube.com>
 * This file contains Inquiry Type related controller functions
 */

namespace JobCard\Controller;

use Zend\View\Model\ViewModel;
use Core\Controller\CoreController;
use JobCard\Form\InquiryTypeForm;

class InquiryTypeController extends CoreController
{

    protected $sideMenus = 'crm_side_menu';
    protected $upperMenus = 'inquiry_setting_upper_menu';
    protected $downMenus = 'crm_settings_up_down_menu';

    /**
     * @author Sandun <sandun@thinkcube.com>
     * inquiry type creates view
     * @return \Zend\View\Model\ViewModel
     */
    public function createAction()
    {
        $this->getSideAndUpperMenus('Settings', 'Inquiry Type', 'CRM', 'CRM Inquiry Settings');
        $inquiryTypeList = $this->getPaginatedInquiryTypes();
        $inquiryForm = new InquiryTypeForm();

        $inquiryView = new ViewModel(array(
            'inquiryForm' => $inquiryForm,
            'inquiryTypeList' => $inquiryTypeList,
            'paginated' => true
        ));
        $this->getViewHelper('HeadScript')->prependFile('/js/job-card/inquiryType.js');

        return $inquiryView;
    }

    /**
     * @author Sandun <sandun@thinkcube.com>
     * pageinator list of inquiry types
     * @param int $perPage
     * @return paginator
     */
    protected function getPaginatedInquiryTypes($perPage = 6)
    {
        $this->paginator = $this->CommonTable('JobCard\Model\InquiryTypeTable')->fetchAll(true);
        $this->paginator->setCurrentPageNumber((int) $this->params()->fromRoute('param1', 1));
        $this->paginator->setItemCountPerPage($perPage);
        $this->setLogMessage('Inquiry Type list accessed');

        return $this->paginator;
    }

}
