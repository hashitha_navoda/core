<?php

namespace JobCard\Controller;

use Core\Controller\CoreController;
use Zend\Session\Container;
use Zend\View\Model\ViewModel;
use JobCard\Form\ActivityTypeForm;

class ActivityTypeController extends CoreController
{

    protected $sideMenus = 'job_card_side_menu';
    protected $upperMenus = 'job_card_category_set_up_upper_menu';
    protected $downMenus = 'job_card_set_up_down_menu';

    public function createAction()
    {

        $this->getSideAndUpperMenus('Job Card Setup', 'Activity Types', 'JobCard', 'Category Setup');
        $actiyityTypes = $this->getPaginatedActivityTypes();
        $addActivityTypeForm = new ActivityTypeForm();
        $view = new ViewModel(array(
            'addActivityTypeForm' => $addActivityTypeForm,
            'activityTypes' => $actiyityTypes,
            'paginated' => true
        ));
        $this->getViewHelper('HeadScript')->prependFile('/js/job-card/activityType.js');
        return $view;
    }

    public function getPaginatedActivityTypes($perPage = 6)
    {
        $this->activityType = $this->CommonTable('JobCard/Model/ActivityTypeTable')->fetchAll(true);
        $this->activityType->setCurrentPageNumber((int) $this->params()->fromRoute('param1', 1));
        $this->activityType->setItemCountPerPage($perPage);
        return $this->activityType;
    }

}
