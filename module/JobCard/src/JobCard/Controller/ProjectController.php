<?php

/**
 * @author Ashan Madushka <ashan@thinkcube.com>
 * This file contains Project related controller functions
 */

namespace JobCard\Controller;

use Zend\View\Model\ViewModel;
use Core\Controller\CoreController;
use JobCard\Form\ProjectForm;
use Zend\Session\Container;

class ProjectController extends CoreController
{

    protected $sideMenus = 'job_card_side_menu';
    protected $upperMenus = 'job_card_project_upper_menu';

    //create action of project
    public function createAction()
    {
        $this->getSideAndUpperMenus('Projects', 'Create Project', 'JobCard');

        $locationID = $this->user_session->userActiveLocation['locationID'];
        $reference = $this->getReferenceNoForLocation(21, $locationID);
        $projectReferenceNumber = $reference['refNo'];
        $projectReferenceID = $reference['locRefID'];
        $customerID = $this->params('param1');
        $cusProfID = $this->params('param2');
        $inqID = $this->params('param3');
        $userdateFormat = $this->getUserDateFormat();
        $form = new ProjectForm($userdateFormat);

        if ($projectReferenceNumber) {
            $form->get('projectCode')->setValue($projectReferenceNumber)->setAttribute('disabled', true);
        }
        if ($cusProfID && $customerID) {
            $cusData = $this->CommonTable('Invoice/Model/CustomerTable')->getCustomerByID($customerID);
            $cusProfile = (object) $this->CommonTable('Invoice/Model/CustomerProfileTable')->getProfileByCustomerID($customerID);
            $cusProfileList = array();
            foreach ($cusProfile as $cP) {
                $cusProfileList[$cP[customerProfileID]] = $cP[customerProfileName];
            }
            $form->get('projectCustomer')->setValue($cusData->customerName);
            $form->get('projectCustomer')->setAttribute('data-cusID', $customerID);
            if ($inqID) {
                $form->get('projectCode')->setAttribute('data-inquiryid', $inqID);
            }
        }
        $view = new ViewModel(array(
            'form' => $form,
            'editMode' => false,
            'customerProfiles' => $cusProfileList,
            'customerProfileID' => $cusProfID,
            'projectCustomerID' => $customerID,
        ));

        if ($projectReferenceNumber == '' || $projectReferenceNumber == NULL) {
            if ($projectReferenceID == null) {
                $title = 'Project Reference Number not set';
                $msg = $this->getMessage('ERR_PROJECR_ADD_REF');
            } else {
                $title = 'Project Reference Number has reache the maximum limit ';
                $msg = $this->getMessage('ERR_PROJECR_CHANGE_REF');
            }
            $refNotSet = new ViewModel(array(
                'title' => $title,
                'msg' => $msg,
                'controller' => 'company',
                'action' => 'reference',
            ));
            $refNotSet->setTemplate('/core/modal/entity-missing-pre-requisites-notification');
            $view->addChild($refNotSet, 'refNotSet');
        }

        $this->getViewHelper('HeadScript')->prependFile('/js/job-card/project.js');
        $this->getViewHelper('HeadScript')->prependFile('/js/time-duration.js');
        $this->getServiceLocator()->get('viewhelpermanager')->get('jsvarsHelper')->setVars([
            'getProjectTypes', 'allCustomerNames', 'getEmployees', 'getDivisions'
        ]);
        return $view;
    }

    //list action of project
    public function listAction()
    {
        $this->getSideAndUpperMenus('Projects', 'Project List', 'JobCard');
        $this->getPaginatedProjects();

        $projectList = new ViewModel(array(
            'project_list' => $this->paginator,
            'status' => $this->getStatusesList(),
            'userDateTimeFormat' => $this->convertUserDateFormatToPhpFormat() . ' H:i',
        ));

        $projectList->setTemplate('job-card/project/project-list');
        $projectListView = new ViewModel(['userDateFormat' => $this->getUserDateFormat(),]);
        $projectListView->addChild($projectList, 'projectList');

        $this->getViewHelper('HeadScript')->prependFile('/js/job-card/project-list.js');
        return $projectListView;
    }

    public function getPaginatedProjects()
    {
        $locationID = $this->user_session->userActiveLocation['locationID'];
        $this->paginator = $this->CommonTable('JobCard/Model/ProjectTable')->fetchAll(true, $locationID);
        $this->paginator->setCurrentPageNumber((int) $this->params()->fromRoute('param1', 1));
        $this->paginator->setItemCountPerPage(8);
    }

    public function editAction()
    {
        $projectID = $this->params('param1');
        $locationID = $this->user_session->userActiveLocation['locationID'];

        $projectEdit = $this->createAction();
        $projectEdit->setTemplate('job-card/project/create');
        $projectEdit->editMode = true;
        $this->getSideAndUpperMenus('Projects', 'Project List', 'JobCard');

        $jobList = $this->CommonTable('JobCard/Model/JobTable')->getJobListByProjectID((int) $projectID, $locationID);

        $projectData = (object) $this->CommonTable('JobCard/Model/ProjectTable')->getProjectDetailsByProjectID($projectID)->current();

        $userDateFormat = $this->convertUserDateFormatToPhpFormat() . ' H:i';
        if ($projectData->projectStartingAt != '0000-00-00 00:00:00') {
//            $startingTime = date($userDateFormat, strtotime($projectData->projectStartingAt));
            $startingTime = $this->getUserDateTime($projectData->projectStartingAt, $userDateFormat);
            $projectEdit->form->get('projectStartingDate')->setValue($startingTime);
        }
        if ($projectData->projectEndAt != '0000-00-00 00:00:00') {
//            $endTime = date($userDateFormat, strtotime($projectData->projectEndAt));
            $endTime = $this->getUserDateTime($projectData->projectEndAt, $userDateFormat);
            $projectEdit->form->get('projectEndingDate')->setValue($endTime);
        }
        $projectEdit->form->get('projectCode')->setValue($projectData->projectCode);
        $projectEdit->form->get('projectName')->setValue($projectData->projectName);
        $projectEdit->form->get('projectDescription')->setValue($projectData->projectDescription);
        $projectEdit->form->get('projectExtimatedTimeDuration')->setValue($projectData->projectEstimatedTimeDuration);
        $projectEdit->form->get('projectExtimatedCost')->setValue(number_format($projectData->projectEstimatedCost,2));
        $projectEdit->customerProfileID = $projectData->customerProfileID;
        $projectEdit->customerProfileHidden = false;
        if ($jobList->count() > 0) {
            $projectEdit->customerProfileHidden = true;
        }
        $projectEdit->projectID = $projectID;
        $projectEdit->projectTypeID = $projectData->projectTypeID;
        $projectEdit->projectCustomerID = $projectData->customerID;

        $projectOwnersData = $this->CommonTable('JobCard/Model/ProjectOwnersTable')->getProjectOwnersByProjectID($projectID);
        $projectOwners = array();
        foreach ($projectOwnersData as $value) {
            $value = (object) $value;
            $projectOwners[$value->employeeID] = $value->employeeFirstName . ' - ' . $value->employeeSecondName;
        }
        $projectEdit->projectOwners = $projectOwners;

        $projectSupervisorsData = $this->CommonTable('JobCard\Model\ProjectSupervisorsTable')->getProjectSupervisorsByProjectID($projectID);
        $projectSupervisors = array();
        foreach ($projectSupervisorsData as $value) {
            $value = (object) $value;
            $projectSupervisors[$value->employeeId] = $value->employeeFirstName . ' - ' . $value->employeeSecondName;
        }
        $projectEdit->projectSupervisors = $projectSupervisors;

        $customerProfileData = $this->CommonTable('Invoice\Model\CustomerProfileTable')->getProfileByCustomerID($projectData->customerID);
        $customerProfiles = array();
        foreach ($customerProfileData as $value) {
            $value = (object) $value;
            $customerProfiles[$value->customerProfileID] = $value->customerProfileName;
        }
        $projectEdit->customerProfiles = $customerProfiles;

        $projectDivisionsData = $this->CommonTable('JobCard\Model\ProjectDivisionsTable')->getProjectDivisionsByProjectID($projectID);
        $projectDivision = array();
        foreach ($projectDivisionsData as $value) {
            $value = (object) $value;
            $projectDivision[$value->divisionID] = $value->divisionName;
        }
        $projectEdit->projectDivisions = $projectDivision;

        $index = new ViewModel();
        $index->addChild($projectEdit, 'projectDetails');

        return $index;
    }

    public function viewAction()
    {
        $projectID = $this->params('param1');

        $this->getSideAndUpperMenus('Projects', 'Project List', 'JobCard');

        $projectData = (object) $this->CommonTable('JobCard/Model/ProjectTable')->getProjectDetailsByProjectID($projectID)->current();

        $projectOwnersData = $this->CommonTable('JobCard/Model/ProjectOwnersTable')->getProjectOwnersByProjectID($projectID);
        $projectOwners = array();
        foreach ($projectOwnersData as $value) {
            $value = (object) $value;
            $projectOwners[$value->employeeId] = $value->employeeFirstName . ' - ' . $value->employeeSecondName;
        }
        $projectEdit->projectOwners = $projectOwners;

        $projectSupervisorsData = $this->CommonTable('JobCard\Model\ProjectSupervisorsTable')->getProjectSupervisorsByProjectID($projectID);
        $projectSupervisors = array();
        foreach ($projectSupervisorsData as $value) {
            $value = (object) $value;
            $projectSupervisors[$value->employeeId] = $value->employeeFirstName . ' - ' . $value->employeeSecondName;
        }
        $projectEdit->projectSupervisors = $projectSupervisors;

        $customerProfileData = $this->CommonTable('Invoice\Model\CustomerProfileTable')->getProfileByCustomerID($projectData->customerID);
        $customerProfiles = array();
        foreach ($customerProfileData as $value) {
            $value = (object) $value;
            if ($projectData->customerProfileID == $value->customerProfileID) {
                $customerProfiles[$value->customerProfileID] = $value->customerProfileName;
                $customerStatus = $value->customerStatus;
            }
        }
        $projectEdit->customerProfiles = $customerProfiles;

        $projectDivisionsData = $this->CommonTable('JobCard\Model\ProjectDivisionsTable')->getProjectDivisionsByProjectID($projectID);
        $projectDivision = array();
        foreach ($projectDivisionsData as $value) {
            $value = (object) $value;
            $projectDivision[$value->divisionID] = $value->divisionName;
        }
        $projectEdit->projectDivisions = $projectDivision;

        $userDateFormat = $this->convertUserDateFormatToPhpFormat() . ' H:i';

        //check date exist or not
        $startDate = null;
        if($projectData->projectStartingAt != 0){
            $startDate = $this->getUserDateTime($projectData->projectStartingAt, $userDateFormat);
        }
        $endDate = null;
        if($projectData->projectEndAt != 0){
            $endDate = $this->getUserDateTime($projectData->projectEndAt, $userDateFormat);
        }


        $index = new ViewModel(
                array(
            'projectCode' => $projectData->projectCode,
            'projectName' => $projectData->projectName,
            'projectType' => $projectData->projectTypeCode . '-' . $projectData->projectTypeName,
            'projectDescription' => $projectData->projectDescription,
            'timeDuration' => $projectData->projectEstimatedTimeDuration,
            'startTime' => $startDate,
            'endTime' => $endDate,
            'projectExtimatedCost' => $projectData->projectEstimatedCost,
            'customerName' => $projectData->customerName . '-' . $projectData->customerShortName,
            'projectOwners' => $projectOwners,
            'projectSupervisors' => $projectSupervisors,
            'customerProfile' => $customerProfiles,
            'projectDivisions' => $projectDivision,
            'projectID' => $projectID,
            'customerStatus' => $customerStatus
                )
        );
        $this->getViewHelper('HeadScript')->prependFile('/js/job-card/project-list.js');

        return $index;
    }

}
