<?php

/**
 * @author Sharmilan <sharmilan@thinkcube.com>
 * This file contails for which are the emails to be send to employees
 */

namespace JobCard\Controller;

use Zend\View\Model\ViewModel;
use Core\Controller\CoreController;

class InquirySetupController extends CoreController
{

    protected $sideMenus = 'job_card_side_menu';
    protected $upperMenus = 'job_card_general_set_up_upper_menu';
    protected $downMenus = 'job_card_set_up_down_menu';

    public function indexAction()
    {
        $this->getSideAndUpperMenus('Job Card Setup', 'Inquiry Setup', 'JobCard', 'General Setup');
        $inquirySetupDetails = $this->CommonTable('JobCard\Model\InquirySetupTable')->fetchAll();
        $inquirySetupArray = array();
        foreach ($inquirySetupDetails as $inquirySetupDetails) {
            $inquirySetupArray[] = $inquirySetupDetails;
        }
        $this->getViewHelper('HeadScript')->prependFile('/js/job-card/inquiry-setup.js');
        return new ViewModel(array('inquirySetupDetails' => $inquirySetupArray));
    }

    /**
     * @author Sharmilan <sharmilan@thinkcube.com>
     * Update all Inquiry setup status
     * @return type
     */
    public function updateAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            foreach ($request->getPost() as $inquirySetupID => $inquirySetupStatus) {
                if ($inquirySetupStatus == '0') {
//                  set related emails as send in inquity Log Email Schedule
                    $this->CommonTable('JobCard\Model\InquiryLogEmailScheduleTable')->updateEmailSendStatusByInquirySetupID($inquirySetupID, '1');
                }
                $this->CommonTable('JobCard\Model\InquirySetupTable')->updateEmailSendStatus($inquirySetupID, $inquirySetupStatus);
            }
            $this->msg = 'Sucessfullly Inquiry Setup Updated';
            $this->status = true;
            return $this->JSONRespond();
        }
        $this->msg = 'couldn\'t Update the Inquiry Setup';
        $this->status = false;
        return $this->JSONRespond();
    }

}
