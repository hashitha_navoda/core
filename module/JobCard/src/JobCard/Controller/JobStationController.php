<?php

namespace JobCard\Controller;

use Core\Controller\CoreController;
use Zend\Session\Container;
use Zend\View\Model\ViewModel;
use JobCard\Form\JobStationForm;

class JobStationController extends CoreController
{

    protected $sideMenus = 'job_card_side_menu';
    protected $upperMenus = 'job_card_general_set_up_upper_menu';
    protected $downMenus = 'job_card_set_up_down_menu';

    public function createAction()
    {
        $this->getSideAndUpperMenus('Job Card Setup', 'Job Stations', 'JobCard', 'General Setup');
        $locationID = $this->user_session->userActiveLocation['locationID'];
        $jobStation = $this->getPaginatedJobStation($locationID);
        $addJobStationForm = new JobStationForm();
        $view = new ViewModel(array(
            'addJobStationForm' => $addJobStationForm,
            'jobStation' => $jobStation,
            'paginated' => true
        ));
        $this->getViewHelper('HeadScript')->prependFile('/js/job-card/job-station.js');
        return $view;
    }

    public function getPaginatedJobStation($locationID)
    {
        $this->jobStation = $this->CommonTable('JobCard/Model/JobStationTable')->fetchAll($locationID, true);
        $this->jobStation->setCurrentPageNumber((int) $this->params()->fromRoute('param1', 1));
        $this->jobStation->setItemCountPerPage(6);
        return $this->jobStation;
    }

}
