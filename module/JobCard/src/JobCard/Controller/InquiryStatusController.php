<?php

/**
 * @author Sandun Dissanayake <sandun@thinkcube.com>
 * This file contains Inquiry Type related controller functions
 */

namespace JobCard\Controller;

use Zend\View\Model\ViewModel;
use Core\Controller\CoreController;
use JobCard\Form\InquiryStatusForm;

class InquiryStatusController extends CoreController
{

    protected $sideMenus = 'crm_side_menu';
    protected $upperMenus = 'inquiry_setting_upper_menu';
    protected $downMenus = 'crm_settings_up_down_menu';

    /**
     * @author Sandun <sandun@thinkcube.com>
     * inquiry status creates view
     * @return \Zend\View\Model\ViewModel
     */
    public function createAction()
    {
        $this->getSideAndUpperMenus('Settings', 'Inquiry Status', 'CRM', 'CRM Inquiry Settings');
        $inquiryStatusList = $this->getPaginatedInquiryStatus();
        $resInquiryStatusType = $this->CommonTable('JobCard\Model\InquiryStatusTypeTable')->fetchAll();
        $inquiryStatusTypeList = Array();
        foreach ($resInquiryStatusType as $r) {
            $inquiryStatusTypeList[$r['inquiryStatusTypeID']] = $r['inquiryStatusTypeName'];
        }
        $inquiryStatusForm = new InquiryStatusForm($inquiryStatusTypeList);

        $inquiryView = new ViewModel(array(
            'inquiryStatusForm' => $inquiryStatusForm,
            'inquiryStatusList' => $inquiryStatusList,
            'paginated' => true
        ));
        $this->getViewHelper('HeadScript')->prependFile('/js/job-card/inquiryStatus.js');

        return $inquiryView;
    }

    /**
     * @author Sandun <sandun@thinkcube.com>
     * pageinator list of inquiry status
     * @param int $perPage
     * @return paginator
     */
    protected function getPaginatedInquiryStatus($perPage = 6)
    {
        $this->paginator = $this->CommonTable('JobCard\Model\InquiryStatusTable')->fetchAll(true);
        $this->paginator->setCurrentPageNumber((int) $this->params()->fromRoute('param1', 1));
        $this->paginator->setItemCountPerPage($perPage);
        $this->setLogMessage('Inquiry Status list accessed');

        return $this->paginator;
    }

}
