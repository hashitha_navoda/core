<?php

/**
 * @author Madawa Chandrarathne <madawa@thinkcube.com>
 * This file vehicle related controller actions
 */

namespace JobCard\Controller;

use Zend\View\Model\ViewModel;
use Core\Controller\CoreController;
use JobCard\Form\VehicleForm;

class VehicleController extends CoreController
{

    protected $sideMenus = 'job_card_side_menu';
    protected $upperMenus = 'job_card_general_set_up_upper_menu';
    protected $downMenus = 'job_card_set_up_down_menu';

    public function indexAction()
    {
        $this->getSideAndUpperMenus('Job Card Setup', 'Vehicle Setup', 'JobCard', 'General Setup');

        $page = $this->params('param1', 1);
        
        $vehicleForm = new VehicleForm();
        
        $paginator = $this->CommonTable('JobCard\Model\VehicleTable')->fetchAll(true,array('vehicle.vehicleId DESC'));        
        
        $paginator->setCurrentPageNumber($page);
        $paginator->setItemCountPerPage(10);
        
        return new ViewModel(array('form'=>$vehicleForm,'vehicles'=>$paginator,'paginated'=>true));
    }
    
}
