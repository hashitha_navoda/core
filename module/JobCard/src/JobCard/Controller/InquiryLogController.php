<?php

namespace JobCard\Controller;

use Core\Controller\CoreController;
use Zend\Session\Container;
use Zend\View\Model\ViewModel;
use JobCard\Form\InquiryLogForm;
use Invoice\Form\AddCustomerForm;
use Core\Model\Notification;

class InquiryLogController extends CoreController
{

    protected $sideMenus = 'crm_side_menu';
    protected $upperMenus = 'inquiry_upper_menu';
    protected $useAccounting;

    public function __construct()
    {
        parent::__construct();
        $this->user_session = new Container('ezBizUser');
        $this->useAccounting = $this->user_session->useAccounting;
    }

    //this function use to load create inquiry log page with some existing data
    public function createAction()
    {
        $this->sideMenus = 'crm_side_menu';
        $this->upperMenus = 'inquiry_upper_menu';
        $this->getSideAndUpperMenus('Inquiry Log', 'Create', 'CRM');
        $inqComFlag = 'Complain';
        return $this->index($inqComFlag);
    }

    public function createInquiryAction()
    {
        $this->sideMenus = 'job_card_side_menu';
        $this->upperMenus = 'inquiry_upper_menu_job_card';
        $this->getSideAndUpperMenus('Inquiry Log', 'Create', 'JobCard');
        $inqComFlag = 'Inquiry';
        return $this->index($inqComFlag);
    }

    private function index($inqComFlag)
    {
        $inqComType = $inqComFlag;
        $locationID = $this->user_session->userActiveLocation['locationID'];
        $reference = $this->getReferenceNoForLocation(24, $locationID);
        $inquiryLogReferenceNumber = $reference['refNo'];
        $inquiryLogReferenceNumberLocationID = $reference['locRefID'];
        $userName = $this->user_session->username;
        $userID = $this->user_session->userID;
        $userDateFormat = $this->convertUserDateFormatToPhpFormat();
        $currentdate = $this->getUserDateTime($this->getGMTDateTime(), $userDateFormat . " H:i");
        $userDateFormat = $this->getUserDateFormat();
        $config = $this->getServiceLocator()->get('config');
        $documentTypeName = $config['documentTypeName'];
        $inquiryLogForm = new InquiryLogForm($userDateFormat, $userName, $inquiryLogReferenceNumber, $currentdate, $userID, $this->isSuperAdmin(), $documentTypeName);
        $this->getServiceLocator()->get('viewhelpermanager')->get('jsvarsHelper')->setVars([
            'getAllProject', 'getAllJobStation',
            'getAllInquiryStatusNames', 'getAllInquiryTypes', 'jobReference',
            'getAllActivityReferenceNumbers', 'getEmployees'
        ]);
        $this->getViewHelper('HeadScript')->prependFile('/js/job-card/inquiry-log.js');
        $this->getViewHelper('HeadScript')->prependFile('/js/invoice/modalAddCustomer.js');
        $paymentTerms = $this->CommonTable('Core\Model\PaymentTermTable')->fetchAll();
        $terms = array();
        while ($row = $paymentTerms->current()) {
            $id = $row->paymentTermID;
            if ($id == NULL) {
                $id = 0;
            }
            $terms[$id] = $row->paymentTermName;
        }
        $cusRefData = $this->getReferenceNoForLocation(20, $locationID);
        $cusRid = $refData["refNo"];
        $locationReferenceID = $cusRefData["locRefID"];
        $customerCode = $this->getReferenceNumber($locationReferenceID);

        $currencyResultSet = $this->CommonTable('Core\Model\CurrencyTable')->fetchAll();
        $currencies = array();
        $currencies['0'] = '';
        foreach ($currencyResultSet as $row) {
            $currencies[$row->currencyID] = $row->currencyName;
        }

        $customerCategories[''] = '';
        $existingCustomerCategories = $this->CommonTable('Invoice\Model\CustomerCategoryTable')->fetchAllForDorpDown();
        $customerCategories = $customerCategories + $existingCustomerCategories;

        //price list
        $customerPriceLists[''] = '';
        $existingCustomerPriceLists = $this->CommonTable('Inventory\Model\PriceListTable')->fetchAllForDorpDown();
        $customerPriceLists = $customerPriceLists + $existingCustomerPriceLists;

        $currencyValue = $this->getDefaultCurrency();
        $customerTitle = $config['personTitle'];
        $countries = ['' => ''] + $config['all_countries'];
        $customerForm = new AddCustomerForm(NULL, $terms, $currencies, $customerTitle, $currencyValue, $customerCategories, NULL, NULL,NULL, $customerPriceLists);
        $customerForm->get('customerFormsubmit')->setValue(_("Add New Customer"));
        $customerForm->get('customerCode')->setAttribute('value', $customerCode);

        $financeAccountsArray = $this->getFinanceAccounts();

        $glAccountExist = $this->CommonTable('Accounting\Model\GlAccountSetupTable')->fetchAll();
        if ($this->useAccounting == 1) {
            $glAccountSetupData = $glAccountExist->current();

            if($glAccountSetupData->glAccountSetupSalesAndCustomerReceivableAccountID != ''){
                $customerReceviableAccountID = $glAccountSetupData->glAccountSetupSalesAndCustomerReceivableAccountID;
                $customerReceviableAccountName = $financeAccountsArray[$customerReceviableAccountID]['financeAccountsCode']."_".$financeAccountsArray[$customerReceviableAccountID]['financeAccountsName'];
                $customerForm->get('customerReceviableAccountID')->setAttribute('data-id',$customerReceviableAccountID);
                $customerForm->get('customerReceviableAccountID')->setAttribute('data-value',$customerReceviableAccountName);
            }

            if($glAccountSetupData->glAccountSetupSalesAndCustomerSalesAccountID != ''){
                $customerSalesAccountID = $glAccountSetupData->glAccountSetupSalesAndCustomerSalesAccountID;
                $customerSalesAccountName = $financeAccountsArray[$customerSalesAccountID]['financeAccountsCode']."_".$financeAccountsArray[$customerSalesAccountID]['financeAccountsName'];
                $customerForm->get('customerSalesAccountID')->setAttribute('data-id',$customerSalesAccountID);
                $customerForm->get('customerSalesAccountID')->setAttribute('data-value',$customerSalesAccountName);
            }

            if($glAccountSetupData->glAccountSetupSalesAndCustomerSalesDiscountAccountID != ''){
                $customerSalesDiscountAccountID = $glAccountSetupData->glAccountSetupSalesAndCustomerSalesDiscountAccountID;
                $customerSalesDiscountAccountName = $financeAccountsArray[$customerSalesDiscountAccountID]['financeAccountsCode']."_".$financeAccountsArray[$customerSalesDiscountAccountID]['financeAccountsName'];
                $customerForm->get('customerSalesDiscountAccountID')->setAttribute('data-id',$customerSalesDiscountAccountID);
                $customerForm->get('customerSalesDiscountAccountID')->setAttribute('data-value',$customerSalesDiscountAccountName);
            }

            if($glAccountSetupData->glAccountSetupSalesAndCustomerAdvancePaymentAccountID != ''){
                $customerAdvancePaymentAccountID = $glAccountSetupData->glAccountSetupSalesAndCustomerAdvancePaymentAccountID;
                $customerAdvancePaymentAccountName = $financeAccountsArray[$customerAdvancePaymentAccountID]['financeAccountsCode']."_".$financeAccountsArray[$customerAdvancePaymentAccountID]['financeAccountsName'];
                $customerForm->get('customerAdvancePaymentAccountID')->setAttribute('data-id',$customerAdvancePaymentAccountID);
                $customerForm->get('customerAdvancePaymentAccountID')->setAttribute('data-value',$customerAdvancePaymentAccountName);
            }
        }

        $customerAddView = new ViewModel(array(
            'customerForm' => $customerForm,
            'countries' => $countries,
            'useAccounting' => $this->useAccounting
        ));
        $customerAddView->setTemplate('invoice/customer/customer-add-modal');
        $view = new ViewModel(array(
            'formData' => $inquiryLogForm,
            'jobFlag' => $jobFlag,
            'inqComType' => $inqComType,
        ));
        $view->setTemplate('job-card/inquiry-log/create');
        $customerHistoryView = new ViewModel();
        $customerHistoryView->setTemplate('job-card/inquiry-log/customer-history');
        $documentView = new ViewModel();
        $documentView->setTemplate('job-card/inquiry-log/document-view');
        if ($inquiryLogReferenceNumberLocationID == '' || $inquiryLogReferenceNumber == null) {
            if ($inquiryLogReferenceNumberLocationID == null) {
                $title = 'Inquiry Log Reference Number not set';
                $msg = $this->getMessage('ERR_JOB_ADD_REF');
            } else {
                $title = 'Inquiry Log Reference Number has reache the maximum limit ';
                $msg = $this->getMessage('ERR_JOB_CHANGE_REF');
            }
            $refNotSet = new ViewModel(array(
                'title' => $title,
                'msg' => $msg,
                'controller' => 'company',
                'action' => 'reference'
            ));
            $refNotSet->setTemplate('/core/modal/entity-missing-pre-requisites-notification');
            $view->addChild($refNotSet, 'refNotSet');
        }
        $view->addChild($customerAddView, 'customerAddView');
        $view->addChild($customerHistoryView, 'customerHistoryView');
        $view->addChild($documentView, 'documentView');

        return $view;
    }

    /**
     * Inqiry log list view from Job card module
     * @return ViewModel
     */
    public function listInquiryAction()
    {
        $this->sideMenus = 'job_card_side_menu';
        $this->upperMenus = 'inquiry_upper_menu_job_card';
        $this->getSideAndUpperMenus('Inquiry Log', 'View', 'JobCard');
        $locationID = $this->user_session->userActiveLocation['locationID'];
        $inquiryLogList = $this->getPaginatedInquiryLog($locationID);
        $paginatedflag = true;
        $jobView = new ViewModel(array(
            'inquiryLogList' => $inquiryLogList,
            'paginated' => $paginatedflag,
            'status' => $this->getStatusesList(),
            'viewAction' => 'inquiry-log-view',
            'editAction' => 'inquiry-log-edit',
            'userDateFormat' => $this->getUserDateFormat(),
        ));
        $customerHistoryView = new ViewModel();
        $customerHistoryView->setTemplate('job-card/inquiry-log/customer-history');
        $jobView->addChild($customerHistoryView, 'customerHistoryView');
        $documentView = new ViewModel();
        $documentView->setTemplate('job-card/inquiry-log/document-view');
        $jobView->addChild($documentView, 'documentView');

        $jobView->setTemplate('job-card/inquiry-log/list');
        $this->getServiceLocator()->get('viewhelpermanager')->get('jsvarsHelper')->setVars([
            'customerListWithoutDefaultCustomer', 'getAllJobTypes', 'getAllProject', 'getAllJobStation',
            'getAllInquiryStatusNames', 'getAllInquiryTypes', 'jobReference', 'getAllActivityReferenceNumbers', 'getEmployees',
            'taxes'
        ]);

        $this->getViewHelper('HeadScript')->prependFile('/js/job-card/inquiry-log.js');
        return $jobView;
    }

//this function use to load all inquiry log records as a list
    public function listAction($jobCardView = false)
    {
        $this->sideMenus = 'crm_side_menu';
        $this->upperMenus = 'inquiry_upper_menu';
        $this->getSideAndUpperMenus('Inquiry Log', 'View', 'CRM');
        $locationID = $this->user_session->userActiveLocation['locationID'];
        $inquiryLogList = $this->getPaginatedInquiryLog($locationID);
        $paginatedflag = true;
        $jobView = new ViewModel(array(
            'inquiryLogList' => $inquiryLogList,
            'paginated' => $paginatedflag,
            'status' => $this->getStatusesList(),
            'viewAction' => 'view',
            'editAction' => 'edit',
            'userDateFormat' => $this->getUserDateFormat(),
        ));

        $customerHistoryView = new ViewModel();
        $customerHistoryView->setTemplate('job-card/inquiry-log/customer-history');
        $jobView->addChild($customerHistoryView, 'customerHistoryView');

        $documentView = new ViewModel();
        $documentView->setTemplate('job-card/inquiry-log/document-view');
        $jobView->addChild($documentView, 'documentView');

        $jobView->setTemplate('job-card/inquiry-log/list');
        $this->getServiceLocator()->get('viewhelpermanager')->get('jsvarsHelper')->setVars([
            'customerListWithoutDefaultCustomer', 'getAllJobTypes', 'getAllProject', 'getAllJobStation',
            'getAllInquiryStatusNames', 'getAllInquiryTypes', 'jobReference', 'getAllActivityReferenceNumbers',
            'getEmployees'
        ]);
        $this->getViewHelper('HeadScript')->prependFile('/js/job-card/inquiry-log.js');
        return $jobView;
    }

    //this function use to load some specific records with full details.
    public function viewAction()
    {
        $inquiryLogID = $this->params()->fromRoute('param1');
        $this->getSideAndUpperMenus('Inquiry Log', 'View', 'CRM');
        return $this->inquiryLogView($inquiryLogID);
    }

    /**
     * Display the selected Inquiry Log details from Job Card modules
     * @return viewModel
     */
    public function inquiryLogViewAction()
    {
        $inquiryLogID = $this->params()->fromRoute('param1');
        $this->sideMenus = 'job_card_side_menu';
        $this->upperMenus = 'inquiry_upper_menu_job_card';
        $this->getSideAndUpperMenus('Inquiry Log', 'View', 'JobCard');
        return $this->inquiryLogView($inquiryLogID);
    }

    public function inquiryLogView($inquiryLogID)
    {
        if ($inquiryLogID) {
            $result = $this->CommonTable('JobCard/Model/InquiryLogTable')->getInquiryLogDetails($inquiryLogID);
            $resultList = (object) $result->current();

            $documentList = $this->CommonTable('JobCard\Model\InquiryComplainRelationTable')->getDocumentForGivenInqID($inquiryLogID);
            foreach ($documentList as $dL) {

                if ($resultList->inquiryComplainType != 'Complain') {
                    $complainType = false;
                    if ($dL['documentTypeID'] == '21') {
                        $projectDetails = $this->CommonTable('JobCard\Model\ProjectTable')->getProjectDetailsByProjectID($dL['inquiryComplainRelationDocumentID'])->current();
                        $projectCode = $projectDetails['projectCode'];
                    }
                    if ($dL['documentTypeID'] == '22') {
                        $jobDetails = $this->CommonTable('JobCard\Model\JobTable')->getJobDetails($dL['inquiryComplainRelationDocumentID'])->current();
                        $jobCode = $jobDetails['jobReferenceNumber'];
                    }
                    if ($dL['documentTypeID'] == '23') {
                        $activityDetails = $this->CommonTable('JobCard\Model\ActivityTable')->getActivityByActivityID($dL['inquiryComplainRelationDocumentID'])->current();
                        $activityCode = $activityDetails['activityCode'];
                    }
                } else {
                    $complainType = true;
                }
            }


//            check current user id assigned to this inquiry log
//            if assigned change notification status as close

            $logInquiryDetails = $this->CommonTable('JobCard/Model/InquiryLogTable')->getInquiryLogDetailsWithEmployeeDetails($resultList->inquiryLogID);
            foreach ($logInquiryDetails as $logInquiry) {
                if ($logInquiry['userID'] && ($logInquiry['userID'] == $this->userID)) {
                    $notification = new Notification();
                    $notification->userID = $this->userID;
                    $notification->locationID = $this->user_session->userActiveLocation['locationID'];
                    $notification->status = '1';
                    $notification->notificationReferID = $resultList->inquiryLogID;
                    $this->CommonTable('Core\Model\NotificationTable')->changeStatusByRefIDAndUserID($notification);
                }
            }


            $inquiryTypeDetails = $this->CommonTable('JobCard/Model/InquiryTypeTable')->getInquiryTypeByInquiryTypeID($resultList->inquiryTypeID);
            $inquiryTypeDetail = (object) $inquiryTypeDetails->current();
            $inquiryStatusDetails = $this->CommonTable('JobCard/Model/InquiryStatusTable')->getInquiryStatusByInquiryStatusID($resultList->inquiryStatusID);
            $inquiryStatusDetail = (object) $inquiryStatusDetails->current();
            $customerProfileDetails = (object) $this->CommonTable('Invoice/Model/CustomerProfileTable')->getProfileByCustomerProfileID($resultList->customerProfileID)->current();
            $logedUser = $this->CommonTable('User/Model/UserTable')->getUser($resultList->inquiryLogCurrentUserID);
            $inquiryLogDutyManagerNames = $this->CommonTable('JobCard/Model/InquiryLogDutyManagerTable')->getInquiryLogDutyManagerNameByInquiryLogID($resultList->inquiryLogID);
            $inc = 0;
            foreach ($inquiryLogDutyManagerNames as $iLMN) {
                $dutyManagerNameList[$inc] = array('fName' => $iLMN[employeeFirstName], 'sName' => $iLMN[employeeSecondName]);
                $inc++;
            }
            $this->getViewHelper('HeadScript')->prependFile('/js/job-card/inquiry-log-view.js');
            $this->getViewHelper('HeadScript')->prependFile('/js/job-card/inquiry-log.js');
            if ($resultList->inquiryComplainType == 'Complain') {

            }
            $jobRelatedSubViewFlag = false;
            if ($this->getRequest()->isPost()) {
                $jobRelatedSubViewFlag = true;
            }

            $view = new viewModel(array(
                'inquiryLogReference' => $resultList->inquiryLogReference,
                'inquiryLogDescription' => $resultList->inquiryLogDescription,
                'customerName' => $resultList->customerName,
                'inquiryLogDateAndTime' => $this->getUserDateTime($resultList->inquiryLogDateAndTime),
                'projectCode' => $projectCode,
                'jobReferenceNumber' => $jobCode,
                'activityCode' => $activityCode,
                'customerProfileName' => $customerProfileDetails->customerProfileName,
                'customerProfileMobileTP1' => $resultList->inquiryLogTP1,
                'customerProfileLandTP1' => $resultList->inquiryLogTP2,
                'address' => $resultList->inquiryLogAddress,
                'username' => $logedUser->username,
                'inquiryStatusName' => $inquiryStatusDetail->inquiryStatusName,
                'inquiryTypeCode' => $inquiryTypeDetail->inquiryTypeCode,
                'dutyManagerList' => $dutyManagerNameList,
                'complainType' => $complainType,
                'jobRelatedSubViewFlag' => $jobRelatedSubViewFlag,
                'inquiryID' => $inquiryLogID,
            ));
            $view->setTemplate('job-card/inquiry-log/view');
            if ($this->getRequest()->isPost()) {
                $view->setTerminal(TRUE);
                $this->html = $view;
                $this->status = TRUE;
                return $this->JSONRespondHtml();
            } else {
                $customerHistoryView = new ViewModel();
                $customerHistoryView->setTemplate('job-card/inquiry-log/customer-history');
                $documentView = new ViewModel();
                $documentView->setTemplate('job-card/inquiry-log/document-view');
                $view->addChild($customerHistoryView, 'customerHistoryView');
                $view->addChild($documentView, 'documentView');
                return $view;
            }
        }
    }

    public function inquiryLogEditAction()
    {
        $inquiryLogID = $this->params()->fromRoute('param1');
        $this->sideMenus = 'job_card_side_menu';
        $this->upperMenus = 'inquiry_upper_menu_job_card';
        $this->getSideAndUpperMenus('Inquiry Log', 'Create', 'JobCard');
        return $this->edit($inquiryLogID);
    }

//this function use to edit existing Inquiry Logs
    public function editAction()
    {
        $inquiryLogID = $this->params()->fromRoute('param1');
        $this->getSideAndUpperMenus('Inquiry Log', 'View', 'CRM');
        return $this->edit($inquiryLogID);
    }

    public function edit($inquiryLogID)
    {
        if ($inquiryLogID) {
            $userDateFormat = $this->getUserDateFormat();
            $result = $this->CommonTable('JobCard/Model/InquiryLogTable')->getInquiryLogDetails($inquiryLogID);
            $resultList = (object) $result->current();
            $documentList = $this->CommonTable('JobCard\Model\InquiryComplainRelationTable')->getDocumentForGivenInqID($inquiryLogID);
            foreach ($documentList as $dL) {
                if ($resultList->inquiryComplainType != 'Complain') {
                    if ($dL['documentTypeID'] == '21') {
                        $projectID = $dL['inquiryComplainRelationDocumentID'];
                    } else if ($dL['documentTypeID'] == '22') {
                        $jobID = $dL['inquiryComplainRelationDocumentID'];
                    } else if ($dL['documentTypeID'] == '23') {
                        $activityID = $dL['inquiryComplainRelationDocumentID'];
                    }
                }
            }

            $inquiryTypeDetails = $this->CommonTable('JobCard/Model/InquiryTypeTable')->getInquiryTypeByInquiryTypeID($resultList->inquiryTypeID);
            $inquiryTypeDetail = (object) $inquiryTypeDetails->current();
            $inquiryStatusDetails = $this->CommonTable('JobCard/Model/InquiryStatusTable')->getInquiryStatusByInquiryStatusID($resultList->inquiryStatusID);
            $inquiryStatusDetail = (object) $inquiryStatusDetails->current();
            $logedUser = $this->CommonTable('User/Model/UserTable')->getUser($resultList->inquiryLogCurrentUserID);
            $cusProfileList = $this->CommonTable('Invoice/Model/CustomerProfileTable')->getProfileByCustomerID($resultList->customerID);
            $inquiryLogDutyManagerNames = $this->CommonTable('JobCard/Model/InquiryLogDutyManagerTable')->getInquiryLogDutyManagerNameByInquiryLogID($resultList->inquiryLogID);
            $inc = 0;
            foreach ($inquiryLogDutyManagerNames as $iLMN) {
                $dutyManagerNameList[$inc] = array('fName' => $iLMN[employeeFirstName], 'sName' => $iLMN[employeeSecondName], 'id' => $iLMN[employeeID]);
                $inc++;
            }
            $config = $this->getServiceLocator()->get('config');
            $documentTypeName = $config['documentTypeName'];
            $userDateFormat = $this->convertUserDateFormatToPhpFormat() . ' H:i';
            $inquiryTime = $this->getUserDateTime($resultList->inquiryLogDateAndTime, $userDateFormat);
            $inquiryLogForm = new InquiryLogForm($userDateFormat, '', '', '', '', '', $documentTypeName);
            $inquiryLogForm->get('inquiryLogReference')->setValue($resultList->inquiryLogReference)->setAttribute('data-inqLogID', $inquiryLogID);
            $inquiryLogForm->get('inquiryLogDescription')->setValue($resultList->inquiryLogDescription);
            $inquiryLogForm->get('inquirylogUser')->setValue($logedUser->username)->setAttribute('data-userid', $logedUser->userID);
            $inquiryLogForm->get('inquiryLogDateAndTime')->setValue($inquiryTime);
            $inquiryLogForm->get('inquiryLogContactNo1')->setValue($resultList->inquiryLogTP1);
            $inquiryLogForm->get('inquiryLogContactNo2')->setValue($resultList->inquiryLogTP2);
            $inquiryLogForm->get('inquiryLogAddress')->setValue($resultList->inquiryLogAddress);
            $inquiryLogForm->get('inquiryComplainType')->setValue($resultList->inquiryComplainType)->setAttribute('disabled', true);
            $inquiryLogForm->get('documentTypeName')->setValue($documentTypeValue)->setAttribute('disabled', true);
            $inquiryLogForm->get('documentTypeID')->setValue($documentCode)->setAttribute('disabled', true);

            $this->getServiceLocator()->get('viewhelpermanager')->get('jsvarsHelper')->setVars([
                'customerListWithoutDefaultCustomer', 'getAllJobTypes', 'getAllProject', 'getAllJobStation',
                'getAllInquiryStatusNames', 'getAllInquiryTypes', 'jobReference', 'getAllActivityReferenceNumbers', 'getEmployees'
            ]);

            $this->getViewHelper('HeadScript')->prependFile('/js/job-card/inquiry-log.js');
            $this->getViewHelper('HeadScript')->prependFile('/js/job-card/inquiry-log-view.js');

            $view = new viewModel(array(
                'inquiryLogForm' => $inquiryLogForm,
                'dutyManagerNameList' => $dutyManagerNameList,
                'customerProfile' => $cusProfileList,
                'customerProfileID' => $resultList->customerProfileID,
                'proID' => $projectID,
                'inquiryTypeId' => $inquiryTypeDetail->inquiryTypeID,
                'customerID' => $resultList->customerID,
                'customerName'=>$resultList->customerName."-".$resultList->customerCode,
                'inquiryStatusId' => $inquiryStatusDetail->inquiryStatusID,
                'jobID' => $jobID,
                'actID' => $activityID,
                'inquiryComplainType' => $resultList->inquiryComplainType,
                'documentTypeValue' => $documentTypeValue,
                'inquiryLogID' => $inquiryLogID,
            ));
            $view->setTemplate('job-card/inquiry-log/edit');
            $customerHistoryView = new ViewModel();
            $customerHistoryView->setTemplate('job-card/inquiry-log/customer-history');
            $documentView = new ViewModel();
            $documentView->setTemplate('job-card/inquiry-log/document-view');
            $view->addChild($customerHistoryView, 'customerHistoryView');
            $view->addChild($documentView, 'documentView');
            return $view;
        }
    }

    public function getPaginatedInquiryLog($locationID)
    {
        $paginator = $this->CommonTable('JobCard/Model/inquiryLogTable')->fetchAll($locationID, true);
        $paginator->setCurrentPageNumber((int) $this->params()->fromRoute('param1', 1));
        $paginator->setItemCountPerPage(10);
        return $paginator;
    }

}
