<?php

/**
 * @author Ashan Madushka <ashan@thinkcube.com>
 * This file contains Project Type related controller functions
 */

namespace JobCard\Controller;

use Zend\View\Model\ViewModel;
use Core\Controller\CoreController;
use JobCard\Form\ProjectTypeForm;
use Zend\Session\Container;

class ProjectTypeController extends CoreController
{

    protected $sideMenus = 'job_card_side_menu';
    protected $upperMenus = 'job_card_category_set_up_upper_menu';
    protected $downMenus = 'job_card_set_up_down_menu';

    //create action of project type
    public function createAction()
    {
        $this->getSideAndUpperMenus('Job Card Setup', 'Project Types', 'JobCard', 'Category Setup');

        $projectTypeList = $this->getPaginatedProjectTypes();

        $form = new ProjectTypeForm();

        $view = new ViewModel(array(
            'form' => $form,
            'projectTypeList' => $projectTypeList,
            'paginated' => true
        ));

        $this->getViewHelper('HeadScript')->prependFile('/js/job-card/projectType.js');

        return $view;
    }

    protected function getPaginatedProjectTypes($perPage = 6)
    {
        $this->paginator = $this->CommonTable('JobCard\Model\ProjectTypeTable')->fetchAll(true);
        $this->paginator->setCurrentPageNumber((int) $this->params()->fromRoute('param1', 1));
        $this->paginator->setItemCountPerPage($perPage);
        $this->setLogMessage('Project Type list accessed');

        return $this->paginator;
    }

}
