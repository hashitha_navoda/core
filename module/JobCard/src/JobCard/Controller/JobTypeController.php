<?php

/**
 * @author Ashan Madushka <ashan@thinkcube.com>
 * This file contains Job Type related controller functions
 */

namespace JobCard\Controller;

use Zend\View\Model\ViewModel;
use Core\Controller\CoreController;
use JobCard\Form\JobTypeForm;
use Zend\Session\Container;

class JobTypeController extends CoreController
{

    protected $sideMenus = 'job_card_side_menu';
    protected $upperMenus = 'job_card_category_set_up_upper_menu';
    protected $downMenus = 'job_card_set_up_down_menu';

    //create action of job type
    public function createAction()
    {
        $this->getSideAndUpperMenus('Job Card Setup', 'Job Types', 'JobCard', 'Category Setup');
        $jobTypeList = $this->getPaginatedJobTypes();
        $form = new JobTypeForm();
        $view = new ViewModel(array(
            'form' => $form,
            'jobTypeList' => $jobTypeList,
            'paginated' => true
        ));
        $this->getViewHelper('HeadScript')->prependFile('/js/job-card/jobType.js');
        return $view;
    }

    protected function getPaginatedJobTypes($perPage = 6)
    {
        $this->paginator = $this->CommonTable('JobCard\Model\JobTypeTable')->fetchAll(true);
        $this->paginator->setCurrentPageNumber((int) $this->params()->fromRoute('param1', 1));
        $this->paginator->setItemCountPerPage($perPage);
        $this->setLogMessage('Job Type list accessed');
        return $this->paginator;
    }

}
