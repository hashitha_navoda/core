<?php

namespace JobCard\Controller;

use Core\Controller\CoreController;
use Zend\Session\Container;
use Zend\View\Model\ViewModel;
use JobCard\Form\DesignationForm;

class DesignationsController extends CoreController
{

    protected $sideMenus = 'job_card_side_menu';
    protected $upperMenus = 'job_card_employee_set_up_upper_menu';
    protected $downMenus = 'job_card_set_up_down_menu';

    public function createAction()
    {
        $this->getSideAndUpperMenus('Job Card Setup', 'Designations', 'JobCard', 'Employee Setup');
        $designation = $this->getPaginatedDesignations();
        $addDesignationForm = new DesignationForm();
        $view = new ViewModel(array(
            'addDesignationForm' => $addDesignationForm,
            'designation' => $designation,
            'paginated' => true
        ));
        $this->getViewHelper('HeadScript')->prependFile('/js/job-card/designation.js');
        return $view;
    }

    public function getPaginatedDesignations($perPage = 6)
    {
        $this->designation = $this->CommonTable('JobCard/Model/DesignationTable')->fetchAll(true);
        $this->designation->setCurrentPageNumber((int) $this->params()->fromRoute('param1', 1));
        $this->designation->setItemCountPerPage($perPage);
        return $this->designation;
    }

}
