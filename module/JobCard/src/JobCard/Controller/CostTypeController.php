<?php

/**
 * @author Ashan Madushka <ashan@thinkcube.com>
 * This file contains Cost Type related controller functions
 */

namespace JobCard\Controller;

use Zend\View\Model\ViewModel;
use Core\Controller\CoreController;
use JobCard\Form\CostTypeForm;
use Zend\Session\Container;

class CostTypeController extends CoreController
{

    protected $sideMenus = 'job_card_side_menu';
    protected $upperMenus = 'job_card_general_set_up_upper_menu';
    protected $downMenus = 'job_card_set_up_down_menu';

    //create action of cost type
    public function createAction()
    {
        $this->getSideAndUpperMenus('Job Card Setup', 'Cost Types', 'JobCard', 'General Setup');

        $costTypeList = $this->getPaginatedCostTypes();

        $form = new CostTypeForm();

        $view = new ViewModel(array(
            'form' => $form,
            'costTypeList' => $costTypeList,
            'paginated' => true
        ));

        $this->getViewHelper('HeadScript')->prependFile('/js/job-card/costType.js');
        $this->getServiceLocator()->get('viewhelpermanager')->get('jsvarsHelper')->setVars(['getAllActiveNonInventoryItems']);
        return $view;
    }

    protected function getPaginatedCostTypes($perPage = 6)
    {
        $this->paginator = $this->CommonTable('JobCard\Model\CostTypeTable')->fetchAll(true);
        $this->paginator->setCurrentPageNumber((int) $this->params()->fromRoute('param1', 1));
        $this->paginator->setItemCountPerPage($perPage);
        $this->setLogMessage('Cost Type list accessed');

        return $this->paginator;
    }

}
