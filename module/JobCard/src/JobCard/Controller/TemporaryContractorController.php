<?php

namespace JobCard\Controller;

use Core\Controller\CoreController;
use Zend\Session\Container;
use Zend\View\Model\ViewModel;
use JobCard\Form\TemporaryContractorForm;

class TemporaryContractorController extends CoreController
{

    protected $sideMenus = 'job_card_side_menu';
    protected $upperMenus = 'job_card_contractor_set_up_upper_menu';
    protected $downMenus = 'job_card_set_up_down_menu';
    protected $paginator;

    public function indexAction()
    {
        $this->getSideAndUpperMenus('Job Card Setup', 'Temporary Contractor', 'JobCard', 'Contractor Setup');

        $divisions = $this->CommonTable('JobCard/Model/DivisionTable')->getAllDivisions();
        $designation = $this->CommonTable('JobCard/Model/DesignationTable')->getAllDesignations();
        foreach ($designation as $a) {
            $destinationData[$a['designationID']] = $a['designationName'];
        }
        foreach ($divisions as $b) {
            $divisionsData[$b['divisionID']] = $b['divisionName'];
        }

        $temporaryContractorForm = new TemporaryContractorForm(array(
            'division' => $divisionsData,
            'designation' => $destinationData,
        ));

        $viewmodel = new ViewModel(array(
            'temporaryContractorForm' => $temporaryContractorForm,
        ));

        $searchHeader = new ViewModel();
        $searchHeader->setTemplate('job-card/temporary-contractor/search-header');

        $this->getpaginatedTemporaryContractor();

        $temporaryContractorListView = new ViewModel(array(
            'temporaryContractors' => $this->paginator,
            'paginated' => true,
        ));
        $temporaryContractorListView->setTemplate('job-card/temporary-contractor/list');


        $viewmodel->addChild($searchHeader, 'temporaryContractorSearchHeader');
        $viewmodel->addChild($temporaryContractorListView, 'temporaryContractorList');

        $this->getViewHelper('HeadScript')->prependFile('/js/job-card/temporary-contractor.js');
        return $viewmodel;
    }

    private function getpaginatedTemporaryContractor($size = 6)
    {
        $this->paginator = $this->CommonTable('JobCard\Model\TemporaryContractorTable')->fetchAll(true);
        $this->paginator->setCurrentPageNumber((int) $this->params()->fromRoute('param1', 1));
        $this->paginator->setItemCountPerPage($size);
    }

}
