<?php

namespace JobCard\Controller;

use Core\Controller\CoreController;
use Zend\Session\Container;
use Zend\View\Model\ViewModel;
use JobCard\Form\JobForm;
use Invoice\Form\AddCustomerForm;
use Zend\I18n\Translator\Translator;

class JobController extends CoreController
{

    protected $sideMenus = 'job_card_side_menu';
    protected $upperMenus = 'job_card_job_upper_menu';
    private $_jobViewData;
    private $useAccounting;

    public function __construct()
    {
        parent::__construct();
        $this->user_session = new Container('ezBizUser');
        $this->useAccounting = $this->user_session->useAccounting;
    }

    public function createAction()
    {
        $this->getSideAndUpperMenus('Job', 'Create', 'JobCard');
        $locationID = $this->user_session->userActiveLocation['locationID'];
        $userDateFormat = $this->getUserDateFormat();
        $reference = $this->getReferenceNoForLocation(22, $locationID);
        $jobReferenceNumber = $reference['refNo'];
        $jobReferenceNumberLocationID = $reference['locRefID'];
        $projectID = $this->params('param1');
        $inqID = $this->params('param2');
        $projectCode = "";

        $paymentTerms = $this->CommonTable('Core\Model\PaymentTermTable')->fetchAll();
        $terms = array();
        while ($row = $paymentTerms->current()) {
            $id = $row->paymentTermID;
            if ($id == NULL) {
                $id = 0;
            }
            $terms[$id] = $row->paymentTermName;
        }
        $cusRefData = $this->getReferenceNoForLocation(20, $locationID);
        $locationReferenceID = $cusRefData["locRefID"];
        $customerCode = $this->getReferenceNumber($locationReferenceID);

        $currencyResultSet = $this->CommonTable('Core\Model\CurrencyTable')->fetchAll();
        $currencies = array();
        $currencies['0'] = '';
        foreach ($currencyResultSet as $row) {
            $currencies[$row->currencyID] = $row->currencyName;
        }
        $currencyValue = $this->getDefaultCurrency();
        $config = $this->getServiceLocator()->get('config');
        $customerTitle = $config['personTitle'];
        $countries = ['' => ''] + $config['all_countries'];
        $customerForm = new AddCustomerForm(NULL, $terms, $currencies, $customerTitle, $currencyValue);
        $customerForm->get('customerFormsubmit')->setValue(_("Add New Customer"));
        $customerForm->get('customerCode')->setAttribute('value', $customerCode);

        $financeAccountsArray = $this->getFinanceAccounts();

        $glAccountExist = $this->CommonTable('Accounting\Model\GlAccountSetupTable')->fetchAll();
        if ($this->useAccounting == 1) {
            $glAccountSetupData = $glAccountExist->current();

            if($glAccountSetupData->glAccountSetupSalesAndCustomerReceivableAccountID != ''){
                $customerReceviableAccountID = $glAccountSetupData->glAccountSetupSalesAndCustomerReceivableAccountID;
                $customerReceviableAccountName = $financeAccountsArray[$customerReceviableAccountID]['financeAccountsCode']."_".$financeAccountsArray[$customerReceviableAccountID]['financeAccountsName'];
                $customerForm->get('customerReceviableAccountID')->setAttribute('data-id',$customerReceviableAccountID);
                $customerForm->get('customerReceviableAccountID')->setAttribute('data-value',$customerReceviableAccountName);
            }

            if($glAccountSetupData->glAccountSetupSalesAndCustomerSalesAccountID != ''){
                $customerSalesAccountID = $glAccountSetupData->glAccountSetupSalesAndCustomerSalesAccountID;
                $customerSalesAccountName = $financeAccountsArray[$customerSalesAccountID]['financeAccountsCode']."_".$financeAccountsArray[$customerSalesAccountID]['financeAccountsName'];
                $customerForm->get('customerSalesAccountID')->setAttribute('data-id',$customerSalesAccountID);
                $customerForm->get('customerSalesAccountID')->setAttribute('data-value',$customerSalesAccountName);
            }

            if($glAccountSetupData->glAccountSetupSalesAndCustomerSalesDiscountAccountID != ''){
                $customerSalesDiscountAccountID = $glAccountSetupData->glAccountSetupSalesAndCustomerSalesDiscountAccountID;
                $customerSalesDiscountAccountName = $financeAccountsArray[$customerSalesDiscountAccountID]['financeAccountsCode']."_".$financeAccountsArray[$customerSalesDiscountAccountID]['financeAccountsName'];
                $customerForm->get('customerSalesDiscountAccountID')->setAttribute('data-id',$customerSalesDiscountAccountID);
                $customerForm->get('customerSalesDiscountAccountID')->setAttribute('data-value',$customerSalesDiscountAccountName);
            }

            if($glAccountSetupData->glAccountSetupSalesAndCustomerAdvancePaymentAccountID != ''){
                $customerAdvancePaymentAccountID = $glAccountSetupData->glAccountSetupSalesAndCustomerAdvancePaymentAccountID;
                $customerAdvancePaymentAccountName = $financeAccountsArray[$customerAdvancePaymentAccountID]['financeAccountsCode']."_".$financeAccountsArray[$customerAdvancePaymentAccountID]['financeAccountsName'];
                $customerForm->get('customerAdvancePaymentAccountID')->setAttribute('data-id',$customerAdvancePaymentAccountID);
                $customerForm->get('customerAdvancePaymentAccountID')->setAttribute('data-value',$customerAdvancePaymentAccountName);
            }
        }

        $customerAddView = new ViewModel(array(
            'customerForm' => $customerForm,
            'countries' => $countries,
            'useAccounting' =>$this->useAccounting
        ));
        $customerAddView->setTemplate('invoice/customer/customer-add-modal');

        $this->getServiceLocator()->get('viewhelpermanager')->get('jsvarsHelper')->setVars([
            'customerListWithoutDefaultCustomer', 'getAllJobTypes', 'getAllProject', 'getAllJobStation', 'getEmployees'
        ]);

        $this->getViewHelper('HeadScript')->prependFile('/js/job-card/job.js');
        $this->getViewHelper('HeadScript')->prependFile('/js/time-duration.js');
        $this->getViewHelper('HeadScript')->prependFile('/css/datetimepicker.css');
        $this->getViewHelper('HeadScript')->prependFile('/js/invoice/modalAddCustomer.js');
        $jobForm = new JobForm($userDateFormat, $jobReferenceNumber);
        $customerName = '';
        $cusProfileList = array();
        $customerProfileID = '';
        if ($projectID != '' && $projectID != '0') {
            $projectData = (object) $this->CommonTable('JobCard/Model/ProjectTable')->getProjectDetailsByProjectID($projectID)->current();
            $projectCode = $projectData->projectCode;
            $cusProfile = (object) $this->CommonTable('Invoice/Model/CustomerProfileTable')->getProfileByCustomerID($projectData->customerID);
            foreach ($cusProfile as $cP) {
                $cusProfileList[$cP[customerProfileID]] = $cP[customerProfileName];
            }
            $customerProfileID = $projectData->customerProfileID;
            $customerName = $projectData->customerID;
        }
        if ($projectID == '0' && $inqID) {
            $inqData = (object) $this->CommonTable("JobCard/Model/InquiryLogTable")->getInquiryLogDetails($inqID)->current();
            $cusProfile = (object) $this->CommonTable('Invoice/Model/CustomerProfileTable')->getProfileByCustomerID($inqData->customerID);
            foreach ($cusProfile as $cP) {
                $cusProfileList[$cP[customerProfileID]] = $cP[customerProfileName];
            }
            $customerProfileID = $inqData->customerProfileID;
            $customerName = $inqData->customerID;
            $projectID = null;
        }

        $view = new ViewModel(array(
            'jobForm' => $jobForm,
            'projectID' => $projectID,
            'projectCode' => $projectCode,
            'customerProfile' => $cusProfileList,
            'customerProfileID' => $customerProfileID,
            'projectID' => $projectID,
            'customerName' => $customerName,
        ));
        if ($inqID) {
            $jobForm->get('jobReference')->setAttribute('data-inquiryid', $inqID);
        }
        if ($jobReferenceNumber == '' || $jobReferenceNumber == null) {
            if ($jobReferenceNumberLocationID == null) {
                $title = 'Job Reference Number not set';
                $msg = $this->getMessage('ERR_JOB_ADD_REF');
            } else {
                $title = 'Job Reference Number has reache the maximum limit ';
                $msg = $this->getMessage('ERR_JOB_CHANGE_REF');
            }
            $refNotSet = new ViewModel(array(
                'title' => $title,
                'msg' => $msg,
                'controller' => 'company',
                'action' => 'reference'
            ));
            $refNotSet->setTemplate('/core/modal/entity-missing-pre-requisites-notification');
            $view->addChild($refNotSet, 'refNotSet');
        }
        $jobWeight = new ViewModel();
        $jobWeight->setTemplate('/job-card/job/add_weight');
        $view->addChild($jobWeight, 'jobWeight');

        $relatedDocumentsJobView = new ViewModel();
        $relatedDocumentsJobView->setTemplate('/job-card/job/job_document_view');
        $view->addChild($relatedDocumentsJobView, 'relatedJobDocu');

        $relatedDocuments = new ViewModel();
        $relatedDocuments->setTemplate('/job-card/job/related_doc');
        $view->addChild($relatedDocuments, 'relatedDocu');

        $view->addChild($customerAddView, 'customerAddView');

        return $view;
    }

    public function listAction()
    {
        $this->getSideAndUpperMenus('Job', 'View', 'JobCard');
        $locationID = $this->user_session->userActiveLocation['locationID'];
        $userDateFormat = $this->getUserDateFormat();
        $projectID = $this->params('param2');
        if ($projectID == '') {
            $jobList = $this->getPaginatedJob($locationID);
            $paginatedflag = true;
        } else {
            $jobList = $this->CommonTable('JobCard/Model/JobTable')->getJobListByProjectID((int) $projectID, $locationID);
            $paginatedflag = false;
        }
        $jobView = new ViewModel(array(
            'jobList' => $jobList,
            'paginated' => $paginatedflag,
            'status' => $this->getStatusesList(),
            'userDateFormat' => $userDateFormat,
        ));

        $this->getServiceLocator()->get('viewhelpermanager')->get('jsvarsHelper')->setVars([
            'customerListWithoutDefaultCustomer', 'getAllJobTypes', 'getAllProject', 'getAllJobStation', 'getEmployees'
        ]);
        $this->getViewHelper('HeadScript')->prependFile('/js/job-card/job.js');
        $this->getViewHelper('HeadScript')->prependFile('/js/time-duration.js');
        $this->getViewHelper('HeadScript')->prependFile('/css/datetimepicker.css');
        return $jobView;
    }

    public function viewAction()
    {
        $jobID = $this->params()->fromRoute('param1');
        if ($jobID) {
            $this->getSideAndUpperMenus('Job', 'View', 'JobCard');
            $result = $this->CommonTable('JobCard/Model/JobTable')->getJobDetails($jobID);
            $resultList = (object) $result->current();
            $jobLocation = $this->CommonTable('JobCard/Model/JobStationTable')->getJobStationByJobStationID($resultList->jobStation);
            $jobStation = (object) $jobLocation->current();
            $supervisorNames = $this->CommonTable('JobCard/Model/JobSupervisorTable')->getJobSupervisorName($resultList->jobReferenceNumber);
            $inc = 0;
            foreach ($supervisorNames as $sn) {
                $supervisorNameList[$inc] = array('fName' => $sn[employeeFirstName], 'sName' => $sn[employeeSecondName]);
                $inc++;
            }
            $ownerNames = $this->CommonTable('JobCard/Model/JobOwnerTable')->getJobOwnerName($resultList->jobReferenceNumber);
            $incs = 0;
            foreach ($ownerNames as $on) {
                $ownerNameList[$incs] = array('fName' => $on[employeeFirstName], 'sName' => $on[employeeSecondName]);
                $incs++;
            }
            if ($resultList->projectId) {
                $projectData = (object) $this->CommonTable('JobCard/Model/ProjectTable')->getProjectDetailsByProjectID($resultList->projectId)->current();
                $projectCode = $projectData->projectCode;
            }
            $cusProfile = (object) $this->CommonTable('Invoice/Model/CustomerProfileTable')->getProfileByCustomerProfileID($resultList->customerProfileID)->current();
            $cusProfileName = $cusProfile->customerProfileName;
            $cusStatus = $cusProfile->customerStatus;
            $this->getViewHelper('HeadScript')->prependFile('/js/job-card/job-view.js');
            $estimateTime = $this->timeCalcutator($resultList->jobEstimatedTime);
            $jobForm = new JobForm('', '');

            $DateFormat = $this->convertUserDateFormatToPhpFormat() . ' H:i';

            $jobForm->get('jobName')->setValue($resultList->jobName);
            $jobForm->get('jobName')->setAttribute('disabled', true);
            $jobForm->get('customerName')->setValue($resultList->customerName);
            $jobForm->get('customerName')->setAttribute('disabled', true);
            $jobForm->get('customerAddress')->setValue($resultList->jobAddress);
            $jobForm->get('customerAddress')->setAttribute('disabled', true);
            $jobForm->get('jobType')->setValue($resultList->jobTypeName);
            $jobForm->get('jobType')->setAttribute('disabled', true);
            $jobForm->get('projectReference')->setValue($projectCode);
            $jobForm->get('projectReference')->setAttribute('disabled', true);
            $jobForm->get('jobDescription')->setValue($resultList->jobDescription);
            $jobForm->get('jobDescription')->setAttribute('disabled', true);
            $jobForm->get('jobOwner')->setAttribute('disabled', true);
            $jobForm->get('jobSupervisor')->setAttribute('disabled', true);
            if($resultList->jobStartingTime != NULL){
                $jobForm->get('jobStartingDate')->setValue($this->getUserDateTime($resultList->jobStartingTime, $DateFormat));
            }
            $jobForm->get('jobStartingDate')->setAttribute('disabled', true);
            $jobForm->get('jobEstimetedTime')->setValue($estimateTime);
            $jobForm->get('jobEstimetedTime')->setAttribute('disabled', true);
            if($resultList->jobEndTime != NULL){
                $jobForm->get('jobEndingDate')->setValue($this->getUserDateTime($resultList->jobEndTime, $DateFormat));
            }
            $jobForm->get('jobEndingDate')->setAttribute('disabled', true);
            $jobForm->get('estimatedCost')->setValue($resultList->jobEstimatedCost);
            $jobForm->get('estimatedCost')->setAttribute('disabled', true);
            $jobForm->get('jobStation')->setValue($jobStation->jobStationName);
            $jobForm->get('jobStation')->setAttribute('data-jbStationid', $resultList->jobStation);
            $jobForm->get('jobStation')->setAttribute('disabled', true);
            $jobForm->get('jobReference')->setValue($resultList->jobReferenceNumber);
            $jobForm->get('jobReference')->setAttribute('disabled', true);
            $jobForm->get('jobRepeatEnabled')->setValue($resultList->jobRepeatEnabled);
            $jobForm->get('jobRepeatEnabled')->setAttribute('disabled', true);
            $jobForm->get('jobRepeatComment')->setValue($resultList->jobRepeatComment);
            $jobForm->get('jobRepeatComment')->setAttribute('disabled', true);
            $view = new viewModel(array(
                'jobDetails' => $jobForm,
                'supervisorList' => $supervisorNameList,
                'ownerList' => $ownerNameList,
                'jobID' => $jobID,
                'cusProfileName' => $cusProfileName,
                'customerStatus' => $cusStatus
            ));
            return $view;
        }
    }

    public function editAction()
    {
        $jobID = $this->params()->fromRoute('param1');
        if ($jobID) {
            $this->getSideAndUpperMenus('Job', 'View', 'JobCard');
            $userDateFormat = $this->getUserDateFormat();
            $result = $this->CommonTable('JobCard/Model/JobTable')->getJobDetails($jobID);
            $resultList = (object) $result->current();
            $jobLocation = $this->CommonTable('JobCard/Model/JobStationTable')->getJobStationByJobStationID($resultList->jobStation);
            $jobStation = (object) $jobLocation->current();
            $supervisorNames = $this->CommonTable('JobCard/Model/JobSupervisorTable')->getJobSupervisorName($resultList->jobReferenceNumber);
            $inc = 0;
            foreach ($supervisorNames as $sn) {
                $supervisorNameList[$inc] = array('fName' => $sn[employeeFirstName], 'sName' => $sn[employeeSecondName], 'id' => $sn[jobSupervisorID]);
                $inc++;
            }
            $ownerNames = $this->CommonTable('JobCard/Model/JobOwnerTable')->getJobOwnerName($resultList->jobReferenceNumber);
            $incs = 0;
            foreach ($ownerNames as $on) {
                $ownerNameList[$incs] = array('fName' => $on[employeeFirstName], 'sName' => $on[employeeSecondName], 'id' => $on[jobOwnerID]);
                $incs++;
            }
            if ($resultList->projectId) {
                $projectData = (object) $this->CommonTable('JobCard/Model/ProjectTable')->getProjectDetailsByProjectID($resultList->projectId)->current();
                $projectCode = $projectData->projectCode;
            }
            $cusProfile = (object) $this->CommonTable('Invoice/Model/CustomerProfileTable')->getProfileByCustomerID($resultList->customerID);
            $cusProfileList = array();
            foreach ($cusProfile as $cP) {
                $cusProfileList[$cP[customerProfileID]] = $cP[customerProfileName];
            }

            $this->getServiceLocator()->get('viewhelpermanager')->get('jsvarsHelper')->setVars([
                'customerListWithoutDefaultCustomer', 'getAllJobTypes', 'getAllProject', 'getAllJobStation', 'getEmployees'
            ]);
            $this->getViewHelper('HeadScript')->prependFile('/js/job-card/job.js');
            $this->getViewHelper('HeadScript')->prependFile('/js/time-duration.js');
            $this->getViewHelper('HeadScript')->prependFile('/css/datetimepicker.css');
            $jobForm = new JobForm($userDateFormat, '');
            $userDateFormat = $this->convertUserDateFormatToPhpFormat() . ' H:i';
            if ($resultList->jobStartingTime) {
//                $startingTime = date($userDateFormat, strtotime($resultList->jobStartingTime));
                $startingTime = $this->getUserDateTime($resultList->jobStartingTime, $userDateFormat);
                $jobForm->get('jobStartingDate')->setValue($startingTime);
            }
            if ($resultList->jobEndTime) {
//                $endTime = date($userDateFormat, strtotime($resultList->jobEndTime));
                $endTime = $this->getUserDateTime($resultList->jobEndTime, $userDateFormat);
                $jobForm->get('jobEndingDate')->setValue($endTime);
            }
            $jobForm->get('jobName')->setValue($resultList->jobName);
            $jobForm->get('jobDescription')->setValue($resultList->jobDescription);
            $jobForm->get('jobEstimetedTime')->setValue($resultList->jobEstimatedTime);
            $jobForm->get('estimatedCost')->setValue($resultList->jobEstimatedCost);
            $jobForm->get('jobReference')->setValue($resultList->jobReferenceNumber);
            $jobForm->get('jobReference')->setAttribute('disabled', true);
            $jobForm->get('customerAddress')->setValue($resultList->jobAddress);
            $jobForm->get('jobRepeatEnabled')->setValue($resultList->jobRepeatEnabled);
            $jobForm->get('jobRepeatComment')->setValue($resultList->jobRepeatComment);

            $view = new viewModel(array(
                'jobDetails' => $jobForm,
                'supervisorList' => $supervisorNameList,
                'ownerList' => $ownerNameList,
                'jobID' => $jobID,
                'customerProfile' => $cusProfileList,
                'customerProfileID' => $resultList->customerProfileID,
                'projectId' => $resultList->projectId,
                'customerID' => $resultList->customerID,
                'jobTypeID' => $resultList->jobTypeId,
                'jobStation' => $resultList->jobStation
            ));
            $relatedDocumentsJobView = new ViewModel();
            $relatedDocumentsJobView->setTemplate('/job-card/job/job_document_view');
            $view->addChild($relatedDocumentsJobView, 'relatedJobDocu');

            $relatedDocuments = new ViewModel();
            $relatedDocuments->setTemplate('/job-card/job/related_doc');
            $view->addChild($relatedDocuments, 'relatedDocu');

            return $view;
        }
    }

    public function getPaginatedJob($locationID)
    {
        $this->job = $this->CommonTable('JobCard/Model/JobTable')->fetchAll($locationID, true);
        $this->job->setCurrentPageNumber((int) $this->params()->fromRoute('param1', 1));
        $this->job->setItemCountPerPage(15);
        return $this->job;
    }

    public function documentPreviewAction()
    {

        $jobID = $this->params()->fromRoute('param1');
        $data = $this->getDataForDocument($jobID);

        if (!$data['customerName']) {
            $customerSalutaion = "";
            $customerNameString = "";
        } else {
            $customerSalutaion = "Dear " . $data['customerName'] . ",";
            $customerNameString = "<strong>Customer Name:</strong>" . $data['customerName'] . " <br />";
        }

        $path = "/job/document/"; //.$invoiceID;
        $translator = new Translator();
        $createNew = $translator->translate('New Job');
        $createPath = "/job/create";
        $data["email"] = array(
            "to" => $data['customerEmail'],
            "subject" => "Invoice from " . $data['companyName'],
            "body" => <<<EMAILBODY

{$customerSalutaion} <br /><br />

Thank you for your business. <br /><br />

An Job has been generated for you from {$data['companyName']} and is attached herewith. <br /><br />

{$customerNameString}
<strong>Job No:</strong> {$data['jobReferenceNumber']} <br />
<strong>Job Start at:</strong> {$data['jobStartingTime']} <br />
<strong>Job End at:</strong> {$data['jobEndTime']} <br /><br />

Best Regards, <br />
{$data['companyName']}

EMAILBODY
        );
        $documentType = 'Job';
      
        $jobView = $this->getCommonPreview($data, $path, $createNew, $documentType, $jobID, $createPath);
        $jobView->setTerminal(TRUE);

        return $jobView;
    }

    public function documentPdfAction()
    {
        $jobID = $this->params()->fromRoute('param1');
        $templateID = $this->params()->fromRoute('param2');
        $documentType = 'Job';

        $sm = $this->getServiceLocator();
        $tpl = $sm->get('Template');
        if (empty($templateID)) {
            $templateID = $tpl->getDefaultTemplate($documentType)['templateID'];
        }

        // Get template details
        $this->templateDetails = $tpl->getTemplateByID($templateID);
        $pathToDocument = '/job-card/job/templates/' . strtolower(trim(preg_replace('/\(.*\)/i', '', $this->templateDetails['documentSizeName'])));

        $documentData = $this->getDataForDocument($jobID);

        $documentData['data_table'] = $this->rendererDocumentDataTable($pathToDocument, $documentData);

        $this->generateDocumentPdf($jobID, $documentType, $documentData, $templateID);

        return;
    }

    public function getDataForDocument($jobID)
    {

        if (!empty($this->_jobViewData)) {
            return $this->_jobViewData;
        }

        $data = $this->getDataForDocumentView();

        $jobDetails = (array) $this->CommonTable('JobCard/Model/JobTable')->getJobDetails($jobID)->current();
        $jobOwners = $this->CommonTable('JobCard\Model\JobOwnerTable')->getJobOwnersDetailsByJobReference($jobDetails['jobReferenceNumber']);

        $owners = '';
        foreach ($jobOwners as $owner) {
            $owners.=$owner['employeeFirstName'] . '-' . $owner['employeeSecondName'];
        }
        $jobDetails['owners'] = $owners;

        $jobSupervisors = $this->CommonTable('JobCard\Model\JobSupervisorTable')->getJobSupervisorsDetailsByJobReference($jobDetails['jobReferenceNumber']);

        $supervisors = '';
        foreach ($jobSupervisors as $supervisor) {
            $supervisors.=$supervisor['employeeFirstName'] . '-' . $supervisor['employeeSecondName'];
        }
        $jobDetails['supervisors'] = $supervisors;

        $jobDetails['jobStartingTime'] = $this->convertDateToUserFormat($jobDetails['jobStartingTime']);
        $jobDetails['jobEndTime'] = $this->convertDateToUserFormat($jobDetails['jobEndTime']);

//      replace customer Address and Email from primary profile of current customer
        $addressArray[] = $jobDetails['customerProfileLocationNo'];
        $addressArray[] = $jobDetails['customerProfileLocationRoadName1'];
        $addressArray[] = $jobDetails['customerProfileLocationRoadName2'];
        $addressArray[] = $jobDetails['customerProfileLocationRoadName3'];
        $addressArray[] = $jobDetails['customerProfileLocationSubTown'];
        $addressArray[] = $jobDetails['customerProfileLocationTown'];
        $addressArray[] = $jobDetails['customerProfileLocationPostalCode'];
        $addressArray[] = $jobDetails['customerProfileLocationCountry'];
        $address = implode(',', array_filter($addressArray));
        $jobDetails['customerAddress'] = $address . '.';
        $jobDetails['customerEmail'] = $jobDetails['customerProfileEmail'];
        $jobDetails['createdTimeStamp'] = $this->getUserDateTime($jobDetails['createdTimeStamp']);

        $data = array_merge($data, $jobDetails);

        //job details
        $locationID = $this->user_session->userActiveLocation['locationID'];

        $activityArray = $this->CommonTable('JobCard\Model\ActivityTable')->getActivityListByJobID($jobID);
        foreach ($activityArray as $values) {
            $values = (object) $values;
            //get activity raw materials details
            $activityRawData = $this->CommonTable('JobCard\Model\ActivityTable')->getActivityRawMaterialsByActivityID($values->activityId);
            foreach ($activityRawData as $rawValues) {
                $rawValues = (object) $rawValues;
                if ($rawValues->activityRawMaterialId != '') {
                    $subProduct = [
                        'productBatchID' => $rawValues->productBatchID,
                        'productBatchCode' => $rawValues->productBatchCode,
                        'productSerialID' => $rawValues->productSerialID,
                        'productSerialCode' => $rawValues->productSerialCode,
                    ];
                    $produtArray = [
                        'productID' => $rawValues->productID,
                        'productCode' => $rawValues->productCode,
                        'productName' => $rawValues->productName,
                        'productQty' => $rawValues->activityRawMaterialQuantity,
                        'productUnitPrice' => $rawValues->activityRawMaterialCost,
                        'activityRawMaterialId' => $rawValues->activityRawMaterialId,
                        'subProduct' => $subProduct,
                    ];
                    //add activity raw materials product and sub product data to array
                    $product[] = $produtArray;
                }
            }
        }

        $activityTemporaryProduct = $this->CommonTable('JobCard\Model\TemporaryProductTable')->getTemporaryProductsByJobID($jobID);
        foreach ($activityTemporaryProduct as $tempVal) {
            $tempVal = (object) $tempVal;
            $tempProdutArray = [
                'productID' => $tempVal->temporaryProductID,
                'productCode' => $tempVal->temporaryProductCode,
                'productName' => $tempVal->temporaryProductName,
                'productQty' => $tempVal->temporaryProductQuantity,
                'productUnitPrice' => $tempVal->temporaryProductPrice,
                'ProductBatches' => $tempVal->temporaryProductBatch,
                'ProductSerials' => $tempVal->temporaryProductSerial,
            ];
            $activityTempProduct[$tempVal->temporaryProductID] = $tempProdutArray;
        }
//check product exist or not.
        if ($product) {
            //set same prodct in to one index and add sub product details to product array
            foreach ($product as $values) {

                $values = (object) $values;
                $productID = $values->productID;
                $productBatchID = $values->subProduct['productBatchID'];
                $productBatchCode = $values->subProduct['productBatchCode'];
                $productSerialID = $values->subProduct['productSerialID'];
                $productSerialCode = $values->subProduct['productSerialCode'];
                $activityRawMaterialId = $values->activityRawMaterialId;

                if ($productBatchID != '' && $productSerialID != '') {
                    $serials = [
                        'productSerialID' => $productSerialID,
                        'productSerialCode' => $productSerialCode,
                    ];
                    $pSerials[$productID][$productBatchID][$productSerialID] = $serials;
                    $batches = [
                        'productBatchID' => $productBatchID,
                        'productBatchCode' => $productBatchCode,
                        'productSerials' => $pSerials[$productID][$productBatchID],
                    ];
                    $productBatchSerial[$productID][$productBatchID] = $batches;
                } else if ($productBatchID != '') {
                    $batches = [
                        'productBatchID' => $productBatchID,
                        'productBatchCode' => $productBatchCode,
                    ];
                    $productBatches[$productID][$productBatchID] = $batches;
                } else if ($productSerialID != '') {
                    $serials = [
                        'productSerialID' => $productSerialID,
                        'productSerialCode' => $productSerialCode,
                    ];
                    $productSerials[$productID][$productSerialID] = $serials;
                }

                //get total quantity of the each products
                if (!$activityRawMaterialIds[$activityRawMaterialId]) {
                    $activityRawMaterialIds[$activityRawMaterialId] = true;
                    if ($productQty[$productID]) {
                        $productQty[$productID] += $values->productQty;
                    } else {
                        $productQty[$productID] = $values->productQty;
                    }


                    if ($productUnitPrice[$productID]) {
                        $productUnitPrice[$productID] += $values->productUnitPrice * $values->productQty;
                    } else {
                        $productUnitPrice[$productID] = $values->productUnitPrice * $values->productQty;
                    }
                }


//            $productQty[$productID][$activityRawMaterialId] = $values->productQty;
                $produtArray = [
                    'productID' => $values->productID,
                    'productCode' => $values->productCode,
                    'productName' => $values->productName,
                    'productQty' => number_format($productQty[$productID], 2),
                    'productUnitPrice' => number_format($productUnitPrice[$productID] / $productQty[$productID], 2),
                    'ProductBatches' => $productBatches[$productID],
                    'ProductSerials' => $productSerials[$productID],
                    'ProductBatchSerials' => $productBatchSerial[$productID],
                    'ProductTotalPrice' => number_format($productQty[$productID] * ($productUnitPrice[$productID] / $productQty[$productID]), 2),
                ];
                $activityProduct[$productID] = $produtArray;
            }
        }

//pass activity produts data
        $data['activityProducts'] = $activityProduct;
        $data['activityTempProducts'] = $activityTempProduct;

        return $this->_jobViewData = $data;
    }

    public function documentAction()
    {
        $documentType = 'Job';
        $jobID = $this->params()->fromRoute('param1');
        $templateID = $this->params()->fromRoute('param2');
        
        echo $this->generateDocument($jobID, $documentType, $templateID);
        exit;
    }

    public function getDocumentDataTable($jobID, $documentSize = 'A4')
    {
        
        $data_table_vars = $this->getDataForDocument($jobID);
        $view = new ViewModel($data_table_vars);
        $view->setTemplate('/job-card/job/templates/' . strtolower($documentSize));
        $view->setTerminal(TRUE);

        return $this->getServiceLocator()
                        ->get('viewrenderer')
                        ->render($view);
    }

}
