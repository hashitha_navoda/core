<?php

namespace JobCard\Controller;

use Core\Controller\CoreController;
use Zend\Session\Container;
use Zend\View\Model\ViewModel;
use JobCard\Form\ContractorForm;

class ContractorController extends CoreController
{

    protected $sideMenus = 'job_card_side_menu';
    protected $upperMenus = 'job_card_contractor_set_up_upper_menu';
    protected $downMenus = 'job_card_set_up_down_menu';

    public function createAction()
    {
        $this->getSideAndUpperMenus('Job Card Setup', 'Contractor', 'JobCard', 'Contractor Setup');
        $this->getViewHelper('HeadScript')->prependFile('/js/job-card/contractor.js');
        $contractors = $this->getPaginatedContractor();

        $divisions = $this->CommonTable('JobCard/Model/DivisionTable')->getAllDivisions();
        $designation = $this->CommonTable('JobCard/Model/DesignationTable')->getAllDesignations();
        foreach ($designation as $a) {
            $destinationData[$a['designationID']] = $a['designationName'];
        }
        foreach ($divisions as $b) {
            $divisionsData[$b['divisionID']] = $b['divisionName'];
        }

        $contractorForm = new ContractorForm(array(
            'division' => $divisionsData,
            'designation' => $destinationData,
                )
        );

        $view = new ViewModel(array(
            'contractorForm' => $contractorForm,
            'contractorList' => $contractors,
            'paginated' => true,
        ));
        return $view;
    }

    public function getPaginatedContractor()
    {

        $this->paginator = $this->CommonTable('JobCard/Model/ContractorTable')->fetchAll(true);
        $this->paginator->setCurrentPageNumber((int) $this->params()->fromRoute('param1', 1));
        $this->paginator->setItemCountPerPage(5);
        return $this->paginator;
    }

}
