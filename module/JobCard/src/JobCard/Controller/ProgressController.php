<?php

/**
 * @author Ashan Madushka <ashan@thinkcube.com>
 * This file contains Progress related controller functions
 */

namespace JobCard\Controller;

use Zend\View\Model\ViewModel;
use Core\Controller\CoreController;
use Zend\Session\Container;

class ProgressController extends CoreController
{

    protected $sideMenus = 'job_card_side_menu';
    protected $upperMenus = 'progress_upper_menu';

//    create action of progress
    public function createAction()
    {
        $this->getSideAndUpperMenus('Progress Update', 'Update Progress', 'JobCard');
        $view = new ViewModel();

        $this->getViewHelper('HeadScript')->prependFile('/js/job-card/progress.js');
        $this->getServiceLocator()->get('viewhelpermanager')->get('jsvarsHelper')->setVars([
            'getAllProjectWithProgress', 'jobReference'
        ]);
        return $view;
    }

}
