<?php

namespace JobCard\Controller;

use Core\Controller\CoreController;
use Zend\Session\Container;
use Zend\View\Model\ViewModel;
use JobCard\Form\EmployeesForm;

class EmployeesController extends CoreController
{

    protected $sideMenus = 'job_card_side_menu';
    protected $upperMenus = 'job_card_employee_set_up_upper_menu';
    protected $downMenus = 'job_card_set_up_down_menu';

    public function createAction()
    {
        $this->getSideAndUpperMenus('Job Card Setup', 'Employees', 'JobCard', 'Employee Setup');
        $employees = $this->getPaginatedEmployees();
        $desti = array();
        $div = array();
        $divisions = $this->CommonTable('JobCard/Model/DivisionTable')->getAllDivisions();
        $designation = $this->CommonTable('JobCard/Model/DesignationTable')->getAllDesignations();
        foreach ($designation as $a) {
            $desti[$a['designationID']] = $a['designationName'];
        }
        foreach ($divisions as $b) {
            $div[$b['divisionID']] = $b['divisionName'];
        }
        $activeUsers = $this->CommonTable('User\Model\UserTable')->getActiveUsers();
        $users = array();
        foreach ($activeUsers as $activeUser) {
            $users[$activeUser['userID']] = $activeUser['userUsername'];
        }

        $addEmployeeForm = new EmployeesForm(array(
            'division' => $div,
            'designation' => $desti,
            'users' => $users
        ));
        $view = new ViewModel(array(
            'addEmployeeForm' => $addEmployeeForm,
            'employees' => $employees,
            'paginated' => true,
            'employee_list' => $this->employee,
        ));
        $this->getViewHelper('HeadScript')->prependFile('/js/job-card/employee.js');
        return $view;
    }

    public function getPaginatedEmployees()
    {

        $this->employee = $this->CommonTable('JobCard/Model/EmployeeTable')->fetchAll(true);
        $this->employee->setCurrentPageNumber((int) $this->params()->fromRoute('param1', 1));
        $this->employee->setItemCountPerPage(5);
        return $this->employee;
    }

}
