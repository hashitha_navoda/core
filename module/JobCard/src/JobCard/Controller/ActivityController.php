<?php

/**
 * @author Sandun Dissanayake <sandun@thinkcube.com>
 * This file contains Activity related controller functions
 */

namespace JobCard\Controller;

use Zend\View\Model\ViewModel;
use Core\Controller\CoreController;
use JobCard\Form\ActivityForm;
use JobCard\Form\ContractorForm;
use JobCard\Form\TemporaryContractorForm;
use JobCard\Form\TemporaryProductForm;
use JobCard\Form\ActivityTypeForm;
use Inventory\Form\InventoryAdjustmentForm;

class ActivityController extends CoreController
{

    protected $sideMenus = 'job_card_side_menu';
    protected $upperMenus = 'activity_upper_menu';

    public function createAction()
    {
        $this->getSideAndUpperMenus('Activity', 'Create', 'JobCard');
        $locationID = $this->user_session->userActiveLocation['locationID'];
        $reference = $this->getReferenceNoForLocation(23, $locationID);
        $activityReferenceNumber = $reference['refNo'];
        $activityReferenceID = $reference['locRefID'];
        $jobID = $this->params('param1');
        $inqID = $this->params('param2');
        $userdateFormat = $this->getUserDateFormat();
        $activityForm = new ActivityForm($userdateFormat);
        $projectCode = '';
        $projectID = '';
        $jobArray = "";

        if ($jobID) {
            $jobDetails = $this->CommonTable('JobCard/Model/JobTable')->getJobDetails($jobID);
            $resultList = (object) $jobDetails->current();
            if ($inqID) {
                $activityForm->get('activityID')->setAttribute('data-inquiryid', $inqID);
            }
            if ($resultList->projectId) {
                $projectID = $resultList->projectId;
                $projectData = (object) $this->CommonTable('JobCard/Model/ProjectTable')->getProjectDetailsByProjectID($projectID)->current();
                $projectCode = $projectData->projectCode;
                $jobData = (object) $this->CommonTable('JobCard/Model/JobTable')->getJobListByProjectID($projectID, $locationID);
                foreach ($jobData as $key) {
                    $key = (object) $key;
                    $jobArray[$key->jobId] = $key->jobReferenceNumber;
                }
            } else {
                $jobData = (object) $this->CommonTable('JobCard/Model/JobTable')->getJobDetails($jobID)->current();
                $jobArray[$jobData->jobId] = $jobData->jobReferenceNumber;
            }
            $jobName = $resultList->jobName;
            $customerName = $resultList->customerName;
        }

        $view = new ViewModel(array(
            'activityForm' => $activityForm,
            'locationID' => $locationID,
            'projectCode' => $projectCode,
            'projectID' => $projectID,
            'jobArray' => $jobArray,
            'jobID' => $jobID,
            'jobName' => $jobName,
            'customerName' => $customerName,
        ));

        if ($activityReferenceNumber) {
            $activityForm->get('activityID')->setValue($activityReferenceNumber)->setAttribute('disabled', true);
        }
        $activityForm->get('locationID')->setValue($locationID);

        $divisions = $this->CommonTable('JobCard/Model/DivisionTable')->getAllDivisions();
        $designation = $this->CommonTable('JobCard/Model/DesignationTable')->getAllDesignations();

        $destinationData = [];
        $divisionsData = [];
        foreach ($designation as $a) {
            $destinationData[$a['designationID']] = $a['designationName'];
        }
        foreach ($divisions as $b) {
            $divisionsData[$b['divisionID']] = $b['divisionName'];
        }

        //add modal of activity type form into main window
        $activityTypeForm = new ActivityTypeForm();
        $activityTypeAddView = new ViewModel(array('activityTypeForm' => $activityTypeForm));
        $activityTypeAddView->setTemplate('job-card/activity-type/activity-type-add-modal');
        $view->addChild($activityTypeAddView, 'activityTypeAddView');

        //add modal of contractor form into main window
        $contractorForm = new ContractorForm(array(
            'division' => $divisionsData,
            'designation' => $destinationData,
        ));
        $contractorAddView = new ViewModel(array('contractorForm' => $contractorForm));
        $contractorAddView->setTemplate('job-card/contractor/contractor-add-modal');
        $view->addChild($contractorAddView, 'contractorAddView');

        //add modal of temporary contractor form into main window
        $tempContractorForm = new TemporaryContractorForm(array(
            'division' => $divisionsData,
            'designation' => $destinationData,
        ));
        $tempContractorAddView = new ViewModel(array('tempContractorForm' => $tempContractorForm));
        $tempContractorAddView->setTemplate('job-card/temporary-contractor/temp-contractor-add-modal');
        $view->addChild($tempContractorAddView, 'tempContractorAddView');

        $uomList = $this->CommonTable('Settings\Model\uomTable')->activeFetchAll();
        $tempData = array(
            'tempQty' => 1,
        );
        $temporaryProductForm = new TemporaryProductForm($tempData);
        $temporaryProductAddView = new ViewModel(array('tempProductForm' => $temporaryProductForm, 'uomList' => $uomList));
        $temporaryProductAddView->setTemplate('job-card/temporary-product/temp-product-add-modal');
        $view->addChild($temporaryProductAddView, 'tempProductAddView');

        $activityWeight = new ViewModel();
        $activityWeight->setTemplate('/job-card/activity/add_weight');
        $view->addChild($activityWeight, 'activityWeight');

        $inveAdjustForm = new InventoryAdjustmentForm(array(
            'name' => 'inventoryAdjustForm'));

        $rawMaterialProduct = new ViewModel(array(
            'inventoryAdjustForm' => $inveAdjustForm,
            'companyCurrencySymbol' => $this->companyCurrencySymbol
        ));
        $rawMaterialProduct->setTemplate("/job-card/activity/add-raw-material-product");
        $view->addChild($rawMaterialProduct, 'rawMaterialProduct');

        $this->getViewHelper('HeadScript')->prependFile('/js/time-duration.js');
        $this->getViewHelper('HeadScript')->prependFile('/js/job-card/activity.js');
        $this->getViewHelper('HeadScript')->prependFile('/js/job-card/contractor.js');
        $this->getViewHelper('HeadScript')->prependFile('/js/job-card/raw-material.js');
        $this->getServiceLocator()->get('viewhelpermanager')->get('jsvarsHelper')->setVars([
            'activityTypes', 'division', 'contractors', 'temporaryContractors', 'getEmployees',
            'rawMaterialProducts', 'fixedAssetsProducts', 'temporaryProducts', 'uomByID', 'jobReference',
            'costTypes', 'uomAbbrByProductID', 'getAllProject'
        ]);

        $this->getViewHelper('HeadScript')->prependFile('/js/jcrop/jquery.Jcrop.min.js');
        $this->getViewHelper('HeadScript')->prependFile('/js/jcrop/script.js');
        $this->getViewHelper('HeadScript')->prependFile('/js/jquery-ui.js');
        $this->getViewHelper('HeadLink')->prependStylesheet('/css/jcrop/jquery.Jcrop.css');
        $this->getViewHelper('HeadLink')->prependStylesheet('/css/jcrop/jquery.Jcrop.min.css');
        $this->getViewHelper('HeadLink')->prependStylesheet('/css/inventory/product.css');

//      for  image upload
        $this->getViewHelper('HeadLink')->appendStylesheet('/assets/image-uploader/css/jquery.fileupload.css');
        $this->getViewHelper('HeadLink')->appendStylesheet('/assets/image-uploader/css/jquery.fileupload-ui.css');
        $this->getViewHelper('HeadLink')->appendStylesheet('/assets/image-uploader/css/delete-image.css');

        $this->getViewHelper('HeadScript')->appendFile('/assets/image-uploader/js/cors/jquery.postmessage-transport.js');
        $this->getViewHelper('HeadScript')->appendFile('/assets/image-uploader/js/cors/jquery.xdr-transport.js');
        $this->getViewHelper('HeadScript')->appendFile('/assets/image-uploader/js/vendor/jquery.ui.widget.js');
        $this->getViewHelper('HeadScript')->appendFile('/assets/image-uploader/js/load-image.all.min.js');
        $this->getViewHelper('HeadScript')->appendFile('/assets/image-uploader/js/jquery.iframe-transport.js');
        $this->getViewHelper('HeadScript')->appendFile('/assets/image-uploader/js/jquery.fileupload.js');
        $this->getViewHelper('HeadScript')->appendFile('/assets/image-uploader/js/jquery.fileupload-ui.js');
        $this->getViewHelper('HeadScript')->appendFile('/assets/image-uploader/js/tmpl.min.js');
        $this->getViewHelper('HeadScript')->appendFile('/assets/image-uploader/js/canvas-to-blob.min.js');
        $this->getViewHelper('HeadScript')->appendFile('/assets/image-uploader/js/jquery.blueimp-gallery.min.js');
        $this->getViewHelper('HeadScript')->appendFile('/assets/image-uploader/js/jquery.fileupload-process.js');
        $this->getViewHelper('HeadScript')->appendFile('/assets/image-uploader/js/jquery.fileupload-image.js');
        $this->getViewHelper('HeadScript')->appendFile('/assets/image-uploader/js/jquery.fileupload-validate.js');
        $this->getViewHelper('HeadScript')->appendFile('/js/job-card/temporary-product.js');

        if ($activityReferenceNumber == '' || $activityReferenceNumber == NULL) {
            if ($activityReferenceID == NULL) {
                $title = 'Activity Reference Number not set';
                $msg = $this->getMessage('ERR_ADD_REF');
            } else {
                $title = 'Activity Reference Number has reache the maximum limit ';
                $msg = $this->getMessage('ERR_CHANGE_REF');
            }
            $refNotSet = new ViewModel(array(
                'title' => $title,
                'msg' => $msg,
                'controller' => 'company',
                'action' => 'reference',
            ));
            $refNotSet->setTemplate('/core/modal/entity-missing-pre-requisites-notification');
            $view->addChild($refNotSet, 'refNotSet');
        }
//        clear user temporary attachments folder
        $this->getTemporaryAttachmentDirectoryPath(true);

        $relatedDocumentsJobView = new ViewModel();
        $relatedDocumentsJobView->setTemplate('/job-card/job/job_document_view');
        $view->addChild($relatedDocumentsJobView, 'relatedJobDocu');

        $relatedDocuments = new ViewModel();
        $relatedDocuments->setTemplate('/job-card/job/related_doc');
        $view->addChild($relatedDocuments, 'relatedDocu');

        return $view;
    }

    public function listAction()
    {
        $this->getSideAndUpperMenus('Activity', 'View', 'JobCard');
        $locationID = $this->user_session->userActiveLocation['locationID'];
        $userDateFormat = $this->getUserDateFormat();
        $jobID = $this->params('param2');

        if ($jobID == '') {
            $activityList = $this->getPaginatedActivity($locationID, 6);
            $paginatedflag = true;
        } else {
            $activityList = $this->CommonTable('JobCard/Model/ActivityTable')->getActivityListByJobID($jobID);
            $paginatedflag = false;
        }

        $this->getViewHelper('HeadScript')->prependFile('/js/time-duration.js');
        $this->getViewHelper('HeadScript')->prependFile('/js/job-card/edit-activity.js');
        $this->getViewHelper('HeadScript')->prependFile('/js/job-card/contractor.js');
        $this->getServiceLocator()->get('viewhelpermanager')->get('jsvarsHelper')->setVars([
            'activityTypes', 'division', 'contractors', 'temporaryContractors', 'getEmployees',
            'rawMaterialProducts', 'fixedAssetsProducts', 'temporaryProducts', 'uomByID', 'jobReference',
            'costTypes', 'uomAbbrByProductID', 'getAllProject'
        ]);

        $activityView = new ViewModel(array(
            'activityList' => $activityList,
            'paginated' => $paginatedflag,
            'status' => $this->getStatusesList(),
            'userDateFormat' => $userDateFormat,
        ));
        return $activityView;
    }

    public function viewAction()
    {
        $activityID = $this->params()->fromRoute('param1');
        if ($activityID) {
            $this->getSideAndUpperMenus('Activity', 'View', 'JobCard');
            $activityDetails = $this->_getActivityDetails($activityID);
        } else {
            $this->msg = $this->getMessage('ERR_INVENADJ_NODATA');
            $this->status = FALSE;

            return $this->JSONRespond();
        }

        $DateFormat = $this->convertUserDateFormatToPhpFormat() . ' H:i';
        if ($activityDetails[$activityID]['activityStartingTime'] && $activityDetails[$activityID]['activityStartingTime'] != '0000-00-00 00:00:00') {
            $startingTime = $this->getUserDateTime($activityDetails[$activityID]['activityStartingTime'], $DateFormat);
        }
        if ($activityDetails[$activityID]['activityEndTime'] && $activityDetails[$activityID]['activityEndTime'] != '0000-00-00 00:00:00') {
            $endTime = $this->getUserDateTime($activityDetails[$activityID]['activityEndTime'], $DateFormat);
        }

        $activityForm = new ActivityForm('');
        $locationID = $this->user_session->userActiveLocation['locationID'];
        $activityForm->get('activityID')->setValue($activityDetails[$activityID]['aCD'])->setAttribute('disabled', true);
        $activityForm->get('locationID')->setValue($locationID)->setAttribute('disabled', true);
        $activityForm->get('activityName')->setValue($activityDetails[$activityID]['aName'])->setAttribute('disabled', true);
        $activityForm->get('projectReferenceID')->setValue($activityDetails[$activityID]['projectCode'])->setAttribute('disabled', true);
        $activityForm->get('projectReferenceID')->setAttribute('data-projectid', $activityDetails[$activityID]['projectId']);
        $activityForm->get('jobReferenceID')->setValue($activityDetails[$activityID]['jRefNumber'])->setAttribute('disabled', true);
        $activityForm->get('activityTypeID')->setValue($activityDetails[$activityID]['aType'])->setAttribute('disabled', true);
        $activityForm->get('activityEstimateTime')->setValue($this->timeCalcutator($activityDetails[$activityID]['activityEstimateTime']))->setAttribute('disabled', true);
        $activityForm->get('startTime')->setValue($startingTime)->setAttribute('disabled', true);
        $activityForm->get('endTime')->setValue($endTime)->setAttribute('disabled', true);
        $activityForm->get('activityDescription')->setValue($activityDetails[$activityID]['aDescription'])->setAttribute('disabled', true);
        $activityForm->get('activityEstimateCost')->setValue($activityDetails[$activityID]['activityEstimatedCost'])->setAttribute('disabled', true);
        $activityForm->get('activityRepeatEnabled')->setValue($activityDetails[$activityID]['activityRepeatEnabled'])->setAttribute('disabled', true);
        $activityForm->get('activityRepeatComment')->setValue($activityDetails[$activityID]['activityRepeatComment'])->setAttribute('disabled', true);

        $this->getViewHelper('HeadLink')->appendStylesheet('/assets/image-uploader/css/delete-image.css');
        $this->getViewHelper('HeadScript')->prependFile('/js/job-card/activity-view.js');

        $jobRelatedSubViewFlag = false;
        if ($this->getRequest()->isPost()) {
            $jobRelatedSubViewFlag = true;
        }

        $adjustmentView = new ViewModel(array(
            'activityForm' => $activityForm,
            'activityID' => $activityID,
            'activityData' => $activityDetails[$activityID],
            'companyCurrencySymbol' => $this->companyCurrencySymbol,
            'jobRelatedSubViewFlag' => $jobRelatedSubViewFlag
                )
        );
        $adjustmentView->setTemplate('job-card/activity/view');
        if ($this->getRequest()->isPost()) {
            $adjustmentView->setTerminal(TRUE);
            $this->html = $adjustmentView;
            $this->status = TRUE;
            return $this->JSONRespondHtml();
        } else {
            return $adjustmentView;
        }
    }

    public function editAction()
    {
        $activityID = $this->params()->fromRoute('param1');

        if ($activityID) {
            $this->getSideAndUpperMenus('Activity', 'View', 'JobCard');
            $activityDetails = $this->_getActivityDetails($activityID);
        } else {
            $this->msg = $this->getMessage('ERR_INVENADJ_NODATA');
            $this->status = FALSE;
            return $this->JSONRespond();
        }

        $DateFormat = $this->convertUserDateFormatToPhpFormat() . ' H:i';
        $userdateFormat = $this->getUserDateFormat();
        $activityForm = new ActivityForm($userdateFormat);
        if ($activityDetails[$activityID]['activityStartingTime'] && $activityDetails[$activityID]['activityStartingTime'] != '0000-00-00 00:00:00') {
//            $startingTime = date($DateFormat, strtotime($activityDetails[$activityID]['activityStartingTime']));
            $startingTime = $this->getUserDateTime($activityDetails[$activityID]['activityStartingTime'], $DateFormat);
            $activityForm->get('startTime')->setValue($startingTime);
        }
        if ($activityDetails[$activityID]['activityEndTime'] && $activityDetails[$activityID]['activityEndTime'] != '0000-00-00 00:00:00') {
//            $endTime = date($DateFormat, strtotime($activityDetails[$activityID]['activityEndTime']));
            $endTime = $this->getUserDateTime($activityDetails[$activityID]['activityEndTime'], $DateFormat);
            $activityForm->get('endTime')->setValue($endTime);
        }
        $activityForm->get('activityID')->setValue($activityDetails[$activityID]['aCD'])->setAttribute('disabled', true);
        $activityForm->get('activityName')->setValue($activityDetails[$activityID]['aName']);
        $activityForm->get('projectReferenceID')->setValue($activityDetails[$activityID]['projectCode'])->setAttribute('disabled', true);
        $activityForm->get('jobReferenceID')->setValue($activityDetails[$activityID]['jRefNumber']);
        $activityForm->get('activityTypeID')->setValue($activityDetails[$activityID]['aType']);
        $activityForm->get('activityEstimateTime')->setValue($activityDetails[$activityID]['activityEstimateTime'])->setAttribute('disabled', true);
        $activityForm->get('activityEstimateCost')->setValue($activityDetails[$activityID]['activityEstimatedCost']);
        $activityForm->get('activityDescription')->setValue($activityDetails[$activityID]['aDescription']);
        $activityForm->get('activityRepeatEnabled')->setValue($activityDetails[$activityID]['activityRepeatEnabled']);
        $activityForm->get('activityRepeatComment')->setValue($activityDetails[$activityID]['activityRepeatComment']);

        $this->getViewHelper('HeadScript')->prependFile('/js/time-duration.js');
        $this->getViewHelper('HeadScript')->prependFile('/js/job-card/edit-activity.js');
        $this->getViewHelper('HeadScript')->prependFile('/js/job-card/contractor.js');
        $this->getServiceLocator()->get('viewhelpermanager')->get('jsvarsHelper')->setVars([
            'activityTypes', 'division', 'contractors', 'temporaryContractors', 'getEmployees',
            'rawMaterialProducts', 'fixedAssetsProducts', 'temporaryProducts', 'uomByID', 'jobReference',
            'costTypes', 'uomAbbrByProductID', 'getAllProject'
        ]);

        $this->getViewHelper('HeadScript')->prependFile('/js/jcrop/jquery.Jcrop.min.js');
        $this->getViewHelper('HeadScript')->prependFile('/js/jcrop/script.js');
        $this->getViewHelper('HeadScript')->prependFile('/js/jquery-ui.js');
        $this->getViewHelper('HeadLink')->prependStylesheet('/css/jcrop/jquery.Jcrop.css');
        $this->getViewHelper('HeadLink')->prependStylesheet('/css/jcrop/jquery.Jcrop.min.css');
        $this->getViewHelper('HeadLink')->prependStylesheet('/css/inventory/product.css');

        //      for  image upload
        $this->getViewHelper('HeadLink')->appendStylesheet('/assets/image-uploader/css/jquery.fileupload.css');
        $this->getViewHelper('HeadLink')->appendStylesheet('/assets/image-uploader/css/jquery.fileupload-ui.css');
        $this->getViewHelper('HeadLink')->appendStylesheet('/assets/image-uploader/css/delete-image.css');

        $this->getViewHelper('HeadScript')->appendFile('/assets/image-uploader/js/cors/jquery.postmessage-transport.js');
        $this->getViewHelper('HeadScript')->appendFile('/assets/image-uploader/js/cors/jquery.xdr-transport.js');
        $this->getViewHelper('HeadScript')->appendFile('/assets/image-uploader/js/vendor/jquery.ui.widget.js');
        $this->getViewHelper('HeadScript')->appendFile('/assets/image-uploader/js/load-image.all.min.js');
        $this->getViewHelper('HeadScript')->appendFile('/assets/image-uploader/js/jquery.iframe-transport.js');
        $this->getViewHelper('HeadScript')->appendFile('/assets/image-uploader/js/jquery.fileupload.js');
        $this->getViewHelper('HeadScript')->appendFile('/assets/image-uploader/js/jquery.fileupload-ui.js');
        $this->getViewHelper('HeadScript')->appendFile('/assets/image-uploader/js/tmpl.min.js');
        $this->getViewHelper('HeadScript')->appendFile('/assets/image-uploader/js/canvas-to-blob.min.js');
        $this->getViewHelper('HeadScript')->appendFile('/assets/image-uploader/js/jquery.blueimp-gallery.min.js');
        $this->getViewHelper('HeadScript')->appendFile('/assets/image-uploader/js/jquery.fileupload-process.js');
        $this->getViewHelper('HeadScript')->appendFile('/assets/image-uploader/js/jquery.fileupload-image.js');
        $this->getViewHelper('HeadScript')->appendFile('/assets/image-uploader/js/jquery.fileupload-validate.js');
        $this->getViewHelper('HeadScript')->appendFile('/js/job-card/temporary-product.js');

        $view = new ViewModel(array(
            'activityForm' => $activityForm,
            'activityID' => $activityID,
            'activityData' => $activityDetails[$activityID],
            'projectID' => $activityDetails[$activityID]['projectId'],
            'jobID' => $activityDetails[$activityID]['jobID'],
            'activityTypeID' => $activityDetails[$activityID]['aTypeID'],
            'companyCurrencySymbol' => $this->companyCurrencySymbol,
                )
        );
        $view->setTemplate('job-card/activity/edit');
        $locationID = $this->user_session->userActiveLocation['locationID'];
        $activityForm->get('locationID')->setValue($locationID);

        $divisions = $this->CommonTable('JobCard/Model/DivisionTable')->getAllDivisions();
        $designation = $this->CommonTable('JobCard/Model/DesignationTable')->getAllDesignations();
        foreach ($designation as $a) {
            $destinationData[$a['designationID']] = $a['designationName'];
        }
        foreach ($divisions as $b) {
            $divisionsData[$b['divisionID']] = $b['divisionName'];
        }

        $contractorForm = new ContractorForm(array(
            'division' => $divisionsData,
            'designation' => $destinationData,
        ));
        $contractorAddView = new ViewModel(array('contractorForm' => $contractorForm));
        $contractorAddView->setTemplate('job-card/contractor/contractor-add-modal');
        $view->addChild($contractorAddView, 'contractorAddView');

        $tempContractorForm = new TemporaryContractorForm(array(
            'division' => $divisionsData,
            'designation' => $destinationData,
                )
        );
        $tempContractorAddView = new ViewModel(array('tempContractorForm' => $tempContractorForm));
        $tempContractorAddView->setTemplate('job-card/temporary-contractor/temp-contractor-add-modal');
        $view->addChild($tempContractorAddView, 'tempContractorAddView');

        $uomList = $this->CommonTable('Settings\Model\uomTable')->activeFetchAll();
        $tempData = array(
            'tempQty' => 1,
        );
        $temporaryProductForm = new TemporaryProductForm($tempData);
        $temporaryProductAddView = new ViewModel(array('tempProductForm' => $temporaryProductForm, 'uomList' => $uomList));
        $temporaryProductAddView->setTemplate('job-card/temporary-product/temp-product-add-modal');
        $view->addChild($temporaryProductAddView, 'tempProductAddView');

        $relatedDocumentsJobView = new ViewModel();
        $relatedDocumentsJobView->setTemplate('/job-card/job/job_document_view');
        $view->addChild($relatedDocumentsJobView, 'relatedJobDocu');

        $relatedDocuments = new ViewModel();
        $relatedDocuments->setTemplate('/job-card/job/related_doc');
        $view->addChild($relatedDocuments, 'relatedDocu');

        return $view;
    }

    private function _getActivityDetails($activityID)
    {
        $activityTblResult = $this->CommonTable('JobCard/Model/ActivityTable')->getActivityDetailsByActivityID($activityID);
        $activityDetails = array();
        //activity details
        if (isset($activityTblResult)) {
            foreach ($activityTblResult as $a) {
                $tempData = array();
                $tempData['aID'] = $a['activityId'];
                $tempData['aCD'] = $a['activityCode'];
                $tempData['aName'] = $a['activityName'];
                $tempData['jRefNumber'] = $a['jobReferenceNumber'];
                $tempData['projectId'] = $a['projectId'];
                $tempData['projectCode'] = $a['projectCode'];
                $tempData['jobID'] = $a['jobId'];
                $tempData['aTypeID'] = $a['activityTypeID'];
                $tempData['aType'] = $a['activityTypeName'];
                $tempData['activityStartingTime'] = $a['activityStartingTime'];
                $tempData['activityEndTime'] = $a['activityEndTime'];
                $tempData['activityEstimateTime'] = $a['activityEstimateTime'];
                $tempData['aDescription'] = $a['activityDescription'];
                $tempData['activityEstimatedCost'] = number_format($a['activityEstimatedCost'], 2);
                $tempData['activityRepeatEnabled'] = $a['activityRepeatEnabled'];
                $tempData['activityRepeatComment'] = $a['activityRepeatComment'];
                $tempData['cusName'] = $a['customerName'];
                $activityDetails[$a['activityId']] = $tempData;
            }
        }

        //contractor data put  into activity details array
        $activityContractorResult = $this->CommonTable('JobCard/Model/ActivityTable')->getActivityContractorDetailsByActivityID($activityID);
        if (isset($activityContractorResult)) {
            foreach ($activityContractorResult as $c) {
                if ($c['contractorId'] != NULL) {
                    $tempData = array();
                    $tempData['cFN'] = $c['contractorFirstName'];
                    $tempData['cSN'] = $c['contractorSecondName'];
                    $contractor['contractors'][$c['contractorId']] = $tempData;
                    $activityDetails[$c['activityId']] = array_merge($activityDetails[$c['activityId']], $contractor);
                }
            }
        }
        //temporay contractor put  data into activity details array
        $activityTempContractorResult = $this->CommonTable('JobCard/Model/ActivityTable')->getActivityTempContractorDetailsByActivityID($activityID);
        if (isset($activityTempContractorResult)) {
            foreach ($activityTempContractorResult as $c) {
                if ($c['contractorID'] != NULL) {
                    $tempData = array();
                    $tempData['cTFN'] = $c['temporaryContractorFirstName'];
                    $tempData['cTSN'] = $c['temporaryContractorSecondName'];
                    $tempContractor['tempContractors'][$c['contractorID']] = $tempData;
                    $activityDetails[$c['activityId']] = array_merge($activityDetails[$c['activityId']], $tempContractor);
                }
            }
        }
        //owner data put  into activity details array
        $activityOwnerResult = $this->CommonTable('JobCard/Model/ActivityTable')->getActivityOwnerDetailsByActivityID($activityID);
        if (isset($activityOwnerResult)) {
            foreach ($activityOwnerResult as $e) {
                if ($e['employeeID'] != NULL) {
                    $tempData = array();
                    $tempData['oFN'] = $e['employeeFirstName'];
                    $tempData['oTSN'] = $e['employeeSecondName'];
                    $employee['owners'][$e['employeeID']] = $tempData;
                    $activityDetails[$e['activityId']] = array_merge($activityDetails[$e['activityId']], $employee);
                }
            }
        }
        //supervisor data put  into activity details array
        $activitySupervisorResult = $this->CommonTable('JobCard/Model/ActivityTable')->getActivitySupervisorDetailsByActivityID($activityID);
        if (isset($activitySupervisorResult)) {
            foreach ($activitySupervisorResult as $e) {
                if ($e['employeeID'] != NULL) {
                    $tempData = array();
                    $tempData['sFN'] = $e['employeeFirstName'];
                    $tempData['sTSN'] = $e['employeeSecondName'];
                    $supervisors['supervisors'][$e['employeeID']] = $tempData;
                    $activityDetails[$e['activityId']] = array_merge($activityDetails[$e['activityId']], $supervisors);
                }
            }
        }
        //raw material data puts  into activity details array
        $activityRawMResult = $this->CommonTable('JobCard/Model/ActivityTable')->getActivityRawMDetailsByActivityID($activityID);
        if (isset($activityRawMResult)) {

            foreach ($activityRawMResult as $a) {
                $uomList = $this->CommonTable('Inventory\model\ProductUomTable')->getProductUomByProductID($a['rMProductID']);
                $uomIdList = array();
                foreach ($uomList as $uList) {
                    $uomIdList[] = $uList;
                }
                $rowMaterialQty = $this->getProductQuantityViaDisplayUom($a['activityRawMaterialQuantity'], $uomIdList);

                $rowMaterialPrice = $this->getProductUnitPriceViaDisplayUom($a['activityRawMaterialCost'], $uomIdList);
                $rawMData = array();
                $rawMaterialProducts = (isset($activityDetails[$a['activityId']]['rMProducts'])) ? $activityDetails[$a['activityId']]['rMProducts'] : array();

                if ($a['rMProductID'] != NULL) {

                    $rMBatches = (isset($rawMaterialProducts[$a['rMProductCode']]['bP'])) ? $rawMaterialProducts[$a['rMProductCode']]['bP'] : array();
                    $rMSerials = (isset($rawMaterialProducts[$a['rMProductCode']]['sP'])) ? $rawMaterialProducts[$a['rMProductCode']]['sP'] : array();
                    $rMProduct = (isset($rawMaterialProducts[$a['rMProductCode']]['pP'])) ? $rawMaterialProducts[$a['rMProductCode']]['pP'] : array();

                    $rawMaterialProducts[$a['rMProductCode']] = array('aPID' => $a['rMProductID'], 'aPC' => $a['rMProductCode'], 'aPN' => $a['rMProductName'], 'aPQ' => $rowMaterialQty['quantity'], 'aPUom' => $rowMaterialQty['uomAbbr'], 'uPrice' => $rowMaterialPrice);

                    if ($a['productBatchID'] != NULL && $a['productSerialID'] == NULL) {
                        $rMBatches[$a['productBatchID']] = array('bID' => $a['productBatchID'], 'bC' => $a['rMBatchCode'], 'bQ' => $a['activityRawMaterialQuantity']);
                    }

                    if ($a['productSerialID']) {
                        $rMSerials[$a['productSerialID']] = array('sID' => $a['productSerialID'], 'bID' => $a['productBatchID'], 'sC' => $a['rMSerialCode'], 'bC' => $a['rMBatchCode']);
                    }

                    if ($a['productBatchID'] == NULL && $a['productSerialID'] == NULL) {
                        $rMProduct[$a['rMLPID']] = array('pC' => $a['rMProductCode']);
                    }

                    $rawMaterialProducts[$a['rMProductCode']]['bP'] = $rMBatches;
                    $rawMaterialProducts[$a['rMProductCode']]['sP'] = $rMSerials;
                    $rawMaterialProducts[$a['rMProductCode']]['pP'] = $rMProduct;
                }
                $rawMData['rMProducts'] = $rawMaterialProducts;
                $activityDetails[$a['activityId']] = array_merge($activityDetails[$a['activityId']], $rawMData);
            }
        }

        //fixed assets data puts  into activity details array
        $activityFixedAResult = $this->CommonTable('JobCard/Model/ActivityTable')->getActivityFixedAssetDetailsByActivityID($activityID);
        if (isset($activityFixedAResult)) {

            foreach ($activityFixedAResult as $a) {
                $uomList = $this->CommonTable('Inventory\model\ProductUomTable')->getProductUomByProductID($a['fAProductID']);
                $uomIdList = array();
                foreach ($uomList as $uList) {
                    $uomIdList[] = $uList;
                }
                $fixedAssetQty = $this->getProductQuantityViaDisplayUom($a['activityFixedAssetQuantity'], $uomIdList);

                $fixedAssetPrice = $this->getProductUnitPriceViaDisplayUom($a['activityFixedAssetCost'], $uomIdList);

                $tempData = array();
                $fixedAssetsProducts = (isset($activityDetails[$a['activityId']]['fAProducts'])) ? $activityDetails[$a['activityId']]['fAProducts'] : array();

                if ($a['fAProductID'] != NULL) {

                    $fABatches = (isset($fixedAssetsProducts[$a['fAProductCode']]['bP'])) ? $fixedAssetsProducts[$a['fAProductCode']]['bP'] : array();
                    $fASerials = (isset($fixedAssetsProducts[$a['fAProductCode']]['sP'])) ? $fixedAssetsProducts[$a['fAProductCode']]['sP'] : array();
                    $fAProduct = (isset($fixedAssetsProducts[$a['fAProductCode']]['pP'])) ? $fixedAssetsProducts[$a['fAProductCode']]['pP'] : array();

                    $fixedAssetsProducts[$a['fAProductCode']] = array('aPID' => $a['fAProductID'], 'aPC' => $a['fAProductCode'], 'aPN' => $a['fAProductName'], 'aPQ' => $fixedAssetQty['quantity'], 'aPUom' => $fixedAssetQty['uomAbbr'], 'uPrice' => $fixedAssetPrice);

                    if ($a['fABatchID'] != NULL && $a['fASerialID'] == NULL) {
                        $fABatches[$a['fABatchID']] = array('bID' => $a['fABatchID'], 'bC' => $a['fABatchCode'], 'bQ' => $a['activityFixedAssetSubProductQuantity']);
                    }

                    if ($a['fASerialID']) {
                        $fASerials[$a['fASerialID']] = array('sC' => $a['fASerialCode'], 'bC' => $a['fABatchCode'], 'bID' => $a['fABatchID'], 'sID' => $a['fASerialID']);
                    }

                    if ($a['fABatchID'] == NULL && $a['fASerialID'] == NULL) {
                        $fAProduct[$a['fALPID']] = array('pC' => $a['fAProductCode']);
                    }

                    $fixedAssetsProducts[$a['fAProductCode']]['bP'] = $fABatches;
                    $fixedAssetsProducts[$a['fAProductCode']]['sP'] = $fASerials;
                    $fixedAssetsProducts[$a['fAProductCode']]['pP'] = $fAProduct;
                }

                $tempData['fAProducts'] = $fixedAssetsProducts;
                $activityDetails[$a['activityId']] = array_merge($activityDetails[$a['activityId']], $tempData);
            }
        }
        //puts temporay products
        $activityTempProductResult = $this->CommonTable('JobCard\Model\ActivityTemporaryProductTable')->getActivityTempProdcutDetailsByActivityID($activityID);
        if (isset($activityTempProductResult)) {

            foreach ($activityTempProductResult as $p) {
                $tempData = array();
                $activityTempProducts = (isset($activityDetails[$a['activityId']]['tempProducts'])) ? $activityDetails[$a['activityId']]['tempProducts'] : array();

                if ($p['temporaryProductID'] != NULL) {

                    $activityTempProducts[$p['temporaryProductID']] = array('aPID' => $p['temporaryProductID'], 'tPC' => $p['temporaryProductCode'], 'tPN' => $p['temporaryProductName'], 'tPQ' => $p['temporaryProductQuantity'], 'aPUom' => $p['uomAbbr'], 'uomID' => $p['uomID'], 'bCode' => $p['temporaryProductBatch'], 'sCode' => $p['temporaryProductSerial']);
                }

                $tempData['tempProducts'] = $activityTempProducts;
                $activityDetails[$p['activityId']] = array_merge($activityDetails[$p['activityId']], $tempData);
            }
        }
        //cost type data  put into activity details array
        $activityCostTypeResult = $this->CommonTable('JobCard/Model/ActivityTable')->getActivityCostTypeDetailsByActivityID($activityID);
        if (isset($activityCostTypeResult)) {
            foreach ($activityCostTypeResult as $t) {
                if ($t['costTypeId'] != NULL) {
                    $tempData = array();
                    $tempData['cTID'] = $t['costTypeId'];
                    $tempData['cTN'] = $t['costTypeName'];
                    $tempData['cTEstimateCost'] = number_format($t['activityCostTypeEstimatedCost'], 2) ;
                    $tempData['cTActualCost'] = number_format($t['activityCostTypeActualCost'], 2) ;
                    $costType['costTypes'][$t['costTypeId']] = $tempData;
                    $activityDetails[$t['activityId']] = array_merge($activityDetails[$t['activityId']], $costType);
                }
            }
        }

        //get activity vehicle details
        $activityVehicles = $this->CommonTable('JobCard/Model/ActivityVehicleTable')->getActivityVehiclesByActivityId($activityID);
        $vehicleList = [];
        foreach ($activityVehicles as $vehicle) {
            $data = array();
            $data['vehicleId'] = $vehicle['vehicleId'];
            $data['registrationNumber'] = $vehicle['vehicleRegistrationNumber'];
            $data['startMileage'] = $vehicle['activityVehicleStartMileage'];
            $data['endMileage'] = $vehicle['activityVehicleEndMileage'];
            $data['distance'] = ($vehicle['activityVehicleEndMileage'] - $vehicle['activityVehicleStartMileage']);
            $data['cost'] = $vehicle['activityVehicleCost'];
            $vehicleList[] = $data;
        }
        $activityDetails[$vehicle['activityId']]['vehicle'] = $vehicleList;
        return $activityDetails;
    }

    protected function getPaginatedActivity($locationID, $perPage = 6)
    {
        $this->paginator = $this->CommonTable('JobCard\Model\ActivityTable')->fechAll($locationID, true);
        $this->paginator->setCurrentPageNumber((int) $this->params()->fromRoute('param1', 1));
        $this->paginator->setItemCountPerPage($perPage);
        $this->setLogMessage('Activity Type list accessed');
        return $this->paginator;
    }

}
