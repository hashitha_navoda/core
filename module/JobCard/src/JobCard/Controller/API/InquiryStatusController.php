<?php

/**
 * @author Sandun <sandun@thinkcube.com>
 * This file contains Inquiry Status related Api controller functions
 */

namespace JobCard\Controller\API;

use Zend\View\Model\ViewModel;
use Core\Controller\CoreController;
use JobCard\Model\InquiryStatus;

class InquiryStatusController extends CoreController
{

    /**
     * @author Sandun <sandun@thinkcube.com>
     * inquiry status save or update function
     * @return JSONRespond
     */
    public function addAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $this->beginTransaction();
            $inquiryStatusName = $request->getPost('inquiryStatusName');
            $inquiryStatusTypeID = $request->getPost('inquiryStatusTypeID');
            $editMode = $request->getPost('editMode');

            //check wheter editMode is true or false.
            //if it is true then update the inquiry status otherwise save the inquiry status
            if ($editMode == 'true') {
                $inquiryStatusID = $request->getPost('inquiryStatusID');
                $data = array(
                    'inquiryStatusName' => $inquiryStatusName,
                    'inquiryStatusTypeID' => $inquiryStatusTypeID,
                    'inquiryStatusID' => $inquiryStatusID,
                );

                $inquiryStatusData = new InquiryStatus();
                $inquiryStatusData->exchangeArray($data);
                $updateInquiryStatus = $this->CommonTable('JobCard\Model\InquiryStatusTable')->updateInquiryStatus($inquiryStatusData);
                if ($updateInquiryStatus) {
                    $this->status = true;
                    $this->data = array('inquiryStatusID' => $inquiryStatusID);
                    $this->msg = $this->getMessage('SUCC_INQUSTATUS_UPDATE', array($inquiryStatusName));
                    $this->flashMessenger()->addMessage($this->msg);
                    $this->commit();
                } else {
                    $this->status = false;
                    $this->msg = $this->getMessage('ERR_INQUSTATUS_UPDATE');
                    $this->rollback();
                }
            } else {
                //check whether inquery status name already in database or not.
                $updateInquiryStatus = $this->CommonTable('JobCard\Model\InquiryStatusTable')->getInquiryStatusByInquiryStatusName($inquiryStatusName);
                if (!($updateInquiryStatus->count() > 0)) {
                    $entityID = $this->createEntity();
                    $data = array(
                        'inquiryStatusName' => $inquiryStatusName,
                        'inquiryStatusTypeID' => $inquiryStatusTypeID,
                        'entityID' => $entityID
                    );

                    $inquiryStatusData = new InquiryStatus();
                    $inquiryStatusData->exchangeArray($data);
                    $inquiryStatusID = $this->CommonTable('JobCard\Model\InquiryStatusTable')->saveInquiryStatus($inquiryStatusData);
                    if ($inquiryStatusID) {
                        $this->status = true;
                        $this->data = array('inquiryStatusID' => $inquiryStatusID);
                        $this->msg = $this->getMessage('SUCC_INQUSTATUS_SAVE');
                        $this->flashMessenger()->addMessage($this->msg);
                        $this->commit();
                    } else {
                        $this->status = false;
                        $this->msg = $this->getMessage('ERR_INQUSTATUS_SAVE');
                        $this->rollback();
                    }
                } else {
                    $this->status = false;
                    $this->msg = $this->getMessage('ERR_INQUSTATUS_NAME_ALREDY_EXIT');
                }
            }
            return $this->JSONRespond();
        }
    }

    /**
     * @author Sandun <sandun@thinkcube.com>
     * get project types by inquiry type search key
     * @return JSONRespondHtml
     */
    public function getInquiryStatusBySearchKeyAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $paginated = false;
            $inquiryStatusSearchKey = $request->getPost('inquiryStatusSearchKey');
            $tbl = $this->CommonTable('JobCard\Model\InquiryStatusTable');
            $inquiryStatusList = NULL;

            if (!empty($inquiryStatusSearchKey)) {
                $inquiryStatusList = $tbl->getInquiryStatusforSearch($inquiryStatusSearchKey);
            } else {
                $paginated = true;
                $inquiryStatusList = $this->getPaginatedInquiryStatus();
                $paginated = true;
            }

            $view = new ViewModel(array(
                'inquiryStatusList' => $inquiryStatusList,
                'paginated' => $paginated
            ));

            $view->setTerminal(true);
            $view->setTemplate('job-card/inquiry-status/list.phtml');

            $this->status = true;
            $this->html = $view;
            return $this->JSONRespondHtml();
        }
    }

    /**
     * @author Sandun <sandun@thinkcube.com>
     * delete inquiry status by inquiry type id
     * @return JSONRespond
     */
    public function deleteInquiryStatusByInquiryStatusIDAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $inquiryStatusID = $request->getPost('inquiryStatusID');
            $inquiryStatusName = $request->getPost('inquiryStatusName');
            $inquiryLogData = $this->CommonTable('JobCard/Model/InquiryLogTable')->getInquiryLogDetailsByInquiryStatusID($inquiryStatusID)->current();
            if (!$inquiryLogData) {

                if (!empty($inquiryStatusID)) {
                    $result = $this->CommonTable('JobCard\Model\InquiryStatusTable')->getInquiryStatusByInquiryStatusID($inquiryStatusID)->current();
                    $entityID = $result['entityID'];
                    $this->updateDeleteInfoEntity($entityID);
                    $this->status = true;
                    $this->msg = $this->getMessage('SUCC_INQUSTATUS_DELETE', array($inquiryStatusName));
                    $this->flashMessenger()->addMessage($this->msg);
                } else {
                    $this->status = false;
                    $this->msg = $this->getMessage('ERR_INQUSTATUS_DELETE');
                }
            } else {
                $this->status = false;
                $this->data = 'deactive';
//                $this->msg = $this->getMessage('ERR_INQUSTATUS_ALREADY_USED');
            }
            return $this->JSONRespond();
        }
    }

    /**
     * @author Sandun <sandun@thinkcube.com>
     * pageinator list of inquiry status
     * @param int $perPage
     * @return paginator
     */
    protected function getPaginatedInquiryStatus($perPage = 6)
    {
        $this->paginator = $this->CommonTable('JobCard\Model\InquiryStatusTable')->fetchAll(true);
        $this->paginator->setCurrentPageNumber((int) $this->params()->fromRoute('param1', 1));
        $this->paginator->setItemCountPerPage($perPage);
        $this->setLogMessage('Inquiry Status list accessed');

        return $this->paginator;
    }

    /**
     * @author Lahiru Abeysinghe <lahiru@thinkcube.com>
     * get all Inquiry status that related to given search Key
     * @return JSONRespond
     */
    public function searchInquiryStatusForDropdownAction()
    {
        $searchrequest = $this->getRequest();
        if ($searchrequest->isPost()) {
            $searchKey = $searchrequest->getPost('searchKey');
            $inquiryStatus = $this->CommonTable('JobCard/Model/InquiryStatusTable')->getInquiryStatusforSearch($searchKey);
            $inquiryStatusList = array();
            foreach ($inquiryStatus as $iS) {
                $temp['value'] = $iS['inquiryStatusID'];
                $temp['text'] = $iS['inquiryStatusName'];
                $inquiryStatusList[] = $temp;
            }
            $this->data = array('list' => $inquiryStatusList);
            return $this->JSONRespond();
        }
    }

    /**
     * @author Lahiru Abeysinghe <lahiru@thinkcube.com>
     * use to change InquiryStatus active state.
     */
    public function changeStatusIDAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $inquiryStatusID = $request->getPost('inquiryStatusID');
            $status = $request->getPost('status');
            $updateState = $this->CommonTable('JobCard/Model/InquiryStatusTable')->updateInquiryStatusState($inquiryStatusID, $status);
            if ($updateState) {
                $this->status = true;
                $this->msg = $this->getMessage('SUCC_INQ_STATUS_STATUS_UPDATE');
                return $this->JSONRespond();
            }
        }
    }

}
