<?php

namespace JobCard\Controller\API;

use Zend\View\Model\ViewModel;
use Core\Controller\CoreController;
use JobCard\Model\TemporaryProduct;

class TemporaryProductController extends CoreController
{

    public function saveAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $temporaryProductCode = $request->getPost('temporaryProductCode');
            $temporaryProductName = $request->getPost('temporaryProductName');
            $temporaryProductDescription = $request->getPost('temporaryProductDescription');
            $temporaryProductQuantity = $request->getPost('temporaryProductQuantity');
            $temporaryProductPrice = $request->getPost('temporaryProductPrice');
            $uomID = $request->getPost('uomID');
            $batchProduct = $request->getPost('batchProduct');
            $serialProduct = $request->getPost('serialProduct');
            $jobID = $request->getPost('jobID');
            $projectID = $request->getPost('projectID');
            $temporaryProductImages = $request->getPost('temporaryProductImages');
            $checkFlag = true;
            if ($projectID) {
                $tempData = $this->CommonTable('JobCard/Model/TemporaryProductTable')->getTemporaryProductsByProjectID($projectID);
            } else {
                $tempData = $this->CommonTable('JobCard/Model/TemporaryProductTable')->getTemporaryProductsByJobID($jobID);
            }
            foreach ($tempData as $data) {
                if ($data['temporaryProductCode'] == $temporaryProductCode) {
                    $checkFlag = false;
                }
            }
            if ($checkFlag) {
                $entityID = $this->createEntity();
                $data = array(
                    'temporaryProductCode' => $temporaryProductCode,
                    'temporaryProductName' => $temporaryProductName,
                    'temporaryProductDescription' => $temporaryProductDescription,
                    'temporaryProductQuantity' => $temporaryProductQuantity,
                    'temporaryProductPrice' => $temporaryProductPrice,
                    'uomID' => $uomID,
                    'temporaryProductImageURL' => NULL,
                    'temporaryProductSerial' => $serialProduct,
                    'temporaryProductBatch' => $batchProduct,
                    'entityID' => $entityID,
                );
                $tempProductData = new TemporaryProduct();
                $tempProductData->exchangeArray($data);
                $tempProductID = $this->CommonTable('JobCard/Model/TemporaryProductTable')->saveTemporaryProduct($tempProductData);

                if ($tempProductID) {
                    if ($temporaryProductImages) {
                        foreach ($temporaryProductImages as $pImage) {
                            //  get entity sub type ID
                            $entitySubType = $this->getServiceLocator()->get('config')['entitySubType'];
                            $entitySubTypeID = array_search('image', $entitySubType);
                            $isSaved = $this->saveAttachment($pImage['imgName'], $pImage['imgFolderPath'], $pImage['imgSize'], $entityID, $entitySubTypeID);
                            if ($isSaved) {
                                //  make folder if not exists
                                $upload_dir = $this->getImageAttachmentsDirectoryPath() . $pImage['imgFolderPath'];
                                if (!is_dir($upload_dir)) {
                                    mkdir($upload_dir, 0777, true);
                                }
                                // copy phsyical files to attachments folder
                                copy($this->getTemporaryAttachmentDirectoryPath() . $pImage['imgFolderPath'] . $pImage['imgName'], $upload_dir . $pImage['imgName']);
                            }
                        }
                    }

                    $this->status = true;
                    $this->data = $tempProductID;
                    $this->msg = $this->getMessage('SUCC_TEMP_PRO_SAVE');
                    $this->flashMessenger()->addMessage($this->msg);
                    $this->commit();
                } else {
                    $this->status = false;
                    $this->msg = $this->getMessage('ERR_TEMP_PRO_SAVE');
                    $this->rollback();
                }
            } else {
                $this->status = false;
                $this->msg = $this->getMessage('ERR_TEMP_PRO_NAME_ALREDY_EXIST');
            }
            return $this->JSONRespond();
        }
    }

    public function getTemporaryProductsAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $productsList = $this->CommonTable('JobCard\Model\TemporaryProductTable')->getTemporaryProductDetails();
            $products = array();
            foreach ($productsList as $row) {
                $tempP = array();
                $tempP['pC'] = $row['temporaryProductCode'];
                $tempP['pN'] = $row['temporaryProductName'];
                $tempP['bP'] = $row['temporaryProductBatch'];
                $tempP['sP'] = $row['temporaryProductSerial'];
                $tempP['price'] = $row['price'];

                $uom = (isset($products[$row['temporaryProductID']]['uom'])) ? $products[$row['temporaryProductID']]['uom'] : array();
                if ($row['uomID'] != NULL) {
                    $uom[$row['uomID']] = array('uA' => $row['uomAbbr'], 'uN' => $row['uomName'], 'uDP' => $row['uomDecimalPlace'], 'uS' => $row['uomState'], 'uC' => $row['productUomConversion'], 'pUI' => $row['productUomID'], 'uomID' => $row['uomID']);
                }

                $batchSerial = (isset($products[$row['temporaryProductID']]['batchSerial'])) ? $products[$row['temporaryProductID']]['batchSerial'] : array();
                $batchIDsInBatchSerial = (isset($products[$row['temporaryProductID']]['productIDs'])) ? $products[$row['temporaryProductID']]['productIDs'] : array();
                if ($row['serialActivityBatchID'] != NULL && $row['serialActivityBatchID'] == $row['activityBatchID'] && $row['productSerialSold'] != 1) {
                    $batchSerial[$row['activitySerialID']] = array('PBID' => $row['serialActivityBatchID'], 'PBC' => $row['activityBatchCode'], 'PSID' => $row['activitySerialID'], 'PSC' => $row['activitySerialCode'], 'PSS' => $row['activitySerialSold']);
                    if ($row['activityBatchID'] != NULL) {
                        $batchIDsInBatchSerial[$row['activityBatchID']] = array('PBID' => $row['activityBatchID']);
                    }
                }

                $batch = (isset($products[$row['temporaryProductID']]['batch'])) ? $products[$row['temporaryProductID']]['batch'] : array();
                if ($row['activityBatchID'] != NULL && $row['serialActivityBatchID'] != $row['activityBatchID']) {
                    $batch[$row['activityBatchID']] = array('PBID' => $row['activityBatchID'], 'PBC' => $row['activityBatchCode'], 'PBQ' => $row['activityBatchQuantity'], 'PSID' => $row['activitySerialID']);
                }

                $serial = (isset($products[$row['temporaryProductID']]['serial'])) ? $products[$row['temporaryProductID']]['serial'] : array();
                if ($row['activitySerialID'] != NULL && $row['serialActivityBatchID'] == NULL && $row['activitySerialSold'] != 1) {

                    $serial[$row['activitySerialID']] = array('PBID' => $row['serialActivityBatchID'], 'PBC' => $row['activityBatchCode'], 'PSID' => $row['activitySerialID'], 'PSC' => $row['activitySerialCode'], 'PSS' => $row['activitySerialSold'],);
                }

                $tempP['uom'] = $uom;
                $tempP['batch'] = $batch;
                $tempP['serial'] = $serial;
                $tempP['batchSerial'] = $batchSerial;
                $tempP['productIDs'] = $batchIDsInBatchSerial;
                $products[$row['temporaryProductID']] = $tempP;
            }
            $this->data = $products;
            return $this->JSONRespond();
        }
    }

    /**
     * @author Ashan <ashan@thinkcube.com>
     * get temp products related to search key
     */
    public function getTemporaryProductsBySearchKeyAction()
    {
        $searchrequest = $this->getRequest();
        if ($searchrequest->isPost()) {
            $tempProductSearchKey = $searchrequest->getPost('searchKey');
            $jobID = $searchrequest->getPost('addFlag');

            $tempProductList = array();
            if ($jobID) {
                //get job details form job id
                $jobs = $this->CommonTable('JobCard/Model/JobTable')->getJobDetails($jobID)->current();
                $projectID = $jobs['projectId'];

                if ($projectID) {
                    $tempData = $this->CommonTable('JobCard/Model/TemporaryProductTable')->searchTemporaryProductsBySearchKeyWithProjectID($tempProductSearchKey, $projectID);
                } else {
                    $tempData = $this->CommonTable('JobCard/Model/TemporaryProductTable')->searchTemporaryProductsBySearchKeyWithJobID($tempProductSearchKey, $jobID);
                }

                foreach ($tempData as $data) {
                    $temp['value'] = $data['temporaryProductID'];
                    $temp['text'] = $data['temporaryProductCode'] . ' - ' . $data['temporaryProductSerial'];
                    $tempProductList[] = $temp;
                }
            } else {

                $tempProductData = $this->CommonTable('JobCard/Model/TemporaryProductTable')->getTemporaryProductBySearch($tempProductSearchKey);

                foreach ($tempProductData as $data) {
                    $temp['value'] = $data['temporaryProductID'];
                    $temp['text'] = $data['temporaryProductCode'] . ' - ' . $data['temporaryProductName'];
                    $tempProductList[] = $temp;
                }
            }

            $this->data = array('list' => $tempProductList);
            return $this->JSONRespond();
        }
    }

}
