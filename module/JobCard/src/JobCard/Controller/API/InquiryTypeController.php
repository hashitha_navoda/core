<?php

/**
 * @author Sandun <sandun@thinkcube.com>
 * This file contains Inquiry Type related Api controller functions
 */

namespace JobCard\Controller\API;

use Zend\View\Model\ViewModel;
use Core\Controller\CoreController;
use JobCard\Model\InquiryType;

class InquiryTypeController extends CoreController
{

    /**
     * @author Sandun <sandun@thinkcube.com>
     * inquiry type save and update function
     * @return JSONRespond
     */
    public function addAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $this->beginTransaction();

            $inquiryTypeCode = $request->getPost('inquiryTypeCode');
            $inquiryTypeName = $request->getPost('inquiryTypeName');
            $editMode = $request->getPost('editMode');

            //check wheter editMode is true or false.
            //if it is true then update the inquiry type otherwise save the inquiry type
            if ($editMode == 'true') {
                $inquiryTypeID = $request->getPost('inquiryTypeID');
                $data = array(
                    'inquiryTypeName' => $inquiryTypeName,
                    'inquiryTypeID' => $inquiryTypeID,
                );

                $inquiryTypeData = new InquiryType();
                $inquiryTypeData->exchangeArray($data);
                $updateInquiry = $this->CommonTable('JobCard\Model\InquiryTypeTable')->updateInquiryType($inquiryTypeData);
                if ($updateInquiry) {
                    $this->status = true;
                    $this->data = array('inquiryTypeID' => $inquiryTypeID);
                    $this->msg = $this->getMessage('SUCC_INQUTYPE_UPDATE', array($inquiryTypeCode));
                    $this->flashMessenger()->addMessage($this->msg);
                    $this->commit();
                } else {
                    $this->status = false;
                    $this->msg = $this->getMessage('ERR_INQUTYPE_UPDATE');
                    $this->rollback();
                }
            } else {
                //check whether project type code alredy in database or not.
                $updateInquiry = $this->CommonTable('JobCard\Model\InquiryTypeTable')->getInquiryTypeByInquiryTypeCode($inquiryTypeCode);
                if (!($updateInquiry->count() > 0)) {
                    $entityID = $this->createEntity();
                    $data = array(
                        'inquiryTypeCode' => $inquiryTypeCode,
                        'inquiryTypeName' => $inquiryTypeName,
                        'entityID' => $entityID
                    );

                    $inquiryTypeData = new InquiryType();
                    $inquiryTypeData->exchangeArray($data);
                    $inquiryTypeID = $this->CommonTable('JobCard\Model\InquiryTypeTable')->saveInquiryType($inquiryTypeData);
                    if ($inquiryTypeID) {
                        $this->status = true;
                        $this->data = array('inquiryTypeID' => $inquiryTypeID);
                        $this->msg = $this->getMessage('SUCC_INQUTYPE_SAVE');
                        $this->flashMessenger()->addMessage($this->msg);
                        $this->commit();
                    } else {
                        $this->status = false;
                        $this->msg = $this->getMessage('ERR_INQUTYPE_SAVE');
                        $this->rollback();
                    }
                } else {
                    $this->status = false;
                    $this->data = 'PTCERR';
                    $this->msg = $this->getMessage('ERR_INQUTYPE_CODE_ALREDY_EXIT');
                }
            }
            return $this->JSONRespond();
        }
    }

    /**
     * @author Sandun <sandun@thinkcube.com>
     * get project types by inquiry type search key
     * @return JSONRespondHtml
     */
    public function getInquiryTypesBySearchKeyAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $paginated = false;
            $inquiryTypeSearchKey = $request->getPost('inquiryTypeSearchKey');

            if (!empty($inquiryTypeSearchKey)) {
                $tbl = $this->CommonTable('JobCard\Model\InquiryTypeTable');
                $inquiryTypeList = $tbl->getInquiryTypeforSearch($inquiryTypeSearchKey);
                $this->status = true;
            } else {
                $this->status = true;
                $inquiryTypeList = $this->getPaginatedInquiryTypes();
                $paginated = true;
            }

            $view = new ViewModel(array(
                'inquiryTypeList' => $inquiryTypeList,
                'paginated' => $paginated
            ));

            $view->setTerminal(true);
            $view->setTemplate('job-card/inquiry-type/list.phtml');

            $this->html = $view;
            return $this->JSONRespondHtml();
        }
    }

    /**
     * @author Sandun <sandun@thinkcube.com>
     * delete inquiry types by inquiry type id
     * @return JSONRespond
     */
    public function deleteInquiryTypeByInquiryTypeIDAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $inquiryTypeID = $request->getPost('inquiryTypeID');
            $inquiryTypeCode = $request->getPost('inquiryTypeCode');
            $inquiryLogData = $this->CommonTable('JobCard/Model/InquiryLogTable')->getInquiryLogDetailsByInquiryTypeID($inquiryTypeID)->current();
            if (!$inquiryLogData) {
                if (!empty($inquiryTypeID)) {
                    $result = $this->CommonTable('JobCard\Model\InquiryTypeTable')->getInquiryTypeByInquiryTypeID($inquiryTypeID)->current();
                    $entityID = $result['entityID'];
                    $this->updateDeleteInfoEntity($entityID);
                    $this->status = true;
                    $this->msg = $this->getMessage('SUCC_INQUTYPE_DELETE', array($inquiryTypeCode));
                    $this->flashMessenger()->addMessage($this->msg);
                } else {
                    $this->status = false;
                    $this->msg = $this->getMessage('ERR_INQUTYPE_DELETE');
                }
            } else {
                $this->status = false;
                $this->data = 'deactive';
            }
            return $this->JSONRespond();
        }
    }

    protected function getPaginatedInquiryTypes($perPage = 6)
    {
        $this->paginator = $this->CommonTable('JobCard\Model\InquiryTypeTable')->fetchAll(true);
        $this->paginator->setCurrentPageNumber((int) $this->params()->fromRoute('param1', 1));
        $this->paginator->setItemCountPerPage($perPage);
        $this->setLogMessage('Job Inquiry list accessed');

        return $this->paginator;
    }

    /**
     * @author Lahiru Abeysinghe <lahiru@thinkcube.com>
     * get all Inquiry types that related to given search Key
     * @return JSONRespond
     */
    public function searchInquiryTypesForDropdownAction()
    {
        $searchrequest = $this->getRequest();
        if ($searchrequest->isPost()) {
            $searchKey = $searchrequest->getPost('searchKey');
            $inquiryType = $this->CommonTable('JobCard/Model/InquiryTypeTable')->getInquiryTypeforSearch($searchKey);
            $inquiryTypeList = array();
            foreach ($inquiryType as $iT) {
                $temp['value'] = $iT['inquiryTypeID'];
                $temp['text'] = $iT['inquiryTypeCode'] . '-' . $iT['inquiryTypeName'];
                $inquiryTypeList[] = $temp;
            }
            $this->data = array('list' => $inquiryTypeList);
            return $this->JSONRespond();
        }
    }

    /**
     * @author Lahiru Abeysinghe <lahiru@thinkcube.com>
     *
     * use to change active state
     */
    public function changeStatusIDAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $inquiryTypeID = $request->getPost('inquiryTypeID');
            $status = $request->getPost('status');
            $updateState = $this->CommonTable('JobCard/Model/InquiryTypeTable')->updateInquiryTypeState($inquiryTypeID, $status);
            if ($updateState) {
                $this->status = true;
                $this->msg = $this->getMessage('SUCC_INQ_TYPE_STATUS_UPDATE');
                return $this->JSONRespond();
            }
        }
    }

}
