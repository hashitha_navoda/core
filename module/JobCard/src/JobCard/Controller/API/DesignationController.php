<?php

namespace JobCard\Controller\API;

use Zend\View\Model\ViewModel;
use Core\Controller\CoreController;
use JobCard\Form\DesignationForm;
use Zend\Session\Container;
use JobCard\Model\Designation;

class DesignationController extends CoreController
{

    /**
     * @author Lahiru Abeysinghe <lahiru@thinkcube.com>
     * this function use to add new Designations in to the system.
     */
    public function addAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $this->beginTransaction();
            $designationName = $request->getPost('designation');
            $designationID = $request->getPost('designationID');
            $result = $this->CommonTable('JobCard/Model/DesignationTable')->getDesignations($designationName);
            if (!($result->count() > 0)) {
                $entityID = $this->createEntity();
                $data = array(
                    'designationID' => $designationID,
                    'designationName' => $designationName,
                    'entityID' => $entityID,
                );
                $designationData = new Designation();
                $designationData->exchangeArray($data);
                $savedResult = $this->CommonTable('JobCard/Model/DesignationTable')->saveDesignations($designationData);
                if ($savedResult) {
                    $this->status = true;
                    $this->msg = $this->getMessage('SUCC_JOB_DESIG_SAVE');
                    $this->flashMessenger()->addMessage($this->msg);
                    $this->commit();
                } else {
                    $this->status = false;
                    $this->msg = $this->getMessage('ERR_JOB_DESIG_SAVE');
                    $this->rollback();
                }
            } else {
                $this->status = false;
                $this->data = 'DESIGNATION_EXIST';
                $this->msg = $this->getMessage('ERR_JOB_DESIG_CODE_ALREDY_EXIST');
                $this->rollback();
            }
            return $this->JSONRespond();
        }
    }

    /**
     * @author Lahiru Abeysinghe <lahiru@thinkcube.com>
     * this function use to update existing Designations
     */
    function updateAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $this->beginTransaction();
            $designationName = $request->getPost('designation');
            $designationID = $request->getPost('designationID');
            $data = array(
                'designationID' => $designationID,
                'designationName' => $designationName,
            );

            $designationData = new Designation();
            $designationData->exchangeArray($data);

            $updatedData = $this->CommonTable('JobCard/Model/DesignationTable')->updateDesignation($designationData);
            if ($updatedData) {
                $this->status = true;
                $this->msg = $this->getMessage('SUCC_JOB_DESIG_UPDATE');
                $this->flashMessenger()->addMessage($this->msg);
                $this->commit();
            } else {
                $this->status = false;
                $this->msg = $this->getMessage('ERR_JOB_DESIG_UPDATE');
                $this->rollback();
            }

            return $this->JSONRespond();
        }
    }

    /**
     * @author Lahiru Abeysinghe <lahiru@thinkcube.com>
     * this function use to delete existing
     */
    function deleteAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $this->beginTransaction();
            $designationID = $request->getPost('designationID');
            $empDetails = $this->CommonTable('JobCard/Model/EmployeeTable')->getEmployeeDetailsByDesignationID($designationID)->current();
            if (!$empDetails) {
                $designationDetails = (object) $this->CommonTable('JobCard/Model/DesignationTable')->getDesignationDetailByDesignationID($designationID)->current();
                $result = $this->updateDeleteInfoEntity($designationDetails->entityID);
                if ($result) {
                    $this->status = true;
                    $this->msg = $this->getMessage('SUCC_JOB_DESIG_DELETE');
                    $this->flashMessenger()->addMessage($this->msg);
                    $this->commit();
                } else {
                    $this->status = false;
                    $this->msg = $this->getMessage('ERR_JOB_DESIG_DELETE');
                    $this->rollback();
                }
            } else {
                $this->status = false;
                $this->data = 'deactive';
                $this->rollback();
            }

            return $this->JSONRespond();
        }
    }

    /**
     * @author Lahiru Abeysinghe <lahiru@thinkcube.com>
     * this function use to search existing Designation from the system
     *
     */
    public function getDesignationBySearchKeyAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $paginated = false;
            $designationSearchKey = $request->getPost('searchKey');

            if (!empty($designationSearchKey)) {
                $DesignationList = $this->CommonTable('JobCard/Model/DesignationTable')->DesignationSearchByKey($designationSearchKey);
                $this->status = true;
            } else {
                $this->status = true;
                $DesignationList = $this->getPaginatedDesignations();
                $paginated = true;
            }
            $view = new ViewModel(array(
                'designation' => $DesignationList,
                'paginated' => $paginated,
            ));

            $view->setTerminal(true);
            $view->setTemplate('job-card/designations/list.phtml');

            $this->html = $view;
            return $this->JSONRespondHtml();
        }
    }

    public function getPaginatedDesignations($perPage = 6)
    {
        $this->designation = $this->CommonTable('JobCard/Model/DesignationTable')->fetchAll(true);
        $this->designation->setCurrentPageNumber((int) $this->params()->fromRoute('param1', 1));
        $this->designation->setItemCountPerPage($perPage);
        return $this->designation;
    }

    /**
     * @author Lahiru Abeysinghe <lahiru@thinkcube.com>
     * use to change Designation active state
     */
    public function changeStatusIDAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $designationID = $request->getPost('designationID');
            $status = $request->getPost('status');

            $updateState = $this->CommonTable('JobCard/Model/DesignationTable')->updateDesignationState($designationID, $status);
            if ($updateState) {
                $this->status = true;
                $this->msg = $this->getMessage('SUCC_DES_STATUS_UPDATE');
                return $this->JSONRespond();
            }
        }
    }

}
