<?php
/**
 * @author Madawa Chandrarathne <madawa@thinkcube.com>
 * This file contains job dispatch related controller functions
 */

namespace JobCard\Controller\API;

use Zend\View\Model\ViewModel;
use Core\Controller\CoreController;
use JobCard\Model\ProjectOwners;
use JobCard\Model\JobOwner;
use JobCard\Model\ActivityOwner;

class JobDispatchController extends CoreController
{
    public function getDispatcherAction()
    {
        $this->status = false;
        $this->msg    = $this->getMessage('ERR_INVALID_REQUEST');
        $this->html   = null;
        
        $request = $this->getRequest();
        if ($request->isPost()) {
            $postData   = $request->getPost()->toArray();
            $locationId = $this->user_session->userActiveLocation["locationID"];
            
            $project = $job = array();            
            if($postData['selectedItemType'] == 'project') {
                $project = $this->getProjectDetails( $postData['selectedItemId'], $locationId);
            } else {
                $job = $this->getJobDetails( $postData['selectedItemId'], $locationId);
            }
            $view = new ViewModel(array(
                'project' => $project,
                'job' => $job
            ));

            $view->setTerminal(true);
            $view->setTemplate('job-card/job-dispatch/tree.phtml');
            $this->status = true;
            $this->html = $view;
            $this->data = empty($project) ? $job : $project;
        }
        return $this->JSONRespondHtml();
    }
    
    public function updateEmployeesAction() 
    {
        $this->status = false;
        $this->msg    = $this->getMessage('ERR_INVALID_REQUEST');
        $this->html   = null;
        
        $request = $this->getRequest();
        if ($request->isPost()) {
            $postData = $request->getPost()->toArray();
            
            $employeeId = $postData['employeeId'];            
            $taskId = $postData['taskId'];
            $taskType = $postData['taskType'];
            $action = $postData['action'];
            
            switch ($taskType) {
                case 'project':
                    $this->updateProjectEmployees( $taskId, $employeeId, $action);
                    break;
                
                case 'job':
                    $this->updateJobEmployees( $taskId, $employeeId, $action);
                    break;
                
                case 'activity':
                    $this->updateActivityEmployees( $taskId, $employeeId, $action);
                    break;

                default:
                    error_log('invalid request');
                    break;
            }            
        }
        return $this->JSONRespondHtml();        
    }
    
    /**
     * Assign / Remove project employee
     * @param int $projectId
     * @param int $employeeId
     * @param string $action
     */
    private function updateProjectEmployees( $projectId, $employeeId, $action)
    {
        $project = $this->CommonTable('JobCard\Model\ProjectTable')->getProjectDetailsByProjectID($projectId)->current();
        if($project){            
            if($action == 'add') {//for assign new employee                
                $isEmployeeExist = $this->CommonTable('JobCard\Model\ProjectOwnersTable')->isProjectEmployeeExist( $projectId, $employeeId);
                if(!$isEmployeeExist){
                    $employee = new ProjectOwners();
                    $employee->exchangeArray(['projectId'=>$projectId,'employeeId'=>$employeeId]);
                    $projectEmployee = $this->CommonTable('JobCard\Model\ProjectOwnersTable')->saveProjectOwners($employee);
                    if($projectEmployee){
                        $this->status = true;
                        $this->msg = $this->getMessage( 'SUCC_EMP_ASSIGN', ['project']);
                    } else {
                        $this->msg = $this->getMessage( 'ERR_EMP_ASSIGN', ['project']);
                    }
                } else {
                    $this->msg = $this->getMessage( 'ERR_DUPLICATE_EMP', ['project']);
                }                
            } else {//for delete existing employee
                $projectEmployee = $this->CommonTable('JobCard\Model\ProjectOwnersTable')->deleteProjectOwnerByProjectAndOwnerID( $projectId, $employeeId);
                if($projectEmployee){
                    $this->status = true;
                    $this->msg = $this->getMessage( 'SUCC_EMP_REMOVE', ['project']);
                } else {
                    $this->msg = $this->getMessage( 'ERR_EMP_REMOVE', ['project']);
                }
            }
        } else {
            $this->msg = $this->getMessage('ERR_PROJECT_DOSNOT_EXIST');
        }
    }
    
    /**
     * Assign / Remove job employee
     * @param int $jobId
     * @param int $employeeId
     * @param string $action
     */
    private function updateJobEmployees( $jobId, $employeeId, $action)
    {
        $job = $this->CommonTable('JobCard\Model\JobTable')->getJobDetails($jobId)->current();
        if ($job) {
            if($action == 'add') {//for assign new employee                
                $isEmployeeExist = $this->CommonTable('JobCard\Model\JobOwnerTable')->isJobEmployeeExist( $job['jobReferenceNumber'], $employeeId);
                if(!$isEmployeeExist){
                    $employee = new JobOwner();
                    $employee->exchangeArray(['referenceCode'=>$job['jobReferenceNumber'],'jobOwnerID'=>$employeeId]);
                    $jobEmployee = $this->CommonTable('JobCard\Model\JobOwnerTable')->saveJobOwner($employee);
                    if($jobEmployee){
                        $this->status = true;
                        $this->msg = $this->getMessage( 'SUCC_EMP_ASSIGN', ['job']);
                    } else {
                        $this->msg = $this->getMessage( 'ERR_EMP_ASSIGN', ['job']);
                    }
                } else {
                    $this->msg = $this->getMessage( 'ERR_DUPLICATE_EMP', ['job']);
                }                
            } else {//for delete existing employee
                $jobEmployee = $this->CommonTable('JobCard\Model\JobOwnerTable')->deleteJobOwnerByJobCodeAndOwnerId( $job['jobReferenceNumber'], $employeeId);
                if($jobEmployee){
                    $this->status = true;
                    $this->msg = $this->getMessage( 'SUCC_EMP_REMOVE', ['job']);
                } else {
                    $this->msg = $this->getMessage( 'ERR_EMP_REMOVE', ['job']);
                }
            }
        } else {
            $this->msg = $this->getMessage('ERR_JOB_NOT_EXIST');
        }
    }
    
    /**
     * Assign / Remove activity employee
     * @param int $activityId
     * @param int $employeeId
     * @param string $action
     */
    private function updateActivityEmployees( $activityId, $employeeId, $action)
    {
        $activity = $this->CommonTable('JobCard/Model/ActivityTable')->getActivityByID($activityId);
        if ($activity){
            if($action == 'add') {//for assign new employee                
                $isEmployeeExist = $this->CommonTable('JobCard\Model\ActivityOwnerTable')->isActivityEmployeeExist( $activityId, $employeeId);
                if(!$isEmployeeExist){
                    $employee = new ActivityOwner();
                    $employee->exchangeArray(['activityID'=>$activityId,'employeeID'=>$employeeId]);
                    $activityEmployee = $this->CommonTable('JobCard\Model\ActivityOwnerTable')->saveActivityOwner($employee);
                    if($activityEmployee){
                        $this->status = true;
                        $this->msg = $this->getMessage( 'SUCC_EMP_ASSIGN', ['activity']);
                        $this->data = $this->getAssignedEmployeeDetails($employeeId);
                    } else {
                        $this->msg = $this->getMessage( 'ERR_EMP_ASSIGN', ['activity']);
                    }
                } else {
                    $this->msg = $this->getMessage( 'ERR_DUPLICATE_EMP', ['activity']);
                }                
            } else {//for delete existing employee
                $activityEmployee = $this->CommonTable('JobCard\Model\ActivityOwnerTable')->deleteActivityOwnerByActivityIdAndEmployeeId( $activityId, $employeeId);
                if($activityEmployee){
                    $this->status = true;
                    $this->msg = $this->getMessage( 'SUCC_EMP_REMOVE', ['activity']);
                } else {
                    $this->msg = $this->getMessage( 'ERR_EMP_REMOVE', ['activity']);
                }
            }
        } else {
            $this->getMessage('ERR_ACTIVITY_DOSNOT_EXIST');
        }
    }
    
    /**
     * Get assign employee details
     * @param int $employeeId
     * @return array
     */
    private function getAssignedEmployeeDetails($employeeId)
    {
        $employee = [];
        $employeeDetails = $this->CommonTable('JobCard/Model/EmployeeTable')->getEmployeeDetailsByEmployeeID($employeeId)->current();
        if($employeeDetails){
            $employee = [
                'employeeId' => $employeeDetails['employeeID'], 
                'employeeName' => $employeeDetails['employeeFirstName'].' '.$employeeDetails['employeeSecondName']
            ];
        }
        return $employee;
    }

        /**
     * Get project details
     * @param int $projectId
     * @param int $locationId
     * @return mixed
     */
    private function getProjectDetails( $projectId, $locationId)
    {
        $jobArr = [];
        //get project details
        $projectData = $this->CommonTable('JobCard/Model/ProjectTable')->getProjectDetailsByProjectID($projectId)->current();
        if(!empty($projectData)){
            $projectData['timeDuration'] = $this->convertMinutesToDays($projectData['projectEstimatedTimeDuration']);
            $projectData['owner'] = $this->getOwners('project',$projectId,false);
            //get job list
            $jobs = $this->CommonTable('JobCard/Model/JobTable')->getJobListByProjectID($projectId, $locationId);
            foreach ($jobs as $job){
                $job['timeDuration'] = $this->convertMinutesToDays($job['jobEstimatedTime']);
                $job['owner'] = $this->getOwners('job',$job['jobReferenceNumber'],false);
                //get job activities
                $activities = $this->CommonTable('JobCard\Model\ActivityTable')->getActivityListByJobID( $job['jobId'], $locationId);
                $activityArr = [];
                foreach ($activities as $activity) {
                    $activity['timeDuration'] = $this->convertMinutesToDays( $activity['activityEstimateTime']);
                    $activity['owner'] = $this->getOwners( 'activity', $activity['activityId'], false);
                    $activity['ownerArr'] = $this->getOwners( 'activity', $activity['activityId'], true);
                    $activityArr[] = $activity;
                }
                $job['activities'] = $activityArr;
                $jobArr [] = $job;
            }
            $projectData['jobs'] = $jobArr;
        }
        return $projectData;
    }
    
    /**
     * Get job details
     * @param type $jobId
     * @param type $locationId
     * @return type
     */
    private function getJobDetails( $jobId, $locationId)
    {
        $activityArr = [];
        //get job list
        $job = $this->CommonTable('JobCard/Model/JobTable')->getJobDetails( $jobId)->current();
        if(!empty($job)){
            $job['timeDuration'] = $this->convertMinutesToDays($job['jobEstimatedTime']);
            $job['owner'] = $this->getOwners( 'job', $job['jobReferenceNumber'],false);
            //get activity list
            $activities = $this->CommonTable('JobCard\Model\ActivityTable')->getActivityListByJobID( $job['jobId'], $locationId);
            foreach ($activities as $activity){
                $activity['timeDuration'] = $this->convertMinutesToDays( $activity['activityEstimateTime']);
                $activity['owner'] = $this->getOwners( 'activity', $activity['activityId'], false);
                $activity['ownerArr'] = $this->getOwners( 'activity', $activity['activityId'], true);
                $activityArr[] = $activity;                
            }
            $job['activities'] = $activityArr;
        }
        return $job;
    }
    
    /**
     * Convert minutes to human readable days / hours / minutes
     * @param int $minutes
     * @return string
     */
    private function convertMinutesToDays( $minutes){
        if(!empty(floatval($minutes))){
            $d = floor ($minutes / 1440);
            $h = floor (($minutes - $d * 1440) / 60);
            $m = $minutes - ($d * 1440) - ($h * 60);            
            return "{$d} days {$h} hour {$m} min";
        } else {
            return '-';
        }
    }
    
    /**
     * Get owner list as string
     * @param string $objType ['project','job','activity']
     * @param int $objId
     * @param boolean $asArray
     * @return string
     */
    private function getOwners( $objType, $objId, $asArray = true) {
        $ownerArr = [];
        switch ($objType){
            case 'project':
                //get project owners
                $projectOwners = $this->CommonTable('JobCard\Model\ProjectOwnersTable')->getProjectOwnersByProjectID($objId);
                foreach ($projectOwners as $owner) {
                    $ownerArr[] = $owner['employeeFirstName'].' '.$owner['employeeSecondName'];
                }
                break;
            case 'job':
                //get job owners
                $jobOwners = $this->CommonTable('JobCard\Model\JobOwnerTable')->getJobOwnersDetailsByJobReference($objId);
                foreach ($jobOwners as $owner) {
                    $ownerArr[] = $owner['employeeFirstName'].' '.$owner['employeeSecondName'];
                }
                break;
            case 'activity':
                //get activity owners
                $activityOwners = $this->CommonTable('JobCard\Model\ActivityOwnerTable')->getActivityOwnersByActivityId( $objId);
                foreach ($activityOwners as $owner) {
                    $ownerArr[$owner['employeeID']] = $owner['employeeFirstName'].' '.$owner['employeeSecondName'];
                }
                break;
            default :
                error_log('invalid request');
        }
        if ($asArray) {
            $owners = $ownerArr;
        } else {
            $owners = empty($ownerArr) ? '-' : implode(", ", $ownerArr);
        }        
        return $owners;
    }
}
