<?php

namespace JobCard\Controller\API;

use Zend\View\Model\ViewModel;
use Core\Controller\CoreController;
use JobCard\Form\DivisionForm;
use Zend\Session\Container;
use JobCard\Model\Division;

class DivisionController extends CoreController
{

    /**
     * @author Lahiru Abeysinghe <lahiru@thinkcube.com>
     * this function use to add new Divisions in to the system
     *
     */
    public function addAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $this->beginTransaction();
            $divisionName = $request->getPost('division');
            $divisionID = $request->getPost('divisionID');
            $result = $this->CommonTable('JobCard/Model/DivisionTable')->getDivisions($divisionName);
            if (!($result->count() > 0)) {
                $entityID = $this->createEntity();
                $data = array(
                    'divisionID' => $divisionID,
                    'divisionName' => $divisionName,
                    'entityID' => $entityID,
                );
                $divisionData = new Division();
                $divisionData->exchangeArray($data);
                $savedResult = $this->CommonTable('JobCard/Model/DivisionTable')->saveDivisions($divisionData);
                if ($savedResult) {
                    $this->status = true;
                    $this->msg = $this->getMessage('SUCC_DIV_SAVE');
                    $this->flashMessenger()->addMessage($this->msg);
                    $this->commit();
                } else {
                    $this->status = false;
                    $this->msg = $this->getMessage('ERR_DIV_SAVE');
                    $this->rollback();
                }
            } else {
                $this->status = false;
                $this->data = 'DESIGNATION_EXIST';
                $this->msg = $this->getMessage('ERR_DIV_ALREDY_EXIST');
                $this->rollback();
            }
            return $this->JSONRespond();
        }
    }

    /**
     * @author Lahiru Abeysinghe <lahiru@thinkcube.com>
     * this function use to update existing Divisions
     *
     */
    function updateAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $this->beginTransaction();
            $divisionName = $request->getPost('division');
            $divisionID = $request->getPost('divisionID');
            $data = array(
                'divisionID' => $divisionID,
                'divisionName' => $divisionName,
            );
            $divisionData = new Division();
            $divisionData->exchangeArray($data);

            $updatedData = $this->CommonTable('JobCard/Model/DivisionTable')->updateDivision($divisionData);
            if ($updatedData) {
                $this->status = true;
                $this->msg = $this->getMessage('SUCC_DIV_UPDATE');
                $this->flashMessenger()->addMessage($this->msg);
                $this->commit();
            } else {
                $this->status = false;
                $this->msg = $this->getMessage('ERR_DIV_UPDATE');
                $this->rollback();
            }

            return $this->JSONRespond();
        }
    }

    /**
     * @author Lahiru Abeysinghe <lahiru@thinkcube.com>
     * this function use to delete existing Divisions in to the system
     *
     */
    function deleteAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $this->beginTransaction();
            $divisionID = $request->getPost('divisionID');
            $empData = $this->CommonTable('JobCard/Model/EmployeeTable')->getEmployeeDetailsByDivisionID($divisionID)->current();
            if (!$empData) {
                $devisionData = (object) $this->CommonTable('JobCard/Model/DivisionTable')->getDivisionByDivisionID($divisionID)->current();
                $result = $this->updateDeleteInfoEntity($devisionData->entityID);
                if ($result) {
                    $this->status = true;
                    $this->msg = $this->getMessage('SUCC_DIV_DELETE');
                    $this->flashMessenger()->addMessage($this->msg);
                    $this->commit();
                } else {
                    $this->status = false;
                    $this->msg = $this->getMessage('ERR_DIV_DELETE');
                    $this->rollback();
                }
            } else {
                $this->status = false;
                $this->data = 'deactive';
                $this->rollback();
            }
            return $this->JSONRespond();
        }
    }

    /**
     * @author Lahiru Abeysinghe <lahiru@thinkcube.com>
     * this function use to search Divisions
     *
     */
    public function getDivisionBySearchKeyAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $paginated = false;
            $divisionSearchKey = $request->getPost('searchKey');

            if (!empty($divisionSearchKey)) {
                $DivisionList = $this->CommonTable('JobCard/Model/DivisionTable')->DivisionSearchByKey($divisionSearchKey);
                $this->status = true;
            } else {
                $division = $this->getPaginatedDivisions();
                $DivisionList = $division;
                $this->status = true;
                $paginated = true;
            }
            $view = new ViewModel(array(
                'division' => $DivisionList,
                'paginated' => $paginated
            ));

            $view->setTerminal(true);
            $view->setTemplate('job-card/divisions/list.phtml');

            $this->html = $view;
            return $this->JSONRespondHtml();
        }
    }

    public function getPaginatedDivisions()
    {
        $this->division = $this->CommonTable('JobCard/Model/DivisionTable')->fetchAll(true);
        $this->division->setCurrentPageNumber((int) $this->params()->fromRoute('param1', 1));
        $this->division->setItemCountPerPage(5);
        return $this->division;
    }

    /**
     * @author Ashan <ashan@thinkcube.com>
     * Search divisions for dropdown
     *
     */
    public function searchDivisionsForDropdownAction()
    {
        $searchrequest = $this->getRequest();
        if ($searchrequest->isPost()) {
            $searchKey = $searchrequest->getPost('searchKey');
            $divisions = $this->CommonTable('JobCard/Model/DivisionTable')->DivisionSearchByKey($searchKey);
            $divisionList = array();
            foreach ($divisions as $division) {
                $temp['value'] = $division['divisionID'];
                $temp['text'] = $division['divisionName'];
                $divisionList[] = $temp;
            }
            $this->data = array('list' => $divisionList);
            return $this->JSONRespond();
        }
    }

    /**
     * @author Lahiru Abeysinghe <lahiru@thinkcube.com>
     * use to change division active state
     *
     */
    public function changeStatusIDAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $divisionID = $request->getPost('divisionID');
            $status = $request->getPost('status');
            $updateState = $this->CommonTable('JobCard/Model/DivisionTable')->updateDivisionState($divisionID, $status);
            if ($updateState) {
                $this->status = true;
                $this->msg = $this->getMessage('SUCC_DES_STATUS_UPDATE');
                return $this->JSONRespond();
            }
        }
    }

}
