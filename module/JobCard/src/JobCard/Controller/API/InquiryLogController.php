<?php

namespace JobCard\Controller\API;

use Zend\View\Model\ViewModel;
use Core\Controller\CoreController;
use JobCard\Form\InquiryLogForm;
use Zend\Session\Container;
use JobCard\Model\InquiryLog;
use JobCard\Model\InquiryComplainRelation;
use JobCard\Model\InquiryLogEmailSchedule;
use Core\Model\Notification;

class InquiryLogController extends CoreController
{

    /**
     * @author Lahiru Abeysinghe <lahiru@thinkcube.com>
     * This function use to add inquiry log in to the system
     */
    public function addAction()
    {
        $request = $this->getRequest();
        $userLocation = $this->user_session->userActiveLocation['locationID'];
        if ($request->isPost()) {
            $inquiryTime = $request->getPost('inquiryDate');
            $inqReferenceCode = $request->getPost('inquiryLogReference');
            $entityID = $this->createEntity();

            $inqLogTime = $this->getGMTDateTime('Y-m-d H:i:s', $inquiryTime);
            $reference = $this->getReferenceNoForLocation('24', $userLocation);
            $locationReferenceID = $reference['locRefID'];
            while ($inqReferenceCode) {
                if ($this->CommonTable('JobCard\Model\InquiryLogTable')->getInquiryLogByCode($inqReferenceCode)) {
                    if ($locationReferenceID) {
                        $newInqLgRefCode = $this->getReferenceNumber($locationReferenceID);
                        if ($inqReferenceCode == $newInqLgRefCode) {
                            $this->updateReferenceNumber($locationReferenceID);
                            $inqReferenceCode = $this->getReferenceNumber($locationReferenceID);
                        } else {
                            $inqReferenceCode = $newInqLgRefCode;
                        }
                    } else {
                        $this->status = false;
                        $this->msg = $this->getMessage('ERR_INQ_LOG_CODE_EXIST');
                        return $this->JSONRespond();
                    }
                } else {
                    break;
                }
            }
            $data = array(
                'customerID' => $request->getPost('cusName'),
                'customerProfileID' => $request->getPost('customerProfileID'),
                'inquiryTypeID' => $request->getPost('inquirylogType'),
                'inquiryLogDateAndTime' => $inqLogTime,
                'inquiryLogDescription' => $request->getPost('inquiryLogDescrip'),
                'inquiryLogCurrentUserID' => $request->getPost('inquiryLogUser'),
                'inquiryLogReference' => $inqReferenceCode,
                'inquiryStatusID' => $request->getPost('inquiryLogStatus'),
                'inquiryLogTP1' => $request->getPost('inquiryLogContactNo1'),
                'inquiryLogTP2' => $request->getPost('inquiryLogContactNo2'),
                'inquiryLogAddress' => $request->getPost('inquiryLogAddress'),
                'locationID' => $userLocation,
                'entityID' => $entityID,
                'inquiryComplainType' => $request->getPost('inquiryComplainType'),
            );
            $inquiryLogData = new InquiryLog();
            $inquiryLogData->exchangeArray($data);
            $savedResult = $this->CommonTable('JobCard/Model/InquiryLogTable')->saveInquiryLog($inquiryLogData);
            $inquiryComplainRelation = new InquiryComplainRelation();
            if ($request->getPost('inquiryComplainType') == 'Inquiry') {
                if ($request->getPost('projectID')) {
                    $inquiryComplainData = array(
                        'inquiryLogID' => $savedResult,
                        'documentTypeID' => '21',
                        'inquiryComplainRelationDocumentID' => $request->getPost('projectID'),
                    );
                    $inquiryComplainRelation->exchangeArray($inquiryComplainData);
                    $this->CommonTable('JobCard\Model\InquiryComplainRelationTable')->saveRelatedDocument($inquiryComplainRelation);
                }
                if ($request->getPost('jobID')) {
                    $inquiryComplainData = array(
                        'inquiryLogID' => $savedResult,
                        'documentTypeID' => '22',
                        'inquiryComplainRelationDocumentID' => $request->getPost('jobID'),
                    );
                    $inquiryComplainRelation->exchangeArray($inquiryComplainData);
                    $this->CommonTable('JobCard\Model\InquiryComplainRelationTable')->saveRelatedDocument($inquiryComplainRelation);
                }
                if ($request->getPost('activityID')) {
                    $inquiryComplainData = array(
                        'inquiryLogID' => $savedResult,
                        'documentTypeID' => '23',
                        'inquiryComplainRelationDocumentID' => $request->getPost('activityID'),
                    );
                    $inquiryComplainRelation->exchangeArray($inquiryComplainData);
                    $this->CommonTable('JobCard\Model\InquiryComplainRelationTable')->saveRelatedDocument($inquiryComplainRelation);
                }
            } else {
                //check data exists or not..
                if (count($request->getPost('datatypeListWithRefIds')) > 0) {
                    //save all related documents ids in inquiry complain table
                    foreach ($request->getPost('datatypeListWithRefIds') as $key => $value) {
                        $inquiryComplainData = array(
                            'inquiryLogID' => $savedResult,
                            'documentTypeID' => $value[0],
                            'inquiryComplainRelationDocumentID' => $value[1],
                        );
                        $inquiryComplainRelation->exchangeArray($inquiryComplainData);
                        $this->CommonTable('JobCard\Model\InquiryComplainRelationTable')->saveRelatedDocument($inquiryComplainRelation);
                    }
                }
            }
            if ($savedResult) {
                $dutyManagerList = $request->getPost('inquiryLogDutymanager');
                $dutyManagerListLength = $request->getPost('inquiryLogDutymanagerLength');
                if (count($dutyManagerList) > 0) {
                    $saveDutyManager = $this->CommonTable('JobCard/Model/InquiryLogDutyManagerTable')->saveDutyManager($savedResult, $dutyManagerList, $dutyManagerListLength);
                } else {
                    $saveDutyManager = true;
                }
            }

            if ($saveDutyManager) {
                $inquirySetupStatusResult = $this->CommonTable('JobCard\Model\InquirySetupTable')->getStatusByInquirySetupID('1');
                if ($inquirySetupStatusResult) {
                    $inquiryLogEmailSchedule = new InquiryLogEmailSchedule();
                    $inquiryScheduleDateTime = $this->getInquiryScheduleDateTimeByInquiryStatusID($request->getPost('inquiryLogStatus'), $inqLogTime);
                    $inquiryLogEmailScheduleData = array(
                        'inquiryLogEmailScheduleDateTime' => $inquiryScheduleDateTime,
                        'createdAt' => $this->getGMTDateTime(),
                        'inquirySetupID' => $inquirySetupStatusResult,
                        'inquiryLogID' => $savedResult,
                    );
                    $inquiryLogEmailSchedule->exchangeArray($inquiryLogEmailScheduleData);
                    $this->CommonTable('JobCard\Model\InquiryLogEmailScheduleTable')->save($inquiryLogEmailSchedule);
                }
                if ($request->getPost('inquiryComplainType') == 'Inquiry') {
                    $this->msg = $this->getMessage('SUCC_INQUIRY_LOG_SAVE');
                } else {
                    $this->msg = $this->getMessage('SUCC_COMPLAIN_SAVE');
                }
                $this->status = true;
                $this->flashMessenger()->addMessage($this->msg);
                $this->data = $savedResult;
                $this->commit();
                if ($locationReferenceID) {
                    $this->updateReferenceNumber($locationReferenceID);
                }
            } else {
                $this->status = false;
                $this->msg = $this->getMessage('ERR_INQUIRY_LOG_SAVE');
                $this->rollback();
            }


            return $this->JSONRespond();
        }
    }

    /**
     * @author Lahiru Abeysinghe <lahiru@thinkcube.com>
     * this function use to update existing inquiry log records.
     */
    function updateAction()
    {
        $request = $this->getRequest();
        $userLocation = $this->user_session->userActiveLocation['locationID'];
        if ($request->isPost()) {
            $inquiryTime = $request->getPost('inquiryDate');
            $inqLogTime = $this->getGMTDateTime('Y-m-d H:i:s', $inquiryTime);

            $inqLogData = $this->CommonTable('JobCard/Model/InquiryLogTable')->getInquiryLogDetails($inquiryLogID)->current();
            $data = array(
                'inquiryLogID' => $request->getPost('inquiryLogId'),
                'customerID' => $request->getPost('cusName'),
                'customerProfileID' => $request->getPost('customerProfileID'),
                'inquiryTypeID' => $request->getPost('inquirylogType'),
                'inquiryLogDateAndTime' => $inqLogTime,
                'inquiryLogDescription' => $request->getPost('inquiryLogDescrip'),
                'inquiryLogCurrentUserID' => $inqLogData['inquiryLogCurrentUserID'],
                'inquiryLogReference' => $request->getPost('inquiryLogReference'),
                'inquiryStatusID' => $request->getPost('inquiryLogStatus'),
                'inquiryLogTP1' => $request->getPost('inquiryLogContactNo1'),
                'inquiryLogTP2' => $request->getPost('inquiryLogContactNo2'),
                'inquiryLogAddress' => $request->getPost('inquiryLogAddress'),
                'locationID' => $userLocation,
            );
            $inquiryLogData = new InquiryLog();
            $inquiryLogData->exchangeArray($data);
            if ($inquiryComplainType = 'Inquiry') {
                $inquiryComplainRelation = new InquiryComplainRelation();
                if ($request->getPost('projectID')) {
                    $inquiryComplainData = array(
                        'inquiryLogID' => $request->getPost('inquiryLogId'),
                        'documentTypeID' => '21',
                        'inquiryComplainRelationDocumentID' => $request->getPost('projectID'),
                    );
                    $inquiryComplainRelation->exchangeArray($inquiryComplainData);
                    $this->CommonTable('JobCard\Model\InquiryComplainRelationTable')->updateRelatedDocument($inquiryComplainRelation);
                }
                if ($request->getPost('jobID')) {
                    $inquiryComplainData = array(
                        'inquiryLogID' => $request->getPost('inquiryLogId'),
                        'documentTypeID' => '22',
                        'inquiryComplainRelationDocumentID' => $request->getPost('jobID'),
                    );
                    $inquiryComplainRelation->exchangeArray($inquiryComplainData);
                    $this->CommonTable('JobCard\Model\InquiryComplainRelationTable')->updateRelatedDocument($inquiryComplainRelation);
                }
                if ($request->getPost('activityID')) {
                    $inquiryComplainData = array(
                        'inquiryLogID' => $request->getPost('inquiryLogId'),
                        'documentTypeID' => '23',
                        'inquiryComplainRelationDocumentID' => $request->getPost('activityID'),
                    );
                    $inquiryComplainRelation->exchangeArray($inquiryComplainData);
                    $this->CommonTable('JobCard\Model\InquiryComplainRelationTable')->updateRelatedDocument($inquiryComplainRelation);
                }
            }
            $dutyManagerList = $request->getPost('inquiryLogDutymanager');
            $dutyManagerListLength = $request->getPost('inquiryLogDutymanagerLength');
            $savedResult = $this->CommonTable('JobCard/Model/InquiryLogTable')->updateInquiryLog($inquiryLogData);
            if (count($dutyManagerList) > 0) {
                $savedDutyManager = $this->CommonTable('JobCard/Model/InquiryLogDutyManagerTable')->updateDutyManager($data[inquiryLogID], $dutyManagerList, $dutyManagerListLength);
            } else {
                $savedDutyManager = true;
            }
            if ($savedResult && $savedDutyManager) {
                $inquirySetupStatusResult = $this->CommonTable('JobCard\Model\InquirySetupTable')->getStatusByInquirySetupID('2');
                if ($inquirySetupStatusResult) {
                    $inquiryLogEmailSchedule = new InquiryLogEmailSchedule();
                    $inquiryScheduleDateTime = $this->getInquiryScheduleDateTimeByInquiryStatusID($request->getPost('inquiryLogStatus'), $inqLogTime);
                    $inquiryLogEmailScheduleData = array(
                        'inquiryLogEmailScheduleDateTime' => $inquiryScheduleDateTime,
                        'createdAt' => $this->getGMTDateTime(),
                        'inquirySetupID' => '2',
                        'inquiryLogID' => $request->getPost('inquiryLogId'),
                    );
                    $inquiryLogEmailSchedule->exchangeArray($inquiryLogEmailScheduleData);
                    $this->CommonTable('JobCard\Model\InquiryLogEmailScheduleTable')->save($inquiryLogEmailSchedule);
                }
                $this->status = true;
                $this->msg = $this->getMessage('SUCC_INQLG_UPDATE');
                $this->flashMessenger()->addMessage($this->msg);
                $this->commit();
            } else {
                $this->status = false;
                $this->msg = $this->getMessage('ERR_INQLG_UPDATE');
                $this->rollback();
            }

            return $this->JSONRespond();
        }
    }

    /**
     * @author Lahiru Abeysinghe <lahiru@thinkcube.com>
     * this function use to delete existing inquiry log records.
     */
    function deleteAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $inquiryLogID = $request->getPost('inquiryLogID');
            $inquiryData = (object) $this->CommonTable('JobCard/Model/InquiryLogTable')->getInquiryLogDetails($inquiryLogID)->current();

            $result = $this->updateDeleteInfoEntity($inquiryData->entityID);
            if ($result) {
                $inquirySetupStatusResult = $this->CommonTable('JobCard\Model\InquirySetupTable')->getStatusByInquirySetupID('3');
                if ($inquirySetupStatusResult) {
                    $inquiryLogEmailSchedule = new InquiryLogEmailSchedule();
                    $inquiryLogEmailScheduleData = array(
                        'inquiryLogEmailScheduleDateTime' => $this->getGMTDateTime(),
                        'createdAt' => $this->getGMTDateTime(),
                        'inquirySetupID' => '3',
                        'inquiryLogID' => $inquiryData->inquiryLogID,
                    );
                    $inquiryLogEmailSchedule->exchangeArray($inquiryLogEmailScheduleData);
                    $this->CommonTable('JobCard\Model\InquiryLogEmailScheduleTable')->save($inquiryLogEmailSchedule);
                }
                $this->status = true;
                $this->msg = $this->getMessage('SUCC_INQ_LOG_DELETE');
                $this->flashMessenger()->addMessage($this->msg);
                $this->commit();
            } else {
                $this->status = false;
                $this->msg = $this->getMessage('ERR_INQ_LOG_DELETE');
                $this->rollback();
            }

            return $this->JSONRespond();
        }
    }

    /**
     * @author Lahiru Abeysinghe <lahiru@thinkcube.com>
     * thiis function use to find some specific inquiry log records.
     */
    public function getInquiryLogBySearchKeyAction()
    {
        $request = $this->getRequest();
        $locationID = $this->user_session->userActiveLocation['locationID'];
        if ($request->isPost()) {
            $paginated = false;
            $searchKey = $request->getPost('searchKey');
            if (!empty($searchKey)) {
                $inquiryLogList = $this->setInquiryTypeAndComplainType($this->CommonTable('JobCard/Model/InquiryLogTable')->inquiryLogSearchByKey($searchKey, $locationID));
                $this->status = true;
            } else {
                $this->status = true;
                $inquiryLogList = $this->setInquiryTypeAndComplainType($this->getPaginatedInquiryLog($locationID));
            }
            $customerHistoryView = new ViewModel();
            $customerHistoryView->setTemplate('job-card/inquiry-log/customer-history');

            $documentView = new ViewModel();
            $documentView->setTemplate('job-card/inquiry-log/document-view');

            $view = new ViewModel(array(
                'inquiryLogList' => $inquiryLogList,
                'paginated' => $paginated,
                'status' => $this->getStatusesList(),
                'customerHistoryView' => $this->htmlRender($customerHistoryView),
                'documentView' => $this->htmlRender($documentView),
            ));

            $view->setTerminal(true);
            $view->setTemplate('job-card/inquiry-log/list.phtml');

            $this->html = $view;
            return $this->JSONRespondHtml();
        }
    }

    public function getPaginatedInquiryLog($locationID)
    {
        $this->inquiryLog = $this->CommonTable('JobCard/Model/inquiryLogTable')->fetchAll($locationID, true);
        $this->inquiryLog->setCurrentPageNumber((int) $this->params()->fromRoute('param1', 1));
        $this->inquiryLog->setItemCountPerPage(15);
        return $this->inquiryLog;
    }

    /**
     * @author Lahiru Abeysinghe <lahiru@thinkcube.com>
     * this function use to find all Job references related to given project id
     */
    public function getJobReferenceByProjectIDAction()
    {
        $request = $this->getRequest();
        $locationID = $this->user_session->userActiveLocation['locationID'];
        if ($request->isPost()) {
            $projectID = $request->getPost('projectId');
            $jobReferenceData = $this->CommonTable('JobCard\Model\JobTable')->getJobListByProjectID($projectID, $locationID);
            $jobReferenceDetails = array();
            foreach ($jobReferenceData as $j) {
                $jobReferenceDetails[$j['jobId']] = $j['jobReferenceNumber'];
            }
            $this->status = true;
            $this->data = $jobReferenceDetails;
            return $this->JSONRespond();
        }
    }

    /**
     * @author Lahiru Abeysinghe <lahiru@thinkcube.com>
     * this function use to find all activity references that related to given job id
     */
    public function getActivityReferenceByJobIDAction()
    {
        $request = $this->getRequest();
        $locationID = $this->user_session->userActiveLocation['locationID'];
        if ($request->isPost()) {
            $jobID = $request->getPost('jobId');
            $activityReferenceData = $this->CommonTable('JobCard\Model\ActivityTable')->getActivityListByJobID($jobID, $locationID);
            $activityReferenceDetails = array();
            foreach ($activityReferenceData as $a) {
                $activityReferenceDetails[$a['activityId']] = $a['activityCode'];
            }
            $this->status = true;
            $this->data = $activityReferenceDetails;
            return $this->JSONRespond();
        }
    }

    /**
     * @author Sharmilan <sharmilan@thinkcube.com>
     * Send Email to the Inquiry Log assigned Employees
     * @param type $inquiryLogID
     */
    public function sendEmailToInquiryLogEmployees($inquiryLogID = false, $inquirySetupName = 'create')
    {
        if ($inquiryLogID) {
            $logInquiryDetails = $this->CommonTable('JobCard/Model/InquiryLogTable')->getInquiryLogDetailsWithEmployeeDetails($inquiryLogID);
            foreach ($logInquiryDetails as $logInquiry) {
                $logInquiry = (object) $logInquiry;

                if ($logInquiry->employeeEmail) {
                    $inquiryLogEmailView = new ViewModel(array(
                        'employeeName' => $logInquiry->employeeFirstName . ' ' . $logInquiry->employeeSecondName,
                        'inquiryLogReference' => $logInquiry->inquiryLogReference,
                        'inquiryLogDateAndTime' => $this->getUserDateTime($logInquiry->inquiryLogDateAndTime),
                        'inquiryLogDescription' => $logInquiry->inquiryLogDescription,
                        'inquirySetupName' => $inquirySetupName,
                    ));
                    $inquiryLogEmailView->setTemplate('/core/email/log-inquiry');
                    $renderedEmailHtml = $this->htmlRender($inquiryLogEmailView);
                    $this->sendEmail($logInquiry->employeeEmail, 'You are assigned A Inquiry Log', $renderedEmailHtml);
                }
            }
            $this->status = true;
            $this->msg = "Inquiry log Details Has been sent.";
            return $this->JSONRespond();
        }
        $this->status = false;
        $this->msg = "Inquiry Log is Empty";
        return $this->JSONRespond();
    }

    /**
     * @author Sharmilan <sharmilan@thinkcube.com>
     * Get Schedule Date time by inquiry status ID, send the email to employees
     * @param type $inquiryStatusID
     * @param \DateTime $inquiryDateTime
     * @param type $format
     * @return type
     */
    public function getInquiryScheduleDateTimeByInquiryStatusID($inquiryStatusID, $inquiryDateTime, $format = 'Y-m-d H:i:s')
    {
        $inquiryStatus = $this->CommonTable('JobCard\Model\InquiryStatusTable')->getInquiryStatusByInquiryStatusID($inquiryStatusID);
        foreach ($inquiryStatus as $value) {
            $inquiryStatusTypeID = $value['inquiryStatusTypeID'];
        }

        $inquiryDateTime = new \DateTime($inquiryDateTime);
        switch ($inquiryStatusTypeID) {
            case '1':
                $intervel = new \DateInterval('PT0M');
                break;
            case '2':
                $intervel = new \DateInterval('PT1M');
                break;
            case '3':
                $intervel = new \DateInterval('PT1H');
                break;
            case '4':
                $intervel = new \DateInterval('P1M');
                break;
            default:
                break;
        }
        $inquiryDateTime->add($intervel);
        return $inquiryDateTime->format($format);
    }

    /**
     * @author Sharmilan <sharmilan@thinkcube.com>
     * Save as notification, to show assigned user
     * @param type $inquiryLog
     */
    public function saveInquiryLogNotification($inquiryLog)
    {
        if ($inquiryLog) {
            $logInquiryDetails = $this->CommonTable('JobCard/Model/InquiryLogTable')->getInquiryLogDetailsWithEmployeeDetails($inquiryLog['inquiryLogID']);
            foreach ($logInquiryDetails as $logInquiry) {
                if ($logInquiry['userID']) {
                    $logInquiry = (object) $logInquiry;
                    $notification = new Notification();
                    $notification->locationID = $logInquiry->locationID;
                    $notification->notificationReferID = $logInquiry->inquiryLogID;
                    $notification->notificationUpdatedDateTime = $this->getGMTDateTime();
                    $notification->notificationType = '5';
                    $notification->status = '0';
                    $notification->notificationTitle = 'Inquiry has been ' . $inquiryLog['inquirySetupName'] . 'd';
                    $notification->notificationBody = "The Inquiry with the reference no " . $logInquiry->inquiryLogReference . " has been " . $inquiryLog['inquirySetupName'] . 'd.';
                    $notification->userID = $logInquiry->userID;
                    $this->CommonTable('Core\Model\NotificationTable')->saveNotification($notification);
                }
            }
        }
    }

    /**
     * This will send the emails to inquiry log assigned employees and runs as cronjob
     * @author Sharmilan <sharmilan@thinkcube.com>
     */
    public function sendEmailToInquiryLogEmployeesAction()
    {
        $inquiryLogs = $this->CommonTable('JobCard\Model\InquiryLogEmailScheduleTable')->getInquiryLogIDsByScheduleDateTime($this->getGMTDateTime());
        foreach ($inquiryLogs as $inquiryLog) {
            $this->sendEmailToInquiryLogEmployees($inquiryLog['inquiryLogID'], $inquiryLog['inquirySetupName']);
//            save new notification without deleted notification
            if ($inquiryLog['inquirySetupID'] != '3') {
                $this->saveInquiryLogNotification($inquiryLog);
            }
            $this->CommonTable('JobCard\Model\InquiryLogEmailScheduleTable')->updateEmailSendStatus($inquiryLog['inquiryLogID'], '1');
        }
        echo $_SERVER['HTTP_HOST'] . " \n";
        echo "Successfully updated Inquiry log schedules. \n----------- \n\n";
        exit;
    }

    public function searchDocumentsForDropdownAction()
    {
        $request = $this->getRequest();
        $locationID = $this->user_session->userActiveLocation['locationID'];
        if ($request->isPost()) {
            $searchKey = $request->getPost('searchKey');
            $typeKey = $request->getPost('addFlag');
            switch ($typeKey) {
                case '1':
                    $referenceValue = $this->CommonTable('Invoice\Model\QuotationTable')->searchQuotationForDropDown($locationID, $searchKey);
                    $referenceNumber = array();
                    foreach ($referenceValue as $refValue) {
                        $temp['value'] = $refValue['quotationID'];
                        $temp['text'] = $refValue['quotationCode'];
                        $referenceNumber[] = $temp;
                    }
                    break;
                case '2':
                    $referenceValue = $this->CommonTable('Invoice\Model\SalesOrderTable')->searchSalesOrdersForDropDown($locationID, $searchKey);
                    $referenceNumber = array();
                    foreach ($referenceValue as $refValue) {
                        $temp['value'] = $refValue['soID'];
                        $temp['text'] = $refValue['soCode'];
                        $referenceNumber[] = $temp;
                    }
                    break;
                case '3':
                    $referenceValue = $this->CommonTable('Invoice\Model\InvoiceTable')->searchInvoicesForDropDown($locationID, $searchKey);
                    $referenceNumber = array();
                    foreach ($referenceValue as $refValue) {
                        $temp['value'] = $refValue['salesInvoiceID'];
                        $temp['text'] = $refValue['salesInvoiceCode'];
                        $referenceNumber[] = $temp;
                    }
                    break;
                case '21':
                    $referenceValue = $this->CommonTable('JobCard\Model\ProjectTable')->searchProjectsForDropDown($locationID, $searchKey);
                    $referenceNumber = array();
                    foreach ($referenceValue as $refValue) {
                        $temp['value'] = $refValue['projectId'];
                        $temp['text'] = $refValue['projectCode'];
                        $referenceNumber[] = $temp;
                    }
                    break;
                case '22':
                    $referenceValue = $this->CommonTable('JobCard\Model\JobTable')->searchJobsForDropDown($locationID, $searchKey);
                    $referenceNumber = array();
                    foreach ($referenceValue as $refValue) {
                        $temp['value'] = $refValue['jobId'];
                        $temp['text'] = $refValue['jobReferenceNumber'];
                        $referenceNumber[] = $temp;
                    }
                    break;
                case '23':
                    $referenceValue = $this->CommonTable('JobCard\Model\ActivityTable')->searchActivitiesForDropDown($locationID, $searchKey);
                    $referenceNumber = array();
                    foreach ($referenceValue as $refValue) {
                        $temp['value'] = $refValue['activityId'];
                        $temp['text'] = $refValue['activityCode'];
                        $referenceNumber[] = $temp;
                    }
                    break;
                case '24':
                    $referenceValue = $this->CommonTable('JobCard\Model\InquiryLogTable')->searchInquiryLogsForDropDown($locationID, $searchKey);
                    $referenceNumber = array();
                    foreach ($referenceValue as $refValue) {
                        $temp['value'] = $refValue['inquiryLogID'];
                        $temp['text'] = $refValue['inquiryLogReference'];
                        $referenceNumber[] = $temp;
                    }
                    break;
            }
            $this->status = true;
            $this->data = array('list' => $referenceNumber);
            return $this->JSONRespond();
        }
    }

    public function getAllDocumentDetailsByCustomerIDAction()
    {
        $request = $this->getRequest();
        $locationID = $this->user_session->userActiveLocation['locationID'];
        if ($request->isPost()) {
            $customerID = $request->getPost('customerID');
            $quotationData = $this->CommonTable('Invoice\Model\QuotationTable')->getQuotaionByCustomerID($customerID, $locationID);
            $sOData = $this->CommonTable('Invoice\Model\SalesOrderTable')->getSalesOrdersByCustomerID($customerID);
            $inviceData = $this->CommonTable('Invoice\Model\InvoiceTable')->getInvoiceByCustomerID($customerID, $locationID);
            $paymentsData = $this->CommonTable('Invoice\Model\PaymentsTable')->getPaymentsByCustomerID($customerID, $locationID);
            $returnsData = $this->CommonTable('SalesReturnsTable')->getReturnsByCustomerID($customerID, $locationID);
            $creditNoteData = $this->CommonTable('Invoice\Model\CreditNoteTable')->getCreditNoteByCustomerIDAndLocationID($customerID, $locationID);
            $creditNotePaymentData = $this->CommonTable('Invoice\Model\CreditNotePaymentTable')->getCreditNotePaymentsByCustomerIDAndLocationID($customerID, $locationID);
            $jobData = $this->CommonTable('JobCard\Model\JobTable')->getJobDetailsByCustomerID($customerID);
            $projectData = $this->CommonTable('JobCard\Model\ProjectTable')->getProjectDetailsByCustomerID($customerID);
            $dataExistsFlag = false;
            if (isset($quotationData)) {
                foreach ($quotationData as $qD) {
                    $quotData = array(
                        'type' => 'quotation',
                        'documentID' => $qD['quotationID'],
                        'code' => $qD['quotationCode'],
                        'amount' => number_format($qD['quotationTotalAmount'], 2),
                        'issuedDate' => $qD['quotationIssuedDate'],
                    );
                    $quotationDetails[] = $quotData;
                    if (isset($qD)) {
                        $dataExistsFlag = true;
                    }
                }
            }
            if (isset($sOData)) {
                foreach ($sOData as $sOD) {
                    $soDta = array(
                        'type' => 'salesOrder',
                        'documentID' => $sOD['soID'],
                        'code' => $sOD['soCode'],
                        'amount' => number_format($sOD['totalAmount'], 2),
                        'issuedDate' => $sOD['issuedDate'],
                    );
                    $soDetails[] = $soDta;
                    if (isset($sOD)) {
                        $dataExistsFlag = true;
                    }
                }
            }
            if (isset($inviceData)) {
                foreach ($inviceData as $invDta) {
                    $invDeta = array(
                        'type' => 'invoice',
                        'documentID' => $invDta['salesInvoiceID'],
                        'code' => $invDta['salesInvoiceCode'],
                        'amount' => number_format($invDta['salesinvoiceTotalAmount'], 2),
                        'issuedDate' => $invDta['salesInvoiceIssuedDate'],
                    );
                    $invoiceDetails[] = $invDeta;
                    if (isset($invDta)) {
                        $dataExistsFlag = true;
                    }
                }
            }
            if (isset($paymentsData)) {
                foreach ($paymentsData as $pData) {
                    $paymentDta = array(
                        'type' => 'payment',
                        'documentID' => $pData['incomingPaymentID'],
                        'code' => $pData['incomingPaymentCode'],
                        'amount' => number_format($pData['incomingPaymentAmount'], 2),
                        'issuedDate' => $pData['incomingPaymentDate'],
                    );
                    $paymentsDetails[] = $paymentDta;
                    if (isset($pData)) {
                        $dataExistsFlag = true;
                    }
                }
            }
            if (isset($returnsData)) {
                foreach ($returnsData as $rDta) {
                    $returnData = array(
                        'type' => 'salesReturn',
                        'documentID' => $rDta['salesReturnID'],
                        'code' => $rDta['salesReturnCode'],
                        'amount' => number_format($rDta['salesReturnTotal'], 2),
                        'issuedDate' => $rDta['salesReturnDate'],
                    );
                    $returnsDetails[] = $returnData;
                    if (isset($rDta)) {
                        $dataExistsFlag = true;
                    }
                }
            }
            if (isset($jobData)) {
                foreach ($jobData as $jD) {
                    $jobDta = array(
                        'type' => 'job',
                        'documentID' => $jD['jobId'],
                        'code' => $jD['jobReferenceNumber'],
                        'amount' => '-',
                        'issuedDate' => '-',
                    );
                    $jobDetails[] = $jobDta;
                    if (isset($jD)) {
                        $dataExistsFlag = true;
                    }
                }
            }

            if (isset($projectData)) {
                foreach ($projectData as $prDta) {
                    $proDta = array(
                        'type' => 'project',
                        'documentID' => $prDta['projectId'],
                        'code' => $prDta['projectCode'],
                        'amount' => '-',
                        'issuedDate' => '-',
                    );
                    $projectDetails[] = $proDta;
                    if (isset($prDta)) {
                        $dataExistsFlag = true;
                    }
                }
            }

            $documentDetails = array(
                'quotationDetails' => $quotationDetails,
                'sODetails' => $soDetails,
                'invoiceDetails' => $invoiceDetails,
                'paymentDetals' => $paymentsDetails,
                'returnDetails' => $returnsDetails,
//                'creditNoteDetails' => $creditNoteData,
//                'creditNotePaymentDetails' => $creditNotePaymentData,
                'jobDetails' => $jobDetails,
                'projectDetails' => $projectDetails,
            );
            if ($dataExistsFlag == true) {
                $this->data = $documentDetails;
                $this->status = true;
            } else {
                $this->status = false;
            }

            return $this->JSONRespond();
        }
    }

    /**
     * @author sharmilan <sharmilan@thinkcube.com>
     * search all inquiry logs for dropdown
     * @return type
     */
    public function searchInquiryLogForDropdownAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $locationID = $this->user_session->userActiveLocation['locationID'];
            $searchKey = $request->getPost('searchKey');
            $inquiryLogs = $this->CommonTable('JobCard/Model/InquiryLogTable')->searchInquiryLogForDropdown($locationID, $searchKey);

            $inquiryLogList = [];
            foreach ($inquiryLogs as $inquiryLog) {
                $temp['value'] = $inquiryLog['inquiryLogID'];
                $temp['text'] = $inquiryLog['inquiryLogReference'];
                $inquiryLogList[] = $temp;
            }
            $this->data = ['list' => $inquiryLogList];
            return $this->JSONRespond();
        }
    }

    /**
     * @author sharmilan <sharmilan@thinkcube.com>
     * get Inquiry Log by search
     * @return type
     */
    public function getInquiryLogBySearchAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $locationID = $this->user_session->userActiveLocation['locationID'];
            $inquiryLogID = $request->getPost('inquiryLogID');
            $inquiryLogs = $this->CommonTable('JobCard/Model/InquiryLogTable')->getInquiryLogsForView($locationID, $inquiryLogID);
            $inquiryLogList = $this->setInquiryTypeAndComplainType($inquiryLogs);

            if ($inquiryLogID) {
                $paginated = false;
            } else {
                $paginated = true;
            }

            $customerHistoryView = new ViewModel();
            $customerHistoryView->setTemplate('job-card/inquiry-log/customer-history');

            $documentView = new ViewModel();
            $documentView->setTemplate('job-card/inquiry-log/document-view');


            $view = new ViewModel([
                'inquiryLogList' => $inquiryLogList,
                'paginated' => $paginated,
                'status' => $this->getStatusesList(),
                'customerHistoryView' => $this->htmlRender($customerHistoryView),
                'documentView' => $this->htmlRender($documentView),
            ]);
            $view->setTerminal(true);
            $view->setTemplate('job-card/inquiry-log/list-table.phtml');


            $this->status = true;
            $this->html = $view;
            return $this->JSONRespondHtml();
        }
    }

    /**
     * @author sharmilan <sharmilan@thinkcube.com>
     * Get Inquiry Logs by date range
     * @return type
     */
    public function getInquiryLogByDateRangeAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $locationID = $this->user_session->userActiveLocation['locationID'];
            $fromDate = $this->getGMTDateTime('Y-m-d H:i:s', $request->getPost('fromDate') . ' 00:00');
            $toDate = $this->getGMTDateTime('Y-m-d H:i:s', $request->getPost('toDate') . ' 23:59');
            $inquiryLogs = $this->CommonTable('JobCard/Model/InquiryLogTable')->getInquiryLogsForViewByDateRange($locationID, $fromDate, $toDate);
            $inquiryLogList = $this->setInquiryTypeAndComplainType($inquiryLogs, false);

            $customerHistoryView = new ViewModel();
            $customerHistoryView->setTemplate('job-card/inquiry-log/customer-history');

            $documentView = new ViewModel();
            $documentView->setTemplate('job-card/inquiry-log/document-view');

            $view = new ViewModel([
                'inquiryLogList' => $inquiryLogList,
                'paginated' => false,
                'status' => $this->getStatusesList(),
                'viewAction' => 'view',
                'customerHistoryView' => $this->htmlRender($customerHistoryView),
                'documentView' => $this->htmlRender($documentView),
            ]);

            $view->setTerminal(true);
            $view->setTemplate('job-card/inquiry-log/list-table.phtml');

            $this->status = true;
            $this->html = $view;
            return $this->JSONRespondHtml();
        }
    }

    /**
     * this function use to get mapped documents details that related to the given document ID
     * @author Lahiru Abeysinghe <lahiru@thinkcube.com>
     * @return JSON respond
     */
    public function getAllDocumentDetailsByInqIDAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $inquiryLogID = $request->getPost('documentID');
            $documentList = $this->CommonTable('JobCard\Model\InquiryComplainRelationTable')->getDocumentForGivenInqID($inquiryLogID);
            $quotationListArray;
            foreach ($documentList as $list) {
                switch ($list['documentTypeID']) {
                    case '1':
                        $quotationDetails = (object) $this->CommonTable('Invoice\Model\QuotationTable')->getQuotaionByID($list['inquiryComplainRelationDocumentID']);
                        $currencyRate = ($quotationDetails->quotationCustomCurrencyRate != 0) ? $quotationDetails->quotationCustomCurrencyRate : 1;
                        $quotData = array(
                            'type' => 'quotation',
                            'documentID' => $quotationDetails->quotationID,
                            'code' => $quotationDetails->quotationCode,
                            'amount' => number_format($quotationDetails->totalAmount / $currencyRate, 2),
                            'issuedDate' => $quotationDetails->issuedDate
                        );
                        $quotationListArray[] = $quotData;
                        break;
                    case '2':
                        $salesOrder = $this->CommonTable('Invoice\Model\SalesOrderTable')->getSalesOrdersByID($list['inquiryComplainRelationDocumentID']);
                        $currencyRate = ($salesOrder->salesOrdersCustomCurrencyRate != 0) ? $salesOrder->salesOrdersCustomCurrencyRate : 1;
                        $soDta = array(
                            'type' => 'salesOrder',
                            'documentID' => $salesOrder->soID,
                            'code' => $salesOrder->soCode,
                            'amount' => number_format($salesOrder->totalAmount / $currencyRate, 2),
                            'issuedDate' => $salesOrder->issuedDate
                        );
                        $soDetails[] = $soDta;
                        break;
                    case '3':
                        $invoice = $this->CommonTable('Invoice\Model\InvoiceTable')->getInvoiceByInvoiceID($list['inquiryComplainRelationDocumentID'])->current();
                        $currencyRate = ($invoice->salesInvoiceCustomCurrencyRate != 0) ? $invoice->salesInvoiceCustomCurrencyRate : 1;
                        $invDeta = array(
                            'type' => 'invoice',
                            'documentID' => $invoice->salesInvoiceID,
                            'code' => $invoice->salesInvoiceCode,
                            'amount' => number_format($invoice->salesinvoiceTotalAmount / $currencyRate, 2),
                            'issuedDate' => $invoice->salesInvoiceIssuedDate
                        );
                        $invoiceDetails[] = $invDeta;
                        break;
                    case '21':
                        $project = (object) $this->CommonTable('JobCard\Model\ProjectTable')->getProjectDetailsByProjectID($list['inquiryComplainRelationDocumentID'])->current();
                        $proDta = array(
                            'type' => 'project',
                            'documentID' => $project->projectId,
                            'code' => $project->projectCode,
                            'amount' => '-',
                            'issuedDate' => '-'
                        );
                        $projectDetails[] = $proDta;
                        break;
                    case '22':
                        $job = (object) $this->CommonTable('JobCard\Model\JobTable')->getJobDetails($list['inquiryComplainRelationDocumentID'])->current();
                        $jobData = array(
                            'type' => 'job',
                            'documentID' => $job->jobId,
                            'code' => $job->jobReferenceNumber,
                            'amount' => '-',
                            'issuedDate' => '-'
                        );
                        $jobDetails[] = $jobData;
                        break;
                    case '23':
                        $activity = (object) $this->CommonTable('JobCard\Model\ActivityTable')->getActivityByActivityID($list['inquiryComplainRelationDocumentID'])->current();
                        $actData = array(
                            'type' => 'activity',
                            'documentID' => $activity->activityId,
                            'code' => $activity->activityCode,
                            'amount' => '-',
                            'issuedDate' => '-'
                        );
                        $activityDetails[] = $actData;
                        break;
                    case '24':
                        $inquiryLog = (object) $this->CommonTable('JobCard\Model\InquiryLogTable')->getInquiryLogDetailsByInquiryLogID($list['inquiryComplainRelationDocumentID'])->current();
                        $inqData = array(
                            'type' => 'inquiryLog',
                            'documentID' => $inquiryLog->inquiryLogID,
                            'code' => $inquiryLog->inquiryLogReference,
                            'amount' => '-',
                            'issuedDate' => '-'
                        );
                        $inquiryDetails[] = $inqData;
                        break;
                }
            }
            $documentDetails = array(
                'quotationDetails' => $quotationListArray,
                'sODetails' => $soDetails,
                'invoiceDetails' => $invoiceDetails,
                'jobDetails' => $jobDetails,
                'projectDetails' => $projectDetails,
                'activityDetails' => $activityDetails,
                'inquiryDetails' => $inquiryDetails,
            );
            $this->status = true;
            $this->data = $documentDetails;
            return $this->JSONRespond();
        }
    }

    public function changeInquiryLogStatusAction()
    {
        $this->status = false;
        $this->msg = $this->getMessage('ERR_INVALID_REQUEST');
        $this->data = null;

        $request = $this->getRequest();
        if ($request->isPost()) {
            $postData = $request->getPost()->toArray();
            //check whether inquiry log exist
            $inquiryLog = $this->CommonTable('JobCard\Model\InquiryLogTable')->getInquiryLogDetailsByInquiryLogID($postData['inquiryLogId'])->current();
            if ($inquiryLog) {
                try {
                    $this->beginTransaction();

                    $updateStatue = $this->CommonTable('JobCard\Model\InquiryLogTable')->updateInquiryLogDetails(array('statusID' => $postData['statusId']), $postData['inquiryLogId']);
                    if ($updateStatue) {
                        $this->commit();
                        $this->status = true;
                        $this->msg = $this->getMessage('SUCC_INQLG_UPDATE');
                    } else {
                        $this->msg = $this->getMessage('ERR_INQLG_UPDATE');
                    }
                } catch (\Exception $ex) {
                    $this->rollback();
                    $this->msg = $this->getMessage('ERR_INQLG_UPDATE');
                }
            } else {
                $this->getMessage('ERR_INQ_LOG_DOES_NOT_EXIST');
            }
        }
        return $this->JSONRespond();
    }

}
