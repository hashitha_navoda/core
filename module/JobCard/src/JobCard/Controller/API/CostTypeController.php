<?php

/**
 * @author Ashan Madushka <ashan@thinkcube.com>
 * This file contains Cost Type related Api controller functions
 */

namespace JobCard\Controller\API;

use Zend\View\Model\ViewModel;
use Core\Controller\CoreController;
use JobCard\Form\CostTypeForm;
use Zend\Session\Container;
use JobCard\Model\CostType;

class CostTypeController extends CoreController
{

//cost type save and update function
    public function addAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $this->beginTransaction();

            $costTypeName = $request->getPost('costTypeName');
            $costTypeDescription = $request->getPost('costTypeDescription');
            $productID = $request->getPost('productID');
            $minimumValue = $request->getPost('minimumValue');
            $maximumValue = $request->getPost('maximumValue');
            $editMode = $request->getPost('editMode');
//check wheter editMode is true or false. if it is true then update the cost type otherwise save the cost type
            if ($editMode == 'true') {
                $costTypeID = $request->getPost('costTypeID');
                $data = array(
                    'costTypeDescription' => $costTypeDescription,
                    'productID' => $productID,
                    'costTypeMinimumValue' => $minimumValue,
                    'costTypeMaximumValue' => $maximumValue,
                    'costTypeID' => $costTypeID,
                );
                $costTypeData = new CostType();
                $costTypeData->exchangeArray($data);
                $result = $this->CommonTable('JobCard\Model\CostTypeTable')->updateCostType($costTypeData);
                if ($result) {
                    $this->status = true;
                    $this->data = array('costTypeID' => $costTypeID);
                    $this->msg = $this->getMessage('SUCC_COSTYPE_UPDATE', array($costTypeName));
                    $this->flashMessenger()->addMessage($this->msg);
                    $this->commit();
                } else {
                    $this->status = false;
                    $this->msg = $this->getMessage('ERR_COSTYPE_UPDATE');
                    $this->rollback();
                }
            } else {
//check whether cost type Name alredy in database or not.
                $result = $this->CommonTable('JobCard\Model\CostTypeTable')->getCostTypeByCostTypeName($costTypeName);
                if (!($result->count() > 0)) {
                    $entityID = $this->createEntity();
                    $data = array(
                        'costTypeName' => $costTypeName,
                        'costTypeDescription' => $costTypeDescription,
                        'productID' => $productID,
                        'costTypeMinimumValue' => $minimumValue,
                        'costTypeMaximumValue' => $maximumValue,
                        'entityID' => $entityID
                    );

                    $costTypeData = new CostType();
                    $costTypeData->exchangeArray($data);
                    $costTypeID = $this->CommonTable('JobCard\Model\CostTypeTable')->saveCostType($costTypeData);
                    if ($costTypeID) {
                        $this->status = true;
                        $this->data = array('costTypeID' => $costTypeID);
                        $this->msg = $this->getMessage('SUCC_COSTYPE_SAVE');
                        $this->flashMessenger()->addMessage($this->msg);
                        $this->commit();
                    } else {
                        $this->status = false;
                        $this->msg = $this->getMessage('ERR_COSTYPE_SAVE');
                        $this->rollback();
                    }
                } else {
                    $this->status = false;
                    $this->data = 'CTCERR';
                    $this->msg = $this->getMessage('ERR_COSTYPE_NAME_ALREDY_EXIXT');
                }
            }
            return $this->JSONRespond();
        }
    }

//get cost types by cost type search key
    public function getCostTypesBySearchKeyAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $paginated = false;
            $costTypeSearchKey = $request->getPost('costTypeSearchKey');
            if (!empty($costTypeSearchKey)) {
                $tbl = $this->CommonTable('JobCard\Model\CostTypeTable');
                $costTypeList = $tbl->getCostTypeforSearch($costTypeSearchKey);
                $this->status = true;
            } else {
                $this->status = true;
                $costTypeList = $this->getPaginatedCostTypes();
                $paginated = true;
            }

            $view = new ViewModel(array(
                'costTypeList' => $costTypeList,
                'paginated' => $paginated
            ));

            $view->setTerminal(true);
            $view->setTemplate('job-card/cost-type/list.phtml');

            $this->html = $view;
            return $this->JSONRespondHtml();
        }
    }

    //delete cost types by cost type id
    public function deleteCostTypeByCostTypeIDAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $costTypeID = $request->getPost('costTypeID');
            $costTypeName = $request->getPost('costTypeName');
            $activityData = $this->CommonTable('JobCard/Model/ActivityCostTypeTable')->getActivityByActivityCostID($costTypeID)->current();
            if (!$activityData) {
                if (!empty($costTypeID)) {
                    $result = $this->CommonTable('JobCard\Model\CostTypeTable')->getCostTypeByCostTypeID($costTypeID)->current();
                    $entityID = $result['entityID'];
                    $this->updateDeleteInfoEntity($entityID);
                    $this->status = true;
                    $this->msg = $this->getMessage('SUCC_COSTYPE_DELETE', array($costTypeName));
                    $this->flashMessenger()->addMessage($this->msg);
                } else {
                    $this->status = false;
                    $this->msg = $this->getMessage('ERR_COSTYPE_DELETE');
                }
            } else {
                $this->status = false;
                $this->data = 'deactive';
            }
            return $this->JSONRespond();
        }
    }

    protected function getPaginatedCostTypes($perPage = 6)
    {
        $this->paginator = $this->CommonTable('JobCard\Model\CostTypeTable')->fetchAll(true);
        $this->paginator->setCurrentPageNumber((int) $this->params()->fromRoute('param1', 1));
        $this->paginator->setItemCountPerPage($perPage);
        $this->setLogMessage('Cost Type list accessed');

        return $this->paginator;
    }

    /**
     * @author Lahiru Abeysinghe <lahiru@thinkcube.com>
     * use to change active state
     */
    public function changeStatusIDAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $costTypeID = $request->getPost('costTypeID');
            $status = $request->getPost('status');
            $updateState = $this->CommonTable('JobCard/Model/CostTypeTable')->updateCostTypeState($costTypeID, $status);
            if ($updateState) {
                $this->status = true;
                $this->msg = $this->getMessage('SUCC_CST_TYPE_STATUS_UPDATE');
                return $this->JSONRespond();
            }
        }
    }

}
