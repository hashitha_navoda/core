<?php

namespace JobCard\Controller\API;

use Zend\View\Model\ViewModel;
use Core\Controller\CoreController;
use JobCard\Form\ActiveTypeForm;
use Zend\Session\Container;
use JobCard\Model\ActivityType;

class ActivityTypeController extends CoreController
{

    /**
     * @author Lahiru Abeysinghe <lahiru@thinkcube.com>
     * this function use to add new Activty types in to the system
     */
    public function addAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $this->beginTransaction();
            $activityTypeName = $request->getPost('activityTypeName');
            $maxValue = $request->getPost('maxValue');
            $minValue = $request->getPost('minValue');
            $result = $this->CommonTable('JobCard/Model/ActivityTypeTable')->getActivityTypes($activityTypeName);
            if (!($result->count() > 0)) {
                $entityID = $this->createEntity();
                $data = array(
                    'activityTypeName' => $activityTypeName,
                    'activityTypeMaxValue' => $maxValue,
                    'activityTypeMinValue' => $minValue,
                    'entityID' => $entityID,
                );
                $activityTypeData = new ActivityType();
                $activityTypeData->exchangeArray($data);
                $savedResult = $this->CommonTable('JobCard/Model/ActivityTypeTable')->saveActivityTypes($activityTypeData);
                if ($savedResult) {
                    $this->status = true;
                    $this->data = $savedResult;
                    $this->msg = $this->getMessage('SUCC_ACT_TYPE_SAVE');
                    $this->flashMessenger()->addMessage($this->msg);
                    $this->commit();
                } else {
                    $this->status = false;
                    $this->msg = $this->getMessage('ERR_ACT_TYPE_SAVE');
                    $this->rollback();
                }
            } else {
                $this->status = false;
                $this->data = 'ACTIVITY_TYPE_EXIST';
                $this->msg = $this->getMessage('ERR_ACT_TYPE_ALREDY_EXIST');
            }
            return $this->JSONRespond();
        }
    }

    /**
     * @author Lahiru Abeysinghe <lahiru@thinkcube.com>
     * this function use to update existing activity types
     */
    function updateAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $this->beginTransaction();
            $activityTypeName = $request->getPost('activityTypeName');
            $maxValue = $request->getPost('maxValue');
            $minValue = $request->getPost('minValue');
            $activityTypeID = $request->getPost('activityTypeID');
            $result = $this->CommonTable('JobCard/Model/ActivityTypeTable')->getActivityTypesByNameAndID($activityTypeName, $activityTypeID);
            if (!($result->count() > 0)) {
                $data = array(
                    'activityTypeID' => $activityTypeID,
                    'activityTypeName' => $activityTypeName,
                    'activityTypeMaxValue' => $maxValue,
                    'activityTypeMinValue' => $minValue,
                );
                $activityTypeData = new ActivityType();
                $activityTypeData->exchangeArray($data);

                $updatedData = $this->CommonTable('JobCard/Model/ActivityTypeTable')->updateActivityType($activityTypeData);
                if ($updatedData) {
                    $this->status = true;
                    $this->msg = $this->getMessage('SUCC_ACT_TYPE_UPDATE');
                    $this->flashMessenger()->addMessage($this->msg);
                    $this->commit();
                } else {
                    $this->status = false;
                    $this->msg = $this->getMessage('ERR_ACT_TYPE_UPDATE');
                    $this->rollback();
                }
            } else {
                $this->status = false;
                $this->data = 'ACTIVITY_TYPE_EXIST';
                $this->msg = $this->getMessage('ERR_ACT_TYPE_ALREDY_EXIST');
            }

            return $this->JSONRespond();
        }
    }

    /**
     * @author Lahiru Abeysinghe <lahiru@thinkcube.com>
     * this function use to delete activity types
     */
    function deleteAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $this->beginTransaction();
            $activityTypeID = $request->getPost('activityTypeID');
            $activityData = $this->CommonTable('JobCard/Model/ActivityTable')->getActivityByActivityTypeID($activityTypeID)->current();
            if (!$activityData) {
                $activityTypeData = (object) $this->CommonTable('JobCard/Model/ActivityTypeTable')->getActivityTypeDataByActivityTypeID($activityTypeID)->current();
                $result = $this->updateDeleteInfoEntity($activityTypeData->entityID);
                if ($result) {
                    $this->status = true;
                    $this->msg = $this->getMessage('SUCC_ACT_TYPE_DELETE');
                    $this->flashMessenger()->addMessage($this->msg);
                    $this->commit();
                } else {
                    $this->status = false;
                    $this->msg = $this->getMessage('ERR_ACT_TYPE_DELETE');
                    $this->rollback();
                }
            } else {
                $this->status = false;
                $this->data = 'deactive';
                $this->rollback();
            }

            return $this->JSONRespond();
        }
    }

    /**
     * @author Lahiru Abeysinghe <lahiru@thinkcube.com>
     * this function use to search activity types
     */
    public function getActivityTypeBySearchKeyAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $paginated = false;
            $activityTypeSearchKey = $request->getPost('searchKey');

            if (!empty($activityTypeSearchKey)) {
                $activityTypeList = $this->CommonTable('JobCard/Model/ActivityTypeTable')->ActivityTypeSearchByKey($activityTypeSearchKey);
                $this->status = true;
            } else {
                $this->status = true;
                $activityTypeList = $this->getPaginatedActivityTypes();
                $paginated = true;
            }
            $view = new ViewModel(array(
                'activityTypes' => $activityTypeList,
                'paginated' => $paginated
            ));

            $view->setTerminal(true);
            $view->setTemplate('job-card/activity-type/list.phtml');

            $this->html = $view;
            return $this->JSONRespondHtml();
        }
    }

    public function getPaginatedActivityTypes($perPage = 6)
    {
        $this->activityType = $this->CommonTable('JobCard/Model/ActivityTypeTable')->fetchAll(true);
        $this->activityType->setCurrentPageNumber((int) $this->params()->fromRoute('param1', 1));
        $this->activityType->setItemCountPerPage($perPage);
        return $this->activityType;
    }

    /**
     * @author Lahiru Abeysinghe <lahiru@thinkcube.com>
     * use to change active state
     */
    public function changeStatusIDAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $activityTypeID = $request->getPost('activityTypeID');
            $status = $request->getPost('status');
            $updateState = $this->CommonTable('JobCard/Model/ActivityTypeTable')->updateActivityTypeState($activityTypeID, $status);
            if ($updateState) {
                $this->status = true;
                $this->msg = $this->getMessage('SUCC_ACT_TYPE_STATUS_UPDATE');
                return $this->JSONRespond();
            }
        }
    }

}
