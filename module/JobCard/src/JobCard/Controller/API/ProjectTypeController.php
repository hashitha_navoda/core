<?php

/**
 * @author Ashan Madushka <ashan@thinkcube.com>
 * This file contains Project Type related Api controller functions
 */

namespace JobCard\Controller\API;

use Zend\View\Model\ViewModel;
use Core\Controller\CoreController;
use Zend\Session\Container;
use JobCard\Model\ProjectType;

class ProjectTypeController extends CoreController
{

//project type save and update function
    public function addAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $this->beginTransaction();

            $projectTypeCode = $request->getPost('projectTypeCode');
            $projectTypeName = $request->getPost('projectTypeName');
            $editMode = $request->getPost('editMode');
            //check wheter editMode is true or false. if it is true then update the project type otherwise save the project type
            if ($editMode == 'true') {
                $projectTypeID = $request->getPost('projectTypeID');
                $data = array(
                    'projectTypeName' => $projectTypeName,
                    'projectTypeID' => $projectTypeID,
                );

                $projectTypeData = new ProjectType();
                $projectTypeData->exchangeArray($data);
                $result = $this->CommonTable('JobCard\Model\ProjectTypeTable')->updateProjectType($projectTypeData);
                if ($result) {
                    $this->status = true;
                    $this->data = array('projectTypeID' => $projectTypeID);
                    $this->msg = $this->getMessage('SUCC_PROTYPE_UPDATE', array($projectTypeCode));
                    $this->flashMessenger()->addMessage($this->msg);
                    $this->commit();
                } else {
                    $this->status = false;
                    $this->msg = $this->getMessage('ERR_PROTYPE_UPDATE');
                    $this->rollback();
                }
            } else {
                //check whether project type code alredy in database or not.
                $result = $this->CommonTable('JobCard\Model\ProjectTypeTable')->getProjectTypeByProjectTypeCode($projectTypeCode);
                if (!($result->count() > 0)) {
                    $entityID = $this->createEntity();
                    $data = array(
                        'projectTypeCode' => $projectTypeCode,
                        'projectTypeName' => $projectTypeName,
                        'entityID' => $entityID
                    );

                    $projectTypeData = new ProjectType();
                    $projectTypeData->exchangeArray($data);
                    $projectTypeID = $this->CommonTable('JobCard\Model\ProjectTypeTable')->saveProjectType($projectTypeData);
                    if ($projectTypeID) {
                        $this->status = true;
                        $this->data = array('projectTypeID' => $projectTypeID);
                        $this->msg = $this->getMessage('SUCC_PROTYPE_SAVE');
                        $this->flashMessenger()->addMessage($this->msg);
                        $this->commit();
                    } else {
                        $this->status = false;
                        $this->msg = $this->getMessage('ERR_PROTYPE_SAVE');
                        $this->rollback();
                    }
                } else {
                    $this->status = false;
                    $this->data = 'PTCERR';
                    $this->msg = $this->getMessage('ERR_PROTYPE_CODE_ALREDY_EXIXT');
                }
            }
            return $this->JSONRespond();
        }
    }

    //get project types by project type search key
    public function getProjectTypesBySearchKeyAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $paginated = false;
            $projectTypeSearchKey = $request->getPost('projectTypeSearchKey');

            if (!empty($projectTypeSearchKey)) {
                $tbl = $this->CommonTable('JobCard\Model\ProjectTypeTable');
                $projectTypeList = $tbl->getProjectTypeforSearch($projectTypeSearchKey);
                $this->status = true;
            } else {
                $this->status = true;
                $projectTypeList = $this->getPaginatedProjectTypes();
                $paginated = true;
            }

            $view = new ViewModel(array(
                'projectTypeList' => $projectTypeList,
                'paginated' => $paginated
            ));

            $view->setTerminal(true);
            $view->setTemplate('job-card/project-type/list.phtml');

            $this->html = $view;
            return $this->JSONRespondHtml();
        }
    }

    //delete project types by job type id
    public function deleteProjectTypeByProjectTypeIDAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $projectTypeID = $request->getPost('projectTypeID');
            $projectTypeCode = $request->getPost('projectTypeCode');
            $projectData = $this->CommonTable('JobCard/Model/ProjectTable')->getProjectDetailsByProjectTypeID($projectTypeID)->current();
            if (!$projectData) {
                if (!empty($projectTypeID)) {
                    $result = $this->CommonTable('JobCard\Model\ProjectTypeTable')->getProjectTypeByProjectTypeID($projectTypeID)->current();
                    $entityID = $result['entityID'];
                    $this->updateDeleteInfoEntity($entityID);
                    $this->status = true;
                    $this->msg = $this->getMessage('SUCC_PROTYPE_DELETE', array($projectTypeCode));
                    $this->flashMessenger()->addMessage($this->msg);
                } else {
                    $this->status = false;
                    $this->msg = $this->getMessage('ERR_PROTYPE_DELETE');
                }
            } else {
                $this->status = false;
                $this->data = 'deactive';
            }
            return $this->JSONRespond();
        }
    }

    protected function getPaginatedProjectTypes($perPage = 6)
    {
        $this->paginator = $this->CommonTable('JobCard\Model\ProjectTypeTable')->fetchAll(true);
        $this->paginator->setCurrentPageNumber((int) $this->params()->fromRoute('param1', 1));
        $this->paginator->setItemCountPerPage($perPage);
        $this->setLogMessage('Project Type list accessed');

        return $this->paginator;
    }

    /**
     * @author Ashan <ashan@thinkcube.com>
     * Search project types for dropdown
     * @return type
     */
    public function searchProjectTypesForDropdownAction()
    {
        $searchrequest = $this->getRequest();
        if ($searchrequest->isPost()) {
            $searchKey = $searchrequest->getPost('searchKey');
            $projectTypes = $this->CommonTable('JobCard\Model\ProjectTypeTable')->searchProjectTypesForDropDown($searchKey);
            $projectTypeList = array();
            foreach ($projectTypes as $projectTypes) {
                $temp['value'] = $projectTypes['projectTypeID'];
                $temp['text'] = $projectTypes['projectTypeCode'] . '-' . $projectTypes['projectTypeName'];
                $projectTypeList[] = $temp;
            }

            $this->data = array('list' => $projectTypeList);
            return $this->JSONRespond();
        }
    }

    /**
     * @author Lahiru Abeysinghe <lahiru@thinkcube.com>
     * use to change active state
     */
    public function changeStatusIDAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $projectTypeID = $request->getPost('projectTypeID');
            $status = $request->getPost('status');
            $updateState = $this->CommonTable('JobCard/Model/ProjectTypeTable')->updateProjectTypeState($projectTypeID, $status);
            if ($updateState) {
                $this->status = true;
                $this->msg = $this->getMessage('SUCC_PROTYPE_STATUS_UPDATE');
                return $this->JSONRespond();
            }
        }
    }

}
