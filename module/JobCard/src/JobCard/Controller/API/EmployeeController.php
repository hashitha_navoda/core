<?php

namespace JobCard\Controller\API;

use Zend\View\Model\ViewModel;
use Core\Controller\CoreController;
use JobCard\Form\EmployeesForm;
use Zend\Session\Container;
use JobCard\Model\Employee;

class EmployeeController extends CoreController
{

    /**
     * @author Lahiru Abeysinghe <lahiru@thinkcube.com>
     * this function use to add new employees in to the system
     *
     */
    public function addAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $this->beginTransaction();
            $empFName = $request->getPost('empFName');
            $empSName = $request->getPost('empSName');
            $designation = $request->getPost('designation');
            $division = $request->getPost('division');
            $email = $request->getPost('employeeEmail');
            $tp = $request->getPost('telephoneNumber');
            $hourlyCost = $request->getPost('empHorlyCost');
            $userID = $request->getPost('userID');
            $entityID = $this->createEntity();
            $result = $this->CommonTable('JobCard/Model/EmployeeTable')->getEmployees($empFName, $empSName, $designation, $division, $email);
            if (!($result->count() > 0)) {
                $data = array(
                    'employeeFirstName' => $empFName,
                    'employeeSecondName' => $empSName,
                    'designationID' => $designation,
                    'divisionID' => $division,
                    'employeeEmail' => $email,
                    'employeeTP' => $tp,
                    'employeeHourlyCost' => $hourlyCost,
                    'entityID' => $entityID,
                    'userID' => $userID
                );
                $employeesData = new Employee();
                $employeesData->exchangeArray($data);
                $savedResult = $this->CommonTable('JobCard/Model/EmployeeTable')->saveEmployees($employeesData);
                if ($savedResult) {
                    $this->status = true;
                    $this->msg = $this->getMessage('SUCC_EMP_SAVE');
                    $this->flashMessenger()->addMessage($this->msg);
                    $this->commit();
                } else {
                    $this->status = false;
                    $this->msg = $this->getMessage('ERR_EMP_SAVE');
                    $this->rollback();
                }
            } else {
                $this->status = false;
                $this->data = 'EMP_EXIST';
                $this->msg = $this->getMessage('ERR_EMP_ALREDY_EXIST');
                $this->rollback();
            }
            return $this->JSONRespond();
        }
    }

    /**
     * @author Lahiru Abeysinghe <lahiru@thinkcube.com>
     * this function use to update existing employees
     *
     */
    function updateAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $this->beginTransaction();
            $empID = $request->getPost('employeesID');
            $empFName = $request->getPost('empFName');
            $empSName = $request->getPost('empSName');
            $designation = $request->getPost('designation');
            $division = $request->getPost('division');
            $tp = $request->getPost('telephoneNumber');
            $email = $request->getPost('employeeEmail');
            $userID = $request->getPost('userID');
            $hourlyCost = $request->getPost('empHorlyCost');
            $data = array(
                'employeeID' => $empID,
                'employeeFirstName' => $empFName,
                'employeeSecondName' => $empSName,
                'designationID' => $designation,
                'divisionID' => $division,
                'employeeTP' => $tp,
                'employeeEmail' => $email,
                'employeeHourlyCost' => $hourlyCost,
                'userID' => $userID,
            );

            $employeesData = new Employee();
            $employeesData->exchangeArray($data);

            $updatedData = $this->CommonTable('JobCard/Model/EmployeeTable')->updateEmployees($employeesData);
            if ($updatedData) {
                $this->status = true;
                $this->msg = $this->getMessage('SUCC_EMP_UPDATE');
                $this->flashMessenger()->addMessage($this->msg);
                $this->commit();
            } else {
                $this->status = false;
                $this->msg = $this->getMessage('ERR_EMP_UPDATE');
                $this->rollback();
            }

            return $this->JSONRespond();
        }
    }

    /**
     * @author Lahiru Abeysinghe <lahiru@thinkcube.com>
     * this function use to delete existing employees
     *
     */
    function deleteAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $this->beginTransaction();
            $employeesID = $request->getPost('employeesID');
            $jobOwner = $this->CommonTable('JobCard/Model/JobOwnerTable')->getJobOwnerDetailsByEmployeeID($employeesID)->current();
            $jobSupervosor = $this->CommonTable('JobCard/Model/JobSupervisorTable')->getJobSupervisorDetailsByEmployeeID($employeesID)->current();
            $projectSupervosor = $this->CommonTable('JobCard/Model/ProjectSupervisorsTable')->getProjectSupervisorDetailsByEmployeeID($employeesID)->current();
            $projectOwner = $this->CommonTable('JobCard/Model/ProjectOwnersTable')->getProjectOwnerDetailsByEmployeeID($employeesID)->current();
            $inqDutyManager = $this->CommonTable('JobCard/Model/InquiryLogDutyManagerTable')->getInquiryLogDutyManagerDetailsByEmployeeID($employeesID)->current();
            if (!$jobOwner && !$jobSupervosor && !$projectOwner && !$projectSupervosor && !$inqDutyManager) {
                $empDetails = (object) $this->CommonTable('JobCard/Model/EmployeeTable')->getEmployeeDetailsByEmployeeID($employeesID)->current();
                $result = $this->updateDeleteInfoEntity($empDetails->entityID);
                if ($result) {
                    $this->status = true;
                    $this->msg = $this->getMessage('SUCC_EMP_DELETE');
                    $this->flashMessenger()->addMessage($this->msg);
                    $this->commit();
                } else {
                    $this->status = false;
                    $this->msg = $this->getMessage('ERR_EMP_DELETE');
                    $this->rollback();
                }
            } else {
                $this->status = false;
                $this->data = 'deactive';
                $this->rollback();
            }

            return $this->JSONRespond();
        }
    }

    /**
     * @author Lahiru Abeysinghe <lahiru@thinkcube.com>
     * this function use to search employees
     *
     */
    public function getEmployeeBySearchKeyAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $paginated = false;
            $employeeSearchKey = $request->getPost('searchKey');

            if (!empty($employeeSearchKey)) {
                $employeeList = $this->CommonTable('JobCard/Model/EmployeeTable')->EmployeeSearchByKey($employeeSearchKey);
                $this->status = true;
            } else {
                $this->status = true;
                $employeeList = $this->getPaginatedEmployees();
                $paginated = true;
            }
            $view = new ViewModel(array(
                'employees' => $employeeList,
                'paginated' => $paginated
            ));

            $view->setTerminal(true);
            $view->setTemplate('job-card/employees/list.phtml');

            $this->html = $view;
            return $this->JSONRespondHtml();
        }
    }

    public function getPaginatedEmployees()
    {
        $this->employee = $this->CommonTable('JobCard/Model/EmployeeTable')->fetchAll(true);
        $this->employee->setCurrentPageNumber((int) $this->params()->fromRoute('param1', 1));
        $this->employee->setItemCountPerPage(5);
        return $this->employee;
    }

    /**
     * @author Ashan <ashan@thinkcube.com>
     * Search employee for dropdown
     * @return type
     */
    public function searchEmployeeForDropdownAction()
    {
        $searchrequest = $this->getRequest();
        if ($searchrequest->isPost()) {
            $searchKey = $searchrequest->getPost('searchKey');
            $employee = $this->CommonTable('JobCard/Model/EmployeeTable')->searchEmployeeForDropdown($searchKey);
            $employeeList = array();
            foreach ($employee as $employee) {
                $temp['value'] = $employee['employeeID'];
                $temp['text'] = $employee['employeeFirstName'] . ' ' . $employee['employeeSecondName'];
                $employeeList[] = $temp;
            }
            $this->data = array('list' => $employeeList);
            return $this->JSONRespond();
        }
    }

    /**
     * @author Lahiru Abeysinghe <lahiru@thinkcube.com>
     * use to change employee active state
     */
    public function changeStatusIDAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $employeeID = $request->getPost('employeeID');
            $status = $request->getPost('status');
            $updateState = $this->CommonTable('JobCard/Model/EmployeeTable')->updateEmployeeState($employeeID, $status);
            if ($updateState) {
                $this->status = true;
                $this->msg = $this->getMessage('SUCC_EMP_STATUS_UPDATE');
                return $this->JSONRespond();
            }
        }
    }

}
