<?php

/**
 * @author Sandun <sandun@thinkcube.com>
 * this file contains,function of related to activity
 */

namespace JobCard\Controller\API;

use Core\Controller\CoreController;
use JobCard\Model\Activity;
use JobCard\Model\ActivityOwner;
use JobCard\Model\ActivitySupervisor;
use JobCard\Model\ActivityRawMaterial;
use JobCard\Model\ActivitySerial;
use JobCard\Model\ActivityBatch;
use JobCard\Model\ActivityFixedAsset;
use JobCard\Model\ActivityContractor;
use JobCard\Model\ActivityTemporaryContractor;
use JobCard\Model\ActivityTemporaryProduct;
use JobCard\Model\ActivityRawMaterialSubProduct;
use JobCard\Model\ActivityCostType;
use JobCard\Model\ActivityFixedAssetSubProduct;
use Inventory\Model\ItemOut;
use Inventory\Model\ItemIn;
use Zend\View\Model\ViewModel;
use JobCard\Model\ActivityVehicle;

class ActivityController extends CoreController
{

    public function saveAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $this->beginTransaction();
            $locationID = $this->user_session->userActiveLocation['locationID'];
            $result = $this->getReferenceNoForLocation(23, $locationID);
            $locationReferenceID = $result['locRefID'];
            $activityName = $request->getPost('activityName');
            $jobReferenceID = $request->getPost('jobReferenceID');
            $projectReferenceID = $request->getPost('projectReferenceID');
            $activityTypeID = $request->getPost('activityTypeID');
            $activityDescription = $request->getPost('activityDescription');
            $activityEstimateTime = $request->getPost('activityEstimateTime');
            $activityEstimateCost = $request->getPost('estimateCost');
            $activityCurrentWeight = $request->getPost('currentWeight');
            $isUpdateActivity = $request->getPost('isUpdateActivity');
            $activityRepeatEnabled = $request->getPost('activityRepeatEnabled');
            $activityRepeatComment = $request->getPost('activityRepeatComment');

            $startingTime = '';
            $endingTime = '';
            if ($request->getPost('startTime') != '') {
//                $startingTimes = preg_replace("/(\d+)\D+(\d+)\D+(\d+)/", "$3-$2-$1", $request->getPost('startTime'));
//                $startingTime = date('Y-m-d H:i:s', strtotime($startingTimes));
                $startingTime = $this->getGMTDateTime('Y-m-d H:i:s', $request->getPost('startTime'));
            }
            if ($request->getPost('endTime') != '') {
//                $endingTimes = preg_replace("/(\d+)\D+(\d+)\D+(\d+)/", "$3-$2-$1", $request->getPost('endTime'));
//                $endingTime = date('Y-m-d H:i:s', strtotime($endingTimes));
                $endingTime = $this->getGMTDateTime('Y-m-d H:i:s', $request->getPost('endTime'));
            }

            $activityCode = $request->getPost('activityCode');
            //save activity Data
            $entityID = $this->createEntity();
            $activityData = array(
                'activityId' => $request->getPost('activityID'),
                'locationID' => $locationID,
                'activityCode' => $activityCode,
                'activityName' => $activityName,
                'activityTypeID' => $activityTypeID,
                'activityDescription' => $activityDescription,
                'activityStartingTime' => $startingTime,
                'activityEndTime' => $endingTime,
                'activityEstimateTime' => $activityEstimateTime,
                'activityEstimatedCost' => $activityEstimateCost,
                'projectId' => $projectReferenceID,
                'jobId' => $jobReferenceID,
                'activityWeight' => $activityCurrentWeight,
                'activityProgress' => 0,
                'activitySortOrder' => 0,
                'activityStatus' => 3,
                'entityID' => $entityID,
                'activityRepeatEnabled' => $activityRepeatEnabled,
                'activityRepeatComment' => $activityRepeatComment,
            );
            $activity = new Activity();
            $activity->exchangeArray($activityData);

            if (is_null($request->getPost('activityID'))) {
                while ($activityCode) {
                    if ($this->CommonTable('JobCard\Model\ActivityTable')->getActivityDetailsByActivityCode($activityCode)->current()) {
                        if ($locationReferenceID) {
                            $newActivityCode = $this->getReferenceNumber($locationReferenceID);
                            if ($activityCode == $newActivityCode) {
                                $this->updateReferenceNumber($locationReferenceID);
                                $activityCode = $this->getReferenceNumber($locationReferenceID);
                            } else {
                                $activityCode = $newActivityCode;
                            }
                        } else {
                            $this->rollback();
                            $this->status = false;
                            $this->msg = $this->getMessage('ERR_ACTIVITY_CODE_EXIST');
                            return $this->JSONRespond();
                        }
                    } else {
                        break;
                    }
                }
                $activityID = $this->CommonTable('JobCard\Model\ActivityTable')->saveActivity($activity);
            } else {
                $activityID = $request->getPost('activityID');
                $activityUpdateData = array(
                    'locationID' => $locationID,
                    'activityCode' => $activityCode,
                    'activityName' => $activityName,
                    'activityTypeID' => $activityTypeID,
                    'activityDescription' => $activityDescription,
                    'activityStartingTime' => $startingTime,
                    'activityEndTime' => $endingTime,
                    'activityEstimateTime' => $activityEstimateTime,
                    'activityEstimatedCost' => $activityEstimateCost,
                    'projectId' => $projectReferenceID,
                    'jobId' => $jobReferenceID,
                    'activityRepeatEnabled' => $activityRepeatEnabled,
                    'activityRepeatComment' => $activityRepeatComment,
//                    'activityWeight' => $activityCurrentWeight,
//                    'activityProgress' => 0,
//                    'activitySortOrder' => 0,
//                    'activityStatus' => 3,
                );
                //update activity Table
                $this->CommonTable('JobCard\Model\ActivityTable')->updateActivity($activityUpdateData, $activityID);
                //updates other tables which related to activity
                $data = $this->_updateActivityDetails($activityID);
                if(!$data['status']){
                    $this->rollback();
                    $this->status = false;
                    $this->msg = $data['msg'];
                    return $this->JSONRespond();
                }
            }

            if ($activityID) {

                //save Contractors data
                $contractors = $request->getPost('contractorData');
                if (count($contractors) > 0) {
                    foreach ($contractors as $c) {

                        $contractorData = array(
                            'activityId' => $activityID,
                            'contractorId' => $c['contractorID']
                        );
                        $contractor = new ActivityContractor();
                        $contractor->exchangeArray($contractorData);
                        $this->CommonTable('JobCard\Model\ActivityContractorTable')->saveActivityContractor($contractor);
                    }
                }

                //save temporary Contractors data
                $tempContractors = $request->getPost('tempContractorData');
                if (count($tempContractors) > 0) {
                    foreach ($tempContractors as $tC) {
                        $tempContractorData = array(
                            'activityId' => $activityID,
                            'contractorId' => $tC['tempContractorID']
                        );
                        $tempContractor = new ActivityTemporaryContractor();
                        $tempContractor->exchangeArray($tempContractorData);
                        $this->CommonTable('JobCard\Model\ActivityTemporaryContractorTable')->saveActivityTemporaryContractor($tempContractor);
                    }
                }

                //save activity owners data
                $activityOwners = $request->getPost('activityOwnerData');
                if (count($activityOwners) > 0) {

                    foreach ($activityOwners as $a) {

                        $activityOwnersData = array(
                            'activityID' => $activityID,
                            'employeeID' => $a['activityOwnerID']
                        );
                        $activityOwner = new ActivityOwner();
                        $activityOwner->exchangeArray($activityOwnersData);
                        $this->CommonTable('JobCard\Model\ActivityOwnerTable')->saveActivityOwner($activityOwner);
                    }
                }

                //save activity supervisor data
                $activitySupervisors = $request->getPost('activitySupervisorData');
                if (count($activitySupervisors) > 0) {
                    foreach ($activitySupervisors as $aS) {

                        $activitySupervisorData = array(
                            'activityID' => $activityID,
                            'employeeID' => $aS['activitySupervisorID']
                        );
                        $activitySupervisor = new ActivitySupervisor();
                        $activitySupervisor->exchangeArray($activitySupervisorData);
                        $this->CommonTable('JobCard\Model\ActivitySupervisorTable')->saveActivitySupervisor($activitySupervisor);
                    }
                }
                $quantityUpdatedProductIDs = [];

                //save raw material data
                $rawMaterials = $request->getPost('rawMaterialData');
                if (count($rawMaterials) > 0) {
                    foreach ($rawMaterials as $rM) {
                        $quantityUpdatedProductIDs[] = $rM['rawMaterialProID'];
                        $locationProductData = $this->CommonTable('Inventory\Model\LocationProductTable')->getLocationProduct($rM['rawMaterialProID'], $locationID);
                        $locationProductID = $locationProductData->locationProductID;
                        $averageCostingPrice = $locationProductData->locationProductAverageCostingPrice;
                        $activityRawMaterialData = array(
                            'activityId' => $activityID,
                            'locationProductId' => $locationProductID,
                            'activityRawMaterialQuantity' => $rM['rawMaterialProTotalQty'],
                            'activityRawMaterialCost' => $rM['rawMaterialProUnitPrice'],
                        );
                        $activityRawMaterial = new ActivityRawMaterial();
                        $activityRawMaterial->exchangeArray($activityRawMaterialData);
                        $activityRawMaterialID = $this->CommonTable('JobCard\Model\ActivityRawMaterialTable')->saveActivityRawMaterial($activityRawMaterial);

                        //updates quantity on location product table
                        $newPQty = floatval($locationProductData->locationProductQuantity) - floatval($rM['rawMaterialProTotalQty']);
                        $newPQtyData = array(
                            'locationProductQuantity' => $newPQty
                        );
                        $this->CommonTable('Inventory\Model\LocationProductTable')->updateLocationProductQuantity($newPQtyData, $locationProductID);

                        //if raw material sub products have
                        if (isset($rM['rawMaterialSubProducts']) && is_array($rM['rawMaterialSubProducts'])) {

                            foreach ($rM['rawMaterialSubProducts'] as $v) {

                                if (isset($v['serialID']) && !isset($v['batchID'])) {
                                    $this->_updateSerialProducts($v['serialID'], 1);
                                } else if (!isset($v['serialID']) && isset($v['batchID'])) {
                                    $this->_updateBatchProducts($v['batchID'], $v['qtyByBase']);
                                } else if (isset($v['serialID']) && isset($v['batchID'])) {
                                    $this->_updateBatchSerialProducts($v['serialID'], $v['batchID'], 1);
                                }
                                //save raw material sub products
                                $this->_saveRawMaterialSubProducts($v, $activityRawMaterialID);

                                //save data into itemOut table and update itemIn table data according to locationProductID
                                if ($v['batchID']) {
                                    if ($v['serialID']) {
                                        $itemInDetails = $this->CommonTable('Inventory\Model\ItemInTable')->getFirstAvailableBatchSerialProductDetails($locationProductID, $v['batchID'], $v['serialID']);
                                        $itemOutM = new ItemOut();
                                        $itemOutData = array(
                                            'itemOutDocumentType' => 'Activity',
                                            'itemOutDocumentID' => $activityID,
                                            'itemOutLocationProductID' => $locationProductID,
                                            'itemOutBatchID' => $v['batchID'],
                                            'itemOutSerialID' => $v['serialID'],
                                            'itemOutItemInID' => $itemInDetails['itemInID'],
                                            'itemOutQty' => $v['qtyByBase'],
                                            'itemOutPrice' => $rM['rawMaterialProUnitPrice'],
                                            'itemOutAverageCostingPrice' => $averageCostingPrice,
                                            'itemOutDateAndTime' => $this->getGMTDateTime()
                                        );

                                        $itemOutM->exchangeArray($itemOutData);
                                        $this->CommonTable('Inventory\Model\ItemOutTable')->saveItemOut($itemOutM);
                                        $updateQty = $itemInDetails['itemInSoldQty'] + $v['qtyByBase'];
                                        $this->CommonTable('Inventory\Model\ItemInTable')->updateItemInSoldQtyDetails($itemInDetails['itemInID'], $updateQty);
                                    } else {
                                        $itemInDetails = $this->CommonTable('Inventory\Model\ItemInTable')->getFirstAvailableBatchProductDetails($locationProductID, $v['batchID']);
                                        $itemOutM = new ItemOut();
                                        $itemOutData = array(
                                            'itemOutDocumentType' => 'Activity',
                                            'itemOutDocumentID' => $activityID,
                                            'itemOutLocationProductID' => $locationProductID,
                                            'itemOutBatchID' => $v['batchID'],
                                            'itemOutSerialID' => NULL,
                                            'itemOutItemInID' => $itemInDetails['itemInID'],
                                            'itemOutQty' => $v['qtyByBase'],
                                            'itemOutPrice' => $rM['rawMaterialProUnitPrice'],
                                            'itemOutAverageCostingPrice' => $averageCostingPrice,
                                            'itemOutDateAndTime' => $this->getGMTDateTime()
                                        );
                                        $itemOutM->exchangeArray($itemOutData);
                                        $this->CommonTable('Inventory\Model\ItemOutTable')->saveItemOut($itemOutM);
                                        $updateQty = $itemInDetails['itemInSoldQty'] + $v['qtyByBase'];
                                        $this->CommonTable('Inventory\Model\ItemInTable')->updateItemInSoldQtyDetails($itemInDetails['itemInID'], $updateQty);
                                    }
                                } else if ($v['serialID']) {
                                    $itemInDetails = $this->CommonTable('Inventory\Model\ItemInTable')->getFirstAvailableSerialProductDetails($locationProductID, $v['serialID']);
                                    $itemOutM = new ItemOut();
                                    $itemOutData = array(
                                        'itemOutDocumentType' => 'Activity',
                                        'itemOutDocumentID' => $activityID,
                                        'itemOutLocationProductID' => $locationProductID,
                                        'itemOutBatchID' => NULL,
                                        'itemOutSerialID' => $v['serialID'],
                                        'itemOutItemInID' => $itemInDetails['itemInID'],
                                        'itemOutQty' => 1,
                                        'itemOutPrice' => $rM['rawMaterialProUnitPrice'],
                                        'itemOutAverageCostingPrice' => $averageCostingPrice,
                                        'itemOutDateAndTime' => $this->getGMTDateTime()
                                    );

                                    $itemOutM->exchangeArray($itemOutData);
                                    $this->CommonTable('Inventory\Model\ItemOutTable')->saveItemOut($itemOutM);
                                    $this->CommonTable('Inventory\Model\ItemInTable')->updateItemInSoldQtyDetails($itemInDetails['itemInID'], 1);
                                }
                            }
                        } else {
                            //if there has no raw material sub products(that is mean global product)
                            $sellingQty = $rM['rawMaterialProTotalQty'];
                            while ($sellingQty != 0) {
                                $itemInDetails = $this->CommonTable('Inventory\Model\ItemInTable')->getFirstAvailableProductDetails($locationProductID);
                                if (!$itemInDetails) {
                                    break;
                                }

                                $itemOutData = array(
                                    'itemOutDocumentType' => 'Activity',
                                    'itemOutDocumentID' => $activityID,
                                    'itemOutLocationProductID' => $locationProductID,
                                    'itemOutBatchID' => NULL,
                                    'itemOutSerialID' => NULL,
                                    'itemOutItemInID' => $itemInDetails['itemInID'],
                                    'itemOutQty' => $sellingQty,
                                    'itemOutPrice' => $rM['rawMaterialProUnitPrice'],
                                    'itemOutAverageCostingPrice' => $averageCostingPrice,
                                    'itemOutDateAndTime' => $this->getGMTDateTime()
                                );
                                if ($itemInDetails['itemInRemainingQty'] > $sellingQty) {

                                    $updateQty = $itemInDetails['itemInSoldQty'] + $sellingQty;

                                    $itemOutM = new ItemOut();
                                    $itemOutM->exchangeArray($itemOutData);
                                    $this->CommonTable('Inventory\Model\ItemOutTable')->saveItemOut($itemOutM);
                                    $this->CommonTable('Inventory\Model\ItemInTable')->updateItemInSoldQtyDetails($itemInDetails['itemInID'], $updateQty);
                                    $sellingQty = 0;
                                    break;
                                } else {

                                    $updateQty = $itemInDetails['itemInSoldQty'] + $itemInDetails['itemInRemainingQty'];
                                    $itemOutData['itemOutQty'] = $itemInDetails['itemInRemainingQty'];

                                    $itemOutM = new ItemOut();
                                    $itemOutM->exchangeArray($itemOutData);
                                    $this->CommonTable('Inventory\Model\ItemOutTable')->saveItemOut($itemOutM);
                                    $this->CommonTable('Inventory\Model\ItemInTable')->updateItemInSoldQtyDetails($itemInDetails['itemInID'], $updateQty);
                                    $sellingQty = $sellingQty - $itemInDetails['itemInRemainingQty'];
                                }
                            }
                        }
                    }
                }

                //save fixed assests data
                $fixedAssets = $request->getPost('fixedAssetsData');
                if (count($fixedAssets) > 0) {

                    foreach ($fixedAssets as $f) {
                        $quantityUpdatedProductIDs[] = $f['fixedAssetsProID'];
                        $locationProductData = $this->CommonTable('Inventory\Model\LocationProductTable')->getLocationProduct($f['fixedAssetsProID'], $locationID);
                        $locationProductID = $locationProductData->locationProductID;
                        $activityFixedAssetData = array(
                            'activityId' => $activityID,
                            'locationProductId' => $locationProductData->locationProductID,
                            'activityFixedAssetQuantity' => $f['fixedAssetsProTotalQty'],
                            'activityFixedAssetCost' => $f['fixedAssetsProUnitPrice'],
                        );
                        $activityFixedAsset = new ActivityFixedAsset();
                        $activityFixedAsset->exchangeArray($activityFixedAssetData);
                        $activityFixedAssestsID = $this->CommonTable('JobCard\Model\ActivityFixedAssetTable')->saveActivityFixedAsset($activityFixedAsset);
                        if (isset($f['fixedAssetsSubProducts']) && is_array($f['fixedAssetsSubProducts'])) {
                            foreach ($f['fixedAssetsSubProducts'] as $v) {

                                if (isset($v['serialID']) && !isset($v['batchID'])) {
                                    $this->_updateSerialProducts($v['serialID'], 1);
                                } else if (!isset($v['serialID']) && isset($v['batchID'])) {
                                    $this->_updateBatchProducts($v['batchID'], $v['qtyByBase']);
                                } else if (isset($v['serialID']) && isset($v['batchID'])) {
                                    $this->_updateBatchSerialProducts($v['serialID'], $v['batchID'], 1);
                                }
                                //save fixed assets sub products
                                $this->_saveFixedAssestsSubProducts($v, $activityFixedAssestsID);
                            }
                        }
                    }
                }
                if (count($quantityUpdatedProductIDs) > 0) {
                    $eventParameter = ['productIDs' => $quantityUpdatedProductIDs, 'locationIDs' => [$locationID]];
                    $this->getEventManager()->trigger('productQuantityUpdated', $this, $eventParameter);
                }

                //save temporary products data
                $tempProducts = $request->getPost('tempProductData');
                if ($tempProducts) {
                    foreach ($tempProducts as $t) {
                        // Add temporary product
                        $tempProductData = array(
                            'activityId' => $activityID,
                            'temporaryProductID' => $t['proID'],
                        );
                        $temporaryProduct = new ActivityTemporaryProduct();
                        $temporaryProduct->exchangeArray($tempProductData);
                        $this->CommonTable('JobCard\Model\ActivityTemporaryProductTable')->saveActivityTemporaryProduct($temporaryProduct);
                    }
                }

                //save activity cost types
                $costTypes = $request->getPost('costTypeData');
                if (count($costTypes) > 0) {
                    foreach ($costTypes as $c) {
                        $costTypeData = array(
                            'activityId' => $activityID,
                            'costTypeId' => $c['costTypeID'],
                            'activityCostTypeEstimatedCost' => $c['estimateCost'],
                            'activityCostTypeActualCost' => $c['actualCost'],
                        );
                        $activityCostType = new ActivityCostType();
                        $activityCostType->exchangeArray($costTypeData);
                        $this->CommonTable('JobCard\Model\ActivityCostTypeTable')->saveActivityCostType($activityCostType);
                    }
                }

                //save activity vehicle
                $vehicles = $request->getPost('vehicleData');
                if (count($vehicles) > 0) {
                    foreach ($vehicles as $vehicle) {
                        $data = array(
                            'activityVehicleStartMileage' => $vehicle['activityVehicleStartMileage'],
                            'activityVehicleEndMileage' => $vehicle['activityVehicleEndMileage'],
                            'activityVehicleCost' => $vehicle['activityVehicleCost'],
                            'vehicleId' => $vehicle['vehicleId'],
                            'activityId' => $activityID,
                            'entityId' => $this->createEntity()
                        );
                        $activityVehicle = new ActivityVehicle();
                        $activityVehicle->exchangeArray($data);
                        $this->CommonTable('JobCard\Model\ActivityVehicleTable')->saveActivityVehicle($activityVehicle);
                    }
                }

                $inqID = $request->getPost('inqID');
                if ($inqID) {
                    $inqData = array(
                        'inquiryComplainRelationDocumentID' => $activityID,
                        'inquiryLogID' => $inqID,
                        'documentTypeID' => '23',
                    );
                    $inqDetails = (object) $inqData;
                    $this->CommonTable('JobCard\Model\InquiryComplainRelationTable')->saveRelatedDocument($inqDetails);
                }
                if ($locationReferenceID && !$request->getPost('activityID')) {
                    $this->updateReferenceNumber($locationReferenceID);
                }

                $this->commit();
                $this->status = true;
                $msg = ($isUpdateActivity == 'true') ? 'SUCC_UPDATED_ACTIVITY' : 'SUCC_ADDED_ACTIVITY';
                $this->msg = $this->getMessage($msg, array($activityCode));
                $this->flashMessenger()->addMessage($this->msg);
                $this->data = $activityID;
            } else {
                $this->status = false;
                $this->msg = $this->getMessage('ERR_OCURED_WHILE_ADDED_ACTIVITY', array($activityCode));
                $this->rollback();
            }

            return $this->JSONRespond();
        }
    }

    /**
     * @author Sandun <sandun@thinkcube.com>
     * @param array $data
     * @param int $activityRawMaterialID
     * save raw material products
     */
    private function _saveRawMaterialSubProducts($data, $activityRawMaterialID)
    {
        if (isset($data) && isset($activityRawMaterialID)) {
            $batchID = (isset($data['batchID'])) ? $data['batchID'] : NULL;
            $serialID = (isset($data['serialID'])) ? $data['serialID'] : NULL;

            $rawMaterialSubProductsData = array(
                'activityRawMaterialId' => $activityRawMaterialID,
                'productBatchID' => $batchID,
                'productSerialID' => $serialID,
                'activityRawMaterialSubProductQuantity' => $data['qtyByBase']
            );
            $rawMaterialSubProducts = new ActivityRawMaterialSubProduct();
            $rawMaterialSubProducts->exchangeArray($rawMaterialSubProductsData);
            $this->CommonTable('JobCard\Model\ActivityRawMaterialSubProductTable')->saveActivityRawMaterialSubProduct($rawMaterialSubProducts);
        }
    }

    /**
     * @author Sandun <sandun@thinkcube.com>
     * @param array $data
     * @param int $activityFixedAssetsID
     * save raw fixed assests products
     */
    private function _saveFixedAssestsSubProducts($data, $activityFixedAssetsID)
    {
        if (isset($data) && isset($activityFixedAssetsID)) {
            $batchID = (isset($data['batchID'])) ? $data['batchID'] : NULL;
            $serialID = (isset($data['serialID'])) ? $data['serialID'] : NULL;

            $fixedAssestsSubProductsData = array(
                'activityFixedAssetId' => $activityFixedAssetsID,
                'productBatchID' => $batchID,
                'productSerialID' => $serialID,
                'activityFixedAssetSubProductQuantity' => $data['qtyByBase']
            );
            $fixedAssestsSubProducts = new ActivityFixedAssetSubProduct();
            $fixedAssestsSubProducts->exchangeArray($fixedAssestsSubProductsData);
            $this->CommonTable('JobCard\Model\ActivityFixedAssetSubProductTable')->saveActivityFixedAssetSubProduct($fixedAssestsSubProducts);
        }
    }

    /**
     * @author Sandun <sandun@thinkcube.com>
     * @param array $data
     * update serial products
     */
    private function _updateSerialProducts($serialID, $value)
    {
        if (isset($serialID)) {
            $serialData = array(
                'productSerialSold' => $value
            );
            $this->CommonTable('Inventory\Model\ProductSerialTable')->updateBySerialID($serialData, $serialID);
        }
    }

    /**
     * @author Sandun <sandun@thinkcube.com>
     * @param array $data
     * updates batch products
     */
    private function _updateBatchProducts($batchID, $baseQty)
    {
        if (isset($batchID)) {
            $batchProduct = $this->CommonTable('Inventory\Model\ProductBatchTable')->getBatchProducts($batchID);
            if ($batchProduct->productBatchQuantity < $baseQty) {
                $this->status = false;
                $this->msg = $this->getMessage('REQ_TRAN_REFRESH');
                return $this->JSONRespond();
            }
            $productBatchQuentity = $batchProduct->productBatchQuantity - $baseQty;
            $productBatchQty = array(
                'productBatchQuantity' => $productBatchQuentity,
            );

            $this->CommonTable('Inventory\Model\ProductBatchTable')->updateProductBatchQuantity($batchID, $productBatchQty);
        }
    }

    /**
     * @author Sandun <sandun@thinkcube.com>
     * @param array $data
     * updates batch serial products
     */
    private function _updateBatchSerialProducts($serialID, $batchID, $value)
    {
        if (isset($serialID)) {
            $serialData = array(
                'productSerialSold' => $value
            );
            $this->CommonTable('Inventory\Model\ProductSerialTable')->updateBatchSerialID($serialID, $batchID, $serialData);
        }
    }

    /**
     * @author Sandun <sandun@thinkcube.com>
     * @param int $temporaryProductID
     * @param string $batchCode
     * @param int $batchQty
     * @return $batchID
     * save activity batch items
     */
    private function _saveActivityBatchItems($temporaryProductID, $batchCode, $batchQty)
    {
        $activityBatchData = array(
            'temporaryProductID' => $temporaryProductID,
            'activityBatchCode' => $batchCode,
            'activityBatchQuantity' => $batchQty
        );
        $activityBatch = new ActivityBatch();
        $activityBatch->exchangeArray($activityBatchData);
        $batchID = $this->CommonTable('JobCard\Model\ActivityBatchTable')->saveBatchProduct($activityBatch);
        return $batchID;
    }

    /**
     * @author Sandun <sandun@thinkcube.com>
     * @param int $temporaryProductID
     * @param int $batchID
     * @param string $serialCode
     * @return int $serialID
     */
    private function _saveActivitySerialItems($temporaryProductID, $batchID, $serialCode)
    {
        $activitySerialData = array(
            'temporaryProductID' => $temporaryProductID,
            'activityBatchID' => $batchID,
            'activitySerialCode' => $serialCode,
            'activitySerialSold' => 0
        );
        $activitySerial = new ActivitySerial();
        $activitySerial->exchangeArray($activitySerialData);
        $serialID = $this->CommonTable('JobCard\Model\ActivitySerialTable')->saveSerialProduct($activitySerial);
        return $serialID;
    }

    /**
     * @author Sandun <sandun@thinkcube.com>
     * @param int $activityID
     * delete details regading to activity id
     */
    private function _updateActivityDetails($activityID)
    {
        $this->CommonTable('JobCard\Model\ActivityContractorTable')->deleteActivityContractor($activityID);
        $this->CommonTable('JobCard\Model\ActivityTemporaryContractorTable')->deleteActivityTemporaryContractor($activityID);
        $this->CommonTable('JobCard\Model\ActivityOwnerTable')->deleteActivityOwner($activityID);
        $this->CommonTable('JobCard\Model\ActivitySupervisorTable')->deleteActivitySupervisor($activityID);

        $result = $this->CommonTable('Settings\Model\DisplaySetupTable')->fetchAllDetails();
        $displayData = $result->current();
        $FIFOCostingFlag = $displayData['FIFOCostingFlag'];
        $averageCostingFlag = $displayData['averageCostingFlag'];

        if($FIFOCostingFlag == 0 && $averageCostingFlag == 0){
            return array(
                'status' => false,
                'msg' => $this->getMessage('ERR_COSTING_METHOD_NOT_CHOOSE'),
                );
        }

        $rawMaterialData = $this->CommonTable('JobCard\Model\ActivityRawMaterialTable')->getRawMaterialByActivityID($activityID);
        $locations = [];
        $productIDs = [];
        if (isset($rawMaterialData) && count($rawMaterialData) > 0) {
            foreach ($rawMaterialData as $r) {
                $locations[] = $r['locationID'];
                $productIDs[] = $r['productID'];

                //updates rawMaterial sub products
                $getActivityRMProductData = $this->CommonTable('JobCard\Model\ActivityRawMaterialTable')->getRawMaterialByActivityID($activityID);
                if ($getActivityRMProductData != NULL) {
                    //get all data(batch,serial) which related to delete activityID
                    //get products details by related activityID
                    foreach ($getActivityRMProductData as $key => $g) {
                        $activityRMProducts['products'][$g['productID']] = array(
                            'productID' => $g['productID'],
                            'activityID' => $activityID,
                            'activityRawMaterialQuantity' => $g['activityRawMaterialQuantity'],
                            'locationProductID' => $g['locationProductID']
                        );

                        $rawMeterialSubProducts = [];
                        $getProductData = $this->CommonTable('JobCard\Model\ActivityRawMaterialSubProductTable')->getSubRawMeterialProduct($activityID, $g['locationProductID']);
                        if ($getProductData != NULL) {
                            foreach ($getProductData as $key => $s) {
                                if ($s['productBatchID'] != NULL || $s['productSerialID'] != NULL) {

                                    $rawMeterialSubProducts[] = array(
                                        'batchID' => $s['productBatchID'],
                                        'serialID' => $s['productSerialID'],
                                        'qtyByBase' => $s['activityRawMaterialQuantity'],
                                        'productPrice' => $s['activityRawMaterialCost'],
                                        'locationProductID' => $s['locationProductId'],
                                    );
                                }
                            }
                            $activityRMProducts['subProducts'][$g['productID']] = $rawMeterialSubProducts;
                        }
                    }

                    foreach ($activityRMProducts['products'] as $key => $p) {
                        $batchProducts = $activityRMProducts['subProducts'][$p['productID']];

                        $loreturnQty = $returnQty = $p['activityRawMaterialQuantity'];
                        $locationProductID = $p['locationProductID'];

                        $locationProduct = $this->CommonTable('Inventory\Model\LocationProductTable')->getLocationProductByLocationProductID($locationProductID);

                        $locationProductAverageCostingPrice = $locationProduct->locationProductAverageCostingPrice;
                        $locationProductQuantity = $locationProduct->locationProductQuantity;
                        $locationProductLastItemInID = $locationProduct->locationProductLastItemInID;

                        $locationProductData = array(
                            'locationProductQuantity' => $locationProduct->locationProductQuantity + $loreturnQty,
                        );
                        $this->CommonTable('Inventory\Model\LocationProductTable')->updateQuantityByID($locationProductData, $locationProductID);

                        if (!count($batchProducts) > 0) {

                            $itemReturnQty = $returnQty;
                            //add item in details for non serial and batch products
                            $itemOutDetails = $this->CommonTable('Inventory\Model\ItemOutTable')->getItemOutDetailsForProductByDocument($locationProduct->locationProductID, 'Activity', $activityID);

                            foreach (array_reverse($itemOutDetails) as $itemOutData) {
                                if ($itemReturnQty != 0) {
                                    if ($itemOutData->itemOutQty > $itemOutData->itemOutReturnQty) {
                                        $itemInPreDetails = $this->CommonTable('Inventory\Model\ItemInTable')->getItemInDetails($itemOutData->itemOutItemInID);

                                        $unitPrice = $itemInPreDetails['itemInPrice'];
                                        $unitDiscount = $itemInPreDetails['itemInDiscount'];
                                        if($averageCostingFlag == 1){
                                            $unitPrice = $itemOutData->itemOutAverageCostingPrice;
                                            $unitDiscount = 0;
                                        }

                                        $leftQty = floatval($itemOutData->itemOutQty) - floatval($itemOutData->itemOutReturnQty);
                                        $itemInInsertQty = 0;
                                        $itemOutUpdateReturnQty = 0;
                                        if ($leftQty >= $itemReturnQty) {
                                            $itemInInsertQty = $itemReturnQty;
                                            $itemOutUpdateReturnQty = floatval($itemOutData->itemOutReturnQty) + floatval($itemReturnQty);
                                            $itemReturnQty = 0;
                                        } else {
                                            $itemInInsertQty = $leftQty;
                                            $itemOutUpdateReturnQty = $itemOutData->itemOutQty;
                                            $itemReturnQty = floatval($itemReturnQty) - floatval($leftQty);
                                        }
                                        //hit data to the item in table with left qty
                                        $newItemInIndex = $this->CommonTable('Inventory\Model\ItemInTable')->getNewItemInIndex($itemInPreDetails['itemInIndexID']);
                                        $itemInData = array(
                                            'itemInIndex' => $newItemInIndex,
                                            'itemInDocumentType' => 'Delete Activity Item',
                                            'itemInDocumentID' => $activityID,
                                            'itemInLocationProductID' => $locationProduct->locationProductID,
                                            'itemInBatchID' => NULL,
                                            'itemInSerialID' => NULL,
                                            'itemInQty' => $itemInInsertQty,
                                            'itemInPrice' => $unitPrice,
                                            'itemInDiscount' => $unitDiscount,
                                            'itemInDateAndTime' => $this->getGMTDateTime(),
                                        );
                                        $itemInModel = new ItemIn();
                                        $itemInModel->exchangeArray($itemInData);
                                        $this->CommonTable('Inventory\Model\ItemInTable')->saveItemIn($itemInModel);
                                        //update item out return qty
                                        $this->CommonTable('Inventory\Model\ItemOutTable')->updateItemOutReturnQty($itemOutData->itemOutID, $itemOutUpdateReturnQty);
                                    }
                                } else {
                                    break;
                                }
                            }
                        }

                        if (count($batchProducts) > 0) {
                            foreach ($batchProducts as $batchKey => $batchValue) {
                                $lProductID = $batchValue['locationProductID'];
                                if ($batchValue['qtyByBase'] != 0 && $batchValue['batchID']) {

                                    $batchProduct = $this->CommonTable('Inventory\Model\ProductBatchTable')->getBatchProducts($batchValue['batchID']);
                                    $productBatchQuentity = $batchProduct->productBatchQuantity + $batchValue['qtyByBase'];
                                    $productBatchQty = array(
                                        'productBatchQuantity' => $productBatchQuentity,
                                    );
                                    $this->CommonTable('Inventory\Model\ProductBatchTable')->updateProductBatchQuantity($batchValue['batchID'], $productBatchQty);
                                    if ($batchValue['serialID']) {
                                        //Add details to item in table for batch and serial products
                                        $itemInDetails = $this->CommonTable('Inventory\Model\ItemInTable')->getBatchSerialProductDetails($lProductID, $batchValue['batchID'], $batchValue['serialID']);
                                        $itemOutDetails = $this->CommonTable('Inventory\Model\ItemOutTable')->getBatchSerialItemOutDetails($batchValue['serialID'], $batchValue['batchID'], 'Activity', $activityID);

                                        $unitPrice = $itemInDetails['itemInPrice'];
                                        $unitDiscount = $itemInDetails['itemInDiscount'];
                                        if($averageCostingFlag == 1){
                                            $unitPrice = $itemOutDetails->itemOutAverageCostingPrice;
                                            $unitDiscount = 0;
                                        }

                                        $newItemInIndex = $this->CommonTable('Inventory\Model\ItemInTable')->getNewItemInIndex($itemInDetails['itemInID']);
                                        $itemInData = array(
                                            'itemInIndex' => $newItemInIndex,
                                            'itemInDocumentType' => 'Delete Activity Item',
                                            'itemInDocumentID' => $activityID,
                                            'itemInLocationProductID' => $lProductID,
                                            'itemInBatchID' => $batchValue['batchID'],
                                            'itemInSerialID' => $batchValue['serialID'],
                                            'itemInQty' => 1,
                                            'itemInPrice' => $unitPrice,
                                            'itemInDiscount' => $unitDiscount,
                                            'itemInDateAndTime' => $this->getGMTDateTime(),
                                        );
                                        $itemInModel = new ItemIn();
                                        $itemInModel->exchangeArray($itemInData);
                                        $this->CommonTable('Inventory\Model\ItemInTable')->saveItemIn($itemInModel);
                                        $this->CommonTable('Inventory\Model\ItemOutTable')->updateItemOutReturnQty($itemOutDetails->itemOutID, '1');
                                    } else {
                                        //Add details to item in table for batch products
                                        $itemInDetails = $this->CommonTable('Inventory\Model\ItemInTable')->getBatchProductDetails($lProductID, $batchValue['batchID']);
                                        $itemOutDetails = $this->CommonTable('Inventory\Model\ItemOutTable')->getBatchItemOutDetails($batchValue['batchID'], 'Activity', $activityID);

                                        $unitPrice = $itemInDetails['itemInPrice'];
                                        $unitDiscount = $itemInDetails['itemInDiscount'];
                                        if($averageCostingFlag == 1){
                                            $unitPrice = $itemOutDetails->itemOutAverageCostingPrice;
                                            $unitDiscount = 0;
                                        }

                                        if ($itemInDetails['itemInID'] != NULL) {
                                            $newItemInIndex = $this->CommonTable('Inventory\Model\ItemInTable')->getNewItemInIndex($itemInDetails['itemInID']);
                                            $itemInData = array(
                                                'itemInIndex' => $newItemInIndex,
                                                'itemInDocumentType' => 'Delete Activity Item',
                                                'itemInDocumentID' => $activityID,
                                                'itemInLocationProductID' => $lProductID,
                                                'itemInBatchID' => $batchValue['batchID'],
                                                'itemInSerialID' => NULL,
                                                'itemInQty' => $batchValue['qtyByBase'],
                                                'itemInPrice' => $unitPrice,
                                                'itemInDiscount' => $unitDiscount,
                                                'itemInDateAndTime' => $this->getGMTDateTime(),
                                            );
                                            $itemInModel = new ItemIn();
                                            $itemInModel->exchangeArray($itemInData);
                                            $this->CommonTable('Inventory\Model\ItemInTable')->saveItemIn($itemInModel);
                                            $this->CommonTable('Inventory\Model\ItemOutTable')->updateItemOutReturnQty($itemOutDetails->itemOutID, $batchValue['qtyByBase']);
                                        }
                                    }
                                } else if ($batchValue['qtyByBase'] != 0 && $batchValue['serialID']) {
                                    //Add details to item in table for serial products
                                    $itemInDetails = $this->CommonTable('Inventory\Model\ItemInTable')->getSerialProductDetails($lProductID, $batchValue['serialID']);
                                    $itemOutDetails = $this->CommonTable('Inventory\Model\ItemOutTable')->getSerialItemOutDetails($batchValue['serialID'], 'Activity', $activityID);

                                    $unitPrice = $itemInDetails['itemInPrice'];
                                    $unitDiscount = $itemInDetails['itemInDiscount'];
                                    if($averageCostingFlag == 1){
                                        $unitPrice = $itemOutDetails->itemOutAverageCostingPrice;
                                        $unitDiscount = 0;
                                    }

                                    $newItemInIndex = $this->CommonTable('Inventory\Model\ItemInTable')->getNewItemInIndex($itemInDetails['itemInID']);
                                    $itemInData = array(
                                        'itemInIndex' => $newItemInIndex,
                                        'itemInDocumentType' => 'Delete Activity Item',
                                        'itemInDocumentID' => $activityID,
                                        'itemInLocationProductID' => $lProductID,
                                        'itemInBatchID' => NULL,
                                        'itemInSerialID' => $batchValue['serialID'],
                                        'itemInQty' => 1,
                                        'itemInPrice' => $unitPrice,
                                        'itemInDiscount' => $unitDiscount,
                                        'itemInDateAndTime' => $this->getGMTDateTime(),
                                    );
                                    $itemInModel = new ItemIn();
                                    $itemInModel->exchangeArray($itemInData);
                                    $this->CommonTable('Inventory\Model\ItemInTable')->saveItemIn($itemInModel);
                                    $this->CommonTable('Inventory\Model\ItemOutTable')->updateItemOutReturnQty($itemOutDetails->itemOutID, '1');
                                }
                                if ($batchValue['qtyByBase'] != 0 && $batchValue['serialID']) {
                                    $serialProductData = array(
                                        'productSerialSold' => '0',
                                    );
                                    $this->CommonTable('Inventory\Model\ProductSerialTable')->updateProductSerialData($serialProductData, $batchValue['serialID']);
                                }
                            }
                        }

                        $itemInData = $this->CommonTable('Inventory\Model\ItemInTable')->getItemInProductDetails($locationProductID);
                        $locationPrTotalQty = 0;
                        $locationPrTotalPrice = 0;
                        $newItemInIds = array();
                        if(count($itemInData) > 0){
                            foreach ($itemInData as $key => $value) {
                                if($value->itemInID > $locationProductLastItemInID){
                                    $newItemInIds[] = $value->itemInID;
                                    $remainQty = $value->itemInQty - $value->itemInSoldQty;
                                    if($remainQty > 0){
                                        $itemInPrice = $remainQty*$value->itemInPrice;
                                        $itemInDiscount = $remainQty*$value->itemInDiscount;
                                        $locationPrTotalQty += $remainQty;
                                        $locationPrTotalPrice += $itemInPrice - $itemInDiscount;
                                    }
                                    $locationProductLastItemInID = $value->itemInID;
                                }
                            }
                        }
                        $locationPrTotalQty += $locationProductQuantity;
                        $locationPrTotalPrice += $locationProductQuantity*$locationProductAverageCostingPrice;

                        if($locationPrTotalQty > 0){
                            $newAverageCostinPrice = $locationPrTotalPrice/$locationPrTotalQty;
                            $lpdata = array(
                                'locationProductAverageCostingPrice' => $newAverageCostinPrice,
                                'locationProductLastItemInID' => $locationProductLastItemInID,
                                );
                            $this->CommonTable('Inventory\Model\LocationProductTable')->updateLocationProductByArray($lpdata, $locationProductID);

                            foreach ($newItemInIds as $inID) {
                                $intemInData = array(
                                    'itemInAverageCostingPrice' =>  $newAverageCostinPrice,
                                    );
                                $result = $this->CommonTable('Inventory\Model\ItemInTable')->updateItemInDataByItemInId($intemInData, $inID);
                            }
                        }
                    }
                }
                $this->CommonTable('JobCard\Model\ActivityRawMaterialSubProductTable')->deleteActivitySubRawMaterial($v['activityRawMaterialId']);
                $this->CommonTable('JobCard\Model\ActivityRawMaterialTable')->deleteActivityRawMaterial($activityID);
//                $this->CommonTable('Inventory\Model\ItemOutTable')->deleteActivity($activityID);
            }
        }

        //update fixedAsset products
        $fixedAssetsData = $this->CommonTable('JobCard\Model\ActivityFixedAssetTable')->getFixedAssetsByActivityID($activityID);
        if (isset($fixedAssetsData) && count($fixedAssetsData) > 0) {
            foreach ($fixedAssetsData as $f) {
                //updates quantity on location product table
                $locationProductData = $this->CommonTable('Inventory\Model\LocationProductTable')->getLocationProductByLocationProductID($f['locationProductId']);
                $newPQty = floatval($locationProductData->locationProductQuantity) + floatval($f['activityRawMaterialQuantity']);
                $newPQtyData = array(
                    'locationProductQuantity' => $newPQty
                );
                $this->CommonTable('Inventory\Model\LocationProductTable')->updateLocationProductQuantity($newPQtyData, $f->locationProductId);
                $locations[] = $f['locationID'];
                $productIDs[] = $f['productID'];

                //updates rawMaterial sub products
                $fixedAssetsSubData = $this->CommonTable('JobCard\Model\ActivityFixedAssetSubProductTable')->getDataByFixedAssetsID($f['activityFixedAssetID']);
                if (isset($fixedAssetsSubData)) {
                    foreach ($fixedAssetsSubData as $v) {

                        if (isset($v['productSerialID']) && !isset($v['productBatchID'])) {
                            $this->_updateSerialProducts($v['productSerialID'], 0);
                        } else if (!isset($v['productSerialID']) && isset($v['productBatchID'])) {
                            $this->_updateBatchProducts($v['productBatchID'], $v['activityFixedAssetSubProductQuantity']);
                        } else if (isset($v['productSerialID']) && isset($v['productBatchID'])) {
                            $this->_updateBatchSerialProducts($v['productSerialID'], $v['productBatchID'], 0);
                        }
                        $this->CommonTable('JobCard\Model\ActivityFixedAssetSubProductTable')->deleteActivitySubFixedAssets($v['activityFixedAssetID']);
                    }
                }
            }
        }
        if (count($productIDs) > 0) {
            $eventParameter = ['productIDs' => $productIDs, 'locationIDs' => $locations];
            $this->getEventManager()->trigger('productQuantityUpdated', $this, $eventParameter);
        }

        $this->CommonTable('JobCard\Model\ActivityFixedAssetTable')->deleteActivityFixedAssets($activityID);

        $this->CommonTable('JobCard\Model\ActivityTemporaryProductTable')->deleteActivityTempProdcutDetailsByActivityID($activityID);

        $this->CommonTable('JobCard\Model\ActivityCostTypeTable')->deleteCostTypeByActivityID($activityID);

        $this->CommonTable('JobCard\Model\ActivityVehicleTable')->deleteVehicleByActivityId($activityID);

        return array(
            'status' => true,
            );
    }

    //get activities by job id
    public function getActivitiesByJobIDAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $jobID = $request->getPost('jobID');
            $jobCode = $request->getPost('jobCode');
            $activityData = $this->CommonTable('JobCard\Model\ActivityTable')->getActivityListByJobID($jobID);
            $activitiArray = '';
            foreach ($activityData as $val) {
                $val = (object) $val;
                $customerStatus = $val->customerStatus;
                $progress = ($val->activityProgress != '') ? $val->activityProgress : '0.00';
                $activitiArray[$val->activityId] = $val->activityCode . ' (' . $progress . '%) - ' . $val->activityName;
            }

            if ($activitiArray) {
                if($customerStatus == 2){
                    $this->msg = $this->getMessage('ERR_JOB_CUSTOMER_INACTIVE');
                    $this->status = false;$this->data = array('projectID' => false, 'projectCode' => "");
                    return $this->JSONRespond();

                }
                $this->data = array('activityData' => $activitiArray);
                $this->status = true;
            } else {
               $this->msg = $this->getMessage('ERR_DOSNOT_HAVE_ACTIVITY', [$jobCode]);
                $this->status = false;
                $this->data = array('projectID' => $projectData->projectId, 'projectCode' => $projectData->projectCode . ' (' . $projectData->projectProgress . ")");
            }

            $jobData = (object) $this->CommonTable('JobCard\Model\JobTable')->getJobDetails($jobID)->current();
            if ($jobData->projectId) {
                $projectData = (object) $this->CommonTable('JobCard/Model/ProjectTable')->getProjectDetailsByProjectID($jobData->projectId)->current();
                $this->data = array_merge($this->data, array('projectID' => $projectData->projectId, 'projectCode' => $projectData->projectCode . ' (' . $projectData->projectProgress . "%)"));
            }
            return $this->JSONRespond();
        }
    }

//get activity details by activity id
    public function getActivityByActivityIdAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $activityID = $request->getPost('activityID');
            $activityCode = $request->getPost('activityCode');
            $activityData = $this->CommonTable('JobCard\Model\ActivityTable')->getActivityByActivityID($activityID)->current();

            if ($activityData) {
                $this->data = $activityData['activityProgress'];
                $this->status = true;
            } else {
                $this->msg = $this->getMessage('ERR_DOSNOT_HAVE_ACTIVITY_LIKE_THIS', $activityCode);
                $this->status = false;
            }
            return $this->JSONRespond();
        }
    }

//get activity by search key
    public function searchActivitiesForDropdownAction()
    {
        $searchrequest = $this->getRequest();
        if ($searchrequest->isPost()) {
            $activitySearchKey = $searchrequest->getPost('searchKey');
            $jobID = $searchrequest->getPost('addFlag');
            $locationID = $this->user_session->userActiveLocation["locationID"];
            if ($jobID == '0') {
                $searchDropDownFlag = true;
            }
            if ($jobID) {
                $activities = $this->CommonTable('JobCard\Model\ActivityTable')->searchActivitiesForDropDown($locationID, $activitySearchKey, $jobID, $searchDropDownFlag);
            } else {
                $activities = $this->CommonTable('JobCard\Model\ActivityTable')->searchActivitiesForDropDown($locationID, $activitySearchKey, null, $searchDropDownFlag);
            }
            $activityList = array();
            foreach ($activities as $activity) {
                $temp['value'] = $activity['activityId'];
                $temp['text'] = $activity['activityCode'] . ' - ' . $activity['activityName'];
                $activityList[] = $temp;
            }

            $this->data = array('list' => $activityList);
            return $this->JSONRespond();
        }
    }

    public function getActivityDataAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $activityID = $request->getPost('activityID');
            $activityData = $this->_getActivityDetails($activityID);

            if ($activityData) {
                $this->data = $activityData;
                $this->status = true;
            } else {
                $this->msg = $this->getMessage('ERR_DOSNOT_HAVE_ACTIVITY_LIKE_THIS', $activityID);
                $this->status = false;
            }
            return $this->JSONRespond();
        }
    }

//get activity product details by activityID
    public function getActivityDetailsByActivityIDAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $checkInvoice = true;
            $jobInvoice = true;
            $projectInvoice = true;
            $activityID = $request->getPost('activityID');
            $activityCode = $request->getPost('activityCode');
            $locationID = $this->user_session->userActiveLocation['locationID'];
//get parent job details by activity id
            $result = (object) $this->CommonTable('JobCard\Model\ActivityTable')->getActivityByActivityID($activityID)->current();
            $jobID = $result->jobId;
            if ($jobID) {
                //check whether the activity parent job was alredy invoice or not
                $jobInvoiceDetails = (object) $this->CommonTable('Invoice\Model\InvoiceTable')->getInvoiceDetailsByjobID($jobID);
                if ($jobInvoiceDetails->count() > 0) {
                    $checkInvoice = false;
                    $jobInvoice = false;
                } else {
                    //get parent project details by job id
                    $jobresult = (object) $this->CommonTable('JobCard\Model\JobTable')->getJobDetails($jobID)->current();
                    $projectID = $jobresult->projectId;
                    if ($projectID) {
                        //check whether the job parent project is alredy invoice or not
                        $invoiceDetails = (object) $this->CommonTable('Invoice\Model\InvoiceTable')->getInvoiceDetailsByProjectID($projectID);
                        if ($invoiceDetails->count() > 0) {
                            $checkInvoice = false;
                            $projectInvoice = false;
                        }
                    }
                }
                $cusProfDetails = $this->CommonTable('Invoice\Model\CustomerProfileTable')->getProfileByCustomerProfileID($jobresult->customerProfileID)->current();

                if($cusProfDetails["customerStatus"] == 2){
                    $this->msg = $this->getMessage('ERR_ACTIVITY_CUSTOMER_INACTIVE');
                    $this->status = false;
                    return $this->JSONRespond();
                }

            }

            if ($checkInvoice) {
                $data = $this->getActivityRelatedProductByActivityID($activityID, $locationID);
                if ($data['flag'] == 'hasProducts') {
                    $this->data = array(
                        'activityProducts' => $data['activityProducts'],
                        'customer' => $data['customer'],
                        'locationProducts' => $data['locationProducts'],
                        'customerProfID' => $jobresult->customerProfileID,
                        'customerProfName' => $cusProfDetails['customerProfileName'],
                    );
                    $this->status = true;
                } else if ($data['flag'] == 'invoiced') {
                    $this->msg = $this->getMessage('ERR_PRODUCT_WAS_INVOICED', $activityCode);
                    $this->status = false;
                } else if ($data['flag'] == 'notProducts') {
                    $this->msg = $this->getMessage('ERR_DOSNOT_HAVE_ANY_PRODUCT_FOR_ACTIVITY');
                    $this->status = false;
                }
            } else {
                if (!$jobInvoice) {
                    $this->msg = $this->getMessage('ERR_PRODUCT_WAS_INVOICED_IN_JOB_ACTIVITY', $activityCode);
                    $this->status = false;
                } else if (!$projectInvoice) {
                    $this->msg = $this->getMessage('ERR_PRODUCT_WAS_INVOICED_IN_PROJECT_ACTIVITY', $activityCode);
                    $this->status = false;
                }
            }
            return $this->JSONRespond();
        }
    }

    private function _getActivityDetails($activityID)
    {
        $activityTblResult = $this->CommonTable('JobCard/Model/ActivityTable')->getActivityDetailsByActivityID($activityID);

        $activityDetails = array();
        //activity details
        if (isset($activityTblResult)) {
            foreach ($activityTblResult as $a) {
                $tempData = array();
                $tempData['aID'] = $a['activityId'];
                $tempData['aCD'] = $a['activityCode'];
                $tempData['aName'] = $a['activityName'];
                $tempData['jRefNumber'] = $a['jobReferenceNumber'];
                $tempData['jobID'] = $a['jobId'];
                $tempData['aTypeID'] = $a['activityTypeID'];
                $tempData['aType'] = $a['activityTypeName'];
                $tempData['activityStartingTime'] = $a['activityStartingTime'];
                $tempData['activityEndTime'] = $a['activityEndTime'];
                $tempData['activityEstimateTime'] = $this->timeCalcutator($a['activityEstimateTime']);
                $tempData['aDescription'] = $a['activityDescription'];
                $activityDetails[$a['activityId']] = $tempData;
            }
        }
//contractor data put  into activity details array
        $activityContractorResult = $this->CommonTable('JobCard/Model/ActivityTable')->getActivityContractorDetailsByActivityID($activityID);
        if (isset($activityContractorResult)) {
            foreach ($activityContractorResult as $c) {
                if ($c['contractorId'] != NULL) {
                    $tempData = array();
                    $tempData['cFN'] = $c['contractorFirstName'];
                    $tempData['cSN'] = $c['contractorSecondName'];
                    $contractor['contractors'][$c['contractorId']] = $tempData;
                    $activityDetails[$c['activityId']] = array_merge($activityDetails[$c['activityId']], $contractor);
                }
            }
        }
//temporay contractor put  data into activity details array
        $activityTempContractorResult = $this->CommonTable('JobCard/Model/ActivityTable')->getActivityTempContractorDetailsByActivityID($activityID);
        if (isset($activityTempContractorResult)) {
            foreach ($activityTempContractorResult as $c) {
                if ($c['contractorId'] != NULL) {
                    $tempData = array();
                    $tempData['cTFN'] = $c['contractorFirstName'];
                    $tempData['cTSN'] = $c['contractorSecondName'];
                    $tempContractor['tempContractors'][$c['contractorId']] = $tempData;
                    $activityDetails[$c['activityId']] = array_merge($activityDetails[$c['activityId']], $tempContractor);
                }
            }
        }
//owner data put  into activity details array
        $activityOwnerResult = $this->CommonTable('JobCard/Model/ActivityTable')->getActivityOwnerDetailsByActivityID($activityID);
        if (isset($activityOwnerResult)) {
            foreach ($activityOwnerResult as $e) {
                if ($e['employeeID'] != NULL) {
                    $tempData = array();
                    $tempData['oFN'] = $e['employeeFirstName'];
                    $tempData['oTSN'] = $e['employeeSecondName'];
                    $employee['owners'][$e['employeeID']] = $tempData;
                    $activityDetails[$e['activityId']] = array_merge($activityDetails[$e['activityId']], $employee);
                }
            }
        }
//supervisor data put  into activity details array
        $activitySupervisorResult = $this->CommonTable('JobCard/Model/ActivityTable')->getActivitySupervisorDetailsByActivityID($activityID);
        if (isset($activitySupervisorResult)) {
            foreach ($activitySupervisorResult as $e) {
                if ($e['employeeID'] != NULL) {
                    $tempData = array();
                    $tempData['sFN'] = $e['employeeFirstName'];
                    $tempData['sTSN'] = $e['employeeSecondName'];
                    $supervisors['supervisors'][$e['employeeID']] = $tempData;
                    $activityDetails[$e['activityId']] = array_merge($activityDetails[$e['activityId']], $supervisors);
                }
            }
        }
        //raw material data puts  into activity details array
        $activityRawMResult = $this->CommonTable('JobCard/Model/ActivityTable')->getActivityRawMDetailsByActivityID($activityID);
        if (isset($activityRawMResult)) {

            foreach ($activityRawMResult as $a) {
                $rawMData = array();
                $rawMaterialProducts = (isset($activityDetails[$a['activityId']]['rMProducts'])) ? $activityDetails[$a['activityId']]['rMProducts'] : array();

                if ($a['rMProductID'] != NULL) {

                    $rMBatches = (isset($rawMaterialProducts[$a['rMProductCode']]['bP'])) ? $rawMaterialProducts[$a['rMProductCode']]['bP'] : array();
                    $rMSerials = (isset($rawMaterialProducts[$a['rMProductCode']]['sP'])) ? $rawMaterialProducts[$a['rMProductCode']]['sP'] : array();
                    $rMProduct = (isset($rawMaterialProducts[$a['rMProductCode']]['pP'])) ? $rawMaterialProducts[$a['rMProductCode']]['pP'] : array();

                    $rawMaterialProducts[$a['rMProductCode']] = array('aPID' => $a['rMProductID'], 'aPC' => $a['rMProductCode'], 'aPN' => $a['rMProductName'], 'aPQ' => $a['activityRawMaterialQuantity'], 'aPUom' => $a['uomAbbr'], 'aPrice' => $a['activityRawMaterialCost']);

                    if ($a['productBatchID'] != NULL && $a['productSerialID'] == NULL) {
                        $rMBatches[$a['productBatchID']] = array('bID' => $a['productBatchID'], 'bC' => $a['rMBatchCode'], 'bQ' => $a['activityRawMaterialQuantity']);
                    }

                    if ($a['productSerialID']) {
                        $rMSerials[$a['productSerialID']] = array('sID' => $a['productSerialID'], 'bID' => $a['productBatchID'], 'sC' => $a['rMSerialCode'], 'bC' => $a['rMBatchCode']);
                    }

                    if ($a['productBatchID'] == NULL && $a['productSerialID'] == NULL) {
                        $rMProduct[$a['rMLPID']] = array('pC' => $a['rMProductCode']);
                    }

                    $rawMaterialProducts[$a['rMProductCode']]['bP'] = $rMBatches;
                    $rawMaterialProducts[$a['rMProductCode']]['sP'] = $rMSerials;
                    $rawMaterialProducts[$a['rMProductCode']]['pP'] = $rMProduct;
                }
                $rawMData['rMProducts'] = $rawMaterialProducts;
                $activityDetails[$a['activityId']] = array_merge($activityDetails[$a['activityId']], $rawMData);
            }
        }

        //fixed assets data puts  into activity details array
        $activityFixedAResult = $this->CommonTable('JobCard/Model/ActivityTable')->getActivityFixedAssetDetailsByActivityID($activityID);
        if (isset($activityFixedAResult)) {

            foreach ($activityFixedAResult as $a) {
                $tempData = array();
                $fixedAssetsProducts = (isset($activityDetails[$a['activityId']]['fAProducts'])) ? $activityDetails[$a['activityId']]['fAProducts'] : array();

                if ($a['fAProductID'] != NULL) {

                    $fABatches = (isset($fixedAssetsProducts[$a['fAProductCode']]['bP'])) ? $fixedAssetsProducts[$a['fAProductCode']]['bP'] : array();
                    $fASerials = (isset($fixedAssetsProducts[$a['fAProductCode']]['sP'])) ? $fixedAssetsProducts[$a['fAProductCode']]['sP'] : array();
                    $fAProduct = (isset($fixedAssetsProducts[$a['fAProductCode']]['pP'])) ? $fixedAssetsProducts[$a['fAProductCode']]['pP'] : array();

                    $fixedAssetsProducts[$a['fAProductCode']] = array('aPID' => $a['fAProductID'], 'aPC' => $a['fAProductCode'], 'aPN' => $a['fAProductName'], 'aPQ' => $a['activityFixedAssetQuantity'], 'aPUom' => $a['uomAbbr'], 'uomID' => $a['uomID'], 'aPrice' => $a['activityFixedAssetCost']);

                    if ($a['fABatchID'] != NULL && $a['fASerialID'] == NULL) {
                        $fABatches[$a['fABatchID']] = array('bID' => $a['fABatchID'], 'bC' => $a['fABatchCode'], 'bQ' => $a['activityFixedAssetSubProductQuantity']);
                    }

                    if ($a['fASerialID']) {
                        $fASerials[$a['fASerialID']] = array('sC' => $a['fASerialCode'], 'bC' => $a['fABatchCode'], 'bID' => $a['fABatchID'], 'sID' => $a['fASerialID']);
                    }

                    if ($a['fABatchID'] == NULL && $a['fASerialID'] == NULL) {
                        $fAProduct[$a['fALPID']] = array('pC' => $a['fAProductCode']);
                    }

                    $fixedAssetsProducts[$a['fAProductCode']]['bP'] = $fABatches;
                    $fixedAssetsProducts[$a['fAProductCode']]['sP'] = $fASerials;
                    $fixedAssetsProducts[$a['fAProductCode']]['pP'] = $fAProduct;
                }

                $tempData['fAProducts'] = $fixedAssetsProducts;
                $activityDetails[$a['activityId']] = array_merge($activityDetails[$a['activityId']], $tempData);
            }
        }
//puts temporay products
        $activityTempProductResult = $this->CommonTable('JobCard/Model/ActivityTemporaryProductTable')->getActivityTempProdcutDetailsByActivityID($activityID);
        if (isset($activityTempProductResult)) {

            foreach ($activityTempProductResult as $p) {
                $tempData = array();
                $activityTempProducts = (isset($activityDetails[$a['activityId']]['tempProducts'])) ? $activityDetails[$a['activityId']]['tempProducts'] : array();
                if ($p['temporaryProductID'] != NULL) {

                    $activityTempProducts[$p['temporaryProductID']] = array('aPID' => $p['temporaryProductID'], 'tPC' => $p['temporaryProductCode'], 'tPN' => $p['temporaryProductName'], 'tPQ' => $p['temporaryProductQuantity'], 'tPD' => $p['temporaryProductDescription'], 'tPUPrice' => $p['temporaryProductPrice'], 'aPUom' => $p['uomAbbr'], 'uomID' => $p['uomID'], 'bCode' => $p['temporaryProductBatch'], 'sCode' => $p['temporaryProductSerial']);
                }

                $tempData['tempProducts'] = $activityTempProducts;
                $activityDetails[$p['activityId']] = array_merge($activityDetails[$p['activityId']], $tempData);
            }
        }
//cost type data  put into activity details array
        $activityCostTypeResult = $this->CommonTable('JobCard/Model/ActivityTable')->getActivityCostTypeDetailsByActivityID($activityID);
        if (isset($activityCostTypeResult)) {
            foreach ($activityCostTypeResult as $t) {
                if ($t['costTypeId'] != NULL) {
                    $tempData = array();
                    $tempData['cTN'] = $t['costTypeName'];
                    $tempData['cTID'] = $t['costTypeId'];
                    $tempData['cTEstimateCost'] = $t['activityCostTypeEstimatedCost'];
                    $tempData['cTActualCost'] = $t['activityCostTypeActualCost'];
                    $costType['costTypes'][$t['costTypeId']] = $tempData;
                    $activityDetails[$t['activityId']] = array_merge($activityDetails[$t['activityId']], $costType);
                }
            }
        }

        //get activity vehicle details
        $activityVehicles = $this->CommonTable('JobCard/Model/ActivityVehicleTable')->getActivityVehiclesByActivityId($activityID);
        $vehicleList = [];
        foreach ($activityVehicles as $vehicle) {
            $data = array();
            $data['vehicleId'] = $vehicle['vehicleId'];
            $data['registrationNumber'] = $vehicle['vehicleRegistrationNumber'];
            $data['startMileage'] = $vehicle['activityVehicleStartMileage'];
            $data['endMileage'] = $vehicle['activityVehicleEndMileage'];
            $data['distance'] = ($vehicle['activityVehicleEndMileage'] - $vehicle['activityVehicleStartMileage']);
            $data['cost'] = $vehicle['activityVehicleCost'];
            $vehicleList[] = $data;
        }
        $activityDetails[$vehicle['activityId']]['vehicle'] = $vehicleList;

        return $activityDetails;
    }

    public function deleteAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $activityID = $request->getPost('activityID');
            $activityData = $this->CommonTable('JobCard/Model/ActivityTable')->getActivityByID($activityID);
            $entityID = $activityData->current()['entityID'];
            $updateEntity = $this->updateDeleteInfoEntity($entityID);
            $data = $this->_updateActivityDetails($activityID);
            if(!$data['status']){
                $this->status = false;
                $this->msg = $data['msg'];
                return $this->JSONRespond();
            }
            //delete that temp product
            $this->CommonTable('JobCard\Model\ActivityTemporaryProductTable')->deleteActivityTempProdcutDetailsByActivityID($activityID);

            if ($updateEntity) {
                $updateData = array(
                    'activityId' => $activityID,
                    'activityStatus' => 5
                );
                $activity = new Activity();
                $activity->exchangeArray($updateData);
                $this->CommonTable('JobCard\Model\ActivityTable')->updateActivityStatus($activity);

                $this->status = true;
                $this->msg = $this->getMessage('SUCC_ACT_DELETE');
                $this->flashMessenger()->addMessage($this->msg);
                $this->commit();
            } else {
                $this->status = false;
                $this->msg = $this->getMessage('ERR_ACT_DELETE');
                $this->rollback();
            }

            return $this->JSONRespond();
        }
    }

    public function getActivityBySearchKeyAction()
    {
        $request = $this->getRequest();
        $locationID = $this->user_session->userActiveLocation['locationID'];
        if ($request->isPost()) {
            $paginated = false;
            $activitySearchKey = $request->getPost('searchKey');

            if (!empty($activitySearchKey)) {
                $activityList = $this->CommonTable('JobCard/Model/ActivityTable')->activitySearchByKey($activitySearchKey, $locationID);
                $this->status = true;
            } else {
                $this->status = true;
                $activityList = $this->getPaginatedActivity($locationID);
            }

            $view = new ViewModel(array(
                'activityList' => $activityList,
                'paginated' => $paginated,
                'status' => $this->getStatusesList(),
            ));
            $view->setTerminal(true);
            $view->setTemplate('job-card/activity/list.phtml');

            $this->html = $view;
            return $this->JSONRespondHtml();
        }
    }

    protected function getPaginatedActivity($locationID, $perPage = 6)
    {
        $this->paginator = $this->CommonTable('JobCard\Model\ActivityTable')->fechAll($locationID, true);
        $this->paginator->setCurrentPageNumber((int) $this->params()->fromRoute('param1', 1));
        $this->paginator->setItemCountPerPage($perPage);
        $this->setLogMessage('Activity Type list accessed');
        return $this->paginator;
    }

    public function getAllActivityWeightAction()
    {
        $request = $this->getRequest();
        $locationID = $this->user_session->userActiveLocation['locationID'];
        if ($request->isPost()) {
            $jobID = $request->getPost('jobID');
            $activityData = (object) $this->CommonTable('JobCard\Model\ActivityTable')->getActivityListByJobID($jobID, $locationID);
            foreach ($activityData as $a) {
                $activityDataList[$a['activityId']] = array('activityName' => $a['activityName'], 'activityCode' => $a['activityCode'], 'activityWeight' => $a['activityWeight']);
                $activityWeightSum+=$a['activityWeight'];
            }
            if (!$activityWeightSum) {
                $activityWeightSum = 0;
            }
            $totalData = array('activityDataList' => $activityDataList, 'activityWeightSum' => $activityWeightSum);
            if ($activityData) {
                $this->data = $totalData;
                $this->status = true;
            } else {
                $this->status = false;
            }

            $this->setLogMessage("Retrive activity list for dropdown.");
            return $this->JSONRespond();
        }
    }

    public function updateWeightAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $weightIds = $request->getPost('weightIds');
            $weightValues = $request->getPost('weightValues');
            $jobID = $request->getPost('jobID');
            for ($i = 1; $i < sizeof($weightIds); $i++) {
                $data = array(
                    'activityId' => $weightIds[$i],
                    'jobId' => $jobID,
                    'activityWeight' => $weightValues[$i],
                );
                $updateWeight = $this->CommonTable('JobCard/Model/ActivityTable')->updateActivityWeight($data);
            }
            if ($updateWeight) {
                $this->status = true;
            } else {
                $this->status = false;
            }
            return $this->JSONRespond();
        }
    }

    /**
     * @author Sandun <sandun@thinkcube.com>
     * Search activity types for dropdown
     * @return type
     */
    public function searchActivityTypesForDropdownAction()
    {
        $searchrequest = $this->getRequest();
        if ($searchrequest->isPost()) {
            $searchKey = $searchrequest->getPost('searchKey');
            $activityTypes = $this->CommonTable('JobCard\Model\ActivityTypeTable')->searchActivityTypesForDropDown($searchKey);
            $activityTypeList = array();
            foreach ($activityTypes as $a) {
                $temp['value'] = $a['activityTypeID'];
                $temp['text'] = $a['activityTypeName'];
                $activityTypeList[] = $temp;
            }

            $this->data = array('list' => $activityTypeList);
            return $this->JSONRespond();
        }
    }

    /**
     * @author Sandun <sandun@thinkcube.com>
     * Search job reference for dropdown
     * @return type
     */
    public function searchJobReferenceForDropdownAction()
    {
        $searchrequest = $this->getRequest();
        if ($searchrequest->isPost()) {
            $jobSearchKey = $searchrequest->getPost('searchKey');
            $addFlag = $searchrequest->getPost('addFlag');

            $locationID = $this->user_session->userActiveLocation["locationID"];
            $jobReferences = $this->CommonTable('JobCard/Model/JobTable')->searchJobsForDropDown($locationID, $jobSearchKey, $addFlag);

//            $otherOptions[] = ["value" => "reset", "text" => "Reset Select"];

            $jobRefList = array();
            foreach ($jobReferences as $j) {
                $temp['value'] = $j['jobId'];
                $temp['text'] = $j['jobReferenceNumber'];
                $jobRefList[] = $temp;
            }

            $this->data = array('list' => $jobRefList, 'otherOptions' => $otherOptions);
            return $this->JSONRespond();
        }
    }

    /**
     * @author Sandun <sandun@thinkcube.com>
     * Search contractor for dropdown
     * @return type
     */
    public function searchContractorForDropdownAction()
    {
        $searchrequest = $this->getRequest();
        if ($searchrequest->isPost()) {
            $searchKey = $searchrequest->getPost('searchKey');
            $jobReferences = $this->CommonTable('JobCard\Model\ContractorTable')->searchContractorForDropDown($searchKey);
            $jobRefList = array();
            foreach ($jobReferences as $j) {
                $temp['value'] = $j['contractorID'];
                $temp['text'] = $j['contractorFirstName'] . ' - ' . $j['contractorSecondName'];
                $jobRefList[] = $temp;
            }

            $this->data = array('list' => $jobRefList);
            return $this->JSONRespond();
        }
    }

    /**
     * @author Sandun <sandun@thinkcube.com>
     * Search Temporary contractor for dropdown
     * @return type
     */
    public function searchTempContractorForDropdownAction()
    {
        $searchrequest = $this->getRequest();
        if ($searchrequest->isPost()) {
            $searchKey = $searchrequest->getPost('searchKey');
            $jobReferences = $this->CommonTable('JobCard\Model\TemporaryContractorTable')->searchTempContractorForDropdown($searchKey);
            $jobRefList = array();
            foreach ($jobReferences as $j) {
                $temp['value'] = $j['temporaryContractorID'];
                $temp['text'] = $j['temporaryContractorFirstName'] . ' - ' . $j['temporaryContractorSecondName'];
                $jobRefList[] = $temp;
            }

            $this->data = array('list' => $jobRefList);
            return $this->JSONRespond();
        }
    }

    public function searchEmployeeForDropdownAction()
    {
        $searchrequest = $this->getRequest();
        if ($searchrequest->isPost()) {
            $searchKey = $searchrequest->getPost('searchKey');
            $employees = $this->CommonTable('JobCard\Model\EmployeeTable')->searchEmployeeForDropdown($searchKey);
            $employeeList = array();
            foreach ($employees as $e) {
                $temp['value'] = $e['employeeID'];
                $temp['text'] = $e['employeeFirstName'] . ' - ' . $e['employeeSecondName'];
                $employeeList[] = $temp;
            }

            $this->data = array('list' => $employeeList);
            return $this->JSONRespond();
        }
    }

    public function searchRawMaterialProductForDropdownAction()
    {
        $searchrequest = $this->getRequest();
        if ($searchrequest->isPost()) {
            //add flag is considered to get without batch and serial product
            $addFlag = $searchrequest->getPost('addFlag');
            $searchKey = $searchrequest->getPost('searchKey');
            $rawMaterialProData = $this->CommonTable('Inventory\Model\ProductTable')->getRawMaterialItems($searchKey, $addFlag);
            $rawMaterialList = array();
            foreach ($rawMaterialProData as $r) {
                $temp['value'] = $r['productID'];
                $temp['text'] = $r['productName'] . ' - ' . $r['productCode'];
                $rawMaterialList[] = $temp;
            }

            $this->data = array('list' => $rawMaterialList);
            return $this->JSONRespond();
        }
    }

    public function searchFixedAssetsProductForDropdownAction()
    {
        $searchrequest = $this->getRequest();
        if ($searchrequest->isPost()) {

            $searchKey = $searchrequest->getPost('searchKey');
            $fixedAssetsProData = $this->CommonTable('Inventory\Model\ProductTable')->getFixedAssetsItems($searchKey);
            $fixedAssetsList = array();
            foreach ($fixedAssetsProData as $f) {
                $temp['value'] = $f['productID'];
                $temp['text'] = $f['productName'] . ' - ' . $f['productCode'];
                $fixedAssetsList[] = $temp;
            }

            $this->data = array('list' => $fixedAssetsList);
            return $this->JSONRespond();
        }
    }

    public function searchCostTypeForDropdownAction()
    {
        $searchrequest = $this->getRequest();
        if ($searchrequest->isPost()) {

            $searchKey = $searchrequest->getPost('searchKey');
            $costTypesData = $this->CommonTable('JobCard\Model\CostTypeTable')->searchCostTypeForDropdown($searchKey);
            $costTypeList = array();
            foreach ($costTypesData as $f) {
                $temp['value'] = $f['costTypeID'];
                $temp['text'] = $f['costTypeName'];
                $costTypeList[] = $temp;
            }

            $this->data = array('list' => $costTypeList);
            return $this->JSONRespond();
        }
    }

    /**
     * Save uploaded images into user wise temporary folder
     * @author Sharmilan <sharmilan@thinkcube.com>
     * @return array saved image info
     */
    public function saveImageAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            foreach ($request->getFiles() as $uploadFile) {
                $fileName = $uploadFile[0]['name'];

                //  if file path is same then repeat this with different path name
                do {
                    //  Generate a md5 string using filename and current time
                    $filePathCrpt = md5($fileName . microtime() . rand(0, 1000));
                    //  Create folder structure without having same file inside same folder
                    $folderPath = substr($filePathCrpt, 0, 4) . '/' . substr($filePathCrpt, 3, 4) . '/';
                    $folderFullPath = $this->getTemporaryAttachmentDirectoryPath() . $folderPath;

                    if (!$fileExists = file_exists($folderFullPath . $fileName)) {
                        //  save image
                        $upload_handler = $this->UploadHandler()->__construct([
                            'upload_dir' => $folderFullPath,
                            'script_url' => 'api/activity/delete-image',
                            'folderPath' => $folderPath,
                        ]);
                        $resp = $this->UploadHandler()->post();
                    }
                } while ($fileExists);
            }
            $this->data = $resp;
        }
        return $this->JSONRespond();
    }

    /**
     * Remove Image from Temporary saved image
     * @author Sharmilan <sharmilan@thinkcube.com>
     */
    public function deleteImageAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $filePath = $request->getPost('folderPath');
            $fileName = urldecode($request->getPost('fileName'));

            $upload_handler = $this->UploadHandler()->__construct([
                'upload_dir' => $this->getTemporaryAttachmentDirectoryPath() . $filePath,
            ]);
            $response = $this->UploadHandler()->deleteImage($fileName);
            $this->data = $response;
        }
        return $this->JSONRespond();
    }

    public function getActivityByProjectIDAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $projectID = $request->getPost('projectID');
            $projectCode = $request->getPost('projectCode');
            $activityData = $this->CommonTable('JobCard\Model\ActivityTable')->getActivityByProjectID($projectID);
            $activityList = '';
            foreach ($activityData as $val) {
                $val = (object) $val;
                $progress = ($val->activityProgress != '') ? $val->activityProgress : '0.00';
                $activityList[$val->activityId] = $val->activityCode . ' (' . $progress . '%)';
            }

            if ($activityList) {
                $this->data = $activityList;
                $this->status = true;
            } else {
                $this->msg = $this->getMessage('ERR_DOSNOT_HAVE_ACTIVITYS', $projectCode);
                $this->status = false;
            }
            return $this->JSONRespond();
        }
    }

    /**
     * Get Temporary details by temporary product details with images
     * @author Sharmilan <sharmilan@thinkcubecom>
     * @return type
     */
    public function getTemporaryProductDetailsAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $temporaryProductID = $request->getPost('temporaryProductID');
            $temporaryProducts = $this->CommonTable('JobCard\Model\TemporaryProductTable')->getTemporaryProductDetailByID($temporaryProductID);
            foreach ($temporaryProducts as $temporaryProduct) {
                $temporaryProduct = (object) $temporaryProduct;
                $temporaryProductData['tPC'] = $temporaryProduct->temporaryProductCode;
                $temporaryProductData['tPN'] = $temporaryProduct->temporaryProductName;
                $temporaryProductData['tPD'] = $temporaryProduct->temporaryProductDescription;
                $temporaryProductData['tPQ'] = $temporaryProduct->temporaryProductQuantity;
                $temporaryProductData['aPUom'] = $temporaryProduct->uomAbbr;
                $temporaryProductData['tPP'] = number_format($temporaryProduct->temporaryProductPrice, 2) ;
                $temporaryProductData['bCode'] = $temporaryProduct->temporaryProductBatch;
                $temporaryProductData['sCode'] = $temporaryProduct->temporaryProductSerial;
                $temporaryProductData['images'][] = $this->getImageAttachmentsDirectoryPath(true) . $temporaryProduct->attachmentPath . $temporaryProduct->attachmentName;
            }

            $this->data = array('tProductData' => $temporaryProductData);
            $this->status = true;
            return $this->JSONRespond();
        }
    }

    /**
     * Get Activitis list For Typeahead
     * @author sharmilan <sharmilan@thinkcube.com>
     * @return type
     */
    public function getActivitiesForTypeaheadAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $activitySearchKey = $request->getPost('query');

            $locationID = $this->user_session->userActiveLocation["locationID"];
            $activities = $this->CommonTable('JobCard/Model/ActivityTable')->searchActivitiesForTypeahead($locationID, $activitySearchKey);
            $activityList = [];
            foreach ($activities as $activity) {
                $activityList[] = $activity['activityName'];
            }
            $this->data = $activityList;
            $this->status = true;
            return $this->JSONRespond();
        }
    }

    /**
     * Get All Activities for dropdown by search
     * @author sharmilan <sharmilan@thinkcube.com>
     * @return type
     */
    public function getAllActivitiesForDropdownAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $activitySearchKey = $request->getPost('searchKey');
            $locationID = $request->getPost('locationID');
            if ($locationID == '' || $locationID == null) {
                $locationID = $this->user_session->userActiveLocation["locationID"];
            }
            $activities = $this->CommonTable('JobCard/Model/ActivityTable')->searchAllActivitiesForDropdown($locationID, $activitySearchKey);
            $activityList = [];
            foreach ($activities as $activity) {
                $temp['value'] = $activity['activityId'];
                $temp['text'] = $activity['activityCode'] . ' - ' . $activity['activityName'];
                $activityList[] = $temp;
            }

            $this->setLogMessage("Retrive activity list for dropdown.");
            $this->data = array('list' => $activityList);
            return $this->JSONRespond();
        }
    }

    /**
     * Get Activity List View by search
     * @author sharmilan <sharmilan@thinkcube.com>
     * @return type
     */
    public function getActivityListViewBySearchAction()
    {
        $request = $this->getRequest();
        $locationID = $this->user_session->userActiveLocation['locationID'];
        if ($request->isPost()) {
            $paginated = false;
            $activityID = $request->getPost('activityID');

            if ($activityID) {
                $activityList = $this->CommonTable('JobCard/Model/ActivityTable')->getActivityListView($locationID, $activityID);
            } else {
                $paginated = true;
                $activityList = $this->getPaginatedActivity($locationID);
            }

            $view = new ViewModel(array(
                'activityList' => $activityList,
                'paginated' => $paginated,
                'status' => $this->getStatusesList(),
            ));
            $view->setTerminal(true);
            $view->setTemplate('job-card/activity/list-table.phtml');

            $this->status = true;
            $this->html = $view;
            return $this->JSONRespondHtml();
        }
    }

    /**
     * Get Activity List View by date range
     * @author sharmilan <sharmilan@thinkcube.com>
     * @return type
     */
    public function getActivityListViewDateRangeAction()
    {
        $request = $this->getRequest();
        $locationID = $this->user_session->userActiveLocation['locationID'];
        if ($request->isPost()) {
            $fromDate = $this->getGMTDateTime('Y-m-d H:i:s', $request->getPost('fromDate') . ' 00:00');
            $toDate = $this->getGMTDateTime('Y-m-d H:i:s', $request->getPost('toDate') . ' 23:59');

            $activityList = $this->CommonTable('JobCard/Model/ActivityTable')->getActivityListViewByDateRange($locationID, $fromDate, $toDate);

            $view = new ViewModel(array(
                'activityList' => $activityList,
                'paginated' => false,
                'status' => $this->getStatusesList(),
            ));
            $view->setTerminal(true);
            $view->setTemplate('job-card/activity/list-table.phtml');

            $this->status = true;
            $this->html = $view;
            return $this->JSONRespondHtml();
        }
    }

    public function getActivityTypesAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $activityTypeID = $request->getPost('activityTypeID');
            $activityType = $this->CommonTable('JobCard/Model/ActivityTypeTable')->getActivityTypeDataByActivityTypeID($activityTypeID)->current();
            if ($activityType) {
                $this->data = $activityType['activityTypeName'];
                $this->status = true;
            } else {
                $this->status = false;
            }
            return $this->JSONRespond();
        }
    }

    public function getAllRelatedDocumentsforActivityAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $jobID = $request->getPost('jobID');
            $ActivityID = $request->getPost('activityID');
            $locationID = $this->user_session->userActiveLocation["locationID"];
            $parentJob = $this->CommonTable('JobCard/Model/JobTable')->getJobDetails($jobID)->current();
            if ($parentJob['projectId']) {
                $jobs = $this->CommonTable('JobCard/Model/JobTable')->getJobListByProjectID($parentJob['projectId'], $locationID);
                $project = $this->CommonTable('JobCard/Model/ProjectTable')->getProjectDetailsByProjectID($parentJob['projectId'])->current();
            }

            $activities = $this->CommonTable('JobCard/Model/activityTable')->getActivityListByJobID($jobID, $locationID);
            $inquiryLogs = $this->CommonTable('JobCard/Model/inquiryLogTable')->getInquirylogsRelatedToActivityID($ActivityID);
            if (isset($project)) {
                $projectList['projectId'] = array(
                    'name' => $project['projectName'],
                    'code' => $project['projectCode'],
                    'createTime' => $project['createdTimeStamp'],
                    'startTime' => $project['projectStartingAt']
                );
            }
            $jobList = [];
            $activityList = [];
            $inquiryLogList = [];
            if (isset($jobs)) {
                foreach ($jobs as $job) {
                    $jobList[$job['jobId']] = array(
                        'name' => $job['jobName'],
                        'code' => $job['jobReferenceNumber'],
                        'createTime' => $job['createdTimeStamp'],
                        'startTime' => $job['jobStartingTime'],
                        'jobID' => $job['jobId']
                    );
                }
            }
            if (isset($activities)) {
                foreach ($activities as $activity) {
                    $activityList[$activity['activityId']] = array(
                        'name' => $activity['activityName'],
                        'code' => $activity['activityCode'],
                        'createTime' => $activity['createdTimeStamp'],
                        'startTime' => $activity['activityStartingTime'],
                        'activityID' => $activity['activityId']
                    );
                }
            }

            if (isset($inquiryLogs)) {
                foreach ($inquiryLogs as $inquiryLog) {
                    $inquiryLogList[$inquiryLog['inquiryLogID']] = array(
                        'name' => $inquiryLog['inquiryLogDescription'],
                        'code' => $inquiryLog['inquiryLogReference'],
                        'createTime' => $inquiryLog['inquiryLogDateAndTime'],
                        'startTime' => $inquiryLog['inquiryLogDateAndTime'],
                        'inquiryLogID' => $inquiryLog['inquiryLogID']
                    );
                }
            }
            $jobRelatedDetails['project'] = $projectList;
            $jobRelatedDetails['job'] = $jobList;
            $jobRelatedDetails['activity'] = $activityList;
            $jobRelatedDetails['inquiryLog'] = $inquiryLogList;

            $this->data = $jobRelatedDetails;
            $this->status = true;
            return $this->JSONRespond();
        }
    }

    //Update returned location Products
    public function updateLocationProductQty($locationPID, $returnedQty)
    {
        $currentPQty = $this->CommonTable('Inventory\Model\LocationProductTable')->getLocationProductByLocationProductID($locationPID);
        $newPQty = floatval($currentPQty->locationProductQuantity) + floatval($returnedQty);
        $newPQtyData = array(
            'locationProductQuantity' => $newPQty
        );
        $this->CommonTable('Inventory\Model\LocationProductTable')->updateLocationProductQuantity($newPQtyData, $locationPID);
    }

    public function changeActivityStatusAction()
    {
        $this->status = false;
        $this->msg = $this->getMessage('ERR_INVALID_REQUEST');
        $this->data = null;

        $request = $this->getRequest();
        if ($request->isPost()) {
            $postData = $request->getPost()->toArray();
            //check whether activity exist
            $activity = $this->CommonTable('JobCard/Model/ActivityTable')->getActivityByID($postData['activityId']);
            if ($activity) {
                try {
                    $this->beginTransaction();
                    //if status complete
                    if ($postData['activityStatus'] == 9) {
                        $postData['activityProgress'] = 100;
                    }

                    $updateStatue = $this->CommonTable('JobCard\Model\ActivityTable')->updateActivity($postData, $postData['activityId']);
                    if ($updateStatue) {
                        $this->commit();
                        $this->status = true;
                        $this->msg = $this->getMessage('SUCC_UPDATED_ACTIVITY', '');
                    } else {
                        $this->msg = $this->getMessage('ERR_UPDATED_ACTIVITY');
                    }
                } catch (\Exception $ex) {
                    $this->rollback();
                    $this->msg = $this->getMessage('ERR_UPDATED_ACTIVITY');
                }
            } else {
                $this->getMessage('ERR_ACTIVITY_DOSNOT_EXIST');
            }
        }

        return $this->JSONRespond();
    }

}
