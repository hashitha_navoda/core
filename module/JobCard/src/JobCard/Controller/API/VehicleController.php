<?php

/**
 * @author Madawa Chandrarathne <madawa@thinkcube.com>
 * This file vehicle related api controller actions
 */

namespace JobCard\Controller\API;

use Core\Controller\CoreController;
use JobCard\Form\VehicleForm;
use JobCard\Model\Vehicle;
use Zend\View\Model\ViewModel;

class VehicleController extends CoreController
{

    /**
     * Create Vehicle
     * @return JsonModel
     */
    public function createAction()
    {
        $this->status = false;
        $this->msg = $this->getMessage('ERR_INVALID_REQUEST');
        $this->data = null;

        $request = $this->getRequest();
        if ($request->isPost()) {

            $postData = $request->getPost()->toArray();

            $vehicle = new Vehicle();
            $vehicle->vehicleMake = $postData['vehicleMake'];
            $vehicle->vehicleModel = $postData['vehicleModel'];
            $vehicle->vehicleRegistrationNumber = $postData['vehicleRegistrationNumber'];
            $vehicle->vehicleCost = $postData['vehicleCost'];
            $vehicle->entityId = $this->createEntity();

            $form = new VehicleForm();
            $form->setInputFilter($vehicle->getInputFilter());
            $form->setData($postData);

            if ($form->isValid()) {
                $res = $this->CommonTable('JobCard\Model\VehicleTable')->getVehicleByRegistrationNumber($postData['vehicleRegistrationNumber']);
                if (is_null($res)) {
                    $result = $this->CommonTable('JobCard\Model\VehicleTable')->saveVehicle($vehicle);
                    if ($result) {
                        $this->status = true;
                        $this->msg = $this->getMessage('SUCC_VHCL_SAVE');
                        $this->data = $result;
                    } else {
                        $this->msg = $this->getMessage('ERR_VHCL_SAVE');
                    }
                } else {
                    $this->msg = $this->getMessage('ERR_VHCL_DUPLICATE');
                }
            } else {
                $this->msg = $this->getMessage('ERR_VHCL_VALIDATION');
            }
        }

        return $this->JSONRespond();
    }

    /**
     * Update Vehicle
     * @return JsonModel
     */
    public function updateAction()
    {

        $this->status = false;
        $this->msg = $this->getMessage('ERR_INVALID_REQUEST');
        $this->data = null;

        if ($this->getRequest()->isPost()) {

            $postData = $this->getRequest()->getPost()->toArray();

            $form = new VehicleForm();
            $vehicle = new Vehicle();
            $form->setInputFilter($vehicle->getInputFilter());
            $form->setData($postData);

            if ($form->isValid()) {
                $res = $this->CommonTable('JobCard\Model\VehicleTable')->getVehicleByRegistrationNumber($postData['vehicleRegistrationNumber']);
                if (is_array($res) && ($form->getData()['vehicleId'] != $res['vehicleId'])) {
                    $this->msg = $this->getMessage('ERR_VHCL_DUPLICATE');
                } else {
                    try {
                        $this->CommonTable('JobCard\Model\VehicleTable')->updateVehicle($postData, $postData['vehicleId']);
                        $this->updateEntity($postData['entityId']);
                        $this->status = true;
                        $this->msg = $this->getMessage('SUCC_VHCL_UPDATE');
                    } catch (\Exception $e) {
                        $this->msg = $e->getMessage();
                    }
                }
            } else {
                $this->msg = $this->getMessage('ERR_VHCL_VALIDATION');
            }
        }
        return $this->JSONRespond();
    }

    /**
     * Delete Vehicle
     * @return JsonModel
     */
    public function deleteAction()
    {
        $this->status = false;
        $this->msg = $this->getMessage('ERR_INVALID_REQUEST');
        $this->data = null;

        if ($this->getRequest()->isPost()) {

            $vehicleId = $this->getRequest()->getPost('vehicleId');

            $vehicle = $this->CommonTable('JobCard\Model\VehicleTable')->getVehicleByVehicleId($vehicleId);
            if ($vehicle) {
                if ($this->updateDeleteInfoEntity($vehicle['entityId'])) {
                    $this->status = true;
                    $this->msg = $this->getMessage('SUCC_VHCL_DELETE');
                } else {
                    $this->msg = $this->getMessage('ERR_VHCL_DELETE_FAILED');
                }
            } else {
                $this->msg = $this->getMessage('ERR_VHCL_NOT_FOUND');
            }
        }

        return $this->JSONRespond();
    }

    /**
     * Get Vehicle
     * @return JsonModel
     */
    public function getVehicleAction()
    {
        $this->status = false;
        $this->msg = $this->getMessage('ERR_INVALID_REQUEST');
        $this->data = null;

        if ($this->getRequest()->isPost()) {

            $vehicleId = $this->getRequest()->getPost('vehicleId');

            $this->data = $this->CommonTable('JobCard\Model\VehicleTable')->getVehicleByVehicleId($vehicleId);
            if ($this->data) {
                $this->status = true;
                $this->msg = $this->getMessage('SUCC_VHCL_RETRIEVED');
            } else {
                $this->msg = $this->getMessage('ERR_VHCL_NOT_FOUND');
            }
        }

        return $this->JSONRespond();
    }

    /**
     * get all Vehicles
     * @return ViewModel
     */
    public function getVehiclesAction()
    {
        //get page number
        $page = $this->params('param1', 1);
        //get vehicle list
        $paginator = $this->CommonTable('JobCard\Model\VehicleTable')->fetchAll(true, array('vehicle.vehicleId DESC'));
        $paginator->setCurrentPageNumber($page);
        $paginator->setItemCountPerPage(10);

        $view = new ViewModel(array('vehicles' => $paginator));
        $view->setTerminal(true);
        $view->setTemplate('job-card/vehicle/get-vehicles');
        return $view;
    }

    /**
     * search Vehicles
     * @return ViewModel
     */
    public function searchAction()
    {
        if ($this->getRequest()->isPost()) {

            $key = $this->getRequest()->getPost('key');
            $paginated = $this->getRequest()->getPost('paginated');
            $page = $this->getRequest()->getPost('page');

            $vehicles = $this->CommonTable('JobCard\Model\VehicleTable')->searchVehicle($key, array('vehicle.vehicleId DESC'), $paginated);
            if ($paginated == '1') {
                $vehicles->setCurrentPageNumber($page);
                $vehicles->setItemCountPerPage(10);
            }

            $view = new ViewModel(array('vehicles' => $vehicles, 'paginated' => $paginated));
            $view->setTerminal(true);
            $view->setTemplate('job-card/vehicle/list');
            return $view;
        }
    }

    /**
     * Get vehicle list for dropdown
     * @return JsonModel
     */
    public function searchVehicleForDropdownAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $searchKey = $request->getPost('searchKey');

            $vehicles = $this->CommonTable('JobCard\Model\VehicleTable')->searchVehicle($searchKey, array('vehicle.vehicleId ASC'), false);
            $vehicleList = array();
            foreach ($vehicles as $vehicle) {
                $temp['value'] = $vehicle['vehicleId'];
                $temp['text'] = $vehicle['vehicleRegistrationNumber'] . ' (' . $vehicle['vehicleMake'] . '-' . $vehicle['vehicleModel'] . ')';
                $vehicleList[] = $temp;
            }

            $this->data = array('list' => $vehicleList);
            return $this->JSONRespond();
        }
    }

}
