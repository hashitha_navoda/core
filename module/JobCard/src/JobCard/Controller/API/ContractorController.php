<?php

namespace JobCard\Controller\API;

use Zend\View\Model\ViewModel;
use Core\Controller\CoreController;
use JobCard\Model\Contractor;

class ContractorController extends CoreController
{

    public function saveContractorAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $firstName = $request->getPost('contractorFirstName');
            $secondName = $request->getPost('contractorSecondName');
            $designation = $request->getPost('designationID');
            $division = $request->getPost('divisionID');
            $telNumber = $request->getPost('telNumber');
            $editMode = $request->getPost('editMode');

            if ($editMode == 'true') {

                $contractorID = $request->getPost('contractorID');
                $data = array(
                    'designationID' => $designation,
                    'divisionID' => $division,
                    'contractorTP' => $telNumber,
                    'contractorID' => $contractorID
                );

                $contractorData = new Contractor();
                $contractorData->exchangeArray($data);
                $lastRow = $this->CommonTable('JobCard/Model/ContractorTable')->updateContractor($contractorData);

                if ($lastRow) {
                    $this->status = true;
                    $this->data = array('contractorID' => $contractorID);
                    $this->msg = $this->getMessage('SUCC_CONTRACT_UPDATE', array($firstName));
                    $this->flashMessenger()->addMessage($this->msg);
                    $this->commit();
                } else {
                    $this->status = false;
                    $this->msg = $this->getMessage('ERR_CONTRACT_UPDATE');
                    $this->rollback();
                }
            } else {
                $result = $this->CommonTable('JobCard/Model/ContractorTable')->getContractor($firstName, $secondName);

                if (!($result->count() > 0)) {
                    $data = array(
                        'contractorFirstName' => $firstName,
                        'contractorSecondName' => $secondName,
                        'designationID' => $designation,
                        'divisionID' => $division,
                        'contractorTP' => $telNumber,
                        'entityID' => $this->createEntity()
                    );
                    $contractorData = new Contractor();
                    $contractorData->exchangeArray($data);
                    $lastRow = $this->CommonTable('JobCard/Model/ContractorTable')->saveContractor($contractorData);

                    if ($lastRow) {
                        $this->status = true;
                        $this->data = $lastRow;
                        $this->msg = $this->getMessage('SUCC_CONTRACTR_SAVE');
                        $this->flashMessenger()->addMessage($this->msg);
                        $this->commit();
                    } else {
                        $this->status = false;
                        $this->msg = $this->getMessage('ERR_CONTRACTR_SAVE');
                        $this->rollback();
                    }
                } else {
                    $this->status = false;
                    $this->msg = $this->getMessage('ERR_CONTRACTR_NAME_ALREDY_EXIST');
                }
            }
            return $this->JSONRespond();
        }
    }

    public function deleteContractorAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $contractorID = $request->getPost('contractorID');
            $contractFName = $request->getPost('contractorFirstName');
            $contractorData = $this->CommonTable('JobCard/Model/ContractorTable')->getContractorByID($contractorID)->current();
            //get all activity details that related to given contractor
            $activities = $this->CommonTable('JobCard/Model/ActivityContractorTable')
                            ->getActivityByActivityTypeID($contractorID)->current();
            if ($activities) {
                $this->status = false;
                $this->data = 'deactive';
            } else {
                if ($contractorData) {
                    if (!empty($contractorID)) {
                        $entityID = $contractorData['entityID'];
                        $this->updateDeleteInfoEntity($entityID);
                        $this->status = true;
                        $this->msg = $this->getMessage('SUCC_CONTRACT_DELETE', array($contractFName));
                        $this->flashMessenger()->addMessage($this->msg);
                    }
                } else {
                    $this->status = false;
                    $this->msg = $this->getMessage('ERR_CONTRACT_DELETE');
                }
            }
            return $this->JSONRespond();
        }
    }

    public function getContractorBySearchKeyAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $paginated = false;
            $contractorSearchKey = $request->getPost('contractorSearchKey');
            if (!empty($contractorSearchKey)) {
                $tbl = $this->CommonTable('JobCard\Model\ContractorTable');
                $contractorList = $tbl->getContractorforSearch($contractorSearchKey);
                $this->status = true;
            } else {
                $this->status = true;
                $contractorList = $this->getPaginatedContractor();
                $paginated = true;
            }

            $view = new ViewModel(array(
                'contractorList' => $contractorList,
                'paginated' => $paginated
            ));

            $view->setTerminal(true);
            $view->setTemplate('job-card/contractor/list.phtml');

            $this->html = $view;
            return $this->JSONRespondHtml();
        }
    }

    public function getPaginatedContractor()
    {

        $this->paginator = $this->CommonTable('JobCard/Model/ContractorTable')->fetchAll(true);
        $this->paginator->setCurrentPageNumber((int) $this->params()->fromRoute('param1', 1));
        $this->paginator->setItemCountPerPage(5);
        return $this->paginator;
    }

    /**
     * @author Lahiru Abeysinghe <lahiru@thinkcube.com>
     * use to change Contractor status
     */
    public function changeStatusIDAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $contractorID = $request->getPost('contractorID');
            $status = $request->getPost('status');
            $updateState = $this->CommonTable('JobCard/Model/ContractorTable')->updateContractorState($contractorID, $status);
            if ($updateState) {
                $this->status = true;
                $this->msg = $this->getMessage('SUCC_CONTRACTOR_STATUS_UPDATE');
                return $this->JSONRespond();
            }
        }
    }

}
