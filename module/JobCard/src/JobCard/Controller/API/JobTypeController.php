<?php

/**
 * @author Ashan Madushka <ashan@thinkcube.com>
 * This file contains Job Type related Api controller functions
 */

namespace JobCard\Controller\API;

use Zend\View\Model\ViewModel;
use Core\Controller\CoreController;
use JobCard\Form\JobTypeForm;
use Zend\Session\Container;
use JobCard\Model\JobType;

class JobTypeController extends CoreController
{

//job type save and update function
    public function addAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $this->beginTransaction();

            $jobTypeCode = $request->getPost('jobTypeCode');
            $jobTypeName = $request->getPost('jobTypeName');
            $editMode = $request->getPost('editMode');
//check wheter editMode is true or false. if it is true then update the job type otherwise save the job type
            if ($editMode == 'true') {
                $jobTypeID = $request->getPost('jobTypeID');
                $data = array(
                    'jobTypeName' => $jobTypeName,
                    'jobTypeID' => $jobTypeID,
                );

                $jobTypeData = new JobType();
                $jobTypeData->exchangeArray($data);
                $result = $this->CommonTable('JobCard\Model\JobTypeTable')->updateJobType($jobTypeData);
                if ($result) {
                    $this->status = true;
                    $this->data = array('jobTypeID' => $jobTypeID);
                    $this->msg = $this->getMessage('SUCC_JBTYPE_UPDATE', array($jobTypeCode));
                    $this->flashMessenger()->addMessage($this->msg);
                    $this->commit();
                } else {
                    $this->status = false;
                    $this->msg = $this->getMessage('ERR_JBTYPE_UPDATE');
                    $this->rollback();
                }
            } else {
//check whether job type code alredy in database or not.
                $result = $this->CommonTable('JobCard\Model\JobTypeTable')->getJobTypeByJobTypeCode($jobTypeCode);
                if (!($result->count() > 0)) {
                    $entityID = $this->createEntity();
                    $data = array(
                        'jobTypeCode' => $jobTypeCode,
                        'jobTypeName' => $jobTypeName,
                        'entityID' => $entityID
                    );

                    $jobTypeData = new JobType();
                    $jobTypeData->exchangeArray($data);
                    $jobTypeID = $this->CommonTable('JobCard\Model\JobTypeTable')->saveJobType($jobTypeData);
                    if ($jobTypeID) {
                        $this->status = true;
                        $this->data = array('jobTypeID' => $jobTypeID);
                        $this->msg = $this->getMessage('SUCC_JBTYPE_SAVE');
                        $this->flashMessenger()->addMessage($this->msg);
                        $this->commit();
                    } else {
                        $this->status = false;
                        $this->msg = $this->getMessage('ERR_JBTYPE_SAVE');
                        $this->rollback();
                    }
                } else {
                    $this->status = false;
                    $this->data = 'JTCERR';
                    $this->msg = $this->getMessage('ERR_JBTYPE_CODE_ALREDY_EXIXT');
                }
            }
            return $this->JSONRespond();
        }
    }

//get job types by job type search key
    public function getJobTypesBySearchKeyAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $paginated = false;
            $jobTypeSearchKey = $request->getPost('jobTypeSearchKey');

            if (!empty($jobTypeSearchKey)) {
                $tbl = $this->CommonTable('JobCard\Model\JobTypeTable');
                $jobTypeList = $tbl->getJobTypeforSearch($jobTypeSearchKey);
                $this->status = true;
            } else {
                $this->status = true;
                $jobTypeList = $this->getPaginatedJobTypes();
                $paginated = true;
            }

            $view = new ViewModel(array(
                'jobTypeList' => $jobTypeList,
                'paginated' => $paginated
            ));

            $view->setTerminal(true);
            $view->setTemplate('job-card/job-type/list.phtml');

            $this->html = $view;
            return $this->JSONRespondHtml();
        }
    }

    //delete job types by job type id
    public function deleteJobTypeByJobTypeIDAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $jobTypeID = $request->getPost('jobTypeID');
            $jobTypeCode = $request->getPost('jobTypeCode');
            //use this line for find, uses of this job type
            $jobDetails = $this->CommonTable('JobCard/Model/JobTable')->getJobDetailsByJobTypeID($jobTypeID)->current();
            if (!$jobDetails) {
                if (!empty($jobTypeID)) {
                    $result = $this->CommonTable('JobCard\Model\JobTypeTable')->getJobTypeByJobTypeID($jobTypeID)->current();
                    $entityID = $result['entityID'];
                    $this->updateDeleteInfoEntity($entityID);
                    $this->status = true;
                    $this->msg = $this->getMessage('SUCC_JBTYPE_DELETE', array($jobTypeCode));
                    $this->flashMessenger()->addMessage($this->msg);
                } else {
                    $this->status = false;
                    $this->msg = $this->getMessage('ERR_JBTYPE_DELETE');
                }
            } else {
                $this->status = false;
                $this->data = 'deactive';
            }
            return $this->JSONRespond();
        }
    }

    protected function getPaginatedJobTypes($perPage = 6)
    {
        $this->paginator = $this->CommonTable('JobCard\Model\JobTypeTable')->fetchAll(true);
        $this->paginator->setCurrentPageNumber((int) $this->params()->fromRoute('param1', 1));
        $this->paginator->setItemCountPerPage($perPage);
        $this->setLogMessage('Job Type list accessed');
        return $this->paginator;
    }

    /**
     * @author Ashan <ashan@thinkcube.com>
     * Search job types for dropdown
     * @return type
     */
    public function searchJobTypesForDropdownAction()
    {
        $searchrequest = $this->getRequest();
        if ($searchrequest->isPost()) {
            $searchKey = $searchrequest->getPost('searchKey');
            $jobTypes = $this->CommonTable('JobCard\Model\JobTypeTable')->getJobTypeforSearch($searchKey);
            $jobTypeList = array();
            foreach ($jobTypes as $jobType) {
                $temp['value'] = $jobType['jobTypeId'];
                $temp['text'] = $jobType['jobTypeCode'] . '-' . $jobType['jobTypeName'];
                $jobTypeList[] = $temp;
            }

            $this->data = array('list' => $jobTypeList);
            return $this->JSONRespond();
        }
    }

    /**
     * @author Lahiru Abeysinghe <lahiru@thinkcube.com>
     * use to change active state
     */
    public function changeStatusIDAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $jobTypeID = $request->getPost('jobTypeID');
            $status = $request->getPost('status');
            $updateState = $this->CommonTable('JobCard/Model/JobTypeTable')->updateJobTypeState($jobTypeID, $status);
            if ($updateState) {
                $this->status = true;
                $this->msg = $this->getMessage('SUCC_JOBTYPE_STATUS_UPDATE');
                return $this->JSONRespond();
            }
        }
    }

}
