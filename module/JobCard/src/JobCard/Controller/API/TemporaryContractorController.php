<?php

namespace JobCard\Controller\API;

use Core\Controller\CoreController;
use JobCard\Model\TemporaryContractor;
use Zend\View\Model\ViewModel;

class TemporaryContractorController extends CoreController
{

    public $paginator;

    public function saveTemporaryContractorAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $firstName = $request->getPost('temporaryContractorFirstName');
            $secondName = $request->getPost('temporaryContractorSecondName');
            $designation = $request->getPost('designationID');
            $division = $request->getPost('divisionID');
            $telNumber = $request->getPost('temporaryContractorTP');

            $editMode = $request->getPost('editMode');
            $data = array(
                'temporaryContractorFirstName' => $firstName,
                'temporaryContractorSecondName' => $secondName,
                'designationID' => $designation,
                'divisionID' => $division,
                'temporaryContractorTP' => $telNumber,
            );

            if ($editMode == 'true') {
                $contractorID = $request->getPost('contractorID');
                $data['temporaryContractorID'] = $contractorID;
                $tempContractorData = new TemporaryContractor();
                $tempContractorData->exchangeArray($data);
                $lastRow = $this->CommonTable('JobCard/Model/TemporaryContractorTable')->updateTemporaryContractor($tempContractorData);

                if ($lastRow) {
                    $this->status = true;
                    $this->msg = $this->getMessage('SUCC_CONTRACT_UPDATE', array($firstName));
                    $this->commit();
                } else {
                    $this->status = false;
                    $this->msg = $this->getMessage('ERR_CONTRACT_UPDATE');
                    $this->rollback();
                }
            } else {
                $result = $this->CommonTable('JobCard/Model/TemporaryContractorTable')->getTempContractor($firstName, $secondName);

                if (!($result->count() > 0)) {
                    $data['status'] = 1;
                    $tempContractorData = new TemporaryContractor();
                    $tempContractorData->exchangeArray($data);
                    $lastRow = $this->CommonTable('JobCard/Model/TemporaryContractorTable')->saveTempContractor($tempContractorData);

                    if ($lastRow) {
                        $this->status = true;
                        $this->msg = $this->getMessage('SUCC_TEMP_CONTRACTR_SAVE');
                        $this->commit();
                    } else {
                        $this->status = false;
                        $this->msg = $this->getMessage('ERR_TEMP_CONTRACTR_SAVE');
                        $this->rollback();
                    }
                } else {
                    $this->status = false;
                    $this->msg = $this->getMessage('ERR_TEMP_CONTRACTR_NAME_ALREDY_EXIST');
                }
            }
            $isModalRequest = $request->getPost('isModalRequest');
            if ($isModalRequest) {
                $this->data = $lastRow;
            } else {
                $this->paginatedTemporaryContractor();
                $view = new ViewModel(array(
                    'temporaryContractors' => $this->paginator,
                    'paginated' => true
                ));

                $view->setTerminal(true);
                $view->setTemplate('job-card/temporary-contractor/list.phtml');
                $this->html = $view;
            }
            return $this->JSONRespondHtml();
        }
    }

    public function searchTemporaryContractorAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $paginated = false;
            $contractorSearchKey = $request->getPost('contractorSearchKey');
            if (!empty($contractorSearchKey)) {
                $contractorList = $this->CommonTable('JobCard\Model\TemporaryContractorTable')->getTemporaryContractorforSearch($contractorSearchKey);

                $this->status = true;
            } else {
                $this->paginatedTemporaryContractor();
                $this->status = true;
                $contractorList = $this->paginator;
                $paginated = true;
            }

            $view = new ViewModel(array(
                'temporaryContractors' => $contractorList,
                'paginated' => $paginated
            ));

            $view->setTerminal(true);
            $view->setTemplate('job-card/temporary-contractor/list.phtml');

            $this->html = $view;
            return $this->JSONRespondHtml();
        }
    }

    public function getTemporaryContractorAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $tempContractorID = $request->getPost('contractorID');
            $contractorData = $this->CommonTable('JobCard\Model\TemporaryContractorTable')->getTemporaryContractorByID($tempContractorID);

            $this->status = TRUE;
            $this->data = $contractorData->current();
            return $this->JSONRespond();
        }
    }

    public function deleteTemporaryContractorAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $tempContractorID = $request->getPost('contractorID');

//            check already used in any activity
            $res = $this->CommonTable('JobCard\ModelActivityTemporaryContractorTable')->getDataByTeamporaryContractorID($tempContractorID);

            if (!(count($res) > 0)) {
                $this->CommonTable('JobCard\Model\TemporaryContractorTable')->deleteTemporaryContractor($tempContractorID);
                $this->paginatedTemporaryContractor();
                $view = new ViewModel(array(
                    'temporaryContractors' => $this->paginator,
                    'paginated' => true,
                ));

                $view->setTerminal(true);
                $view->setTemplate('job-card/temporary-contractor/list.phtml');

                $this->html = $view;
                $this->status = true;
                $this->msg = $this->getMessage('SUCC_TEMP_CONTRACT_DELETE');
            } else {
                $this->msg = $this->getMessage('ERR_TEMP_CONTRAC_DELETE');
                $this->status = false;
            }
        }
        return $this->JSONRespondHtml();
    }

    public function updateTemporaryContractorStatusAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $tempContractorID = $request->getPost('contractorID');
            $status = $request->getPost('status');

//            check already used in any activity
            $res = $this->CommonTable('JobCard\ModelActivityTemporaryContractorTable')->getDataByTeamporaryContractorID($tempContractorID);

            if (!(count($res) > 0)) {
                $this->CommonTable('JobCard\Model\TemporaryContractorTable')->updateStatusTemporaryContractor($status, $tempContractorID);
                $this->paginatedTemporaryContractor();
                $view = new ViewModel(array(
                    'temporaryContractors' => $this->paginator,
                    'paginated' => true,
                ));

                $view->setTerminal(true);
                $view->setTemplate('job-card/temporary-contractor/list.phtml');

                $this->html = $view;
                $this->status = true;
                $this->msg = $this->getMessage('SUCC_TEMP_CONTRACT_STATUS_UPDATE');
            } else {
                $this->msg = $this->getMessage('ERR_TEMP_CONTRACT_STATUS_UPDATE');
                $this->status = false;
            }
        }
        return $this->JSONRespondHtml();
    }

    private function paginatedTemporaryContractor($size = 6)
    {
        $this->paginator = $this->CommonTable('JobCard\Model\TemporaryContractorTable')->fetchAll(true);
        $this->paginator->setCurrentPageNumber((int) $this->params()->fromRoute('param1', 1));
        $this->paginator->setItemCountPerPage($size);
    }

}
