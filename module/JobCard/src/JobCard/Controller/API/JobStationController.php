<?php

namespace JobCard\Controller\API;

use Zend\View\Model\ViewModel;
use Core\Controller\CoreController;
use JobCard\Form\JobStationForm;
use Zend\Session\Container;
use JobCard\Model\JobStation;

class JobStationController extends CoreController
{

    /**
     * @author Lahiru Abeysinghe <lahiru@thinkcube.com>
     * this function use to add new job Stations into the systems
     *
     */
    public function addAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $this->beginTransaction();
            $jobStationName = $request->getPost('jobStationName');
            $maxValue = $request->getPost('maxValue');
            $minValue = $request->getPost('minValue');
            $locationID = $this->user_session->userActiveLocation['locationID'];
            $result = $this->CommonTable('JobCard/Model/JobStationTable')->getJobStation($jobStationName, $locationID);
            if (!($result->count() > 0)) {
                $entityID = $this->createEntity();
                $data = array(
                    'jobStationName' => $jobStationName,
                    'jobStationMaxValue' => $maxValue,
                    'jobStationMinValue' => $minValue,
                    'locationID' => $locationID,
                    'entityID' => $entityID,
                );
                $jobStationData = new JobStation();
                $jobStationData->exchangeArray($data);
                $savedResult = $this->CommonTable('JobCard/Model/JobStationTable')->saveJobStation($jobStationData);
                if ($savedResult) {
                    $this->status = true;
                    $this->msg = $this->getMessage('SUCC_JOB_ST_SAVE');
                    $this->flashMessenger()->addMessage($this->msg);
                    $this->commit();
                } else {
                    $this->status = false;
                    $this->msg = $this->getMessage('ERR_JOB_ST_SAVE');
                    $this->rollback();
                }
            } else {
                $this->status = false;
                $this->data = 'JOB_STATION_EXIST';
                $this->msg = $this->getMessage('ERR_JOB_ST_ALREDY_EXIST');
                $this->rollback();
            }
            return $this->JSONRespond();
        }
    }

    /**
     * @author Lahiru Abeysinghe <lahiru@thinkcube.com>
     * this function use to update existing job stations
     *
     */
    function updateAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $this->beginTransaction();
            $jobStationName = $request->getPost('jobStationName');
            $maxValue = $request->getPost('maxValue');
            $minValue = $request->getPost('minValue');
            $jobStationID = $request->getPost('jobStationID');
            $locationID = $this->user_session->userActiveLocation['locationID'];
            $result = $this->CommonTable('JobCard/Model/JobStationTable')->getJobStationByNameAndID($jobStationName, $locationID, $jobStationID);
            if (!($result->count() > 0)) {
                $data = array(
                    'jobStationID' => $jobStationID,
                    'jobStationName' => $jobStationName,
                    'jobStationMaxValue' => $maxValue,
                    'jobStationMinValue' => $minValue,
                );
                $jobStationData = new JobStation();
                $jobStationData->exchangeArray($data);
                $updatedData = $this->CommonTable('JobCard/Model/JobStationTable')->updateJobStation($jobStationData);
                if ($updatedData) {
                    $this->status = true;
                    $this->msg = $this->getMessage('SUCC_JOB_ST_UPDATE');
                    $this->flashMessenger()->addMessage($this->msg);
                    $this->commit();
                } else {
                    $this->status = false;
                    $this->msg = $this->getMessage('ERR_JOB_ST_UPDATE');
                    $this->rollback();
                }
            } else {
                $this->status = false;
                $this->data = 'JOB_STATION_EXIST';
                $this->msg = $this->getMessage('ERR_JOB_ST_ALREDY_EXIST');
                $this->rollback();
            }

            return $this->JSONRespond();
        }
    }

    /**
     * @author Lahiru Abeysinghe <lahiru@thinkcube.com>
     * this function use to delete job stations
     *
     */
    function deleteAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $this->beginTransaction();
            $jobStationID = $request->getPost('jobStationID');
            $jobData = $this->CommonTable('JobCard/Model/JobTable')->getJobDetailsByJobStationID($jobStationID)->current();
            if (!$jobData) {
                $jobStationData = (object) $this->CommonTable('JobCard/Model/JobStationTable')->getJobStationByJobStationID($jobStationID)->current();
                $result = $this->updateDeleteInfoEntity($jobStationData->entityID);
                if ($result) {
                    $this->status = true;
                    $this->msg = $this->getMessage('SUCC_JOB_ST_DELETE');
                    $this->flashMessenger()->addMessage($this->msg);
                    $this->commit();
                } else {
                    $this->status = false;
                    $this->msg = $this->getMessage('ERR_JOB_ST_DELETE');
                    $this->rollback();
                }
            } else {
                $this->status = false;
                $this->data = 'deactive';
                $this->rollback();
            }

            return $this->JSONRespond();
        }
    }

    /**
     * @author Lahiru Abeysinghe <lahiru@thinkcube.com>
     * this function use to search jobStations
     *
     */
    public function getJobStationBySearchKeyAction()
    {
        $request = $this->getRequest();
        $locationID = $this->user_session->userActiveLocation['locationID'];
        if ($request->isPost()) {
            $paginated = false;
            $jobStationSearchKey = $request->getPost('searchKey');

            if (!empty($jobStationSearchKey)) {
                $jobStationList = $this->CommonTable('JobCard/Model/JobStationTable')->JobStationSearchByKey($jobStationSearchKey, $locationID);
                $this->status = true;
            } else {
                $this->status = true;
                $jobStationList = $this->getPaginatedJobStation($locationID);
                $paginated = true;
            }
            $view = new ViewModel(array(
                'jobStation' => $jobStationList,
                'paginated' => $paginated
            ));

            $view->setTerminal(true);
            $view->setTemplate('job-card/job-station/list.phtml');

            $this->html = $view;
            return $this->JSONRespondHtml();
        }
    }

    public function getPaginatedJobStation($locationID)
    {
        $this->jobStation = $this->CommonTable('JobCard/Model/JobStationTable')->fetchAll($locationID, true);
        $this->jobStation->setCurrentPageNumber((int) $this->params()->fromRoute('param1', 1));
        $this->jobStation->setItemCountPerPage(6);
        return $this->jobStation;
    }

    /**
     * @author Ashan <ashan@thinkcube.com>
     * search job station for dropdown
     */
    public function searchJobStationForDropdownAction()
    {
        $searchrequest = $this->getRequest();
        if ($searchrequest->isPost()) {
            $searchKey = $searchrequest->getPost('searchKey');
            $locationID = $this->user_session->userActiveLocation["locationID"];
            $jobStations = $this->CommonTable('JobCard/Model/JobStationTable')->jobStationSearchByKey($searchKey, $locationID);
            $jobStationList = array();
            foreach ($jobStations as $jobStation) {
                $temp['value'] = $jobStation['jobStationID'];
                $temp['text'] = $jobStation['jobStationName'];
                $jobStationList[] = $temp;
            }

            $this->data = array('list' => $jobStationList);
            return $this->JSONRespond();
        }
    }

    /**
     * @author Lahiru Abeysinghe <lahiru@thinkcube.com>
     * use to change jobStation active state
     */
    public function changeStatusIDAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $jobStationID = $request->getPost('jobStationID');
            $status = $request->getPost('status');
            $updateState = $this->CommonTable('JobCard/Model/JobStationTable')->updateJobStationState($jobStationID, $status);
            if ($updateState) {
                $this->status = true;
                $this->msg = $this->getMessage('SUCC_JOB_STTION_STATUS_UPDATE');
                return $this->JSONRespond();
            }
        }
    }

}
