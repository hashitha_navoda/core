<?php

namespace JobCard\Controller\API;

use Zend\View\Model\ViewModel;
use Core\Controller\CoreController;
use JobCard\Form\JobForm;
use Zend\Session\Container;
use JobCard\Model\Job;

class JobController extends CoreController
{

    /**
     * @author Lahiru Abeysinghe <lahiru@thinkcube.com>
     * this function use to add new jobs in to the system
     *
     */
    public function addAction()
    {
        $request = $this->getRequest();
        $userLocation = $this->user_session->userActiveLocation['locationID'];
        if ($request->isPost()) {
            $this->beginTransaction();
            $cusName = $request->getPost('cusName');
            $cusProID = $request->getPost('customerProfileID');
            $jobname = $request->getPost('jobName');
            $jobType = $request->getPost('jobType');
            $jobOwner = $request->getPost('jobOwner');
            $jobOwnerLength = $request->getPost('jobOwnerLenght');
            $jobSupervisor = $request->getPost('jobSupervisor');
            $jobSupervisorLength = $request->getPost('jobSupervisorLength');
            $jobDescription = $request->getPost('jobDescrip');
            $jobStartTime = $request->getPost('jobSTime');
            $jobEndTime = $request->getPost('jobETime');
            $jobEstimateCost = floatval($request->getPost('jobEstCost'));
            $jobStation = $request->getPost('jobStation');
            $jobReference = $request->getPost('jobReference');
            $projectID = $request->getPost('projectID');
            $currentWeight = $request->getPost('currentWeight');
            $days = $request->getPost('days');
            $hours = $request->getPost('hours');
            $minutes = $request->getPost('minutes');
            $address = $request->getPost('address');
            $jobRepeatEnabled = $request->getPost('jobRepeatEnabled');
            $jobRepeatComment = $request->getPost('jobRepeatComment');

            if (!$cusName) {
                $proData = $this->CommonTable('JobCard/Model/ProjectTable')->getProjectDetailsByProjectID($projectID);
                $pData = (object) $proData->current();
                $cusName = $pData->customerID;
                $cusProID = $pData->customerProfileID;
            }
            if ($jobStartTime) {
//                $jobSTime = preg_replace("/(\d+)\D+(\d+)\D+(\d+)/", "$3-$2-$1", $jobStartTime);
//                $jobStartTime = date('Y-m-d H:i:s', strtotime($jobSTime));
                $jobStartTime = $this->getGMTDateTime('Y-m-d H:i:s', $jobStartTime);
            }
            if ($jobEndTime) {
//                $jobETime = preg_replace("/(\d+)\D+(\d+)\D+(\d+)/", "$3-$2-$1", $jobEndTime);
//                $jobEndTime = date('Y-m-d H:i:s', strtotime($jobETime));
                $jobEndTime = $this->getGMTDateTime('Y-m-d H:i:s', $jobEndTime);
            }
            $estimetedTime = $this->calculateTime($days, $hours, $minutes);
            $reference = $this->getReferenceNoForLocation('22', $userLocation);
            $locationReferenceID = $reference['locRefID'];
            while ($jobReference) {
                if ($this->CommonTable('JobCard\Model\JobTable')->getJobByCode($jobReference)) {
                    if ($locationReferenceID) {
                        $newJobCode = $this->getReferenceNumber($locationReferenceID);
                        if ($jobReference == $newJobCode) {
                            $this->updateReferenceNumber($locationReferenceID);
                            $jobReference = $this->getReferenceNumber($locationReferenceID);
                        } else {
                            $jobReference = $newJobCode;
                        }
                    } else {
                        $this->status = false;
                        $this->msg = $this->getMessage('ERR_JOB_CODE_EXIST');
                        return $this->JSONRespond();
                    }
                } else {
                    break;
                }
            }
            $entityID = $this->createEntity();
            $statusName = 'Open';
            $statusID = $this->getStatusIDByStatusName($statusName);
            $data = array(
                'jobName' => $jobname,
                'customerID' => $cusName,
                'jobTypeId' => $jobType,
                'jobDescription' => $jobDescription,
                'jobStartingTime' => $jobStartTime,
                'jobEndTime' => $jobEndTime,
                'jobEstimatedCost' => $jobEstimateCost,
                'projectId' => $projectID,
                'jobStation' => $jobStation,
                'jobEstimatedTime' => $estimetedTime,
                'jobReferenceNumber' => $jobReference,
                'customerProfileID' => $cusProID,
                'jobWeight' => $currentWeight,
                'locationID' => $userLocation,
                'entityID' => $entityID,
                'jobStatus' => $statusID,
                'jobAddress' => $address,
                'jobRepeatEnabled' => $jobRepeatEnabled,
                'jobRepeatComment' => $jobRepeatComment,
            );
            $jobData = new Job();
            $jobData->exchangeArray($data);
            $savedResult = $this->CommonTable('JobCard/Model/JobTable')->saveJob($jobData);
            if ($jobSupervisorLength > 0) {
                $saveSupervisor = $this->CommonTable('JobCard/Model/JobSupervisorTable')->saveSupervisor($jobReference, $jobSupervisor, $jobSupervisorLength);
            } else {
                $saveSupervisor = true;
            }
            if ($jobOwnerLength > 0) {
                $saveOwner = $this->CommonTable('JobCard/Model/JobOwnerTable')->saveOwner($jobReference, $jobOwner, $jobOwnerLength);
            } else {
                $saveOwner = true;
            }

            if ($savedResult && $saveOwner && $saveSupervisor) {
                $inqID = $request->getPost('inqID');
                if ($inqID) {
                    $inqData = array(
                        'inquiryComplainRelationDocumentID' => $savedResult,
                        'inquiryLogID' => $inqID,
                        'documentTypeID' => '22',
                    );
                    $inqDetails = (object) $inqData;
                    $this->CommonTable('JobCard\Model\InquiryComplainRelationTable')->saveRelatedDocument($inqDetails);
                }
                $this->status = true;
                $this->msg = $this->getMessage('SUCC_JOB_SAVE');
                $this->flashMessenger()->addMessage($this->msg);
                $this->data = $savedResult;
                if ($locationReferenceID) {
                    $this->updateReferenceNumber($locationReferenceID);
                }
                $this->commit();
            } else {
                $this->status = false;
                $this->msg = $this->getMessage('ERR_JOB_SAVE');
                $this->rollback();
            }
            return $this->JSONRespond();
        }
    }

    /**
     * @author Lahiru Abeysinghe <lahiru@thinkcube.com>
     * this function use to update exiting jobs
     *
     */
    function updateAction()
    {
        $request = $this->getRequest();
        $userLocation = $this->user_session->userActiveLocation['locationID'];
        if ($request->isPost()) {
            $this->beginTransaction();
            $jobId = $request->getPost('jobId');
            $cusName = $request->getPost('cusName');
            $cusProID = $request->getPost('customerProfileID');
            $jobname = $request->getPost('jobName');
            $jobType = $request->getPost('jobType');
            $jobOwner = $request->getPost('jobOwner');
            $jobOwnerLength = $request->getPost('jobOwnerLenght');
            $jobSupervisor = $request->getPost('jobSupervisor');
            $jobSupervisorLength = $request->getPost('jobSupervisorLength');
            $jobDescription = $request->getPost('jobDescrip');
            $jobStartTime = $request->getPost('jobSTime');
            $jobEndTime = $request->getPost('jobETime');
            $jobEstimateCost = floatval($request->getPost('jobEstCost'));
            $jobStation = $request->getPost('jobStation');
            $jobReference = $request->getPost('jobReference');
            $projectID = $request->getPost('projectID');
            $days = $request->getPost('days');
            $hours = $request->getPost('hours');
            $minutes = $request->getPost('minutes');
            $address = $request->getPost('address');
            $jobRepeatEnabled = $request->getPost('jobRepeatEnabled');
            $jobRepeatComment = $request->getPost('jobRepeatComment');

            if ($jobStartTime) {
//                $jobSTime = preg_replace("/(\d+)\D+(\d+)\D+(\d+)/", "$3-$2-$1", $jobStartTime);
//                $jobStartTime = date('Y-m-d H:i:s', strtotime($jobSTime));
                $jobStartTime = $this->getGMTDateTime('Y-m-d H:i:s', $jobStartTime);
            }
            if ($jobEndTime) {
//                $jobETime = preg_replace("/(\d+)\D+(\d+)\D+(\d+)/", "$3-$2-$1", $jobEndTime);
//                $jobEndTime = date('Y-m-d H:i:s', strtotime($jobETime));
                $jobEndTime = $this->getGMTDateTime('Y-m-d H:i:s', $jobEndTime);
            }
            $estimetedTime = $this->calculateTime($days, $hours, $minutes);
            if ($jobStation == null || $jobStation == '') {
                $jobStation = $userLocation;
            }
            $data = array(
                'jobId' => $jobId,
                'jobName' => $jobname,
                'customerID' => $cusName,
                'jobTypeId' => $jobType,
                'jobDescription' => $jobDescription,
                'jobStartingTime' => $jobStartTime,
                'jobEndTime' => $jobEndTime,
                'jobEstimatedCost' => $jobEstimateCost,
                'projectId' => $projectID,
                'jobStation' => $jobStation,
                'jobEstimatedTime' => $estimetedTime,
                'jobReferenceNumber' => $jobReference,
                'customerProfileID' => $cusProID,
                'jobAddress' => $address,
                'jobRepeatEnabled' => $jobRepeatEnabled,
                'jobRepeatComment' => $jobRepeatComment,
            );
            $jobData = new Job();
            $jobData->exchangeArray($data);
            $savedResult = $this->CommonTable('JobCard/Model/JobTable')->updateJob($jobData);
            if ($jobSupervisorLength > 0) {
                $saveSupervisor = $this->CommonTable('JobCard/Model/JobSupervisorTable')->updateSupervisor($jobReference, $jobSupervisor, $jobSupervisorLength);
            } else {
                $saveSupervisor = $this->CommonTable('JobCard/Model/JobSupervisorTable')->updateSupervisor($jobReference);
            }
            if ($jobOwnerLength > 0) {
                $saveOwner = $this->CommonTable('JobCard/Model/JobOwnerTable')->updateOwner($jobReference, $jobOwner, $jobOwnerLength);
            } else {
                $saveOwner = $this->CommonTable('JobCard/Model/JobOwnerTable')->updateOwner($jobReference);
            }
            if ($savedResult && $saveSupervisor && $saveOwner) {
                $this->status = true;
                $this->msg = $this->getMessage('SUCC_JOB_UPDATE');
                $this->flashMessenger()->addMessage($this->msg);
                $this->commit();
            } else {
                $this->status = false;
                $this->msg = $this->getMessage('ERR_JOB_UPDATE');
                $this->rollback();
            }

            return $this->JSONRespond();
        }
    }

    /**
     * @author Lahiru Abeysinghe <lahiru@thinkcube.com>
     * this function use to delete existing jobs from the system
     *
     */
    function deleteAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $this->beginTransaction();
            $jobID = $request->getPost('jobID');
            $jobData = (object) $this->CommonTable('JobCard/Model/JobTable')->getJobDetails($jobID)->current();
            $result = $this->CommonTable('JobCard/Model/ActivityTable')->getActivityListByJobID($jobID)->current();
            if (!$result) {
                $updatedData = $this->updateDeleteInfoEntity($jobData->entityID);
                $statusName = 'Cancel';
                $statusID = $this->getStatusIDByStatusName($statusName);
                $updateJobData = $this->CommonTable('JobCard/Model/JobTable')->updateJobstatusID($jobID, $statusID);
                if ($updatedData && $updateJobData) {
                    $this->status = true;
                    $this->msg = $this->getMessage('SUCC_JOB_DELETE');
                    $this->flashMessenger()->addMessage($this->msg);
                    $this->commit();
                } else {
                    $this->status = false;
                    $this->msg = $this->getMessage('ERR_JOB_DELETE');
                    $this->rollback();
                }
            } else {
                $this->status = false;
                $this->msg = $this->getMessage('ERR_JOB_USED_DELETE');
                $this->rollback();
            }

            return $this->JSONRespond();
        }
    }

    /**
     * @author Lahiru Abeysinghe <lahiru@thinkcube.com>
     * this function use to search jobs
     *
     */
    public function getJobBySearchKeyAction()
    {
        $request = $this->getRequest();
        $locationID = $this->user_session->userActiveLocation['locationID'];
        if ($request->isPost()) {
            $paginated = false;
            $jobSearchKey = $request->getPost('searchKey');

            if (!empty($jobSearchKey)) {
                $jobList = $this->CommonTable('JobCard/Model/JobTable')->jobSearchByKey($jobSearchKey, $locationID);
                $this->status = true;
            } else {
                $this->status = true;
                $jobList = $this->getPaginatedJob($locationID);
            }
            $view = new ViewModel(array(
                'jobList' => $jobList,
                'paginated' => $paginated,
                'status' => $this->getStatusesList(),
            ));

            $view->setTerminal(true);
            $view->setTemplate('job-card/job/list.phtml');

            $this->html = $view;
            return $this->JSONRespondHtml();
        }
    }

    public function getPaginatedJob($locationID)
    {
        $this->job = $this->CommonTable('JobCard/Model/JobTable')->fetchAll($locationID, true);
        $this->job->setCurrentPageNumber((int) $this->params()->fromRoute('param1', 1));
        $this->job->setItemCountPerPage(15);
        return $this->job;
    }

    /**
     * @author Lahiru Abeysinghe <lahiru@thinkcube.com>
     * @param int $days
     * @param int $hours
     * @param int $minutes
     * @return int
     * calculate total minutes of the given inputs.
     */
    public function calculateTime($days, $hours, $minutes)
    {
        $totalTime = $minutes + ($hours * 60) + (($days * 24) * 60);
        return $totalTime;
    }

    /**
     * @author Lahiru Abeysinghe <lahiru@thinkcube.com>
     * get all jobs that related to given project ID
     *
     */
    public function getJobsByProjectIDAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $projectID = $request->getPost('projectID');
            $projectCode = $request->getPost('projectCode');
            $locationID = $this->user_session->userActiveLocation['locationID'];
            $jobsData = $this->CommonTable('JobCard\Model\JobTable')->getJobListByProjectID($projectID, $locationID);
            $jobsArray = '';
            foreach ($jobsData as $val) {
                $val = (object) $val;
                $customerStatus = $val->customerStatus;
                $progress = ($val->jobProgress != '') ? $val->jobProgress : '0.00';
                $jobsArray[$val->jobId] = $val->jobReferenceNumber . ' (' . $progress . '%) - ' . $val->jobName;
            }
            if ($jobsArray) {
                if($customerStatus == 2){
                    $this->msg = $this->getMessage('ERR_PROJECT_CUSTOMER_INACTIVE');
                    $this->status = false;
                    return $this->JSONRespond();
                }
                $this->data = $jobsArray;
                $this->status = true;
            } else {
                $this->msg = $this->getMessage('ERR_DOSNOT_HAVE_JOB', [$projectCode]);
                $this->status = false;
            }
            return $this->JSONRespond();
        }
    }

    /**
     * @author Lahiru Abeysinghe <lahiru@thinkcube.com>
     * get all job weights that related to given projectID
     *
     */
    public function getAllJobWeightAction()
    {
        $request = $this->getRequest();
        $locationID = $this->user_session->userActiveLocation['locationID'];
        if ($request->isPost()) {
            $projectID = $request->getPost('projectID');
            $jobsData = (object) $this->CommonTable('JobCard\Model\JobTable')->getJobListByProjectID($projectID, $locationID);
            foreach ($jobsData as $jD) {
                $jobDataList[$jD[jobId]] = array('jobRef' => $jD[jobName], 'jobWeight' => $jD[jobWeight]);
                $jobWeightSum+=$jD[jobWeight];
            }
            if (!$jobWeightSum) {
                $jobWeightSum = 0;
            }
            $totalData = array('jobDataList' => $jobDataList, 'jobWeightSum' => $jobWeightSum);
            if ($jobsData) {
                $this->data = $totalData;
                $this->status = true;
            } else {
                $this->status = false;
            }
            return $this->JSONRespond();
        }
    }

    /**
     * @author Lahiru Abeysinghe <lahiru@thinkcube.com>
     * update job weights
     *
     */
    public function updateWeightAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $array = $request->getPost('array');
            $array2 = $request->getPost('array2');
            $prID = $request->getPost('projectID');
            for ($i = 1; $i < sizeof($array); $i++) {
                $data = array(
                    'projectId' => $prID,
                    'jobWeight' => $array2[$i],
                    'jobId' => $array[$i],
                );
                $updateWeight = $this->CommonTable('JobCard/Model/JobTable')->updateJobWeight($data);
            }
            if ($updateWeight) {
                $this->status = true;
            } else {
                $this->status = false;
            }
            return $this->JSONRespond();
        }
    }

    /**
     * @author Ashan <ashan@thinkcube.com>
     * get jobs related to search key
     */
    public function searchJobsForDropdownAction()
    {
        $searchrequest = $this->getRequest();
        if ($searchrequest->isPost()) {
            $jobSearchKey = $searchrequest->getPost('searchKey');
            $projectID = $searchrequest->getPost('addFlag');

            $locationID = $this->user_session->userActiveLocation["locationID"];
            if ($projectID) {
                $jobs = $this->CommonTable('JobCard/Model/JobTable')->searchJobsForDropDown($locationID, $jobSearchKey, $projectID);
            } else {
                $jobs = $this->CommonTable('JobCard/Model/JobTable')->searchJobsForDropDown($locationID, $jobSearchKey);
            }

            $jobList = array();
            foreach ($jobs as $job) {
                $temp['value'] = $job['jobId'];
                $temp['text'] = $job['jobReferenceNumber'] . " (" . $job['jobProgress'] . "%) - " . $job['jobName'];
                $jobList[] = $temp;
            }

            $this->data = array('list' => $jobList);
            return $this->JSONRespond();
        }
    }

    public function searchJobsForDropdownByProjectIDAction()
    {
        $searchrequest = $this->getRequest();
        if ($searchrequest->isPost()) {
            $jobSearchKey = $searchrequest->getPost('searchKey');
            $projectID = $searchrequest->getPost('addFlag');
            $locationID = $this->user_session->userActiveLocation["locationID"];
            $jobs = $this->CommonTable('JobCard/Model/JobTable')->searchJobsForDropDown($locationID, $jobSearchKey, $projectID);

            $jobList = array();
            foreach ($jobs as $job) {
                $temp['value'] = $job['jobId'];
                $temp['text'] = $job['jobReferenceNumber'] . " (" . $job['jobProgress'] . "%)";
                $jobList[] = $temp;
            }

            $this->data = array('list' => $jobList);
            return $this->JSONRespond();
        }
    }

    /**
     * @author Ashan <ashan@thinkcube.com>
     * get job related all produts details
     */
    public function getJobProductsDetailsByJobIDAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $checkInvoice = true;
            $jobID = $request->getPost('jobID');
            $jobCode = $request->getPost('jobCode');
            $locationID = $this->user_session->userActiveLocation['locationID'];
            //get parent project details by job id
            $result = (object) $this->CommonTable('JobCard\Model\JobTable')->getJobDetails($jobID)->current();
            $cusProfDetails = $this->CommonTable('Invoice\Model\CustomerProfileTable')->getProfileByCustomerProfileID($result->customerProfileID)->current();
            if($cusProfDetails['customerStatus'] == 2){
                $this->msg = $this->getMessage('ERR_JOB_CUSTOMER_INACTIVE');
                $this->status = false;
                return $this->JSONRespond();
            }
            $projectID = $result->projectId;
            if ($projectID) {
                //check whether the job parent project is alredy invoice or not
                $invoiceDetails = (object) $this->CommonTable('Invoice\Model\InvoiceTable')->getInvoiceDetailsByProjectID($projectID);
                if ($invoiceDetails->count() > 0) {
                    $checkInvoice = false;
                }
            }
            if ($checkInvoice) {
                $data = $this->getAllActivitiesProductsByJobId($jobID, $locationID);
                if ($data['flag'] == 'hasProducts') {
                    $this->data = array(
                        'activityProducts' => $data['activityProducts'],
                        'customer' => $data['customer'],
                        'locationProducts' => $data['locationProducts'],
                        'customerProfID' => $result->customerProfileID,
                        'customerProfName' => $cusProfDetails['customerProfileName'],
                    );
                    $this->status = true;
                } else if ($data['flag'] == 'invoiced') {
                    $this->msg = $this->getMessage('ERR_PRODUCT_WAS_INVOICED_IN_JOB', $jobCode);
                    $this->status = false;
                } else if ($data['flag'] == 'noActivity') {
                    $this->msg = $this->getMessage('ERR_DOSNOT_HAVE_ANY_ACTIVITY_FOR_JOB');
                    $this->status = false;
                }
            } else {
                $this->msg = $this->getMessage('ERR_PRODUCT_WAS_INVOICED_IN_JOB_PARENT_PROJECT', $jobCode);
                $this->status = false;
            }
            return $this->JSONRespond();
        }
    }

    /**
     * @author Lahiru Abeysinghe <lahiru@thinkcube.com>
     * this function gets all Job supervisor IDs and JobOwner IDs that related to the given JobID
     * @return JSON Respond
     */
    public function getJobSupervisorAndJobOwnerByJobIDAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $jobID = $request->getPost('jobID');
            $details = (object) $this->CommonTable('JobCard\Model\JobTable')->getJobDetails($jobID)->current();
            $supervisorNames = $this->CommonTable('JobCard/Model/JobSupervisorTable')->getJobSupervisorName($details->jobReferenceNumber);
            $inc = 0;
            foreach ($supervisorNames as $sn) {
                $supervisorNameList[$inc] = array('id' => $sn[jobSupervisorID]);
                $inc++;
            }
            $ownerNames = $this->CommonTable('JobCard/Model/JobOwnerTable')->getJobOwnerName($details->jobReferenceNumber);
            $incs = 0;
            foreach ($ownerNames as $on) {
                $ownerNameList[$incs] = array('id' => $on[jobOwnerID]);
                $incs++;
            }
            $employee['0'] = $supervisorNameList;
            $employee['1'] = $ownerNameList;
            $employee['2'] = $inc;
            $employee['3'] = $incs;

            $this->status = true;
            $this->data = $employee;

            return $this->JSONRespond();
        }
    }

    //send job email
    public function sendEmailAction()
    {
        $request = $this->getRequest();
        $documentType = 'Job';

        if ($request->isPost()) {
            $this->mailDocumentAsTemplate($request, $documentType);
            $this->status = TRUE;
            $this->msg = $this->getMessage('SUCC_JOB_EMAIL_SENT');
            return $this->JSONRespond();
        }
        $this->status = FALSE;
        $this->msg = $this->getMessage('ERR_JOB_EMAIL_SENT');
        return $this->JSONRespond();
    }

    /**
     * Get Active jobs Name list for typeahead
     * @author sharmilan <sharmilan@thinkcube.com>
     * @return type
     */
    public function getJobsForTypeaheadAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $jobSearchKey = $request->getPost('query');

            $locationID = $this->user_session->userActiveLocation["locationID"];
            $jobs = $this->CommonTable('JobCard/Model/JobTable')->searchJobsForTypeahead($locationID, $jobSearchKey);
            $jobList = [];
            foreach ($jobs as $job) {
                $jobList[] = $job['jobName'];
            }
            $this->data = $jobList;
            $this->status = true;
            return $this->JSONRespond();
        }
    }

    public function getAllRelatedDocumentsAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $projectID = $request->getPost('projectID');
            $jobID = $request->getPost('jobID');
            $locationID = $this->user_session->userActiveLocation["locationID"];
            if ($projectID) {
                $jobs = $this->CommonTable('JobCard/Model/JobTable')->getJobListByProjectID($projectID, $locationID);
                $project = $this->CommonTable('JobCard/Model/ProjectTable')->getProjectDetailsByProjectID($projectID)->current();
            } if ($jobID) {
                $activities = $this->CommonTable('JobCard/Model/activityTable')->getActivityListByJobID($jobID, $locationID);
                $inquiryLogs = $this->CommonTable('JobCard/Model/inquiryLogTable')->getInquirylogsRelatedToJobID($jobID);
            }
            if (isset($project)) {
                $projectList['projectId'] = array(
                    'name' => $project['projectName'],
                    'code' => $project['projectCode'],
                    'createTime' => $project['createdTimeStamp'],
                    'startTime' => $project['projectStartingAt']
                );
            }
            $jobList = [];
            $activityList = [];
            $inquiryLogList = [];
            if (isset($jobs)) {
                foreach ($jobs as $job) {
                    $jobList[$job['jobId']] = array(
                        'name' => $job['jobName'],
                        'code' => $job['jobReferenceNumber'],
                        'createTime' => $job['createdTimeStamp'],
                        'startTime' => $job['jobStartingTime'],
                        'jobID' => $job['jobId']
                    );
                }
            }
            if (isset($activities)) {
                foreach ($activities as $activity) {
                    $activityList[$activity['activityId']] = array(
                        'name' => $activity['activityName'],
                        'code' => $activity['activityCode'],
                        'createTime' => $activity['createdTimeStamp'],
                        'startTime' => $activity['activityStartingTime'],
                        'activityID' => $activity['activityId']
                    );
                }
            }

            if (isset($inquiryLogs)) {
                foreach ($inquiryLogs as $inquiryLog) {
                    $inquiryLogList[$inquiryLog['inquiryLogID']] = array(
                        'name' => $inquiryLog['inquiryLogDescription'],
                        'code' => $inquiryLog['inquiryLogReference'],
                        'createTime' => $inquiryLog['inquiryLogDateAndTime'],
                        'startTime' => $inquiryLog['inquiryLogDateAndTime'],
                        'inquiryLogID' => $inquiryLog['inquiryLogID']
                    );
                }
            }
            $jobRelatedDetails['project'] = $projectList;
            $jobRelatedDetails['job'] = $jobList;
            $jobRelatedDetails['activity'] = $activityList;
            $jobRelatedDetails['inquiryLog'] = $inquiryLogList;

            $this->data = $jobRelatedDetails;
            $this->status = true;
            return $this->JSONRespond();
        }
    }

    /**
     * Get All jobs for dropdown for view
     * @author sharmilan <sharmilan@thinkcube.com>
     * @return type
     */
    public function searchAllJobsForDropdownAction()
    {
        $searchrequest = $this->getRequest();
        if ($searchrequest->isPost()) {
            $jobSearchKey = $searchrequest->getPost('searchKey');
            $locationID = $searchrequest->getPost('locationID');
            if ($locationID == '' || $locationID == null) {
                $locationID = $this->user_session->userActiveLocation["locationID"];
            }
            $jobs = $this->CommonTable('JobCard/Model/JobTable')->searchAllJobsForDropDown($locationID, $jobSearchKey);

            $jobList = array();
            foreach ($jobs as $job) {
                $temp['value'] = $job['jobId'];
                $temp['text'] = $job['jobReferenceNumber'] . ' - ' . $job['jobName'];
                $jobList[] = $temp;
            }

            $this->setLogMessage("Retrive Job list for dropdown.");
            $this->data = array('list' => $jobList);
            return $this->JSONRespond();
        }
    }

    /**
     * Get Job list view by using jobID
     * @author sharmilan <sharmilan@thinkcube.com>
     * @return type
     */
    public function getJobListViewBySearchAction()
    {
        $request = $this->getRequest();
        $locationID = $this->user_session->userActiveLocation['locationID'];
        if ($request->isPost()) {
            $paginated = false;
            $jobID = $request->getPost('jobID');

            if ($jobID) {
                $jobList = $this->CommonTable('JobCard/Model/JobTable')->getJobListData($jobID, $locationID);
            } else {
                $paginated = TRUE;
                $jobList = $this->getPaginatedJob($locationID);
            }
            $view = new ViewModel(array(
                'jobList' => $jobList,
                'paginated' => $paginated,
                'status' => $this->getStatusesList(),
            ));

            $view->setTerminal(true);
            $view->setTemplate('job-card/job/list-table.phtml');

            $this->status = true;
            $this->html = $view;
            return $this->JSONRespondHtml();
        }
    }

    /**
     * @author sharmilan <sharmilan@thinkcube.com>
     * Get Job List View By date Range
     * @return type
     */
    public function getJobListViewByDateRangeAction()
    {
        $request = $this->getRequest();
        $locationID = $this->user_session->userActiveLocation['locationID'];
        if ($request->isPost()) {
            $fromDate = $this->getGMTDateTime('Y-m-d H:i:s', $request->getPost('fromDate') . ' 00:00');
            $toDate = $this->getGMTDateTime('Y-m-d H:i:s', $request->getPost('toDate') . ' 23:59');

            $jobList = $this->CommonTable('JobCard/Model/JobTable')->getJobListDataByDateRange($locationID, $fromDate, $toDate);

            $view = new ViewModel(array(
                'jobList' => $jobList,
                'paginated' => false,
                'status' => $this->getStatusesList(),
            ));

            $view->setTerminal(true);
            $view->setTemplate('job-card/job/list-table.phtml');

            $this->status = true;
            $this->html = $view;
            return $this->JSONRespondHtml();
        }
    }

    public function changeJobStatusAction()
    {
        $this->status = false;
        $this->msg = $this->getMessage('ERR_INVALID_REQUEST');
        $this->data = null;

        $request = $this->getRequest();
        if ($request->isPost()) {
            $postData = $request->getPost()->toArray();

            $job = $this->CommonTable('JobCard\Model\JobTable')->getJobDetails($postData['jobId'])->current();
            if ($job) {
                $activityData = [];
                switch ($postData['jobStatus']) {
                    case '3': //open
                        $activityData = array('activityStatus' => '3');
                        break;
                    case '4': //closed
                        $activityData = array('activityStatus' => '4');
                        break;
                    case '8': //in progress
                        $activityData = array('activityStatus' => '3');
                        break;
                    case '9': //completed
                        $postData['jobProgress'] = 100;
                        $activityData = array('activityStatus' => '9', 'activityProgress' => 100);
                        break;
                }

                try {
                    $this->beginTransaction();

                    $updateJobStatus = $this->CommonTable('JobCard\Model\JobTable')->updateJobByArray($postData, $postData['jobId']);
                    //get activity list of the job
                    $jobActivities = $this->CommonTable('JobCard\Model\ActivityTable')->getActivityListByJobID($job['jobId']);
                    $activityList = [];
                    foreach ($jobActivities as $activity) {
                        $activityList[] = $activity['activityId'];
                    }
                    //update activity list
                    if ($updateJobStatus && !empty($activityList)) {
                        $updateActivityStatue = $this->CommonTable('JobCard\Model\ActivityTable')->updateMultipleActivities($activityData, $activityList);
                    } else {
                        $updateActivityStatue = true;
                    }

                    if ($updateJobStatus && $updateActivityStatue) {
                        $this->commit();
                        $this->status = true;
                        $this->msg = $this->getMessage('SUCC_JOB_UPDATE');
                    } else {
                        $this->rollback();
                        $this->msg = $this->getMessage('ERR_JOB_UPDATE');
                    }
                } catch (\Exception $ex) {
                    $this->rollback();
                    $this->msg = $this->getMessage('ERR_JOB_UPDATE');
                }
            } else {
                $this->msg = $this->getMessage('ERR_JOB_NOT_EXIST');
            }
        }
        return $this->JSONRespond();
    }

}
