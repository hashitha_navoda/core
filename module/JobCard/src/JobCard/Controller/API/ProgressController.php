<?php

/**
 * @author Ashan Madushka <ashan@thinkcube.com>
 * This file contains Progress related Api controller functions
 */

namespace JobCard\Controller\API;

use Zend\View\Model\ViewModel;
use Core\Controller\CoreController;
use Zend\Session\Container;
use JobCard\Model\Activity;
use JobCard\Model\Job;
use JobCard\Model\Project;

class ProgressController extends CoreController
{

//    update activitys ,jobs and projects
    public function updateActivityAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $locationID = $this->user_session->userActiveLocation["locationID"];
            $projectID = $request->getPost('projectID');
            $jobID = $request->getPost('jobID');
            $activityID = $request->getPost('activityID');
            $activityCode = $request->getPost('activityCode');
            $progress = $request->getPost('progress');
            $this->beginTransaction();
            $data = array(
                'activityId' => $activityID,
                'activityProgress' => $progress
            );
            $activityData = new Activity();
            $activityData->exchangeArray($data);
            $result = $this->CommonTable('JobCard\Model\ActivityTable')->updateActivityProgress($activityData);
            $this->status = false;
            if ($result) {
                $activitiesData = $this->CommonTable('JobCard\Model\ActivityTable')->getActivityListByJobID($jobID);
                $progressPercentage = 0.00;
                foreach ($activitiesData as $value) {
                    $value = (object) $value;
                    $projectID = $value->projectId;
                    $progressPercentage+=($value->activityWeight / 100) * ($value->activityProgress / 100);
                }
                $progressPercentage = $progressPercentage * 100;
                $jobData = array(
                    'jobId' => $jobID,
                    'jobProgress' => $progressPercentage,
                );
                $jobDetails = new Job();
                $jobDetails->exchangeArray($jobData);
                $jobResult = $this->CommonTable('JobCard/Model/JobTable')->updateJobProgress($jobDetails);
                if ($jobResult) {
                    $jobData = $this->CommonTable('JobCard/Model/JobTable')->getJobListByProjectID($projectID, $locationID);
                    $projectProgressPercentage = 0.00;
                    foreach ($jobData as $value) {
                        $value = (object) $value;
                        $projectProgressPercentage+=($value->jobWeight / 100) * ($value->jobProgress / 100);
                    }
                    $projectProgressPercentage = $projectProgressPercentage * 100;

                    $data = array(
                        'projectId' => $projectID,
                        'projectProgress' => $projectProgressPercentage,
                    );
                    $projectDetails = new Project();
                    $projectDetails->exchangeArray($data);
                    $projectResult = $this->CommonTable('JobCard\Model\ProjectTable')->updateProjectProgress($projectDetails);
                    if ($projectResult) {
                        $this->commit();
                        $this->status = true;
                        $this->msg = $this->getMessage('SUCC_UPDATE_ACTIVITY_PROGRESS', [$activityCode]);
                        $this->flashMessenger()->addMessage($this->msg);
                    } else {
                        $this->msg = $this->getMessage('ERR_UPDATE_PROJECT_PROGRESS');
                        $this->rollback();
                    }
                } else {
                    $this->msg = $this->getMessage('ERR_UPDATE_JOB_PROGRESS');
                    $this->rollback();
                }
            } else {
                $this->msg = $this->getMessage('ERR_UPDATE_ACTIVITY_PROGRESS');
                $this->rollback();
            }
            return $this->JSONRespond();
        }
    }

}
