<?php

/**
 * @author Ashan Madushka <ashan@thinkcube.com>
 * This file contains Project related Api controller functions
 */

namespace JobCard\Controller\API;

use Zend\View\Model\ViewModel;
use Core\Controller\CoreController;
use Zend\Session\Container;
use JobCard\Model\Project;
use JobCard\Model\ProjectOwners;
use JobCard\Model\ProjectSupervisors;
use JobCard\Model\ProjectDivisions;
use DateTime;

class ProjectController extends CoreController
{

//save project details
    public function saveProjectAction()
    {

        $request = $this->getRequest();
        if ($request->isPost()) {
            $this->beginTransaction();

            $locationID = $this->user_session->userActiveLocation['locationID'];
            $result = $this->getReferenceNoForLocation(21, $locationID);
            $locationReferenceID = $result['locRefID'];

            $projectCode = $request->getPost('projectCode');
            while ($projectCode) {
                if ($this->CommonTable('JobCard\Model\ProjectTable')->getProjectDetailsByProjectCode($projectCode)->current()) {
                    if ($locationReferenceID) {
                        $newProjectCode = $this->getReferenceNumber($locationReferenceID);
                        if ($projectCode == $newProjectCode) {
                            $this->updateReferenceNumber($locationReferenceID);
                            $projectCode = $this->getReferenceNumber($locationReferenceID);
                        } else {
                            $projectCode = $newProjectCode;
                        }
                    } else {
                        $this->rollback();
                        $this->status = false;
                        $this->msg = $this->getMessage('ERR_PROJECT_CODE_EXIST');
                        return $this->JSONRespond();
                    }
                } else {
                    break;
                }
            }

            $projectName = $request->getPost('projectName');
            $projectTypeID = $request->getPost('projectTypeID');
            $projectDescription = $request->getPost('projectDescription');
            $owners = $request->getPost('owners');
            $supervisors = $request->getPost('supervisors');
            $customerID = $request->getPost('customerID');
            $customerProfileID = $request->getPost('customerProfileID');
            $durationMinutes = $request->getPost('durationMinutes');
            $extimatedCost = $request->getPost('extimatedCost');
            $divisions = $request->getPost('divisions');
            $startingTime = '';
            $endingTime = '';
            $DateFormat = $this->convertUserDateFormatToPhpFormat() . ' H:i';
            if ($request->getPost('startingTime') != '') {
                $startingTime = $this->getGMTDateTime('Y-m-d H:i:s', $request->getPost('startingTime'));
            }
            if ($request->getPost('endingTime') != '') {
                $endingTime = $this->getGMTDateTime('Y-m-d H:i:s', $request->getPost('endingTime'));
            }

            $this->beginTransaction();

            //save project Data
            $entityID = $this->createEntity();
            $statusName = 'Open';
            $statusID = $this->getStatusIDByStatusName($statusName);
            $projectData = array(
                'projectCode' => $projectCode,
                'projectName' => $projectName,
                'projectType' => $projectTypeID,
                'projectDescription' => $projectDescription,
                'projectEstimatedTimeDuration' => $durationMinutes,
                'projectStartingAt' => $startingTime,
                'projectEndAt' => $endingTime,
                'projectEstimatedCost' => $extimatedCost,
                'locationId' => $locationID,
                'projectProgress' => 0.00,
                'projectStatus' => 3,
                'customerID' => $customerID,
                'customerProfileID' => $customerProfileID,
                'entityID' => $entityID,
                'projectStatus' => $statusID,
            );
            $project = new Project;
            $project->exchangeArray($projectData);
            $projectID = $this->CommonTable('JobCard\Model\ProjectTable')->saveProject($project);
            if ($projectID) {
                //save project owners
                if (isset($owners)) {
                    foreach ($owners as $key) {
                        $projectOwnersData = array(
                            'projectId' => $projectID,
                            'employeeId' => $key,
                        );
                        $projectOwners = new ProjectOwners;
                        $projectOwners->exchangeArray($projectOwnersData);
                        $this->CommonTable('JobCard\Model\ProjectOwnersTable')->saveProjectOwners($projectOwners);
                    }
                }

                //save project supervisors
                if (isset($supervisors)) {
                    foreach ($supervisors as $key) {
                        $projectSupervisorsData = array(
                            'projectId' => $projectID,
                            'employeeId' => $key,
                        );
                        $projectSupervisors = new ProjectSupervisors;
                        $projectSupervisors->exchangeArray($projectSupervisorsData);
                        $this->CommonTable('JobCard\Model\ProjectSupervisorsTable')->saveProjectSupervisors($projectSupervisors);
                    }
                }
                //save project Divisions
                if (isset($divisions)) {
                    foreach ($divisions as $key) {
                        $projectDivisionsData = array(
                            'projectId' => $projectID,
                            'divisionId' => $key,
                        );
                        $projectDivisions = new ProjectDivisions;
                        $projectDivisions->exchangeArray($projectDivisionsData);
                        $this->CommonTable('JobCard\Model\ProjectDivisionsTable')->saveProjectDivisions($projectDivisions);
                    }
                }
                $inqID = $request->getPost('inqID');
                if ($inqID) {
                    $inquiryData = array(
                        'inquiryComplainRelationDocumentID' => $projectID,
                        'inquiryLogID' => $inqID,
                        'documentTypeID' => '21'
                    );
                    $inquiryDetails = (object) $inquiryData;
                    $this->CommonTable('JobCard\Model\InquiryComplainRelationTable')->saveRelatedDocument($inquiryDetails);
                }
                if ($locationReferenceID) {
                    $this->updateReferenceNumber($locationReferenceID);
                }
                $this->commit();
                $this->status = true;
                $this->msg = $this->getMessage('SUCC_ADDED_PROJECT', array($projectCode));
                $this->flashMessenger()->addMessage($this->msg);
                $this->data = $projectID;
            } else {
                $this->status = false;
                $this->msg = $this->getMessage('ERR_OCURED_WHILE_ADDED_PROJECT', array($projectCode));
                $this->rollback();
            }

            return $this->JSONRespond();
        }
    }

    //get project list by search key
    public function getProjectSearchListAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $serchKey = $request->getPost('searchKey');
            $proList = $this->CommonTable('JobCard\Model\ProjectTable')->getProjectListBySearchKey($serchKey);
            $projectList = new ViewModel(array(
                'project_list' => $proList,
                'paginated' => false,
                'status' => $this->getStatusesList(),
            ));
            $projectList->setTemplate('job-card/project/project-list');
            $this->html = $projectList;
            $this->status = true;
            return $this->JSONRespondHtml();
        }
    }

    public function deleteProjectByProjectIDAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $locationID = $this->user_session->userActiveLocation['locationID'];
            $projectID = $request->getPost('projectID');
            $proList = $this->CommonTable('JobCard\Model\ProjectTable')->getProjectDetailsByProjectID($projectID)->Current();
            $entityID = $proList['entityID'];
            $projectCode = $proList['projectCode'];
            $jobList = $this->CommonTable('JobCard/Model/JobTable')->getJobListByProjectID($projectID, $locationID)->current();
            if (!$jobList) {
                $statusName = 'Cancel';
                $statusID = $this->getStatusIDByStatusName($statusName);
                $data = array(
                    'projectStatus' => $statusID,
                    'projectId' => $projectID,
                );
                if ($this->updateDeleteInfoEntity($entityID)) {
                    $this->status = true;
                    $this->msg = $this->getMessage('SUCC_DELETED_PROJECT', array($projectCode));
                } else {
                    $this->status = false;
                    $this->msg = $this->getMessage('ERR_OCURED_WHILE_DELETING_PROJECT', array($projectCode));
                }
            } else {
                $this->status = false;
                $this->msg = $this->getMessage('ERR_PROJECTS_ID_USED_FOR_JOB');
            }
            $this->flashMessenger()->addMessage($this->msg);
            return $this->JSONRespond();
        }
    }

    //update Project Details
    public function updateProjectAction()
    {

        $request = $this->getRequest();
        if ($request->isPost()) {
            $this->beginTransaction();

            $locationID = $this->user_session->userActiveLocation['locationID'];
            $projectCode = $request->getPost('projectCode');
            $projectID = $request->getPost('projectID');
            $projectName = $request->getPost('projectName');
            $projectTypeID = $request->getPost('projectTypeID');
            $projectDescription = $request->getPost('projectDescription');
            $owners = $request->getPost('owners');
            $supervisors = $request->getPost('supervisors');
            $customerID = $request->getPost('customerID');
            $customerProfileID = $request->getPost('customerProfileID');
            $durationMinutes = $request->getPost('durationMinutes');
            $extimatedCost = $request->getPost('extimatedCost');
            $divisions = $request->getPost('divisions');
            $startingTime = '';
            $endingTime = '';
            $DateFormat = $this->convertUserDateFormatToPhpFormat() . ' H:i';
            if ($request->getPost('startingTime') != '') {
                $startingTime = $this->getGMTDateTime('Y-m-d H:i:s', $request->getPost('startingTime'));
            }
            if ($request->getPost('endingTime') != '') {
                $endingTime = $this->getGMTDateTime('Y-m-d H:i:s', $request->getPost('endingTime'));
            }

            $this->beginTransaction();

            //update project Data
            $projectData = array(
                'projectId' => $projectID,
                'projectCode' => $projectCode,
                'projectName' => $projectName,
                'projectType' => $projectTypeID,
                'projectDescription' => $projectDescription,
                'projectEstimatedTimeDuration' => $durationMinutes,
                'projectStartingAt' => $startingTime,
                'projectEndAt' => $endingTime,
                'projectEstimatedCost' => $extimatedCost,
                'locationId' => $locationID,
                'projectProgress' => 0.00,
                'customerID' => $customerID,
                'customerProfileID' => $customerProfileID,
            );
            $project = new Project;
            $project->exchangeArray($projectData);
            $result = $this->CommonTable('JobCard\Model\ProjectTable')->updateProject($project);
            if ($result) {
                //update project owners
                $addedProjectOwners = $this->CommonTable('JobCard\Model\ProjectOwnersTable')->getProjectOwnersByProjectID($projectID);
                foreach ($addedProjectOwners as $key) {
                    $this->CommonTable('JobCard\Model\ProjectOwnersTable')->deleteProjectOwnerByProjectAndOwnerID($projectID, $key['employeeID']);
                }

                foreach ($owners as $key) {
                    $projectOwnersData = array(
                        'projectId' => $projectID,
                        'employeeId' => $key,
                    );
                    $projectOwners = new ProjectOwners;
                    $projectOwners->exchangeArray($projectOwnersData);
                    $this->CommonTable('JobCard\Model\ProjectOwnersTable')->saveProjectOwners($projectOwners);
                }

                //update project supervisors
                $addedProjectSupervisors = $this->CommonTable('JobCard\Model\ProjectSupervisorsTable')->getProjectSupervisorsByProjectID($projectID);
                foreach ($addedProjectSupervisors as $key) {
                    $this->CommonTable('JobCard\Model\ProjectSupervisorsTable')->deleteProjectSupervisorsByProjectAndSupervisorID($projectID, $key['employeeID']);
                }

                foreach ($supervisors as $key) {
                    $projectSupervisorsData = array(
                        'projectId' => $projectID,
                        'employeeId' => $key,
                    );
                    $projectSupervisors = new ProjectSupervisors;
                    $projectSupervisors->exchangeArray($projectSupervisorsData);
                    $this->CommonTable('JobCard\Model\ProjectSupervisorsTable')->saveProjectSupervisors($projectSupervisors);
                }

                //update project Divisions
                $addedProjectDivisions = $this->CommonTable('JobCard\Model\ProjectDivisionsTable')->getProjectDivisionsByProjectID($projectID);
                foreach ($addedProjectDivisions as $key) {
                    $this->CommonTable('JobCard\Model\ProjectDivisionsTable')->deleteProjectDivisionsByProjectAndDivisionID($projectID, $key['divisionID']);
                }
                foreach ($divisions as $key) {
                    $projectDivisionsData = array(
                        'projectId' => $projectID,
                        'divisionId' => $key,
                    );
                    $projectDivisions = new ProjectDivisions;
                    $projectDivisions->exchangeArray($projectDivisionsData);
                    $this->CommonTable('JobCard\Model\ProjectDivisionsTable')->saveProjectDivisions($projectDivisions);
                }

                $this->commit();
                $this->status = true;
                $this->msg = $this->getMessage('SUCC_UPDATE_PROJECT', array($projectCode));
                $this->flashMessenger()->addMessage($this->msg);
                $this->data = $projectID;
            } else {
                $this->status = false;
                $this->msg = $this->getMessage('ERR_OCURED_WHILE_UPDATE_PROJECT', array($projectCode));
                $this->rollback();
            }

            return $this->JSONRespond();
        }
    }

    public function getProjectDetailsAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $this->beginTransaction();
            $projectID = $request->getPost('projectID');
            $pData = $this->CommonTable('JobCard/Model/ProjectTable')->getProjectDetailsByProjectID($projectID);
            $projectData = (object) $pData->current();
            if ($projectData) {
                if($projectData->customerStatus == 2){
                    $this->status = false;
                    $this->msg = $this->getMessage('ERR_PROJECT_CUSTOMER_INACTIVE');
                    return $this->JSONRespond();
                }
                $this->status = true;
                $this->data = $projectData;
            } else {
                $this->status = false;
            }
            return $this->JSONRespond();
        }
    }

    /**
     * @author Ashan <ashan@thinkcube.com>
     * search projects for dropdown
     */
    public function searchProjectsForDropdownAction()
    {
        $searchrequest = $this->getRequest();
        if ($searchrequest->isPost()) {
            $projectSearchKey = $searchrequest->getPost('searchKey');
            $locationID = $searchrequest->getPost('locationID');
            if ($locationID == '' || $locationID == null) {
                $locationID = $this->user_session->userActiveLocation["locationID"];
            }
            $addFlag = $searchrequest->getPost('addFlag');

            $projects = $this->CommonTable('JobCard/Model/ProjectTable')->searchProjectsForDropDown($locationID, $projectSearchKey);

//            if (isset($addFlag) && $addFlag == 1) {
//                $otherOptions[] = ["value" => "reset", "text" => "Reset Select"];
//            }

            $projectList = array();
            foreach ($projects as $project) {
                $temp['value'] = $project['projectId'];
                $temp['text'] = $project['projectCode'] . " (" . $project['projectProgress'] . "%) - " . $project['projectName'];
                $projectList[] = $temp;
            }

            $this->setLogMessage("Retrive project list for dropdown.");
            $this->data = array('list' => $projectList);
            return $this->JSONRespond();
        }
    }

    /**
     * @author Ashan <ashan@thinkcube.com>
     * get project related all produts details
     */
    public function getProjectProductsDetailsByProjectIDAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $projectID = $request->getPost('projectID');
            $projectCode = $request->getPost('projectCode');
            $locationID = $this->user_session->userActiveLocation['locationID'];
            $data = $this->getAllJobProductsByProjectId($projectID, $locationID);
            $proData = $this->CommonTable('JobCard\Model\ProjectTable')->getProjectDetailsByProjectID($projectID)->current();
            $cusProfDetails = $this->CommonTable('Invoice\Model\CustomerProfileTable')->getProfileByCustomerProfileID($proData['customerProfileID'])->current();

            if($cusProfDetails['customerStatus'] == 2){
                $this->msg = $this->getMessage('ERR_PROJECT_CUSTOMER_INACTIVE');
                $this->status = false;
                return $this->JSONRespond();
            }

            if ($data['flag'] == 'hasProducts') {
                $this->data = array(
                    'activityProducts' => $data['activityProducts'],
                    'customer' => $data['customer'],
                    'locationProducts' => $data['locationProducts'],
                    'customerProfName' => $cusProfDetails['customerProfileName'],
                );
                $this->status = true;
            } else if ($data['flag'] == 'invoiced') {
                $this->msg = $this->getMessage('ERR_PRODUCT_WAS_INVOICED_IN_PROJECT', $projectCode);
                $this->status = false;
            } else if ($data['flag'] == 'noJobs') {
                $this->msg = $this->getMessage('ERR_DOSNOT_HAVE_ANY_JOB_FOR_PROJECT');
                $this->status = false;
            }
            return $this->JSONRespond();
        }
    }

    /**
     * @author Lahiru Abeysinghe <lahiru@thinkcube.com>
     * this function gets all Project supervisor IDs and Project Owner IDs that related to the given ProjectID
     * @return JSON Respond
     */
    public function getProjectSupervisorAndProjectOwnerByProjectIDAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $projectID = $request->getPost('projectID');
            $projectOwnersData = $this->CommonTable('JobCard/Model/ProjectOwnersTable')->getProjectOwnersByProjectID($projectID);
            $projectOwners = array();
            $inc = 0;
            foreach ($projectOwnersData as $value) {
                $value = (object) $value;
                $projectOwners[$inc] = $value->employeeID;
                $inc++;
            }
            $projectSupervisorsData = $this->CommonTable('JobCard\Model\ProjectSupervisorsTable')->getProjectSupervisorsByProjectID($projectID);
            $projectSupervisors = array();
            $incs = 0;
            foreach ($projectSupervisorsData as $value) {
                $value = (object) $value;
                $projectSupervisors[$incs] = $value->employeeId;
                $incs++;
            }

            $employee['0'] = $projectSupervisors;
            $employee['1'] = $projectOwners;
            $employee['2'] = $inc;
            $employee['3'] = $incs;

            $this->status = true;
            $this->data = $employee;

            return $this->JSONRespond();
        }
    }

    /**
     * Get project List view
     * @author sharmilan <sharmilan@thinkcube.com>
     * @return type
     */
    public function getProjectListViewByIDAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $projectID = $request->getPost('projectID');
            $locationID = $this->user_session->userActiveLocation['locationID'];
            $proList = $this->CommonTable('JobCard\Model\ProjectTable')->getProjectLisViewtByID($locationID, $projectID);

            if ($projectID) {
                $projectList = new ViewModel(array(
                    'project_list' => $proList,
                    'paginated' => false,
                    'status' => $this->getStatusesList(),
                    'userDateTimeFormat' => $this->convertUserDateFormatToPhpFormat() . ' H:i',
                ));
            } else {
                $paginator = $this->CommonTable('JobCard/Model/ProjectTable')->fetchAll(true, $locationID);
                $paginator->setCurrentPageNumber((int) $this->params()->fromRoute('param1', 1));
                $paginator->setItemCountPerPage(8);

                $projectList = new ViewModel(array(
                    'project_list' => $paginator,
                    'status' => $this->getStatusesList(),
                    'userDateTimeFormat' => $this->convertUserDateFormatToPhpFormat() . ' H:i',
                ));
            }

            $projectList->setTemplate('job-card/project/project-list');
            $projectList->setTerminal(true);

            $this->html = $projectList;
            $this->status = true;
            return $this->JSONRespondHtml();
        }
    }

    public function getProjectListViewByDateRangeAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $fromDate = $this->getGMTDateTime('Y-m-d H:i:s', $request->getPost('fromDate') . ' 00:00');
            $toDate = $this->getGMTDateTime('Y-m-d H:i:s', $request->getPost('toDate') . ' 23:59');
            $locationID = $this->user_session->userActiveLocation['locationID'];
            $proList = $this->CommonTable('JobCard\Model\ProjectTable')->getProjectLisViewtByDateRange($locationID, $fromDate, $toDate);

            $projectList = new ViewModel(array(
                'project_list' => $proList,
                'paginated' => false,
                'status' => $this->getStatusesList(),
                'userDateTimeFormat' => $this->convertUserDateFormatToPhpFormat() . ' H:i',
            ));


            $projectList->setTemplate('job-card/project/project-list');
            $projectList->setTerminal(true);

            $this->html = $projectList;
            $this->status = true;
            return $this->JSONRespondHtml();
        }
    }

    public function changeProjectStatusAction()
    {
        $this->status = false;
        $this->msg = $this->getMessage('ERR_INVALID_REQUEST');
        $this->data = null;

        $request = $this->getRequest();
        if ($request->isPost()) {
            $postData = $request->getPost()->toArray();
            //check whether project exist
            $project = $this->CommonTable('JobCard\Model\ProjectTable')->getProjectDetailsByProjectID($postData['projectId'])->current();
            if ($project) {
                $jobData = $activityData = [];
                switch ($postData['projectStatus']) {
                    case '3': //open
                        $jobData = array('jobStatus' => '3');
                        $activityData = array('activityStatus' => '3');
                        break;
                    case '4': //closed
                        $jobData = array('jobStatus' => '4');
                        $activityData = array('activityStatus' => '4');
                        break;
                    case '8': //in progress
                        $jobData = array('jobStatus' => '3');
                        $activityData = array('activityStatus' => '3');
                        break;
                    case '9': //completed
                        $postData['projectProgress'] = 100;
                        $jobData = array('jobStatus' => '9', 'jobProgress' => 100);
                        $activityData = array('activityStatus' => '9', 'activityProgress' => 100);
                        break;
                }

                try {
                    $this->beginTransaction();
                    //update project status
                    $updateProjectStatue = $this->CommonTable('JobCard\Model\ProjectTable')->updateProjectByArray($postData, $postData['projectId']);
                    if ($updateProjectStatue) {
                        $this->updateEntity($project['entityID']);
                        $locationId = $this->user_session->userActiveLocation['locationID'];
                        //get project jobs
                        $projectJobs = $this->CommonTable('JobCard\Model\JobTable')->getJobListByProjectID($postData['projectId'], $locationId);
                        $jobList = $activityList = [];
                        foreach ($projectJobs as $job) {
                            $jobList[] = $job['jobId'];
                            //get job activity list
                            $jobActivities = $this->CommonTable('JobCard\Model\ActivityTable')->getActivityListByJobID($job['jobId']);
                            foreach ($jobActivities as $activity) {
                                $activityList[] = $activity['activityId'];
                            }
                        }
                        //update job list of the project
                        if (!empty($jobList)) {
                            $updateJobStatus = $this->CommonTable('JobCard\Model\JobTable')->updateMultipleJobs($jobData, $jobList);
                        } else {
                            $updateJobStatus = true;
                        }

                        //update activity list of the job
                        if ($updateJobStatus && !empty($activityList)) {
                            $updateActivityStatue = $this->CommonTable('JobCard\Model\ActivityTable')->updateMultipleActivities($activityData, $activityList);
                        } else {
                            $updateActivityStatue = true;
                        }

                        if ($updateJobStatus && $updateActivityStatue) {
                            $this->commit();
                            $this->status = true;
                            $this->msg = $this->getMessage('SUCC_UPDATE_PROJECT', [$project['projectCode']]);
                        } else {
                            $this->rollback();
                            $this->msg = $this->getMessage('ERR_OCURED_WHILE_UPDATE_PROJECT', $project['projectCode']);
                        }
                    } else {
                        $this->rollback();
                        $this->msg = $this->getMessage('ERR_OCURED_WHILE_UPDATE_PROJECT', $project['projectCode']);
                    }
                } catch (\Exception $ex) {
                    $this->rollback();
                    $this->msg = $this->getMessage('ERR_OCURED_WHILE_UPDATE_PROJECT', $project['projectCode']);
                }
            } else {
                $this->msg = $this->getMessage('ERR_PROJECT_DOSNOT_EXIST');
            }
        }
        return $this->JSONRespond();
    }

}
