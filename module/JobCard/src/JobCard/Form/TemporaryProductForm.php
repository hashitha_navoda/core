<?php

/**
 * @author Sandun <sandun@thinkcube.com>
 * This file contains temporary item related Form fields
 */

namespace JobCard\Form;

use Zend\Form\Form;

class TemporaryProductForm extends Form
{

    public function __construct($data = NULL)
    {
        parent::__construct('temporaryProduct');
        $this->setAttribute('method', 'post');
        $this->setAttribute('class', "form-horizontal");
        $this->setAttribute('role', "form");
        $this->add(array(
            'name' => 'temporaryProductID',
            'type' => 'Hidden',
        ));
        $this->add(array(
            'name' => 'temporaryProductCode',
            'type' => 'Text',
            'options' => array(
            ),
            'attributes' => array(
                'id' => 'temporaryProductCode',
                'class' => 'form-control',
                'placeholder' => _('Eg: LP10')
            ),
        ));
        $this->add(array(
            'name' => 'temporaryProductName',
            'type' => 'Text',
            'options' => array(
            ),
            'attributes' => array(
                'id' => 'temporaryProductName',
                'class' => 'form-control',
                'placeholder' => _('Eg: Laptop')
            ),
        ));
        $this->add(array(
            'name' => 'temporaryProductDescription',
            'type' => 'textarea',
            'options' => array(
            ),
            'attributes' => array(
                'id' => 'temporaryProductDescription',
                'class' => 'form-control',
            ),
        ));

        $this->add(array(
            'name' => 'batchProduct',
            'type' => 'Text',
            'attributes' => array(
                'id' => 'batchProduct',
                'class' => 'form-control',
                'placeholder' => _('Eg: BATCH001')
            ),
        ));
        $this->add(array(
            'name' => 'serialProduct',
            'type' => 'Text',
            'attributes' => array(
                'id' => 'serialProduct',
                'class' => 'form-control',
                'placeholder' => _('Eg: SERIAL001')
            ),
        ));

        $this->add(array(
            'name' => 'temporaryProductQuantity',
            'type' => 'Text',
            'options' => array(
            ),
            'attributes' => array(
                'id' => 'temporaryProductQuantity',
                'class' => 'form-control',
                'placeholder' => _('eg : 100'),
                'value' => $data['tempQty'],
                'disabled' => true,
            ),
        ));

        $this->add(array(
            'name' => 'temporaryProductPrice',
            'type' => 'Text',
            'options' => array(
            ),
            'attributes' => array(
                'id' => 'temporaryProductPrice',
                'class' => 'form-control',
                'placeholder' => _('eg : $100')
            ),
        ));

        $this->add(array(
            'name' => 'addTempProductBtn',
            'type' => 'submit',
            'attributes' => array(
                'value' => 'Save',
                'id' => 'tempProductSave',
                'class' => 'btn btn-primary',
            ),
        ));
    }

    public function populate($data)
    {
        foreach ($data as $field => $value) {
            if ($this->has($field)) {
                $this->get($field)->setValue($value);
            }
        }

        return $this;
    }

}
