<?php

/**
 * @author Sandun <sandun@thinkcube.com>
 * Inquiry status form
 */

namespace JobCard\Form;

use Zend\Form\Form;

class InquiryStatusForm extends Form {

    public function __construct($inquiryStatusTypes = NULL) {
        parent::__construct('inquiryStatus');
        $this->setAttribute('method', 'post');
        $this->setAttribute('class', "");
        $this->setAttribute('role', "form");

        $this->add(array(
            'name' => 'inquiryStatusID',
            'type' => 'Hidden',
        ));

        $this->add(array(
            'name' => 'inquiryStatusName',
            'type' => 'Text',
            'options' => array(
            ),
            'attributes' => array(
                'id' => 'inquiryStatusName',
                'autocomplete' => false,
                'class' => 'form-control',
                'placeholder' => _('Eg: Repair')
            ),
        ));

        $this->add(array(
            'name' => 'inquiryStatusTypeID',
            'type' => 'select',
            'attributes' => array(
                'id' => 'inquiryStatusTypeID',
                'class' => 'form-control'
            ),
        ));

        $this->add(array(
            'name' => 'inquiryStatusTypeID',
            'type' => 'select',
            'options' => array(
                'disable_inarray_validator' => true,
                'empty_option' => 'Select an Inquiry Status Type',
                'value_options' => $inquiryStatusTypes
            ),
            'attributes' => array(
                'id' => 'inquiryStatusTypeID',
                'class' => 'form-control',
            ),
        ));
    }

}
