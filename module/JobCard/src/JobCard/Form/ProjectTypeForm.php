<?php

/**
 * @author Ashan Madushka   <ashan@thinkcube.com>
 */

namespace JobCard\Form;

use Zend\Form\Form;

class ProjectTypeForm extends Form
{

    public function __construct()
    {
        parent::__construct('projectType');
        $this->setAttribute('method', 'post');
        $this->setAttribute('class', "form-horizontal");
        $this->setAttribute('role', "form");

        $this->add(array(
            'name' => 'projectTypeID',
            'type' => 'Hidden',
        ));

        $this->add(array(
            'name' => 'projectTypeCode',
            'type' => 'Text',
            'options' => array(
            ),
            'attributes' => array(
                'id' => 'projectTypeCode',
                'autocomplete' => false,
                'class' => 'form-control',
                'placeholder' => _('Eg: Repair')
            ),
        ));

        $this->add(array(
            'name' => 'projectTypeName',
            'type' => 'Text',
            'options' => array(
            ),
            'attributes' => array(
                'id' => 'projectTypeName',
                'class' => 'form-control',
                'autocomplete' => false,
                'placeholder' => _('Eg: R_123')
            ),
        ));
    }

}
