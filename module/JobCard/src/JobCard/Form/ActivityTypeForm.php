<?php

namespace JobCard\Form;

use Zend\Form\Form;

class ActivityTypeForm extends Form
{

    function __construct()
    {

        parent::__construct('activityType');
        $this->setAttribute('method', 'post');
        $this->setAttribute("class", "form-horizontal");
        $this->setAttribute('role', "form");

        $this->add(array(
            'name' => 'activityTypeID',
            'type' => 'Hidden',
            'attribute' => array(
                'id' => 'activityTypeID'
            ),
        ));
        $this->add(array(
            'name' => 'activityTypeName',
            'type' => 'text',
            'attributes' => array(
                'id' => 'activityTypeName',
                'class' => 'form-control',
                'placeholder' => _('Activity Type Name'),
            ),
        ));
        $this->add(array(
            'name' => 'maxValue',
            'type' => 'text',
            'attributes' => array(
                'id' => 'maxValue',
                'class' => 'form-control',
                'placeholder' => _('Max Value'),
            )
        ));
        $this->add(array(
            'name' => 'minValue',
            'type' => 'text',
            'attributes' => array(
                'id' => 'minValue',
                'class' => 'form-control',
                'placeholder' => _('Min Value'),
            )
        ));
    }

}
