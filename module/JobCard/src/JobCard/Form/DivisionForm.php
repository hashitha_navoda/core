<?php

namespace JobCard\Form;

use Zend\Form\Form;

class DivisionForm extends Form
{

    function __construct()
    {
        parent::__construct('division');
        $this->setAttribute('method', 'post');
        $this->setAttribute("class", "form-horizontal");
        $this->setAttribute('role', "form");

        $this->add(array(
            'name' => 'divisionID',
            'type' => 'Hidden',
            'attribute' => array(
                'id' => 'divisionID'
            ),
        ));
        $this->add(array(
            'name' => 'divisionName',
            'type' => 'text',
            'attributes' => array(
                'id' => 'divisionName',
                'class' => 'form-control',
                'placeholder' => _('division Name'),
            ),
        ));
    }

}
