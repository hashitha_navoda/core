<?php

/**
 * @author Ashan Madushka   <ashan@thinkcube.com>
 * this file contails project form fields
 */

namespace JobCard\Form;

use Zend\Form\Form;

class ProjectForm extends Form
{

    public function __construct($userdateFormat)
    {
        parent::__construct('project');
        $this->setAttribute('method', 'post');
        $this->setAttribute('class', "form-horizontal");
        $this->setAttribute('role', "form");

        $this->add(array(
            'name' => 'projectID',
            'type' => 'Hidden',
        ));

        $this->add(array(
            'name' => 'projectCode',
            'type' => 'Text',
            'options' => array(
            ),
            'attributes' => array(
                'id' => 'projectCode',
                'autocomplete' => false,
                'class' => 'form-control',
                'placeholder' => _('Eg: PR_001')
            ),
        ));

        $this->add(array(
            'name' => 'projectName',
            'type' => 'Text',
            'options' => array(
            ),
            'attributes' => array(
                'id' => 'projectName',
                'class' => 'form-control',
                'autocomplete' => false,
                'placeholder' => _('Eg: Web Site Create')
            ),
        ));

        $this->add(array(
            'name' => 'projectType',
            'type' => 'Text',
            'options' => array(
            ),
            'attributes' => array(
                'id' => 'projectType',
                'class' => 'form-control',
                'autocomplete' => false,
                'placeholder' => _('Eg: Development')
            ),
        ));

        $this->add(array(
            'name' => 'projectOwners',
            'type' => 'Text',
            'options' => array(
            ),
            'attributes' => array(
                'id' => 'projectOwners',
                'class' => 'form-control',
                'autocomplete' => false,
                'placeholder' => _('Add Owners')
            ),
        ));

        $this->add(array(
            'name' => 'projectSupervisors',
            'type' => 'Text',
            'options' => array(
            ),
            'attributes' => array(
                'id' => 'projectSupervisors',
                'class' => 'form-control',
                'autocomplete' => false,
                'placeholder' => _('Eg: Amal')
            ),
        ));

        $this->add(array(
            'name' => 'projectDescription',
            'type' => 'Textarea',
            'options' => array(
            ),
            'attributes' => array(
                'id' => 'projectDescription',
                'class' => 'form-control',
                'autocomplete' => false,
//                'placeholder' => _('Eg: ......')
            ),
        ));

        $this->add(array(
            'name' => 'projectCustomer',
            'type' => 'Text',
            'options' => array(
            ),
            'attributes' => array(
                'id' => 'projectCustomer',
                'class' => 'form-control',
                'autocomplete' => false,
                'placeholder' => _('Eg: Kamal')
            ),
        ));

        $this->add(array(
            'name' => 'projectExtimatedTimeDuration',
            'type' => 'Text',
            'options' => array(
            ),
            'attributes' => array(
                'id' => 'projectExtimatedTimeDuration',
                'class' => 'form-control',
                'autocomplete' => false,
                'placeholder' => _('Eg: 4 Days')
            ),
        ));

        $this->add(array(
            'name' => 'projectStartingDate',
            'type' => 'text',
            'options' => array(
            ),
            'attributes' => array(
                'id' => 'projectStartingDate',
                'class' => 'form-control',
                'readonly' => true,
                'style' => "background-color: white",
                'autocomplete' => false,
                'placeholder' => _('Eg: ' . $userdateFormat),
                'data-date-format' => $userdateFormat
            ),
        ));

        $this->add(array(
            'name' => 'projectEndingDate',
            'type' => 'text',
            'options' => array(
            ),
            'attributes' => array(
                'id' => 'projectEndingDate',
                'class' => 'form-control',
                'readonly' => true,
                'style' => "background-color: white",
                'autocomplete' => false,
                'placeholder' => _('Eg: ' . $userdateFormat),
                'data-date-format' => $userdateFormat
            ),
        ));

        $this->add(array(
            'name' => 'projectExtimatedCost',
            'type' => 'Text',
            'options' => array(
            ),
            'attributes' => array(
                'id' => 'projectExtimatedCost',
                'class' => 'form-control',
                'autocomplete' => false,
                'placeholder' => _('Eg: $1000')
            ),
        ));

        $this->add(array(
            'name' => 'projectDivisions',
            'type' => 'Text',
            'options' => array(
            ),
            'attributes' => array(
                'id' => 'projectDivisions',
                'class' => 'form-control',
                'autocomplete' => false,
                'placeholder' => _('Eg: Finance')
            ),
        ));
    }

}
