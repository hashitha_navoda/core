<?php

namespace JobCard\Form;

use Zend\Form\Form;

class EmployeesForm extends Form {

    function __construct($data) {
        $designation = $data['designation'];
        $division = $data['division'];
        $users = $data['users'];
        parent::__construct('employees');
        $this->setAttribute('method', 'post');
        $this->setAttribute("class", "");
        $this->setAttribute('role', "form");

        $this->add(array(
            'name' => 'employeeID',
            'type' => 'Hidden',
            'attribute' => array(
                'id' => 'employeeID'
            ),
        ));
        $this->add(array(
            'name' => 'employeeFirstName',
            'type' => 'text',
            'attributes' => array(
                'id' => 'employeeFirstName',
                'class' => 'form-control',
                'placeholder' => _('First Name'),
            ),
        ));
        $this->add(array(
            'name' => 'employeeSecondName',
            'type' => 'text',
            'attributes' => array(
                'id' => 'employeeSecondName',
                'class' => 'form-control',
                'placeholder' => _('Last Name'),
            ),
        ));
        $this->add(array(
            'name' => 'designation',
            'type' => 'Zend\Form\Element\Select',
            'attributes' => array(
                'id' => 'designation',
                'class' => 'form-control selectpicker',
                'data-live-search' => true,
                'placeholder' => _('Select Designation'),
            ),
            'options' => array(
                'empty_option' => 'Select Designation',
                'value_options' => $designation,
            ),
        ));
        $this->add(array(
            'name' => 'division',
            'type' => 'Zend\Form\Element\Select',
            'attributes' => array(
                'id' => 'division',
                'class' => 'form-control selectpicker',
                'data-live-search' => true,
                'placeholder' => _('Select Division'),
            ),
            'options' => array(
                'empty_option' => 'Select Division',
                'value_options' => $division,
            ),
        ));
        $this->add(array(
            'name' => 'userID',
            'type' => 'Zend\Form\Element\Select',
            'attributes' => array(
                'id' => 'userID',
                'class' => 'form-control selectpicker',
                'data-live-search' => true,
                'placeholder' => _('Select User'),
            ),
            'options' => array(
                'empty_option' => 'Select User',
                'value_options' => $users,
            ),
        ));
        $this->add(array(
            'name' => 'employeeEmail',
            'type' => 'text',
            'attributes' => array(
                'id' => 'employeeEmail',
                'class' => 'form-control',
                'placeholder' => _('Email Address'),
            ),
        ));
        $this->add(array(
            'name' => 'telephoneNumber',
            'type' => 'text',
            'attributes' => array(
                'id' => 'telephoneNumber',
                'class' => 'form-control',
                'placeholder' => _('Telephone Number'),
            ),
        ));
        $this->add(array(
            'name' => 'empHourlyCost',
            'type' => 'text',
            'attributes' => array(
                'id' => 'empHourlyCost',
                'class' => 'form-control',
                'placeholder' => _('Hourly Cost'),
            )
        ));
    }

}
