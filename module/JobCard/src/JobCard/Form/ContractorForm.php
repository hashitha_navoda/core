<?php

namespace JobCard\Form;

use Zend\Form\Form;

class ContractorForm extends Form {

    function __construct($data) {
        $designation = $data['designation'];
        $division = $data['division'];
        parent::__construct('contractor');
        $this->setAttribute('method', 'post');
        $this->setAttribute("class", "");
        $this->setAttribute('role', "form");

        $this->add(array(
            'name' => 'contractorID',
            'type' => 'Hidden',
            'attribute' => array(
                'id' => 'contractorID'
            ),
        ));
        $this->add(array(
            'name' => 'contractorFirstName',
            'type' => 'text',
            'attributes' => array(
                'id' => 'contractorFirstName',
                'class' => 'form-control',
                'placeholder' => _('First Name'),
            ),
        ));
        $this->add(array(
            'name' => 'contractorSecondName',
            'type' => 'text',
            'attributes' => array(
                'id' => 'contractorSecondName',
                'class' => 'form-control',
                'placeholder' => _('Second Name'),
            ),
        ));
        $this->add(array(
            'name' => 'designationID',
            'type' => 'Zend\Form\Element\Select',
            'attributes' => array(
                'id' => 'designationID',
                'class' => 'form-control selectpicker',
                'data-live-search' => true,
                'placeholder' => _('Select Designation'),
            ),
            'options' => array(
                'empty_option' => 'Select Designation',
                'value_options' => $designation,
            ),
        ));
        $this->add(array(
            'name' => 'divisionID',
            'type' => 'Zend\Form\Element\Select',
            'attributes' => array(
                'id' => 'divisionID',
                'class' => 'form-control selectpicker',
                'data-live-search' => true,
                'placeholder' => _('Select Division'),
            ),
            'options' => array(
                'empty_option' => 'Select Division',
                'value_options' => $division,
            ),
        ));
        $this->add(array(
            'name' => 'telNumber',
            'type' => 'text',
            'attributes' => array(
                'id' => 'telNumber',
                'class' => 'form-control',
                'placeholder' => _('Telephone Number'),
            ),
        ));
        $this->add(array(
            'name' => 'contractorFormButton',
            'attributes' => array(
                'type' => 'submit',
                'value' => _('Add'),
                'id' => 'contractorSave',
                'class' => 'btn btn-primary'
            ),
        ));
    }

}
