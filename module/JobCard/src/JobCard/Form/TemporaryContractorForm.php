<?php

namespace JobCard\Form;

use Zend\Form\Form;

class TemporaryContractorForm extends Form {

    function __construct($data) {
        $designation = $data['designation'];
        $division = $data['division'];

        parent::__construct('temporaryContractor');
        $this->setAttribute('method', 'post');
        $this->setAttribute("class", "");
        $this->setAttribute('role', "form");

        $this->add(array(
            'name' => 'temporaryContractorID',
            'type' => 'Hidden',
            'attribute' => array(
                'id' => 'temporaryContractorID'
            ),
        ));
        $this->add(array(
            'name' => 'temporaryContractorFirstName',
            'type' => 'text',
            'attributes' => array(
                'id' => 'temporaryContractorFirstName',
                'class' => 'form-control',
                'placeholder' => _('First Name'),
            ),
        ));
        $this->add(array(
            'name' => 'temporaryContractorSecondName',
            'type' => 'text',
            'attributes' => array(
                'id' => 'temporaryContractorSecondName',
                'class' => 'form-control',
                'placeholder' => _('Second Name'),
            ),
        ));
        $this->add(array(
            'name' => 'designationID',
            'type' => 'Zend\Form\Element\Select',
            'attributes' => array(
                'id' => 'designationID',
                'class' => 'form-control selectpicker',
                'data-live-search' => true,
                'placeholder' => _('Select Designation'),
            ),
            'options' => array(
                'empty_option' => 'Select Designation',
                'value_options' => $designation,
            ),
        ));
        $this->add(array(
            'name' => 'divisionID',
            'type' => 'Zend\Form\Element\Select',
            'attributes' => array(
                'id' => 'divisionID',
                'class' => 'form-control selectpicker',
                'data-live-search' => true,
                'placeholder' => _('Select Division'),
            ),
            'options' => array(
                'empty_option' => 'Select Division',
                'value_options' => $division,
            ),
        ));
        $this->add(array(
            'name' => 'temporaryContractorTP',
            'type' => 'text',
            'attributes' => array(
                'id' => 'temporaryContractorTP',
                'class' => 'form-control',
                'placeholder' => _('Telephone Number'),
            ),
        ));
        $this->add(array(
            'name' => 'temporaryContractorFormButton',
            'attributes' => array(
                'type' => 'submit',
                'value' => _('Add'),
                'id' => 'temporaryContractorSave',
                'class' => 'btn btn-primary'
            ),
        ));
    }

}
