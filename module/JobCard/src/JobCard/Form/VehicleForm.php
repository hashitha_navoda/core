<?php

namespace JobCard\Form;

use Zend\Form\Form;

class VehicleForm extends Form {

    public function __construct() {
        parent::__construct('vehicle');
        $this->setAttribute('method', 'post');
        $this->setAttribute('class', "");
        $this->setAttribute('role', "form");

        $this->add(array(
            'name' => 'vehicleId',
            'type' => 'Hidden',
        ));

        $this->add(array(
            'name' => 'vehicleMake',
            'type' => 'Text',
            'attributes' => array(
                'id' => 'vehicleMake',
                'class' => 'form-control',
                'placeholder' => _('Eg: Toyota')
            ),
        ));

        $this->add(array(
            'name' => 'vehicleModel',
            'type' => 'Text',
            'attributes' => array(
                'id' => 'vehicleModel',
                'class' => 'form-control',
                'placeholder' => _('Eg: Corolla')
            ),
        ));

        $this->add(array(
            'name' => 'vehicleRegistrationNumber',
            'type' => 'Text',
            'attributes' => array(
                'id' => 'vehicleRegistrationNumber',
                'class' => 'form-control',
                'placeholder' => _('Eg: HE - 6788')
            ),
        ));

        $this->add(array(
            'name' => 'vehicleCost',
            'type' => 'Text',
            'attributes' => array(
                'id' => 'vehicleCost',
                'class' => 'form-control',
                'placeholder' => _('Eg: 250.00')
            ),
        ));
    }

}
