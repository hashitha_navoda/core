<?php

/**
 * @author Sandun <sandun@thinkcube.com>
 * Inquiry type form
 */

namespace JobCard\Form;

use Zend\Form\Form;

class InquiryTypeForm extends Form
{

    public function __construct()
    {
        parent::__construct('inquiryType');
        $this->setAttribute('method', 'post');
        $this->setAttribute('class', "form-horizontal");
        $this->setAttribute('role', "form");

        $this->add(array(
            'name' => 'inquiryTypeID',
            'type' => 'Hidden',
        ));

        $this->add(array(
            'name' => 'inquiryTypeName',
            'type' => 'Text',
            'options' => array(
            ),
            'attributes' => array(
                'id' => 'inquiryTypeName',
                'autocomplete' => false,
                'class' => 'form-control',
                'placeholder' => _('Eg: Repair')
            ),
        ));

        $this->add(array(
            'name' => 'inquiryTypeCode',
            'type' => 'Text',
            'options' => array(
            ),
            'attributes' => array(
                'id' => 'inquiryTypeCode',
                'class' => 'form-control',
                'autocomplete' => false,
                'placeholder' => _('Eg: R_123')
            ),
        ));
    }

}
