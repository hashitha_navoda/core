<?php

namespace JobCard\Form;

use Zend\Form\Form;

class DesignationForm extends Form
{

    function __construct()
    {

        parent::__construct('designation');
        $this->setAttribute('method', 'post');
        $this->setAttribute("class", "form-horizontal");
        $this->setAttribute('role', "form");

        $this->add(array(
            'name' => 'designationID',
            'type' => 'Hidden',
            'attribute' => array(
                'id' => 'designationID'
            ),
        ));
        $this->add(array(
            'name' => 'designationName',
            'type' => 'text',
            'attributes' => array(
                'id' => 'designationName',
                'class' => 'form-control',
                'placeholder' => _('Designation'),
            ),
        ));
    }

}
