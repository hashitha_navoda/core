<?php

namespace JobCard\Form;

use Zend\Form\Form;

class ActivityForm extends Form
{

    function __construct($userdateFormat)
    {

        parent::__construct('activityForm');
        $this->setAttribute('method', 'post');
        $this->setAttribute("class", "form-horizontal");
        $this->setAttribute('role', "form");

        $this->add(array(
            'name' => 'locationID',
            'type' => 'text',
            'attributes' => array(
                'id' => 'locationID',
                'class' => 'form-control hidden'
            ),
        ));

        $this->add(array(
            'name' => 'activityID',
            'type' => 'text',
            'attributes' => array(
                'id' => 'activityID',
                'class' => 'form-control'
            ),
        ));
        $this->add(array(
            'name' => 'activityName',
            'type' => 'text',
            'attributes' => array(
                'id' => 'activityName',
                'class' => 'form-control',
                'placeholder' => _('Activity Name'),
            ),
        ));
        $this->add(array(
            'name' => 'projectReferenceID',
            'type' => 'text',
            'attributes' => array(
                'id' => 'projectReferenceID',
                'class' => 'form-control',
                'placeholder' => _('Project Reference'),
            ),
        ));
        $this->add(array(
            'name' => 'jobReferenceID',
            'type' => 'text',
            'attributes' => array(
                'id' => 'jobReferenceID',
                'class' => 'form-control',
                'placeholder' => _('Job Reference'),
            ),
        ));
        $this->add(array(
            'name' => 'activityTypeID',
            'type' => 'text',
            'attributes' => array(
                'id' => 'activityTypeID',
                'class' => 'form-control',
                'placeholder' => _('Activity Type'),
            ),
        ));
        $this->add(array(
            'name' => 'contractorID',
            'type' => 'text',
            'attributes' => array(
                'id' => 'contractorID',
                'class' => 'form-control',
                'placeholder' => _('Contractors'),
            ),
        ));
        $this->add(array(
            'name' => 'temporaryContractorID',
            'type' => 'text',
            'attributes' => array(
                'id' => 'temporaryContractorID',
                'class' => 'form-control',
                'placeholder' => _('Temporary Contractors'),
            ),
        ));
        $this->add(array(
            'name' => 'activityDescription',
            'type' => 'textarea',
            'attributes' => array(
                'id' => 'activityDescription',
                'class' => 'form-control',
                'placeholder' => _('Activity Description'),
            ),
        ));
        $this->add(array(
            'name' => 'activityOwnerID',
            'type' => 'text',
            'attributes' => array(
                'id' => 'activityOwnerID',
                'class' => 'form-control',
                'placeholder' => _('Activity Owners'),
            ),
        ));
        $this->add(array(
            'name' => 'activitySupervisorID',
            'type' => 'text',
            'attributes' => array(
                'id' => 'activitySupervisorID',
                'class' => 'form-control',
                'placeholder' => _('Activity Supervisors'),
            ),
        ));
        $this->add(array(
            'name' => 'rawMaterialID',
            'type' => 'text',
            'attributes' => array(
                'id' => 'rawMaterialID',
                'class' => 'form-control',
                'placeholder' => _('Required Raw Materials'),
            ),
        ));
        $this->add(array(
            'name' => 'fixedAssetsID',
            'type' => 'text',
            'attributes' => array(
                'id' => 'fixedAssetsID',
                'class' => 'form-control',
                'placeholder' => _('Required Fixed Assets'),
            ),
        ));
        $this->add(array(
            'name' => 'activityEstimateTime',
            'type' => 'text',
            'attributes' => array(
                'id' => 'estimateTime',
                'class' => 'form-control',
                'autocomplete' => false,
                'placeholder' => _('Eg: ' . $userdateFormat),
                'data-date-format' => $userdateFormat
            ),
        ));
        $this->add(array(
            'name' => 'startTime',
            'type' => 'text',
            'attributes' => array(
                'id' => 'startTime',
                'class' => 'form-control',
                'readonly' => true,
                'style' => "background-color: white",
                'autocomplete' => false,
                'placeholder' => _('Eg: ' . $userdateFormat),
                'data-date-format' => $userdateFormat
            ),
        ));
        $this->add(array(
            'name' => 'endTime',
            'type' => 'text',
            'attributes' => array(
                'id' => 'endTime',
                'class' => 'form-control',
                'readonly' => true,
                'style' => "background-color: white",
                'autocomplete' => false,
                'placeholder' => _('Eg: ' . $userdateFormat),
                'data-date-format' => $userdateFormat
            ),
        ));
        $this->add(array(
            'name' => 'activityEstimateCost',
            'type' => 'text',
            'attributes' => array(
                'id' => 'activityEstimateCost',
                'class' => 'form-control',
                'placeholder' => _('Eg : $100'),
            ),
        ));
        $this->add(array(
            'name' => 'temporaryProductID',
            'type' => 'text',
            'attributes' => array(
                'id' => 'temporaryProductID',
                'class' => 'form-control',
                'placeholder' => _('Required Temporary Product'),
            ),
        ));

        $this->add(array(
            'name' => 'costTypeID',
            'type' => 'text',
            'attributes' => array(
                'id' => 'costTypeID',
                'class' => 'form-control',
                'placeholder' => _('Required Cost Type'),
            ),
        ));
        $this->add(array(
            'name' => 'activityRepeatComment',
            'type' => 'textarea',
            'attributes' => array(
                'id' => 'activityRepeatComment',
                'class' => 'form-control',
                'placeholder' => _('Activity Repeat Comment'),
            ),
        ));
        $this->add(array(
            'name' => 'activityRepeatEnabled',
            'type' => 'checkbox',
            'attributes' => array(
                'id' => 'activityRepeatEnabled',
            ),
        ));
    }

}
