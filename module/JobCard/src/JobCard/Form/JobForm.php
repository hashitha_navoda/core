<?php

namespace JobCard\Form;

use Zend\Form\Form;

class JobForm extends Form
{

    function __construct($dateFormat, $reference)
    {

        parent::__construct('job');
        $this->setAttribute('method', 'post');
        $this->setAttribute("class", "form-horizontal");
        $this->setAttribute('role', "form");
        $this->add(array(
            'name' => 'jobID',
            'type' => 'Hidden',
            'attribute' => array(
                'id' => 'jobID'
            ),
        ));
        $this->add(array(
            'name' => 'jobName',
            'type' => 'text',
            'attributes' => array(
                'id' => 'jobName',
                'class' => 'form-control',
                'placeholder' => _('ex: Job1')
            ),
        ));
        $this->add(array(
            'name' => 'customerName',
            'type' => 'text',
            'attributes' => array(
                'id' => 'customerName',
                'class' => 'form-control',
                'placeholder' => _('ex: Mr Clarke')
            ),
        ));
        $this->add(array(
            'name' => 'jobType',
            'type' => 'text',
            'attributes' => array(
                'id' => 'jobType',
                'class' => 'form-control',
                'placeholder' => _('ex: Repair')
            ),
        ));
        $this->add(array(
            'name' => 'jobOwner',
            'type' => 'text',
            'attributes' => array(
                'id' => 'jobOwner',
                'class' => 'form-control',
                'autocomplete' => false,
                'placeholder' => _('ex: Mr John'),
            ),
        ));
        $this->add(array(
            'name' => 'jobSupervisor',
            'type' => 'text',
            'attributes' => array(
                'id' => 'jobSupervisor',
                'class' => 'form-control',
                'placeholder' => _('ex: Mr John'),
            ),
        ));
        $this->add(array(
            'name' => 'jobDescription',
            'type' => 'textarea',
            'attributes' => array(
                'id' => 'jobDescription',
                'class' => 'form-control',
                'placeholder' => _('Job Description'),
            ),
        ));
        $this->add(array(
            'name' => 'jobStartingDate',
            'type' => 'text',
            'attributes' => array(
                'id' => 'jobStartingDate',
                'class' => 'form-control',
                'readonly' => true,
                'style' => "background-color: white",
                'placeholder' => _('Job Starting Date'),
                'data-date-format' => $dateFormat
            ),
        ));
        $this->add(array(
            'name' => 'jobEstimetedTime',
            'type' => 'text',
            'attributes' => array(
                'id' => 'jobEstimetedTime',
                'class' => 'form-control',
                'placeholder' => _('Job Estimated Time'),
            ),
        ));
        $this->add(array(
            'name' => 'jobEndingDate',
            'type' => 'text',
            'attributes' => array(
                'id' => 'jobEndingDate',
                'class' => 'form-control',
                'readonly' => true,
                'style' => "background-color: white",
                'placeholder' => _('Job Ending Date'),
                'data-date-format' => $dateFormat
            ),
        ));
        $this->add(array(
            'name' => 'estimatedCost',
            'type' => 'text',
            'attributes' => array(
                'id' => 'estimatedCost',
                'class' => 'form-control',
                'placeholder' => _('Estimated Cost'),
            ),
        ));
        $this->add(array(
            'name' => 'jobStation',
            'type' => 'text',
            'attributes' => array(
                'id' => 'jobStation',
                'class' => 'form-control',
                'placeholder' => _('Job Station'),
            ),
        ));
        $this->add(array(
            'name' => 'projectReference',
            'type' => 'text',
            'attributes' => array(
                'id' => 'projectReference',
                'class' => 'form-control',
                'placeholder' => _('project Reference'),
            ),
        ));
        $this->add(array(
            'name' => 'jobReference',
            'type' => 'text',
            'attributes' => array(
                'id' => 'jobReference',
                'class' => 'form-control',
                'placeholder' => _('Job Reference'),
                'disabled' => true,
                'value' => $reference,
            ),
        ));
        $this->add(array(
            'name' => 'customerAddress',
            'type' => 'text',
            'attributes' => array(
                'id' => 'customerAddress',
                'class' => 'form-control',
            ),
        ));
        $this->add(array(
            'name' => 'jobRepeatComment',
            'type' => 'textarea',
            'attributes' => array(
                'id' => 'jobRepeatComment',
                'class' => 'form-control',
                'placeholder' => _('Job Repeat Comment'),
            ),
        ));
        $this->add(array(
            'name' => 'jobRepeatEnabled',
            'type' => 'checkbox',
            'attributes' => array(
                'id' => 'jobRepeatEnabled',
            ),
        ));
    }

}
