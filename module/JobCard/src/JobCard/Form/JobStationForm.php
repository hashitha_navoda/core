<?php

namespace JobCard\Form;

use Zend\Form\Form;

class JobStationForm extends Form {

    function __construct() {

        parent::__construct('jobStation');
        $this->setAttribute('method', 'post');
        $this->setAttribute("class", "");
        $this->setAttribute('role', "form");

        $this->add(array(
            'name' => 'jobStationID',
            'type' => 'Hidden',
            'attribute' => array(
                'id' => 'jobStationID'
            ),
        ));
        $this->add(array(
            'name' => 'jobStationName',
            'type' => 'text',
            'attributes' => array(
                'id' => 'jobStationName',
                'class' => 'form-control',
                'placeholder' => _('Job Station Name'),
            ),
        ));
        $this->add(array(
            'name' => 'maxValue',
            'type' => 'text',
            'attributes' => array(
                'id' => 'maxValue',
                'class' => 'form-control',
                'placeholder' => _('Max Value'),
            )
        ));
        $this->add(array(
            'name' => 'minValue',
            'type' => 'text',
            'attributes' => array(
                'id' => 'minValue',
                'class' => 'form-control',
                'placeholder' => _('Min Value'),
            )
        ));
    }

}
