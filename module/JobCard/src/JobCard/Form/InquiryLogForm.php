<?php

namespace JobCard\Form;

use Zend\Form\Form;

class InquiryLogForm extends Form
{

    function __construct($dateFormat, $userName, $referenceNumber, $currentdate, $userID, $isSuperAdmin, $documentTypeName)
    {
        parent::__construct('job');
        $this->setAttribute('method', 'post');
        $this->setAttribute("class", "form-horizontal");
        $this->setAttribute('role', "form");
        $this->add(array(
            'name' => 'inquryLogID',
            'type' => 'Hidden',
            'attribute' => array(
                'id' => 'inquryLogID'
            ),
        ));
        $this->add(array(
            'name' => 'inquiryLogReference',
            'type' => 'text',
            'attributes' => array(
                'id' => 'inquiryLogReference',
                'class' => 'form-control',
                'value' => $referenceNumber,
                'placeholder' => _('ex: Inquiry Log Reference'),
                'readonly' => true,
                'style' => "background-color: white",
            ),
        ));
        $this->add(array(
            'name' => 'inquiryLogContactNo1',
            'type' => 'text',
            'attributes' => array(
                'id' => 'inquiryLogContactNo1',
                'class' => 'form-control',
                'placeholder' => _('TP_1')
            ),
        ));
        $this->add(array(
            'name' => 'inquiryLogContactNo2',
            'type' => 'text',
            'attributes' => array(
                'id' => 'inquiryLogContactNo2',
                'class' => 'form-control',
                'placeholder' => _('TP_2')
            ),
        ));
        $this->add(array(
            'name' => 'inquiryLogAddress',
            'type' => 'text',
            'attributes' => array(
                'id' => 'inquiryLogAddress',
                'class' => 'form-control',
                'placeholder' => _('Address'),
            ),
        ));
        $this->add(array(
            'name' => 'inquiryTypeID',
            'type' => 'text',
            'attributes' => array(
                'id' => 'inquiryTypeID',
                'class' => 'form-control',
                'placeholder' => _('Inquiry Type'),
            ),
        ));
        $attributes = array(
            'id' => 'inquiryLogDateAndTime',
            'class' => 'form-control',
            'readonly' => true,
            'placeholder' => _('Date And Time'),
            'data-date-format' => $dateFormat,
            'value' => $currentdate,
        );
        if (!$isSuperAdmin) {
            $attributes['disabled'] = 'disabled';
        }
        $this->add(array(
            'name' => 'inquiryLogDateAndTime',
            'type' => 'text',
            'attributes' => $attributes,
        ));
        $this->add(array(
            'name' => 'inquirylogUser',
            'type' => 'text',
            'attributes' => array(
                'id' => 'inquirylogUser',
                'class' => 'form-control',
                'readonly' => true,
                'style' => "background-color: white",
                'value' => $userName,
                'data-userid' => $userID,
            ),
        ));
        $this->add(array(
            'name' => 'inquiryLogStatusID',
            'type' => 'text',
            'attributes' => array(
                'id' => 'inquiryLogStatusID',
                'class' => 'form-control',
                'placeholder' => _('Inquiry Log Status'),
            ),
        ));
        $this->add(array(
            'name' => 'inquiryLogDescription',
            'type' => 'textarea',
            'attributes' => array(
                'id' => 'inquiryLogDescription',
                'class' => 'form-control',
                'placeholder' => _('Inquiry Log Description'),
            ),
        ));
        $this->add(array(
            'name' => 'inquiryLogDutyManager',
            'type' => 'text',
            'attributes' => array(
                'id' => 'inquiryLogDutyManager',
                'class' => 'form-control',
                'placeholder' => _('Duty Manager'),
            ),
        ));
        $this->add(array(
            'name' => 'proRef',
            'type' => 'text',
            'attributes' => array(
                'id' => 'proRef',
                'class' => 'form-control',
                'placeholder' => _('Project Reference'),
            ),
        ));
        $this->add(array(
            'name' => 'jobRef',
            'type' => 'text',
            'attributes' => array(
                'id' => 'jobRef',
                'class' => 'form-control',
                'placeholder' => _('Job Reference'),
            ),
        ));
        $this->add(array(
            'name' => 'actRef',
            'type' => 'text',
            'attributes' => array(
                'id' => 'actRef',
                'class' => 'form-control',
                'placeholder' => _('Activity Reference'),
            ),
        ));
        $this->add(array(
            'name' => 'inquiryLogCustomerID',
            'type' => 'text',
            'attributes' => array(
                'id' => 'inquiryLogCustomerID',
                'class' => 'form-control',
                'placeholder' => _('Customer Name'),
            ),
        ));
        $this->add(array(
            'name' => 'inquiryLogCustomerSearchID',
            'type' => 'text',
            'attributes' => array(
                'id' => 'inquiryLogCustomerSearchID',
                'class' => 'form-control',
            ),
        ));
        $this->add(array(
            'name' => 'inquiryComplainType',
            'type' => 'Zend\Form\Element\Select',
            'attributes' => array(
                'id' => 'inquiryComplainType',
                'class' => 'form-control selectpicker'
            ),
            'options' => array(
                'value_options' => array(
                    'Complain' => 'Complain',
                    'Inquiry' => 'Inquiry',
                ),
            )
        ));
        $this->add(array(
            'name' => 'documentTypeName',
            'type' => 'Zend\Form\Element\Select',
            'attributes' => array(
                'id' => 'documentTypeName',
                'class' => 'form-control selectpicker'
            ),
            'options' => array(
                'value_options' => $documentTypeName,
            ),
        ));
        $this->add(array(
            'name' => 'documentTypeID',
            'type' => 'text',
            'attributes' => array(
                'id' => 'documentTypeID',
                'class' => 'form-control',
            ),
        ));
        $this->add(array(
            'name' => 'qotDocumentTypeID',
            'type' => 'text',
            'attributes' => array(
                'id' => 'qotDocumentTypeID',
                'class' => 'form-control',
            ),
        ));
        $this->add(array(
            'name' => 'sODocumentTypeID',
            'type' => 'text',
            'attributes' => array(
                'id' => 'sODocumentTypeID',
                'class' => 'form-control',
            ),
        ));
        $this->add(array(
            'name' => 'invDocumentTypeID',
            'type' => 'text',
            'attributes' => array(
                'id' => 'invDocumentTypeID',
                'class' => 'form-control',
            ),
        ));
        $this->add(array(
            'name' => 'proDocumentTypeID',
            'type' => 'text',
            'attributes' => array(
                'id' => 'proDocumentTypeID',
                'class' => 'form-control',
            ),
        ));
        $this->add(array(
            'name' => 'jobDocumentTypeID',
            'type' => 'text',
            'attributes' => array(
                'id' => 'jobDocumentTypeID',
                'class' => 'form-control',
            ),
        ));
        $this->add(array(
            'name' => 'actDocumentTypeID',
            'type' => 'text',
            'attributes' => array(
                'id' => 'actDocumentTypeID',
                'class' => 'form-control',
            ),
        ));
    }

}
