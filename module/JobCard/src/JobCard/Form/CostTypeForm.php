<?php

/**
 * @author Ashan Madushka   <ashan@thinkcube.com>
 */

namespace JobCard\Form;

use Zend\Form\Form;

class CostTypeForm extends Form {

    public function __construct() {
        parent::__construct('costType');
        $this->setAttribute('method', 'post');
        $this->setAttribute('class', "");
        $this->setAttribute('role', "form");

        $this->add(array(
            'name' => 'costTypeID',
            'type' => 'Hidden',
        ));

        $this->add(array(
            'name' => 'costTypeName',
            'type' => 'Text',
            'options' => array(
            ),
            'attributes' => array(
                'id' => 'costTypeName',
                'autocomplete' => false,
                'class' => 'form-control',
                'placeholder' => _('Eg: Repair')
            ),
        ));

        $this->add(array(
            'name' => 'costTypeDescription',
            'type' => 'Text',
            'options' => array(
            ),
            'attributes' => array(
                'id' => 'costTypeDescription',
                'class' => 'form-control',
                'autocomplete' => false,
                'placeholder' => _('Eg: R_123')
            ),
        ));

        $this->add(array(
            'name' => 'costItem',
            'type' => 'Text',
            'options' => array(
            ),
            'attributes' => array(
                'id' => 'costItem',
                'autocomplete' => false,
                'class' => 'form-control',
                'placeholder' => _('Eg: Transport')
            ),
        ));

        $this->add(array(
            'name' => 'maximumValue',
            'type' => 'Text',
            'options' => array(
            ),
            'attributes' => array(
                'id' => 'maximumValue',
                'autocomplete' => false,
                'class' => 'form-control',
                'placeholder' => _('Eg: 1000')
            ),
        ));
        $this->add(array(
            'name' => 'minimumValue',
            'type' => 'Text',
            'options' => array(
            ),
            'attributes' => array(
                'id' => 'minimumValue',
                'autocomplete' => false,
                'class' => 'form-control',
                'placeholder' => _('Eg: 100')
            ),
        ));
    }

}
