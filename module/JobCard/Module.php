<?php

namespace JobCard;

use JobCard\Model\JobType;
use JobCard\Model\JobTypeTable;
use JobCard\Model\ProjectType;
use JobCard\Model\ProjectTypeTable;
use JobCard\Model\CostType;
use JobCard\Model\CostTypeTable;
use JobCard\Model\InquiryType;
use JobCard\Model\InquiryTypeTable;
use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use JobCard\Model\Designation;
use JobCard\Model\DesignationTable;
use JobCard\Model\Division;
use JobCard\Model\DivisionTable;
use JobCard\Model\Employee;
use JobCard\Model\EmployeeTable;
use JobCard\Model\ActivityType;
use JobCard\Model\ActivityTypeTable;
use JobCard\Model\InquiryStatus;
use JobCard\Model\InquiryStatusTable;
use JobCard\Model\InquiryStatusType;
use JobCard\Model\InquiryStatusTypeTable;
use JobCard\Model\Contractor;
use JobCard\Model\ContractorTable;
use JobCard\Model\Job;
use JobCard\Model\JobTable;
use JobCard\Model\JobSupervisor;
use JobCard\Model\JobSupervisorTable;
use JobCard\Model\JobOwner;
use JobCard\Model\JobOwnerTable;
use JobCard\Model\Project;
use JobCard\Model\ProjectTable;
use JobCard\Model\ProjectOwners;
use JobCard\Model\ProjectOwnersTable;
use JobCard\Model\ProjectSupervisors;
use JobCard\Model\ProjectSupervisorsTable;
use JobCard\Model\ProjectDivisions;
use JobCard\Model\ProjectDivisionsTable;
use JobCard\Model\TemporaryContractor;
use JobCard\Model\TemporaryContractorTable;
use JobCard\Model\JobStation;
use JobCard\Model\JobStationTable;
use JobCard\Model\TemporaryProduct;
use JobCard\Model\TemporaryProductTable;
use JobCard\Model\Activity;
use JobCard\Model\ActivityTable;
use JobCard\Model\ActivityContractor;
use JobCard\Model\ActivityContractorTable;
use JobCard\Model\ActivityTemporaryContractor;
use JobCard\Model\ActivityTemporaryContractorTable;
use JobCard\Model\ActivityOwner;
use JobCard\Model\ActivityOwnerTable;
use JobCard\Model\ActivitySupervisor;
use JobCard\Model\ActivitySupervisorTable;
use JobCard\Model\ActivityRawMaterial;
use JobCard\Model\ActivityRawMaterialTable;
use JobCard\Model\ActivityFixedAsset;
use JobCard\Model\ActivityFixedAssetTable;
use JobCard\Model\ActivityBatch;
use JobCard\Model\ActivityBatchTable;
use JobCard\Model\ActivitySerial;
use JobCard\Model\ActivitySerialTable;
use JobCard\Model\InquiryLog;
use JobCard\Model\InquiryLogDutyManager;
use JobCard\Model\InquiryComplainRelation;
use JobCard\Model\InquiryLogTable;
use JobCard\Model\InquiryComplainRelationTable;
use JobCard\Model\InquiryLogDutyManagerTable;
use JobCard\Model\ActivityRawMaterialSubProduct;
use JobCard\Model\ActivityRawMaterialSubProductTable;
use JobCard\Model\ActivityCostType;
use JobCard\Model\ActivityCostTypeTable;
use JobCard\Model\ActivityFixedAssetSubProduct;
use JobCard\Model\ActivityFixedAssetSubProductTable;
use JobCard\Model\InquiryLogEmailSchedule;
use JobCard\Model\InquiryLogEmailScheduleTable;
use JobCard\Model\InquirySetup;
use JobCard\Model\InquirySetupTable;
use JobCard\Model\ActivityTemporaryProduct;
use JobCard\Model\ActivityTemporaryProductTable;

class Module
{

    public function onBootstrap(MvcEvent $e)
    {
        $eventManager = $e->getApplication()->getEventManager();
        $moduleRouteListener = new ModuleRouteListener();
        $moduleRouteListener->attach($eventManager);

        $config = $this->getConfig();
    }

    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\ClassMapAutoloader' => array(
                __DIR__ . '/autoload_classmap.php',
            ),
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }

    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getServiceConfig()
    {
        return array(
            'invokables' => array(
//                'PaymentsAPI' => 'Invoice\Controller\CustomerPaymentsAPIController',
            ),
            'aliases' => array(
                'ActivityTable' => 'JobCard\Model\ActivityTable',
            ),
            'factories' => array(
                'JobCard\Model\JobTypeTable' => function($sm) {
            $tableGateway = $sm->get('JobTypeTableGateway');
            $table = new JobTypeTable($tableGateway);
            return $table;
        },
                'JobTypeTableGateway' => function ($sm) {
            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new JobType());
            return new TableGateway('jobType', $dbAdapter, null, $resultSetPrototype);
        },
                'JobCard\Model\DesignationTable' => function($sm) {
            $tableGateway = $sm->get('DesignationTableGateway');
            $table = new DesignationTable($tableGateway);
            return $table;
        },
                'DesignationTableGateway' => function ($sm) {
            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new Designation());
            return new TableGateway('designation', $dbAdapter, null, $resultSetPrototype);
        },
                'JobCard\Model\DivisionTable' => function($sm) {
            $tableGateway = $sm->get('DivisionTableGateway');
            $table = new DivisionTable($tableGateway);
            return $table;
        },
                'DivisionTableGateway' => function ($sm) {
            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new Division());
            return new TableGateway('division', $dbAdapter, null, $resultSetPrototype);
        },
                'JobCard\Model\EmployeeTable' => function($sm) {
            $tableGateway = $sm->get('EmployeeTableGateway');
            $table = new EmployeeTable($tableGateway);
            return $table;
        },
                'EmployeeTableGateway' => function ($sm) {
            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new Employee());
            return new TableGateway('employee', $dbAdapter, null, $resultSetPrototype);
        },
                'JobCard\Model\ActivityTypeTable' => function($sm) {
            $tableGateway = $sm->get('ActivityTypeTableGateway');
            $table = new ActivityTypeTable($tableGateway);
            return $table;
        },
                'ActivityTypeTableGateway' => function ($sm) {
            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new ActivityType());
            return new TableGateway('activityType', $dbAdapter, null, $resultSetPrototype);
        },
                'JobCard\Model\ProjectTypeTable' => function($sm) {
            $tableGateway = $sm->get('ProjectTypeTableGateway');
            $table = new ProjectTypeTable($tableGateway);
            return $table;
        },
                'ProjectTypeTableGateway' => function ($sm) {
            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new ProjectType());
            return new TableGateway('projectType', $dbAdapter, null, $resultSetPrototype);
        },
        //         'JobCard\Model\CostTypeTable' => function($sm) {
        //     $tableGateway = $sm->get('CostTypeTableGateway');
        //     $table = new CostTypeTable($tableGateway);
        //     return $table;
        // },
        //         'CostTypeTableGateway' => function ($sm) {
        //     $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
        //     $resultSetPrototype = new ResultSet();
        //     $resultSetPrototype->setArrayObjectPrototype(new CostType());
        //     return new TableGateway('costType', $dbAdapter, null, $resultSetPrototype);
        // },
                'JobCard\Model\InquiryTypeTable' => function($sm) {
            $tableGateway = $sm->get('InquiryTypeTableGateway');
            $table = new InquiryTypeTable($tableGateway);
            return $table;
        },
                'InquiryTypeTableGateway' => function ($sm) {
            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new InquiryType());
            return new TableGateway('inquiryType', $dbAdapter, null, $resultSetPrototype);
        },
                'JobCard\Model\InquiryStatusTable' => function($sm) {
            $tableGateway = $sm->get('InquiryStatusTableGateway');
            $table = new InquiryStatusTable($tableGateway);
            return $table;
        },
                'InquiryStatusTableGateway' => function ($sm) {
            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new InquiryStatus());
            return new TableGateway('inquiryStatus', $dbAdapter, null, $resultSetPrototype);
        },
                'JobCard\Model\InquiryStatusTypeTable' => function($sm) {
            $tableGateway = $sm->get('InquiryStatusTypeTableGateway');
            $table = new InquiryStatusTypeTable($tableGateway);
            return $table;
        },
                'InquiryStatusTypeTableGateway' => function ($sm) {
            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new InquiryStatusType());
            return new TableGateway('inquiryStatusType', $dbAdapter, null, $resultSetPrototype);
        },
                'JobCard\Model\ContractorTable' => function($sm) {
            $tableGateway = $sm->get('ContractorTableGateway');
            $table = new ContractorTable($tableGateway);
            return $table;
        },
                'ContractorTableGateway' => function ($sm) {
            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new Contractor());
            return new TableGateway('contractor', $dbAdapter, null, $resultSetPrototype);
        },
                'JobCard\Model\JobTable' => function($sm) {
            $tableGateway = $sm->get('JobTableGateway');
            $table = new JobTable($tableGateway);
            return $table;
        },
                'JobTableGateway' => function ($sm) {
            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new Job());
            return new TableGateway('job', $dbAdapter, null, $resultSetPrototype);
        },
                'JobCard\Model\JobSupervisorTable' => function($sm) {
            $tableGateway = $sm->get('JobSupervisorTableGateway');
            $table = new JobSupervisorTable($tableGateway);
            return $table;
        },
                'JobSupervisorTableGateway' => function ($sm) {
            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new JobSupervisor());
            return new TableGateway('jobSupervisor', $dbAdapter, null, $resultSetPrototype);
        },
                'JobCard\Model\JobOwnerTable' => function($sm) {
            $tableGateway = $sm->get('JobOwnerTableGateway');
            $table = new JobOwnerTable($tableGateway);
            return $table;
        },
                'JobOwnerTableGateway' => function ($sm) {
            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new JobOwner());
            return new TableGateway('jobOwner', $dbAdapter, null, $resultSetPrototype);
        },
                'JobCard\Model\ProjectTable' => function($sm) {
            $tableGateway = $sm->get('ProjectTableGateway');
            $table = new ProjectTable($tableGateway);
            return $table;
        },
                'ProjectTableGateway' => function ($sm) {
            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new Project());
            return new TableGateway('project', $dbAdapter, null, $resultSetPrototype);
        },
                'JobCard\Model\ProjectOwnersTable' => function($sm) {
            $tableGateway = $sm->get('ProjectOwnersTableGateway');
            $table = new ProjectOwnersTable($tableGateway);
            return $table;
        },
                'ProjectOwnersTableGateway' => function ($sm) {
            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new ProjectOwners());
            return new TableGateway('projectOwners', $dbAdapter, null, $resultSetPrototype);
        },
                'JobCard\Model\ProjectSupervisorsTable' => function($sm) {
            $tableGateway = $sm->get('ProjectSupervisorsTableGateway');
            $table = new ProjectSupervisorsTable($tableGateway);
            return $table;
        },
                'ProjectSupervisorsTableGateway' => function ($sm) {
            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new ProjectSupervisors());
            return new TableGateway('projectSupervisors', $dbAdapter, null, $resultSetPrototype);
        },
                'JobCard\Model\ProjectDivisionsTable' => function($sm) {
            $tableGateway = $sm->get('ProjectDivisionsTableGateway');
            $table = new ProjectDivisionsTable($tableGateway);
            return $table;
        },
                'ProjectDivisionsTableGateway' => function ($sm) {
            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new ProjectDivisions());
            return new TableGateway('projectDivision', $dbAdapter, null, $resultSetPrototype);
        },
                'JobCard\Model\TemporaryContractorTable' => function($sm) {
            $tableGateway = $sm->get('TemporaryContractorTableGateway');
            $table = new TemporaryContractorTable($tableGateway);
            return $table;
        },
                'TemporaryContractorTableGateway' => function ($sm) {
            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new TemporaryContractor());
            return new TableGateway('temporaryContractor', $dbAdapter, null, $resultSetPrototype);
        },
                'JobCard\Model\JobStationTable' => function($sm) {
            $tableGateway = $sm->get('JobStationTableGateway');
            $table = new JobStationTable($tableGateway);
            return $table;
        },
                'JobStationTableGateway' => function ($sm) {
            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new JobStation());
            return new TableGateway('jobStation', $dbAdapter, null, $resultSetPrototype);
        },
                'JobCard\Model\TemporaryProductTable' => function($sm) {
            $tableGateway = $sm->get('TemporaryProductTableGateway');
            $table = new TemporaryProductTable($tableGateway);
            return $table;
        },
                'TemporaryProductTableGateway' => function ($sm) {
            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new TemporaryProduct());
            return new TableGateway('temporaryProduct', $dbAdapter, null, $resultSetPrototype);
        },
                'JobCard\Model\ActivityTable' => function($sm) {
            $tableGateway = $sm->get('ActivityTableGateway');
            $table = new ActivityTable($tableGateway);
            return $table;
        },
                'ActivityTableGateway' => function ($sm) {
            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new Activity());
            return new TableGateway('activity', $dbAdapter, null, $resultSetPrototype);
        },
                'JobCard\Model\ActivityContractorTable' => function($sm) {
            $tableGateway = $sm->get('ActivityContractorTableGateway');
            $table = new ActivityContractorTable($tableGateway);
            return $table;
        },
                'ActivityContractorTableGateway' => function ($sm) {
            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new ActivityContractor());
            return new TableGateway('activityContractor', $dbAdapter, null, $resultSetPrototype);
        },
                'JobCard\Model\ActivityTemporaryContractorTable' => function($sm) {
            $tableGateway = $sm->get('ActivityTemporaryContractorTableGateway');
            $table = new ActivityTemporaryContractorTable($tableGateway);
            return $table;
        },
                'ActivityTemporaryContractorTableGateway' => function ($sm) {
            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new ActivityTemporaryContractor());
            return new TableGateway('activityTemporaryContractor', $dbAdapter, null, $resultSetPrototype);
        },
                'JobCard\Model\ActivityOwnerTable' => function($sm) {
            $tableGateway = $sm->get('ActivityOwnerTableGateway');
            $table = new ActivityOwnerTable($tableGateway);
            return $table;
        },
                'ActivityOwnerTableGateway' => function ($sm) {
            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new ActivityOwner());
            return new TableGateway('activityOwner', $dbAdapter, null, $resultSetPrototype);
        },
                'JobCard\Model\ActivitySupervisorTable' => function($sm) {
            $tableGateway = $sm->get('ActivitySupervisorTableGateway');
            $table = new ActivitySupervisorTable($tableGateway);
            return $table;
        },
                'ActivitySupervisorTableGateway' => function ($sm) {
            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new ActivitySupervisor());
            return new TableGateway('activitySupervisor', $dbAdapter, null, $resultSetPrototype);
        },
                'JobCard\Model\ActivityRawMaterialTable' => function($sm) {
            $tableGateway = $sm->get('ActivityRawMaterialTableGateway');
            $table = new ActivityRawMaterialTable($tableGateway);
            return $table;
        },
                'ActivityRawMaterialTableGateway' => function ($sm) {
            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new ActivityRawMaterial());
            return new TableGateway('activityRawMaterial', $dbAdapter, null, $resultSetPrototype);
        },
                'JobCard\Model\ActivityFixedAssetTable' => function($sm) {
            $tableGateway = $sm->get('ActivityFixedAssetTableGateway');
            $table = new ActivityFixedAssetTable($tableGateway);
            return $table;
        },
                'ActivityFixedAssetTableGateway' => function ($sm) {
            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new ActivityFixedAsset());
            return new TableGateway('activityFixedAsset', $dbAdapter, null, $resultSetPrototype);
        },
                'JobCard\Model\ActivityBatchTable' => function($sm) {
            $tableGateway = $sm->get('ActivityBatchTableGateway');
            $table = new ActivityBatchTable($tableGateway);
            return $table;
        },
                'ActivityBatchTableGateway' => function ($sm) {
            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new ActivityBatch());
            return new TableGateway('activityBatch', $dbAdapter, null, $resultSetPrototype);
        },
                'JobCard\Model\ActivitySerialTable' => function($sm) {
            $tableGateway = $sm->get('ActivitySerialTableGateway');
            $table = new ActivitySerialTable($tableGateway);
            return $table;
        },
                'ActivitySerialTableGateway' => function ($sm) {
            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new ActivitySerial());
            return new TableGateway('activitySerial', $dbAdapter, null, $resultSetPrototype);
        },
                'JobCard\Model\InquiryLogTable' => function($sm) {
            $tableGateway = $sm->get('InquiryLogTableGateway');
            $table = new InquiryLogTable($tableGateway);
            return $table;
        },
                'InquiryLogTableGateway' => function ($sm) {
            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new InquiryLog());
            return new TableGateway('inquiryLog', $dbAdapter, null, $resultSetPrototype);
        },
                'JobCard\Model\InquiryLogDutyManagerTable' => function($sm) {
            $tableGateway = $sm->get('InquiryLogDutyManagerTableGateway');
            $table = new InquiryLogDutyManagerTable($tableGateway);
            return $table;
        },
                'InquiryLogDutyManagerTableGateway' => function ($sm) {
            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new InquiryLogDutyManager());
            return new TableGateway('inquiryLogDutyManager', $dbAdapter, null, $resultSetPrototype);
        },
                'JobCard\Model\ActivityRawMaterialSubProductTable' => function($sm) {
            $tableGateway = $sm->get('ActivityRawMaterialSubProductTableGateway');
            $table = new ActivityRawMaterialSubProductTable($tableGateway);
            return $table;
        },
                'ActivityRawMaterialSubProductTableGateway' => function ($sm) {
            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new ActivityRawMaterialSubProduct());
            return new TableGateway('activityRawMaterialSubProduct', $dbAdapter, null, $resultSetPrototype);
        },
                'JobCard\Model\ActivityCostTypeTable' => function($sm) {
            $tableGateway = $sm->get('ActivityCostTypeTableGateway');
            $table = new ActivityCostTypeTable($tableGateway);
            return $table;
        },
                'ActivityCostTypeTableGateway' => function ($sm) {
            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new ActivityCostType());
            return new TableGateway('activityCostType', $dbAdapter, null, $resultSetPrototype);
        },
                'JobCard\Model\ActivityFixedAssetSubProductTable' => function($sm) {
            $tableGateway = $sm->get('ActivityFixedAssetSubProductTableGateway');
            $table = new ActivityFixedAssetSubProductTable($tableGateway);
            return $table;
        },
                'ActivityFixedAssetSubProductTableGateway' => function ($sm) {
            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new ActivityFixedAssetSubProduct());
            return new TableGateway('activityFixedAssetSubProduct', $dbAdapter, null, $resultSetPrototype);
        },
                'JobCard\Model\InquiryLogEmailScheduleTable' => function($sm) {
            $tableGateway = $sm->get('InquiryLogEmailScheduleTableGateway');
            $table = new InquiryLogEmailScheduleTable($tableGateway);
            return $table;
        },
                'InquiryLogEmailScheduleTableGateway' => function ($sm) {
            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new InquiryLogEmailSchedule());
            return new TableGateway('inquiryLogEmailSchedule', $dbAdapter, null, $resultSetPrototype);
        },
                'JobCard\Model\InquirySetupTable' => function($sm) {
            $tableGateway = $sm->get('InquirySetupTableGateway');
            $table = new InquirySetupTable($tableGateway);
            return $table;
        },
                'InquirySetupTableGateway' => function ($sm) {
            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new InquirySetup());
            return new TableGateway('inquirySetup', $dbAdapter, null, $resultSetPrototype);
        },
                'JobCard\Model\ActivityTemporaryProductTable' => function($sm) {
            $tableGateway = $sm->get('ActivityTemporaryProductTableGateway');
            $table = new ActivityTemporaryProductTable($tableGateway);
            return $table;
        },
                'ActivityTemporaryProductTableGateway' => function ($sm) {
            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new ActivityTemporaryProduct());
            return new TableGateway('activityTemporaryProduct', $dbAdapter, null, $resultSetPrototype);
        },
                'JobCard\Model\InquiryComplainRelationTable' => function($sm) {
            $tableGateway = $sm->get('InquiryComplainRelationTableGateway');
            $table = new InquiryComplainRelationTable($tableGateway);
            return $table;
        },
                'InquiryComplainRelationTableGateway' => function ($sm) {
            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new InquiryComplainRelation());
            return new TableGateway('inquiryComplainRelation', $dbAdapter, null, $resultSetPrototype);
        },
        'JobCard\Model\VehicleTable' => function($sm) {
            $tableGateway = $sm->get('VehicleTableGateway');
            $table = new Model\VehicleTable($tableGateway);
            return $table;
        },
        'VehicleTableGateway' => function ($sm) {
            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new Model\Vehicle());
            return new TableGateway('vehicle', $dbAdapter, null, $resultSetPrototype);
        },
        'JobCard\Model\ActivityVehicleTable' => function($sm) {
            $tableGateway = $sm->get('ActivityVehicleTableGateway');
            $table = new Model\ActivityVehicleTable($tableGateway);
            return $table;
        },
        'ActivityVehicleTableGateway' => function ($sm) {
            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new Model\ActivityVehicle());
            return new TableGateway('activityVehicle', $dbAdapter, null, $resultSetPrototype);
        }
            ),
        );
    }

}
