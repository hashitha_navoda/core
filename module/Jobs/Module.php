<?php

namespace Jobs;

use Jobs\Model\Department;
use Jobs\Model\DepartmentTable;
use Jobs\Model\Designation;
use Jobs\Model\DesignationTable;
use Jobs\Model\Employee;
use Jobs\Model\EmployeeDesignation;
use Jobs\Model\EmployeeDesignationTable;
use Jobs\Model\EmployeeTable;
use Jobs\Model\EmployeeTeam;
use Jobs\Model\EmployeeTeamTable;
use Jobs\Model\JobApproval;
use Jobs\Model\JobApprovalTable;
use Jobs\Model\JobContractor;
use Jobs\Model\JobContractorTable;
use Jobs\Model\JobEmployee;
use Jobs\Model\JobEmployeeTable;
use Jobs\Model\JobManager;
use Jobs\Model\JobManagerTable;
use Jobs\Model\JobProduct;
use Jobs\Model\JobProductTable;
use Jobs\Model\JobSupervisor;
use Jobs\Model\JobSupervisorTable;
use Jobs\Model\Job;
use Jobs\Model\JobTable;
use Jobs\Model\JobTask;
use Jobs\Model\JobTaskContractor;
use Jobs\Model\JobTaskContractorTable;
use Jobs\Model\JobSubTask;
use Jobs\Model\JobSubTaskTable;
use Jobs\Model\JobTaskEmployee;
use Jobs\Model\JobTaskEmployeeTable;
use Jobs\Model\JobTaskProduct;
use Jobs\Model\JobTaskProductTable;
use Jobs\Model\JobTaskTable;
use Jobs\Model\JobType;
use Jobs\Model\JobTypeTable;
use Jobs\Model\JobVehicle;
use Jobs\Model\JobVehicleTable;
use Jobs\Model\ProductJobApproval;
use Jobs\Model\ProductJobApprovalTable;
use Jobs\Model\ProjectMaterial;
use Jobs\Model\ProjectMaterialTable;
use Jobs\Model\Project;
use Jobs\Model\ProjectTable;
use Jobs\Model\ProjectType;
use Jobs\Model\ProjectTypeTable;
use Jobs\Model\RateCard;
use Jobs\Model\RateCardTable;
use Jobs\Model\RateCardTask;
use Jobs\Model\RateCardTaskTable;
use Jobs\Model\RateCardTaskSubTask;
use Jobs\Model\RateCardTaskSubTaskTable;
use Jobs\Model\RelatedFile;
use Jobs\Model\RelatedFileTable;
use Jobs\Model\ResourceSetup;
use Jobs\Model\ResourceSetupTable;
use Jobs\Model\ServiceVehicle;
use Jobs\Model\ServiceVehicleTable;
use Jobs\Model\Station;
use Jobs\Model\StationTable;
use Jobs\Model\Task;
use Jobs\Model\TaskCard;
use Jobs\Model\TaskCardTable;
use Jobs\Model\TaskCardTask;
use Jobs\Model\TaskCardTaskProduct;
use Jobs\Model\TaskCardTaskProductTable;
use Jobs\Model\TaskCardTaskTable;
use Jobs\Model\TaskTable;
use Jobs\Model\Team;
use Jobs\Model\TeamTable;
use Jobs\Model\Vehicle;
use Jobs\Model\VehicleModel;
use Jobs\Model\VehicleModelTable;
use Jobs\Model\VehicleSubModel;
use Jobs\Model\VehicleSubModelTable;
use Jobs\Model\MaterialRequest;
use Jobs\Model\MaterialRequestTable;
use Jobs\Model\MaterialRequestJobProduct;
use Jobs\Model\MaterialRequestJobProductTable;
use Jobs\Model\ContractorTable;
use Jobs\Model\JobGeneralSettings;
use Jobs\Model\JobGeneralSettingsTable;
use Jobs\Model\ProjectManager;
use Jobs\Model\ProjectManagerTable;
use Jobs\Model\ProjectSupervisor;
use Jobs\Model\ProjectSupervisorTable;
use Jobs\Model\VehicleTable;
use Jobs\Model\VehicleType;
use Jobs\Model\VehicleTypeTable;
use Jobs\Model\CostType;
use Jobs\Model\CostTypeTable;
use Jobs\Model\EventScore;
use Jobs\Model\EventScoreTable;
use Jobs\Model\ActivityCostType;
use Jobs\Model\ActivityCostTypeTable;
use Jobs\Model\JobTaskCost;
use Jobs\Model\JobTaskCostTable;
use Jobs\Model\JobProductCost;
use Jobs\Model\JobProductCostTable;
use Jobs\Model\JobCost;
use Jobs\Model\FuelType;
use Jobs\Model\FuelTypeTable;
use Jobs\Model\JobCostTable;
use Jobs\Model\JobUserRole;
use Jobs\Model\JobUserRoleTable;
use Jobs\Model\OtherJobCost;
use Jobs\Model\OtherJobCostTable;
use Jobs\Model\TaskCardTaskUnitPrice;
use Jobs\Model\TaskCardTaskUnitPriceTable;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;
use Jobs\Model\ServiceSubTaskTable;
use Jobs\Model\ServiceSubTask;
use Jobs\Model\DepartmentStation;
use Jobs\Model\DepartmentStationTable;
use Jobs\Model\EmployeeDepartment;
use Jobs\Model\EmployeeDepartmentTable;
use Jobs\Model\JobTaskEmployeeWorkTime;
use Jobs\Model\JobTaskEmployeeWorkTimeTable;
use Jobs\Model\ServiceVehicleType;
use Jobs\Model\ServiceVehicleTypeTable;
use Jobs\Model\ServiceEmployee;
use Jobs\Model\ServiceEmployeeTable;
use Jobs\Model\JobVehicleStatus;
use Jobs\Model\JobVehicleStatusTable;
use Jobs\Model\JobVehicleRemark;
use Jobs\Model\JobVehicleRemarkTable;
use Jobs\Model\ServiceInceniveRate;
use Jobs\Model\ServiceInceniveRateTable;
use Jobs\Model\ServiceProduct;
use Jobs\Model\ServiceProductTable;
use Jobs\Model\VehicleStatusCheck;
use Jobs\Model\VehicleStatusCheckTable;
use Jobs\Model\JobTempMaterial;
use Jobs\Model\JobTempMaterialTable;
use Jobs\Model\JobCardVehicleType;
use Jobs\Model\JobCardVehicleTypeTable;
use Jobs\Model\JobCardIncentiveRate;
use Jobs\Model\JobCardIncentiveRateTable;
use Jobs\Model\JobCardServiceIncentiveRate;
use Jobs\Model\JobCardServiceIncentiveRateTable;
use Jobs\Model\JobCardCost;
use Jobs\Model\JobCardCostTable;


class Module {

	public function onBootstrap(MvcEvent $e) {
		$eventManager = $e->getApplication()->getEventManager();
		$moduleRouteListener = new ModuleRouteListener();
		$moduleRouteListener->attach($eventManager);

		$config = $this->getConfig();
	}

	public function getAutoloaderConfig() {
		return array(
			'Zend\Loader\ClassMapAutoloader' => array(
				__DIR__ . '/autoload_classmap.php',
			),
			'Zend\Loader\StandardAutoloader' => array(
				'namespaces' => array(
					__NAMESPACE__=> __DIR__ . '/src/' . __NAMESPACE__,
				),
			),
		);
	}

	public function getConfig() {
		return include __DIR__ . '/config/module.config.php';
	}

    public function getServiceConfig()
    {
        return array(
            'invokables' => array(
                'ProjectService' => 'Jobs\Service\ProjectService',
                'TournamentService' => 'Jobs\Service\TournamentService',
                'DesignationService' => 'Jobs\Service\DesignationService',
                'DesignationController' => 'Jobs\Controller\DesignationController',
                'ProjectSetupService' => 'Jobs\Service\ProjectSetupService',
                'ProjectSetupController' => 'Jobs\Controller\ProjectSetupController',
                'ContractorController' => 'Jobs\Controller\ContractorController',
                'ContractorService' => 'Jobs\Service\ContractorService',
                'VehicleController' => 'Jobs\Controller\VehicleController',
                'EventController' => 'Jobs\Controller\EventController',
                'VehicleService' => 'Jobs\Service\VehicleService',
                'GeneralSetupController' => 'Jobs\Controller\GeneralSetupController',
                'GeneralSetupService' => 'Jobs\Service\GeneralSetupService',
                'DepartmentService' => 'Jobs\Service\DepartmentService',
                'DepartmentController' => 'Jobs\Controller\DepartmentController',
                'JobSetupService' => 'Jobs\Service\JobSetupService',
                'JobSetupController' => 'Jobs\Controller\JobSetupController',
                'EmployeeService' => 'Jobs\Service\EmployeeService',
                'ParticipantService' => 'Jobs\Service\ParticipantService',
                'EmployeeController' => 'Jobs\Controller\EmployeeController',
                'TaskSetupService' => 'Jobs\Service\TaskSetupService',
                'TaskCardService' => 'Jobs\Service\TaskCardService',
                'JobService' => 'Jobs\Service\JobService',
                'EventService' => 'Jobs\Service\EventService',
                'ScoreUpdateService' => 'Jobs\Service\ScoreUpdateService',
                'TaskSetupController' => 'Jobs\Controller\TaskSetupController',
                'TeamService' => 'Jobs\Service\TeamService',
                'TeamController' => 'Jobs\Controller\TeamController',
                'TaskCardController' => 'Jobs\Controller\TaskCardController',
                'MaterialRequisitionService' => 'Jobs\Service\MaterialRequisitionService',
                'ProjectController' => 'Jobs\Controller\ProjectController',
                'ProgressService' => 'Jobs\Service\ProgressService',
                'ProgressController' => 'Jobs\Controller\ProgressController',
                'ResourceManagementService' => 'Jobs\Service\ResourceManagementService',
                'JobController' => 'Jobs\Controller\JobController',
                'JobControllerAPI' => 'Jobs\Controller\API\JobController',
                'CostTypeController' => 'Jobs\Controller\CostTypeController',
                'CostTypeService' => 'Jobs\Service\CostTypeService',
                'CostService' => 'Jobs\Service\CostService',
                'RateCardService' => 'Jobs\Service\RateCardService',
                'RateCardController' => 'Jobs\Controller\RateCardController',
                'ResourcesController' => 'Jobs\Controller\ResourcesController',
                'ResourcesService' => 'Jobs\Service\ResourcesService',
                'IncentivesController' => 'Jobs\Controller\IncentivesController',
                'IncentivesService' => 'Jobs\Service\IncentivesService',
                'JobDashboardService' => 'Jobs\Service\JobDashboardService',
                'ServiceJobDashboardController' => 'Jobs\Controller\ServiceJobDashboardController',
                'MaterialManagementController' => 'Jobs\Controller\MaterialManagementController',
                'ServiceJobsController' => 'Jobs\Controller\ServiceJobsController',
                'ServiceJobsControllerAPI' => 'Jobs\Controller\API\ServiceJobsController',
                'ServiceCostController' => 'Jobs\Controller\ServiceCostController',
                'VehicleReleaseController' => 'Jobs\Controller\VehicleReleaseController',
                'VehicleReleaseService' => 'Jobs\Service\VehicleReleaseService',
                'EmployeeManagementService' => 'Jobs\Service\EmployeeManagementService',
            ),

            'shared' => array(
                'ProjectSetupService' => true,
                'ProjectService' => true,
                'TournamentService' => true,
                'VehicleService' => true,
                'GeneralSetupService' => true,
                'JobSetupService' => true,
                'TaskSetupService' => true,
                'MaterialRequisitionService' => true,
                'JobService' => true,
                'EventService' => true,
                'ScoreUpdateService' => true,
                'ResourceManagementService' => true,
                'ContractorService' => true,
                'ResourcesService' => true,
                'IncentivesService' => true,
                'VehicleReleaseService' => true,
                'EmployeeManagementService' => true
            ),
            'aliases' => array(
                'DesignationTable'=> 'Jobs\Model\DesignationTable',
                'ProjectTypeTable' => 'Jobs\Model\ProjectTypeTable',
                'ContractorTable' => 'Jobs\Model\ContractorTable',
                'VehicleTable' => 'Jobs\Model\VehicleTable',
                'ProjectTable' => 'Jobs\Model\ProjectTable',
                'MaterialRequestTable' => 'Jobs\Model\MaterialRequestTable',
                'MaterialRequestJobProductTable' => 'Jobs\Model\MaterialRequestJobProductTable',
                'JobGeneralSettingsTable' => 'Jobs\Model\JobGeneralSettingsTable',
                'DepartmentTable'=> 'Jobs\Model\DepartmentTable',
                'DepartmentStationTable'=> 'Jobs\Model\DepartmentStationTable',
                'JobTypeTable' => 'Jobs\Model\JobTypeTable',
                'JobTable' => 'Jobs\Model\JobTable',
                'EmployeeTable' => 'Jobs\Model\EmployeeTable',
                'EmployeeDesignationTable' => 'Jobs\Model\EmployeeDesignationTable',
                'TaskTable' => 'Jobs\Model\TaskTable',
                'TaskCardTable' => 'Jobs\Model\TaskCardTable',
                'TaskCardTaskTable' => 'Jobs\Model\TaskCardTaskTable',
                'TeamTable' => 'Jobs\Model\TeamTable',
                'EmployeeTeamTable' => 'Jobs\Model\EmployeeTeamTable',
                'TaskCardTaskProductTable' => 'Jobs\Model\TaskCardTaskProductTable',
                'JobTaskTable' => 'Jobs\Model\JobTaskTable',
                'JobEmployeeTable' => 'Jobs\Model\JobEmployeeTable',
                'JobProductTable' => 'Jobs\Model\JobProductTable',
                'JobTaskProductTable' => 'Jobs\Model\JobTaskProductTable',
                'JobSupervisorTable' => 'Jobs\Model\JobSupervisorTable',
                'JobManagerTable' => 'Jobs\Model\JobManagerTable',
                'ProjectManagerTable' => 'Jobs\Model\ProjectManagerTable',
                'ProjectSupervisorTable' => 'Jobs\Model\ProjectSupervisorTable',
                'ProjectMaterialTable' => 'Jobs\Model\ProjectMaterialTable',
                'ProjectTable' => 'Jobs\Model\ProjectTable',
                'JobContractorTable' => 'Jobs\Model\JobContractorTable',
                'JobTaskContractorTable' => 'Jobs\Model\JobTaskContractorTable',
                'JobSubTaskTable' => 'Jobs\Model\JobSubTaskTable',
                'JobVehicleTable' => 'Jobs\Model\JobVehicleTable',
                'CostTypeTable' => 'Jobs\Model\CostTypeTable',
                'EventScoreTable' => 'Jobs\Model\EventScoreTable',
                'ActivityCostTypeTable' => 'Jobs\Model\ActivityCostTypeTable',
                'JobTaskCostTable' => 'Jobs\Model\JobTaskCostTable',
                'JobProductCostTable' => 'Jobs\Model\JobProductCostTable',
                'JobCostTable' => 'Jobs\Model\JobCostTable',
                'OtherJobCostTable' => 'Jobs\Model\OtherJobCostTable',
                'JobUserRoleTable' => 'Jobs\Model\JobUserRoleTable',
                'VehicleTypeTable' => 'Jobs\Model\VehicleTypeTable',
                'VehicleModelTable' => 'Jobs\Model\VehicleModelTable',
                'VehicleSubModelTable' => 'Jobs\Model\VehicleSubModelTable',
                'ServiceSubTaskTable'=> 'Jobs\Model\ServiceSubTaskTable',
                'EmployeeDepartmentTable' => 'Jobs\Model\EmployeeDepartmentTable',
                'RateCardTable' => 'Jobs\Model\RateCardTable',
                'RateCardTaskTable' => 'Jobs\Model\RateCardTaskTable',
                'RateCardTaskSubTaskTable' => 'Jobs\Model\RateCardTaskSubTaskTable',
                'ServiceVehicleTable' => 'Jobs\Model\ServiceVehicleTable',
                'TaskCardTaskUnitPriceTable' => 'Jobs\Model\TaskCardTaskUnitPriceTable',
                'JobTaskEmployeeTable' => 'Jobs\Model\JobTaskEmployeeTable',
                'JobTaskEmployeeWorkTimeTable' => 'Jobs\Model\JobTaskEmployeeWorkTimeTable',
                'ServiceVehicleTypeTable' => 'Jobs\Model\ServiceVehicleTypeTable',
                'ServiceEmployeeTable' => 'Jobs\Model\ServiceEmployeeTable',
                'JobVehicleStatusTable' => 'Jobs\Model\JobVehicleStatusTable',
                'JobVehicleRemarkTable' => 'Jobs\Model\JobVehicleRemarkTable',
                'ServiceInceniveRateTable' => 'Jobs\Model\ServiceInceniveRateTable',
                'ServiceProductTable' => 'Jobs\Model\ServiceProductTable',
                'VehicleStatusCheckTable' => 'Jobs\Model\VehicleStatusCheckTable',
                'JobTempMaterialTable' => 'Jobs\Model\JobTempMaterialTable',
                'JobCardVehicleTypeTable' => 'Jobs\Model\JobCardVehicleTypeTable',
                'JobCardIncentiveRateTable' => 'Jobs\Model\JobCardIncentiveRateTable',
                'JobCardServiceIncentiveRateTable' => 'Jobs\Model\JobCardServiceIncentiveRateTable',
                'JobCardCostTable' => 'Jobs\Model\JobCardCostTable',
            ),
            'factories' => array(
                'Jobs\Model\ProjectMaterialTable' => function($sm) {
                    $tableGateway = $sm->get('ProjectMaterialTableGateway');
                    $table = new ProjectMaterialTable($tableGateway);
                    return $table;
                },
                'ProjectMaterialTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new ProjectMaterial());
                    return new TableGateway('projectMaterial', $dbAdapter, null, $resultSetPrototype);
                },
                'Jobs\Model\EmployeeDesignationTable' => function($sm) {
                    $tableGateway = $sm->get('EmployeeDesignationTableGateway');
                    $table = new EmployeeDesignationTable($tableGateway);
                    return $table;
                },
                'EmployeeDesignationTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new EmployeeDesignation());
                    return new TableGateway('employeeDesignation', $dbAdapter, null, $resultSetPrototype);
                },
                'Jobs\Model\RelatedFileTable' => function($sm) {
                    $tableGateway = $sm->get('RelatedFileTableGateway');
                    $table = new RelatedFileTable($tableGateway);
                    return $table;
                },
                'RelatedFileTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new RelatedFile());
                    return new TableGateway('relatedFile', $dbAdapter, null, $resultSetPrototype);
                },
                'Jobs\Model\JobContractorTable' => function($sm) {
                    $tableGateway = $sm->get('JobContractorTableGateway');
                    $table = new JobContractorTable($tableGateway);
                    return $table;
                },
                'JobContractorTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new JobContractor());
                    return new TableGateway('jobContractor', $dbAdapter, null, $resultSetPrototype);
                },
                'Jobs\Model\JobEmployeeTable' => function($sm) {
                    $tableGateway = $sm->get('JobEmployeeTableGateway');
                    $table = new JobEmployeeTable($tableGateway);
                    return $table;
                },
                'JobEmployeeTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new JobEmployee());
                    return new TableGateway('jobEmployee', $dbAdapter, null, $resultSetPrototype);
                },
                'Jobs\Model\TaskTable' => function($sm) {
                    $tableGateway = $sm->get('TaskTableGateway');
                    $table = new TaskTable($tableGateway);
                    return $table;
                },
                'TaskTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Task());
                    return new TableGateway('task', $dbAdapter, null, $resultSetPrototype);
                },
                'Jobs\Model\TaskCardTable' => function($sm) {
                    $tableGateway = $sm->get('TaskCardTableGateway');
                    $table = new TaskCardTable($tableGateway);
                    return $table;
                },
                'TaskCardTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new TaskCard());
                    return new TableGateway('taskCard', $dbAdapter, null, $resultSetPrototype);
                },
                'Jobs\Model\TaskCardTaskTable' => function($sm) {
                    $tableGateway = $sm->get('TaskCardTaskTableGateway');
                    $table = new TaskCardTaskTable($tableGateway);
                    return $table;
                },
                'TaskCardTaskTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new TaskCardTask());
                    return new TableGateway('taskCardTask', $dbAdapter, null, $resultSetPrototype);
                },
                'Jobs\Model\TaskCardTaskProductTable' => function($sm) {
                    $tableGateway = $sm->get('TaskCardTaskProductTableGateway');
                    $table = new TaskCardTaskProductTable($tableGateway);
                    return $table;
                },
                'TaskCardTaskProductTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new TaskCardTaskProduct());
                    return new TableGateway('taskCardTaskProduct', $dbAdapter, null, $resultSetPrototype);
                },
                'Jobs\Model\RateCardTable' => function($sm) {
                    $tableGateway = $sm->get('RateCardTableGateway');
                    $table = new RateCardTable($tableGateway);
                    return $table;
                },
                'RateCardTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new RateCard());
                    return new TableGateway('rateCard', $dbAdapter, null, $resultSetPrototype);
                },
                'Jobs\Model\RateCardTaskTable' => function($sm) {
                    $tableGateway = $sm->get('RateCardTaskTableGateway');
                    $table = new RateCardTaskTable($tableGateway);
                    return $table;
                },
                'RateCardTaskTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new RateCardTask());
                    return new TableGateway('rateCardTask', $dbAdapter, null, $resultSetPrototype);
                },
                'Jobs\Model\RateCardTaskSubTaskTable' => function($sm) {
                    $tableGateway = $sm->get('RateCardTaskSubTaskTableGateway');
                    $table = new RateCardTaskSubTaskTable($tableGateway);
                    return $table;
                },
                'RateCardTaskSubTaskTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new RateCardTaskSubTask());
                    return new TableGateway('rateCardTaskSubTasks', $dbAdapter, null, $resultSetPrototype);
                },
                'Jobs\Model\JobTaskTable' => function($sm) {
                    $tableGateway = $sm->get('JobTaskTableGateway');
                    $table = new JobTaskTable($tableGateway);
                    return $table;
                },
                'JobTaskTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new JobTask());
                    return new TableGateway('jobTask', $dbAdapter, null, $resultSetPrototype);
                },
                'Jobs\Model\JobTaskEmployeeTable' => function($sm) {
                    $tableGateway = $sm->get('JobTaskEmployeeTableGateway');
                    $table = new JobTaskEmployeeTable($tableGateway);
                    return $table;
                },
                'JobTaskEmployeeTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new JobTaskEmployee());
                    return new TableGateway('jobTaskEmployee', $dbAdapter, null, $resultSetPrototype);
                },
                'Jobs\Model\JobTaskContractorTable' => function($sm) {
                    $tableGateway = $sm->get('JobTaskContractorTableGateway');
                    $table = new JobTaskContractorTable($tableGateway);
                    return $table;
                },
                'JobTaskContractorTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new JobTaskContractor());
                    return new TableGateway('jobTaskContractor', $dbAdapter, null, $resultSetPrototype);
                },
                'Jobs\Model\JobTaskProductTable' => function($sm) {
                    $tableGateway = $sm->get('JobTaskProductTableGateway');
                    $table = new JobTaskProductTable($tableGateway);
                    return $table;
                },
                'JobTaskProductTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new JobTaskProduct());
                    return new TableGateway('jobTaskProduct', $dbAdapter, null, $resultSetPrototype);
                },
                'Jobs\Model\JobProductTable' => function($sm) {
                    $tableGateway = $sm->get('JobProductTableGateway');
                    $table = new JobProductTable($tableGateway);
                    return $table;
                },
                'JobProductTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new JobProduct());
                    return new TableGateway('jobProduct', $dbAdapter, null, $resultSetPrototype);
                },
                'Jobs\Model\JobApprovalTable' => function($sm) {
                    $tableGateway = $sm->get('JobApprovalTableGateway');
                    $table = new JobApprovalTable($tableGateway);
                    return $table;
                },
                'JobApprovalTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new JobApproval());
                    return new TableGateway('jobApproval', $dbAdapter, null, $resultSetPrototype);
                },
                'Jobs\Model\ProductJobApprovalTable' => function($sm) {
                    $tableGateway = $sm->get('ProductJobApprovalTableGateway');
                    $table = new ProductJobApprovalTable($tableGateway);
                    return $table;
                },
                'ProductJobApprovalTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new ProductJobApproval());
                    return new TableGateway('productJobApproval', $dbAdapter, null, $resultSetPrototype);
                },
                'Jobs\Model\ResourceSetupTable' => function($sm) {
                    $tableGateway = $sm->get('ResourceSetupTableGateway');
                    $table = new ResourceSetupTable($tableGateway);
                    return $table;
                },
                'ResourceSetupTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new ResourceSetup());
                    return new TableGateway('resourceSetup', $dbAdapter, null, $resultSetPrototype);
                },
                'Jobs\Model\ServiceVehicleTable' => function($sm) {
                    $tableGateway = $sm->get('ServiceVehicleTableGateway');
                    $table = new ServiceVehicleTable($tableGateway);
                    return $table;
                },
                'ServiceVehicleTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new ServiceVehicle());
                    return new TableGateway('serviceVehicle', $dbAdapter, null, $resultSetPrototype);
                },
                'Jobs\Model\VehicleTypeTable' => function($sm) {
                    $tableGateway = $sm->get('VehicleTypeTableGateway');
                    $table = new VehicleTypeTable($tableGateway);
                    return $table;
                },
                'VehicleTypeTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new VehicleType());
                    return new TableGateway('vehicleType', $dbAdapter, null, $resultSetPrototype);
                },
                'Jobs\Model\VehicleTable' => function($sm) {
                    $tableGateway = $sm->get('VehicleTableGateway');
                    $table = new VehicleTable($tableGateway);
                    return $table;
                },
                'VehicleTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Vehicle());
                    return new TableGateway('vehicle', $dbAdapter, null, $resultSetPrototype);
                },
                'Jobs\Model\VehicleModelTable' => function($sm) {
                    $tableGateway = $sm->get('VehicleModelTableGateway');
                    $table = new VehicleModelTable($tableGateway);
                    return $table;
                },
                'VehicleModelTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new VehicleModel());
                    return new TableGateway('vehicleModel', $dbAdapter, null, $resultSetPrototype);
                },
                'Jobs\Model\VehicleSubModelTable' => function($sm) {
                    $tableGateway = $sm->get('VehicleSubModelTableGateway');
                    $table = new VehicleSubModelTable($tableGateway);
                    return $table;
                },
                'VehicleSubModelTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new VehicleSubModel());
                    return new TableGateway('vehiclSubModel', $dbAdapter, null, $resultSetPrototype);
                },
                'Jobs\Model\JobVehicleTable' => function($sm) {
                    $tableGateway = $sm->get('JobVehicleTableGateway');
                    $table = new JobVehicleTable($tableGateway);
                    return $table;
                },
                'JobVehicleTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new JobVehicle());
                    return new TableGateway('jobVehicle', $dbAdapter, null, $resultSetPrototype);
                },
                'Jobs\Model\OtherJobCostTable' => function($sm) {
                    $tableGateway = $sm->get('OtherJobCostTableGateway');
                    $table = new OtherJobCostTable($tableGateway);
                    return $table;
                },
                'OtherJobCostTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new OtherJobCost());
                    return new TableGateway('otherJobCost', $dbAdapter, null, $resultSetPrototype);
                },
                'Jobs\Model\DepartmentTable' => function($sm) {
                    $tableGateway = $sm->get('DepartmentTableGateway');
                    $table = new DepartmentTable($tableGateway);
                    return $table;
                },
                'DepartmentTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Department());
                    return new TableGateway('department', $dbAdapter, null, $resultSetPrototype);
                },
                'Jobs\Model\TeamTable' => function($sm) {
                    $tableGateway = $sm->get('TeamTableGateway');
                    $table = new TeamTable($tableGateway);
                    return $table;
                },
                'TeamTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Team());
                    return new TableGateway('team', $dbAdapter, null, $resultSetPrototype);
                },
                'Jobs\Model\EmployeeTeamTable' => function($sm) {
                    $tableGateway = $sm->get('EmployeeTeamTableGateway');
                    $table = new EmployeeTeamTable($tableGateway);
                    return $table;
                },
                'EmployeeTeamTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new EmployeeTeam());
                    return new TableGateway('employeeTeam', $dbAdapter, null, $resultSetPrototype);
                },
                'Jobs\Model\StationTable' => function($sm) {
                    $tableGateway = $sm->get('StationTableGateway');
                    $table = new StationTable($tableGateway);
                    return $table;
                },
                'StationTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Station());
                    return new TableGateway('station', $dbAdapter, null, $resultSetPrototype);
                },
                'Jobs\Model\DesignationTable' => function($sm) {
                    $tableGateway = $sm->get('DesignationTableGateway');
                    $table = new DesignationTable($tableGateway);
                    return $table;
                },
                'DesignationTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Designation());
                    return new TableGateway('designation', $dbAdapter, null, $resultSetPrototype);
                },
                'Jobs\Model\ProjectTypeTable' => function($sm) {
                    $tableGateway = $sm->get('ProjectTypeTableGateway');
                    $table = new ProjectTypeTable($tableGateway);
                    return $table;
                },
                'ProjectTypeTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Station());
                    return new TableGateway('projectType', $dbAdapter, null, $resultSetPrototype);
                },
                'Jobs\Model\ContractorTable' => function($sm) {
                    $tableGateway = $sm->get('ContractorTableGateway');
                    $table = new ContractorTable($tableGateway);
                    return $table;
                },
                'ContractorTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Station());
                    return new TableGateway('contractor', $dbAdapter, null, $resultSetPrototype);
                },
                'Jobs\Model\ProjectTable' => function($sm) {
                    $tableGateway = $sm->get('ProjectTableGateway');
                    $table = new ProjectTable($tableGateway);
                    return $table;
                },
                'ProjectTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Project());
                    return new TableGateway('project', $dbAdapter, null, $resultSetPrototype);
                },
                'Jobs\Model\JobTypeTable' => function($sm) {
                    $tableGateway = $sm->get('JobTypeTableGateway');
                    $table = new JobTypeTable($tableGateway);
                    return $table;
                },
                'JobTypeTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Station());
                    return new TableGateway('jobType', $dbAdapter, null, $resultSetPrototype);
                },
                'Jobs\Model\JobTable' => function($sm) {
                    $tableGateway = $sm->get('JobTableGateway');
                    $table = new JobTable($tableGateway);
                    return $table;
                },
                'JobTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Job());
                    return new TableGateway('job', $dbAdapter, null, $resultSetPrototype);
                },
                'Jobs\Model\EmployeeTable' => function($sm) {
                    $tableGateway = $sm->get('EmployeeTableGateway');
                    $table = new EmployeeTable($tableGateway);
                    return $table;
                },
                'EmployeeTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Employee());
                    return new TableGateway('employee', $dbAdapter, null, $resultSetPrototype);
                },
                'Jobs\Model\MaterialRequestTable' => function($sm) {
                    $tableGateway = $sm->get('MaterialRequestTableGateway');
                    $table = new MaterialRequestTable($tableGateway);
                    return $table;
                },
                'MaterialRequestTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new MaterialRequest());
                    return new TableGateway('materialRequest', $dbAdapter, null, $resultSetPrototype);
                },
                'Jobs\Model\MaterialRequestJobProductTable' => function($sm) {
                    $tableGateway = $sm->get('MaterialRequestJobProductTableGateway');
                    $table = new MaterialRequestJobProductTable($tableGateway);
                    return $table;
                },
                'MaterialRequestJobProductTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new MaterialRequestJobProduct());
                    return new TableGateway('materialRequestJobProduct', $dbAdapter, null, $resultSetPrototype);
                },
                'Jobs\Model\ProjectManagerTable' => function($sm) {
                    $tableGateway = $sm->get('ProjectManagerTableGateway');
                    $table = new ProjectManagerTable($tableGateway);
                    return $table;
                },
                'ProjectManagerTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new ProjectManager());
                    return new TableGateway('projectManagers', $dbAdapter, null, $resultSetPrototype);
                },
                'Jobs\Model\ProjectSupervisorTable' => function($sm) {
                    $tableGateway = $sm->get('ProjectSupervisorTableGateway');
                    $table = new ProjectSupervisorTable($tableGateway);
                    return $table;
                },
                'ProjectSupervisorTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new ProjectSupervisor());
                    return new TableGateway('projectSupervisors', $dbAdapter, null, $resultSetPrototype);
                },
                'Jobs\Model\JobSupervisorTable' => function ($sm) {
                    $tableGateway = $sm->get('JobSupervisorTableGateway');
                    $table = new JobSupervisorTable($tableGateway);
                    return $table;
                },
                'JobSupervisorTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new JobSupervisor());
                    return new TableGateway('jobSupervisor', $dbAdapter, null, $resultSetPrototype);
                },
                'Jobs\Model\JobManagerTable' => function ($sm) {
                    $tableGateway = $sm->get('JobManagerTableGateway');
                    $table = new JobManagerTable($tableGateway);
                    return $table;
                },
                'JobManagerTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new JobManager());
                    return new TableGateway('jobManager', $dbAdapter, null, $resultSetPrototype);
                },    
                'Jobs\Model\CostTypeTable' => function($sm) {
                    $tableGateway = $sm->get('CostTypeTableGateway');
                    $table = new CostTypeTable($tableGateway);
                    return $table;
                },
                'CostTypeTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new CostType());
                    return new TableGateway('costType', $dbAdapter, null, $resultSetPrototype);
                },
                'Jobs\Model\EventScoreTable' => function($sm) {
                    $tableGateway = $sm->get('EventScoreTableGateway');
                    $table = new EventScoreTable($tableGateway);
                    return $table;
                },
                'EventScoreTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new EventScore());
                    return new TableGateway('eventScore', $dbAdapter, null, $resultSetPrototype);
                },
                'Jobs\Model\ActivityCostTypeTable' => function($sm) {
                    $tableGateway = $sm->get('ActivityCostTypeTableGateway');
                    $table = new ActivityCostTypeTable($tableGateway);
                    return $table;
                },
                'ActivityCostTypeTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new ActivityCostType());
                    return new TableGateway('activityCostType', $dbAdapter, null, $resultSetPrototype);
                },
                'JobGeneralSettingsTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new JobGeneralSettings());
                    return new TableGateway('jobGeneralSettings', $dbAdapter, null, $resultSetPrototype);
                },
                'Jobs\Model\JobGeneralSettingsTable' => function($sm) {
                    $tableGateway = $sm->get('JobGeneralSettingsTableGateway');
                    $table = new JobGeneralSettingsTable($tableGateway);
                    return $table;
                },
                'JobTaskCostTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new JobTaskCost());
                    return new TableGateway('jobTaskCost', $dbAdapter, null, $resultSetPrototype);
                },
                'Jobs\Model\JobTaskCostTable' => function($sm) {
                    $tableGateway = $sm->get('JobTaskCostTableGateway');
                    $table = new JobTaskCostTable($tableGateway);
                    return $table;
                },
                'JobProductCostTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new JobProductCost());
                    return new TableGateway('jobProductCost', $dbAdapter, null, $resultSetPrototype);
                },
                'Jobs\Model\JobProductCostTable' => function($sm) {
                    $tableGateway = $sm->get('JobProductCostTableGateway');
                    $table = new JobProductCostTable($tableGateway);
                    return $table;
                },
                'JobCostTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new JobCost());
                    return new TableGateway('jobCost', $dbAdapter, null, $resultSetPrototype);
                },
                'Jobs\Model\JobCostTable' => function($sm) {
                    $tableGateway = $sm->get('JobCostTableGateway');
                    $table = new JobCostTable($tableGateway);
                    return $table;
                },
                'FuelTypeTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new FuelType());
                    return new TableGateway('fuelType', $dbAdapter, null, $resultSetPrototype);
                },
                'Jobs\Model\FuelTypeTable' => function($sm) {
                    $tableGateway = $sm->get('FuelTypeTableGateway');
                    $table = new FuelTypeTable($tableGateway);
                    return $table;
                },
                'JobUserRoleTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new JobUserRole());
                    return new TableGateway('jobUserRole', $dbAdapter, null, $resultSetPrototype);
                },
                'Jobs\Model\JobUserRoleTable' => function($sm) {
                    $tableGateway = $sm->get('JobUserRoleTableGateway');
                    $table = new JobUserRoleTable($tableGateway);
                    return $table;
                },
                'Jobs\Model\ServiceSubTaskTable' => function($sm) {
                    $tableGateway = $sm->get('ServiceSubTaskTableGateway');
                    $table = new ServiceSubTaskTable($tableGateway);
                    return $table;
                },
                'ServiceSubTaskTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new ServiceSubTask());
                    return new TableGateway('serviceSubTask', $dbAdapter, null, $resultSetPrototype);
                },
                'Jobs\Model\DepartmentStationTable' => function($sm) {
                    $tableGateway = $sm->get('DepartmentStationTableGateway');
                    $table = new DepartmentStationTable($tableGateway);
                    return $table;
                },
                'DepartmentStationTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new DepartmentStation());
                    return new TableGateway('departmentStation', $dbAdapter, null, $resultSetPrototype);
                },
                'Jobs\Model\EmployeeDepartmentTable' => function($sm) {
                    $tableGateway = $sm->get('EmployeeDepartmentTableGateway');
                    $table = new EmployeeDepartmentTable($tableGateway);
                    return $table;
                },
                'EmployeeDepartmentTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new EmployeeDepartment());
                    return new TableGateway('employeeDepartment', $dbAdapter, null, $resultSetPrototype);
                },
                'Jobs\Model\JobSubTaskTable' => function($sm) {
                    $tableGateway = $sm->get('JobSubTaskTableGateway');
                    $table = new JobSubTaskTable($tableGateway);
                    return $table;
                },
                'JobSubTaskTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new JobSubTask());
                    return new TableGateway('jobSubTask', $dbAdapter, null, $resultSetPrototype);
                },
                'TaskCardTaskUnitPriceTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new TaskCardTaskUnitPrice());
                    return new TableGateway('taskCardTaskUnitPrice', $dbAdapter, null, $resultSetPrototype);
                },
                'Jobs\Model\TaskCardTaskUnitPriceTable' => function($sm) {
                    $tableGateway = $sm->get('TaskCardTaskUnitPriceTableGateway');
                    $table = new TaskCardTaskUnitPriceTable($tableGateway);
                    return $table;
                },
                'Jobs\Model\JobTaskEmployeeWorkTimeTable' => function($sm) {
                    $tableGateway = $sm->get('JobTaskEmployeeWorkTimeTableGateway');
                    $table = new JobTaskEmployeeWorkTimeTable($tableGateway);
                    return $table;
                },
                'JobTaskEmployeeWorkTimeTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new JobTaskEmployeeWorkTime());
                    return new TableGateway('jobTaskEmployeeWorkTime', $dbAdapter, null, $resultSetPrototype);
                },
                'Jobs\Model\ServiceVehicleTypeTable' => function($sm) {
                    $tableGateway = $sm->get('ServiceVehicleTypeTableGateway');
                    $table = new ServiceVehicleTypeTable($tableGateway);
                    return $table;
                },
                'ServiceVehicleTypeTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new ServiceVehicleType());
                    return new TableGateway('serviceVehicleType', $dbAdapter, null, $resultSetPrototype);
                },
                'Jobs\Model\ServiceEmployeeTable' => function($sm) {
                    $tableGateway = $sm->get('ServiceEmployeeTableGateway');
                    $table = new ServiceEmployeeTable($tableGateway);
                    return $table;
                },
                'ServiceEmployeeTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new ServiceEmployee());
                    return new TableGateway('serviceEmployee', $dbAdapter, null, $resultSetPrototype);
                },
                'Jobs\Model\JobVehicleStatusTable' => function($sm) {
                    $tableGateway = $sm->get('JobVehicleStatusTableGateway');
                    $table = new JobVehicleStatusTable($tableGateway);
                    return $table;
                },
                'JobVehicleStatusTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new JobVehicleStatus());
                    return new TableGateway('jobVehicleStatus', $dbAdapter, null, $resultSetPrototype);
                },
                'Jobs\Model\JobVehicleRemarkTable' => function($sm) {
                    $tableGateway = $sm->get('JobVehicleRemarkTableGateway');
                    $table = new JobVehicleRemarkTable($tableGateway);
                    return $table;
                },
                'JobVehicleRemarkTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new JobVehicleRemark());
                    return new TableGateway('jobVehicleRemark', $dbAdapter, null, $resultSetPrototype);
                },
                'Jobs\Model\ServiceInceniveRateTable' => function($sm) {
                    $tableGateway = $sm->get('ServiceInceniveRateTableGateway');
                    $table = new ServiceInceniveRateTable($tableGateway);
                    return $table;
                },
                'ServiceInceniveRateTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new ServiceInceniveRate());
                    return new TableGateway('serviceInceniveRate', $dbAdapter, null, $resultSetPrototype);
                },
                'Jobs\Model\ServiceProductTable' => function($sm) {
                    $tableGateway = $sm->get('ServiceProductTableGateway');
                    $table = new ServiceProductTable($tableGateway);
                    return $table;
                },
                'ServiceProductTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new ServiceProduct());
                    return new TableGateway('serviceProduct', $dbAdapter, null, $resultSetPrototype);
                },
                'Jobs\Model\VehicleStatusCheckTable' => function($sm) {
                    $tableGateway = $sm->get('VehicleStatusCheckTableGateway');
                    $table = new VehicleStatusCheckTable($tableGateway);
                    return $table;
                },
                'VehicleStatusCheckTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new VehicleStatusCheck());
                    return new TableGateway('vehicleStatusCheck', $dbAdapter, null, $resultSetPrototype);
                },
                'Jobs\Model\JobTempMaterialTable' => function($sm) {
                    $tableGateway = $sm->get('JobTempMaterialTableGateway');
                    $table = new JobTempMaterialTable($tableGateway);
                    return $table;
                },
                'JobTempMaterialTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new JobTempMaterial());
                    return new TableGateway('jobTempMaterial', $dbAdapter, null, $resultSetPrototype);
                },
                'Jobs\Model\JobCardVehicleTypeTable' => function($sm) {
                    $tableGateway = $sm->get('JobCardVehicleTypeTableGateway');
                    $table = new JobCardVehicleTypeTable($tableGateway);
                    return $table;
                },
                'JobCardVehicleTypeTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new JobCardVehicleType());
                    return new TableGateway('jobCardVehicleType', $dbAdapter, null, $resultSetPrototype);
                },
                'Jobs\Model\JobCardIncentiveRateTable' => function($sm) {
                    $tableGateway = $sm->get('JobCardIncentiveRateTableGateway');
                    $table = new JobCardIncentiveRateTable($tableGateway);
                    return $table;
                },
                'JobCardIncentiveRateTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new JobCardIncentiveRate());
                    return new TableGateway('jobCardIncentiveRate', $dbAdapter, null, $resultSetPrototype);
                },
                'Jobs\Model\JobCardServiceIncentiveRateTable' => function($sm) {
                    $tableGateway = $sm->get('JobCardServiceIncentiveRateTableGateway');
                    $table = new JobCardServiceIncentiveRateTable($tableGateway);
                    return $table;
                },
                'JobCardServiceIncentiveRateTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new JobCardServiceIncentiveRate());
                    return new TableGateway('jobCardServiceIncentiveRate', $dbAdapter, null, $resultSetPrototype);
                },
                'Jobs\Model\JobCardCostTable' => function($sm) {
                    $tableGateway = $sm->get('JobCardCostTableGateway');
                    $table = new JobCardCostTable($tableGateway);
                    return $table;
                },
                'JobCardCostTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new JobCardCost());
                    return new TableGateway('jobCardCost', $dbAdapter, null, $resultSetPrototype);
                }
                
            ),
        );
    }
}
