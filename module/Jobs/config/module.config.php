<?php

return array(
	'controllers' => array(
		'invokables' => array(
			'Jobs\Controller\JobSetup' => 'Jobs\Controller\JobSetupController',
			'Jobs\Controller\API\JobSetup' => 'Jobs\Controller\API\JobSetupController',
			'Jobs\Controller\Project' => 'Jobs\Controller\ProjectController',
			'Jobs\Controller\ProjectSetup' => 'Jobs\Controller\ProjectSetupController',
            'Jobs\Controller\TournamentSetup' => 'Jobs\Controller\TournamentSetupController',
            'Jobs\Controller\TournamentTeam' => 'Jobs\Controller\TournamentTeamController',
            'Jobs\Controller\API\TournamentTeam' => 'Jobs\Controller\API\TournamentTeamController',
            'Jobs\Controller\EventSetup' => 'Jobs\Controller\EventSetupController',
            'Jobs\Controller\ParticipantType' => 'Jobs\Controller\ParticipantTypeController',
            'Jobs\Controller\ParticipantSubType' => 'Jobs\Controller\ParticipantSubTypeController',
            'Jobs\Controller\Participant' => 'Jobs\Controller\ParticipantController',
            'Jobs\Controller\Tournament' => 'Jobs\Controller\TournamentController',
            'Jobs\Controller\Event' => 'Jobs\Controller\EventController',
            'Jobs\Controller\ScoreUpdate' => 'Jobs\Controller\ScoreUpdateController',
			'Jobs\Controller\API\ProjectSetup' => 'Jobs\Controller\API\ProjectSetupController',
            'Jobs\Controller\API\ScoreUpdate' => 'Jobs\Controller\API\ScoreUpdateController',
            'Jobs\Controller\API\Participant' => 'Jobs\Controller\API\ParticipantController',
			'Jobs\Controller\TaskSetup' => 'Jobs\Controller\TaskSetupController',
			'Jobs\Controller\Vehicle' => 'Jobs\Controller\VehicleController',
			'Jobs\Controller\API\Vehicle' => 'Jobs\Controller\API\VehicleController',
			'Jobs\Controller\API\TaskSetup' => 'Jobs\Controller\API\TaskSetupController',
			'Jobs\Controller\Designation' => 'Jobs\Controller\DesignationController',
			'Jobs\Controller\Department' => 'Jobs\Controller\DepartmentController',
			'Jobs\Controller\Team' => 'Jobs\Controller\TeamController',
			'Jobs\Controller\Employee' => 'Jobs\Controller\EmployeeController',
			'Jobs\Controller\API\Employee' => 'Jobs\Controller\API\EmployeeAPIController',
			'Jobs\Controller\TaskCard' => 'Jobs\Controller\TaskCardController',
			'Jobs\Controller\API\TaskCard' => 'Jobs\Controller\API\TaskCardController',
			'Jobs\Controller\API\Project' => 'Jobs\Controller\API\ProjectController',
            'Jobs\Controller\API\Tournament' => 'Jobs\Controller\API\TournamentController',
			'Jobs\Controller\API\Designation' => 'Jobs\Controller\API\DesignationAPIController',
			'Jobs\Controller\API\Department' => 'Jobs\Controller\API\DepartmentAPIController',
			'Jobs\Controller\API\Team' => 'Jobs\Controller\API\TeamAPIController',
			'Jobs\Controller\Job' => 'Jobs\Controller\JobController',
			'Jobs\Controller\API\Job' => 'Jobs\Controller\API\JobController',
            'Jobs\Controller\API\Event' => 'Jobs\Controller\API\EventController',
            'Jobs\Controller\ResourceManagement' => 'Jobs\Controller\ResourceManagementController',
            'Jobs\Controller\API\ResourceManagement' => 'Jobs\Controller\API\ResourceManagementController',
            'Jobs\Controller\API\Contractor' => 'Jobs\Controller\API\ContractorController',
            'Jobs\Controller\MaterialRequisition' => 'Jobs\Controller\MaterialRequisitionController',
            'Jobs\Controller\Api\MaterialRequisition' => 'Jobs\Controller\API\MaterialRequisitionController',
            'Jobs\Controller\GeneralSetup' => 'Jobs\Controller\GeneralSetupController',
            'Jobs\Controller\API\GeneralSetup' => 'Jobs\Controller\API\GeneralSetupController',
            'Jobs\Controller\Cost' => 'Jobs\Controller\CostController',
            'Jobs\Controller\API\Cost' => 'Jobs\Controller\API\CostController',
            'Jobs\Controller\CostType' => 'Jobs\Controller\CostTypeController',
            'Jobs\Controller\API\CostType' => 'Jobs\Controller\API\CostTypeController',
            'Jobs\Controller\Progress' => 'Jobs\Controller\ProgressController',
            'Jobs\Controller\API\Progress' => 'Jobs\Controller\API\ProgressController',
            'Jobs\Controller\ResourceManagement' => 'Jobs\Controller\ResourceManagementController',
            'Jobs\Controller\API\ResourceManagement' => 'Jobs\Controller\API\ResourceManagementController',
            'Jobs\Controller\Resources' => 'Jobs\Controller\ResourcesController',
            'Jobs\Controller\API\Resources' => 'Jobs\Controller\API\ResourcesController',
            'Jobs\Controller\ServiceSetup' => 'Jobs\Controller\ServiceSetupController',
            'Jobs\Controller\API\ServiceSetup' => 'Jobs\Controller\API\ServiceSetupController',
            'Jobs\Controller\DepartmentStation' => 'Jobs\Controller\DepartmentStationController',
            'Jobs\Controller\API\DepartmentStation' => 'Jobs\Controller\API\DepartmentStationController',
            'Jobs\Controller\JobCardSetup' => 'Jobs\Controller\JobCardSetupController',
            'Jobs\Controller\API\JobCardSetup' => 'Jobs\Controller\API\JobCardSetupController',
            'Jobs\Controller\ServiceEmployee' => 'Jobs\Controller\ServiceEmployeeController',
            'Jobs\Controller\API\ServiceEmployee' => 'Jobs\Controller\API\ServiceEmployeeController',
            'Jobs\Controller\RateCard' => 'Jobs\Controller\RateCardController',
            'Jobs\Controller\API\RateCard' => 'Jobs\Controller\API\RateCardController',
            'Jobs\Controller\ServiceJobs' => 'Jobs\Controller\ServiceJobsController',
            'Jobs\Controller\API\ServiceJobs' => 'Jobs\Controller\API\ServiceJobsController',
            'Jobs\Controller\Incentives' => 'Jobs\Controller\IncentivesController',
            'Jobs\Controller\API\Incentives' => 'Jobs\Controller\API\IncentivesController',
            'Jobs\Controller\ServiceJobDashboard' => 'Jobs\Controller\ServiceJobDashboardController',
            'Jobs\Controller\API\ServiceJobDashboard' => 'Jobs\Controller\API\ServiceJobDashboardController',
            'Jobs\Controller\Contractor' => 'Jobs\Controller\ContractorController',
            'Jobs\Controller\MaterialManagement' => 'Jobs\Controller\MaterialManagementController',
            'Jobs\Controller\API\MaterialManagement' => 'Jobs\Controller\API\MaterialManagementController',
            'Jobs\Controller\ServiceProgressUpdate' => 'Jobs\Controller\ServiceProgressUpdateController',
            'Jobs\Controller\API\ServiceProgressUpdate' => 'Jobs\Controller\API\ServiceProgressUpdateController',
            'Jobs\Controller\ServiceCost' => 'Jobs\Controller\ServiceCostController',
            'Jobs\Controller\API\ServiceCost' => 'Jobs\Controller\API\ServiceCostController',
            'Jobs\Controller\VehicleRelease' => 'Jobs\Controller\VehicleReleaseController',
            'Jobs\Controller\API\VehicleRelease' => 'Jobs\Controller\API\VehicleReleaseController',
            'Jobs\Controller\EmployeeManagement' => 'Jobs\Controller\EmployeeManagementController',
            'Jobs\Controller\API\EmployeeManagement' => 'Jobs\Controller\API\EmployeeManagementController',
            'Jobs\Controller\VehicleHistory' => 'Jobs\Controller\VehicleHistoryController',
        ),
	),
	'router' => array(
		'routes' => array(
			'jobs' => array(
				'type' => 'segment',
				'options' => array(
					'route' => '/jobs[/:action][/:param1][/:param2][/:param3]',
					'constraints' => array(
						'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
					),
					'defaults' => array(
						'controller' => 'Jobs\Controller\Job',
                        'action' => 'create',
                    ),
                ),
            ),
            'jobApi' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/job-api[/:action][/:param1][/:param2][/:param3]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Jobs\Controller\API\Job',
                        'action' => 'create',
                    ),
                ),
            ),
            'eventApi' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/event-api[/:action][/:param1][/:param2][/:param3]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Jobs\Controller\API\Event',
                        'action' => 'create',
                    ),
                ),
            ),
            'tournamentTeamApi' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/tournament-team-api[/:action][/:param1][/:param2][/:param3]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Jobs\Controller\API\TournamentTeam',
                        'action' => 'create',
                    ),
                ),
            ),
            'project' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/project[/:action][/:param1][/:param2][/:param3]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Jobs\Controller\Project',
                        'action' => 'index',
                    )
                ),
            ),
            'project-api' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/api/project[/:action][/:param1][/:param2][/:param3]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Jobs\Controller\API\Project',
                        'action' => 'create',
                    )
                ),
            ),
			'tournament-api' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/api/tournament[/:action][/:param1][/:param2][/:param3]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Jobs\Controller\API\Tournament',
                        'action' => 'create',
                    )
                ),
            ),
            'schedular' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/schedular[/:action][/:param1][/:param2][/:param3]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Jobs\Controller\Schedular',
                        'action' => 'create',
                    )
                ),
            ),
            'resourceManagement' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/resourceManagement[/:action][/:param1][/:param2][/:param3]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Jobs\Controller\ResourceManagement',
                        'action' => 'index',
                    )
                ),
            ),
            'materialManagement' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/materialManagement[/:action][/:param1][/:param2][/:param3]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Jobs\Controller\MaterialManagement',
                        'action' => 'index',
                    )
                ),
            ),
            'serviceCost' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/serviceCost[/:action][/:param1][/:param2][/:param3]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Jobs\Controller\ServiceCost',
                        'action' => 'index',
                    )
                ),
            ),
            'vehicleRelease' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/vehicleRelease[/:action][/:param1][/:param2][/:param3]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Jobs\Controller\VehicleRelease',
                        'action' => 'index',
                    )
                ),
            ),
            'projectSetup' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/project_setup[/:action][/:param1][/:param2][/:param3]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Jobs\Controller\ProjectSetup',
                        'action' => 'index',
                    )
                ),
            ),
            'tournamentSetup' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/tournament_setup[/:action][/:param1][/:param2][/:param3]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Jobs\Controller\TournamentSetup',
                        'action' => 'index',
                    )
                ),
            ),
            'eventSetup' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/event_setup[/:action][/:param1][/:param2][/:param3]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Jobs\Controller\EventSetup',
                        'action' => 'index',
                    )
                ),
            ),
            'participantType' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/participant_type[/:action][/:param1][/:param2][/:param3]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Jobs\Controller\ParticipantType',
                        'action' => 'index',
                    )
                ),
            ),
            'participantSubType' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/participant_sub_type[/:action][/:param1][/:param2][/:param3]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Jobs\Controller\ParticipantSubType',
                        'action' => 'index',
                    )
                ),
            ),
            'participant' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/participant[/:action][/:param1][/:param2][/:param3]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Jobs\Controller\Participant',
                        'action' => 'index',
                    )
                ),
            ),
            'tournament' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/tournament[/:action][/:param1][/:param2][/:param3]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Jobs\Controller\Tournament',
                        'action' => 'index',
                    )
                ),
            ),
            'event' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/event[/:action][/:param1][/:param2][/:param3]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Jobs\Controller\Event',
                        'action' => 'create',
                    )
                ),
            ),
            'scoreUpdate' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/scoreUpdate[/:action][/:param1][/:param2][/:param3]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param1' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param2' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Jobs\Controller\ScoreUpdate',
                    ),
                ),
            ),
            'tournamentTeam' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/tournament_team[/:action][/:param1][/:param2][/:param3]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Jobs\Controller\TournamentTeam',
                        'action' => 'index',
                    )
                ),
            ),
            'projectSetupApi' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/project_setup_api[/:action][/:param1][/:param2][/:param3]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Jobs\Controller\API\ProjectSetup',
                        'action' => 'index',
                    )
                ),
            ),
            'jobSetup' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/job_setup[/:action][/:param1][/:param2][/:param3]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Jobs\Controller\JobSetup',
                        'action' => 'index',
                    )
                ),
            ),
            'jobSetupApi' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/job_setup_api[/:action][/:param1][/:param2][/:param3]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Jobs\Controller\API\JobSetup',
                        'action' => 'index',
                    )
                ),
            ),
            'taskSetup' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/task_setup[/:action][/:param1][/:param2][/:param3]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Jobs\Controller\TaskSetup',
                        'action' => 'index',
                    )
                ),
            ),
            'taskSetupApi' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/task_setup_api[/:action][/:param1][/:param2][/:param3]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Jobs\Controller\API\TaskSetup',
                        'action' => 'index',
                    )
                ),
            ),
            'taskCard' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/task_card[/:action][/:param1][/:param2][/:param3]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Jobs\Controller\TaskCard',
                        'action' => 'index',
                    )
                ),
            ),
            'taskCardApi' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/task_card_api[/:action][/:param1][/:param2][/:param3]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Jobs\Controller\API\TaskCard',
                    )
                ),
            ),
            'jobsReport' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/jobsReport[/:action][/:param1][/:param2][/:param3]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Jobs\Controller\JobsReport',
                        'action' => 'create',
                    )
                ),
            ),
            'generalSetup' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/general_setup[/:action][/:param1][/:param2][/:param3]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Jobs\Controller\GeneralSetup',
                        'action' => 'index',
                    )
                ),
            ),
			'resourceManagementApi' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/api/resourceManagement[/:action][/:param1][/:param2][/:param3]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Jobs\Controller\API\ResourceManagement',
                        'action' => 'index',
                    )
                ),
            ),
            'materialManagementApi' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/api/materialManagement[/:action][/:param1][/:param2][/:param3]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Jobs\Controller\API\MaterialManagement',
                        'action' => 'index',
                    )
                ),
            ),
            'serviceCostApi' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/api/serviceCost[/:action][/:param1][/:param2][/:param3]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Jobs\Controller\API\ServiceCost',
                        'action' => 'index',
                    )
                ),
            ),
            'vehicleReleaseApi' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/api/vehicleRelease[/:action][/:param1][/:param2][/:param3]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Jobs\Controller\API\VehicleRelease',
                        'action' => 'index',
                    )
                ),
            ),
            'generalSetupApi' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/api/general_setup[/:action][/:param1][/:param2][/:param3]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Jobs\Controller\API\GeneralSetup',
                        'action' => 'index',
                    )
                ),
            ),
			'cost' => array(
				'type' => 'segment',
				'options' => array(
					'route' => '/cost[/:action][/:param1][/:param2][/:param3]',
					'constraints' => array(
						'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
					),
					'defaults' => array(
						'controller' => 'Jobs\Controller\Cost',
						'action' => 'index',
					),
				),
			),
			'costApi' => array(
				'type' => 'segment',
				'options' => array(
					'route' => '/api/cost[/:action][/:param1][/:param2][/:param3]',
					'constraints' => array(
						'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
					),
					'defaults' => array(
						'controller' => 'Jobs\Controller\API\Cost',
						'action' => 'index',
					),
				),
			),
			'projectSetup' => array(
				'type' => 'segment',
				'options' => array(
					'route' => '/project_setup[/:action][/:param1][/:param2][/:param3]',
					'constraints' => array(
						'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
					),
					'defaults' => array(
						'controller' => 'Jobs\Controller\ProjectSetup',
						'action' => 'index',
					),
				),
			),
			'projectSetupApi' => array(
				'type' => 'segment',
				'options' => array(
					'route' => '/project_setup_api[/:action][/:param1][/:param2][/:param3]',
					'constraints' => array(
						'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
					),
					'defaults' => array(
						'controller' => 'Jobs\Controller\API\ProjectSetup',
						'action' => 'index',
					),
				),
			),
			'jobSetup' => array(
				'type' => 'segment',
				'options' => array(
					'route' => '/job_setup[/:action][/:param1][/:param2][/:param3]',
					'constraints' => array(
						'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
					),
					'defaults' => array(
						'controller' => 'Jobs\Controller\JobSetup',
						'action' => 'index',
					),
				),
			),
			'jobSetupApi' => array(
				'type' => 'segment',
				'options' => array(
					'route' => '/job_setup_api[/:action][/:param1][/:param2][/:param3]',
					'constraints' => array(
						'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
					),
					'defaults' => array(
						'controller' => 'Jobs\Controller\API\JobSetup',
						'action' => 'index',
					),
				),
			),
			'taskSetup' => array(
				'type' => 'segment',
				'options' => array(
					'route' => '/task_setup[/:action][/:param1][/:param2][/:param3]',
					'constraints' => array(
						'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
					),
					'defaults' => array(
						'controller' => 'Jobs\Controller\TaskSetup',
						'action' => 'index',
					),
				),
			),
			'taskSetupApi' => array(
				'type' => 'segment',
				'options' => array(
					'route' => '/task_setup_api[/:action][/:param1][/:param2][/:param3]',
					'constraints' => array(
						'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
					),
					'defaults' => array(
						'controller' => 'Jobs\Controller\API\TaskSetup',
						'action' => 'index',
					),
				),
			),
			'taskCard' => array(
				'type' => 'segment',
				'options' => array(
					'route' => '/task_card[/:action][/:param1][/:param2][/:param3]',
					'constraints' => array(
						'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
					),
					'defaults' => array(
						'controller' => 'Jobs\Controller\TaskCard',
						'action' => 'index',
					),
				),
			),
			'taskCardApi' => array(
				'type' => 'segment',
				'options' => array(
					'route' => '/task_card_api[/:action][/:param1][/:param2][/:param3]',
					'constraints' => array(
						'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
					),
					'defaults' => array(
						'controller' => 'Jobs\Controller\API\TaskCard',
					),
				),
			),
			'jobsReport' => array(
				'type' => 'segment',
				'options' => array(
					'route' => '/jobsReport[/:action][/:param1][/:param2][/:param3]',
					'constraints' => array(
						'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
					),
					'defaults' => array(
						'controller' => 'Jobs\Controller\JobsReport',
						'action' => 'create',
					),
				),
			),
			'vehicle' => array(
				'type' => 'segment',
				'options' => array(
					'route' => '/vehicle[/:action][/:param1][/:param2][/:param3]',
					'constraints' => array(
						'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
					),
					'defaults' => array(
						'controller' => 'Jobs\Controller\Vehicle',
						'action' => 'create',
					),
				),
			),
			'vehicleApi' => array(
				'type' => 'segment',
				'options' => array(
					'route' => '/api/vehicle[/:action][/:param1][/:param2][/:param3]',
					'constraints' => array(
						'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
					),
					'defaults' => array(
						'controller' => 'Jobs\Controller\API\Vehicle',
						'action' => 'index',
					),
				),
			),
			'designation' => array(
				'type' => 'segment',
				'options' => array(
					'route' => '/designations[/:action][/:param1][/:param2]',
					'constraints' => array(
						'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
					),
					'defaults' => array(
						'controller' => 'Jobs\Controller\Designation',
						'action' => 'create',
					),
				),
			),
			'designationApi' => array(
				'type' => 'segment',
				'options' => array(
					'route' => '/designation-api[/:action][/:param1][/:param2]',
					'constraints' => array(
						'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
						'param1' => '[a-zA-Z][a-zA-Z0-9_-]*',
						'param2' => '[a-zA-Z][a-zA-Z0-9_-]*',
					),
					'defaults' => array(
						'controller' => 'Jobs\Controller\API\Designation',
					),
				),
			),
			'contractor' => array(
				'type' => 'segment',
				'options' => array(
					'route' => '/contractor[/:action][/:param1][/:param2]',
					'constraints' => array(
						'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
					),
					'defaults' => array(
						'controller' => 'Jobs\Controller\Contractor',
						'action' => 'create',
					),
				),
			),
			'department' => array(
				'type' => 'segment',
				'options' => array(
					'route' => '/department[/:action][/:param1][/:param2]',
					'constraints' => array(
						'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
					),
					'defaults' => array(
						'controller' => 'Jobs\Controller\Department',
						'action' => 'create',
					),
				),
			),
			'departmentApi' => array(
				'type' => 'segment',
				'options' => array(
					'route' => '/department-api[/:action][/:param1][/:param2]',
					'constraints' => array(
						'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
						'param1' => '[a-zA-Z][a-zA-Z0-9_-]*',
						'param2' => '[a-zA-Z][a-zA-Z0-9_-]*',
					),
					'defaults' => array(
						'controller' => 'Jobs\Controller\API\Department',
					),
				),
			),
			'teams' => array(
				'type' => 'segment',
				'options' => array(
					'route' => '/teams[/:action][/:param1][/:param2]',
					'constraints' => array(
						'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
					),
					'defaults' => array(
						'controller' => 'Jobs\Controller\Team',
						'action' => 'create',
					),
				),
			),
			'employees' => array(
				'type' => 'segment',
				'options' => array(
					'route' => '/employees[/:action][/:param1][/:param2]',
					'constraints' => array(
						'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
					),
					'defaults' => array(
						'controller' => 'Jobs\Controller\Employee',
						'action' => 'create',
					),
				),
			),
			'employeeApi' => array(
				'type' => 'segment',
				'options' => array(
					'route' => '/employee-api[/:action][/:param1][/:param2]',
					'constraints' => array(
						'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
						'param1' => '[a-zA-Z][a-zA-Z0-9_-]*',
						'param2' => '[a-zA-Z][a-zA-Z0-9_-]*',
					),
					'defaults' => array(
						'controller' => 'Jobs\Controller\API\Employee',
					),
				),
			),
			'participantApi' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/participant-api[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param1' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param2' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Jobs\Controller\API\Participant',
                    ),
                ),
            ),
            'teamApi' => array(
				'type' => 'segment',
				'options' => array(
					'route' => '/team-api[/:action][/:param1][/:param2]',
					'constraints' => array(
						'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
						'param1' => '[a-zA-Z][a-zA-Z0-9_-]*',
						'param2' => '[a-zA-Z][a-zA-Z0-9_-]*',
					),
					'defaults' => array(
						'controller' => 'Jobs\Controller\API\Team',
					),
				),
			),
            'contractorapi' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/api/contractor[/:action][/:param1][/:param2]',
                     'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                    	'controller' => 'Jobs\Controller\API\Contractor',
                        'action' => 'create',
                    ),
                ),
            ),
            'progress' => array(
				'type' => 'segment',
				'options' => array(
					'route' => '/progress[/:action][/:param1][/:param2][/:param3]',
					'constraints' => array(
						'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
						'param1' => '[a-zA-Z][a-zA-Z0-9_-]*',
						'param2' => '[a-zA-Z][a-zA-Z0-9_-]*',
					),
					'defaults' => array(
						'controller' => 'Jobs\Controller\Progress',
					),
				),
			),
            'progressApi' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/progress-api[/:action][/:param1][/:param2][/:param3]',
                	'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                    	'controller' => 'Jobs\Controller\API\Progress',
                    ),
                ),
            ),
			'scoreUpdateApi' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/score-update-api[/:action][/:param1][/:param2][/:param3]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Jobs\Controller\API\ScoreUpdate',
                    ),
                ),
            ),
            'costType' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/cost-type[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                    	'controller' => 'Jobs\Controller\CostType',
                        'action' => 'index',
                    ),
                ),
            ),
            'costTypeApi' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/cost-type-api[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                    	'controller' => 'Jobs\Controller\API\CostType',
                        'action' => 'index',
                    ),
                ),
            ),
            'materialRequisition' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/material-requisition[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param1' => '[a-zA-Z0-9_-]*',
                        'param2' => '[a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Jobs\Controller\MaterialRequisition',
                        'action' => 'index'
                    ),
                ),
            ),
            'materialRequisitionApi' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/material-requisition-api[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param1' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param2' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                    	'controller' => 'Jobs\Controller\API\MaterialRequisition',
                    ),
                ),
            ),
            'resources' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/resources[/:action][/:param1][/:param2][/:param3]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Jobs\Controller\Resources',
                        'action' => 'index',
                    ),
                ),
            ),
            'resourcesApi' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/api/resources[/:action][/:param1][/:param2][/:param3]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Jobs\Controller\API\Resources',
                    ),
                ),
            ),
            'serviceSetup' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/serviceSetup[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Jobs\Controller\ServiceSetup',
                        'action' => 'create',
                    ),
                ),
            ),
            'serviceSetupApi' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/serviceSetup-api[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param1' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param2' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Jobs\Controller\API\ServiceSetup',
                    ),
                ),
            ),
            'departmentStation' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/departmentStation[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Jobs\Controller\DepartmentStation',
                        'action' => 'create',
                    ),
                ),
            ),
            'departmentStationApi' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/departmentStation-api[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param1' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param2' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Jobs\Controller\API\DepartmentStation',
                    ),
                ),
            ),
            'jobCardSetup' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/job-card-setup[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Jobs\Controller\JobCardSetup',
                        'action' => 'create',
                    ),
                ),
            ),
            'jobCardSetupApi' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/job-card-setup-api[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param1' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param2' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Jobs\Controller\API\JobCardSetup',
                    ),
                ),
            ),
            'serviceEmployee' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/service-employee[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Jobs\Controller\ServiceEmployee',
                        'action' => 'create',
                    ),
                ),
            ),
            'serviceEmployeeApi' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/service-employee-api[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param1' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param2' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Jobs\Controller\API\ServiceEmployee',
                    ),
                ),
            ),
            'rateCard' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/rate-card[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Jobs\Controller\RateCard',
                        'action' => 'create',
                    ),
                ),
            ),
            'rateCardApi' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/rate-card-api[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param1' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param2' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Jobs\Controller\API\RateCard',
                    ),
                ),
            ),
            'serviceJobs' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/service-job[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Jobs\Controller\ServiceJobs',
                        'action' => 'create',
                    ),
                ),
            ),
            'serviceJobsApi' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/service-job-api[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param1' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param2' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Jobs\Controller\API\ServiceJobs',
                    ),
                ),
            ),
            'incentives' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/incentives[/:action][/:param1][/:param2][/:param3]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Jobs\Controller\Incentives',
                        'action' => 'index',
                    ),
                ),
            ),
            'incentivesApi' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/api/incentives[/:action][/:param1][/:param2][/:param3]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Jobs\Controller\API\Incentives',

                    ),
                ),
            ),
            'serviceJobDashboard' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/service-job-dashboard[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Jobs\Controller\ServiceJobDashboard',
                        'action' => 'index',
                    ),
                ),
            ),
            'serviceJobDashboardApi' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/service-job-dashboard-api[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param1' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param2' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Jobs\Controller\API\ServiceJobDashboard',
                    ),
                ),
            ),
            'serviceProgressUpdate' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/service-progress-update[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Jobs\Controller\ServiceProgressUpdate',
                        'action' => 'index',
                    ),
                ),
            ),
            'serviceProgressUpdateApi' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/service-progress-update-api[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param1' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param2' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Jobs\Controller\API\ServiceProgressUpdate',
                    ),
                ),
            ),
            'employeeManagement' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/employee-management[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Jobs\Controller\EmployeeManagement',
                        'action' => 'index',
                    ),
                ),
            ),
            'employeeManagementApi' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/employee-management-api[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param1' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param2' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Jobs\Controller\API\EmployeeManagement',
                    ),
                ),
            ),
            'vehicleHistory' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/vehicle-history[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Jobs\Controller\vehicleHistory',
                        'action' => 'index',
                    ),
                ),
            ),
       	),
	),
    'translator' => array(
		'translation_file_patterns' => array(
			array(
				'type' => 'gettext',
				'base_dir' => __DIR__ . '/../language',
				'pattern' => '%s.mo',
			),
		),
	),
	'view_manager' => array(
		'template_path_stack' => array(
			'Jobs' => __DIR__ . '/../view',
		),
		'strategies' => array(
			'ViewJsonStrategy',
		),
	),
);
