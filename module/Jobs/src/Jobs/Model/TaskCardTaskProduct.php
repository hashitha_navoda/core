<?php

namespace Jobs\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class TaskCardTaskProduct
{

    public $taskCardTaskProductID;
    public $taskCardTaskID;
    public $locationProductID;
    public $taskCardTaskProductName;
    public $taskCardTaskProductType;
    public $taskCardTaskProductQty;
    public $taskCardTaskProductUomID;
    protected $inputFilter;
    
    public function exchangeArray($data)
    {
        $this->taskCardTaskProductID = (!empty($data['taskCardTaskProductID'])) ? $data['taskCardTaskProductID'] : null;
        $this->taskCardTaskID = (!empty($data['taskCardTaskID'])) ? $data['taskCardTaskID'] : null;
        $this->locationProductID = (!empty($data['locationProductID'])) ? $data['locationProductID'] : null;
        $this->taskCardTaskProductName = (!empty($data['taskCardTaskProductName'])) ? $data['taskCardTaskProductName'] : null;
        $this->taskCardTaskProductType = (!empty($data['taskCardTaskProductType'])) ? $data['taskCardTaskProductType'] : null;
        $this->taskCardTaskProductQty = (!empty($data['taskCardTaskProductQty'])) ? $data['taskCardTaskProductQty'] : null;
        $this->taskCardTaskProductUomID = (!empty($data['taskCardTaskProductUomID'])) ? $data['taskCardTaskProductUomID'] : null;
        
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {
        
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

}