<?php

namespace Jobs\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Jobs\Model\rateCardTask;
use Zend\Db\Sql\Sql;
use Core\Model\ProductTable as CoreProductTable;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Predicate\PredicateSet;
use Zend\Db\Sql\Predicate\Between;
use Zend\Db\Sql\Predicate\Operator;

class RateCardTaskSubTaskTable extends CoreProductTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    /**
     * save rate card tasks sub task
     * @param array $rateCardTaskSubTskData
     * @return mixed
     */
    public function saveRateCardTaskSubTask($rateCardTaskSubTskData)
    {
        $rateCardTaskSubTaskData = array(
            'serviceInceniveRateId' => $rateCardTaskSubTskData->serviceInceniveRateId,
            'subTaskID' => $rateCardTaskSubTskData->subTaskID,
            'rateCardTaskSubTaskRate' => $rateCardTaskSubTskData->rateCardTaskSubTaskRate,
            'uomID' => $rateCardTaskSubTskData->uomID,
        );
        if ($this->tableGateway->insert($rateCardTaskSubTaskData)) {
            $result = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
            return $result;
        } else {
            return FALSE;
        }
    }

    /**
     * fetch rate card related task sub task rate  by rate card task id
     * @param int $rateCardTaskID
     * @return mixed
     */
    public function getRelatedServiceSubtasksRates($serviceInceniveRateId)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('rateCardTaskSubTasks');
        $select->join('serviceSubTask', 'rateCardTaskSubTasks.subTaskId=  serviceSubTask.subTaskId', array("*"), "left");
        $select->where(array('rateCardTaskSubTasks.serviceInceniveRateId' => $serviceInceniveRateId, 'isDeleted' => 0));
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if (!$rowset) {
            return NULL;
        }
        return $rowset;
    }

    //update rate card task sub task as deleted by rate card task id
    public function updateDeleteRateCardTaskSubTask($serviceInceniveRateId)
    {
        $rateCardTaskSubTaskData = array(
            'isDeleted' => '1',
        );
        try {
            $this->tableGateway->update($rateCardTaskSubTaskData, array('serviceInceniveRateId' => $serviceInceniveRateId));
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
        return true;
    }

}
