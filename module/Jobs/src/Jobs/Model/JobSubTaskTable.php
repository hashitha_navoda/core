<?php

namespace Jobs\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Jobs\Model\JobTaskContractor;
use Zend\Db\Sql\Sql;
use Core\Model\ProductTable as CoreProductTable;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Predicate\PredicateSet;
use Zend\Db\Sql\Predicate\Between;
use Zend\Db\Sql\Predicate\Operator;

class JobSubTaskTable extends CoreProductTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function saveJobSubTask($jobSubTaskData) {
		$jobSubTaskSaveData = array(
			'subTaskId' => $jobSubTaskData->subTaskId,
			'jobTaskId' => $jobSubTaskData->jobTaskId,
			'isDeleted' => 0
		);
		if ($this->tableGateway->insert($jobSubTaskSaveData)) {
			$result = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
			return $result;
		} else {
			return FALSE;
		}
	}


	/**
	 * get sub tasks by id
	 * @param type $jobTaskID
	 * @param type $subTaskID
	 * @return type
	 */
	public function getSubTaskById($jobTaskID, $subTaskID) {
		$adapter = $this->tableGateway->getAdapter();
		$sql = new Sql($adapter);
		$select = $sql->select();
		$select->from('jobSubTask')
			->columns(array('*'));
		$select->where(['jobSubTask.jobTaskId' => $jobTaskID, 'jobSubTask.subTaskId' => $subTaskID]);

		$statement = $sql->prepareStatementForSqlObject($select);
		$rowset = $statement->execute();
		if (!$rowset) {
			return NULL;
		}
		return $rowset;
	}

	/**
     * delete job sub task by job task id
     * @param int $jobTaskId
     * @return mixed
     */
    public function deleteJobSubTaskJobTaskID($jobTaskId)
    {
        $jobSubTaskData = array(
            'isDeleted' => 1,
        );
        try {
            $this->tableGateway->update($jobSubTaskData, array('jobTaskId' => $jobTaskId));
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
        return true;
    }

    public function checkSubTaskUsedInJob($subTaskID) 
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('jobSubTask')
                ->columns(array('*'))
                ->where(array('isDeleted' => 0, 'subTaskId' => $subTaskID));
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if ($rowset->count() > 0) {
            return false;
        }
        return true;
    }

    /**
     * get sub tasks by id
     * @param type $jobTaskID
     * @param type $subTaskID
     * @return type
     */
    public function getSubTaskByJobTaskId($jobTaskID) {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('jobSubTask')
            ->columns(array('*'));
        $select->join('serviceSubTask', 'jobSubTask.subTaskId = serviceSubTask.subTaskId', array('*'), 'left');
        $select->where(['jobSubTask.jobTaskId' => $jobTaskID]);

        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if (!$rowset) {
            return NULL;
        }
        return $rowset;
    }
}
