<?php

namespace Jobs\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class VehicleModel
{

    public $vehicleModelID;
    public $vehicleModelName;
    public $vehicleTypeID;
    public $vehicleTypeDelete;
    
    protected $inputFilter;
    
    public function exchangeArray($data)
    {
        $this->vehicleModelID = (!empty($data['vehicleModelID'])) ? $data['vehicleModelID'] : null;
        $this->vehicleModelName = (!empty($data['vehicleModelName'])) ? $data['vehicleModelName'] : null;
        $this->vehicleTypeID = (!empty($data['vehicleTypeID'])) ? $data['vehicleTypeID'] : null;
        $this->vehicleTypeDelete = (!empty($data['vehicleTypeDelete'])) ? $data['vehicleTypeDelete'] : '0';
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {
        
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

}