<?php

namespace Jobs\Model;

use Zend\Db\TableGateway\TableGateway;

class JobManagerTable {

	protected $tableGateway;

	public function __construct(TableGateway $tableGateway) {
		$this->tableGateway = $tableGateway;
	}

	public function saveManager($jobManagerData) {
		$jobManagerSaveData = array(
			'employeeID' => $jobManagerData->employeeID,
			'jobID' => $jobManagerData->jobID,
		);
		if ($this->tableGateway->insert($jobManagerSaveData)) {
			$result = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue('jobManagerID');
			return $result;
		} else {
			return FALSE;
		}
	}

	public function deleteJobManagerByJobID($jobID) {
		try {
			$result = $this->tableGateway->delete(array('jobID' => $jobID));
			return $result;
		} catch (\Exception $e) {
			$error = 0;
			return $error;
		}
	}
}
