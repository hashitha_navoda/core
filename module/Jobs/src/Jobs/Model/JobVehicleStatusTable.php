<?php

namespace Jobs\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Jobs\Model\JobVehicle;
use Zend\Db\Sql\Sql;
use Core\Model\ProductTable as CoreProductTable;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Predicate\PredicateSet;
use Zend\Db\Sql\Predicate\Between;
use Zend\Db\Sql\Predicate\Operator;

class JobVehicleStatusTable extends CoreProductTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }


    public function saveJobVehicleStatus($jobVehicleData) {
		$jobVehicleSaveData = array(
			'jobVehicleID' => $jobVehicleData->jobVehicleID,
			'airFilter' => $jobVehicleData->airFilter,
			'engineOil' => $jobVehicleData->engineOil,
            'wiperFluid' => $jobVehicleData->wiperFluid,
            'wiperBlades' => $jobVehicleData->wiperBlades,
            'tyres' => $jobVehicleData->tyres,
            'checkBelts' => $jobVehicleData->checkBelts,
            'coolant' => $jobVehicleData->coolant,
            'brakeFluid' => $jobVehicleData->brakeFluid,
            'steeringFluid' => $jobVehicleData->steeringFluid,
            'transmissionFluid' => $jobVehicleData->transmissionFluid,
            'frontLeftTyrePressure' => $jobVehicleData->frontLeftTyrePressure,
            'frontRightTyrePressure' => $jobVehicleData->frontRightTyrePressure,
            'rearLeftTyrePressure' => $jobVehicleData->rearLeftTyrePressure,
            'rearRightTyrePressure' => $jobVehicleData->rearRightTyrePressure,
            'battery' => $jobVehicleData->battery,
            'odoMeter' => $jobVehicleData->odoMeter,
            'brakePadsFrontfactor' => $jobVehicleData->brakePadsFrontfactor,
            'brakePadsRearfactor' => $jobVehicleData->brakePadsRearfactor,
            'brakePadsChangeFluid' => $jobVehicleData->brakePadsChangeFluid,
            'oilFilter' => $jobVehicleData->oilFilter,
            'gearBoxOil' => $jobVehicleData->gearBoxOil,
            'differentialOil' => $jobVehicleData->differentialOil,
            'acCabinFilter' => $jobVehicleData->acCabinFilter,
            'fuelFilter' => $jobVehicleData->fuelFilter,
            'radiatorCoolantLevel' => $jobVehicleData->radiatorCoolantLevel,
            'powerSteeringOil' => $jobVehicleData->powerSteeringOil,
            'radiatorHosesdriveBelts' => $jobVehicleData->radiatorHoses,
            'driveBelts' => $jobVehicleData->driveBelts,
            'engineMount' => $jobVehicleData->engineMount,
            'shockMounts' => $jobVehicleData->shockMounts,
            'silencerMounts' => $jobVehicleData->silencerMounts,
            'axleBoots' => $jobVehicleData->axleBoots,
            'headLamps' => $jobVehicleData->headLamps,
            'signalLamps' => $jobVehicleData->signalLamps,
            'parkingLamps' => $jobVehicleData->parkingLamps,
            'fogLamps' => $jobVehicleData->fogLamps,
            'tailLamp' => $jobVehicleData->tailLamp,
            'roomLamp' => $jobVehicleData->roomLamp,
            'wScreenWasherFluid' => $jobVehicleData->wScreenWasherFluid,
            'rackBoots' => $jobVehicleData->rackBoots,
            'brakePadsLiners' => $jobVehicleData->brakePadsLiners,
            'tyreWareUneven' => $jobVehicleData->tyreWareUneven,
            'wheelNutsStuds' => $jobVehicleData->wheelNutsStuds,
            'entityID' => $jobVehicleData->entityID
		);
		if ($this->tableGateway->insert($jobVehicleSaveData)) {
			$result = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
			return $result;
		} else {
			return FALSE;
		}
	}

	/**
     * get job vehicle by job id
     * @param int $jobId
     * @param int $vehicleId
     * @return mixed
     */
    public function getJobVehicleStatusByJobID($jobID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('jobVehicleStatus');
        $select->columns(array("*"));
        $select->join('entity', 'jobVehicleStatus.entityID = entity.entityID', array('*'), 'left');
        $select->join('jobVehicle', 'jobVehicleStatus.jobVehicleID = jobVehicle.jobVehicleID', array('jobId'), 'left');
        $select->where(array('jobVehicle.jobId' => $jobID, 'entity.deleted'=> 0));
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if (!$rowset) {
            return NULL;
        }
        return $rowset;
    }
}
