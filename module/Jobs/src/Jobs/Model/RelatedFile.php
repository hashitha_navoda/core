<?php

namespace Jobs\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class RelatedFile
{

    public $relatedFileID;
    public $relatedFilePath;
    public $documentTypeID;
    public $documentID;
    protected $inputFilter;
    
    public function exchangeArray($data)
    {
        $this->relatedFileID = (!empty($data['relatedFileID'])) ? $data['relatedFileID'] : null;
        $this->relatedFilePath = (!empty($data['relatedFilePath'])) ? $data['relatedFilePath'] : null;
        $this->documentTypeID = (!empty($data['documentTypeID'])) ? $data['documentTypeID'] : null;
        $this->documentID = (!empty($data['documentID'])) ? $data['documentID'] : null;
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {
        
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

}
