<?php

namespace Jobs\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class RateCardTask
{

    public $rateCardTaskID;
    public $rateCardID;
    public $taskID;
    public $rateCardTaskRatePerUnit;
    public $uomID;
    protected $inputFilter;
    
    public function exchangeArray($data)
    {
        $this->rateCardTaskID = (!empty($data['rateCardTaskID'])) ? $data['rateCardTaskID'] : null;
        $this->rateCardID = (!empty($data['rateCardID'])) ? $data['rateCardID'] : null;
        $this->taskID = (!empty($data['taskID'])) ? $data['taskID'] : null;
        $this->rateCardTaskRatePerUnit = (!empty($data['rateCardTaskRatePerUnit'])) ? $data['rateCardTaskRatePerUnit'] : null;
        $this->uomID = (!empty($data['uomID'])) ? $data['uomID'] : null;
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {
        
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

}