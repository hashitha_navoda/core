<?php

namespace Jobs\Model;

use Zend\Db\Sql\Sql;
use Zend\Db\TableGateway\TableGateway;
use Zend\Stdlib\ArrayUtils;

class JobSupervisorTable {

	protected $tableGateway;

	public function __construct(TableGateway $tableGateway) {
		$this->tableGateway = $tableGateway;
	}

	public function saveSupervisor($jobSupervisorData) {
		$jobSuperviosrSaveData = array(
			'jobSupervisorID' => $jobSupervisorData->jobSupervisorID,
			'jobID' => $jobSupervisorData->jobID,
			'participantTypeID' => $jobSupervisorData->participantTypeID,
			'participantSubTypeID' => $jobSupervisorData->participantSubTypeID,
			'teamID' => $jobSupervisorData->teamID,
		);
		if ($this->tableGateway->insert($jobSuperviosrSaveData)) {
			$result = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue('jobSupervisorTableID');
			return $result;
		} else {
			return FALSE;
		}
	}

	
	public function getJobSupervisorName($ID) {
		$sql = new Sql($this->tableGateway->getAdapter());
		$select = $sql->select();
		$select->from('jobSupervisor')
			->columns(array('*'))
			->join('employee', 'jobSupervisor.jobSupervisorID=employee.employeeID', array("employeeFirstName", "employeeSecondName"), "left")
			->where(array('jobSupervisor.referenceCode' => $ID));
		$statement = $sql->prepareStatementForSqlObject($select);
		$rowset = $statement->execute();
		if (!$rowset) {
			return NULL;
		}
		return $rowset;
	}

//this function get job supervisor details that related to the employee ID
	public function getJobSupervisorDetailsByEmployeeID($ID) {
		$sql = new Sql($this->tableGateway->getAdapter());
		$select = $sql->select();
		$select->from('jobSupervisor')
			->columns(array('*'))
			->where(array('jobSupervisor.jobSupervisorID' => $ID));
		$statement = $sql->prepareStatementForSqlObject($select);
		$rowset = $statement->execute();
		if (!$rowset) {
			return NULL;
		}
		return $rowset;
	}

	//this function use to get job Supervisors details that related to the job reference
	public function getJobSupervisorsDetailsByJobID($jobID) {
		$sql = new Sql($this->tableGateway->getAdapter());
		$select = $sql->select();
		$select->from('jobSupervisor')
			->columns(array('*'))
			->join('employee', 'jobSupervisor.jobSupervisorID =  employee.employeeID', array("employeeFirstName", "employeeSecondName"), "left")
			->where(array('jobSupervisor.jobID' => $jobID));
		$statement = $sql->prepareStatementForSqlObject($select);
		$rowset = $statement->execute();

		if (!$rowset) {
			return NULL;
		}
		return ArrayUtils::iteratorToArray($rowset);
	}

	public function deleteJobSupervisorByJobID($jobID) {
		try {
			$result = $this->tableGateway->delete(array('jobID' => $jobID));
			return $result;
		} catch (\Exception $e) {
			$error = 0;
			return $error;
		}
	}

	 public function getTournamentOfficialsByJobID($id)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('jobSupervisor')
                ->columns(array('*'))
                ->join('employee', 'jobSupervisor.jobSupervisorID = employee.employeeID', array('*'), 'left')
                ->join('department', 'jobSupervisor.participantTypeID = department.departmentID', array('departmentName','departmentCode'), 'left')
                ->join('designation', 'jobSupervisor.participantSubTypeID = designation.designationID', array('designationCode','designationName'), 'left')
                ->where(array('jobSupervisor.jobID' => $id, 'employee.employeeStatus' => true));
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if (!$rowset) {
            return NULL;
        }
        return $rowset;
    }
}
