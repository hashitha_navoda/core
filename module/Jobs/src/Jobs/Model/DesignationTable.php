<?php

namespace Jobs\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Predicate\PredicateSet;
use Zend\Db\Sql\Predicate\Operator;
use Jobs\Model\Designation;
use Core\Model\ProductTable as CoreProductTable;

class DesignationTable extends CoreProductTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    /**
     * fetch all designations 
     * @param boolean $paginated
     * @return mixed
     */
    public function fetchAll($paginated = FALSE)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('designation');
        $select->order('designationID DESC');
        $select->join('entity', 'designation.entityID = entity.entityID', array('deleted'));
        $select->where(array('deleted' => '0'));
        if ($paginated) {
            $paginatorAdapter = new DbSelect(
                    $select, $this->tableGateway->getAdapter());
            $paginator = new Paginator($paginatorAdapter);
            return $paginator;
        } else {
            $statement = $sql->prepareStatementForSqlObject($select);
            $result = $statement->execute();
            return $result;
        }
    }

    /**
     * fetch designation that match for given designation code 
     * @param string $designationCode
     * @return mixed
     */
    public function getDesignations($designationCode)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('designation')
                ->columns(array('designationCode'))
                ->join('entity', 'designation.entityID=  entity.entityID', array("*"), "left")
                ->where(array('designation.designationCode' => $designationCode, "entity.deleted" => 0));
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if (!$rowset) {
            return NULL;
        }
        return $rowset;
    }

    /**
     * save designation
     * @param array $designationData
     * @return mixed
     */
    public function saveDesignations($designationData)
    {
        $DesignationData = array(
            'designationCode' => $designationData->designationCode,
            'designationName' => $designationData->designationName,
            'entityID' => $designationData->entityID,
            'designationStatus' => '1',
        );
        if ($this->tableGateway->insert($DesignationData)) {
            $result = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
            return $result;
        } else {
            return FALSE;
        }
    }

    /**
     * update designation
     * @param array $designationData
     * @return mixed
     */
    public function updateDesignation($designationData)
    {
        $DestinationData = array(
            'designationName' => $designationData->designationName,
            'designationCode' => $designationData->designationCode,
        );
        try {
            $this->tableGateway->update($DestinationData, array('designationID' => $designationData->designationID));
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
        return true;
    }

    /**
     * search designation by search key
     * @param string $keyword
     * @return mixed
     */
    public function designationSearchByKey($keyword)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('designation')
                ->columns(array('*'))
                ->join('entity', 'designation.entityID = entity.entityID', array('*'), 'left');
        $select->where(new PredicateSet(array(new Operator('designationName', 'like', '%' . $keyword . '%'), new Operator('designationCode', 'like', '%' . $keyword . '%')), PredicateSet::OP_OR));
        $select->where(new PredicateSet(array(new Operator('entity.deleted', '=', '0'))));

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return (object) $results;
    }

    public function getDesignationByDesignationID($designationID) {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('designation')
            ->columns(array('*'))
            ->where(array('designation.designationID' => $designationID));
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if (!$rowset) {
            return NULL;
        }
        return $rowset;
    }

    /**
     * update designation status
     * @param int $designationID
     * @param boolean $status
     * @return mixed
     */
    public function updateDesignationState($designationID, $status)
    {
        $designationData = array(
            'designationStatus' => $status,
        );
        try {
            $this->tableGateway->update($designationData, array('designationID' => $designationID));
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
        return true;
    }

    /**
     * Return designation according to the given designation code and not equal 
     * to the given designation id
     * @param string $designationId
     * @param string $designationCode
     * @return mixed
     */
    public function checkDesignationCodeValid($designationId,$designationCode) 
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('designation')
                ->columns(array('*'))
                ->join('entity', 'designation.entityID = entity.entityID', array('deleted'))
                ->where(array('entity.deleted' => '0', 'designation.designationCode' => $designationCode))
                ->where->notEqualTo('designation.designationID',$designationId);
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result->count();
    }

    /**
     * fetch all active designations 
     * @return mixed
     */
    public function fetchActiveDesignations()
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('designation');
        $select->order('designationID DESC');
        $select->join('entity', 'designation.entityID = entity.entityID', array('deleted'));
        $select->where(array('entity.deleted' => '0', 'designation.designationStatus' => 1));
        
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }

}
