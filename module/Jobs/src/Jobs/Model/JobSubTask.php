<?php

namespace Jobs\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class JobSubTask
{

    public $jobSubTaskID;
    public $jobTaskId;
    public $subTaskId;
    protected $inputFilter;
    
    public function exchangeArray($data)
    {
        $this->jobSubTaskID = (!empty($data['jobSubTaskID'])) ? $data['jobSubTaskID'] : null;
        $this->jobTaskId = (!empty($data['jobTaskId'])) ? $data['jobTaskId'] : null;
        $this->subTaskId = (!empty($data['subTaskId'])) ? $data['subTaskId'] : null;
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {
        
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

}