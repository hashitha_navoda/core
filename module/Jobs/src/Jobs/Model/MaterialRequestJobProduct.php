<?php

namespace Jobs\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class MaterialRequestJobProduct
{

    public $materialRequestJobProductId;
    public $materialRequestId;
    public $jobProductId;
    public $requestedQuantity;
    public $issuedQty;

    public function exchangeArray($data)
    {
        $this->materialRequestJobProductId = (!empty($data['materialRequestJobProductId'])) ? $data['materialRequestJobProductId'] : null;
        $this->materialRequestId = (!empty($data['materialRequestId'])) ? $data['materialRequestId'] : null;
        $this->jobProductId = (!empty($data['jobProductId'])) ? $data['jobProductId'] : null;
        $this->requestedQuantity = (!empty($data['requestedQuantity'])) ? $data['requestedQuantity'] : null;
        $this->issuedQty = (!empty($data['issuedQty'])) ? $data['issuedQty'] : null;
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {

    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

}
