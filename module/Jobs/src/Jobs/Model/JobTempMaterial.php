<?php

namespace Jobs\Model;

use Zend\InputFilter\InputFilterInterface;

class JobTempMaterial {

	public $jobTempMaterialID;
	public $jobID;
	public $locationProductID;
	public $productID;
	public $issueQty;
	public $unitPrice;
	public $productUomID;
	public $productDescription;
	protected $inputFilter;

	public function exchangeArray($data) {
		$this->jobTempMaterialID = (!empty($data['jobTempMaterialID'])) ? $data['jobTempMaterialID'] : null;
		$this->jobID = (!empty($data['jobID'])) ? $data['jobID'] : null;
		$this->locationProductID = (!empty($data['locationProductID'])) ? $data['locationProductID'] : null;
		$this->productID = (!empty($data['productID'])) ? $data['productID'] : null;
		$this->issueQty = (!empty($data['issueQty'])) ? $data['issueQty'] : null;
		$this->unitPrice = (!empty($data['unitPrice'])) ? $data['unitPrice'] : null;
		$this->productUomID = (!empty($data['productUomID'])) ? $data['productUomID'] : null;
		$this->productDescription = (!empty($data['productDescription'])) ? $data['productDescription'] : null;
	}

	public function setInputFilter(InputFilterInterface $inputFilter) {
		throw new \Exception("Not used");
	}

	public function getInputFilter() {

	}

	public function getArrayCopy() {
		return get_object_vars($this);
	}

}