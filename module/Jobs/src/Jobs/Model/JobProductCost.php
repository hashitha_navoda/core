<?php

namespace Jobs\Model;

use Zend\InputFilter\InputFilterInterface;

class JobProductCost {

	public $jobProductCostID;
	public $jobProductID;
	public $productCost;
	public $Qty;
	public $jobCostID;
	protected $inputFilter;

	public function exchangeArray($data) {
		$this->jobProductCostID = (!empty($data['jobProductCostID'])) ? $data['jobProductCostID'] : null;
		$this->jobProductID = (!empty($data['jobProductID'])) ? $data['jobProductID'] : null;
		$this->productCost = (!empty($data['productCost'])) ? $data['productCost'] : null;
		$this->Qty = (!empty($data['Qty'])) ? $data['Qty'] : null;
		$this->jobCostID = (!empty($data['jobCostID'])) ? $data['jobCostID'] : null;
	}

	public function setInputFilter(InputFilterInterface $inputFilter) {
		throw new \Exception("Not used");
	}

	public function getInputFilter() {

	}

	public function getArrayCopy() {
		return get_object_vars($this);
	}

}