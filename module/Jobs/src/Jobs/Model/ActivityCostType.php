<?php

/**
 * @author Sandun <sandun@thinkcube.com>
 */

namespace Jobs\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class ActivityCostType
{

    public $activityCostTypeID;
    public $activityId;
    public $costTypeId;
    public $activityCostTypeEstimatedCost;
    public $activityCostTypeActualCost;
    protected $inputFilter;

    public function exchangeArray($data)
    {
        $this->activityCostTypeID = (!empty($data['activityCostTypeID'])) ? $data['activityCostTypeID'] : null;
        $this->activityId = (!empty($data['activityId'])) ? $data['activityId'] : null;
        $this->costTypeId = (!empty($data['costTypeId'])) ? $data['costTypeId'] : null;
        $this->activityCostTypeEstimatedCost = (!empty($data['activityCostTypeEstimatedCost'])) ? $data['activityCostTypeEstimatedCost'] : 0.00;
        $this->activityCostTypeActualCost = (!empty($data['activityCostTypeActualCost'])) ? $data['activityCostTypeActualCost'] : 0.00;
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

}
