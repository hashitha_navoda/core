<?php

namespace Jobs\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Jobs\Model\JobProductCost;
use Zend\Db\Sql\Sql;
use Core\Model\ProductTable as CoreProductTable;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Predicate\PredicateSet;
use Zend\Db\Sql\Predicate\Between;
use Zend\Db\Sql\Predicate\Operator;
use Zend\Db\Sql\Select;

class JobProductCostTable extends CoreProductTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function save(JobProductCost $jobProductCost)
    {
    	$saveData = [
    		'jobProductID' => $jobProductCost->jobProductID,
    		'productCost' => $jobProductCost->productCost,
    		'Qty' => $jobProductCost->Qty,
            'jobCostID' => $jobProductCost->jobCostID
    	];

    	try {
    		$this->tableGateway->insert($saveData);
    		return true;
			
    	} catch (Exception $e) {
    		return false;
    		error_log($e->getMessage());
    	}
    }

    /**
    * this function use to get products that related to the given jobProductCostID
    * @param int $jobCostID
    * return mix
    **/
    public function getJobProductByJobCostID($jobCostID = null, $salesInvoiceId = null)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = new Select();
        $select->from("jobProductCost")
                ->columns(array("*"))
                ->join('jobProduct', 'jobProductCost.jobProductID = jobProduct.jobProductID', array('*'), 'left')
                ->join('locationProduct', 'jobProduct.locationProductID = locationProduct.locationProductID', array('productID','locationProductQuantity'), 'left')
                ->join('product', 'locationProduct.productID = product.productID', array('*'), 'left');
        if (!is_null($jobCostID)) {
            $select->where(array('jobProductCost.jobCostID' => $jobCostID));
        }
        
        if (!is_null($salesInvoiceId)) {
            $select->join('jobCost','jobCost.jobCostID = jobProductCost.jobCostID',array('salesInvocieID'),'left');
            $select->join('invoiceProductDeliveryNoteProduct','invoiceProductDeliveryNoteProduct.jobCostID = jobCost.jobCostID',array('*'),'left');
            $select->where(array('jobCost.salesInvocieID' => $salesInvoiceId));
        }

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        if (!$results) {
            return null;
        }
        return $results;
    }
    

    public function updateJobProductCost($jobProductCostData,$jobProductCostID)
    {
        try {
            $this->tableGateway->update($jobProductCostData, array('jobProductCostID' => $jobProductCostID));
            return true;
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
            return false;
        }
    }

    public function getJobProductCostByJobProductCostID($jobProductCostID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('jobProductCost')
                ->columns(array("*"));
        $select->where->equalTo('jobProductCostID',$jobProductCostID);        
        $query = $sql->prepareStatementForSqlObject($select);
        $rowset = $query->execute();
        return $rowset->current();
    }

    public function getJobProductDeliveryNoteDataByInvoiceId($salesInvoiceId)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = new Select();
        $select->from("jobCost")
                ->columns(array("*"));
        $select->join('invoiceProductDeliveryNoteProduct','invoiceProductDeliveryNoteProduct.jobCostID = jobCost.jobCostID',array('*'),'left');
        $select->where(array('jobCost.salesInvocieID' => $salesInvoiceId));

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        if (!$results) {
            return null;
        }
        return $results;
    }
}
