<?php

namespace Jobs\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Predicate\PredicateSet;
use Zend\Db\Sql\Predicate\Operator;

class VehicleStatusCheckTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll()
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('vehicleStatusCheck');
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();

        $data = [];
        foreach ($result as $value) {
            $data[$value['status']] = $value['vehicleStatusCheckID'];
        }
        return $data;
    }

    public function fetchAllByID()
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('vehicleStatusCheck');
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();

        $data = [];
        foreach ($result as $value) {
            $data[$value['vehicleStatusCheckID']] = $value['status'];
        }
        return $data;
    }
}
