<?php

namespace Jobs\Model;

use Core\Model\ProductTable as CoreProductTable;
use Jobs\Model\EmployeeDesignation;
use Zend\Db\Sql\Predicate\Operator;
use Zend\Db\Sql\Predicate\PredicateSet;
use Zend\Db\Sql\Sql;
use Zend\Db\TableGateway\TableGateway;

class EmployeeDesignationTable extends CoreProductTable {

	protected $tableGateway;

	public function __construct(TableGateway $tableGateway) {
		$this->tableGateway = $tableGateway;
	}

	/**
	 * save employeeDesignation record
	 * @param array $employeeDesignationData
	 * @return mixed
	 */
	public function saveEmployeeDesignation($employeeDesignationData) {
		$EmployeeDesignationData = array(
			'employeeID' => $employeeDesignationData->employeeID,
			'designationID' => $employeeDesignationData->designationID,
		);
		if ($this->tableGateway->insert($EmployeeDesignationData)) {
			$result = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
			return true;
		} else {
			return FALSE;
		}
	}

	/**
	 * fetch employees that match for given employeeID
	 * @param string $departmentCodegetEmployeeDesignations
	 * @return mixed
	 */
	public function getEmployeeDesignations($id) {
		$sql = new Sql($this->tableGateway->getAdapter());
		$select = $sql->select();
		$select->from('employeeDesignation')
			->columns(array('*'))
			->join('designation', 'employeeDesignation.designationID = designation.designationID', array('*'), 'left')
			->where(array('employeeID' => $id, 'employeeDesignation.isDeleted' => false, 'designation.designationStatus' => true));
		$statement = $sql->prepareStatementForSqlObject($select);
		$rowset = $statement->execute();
		if (!$rowset) {
			return NULL;
		}
		return $rowset;
	}

	/**
	 * delete employeeDesignation that match for given employeeDesignationID
	 * @param string $employeeDesignationID
	 * @return mixed
	 */
	public function deleteEmployeesDesignationByemployeeDesignationID($employeeDesignationID) {
		$employeeDesignationData = array(
			'isDeleted' => true,
		);
		try {
			$this->tableGateway->update($employeeDesignationData, array('employeeDesignationID' => $employeeDesignationID));
		} catch (Exception $exc) {
			echo $exc->getTraceAsString();
		}
		return true;
	}

	public function checkDesignationUsed($designationId) {
		$sql = new Sql($this->tableGateway->getAdapter());
		$select = $sql->select();
		$select->from('employeeDesignation')
			->columns(array('*'))
			->join('employee', 'employeeDesignation.employeeID = employee.employeeID', array('*'), 'left')
			->join('entity', 'employee.entityID = entity.entityID', array('*'), 'left')
			->where(array('employeeDesignation.designationID' => $designationId, 'entity.deleted' => 0));
		$statement = $sql->prepareStatementForSqlObject($select);
		$rowset = $statement->execute();

		if ($rowset->count() > 0) {
			return false;
		}
		return true;

	}

	public function getDesignationWiseEmployeeBySearchKey($key = null) {
		$adapter = $this->tableGateway->getAdapter();
		$sql = new Sql($adapter);
		$select = $sql->select();
		$select->from('employeeDesignation')
			->columns(array('*'))
			->join('employee', 'employee.employeeID = employeeDesignation.employeeID', array('*'), 'left')
			->join('designation', 'employeeDesignation.designationID = designation.designationID', array('*'), 'left')
			->join('entity', 'employee.entityID = entity.entityID', array('*'), 'left');
		if (!is_null($key)) {
			$select->where(new PredicateSet(array(new Operator('employee.employeeFirstName', 'like', '%' . $key . '%'), new Operator('employee.employeeSecondName', 'like', '%' . $key . '%'), new Operator('employee.employeeCode', 'like', '%' . $key . '%'), new Operator('designation.designationName', 'like', '%' . $key . '%')), PredicateSet::OP_OR));
		}
        $select->where(new PredicateSet(array(new Operator('entity.deleted', '=', '0'))));
        $select->where->notEqualTo('designation.designationCode','DES001');
        $select->where->notEqualTo('designation.designationCode','DES002');
        $select->where->notEqualTo('designation.designationCode','DES003');
        $select->where->notEqualTo('designation.designationCode','DES004');
        $select->where->notEqualTo('employee.employeeStatus','0');
        $select->group(array('designation.designationID'));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return (object) $results;
    }

    public function fetchEmployeeByDesignationCode($designationCode)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('employeeDesignation')
                ->columns(array('*'))
                ->join('designation', 'employeeDesignation.designationID = designation.designationID', array('*'), 'left')
                ->join('employee', 'employeeDesignation.employeeID = employee.employeeID', array('*'), 'left')
                ->join('entity', 'employee.entityID = entity.entityID', array('*'), 'left')
                ->where(array('designation.designationCode' => $designationCode, 'employeeDesignation.isDeleted' => false, 'employee.employeeStatus' => true, 'entity.deleted' => 0));
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if (!$rowset) {
            return NULL;
        }

        return $rowset;

    }

    /**
     * update employee with updated departmentID
     * @param int $oldId
     * @param int $newId
     * @return mixed
     */
    public function updateEmployeDesignationWithNewId($oldId, $newId)
    {
        $EmployeesData = array(
            'designationID' => $newId,
        );
        try {
            $this->tableGateway->update($EmployeesData, array('designationID' => $oldId));
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
        return true;
    }

    /**
     * fetch inactive employees that match for given projectID
     * @param string $id
     * @return mixed
     */
    public function getInactiveEmployeeDesignations($id)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('employeeDesignation')
                ->columns(array('*'))
                ->join('designation', 'employeeDesignation.designationID = designation.designationID', array('*'), 'left')
                ->join('entity', 'designation.entityID = entity.entityID', array('*'), 'left')
                ->where(array('employeeDesignation.employeeID' => $id, 'designation.designationStatus' => false, 'entity.deleted' => 0));
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();

        if (!$rowset) {
            return NULL;
        }
        return $rowset;
    }
}
