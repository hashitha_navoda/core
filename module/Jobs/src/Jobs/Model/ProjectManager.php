<?php

namespace Jobs\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class ProjectManager
{

    public $projectManagerID;
    public $employeeID;
    public $projectID;
    public $participantTypeID;
    public $participantSubTypeID;
    public $teamID;
    protected $inputFilter;
    
    public function exchangeArray($data)
    {
        $this->projectManagerID = (!empty($data['projectManagerID'])) ? $data['projectManagerID'] : null;
        $this->employeeID = (!empty($data['employeeID'])) ? $data['employeeID'] : null;
        $this->projectID = (!empty($data['projectID'])) ? $data['projectID'] : null;
        $this->participantTypeID = (!empty($data['participantTypeID'])) ? $data['participantTypeID'] : null;
        $this->participantSubTypeID = (!empty($data['participantSubTypeID'])) ? $data['participantSubTypeID'] : null;
        $this->teamID = (!empty($data['teamID'])) ? $data['teamID'] : null;
       
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {
        
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

}