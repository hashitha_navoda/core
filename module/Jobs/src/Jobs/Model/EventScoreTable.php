<?php

namespace Jobs\Model;

use Core\Model\ProductTable as CoreProductTable;
use Zend\Db\TableGateway\TableGateway;
use Jobs\Model\JobCost;
use Zend\Db\Sql\Sql;

class EventScoreTable extends CoreProductTable {

	protected $tableGateway;

	public function __construct(TableGateway $tableGateway) {
		$this->tableGateway = $tableGateway;
	}

	public function updateScore($scoreData) {
		
		$jobCostSaveData = [
			'score' => $scoreData->score,
			'remarks' => $scoreData->remarks,
			'eventID' => $scoreData->eventID,
			'eventParticipantID' => $scoreData->eventParticipantID,
			'deleted' => $scoreData->deleted,
		];
			
		try {
			$this->tableGateway->insert($jobCostSaveData);
			return $this->tableGateway->adapter->getDriver()->getLastGeneratedValue('eventScoreId');
		} catch (Exception $e) {
			return null;
		}
		
	}

	public function updateScoreAsDeleted($eventScoreId)
    {
    	$data = [
    		'deleted' => 1
    	];
        try {
            $this->tableGateway->update($data, array('eventScoreId' => $eventScoreId));
            return true;
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
            return false;
        }
    }

    public function getScoreByEventID($eventID, $type) {
		$sql = new Sql($this->tableGateway->getAdapter());
		$select = $sql->select();
		$select->from('eventScore')
			->columns(array('*'));
		if ($type == "eventWise") {
			$select->where(array('eventScore.eventID' => $eventID, 'eventParticipantID' => null, 'deleted' => 0));
		} else if ($type == "participantWise") {
			$select->where(array('eventScore.eventID' => $eventID, 'deleted' => 0));
			$select->where->isNotNull('eventParticipantID');
		} else {
			$select->where(array('eventScore.eventID' => $eventID, 'deleted' => 0));
		}
		$statement = $sql->prepareStatementForSqlObject($select);
		$rowset = $statement->execute();
		if (!$rowset) {
			return NULL;
		}
		return $rowset;
	}

}
