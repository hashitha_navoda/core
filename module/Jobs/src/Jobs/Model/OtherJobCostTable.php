<?php

namespace Jobs\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Jobs\Model\OtherJobCost;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Select;
use Core\Model\ProductTable as CoreProductTable;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Predicate\PredicateSet;
use Zend\Db\Sql\Predicate\Between;
use Zend\Db\Sql\Predicate\Operator;

class OtherJobCostTable extends CoreProductTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function save(OtherJobCost $otherJobCost) {
		
		$jobOtherCostSaveData = [
			'jobID' => $otherJobCost->jobID,
	 		'costType' => $otherJobCost->costType,
	 		'otherJobCostUnitCost' => $otherJobCost->otherJobCostUnitCost,
	 		'otherJobCostNoOfUnits' => $otherJobCost->otherJobCostNoOfUnits,
	 		'jobCostID' => $otherJobCost->jobCostID
		];
			
		try {
			$this->tableGateway->insert($jobOtherCostSaveData);
			return true;
		} catch (Exception $e) {
			return null;
		}
		
	}
	/**
	* this function use to get other job cost details that related to the given jobCostID and locationID
	* @param int $jobCostID
	* @param int $locationID
	* return mix
	**/
	public function getOtherJobCostDetailsByJobCostIDAndLocationID($jobCostID, $locationID)
	{
		$sql = new Sql($this->tableGateway->getAdapter());
        $select = new Select();
        $select->from("otherJobCost")
                ->columns(array("*"))
                ->join('product', 'otherJobCost.costType = product.productID', array('*'), 'left')
                ->join('productUom', 'product.productID = productUom.productID', array('uomID'), 'left')
                ->join('locationProduct', 'product.productID = locationProduct.productID', array('locationProductID','locationID','locationProductQuantity'), 'left')
                ->where(array('otherJobCost.jobCostID' => $jobCostID, 'locationProduct.locationID' => $locationID,'productUom.productUomDisplay' => 1));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        if (!$results) {
            return null;
        }
        return $results;
	}

    

}
