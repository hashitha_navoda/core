<?php

namespace Jobs\Model;

use Zend\InputFilter\InputFilterInterface;

class FuelType {

	public $fuelTypeID;
	public $fuelTypeName;
	
	protected $inputFilter;

	public function exchangeArray($data) {
		$this->fuelTypeID = (!empty($data['fuelTypeID'])) ? $data['fuelTypeID'] : null;
		$this->fuelTypeName = (!empty($data['fuelTypeName'])) ? $data['fuelTypeName'] : null;
	}

	public function setInputFilter(InputFilterInterface $inputFilter) {
		throw new \Exception("Not used");
	}

	public function getInputFilter() {

	}

	public function getArrayCopy() {
		return get_object_vars($this);
	}

}