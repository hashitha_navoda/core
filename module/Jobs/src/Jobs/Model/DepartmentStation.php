<?php

namespace Jobs\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class DepartmentStation
{

    public $departmentStationID;
    public $departmentStationCode;
    public $departmentStationName;
    public $departmentId;
    
    
    public function exchangeArray($data)
    {
        $this->departmentStationID = (!empty($data['departmentStationId'])) ? $data['departmentStationId'] : null;
        $this->departmentStationCode = (!empty($data['departmentStationCode'])) ? $data['departmentStationCode'] : null;
        $this->departmentStationName = (!empty($data['departmentStationName'])) ? $data['departmentStationName'] : null;
        $this->departmentId = (!empty($data['departmentId'])) ? $data['departmentId'] : null;
       
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {
        
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

}