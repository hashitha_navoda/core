<?php

namespace Jobs\Model;

use Zend\InputFilter\InputFilterInterface;

class ServiceEmployee {

	public $serviceEmployeeId;
	public $jobID;
	public $employeeID;
	public $deleted;
	
	protected $inputFilter;

	public function exchangeArray($data) {
		$this->serviceEmployeeId = (!empty($data['serviceEmployeeId'])) ? $data['serviceEmployeeId'] : null;
		$this->jobID = (!empty($data['jobID'])) ? $data['jobID'] : null;
		$this->employeeID = (!empty($data['employeeID'])) ? $data['employeeID'] : null;
		$this->deleted = (!empty($data['deleted'])) ? $data['deleted'] : null;
	}

	public function setInputFilter(InputFilterInterface $inputFilter) {
		throw new \Exception("Not used");
	}

	public function getInputFilter() {

	}

	public function getArrayCopy() {
		return get_object_vars($this);
	}

}