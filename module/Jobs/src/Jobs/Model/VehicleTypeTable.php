<?php

namespace Jobs\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Jobs\Model\VehicleType;
use Zend\Db\Sql\Sql;
use Core\Model\ProductTable as CoreProductTable;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Predicate\PredicateSet;
use Zend\Db\Sql\Predicate\Between;
use Zend\Db\Sql\Predicate\Operator;

class VehicleTypeTable extends CoreProductTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll($paginated = FALSE) {
    	$adapter = $this->tableGateway->getAdapter();
    	$sql = new Sql($adapter);
    	$select = $sql->select();
    	$select->from('vehicleType');
    	$select->order('vehicleTypeName ASC');
    	$select->join('entity', 'vehicleType.entityId = entity.entityID', array("deleted"), "left");
    	$select->where(array('deleted' => '0'));
        if ($paginated) {
    		$paginatorAdapter = new DbSelect(
    			$select, $this->tableGateway->getAdapter());
    		$paginator = new Paginator($paginatorAdapter);
    		return $paginator;
    	} else {
    		$statement = $sql->prepareStatementForSqlObject($select);
    		$result = $statement->execute();
    		return $result;
    	}
    }

    public function saveVehicleType($vehicleType) {
        $data = array(
            'vehicleTypeID' => $vehicleType->vehicleTypeID,
            'vehicleTypeName' => $vehicleType->vehicleTypeName,
            'vehicleTypeStatus' => $vehicleType->vehicleTypeStatus,
            'entityId' => $vehicleType->entityId,
        );
        if ($this->tableGateway->insert($data)) {
            $result = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
            return $result;
        } else {
            return FALSE;
        }
    }

    public function viewVehicleType($vehicleTypeID) {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('vehicleType');
        $select->join('vehicleModel', 'vehicleType.vehicleTypeID = vehicleModel.vehicleTypeID', array('*'), "left");
        $select->where(array('vehicleType.vehicleTypeID' => $vehicleTypeID, 'vehicleModel.vehicleTypeDelete' => '0'));
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }

    public function changeStatusVehicleType($vehicleTypeID) {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('vehicleType');
        $select->where(array('vehicleType.vehicleTypeID' => $vehicleTypeID));
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute()->current('vehicleTypeStatus');
        $currentStatus = $result['vehicleTypeStatus'];
        if($currentStatus == 1 || $currentStatus == '1') {
            $newStatus = 2;
        } else {
            $newStatus = 1;
        }
        $update = array('vehicleTypeStatus' => $newStatus);
        try {
            $this->tableGateway->update($update, array('vehicleTypeID' => $vehicleTypeID));
            return $newStatus;
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
            return FALSE;
        }
    }

    public function deleteVehicleType($vehicleTypeID) {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('vehicleType');
        $select->where(array('vehicleType.vehicleTypeID' => $vehicleTypeID));
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute()->current('vehicleTypeStatus');
        return $result['entityId'];
    }

    public function updateVehicleType($data, $vehicleTypeID){
        try {
            $this->tableGateway->update($data, array('vehicleTypeID' => $vehicleTypeID));
            return TRUE;
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
            return FALSE;
        }
    }

    public function searchVehicleTypes($key, $paginated = NULL){
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('vehicleType');
        $select->order('vehicleTypeName ASC');
        $select->join('entity', 'vehicleType.entityId = entity.entityID', array("deleted"), "left");
        $select->where(array('deleted' => '0'));
        if (!is_null($key)) {
            $select->where(new PredicateSet(array(new Operator('vehicleType.vehicleTypeName', 'like', '%' . $key . '%')), PredicateSet::OP_OR));
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }

    //get vehicle type for dropdown
    public function getVehicleTypeforDropDown($keyword)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('vehicleType');
        $select->join('entity', 'vehicleType.entityId = entity.entityID', array("deleted"), "left");
        $select->where(new PredicateSet(array(new Operator('vehicleType.vehicleTypeName', 'like', '%' . $keyword . '%')), PredicateSet::OP_OR));
        $select->where(new PredicateSet(array(new Operator('entity.deleted', '=', '0'))));
        $select->where(array('vehicleTypeStatus' => '1'));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return (object) $results;
    }

    //get vehicle model that related to the vehicle type id
    public function loadVehicleTypeForDropdown() {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('vehicleType')
            ->columns(array("*"))
            ->join('entity', 'vehicleType.entityId = entity.entityID', array("deleted"), "left")
            ->where(array('deleted' => '0'));
        
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if (!$rowset) {
            return NULL;
        }
        return $rowset;
    }
}
