<?php

namespace Jobs\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Jobs\Model\VehicleModel;
use Zend\Db\Sql\Sql;
use Core\Model\ProductTable as CoreProductTable;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Predicate\PredicateSet;
use Zend\Db\Sql\Predicate\Between;
use Zend\Db\Sql\Predicate\Operator;

class VehicleSubModelTable extends CoreProductTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function saveVehicleSubModel($vehicleModel) {
        $data = array(
            'vehicleSubModelID' => $vehicleModel->vehicleSubModelID,
            'vehicleSubModelName' => $vehicleModel->vehicleSubModelName,
            'vehicleModelID' => $vehicleModel->vehicleModelID
        );
        if ($this->tableGateway->insert($data)) {
            $result = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
            return $result;
        } else {
            return FALSE;
        }
    }

    public function loadVehicleSubModelForDropdown($vehicleModelID) {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('vehiclSubModel')
            ->columns(array("*"))
            ->where(array('vehiclSubModel.vehicleModelID' => $vehicleModelID));
        
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if (!$rowset) {
            return NULL;
        }
        return $rowset;
    }

    public function deleteVehicleSubModel($vehicleSubModelID) {
        try {
            $result = $this->tableGateway->delete(array('vehicleSubModelID' => $vehicleSubModelID));
            return TRUE;
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
            return FALSE;
        }
    }
}
