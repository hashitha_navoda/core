<?php

namespace Jobs\Model;

use Zend\InputFilter\InputFilterInterface;

class JobManager {

	public $jobManagerID;
	public $employeeID;
	public $jobID;

	public function exchangeArray($data) {
		$this->jobManagerID = (!empty($data['jobManagerID'])) ? $data['jobManagerID'] : null;
		$this->employeeID = (!empty($data['employeeID'])) ? $data['employeeID'] : null;
		$this->jobID = (!empty($data['jobID'])) ? $data['jobID'] : null;
	}

	public function setInputFilter(InputFilterInterface $inputFilter) {
		throw new \Exception("Not used");
	}

	public function getInputFilter() {

	}

	public function getArrayCopy() {
		return get_object_vars($this);
	}

}
