<?php

namespace Jobs\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Jobs\Model\ServiceVehicle;
use Zend\Db\Sql\Sql;
use Core\Model\ProductTable as CoreProductTable;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Predicate\PredicateSet;
use Zend\Db\Sql\Predicate\Between;
use Zend\Db\Sql\Predicate\Operator;

class ServiceVehicleTable extends CoreProductTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll($customerID = null) {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('serviceVehicle');
        $select->columns(array("*"));
        if (!is_null($customerID)) {
            $select->where(array('customerID' => $customerID));
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }

    //get vehicle details by vehicle reg no
    public function getVehicleDetails($vehicleRegNo)
    {
        error_log($vehicleRegNo);
        error_log("Veheicle Registainn");
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('serviceVehicle');
        $select->columns(array("*"));
        //$select->join('jobVehicle', 'serviceVehicle.serviceVehicleID=  jobVehicle.vehicleID', array("jobId"), "left");
       // $select->join('job', 'jobVehicle.jobId =  job.jobId', array("customerID"), "left");
        $select->join('customer', "serviceVehicle.customerID= customer.customerID", array('customerName', 'customerShortName', 'customerCode', 'customerStatus', 'customerTelephoneNumber', 'customerTitle'), 'left');
        $select->join('vehicleType', "serviceVehicle.vehicleType= vehicleType.vehicleTypeID", array('vehicleTypeName'), 'left');
        $select->join('vehicleModel', "serviceVehicle.vehicleModel= vehicleModel.vehicleModelID", array('vehicleModelName'), 'left');
        $select->join('vehiclSubModel', "serviceVehicle.vehicleSubModel= vehiclSubModel.vehicleSubModelID", array('vehicleSubModelName'), 'left');
        $select->join('customerProfile', 'customer.customerID = customerProfile.customerID', '*', 'left');
        $select->where(array('serviceVehicle.serviceVehicleRegNo' => $vehicleRegNo));
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if (!$rowset) {
            return NULL;
        }
        return $rowset;
    }

    public function searchVehiclesForDropDown($searchKey, $customerID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('serviceVehicle');
        $select->where( new PredicateSet(array(
            new Operator('serviceVehicle.serviceVehicleRegNo', 'like', '%' . $searchKey . '%')),
            PredicateSet::OP_OR ));
        if ($customerID != "" && floatval($customerID) != 0) {
            $select->where(array('serviceVehicle.customerID' => $customerID));
        }
        $select->limit(10);
        $statement = $sql->prepareStatementForSqlObject($select);
        return $statement->execute();
    }

    public function getJobsByVehicle($vehicleRegNo)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('serviceVehicle');
        $select->columns(array("*"));
        $select->join('jobVehicle', 'serviceVehicle.serviceVehicleID=  jobVehicle.vehicleID', array("jobId",'jobVehicleKMs'), "left");
        $select->join('job', 'jobVehicle.jobId =  job.jobId', array("*"), "left");
        $select->join('customer', "serviceVehicle.customerID= customer.customerID", array('customerName', 'customerShortName', 'customerCode', 'customerStatus', 'customerTelephoneNumber'), 'left');
        $select->join('entity', 'job.entityID=  entity.entityID', array("*"), "left");
        $select->where(array('serviceVehicle.serviceVehicleRegNo' => $vehicleRegNo, 'jobVehicle.deleted' => 0));
        $select->where->in('job.jobStatus',[9,4, 20]);
        $select->order(['job.jobId DESC']);
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if (!$rowset) {
            return NULL;
        }
        return $rowset;
    }

    public function getVehicleByVehicleID($vehicleID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('serviceVehicle');
        $select->columns(array("*"));
        $select->where(array('serviceVehicle.serviceVehicleID' => $vehicleID));
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if (!$rowset) {
            return NULL;
        }
        return $rowset->current();
    }

    public function getJobsByVehicleWithStatus($vehicleRegNo, $item)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('serviceVehicle');
        $select->columns(array("*"));
        $select->join('jobVehicle', 'serviceVehicle.serviceVehicleID=  jobVehicle.vehicleID', array("jobId",'jobVehicleKMs'), "left");
        $select->join('jobVehicleStatus', 'jobVehicleStatus.jobVehicleID=  jobVehicle.jobVehicleID', array("*"), "left");
        $select->join('job', 'jobVehicle.jobId =  job.jobId', array("*"), "left");
        $select->join('customer', "serviceVehicle.customerID= customer.customerID", array('customerName', 'customerShortName', 'customerCode', 'customerStatus', 'customerTelephoneNumber'), 'left');
        $select->join('entity', 'jobVehicleStatus.entityID=  entity.entityID', array("*"), "left");
        $select->where(array('serviceVehicle.serviceVehicleRegNo' => $vehicleRegNo, 'entity.deleted' => 0));
        $select->where->in('job.jobStatus',[9,4, 20]);
        switch ($item) {
            case 'air-filter':
                $select->where(array('jobVehicleStatus.airFilter' => 1));
                break;
            case 'engine-oil':
                $select->where(array('jobVehicleStatus.engineOil' => 1));
                break;
            case 'wiper-fluid':
                $select->where(array('jobVehicleStatus.wiperFluid' => 1));
                break;
            case 'wiper-blades':
                $select->where(array('jobVehicleStatus.wiperBlades' => 1));
                break;
            case 'tyres':
                $select->where(array('jobVehicleStatus.tyres' => 1));
                break;
            case 'check-belts':
                $select->where(array('jobVehicleStatus.checkBelts' => 1));
                break;
            case 'battery':
                $select->where(array('jobVehicleStatus.battery' => 1));
                break;
            case 'transmission-fluid':
                $select->where(array('jobVehicleStatus.transmissionFluid' => 1));
                break;
            case 'steering-fluid':
                $select->where(array('jobVehicleStatus.steeringFluid' => 1));
                break;
            case 'brake-fluid':
                $select->where(array('jobVehicleStatus.brakeFluid' => 1));
                break;
            case 'coolant':
                $select->where(array('jobVehicleStatus.coolant' => 1));
                break;
            default:
                break;
        }


        $select->order(['job.jobId DESC']);
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if (!$rowset) {
            return NULL;
        }
        return $rowset;
    }

    /**
     * save service vehicle
     * @param array $serviceVehicleData
     * @return mixed
     */
    public function saveVehicle($serviceVehicleData) {
        $data = array(
            'vehicleType' => $serviceVehicleData->vehicleType,
            'fuelTypeID' => $serviceVehicleData->fuelTypeID,
            'customerID' => $serviceVehicleData->customerID,
            'vehicleModel' => $serviceVehicleData->vehicleModel,
            'vehicleSubModel' => $serviceVehicleData->vehicleSubModel,
            'serviceVehicleRegNo' => $serviceVehicleData->serviceVehicleRegNo,
            'serviceVehicleEngineNo' => $serviceVehicleData->serviceVehicleEngineNo,
        );
        if ($this->tableGateway->insert($data)) {
            $result = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
            return $result;
        } else {
            return FALSE;
        }
    }

    /**
     * update service vehicle
     * @param int $serviceVehicleID
     * @param array $serviceVehicleData
     * @return mixed
     */
    public function updateVehicle($serviceVehicleID, $serviceVehicleData) {
        try {
            $this->tableGateway->update($serviceVehicleData, array('serviceVehicleID' => $serviceVehicleID));
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
        return true;
    }


    /**
     * update service vehicle
     * @param int $serviceVehicleID
     * @param array $serviceVehicleData
     * @return mixed
     */
    public function updateVehicleCustomer($vehicleID, $customerID) {
        try {
            $data = array('customerID' => $customerID);
            $this->tableGateway->update($data, array('serviceVehicleID' => $vehicleID));
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
        return true;
    }
}
