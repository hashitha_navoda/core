<?php

namespace Jobs\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Predicate\PredicateSet;
use Zend\Db\Sql\Predicate\Operator;

class ServiceVehicleTypeTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll()
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('serviceVehicleType');
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }

    public function saveServiceVehicleTypeData($data)
    {
        $taskData = array(
            'serviceID' => $data->serviceID,
            'vehicleTypeID' => $data->vehicleTypeID,
            'deleted' => '0'
        );
        if ($this->tableGateway->insert($taskData)) {
            $result = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
            return $result;
        } else {
            return FALSE;
        }
    }

    public function updateDeleted($serviceID)
    {
        $data = array(
            'deleted' => 1,
        );
        try {
            $this->tableGateway->update($data, array('serviceID' => $serviceID));
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
        return true;
    }

    public function getRelatedVehicleTypes($serviceID)
    {

        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('serviceVehicleType');
        $select->where(array('serviceID' => $serviceID, 'deleted' => 0));

        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if (!$rowset) {
            return NULL;
        }
        return $rowset;
    }

    public function getTaskCardList($vehicleTypeID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('serviceVehicleType')
            ->join('task','task.taskID = serviceVehicleType.serviceID',array('taskCode','taskName'),'left')
            ->where(array('serviceVehicleType.deleted' => 0, 'serviceVehicleType.vehicleTypeID' => $vehicleTypeID, 'task.deleted' => 0));
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if (!$rowset) {
            return NULL;
        }
        return $rowset;
    }

    public function getServiceList($vehicleTypeID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('serviceVehicleType')
            ->join('task','task.taskID = serviceVehicleType.serviceID',array('taskCode','taskName'),'left')
            ->join('serviceInceniveRate','serviceInceniveRate.serviceID = serviceVehicleType.serviceID AND serviceInceniveRate.vehicleTypeID = serviceVehicleType.vehicleTypeID',array('serviceInceniveRateId','serviceRate','incentive','incentiveType','deleteStatus' => new Expression('serviceInceniveRate.deleted')),'left')
            ->where(array('serviceVehicleType.deleted' => 0, 'serviceVehicleType.vehicleTypeID' => $vehicleTypeID, 'task.deleted' => 0));
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if (!$rowset) {
            return NULL;
        }
        return $rowset;
    }

}
