<?php

namespace Jobs\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Jobs\Model\Team;
use Zend\Db\Sql\Sql;
use Core\Model\ProductTable as CoreProductTable;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Predicate\PredicateSet;
use Zend\Db\Sql\Predicate\Between;
use Zend\Db\Sql\Predicate\Operator;

class TeamTable extends CoreProductTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    /**
     * fetch all teams 
     * @param boolean $paginated
     * @return mixed
     */
    public function fetchAll($paginated = FALSE)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('team');
        $select->where(array('isDeleted' => false));
        $select->order('teamID DESC');
        if ($paginated) {
            $paginatorAdapter = new DbSelect(
                    $select, $this->tableGateway->getAdapter());
            $paginator = new Paginator($paginatorAdapter);
            return $paginator;
        } else {
            $statement = $sql->prepareStatementForSqlObject($select);
            $result = $statement->execute();
            return $result;
        }
    }

    /**
     * fetch team that match for given team code 
     * @param string $employeeCode
     * @return mixed
     */
    public function getTeamByCode($teamCode)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('team')
                ->columns(array('*'))
                ->where(array('teamCode' => $teamCode, 'isDeleted' => false));
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if (!$rowset) {
            return NULL;
        }
        return $rowset;
    }

    /**
     * save team
     * @param array $deparmentData
     * @return mixed
     */
    public function saveTeams($teamData)
    {
        $teamDataSet = array(
            'teamCode' => $teamData->teamCode,
            'teamName' => $teamData->teamName,
            'teamStatus' => '1',
        );
        if ($this->tableGateway->insert($teamDataSet)) {
            $result = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
            return $result;
        } else {
            return FALSE;
        }
    }

    /**
     * Return team according to the given team code and not equal 
     * to the given team id
     * @param string $teamId
     * @param string $teamCode
     * @return mixed
     */
    public function checkTeamCodeValid($teamId,$teamCode) 
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('team')
                ->columns(array('*'))
                ->where(array('teamCode' => $teamCode, 'isDeleted' => 0))
                ->where->notEqualTo('teamID',$teamId);
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result->count();
    }

    /**
     * update team
     * @param array $teamData
     * @return mixed
     */
    public function updateTeam($teamData)
    {
        $TeamData = array(
            'teamCode' => $teamData->teamCode,
            'teamName' => $teamData->teamName,
        );
        try {
            $this->tableGateway->update($TeamData, array('teamID' => $teamData->teamID));
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
        return true;
    }

    /**
     * update team status
     * @param int $teamID
     * @param boolean $status
     * @return mixed
     */
    public function updateTeamState($teamID, $status)
    {
        $teamData = array(
            'teamStatus' => $status,
        );
        try {
            $this->tableGateway->update($teamData, array('teamID' => $teamID));
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
        return true;
    }

    /**
     * search team by search key
     * @param string $keyword
     * @return mixed
     */
    public function teamSearchByKey($keyword)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('team')
                ->columns(array('*'));
        $select->where(new PredicateSet(array(new Operator('teamName', 'like', '%' . $keyword . '%'), new Operator('teamCode', 'like', '%' . $keyword . '%')), PredicateSet::OP_OR));
        $select->where(array('isDeleted' => 0));

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return (object) $results;
    }

    public function teamSearchByKeyForDropdown($keyword)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('team')
                ->columns(array('*'));
        $select->where(new PredicateSet(array(new Operator('teamName', 'like', '%' . $keyword . '%'), new Operator('teamCode', 'like', '%' . $keyword . '%')), PredicateSet::OP_OR));
        $select->where(array('isDeleted' => 0, 'teamStatus'=> 1));

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return (object) $results;
    }

    /**
     * update team delete
     * @param int $teamID
     * @return mixed
     */
    public function updateTeamDelete($teamID)
    {
        $teamData = array(
            'isDeleted' => true,
        );
        try {
            $this->tableGateway->update($teamData, array('teamID' => $teamID));
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
        return true;
    }

     public function getTeamByID($id)
    {
        $rowset = $this->tableGateway->select(array('teamID' => $id));
        $row = $rowset->current();
        if (!$row) {
            return NULL;
        }
        return $row;
    }
}
