<?php

namespace Jobs\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Jobs\Model\VehicleModel;
use Zend\Db\Sql\Sql;
use Core\Model\ProductTable as CoreProductTable;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Predicate\PredicateSet;
use Zend\Db\Sql\Predicate\Between;
use Zend\Db\Sql\Predicate\Operator;

class VehicleModelTable extends CoreProductTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function saveVehicleModel($vehicleModel) {
        $data = array(
            'vehicleModelID' => $vehicleModel->vehicleModelID,
            'vehicleModelName' => $vehicleModel->vehicleModelName,
            'vehicleTypeID' => $vehicleModel->vehicleTypeID,
            'vehicleTypeDelete' => $vehicleModel->vehicleTypeDelete,
        );
        if ($this->tableGateway->insert($data)) {
            $result = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
            return $result;
        } else {
            return FALSE;
        }
    }

    public function deleteVehicleModel($vehicleModelID) {
        $data = array('vehicleTypeDelete' => '1');
        try {
            $this->tableGateway->update($data, array('vehicleModelID' => $vehicleModelID));
            return TRUE;
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
            return FALSE;
        }
    }

    //get vehicle model that related to the vehicle type id
    public function loadVehicleModelForDropdown($vehicleTypeId) {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('vehicleModel')
            ->columns(array("*"))
            ->where(array('vehicleModel.vehicleTypeID' => $vehicleTypeId, 'vehicleModel.vehicleTypeDelete' => 0));
        
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if (!$rowset) {
            return NULL;
        }
        return $rowset;
    }

    //get vehicle model for dropdown
    public function searchVehicleModelsForDropdown($keyword)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('vehicleModel');
        $select->where(new PredicateSet(array(new Operator('vehicleModel.vehicleModelName', 'like', '%' . $keyword . '%')), PredicateSet::OP_OR));
        $select->where(new PredicateSet(array(new Operator('vehicleTypeDelete', '=', '0'))));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return (object) $results;
    }

    public function viewVehicleModel($vehicleModelID) {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('vehicleModel');
        $select->join('vehiclSubModel', 'vehicleModel.vehicleModelID = vehiclSubModel.vehicleModelID', array('vehicleSubModelID','vehicleSubModelName'), "left");
        $select->where(array('vehicleModel.vehicleModelID' => $vehicleModelID));
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }
}
