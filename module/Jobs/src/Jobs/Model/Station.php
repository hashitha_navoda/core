<?php

namespace Jobs\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class Station
{

    public $stationID;
    public $stationName;
    public $stationCode;
    public $departmentID;
    public $stationStatus;
    protected $inputFilter;
    
    public function exchangeArray($data)
    {
        $this->stationID = (!empty($data['stationID'])) ? $data['stationID'] : null;
        $this->stationName = (!empty($data['stationName'])) ? $data['stationName'] : null;
        $this->stationCode = (!empty($data['stationCode'])) ? $data['stationCode'] : null;
        $this->departmentID = (!empty($data['departmentID'])) ? $data['departmentID'] : null;
        $this->stationStatus = (!empty($data['stationStatus'])) ? $data['stationStatus'] : null;
       
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {
        
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

}