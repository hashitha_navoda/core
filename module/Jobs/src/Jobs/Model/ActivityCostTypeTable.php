<?php

/**
 * @author Sandun <sandun@thinkcube.com>
 */

namespace Jobs\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;

class ActivityCostTypeTable
{

    protected $tableGateway;
    public $adapter;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll()
    {
        $rowset = $this->tableGateway->select();
        return $rowset;
    }

    public function saveActivityCostType(ActivityCostType $activityCostType)
    {
        $serialData = array(
            'activityId' => $activityCostType->activityId,
            'costTypeId' => $activityCostType->costTypeId,
            'activityCostTypeEstimatedCost' => $activityCostType->activityCostTypeEstimatedCost,
            'activityCostTypeActualCost' => $activityCostType->activityCostTypeActualCost
        );
        if ($this->tableGateway->insert($serialData)) {
            $insertedID = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
            return $insertedID;
        } else {
            return FALSE;
        }
    }

    public function updateBySerialIDAndLocationProductID($data, $sId, $locProductID)
    {
        $result = $this->tableGateway->update($data, array('activitySerialID' => $sId));
        return $result;
    }

//get Activity details that related to given Activity Cost ID
    public function getActivityByActivityCostID($activityCostID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('activityCostType')
                ->columns(array('*'))
                ->join('activity', 'activityCostType.activityId=activity.activityId', array("*"), "left")
                ->join('entity', 'activity.entityID=entity.entityID', array("deleted"), "left");
        $select->where(array('activityCostType.costTypeId' => $activityCostID, 'entity.deleted' => 0));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return (object) $results;
    }

    public function deleteCostTypeByActivityID($activityID)
    {
        try {
            $result = $this->tableGateway->delete(array('activityID' => $activityID));
            return $result;
        } catch (\Exception $e) {
            $error = 0;
            return $error;
        }
    }

}
