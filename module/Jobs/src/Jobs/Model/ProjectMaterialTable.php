<?php

namespace Jobs\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Jobs\Model\ProjectMaterial;
use Zend\Db\Sql\Sql;
use Core\Model\ProductTable as CoreProductTable;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Predicate\PredicateSet;
use Zend\Db\Sql\Predicate\Between;
use Zend\Db\Sql\Predicate\Operator;

class ProjectMaterialTable extends CoreProductTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }


     /**
     * save project Material record 
     * @param array $projectMaterialData
     * @return mixed
     */
    public function saveProjectMaterial($projectMaterialData)
    {
        $projectMaterialData = array(
            'projectID' => $projectMaterialData->projectID,
            'productID' => $projectMaterialData->productID,
            'locationProductID' => $projectMaterialData->locationProductID,
        );
        if ($this->tableGateway->insert($projectMaterialData)) {
            $result = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
            return true;
        } else {
            return FALSE;
        }
    }


    /**
     * fetch items that match for given projectID 
     * @param string $id
     * @return mixed
     */
    public function getProjectMaterialsDetailsByProjectID($id)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('projectMaterial')
                ->columns(array('*'))
                ->join('product', 'projectMaterial.productID = product.productID', array('*'))
                ->where(array('projectMaterial.projectID' => $id, 'projectMaterial.isDeleted' => 0));
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if (!$rowset) {
            return NULL;
        }
        return $rowset;
    }

    /**
     * delete projectMaterial that match for given projectMaterialID 
     * @param string $projectMaterialID
     * @return mixed
     */
    public function deleteProjectMaterialByprojectMaterialID($projectMaterialID)
    {
        $projectMaterialData = array(
            'isDeleted' => true,
        );
        try {
            $this->tableGateway->update($projectMaterialData, array('projectMaterialID' => $projectMaterialID));
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
        return true;
    }
    

}
