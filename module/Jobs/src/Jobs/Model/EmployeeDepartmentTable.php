<?php

namespace Jobs\Model;

use Core\Model\ProductTable as CoreProductTable;
use Jobs\Model\EmployeeDesignation;
use Zend\Db\Sql\Predicate\Operator;
use Zend\Db\Sql\Predicate\PredicateSet;
use Zend\Db\Sql\Sql;
use Zend\Db\TableGateway\TableGateway;

class EmployeeDepartmentTable extends CoreProductTable {

	protected $tableGateway;

	public function __construct(TableGateway $tableGateway) {
		$this->tableGateway = $tableGateway;
	}

	/**
	 * save employeeDepartment record
	 * @param array $EmployeeDepartmentData
	 * @return mixed
	 */
	public function saveEmployeeDepartment($EmployeeDepartmentData) {
		$EmployeeDepartmentData = array(
			'employeeId' => $EmployeeDepartmentData->employeeId,
			'departmentId' => $EmployeeDepartmentData->departmentId,
		);
		if ($this->tableGateway->insert($EmployeeDepartmentData)) {
			$result = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
			return true;
		} else {
			return FALSE;
		}
	}

	/**
	 * fetch employee departments that match for given employeeID
	 * @param int $id
	 * @return mixed
	 */
	public function getEmployeeDepartments($id) {

		$sql = new Sql($this->tableGateway->getAdapter());
		$select = $sql->select();
		$select->from('employeeDepartment')
			->columns(array('*'))
			->join('department', 'employeeDepartment.departmentId = department.departmentID', array('*'), 'left')
			->where(array('employeeID' => $id, 'employeeDepartment.isDeleted' => false, 'department.departmentStatus' => true));
		$statement = $sql->prepareStatementForSqlObject($select);
		$rowset = $statement->execute();
		if (!$rowset) {
			return NULL;
		}
		return $rowset;
	}

    /**
     * fetch inactive employees that match for given employee id
     * @param string $id
     * @return mixed
     */
    public function getInactiveEmployeeDepartments($id)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('employeeDepartment')
                ->columns(array('*'))
                ->join('department', 'employeeDepartment.departmentId = department.departmentID', array('*'), 'left')
                ->where(array('employeeDepartment.employeeID' => $id, 'department.departmentStatus' => false));
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();

        if (!$rowset) {
            return NULL;
        }
        return $rowset;
    }

    /**
	 * fetch employee department that match for given employeeID and department id
	 * @param int $deptId
	 * @param int $empId
	 * @return mixed
	 */
	public function getEmpDeptByIds($deptId, $empId) {

		$sql = new Sql($this->tableGateway->getAdapter());
		$select = $sql->select();
		$select->from('employeeDepartment')
			->columns(array('*'))
			->where(array('employeeId' => $empId, 'isDeleted' => false, 'departmentId' => $deptId));
		$statement = $sql->prepareStatementForSqlObject($select);
		$rowset = $statement->execute();
		if (!$rowset) {
			return NULL;
		}
		return $rowset;
	}

	/**
	 * update employee department record with new employee id
	 * @param int $deptId
	 * @param int $oldEmpId
	 * @param int $newEmpId
	 * @return mixed
	 */
	public function updateEmpRecordWithNewEmpID($deptId, $oldEmpId, $newEmpId ) {
		$data = array(
            'employeeId' => $newEmpId,
        );
        try {
            $this->tableGateway->update($data, array('departmentId' => $deptId, 'employeeId' => $oldEmpId));
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
        return true;
	}

	/**
     * change the status of the employee related departemnet by employeeDepartment id
     * @param int $employeeDepartmentID
     * @param string $status
     * @return mixed
     */
    public function changeStatusOfEmployeeeDepartment($employeeDepartmentID, $status)
    {
        $data = array(
            'employeeDepartmentStatus' => $status,
        );
        try {
            $this->tableGateway->update($data, array('employeeDepartmentID' => $employeeDepartmentID));
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
        return true;
    }

    /**
	 * fetch employee departments that match for given departmentID
	 * @param int $id
	 * @return mixed
	 */
	public function getEmployeeDepartmentsByDepartmentId($id, $hideInactiveEmp = false) {

		$sql = new Sql($this->tableGateway->getAdapter());
		$select = $sql->select();
		$select->from('employeeDepartment')
			->columns(array('*'))
			->join('employee', 'employeeDepartment.employeeID = employee.employeeID', array('employeeFirstName', 'employeeSecondName','employeeCode'), 'left')
			->join('entity', 'employee.entityID =entity.entityID', array("deleted"), "left");

		if (is_null($id)) {
			$select->where(array('employeeDepartment.isDeleted' => false, 'entity.deleted' => false));
			$select->group('employeeDepartment.employeeID');
		} else {
			$select->where(array('employeeDepartment.departmentId' => $id, 'employeeDepartment.isDeleted' => false, 'entity.deleted' => false));
		}
		if ($hideInactiveEmp) {
			$select->where(array('employee.employeeStatus' => 1));
		}

		$statement = $sql->prepareStatementForSqlObject($select);
		$rowset = $statement->execute();
		if (!$rowset) {
			return NULL;
		}
		return $rowset;
	}

	/**
	 * update department id with the new department id
	 * @param int $oldDepId
	 * @param int $newDepId
	 * @return mixed
	 */
	public function updateEmployeeDepartmentWithNewDepartmentId($oldDepId, $newDepId ) {
		$data = array(
            'departmentId' => $newDepId,
        );
        try {
            $this->tableGateway->update($data, array('departmentId' => $oldDepId));
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
        return true;
	}
}
