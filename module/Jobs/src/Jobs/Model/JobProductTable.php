<?php

namespace Jobs\Model;

use Core\Model\ProductTable as CoreProductTable;
use Zend\Db\TableGateway\TableGateway;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Predicate\PredicateSet;
use Zend\Db\Sql\Predicate\Operator;

class JobProductTable extends CoreProductTable {

	protected $tableGateway;

	public function __construct(TableGateway $tableGateway) {
		$this->tableGateway = $tableGateway;
	}


    public function updateApprovedQuantity($approvedProducts)
    {
        try {
            foreach ($approvedProducts as $pro) {
                $proData = array('jobProductRequestedQty' => $pro['qty']);
                $condition = array('jobProductId' => $pro['jobProductId']);

                $this->tableGateway->update($proData, $condition);
            }
        } catch (Exception $e) {
            error_log($e->getTraceAsString());
            return false;
        }
        return true;
    }


	public function jobProductsByJobId($jobId) {
		$sql = new Sql($this->tableGateway->getAdapter());
		$select = $sql->select();
		$select->from('jobProduct')
			->columns(array('*'))
			->join('locationProduct', 'locationProduct.locationProductID = jobProduct.locationProductID', array("locationProductID", "productID"), "left")
			->join('product', 'product.productID = locationProduct.productID', array("productCode", "productName"), "left")
            ->join('jobTask', 'jobTask.jobTaskID = jobProduct.jobTaskID', array("jobTaskID", "taskID"), "left")
            ->join('task', 'jobTask.taskID = task.taskID', array("taskID", "taskCode", "taskName"), "left")
            ->join('uom', 'uom.uomID = jobProduct.uomID', array("uomName", "uomAbbr"), "left")
			->where(array('jobProduct.jobID' => $jobId));
		$statement = $sql->prepareStatementForSqlObject($select);
		$rowset = $statement->execute();
		return $rowset;
	}


	public function update($data, $id) {
		try {
		    $this->tableGateway->update($data, array('jobProductID' => $id));
		} catch (Exception $exc) {
		    echo $exc->getTraceAsString();
		    return false;
		}
		return true;
	}


    public function saveJobProduct($jobProductData) {
        $jobProductSaveData = array(
            'jobID' => $jobProductData->jobID,
            'locationProductID' => $jobProductData->locationProductID,
            'jobProductUnitPrice' => $jobProductData->jobProductUnitPrice,
            'uomID' => $jobProductData->uomID,
            'jobProductAllocatedQty' => $jobProductData->jobProductAllocatedQty,
            'jobProductEstimatedQty' => $jobProductData->jobProductEstimatedQty,
            'jobProductRequestedQty' => $jobProductData->jobProductRequestedQty,
            'jobProductUsedQty' => $jobProductData->jobProductUsedQty,
            'jobProductIssuedQty' => $jobProductData->jobProductIssuedQty,
            'jobProductReOrderLevel' => $jobProductData->jobProductReOrderLevel,
            'jobProductMaterialTypeID' => $jobProductData->jobProductMaterialTypeID,
            'jobProductStatus' => $jobProductData->jobProductStatus,
            'jobProductTaskCardID' => $jobProductData->jobProductTaskCardID,
            'jobTaskID' => $jobProductData->jobTaskID,
            'jobProductDescription' => $jobProductData->jobProductDescription,
        );
        if ($this->tableGateway->insert($jobProductSaveData)) {
            $result = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue('jobProductID');
            return $result;
        } else {
            return FALSE;
        }
    }



    public function getProductDetailsByJobProductId($jobProductId)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();

        $select->from('jobProduct')
            ->columns(array('*'))
            ->join('locationProduct', 'locationProduct.locationProductID = jobProduct.locationProductID', array("locationProductID", "productID", "locationProductQuantity"), "left")
            ->join('product', 'product.productID = locationProduct.productID', array("productCode", "productName", "productTypeID"), "left")
            ->join('productHandeling', 'productHandeling.productID = product.productID', array("*"), "left")
            ->join('uom', 'uom.uomID = jobProduct.uomID', array("uomName", "uomAbbr"), "left")
            ->where(array('jobProduct.jobProductID' => $jobProductId));

        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();

        return $rowset->current();
    }


    public function updateIssueQuantity($jobProductId, $issueQty)
    {
        try {
            $proData = array('jobProductIssuedQty' => $issueQty);
            $condition = array('jobProductId' => $jobProductId);

            $this->tableGateway->update($proData, $condition);
        } catch (Exception $e) {
            error_log($e->getTraceAsString());
            return false;
        }
        return true;
    }


    /**
     * get Task Related Products by jobTaskID
     * @param array $jobTaskData
     * @param array $jobID
     * @return mixed
     */
    public function getTaskProductDatailsbyJobTaskID($jobTaskID, $jobID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('jobProduct')
            ->columns(array('*'))
            ->join('jobTask', 'jobProduct.jobTaskID=  jobTask.jobTaskID', array("*"), "left")
            ->join('locationProduct', 'jobProduct.locationProductID=  locationProduct.locationProductID', array("*"), "left")
            ->join('product', 'locationProduct.productID=  product.productID', array("*"), "left")
            ->join('materialRequestJobProduct', 'jobProduct.jobProductID=  materialRequestJobProduct.jobProductID', array("*"), "left")
            ->join('materialRequest', 'materialRequestJobProduct.materialRequestId=  materialRequest.materialRequestId', array("*"), "left")
            ->where(array('jobTask.jobID' => $jobID, 'jobTask.jobTaskID' => $jobTaskID));
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();

        if (!$rowset) {
            return NULL;
        }
        return $rowset;
    }

    public function getJobProductByJobID($jobID) {
        $rowset = $this->tableGateway->select(array('jobID' => $jobID));
        if (!$rowset) {
            return NULL;
        }
        return $rowset;

    }

    /**
     * get job Related other Products by jobID
     * @param array $jobID
     * @return mixed
     */
    public function getJobRelatedOtherMaterials($jobID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('jobProduct')
            ->columns(array('*'))
            ->join('locationProduct', 'jobProduct.locationProductID=  locationProduct.locationProductID', array("*"), "left")
            ->join('product', 'locationProduct.productID=  product.productID', array("*"), "left")
            ->join('materialRequestJobProduct', 'jobProduct.jobProductID=  materialRequestJobProduct.jobProductID', array("*"), "left")
            ->join('materialRequest', 'materialRequestJobProduct.materialRequestId=  materialRequest.materialRequestId', array("*"), "left")
            ->where(array('jobProduct.jobID' => $jobID, 'jobProduct.jobTaskID' => NULL));
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if (!$rowset) {
            return NULL;
        }
        return $rowset;

    }


    public function getJobRelatedMaterials($jobID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('jobProduct')
            ->columns(array('*'))
            ->join('locationProduct', 'jobProduct.locationProductID=  locationProduct.locationProductID', array("*"), "left")
            ->join('product', 'locationProduct.productID=  product.productID', array("*"), "left")
            ->join('materialRequestJobProduct', 'jobProduct.jobProductID=  materialRequestJobProduct.jobProductID', array("*"), "left")
            ->join('materialRequest', 'materialRequestJobProduct.materialRequestId=  materialRequest.materialRequestId', array("*"), "left")
            ->where(array('jobProduct.jobID' => $jobID));
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if (!$rowset) {
            return NULL;
        }
        return $rowset;

    }

    //update jobProduct status
    public function updateJobProductStatus($jobProductID, $jobProductStatus) {
        $data = array('jobProductStatus' => $jobProductStatus);
        try {
            $this->tableGateway->update($data, array('jobProductID' => $jobProductID));
            return true;
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
            return false;
        }
    }

    public function deleteJobProductByJobProductID($jobProductID) {
        try {
            $result = $this->tableGateway->delete(array('jobProductID' => $jobProductID));
            return $result;
        } catch (\Exception $e) {
            $error = 0;
            return $error;
        }
    }

    public function getJobProductByJobProductID($jobProductID) {
        $rowset = $this->tableGateway->select(array('jobProductID' => $jobProductID));
        $row = $rowset->current();
        if (!$row) {
            return NULL;
        }
        return $row;
    }

    public function updateAllocatedQty($jobProductId, $requestedQuantity) {
        try {
            $row = $this->tableGateway->select(array('jobProductId' => $jobProductId))->current();
            $updatedQty = $row->jobProductAllocatedQty + $requestedQuantity;
            $this->tableGateway->update(array('jobProductAllocatedQty' => $updatedQty), array('jobProductId' => $jobProductId));
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
            return false;
        }
        return $updatedQty;
    }

	/**
	* this function use to get items that related to the given job id and not assign to the tasks
	* @param int $jobID
	* @param int $locationID
	* return mix
	**/
	public function getNonTaskProductByJobIDAndLocationID($jobID, $locationID)
	{
		$sql = new Sql($this->tableGateway->getAdapter());
		$select = $sql->select();
		$select->from('jobProduct')
			->columns(array('*'))
			->join('locationProduct', 'locationProduct.locationProductID = jobProduct.locationProductID', array("locationProductID", "productID"), "left")
			->join('product', 'product.productID = locationProduct.productID', array("productCode", "productName"), "left")
			->join('uom', 'uom.uomID = jobProduct.uomID', array("uomName", "uomAbbr"), "left")
			->where(array('jobProduct.jobID' => $jobID));
			 $select->where->AND->NEST
            ->in('jobProduct.jobTaskID', [0])->orPredicate(
                new \Zend\Db\Sql\Predicate\IsNull('jobProduct.jobTaskID')
            );
		$statement = $sql->prepareStatementForSqlObject($select);
		$rowset = $statement->execute();
		return $rowset;
	}

    /**
    * this function use to get not invoiced job product details
    * @param $int $jobID
    * return mix
    **/
    public function getOpenJobProductByJobID($jobID) {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('jobProduct')
            ->columns(array('jobProductID', 'remQty' => new Expression('jobProduct.jobProductUsedQty-jobProduct.jobProductInvoicedQty')))
            ->where(array('jobProduct.jobID' => $jobID));
        $select->having('remQty > 0');
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        return $rowset;
    }

    public function getNotUsedJobProductByJobID($jobID) {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('jobProduct')
            ->columns(array('*'))
            ->where(array('jobProduct.jobID' => $jobID));
        $select->where->AND->NEST
            ->in('jobProduct.jobProductStatus', [0])->orPredicate(
                new \Zend\Db\Sql\Predicate\IsNull('jobProduct.jobProductStatus')
            );
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        return $rowset;
    }

    //get details that related to the given project Id
    public function getJobMaterialsDetailsByProjectID($ID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('job')
                ->columns(array('*'))
                ->join('jobProduct', 'job.jobID=  jobProduct.jobID', array("*"), "left")
                ->join('locationProduct', 'jobProduct.locationProductID=  locationProduct.locationProductID', array("*"), "left")
                ->join('product', 'locationProduct.productID=  product.productID', array("*"), "left")
                ->join('materialRequestJobProduct', 'jobProduct.jobProductID=  materialRequestJobProduct.jobProductID', array("*"), "left")
                ->join('materialRequest', 'materialRequestJobProduct.materialRequestId=  materialRequest.materialRequestId', array("*"), "left")
                ->where(array('job.projectId' => $ID))
                ->where->notEqualTo('job.jobStatus', 5)
                ->where->notEqualTo('job.jobStatus', 10);
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if (!$rowset) {
            return NULL;
        }
        return $rowset;
    }

    /**
     * update JobTaskState status
     * @param int $jobTaskID
     * @return mixed
     */
    public function updateJobProductStatusByJobTaskID($jobTaskID, $jobProductStatus)
    {
        
        $jobProductData = array(
            'jobProductStatus' => $jobProductStatus
        );
        try {
            $this->tableGateway->update($jobProductData, array('jobTaskID' => $jobTaskID));
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
        return true;
    }

    public function getJobProductByjobTaskID($jobTaskID) {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('jobProduct');
        $select->columns(array('jobProductEstimatedQty', 'jobProductUsedQty','jobProductAllocatedQty','jobProductID'));
        $select->join('locationProduct', 'jobProduct.locationProductID=  locationProduct.locationProductID', array('productID','locationProductQuantity'), "left");
        $select->join('product', 'locationProduct.productID = product.productID', array('productCode', 'productName'), "left");
        $select->where(array('jobProduct.jobTaskID' => $jobTaskID));
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if (!$rowset) {
            return NULL;
        }
        return $rowset;

    }

    public function jobProductsQtyForInventoryMarerialRequisition($fromDate, $toDate, $location = null, $product = null) {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('jobProduct')
            ->columns(array('*'))
            ->join('materialRequestJobProduct', 'jobProduct.jobProductID = materialRequestJobProduct.jobProductId', array('reqQty' => new Expression('SUM(requestedQuantity)'), 'issQty' => new Expression('SUM(issuedQty)')), 'left')
            ->join('materialRequest', 'materialRequestJobProduct.materialRequestId = materialRequest.materialRequestId', array('materialRequestDate'), 'left')
            ->join('locationProduct', 'jobProduct.locationProductID = locationProduct.locationProductID', array("locationProductID", "productID"), "left");
        $select->where->between('materialRequest.materialRequestDate', $fromDate, $toDate);
        if ($location != null) {
            $select->where->in('locationProduct.locationID', $location);
        }
        if ($product != null) {
            $select->where->in('locationProduct.productID', $product);
        }
        $select->group('locationProduct.productID');
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        return $rowset;
    }
}
