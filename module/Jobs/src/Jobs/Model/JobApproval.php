<?php

namespace Jobs\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class JobApproval
{

    public $jobApprovalID;
    public $jobID;
    public $userID;
    public $jobApprovalDate;
    protected $inputFilter;
    
    public function exchangeArray($data)
    {
        $this->jobApprovalID = (!empty($data['jobApprovalID'])) ? $data['jobApprovalID'] : null;
        $this->jobID = (!empty($data['jobID'])) ? $data['jobID'] : null;
        $this->userID = (!empty($data['userID'])) ? $data['userID'] : null;
        $this->jobApprovalDate = (!empty($data['jobApprovalDate'])) ? $data['jobApprovalDate'] : null;
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {
        
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

}