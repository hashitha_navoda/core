<?php

namespace Jobs\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class JobContractor
{

    public $jobContractorID;
    public $jobID;
    public $contractorID;
    protected $inputFilter;
    
    public function exchangeArray($data)
    {
        $this->jobContractorID = (!empty($data['jobContractorID'])) ? $data['jobContractorID'] : null;
        $this->jobID = (!empty($data['jobID'])) ? $data['jobID'] : null;
        $this->contractorID = (!empty($data['contractorID'])) ? $data['contractorID'] : null;
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {
        
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

}
