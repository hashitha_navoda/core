<?php

namespace Jobs\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Jobs\Model\Task;
use Zend\Db\Sql\Sql;
use Core\Model\ProductTable as CoreProductTable;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Predicate\PredicateSet;
use Zend\Db\Sql\Predicate\Between;
use Zend\Db\Sql\Predicate\Operator;

class TaskTable extends CoreProductTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll($paginated = FALSE)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('task');
        $select->order('taskID DESC');
        $select->where(array('deleted' => '0'));
        $select->join('department','department.departmentID = task.departmentID',array('departmentCode','departmentName'),'left');
        $select->join('product','task.productID = product.productID',array('productCode','productName'),'left');
        if ($paginated) {
            $paginatorAdapter = new DbSelect(
                    $select, $this->tableGateway->getAdapter());
            $paginator = new Paginator($paginatorAdapter);
            return $paginator;
        } else {
            $statement = $sql->prepareStatementForSqlObject($select);
            $result = $statement->execute();
            return $result;
        }
    }
    
    //update task by task id
    public function updateTask(Task $task)
    {
        $taskData = array(
            'taskName' => $task->taskName,
            'departmentID' => $task->departmentID,
        );
        try {
            $this->tableGateway->update($taskData, array('taskID' => $task->taskID));
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
        return true;
    }

    //get task by task code
    public function getTaskByTaskCode($taskCode)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('task')
                ->columns(array("*"))
                ->where(array('task.taskCode' => $taskCode, "task.deleted" => 0));
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if (!$rowset) {
            return NULL;
        }
        return $rowset;
    }

    //save task function
    public function saveTask(Task $task)
    {
        $taskData = array(
            'taskName' => $task->taskName,
            'taskCode' => $task->taskCode,
            'departmentID' => $task->departmentID,
            'deleted' => '0',
            'productID' => $task->productID
        );
        if ($this->tableGateway->insert($taskData)) {
            $result = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
            return $result;
        } else {
            return FALSE;
        }
    }

    //get Task for search
    public function getTaskforSearch($keyword, $vehicalTypeID = null, $type = null)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('task')
                ->columns(array(
                    'posi' => new Expression('LEAST(IF(POSITION(\'' . $keyword . '\' in taskName )>0,POSITION(\'' . $keyword . '\' in taskName), 9999),'
                            . 'IF(POSITION(\'' . $keyword . '\' in taskCode )>0,POSITION(\'' . $keyword . '\' in taskCode), 9999)) '),
                    'len' => new Expression('LEAST(CHAR_LENGTH(taskName ), CHAR_LENGTH(taskCode )) '),
                    '*'));
        $select->join('department','department.departmentID = task.departmentID',array('departmentCode','departmentName'),'left');
        $select->join('product','task.productID = product.productID',array('productCode','productName'),'left');

        if (!is_null($vehicalTypeID)) {
            $select->join('serviceVehicleType', 'serviceVehicleType.serviceID = task.taskID', array('vehicleTypeID'));
            $select->where(array('task.deleted'=> 0,'vehicleTypeID' => $vehicalTypeID, 'serviceVehicleType.deleted' => 0));
        }

        $select->where(new PredicateSet(array(new Operator('task.taskName', 'like', '%' . $keyword . '%'), new Operator('task.taskCode', 'like', '%' . $keyword . '%')), PredicateSet::OP_OR));
        $select->where(new PredicateSet(array(new Operator('task.deleted', '=', '0'))));
        if ($type != 'service') {
            $select->where(array('status'=> 1));
        }
        $select->order(new Expression('IF(posi > 0, posi, 9999) ASC, len ASC'));
        $select->group('task.taskID');
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return (object) $results;
    }

    //update task as deleted by task id
    public function updateDeleteTask($taskID)
    {
        $taskData = array(
            'deleted' => '1',
        );
        try {
            $this->tableGateway->update($taskData, array('taskID' => $taskID));
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
        return true;
    }

    /**
     * change the station status
     * @param int $departmentID
     * @param string $stationCode
     * @param boolean $status
     * @return mixed
     */
    public function changeStatusID($taskID, $status)
    {
        $taskData = array(
            'status' => $status,
        );
        try {
            $this->tableGateway->update($taskData, array('taskID' => $taskID));
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
        return true;
    }

    /**
     * check availability of task code
     * @param string $taskCode
     * @return boolean
     */
    public function checkTaskCodeAvailability($taskCode)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('task')
                ->columns(array("*"))
                ->where(array('task.taskCode' => $taskCode, "task.deleted" => 0));
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if (count($rowset) > 0) {
            return FALSE;
        } else {
            return TRUE;
        }
    }

    /**
     * update task department record with new department id
     * @param int $oldDepId
     * @param int $newDepId
     * @return mixed
     */
    public function updateTaskDepartmentWithNewDepartmentId($oldDepId, $newDepId ) {
        $data = array(
            'departmentId' => $newDepId,
        );
        try {
            $this->tableGateway->update($data, array('departmentId' => $oldDepId));
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
        return true;
    }

    public function checkDepartmentUsedInService($departmentID) 
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('task')
                ->columns(array('*'))
                ->where(array('deleted' => 0, 'departmentId' => $departmentID));
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if ($rowset->count() > 0) {
            return false;
        }
        return true;
    }

    //get task by task code
    public function getTaskByTaskIDs($taskIds)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('task')
                ->columns(array("*"))
                ->where(array("task.deleted" => 0));
        $select->where->in('task.taskID', $taskIds);
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if (!$rowset) {
            return NULL;
        }
        return $rowset;
    }

}
