<?php

namespace Jobs\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Jobs\Model\RateCard;
use Zend\Db\Sql\Sql;
use Core\Model\ProductTable as CoreProductTable;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Predicate\PredicateSet;
use Zend\Db\Sql\Predicate\Between;
use Zend\Db\Sql\Predicate\Operator;

class RateCardTable extends CoreProductTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    /**
     * fetch all rate cards 
     * @param boolean $paginated
     * @return mixed
     */
    public function fetchAll($paginated = FALSE)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('rateCard');
        $select->order('rateCardID DESC');
        $select->where(array('isDeleted' => '0'));
        if ($paginated) {
            $paginatorAdapter = new DbSelect(
                    $select, $this->tableGateway->getAdapter());
            $paginator = new Paginator($paginatorAdapter);
            return $paginator;
        } else {
            $statement = $sql->prepareStatementForSqlObject($select);
            $result = $statement->execute();
            return $result;
        }
    }


    /**
     * fetch rate card that match for given rate card code 
     * @param string $rateCardCode
     * @return mixed
     */
    public function getRateCards($rateCardCode)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('rateCard')
                ->columns(array('rateCardCode'))
                ->where(array('rateCardCode' => $rateCardCode, "isDeleted" => 0));
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if (!$rowset) {
            return NULL;
        }
        return $rowset;
    }

    /**
     * save rate card
     * @param array $rateCardData
     * @return mixed
     */
    public function saveRateCard($rateCardData)
    {
        $rateCardData = array(
            'rateCardCode' => $rateCardData->rateCardCode,
            'rateCardName' => $rateCardData->rateCardName,
            'rateCardStatus' => $rateCardData->rateCardStatus,
        );
        if ($this->tableGateway->insert($rateCardData)) {
            $result = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
            return $result;
        } else {
            return FALSE;
        }
    }

    //update rate card as deleted by rate card id
    public function updateDeleteRateCard($rateCardID)
    {
        $rateCardData = array(
            'isDeleted' => '1',
        );
        try {
            $this->tableGateway->update($rateCardData, array('rateCardID' => $rateCardID));
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
        return true;
    }

    /**
     * update rate card status
     * @param int $rateCardID
     * @param boolean $status
     * @return mixed
     */
    public function changeStatusID($rateCardID, $status)
    {
        $rateCardData = array(
            'rateCardStatus' => $status,
        );
        try {
            $this->tableGateway->update($rateCardData, array('rateCardID' => $rateCardID));
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
        return true;
    }

    //get rate card for search
    public function rateCardSearchByKey($keyword)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('rateCard')
                ->columns(array(
                    'posi' => new Expression('LEAST(IF(POSITION(\'' . $keyword . '\' in rateCardName )>0,POSITION(\'' . $keyword . '\' in rateCardName), 9999),'
                            . 'IF(POSITION(\'' . $keyword . '\' in rateCardCode )>0,POSITION(\'' . $keyword . '\' in rateCardCode), 9999)) '),
                    'len' => new Expression('LEAST(CHAR_LENGTH(rateCardName ), CHAR_LENGTH(rateCardCode )) '),
                    '*'));
        $select->where(new PredicateSet(array(new Operator('rateCard.rateCardName', 'like', '%' . $keyword . '%'), new Operator('rateCard.rateCardCode', 'like', '%' . $keyword . '%')), PredicateSet::OP_OR));
        $select->where(new PredicateSet(array(new Operator('rateCard.isDeleted', '=', '0'))));
        $select->order(new Expression('IF(posi > 0, posi, 9999) ASC, len ASC'));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return (object) $results;
    }
}
