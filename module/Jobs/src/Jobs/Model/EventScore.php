<?php

namespace Jobs\Model;

use Zend\InputFilter\InputFilterInterface;

class EventScore {

	public $eventScoreId;
	public $score;
	public $remarks;
	public $eventID;
	public $eventParticipantID;
	public $deleted;
	
	protected $inputFilter;

	public function exchangeArray($data) {
		$this->eventScoreId = (!empty($data['eventScoreId'])) ? $data['eventScoreId'] : null;
		$this->score = (!empty($data['score'])) ? $data['score'] : null;
		$this->remarks = (!empty($data['remarks'])) ? $data['remarks'] : null;
		$this->eventID = (!empty($data['eventID'])) ? $data['eventID'] : null;
		$this->eventParticipantID = (!empty($data['eventParticipantID'])) ? $data['eventParticipantID'] : null;
		$this->deleted = (!empty($data['deleted'])) ? $data['deleted'] : 0;
		
	}

	public function setInputFilter(InputFilterInterface $inputFilter) {
		throw new \Exception("Not used");
	}

	public function getInputFilter() {

	}

	public function getArrayCopy() {
		return get_object_vars($this);
	}

}