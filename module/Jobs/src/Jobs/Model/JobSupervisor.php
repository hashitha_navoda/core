<?php

namespace Jobs\Model;

use Zend\InputFilter\InputFilterInterface;

class JobSupervisor {

	public $jobSupervisorTableID;
	public $jobSupervisorID;
	public $jobID;
	public $participantTypeID;
	public $participantSubTypeID;
	public $teamID;

	public function exchangeArray($data) {
		$this->jobSupervisorTableID = (!empty($data['jobSupervisorTableID'])) ? $data['jobSupervisorTableID'] : null;
		$this->jobSupervisorID = (!empty($data['jobSupervisorID'])) ? $data['jobSupervisorID'] : null;
		$this->jobID = (!empty($data['jobID'])) ? $data['jobID'] : null;
		$this->participantTypeID = (!empty($data['participantTypeID'])) ? $data['participantTypeID'] : null;
		$this->participantSubTypeID = (!empty($data['participantSubTypeID'])) ? $data['participantSubTypeID'] : null;
		$this->teamID = (!empty($data['teamID'])) ? $data['teamID'] : null;
	}

	public function setInputFilter(InputFilterInterface $inputFilter) {
		throw new \Exception("Not used");
	}

	public function getInputFilter() {

	}

	public function getArrayCopy() {
		return get_object_vars($this);
	}

}
