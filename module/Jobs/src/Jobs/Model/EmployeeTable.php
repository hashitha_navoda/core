<?php

namespace Jobs\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Predicate\PredicateSet;
use Zend\Db\Sql\Predicate\Operator;
use Zend\Db\Sql\Predicate\IsNotNull;

class EmployeeTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    /**
     * fetch all employees 
     * @param boolean $paginated
     * @return mixed
     */
    public function fetchAll($paginated = FALSE)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('employee');
        $select->order('employeeID DESC');
        $select->join('department', 'employee.departmentID=  department.departmentID', array("departmentName", "departmentCode"), "left");
        $select->join('entity', 'employee.entityID =entity.entityID', array("deleted"), "left");
        $select->where(array('entity.deleted' => 0));
        if ($paginated) {
            $paginatorAdapter = new DbSelect(
                    $select, $this->tableGateway->getAdapter());
            $paginator = new Paginator($paginatorAdapter);
            return $paginator;
        } else {
            $statement = $sql->prepareStatementForSqlObject($select);
            $result = $statement->execute();
            return $result;
        }
    }

    /**
     * fetch employee that match for given employee code 
     * @param string $employeeCode
     * @return mixed
     */
    public function getEmployeeByCode($employeeCode)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('employee')
                ->columns(array('*'))
                ->join('entity', 'employee.entityID =entity.entityID', array("deleted"), "left")
                ->where(array('entity.deleted' => 0, 'employee.employeeCode' => $employeeCode));
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if (!$rowset) {
            return NULL;
        }
        return $rowset;
    }

    /**
     * save employee
     * @param array $employeeData
     * @return mixed
     */
    public function saveEmployees($employeesData)
    {
        $EmployeesData = array(
            'employeeCode' => $employeesData->employeeCode,
            'employeeFirstName' => $employeesData->employeeFirstName,
            'employeeSecondName' => $employeesData->employeeSecondName,
            'departmentID' => $employeesData->departmentID,
            'employeeAddress' => $employeesData->employeeAddress,
            'employeeEmail' => $employeesData->employeeEmail,
            'employeeTP' => $employeesData->employeeTP,
            'employeeIDNo' => $employeesData->employeeIDNo,
            'entityID' => $employeesData->entityID,
            'employeeDOB' => $employeesData->employeeDOB,
            'employeeGender' => $employeesData->employeeGender,
            'employeeStatus' => '1',
        );
        if ($this->tableGateway->insert($EmployeesData)) {
            $result = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
            return $result;
        } else {
            return FALSE;
        }
    }

    /**
     * update employee
     * @param array $employeeData
     * @return mixed
     */
    public function updateEmployee($employeesData)
    {
        $EmployeesData = array(
            'employeeCode' => $employeesData->employeeCode,
            'employeeFirstName' => $employeesData->employeeFirstName,
            'employeeSecondName' => $employeesData->employeeSecondName,
            'departmentID' => $employeesData->departmentID,
            'employeeAddress' => $employeesData->employeeAddress,
            'employeeEmail' => $employeesData->employeeEmail,
            'employeeTP' => $employeesData->employeeTP,
            'employeeIDNo' => $employeesData->employeeIDNo,
            'entityID' => $employeesData->entityID,
        );
        try {
            $this->tableGateway->update($EmployeesData, array('employeeID' => $employeesData->employeeID));
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
        return true;
    }

    /**
     * search designation by search key, Department, Designation
     * @param string $key
     * @param int $designationID
     * @param int $departmentID
     * @return mixed
     */
    public function employeeSearchByKey($key=null, $designationID = null, $departmentID=null, $jobSupervisorFlag = false, $jobManagerFlag = false, $type = 'constructEmp')
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('employee')
                ->columns(array('employeeID', 'employeeCode', 'departmentID', 'employeeFirstName', 'employeeSecondName', 'employeeIDNo', 'employeeAddress', 'employeeTP', 'employeeEmail', 'departmentID',  'employeeStatus'))
                ->join('entity', 'employee.entityID = entity.entityID', array('*'), 'left')
                ->join('department', 'employee.departmentID=  department.departmentID', array("departmentName", "departmentCode"), "left");
        if (!is_null($key)) {
            $select->where(new PredicateSet(array(new Operator('employee.employeeFirstName', 'like', '%' . $key . '%'),new Operator('employee.employeeSecondName', 'like', '%' . $key . '%'), new Operator('employee.employeeCode', 'like', '%' . $key . '%'), new Operator('employee.employeeIDNo', 'like', '%' . $key . '%')), PredicateSet::OP_OR));
        }

        if (!is_null($designationID)) {
            $select->join('employeeDesignation', 'employee.employeeID = employeeDesignation.employeeID', array('*'), 'left');
            $select->where->equalTo('employeeDesignation.designationID', $designationID);
        }

        if (!is_null($departmentID) && $type == 'constructEmp') {
            $select->where->equalTo('employee.departmentID', $departmentID);
        }

        if (!is_null($departmentID) && $type == 'serviceEmp') {
            $select->join('employeeDepartment', 'employee.employeeID = employeeDepartment.employeeId', array('*'), 'left');
            $select->where->equalTo('employeeDepartment.departmentId', $departmentID);
        }
        
        if ($jobSupervisorFlag) {
            $select->join('employeeDesignation', 'employee.employeeID = employeeDesignation.employeeID', array('*'), 'left');
            $select->join('designation', 'employeeDesignation.designationID = designation.designationID', array('*'), 'left');
            $select->where->equalTo('designation.designationCode', "DES004");   
        }

        if ($jobManagerFlag) {
            $select->join('employeeDesignation', 'employee.employeeID = employeeDesignation.employeeID', array('*'), 'left');
            $select->join('designation', 'employeeDesignation.designationID = designation.designationID', array('*'), 'left');
            $select->where->equalTo('designation.designationCode', "DES003");   
        }
        
        $select->where(new PredicateSet(array(new Operator('entity.deleted', '=', '0'))));

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return (object) $results;
    }

    /**
     * update employee status
     * @param int $employeeID
     * @param boolean $status
     * @return mixed
     */
    public function updateEmployeeState($employeeID, $status)
    {
        $employeeData = array(
            'employeeStatus' => $status,
        );
        try {
            $this->tableGateway->update($employeeData, array('employeeID' => $employeeID));
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
        return true;
    }

    /**
     * Return employee according to the given employee code and not equal 
     * to the given employeee id
     * @param string $employeeId
     * @param string $employeeCode
     * @return mixed
     */
    public function checkEmployeeCodeValid($employeeId,$employeeCode) 
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('employee')
                ->columns(array('*'))
                ->join('entity', 'employee.entityID = entity.entityID', array('deleted'))
                ->where(array('entity.deleted' => 0, 'employee.employeeCode' => $employeeCode))
                ->where->notEqualTo('employee.employeeID',$employeeId);
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result->count();
    }

    /**
     * fetch all active employees 
     * @return mixed
     */
    public function fetchActiveEmployees()
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('employee');
        $select->order('employeeID DESC');
        $select->join('entity', 'employee.entityID = entity.entityID', array('deleted'));
        $select->where(array('employeeStatus' => true, 'deleted' => 0));
        
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }

    public function checkDepartmentUsed($departmentID) 
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('employee')
                ->columns(array('*'))
                ->join('entity', 'employee.entityID = entity.entityID', array('deleted'))
                ->where(array('employee.departmentID' => $departmentID, 'entity.deleted' => 0));
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if ($rowset->count() > 0) {
            return false;
        }
        return true;
    }

    /**
     * update employee with updated departmentID
     * @param int $oldId
     * @param int $newId
     * @return mixed
     */
    public function updateDeparmentIDInEmployeeTable($oldId, $newId)
    {
        $EmployeesData = array(
            'departmentID' => $newId,
        );
        try {
            $this->tableGateway->update($EmployeesData, array('departmentID' => $oldId));
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
        return true;
    }

    public function getEmployeeByID($id)
    {
        $rowset = $this->tableGateway->select(array('employeeID' => $id));
        $row = $rowset->current();
        if (!$row) {
            return NULL;
        }
        return $row;
    }


    public function getEmployeeByIds ($idArr) {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('employee');
        $select->join('entity', 'employee.entityID=  entity.entityID', array("*"), "left");
        $select->where(new PredicateSet(array(new Operator('entity.deleted', '=', '0'))));
        $select->where->in('employee.employeeID', $idArr);
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
        
    }
    
    public function searchEmployeeForDropdown($searchKey)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('employee');
        $select->join('entity', 'employee.entityID=  entity.entityID', array("*"), "left");
        $select->where(new PredicateSet(array(new Operator('employee.employeeFirstName', 'like', '%' . $searchKey . '%'), new Operator('employee.employeeSecondName', 'like', '%' . $searchKey . '%')), PredicateSet::OP_OR));
        $select->where(new PredicateSet(array(new Operator('entity.deleted', '=', '0'))));
        $select->where(new PredicateSet(array(new Operator('employee.employeeStatus', '=', '1'))));
        $select->limit(50);
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

    /**
     * fetch employee that match for given employee ID No  
     * @param string $employeeIDNO
     * @return mixed
     */
    public function checkEmployeeIDNovalid($employeeIDNO, $employeeId=null, $updataMode = false)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('employee')
                ->columns(array('*'))
                ->join('entity', 'employee.entityID =entity.entityID', array("deleted"), "left")
                ->where(array('entity.deleted' => 0, 'employee.employeeIDNo' => $employeeIDNO));
        if ($updataMode) {
            $select->where->notEqualTo('employee.employeeID',$employeeId);
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if (!$rowset) {
            return NULL;
        }
        return $rowset;
    }

    public function getEmployeeByDepartmentIDAndDesignationID($departmentID, $designationID) 
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('employee')
            ->columns(array('*'))
            ->join('employeeDepartment', 'employeeDepartment.employeeId = employee.employeeID', array('employeeDepartmentID', 'departmentId'), 'left')
            ->join('employeeDesignation', 'employeeDesignation.employeeID = employee.employeeID', array('employeeDesignationID', 'designationID'), 'left')
            ->join('entity', 'employee.entityID =entity.entityID', array("deleted"), "left");

        if ($departmentID != 0 && $designationID != 0) {
            $select->where(array('employeeDepartment.departmentId' => $departmentID, 'employeeDepartment.isDeleted' => false, 'employeeDesignation.designationID' => $designationID, 'employeeDesignation.isDeleted' => false,'entity.deleted' => false, 'employee.employeeStatus' => true));
        }

        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if (!$rowset) {
            return NULL;
        }
        return $rowset;
    }
}
