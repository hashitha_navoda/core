<?php

namespace Jobs\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class ProjectMaterial
{

    public $projectMaterialID;
    public $projectID;
    public $productID;
    public $locationProductID;
    protected $inputFilter;
    
    public function exchangeArray($data)
    {
        $this->projectMaterialID = (!empty($data['projectMaterialID'])) ? $data['projectMaterialID'] : null;
        $this->projectID = (!empty($data['projectID'])) ? $data['projectID'] : null;
        $this->productID = (!empty($data['productID'])) ? $data['productID'] : null;
        $this->locationProductID = (!empty($data['locationProductID'])) ? $data['locationProductID'] : null;
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {
        
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

}
