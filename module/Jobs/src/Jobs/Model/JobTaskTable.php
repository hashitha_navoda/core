<?php

namespace Jobs\Model;

use Core\Model\ProductTable as CoreProductTable;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;

class JobTaskTable extends CoreProductTable {

	protected $tableGateway;

	public function __construct(TableGateway $tableGateway) {
		$this->tableGateway = $tableGateway;
	}

	public function saveJobTask($jobTaskData) {
		$jobTaskSaveData = array(
			'jobID' => $jobTaskData->jobID,
			'taskID' => $jobTaskData->taskID,
			'uomID' => $jobTaskData->uomID,
			'jobTaskUnitCost' => $jobTaskData->jobTaskUnitCost,
			'jobTaskUnitRate' => $jobTaskData->jobTaskUnitRate,
			'jobTaskEstQty' => $jobTaskData->jobTaskEstQty,
			'jobTaskStatus' => $jobTaskData->jobTaskStatus,
			'jobTaskProgress' => $jobTaskData->jobTaskProgress,
			'jobTaskStartDate' => $jobTaskData->jobTaskStartDate,
			'jobTaskEndDate' => $jobTaskData->jobTaskEndDate,
			'stationID' => $jobTaskData->stationID,
			'jobTaskTaskCardID' => $jobTaskData->jobTaskTaskCardID,
		);
		if ($this->tableGateway->insert($jobTaskSaveData)) {
			$result = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue('jobTaskID');
			return $result;
		} else {
			return FALSE;
		}
	}

	public function getJobTaskDetails($jobID, $taskID = null, $taskCardID = null, $status = null ) {
		$sql = new Sql($this->tableGateway->getAdapter());
		$select = $sql->select();
		$select->from('jobTask')
			->columns(array('*'))
			->join('task', 'jobTask.taskID=  task.taskID', array("*"), "left")
			->join('job', 'jobTask.jobID=  job.jobID', array("*"), "left")
			->join('entity', 'job.entityID=  entity.entityID', array("*"), "left")
			->where(array('jobTask.jobID' => $jobID, 'task.deleted' => 0, 'entity.deleted' => 0));

		if (!is_null($taskID)) {
			$select->where(array('jobTask.taskID' => $taskID));
		}
		if (!is_null($taskCardID)) {
			$select->where(array('jobTask.jobTaskTaskCardID' => $taskCardID));
		}

		if (!is_null($status)) {
			$select->where(array('jobTask.jobTaskStatus' => $status));
		}

		$statement = $sql->prepareStatementForSqlObject($select);
		$rowset = $statement->execute();
		if (!$rowset) {
			return NULL;
		}
		return $rowset;
	}

	/**
     * update JobTaskState status
     * @param int $jobTaskID
     * @return mixed
     */
    public function updateJobTaskStatusByJobTaskID($jobTaskID, $jobTaskStatus)
    {
        $jobTaskData = array(
            'jobTaskStatus' => $jobTaskStatus
        );
        try {
            $this->tableGateway->update($jobTaskData, array('jobTaskID' => $jobTaskID));
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
        return true;
    }

    /**
     * get task details by jobTaskID
     * @param int $jobTaskID
     * @return mixed
     */
	public function getTaskDetailsByJobTaskId ($jobTaskID, $jobEmployeeFlag = false) {
		$sql = new Sql($this->tableGateway->getAdapter());
		$select = $sql->select();
		$select->from('jobTask')
			->columns(array('*'))
			->join('task', 'jobTask.taskID=  task.taskID', array("*"), "left")
			->join('job', 'jobTask.jobID=  job.jobID', array("*"), "left")
			->join('entity', 'job.entityID=  entity.entityID', array("*"), "left")
			->where(array('jobTask.jobTaskID' => $jobTaskID, 'task.deleted' => 0, 'entity.deleted' => 0));
        if ($jobEmployeeFlag) {
            $select->join('jobTaskEmployee','jobTaskEmployee.jobTaskID = jobTask.jobTaskID',array('jobTaskEmployeeID'),'left');
            $select->join('employee','employee.employeeID = jobTaskEmployee.employeeID',array('employeeCode','employeeFirstName','employeeSecondName'),'left');
        }
		$statement = $sql->prepareStatementForSqlObject($select);
		$rowset = $statement->execute();
		if (!$rowset) {
			return NULL;
		}
        if ($jobEmployeeFlag) {
            return $rowset;            
        } else {
		  return $rowset->current();
        }
	}

    public function getTaskDetailsByJobTaskIdForService($jobTaskID, $jobEmployeeFlag = false) {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('jobTask')
            ->columns(array('*'))
            ->join('task', 'jobTask.taskID=  task.taskID', array("*"), "left")
            ->join('job', 'jobTask.jobID=  job.jobID', array("*"), "left")
            ->join('jobVehicle', 'job.jobId =  jobVehicle.jobId', array("vehicleID"), "left")
            ->join('serviceVehicle', 'jobVehicle.vehicleID =  serviceVehicle.serviceVehicleID', array('serviceVehicleID', 'vehicleType', 'vehicleModel', 'serviceVehicleRegNo', 'serviceVehicleEngineNo'), "left")
            ->join('entity', 'job.entityID=  entity.entityID', array("*"), "left")
            ->where(array('jobTask.jobTaskID' => $jobTaskID, 'task.deleted' => 0, 'entity.deleted' => 0, 'jobVehicle.deleted' => 0));
        if ($jobEmployeeFlag) {
            $select->join('jobTaskEmployee','jobTaskEmployee.jobTaskID = jobTask.jobTaskID',array('jobTaskEmployeeID'),'left');
            $select->join('employee','employee.employeeID = jobTaskEmployee.employeeID',array('employeeCode','employeeFirstName','employeeSecondName'),'left');
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if (!$rowset) {
            return NULL;
        }
        if ($jobEmployeeFlag) {
            return $rowset;            
        } else {
          return $rowset->current();
        }
    }

    /**
     * update JobTask progress
     * @param int $jobTaskID
     * @param float $jobTaskProgress
     * @return mixed
     */
    public function updateJobTaskProgress($jobTaskID, $jobTaskProgress, $usedQuantity)
    {

        $jobTaskData = array(
            'jobTaskProgress' => $jobTaskProgress,
            'jobTaskUsedQty' => $usedQuantity,
        );
        try {
            $this->tableGateway->update($jobTaskData, array('jobTaskID' => $jobTaskID));
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
        return true;
    }

    public function deleteJobTaskByJobTaskID($jobTaskID) {
        try {
            $result = $this->tableGateway->delete(array('jobTaskID' => $jobTaskID));
            return $result;
        } catch (\Exception $e) {
            $error = 0;
            return $error;
        }
    }

    /**
    * this function use to get job task and product details to the given job ID, locationID and status Id(s).
    * @param int $jobID
    * @param int $locationID
    * @param array $status
    * return mix
    **/
    public function getJobTaskWithProductDetails($jobID, $locationID, $status = [])
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('jobTask')
            ->columns(array('*'))
            ->join('jobProduct', 'jobTask.jobTaskID = jobProduct.jobTaskID', array("jobProductID",'locationProductID','jobProductUnitPrice','uomID','jobProductAllocatedQty','jobProductRequestedQty','jobProductUsedQty','jobProductIssuedQty','jobProductReOrderLevel','jobProductMaterialTypeID','jobProductStatus','jobProductTaskCardID','jobProductInvoicedQty'), "left")
            ->join('locationProduct', 'jobProduct.locationProductID = locationProduct.locationProductID', array('productID','locationID','locationProductID'), "left")
            ->join('product', 'locationProduct.productID = product.productID', array('*'), "left")
            ->join('task', 'jobTask.taskID = task.taskID', array('*'), "left")
            ->join(array('taskUomTable' => 'uom'), 'jobTask.uomID = taskUomTable.uomID', array('taskUom' => new Expression('taskUomTable.uomName'),'taskUomAbbr' => new Expression('taskUomTable.uomAbbr') ), "left")
            ->join(array('productUom' => 'uom'), 'jobProduct.uomID = productUom.uomID', array('productUom' => new Expression('productUom.uomName'),'productUomAbbr' => new Expression('productUom.uomAbbr')) , "left")
            ->where(array('jobTask.jobID' => $jobID));
        $select->where->in('jobTask.jobTaskStatus', $status);         
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

    /**
    * this function use to update job task table
    * @param array data
    * @param int $jobTaskID
    * return boolean
    **/
    public function updateJobTask($jobTasKData, $jobTaskID) 
    {
        try {
            $this->tableGateway->update($jobTasKData, array('jobTaskID' => $jobTaskID));
            return true;
        } catch (Exception $exc) {
            error_log($exe->getMessage());
            return false;
        }
    }


    /**
    * this function use to get not invoiced job task details
    * @param $int $jobID
    * return mix
    **/
    public function getOpenJobTaskByJobID($jobID) {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('jobTask')
            ->columns(array('jobTaskID', 'remQty' => new Expression('jobTask.jobTaskUsedQty-jobTask.jobTaskInvoicedQty')))
            ->where(array('jobTask.jobID' => $jobID));
        $select->having('remQty > 0');
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        return $rowset;
    }

    public function getOpenStateJobTasjByJobID($jobID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('jobTask')
            ->columns(array('*'))
            ->where(array('jobTask.jobID' => $jobID));
        $select->where->in('jobTask.jobTaskStatus', [3,9,17]);
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        return $rowset; 
    }

    /**
     * get job related services by job Id
     * @param int $jobID
     * @return mixed
     */
    public function getJobRelatedServicesByJobId($jobID) {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('jobTask')
            ->columns(array('*'))
            ->join('task', 'jobTask.taskID=  task.taskID', array("*"), "left")
            ->join('departmentStation', 'jobTask.stationID=  departmentStation.departmentStationId', array("*"), "left")
            ->join('job', 'jobTask.jobID=  job.jobID', array("*"), "left")
            ->join('entity', 'job.entityID=  entity.entityID', array("*"), "left")
            ->join('taskCard', 'jobTask.jobTaskTaskCardID=  taskCard.taskCardID', array("*"), "left")
            ->where(array('jobTask.jobID' => $jobID, 'task.deleted' => 0));

        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if (!$rowset) {
            return NULL;
        }
        return $rowset;
    }

    /**
     * update JobTask Start time stamp
     * @param int $jobTaskID
     * @param string $startedAt
     * @return boolean
     */
    public function updateJobTaskStartTimeByJobTaskID($jobTaskID, $startedAt)
    {
        $jobTaskData = array(
            'startedAt' => $startedAt
        );
        try {
            $this->tableGateway->update($jobTaskData, array('jobTaskID' => $jobTaskID));
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
        return true;
    }

    /**
     * update JobTask End time stamp
     * @param int $jobTaskID
     * @param string $endedAt
     * @return boolean
     */
    public function updateJobTaskEndTimeByJobTaskID($jobTaskID, $endedAt)
    {
        $jobTaskData = array(
            'endedAt' => $endedAt
        );
        try {
            $this->tableGateway->update($jobTaskData, array('jobTaskID' => $jobTaskID));
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
        return true;
    }

     /**
     * get job related services by department Id
     * @param int $departmentID
     * @param string $state
     * @return mixed
     */
    public function getServicesByDepartmentId($departmentID, $state = 'pending') {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('jobTask')
            ->columns(array('*'))
            ->join('task', 'jobTask.taskID=  task.taskID', array("*"), "left")
            ->join('job', 'jobTask.jobID=  job.jobID', array("*"), "left")
            ->join('department', 'task.departmentID=  department.departmentID', array("*"), "left")
            ->join('jobVehicle', 'job.jobId =  jobVehicle.jobId', array("vehicleID"), "left")
            ->join('serviceVehicle', 'jobVehicle.vehicleID =  serviceVehicle.serviceVehicleID', array('serviceVehicleID', 'vehicleType', 'vehicleModel', 'serviceVehicleRegNo', 'serviceVehicleEngineNo'), "left")
            ->join('vehicleModel', "serviceVehicle.vehicleModel= vehicleModel.vehicleModelID", array('vehicleModelName'), 'left')
            ->join('entity', 'job.entityID=  entity.entityID', array("*"), "left")
            ->join('taskCard', 'jobTask.jobTaskTaskCardID=  taskCard.taskCardID', array("*"), "left");

            if ($state == 'pending') {
                $select->where(array('task.departmentID' => $departmentID, 'task.deleted' => 0, 'entity.deleted' => 0, 'jobTask.jobTaskStatus' => 3, 'jobVehicle.deleted' => 0));
            } else if ($state == 'progress') {
                $data = array(8,9,17);
                $select->where(array('task.departmentID' => $departmentID, 'task.deleted' => 0, 'entity.deleted' => 0, 'jobTask.jobTaskStatus' => $data, 'jobVehicle.deleted' => 0));
                $select->where->notEqualTo('job.jobStatus', 20)
                        ->where->notEqualTo('job.jobStatus', 21);
            } else if ($state == 'ended') {
                $select->where(array('task.departmentID' => $departmentID, 'task.deleted' => 0, 'entity.deleted' => 0, 'jobTask.jobTaskStatus' => 18, 'jobVehicle.deleted' => 0));
            }  
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if (!$rowset) {
            return NULL;
        }
        return $rowset;
    }


    /**
     * update JobTask station id
     * @param int $jobTaskID
     * @param int $departmentStationId
     * @return boolean
     */
    public function updateJobTaskStationId($jobTaskID, $departmentStationId)
    {
        $jobTaskData = array(
            'stationId' => $departmentStationId
        );
        try {
            $this->tableGateway->update($jobTaskData, array('jobTaskID' => $jobTaskID));
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
        return true;
    }

     /**
     * get job related services by job task id
     * @param int $jobTaskID
     * @return mixed
     */
    public function getServicesByJobTaskId($jobTaskID) {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('jobTask')
            ->columns(array('*'))
            ->join('task', 'jobTask.taskID=  task.taskID', array("*"), "left")
            ->join('job', 'jobTask.jobID=  job.jobID', array("*"), "left")
            ->join('jobVehicle', 'job.jobId =  jobVehicle.jobId', array("vehicleID"), "left")
            ->join('serviceVehicle', 'jobVehicle.vehicleID =  serviceVehicle.serviceVehicleID', array('serviceVehicleID', 'vehicleType', 'vehicleModel', 'serviceVehicleRegNo', 'serviceVehicleEngineNo'), "left")
            ->join('vehicleModel', "serviceVehicle.vehicleModel= vehicleModel.vehicleModelID", array('vehicleModelName'), 'left')
            ->join('entity', 'job.entityID=  entity.entityID', array("*"), "left")
            ->join('taskCard', 'jobTask.jobTaskTaskCardID=  taskCard.taskCardID', array("*"), "left");
        $select->where(array('jobTask.jobTaskID' => $jobTaskID, 'task.deleted' => 0, 'entity.deleted' => 0, 'jobVehicle.deleted' => 0));
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if (!$rowset) {
            return NULL;
        }
        return $rowset;
    }


     /**
     * get job related due services by job Id
     * @param int $jobID
     * @return mixed
     */
    public function getJobRelatedDueServices($jobID) {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('jobTask')
            ->columns(array('*'))
            ->join('task', 'jobTask.taskID=  task.taskID', array("*"), "left")
            ->join('job', 'jobTask.jobID=  job.jobID', array("*"), "left")
            ->join('department', 'task.departmentID=  department.departmentID', array("*"), "left")
            ->join('entity', 'job.entityID=  entity.entityID', array("*"), "left")
            ->join('taskCard', 'jobTask.jobTaskTaskCardID=  taskCard.taskCardID', array("*"), "left")
            ->where(array('jobTask.jobID' => $jobID, 'jobTask.jobTaskStatus' => 3, 'task.deleted' => 0, 'entity.deleted' => 0));

        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if (!$rowset) {
            return NULL;
        }
        return $rowset;
    }

    /**
     * update JobTask restarted flag
     * @param int $jobTaskID
     * @param boolean $state
     * @return boolean
     */
    public function updateJobTaskIsRestarted($jobTaskID, $state)
    {
        $jobTaskData = array(
            'restarted' => $state,
        );
        try {
            $this->tableGateway->update($jobTaskData, array('jobTaskID' => $jobTaskID));
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
        return true;
    }


     /**
     * get inprogress job task by department station id
     * @param int $departmentStationId
     * @return mixed
     */
	public function getInprogressJobTaskByStation ($departmentStationId) {
		$sql = new Sql($this->tableGateway->getAdapter());
		$select = $sql->select();
		$select->from('jobTask')
			->columns(array('*'))
			->join('job', 'jobTask.jobID=  job.jobID', array("*"), "left")
			->join('entity', 'job.entityID=  entity.entityID', array("*"), "left")
			->where(array('jobTask.stationId' => $departmentStationId, 'jobTask.jobTaskStatus' => 8 ,'entity.deleted' => 0));
		$statement = $sql->prepareStatementForSqlObject($select);
		$rowset = $statement->execute();
		if (!$rowset) {
			return NULL;
		}
		return $rowset;
	}

    /**
     * get Inprogress job tassk by employee id
     * @param int $employeeId
     * @return mixed
     */
    public function getInprogressJobTaskByEmployeeId ($employeeId) {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('jobTask')
            ->columns(array('*'))
            ->join('job', 'jobTask.jobID=  job.jobID', array("*"), "left")
            ->join('entity', 'job.entityID=  entity.entityID', array("*"), "left")
            ->join('jobTaskEmployee', 'jobTask.jobTaskID=  jobTaskEmployee.jobTaskID', array("*"), "left")
            ->where(array('jobTaskEmployee.employeeID' => $employeeId, 'jobTask.jobTaskStatus' => 8 ,'entity.deleted' => 0, 'jobTaskEmployee.isChanged' => false));
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if (!$rowset) {
            return NULL;
        }
        return $rowset;
    }

    /**
     * get task details by taskID
     * @param int $taskID
     * @return mixed
     */
    public function getTaskDetailsByTaskId ($taskID) {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('jobTask')
            ->columns(array('*'))
            ->join('task', 'jobTask.taskID=  task.taskID', array("*"), "left")
            ->join('job', 'jobTask.jobID=  job.jobID', array("*"), "left")
            ->join('entity', 'job.entityID=  entity.entityID', array("*"), "left")
            ->where(array('jobTask.taskID' => $taskID, 'task.deleted' => 0, 'entity.deleted' => 0));
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if (!$rowset) {
            return NULL;
        }
        return $rowset->current();
    }

    /**
     * get task details by jobtaskId and task id
     * @param int $jobTaskID
     * @param int $taskID
     * @return mixed
     */
    public function getTaskDetailsByTaskCardId ($taskCardID, $taskID = null) {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('jobTask')
            ->columns(array('*'))
            ->join('task', 'jobTask.taskID=  task.taskID', array("*"), "left")
            ->join('job', 'jobTask.jobID=  job.jobID', array("*"), "left")
            ->join('entity', 'job.entityID=  entity.entityID', array("*"), "left")
            ->where(array('jobTask.jobTaskTaskCardID' => $taskCardID, 'task.deleted' => 0, 'entity.deleted' => 0));

        if (!is_null($taskID)) {
            $select->where(array('jobTask.taskID' => $taskID));
        }

        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if (!$rowset) {
            return NULL;
        }
        return $rowset->current();
    } 

    /**
     * get task details by jobtaskId and task id
     * @param int $jobTaskID
     * @param int $taskID
     * @return mixed
     */
    public function getServiceDetailsByJobCardIdAndJobId ($jobCardID, $jobID) {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('jobTask')
            ->columns(array('*'))
            ->join('task', 'jobTask.taskID=  task.taskID', array("*"), "left")
            ->join('job', 'jobTask.jobID=  job.jobID', array("*"), "left")
            ->join('entity', 'job.entityID=  entity.entityID', array("*"), "left")
            ->where(array('jobTask.jobTaskTaskCardID' => $jobCardID, 'jobTask.jobID' => $jobID,'task.deleted' => 0, 'entity.deleted' => 0));

        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if (!$rowset) {
            return NULL;
        }
        return $rowset;
    } 
}
