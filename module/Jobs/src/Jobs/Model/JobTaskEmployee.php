<?php

namespace Jobs\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class JobTaskEmployee
{

    public $jobTaskEmpoyeeID;
    public $employeeID;
    public $taskID;
    public $jobTaskID;
    protected $inputFilter;
    
    public function exchangeArray($data)
    {
        $this->jobTaskEmpoyeeID = (!empty($data['jobTaskEmpoyeeID'])) ? $data['jobTaskEmpoyeeID'] : null;
        $this->employeeID = (!empty($data['employeeID'])) ? $data['employeeID'] : null;
        $this->taskID = (!empty($data['taskID'])) ? $data['taskID'] : null;
        $this->jobTaskID = (!empty($data['jobTaskID'])) ? $data['jobTaskID'] : null;
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {
        
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

}