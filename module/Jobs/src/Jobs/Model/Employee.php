<?php

namespace Jobs\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class Employee
{

    public $employeeID;
    public $employeeCode;
    public $employeeFirstName;
    public $employeeSecondName;
    public $departmentID;
    public $employeeAddress;
    public $employeeEmail;
    public $employeeTP;
    public $employeeIDNo;
    public $employeeDOB;
    public $employeeGender;
    public $entityID;

    public function exchangeArray($data)
    {
        $this->employeeID = (!empty($data['employeeID'])) ? $data['employeeID'] : null;
        $this->employeeCode = (!empty($data['employeeCode'])) ? $data['employeeCode'] : null;
        $this->employeeFirstName = (!empty($data['employeeFirstName'])) ? $data['employeeFirstName'] : null;
        $this->employeeSecondName = (!empty($data['employeeSecondName'])) ? $data['employeeSecondName'] : null;
        $this->departmentID = (!empty($data['departmentID'])) ? $data['departmentID'] : null;
        $this->employeeAddress = (!empty($data['employeeAddress'])) ? $data['employeeAddress'] : null;
        $this->employeeEmail = (!empty($data['employeeEmail'])) ? $data['employeeEmail'] : null;
        $this->employeeTP = (!empty($data['employeeTP'])) ? $data['employeeTP'] : null;
        $this->employeeIDNo = (!empty($data['employeeIDNo'])) ? $data['employeeIDNo'] : null;
        $this->employeeDOB = (!empty($data['employeeDOB'])) ? $data['employeeDOB'] : null;
        $this->employeeGender = (!empty($data['employeeGender'])) ? $data['employeeGender'] : null;
        $this->entityID = (!empty($data['entityID'])) ? $data['entityID'] : null;
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory = new InputFactory();

            $inputFilter->add($factory->createInput(array(
                        'name' => 'employeeID',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'Int'),
                        ),
            )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'employeeFirstName',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'StringLength',
                                'options' => array(
                                    'encoding' => 'UTF-8',
                                    'min' => 1,
                                    'max' => 100,
                                ),
                            ),
                        ),
            )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'employeeSecondName',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'StringLength',
                                'options' => array(
                                    'encoding' => 'UTF-8',
                                    'min' => 1,
                                    'max' => 100,
                                ),
                            ),
                        ),
            )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'designationID',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'StringLength',
                                'options' => array(
                                    'encoding' => 'UTF-8',
                                    'min' => 1,
                                    'max' => 100,
                                ),
                            ),
                        ),
            )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'divisionID',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'StringLength',
                                'options' => array(
                                    'encoding' => 'UTF-8',
                                    'min' => 1,
                                    'max' => 100,
                                ),
                            ),
                        ),
            )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'employeeEmail',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'StringLength',
                                'options' => array(
                                    'encoding' => 'UTF-8',
                                    'min' => 1,
                                    'max' => 100,
                                ),
                            ),
                        ),
            )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'employeeTP',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'Int'),
                        ),
            )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'employeeHourlyCost',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'Int'),
                        ),
            )));

            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

}
