<?php

namespace Jobs\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Jobs\Model\TaskCardTaskProduct;
use Zend\Db\Sql\Sql;
use Core\Model\ProductTable as CoreProductTable;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Predicate\PredicateSet;
use Zend\Db\Sql\Predicate\Between;
use Zend\Db\Sql\Predicate\Operator;

class TaskCardTaskProductTable extends CoreProductTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

	//save task card task products function
    public function saveTaskCardTaskProducts(TaskCardTaskProduct $TaskCardTaskProduct)
    {
        $taskCardTaskProductData = array(
            'taskCardTaskID' => $TaskCardTaskProduct->taskCardTaskID,
            'locationProductID' => $TaskCardTaskProduct->locationProductID,
            'taskCardTaskProductName' => $TaskCardTaskProduct->taskCardTaskProductName,
            'taskCardTaskProductType' => $TaskCardTaskProduct->taskCardTaskProductType,
            'taskCardTaskProductQty' => $TaskCardTaskProduct->taskCardTaskProductQty,
            'taskCardTaskProductUomID' => $TaskCardTaskProduct->taskCardTaskProductUomID
        );
        if ($this->tableGateway->insert($taskCardTaskProductData)) {
            $result = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
            return $result;
        } else {
            return FALSE;
        }
    }

    //get product list by task card task id
    public function getProductList($taskCardTaskID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('taskCardTaskProduct')
            ->columns(array('*'))
            ->join('uom', 'taskCardTaskProduct.taskCardTaskProductUomID = uom.uomID', array("*"), "left")
            ->where(array('taskCardTaskProduct.taskCardTaskID' => $taskCardTaskID));
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if (!$rowset) {
            return NULL;
        }
        return $rowset;
    }


    //delete product by task card task product id
    public function deleteProduct($taskCardTaskProductID)
    {
        try {
            $this->tableGateway->delete(array('taskCardTaskProductID' => $taskCardTaskProductID));
            return TRUE;
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
            return FALSE;
        }
    }

    //update task card task products function
    public function updateTaskCardTaskProducts($data, $id)
    {
        try {
            $this->tableGateway->update($data, array('taskCardTaskProductID' => $id));
            return $id;
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
            return FALSE;
        }
    }
}
