<?php

namespace Jobs\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class JobVehicle
{

    public $jobVehicleID;
    public $vehicleID;
    public $jobVehicleCostPerKM;
    public $jobVehicleKMs;
    public $jobId;
    
    protected $inputFilter;
    
    public function exchangeArray($data)
    {
        $this->jobVehicleID = (!empty($data['jobVehicleID'])) ? $data['jobVehicleID'] : null;
        $this->vehicleID = (!empty($data['vehicleID'])) ? $data['vehicleID'] : null;
        $this->jobVehicleCostPerKM = (!empty($data['jobVehicleCostPerKM'])) ? $data['jobVehicleCostPerKM'] : null;
        $this->jobVehicleKMs = (!empty($data['jobVehicleKMs'])) ? $data['jobVehicleKMs'] : null;
        $this->jobId = (!empty($data['jobId'])) ? $data['jobId'] : null;
       
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {
        
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

}