<?php

namespace Jobs\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class Task
{

    public $taskID;
    public $taskCode;
    public $employeeDesignationID;
    public $productID;
    protected $inputFilter;
    
    public function exchangeArray($data)
    {
        $this->taskID = (!empty($data['taskID'])) ? $data['taskID'] : null;
        $this->taskCode = (!empty($data['taskCode'])) ? $data['taskCode'] : null;
        $this->taskName = (!empty($data['taskName'])) ? $data['taskName'] : null;
        $this->departmentID = (!empty($data['departmentID'])) ? $data['departmentID'] : null;
        $this->productID = (!empty($data['productID'])) ? $data['productID'] : null;
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {
        
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

}
