<?php

namespace Jobs\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class Team
{

    public $teamID;
    public $teamCode;
    public $teamName;
   
    public function exchangeArray($data)
    {
        $this->teamID = (!empty($data['teamID'])) ? $data['teamID'] : null;
        $this->teamCode = (!empty($data['teamCode'])) ? $data['teamCode'] : null;
        $this->teamName = (!empty($data['teamName'])) ? $data['teamName'] : null;
       
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {
        
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

}