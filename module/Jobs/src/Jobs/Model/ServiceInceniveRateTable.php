<?php

namespace Jobs\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Jobs\Model\RateCard;
use Zend\Db\Sql\Sql;
use Core\Model\ProductTable as CoreProductTable;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Predicate\PredicateSet;
use Zend\Db\Sql\Predicate\Between;
use Zend\Db\Sql\Predicate\Operator;

class ServiceInceniveRateTable extends CoreProductTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    /**
     * fetch all rate cards 
     * @param boolean $paginated
     * @return mixed
     */
    public function fetchAll($paginated = FALSE)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('serviceInceniveRate');
        $select->order('serviceInceniveRateId DESC');
        $select->where(array('deleted' => 0));
        if ($paginated) {
            $paginatorAdapter = new DbSelect(
                    $select, $this->tableGateway->getAdapter());
            $paginator = new Paginator($paginatorAdapter);
            return $paginator;
        } else {
            $statement = $sql->prepareStatementForSqlObject($select);
            $result = $statement->execute();
            return $result;
        }
    }

    public function saveServiceIncentiveRate($inData)
    {
        $saveData = array(
            'serviceID' => $inData->serviceID,
            'vehicleTypeID' => $inData->vehicleTypeID,
            'serviceRate' => $inData->serviceRate,
            'incentive' => $inData->incentive,
            'incentiveType' => $inData->incentiveType,
            'deleted' => 0,
        );
        if ($this->tableGateway->insert($saveData)) {
            $result = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
            return $result;
        } else {
            return FALSE;
        }
    }

     public function getIncentiveByServiceIdAndVehicleType($serviceID, $vehicleTypeID) {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('serviceInceniveRate')
            ->columns(array('*'))
            ->where(array('serviceInceniveRate.serviceID' => $serviceID, 'serviceInceniveRate.vehicleTypeID' => $vehicleTypeID, 'deleted' => 0));
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if (!$rowset) {
            return NULL;
        }
        return $rowset->current();
    }

    public function updateIncentive($serviceInceniveRateId)
    {
        $data = array(
            'deleted' => 1
        );
        try {
            $this->tableGateway->update($data, array('serviceInceniveRateId' => $serviceInceniveRateId));
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
        return true;
    }
}
