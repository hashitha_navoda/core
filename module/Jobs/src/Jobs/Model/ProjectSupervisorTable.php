<?php

namespace Jobs\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Jobs\Model\ProjectManager;
use Zend\Db\Sql\Sql;
use Core\Model\ProductTable as CoreProductTable;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Predicate\PredicateSet;
use Zend\Db\Sql\Predicate\Between;
use Zend\Db\Sql\Predicate\Operator;

class ProjectSupervisorTable extends CoreProductTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    /**
     * save projectSupervisor record 
     * @param array $projectSupervisorData
     * @return mixed
     */
    public function saveProjectSupervisor($projectSupervisorData)
    {
        $projectSupervisorData = array(
            'projectID' => $projectSupervisorData->projectID,
            'employeeID' => $projectSupervisorData->employeeID
        );
        if ($this->tableGateway->insert($projectSupervisorData)) {
            $result = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
            return true;
        } else {
            return FALSE;
        }
    }

    /**
     * fetch projecSupervisors detail that match for given projectID 
     * @param string $id
     * @return mixed
     */
    public function getProjectSupervisorssDetailsByProjectID($id)
    {   
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('projectSupervisors')
                ->columns(array('*'))
                ->join('employee', 'projectSupervisors.employeeID = employee.employeeID', array('*'), 'left')
                ->join('employeeDesignation', 'employee.employeeID = employeeDesignation.employeeID', array('*'), 'left')
                ->join('designation', 'employeeDesignation.designationID = designation.designationID', array('*'), 'left')
                ->where(array('projectSupervisors.projectID' => $id, 'employee.employeeStatus' => true, 'projectSupervisors.isDeleted' => 0, 'designation.designationCode' => 'DES002', 'employeeDesignation.isDeleted' => 0));
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if (!$rowset) {
            return NULL;
        }
        return $rowset;
    }

    /**
     * delete projectSupervisor that match for given projectSupervisorID 
     * @param string $projectSupervisorID
     * @return mixed
     */
    public function deleteProjectSupervisorprojectSupervisorID($projectSupervisorID)
    {
        $projectSupervisorData = array(
            'isDeleted' => true,
        );
        try {
            $this->tableGateway->update($projectSupervisorData, array('projectSupervisorsId' => $projectSupervisorID));
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
        return true;
    }

    /**
     * check Employee that match for given employeeID used in project as projectSupervisor 
     * @param string $employeeId
     * @return mixed
     */
    public function checkEmployeeUsed($employeeId)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('projectSupervisors')
                ->columns(array('*'))
                ->join('project', 'projectSupervisors.projectID = project.projectId', array('*'), 'left')
                ->join('entity', 'project.entityID = entity.entityID', array('*'), 'left')
                ->where(array('projectSupervisors.employeeID' => $employeeId, 'projectSupervisors.isDeleted' => 0, 'entity.deleted' => 0));
        $select->where->notEqualTo('project.projectStatus',10);
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();

        if ($rowset->count() > 0) {
            return false;
        }
        return true;

    }

     /**
     * update projectManager with updated employeeID
     * @param int $oldId
     * @param int $newId
     * @return mixed
     */
    public function updateProjectSupervisorsTableWithNewEmpId($oldId, $newId)
    {
        $EmployeesData = array(
            'employeeID' => $newId,
        );
        try {
            $this->tableGateway->update($EmployeesData, array('employeeID' => $oldId));
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
        return true;
    }


    /**
     * fetch inactive employees that match for given projectID 
     * @param string $id
     * @return mixed
     */
    public function getInactiveProjectSupervisors($id)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('projectSupervisors')
                ->columns(array('*'))
                ->join('employee', 'projectSupervisors.employeeID = employee.employeeID', array('*'), 'left')
                ->join('entity', 'employee.entityID = entity.entityID', array('*'), 'left')
                ->where(array('projectSupervisors.projectID' => $id, 'employee.employeeStatus' => false, 'entity.deleted' => 0));
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();

        if (!$rowset) {
            return NULL;
        }
        return $rowset;
    }
}
