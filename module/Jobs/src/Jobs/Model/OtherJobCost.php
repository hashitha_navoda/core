<?php

namespace Jobs\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class OtherJobCost
{

    public $otherJobCostID;
    public $jobID;
    public $costType;
    public $otherJobCostUnitCost;
    public $otherJobCostNoOfUnits;
    public $jobCostID;
    protected $inputFilter;
    
    public function exchangeArray($data)
    {
        $this->otherJobCostID = (!empty($data['otherJobCostID'])) ? $data['otherJobCostID'] : null;
        $this->jobID = (!empty($data['jobID'])) ? $data['jobID'] : null;
        $this->costType = (!empty($data['costType'])) ? $data['costType'] : null;
        $this->otherJobCostUnitCost = (!empty($data['otherJobCostUnitCost'])) ? $data['otherJobCostUnitCost'] : null;
        $this->otherJobCostNoOfUnits = (!empty($data['otherJobCostNoOfUnits'])) ? $data['otherJobCostNoOfUnits'] : null;
        $this->jobCostID = (!empty($data['jobCostID'])) ? $data['jobCostID'] : null;
       
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {
        
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

}