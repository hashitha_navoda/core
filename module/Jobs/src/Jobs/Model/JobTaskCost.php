<?php

namespace Jobs\Model;

use Zend\InputFilter\InputFilterInterface;

class JobTaskCost {

	public $jobTaskCostID;
	public $jobTaskID;
	public $unitCost;
	public $unitRate;
	public $Qty;
	public $jobCostID;
	public $isJobCardTask;
	protected $inputFilter;

	public function exchangeArray($data) {
		
		$this->jobTaskCostID = (!empty($data['jobTaskCostID'])) ? $data['jobTaskCostID'] : null;
		$this->jobTaskID = (!empty($data['jobTaskID'])) ? $data['jobTaskID'] : 0;
		$this->unitCost = (!empty($data['unitCost'])) ? $data['unitCost'] : 0;
		$this->unitRate = (!empty($data['unitRate'])) ? $data['unitRate'] : 0;
		$this->Qty = (!empty($data['Qty'])) ? $data['Qty'] : 0;
		$this->jobCostID = (!empty($data['jobCostID'])) ? $data['jobCostID'] : null;
		$this->isJobCardTask = (!empty($data['isJobCardTask'])) ? $data['isJobCardTask'] : 0;
		
	}

	public function setInputFilter(InputFilterInterface $inputFilter) {
		throw new \Exception("Not used");
	}

	public function getInputFilter() {

	}

	public function getArrayCopy() {
		return get_object_vars($this);
	}

}