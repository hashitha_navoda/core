<?php

namespace Jobs\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Jobs\Model\Department;
use Zend\Db\Sql\Sql;
use Core\Model\ProductTable as CoreProductTable;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Predicate\PredicateSet;
use Zend\Db\Sql\Predicate\Between;
use Zend\Db\Sql\Predicate\Operator;

class DepartmentTable extends CoreProductTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    /**
     * fetch all departments 
     * @param boolean $paginated
     * @return mixed
     */
    public function fetchAll($paginated = FALSE, $empMgt = FALSE)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('department');
        $select->where(array('isDeleted' => false));

        if ($empMgt == FALSE) {
            $select->where(array('departmentStatus' => 1));
        }

        $select->order('departmentID DESC');
        if ($paginated) {
            $paginatorAdapter = new DbSelect(
                    $select, $this->tableGateway->getAdapter());
            $paginator = new Paginator($paginatorAdapter);
            return $paginator;
        } else {
            $statement = $sql->prepareStatementForSqlObject($select);
            $result = $statement->execute();
            return $result;
        }
    }

    /**
     * fetch departments that match for given department code 
     * @param string $departmentCode
     * @return mixed
     */
    public function getDepartments($departmentCode)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('department')
                ->columns(array('departmentCode'))
                ->where(array('departmentCode' => $departmentCode, 'isDeleted' => 0));
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if (!$rowset) {
            return NULL;
        }
        return $rowset;
    }

    /**
     * save department
     * @param array $deparmentData
     * @return mixed
     */
    public function saveDepartments($deparmentData)
    {
        $DepartmentData = array(
            'departmentCode' => $deparmentData->departmentCode,
            'departmentName' => $deparmentData->departmentName,
            'departmentStatus' => '1',
        );
        if ($this->tableGateway->insert($DepartmentData)) {
            $result = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
            return $result;
        } else {
            return FALSE;
        }
    }

    /**
     * Return department according to the given department code and not equal 
     * to the given department id
     * @param string $departmentId
     * @param string $departmentCode
     * @return mixed
     */
    public function checkDepartmentCodeValid($departmentId,$departmentCode) 
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('department')
                ->columns(array('*'))               
                ->where(array('departmentCode' => $departmentCode, 'isDeleted' => 0))
                ->where->notEqualTo('departmentID',$departmentId);
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result->count();
    }

    /**
     * update department
     * @param array $deparmentData
     * @return mixed
     */
    public function updateDepartment($departmentData)
    {
        $DepartmentData = array(
            'departmentName' => $departmentData->departmentName,
            'departmentCode' => $departmentData->departmentCode,
        );
        try {
            $this->tableGateway->update($DepartmentData, array('departmentID' => $departmentData->departmentID));
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
        return true;
    }

    /**
     * search department by search key
     * @param string $keyword
     * @return mixed
     */
    public function departmentSearchByKey($keyword)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('department')
                ->columns(array('*'));
        $select->where(new PredicateSet(array(new Operator('departmentName', 'like', '%' . $keyword . '%'), new Operator('departmentCode', 'like', '%' . $keyword . '%')), PredicateSet::OP_OR));
        $select->where(array('isDeleted' => 0));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return (object) $results;
    }

    /**
     * fetch all active departments 
     * @return mixed
     */
    public function fetchActiveDepartments()
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('department');
        $select->order('departmentID DESC');
        $select->where(array('departmentStatus' => 1, 'isDeleted' => false));
        
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }
    
    //get job Type for search
    public function getDepartmentforSearch($keyword)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('department')
                ->columns(array(
                    'posi' => new Expression('LEAST(IF(POSITION(\'' . $keyword . '\' in departmentName )>0,POSITION(\'' . $keyword . '\' in departmentName), 9999),'
                            . 'IF(POSITION(\'' . $keyword . '\' in departmentCode )>0,POSITION(\'' . $keyword . '\' in departmentCode), 9999)) '),
                    'len' => new Expression('LEAST(CHAR_LENGTH(departmentName ), CHAR_LENGTH(departmentCode )) '),
                    '*'));
        $select->where(new PredicateSet(array(new Operator('department.departmentName', 'like', '%' . $keyword . '%'), new Operator('department.departmentCode', 'like', '%' . $keyword . '%')), PredicateSet::OP_OR));
        $select->where(new PredicateSet(array(new Operator('department.departmentStatus', '=', '1'))));
        $select->where(new PredicateSet(array(new Operator('department.isDeleted', '=', '0'))));
        $select->order(new Expression('IF(posi > 0, posi, 9999) ASC, len ASC'));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return (object) $results;
    }

    /**
     * update department delete
     * @param int $departmentID
     * @return mixed
     */
    public function updateDepartmentDelete($departmentID)
    {
        $departmentData = array(
            'isDeleted' => true,
        );
        try {
            $this->tableGateway->update($departmentData, array('departmentID' => $departmentID));
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
        return true;
    }
    /**
     * update department status
     * @param int $departmentID
     * @param boolean $status
     * @return mixed
     */
    public function updateDepartmentState($departmentID, $status)
    {
        $departmentData = array(
            'departmentStatus' => $status,
        );
        try {
            $this->tableGateway->update($departmentData, array('departmentID' => $departmentID));
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
        return true;
    }

    public function getDepartmentByDepartmentID($departmentID) {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('department')
            ->columns(array('*'))
            ->where(array('department.departmentID' => $departmentID));
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if (!$rowset) {
            return NULL;
        }
        return $rowset;
    }

}
