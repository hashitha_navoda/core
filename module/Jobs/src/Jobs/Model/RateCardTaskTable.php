<?php

namespace Jobs\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Jobs\Model\rateCardTask;
use Zend\Db\Sql\Sql;
use Core\Model\ProductTable as CoreProductTable;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Predicate\PredicateSet;
use Zend\Db\Sql\Predicate\Between;
use Zend\Db\Sql\Predicate\Operator;

class RateCardTaskTable extends CoreProductTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    /**
     * save rate card task
     * @param array $rateCardTaskData
     * @return mixed
     */
    public function saveRateCardTask($rateCardTaskData)
    {
        $rateCardTaskData = array(
            'rateCardID' => $rateCardTaskData->rateCardID,
            'taskID' => $rateCardTaskData->taskID,
            'rateCardTaskRatePerUnit' => $rateCardTaskData->rateCardTaskRatePerUnit,
            'uomID' => $rateCardTaskData->uomID,
        );
        if ($this->tableGateway->insert($rateCardTaskData)) {
            $result = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
            return $result;
        } else {
            return FALSE;
        }
    }


    /**
     * fetch rate card related task rates(service rates) by rate card id
     * @param int $rateCardID
     * @return mixed
     */
    public function getRelatedServiceRates($rateCardID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('rateCardTask');
        $select->join('task', 'rateCardTask.taskID=  task.taskID', array("*"), "left");
        $select->join(['uom2' => 'uom'], 'rateCardTask.uomID = uom2.uomID',
            array(
                'rateCardUomId' => new Expression('uom2.uomID'),
                'rateCardUomName' => new Expression('uom2.uomName'),
                'rateCardUomAbbr' => new Expression('uom2.uomAbbr'),
            ),'left');
        
        $select->where(array('rateCardTask.rateCardID' => $rateCardID, 'rateCardTask.isDeleted' => 0));
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if (!$rowset) {
            return NULL;
        }
        return $rowset;
    }


    //update rate card task as deleted by rate card id
    public function updateDeleteRateCardTask($rateCardID)
    {
        $rateCardTaskData = array(
            'isDeleted' => '1',
        );
        try {
            $this->tableGateway->update($rateCardTaskData, array('rateCardID' => $rateCardID));
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
        return true;
    }

}
