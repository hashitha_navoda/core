<?php

namespace Jobs\Model;

use Zend\InputFilter\InputFilterInterface;

class JobCardCost {

	public $jobCardCostID;
	public $jobCardID;
	public $unitCost;
	public $unitRate;
	public $Qty;
	public $jobCostID;
	protected $inputFilter;

	public function exchangeArray($data) {
		
		$this->jobCardCostID = (!empty($data['jobCardCostID'])) ? $data['jobCardCostID'] : null;
		$this->jobCardID = (!empty($data['jobCardID'])) ? $data['jobCardID'] : 0;
		$this->unitCost = (!empty($data['unitCost'])) ? $data['unitCost'] : 0;
		$this->unitRate = (!empty($data['unitRate'])) ? $data['unitRate'] : 0;
		$this->Qty = (!empty($data['Qty'])) ? $data['Qty'] : 0;
		$this->jobCostID = (!empty($data['jobCostID'])) ? $data['jobCostID'] : null;
		
	}

	public function setInputFilter(InputFilterInterface $inputFilter) {
		throw new \Exception("Not used");
	}

	public function getInputFilter() {

	}

	public function getArrayCopy() {
		return get_object_vars($this);
	}

}