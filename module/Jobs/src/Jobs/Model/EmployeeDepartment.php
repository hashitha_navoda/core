<?php

namespace Jobs\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class EmployeeDepartment
{

    public $employeeDepartmentID;
    public $employeeId;
    public $departmentId;
   
    
    public function exchangeArray($data)
    {
        $this->employeeDepartmentID = (!empty($data['employeeDepartmentID'])) ? $data['employeeDepartmentID'] : null;
        $this->employeeId = (!empty($data['employeeId'])) ? $data['employeeId'] : null;
        $this->departmentId = (!empty($data['departmentId'])) ? $data['departmentId'] : null;
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {
        
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

}
