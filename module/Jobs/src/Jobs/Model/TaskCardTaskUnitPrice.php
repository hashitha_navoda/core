<?php

/**
 * @author Ashan Madushka <ashan@thinkcube.com>
 * This file contains Cost Type Table Functions
 */

namespace Jobs\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class TaskCardTaskUnitPrice
{

    public $taskCardTaskUnitPriceID;
    public $taskCardTaskID;
    public $taskID;
    public $rates;
    protected $inputFilter;

    public function exchangeArray($data)
    {
        $this->taskCardTaskUnitPriceID = (!empty($data['taskCardTaskUnitPriceID'])) ? $data['taskCardTaskUnitPriceID'] : null;
        $this->taskCardTaskID = (!empty($data['taskCardTaskID'])) ? $data['taskCardTaskID'] : null;
        $this->taskID = (!empty($data['taskID'])) ? $data['taskID'] : null;
        $this->rates = (!empty($data['rates'])) ? $data['rates'] : null;
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {

    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

}
