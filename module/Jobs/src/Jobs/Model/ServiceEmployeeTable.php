<?php

namespace Jobs\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Predicate\PredicateSet;
use Zend\Db\Sql\Predicate\Operator;

class ServiceEmployeeTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll()
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('serviceEmployee');
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }

    public function saveServiceEmployeeData($data)
    {
        $taskData = array(
            'jobID' => $data->jobID,
            'employeeID' => $data->employeeID,
            'deleted' => '0'
        );
        if ($this->tableGateway->insert($taskData)) {
            $result = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
            return $result;
        } else {
            return FALSE;
        }
    }

    public function updateDeleted($jobID)
    {
        $data = array(
            'deleted' => 1,
        );
        try {
            $this->tableGateway->update($data, array('jobID' => $jobID));
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
        return true;
    }

    public function getRelatedServiceEmployees($jobID)
    {

        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('serviceEmployee');
        $select->where(array('jobID' => $jobID, 'deleted' => 0));

        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if (!$rowset) {
            return NULL;
        }
        return $rowset;
    }
}
