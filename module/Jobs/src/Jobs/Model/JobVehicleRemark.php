<?php

namespace Jobs\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class JobVehicleRemark
{

    public $jobVehicleRemarkID;
    public $jobVehicleID;
    public $remark;
    
    protected $inputFilter;
    
    public function exchangeArray($data)
    {
        $this->jobVehicleRemarkID = (!empty($data['jobVehicleRemarkID'])) ? $data['jobVehicleRemarkID'] : null;
        $this->jobVehicleID = (!empty($data['jobVehicleID'])) ? $data['jobVehicleID'] : null;
        $this->remark = (!empty($data['remark'])) ? $data['remark'] : null;
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {
        
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

}