<?php

namespace Jobs\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Jobs\Model\TaskCard;
use Zend\Db\Sql\Sql;
use Core\Model\ProductTable as CoreProductTable;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Predicate\PredicateSet;
use Zend\Db\Sql\Predicate\Between;
use Zend\Db\Sql\Predicate\Operator;

class TaskCardTable extends CoreProductTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    //get task card by task card code
    public function getTaskCardByTaskCardCode($taskCardCode)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('taskCard')
                ->columns(array("*"))
                ->where(array('taskCard.taskCardCode' => $taskCardCode, "taskCard.deleted" => 0));
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if (!$rowset) {
            return NULL;
        }
        return $rowset;
    }

    //save task card function
    public function saveTaskCard(TaskCard $taskCard)
    {
        $taskData = array(
            'taskCardName' => $taskCard->taskCardName,
            'taskCardCode' => $taskCard->taskCardCode,
            'productID' => $taskCard->productID,
            'deleted' => '0',
        );
        if ($this->tableGateway->insert($taskData)) {
            $result = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
            return $result;
        } else {
            return FALSE;
        }
    }

    public function fetchAll($paginated = FALSE)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('taskCard');
        $select->order('taskCardID ASC');
        $select->join('product','taskCard.productID = product.productID',array('productCode','productName'),'left');
        $select->where(array('deleted' => '0'));
        if ($paginated) {
            $paginatorAdapter = new DbSelect(
                    $select, $this->tableGateway->getAdapter());
            $paginator = new Paginator($paginatorAdapter);
            return $paginator;
        } else {
            $statement = $sql->prepareStatementForSqlObject($select);
            $result = $statement->execute();
            return $result;
        }
    }

    //get Task for search
    public function getTaskCardforSearch($keyword, $type = null)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('taskCard')
                ->columns(array(
                    'posi' => new Expression('LEAST(IF(POSITION(\'' . $keyword . '\' in taskCardName )>0,POSITION(\'' . $keyword . '\' in taskCardName), 9999),'
                            . 'IF(POSITION(\'' . $keyword . '\' in taskCardCode )>0,POSITION(\'' . $keyword . '\' in taskCardCode), 9999)) '),
                    'len' => new Expression('LEAST(CHAR_LENGTH(taskCardName ), CHAR_LENGTH(taskCardCode )) '),
                    '*'));
        $select->where(new PredicateSet(array(new Operator('taskCard.taskCardName', 'like', '%' . $keyword . '%'), new Operator('taskCard.taskCardCode', 'like', '%' . $keyword . '%')), PredicateSet::OP_OR));
        $select->where(new PredicateSet(array(new Operator('deleted', '=', '0'))));
        if ($type != 'jobCard') {
            $select->where(array('status'=> 1));
        }
        $select->order(new Expression('IF(posi > 0, posi, 9999) ASC, len ASC'));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return (object) $results;
    }

    public function getJobCardforSearch($keyword, $vehicleTypeID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('taskCard')
                ->columns(array(
                    'posi' => new Expression('LEAST(IF(POSITION(\'' . $keyword . '\' in taskCardName )>0,POSITION(\'' . $keyword . '\' in taskCardName), 9999),'
                            . 'IF(POSITION(\'' . $keyword . '\' in taskCardCode )>0,POSITION(\'' . $keyword . '\' in taskCardCode), 9999)) '),
                    'len' => new Expression('LEAST(CHAR_LENGTH(taskCardName ), CHAR_LENGTH(taskCardCode )) '),
                    '*'));
        $select->join('taskCardTask', 'taskCard.taskCardID = taskCardTask.taskCardID', array('*'),'left');
        $select->join('jobCardVehicleType', 'jobCardVehicleType.jobCardID = taskCard.taskCardID', array('vehicleTypeID'));
        $select->where(new PredicateSet(array(new Operator('taskCard.taskCardName', 'like', '%' . $keyword . '%'), new Operator('taskCard.taskCardCode', 'like', '%' . $keyword . '%')), PredicateSet::OP_OR));
        $select->where(new PredicateSet(array(new Operator('taskCard.deleted', '=', '0'),new Operator('jobCardVehicleType.deleted', '=', '0'),new Operator('vehicleTypeID', '=', $vehicleTypeID), new Operator('status', '=', 1))));
        $select->where(array('jobCardVehicleType.deleted' => 0));
        $select->order(new Expression('IF(posi > 0, posi, 9999) ASC, len ASC'));
        $select->group('taskCard.taskCardID');
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return (object) $results;
    }

     public function getJobCardforSearchForDashboard($keyword, $vehicleTypeID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('taskCard')
                ->columns(array(
                    'posi' => new Expression('LEAST(IF(POSITION(\'' . $keyword . '\' in taskCardName )>0,POSITION(\'' . $keyword . '\' in taskCardName), 9999),'
                            . 'IF(POSITION(\'' . $keyword . '\' in taskCardCode )>0,POSITION(\'' . $keyword . '\' in taskCardCode), 9999)) '),
                    'len' => new Expression('LEAST(CHAR_LENGTH(taskCardName ), CHAR_LENGTH(taskCardCode )) '),
                    '*'));
        $select->join('taskCardTask', 'taskCard.taskCardID = taskCardTask.taskCardID', array('*'),'left');
        $select->join('jobCardVehicleType', 'jobCardVehicleType.jobCardID = taskCard.taskCardID', array('vehicleTypeID'));
        $select->where(new PredicateSet(array(new Operator('taskCard.taskCardName', 'like', '%' . $keyword . '%'), new Operator('taskCard.taskCardCode', 'like', '%' . $keyword . '%')), PredicateSet::OP_OR));
        $select->where(new PredicateSet(array(new Operator('taskCard.deleted', '=', '0'),new Operator('jobCardVehicleType.deleted', '=', '0'), new Operator('status', '=', 1))));
        $select->where(array('jobCardVehicleType.deleted' => 0));
        $select->order(new Expression('IF(posi > 0, posi, 9999) ASC, len ASC'));
        $select->group('taskCard.taskCardID');
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return (object) $results;
    }

    //update task card as deleted by task card id
    public function updateDeleteTaskCard($taskCardID)
    {
        $taskCardData = array(
            'deleted' => '1',
        );
        try {
            $this->tableGateway->update($taskCardData, array('taskCardID' => $taskCardID));
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
        return true;
    }

    //get task card by task card id
    public function getTaskCardByTaskCardID($taskCardID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('taskCard');
        $select->join('taskCardTask', 'taskCard.taskCardID = taskCardTask.taskCardID', array('*'),'left');
        $select->join('task', 'task.taskID = taskCardTask.taskID', array('*'),'left');
        $select->join('taskCardTaskProduct', 'taskCardTask.taskCardTaskID = taskCardTaskProduct.taskCardTaskID', array('*'),'left');
        $select->join('locationProduct', 'taskCardTaskProduct.locationProductID = locationProduct.locationProductID', array('productID'),'left');
        $select->join('product', 'locationProduct.productID = product.productID', array('productCode', 'productName', 'productTypeID'),'left');
        $select->join('productUom', 'locationProduct.productID = productUom.productID', array('uomID', 'productUomConversion', 'productUomBase'),'left');
        $select->join(['uom1' => 'uom'], 'taskCardTaskProduct.taskCardTaskProductUomID = uom1.uomID',array(
                'tcProductUomId' => new Expression('uom1.uomID'),
                'tcProductUomName' => new Expression('uom1.uomName'),
                'tcProductUomAbbr' => new Expression('uom1.uomAbbr'),
            ),'left');
        $select->join(['uom2' => 'uom'], 'taskCardTask.uomID = uom2.uomID',
            array(
                'taskCardUomId' => new Expression('uom2.uomID'),
                'taskCardUomName' => new Expression('uom2.uomName'),
                'taskCardUomAbbr' => new Expression('uom2.uomAbbr'),
            ),'left');
        $select->where(array('taskCard.taskCardID' => $taskCardID));
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }

    public function getTaskCardByID($id)
    {
        $rowset = $this->tableGateway->select(array('taskCardID' => $id));
        $row = $rowset->current();
        if (!$row) {
            return NULL;
        }
        return $row;
    }

    /**
     * change the task card status
     * @param int $taskCardID
     * @param boolean $status
     * @return mixed
     */
    public function changeStatusID($taskCardID, $status)
    {
        $taskData = array(
            'status' => $status,
        );
        try {
            $this->tableGateway->update($taskData, array('taskCardID' => $taskCardID));
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
        return true;
    }

    /**
     * check availability of task card code
     * @param string $taskCardCode
     * @return boolean
     */
    public function checkTaskCardCodeAvailability($taskCardCode)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('taskCard')
                ->columns(array("*"))
                ->where(array('taskCard.taskCardCode' => $taskCardCode, "taskCard.deleted" => 0));
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if (count($rowset) > 0) {
            return FALSE;
        } else {
            return TRUE;
        }
    }
}
