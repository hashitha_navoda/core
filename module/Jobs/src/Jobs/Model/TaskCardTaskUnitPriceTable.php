<?php

namespace Jobs\Model;

use Core\Model\ProductTable as CoreProductTable;
use Jobs\Model\TaskCardTaskUnitPrice;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Sql;
use Zend\Db\TableGateway\TableGateway;

class TaskCardTaskUnitPriceTable extends CoreProductTable {

	protected $tableGateway;

	public function __construct(TableGateway $tableGateway) {
		$this->tableGateway = $tableGateway;
	}

	
	public function saveTaskRates(TaskCardTaskUnitPrice $taskCardTaskUnitPrice)
    {
        $taskRateData = array(
            'taskCardTaskID' => $taskCardTaskUnitPrice->taskCardTaskID,
            'taskID' => $taskCardTaskUnitPrice->taskID,
            'rates' => $taskCardTaskUnitPrice->rates,
            
        );
        try {
        	$this->tableGateway->insert($taskRateData);
        	return true;
        } catch (Exception $e) {
        	return false;
        }
        
    }


	public function getTaskRatesByTaskCardID($taskCardID) {
		$sql = new Sql($this->tableGateway->getAdapter());
		$select = $sql->select();
		$select->from('taskCardTaskUnitPrice')
			->columns(array('*'))
			->join('taskCardTask', 'taskCardTaskUnitPrice.taskCardTaskID =  taskCardTask.taskCardTaskID', array("taskCardID"), "left")
			->where(array('taskCardTask.taskCardID' => $taskCardID));
		$statement = $sql->prepareStatementForSqlObject($select);
		$rowset = $statement->execute();
		if (!$rowset) {
			return NULL;
		}
		return $rowset;
	}
}