<?php

namespace Jobs\Model;

use Zend\InputFilter\InputFilterInterface;

class JobEmployee {

	public $jobEmployeeID;
	public $jobID;
	public $employeeDesignationID;
	public $jobTaskID;
	protected $inputFilter;

	public function exchangeArray($data) {
		$this->jobEmployeeID = (!empty($data['jobEmployeeID'])) ? $data['jobEmployeeID'] : null;
		$this->jobID = (!empty($data['jobID'])) ? $data['jobID'] : null;
		$this->employeeDesignationID = (!empty($data['employeeDesignationID'])) ? $data['employeeDesignationID'] : null;
		$this->jobTaskID = (!empty($data['jobTaskID'])) ? $data['jobTaskID'] : null;
	}

	public function setInputFilter(InputFilterInterface $inputFilter) {
		throw new \Exception("Not used");
	}

	public function getInputFilter() {

	}

	public function getArrayCopy() {
		return get_object_vars($this);
	}

}
