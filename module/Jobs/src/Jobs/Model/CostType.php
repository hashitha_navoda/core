<?php

/**
 * @author Ashan Madushka <ashan@thinkcube.com>
 * This file contains Cost Type Table Functions
 */

namespace Jobs\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class CostType
{

    public $costTypeID;
    public $costTypeName;
    public $costTypeDescription;
    public $productID;
    public $costTypeMinimumValue;
    public $costTypeMaximumValue;
    public $entityID;
    protected $inputFilter;

    public function exchangeArray($data)
    {
        $this->costTypeID = (!empty($data['costTypeID'])) ? $data['costTypeID'] : null;
        $this->costTypeName = (!empty($data['costTypeName'])) ? $data['costTypeName'] : null;
        $this->costTypeDescription = (!empty($data['costTypeDescription'])) ? $data['costTypeDescription'] : null;
        $this->productID = (!empty($data['productID'])) ? $data['productID'] : null;
        $this->costTypeMinimumValue = (!empty($data['costTypeMinimumValue'])) ? $data['costTypeMinimumValue'] : 0.00;
        $this->costTypeMaximumValue = (!empty($data['costTypeMaximumValue'])) ? $data['costTypeMaximumValue'] : 0.00;
        $this->entityID = (!empty($data['entityID'])) ? $data['entityID'] : null;
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory = new InputFactory();

            $inputFilter->add($factory->createInput(array(
                        'name' => 'costTypeID',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'Int'),
                        ),
            )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'costTypeName',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'StringLength',
                                'options' => array(
                                    'encoding' => 'UTF-8',
                                    'min' => 1,
                                    'max' => 255,
                                ),
                            ),
                        ),
            )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'costTypeDescription',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'StringLength',
                                'options' => array(
                                    'encoding' => 'UTF-8',
                                    'min' => 1,
                                    'max' => 10,
                                ),
                            ),
                        ),
            )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'costItem',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'Int'),
                        ),
            )));

            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

}
