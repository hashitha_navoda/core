<?php

namespace Jobs\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class Department
{

    public $departmentID;
    public $departmentCode;
    public $departmentName;
    
    public function exchangeArray($data)
    {
        $this->departmentID = (!empty($data['departmentID'])) ? $data['departmentID'] : null;
        $this->departmentCode = (!empty($data['departmentCode'])) ? $data['departmentCode'] : null;
        $this->departmentName = (!empty($data['departmentName'])) ? $data['departmentName'] : null;
       
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {
        
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

}