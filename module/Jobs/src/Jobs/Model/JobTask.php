<?php

namespace Jobs\Model;

use Zend\InputFilter\InputFilterInterface;

class JobTask {

	public $jobTaskID;
	public $jobID;
	public $taskID;
	public $uomID;
	public $jobTaskUnitCost;
	public $jobTaskEstQty;
	public $jobTaskStatus;
	public $jobTaskProgress;
	public $jobTaskStartDate;
	public $jobTaskEndDate;
	public $stationID;
	public $jobTaskTaskCardID;
	public $jobTaskInvoicedQty;
	public $jobTaskUsedQty;
	public $jobTaskUnitRate;
	protected $inputFilter;

	public function exchangeArray($data) {
		$this->jobTaskID = (!empty($data['jobTaskID'])) ? $data['jobTaskID'] : null;
		$this->jobID = (!empty($data['jobID'])) ? $data['jobID'] : null;
		$this->taskID = (!empty($data['taskID'])) ? $data['taskID'] : null;
		$this->uomID = (!empty($data['uomID'])) ? $data['uomID'] : null;
		$this->jobTaskUnitCost = (!empty($data['jobTaskUnitCost'])) ? $data['jobTaskUnitCost'] : null;
		$this->jobTaskUnitRate = (!empty($data['jobTaskUnitRate'])) ? $data['jobTaskUnitRate'] : null;
		$this->jobTaskEstQty = (!empty($data['jobTaskEstQty'])) ? $data['jobTaskEstQty'] : null;
		$this->jobTaskStatus = (!empty($data['jobTaskStatus'])) ? $data['jobTaskStatus'] : null;
		$this->jobTaskProgress = (!empty($data['jobTaskProgress'])) ? $data['jobTaskProgress'] : null;
		$this->jobTaskStartDate = (!empty($data['jobTaskStartDate'])) ? $data['jobTaskStartDate'] : null;
		$this->jobTaskEndDate = (!empty($data['jobTaskEndDate'])) ? $data['jobTaskEndDate'] : null;
		$this->stationID = (!empty($data['stationID'])) ? $data['stationID'] : null;
		$this->jobTaskTaskCardID = (!empty($data['jobTaskTaskCardID'])) ? $data['jobTaskTaskCardID'] : null;
		$this->jobTaskInvoicedQty = (!empty($data['jobTaskInvoicedQty'])) ? $data['jobTaskInvoicedQty'] : 0;
		$this->jobTaskUsedQty = (!empty($data['jobTaskUsedQty'])) ? $data['jobTaskUsedQty'] : 0;
	}

	public function setInputFilter(InputFilterInterface $inputFilter) {
		throw new \Exception("Not used");
	}

	public function getInputFilter() {

	}

	public function getArrayCopy() {
		return get_object_vars($this);
	}

}