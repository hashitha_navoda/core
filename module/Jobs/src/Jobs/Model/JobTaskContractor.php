<?php

namespace Jobs\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class JobTaskContractor
{

    public $jobTaskContractorID;
    public $jobTaskID;
    public $contractorID;
    protected $inputFilter;
    
    public function exchangeArray($data)
    {
        $this->jobTaskContractorID = (!empty($data['jobTaskContractorID'])) ? $data['jobTaskContractorID'] : null;
        $this->jobTaskID = (!empty($data['jobTaskID'])) ? $data['jobTaskID'] : null;
        $this->contractorID = (!empty($data['contractorID'])) ? $data['contractorID'] : null;
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {
        
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

}