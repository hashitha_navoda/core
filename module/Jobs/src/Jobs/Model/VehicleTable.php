<?php
namespace Jobs\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Predicate\PredicateSet;
use Zend\Db\Sql\Predicate\Operator;

class VehicleTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    //get all non-deleted vehicles with/out pagination
    public function fetchAllActiveVehicles($paginated = FALSE)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('vehicle');
        $select->order('vehicleCode DESC');
        $select->join('entity', 'vehicle.entityId = entity.entityID', array('deleted'));
        $select->where(array('deleted' => '0'));
        if ($paginated) {
            $paginatorAdapter = new DbSelect(
                    $select, $this->tableGateway->getAdapter());
            $paginator = new Paginator($paginatorAdapter);
            return $paginator;
        } else {
            $statement = $sql->prepareStatementForSqlObject($select);
            $result = $statement->execute();
            return $result;
        }
    }

    //search vehicles by vehicle code/name
    public function searchActiveVehicles($query = null, $paginated = FALSE)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('vehicle');
        $select->join('entity', 'vehicle.entityId = entity.entityID', array('deleted'));
        $select->order('vehicleCode DESC');
        $select->where->equalTo('entity.deleted', 0);
        if($query != NULL){
            $select->where( new PredicateSet(array(
                new Operator('vehicle.vehicleCode', 'like', '%' . $query . '%'),
                new Operator('vehicle.vehicleName', 'like', '%' . $query . '%')),
                PredicateSet::OP_OR ));
            $select->limit(10);
        }    
        if ($paginated) {
            $paginatorAdapter = new DbSelect($select, $this->tableGateway->getAdapter());
            return new Paginator($paginatorAdapter);
        } else {
            $statement = $sql->prepareStatementForSqlObject($select);
            $result = $statement->execute();
            return $result;
        }
    }

    //save vehicle details
    public function saveVehicle(Vehicle $data)
    {
        $vehicleData = array(
            'vehicleName' => $data->vehicleName,
            'vehicleCode' => $data->vehicleCode,
            'vehicleRegistrationNumber' => $data->vehicleRegistrationNumber,
            'vehicleCost' => $data->vehicleCost,
            'entityId' => $data->entityId,
        );
        if ($this->tableGateway->insert($vehicleData)) {
            $result = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
            return $result;
        } else {
            return FALSE;
        }
    }

    //edit vehicle details
    public function editVehicle($data)
    {
        $vehicleId = $data['vehicleId'];
        $vehicleData = array(
            'vehicleName' => $data['vehicleName'],
            'vehicleRegistrationNumber' => $data['vehicleRegistrationNumber'],
            'vehicleCost' => $data['vehicleCost'],
        );

        try {
            $this->tableGateway->update($vehicleData, array('vehicleId' => $vehicleId));
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
        return TRUE;
    }

    //check deletability of vehicle details
    public function vehicleDependability($vehicleId)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('jobVehicle');
        $select->columns(array('*'));
        $select->where(array('vehicleID' => $vehicleId));
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();

        if (count($result) == 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    //check availability of vehicle registration number
    public function checkRegisterNumber($vehRegNum , $vehicleId = null, $editMode = false)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('vehicle');
        $select->columns(array('*'));
        $select->join('entity', 'vehicle.entityId = entity.entityID', array('deleted'));
        $select->where(array('deleted' => '0'));
        $select->where(array('vehicleRegistrationNumber' => $vehRegNum));

        if ($editMode == true) {
            $select->where->notEqualTo('vehicleId',$vehicleId);
        }

        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();

        if (count($result) == 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    //check availability of vehicle code
    public function checkVehicleCode($vehicleCode)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('vehicle');
        $select->columns(array('*'));
        $select->join('entity', 'vehicle.entityId = entity.entityID', array('deleted'));
        $select->where(array('deleted' => '0'));
        $select->where(array('vehicleCode' => $vehicleCode));
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();

        if (count($result) == 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    //change vehicle status
    public function changeVehicleStatus($vehicleId, $vehicleStatus)
    {
        $updatedData = array(
            'vehicleStatus' => $vehicleStatus
        );
        try {
            $this->tableGateway->update($updatedData, array('vehicleId' => $vehicleId));
            return TRUE;
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
            return FALSE;
        }

    }

}
