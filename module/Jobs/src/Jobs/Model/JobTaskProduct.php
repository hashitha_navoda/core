<?php

namespace Jobs\Model;

use Zend\InputFilter\InputFilterInterface;

class JobTaskProduct {

	public $jobTaskProductID;
	public $jobTaskID;
	public $locationProductID;
	public $jobTaskProductQty;
	public $jobProductID;
	protected $inputFilter;

	public function exchangeArray($data) {
		$this->jobTaskProductID = (!empty($data['jobTaskProductID'])) ? $data['jobTaskProductID'] : null;
		$this->jobTaskID = (!empty($data['jobTaskID'])) ? $data['jobTaskID'] : null;
		$this->locationProductID = (!empty($data['locationProductID'])) ? $data['locationProductID'] : null;
		$this->jobTaskProductQty = (!empty($data['jobTaskProductQty'])) ? $data['jobTaskProductQty'] : null;
		$this->jobProductID = (!empty($data['jobProductID'])) ? $data['jobProductID'] : null;
	}

	public function setInputFilter(InputFilterInterface $inputFilter) {
		throw new \Exception("Not used");
	}

	public function getInputFilter() {

	}

	public function getArrayCopy() {
		return get_object_vars($this);
	}

}