<?php

namespace Jobs\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class ServiceProduct
{

    public $serviceProductID;
    public $serviceID;
    public $vehicleTypeID;
    public $locationProductID;
    public $serviceProductType;
    public $serviceProductQty;
    public $serviceProductUomID;
    protected $inputFilter;
    
    public function exchangeArray($data)
    {
        $this->serviceProductID = (!empty($data['serviceProductID'])) ? $data['serviceProductID'] : null;
        $this->serviceID = (!empty($data['serviceID'])) ? $data['serviceID'] : null;
        $this->vehicleTypeID = (!empty($data['vehicleTypeID'])) ? $data['vehicleTypeID'] : null;
        $this->locationProductID = (!empty($data['locationProductID'])) ? $data['locationProductID'] : null;
        $this->serviceProductType = (!empty($data['serviceProductType'])) ? $data['serviceProductType'] : null;
        $this->serviceProductQty = (!empty($data['serviceProductQty'])) ? $data['serviceProductQty'] : null;
        $this->serviceProductUomID = (!empty($data['serviceProductUomID'])) ? $data['serviceProductUomID'] : null;
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {
        
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

}