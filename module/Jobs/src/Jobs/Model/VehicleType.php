<?php

namespace Jobs\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class VehicleType
{

    public $vehicleTypeID;
    public $vehicleTypeName;
    public $vehicleTypeStatus;
    public $entityId;
    
    protected $inputFilter;
    
    public function exchangeArray($data)
    {
        $this->vehicleTypeID = (!empty($data['vehicleTypeID'])) ? $data['vehicleTypeID'] : null;
        $this->vehicleTypeName = (!empty($data['vehicleTypeName'])) ? $data['vehicleTypeName'] : null;
        $this->vehicleTypeStatus = (!empty($data['vehicleTypeStatus'])) ? $data['vehicleTypeStatus'] : null;
        $this->entityId = (!empty($data['entityId'])) ? $data['entityId'] : null;
       
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {
        
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

}