<?php

namespace Jobs\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Jobs\Model\MaterialRequest;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Predicate\PredicateSet;
use Zend\Db\Sql\Predicate\Between;
use Zend\Db\Sql\Predicate\Operator;

class MaterialRequestTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    /**
     * fetch all material requersts
     * @param boolean $paginated
     * @return mixed
     */
    public function all($paginated = FALSE)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('materialRequest');
        $select->join('job', 'materialRequest.jobId = job.jobId', array('jobName'));
        $select->join('project', 'job.projectId = project.projectId', array('projectName'), 'left');
        $select->join('entity', 'materialRequest.entityID = entity.entityID', array('deleted'));
        $select->where(array('deleted' => false));
        $select->order('materialRequestId DESC');
        if ($paginated) {
            $paginatorAdapter = new DbSelect(
                    $select, $this->tableGateway->getAdapter());
            $paginator = new Paginator($paginatorAdapter);
            return $paginator;
        } else {
            $statement = $sql->prepareStatementForSqlObject($select);
            $result = $statement->execute();
            return $result;
        }
    }

    /**
     * search material requersts
     * @param  string  $key       search key
     * @param  string  $fromDate  search from date
     * @param  string  $toDate    search to date
     * @param  string  $fixDate   searchon date
     * @param  boolean $paginated pagination
     * @return mixed
     */
    public function search($key, $fromDate, $toDate, $fixDate, $paginated = true)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('materialRequest');
        $select->join('job', 'materialRequest.jobId = job.jobId', array('jobName'));
        $select->join('project', 'job.projectId = project.projectId', array('projectName'), 'left');
        $select->join('entity', 'materialRequest.entityID = entity.entityID', array('deleted'));
        $select->order('materialRequestDate ASC');
        $select->where(array('deleted' => false));

        if ($fixDate) {
            $select->where(array('materialRequest.materialRequestDate' => $fixDate));
        } elseif ($fromDate && $toDate) {
            $select->where->between('materialRequest.materialRequestDate', $fromDate, $toDate);
        }


        if ($key) {
            $select->where(
                'materialRequestCode like "%' . $key
                . '%" OR jobName like "%' . $key
                . '%" OR projectName like "%' . $key .'%"');
        }

        if ($paginated) {
            $paginatorAdapter = new DbSelect(
                    $select, $this->tableGateway->getAdapter());
            $paginator = new Paginator($paginatorAdapter);
            return $paginator;
        } else {
            $statement = $sql->prepareStatementForSqlObject($select);
            $result = $statement->execute();
            return $result;
        }
    }

    public function updateStatus($requestId, $statusId)
    {
        $reqData = array('materialRequestStatus' => $statusId);
        $condition = array('materialRequestId' => $requestId);
        try {
            $this->tableGateway->update($reqData, $condition);
        } catch (Exception $e) {
            error_log($e->getTraceAsString());
            return false;
        }

        return true;
    }

    /**
     * save material request
     * @param mixed $data
     * @return mixed
     */
    public function save(MaterialRequest $materialRequest)
    {
        $data = array(
            'jobId' => $materialRequest->jobId,
            'materialRequestCode' => $materialRequest->materialRequestCode,
            'materialRequestDate' => $materialRequest->materialRequestDate,
            'materialRequestStatus' => $materialRequest->materialRequestStatus,
            'entityId' => $materialRequest->entityId,
        );
        if ($this->tableGateway->insert($data)) {
            $result = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
            return $result;
        } else {
            return FALSE;
        }
    }

    /**
     * fetch all material requersts
     * @param boolean $paginated
     * @return mixed
     */
    public function getMaterialRequestByJobId($jobId, $paginated = FALSE)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('materialRequest');
        $select->join('job', 'materialRequest.jobId = job.jobId', array('jobName'));
        $select->join('project', 'job.projectId = project.projectId', array('projectName'), 'left');
        $select->join('entity', 'materialRequest.entityID = entity.entityID', array('deleted'));
        $select->where(array('deleted' => false, 'materialRequest.jobId' => $jobId));
        $select->order('materialRequestDate ASC');
        if ($paginated) {
            $paginatorAdapter = new DbSelect(
                    $select, $this->tableGateway->getAdapter());
            $paginator = new Paginator($paginatorAdapter);
            return $paginator;
        } else {
            $statement = $sql->prepareStatementForSqlObject($select);
            $result = $statement->execute();
            return $result;
        }
    }

    /**
     * get material requersts related to job and jobProduct id
     * @param  string  $jobId           job id
     * @param  string  $jobProductId    job product id
     * @return mixed
     */
    public function getTaskByJobs($jobId)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('materialRequest');
        $select->join('materialRequestJobProduct', 'materialRequest.materialRequestId = materialRequestJobProduct.materialRequestId', array('jobProductId', 'requestedQuantity'));
        $select->join('entity', 'materialRequest.entityID = entity.entityID', array('deleted'));
        $select->order('materialRequestDate DESC');
        $select->where(array('deleted' => false));
        $select->where(array('jobId' => $jobId));
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }

    public function getMaterialRequestByMaterialRequestId($materialRequestId)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('materialRequest');
        $select->join('job', 'materialRequest.jobId = job.jobId', array('jobName'));
        $select->join('customer', 'job.customerID = customer.customerID', array("*"), "left");
        $select->join('project', 'job.projectId = project.projectId', array('projectName'), 'left');
        $select->join('materialRequestJobProduct', 'materialRequest.materialRequestId = materialRequestJobProduct.materialRequestId', array('jobProductId', 'requestedQuantity'));
        $select->join('jobProduct', 'materialRequestJobProduct.jobProductId = jobProduct.jobProductID', array('jobProductID','locationProductID','jobProductUnitPrice','uomID','jobProductAllocatedQty','jobProductReOrderLevel','jobProductMaterialTypeID','jobProductTaskCardID','jobProductTaskID' => new Expression('jobProduct.jobTaskID'),'jobProductStatus'),'left');
        $select->join('uom', 'jobProduct.uomID = uom.uomID', array('uomAbbr', 'uomDecimalPlace'),'left');
        $select->join('locationProduct', 'jobProduct.locationProductID = locationProduct.locationProductID', array('productID'),'left');
        $select->join('product', 'locationProduct.productID = product.productID', array('productCode', 'productName', 'productTypeID'),'left');
        $select->join('entity', 'materialRequest.entityID = entity.entityID', array('deleted'));
        $select->where(array('deleted' => false, 'materialRequest.materialRequestId' => $materialRequestId));
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }
}
