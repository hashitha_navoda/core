<?php

namespace Jobs\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class ResourceSetup
{

    public $resourceSetupID;
    public $resourceSetupName;
    public $resourceSetupIsUse;
    protected $inputFilter;
    
    public function exchangeArray($data)
    {
        $this->resourceSetupID = (!empty($data['resourceSetupID'])) ? $data['resourceSetupID'] : null;
        $this->resourceSetupName = (!empty($data['resourceSetupName'])) ? $data['resourceSetupName'] : null;
        $this->resourceSetupIsUse = (!empty($data['resourceSetupIsUse'])) ? $data['resourceSetupIsUse'] : null;
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {
        
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

}