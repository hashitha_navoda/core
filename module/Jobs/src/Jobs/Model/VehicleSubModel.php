<?php

namespace Jobs\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class VehicleSubModel
{

    public $vehicleSubModelID;
    public $vehicleSubModelName;
    public $vehicleModelID;
    
    protected $inputFilter;
    
    public function exchangeArray($data)
    {
        $this->vehicleSubModelID = (!empty($data['vehicleSubModelID'])) ? $data['vehicleSubModelID'] : null;
        $this->vehicleSubModelName = (!empty($data['vehicleSubModelName'])) ? $data['vehicleSubModelName'] : null;
        $this->vehicleModelID = (!empty($data['vehicleModelID'])) ? $data['vehicleModelID'] : null;
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {
        
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

}