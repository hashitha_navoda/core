<?php

namespace Jobs\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class ServiceInceniveRate
{

    public $serviceInceniveRateId;
    public $serviceID;
    public $vehicleTypeID;
    public $serviceRate;
    public $incentive;
    public $incentiveType;
    public $deleted;
    protected $inputFilter;
    
    public function exchangeArray($data)
    {
        $this->serviceInceniveRateId = (!empty($data['serviceInceniveRateId'])) ? $data['serviceInceniveRateId'] : null;
        $this->serviceID = (!empty($data['serviceID'])) ? $data['serviceID'] : null;
        $this->vehicleTypeID = (!empty($data['vehicleTypeID'])) ? $data['vehicleTypeID'] : null;
        $this->serviceRate = (!empty($data['serviceRate'])) ? $data['serviceRate'] : null;
        $this->incentive = (!empty($data['incentive'])) ? $data['incentive'] : null;
        $this->incentiveType = (!empty($data['incentiveType'])) ? $data['incentiveType'] : null;
        $this->deleted = (!empty($data['deleted'])) ? $data['deleted'] : null;
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {
        
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

}