<?php

namespace Jobs\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class ProductJobApproval
{

    public $productJobApprovalID;
    public $jobApprovalID;
    public $jobProductID;
    public $productJobApprovalRequestedQty;
    protected $inputFilter;
    
    public function exchangeArray($data)
    {
        $this->productJobApprovalID = (!empty($data['productJobApprovalID'])) ? $data['productJobApprovalID'] : null;
        $this->jobApprovalID = (!empty($data['jobApprovalID'])) ? $data['jobApprovalID'] : null;
        $this->jobProductID = (!empty($data['jobProductID'])) ? $data['jobProductID'] : null;
        $this->productJobApprovalRequestedQty = (!empty($data['productJobApprovalRequestedQty'])) ? $data['productJobApprovalRequestedQty'] : null;
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {
        
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

}