<?php

namespace Jobs\Model;

use Zend\InputFilter\InputFilterInterface;

class JobUserRole {

	public $jobUserRoleId;
	public $userID;
	public $jobRoleID;

	public function exchangeArray($data) {
		$this->jobUserRoleId = (!empty($data['jobUserRoleId'])) ? $data['jobUserRoleId'] : null;
		$this->userID = (!empty($data['userID'])) ? $data['userID'] : null;
		$this->jobRoleID = (!empty($data['jobRoleID'])) ? $data['jobRoleID'] : null;
	}

	public function setInputFilter(InputFilterInterface $inputFilter) {
		throw new \Exception("Not used");
	}

	public function getInputFilter() {

	}

	public function getArrayCopy() {
		return get_object_vars($this);
	}

}
