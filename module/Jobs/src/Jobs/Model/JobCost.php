<?php

namespace Jobs\Model;

use Zend\InputFilter\InputFilterInterface;

class JobCost {

	public $jobCostID;
	public $jobID;
	
	protected $inputFilter;

	public function exchangeArray($data) {
		$this->jobCostID = (!empty($data['jobCostID'])) ? $data['jobCostID'] : null;
		$this->jobID = (!empty($data['jobID'])) ? $data['jobID'] : null;
		
	}

	public function setInputFilter(InputFilterInterface $inputFilter) {
		throw new \Exception("Not used");
	}

	public function getInputFilter() {

	}

	public function getArrayCopy() {
		return get_object_vars($this);
	}

}