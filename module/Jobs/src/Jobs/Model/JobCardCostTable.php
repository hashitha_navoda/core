<?php

namespace Jobs\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Jobs\Model\JobCardCost;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Select;
use Core\Model\ProductTable as CoreProductTable;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Predicate\PredicateSet;
use Zend\Db\Sql\Predicate\Between;
use Zend\Db\Sql\Predicate\Operator;

class JobCardCostTable extends CoreProductTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function save(JobCardCost $jobCardCost)
    {
    	$saveData = [
    		'jobCardID' => $jobCardCost->jobCardID,
            'unitCost' => $jobCardCost->unitCost,
    		'unitRate' => $jobCardCost->unitRate,
    		'Qty' => $jobCardCost->Qty,
            'jobCostID' => $jobCardCost->jobCostID
    	];

    	try {
    		$this->tableGateway->insert($saveData);
    		return true;
			
    	} catch (Exception $e) {
    		return false;
    		error_log($e->getMessage());
    	}
    }

    /**
    * this function use to get job task cost details for given job cost ID
    * @param int $jobCostID
    * @param int $locationID
    * return mix
    **/
    public function getJobCardByJobCostIDAndLocationID($jobCostID, $locationID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = new Select();
        $select->from("jobCardCost")
                ->columns(array("*"))
                ->join('taskCard', 'jobCardCost.jobCardID = taskCard.taskCardID', array('*'), 'left')
                ->join('product', 'taskCard.productID = product.productID', array('*'), 'left')
                ->join('locationProduct', 'product.productID = locationProduct.productID', array('locationProductID','locationID','locationProductQuantity'), 'left')
                ->where(array('jobCardCost.jobCostID' => $jobCostID, 'locationProduct.locationID' => $locationID));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        if (!$results) {
            return null;
        }
        return $results;
    }


    public function getDefaultJobTaskDetails($jobCostID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = new Select();
        $select->from("jobTaskCost")
                ->columns(array("*"))
                ->join('jobTask', 'jobTaskCost.jobTaskID = jobTask.jobTaskID', array('*'), 'left')
                ->where(array('jobTaskCost.jobCostID' => $jobCostID, 'jobTaskCost.jobTaskID' => 0));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        if (!$results) {
            return null;
        }
        return $results;
    }

    public function getJobCardCostData($jobCardID, $jobCostID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = new Select();
        $select->from("jobCardCost")
                ->columns(array("*"))
                ->join('taskCard', 'jobCardCost.jobCardID = taskCard.taskCardID', array('*'), 'left')
                ->where(array('jobCardCost.jobCostID' => $jobCostID, 'jobCardCost.jobCardID' => $jobCardID));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        if (!$results) {
            return null;
        }

        return $results->current();
    }

}
