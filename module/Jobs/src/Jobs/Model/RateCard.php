<?php

namespace Jobs\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class RateCard
{

    public $rateCardID;
    public $rateCardCode;
    public $rateCardName;
    public $rateCardStatus;
    protected $inputFilter;
    
    public function exchangeArray($data)
    {
        $this->rateCardID = (!empty($data['rateCardID'])) ? $data['rateCardID'] : null;
        $this->rateCardCode = (!empty($data['rateCardCode'])) ? $data['rateCardCode'] : null;
        $this->rateCardName = (!empty($data['rateCardName'])) ? $data['rateCardName'] : null;
        $this->rateCardStatus = (!empty($data['rateCardStatus'])) ? $data['rateCardStatus'] : null;
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {
        
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

}