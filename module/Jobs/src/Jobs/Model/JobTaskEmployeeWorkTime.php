<?php

namespace Jobs\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class JobTaskEmployeeWorkTime
{

    public $jobTaskEmployeeWorkTimeID;
    public $jobTaskEmployeeID;
    public $startedAt;
    public $endedAt;
    protected $inputFilter;
    
    public function exchangeArray($data)
    {
        $this->jobTaskEmployeeWorkTimeID = (!empty($data['jobTaskEmployeeWorkTimeID'])) ? $data['jobTaskEmployeeWorkTimeID'] : null;
        $this->jobTaskEmployeeID = (!empty($data['jobTaskEmployeeID'])) ? $data['jobTaskEmployeeID'] : null;
        $this->startedAt = (!empty($data['startedAt'])) ? $data['startedAt'] : null;
        $this->endedAt = (!empty($data['endedAt'])) ? $data['endedAt'] : null;
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {
        
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

}