<?php

namespace Jobs\Model;

use Core\Model\ProductTable as CoreProductTable;
use Jobs\Model\TaskCardTask;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Sql;
use Zend\Db\TableGateway\TableGateway;

class TaskCardTaskTable extends CoreProductTable {

	protected $tableGateway;

	public function __construct(TableGateway $tableGateway) {
		$this->tableGateway = $tableGateway;
	}

	/**
	 * get task card task by task id
	 */
	public function getTaskCardDetailsByTaskID($ID) {
		$sql = new Sql($this->tableGateway->getAdapter());
		$select = $sql->select();
		$select->from('taskCardTask')
			->columns(array('*'))
			->join('taskCard', 'taskCardTask.taskCardID=  taskCard.taskCardID', array("deleted"), "left")
			->where(array('taskCardTask.taskID' => $ID, 'taskCard.deleted' => 0));
		$statement = $sql->prepareStatementForSqlObject($select);
		$rowset = $statement->execute();
		if (!$rowset) {
			return NULL;
		}
		return $rowset;
	}

	//save task card task function
	public function saveTaskCardTask(TaskCardTask $taskCardTask) {
		$taskCardTaskData = array(
			'taskCardID' => $taskCardTask->taskCardID,
			'taskID' => $taskCardTask->taskID,
			'taskCardTaskRate1' => $taskCardTask->taskCardTaskRate1,
			'taskCardTaskRate2' => $taskCardTask->taskCardTaskRate2,
			'taskCardTaskRate3' => $taskCardTask->taskCardTaskRate3,
			'taskCardTaskIncentiveValue' => $taskCardTask->taskCardTaskIncentiveValue,
			'taskCardTaskIncentiveType' => $taskCardTask->taskCardTaskIncentiveType,
			'uomID' => $taskCardTask->uomID,
		);
		if ($this->tableGateway->insert($taskCardTaskData)) {
			$result = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
			return $result;
		} else {
			return FALSE;
		}
	}

	public function getTaskCardTaskByTaskCardIds($taskCardIds) {
		$sql = new Sql($this->tableGateway->getAdapter());
		$select = new Select();
		$select->from("taskCardTask")
			->columns(array('taskCarduomID' => new Expression('taskCardTask.uomID'), '*'))
			->join("taskCard", "taskCardTask.taskCardID = taskCard.taskCardID", array("deleted"), "left")
			->join('task', 'task.taskID = taskCardTask.taskID', array('*'), 'left')
			->join("taskCardTaskProduct", "taskCardTask.taskCardTaskID = taskCardTaskProduct.taskCardTaskID", array('taskCardTaskIDOfProduct' => new Expression('taskCardTaskProduct.taskCardTaskID'), 'locationProductID', 'taskCardTaskProductQty', 'taskCardTaskProductID'), "left")
			->join('locationProduct', 'taskCardTaskProduct.locationProductID = locationProduct.locationProductID', array('productID', 'locationID'), 'left')
			->join("product", "locationProduct.productID = product.productID", array("*"), "left")
			->join("productHandeling", "product.productID = productHandeling.productID", array("*"), "left")
			->join('productUom', 'product.productID = productUom.productID', array('uomID', 'productUomConversion', 'productUomBase'), 'left')
			->join('uom', 'productUom.uomID = uom.uomID', array('uomAbbr'), 'left')
			->where(array('taskCard.deleted' => 0));
		$select->join(['uom2' => 'uom'], 'taskCardTask.uomID = uom2.uomID', array('taskCardUomAbbr' => new Expression('uom2.uomAbbr'), 'uomName'), 'left');
		if (is_array($taskCardIds)) {
			$select->where->in('taskCardTask.taskCardID', $taskCardIds);
		}
		$statement = $sql->prepareStatementForSqlObject($select);
		$results = $statement->execute();
		if (!$results) {
			return null;
		}
		return $results;
	}

	//get task list by task card id
	public function getTaskList($taskCardID, $taskID = null) {
		$sql = new Sql($this->tableGateway->getAdapter());
		$select = $sql->select();
		$select->from('taskCardTask')
			->columns(array('*'))
			->join('task', 'taskCardTask.taskID=  task.taskID', array("*"), "left")
			->where(array('taskCardTask.taskCardID' => $taskCardID, 'task.deleted' => 0, 'taskCardTask.taskCardTaskDeleted' => 0));
        if (!is_null($taskID)) {
            $select->where(array('taskCardTask.taskID' => $taskID));
        }
		$statement = $sql->prepareStatementForSqlObject($select);
		$rowset = $statement->execute();
		if (!$rowset) {
			return NULL;
		}
		return $rowset;
	}

	/**
     * fetch job card related services
     * @param int $taskCardId
     * @return mixed
     */
    public function getRelatedServices($taskCardId, $mode = false)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('taskCardTask');
        $select->join('task', 'task.taskID = taskCardTask.taskID', array('*'), 'left');
        $select->where(array('taskCardID' => $taskCardId, 'taskCardTaskDeleted' => 0));

        if ($mode) {
            $select->where(array('taskCardTaskStatus' => 1, 'task.status' => 1));
        }
        
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if (!$rowset) {
            return NULL;
        }
        return $rowset;
    }

    /**
     * change the job card status service status
     * @param int $taskCardTaskId
     * @param boolean $status
     * @return boolean
     */
    public function changeJobCardServiceStatus($taskCardTaskId, $status)
    {
        $data = array(
            'taskCardTaskStatus' => $status,
        );
        try {
            $this->tableGateway->update($data, array('taskCardTaskID' => $taskCardTaskId));
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
        return true;
    }

    //update task card as deleted by task card id
    public function updateDeleteTaskCardTask($taskCardID)
    {
        $data = array(
            'taskCardTaskDeleted' => '1',
        );
        try {
            $this->tableGateway->update($data, array('taskCardID' => $taskCardID));
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
        return true;
    }

    /**
     * delete job card service
     * @param int $taskCardTaskID
     * @return mixed
     */
    public function deleteJobCardServiceByTaskCardTaskID($taskCardTaskID)
    {
        $taskCardTaskData = array(
            'taskCardTaskDeleted' => 1,
        );
        try {
            $this->tableGateway->update($taskCardTaskData, array('taskCardTaskID' => $taskCardTaskID));
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
        return true;
    }

    /**
     * update job card service
     * @param int $taskCardTaskID
     * @return mixed
     */
    public function updateTaskCardTask($data, $taskCardTaskID)
    {
        try {
            $this->tableGateway->update($data, array('taskCardTaskID' => $taskCardTaskID));
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
        return true;
    }

	/**
     * fetch job card related products
     * @param int $taskCardId
     * @param int $taskId
     * @return mixed
     */
    public function getTaskCardTaskProducts($taskID, $taskCardId)
    {

        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('taskCardTask');
        $select->join("taskCardTaskProduct", "taskCardTask.taskCardTaskID = taskCardTaskProduct.taskCardTaskID", array('taskCardTaskIDOfProduct' => new Expression('taskCardTaskProduct.taskCardTaskID'), 'locationProductID', 'taskCardTaskProductQty', 'taskCardTaskProductID', 'taskCardTaskProductUomID', 'taskCardTaskProductType'), "left");
        $select->join('locationProduct', 'taskCardTaskProduct.locationProductID = locationProduct.locationProductID', array('productID', 'locationID'), 'left');
		$select->join("product", "locationProduct.productID = product.productID", array("*"), "left");
		$select->join("productHandeling", "product.productID = productHandeling.productID", array("*"), "left");
		$select->join('productUom', 'product.productID = productUom.productID', array('uomID', 'productUomConversion', 'productUomBase'), 'left');
		$select->join('uom', 'productUom.uomID = uom.uomID', array('uomAbbr'), 'left');
 
        $select->where(array('taskID' => $taskID, 'taskCardID' => $taskCardId));
        
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if (!$rowset) {
            return NULL;
        }
        return $rowset;
    }

	public function getTaskCardTaskWithRates($taskCardID) {
		$sql = new Sql($this->tableGateway->getAdapter());
		$select = new Select();
		$select->from("taskCardTask")
			->columns(array('*'))
			->join("taskCardTaskUnitPrice", "taskCardTask.taskCardTaskID = taskCardTaskUnitPrice.taskCardTaskID", array("rates"), "left");
		$select->where->in('taskCardTask.taskCardID', $taskCardID);
			
		
		
		$statement = $sql->prepareStatementForSqlObject($select);
		$results = $statement->execute();
		if (!$results) {
			return null;
		}
		return $results;
	}
    public function getByTaskIds($taskIds)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = new Select();
        $select->from('taskCardTask')
            ->columns(array('*'))
            ->join('taskCard', 'taskCardTask.taskCardID=  taskCard.taskCardID', array('taskCardCode', 'taskCardName'), "left")
             ->join('task', 'taskCardTask.taskID=  task.taskID', array("*"), "left")
             ->where(array('task.deleted' => 0, 'taskCardTask.taskCardTaskDeleted' => 0,'taskCard.status' => 1))
            ->where->in('taskCardTask.taskID', $taskIds);
        $statement = $sql->prepareStatementForSqlObject($select);
         $results = $statement->execute();
         if (!$results) {
             return null;
         }
         return $results;
    }

    public function getTaskCardServiceIncentivesByTaskCardID($taskCardID, $jobCardIncentiveRateID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('taskCardTask')->columns(array("*"));
        $select->join('task', 'task.taskID = taskCardTask.taskID', array('*'),'left');
        if (!empty($jobCardIncentiveRateID)) {
            $select->join('jobCardServiceIncentiveRate', 'jobCardServiceIncentiveRate.serviceID = task.taskID', array('*'),'left');
            $select->where(array('taskCardTask.taskCardID' => $taskCardID, 'taskCardTask.taskCardTaskDeleted' => 0, 'jobCardServiceIncentiveRate.deleted' => 0, 'jobCardServiceIncentiveRate.jobCardIncentiveRateID' => $jobCardIncentiveRateID));
        } else {
            $select->where(array('taskCardTask.taskCardID' => $taskCardID, 'taskCardTask.taskCardTaskDeleted' => 0));
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;



    }
}
