<?php
namespace Jobs\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Predicate\PredicateSet;
use Zend\Db\Sql\Predicate\Operator;

class ContractorTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    //contractor details
    public function fetchAll($paginated = FALSE)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('contractor');
        $select->order('contractorID DESC');
        $select->join('entity', 'contractor.entityID = entity.entityID', array('deleted'));
        $select->where(array('deleted' => '0'));
        if ($paginated) {
            $paginatorAdapter = new DbSelect(
            $select, $this->tableGateway->getAdapter());
            $paginator = new Paginator($paginatorAdapter);
            return $paginator;
        } else {
            $statement = $sql->prepareStatementForSqlObject($select);
            $result = $statement->execute();
            return $result;
        }
    }

    //Insert contractor details.
    public function saveContractor($data)
    {

        if ($this->tableGateway->insert($data)) {
            $result = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
            return $result;
        } else {
            return FALSE;
        }
    }

    //update contractor details
    public function editContractor($data, $contractorID)
    {
        try {
            $this->tableGateway->update($data, array('contractorID' => $contractorID));
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
            return FALSE;
        }
        return TRUE;
    }
    
    //check deletability of contractor details
    public function contractorDependability($contractorID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('contractor');
        $select->columns(array('*'));
        $select->where(array('contractorID' => $contractorID));
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();

        if (count($result) == 0) {
            return FALSE;
        } else {
            return $result;
        }
    }

    // search department
    public function contractorSearchByKey($keyword)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('contractor')
                ->columns(array('*'));
        $select->join('entity', 'contractor.entityID = entity.entityID', array('deleted'));
        $select->where(array('deleted' => '0'));
        $select->where(new PredicateSet(array(new Operator('contractorFirstName', 'like', '%' . $keyword . '%'))));

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return (object) $results;
    }

    //change contractor status
    public function changeContracterStatus($contractorID, $contractorStatus)
    {
        $updatedData = array(
            'contractorStatus' => $contractorStatus
        );
        try {
            $this->tableGateway->update($updatedData, array('contractorID' => $contractorID));
            return TRUE;
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
            return FALSE;
        }

    }
}
