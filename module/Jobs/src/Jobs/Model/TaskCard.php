<?php

namespace Jobs\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class TaskCard
{

    public $taskCardID;
    public $taskCardName;
    public $taskCardCode;
    public $productID;
    public $deleted;
    protected $inputFilter;
    
    public function exchangeArray($data)
    {
        $this->taskCardID = (!empty($data['taskCardID'])) ? $data['taskCardID'] : null;
        $this->taskCardName = (!empty($data['taskCardName'])) ? $data['taskCardName'] : null;
        $this->taskCardCode = (!empty($data['taskCardCode'])) ? $data['taskCardCode'] : null;
        $this->productID = (!empty($data['productID'])) ? $data['productID'] : null;
        $this->deleted = (!empty($data['deleted'])) ? $data['deleted'] : null;
    }

    public function getTaskCardCodeValidate ($code, $table) {
        $result = $table->getTaskCardByTaskCardCode($code);
        if ($result->count() > 0) {
            return false;
        } else {
            return true;
        }
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {
        
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

}
