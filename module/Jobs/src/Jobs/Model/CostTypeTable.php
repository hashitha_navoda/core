<?php

namespace Jobs\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Predicate\PredicateSet;
use Zend\Db\Sql\Predicate\Operator;

class CostTypeTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll($paginated = FALSE)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('costType');
        $select->order('costTypeName DESC');
        $select->join('product', 'product.productID = costType.productID', array('productCode', 'productName'), 'left');
        $select->join('entity', 'costType.entityID = entity.entityID', array('deleted'));
        $select->where(array('entity.deleted' => '0'));
        if ($paginated) {
            $paginatorAdapter = new DbSelect(
                    $select, $this->tableGateway->getAdapter());
            $paginator = new Paginator($paginatorAdapter);
            return $paginator;
        } else {
            $statement = $sql->prepareStatementForSqlObject($select);
            $result = $statement->execute();
            return $result;
        }
    }

    //save cost type function
    public function saveCostType(CostType $costType)
    {
        $CostTypeData = array(
            'costTypeName' => $costType->costTypeName,
            'costTypeDescription' => $costType->costTypeDescription,
            'productID' => $costType->productID,
            'costTypeMinimumValue' => $costType->costTypeMinimumValue,
            'costTypeMaximumValue' => $costType->costTypeMaximumValue,
            'entityID' => $costType->entityID,
        );
        if ($this->tableGateway->insert($CostTypeData)) {
            $result = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
            return $result;
        } else {
            return FALSE;
        }
    }

    //get cost type by cost type name
    public function getCostTypeByCostTypeName($costTypeName)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('costType')
                ->columns(array("*"))
                ->join('entity', 'costType.entityID=  entity.entityID', array("*"), "left")
                ->where(array('costType.costTypeName' => $costTypeName, "entity.deleted" => 0));
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if (!$rowset) {
            return NULL;
        }
        return $rowset;
    }

    //update cost type by cost type id
    public function updateCostType(CostType $CostType)
    {
        $costTypeData = array(
            'costTypeDescription' => $CostType->costTypeDescription,
            'productID' => $CostType->productID,
            'costTypeMinimumValue' => $CostType->costTypeMinimumValue,
            'costTypeMaximumValue' => $CostType->costTypeMaximumValue,
        );
        try {
            $this->tableGateway->update($costTypeData, array('costTypeID' => $CostType->costTypeID));
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
        return true;
    }

    //get cost Type for search
    public function getCostTypeforSearch($keyword)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('costType')
                ->columns(array(
                    'posi' => new Expression('LEAST(IF(POSITION(\'' . $keyword . '\' in costTypeName )>0,POSITION(\'' . $keyword . '\' in costTypeName), 9999),'
                            . 'IF(POSITION(\'' . $keyword . '\' in costTypeDescription )>0,POSITION(\'' . $keyword . '\' in costTypeDescription), 9999)) '),
                    'len' => new Expression('LEAST(CHAR_LENGTH(costTypeName ), CHAR_LENGTH(costTypeDescription)) '),
                    '*'))
                ->join('entity', 'costType.entityID = entity.entityID', array('*'), 'left')
                ->join('product', 'product.productID = costType.productID', array('productCode', 'productName'), 'left');
        $select->where(new PredicateSet(array(new Operator('costType.costTypeName', 'like', '%' . $keyword . '%'), new Operator('costType.costTypeDescription', 'like', '%' . $keyword . '%')), PredicateSet::OP_OR));
        $select->where(new PredicateSet(array(new Operator('entity.deleted', '=', '0'))));
        $select->order(new Expression('IF(posi > 0, posi, 9999) ASC, len ASC'));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return (object) $results;
    }

    //delete cost type by cost type id
    public function deleteCostTypeByCostTypeID($costTypeID)
    {
        try {
            $result = $this->tableGateway->delete(array('costTypeId' => $costTypeID));
            return $result;
        } catch (\Exception $e) {
            $error = 0;
            return $error;
        }
    }

    //get cost type by cost type id
    public function getCostTypeByCostTypeID($costTybeID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('costType')
                ->columns(array("*"))
                ->join('entity', 'costType.entityID=  entity.entityID', array("*"), "left")
                ->where(array('costType.costTypeID' => $costTybeID, "entity.deleted" => 0));
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if (!$rowset) {
            return NULL;
        }
        return $rowset;
    }

    public function searchCostTypeForDropdown($searchKey)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('costType');
        $select->join('entity', 'costType.entityID=  entity.entityID', array("*"), "left");
        $select->where(array("entity.deleted" => 0, "costType.costTypeStatus" => 1));
        $select->where->like('costTypeName', '%' . $searchKey . '%');
        $select->limit(50);
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

    public function updateCostTypeState($costTypeID, $status)
    {
        $costTypeData = array(
            'costTypeStatus' => $status,
        );
        try {
            $this->tableGateway->update($costTypeData, array('costTypeID' => $costTypeID));
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
        return true;
    }

}
