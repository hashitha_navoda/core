<?php

namespace Jobs\Model;

use Zend\InputFilter\InputFilterInterface;

class VehicleStatusCheck {

	public $vehicleStatusCheckID;
	public $status;
	
	protected $inputFilter;

	public function exchangeArray($data) {
		$this->vehicleStatusCheckID = (!empty($data['vehicleStatusCheckID'])) ? $data['vehicleStatusCheckID'] : null;
		$this->status = (!empty($data['status'])) ? $data['status'] : null;
	}

	public function setInputFilter(InputFilterInterface $inputFilter) {
		throw new \Exception("Not used");
	}

	public function getInputFilter() {

	}

	public function getArrayCopy() {
		return get_object_vars($this);
	}

}