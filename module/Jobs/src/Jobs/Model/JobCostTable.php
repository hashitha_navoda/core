<?php

namespace Jobs\Model;

use Core\Model\ProductTable as CoreProductTable;
use Zend\Db\TableGateway\TableGateway;
use Jobs\Model\JobCost;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Predicate\PredicateSet;
use Zend\Db\Sql\Predicate\Between;
use Zend\Db\Sql\Predicate\Operator;

class JobCostTable extends CoreProductTable {

	protected $tableGateway;

	public function __construct(TableGateway $tableGateway) {
		$this->tableGateway = $tableGateway;
	}

	public function save(JobCost $jobCost) {
		
		$jobCostSaveData = [
			'jobID' => $jobCost->jobID,
		];
			
		try {
			$this->tableGateway->insert($jobCostSaveData);
			return $this->tableGateway->adapter->getDriver()->getLastGeneratedValue('jobCostID');
		} catch (Exception $e) {
			return null;
		}
		
	}

	public function updateJobCost($jobCostData,$jobCostID)
    {
        try {
            $this->tableGateway->update($jobCostData, array('jobCostID' => $jobCostID));
            return true;
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
            return false;
        }
    }

    public function getJobCostIDByJobID($jobID) {
    	$sql = new Sql($this->tableGateway->getAdapter());
        $select = new Select();
        $select->from("jobCost")
                ->columns(array("*"))
                ->where(array('jobCost.jobID' => $jobID));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        if (!$results) {
            return null;
        }
        return $results;
    }

}
