<?php

namespace Jobs\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Predicate\PredicateSet;
use Zend\Db\Sql\Predicate\Operator;
use Jobs\Model\Designation;
use Core\Model\ProductTable as CoreProductTable;

class DepartmentStationTable extends CoreProductTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }


    /**
     * save department station
     * @param array $departmentStationData
     * @return mixed
     */
    public function saveDepartmentStation($departmentStationData)
    {
        $departmentStationData = array(
            'departmentStationCode' => $departmentStationData->departmentStationCode,
            'departmentStationName' => $departmentStationData->departmentStationName,
            'departmentID' => $departmentStationData->departmentId,
            'departmentStationStatus' => '1',
        );
        if ($this->tableGateway->insert($departmentStationData)) {
            $result = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
            return $result;
        } else {
            return FALSE;
        }
    }

    /**
     * update delete column 
     * @param int $oldId(departmentId)
     * @return mixed
     */
    public function updateOldDeparmentIdRelateStation($oldId)
    {
        $departmentStationData = array(
            'deleted' => true,
        );
        try {
            $this->tableGateway->update($departmentStationData, array('departmentId' => $oldId));
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
        return true;
    }

    /**
     * fetch department related stations by department Id
     * @param int $departmentId
     * @return mixed
     */
    public function getDepartmentRelatedStations($departmentId)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('departmentStation');

        if (!is_null($departmentId)) {
            $select->where(array('departmentId' => $departmentId, 'deleted' => 0));
        } else {
            $select->where(array('deleted' => 0));
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if (!$rowset) {
            return NULL;
        }
        return $rowset;
    }

    /**
     * change the station status
     * @param int $departmentID
     * @param string $stationCode
     * @param boolean $status
     * @return mixed
     */
    public function changeStationStatus($departmentID, $stationCode, $status)
    {
        $departmentStationData = array(
            'departmentStationStatus' => $status,
        );
        try {
            $this->tableGateway->update($departmentStationData, array('departmentId' => $departmentID, 'departmentStationCode' => $stationCode));
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
        return true;
    }

    /**
     * delete station by station code and department Id
     * @param int $departmentID
     * @param int $stationCode
     * @return mixed
     */
    public function deleteStationByStationCode($departmentID, $stationCode)
    {
        $departmentStationData = array(
            'deleted' => 1,
        );
        try {
            $this->tableGateway->update($departmentStationData, array('departmentId' => $departmentID, 'departmentStationCode' => $stationCode));
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
        return true;
    }
   

}
