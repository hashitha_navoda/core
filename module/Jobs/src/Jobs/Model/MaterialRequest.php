<?php

namespace Jobs\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class MaterialRequest
{

    public $materialRequestId;
    public $jobId;
    public $materialRequestCode;
    public $materialRequestDate;
    public $materialRequestStatus;
    public $entityId;

    public function exchangeArray($data)
    {
        $this->materialRequestId = (!empty($data['materialRequestId'])) ? $data['materialRequestId'] : null;
        $this->jobId = (!empty($data['jobId'])) ? $data['jobId'] : null;
        $this->materialRequestCode = (!empty($data['materialRequestCode'])) ? $data['materialRequestCode'] : null;
        $this->materialRequestDate = (!empty($data['materialRequestDate'])) ? $data['materialRequestDate'] : null;
        $this->materialRequestStatus = (!empty($data['materialRequestStatus'])) ? $data['materialRequestStatus'] : null;
        $this->entityId = (!empty($data['entityId'])) ? $data['entityId'] : null;
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {

    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

}
