<?php

namespace Jobs\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Jobs\Model\JobVehicle;
use Zend\Db\Sql\Sql;
use Core\Model\ProductTable as CoreProductTable;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Predicate\PredicateSet;
use Zend\Db\Sql\Predicate\Between;
use Zend\Db\Sql\Predicate\Operator;

class JobVehicleRemarkTable extends CoreProductTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }


    public function saveJobVehicleRemark($jobVehicleData) {
		$jobVehicleSaveData = array(
			'jobVehicleID' => $jobVehicleData->jobVehicleID,
            'remark' => $jobVehicleData->remark
		);
		if ($this->tableGateway->insert($jobVehicleSaveData)) {
			$result = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
			return $result;
		} else {
			return FALSE;
		}
	}

	/**
     * get job vehicle by job id
     * @param int $jobId
     * @param int $vehicleId
     * @return mixed
     */
    public function getJobVehicleRemarksByJobVehicleID($jobVehicleID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('jobVehicleRemark');
        $select->columns(array("*"));
        $select->where(array('jobVehicleRemark.jobVehicleID' => $jobVehicleID));
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if (!$rowset) {
            return NULL;
        }
        return $rowset;
    }
}
