<?php

namespace Jobs\Model;

use Zend\InputFilter\InputFilterInterface;

class JobProduct {

	public $jobProductID;
	public $jobID;
	public $locationProductID;
	public $jobProductUnitPrice;
	public $uomID;
	public $jobProductAllocatedQty;
	public $jobProductEstimatedQty;
	public $jobProductRequestedQty;
	public $jobProductUsedQty;
	public $jobProductIssuedQty;
	public $jobProductReOrderLevel;
	public $jobProductMaterialTypeID;
	public $jobProductStatus;
	public $jobProductTaskCardID;
	public $jobProductInvoicedQty;
	public $jobProductDescription;
	public $jobTaskID;
	protected $inputFilter;

	public function exchangeArray($data) {
		$this->jobProductID = (!empty($data['jobProductID'])) ? $data['jobProductID'] : null;
		$this->jobID = (!empty($data['jobID'])) ? $data['jobID'] : null;
		$this->locationProductID = (!empty($data['locationProductID'])) ? $data['locationProductID'] : null;
		$this->jobProductUnitPrice = (!empty($data['jobProductUnitPrice'])) ? $data['jobProductUnitPrice'] : null;
		$this->uomID = (!empty($data['uomID'])) ? $data['uomID'] : null;
		$this->jobProductAllocatedQty = (!empty($data['jobProductAllocatedQty'])) ? $data['jobProductAllocatedQty'] : null;
		$this->jobProductEstimatedQty = (!empty($data['jobProductEstimatedQty'])) ? $data['jobProductEstimatedQty'] : null;
		$this->jobProductRequestedQty = (!empty($data['jobProductRequestedQty'])) ? $data['jobProductRequestedQty'] : null;
		$this->jobProductUsedQty = (!empty($data['jobProductUsedQty'])) ? $data['jobProductUsedQty'] : null;
		$this->jobProductIssuedQty = (!empty($data['jobProductIssuedQty'])) ? $data['jobProductIssuedQty'] : null;
		$this->jobProductReOrderLevel = (!empty($data['jobProductReOrderLevel'])) ? $data['jobProductReOrderLevel'] : null;
		$this->jobProductMaterialTypeID = (!empty($data['jobProductMaterialTypeID'])) ? $data['jobProductMaterialTypeID'] : null;
		$this->jobProductStatus = (!empty($data['jobProductStatus'])) ? $data['jobProductStatus'] : null;
		$this->jobProductTaskCardID = (!empty($data['jobProductTaskCardID'])) ? $data['jobProductTaskCardID'] : null;
		$this->jobProductInvoicedQty = (!empty($data['jobProductInvoicedQty'])) ? $data['jobProductInvoicedQty'] : 0;
		$this->jobTaskID = (!empty($data['jobTaskID'])) ? $data['jobTaskID'] : null;
		$this->jobProductDescription = (!empty($data['jobProductDescription'])) ? $data['jobProductDescription'] : null;
	}

	public function setInputFilter(InputFilterInterface $inputFilter) {
		throw new \Exception("Not used");
	}

	public function getInputFilter() {

	}

	public function getArrayCopy() {
		return get_object_vars($this);
	}

}