<?php

namespace Jobs\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Sql;

class JobUserRoleTable {

	protected $tableGateway;

	public function __construct(TableGateway $tableGateway) {
		$this->tableGateway = $tableGateway;
	}

	public function fetchAll() {
		$adapter = $this->tableGateway->getAdapter();
		$sql = new Sql($adapter);
		$select = $sql->select();
		$select->from('jobUserRole');
		$statement = $sql->prepareStatementForSqlObject($select);
		$result = $statement->execute();
		return $result;
	}

	public function saveJobUserRole($jobUserRoleData) {
		$jobUserRoleSaveData = array(
			'userID' => $jobUserRoleData->userID,
			'jobRoleID' => $jobUserRoleData->jobRoleID,
		);
		if ($this->tableGateway->insert($jobUserRoleSaveData)) {
			$result = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue('jobUserRoleId');
			return $result;
		} else {
			return FALSE;
		}
	}

	public function deleteJobUserRoles() {
		try {
			$result = $this->tableGateway->delete('1');
			return $result;
		} catch (\Exception $e) {
			$error = 0;
			return $error;
		}
	}

	public function getJobUserRoleByUserID($userID) {
		$sql = new Sql($this->tableGateway->getAdapter());
		$select = $sql->select();
		$select->from('jobUserRole')
			->columns(array('*'))
			->where(array('jobUserRole.userID' => $userID));
		$statement = $sql->prepareStatementForSqlObject($select);
		$rowset = $statement->execute();
		if (!$rowset) {
			return NULL;
		}
		return $rowset;
	}
}
