<?php

namespace Jobs\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class ServiceSubTask
{

    public $subTaskId;
    public $subTaskCode;
    public $subTaskName;
    public $taskId;
    
    
    public function exchangeArray($data)
    {
        $this->subTaskId = (!empty($data['subTaskId'])) ? $data['subTaskId'] : null;
        $this->subTaskCode = (!empty($data['subTaskCode'])) ? $data['subTaskCode'] : null;
        $this->subTaskName = (!empty($data['subTaskName'])) ? $data['subTaskName'] : null;
        $this->taskId = (!empty($data['taskId'])) ? $data['taskId'] : null;
       
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {
        
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

}
