<?php

namespace Jobs\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Jobs\Model\JobTaskEmployee;
use Zend\Db\Sql\Sql;
use Core\Model\ProductTable as CoreProductTable;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Predicate\PredicateSet;
use Zend\Db\Sql\Predicate\Between;
use Zend\Db\Sql\Predicate\Operator;

class JobTaskEmployeeTable extends CoreProductTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    /**
     * save job task employee 
     * @param array $jobTaskEmployeeData
     * @return mixed
     */
    public function saveJobTaskEmployee($jobTaskEmployeeData) {
		$jobTaskEmployeeSaveData = array(
			'employeeID' => $jobTaskEmployeeData->employeeID,
			'taskID' => $jobTaskEmployeeData->taskID,
			'jobTaskID' => $jobTaskEmployeeData->jobTaskID,
			'isChanged' => 0
		);
		if ($this->tableGateway->insert($jobTaskEmployeeSaveData)) {
			$result = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
			return $result;
		} else {
			return FALSE;
		}
	}

	/**
     * fetch all job task related employees jobtasak id
     * @param int $jobTaskID
     * @return mixed
     */
	public function getJobTaskRelatedEmployees($jobTaskID) {
		$sql = new Sql($this->tableGateway->getAdapter());
		$select = $sql->select();
		$select->from('jobTaskEmployee')
			->columns(array('*'))
			->join('jobTaskEmployeeWorkTime', 'jobTaskEmployee.jobTaskEmployeeID=  jobTaskEmployeeWorkTime.jobTaskEmployeeID', array("*"), "left")
			->where(array('jobTaskEmployee.isChanged' => 0, 'jobTaskEmployee.jobTaskID' => $jobTaskID));

		$statement = $sql->prepareStatementForSqlObject($select);
		$rowset = $statement->execute();
		if (!$rowset) {
			return NULL;
		}
		return $rowset;
	}


	/**
     * update JobTaskEmployee isChanged Flag
     * @param int $jobTaskEmployeeID
     * @param boolean $state
     * @return boolean
     */
    public function updateIsChangedFlag($jobTaskEmployeeID, $state)
    {
        $jobTaskEmployeeData = array(
            'isChanged' => $state
        );
        try {
            $this->tableGateway->update($jobTaskEmployeeData, array('jobTaskEmployeeID' => $jobTaskEmployeeID));
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
        return true;
    }

    /**
     * update job task employee incentive 
     * @param int $employeeID
     * @param int $jobTaskID
     * @param int $incentiveValue
     * @return boolean
     */
    public function updateJobTaskEmployeeIncentive($employeeID, $jobTaskID, $incentiveValue)
    {
        $jobTaskEmployeeData = array(
            'incentiveValue' => $incentiveValue
        );
        try {
            $this->tableGateway->update($jobTaskEmployeeData, array('employeeID' => $employeeID, 'jobTaskID' => $jobTaskID));
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
        return true;
    }

	/**
     * fetch employee job service work time 
     * @param int $departmentID
     * @param int $employeeID
     * @param date $toDate
     * @param date $fromDate
     * @param boolean $unique
     * @return mixed
     */
	public function getEmployeeWorkTimeListByEmployeeID($departmentID, $employeeID, $toDate, $fromDate, $unique =false) {
		$sql = new Sql($this->tableGateway->getAdapter());
		$select = $sql->select();
		$select->from('jobTaskEmployee')
			->columns(array('*'))
			->join('jobTask', 'jobTaskEmployee.jobTaskID=  jobTask.jobTaskID', array("jobID", "jobTaskTaskCardID", "jobTaskID"), "left")
			->join('task', 'task.taskID = jobTask.taskID', array('taskCode','taskName'),'left')
			->join('job', 'jobTask.jobID=  job.jobID', array("*"), "left")
			->join('jobVehicle', 'job.jobId =  jobVehicle.jobId', array("vehicleID"), "left")
			->join('serviceVehicle', 'jobVehicle.vehicleID =  serviceVehicle.serviceVehicleID', array('serviceVehicleID', 'vehicleType', 'vehicleModel', 'serviceVehicleRegNo', 'serviceVehicleEngineNo'), "left")
        	->join('vehicleModel', "serviceVehicle.vehicleModel= vehicleModel.vehicleModelID", array('vehicleModelName'), 'left')
			->join('jobTaskEmployeeWorkTime', 'jobTaskEmployee.jobTaskEmployeeID=  jobTaskEmployeeWorkTime.jobTaskEmployeeID', array("*"), "left");

			if (is_null($departmentID)) {
				$select->where(array('jobTaskEmployee.employeeID' => $employeeID));
			} else {
				$select->where(array('jobTaskEmployee.employeeID' => $employeeID, 'task.departmentID' => $departmentID));
			}

		if (!is_null($fromDate) && !is_null($toDate)) {
			$select->where->between('jobTaskEmployeeWorkTime.startedAt', $fromDate, $toDate);
			$select->where->between('jobTaskEmployeeWorkTime.endedAt', $fromDate, $toDate);
		}

		if ($unique) {
			$select->group('jobTaskEmployee.employeeID');
			$select->group('jobTaskEmployee.jobTaskID');
		}

		$statement = $sql->prepareStatementForSqlObject($select);
		$rowset = $statement->execute();
		if (!$rowset) {
			return NULL;
		}
		return $rowset;
	}


	/**
     * fetch all job service that relate with employee
     * @param int $departmentID
     * @param int $employeeID
     * @param date $toDate
     * @param date $fromDate
     * @return mixed
     */
	public function getJobTasksByEmployeeID($departmentID, $employeeID, $toDate, $fromDate) {
		$sql = new Sql($this->tableGateway->getAdapter());
		$select = $sql->select();
		$select->from('jobTaskEmployee')
			->columns(array('*'))
			->join('jobTask', 'jobTaskEmployee.jobTaskID=  jobTask.jobTaskID', array("*"), "left")
			->join('task', 'task.taskID = jobTask.taskID', array('taskCode','taskName'),'left')
			->join('jobTaskEmployeeWorkTime', 'jobTaskEmployee.jobTaskEmployeeID=  jobTaskEmployeeWorkTime.jobTaskEmployeeID', array("*"), "left")
			->where(array('jobTaskEmployee.employeeID' => $employeeID));
		if (!is_null($fromDate) && !is_null($toDate)) {
			$select->where->between('jobTaskEmployeeWorkTime.startedAt', $fromDate, $toDate);
			$select->where->between('jobTaskEmployeeWorkTime.endedAt', $fromDate, $toDate);
		}
		if (!is_null($departmentID)) {
			$select->where(array('task.departmentID' => $departmentID));
		} 

		$select->group('jobTask.jobTaskID');
		$statement = $sql->prepareStatementForSqlObject($select);
		$rowset = $statement->execute();
		if (!$rowset) {
			return NULL;
		}
		return $rowset;
	}

	/**
     * fetch all job task related employees jobtasak id
     * @param int $jobTaskID
     * @return mixed
     */
	public function getAllJobTaskRelatedEmployees($jobTaskID, $unique = false) {
		$sql = new Sql($this->tableGateway->getAdapter());
		$select = $sql->select();
		$select->from('jobTaskEmployee')
			->columns(array('*'))
			->join('jobTaskEmployeeWorkTime', 'jobTaskEmployee.jobTaskEmployeeID=  jobTaskEmployeeWorkTime.jobTaskEmployeeID', array("*"), "left")
			->join('employee', 'jobTaskEmployee.employeeID=  employee.employeeID', array("*"), "left")
			->where(array('jobTaskEmployee.jobTaskID' => $jobTaskID, 'jobTaskEmployee.isChanged' => 0));
		if($unique) {
			$select->group('jobTaskEmployee.employeeID');
		}

		$statement = $sql->prepareStatementForSqlObject($select);
		$rowset = $statement->execute();
		if (!$rowset) {
			return NULL;
		}
		return $rowset;
	}

	public function deleteJobTaskEmployeeByJobTaskID($jobTaskEmployeeID) {
		try {
			$result = $this->tableGateway->delete(array('jobTaskEmployeeID' => $jobTaskEmployeeID));
			return $result;
		} catch (\Exception $e) {
			$error = 0;
			return $error;
		}
	}

	/**
     * update JobTaskEmployee incentive by jobtaskemployee id
     * @param int $jobTaskEmployeeID
     * @param boolean $incentive value
     * @return boolean
     */
    public function updateJobTaskEmployeeIncentiveVal($jobTaskEmployeeID, $incentiveValue)
    {
        $jobTaskEmployeeData = array(
            'incentiveValue' => $incentiveValue
        );
        try {
            $this->tableGateway->update($jobTaskEmployeeData, array('jobTaskEmployeeID' => $jobTaskEmployeeID));
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
        return true;
    }

    public function getJobTaskEmployee($jobTaskID, $employeeID) {
		$sql = new Sql($this->tableGateway->getAdapter());
		$select = $sql->select();
		$select->from('jobTaskEmployee')
			->columns(array('*'))
			->where(array('jobTaskEmployee.employeeID' => $employeeID, 'jobTaskEmployee.jobTaskID' => $jobTaskID));
		$statement = $sql->prepareStatementForSqlObject($select);
		$rowset = $statement->execute();
		if (!$rowset) {
			return NULL;
		}
		return $rowset->current();
	}

	public function getJobTaskEmployeeByTaskIDEmpID($taskID, $employeeID, $fromDate, $toDate, $locIds) {


		// $sql = new Sql($this->tableGateway->getAdapter());
		// $select = $sql->select();
		// $select->from('jobTaskEmployee')
		// 	->columns(array('*'))
		// 	->join('jobTask', 'jobTaskEmployee.jobTaskID=  jobTask.jobTaskID', array("jobID", "jobTaskTaskCardID", "jobTaskID"), "left")
		// 	->join('task', 'task.taskID = jobTask.taskID', array('taskCode','taskName'),'left')
		// 	->join('job', 'jobTask.jobID=  job.jobID', array("*"), "left")
		// 	->join('jobVehicle', 'job.jobId =  jobVehicle.jobId', array("vehicleID"), "left")
		// 	->join('serviceVehicle', 'jobVehicle.vehicleID =  serviceVehicle.serviceVehicleID', array('serviceVehicleID', 'vehicleType', 'vehicleModel', 'serviceVehicleRegNo', 'serviceVehicleEngineNo'), "left")
  //       	->join('vehicleModel', "serviceVehicle.vehicleModel= vehicleModel.vehicleModelID", array('vehicleModelName'), 'left')
		// 	->join('jobTaskEmployeeWorkTime', 'jobTaskEmployee.jobTaskEmployeeID=  jobTaskEmployeeWorkTime.jobTaskEmployeeID', array("*"), "left");

		// 	$select->where(array('jobTaskEmployee.employeeID' => $employeeID, 'jobTaskEmployee.taskID' => $taskID, 'jobTaskEmployee.isChanged' => 1));

		// if (!is_null($fromDate) && !is_null($toDate)) {
		// 	$select->where->between('jobTaskEmployeeWorkTime.startedAt', $fromDate, $toDate);
		// 	$select->where->between('jobTaskEmployeeWorkTime.endedAt', $fromDate, $toDate);
		// }

		// $select->group('jobTaskEmployee.employeeID');
		// $select->group('jobTaskEmployee.jobTaskID');
		

		// $statement = $sql->prepareStatementForSqlObject($select);
		// $rowset = $statement->execute();
		// if (!$rowset) {
		// 	return NULL;
		// }
		// return $rowset;







































		$sql = new Sql($this->tableGateway->getAdapter());
		$select = $sql->select();
		$select->from('jobTaskEmployee')
			->columns(array('*'))
			->join('jobTask', 'jobTaskEmployee.jobTaskID=  jobTask.jobTaskID', array("*"), "left")
			->join('job', 'jobTask.jobID=  job.jobID', array("locationID"), "left")
			->join('jobTaskEmployeeWorkTime', 'jobTaskEmployee.jobTaskEmployeeID=  jobTaskEmployeeWorkTime.jobTaskEmployeeID', array("*"), "left")
			->where(array('jobTaskEmployee.employeeID' => $employeeID, 'jobTaskEmployee.taskID' => $taskID, 'jobTaskEmployee.isChanged' => 0));
		if (!is_null($fromDate) && !is_null($toDate)) {
			$select->where->between('jobTaskEmployeeWorkTime.startedAt', $fromDate, $toDate);
			$select->where->between('jobTaskEmployeeWorkTime.endedAt', $fromDate, $toDate);
		}

		if ($locIds != null) {
            $select->where->in('job.locationID', $locIds);
        }

		$select->group('jobTaskEmployee.employeeID');
		$select->group('jobTaskEmployee.jobTaskID');
		$statement = $sql->prepareStatementForSqlObject($select);
		$rowset = $statement->execute();
		if (!$rowset) {
			return NULL;
		}
		return $rowset;
	}


	/**
     * update job task employee incentive 
     * @param int $employeeID
     * @param int $jobTaskID
     * @param int $incentiveValue
     * @return boolean
     */
    public function updateJobTaskEmployeeWithNewEmpID($newemployeeID, $oldemployeeID)
    {
        $jobTaskEmployeeData = array(
            'employeeID' => $newemployeeID
        );
        try {
            $this->tableGateway->update($jobTaskEmployeeData, array('employeeID' => $oldemployeeID));
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
        return true;
    }

    public function deleteJobTaskEmployeeRecord($jobTaskID) {
		try {
			$result = $this->tableGateway->delete(array('jobTaskID' => $jobTaskID));
			return $result;
		} catch (\Exception $e) {
			$error = 0;
			return $error;
		}
	}

	/**
     * fetch all job task related employees jobtasak id
     * @param int $jobTaskID
     * @return mixed
     */
	public function getJobTaskRelatedEmployeesByJobTaskID($jobTaskID) {
		$sql = new Sql($this->tableGateway->getAdapter());
		$select = $sql->select();
		$select->from('jobTaskEmployee')
			->columns(array('*'))
			->join('jobTaskEmployeeWorkTime', 'jobTaskEmployee.jobTaskEmployeeID=  jobTaskEmployeeWorkTime.jobTaskEmployeeID', array("*"), "left")
			->join('employee', 'jobTaskEmployee.employeeID=  employee.employeeID', array("*"), "left")
			->where(array('jobTaskEmployee.jobTaskID' => $jobTaskID, 'jobTaskEmployee.isChanged' => 0));
		
		$statement = $sql->prepareStatementForSqlObject($select);
		$rowset = $statement->execute();
		if (!$rowset) {
			return NULL;
		}
		return $rowset;
	}
}
