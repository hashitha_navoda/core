<?php

namespace Jobs\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Jobs\Model\JobProductCost;
use Zend\Db\Sql\Sql;
use Core\Model\ProductTable as CoreProductTable;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Predicate\PredicateSet;
use Zend\Db\Sql\Predicate\Between;
use Zend\Db\Sql\Predicate\Operator;
use Zend\Db\Sql\Select;

class JobTempMaterialTable extends CoreProductTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function save($data)
    {
    	$saveData = [
    		'jobID' => $data->jobID,
    		'locationProductID' => $data->locationProductID,
    		'productID' => $data->productID,
            'issueQty' => $data->issueQty,
            'unitPrice' => $data->unitPrice,
            'productUomID' => $data->productUomID,
            'productDescription' => $data->productDescription
        ];

    	try {
    		$this->tableGateway->insert($saveData);
    		return true;
			
    	} catch (Exception $e) {
    		return false;
    		error_log($e->getMessage());
    	}
    }

    public function deleteMaterialByJobID($jobID) {
        try {
            $result = $this->tableGateway->delete(array('jobID' => $jobID));
            return $result;
        } catch (\Exception $e) {
            $error = 0;
            return $error;
        }
    }

    public function getJobRelatedMaterials($jobID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('jobTempMaterial')
            ->columns(array('*', 'itemDes' => 'productDescription'))
            ->join('locationProduct', 'jobTempMaterial.locationProductID=  locationProduct.locationProductID', array("*"), "left")
            ->join('product', 'locationProduct.productID=  product.productID', array("*"), "left")
            ->where(array('jobTempMaterial.jobID' => $jobID));
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if (!$rowset) {
            return NULL;
        }
        return $rowset;
    }
}
