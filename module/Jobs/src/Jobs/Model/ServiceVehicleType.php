<?php

namespace Jobs\Model;

use Zend\InputFilter\InputFilterInterface;

class ServiceVehicleType {

	public $serviceVehicleTypeId;
	public $serviceID;
	public $vehicleTypeID;
	public $deleted;
	
	protected $inputFilter;

	public function exchangeArray($data) {
		$this->serviceVehicleTypeId = (!empty($data['serviceVehicleTypeId'])) ? $data['serviceVehicleTypeId'] : null;
		$this->serviceID = (!empty($data['serviceID'])) ? $data['serviceID'] : null;
		$this->vehicleTypeID = (!empty($data['vehicleTypeID'])) ? $data['vehicleTypeID'] : null;
		$this->deleted = (!empty($data['deleted'])) ? $data['deleted'] : null;
	}

	public function setInputFilter(InputFilterInterface $inputFilter) {
		throw new \Exception("Not used");
	}

	public function getInputFilter() {

	}

	public function getArrayCopy() {
		return get_object_vars($this);
	}

}