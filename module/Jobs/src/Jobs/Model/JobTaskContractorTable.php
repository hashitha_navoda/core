<?php

namespace Jobs\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Jobs\Model\JobTaskContractor;
use Zend\Db\Sql\Sql;
use Core\Model\ProductTable as CoreProductTable;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Predicate\PredicateSet;
use Zend\Db\Sql\Predicate\Between;
use Zend\Db\Sql\Predicate\Operator;

class JobTaskContractorTable extends CoreProductTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function saveJobTaskContractor($jobTaskContractorData) {
		$jobTaskContractorSaveData = array(
			'contractorID' => $jobTaskContractorData->contractorID,
			'jobTaskID' => $jobTaskContractorData->jobTaskID,
		);
		if ($this->tableGateway->insert($jobTaskContractorSaveData)) {
			$result = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue('jobTaskContractorID');
			return $result;
		} else {
			return FALSE;
		}
	}

	public function deleteJobTaskContractorByJobTaskID($jobTaskID) {
		try {
			$result = $this->tableGateway->delete(array('jobTaskID' => $jobTaskID));
			return $result;
		} catch (\Exception $e) {
			$error = 0;
			return $error;
		}
	}
}
