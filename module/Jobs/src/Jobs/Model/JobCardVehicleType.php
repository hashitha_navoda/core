<?php

namespace Jobs\Model;

use Zend\InputFilter\InputFilterInterface;

class JobCardVehicleType {

	public $jobCardVehicleTypeID;
	public $jobCardID;
	public $vehicleTypeID;
	public $deleted;
	
	protected $inputFilter;

	public function exchangeArray($data) {
		$this->jobCardVehicleTypeID = (!empty($data['jobCardVehicleTypeID'])) ? $data['jobCardVehicleTypeID'] : null;
		$this->jobCardID = (!empty($data['jobCardID'])) ? $data['jobCardID'] : null;
		$this->vehicleTypeID = (!empty($data['vehicleTypeID'])) ? $data['vehicleTypeID'] : null;
		$this->deleted = (!empty($data['deleted'])) ? $data['deleted'] : null;
	}

	public function setInputFilter(InputFilterInterface $inputFilter) {
		throw new \Exception("Not used");
	}

	public function getInputFilter() {

	}

	public function getArrayCopy() {
		return get_object_vars($this);
	}

}