<?php

namespace Jobs\Model;

use Core\Model\ProductTable as CoreProductTable;
use Zend\Db\TableGateway\TableGateway;

class JobTaskProductTable extends CoreProductTable {

	protected $tableGateway;

	public function __construct(TableGateway $tableGateway) {
		$this->tableGateway = $tableGateway;
	}

	public function saveJobTaskProduct($jobTaskproductData) {
		$jobTaskProductSaveData = array(
			'jobTaskID' => $jobTaskproductData->jobTaskID,
			'locationProductID' => $jobTaskproductData->locationProductID,
			'jobTaskProductQty' => $jobTaskproductData->jobTaskProductQty,
			'jobProductID' => $jobTaskproductData->jobProductID,
		);
		if ($this->tableGateway->insert($jobTaskProductSaveData)) {
			$result = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue('jobTaskProductID');
			return $result;
		} else {
			return FALSE;
		}
	}

}
