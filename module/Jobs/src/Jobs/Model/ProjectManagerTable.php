<?php

namespace Jobs\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Jobs\Model\ProjectManager;
use Zend\Db\Sql\Sql;
use Core\Model\ProductTable as CoreProductTable;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Predicate\PredicateSet;
use Zend\Db\Sql\Predicate\Between;
use Zend\Db\Sql\Predicate\Operator;

class ProjectManagerTable extends CoreProductTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    /**
     * save projectManager record 
     * @param array $projectManagerData
     * @return mixed
     */
    public function saveProjectManager($projectManagerData)
    {
        $projectManagerData = array(
            'projectID' => $projectManagerData->projectID,
            'employeeID' => $projectManagerData->employeeID,
            'participantTypeID' => $projectManagerData->participantTypeID,
            'participantSubTypeID' => $projectManagerData->participantSubTypeID,
            'teamID' => $projectManagerData->teamID
        );

        if ($this->tableGateway->insert($projectManagerData)) {
            $result = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
            return true;
        } else {
            return FALSE;
        }
    }

    /**
     * fetch employees that match for given projectID 
     * @param string $id
     * @return mixed
     */
    public function getProjectManagersDetailsByProjectID($id)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('projectManagers')
                ->columns(array('*'))
                ->join('employee', 'projectManagers.employeeID = employee.employeeID', array('*'), 'left')
                ->join('employeeDesignation', 'employee.employeeID = employeeDesignation.employeeID', array('*'), 'left')
                ->join('designation', 'employeeDesignation.designationID = designation.designationID', array('*'), 'left')
                ->where(array('projectManagers.projectID' => $id, 'employee.employeeStatus' => true, 'projectManagers.isDeleted' => 0, 'designation.designationCode' => 'DES001', 'employeeDesignation.isDeleted' => 0));
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();

        if (!$rowset) {
            return NULL;
        }
        return $rowset;
    }

     public function getTournamentOfficialsByProjectID($id)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('projectManagers')
                ->columns(array('*'))
                ->join('employee', 'projectManagers.employeeID = employee.employeeID', array('*'), 'left')
                ->join('department', 'projectManagers.participantTypeID = department.departmentID', array('departmentName','departmentCode'), 'left')
                ->join('designation', 'projectManagers.participantSubTypeID = designation.designationID', array('designationCode','designationName'), 'left')
                ->where(array('projectManagers.projectID' => $id, 'employee.employeeStatus' => true, 'projectManagers.isDeleted' => false));
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if (!$rowset) {
            return NULL;
        }
        return $rowset;
    }


    /**
     * delete projectManager that match for given projectManagerID 
     * @param string $projectManagerID
     * @return mixed
     */
    public function deleteProjectManagerByprojectManagerID($projectManagerID)
    {
        $projectManagerData = array(
            'isDeleted' => true,
        );
        try {
            $this->tableGateway->update($projectManagerData, array('projectManagerID' => $projectManagerID));
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
        return true;
    }

    /**
     * check Employee that match for given employeeID used in project as projectmanager 
     * @param string $employeeId
     * @return mixed
     */
    public function checkEmployeeUsed($employeeId)
    {
    	$sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('projectManagers')
                ->columns(array('*'))
                ->join('project', 'projectManagers.projectID = project.projectId', array('*'), 'left')
                ->join('entity', 'project.entityID = entity.entityID', array('*'), 'left')
                ->where(array('projectManagers.employeeID' => $employeeId, 'projectManagers.isDeleted' => 0, 'entity.deleted' => 0));
        $select->where->notEqualTo('project.projectStatus',10);  
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();

        if ($rowset->count() > 0) {
            return false;
        }
        return true;

    }

    /**
     * update projectManager with updated employeeID
     * @param int $oldId
     * @param int $newId
     * @return mixed
     */
    public function updateProjectManagersTableWithNewEmpId($oldId, $newId)
    {
        $EmployeesData = array(
            'employeeID' => $newId,
        );
        try {
            $this->tableGateway->update($EmployeesData, array('employeeID' => $oldId));
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
        return true;
    }

    /**
     * fetch inactive employees that match for given projectID 
     * @param string $id
     * @return mixed
     */
    public function getInactiveProjectManagers($id)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('projectManagers')
                ->columns(array('*'))
                ->join('employee', 'projectManagers.employeeID = employee.employeeID', array('*'), 'left')
                ->join('entity', 'employee.entityID = entity.entityID', array('*'), 'left')
                ->where(array('projectManagers.projectID' => $id, 'employee.employeeStatus' => false, 'entity.deleted' => 0));
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();

        if (!$rowset) {
            return NULL;
        }
        return $rowset;
    }

}
