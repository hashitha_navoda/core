<?php

namespace Jobs\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Predicate\PredicateSet;
use Zend\Db\Sql\Predicate\Operator;
use Jobs\Model\Designation;
use Core\Model\ProductTable as CoreProductTable;

class ServiceSubTaskTable extends CoreProductTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }


    /**
     * save sub task
     * @param array $subTaskData
     * @return mixed
     */
    public function saveSubTask($subTaskData)
    {
        $subTskData = array(
            'subTaskCode' => $subTaskData->subTaskCode,
            'subTaskName' => $subTaskData->subTaskName,
            'taskId' => $subTaskData->taskId,
            'subTaskStatus' => '1',
        );
        if ($this->tableGateway->insert($subTskData)) {
            $result = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
            return $result;
        } else {
            return FALSE;
        }
    }

    /**
     * update delete column 
     * @param int $taskId
     * @return mixed
     */
    public function updateOldServiceSubTasks($taskId)
    {
        $subTaskData = array(
            'deleted' => true,
        );
        try {
            $this->tableGateway->update($subTaskData, array('taskId' => $taskId));
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
        return true;
    }

    /**
     * fetch service related sub tasks
     * @param int $taskId
     * @return mixed
     */
    public function getRelatedSubTasks($taskId, $mode = false)
    {

        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('serviceSubTask');

        if (!is_null($taskId)) {
            $select->where(array('taskId' => $taskId, 'deleted' => 0));
        } else {
            $select->where(array('deleted' => 0));
        }

        if($mode) {
            $select->where(array('subTaskStatus' => 1));
        }

        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if (!$rowset) {
            return NULL;
        }
        return $rowset;
    }

    /**
     * change the sub task status
     * @param int $subTaskId
     * @param boolean $status
     * @return boolean
     */
    public function changeSubTaskStatus($subTaskId, $status)
    {
        $subTaskData = array(
            'subTaskStatus' => $status,
        );
        try {
            $this->tableGateway->update($subTaskData, array('subTaskId' => $subTaskId));
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
        return true;
    }

    /**
     * delete sub task
     * @param int $subTaskId
     * @return mixed
     */
    public function deleteSubTask($subTaskId)
    {
        $subTaskData = array(
            'deleted' => 1,
        );
        try {
            $this->tableGateway->update($subTaskData, array('subTaskId' => $subTaskId));
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
        return true;
    }
   

}
