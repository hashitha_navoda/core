<?php

namespace Jobs\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class EmployeeTeam
{

    public $employeeTeamID;
    public $employeeID;
    public $teamID;
    public $participantTypeID;
    public $participantSubTypeID;
    protected $inputFilter;
    
    public function exchangeArray($data)
    {
        $this->employeeTeamID = (!empty($data['employeeTeamID'])) ? $data['employeeTeamID'] : null;
        $this->employeeID = (!empty($data['employeeID'])) ? $data['employeeID'] : null;
        $this->teamID = (!empty($data['teamID'])) ? $data['teamID'] : null;
        $this->participantTypeID = (!empty($data['participantTypeID'])) ? $data['participantTypeID'] : null;
        $this->participantSubTypeID = (!empty($data['participantSubTypeID'])) ? $data['participantSubTypeID'] : null;
       
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {
        
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

}