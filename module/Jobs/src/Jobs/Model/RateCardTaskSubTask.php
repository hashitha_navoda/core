<?php

namespace Jobs\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class RateCardTaskSubTask
{

    public $rateCardTaskSubTaskID;
    public $serviceInceniveRateId;
    public $subTaskID;
    public $rateCardTaskSubTaskRate;
    public $uomID;
    protected $inputFilter;
    
    public function exchangeArray($data)
    {
        $this->rateCardTaskSubTaskID = (!empty($data['rateCardTaskSubTaskID'])) ? $data['rateCardTaskSubTaskID'] : null;
        $this->serviceInceniveRateId = (!empty($data['serviceInceniveRateId'])) ? $data['serviceInceniveRateId'] : null;
        $this->subTaskID = (!empty($data['subTaskID'])) ? $data['subTaskID'] : null;
        $this->rateCardTaskSubTaskRate = (!empty($data['rateCardTaskSubTaskRate'])) ? $data['rateCardTaskSubTaskRate'] : null;
        $this->uomID = (!empty($data['uomID'])) ? $data['uomID'] : null;
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {
        
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

}