<?php

namespace Jobs\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Jobs\Model\TaskCardTaskProduct;
use Zend\Db\Sql\Sql;
use Core\Model\ProductTable as CoreProductTable;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Predicate\PredicateSet;
use Zend\Db\Sql\Predicate\Between;
use Zend\Db\Sql\Predicate\Operator;

class ServiceProductTable extends CoreProductTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

	//save task card task products function
    public function saveTaskCardTaskProducts($TaskCardTaskProduct)
    {
        $taskCardTaskProductData = array(
            'serviceID' => $TaskCardTaskProduct->serviceID,
            'vehicleTypeID' => $TaskCardTaskProduct->vehicleTypeID,
            'locationProductID' => $TaskCardTaskProduct->locationProductID,
            'serviceProductType' => $TaskCardTaskProduct->serviceProductType,
            'serviceProductQty' => $TaskCardTaskProduct->serviceProductQty,
            'serviceProductUomID' => $TaskCardTaskProduct->serviceProductUomID
        );
        if ($this->tableGateway->insert($taskCardTaskProductData)) {
            $result = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
            return $result;
        } else {
            return FALSE;
        }
    }

    //get product list by task card task id
    public function getProductList($serviceID, $vehicleTypeID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('serviceProduct')
            ->columns(array('*'))
            ->join('uom', 'serviceProduct.serviceProductUomID = uom.uomID', array("*"), "left")
            ->join('locationProduct', 'locationProduct.locationProductID = serviceProduct.locationProductID', array('productID'), 'left')
            ->join('product', 'locationProduct.productID = product.productID', array("*"), 'left')
            ->where(array('serviceProduct.serviceID' => $serviceID, 'serviceProduct.vehicleTypeID' => $vehicleTypeID));
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if (!$rowset) {
            return NULL;
        }
        return $rowset;
    }


    //delete product by task card task product id
    public function deleteProduct($serviceProductID)
    {
        try {
            $this->tableGateway->delete(array('serviceProductID' => $serviceProductID));
            return TRUE;
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
            return FALSE;
        }
    }

    //update task card task products function
    public function updateTaskCardTaskProducts($data, $id)
    {
        try {
            $this->tableGateway->update($data, array('serviceProductID' => $id));
            return $id;
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
            return FALSE;
        }
    }
}
