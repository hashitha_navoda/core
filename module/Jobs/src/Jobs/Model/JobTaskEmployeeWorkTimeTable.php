<?php

namespace Jobs\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Jobs\Model\JobTaskEmployee;
use Zend\Db\Sql\Sql;
use Core\Model\ProductTable as CoreProductTable;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Predicate\PredicateSet;
use Zend\Db\Sql\Predicate\Between;
use Zend\Db\Sql\Predicate\Operator;

class JobTaskEmployeeWorkTimeTable extends CoreProductTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    /**
     * save job task employee work time
     * @param int $jobTaskEmployeeWorkTimeId
     * @param string $endedAt
     * @return boolean
     */
    public function saveJobTaskEmployeeWorkTime($jobTaskEmployeeWorkTimeData) {
		$jobTaskEmployeeWorkTimeSaveData = array(
			'jobTaskEmployeeID' => $jobTaskEmployeeWorkTimeData->jobTaskEmployeeID,
			'startedAt' => $jobTaskEmployeeWorkTimeData->startedAt,
			'endedAt' => $jobTaskEmployeeWorkTimeData->endedAt,
		);

		if ($this->tableGateway->insert($jobTaskEmployeeWorkTimeSaveData)) {
			$result = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
			return $result;
		} else {
			return FALSE;
		}
	}

	/**
     * update JobTaskEmployee work End time stamp
     * @param int $jobTaskEmployeeWorkTimeId
     * @param string $endedAt
     * @return boolean
     */
    public function updateEndDateTime($jobTaskEmployeeWorkTimeId, $endedAt)
    {
        $jobTaskEmployeeWorkTimeData = array(
            'endedAt' => $endedAt
        );
        try {
            $this->tableGateway->update($jobTaskEmployeeWorkTimeData, array('jobTaskEmployeeWorkTimeId' => $jobTaskEmployeeWorkTimeId));
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
        return true;
    }
}
