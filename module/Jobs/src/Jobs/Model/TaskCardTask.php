<?php

namespace Jobs\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class TaskCardTask
{

    public $taskCardTaskID;
    public $taskCardID;
    public $taskID;
    public $taskCardTaskRate1;
    public $taskCardTaskRate2;
    public $taskCardTaskRate3;
    public $taskCardTaskIncentiveValue;
    public $taskCardTaskIncentiveType;
    public $uomID;
    protected $inputFilter;
    
    public function exchangeArray($data)
    {
        $this->taskCardTaskID = (!empty($data['taskCardTaskID'])) ? $data['taskCardTaskID'] : null;
        $this->taskCardID = (!empty($data['taskCardID'])) ? $data['taskCardID'] : null;
        $this->taskID = (!empty($data['taskID'])) ? $data['taskID'] : null;
        $this->taskCardTaskRate1 = (!empty($data['taskCardTaskRate1'])) ? $data['taskCardTaskRate1'] : 0.00;
        $this->taskCardTaskRate2 = (!empty($data['taskCardTaskRate2'])) ? $data['taskCardTaskRate2'] : 0.00;
        $this->taskCardTaskRate3 = (!empty($data['taskCardTaskRate3'])) ? $data['taskCardTaskRate3'] : 0.00;
        $this->taskCardTaskIncentiveValue = (!empty($data['taskCardTaskIncentiveValue'])) ? $data['taskCardTaskIncentiveValue'] : null;
        $this->taskCardTaskIncentiveType = (!empty($data['taskCardTaskIncentiveType'])) ? $data['taskCardTaskIncentiveType'] : 1;
        $this->uomID = (!empty($data['uomID'])) ? $data['uomID'] : null;
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {
        
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

}