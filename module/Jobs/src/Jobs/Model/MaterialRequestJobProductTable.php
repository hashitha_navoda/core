<?php

namespace Jobs\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Jobs\Model\MaterialRequestJobProduct;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Predicate\PredicateSet;
use Zend\Db\Sql\Predicate\Between;
use Zend\Db\Sql\Predicate\Operator;

class MaterialRequestJobProductTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    /**
     * find material requerst job products by material request id
     * @param integer $requestId
     * @return mixed
     */
    public function findByRequestId($requestId)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('materialRequestJobProduct');
        $select->where(array('materialRequestId' => $requestId));
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();

        return $result;
    }

    /**
     * save material request job products
     * @param mixed $data
     * @return mixed
     */
    public function save(MaterialRequestJobProduct $materialRequestJobProduct)
    {
        $data = array(
            'materialRequestId' => $materialRequestJobProduct->materialRequestId,
            'jobProductId' => $materialRequestJobProduct->jobProductId,
            'requestedQuantity' => $materialRequestJobProduct->requestedQuantity,
        );
        if ($this->tableGateway->insert($data)) {
            $result = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
            return $result;
        } else {
            return FALSE;
        }
    }



    /**
     * check whether Product Related Material Requests are Approve
     * @param type $jobProductId
     * @return mixed
     */
    public function checkProductRelatedRequestsApproved($jobProductId)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('materialRequestJobProduct')
                ->columns(array('*'))
                ->join('materialRequest', 'materialRequestJobProduct.materialRequestId = materialRequest.materialRequestId', array('*'))
                ->join('entity', 'materialRequest.entityId=  entity.entityID', array("deleted"), "left")
                ->where(array('materialRequestJobProduct.jobProductId' => $jobProductId, 'entity.deleted' => 0));
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if (!$rowset) {
            return NULL;
        }
        return $rowset;
    }

    public function getProductDetailsByRequestId($requestId)
    {
        $adapter = $this->tableGateway->getAdapter();

        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('materialRequestJobProduct');

        $select->join('jobProduct', 'jobProduct.jobProductId = materialRequestJobProduct.jobProductId', array('jobProductId', 'jobProductIssuedQty', 'jobProductRequestedQty'));
        $select->join('locationProduct', 'locationProduct.locationProductID = jobProduct.locationProductID', array('locationProductQuantity'));
        $select->join('product', 'product.productID = locationProduct.productID', array('productCode', 'productName','productID'));
        $select->where->equalTo('materialRequestJobProduct.materialRequestId', $requestId);

        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();

        return $result;
    }

    public function update ($data, $id) 
    {
        try {
            $this->tableGateway->update($data, array('materialRequestJobProductId' => $id));
            return true;
        } catch (Exception $e) {
            error_log($e->getTraceAsString());
            return false;
        }

        return true;
    }
}
