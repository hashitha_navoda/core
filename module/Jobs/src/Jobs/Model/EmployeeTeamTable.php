<?php

namespace Jobs\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Jobs\Model\EmployeeTeam;
use Zend\Db\Sql\Sql;
use Core\Model\ProductTable as CoreProductTable;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Predicate\PredicateSet;
use Zend\Db\Sql\Predicate\Between;
use Zend\Db\Sql\Predicate\Operator;

class EmployeeTeamTable extends CoreProductTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    /**
     * save employeeTeam record 
     * @param array $employeeTeamData
     * @return mixed
     */
    public function saveTeamEmployees($employeeTeamData)
    {
        $EmployeeTeamData = array(
            'employeeID' => $employeeTeamData->employeeID,
            'teamID' => $employeeTeamData->teamID,
            'participantTypeID' => $employeeTeamData->participantTypeID,
            'participantSubTypeID' => $employeeTeamData->participantSubTypeID
        );
        if ($this->tableGateway->insert($EmployeeTeamData)) {
            $result = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
            return true;
        } else {
            return FALSE;
        }
    }

    /**
     * fetch employees that match for given teamID 
     * @param string $departmentCode
     * @return mixed
     */
    public function getTeamEmployees($id)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('employeeTeam')
                ->columns(array('*'))
                ->join('employee', 'employeeTeam.employeeID = employee.employeeID', array('*'), 'left')
                ->join('department', 'employeeTeam.participantTypeID = department.departmentID', array('departmentName','departmentCode'), 'left')
                ->join('designation', 'employeeTeam.participantSubTypeID = designation.designationID', array('designationCode','designationName'), 'left')
                ->where(array('employeeTeam.teamID' => $id, 'employee.employeeStatus' => true, 'employeeTeam.isDeleted' => false));
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if (!$rowset) {
            return NULL;
        }
        return $rowset;
    }

    /**
     * delete employeeTeam that match for given employeeTeamID 
     * @param string $employeeTeamID
     * @return mixed
     */
    public function deleteTeamEmployeeByemployeeTeamID($employeeTeamID)
    {
        $employeeTeamData = array(
            'isDeleted' => true,
        );
        try {
            $this->tableGateway->update($employeeTeamData, array('employeeTeamID' => $employeeTeamID));
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
        return true;
    }

    public function checkEmployeeUsed($employeeId)
    {
    	$sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('employeeTeam')
                ->columns(array('*'))
                ->join('team', 'employeeTeam.teamID = team.teamID', array('*'), 'left')
                ->where(array('employeeTeam.employeeID' => $employeeId, 'employeeTeam.isDeleted' => 0,  'team.isDeleted'=> 0));
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();

        if ($rowset->count() > 0) {
            return false;
        }
        return true;

    }

    /**
     * fetch inactive employees that match for given teamID 
     * @param string $id
     * @return mixed
     */
    public function getInactiveTeamEmployees($id)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('employeeTeam')
                ->columns(array('*'))
                ->join('employee', 'employeeTeam.employeeID = employee.employeeID', array('*'), 'left')
                ->join('entity', 'employee.entityID = entity.entityID', array('*'), 'left')
                ->where(array('employeeTeam.teamID' => $id, 'employee.employeeStatus' => false, 'entity.deleted' => 0));
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();

        if (!$rowset) {
            return NULL;
        }
        return $rowset;
    }

}
