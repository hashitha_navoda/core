<?php

namespace Jobs\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class ServiceVehicle
{

    public $serviceVehicleID;
    public $customerID;
    public $vehicleType;
    public $vehicleModel;
    public $vehicleSubModel;
    public $serviceVehicleRegNo;
    public $serviceVehicleEngineNo;
    public $serviceVehicleOdoMeter;
    public $fuelTypeID;
    protected $inputFilter;
    
    public function exchangeArray($data)
    {
        $this->serviceVehicleID = (!empty($data['serviceVehicleID'])) ? $data['serviceVehicleID'] : null;
        $this->customerID = (!empty($data['customerID'])) ? $data['customerID'] : null;
        $this->vehicleType = (!empty($data['vehicleType'])) ? $data['vehicleType'] : null;
        $this->fuelTypeID = (!empty($data['fuelTypeID'])) ? $data['fuelTypeID'] : null;
        $this->vehicleModel = (!empty($data['vehicleModel'])) ? $data['vehicleModel'] : null;
        $this->vehicleSubModel = (!empty($data['vehicleSubModel'])) ? $data['vehicleSubModel'] : null;
        $this->serviceVehicleRegNo = (!empty($data['serviceVehicleRegNo'])) ? $data['serviceVehicleRegNo'] : null;
        $this->serviceVehicleEngineNo = (!empty($data['serviceVehicleEngineNo'])) ? $data['serviceVehicleEngineNo'] : null;
        $this->serviceVehicleOdoMeter = (!empty($data['serviceVehicleOdoMeter'])) ? $data['serviceVehicleOdoMeter'] : null;
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {
        
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

}