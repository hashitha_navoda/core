<?php

namespace Jobs\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class JobVehicleStatus
{

    public $jobVehicleStatusId;
    public $jobVehicleID;
    public $airFilter;
    public $engineOil;
    public $wiperFluid;
    public $wiperBlades;
    public $tyres;
    public $checkBelts;
    public $coolant;
    public $brakeFluid;
    public $steeringFluid;
    public $transmissionFluid;
    public $frontLeftTyrePressure;
    public $frontRightTyrePressure;
    public $rearLeftTyrePressure;
    public $rearRightTyrePressure;
    public $battery;
    public $odoMeter;
    public $brakePadsFrontfactor;
    public $brakePadsRearfactor;
    public $brakePadsChangeFluid;
    public $oilFilter;
    public $gearBoxOil;
    public $differentialOil;
    public $acCabinFilter;
    public $fuelFilter;
    public $radiatorCoolantLevel;
    public $powerSteeringOil;
    public $radiatorHoses;
    public $driveBelts;
    public $engineMount;
    public $shockMounts;
    public $silencerMounts;
    public $axleBoots;
    public $headLamps;
    public $signalLamps;
    public $parkingLamps;
    public $fogLamps;
    public $tailLamp;
    public $roomLamp;
    public $wScreenWasherFluid;
    public $rackBoots;
    public $brakePadsLiners;
    public $tyreWareUneven;
    public $wheelNutsStuds;
    public $entityID;
    
    protected $inputFilter;
    
    public function exchangeArray($data)
    {
        $this->jobVehicleStatusId = (!empty($data['jobVehicleStatusId'])) ? $data['jobVehicleStatusId'] : null;
        $this->jobVehicleID = (!empty($data['jobVehicleID'])) ? $data['jobVehicleID'] : null;
        $this->airFilter = ($data['airFilter'] != "") ? $data['airFilter'] : null;
        $this->engineOil = ($data['engineOil'] != "") ? $data['engineOil'] : null;
        $this->wiperFluid = ($data['wiperFluid'] != "") ? $data['wiperFluid'] : null;
        $this->wiperBlades = ($data['wiperBlades'] != "") ? $data['wiperBlades'] : null;
        $this->tyres = ($data['tyres'] != "") ? $data['tyres'] : null;
        $this->checkBelts = ($data['checkBelts'] != "") ? $data['checkBelts'] : null;
        $this->coolant = ($data['coolant'] != "") ? $data['coolant'] : null;
        $this->brakeFluid = ($data['brakeFluid'] != "") ? $data['brakeFluid'] : null;
        $this->steeringFluid = ($data['steeringFluid'] != "") ? $data['steeringFluid'] : null;
        $this->transmissionFluid = ($data['transmissionFluid'] != "") ? $data['transmissionFluid'] : null;
        $this->battery = ($data['battery'] != "") ? $data['battery'] : null;
        $this->frontLeftTyrePressure = (!empty($data['frontLeftTyrePressure'])) ? $data['frontLeftTyrePressure'] : null;
        $this->frontRightTyrePressure = (!empty($data['frontRightTyrePressure'])) ? $data['frontRightTyrePressure'] : null;
        $this->rearLeftTyrePressure = (!empty($data['rearLeftTyrePressure'])) ? $data['rearLeftTyrePressure'] : null;
        $this->rearRightTyrePressure = (!empty($data['rearRightTyrePressure'])) ? $data['rearRightTyrePressure'] : null;
        $this->odoMeter = (!empty($data['odoMeter'])) ? $data['odoMeter'] : null;
        $this->brakePadsFrontfactor = (!empty($data['brakePadsFrontfactor'])) ? $data['brakePadsFrontfactor'] : null;
        $this->brakePadsRearfactor = (!empty($data['brakePadsRearfactor'])) ? $data['brakePadsRearfactor'] : null;
        $this->brakePadsChangeFluid = (!empty($data['brakePadsChangeFluid'])) ? $data['brakePadsChangeFluid'] : null;
        $this->oilFilter = ($data['oilFilter'] != "") ? $data['oilFilter'] : null;
        $this->gearBoxOil = ($data['gearBoxOil'] != "") ? $data['gearBoxOil'] : null;
        $this->differentialOil = ($data['differentialOil'] != "") ? $data['differentialOil'] : null;
        $this->acCabinFilter = ($data['acCabinFilter'] != "") ? $data['acCabinFilter'] : null;
        $this->fuelFilter = ($data['fuelFilter'] != "") ? $data['fuelFilter'] : null;
        $this->radiatorCoolantLevel = ($data['radiatorCoolantLevel'] != "") ? $data['radiatorCoolantLevel'] : null;
        $this->powerSteeringOil = ($data['powerSteeringOil'] != "") ? $data['powerSteeringOil'] : null;
        $this->radiatorHoses = ($data['radiatorHoses'] != "") ? $data['radiatorHoses'] : null;
        $this->driveBelts = ($data['driveBelts'] != "") ? $data['driveBelts'] : null;
        $this->engineMount = ($data['engineMount'] != "") ? $data['engineMount'] : null;
        $this->shockMounts = ($data['shockMounts'] != "") ? $data['shockMounts'] : null;
        $this->silencerMounts = ($data['silencerMounts'] != "") ? $data['silencerMounts'] : null;
        $this->axleBoots = ($data['axleBoots'] != "") ? $data['axleBoots'] : null;
        $this->headLamps = ($data['headLamps'] != "") ? $data['headLamps'] : null;
        $this->signalLamps = ($data['signalLamps'] != "") ? $data['signalLamps'] : null;
        $this->parkingLamps = ($data['parkingLamps'] != "") ? $data['parkingLamps'] : null;
        $this->fogLamps = ($data['fogLamps'] != "") ? $data['fogLamps'] : null;
        $this->tailLamp = ($data['tailLamp'] != "") ? $data['tailLamp'] : null;
        $this->roomLamp = ($data['roomLamp'] != "") ? $data['roomLamp'] : null;
        $this->wScreenWasherFluid = ($data['wScreenWasherFluid'] != "") ? $data['wScreenWasherFluid'] : null;
        $this->rackBoots = ($data['rackBoots'] != "") ? $data['rackBoots'] : null;
        $this->brakePadsLiners = ($data['brakePadsLiners'] != "") ? $data['brakePadsLiners'] : null;
        $this->tyreWareUneven = ($data['tyreWareUneven'] != "") ? $data['tyreWareUneven'] : null;
        $this->wheelNutsStuds = ($data['wheelNutsStuds'] != "") ? $data['wheelNutsStuds'] : null;
        $this->entityID = (!empty($data['entityID'])) ? $data['entityID'] : null;
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {
        
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

}