<?php

namespace Jobs\Model;

use Core\Model\ProductTable as CoreProductTable;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Sql;

class JobEmployeeTable extends CoreProductTable {

	protected $tableGateway;

	public function __construct(TableGateway $tableGateway) {
		$this->tableGateway = $tableGateway;
	}

	public function saveJobTaskEmployee($jobEmployeeData) {
		$jobEmployeeSaveData = array(
			'jobID' => $jobEmployeeData->jobID,
			'employeeDesignationID' => $jobEmployeeData->employeeDesignationID,
			'jobTaskID' => $jobEmployeeData->jobTaskID,
		);
		if ($this->tableGateway->insert($jobEmployeeSaveData)) {
			$result = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue('jobEmployeeID');
			return $result;
		} else {
			return FALSE;
		}
	}

	public function deleteJobTaskEmployeeByJobTaskID($jobTaskID) {
		try {
			$result = $this->tableGateway->delete(array('jobTaskID' => $jobTaskID));
			return $result;
		} catch (\Exception $e) {
			$error = 0;
			return $error;
		}
	}

	/**
     * fetch all job task related employees by jobid and jobtasak id
     * @param int $jobId
     * @param int $jobTasskID
     * @return mixed
     */
	public function getJobTaskEmployeeDetails($jobID, $jobTaskID) {
		$sql = new Sql($this->tableGateway->getAdapter());
		$select = $sql->select();
		$select->from('jobEmployee')
			->columns(array('*'))
			->join('employeeDesignation', 'jobEmployee.employeeDesignationID=  employeeDesignation.employeeDesignationID', array("*"), "left")
			->join('employee', 'employeeDesignation.employeeID=  employee.employeeID', array("*"), "left")
			->join('designation', 'employeeDesignation.designationID=  designation.designationID', array("*"), "left")
			->join('entity', 'employee.entityID=  entity.entityID', array("*"), "left")
			->where(array('jobEmployee.jobID' => $jobID, 'jobEmployee.jobTaskID' => $jobTaskID));

		$statement = $sql->prepareStatementForSqlObject($select);
		$rowset = $statement->execute();
		if (!$rowset) {
			return NULL;
		}
		return $rowset;
	}

}
