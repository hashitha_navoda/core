<?php

namespace Jobs\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Jobs\Model\RateCard;
use Zend\Db\Sql\Sql;
use Core\Model\ProductTable as CoreProductTable;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Predicate\PredicateSet;
use Zend\Db\Sql\Predicate\Between;
use Zend\Db\Sql\Predicate\Operator;

class JobCardServiceIncentiveRateTable extends CoreProductTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function saveJobCardServiceIncentiveRate($inData)
    {
        $saveData = array(
            'jobCardIncentiveRateID' => $inData->jobCardIncentiveRateID,
            'serviceID' => $inData->serviceID,
            'serviceRate' => $inData->serviceRate,
            'incentive' => $inData->incentive,
            'incentiveType' => $inData->incentiveType,
            'deleted' => 0,
        );
        if ($this->tableGateway->insert($saveData)) {
            $result = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
            return $result;
        } else {
            return FALSE;
        }
    }

    public function getJobCardServiceIncentiveByJobCardIncentiveRateId($jobCardIncentiveRateID) {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('jobCardServiceIncentiveRate')
            ->columns(array('*'))
            ->join('task','task.taskID = jobCardServiceIncentiveRate.serviceID',array('taskCode','taskName'),'left')
            ->where(array('jobCardServiceIncentiveRate.jobCardIncentiveRateID' => $jobCardIncentiveRateID, 'jobCardServiceIncentiveRate.deleted' => 0));
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if (!$rowset) {
            return NULL;
        }
        return $rowset->current();
    }

    public function getJobCardServiceIncentiveByServiceIdAndJobCardRateID($jobCardIncentiveRateID, $serviceID) {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('jobCardServiceIncentiveRate')
            ->columns(array('*'))
            ->join('task','task.taskID = jobCardServiceIncentiveRate.serviceID',array('taskCode','taskName'),'left')
            ->where(array('jobCardServiceIncentiveRate.jobCardIncentiveRateID' => $jobCardIncentiveRateID, 'jobCardServiceIncentiveRate.serviceID' => $serviceID,'jobCardServiceIncentiveRate.deleted' => 0));
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if (!$rowset) {
            return NULL;
        }
        return $rowset->current();
    }

    public function updateJobCardServiceIncentive($jobCardIncentiveRateID)
    {
        $data = array(
            'deleted' => 1
        );
        try {
            $this->tableGateway->update($data, array('jobCardIncentiveRateID' => $jobCardIncentiveRateID));
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
        return true;
    }
}
