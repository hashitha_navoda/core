<?php

namespace Jobs\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class JobGeneralSettings
{

    public $id;
    public $maintainProject;
    public $rawMaterials;
    public $finishedGoods;
    public $fixedAssets;
    public $vehicles;
    public $contractorForJob;
    public $contractorForTask;
    
    public function exchangeArray($data)
    {
        $this->id = (!empty($data['id'])) ? $data['id'] : null;
        $this->maintainProject = (!empty($data['maintainProject'])) ? $data['maintainProject'] : null;
        $this->rawMaterials = (!empty($data['rawMaterials'])) ? $data['rawMaterials'] : null;
        $this->finishedGoods = (!empty($data['finishedGoods'])) ? $data['finishedGoods'] : null;
        $this->fixedAssets = (!empty($data['fixedAssets'])) ? $data['fixedAssets'] : null;
        $this->vehicles = (!empty($data['vehicles'])) ? $data['vehicles'] : null;
        $this->contractorForJob = (!empty($data['contractorForJob'])) ? $data['contractorForJob'] : null;
        $this->contractorForTask = (!empty($data['contractorForTask'])) ? $data['contractorForTask'] : null;
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {
        
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

}