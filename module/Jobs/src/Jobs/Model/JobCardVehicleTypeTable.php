<?php

namespace Jobs\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Predicate\PredicateSet;
use Zend\Db\Sql\Predicate\Operator;

class JobCardVehicleTypeTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll()
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('jobCardVehicleType');
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }

    public function saveJobCardVehicleType($data)
    {
        $taskData = array(
            'jobCardID' => $data->jobCardID,
            'vehicleTypeID' => $data->vehicleTypeID,
            'deleted' => '0'
        );
        if ($this->tableGateway->insert($taskData)) {
            $result = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
            return $result;
        } else {
            return FALSE;
        }
    }

    public function updateDeleted($jobCardID)
    {
        $data = array(
            'deleted' => 1,
        );
        try {
            $this->tableGateway->update($data, array('jobCardID' => $jobCardID));
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
        return true;
    }

    public function getRelatedVehicleTypes($jobCardID)
    {

        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('jobCardVehicleType');
        $select->where(array('jobCardID' => $jobCardID, 'deleted' => 0));

        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if (!$rowset) {
            return NULL;
        }
        return $rowset;
    }

    public function getTaskCardList($vehicleTypeID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('serviceVehicleType')
            ->join('task','task.taskID = serviceVehicleType.serviceID',array('taskCode','taskName'),'left')
            ->where(array('serviceVehicleType.deleted' => 0, 'serviceVehicleType.vehicleTypeID' => $vehicleTypeID, 'task.deleted' => 0));
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if (!$rowset) {
            return NULL;
        }
        return $rowset;
    }

    public function getJobCardForVehicleTypeList($vehicleTypeID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('jobCardVehicleType')
            ->join('taskCard','taskCard.taskCardID = jobCardVehicleType.jobCardID',array('taskCardCode','taskCardName'),'left')
            ->join('jobCardIncentiveRate','jobCardIncentiveRate.jobCardID = jobCardVehicleType.jobCardID AND jobCardIncentiveRate.vehicleTypeID = jobCardVehicleType.vehicleTypeID',array('jobCardIncentiveRateID','serviceRate','incentive','incentiveType','deleteStatus' => new Expression('jobCardIncentiveRate.deleted')),'left')
            ->where(array('jobCardVehicleType.deleted' => 0, 'jobCardVehicleType.vehicleTypeID' => $vehicleTypeID, 'taskCard.deleted' => 0));
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if (!$rowset) {
            return NULL;
        }
        return $rowset;
    }

}
