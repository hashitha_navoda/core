<?php

namespace Jobs\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class ProjectSupervisor
{

    public $projectSupervisorID;
    public $employeeID;
    public $projectID;
    protected $inputFilter;
    
    public function exchangeArray($data)
    {
        $this->projectSupervisorID = (!empty($data['projectSupervisorID'])) ? $data['projectSupervisorID'] : null;
        $this->employeeID = (!empty($data['employeeID'])) ? $data['employeeID'] : null;
        $this->projectID = (!empty($data['projectID'])) ? $data['projectID'] : null;
       
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {
        
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

}