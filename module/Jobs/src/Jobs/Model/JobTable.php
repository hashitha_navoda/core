<?php

namespace Jobs\Model;

use Zend\Db\Sql\Predicate\Operator;
use Zend\Db\Sql\Predicate\PredicateSet;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Where;
use Zend\Db\TableGateway\TableGateway;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\Sql\Expression;

class JobTable {

	protected $tableGateway;

	public function __construct(TableGateway $tableGateway) {
		$this->tableGateway = $tableGateway;
	}

	public function fetchAll($locationID, $paginated = FALSE) {
		$adapter = $this->tableGateway->getAdapter();
		$sql = new Sql($adapter);
		$select = $sql->select();
		$select->from('job');
		$select->order('jobId DESC');
		$select->join('customer', 'job.customerID=  customer.customerID', array("customerName", "customerShortName", "customerStatus"), "left");
		$select->join('jobType', 'job.jobTypeId=  jobType.jobTypeId', array("jobTypeName"), "left");
		$select->join('project', 'job.projectId=  project.projectId', array("projectType"), "left");
		$select->join('projectType', 'projectType.projectTypeID=  project.projectType', array("projectTypeName"), "left");
		$select->join('entity', 'job.entityID=  entity.entityID', array("deleted"), "left");
		$select->where->notEqualTo('job.jobStatus',10);
		if ($locationID) {
			$select->where(array('job.locationID' => $locationID));
		}
		if ($paginated) {
			$paginatorAdapter = new DbSelect(
				$select, $this->tableGateway->getAdapter());
			$paginator = new Paginator($paginatorAdapter);
			return $paginator;
		} else {
			$statement = $sql->prepareStatementForSqlObject($select);
			$result = $statement->execute();
			return $result;
		}
	}

	//get all Job details that related to Location
	public function getAllJobDetails($locationID) {
		$adapter = $this->tableGateway->getAdapter();
		$sql = new Sql($adapter);
		$select = $sql->select();
		$select->from('job');
		$select->order('jobId DESC');
//        $select->join('customer', 'job.customerID=  customer.customerID', array("customerName", "customerShortName"), "left");
		//        $select->join('jobType', 'job.jobTypeId=  jobType.jobTypeId', array("jobTypeName"), "left");
		$select->join('entity', 'job.entityID=  entity.entityID', array("deleted"), "left");
		$select->where(array('job.locationID' => $locationID, 'entity.deleted' => 0));
		$statement = $sql->prepareStatementForSqlObject($select);
		$result = $statement->execute();

		return $result;
	}

//get details that related to the given Job ID
	public function getJobDetails($ID) {
		$sql = new Sql($this->tableGateway->getAdapter());
		$select = $sql->select();
		$select->from('job')
			->columns(array('*'))
			->join('customer', 'job.customerID=  customer.customerID', array("customerName","customerCode", "customerCurrency", "customerShortName", "cusID" => new Expression('job.customerID')), "left")
			->join('jobType', 'job.jobTypeId=  jobType.jobTypeId', array("jobTypeName"), "left")
			->join('status', "job.jobStatus= status.statusID", array('statusName'), 'left')
			->join('customerProfile', 'job.customerProfileID=  customerProfile.customerProfileID', array("*"), "left")
			->join('entity', 'job.entityID=  entity.entityID', array("*"), "left")
			->join('user', 'entity.createdBy = user.userID', array('userUsername'), 'left')
			->where(array('job.jobId' => $ID));
		$statement = $sql->prepareStatementForSqlObject($select);
		$rowset = $statement->execute();
		if (!$rowset) {
			return NULL;
		}
		return $rowset;
	}

//get job details that related to the given jobType ID
	public function getJobDetailsByJobTypeID($ID) {
		$sql = new Sql($this->tableGateway->getAdapter());
		$select = $sql->select();
		$select->from('job')
			->columns(array('*'))
			->join('entity', 'job.entityID=  entity.entityID', array("deleted"), "left")
			->where(array('job.jobTypeId' => $ID, 'entity.deleted' => 0));
		$statement = $sql->prepareStatementForSqlObject($select);
		$rowset = $statement->execute();
		if (!$rowset) {
			return NULL;
		}
		return $rowset;
	}

	/**
	 * @author Lahiru Abeysinghe <lahiru@thinkcube.com>
	 * use to get all job details that related to the given customerID
	 * @param String $customerID
	 *
	 */
	public function getJobDetailsByCustomerID($customerID) {
		$sql = new Sql($this->tableGateway->getAdapter());
		$select = $sql->select();
		$select->from('job')
			->columns(array('*'))
			->join('entity', 'job.entityID=  entity.entityID', array("deleted"), "left")
			->where(array('job.customerID' => $customerID, 'entity.deleted' => 0));
		$statement = $sql->prepareStatementForSqlObject($select);
		$rowset = $statement->execute();
		if (!$rowset) {
			return NULL;
		}
		return $rowset;
	}

//get job details that related to the given jobStation ID
	public function getJobDetailsByJobStationID($ID) {
		$sql = new Sql($this->tableGateway->getAdapter());
		$select = $sql->select();
		$select->from('job')
			->columns(array('*'))
			->join('entity', 'job.entityID=  entity.entityID', array("deleted"), "left")
			->where(array('job.jobStation' => $ID, 'entity.deleted' => 0));
		$statement = $sql->prepareStatementForSqlObject($select);
		$rowset = $statement->execute();
		if (!$rowset) {
			return NULL;
		}
		return $rowset;
	}

//get job details that related to given params

	public function getJob($cusName, $jobname, $jobStation, $locationID) {
		$sql = new Sql($this->tableGateway->getAdapter());
		$select = $sql->select();
		$select->from('job')
			->columns(array('*'))
			->join('customer', 'job.customerID=  customer.customerID', array("customerName", "customerShortName"), "left")
			->join('jobType', 'job.jobTypeId=  jobType.jobTypeId', array("jobTypeName"), "left")
			->where(array('job.customerID' => $cusName, 'job.jobName' => $jobname, 'job.jobStation' => $jobStation, 'job.locationID' => $locationID));
		$statement = $sql->prepareStatementForSqlObject($select);
		$rowset = $statement->execute();
		if (!$rowset) {
			return NULL;
		}
		return $rowset;
	}

	// save job details
	public function saveJob($jobData) {
		$jobSaveData = array(
			'jobName' => $jobData->jobName,
			'customerID' => $jobData->customerID,
			'jobTypeId' => $jobData->jobTypeId,
			'jobDescription' => $jobData->jobDescription,
			'jobStartingTime' => $jobData->jobStartingTime,
			'jobEstimatedTime' => $jobData->jobEstimatedTime,
			'jobEndTime' => $jobData->jobEndTime,
			'jobEstimatedCost' => $jobData->jobEstimatedCost,
			'projectId' => $jobData->projectId,
			'jobStation' => $jobData->jobStation,
			'jobReferenceNumber' => $jobData->jobReferenceNumber,
			'customerProfileID' => $jobData->customerProfileID,
			'jobWeight' => $jobData->jobWeight,
			'locationID' => $jobData->locationID,
			'jobStatus' => $jobData->jobStatus,
			'entityID' => $jobData->entityID,
			'jobAddress' => $jobData->jobAddress,
			'jobRepeatEnabled' => $jobData->jobRepeatEnabled,
			'jobRepeatComment' => $jobData->jobRepeatComment,
			'contactorID' => $jobData->contactorID,
			'jobDueDate' => $jobData->jobDueDate,
			'jobEstimatedEndDate' => $jobData->jobEstimatedEndDate,
			'customerNotes' => $jobData->customerNotes,
			'supervisorId' => $jobData->supervisorId,
			'jobStartDate' => $jobData->jobStartDate,
			'jobAdditionalDetail1' => $jobData->jobAdditionalDetail1,
			'jobAdditionalDetail2'	=> $jobData->jobAdditionalDetail2
		);

		if ($this->tableGateway->insert($jobSaveData)) {
			$result = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue('jobID');
			return $result;
		} else {
			return FALSE;
		}
	}

	//get job details that related to the jobReference Code
	public function getJobByCode($jobRefCode) {
		$adapter = $this->tableGateway->getAdapter();
		$sql = new Sql($adapter);
		$select = $sql->select();
		$select->from('job')
			->columns(array("*"))
			->where(array('job.jobReferenceNumber' => $jobRefCode))
			->where->notEqualTo('job.jobStatus', 10);
		$statement = $sql->prepareStatementForSqlObject($select);
		$rowset = $statement->execute();
		if (!$rowset) {
			return NULL;
		}
		return $rowset;
	}

	//get job details that related to the project id
	public function getJobByProjectId($projectId, $progressFlag = false) {
		$adapter = $this->tableGateway->getAdapter();
		$sql = new Sql($adapter);
		$select = $sql->select();
		$select->from('job')
			->columns(array("*"))
			->where(array('job.projectId' => $projectId));

		if (!$progressFlag) {
			$select->where->notEqualTo('job.jobStatus', 3)
			       ->where->notEqualTo('job.jobStatus', 4)
			       ->where->notEqualTo('job.jobStatus', 9);      
		} 

		$select->where->notEqualTo('job.jobStatus', 5)
				   ->where->notEqualTo('job.jobStatus', 10);
		
		$statement = $sql->prepareStatementForSqlObject($select);
		$rowset = $statement->execute();
		if (!$rowset) {
			return NULL;
		}
		return $rowset;
	}


	//update existing job records
	public function updateJob($jobData) {
		$jobSaveData = array(
			'jobName' => $jobData->jobName,
			'customerID' => $jobData->customerID,
			'jobTypeId' => $jobData->jobTypeId,
			'jobDescription' => $jobData->jobDescription,
			'jobStartingTime' => $jobData->jobStartingTime,
			'jobEstimatedTime' => $jobData->jobEstimatedTime,
			'jobEndTime' => $jobData->jobEndTime,
			'jobEstimatedCost' => $jobData->jobEstimatedCost,
			'projectId' => $jobData->projectId,
			'jobStation' => $jobData->jobStation,
			'jobReferenceNumber' => $jobData->jobReferenceNumber,
			'customerProfileID' => $jobData->customerProfileID,
			'jobAddress' => $jobData->jobAddress,
			'jobRepeatEnabled' => $jobData->jobRepeatEnabled,
			'jobRepeatComment' => $jobData->jobRepeatComment,
		);
		try {
			$this->tableGateway->update($jobSaveData, array('jobId' => $jobData->jobId));
		} catch (Exception $exc) {
			echo $exc->getTraceAsString();
		}
		return true;
	}

	public function updateJobstatusID($jobID, $status) {
		$jobSaveData = array(
			'jobStatus' => $status,
		);
		try {
			$this->tableGateway->update($jobSaveData, array('jobId' => $jobID));
		} catch (Exception $exc) {
			echo $exc->getTraceAsString();
			return false;
		}
		return true;
	}

	public function updateJobData($jobID, $jobUpdateData) {
		try {
			$this->tableGateway->update($jobUpdateData, array('jobId' => $jobID));
		} catch (Exception $exc) {
			echo $exc->getTraceAsString();
		}
		return true;
	}

	public function deleteJobByJobID($jobData) {
		try {
			$result = $this->tableGateway->delete(array('jobId' => $jobData->jobId));
			return $result;
		} catch (\Exception $e) {
			$error = 0;
			return $error;
		}
	}

//search job

	public function jobSearchByKey($jobSearchKey = null, $locationID = null, $projectID = null, $jobSupervisorID = null, $jobStatusID = null, $jobID=null, $filterStatus = null) {

		$adapter = $this->tableGateway->getAdapter();
		$sql = new Sql($adapter);
		$select = $sql->select();
		$select->from('job')
			->columns(array('*'))
			->join('customer', 'job.customerID=  customer.customerID', array("customerName", "customerShortName"), "left")
			->join('entity', 'job.entityID=  entity.entityID', array("deleted"), "left")
			->join('jobType', 'job.jobTypeId=  jobType.jobTypeId', array("jobTypeName"), "left");

		if (!is_null($jobSearchKey)) {
			$select->where(new PredicateSet(array(new Operator('job.jobName', 'like', '%' . $jobSearchKey . '%'), new Operator('job.jobReferenceNumber', 'like', '%' . $jobSearchKey . '%'), new Operator('customer.customerName', 'like', '%' . $jobSearchKey . '%'), new Operator('customer.customerCode', 'like', '%' . $jobSearchKey . '%')), PredicateSet::OP_OR));
		}

		if (!is_null($projectID)) {
			$select->where(new PredicateSet(array(new Operator('job.projectId', '=', $projectID))));
		}

		if (!is_null($jobID)) {
			$select->where(new PredicateSet(array(new Operator('job.jobId', '=', $jobID))));
		}

		if (!is_null($jobSupervisorID)) {
			$select->join('jobSupervisor', 'job.jobId=  jobSupervisor.jobID', array("jobSupervisorID"), "left");
			$select->where(new PredicateSet(array(new Operator('jobSupervisor.jobSupervisorID', '=', $jobSupervisorID))));
		}

		if (!is_null($jobStatusID)) {
			$select->where(new PredicateSet(array(new Operator('job.jobStatus', '=', $jobStatusID))));
		}

		if (!is_null($locationID)) {
			$select->where(new PredicateSet(array(new Operator('job.locationID', '=', $locationID))));
		}

		$select->where(new PredicateSet(array(new Operator('entity.deleted', '=', 0))));

		if ($filterStatus == 0 || $filterStatus == null) {
			$select->where->notEqualTo('job.jobStatus', 10);
		} else {
			$select->where->notEqualTo('job.jobStatus', 3)
					->where->notEqualTo('job.jobStatus', 4)
					->where->notEqualTo('job.jobStatus', 5)
					->where->notEqualTo('job.jobStatus', 9)
					->where->notEqualTo('job.jobStatus', 10);
		}
		$statement = $sql->prepareStatementForSqlObject($select);
		$results = $statement->execute();
		return $results;
	}

//get all jobs that related to the given project id and location id
	public function getJobListByProjectID($projectID, $locationID) {
		$adapter = $this->tableGateway->getAdapter();
		$sql = new Sql($adapter);
		$select = $sql->select();
		$select->from('job')
			->columns(array('*'))
			->join('customer', 'job.customerID=  customer.customerID', array("customerName", "customerShortName", "customerStatus"), "left")
			->join('customerProfile', 'job.customerProfileID=  customerProfile.customerProfileID', array("customerProfileName"), "left")
			->join('status', "job.jobStatus= status.statusID", array('statusName'), 'left')
			->join('jobType', 'job.jobTypeId=  jobType.jobTypeId', array("jobTypeName"), "left")
			->join('entity', 'job.entityID=  entity.entityID', array("deleted", "createdTimeStamp"), "left");
		$select->where(array('job.projectId' => $projectID, 'job.locationID' => $locationID, 'entity.deleted' == 0));
		$statement = $sql->prepareStatementForSqlObject($select);
		$results = $statement->execute();
		return (object) $results;
	}

//use to update job weight
	public function updateJobWeight($jobData) {
		$jobWeightUpdateData = array(
			'jobWeight' => $jobData[jobWeight],
		);
		try {
			$this->tableGateway->update($jobWeightUpdateData, array('jobId' => $jobData[jobId], 'projectId' => $jobData[projectId]));
		} catch (Exception $exc) {
			echo $exc->getTraceAsString();
		}
		return true;
	}

	//update job progress
	public function updateJobProgress(Job $job) {
		$data = array('jobProgress' => $job->jobProgress);
		try {
			$this->tableGateway->update($data, array('jobId' => $job->jobId));
			return true;
		} catch (Exception $exc) {
			echo $exc->getTraceAsString();
			return true;
		}
	}

//get job details that related to the activity id
	public function getJobDetilsByActivityID($activity) {
		$adapter = $this->tableGateway->getAdapter();
		$sql = new Sql($adapter);
		$select = $sql->select();
		$select->from('job')
			->columns(array('*'))
			->join('activity', 'job.jobId=  activity.jobId', array('*'), "left")
			->join('customer', 'job.customerID = customer.customerID', array("customerName", "customerShortName", "customerCurrentBalance", "customerCurrentCredit"), "left");
		$select->where(array('activity.activityId' => $activity));
		$statement = $sql->prepareStatementForSqlObject($select);
		$results = $statement->execute();
		return (object) $results;
	}

	/**
	 * @author ashan <ashan@thinkcube.com>
	 * @param type $locationID
	 * @param type $jobSearchKey
	 * @return $results
	 */
	public function searchJobsForDropDown($locationID, $jobSearchKey, $projectID = null) {
		$adapter = $this->tableGateway->getAdapter();
		$sql = new Sql($adapter);
		$select = $sql->select();
		$select->from('job');
		$select->columns(array('jobReferenceNumber', 'jobId', 'jobProgress', 'jobName'));
		$select->join('entity', 'job.entityID=  entity.entityID', array("deleted"), "left");
		$select->where(array('job.locationID' => $locationID, 'entity.deleted' => 0));
		$select->where->like('jobReferenceNumber', '%' . $jobSearchKey . '%');
		if ($projectID) {
			$select->where(array('job.projectId' => $projectID));
		}
		$select->limit(50);

		$statement = $sql->prepareStatementForSqlObject($select);
		$results = $statement->execute();
		return $results;
	}

	//update job status
	public function updateJobStatus($jobID, $jobStatus) {
		$data = array('jobStatus' => $jobStatus);
		try {
			$this->tableGateway->update($data, array('jobId' => $jobID));
			return true;
		} catch (Exception $exc) {
			echo $exc->getTraceAsString();
			return false;
		}
	}

	public function searchJobReferenceForDropDown($searchKey) {
		$adapter = $this->tableGateway->getAdapter();
		$sql = new Sql($adapter);
		$select = $sql->select();
		$select->from('job');
		$select->join('entity', 'job.entityID=  entity.entityID', array("*"), "left");
		$select->where(array("entity.deleted" => 0));
		$select->where->like('jobReferenceNumber', '%' . $searchKey . '%');
		$select->limit(50);

		$statement = $sql->prepareStatementForSqlObject($select);
		$results = $statement->execute();
		return $results;
	}

	/**
	 * Seach jobs name for typehead
	 * @author sharmilan <sharmilan@thinkcube.com>
	 * @param type $locationID
	 * @param type $jobSearchKey
	 * @return type
	 */
	public function searchJobsForTypeahead($locationID, $jobSearchKey) {
		$adapter = $this->tableGateway->getAdapter();
		$sql = new Sql($adapter);
		$select = $sql->select();
		$select->from('job');
		$select->columns(array('jobName', 'jobId'));
		$select->join('entity', 'job.entityID=  entity.entityID', array("deleted"), "left");
		$select->where(array('job.locationID' => $locationID, 'entity.deleted' => 0));
		$select->where->like('jobName', '%' . $jobSearchKey . '%');
		$select->limit(50);

		$statement = $sql->prepareStatementForSqlObject($select);
		$results = $statement->execute();
		return $results;
	}

	/**
	 * Get All Jobs list for drop down by search key
	 * @author sharmilan <sharmilan@thinkcube.com>
	 * @param type $locationID
	 * @param type $jobSearchKey
	 * @return type
	 */
	public function searchAllJobsForDropDown($locationID, $jobSearchKey) {
		$adapter = $this->tableGateway->getAdapter();
		$sql = new Sql($adapter);
		$select = $sql->select();
		$select->from('job');
		$select->columns(['jobName', 'jobId', 'jobReferenceNumber']);
		$select->where(['job.locationID' => $locationID]);
		$select->where(new PredicateSet([new Operator('jobName', 'like', '%' . $jobSearchKey . '%'),
			new Operator('jobReferenceNumber', 'like', '%' . $jobSearchKey . '%')], PredicateSet::OP_OR));
		$select->limit(50);

		$statement = $sql->prepareStatementForSqlObject($select);
		$results = $statement->execute();
		return $results;
	}

	/**
	 * get job for list view
	 * @author sharmilan <sharmilan@thikcube.com>
	 * @param type $jobID
	 * @param type $locationID
	 * @return type
	 */
	public function getJobListData($jobID, $locationID) {
		$adapter = $this->tableGateway->getAdapter();
		$sql = new Sql($adapter);
		$select = $sql->select();
		$select->from('job')
			->columns(array('*'))
			->join('customer', 'job.customerID=  customer.customerID', array("customerName", "customerShortName", "customerStatus"), "left")
			->join('entity', 'job.entityID=  entity.entityID', array("deleted"), "left")
			->join('jobType', 'job.jobTypeId=  jobType.jobTypeId', array("jobTypeName"), "left");
		$select->where(['job.locationID' => $locationID, 'jobID' => $jobID]);
		$statement = $sql->prepareStatementForSqlObject($select);
		$results = $statement->execute();
		return (object) $results;
	}

	/**
	 * Get jobs for list view by date Range
	 * @author sharmilan <sharmilan@thinkcube.com>
	 * @param type $locationID
	 * @param type $fromDate
	 * @param type $toDate
	 * @return type
	 */
	public function getJobListDataByDateRange($locationID, $fromDate, $toDate) {
		$adapter = $this->tableGateway->getAdapter();
		$sql = new Sql($adapter);
		$select = $sql->select();
		$select->from('job')
			->columns(array('*'))
			->join('customer', 'job.customerID=  customer.customerID', ['customerName', 'customerShortName', 'customerStatus'], 'left')
			->join('entity', 'job.entityID=  entity.entityID', ['deleted', 'createdTimeStamp'], "left")
			->join('jobType', 'job.jobTypeId=  jobType.jobTypeId', ['jobTypeName'], 'left');
		$select->where(['job.locationID' => $locationID]);
		$select->where->greaterThanOrEqualTo('entity.createdTimeStamp', $fromDate)->AND->lessThan('entity.createdTimeStamp', $toDate);
		$statement = $sql->prepareStatementForSqlObject($select);
		$results = $statement->execute();
		return (object) $results;
	}

	/**
	 * Get Repeat jobs for report
	 * @author sharmilan <sharmilan@thinkcube.com>
	 * @param type $locations
	 * @param type $fromDate
	 * @param type $toDate
	 * @return type
	 */
	public function getRepeatJobs($locations, $fromDate = false, $toDate = false) {
		$adapter = $this->tableGateway->getAdapter();
		$sql = new Sql($adapter);
		$select = $sql->select();
		$select->from('job');
		$select->join('entity', 'job.entityID = entity.entityID', ['createdTimeStamp', 'deleted'], 'left');
		$select->join('project', 'job.projectId = project.projectId', ['projectCode', 'projectId'], 'left');
		$select->where(['entity.deleted' => '0', 'job.jobRepeatEnabled' => '1']);
		$select->where->in('job.locationID', $locations);
		if ($fromDate && $toDate) {
			$select->where->between('createdTimeStamp', $fromDate, $toDate);
		}
		$statment = $sql->prepareStatementForSqlObject($select);
		return $statment->execute();
	}

	/**
	 * Update job
	 * @param array $jobData
	 * @param int $jobId
	 * @return boolean
	 */
	public function updateJobByArray($jobData, $jobId) {
		try {
			$this->tableGateway->update($jobData, array('jobId' => $jobId));
			return true;
		} catch (Exception $exc) {
			echo $exc->getTraceAsString();
			return false;
		}
	}

	/**
	 * Update multiple jobs
	 * @param array $jobData
	 * @param array $jobIdArray
	 * @return boolean
	 */
	public function updateMultipleJobs($jobData, $jobIdArray) {
		$where = new Where();
		$where->in('jobId', $jobIdArray);

		try {
			$this->tableGateway->update($jobData, $where);
			return true;
		} catch (Exception $exc) {
			echo $exc->getTraceAsString();
			return false;
		}
	}

	/**
	 * Get all the jobs except closed jobs
	 * @param type $locationId
	 * @return mixed
	 */
	public function getActiveJobtList($locationId) {
		$adapter = $this->tableGateway->getAdapter();
		$sql = new Sql($adapter);
		$select = $sql->select();
		$select->from('job');
		$select->order('jobId DESC');
		$select->join('customer', 'job.customerID=  customer.customerID', array("customerName", "customerShortName", "customerStatus"), "left");
		$select->join('jobType', 'job.jobTypeId=  jobType.jobTypeId', array("jobTypeName"), "left");
		$select->join('entity', 'job.entityID=  entity.entityID', array("deleted"), "left");
		$select->where->notEqualTo('job.jobStatus', 4);
		if ($locationId) {
			$select->where(array('job.locationID' => $locationId));
		}
		$statement = $sql->prepareStatementForSqlObject($select);
		$result = $statement->execute();
		return $result;
	}

	public function getJobsByJobID($jobID) {
		$rowset = $this->tableGateway->select(array('jobId' => $jobID));
		$row = $rowset->current();
		if (!$row) {
			return NULL;
		}
		return $row;
	}

    //get details that related to the given project Id
    public function getJobDetailsAccordingToProject($ID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('job')
                ->columns(array('*'))
                ->join('customer', 'job.customerID=  customer.customerID', array("customerName", "customerShortName"), "left")
                ->join('jobType', 'job.jobTypeId=  jobType.jobTypeId', array("jobTypeName"), "left")
                ->join('status', "job.jobStatus= status.statusID", array('statusName'), 'left')
                ->join('customerProfile', 'job.customerProfileID=  customerProfile.customerProfileID', array("*"), "left")
                ->join('entity', 'job.entityID=  entity.entityID', array("*"), "left")
                ->join('user', 'entity.createdBy = user.userID', array('userUsername'), 'left')
                ->where(array('job.projectId' => $ID))
                ->where->notEqualTo('job.jobStatus', 5)
                ->where->notEqualTo('job.jobStatus', 10);
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if (!$rowset) {
            return NULL;
        }
        return $rowset;
    }

    public function getJobByJobID($jobID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('job')
        	   ->columns(array('*'))
        	   ->join('customer', 'job.customerID=  customer.customerID', array("customerName", "customerCode"), "left")
        	   ->join('entity', 'job.entityID=  entity.entityID', array("*"), "left")
        	   ->join('jobType', 'job.jobTypeId=  jobType.jobTypeId', array("jobTypeName",'jobTypeCode'), "left");
        $select->join('jobContractor', 'job.jobId = jobContractor.jobID', array('jobContractorID','contractorIdOfJob' => new Expression('jobContractor.contractorID')),'left');
        $select->join(['contractor1' => 'contractor'], 'contractor1.contractorID = jobContractor.contractorID', array('jobContractorFirstName' => new Expression('contractor1.contractorFirstName'),'jobContractorLastName' => new Expression('contractor1.contractorSecondName')),'left');
        $select->join('jobManager', 'job.jobId = jobManager.jobID', array('jobManagerID','employeeIdOfJobManager' => new Expression('jobManager.employeeID')),'left');
        $select->join(['employee1' => 'employee'], 'employee1.employeeID = jobManager.employeeID', array('jobMangerFirstName' => new Expression('employee1.employeeFirstName'),'jobMangerSecondName' => new Expression('employee1.employeeSecondName')),'left');
        $select->join('jobSupervisor', 'job.jobId = jobSupervisor.jobID', array('jobSupervisorTableID','jobSupervisorID','participantTypeID','participantSubTypeID','teamID'),'left');
        $select->join(['employee2' => 'employee'], 'employee2.employeeID = jobSupervisor.jobSupervisorID', array('jobSupervisorFirstName' => new Expression('employee2.employeeFirstName'),'jobSupervisorSecondName' => new Expression('employee2.employeeSecondName')),'left');
        $select->join('department', 'jobSupervisor.participantTypeID = department.departmentID', array('participantType' => new Expression('departmentName')), 'left');
        $select->join(['designation2' => 'designation'], 'designation2.designationID = jobSupervisor.participantSubTypeID', array('participantSubType' => new Expression('designation2.designationName')),'left');
        $select->join('jobProduct', 'job.jobId = jobProduct.jobID', array('jobProductID','locationProductID','jobProductUnitPrice','uomID','jobProductAllocatedQty', 'jobProductRequestedQty','jobProductReOrderLevel','jobProductMaterialTypeID','jobProductTaskCardID','jobProductTaskID' => new Expression('jobProduct.jobTaskID'),'jobProductStatus'),'left');
        $select->join('locationProduct', 'jobProduct.locationProductID = locationProduct.locationProductID', array('productID', 'locationProductQuantity', 'defaultPurchasePrice'),'left');
        $select->join('product', 'locationProduct.productID = product.productID', array('productCode', 'productName', 'productTypeID'),'left');
        $select->join('productUom', 'locationProduct.productID = productUom.productID', array('uomID', 'productUomConversion', 'productUomBase'),'left');
        $select->join(['uom1' => 'uom'], 'productUom.uomID = uom1.uomID', array('uomAbbr', 'uomDecimalPlace'),'left');
        $select->join('jobTask', 'job.jobId = jobTask.jobID', array('jobTaskID','taskID','taskUomID' => new Expression('jobTask.uomID'),'jobTaskUnitCost','jobTaskUnitRate','jobTaskEstQty','jobTaskTaskCardID','jobTaskStatus'),'left');
        $select->join('task', 'task.taskID = jobTask.taskID', array('taskCode','taskName'),'left');
        $select->join('jobEmployee', 'job.jobId = jobEmployee.jobID', array('jobEmployeeID','employeeDesignationID','employeeJobTaskID' => new Expression('jobEmployee.jobTaskID')),'left');
        $select->join('employeeDesignation', 'employeeDesignation.employeeDesignationID = jobEmployee.employeeDesignationID', array('*'),'left');
        $select->join(['employee3' => 'employee'], 'employeeDesignation.employeeID = employee3.employeeID', array('jobEmployeeFirstName' => new Expression('employee3.employeeFirstName'),'jobEmployeeSecondName' => new Expression('employee3.employeeSecondName')),'left');
        $select->join('designation', 'employeeDesignation.designationID = designation.designationID', array('*'), 'left');
        $select->join(['uom2' => 'uom'], 'jobTask.uomID = uom2.uomID', array('taskUomAbbr' => new Expression('uom2.uomAbbr'), 'uomName'),'left');
        $select->join('jobTaskContractor', 'jobTask.jobTaskID = jobTaskContractor.jobTaskID', array('jobTaskContractorID','contactorIdOfJobTask' => new Expression('jobTaskContractor.contractorID'),'contactorJobTaskID' => new Expression('jobTaskContractor.jobTaskID')),'left');
        $select->join(['contractor2' => 'contractor'], 'contractor2.contractorID = jobTaskContractor.contractorID', array('jobTaskContractorFirstName' => new Expression('contractor2.contractorFirstName'),'jobTaskContractorLastName' => new Expression('contractor2.contractorSecondName')),'left');
        $select->where(array('job.jobId' => $jobID));
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
	}

    /**
     * update job with updated projectID
     * @param int $oldId
     * @param int $newId
     * @return mixed
     */
    public function updateProjectIDInJobTable($oldId, $newId)
    {
        $JobData = array(
            'projectId' => $newId,
        );
        try {
            $this->tableGateway->update($JobData, array('projectId' => $oldId));
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
        return true;
    }

    //get job list by search key
    public function getJobListBySearchKey($serchKey)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('job');
        $select->columns(array(
            'posi' => new Expression('LEAST(IF(POSITION(\'' . $serchKey . '\' in job.jobReferenceNumber )>0,POSITION(\'' . $serchKey . '\' in job.jobReferenceNumber), 9999),' . 'IF(POSITION(\'' . $serchKey . '\' in job.jobName )>0,POSITION(\'' . $serchKey . '\' in job.jobName), 9999)) '), 'len' => new Expression('LEAST(CHAR_LENGTH(job.jobReferenceNumber ), CHAR_LENGTH(job.jobName )) '), '*',
        ));
        $select->join('entity', 'job.entityID = entity.entityID', array('deleted'));
        $select->join('customer', 'job.customerID= customer.customerID', array('customerName', 'customerShortName', 'customerStatus'));
        $select->join('jobType', 'job.jobTypeId=  jobType.jobTypeId', array("jobTypeName", "jobTypeCode"), "left");
        $select->where(new PredicateSet(array(new Operator('entity.deleted', '=', '0'))));
        $select->where(new PredicateSet(array(new Operator('job.jobReferenceNumber', 'like', '%' . $serchKey . '%'), new Operator('job.jobName', 'like', '%' . $serchKey . '%'), new Operator('customer.customerName', 'like', '%' . $serchKey . '%'), new Operator('customer.customerCode', 'like', '%' . $serchKey . '%')), PredicateSet::OP_OR));
        $select->where->notEqualTo('job.jobStatus', 3)
			->where->notEqualTo('job.jobStatus', 4)
			->where->notEqualTo('job.jobStatus', 5)
			->where->notEqualTo('job.jobStatus', 9)
			->where->notEqualTo('job.jobStatus', 10);
        $select->order(new Expression('IF(posi > 0, posi, 9999) ASC, len ASC'));
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }

    //get details that related to the given project Id
    public function getJobDetailsByProjectID($ID, $checkNotClosed = false)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('job')
                ->columns(array('*'))
                ->join('entity', 'job.entityID=  entity.entityID', array("*"), "left")
                ->where(array('job.projectId' => $ID, 'entity.deleted' => 0))
                ->where->notEqualTo('job.jobStatus',10);

        if ($checkNotClosed) {
        	$select->where->notEqualTo('job.jobStatus',5);
        	$select->where->notEqualTo('job.jobStatus',4);
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if (!$rowset) {
            return NULL;
        }
        return $rowset;
    }


    /**
    * this function use to get job and project details by given search key
    * @param int $locationID
    * @param string $searchKey
    * return mix
    **/
    public function searchJobsAndProJectForDropDown($locationID, $searchKey) {
		$adapter = $this->tableGateway->getAdapter();
		$sql = new Sql($adapter);
		$select = $sql->select();
		$select->from('job');
		$select->columns(array('jobReferenceNumber', 'jobId', 'jobProgress', 'jobName'));
		$select->join('entity', 'job.entityID=  entity.entityID', array("deleted"), "left");
		$select->join('project', 'job.projectId = project.projectId',array('*'),'left');
		$select->where(array('job.locationID' => $locationID, 'entity.deleted' => 0));
		$select->where(new PredicateSet(array(new Operator('job.jobReferenceNumber', 'like', '%' . $searchKey . '%'), new Operator('job.jobName', 'like', '%' . $searchKey . '%'), new Operator('project.projectCode', 'like', '%' . $searchKey . '%'), new Operator('project.projectName', 'like', '%' . $searchKey . '%')), PredicateSet::OP_OR));
		$select->where(array('job.jobStatus' => 8));
		
		$select->limit(50);

		$statement = $sql->prepareStatementForSqlObject($select);
		$results = $statement->execute();
		return $results;
	}

	/**
	* this function use to get job realted details
	* @param int $jobID
	* return mix
	**/
	public function getJobAndCustomerDetailsByJobID($jobID) {
		$sql = new Sql($this->tableGateway->getAdapter());
		$select = $sql->select();
		$select->from('job')
			->columns(array('jobReferenceNumber','jobId','projectId'))
			->join('customer', 'job.customerID = customer.customerID', array('customerID',"customerName", "customerShortName"), "left")
			->join('customerProfile', 'customer.customerID =  customerProfile.customerID', array("customerProfileID"), "left")
			->where(array('job.jobId' => $jobID , 'customerProfile.isPrimary' => 1));
		$statement = $sql->prepareStatementForSqlObject($select);
		$rowset = $statement->execute();
		if (!$rowset) {
			return NULL;
		}
		return $rowset;
	}

	//get job details that related to the project id to verify project deletion
	public function getJobByProjectIdForProjectDeletion($projectId) {
		$adapter = $this->tableGateway->getAdapter();
		$sql = new Sql($adapter);
		$select = $sql->select();
		$select->from('job')
			->columns(array("*"))
			->where(array('job.projectId' => $projectId));

		$select->where->notEqualTo('job.jobStatus', 10);
		$select->where->notEqualTo('job.jobStatus', 5);
		
		$statement = $sql->prepareStatementForSqlObject($select);
		$rowset = $statement->execute();
		if (!$rowset) {
			return NULL;
		}
		return $rowset;
	}

	//get job list for progress Update
	public function getJobListForProgressUpdate($locationID, $paginated = false) {
		$adapter = $this->tableGateway->getAdapter();
		$sql = new Sql($adapter);
		$select = $sql->select();
		$select->from('job');
		$select->order('jobId DESC');
		$select->join('customer', 'job.customerID=  customer.customerID', array("customerName", "customerShortName", "customerStatus"), "left");
		$select->join('jobType', 'job.jobTypeId=  jobType.jobTypeId', array("jobTypeName"), "left");
		$select->join('project', 'job.projectId=  project.projectId', array("projectType"), "left");
		$select->join('projectType', 'projectType.projectTypeID=  project.projectType', array("projectTypeName"), "left");
		$select->join('entity', 'job.entityID=  entity.entityID', array("deleted"), "left");
		$select->where->notEqualTo('job.jobStatus', 5)
			    ->where->notEqualTo('job.jobStatus', 10);
		if ($locationID) {
			$select->where(array('job.locationID' => $locationID));
		}
		if ($paginated) {
			$paginatorAdapter = new DbSelect(
				$select, $this->tableGateway->getAdapter());
			$paginator = new Paginator($paginatorAdapter);
			return $paginator;
		} else {
			$statement = $sql->prepareStatementForSqlObject($select);
			$result = $statement->execute();
			return $result;
		}
	}

	//get job list for Resource management & Cost
	public function getJobListForRespourceManagement($locationID, $paginated) {
		$adapter = $this->tableGateway->getAdapter();
		$sql = new Sql($adapter);
		$select = $sql->select();
		$select->from('job');
		$select->order('jobId DESC');
		$select->join('customer', 'job.customerID=  customer.customerID', array("customerName", "customerShortName", "customerStatus"), "left");
		$select->join('jobType', 'job.jobTypeId=  jobType.jobTypeId', array("jobTypeName"), "left");
		$select->join('project', 'job.projectId=  project.projectId', array("projectType"), "left");
		$select->join('projectType', 'projectType.projectTypeID=  project.projectType', array("projectTypeName"), "left");
		$select->join('entity', 'job.entityID=  entity.entityID', array("deleted"), "left");
		$select->where(array('job.jobStatus' => 8));

		if ($locationID) {
			$select->where(array('job.locationID' => $locationID));
		}
		if ($paginated) {
			$paginatorAdapter = new DbSelect(
				$select, $this->tableGateway->getAdapter());
			$paginator = new Paginator($paginatorAdapter);
			return $paginator;
		} else {
			$statement = $sql->prepareStatementForSqlObject($select);
			$result = $statement->execute();
			return $result;
		}
	}

	/**
	 * get job list by status
	 * @param int $status
	 * @param int $locationID
	 * @return mixesdjob
	 */
	public function getJobListByStatus($status, $locationID, $paginated = false, $startDate = null, $endDate = null, $offset = null, $orderBy=null) {
		$adapter = $this->tableGateway->getAdapter();
		$sql = new Sql($adapter);
		$select = $sql->select();
		$select->from('job')
			->columns(array('*'))
			->join('jobVehicle', 'job.jobId =  jobVehicle.jobId', array("vehicleID","jobVehicleKMs"), "left")
			->join('serviceVehicle', 'jobVehicle.vehicleID =  serviceVehicle.serviceVehicleID', array('serviceVehicleID', 'vehicleType', 'vehicleModel', 'serviceVehicleRegNo', 'serviceVehicleEngineNo','vehicleSubModel'), "left")
			->join('customer', 'serviceVehicle.customerID=  customer.customerID', array("customerName", "customerShortName", "customerStatus","customerTelephoneNumber"), "left")
			->join('user', 'job.supervisorId =  user.userID', array("userID", "userFirstName"), "left")
			->join('vehicleType', "serviceVehicle.vehicleType= vehicleType.vehicleTypeID", array('vehicleTypeName'), 'left')
        	->join('vehicleModel', "serviceVehicle.vehicleModel= vehicleModel.vehicleModelID", array('vehicleModelName'), 'left')
        	->join('vehiclSubModel', "serviceVehicle.vehicleSubModel= vehiclSubModel.vehicleSubModelID", array('vehicleSubModelName'), 'left')
			->join('entity', 'job.entityID=  entity.entityID', array("deleted", "createdTimeStamp", "updatedTimeStamp"), "left")
			->join('jobTask', 'job.jobId =  jobTask.jobID', array("jobTaskTaskCardID"), "left")
			->join('taskCard', 'jobTask.jobTaskTaskCardID=  taskCard.taskCardID', array("taskCardName"), "left");
			if ($status == 9) {
				$arr = array(9, 20, 21);
				$select->where(['job.locationID' => $locationID, 'jobStatus' =>$arr, 'jobVehicle.deleted' => 0]);	
				
			} elseif ($status == 8) {
				$arr = array(8, 17);
				$select->where(['job.locationID' => $locationID, 'jobStatus' =>$arr, 'jobVehicle.deleted' => 0]);	
			} else {
				$select->where(['job.locationID' => $locationID, 'jobStatus' => $status, 'jobVehicle.deleted' => 0]);		
			}

			if(is_null($orderBy)) {
				$select->order('jobId DESC');

			} else {
				$select->order('entity.updatedTimeStamp DESC');
			}

			if (!is_null($offset)) {
				$select->limit(6)
	            ->offset($offset);
			}
		if (!is_null($startDate) && !is_null($endDate)) {
			// $select->where->between('entity.createdTimeStamp', $startDate, $endDate);
		}


		$select->group('job.jobId');
		if ($paginated) {
			$paginatorAdapter = new DbSelect(
			$select, $this->tableGateway->getAdapter());
			$paginator = new Paginator($paginatorAdapter);
			return $paginator;
		} else {
			$statement = $sql->prepareStatementForSqlObject($select);
			$results = $statement->execute();
			return (object) $results;
		}
	}

	/**
	 * get job list by status
	 * @param int $status
	 * @param int $locationID
	 * @return mixesdjob
	 */
	public function getJobCounttByStatus($status, $locationID, $paginated = false, $startDate = null, $endDate = null) {
		$adapter = $this->tableGateway->getAdapter();
		$sql = new Sql($adapter);
		$select = $sql->select();
		$select->from('job')
			->order('jobId DESC')
			->columns(array('*'))
			->join('jobVehicle', 'job.jobId =  jobVehicle.jobId', array("vehicleID","jobVehicleKMs"), "left")
			->join('serviceVehicle', 'jobVehicle.vehicleID =  serviceVehicle.serviceVehicleID', array('serviceVehicleID', 'vehicleType', 'vehicleModel', 'serviceVehicleRegNo', 'serviceVehicleEngineNo','vehicleSubModel'), "left")
			->join('customer', 'serviceVehicle.customerID=  customer.customerID', array("customerName", "customerShortName", "customerStatus","customerTelephoneNumber"), "left")
			->join('user', 'job.supervisorId =  user.userID', array("userID", "userFirstName"), "left")
			->join('vehicleType', "serviceVehicle.vehicleType= vehicleType.vehicleTypeID", array('vehicleTypeName'), 'left')
        	->join('vehicleModel', "serviceVehicle.vehicleModel= vehicleModel.vehicleModelID", array('vehicleModelName'), 'left')
        	->join('vehiclSubModel', "serviceVehicle.vehicleSubModel= vehiclSubModel.vehicleSubModelID", array('vehicleSubModelName'), 'left')
			->join('entity', 'job.entityID=  entity.entityID', array("deleted", "createdTimeStamp", "updatedTimeStamp"), "left")
			->join('jobTask', 'job.jobId =  jobTask.jobID', array("jobTaskTaskCardID"), "left")
			->join('taskCard', 'jobTask.jobTaskTaskCardID=  taskCard.taskCardID', array("taskCardName"), "left");
			if ($status == 9) {
				$arr = array(9, 20, 21);
				$select->where(['job.locationID' => $locationID, 'jobStatus' =>$arr, 'jobVehicle.deleted' => 0]);	
				
			} elseif ($status == 8) {
				$arr = array(8, 17);
				$select->where(['job.locationID' => $locationID, 'jobStatus' =>$arr, 'jobVehicle.deleted' => 0]);	
			} else {
				$select->where(['job.locationID' => $locationID, 'jobStatus' => $status, 'jobVehicle.deleted' => 0]);		
			}
		if (!is_null($startDate) && !is_null($endDate)) {
			// $select->where->between('entity.createdTimeStamp', $startDate, $endDate);
		}
		$select->group('job.jobId');
		$statement = $sql->prepareStatementForSqlObject($select);
		$results = $statement->execute();
		return $results->count();
	}

	/**
     * update JobTask End time stamp
     * @param int $jobTaskID
     * @param string $endedAt
     * @return boolean
     */
    public function updateJobIsRestarted($jobID, $state, $status)
    {
        $jobData = array(
            'isRestarted' => $state,
            'old_status' => $status
        );
        try {
            $this->tableGateway->update($jobData, array('jobId' => $jobID));
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
        return true;
    }


    /**
	 * get job by service jobId
	 * @param int $status
	 * @param int $locationID
	 * @return mixesd
	 */
	public function getServiceJobById($jobID, $locationID = null, $customer = true) {
		$adapter = $this->tableGateway->getAdapter();
		$sql = new Sql($adapter);
		$select = $sql->select();
		$select->from('job')
			->order('jobId DESC')
			->columns(array('*'))
			->join('customer', 'job.customerID=  customer.customerID', array("customerName", "customerShortName", "customerStatus", "customerCode",'customerTelephoneNumber'), "left")
			->join('customerProfile', 'customer.customerID=  customerProfile.customerID', array("customerProfileEmail", "customerProfileLocationNo"), "left")
			->join('jobVehicle', 'job.jobId =  jobVehicle.jobId', array("jobVehicleID","vehicleID", "jobVehicleKMs", "deleted"), "left")
			->join('serviceVehicle', 'jobVehicle.vehicleID =  serviceVehicle.serviceVehicleID', array('serviceVehicleID', 'vehicleType', 'vehicleModel', 'serviceVehicleRegNo', 'serviceVehicleEngineNo','fuelTypeID','vehicleSubModel'), "left")
			->join('user', 'job.supervisorId =  user.userID', array("userID", "userFirstName"), "left")
			->join('vehicleType', "serviceVehicle.vehicleType= vehicleType.vehicleTypeID", array('vehicleTypeName'), 'left')
        	->join('vehicleModel', "serviceVehicle.vehicleModel= vehicleModel.vehicleModelID", array('vehicleModelName'), 'left')
        	->join('vehiclSubModel', "serviceVehicle.vehicleSubModel= vehiclSubModel.vehicleSubModelID", array('vehicleSubModelName'), 'left')
			->join('entity', 'job.entityID=  entity.entityID', array("deleted", "createdTimeStamp", "updatedTimeStamp"), "left")
			->join('jobTask', 'job.jobId =  jobTask.jobID', array("jobTaskTaskCardID"), "left")
			->join('taskCard', 'jobTask.jobTaskTaskCardID=  taskCard.taskCardID', array("taskCardName", "taskCardCode"), "left");
		$select->where(['job.jobId' => $jobID, 'jobVehicle.deleted' => false]);
		
		if (!is_null($locationID)) {
			$select->where(['job.locationID' => $locationID]);	
		}

		if ($customer) {
			$select->where(['customerProfile.isPrimary' => 1]);	
		}

		$select->group('job.jobId');
		
		$statement = $sql->prepareStatementForSqlObject($select);
		$results = $statement->execute();
		return  $results->current();
	
	}


	 /**
     * This function is used to get stated jobs with its services
     */
    public function getStartedJobsWithServices($paginated = false)
    {
    	$adapter = $this->tableGateway->getAdapter();
		$sql = new Sql($adapter);
		$select = $sql->select();
		$select->from('job')
			->columns(array('*'));
        $select->join('user', 'user.userID = job.supervisorId', array('userFirstName','userLastName'),'left');
		$select->join('jobTask', 'job.jobId = jobTask.jobID', array('jobTaskID','taskID','jobTaskStatus','startedAt'),'left');
        $select->join('task', 'task.taskID = jobTask.taskID', array('taskCode','taskName'),'left');
        $select->join('departmentStation', 'departmentStation.departmentStationId = jobTask.stationID', array('departmentStationName'),'left');
     	$select->where->notEqualTo('jobTask.jobTaskStatus', 3)
			->where->notEqualTo('jobTask.jobTaskStatus', 4)
			->where->notEqualTo('jobTask.jobTaskStatus', 5)
			->where->notEqualTo('jobTask.jobTaskStatus', 9)
			->where->notEqualTo('jobTask.jobTaskStatus', 10)
			->where->notEqualTo('job.jobStatus', 5);   
		$select->order('jobId DESC');
		if ($paginated) {
			$paginatorAdapter = new DbSelect(
				$select, $this->tableGateway->getAdapter());
			$paginator = new Paginator($paginatorAdapter);
			return $paginator;
		} else {
			$statement = $sql->prepareStatementForSqlObject($select);
			$result = $statement->execute();
			return $result;
		}
    }

    public function getJobServiceBySearchKey($jobSearchKey = null, $jobCard = null, $serviceType = null, $startDate = null, $endDate = null) {

		$adapter = $this->tableGateway->getAdapter();
		$sql = new Sql($adapter);
		$select = $sql->select();
		$select->from('job')
			->columns(array('*'));
		$select->join('user', 'user.userID = job.supervisorId', array('userFirstName','userLastName'),'left');
		$select->join('jobTask', 'job.jobId = jobTask.jobID', array('jobTaskID','taskID','jobTaskStatus','startedAt'),'left');
        $select->join('task', 'task.taskID = jobTask.taskID', array('taskCode','taskName'),'left');
     	$select->where->notEqualTo('jobTask.jobTaskStatus', 3)
			->where->notEqualTo('jobTask.jobTaskStatus', 4)
			->where->notEqualTo('jobTask.jobTaskStatus', 5)
			->where->notEqualTo('jobTask.jobTaskStatus', 9)
			->where->notEqualTo('jobTask.jobTaskStatus', 10);

		if (!is_null($jobSearchKey)) {
			$select->where(new PredicateSet(array(new Operator('job.jobReferenceNumber', 'like', '%' . $jobSearchKey . '%'))));
		}

		if (!is_null($jobCard)) {
			$select->where(new PredicateSet(array(new Operator('jobTask.jobTaskTaskCardID', '=', $jobCard))));
		}

		if (!is_null($serviceType)) {
			$select->where(new PredicateSet(array(new Operator('task.taskID', '=', $serviceType))));
		}

		if ($startDate != null && $endDate != null) {
			$select->where->between('jobTask.startedAt', $startDate, $endDate);
		}

		$statement = $sql->prepareStatementForSqlObject($select);
		$results = $statement->execute();
		return $results;
	}

	public function getJobServiceByJobServiceID($jobServiceID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('jobTask')
        	   ->columns(array('jobTaskID','taskID','taskUomID' => new Expression('jobTask.uomID'),'jobTaskUnitCost','jobTaskUnitRate','jobTaskEstQty','jobTaskTaskCardID','jobTaskStatus','startedAt'));
        $select->join('job', 'job.jobId = jobTask.jobID', array('*'),'left');
        $select->join('customer', 'job.customerID=  customer.customerID', array("customerName", "customerCode"), "left");
        $select->join('jobProduct', 'jobTask.jobTaskID = jobProduct.jobTaskID', array('jobProductID','locationProductID','jobProductUnitPrice','uomID','jobProductAllocatedQty','jobProductIssuedQty','jobProductReOrderLevel','jobProductMaterialTypeID','jobProductTaskCardID','jobProductTaskID' => new Expression('jobProduct.jobTaskID'),'jobProductStatus'),'left');
        $select->join('locationProduct', 'jobProduct.locationProductID = locationProduct.locationProductID', array('productID'),'left');
        $select->join('product', 'locationProduct.productID = product.productID', array('productCode', 'productName', 'productTypeID'),'left');
        $select->join('productUom', 'locationProduct.productID = productUom.productID', array('uomID', 'productUomConversion', 'productUomBase'),'left');
        $select->join(['uom1' => 'uom'], 'productUom.uomID = uom1.uomID', array('uomAbbr', 'uomDecimalPlace'),'left');
        $select->join('task', 'task.taskID = jobTask.taskID', array('taskCode','taskName'),'left');
        $select->join(['uom2' => 'uom'], 'jobTask.uomID = uom2.uomID', array('taskUomAbbr' => new Expression('uom2.uomAbbr'), 'uomName'),'left');
        $select->join('user', 'user.userID = job.supervisorId', array('userFirstName','userLastName'),'left');
        $select->join('departmentStation', 'departmentStation.departmentStationId = jobTask.stationID', array('departmentStationName'),'left');
        $select->where(array('jobTask.jobTaskID' => $jobServiceID));
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
	}


	/**
     * This function is used to get completed jobs with its services
     */
    public function getCompletedJobsWithServices($paginated = false, $jobID = null, $jobCardFlag = true, $jobCostViewFlag = false, $vehicleReleaseFlag = false)
    {
    	$adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('job')
            ->order('jobId DESC')
            ->columns(array('*'))
            ->join('customer', 'job.customerID = customer.customerID', array("customerName", "customerShortName", "customerStatus",'customerTelephoneNumber'), "left")
            ->join('jobVehicle', 'job.jobId =  jobVehicle.jobId', array("vehicleID"), "left")
            ->join('serviceVehicle', 'jobVehicle.vehicleID =  serviceVehicle.serviceVehicleID', array('serviceVehicleID', 'vehicleType', 'vehicleModel', 'serviceVehicleRegNo', 'serviceVehicleEngineNo'), "left")
            ->join('user', 'job.supervisorId =  user.userID', array("userLastName", "userFirstName"), "left")
            ->join('vehicleType', "serviceVehicle.vehicleType= vehicleType.vehicleTypeID", array('vehicleTypeName'), 'left')
           ->join('vehicleModel', "serviceVehicle.vehicleModel= vehicleModel.vehicleModelID", array('vehicleModelName'), 'left')
            ->join('entity', 'job.entityID=  entity.entityID', array("deleted", "createdTimeStamp", "updatedTimeStamp"), "left")
            ->join('jobTask', 'job.jobId =  jobTask.jobID', array("jobTaskTaskCardID",'jobTaskID'), "left");

        if ($vehicleReleaseFlag) {
        	$select->where->in('jobStatus',[20,21]);
        } else {
        	$select->where->in('jobStatus',[9,20]);
        }

        if ($jobCardFlag) {
        	$select->join('taskCard', 'jobTask.jobTaskTaskCardID=  taskCard.taskCardID', array("taskCardName"), "left");
        }

        if (is_null($jobID)) {
        	$select->group('job.jobId');
        	$select->where(['jobVehicle.deleted' => 0]);
        } else {
        	$select->join('task', 'task.taskID = jobTask.taskID', array('taskCode','taskName','taskID'),'left');
        	$select->join('jobSubTask', 'jobSubTask.jobTaskId = jobTask.jobTaskID', array('jobSubTaskID','jobTaskIdOfJobSubtask' => new Expression('jobSubTask.jobTaskId')),'left');
        	$select->join('serviceSubTask', 'jobSubTask.subTaskId = serviceSubTask.subTaskId', array('subTaskId','subTaskCode','subTaskName'),'left');
        	$select->join('jobProduct', 'job.jobId = jobProduct.jobID', array('jobProductID','locationProductID','jobProductUnitPrice','uomID','jobProductAllocatedQty','jobProductIssuedQty','jobProductReOrderLevel','jobProductMaterialTypeID','jobProductTaskCardID','jobProductTaskID' => new Expression('jobProduct.jobTaskID'),'jobProductStatus'),'left');
	        $select->join('locationProduct', 'jobProduct.locationProductID = locationProduct.locationProductID', array('productID'),'left');
	        $select->join(['product2' => 'product'], 'locationProduct.productID = product2.productID', array('productCode', 'productName', 'productTypeID'),'left');
	        $select->join('productUom', 'locationProduct.productID = productUom.productID', array('uomID', 'productUomConversion', 'productUomBase'),'left');
	        $select->join(['uom1' => 'uom'], 'productUom.uomID = uom1.uomID', array('uomAbbr', 'uomDecimalPlace'),'left');
        	$select->where(['job.jobId' => $jobID, 'jobVehicle.deleted' => 0]);
        }

        if ($jobCostViewFlag) {
        	$select->join('jobTaskCost','jobTask.jobTaskID = jobTaskCost.jobTaskID', array('unitRate','costedTaskQty' => new Expression('jobTaskCost.Qty')),'left');
        	$select->join('jobProductCost','jobProduct.jobProductID = jobProductCost.jobProductID', array('productCost','costedProductQty' => new Expression('jobProductCost.Qty')),'left');
        	$select->join('otherJobCost','job.jobId = otherJobCost.jobID', array('otherJobCostUnitCost','otherJobCostNoOfUnits','otherJobCostID'),'left');
        	$select->join(['product1' => 'product'], 'otherJobCost.costType = product1.productID', array('otherCostProductCode' => new Expression('product1.productCode'),'otherCostProductName' => new Expression('product1.productName')), 'left');
        }

        if ($paginated) {
            $paginatorAdapter = new DbSelect(
            $select, $this->tableGateway->getAdapter());
            $paginator = new Paginator($paginatorAdapter);
            return $paginator;
        } else {
            $statement = $sql->prepareStatementForSqlObject($select);
            $results = $statement->execute();
            return (object) $results;
        }
    }

    /**
     * This function is used to get jobs with its services
     */
    public function getJobsWithServices($paginated = false, $jobID = null, $jobCardFlag = true, $jobCostViewFlag = false, $vehicleReleaseFlag = false)
    {
    	$adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('job')
            ->order('jobId DESC')
            ->columns(array('*'))
            ->join('customer', 'job.customerID = customer.customerID', array("customerName", "customerShortName", "customerStatus",'customerTelephoneNumber'), "left")
            ->join('jobVehicle', 'job.jobId =  jobVehicle.jobId', array("vehicleID"), "left")
            ->join('serviceVehicle', 'jobVehicle.vehicleID =  serviceVehicle.serviceVehicleID', array('serviceVehicleID', 'vehicleType', 'vehicleModel', 'serviceVehicleRegNo', 'serviceVehicleEngineNo'), "left")
            ->join('user', 'job.supervisorId =  user.userID', array("userLastName", "userFirstName"), "left")
            ->join('vehicleType', "serviceVehicle.vehicleType= vehicleType.vehicleTypeID", array('vehicleTypeName'), 'left')
           ->join('vehicleModel', "serviceVehicle.vehicleModel= vehicleModel.vehicleModelID", array('vehicleModelName'), 'left')
            ->join('entity', 'job.entityID=  entity.entityID', array("deleted", "createdTimeStamp", "updatedTimeStamp"), "left")
            ->join('jobTask', 'job.jobId =  jobTask.jobID', array("jobTaskTaskCardID",'jobTaskID'), "left");

        if ($jobCardFlag) {
        	$select->join('taskCard', 'jobTask.jobTaskTaskCardID=  taskCard.taskCardID', array("taskCardName"), "left");
        }

        if (is_null($jobID)) {
        	$select->group('job.jobId');
        } else {
        	$select->join('task', 'task.taskID = jobTask.taskID', array('taskCode','taskName','taskID'),'left');
        	$select->join('jobSubTask', 'jobSubTask.jobTaskId = jobTask.jobTaskID', array('jobSubTaskID','jobTaskIdOfJobSubtask' => new Expression('jobSubTask.jobTaskId')),'left');
        	$select->join('serviceSubTask', 'jobSubTask.subTaskId = serviceSubTask.subTaskId', array('subTaskId','subTaskCode','subTaskName'),'left');
        	$select->join('jobProduct', 'job.jobId = jobProduct.jobID', array('jobProductID','locationProductID','jobProductUnitPrice','uomID','jobProductAllocatedQty','jobProductIssuedQty','jobProductReOrderLevel','jobProductMaterialTypeID','jobProductTaskCardID','jobProductTaskID' => new Expression('jobProduct.jobTaskID'),'jobProductStatus'),'left');
	        $select->join('locationProduct', 'jobProduct.locationProductID = locationProduct.locationProductID', array('productID'),'left');
	        $select->join(['product2' => 'product'], 'locationProduct.productID = product2.productID', array('productCode', 'productName', 'productTypeID'),'left');
	        $select->join('productUom', 'locationProduct.productID = productUom.productID', array('uomID', 'productUomConversion', 'productUomBase'),'left');
	        $select->join(['uom1' => 'uom'], 'productUom.uomID = uom1.uomID', array('uomAbbr', 'uomDecimalPlace'),'left');
        	$select->where(['job.jobId' => $jobID, 'jobVehicle.deleted' => 0]);
        }

        if ($jobCostViewFlag) {
        	$select->join('jobTaskCost','jobTask.jobTaskID = jobTaskCost.jobTaskID', array('unitRate','costedTaskQty' => new Expression('jobTaskCost.Qty')),'left');
        	$select->join('jobProductCost','jobProduct.jobProductID = jobProductCost.jobProductID', array('productCost','costedProductQty' => new Expression('jobProductCost.Qty')),'left');
        	$select->join('otherJobCost','job.jobId = otherJobCost.jobID', array('otherJobCostUnitCost','otherJobCostNoOfUnits','otherJobCostID'),'left');
        	$select->join(['product1' => 'product'], 'otherJobCost.costType = product1.productID', array('otherCostProductCode' => new Expression('product1.productCode'),'otherCostProductName' => new Expression('product1.productName')), 'left');
        }

        if ($paginated) {
            $paginatorAdapter = new DbSelect(
            $select, $this->tableGateway->getAdapter());
            $paginator = new Paginator($paginatorAdapter);
            return $paginator;
        } else {
            $statement = $sql->prepareStatementForSqlObject($select);
            $results = $statement->execute();
            return (object) $results;
        }
    }



    /**
     * This function is used to search completed job in service costing
     *
     */
    public function getJobBySearchKeyForCosting($jobSearchKey = null, $jobCard = null, $serviceType = null, $startDate = null, $endDate = null, $searchForVehicleRelase = false) {

		$adapter = $this->tableGateway->getAdapter();
		$sql = new Sql($adapter);
		$select = $sql->select();
		$select->from('job')
            ->order('jobId DESC')
            ->columns(array('*'))
            ->join('customer', 'job.customerID = customer.customerID', array("customerName", "customerShortName", "customerStatus",'customerTelephoneNumber'), "left")
            ->join('jobVehicle', 'job.jobId =  jobVehicle.jobId', array("vehicleID"), "left")
            ->join('serviceVehicle', 'jobVehicle.vehicleID =  serviceVehicle.serviceVehicleID', array('serviceVehicleID', 'vehicleType', 'vehicleModel', 'serviceVehicleRegNo', 'serviceVehicleEngineNo'), "left")
            ->join('user', 'job.supervisorId =  user.userID', array("userLastName", "userFirstName"), "left")
            ->join('vehicleType', "serviceVehicle.vehicleType= vehicleType.vehicleTypeID", array('vehicleTypeName'), 'left')
           ->join('vehicleModel', "serviceVehicle.vehicleModel= vehicleModel.vehicleModelID", array('vehicleModelName'), 'left')
            ->join('entity', 'job.entityID=  entity.entityID', array("deleted", "createdTimeStamp", "updatedTimeStamp"), "left")
            ->join('jobTask', 'job.jobId =  jobTask.jobID', array("jobTaskTaskCardID"), "left")
            ->join('task', 'jobTask.taskID =  task.taskID', array("*"), "left")
            ->join('taskCard', 'jobTask.jobTaskTaskCardID=  taskCard.taskCardID', array("taskCardName"), "left");
        $select->where('jobTask.jobTaskTaskCardID is NOT NULL');
        $select->group('job.jobId');

        if ($searchForVehicleRelase) {
        	$select->where(['jobStatus' => [20,21]]);
        } else {
        	$select->where(['jobStatus' => [9,20]]);
        }

		if (!is_null($jobSearchKey)) {
			$select->where(new PredicateSet(array(new Operator('serviceVehicle.serviceVehicleRegNo', 'like', '%' . $jobSearchKey . '%'), new Operator('customer.customerName', 'like', '%' . $jobSearchKey . '%'), new Operator('customer.customerTelephoneNumber', 'like', '%' . $jobSearchKey . '%')), PredicateSet::OP_OR));
		}

		if (!is_null($jobCard)) {
			$select->where(new PredicateSet(array(new Operator('jobTask.jobTaskTaskCardID', '=', $jobCard))));
		}

		if (!is_null($serviceType)) {
			$select->where(array('task.taskID' => $serviceType));
		}

		if ($startDate != null && $endDate != null) {
			$select->where->between('job.completed_at', $startDate, $endDate);
		}

		$statement = $sql->prepareStatementForSqlObject($select);
		$results = $statement->execute();
		return $results;
	}


	/**
	 * get job by search key
	 * @param int $status
	 * @param int $jobSearchKey
	 * @param int $jobCard
	 * @param int $serviceType
	 * @param int $startDate
	 * @param int $endDate
	 * @return mixesd
	 */
	public function getJobBySearchKey($status, $jobSearchKey = null, $jobCard = null, $serviceType = null, $startDate = null, $endDate = null) {
		$jobSearchKey = trim($jobSearchKey);
	 	$adapter = $this->tableGateway->getAdapter();
		$sql = new Sql($adapter);
		$select = $sql->select();
		$select->from('job')
			->order('jobId DESC')
			->columns(array('*'))
			->join('customer', 'job.customerID=  customer.customerID', array("customerName", "customerShortName", "customerStatus", "customerTelephoneNumber"), "left")
			->join('jobVehicle', 'job.jobId =  jobVehicle.jobId', array("vehicleID"), "left")
			->join('serviceVehicle', 'jobVehicle.vehicleID =  serviceVehicle.serviceVehicleID', array('serviceVehicleID', 'vehicleType', 'vehicleModel', 'serviceVehicleRegNo', 'serviceVehicleEngineNo'), "left")
			->join('user', 'job.supervisorId =  user.userID', array("userID", "userFirstName"), "left")
			->join('vehicleType', "serviceVehicle.vehicleType= vehicleType.vehicleTypeID", array('vehicleTypeName'), 'left')
        	->join('vehicleModel', "serviceVehicle.vehicleModel= vehicleModel.vehicleModelID", array('vehicleModelName'), 'left')
			->join('entity', 'job.entityID=  entity.entityID', array("deleted", "createdTimeStamp", "updatedTimeStamp"), "left")
			->join('jobTask', 'job.jobId =  jobTask.jobID', array("jobTaskTaskCardID"), "left")
			->join('task', 'task.taskID = jobTask.taskID', array('taskCode','taskName'),'left')
			->join('taskCard', 'jobTask.jobTaskTaskCardID=  taskCard.taskCardID', array("taskCardName"), "left");

			if ($status == 9) {
				$arr = array(9, 20, 21);
				$select->where(['jobStatus' => $arr]);	
			} else {
				$select->where(['jobStatus' => $status]);	
			}
	
		if (!is_null($jobSearchKey)) {
			$select->where(new PredicateSet(array(new Operator('job.jobReferenceNumber', 'like', '%' . $jobSearchKey . '%'), new Operator('customer.customerName', 'like', '%' . $jobSearchKey . '%'), new Operator('serviceVehicle.serviceVehicleRegNo', 'like', '%' . $jobSearchKey . '%'), new Operator('customer.customerTelephoneNumber', 'like', '%' . $jobSearchKey . '%')), PredicateSet::OP_OR));
		}

		if (!is_null($jobCard)) {
			$select->where(new PredicateSet(array(new Operator('jobTask.jobTaskTaskCardID', '=', $jobCard))));
		}

		if (!is_null($serviceType)) {
			$select->where(new PredicateSet(array(new Operator('task.taskID', '=', $serviceType))));
		}

		if ($startDate != null && $endDate != null) {
			$select->where->between('entity.createdTimeStamp', $startDate, $endDate);
		}

		// $select->where('jobTask.jobTaskTaskCardID is NOT NULL');
		$select->group('job.jobId');

		$statement = $sql->prepareStatementForSqlObject($select);
		$results = $statement->execute();
		return $results;
	}

	 /**
     * update Job Complete time stamp
     * @param int $jobID
     * @param string $completedTime
     * @return boolean
     */
    public function updateJobCompletedTimeByJobID($jobID, $completedTime)
    {
        $jobData = array(
            'completed_at' => $completedTime
        );
        try {
            $this->tableGateway->update($jobData, array('jobId' => $jobID));
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
        return true;
    }

    public function getJobEmployeesByJobId($jobID) {
		$adapter = $this->tableGateway->getAdapter();
		$sql = new Sql($adapter);
		$select = $sql->select();
		$select->from('job')
			->order('jobId DESC')
			->columns(array('*'))
			->join('jobTask', 'job.jobId =  jobTask.jobID', array("jobTaskTaskCardID"), "left");
		$select->join('jobTaskEmployee','jobTaskEmployee.jobTaskID = jobTask.jobTaskID',array('jobTaskEmployeeID'),'left');
        $select->join('employee','employee.employeeID = jobTaskEmployee.employeeID',array('employeeCode','employeeFirstName','employeeSecondName'),'left');
		$select->where(['job.jobId' => $jobID]);
		
		$statement = $sql->prepareStatementForSqlObject($select);
		$results = $statement->execute();
		return  $results;
	
	}

	public function updateJobCustomer($jobID, $customerID) {
		$data = array('customerID' => $customerID);
		try {
			$this->tableGateway->update($data, array('jobId' => $jobID));
			return true;
		} catch (Exception $exc) {
			echo $exc->getTraceAsString();
			return false;
		}
	}

	public function getDimensionByDimensionValueIDArr($dimensionValueIds)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('job');
        $select->where->in('job.jobID', $dimensionValueIds);
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if (!$rowset) {
            return NULL;
        }
        return $rowset;
    }

    //get all Job details that related to Location
	public function getJobByID($jobID) {
		$adapter = $this->tableGateway->getAdapter();
		$sql = new Sql($adapter);
		$select = $sql->select();
		$select->from('job');
//        $select->join('customer', 'job.customerID=  customer.customerID', array("customerName", "customerShortName"), "left");
		//        $select->join('jobType', 'job.jobTypeId=  jobType.jobTypeId', array("jobTypeName"), "left");
		$select->join('entity', 'job.entityID=  entity.entityID', array("deleted"), "left");
		$select->where(array('job.jobId' => $jobID, 'entity.deleted' => 0));
		$statement = $sql->prepareStatementForSqlObject($select);
		$result = $statement->execute();

		return $result;
	}


}
