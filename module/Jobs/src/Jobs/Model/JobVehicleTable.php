<?php

namespace Jobs\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Jobs\Model\JobVehicle;
use Zend\Db\Sql\Sql;
use Core\Model\ProductTable as CoreProductTable;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Predicate\PredicateSet;
use Zend\Db\Sql\Predicate\Between;
use Zend\Db\Sql\Predicate\Operator;

class JobVehicleTable extends CoreProductTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }


    public function saveJobVehicle($jobVehicleData) {
		$jobVehicleSaveData = array(
			'vehicleID' => $jobVehicleData->vehicleID,
			'jobVehicleKMs' => $jobVehicleData->jobVehicleKMs,
			'jobId' => $jobVehicleData->jobId,
			'deleted' => 0
		);
		if ($this->tableGateway->insert($jobVehicleSaveData)) {
			$result = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
			return $result;
		} else {
			return FALSE;
		}
	}

	/**
     * get job vehicle by job id
     * @param int $jobId
     * @param int $vehicleId
     * @return mixed
     */
    public function getJobVehicle($jobId, $vehicleId)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('jobVehicle');
        $select->columns(array("*"));
        $select->where(array('vehicleID' => $vehicleId, 'jobId'=> $jobId, 'deleted'=> 0));
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if (!$rowset) {
            return NULL;
        }
        return $rowset;
    }

   	public function getJobVehicleByJobID($jobId)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('jobVehicle');
        $select->columns(array("*"));
        $select->where(array('jobId'=> $jobId, 'deleted'=> 0));
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if (!$rowset) {
            return NULL;
        }
        return $rowset;
    }

    /**
     * delete job vehicle by jobVehicleId
     * @param int $jobId
     * @param int $vehicleId
     * @return mixed
     */
    public function deleteJobVehicle($jobVehicleId)
    {
        $jobVehicleData = array(
            'deleted' => 1,
        );
        try {
            $this->tableGateway->update($jobVehicleData, array('jobVehicleID' => $jobVehicleId));
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
        return true;
    }
    
    public function updateJobVehicle($jobVehicleData, $jobVehicleId)
    {
        try {
            $this->tableGateway->update($jobVehicleData, array('jobVehicleID' => $jobVehicleId));
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
        return true;
    }
    
    public function getServiceVehicleByJobID($jobId)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('jobVehicle');
        $select->columns(array("*"))
               ->join('serviceVehicle', 'jobVehicle.vehicleID =  serviceVehicle.serviceVehicleID', array('serviceVehicleID', 'vehicleType', 'vehicleModel', 'serviceVehicleRegNo', 'serviceVehicleEngineNo','vehicleSubModel'), "left");
        $select->where(array('jobId'=> $jobId, 'deleted'=> 0));
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if (!$rowset) {
            return NULL;
        }
        return $rowset;
    }
}
