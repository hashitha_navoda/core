<?php

namespace Jobs\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class Contractor
{

    public $contractorID;
    public $contractorFirstName;
    public $contractorSecondName;
    public $contractorTP;
    public $entityID;
    public $contractorStatus;
    public $IsPermanentFlag;
    public $assignForJob;
    public $assignForTask;

    public function exchangeArray($data)
    {
        $this->contractorID = (!empty($data['contractorID'])) ? $data['contractorID'] : null;
        $this->contractorFirstName = (!empty($data['contractorFirstName'])) ? $data['contractorFirstName'] : null;
        $this->contractorSecondName = (!empty($data['contractorSecondName'])) ? $data['contractorSecondName'] : null;
        $this->contractorTP = (!empty($data['contractorTP'])) ? $data['contractorTP'] : null;
        $this->entityID = (!empty($data['entityID'])) ? $data['entityID'] : null;
        $this->contractorStatus = (!empty($data['contractorStatus'])) ? $data['contractorStatus'] : null;
        $this->IsPermanentFlag = (!empty($data['IsPermanentFlag'])) ? $data['IsPermanentFlag'] : null;
        $this->assignForJob = (!empty($data['assignForJob'])) ? $data['assignForJob'] : null;
        $this->assignForTask = (!empty($data['assignForTask'])) ? $data['assignForTask'] : null;
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory = new InputFactory();

            $inputFilter->add($factory->createInput(array(
                        'name' => 'contractorID',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'Int'),
                        ),
            )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'contractorFirstName',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'StringLength',
                                'options' => array(
                                    'encoding' => 'UTF-8',
                                    'min' => 1,
                                    'max' => 255,
                                ),
                            ),
                        ),
            )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'projectTypeCode',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'StringLength',
                                'options' => array(
                                    'encoding' => 'UTF-8',
                                    'min' => 1,
                                    'max' => 10,
                                ),
                            ),
                        ),
            )));

            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

}
