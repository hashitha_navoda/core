<?php

namespace Jobs\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Jobs\Model\JobContractor;
use Zend\Db\Sql\Sql;
use Core\Model\ProductTable as CoreProductTable;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Predicate\PredicateSet;
use Zend\Db\Sql\Predicate\Between;
use Zend\Db\Sql\Predicate\Operator;

class JobContractorTable extends CoreProductTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

	public function saveContractor($jobContractorData) {
		$jobContractorSaveData = array(
			'contractorID' => $jobContractorData->contractorID,
			'jobID' => $jobContractorData->jobID,
		);
		if ($this->tableGateway->insert($jobContractorSaveData)) {
			$result = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue('jobContractorID');
			return $result;
		} else {
			return FALSE;
		}
	}    

	public function getJobContractorByJobID($jobID) {
		$sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('jobContractor')
                ->columns(array('*'))
                ->where(array('jobContractor.jobID' => $jobID));
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if (!$rowset) {
            return NULL;
        }
        return $rowset;
	}

	public function getJobContractorDetailsByJobID($jobID) {
		$sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('jobContractor')
                ->columns(array('*'))
                ->join('contractor', 'jobContractor.contractorID =  contractor.contractorID', array('*'))
                ->where(array('jobContractor.jobID' => $jobID));
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if (!$rowset) {
            return NULL;
        }
        return $rowset;
    }

	public function deleteJobContractorByJobID($jobID) {
		try {
			$result = $this->tableGateway->delete(array('jobID' => $jobID));
			return $result;
		} catch (\Exception $e) {
			$error = 0;
			return $error;
		}
	}
}
