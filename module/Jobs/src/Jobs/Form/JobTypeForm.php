<?php

namespace Jobs\Form;

use Zend\Form\Form;

class JobTypeForm extends Form
{

    public function __construct()
    {
        parent::__construct('jobType');
        $this->setAttribute('method', 'post');
        $this->setAttribute('class', "form-horizontal");
        $this->setAttribute('role', "form");

        $this->add(array(
            'name' => 'jobTypeID',
            'type' => 'Hidden',
        ));

        $this->add(array(
            'name' => 'jobTypeName',
            'type' => 'Text',
            'options' => array(
            ),
            'attributes' => array(
                'id' => 'jobTypeName',
                'autocomplete' => false,
                'class' => 'form-control',
                'placeholder' => _('Eg: Repair')
            ),
        ));

        $this->add(array(
            'name' => 'jobTypeCode',
            'type' => 'Text',
            'options' => array(
            ),
            'attributes' => array(
                'id' => 'jobTypeCode',
                'class' => 'form-control',
                'autocomplete' => false,
                'placeholder' => _('Eg: R_123')
            ),
        ));
    }

}
