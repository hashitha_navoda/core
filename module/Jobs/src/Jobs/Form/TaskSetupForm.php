<?php

namespace Jobs\Form;

use Zend\Form\Form;

class TaskSetupForm extends Form
{

    public function __construct($data = NULL)
    {
        
        parent::__construct('taskSetup');
        $this->setAttribute('method', 'post');
        $this->setAttribute('class', "form-horizontal");
        $this->setAttribute('role', "form");

        $this->add(array(
            'name' => 'taskID',
            'type' => 'Hidden',
        ));

        $this->add(array(
            'name' => 'taskName',
            'type' => 'Text',
            'options' => array(
            ),
            'attributes' => array(
                'id' => 'taskName',
                'autocomplete' => false,
                'class' => 'form-control',
                'placeholder' => _('Eg: SLT Tower Rates')
            ),
        ));

        $this->add(array(
            'name' => 'taskCode',
            'type' => 'Text',
            'options' => array(
            ),
            'attributes' => array(
                'id' => 'taskCode',
                'class' => 'form-control',
                'autocomplete' => false,
                'placeholder' => _('Eg: TSK001')
            ),
        ));
    }

}

?>
