<?php

namespace Jobs\Form;

use Zend\Form\Form;

class TaskCardForm extends Form
{

    public function __construct($data = NULL)
    {

        parent::__construct('taskCard');
        $this->setAttribute('method', 'post');
        $this->setAttribute('class', "form-horizontal");
        $this->setAttribute('role', "form");
        
        $this->add(array(
            'name' => 'taskCardName',
            'type' => 'Text',
            'options' => array(
            ),
            'attributes' => array(
                'id' => 'taskCardName',
                'autocomplete' => false,
                'class' => 'form-control',
                'placeholder' => _('Eg: SLT Tower Rate')
            ),
        ));

        $this->add(array(
            'name' => 'taskCardCode',
            'type' => 'Text',
            'options' => array(
            ),
            'attributes' => array(
                'id' => 'taskCardCode',
                'class' => 'form-control',
                'autocomplete' => false,
                'placeholder' => _('Eg: TSKCRD0001')
            ),
        ));

        $this->add(array(
            'name' => 'defaultRate',
            'type' => 'text',
            'attributes' => array(
                'class' => 'form-control text-right',
                'id' => 'defaultRate'
            //'required' => 'true',
            ),
        ));

        $this->add(array(
            'name' => 'rate2',
            'type' => 'text',
            'attributes' => array(
                'class' => 'form-control text-right',
                'id' => 'rate2'
            //'required' => 'true',
            ),
        ));

        $this->add(array(
            'name' => 'rate3',
            'type' => 'text',
            'attributes' => array(
                'class' => 'form-control text-right',
                'id' => 'rate3'
            //'required' => 'true',
            ),
        ));

        $this->add(array(
            'name' => 'incentiveValue',
            'type' => 'text',
            'attributes' => array(
                'class' => 'form-control text-right',
                'id' => 'incentiveValue'
            //'required' => 'true',
            ),
        ));

        $this->add(array(
            'name' => 'submit',
            'type' => 'Submit',
            'attributes' => array(
                'value' => 'Go',
                'id' => 'submitbutton',
                'class' => 'btn btn-primary'
            ),
        ));
    }

}

?>
