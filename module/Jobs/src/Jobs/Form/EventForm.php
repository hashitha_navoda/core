<?php

namespace Jobs\Form;

use Zend\Form\Form;

class EventForm extends Form
{

    public function __construct($data)
    {

        $name = 'jobForm';
        $terms = $data['paymentTerms'];
        $jobtypes = $data['jobtypes'];
        $contractors = $data['contractors'];
        $jobCode = $data['jobCode'];
        
        
        parent::__construct($name);
        $this->setAttribute('method', 'post');
        $this->setAttribute("class", "form-horizontal");
        $this->setAttribute("id", $name);

        $this->add(array(
            'name' => 'jobCode',
            'type' => 'Text',
            'attributes' => array(
                'class' => 'form-control',
                'id' => 'jobCode',
                'value' => $jobCode
            ),
        ));

        $this->add(array(
            'name' => 'jobEstCost',
            'type' => 'text',
            'attributes' => array(
                'class' => 'form-control',
                'id' => 'jobEstCost',
            ),
        ));

        $this->add(array(
            'name' => 'jobValue',
            'type' => 'Text',
            'attributes' => array(
                'class' => 'form-control',
                'id' => 'jobValue',
                'autocomplete' => 'off'
            ),
        ));
        
        $this->add(array(
            'name' => 'jobName',
            'type' => 'Text',
            'attributes' => array(
                'class' => 'form-control',
                'id' => 'jobName',
                'autocomplete' => 'off'
            ),
        ));
        
        $this->add(array(
            'name' => 'jobType',
            'type' => 'Text',
            'attributes' => array(
                'class' => 'form-control',
                'id' => 'jobType',
                'autocomplete' => 'off'
            ),
        ));
        
        $this->add(array(
            'name' => 'customer',
            'type' => 'Text',
            'attributes' => array(
                'class' => 'form-control',
                'id' => 'customer',
                'autocomplete' => 'off'
            ),
        ));
        
        $this->add(array(
            'name' => 'dueDate',
            'type' => 'text',
            'attributes' => array(
                'placeholder' => 'Click to Set a Date',
                'class' => 'datepicker form-control',
                'id' => 'dueDate',
                'data-date-format' => 'yyyy-mm-dd',
            ),
        ));


        $this->add(array(
            'name' => 'estimatedEndDate',
            'type' => 'text',
            'attributes' => array(
                'placeholder' => 'Click to Set a Date',
                'class' => 'datepicker form-control',
                'id' => 'estimatedEndDate',
                'data-date-format' => 'yyyy-mm-dd',
            ),
        ));

        $this->add(array(
            'name' => 'expDate',
            'type' => 'text',
            'attributes' => array(
                'placeholder' => 'Click to Set a Date',
                'class' => 'datepicker form-control',
                'id' => 'expDate',
                'data-date-format' => 'yyyy-mm-dd',
            ),
        ));

        $this->add(array(
            'name' => 'contactorIds',
            'type' => 'Zend\Form\Element\Select',
            'attributes' => array(
                'class' => 'form-control selectpicker',
                'id' => 'contactorIds',
                'data-live-search' => "true",
                'required' => false,
                'multiple' => true,
                'title' => ' ',
            ),
            'options' => array(
                'disable_inarray_validator' => true,
                'value_options' => isset($contractors) ? $contractors : null,
            )
        ));

        $this->add(array(
            'name' => 'jobTypeIds',
            'type' => 'Zend\Form\Element\Select',
            'attributes' => array(
                'class' => 'form-control selectpicker',
                'id' => 'jobTypeIds',
                'data-live-search' => "true",
                'required' => false,
                'title' => ' ',
            ),
            'options' => array(
                'empty_option' => ' ',
                'disable_inarray_validator' => true,
                'value_options' => isset($jobtypes) ? $jobtypes : null,
            )
        ));
    }

}

?>
