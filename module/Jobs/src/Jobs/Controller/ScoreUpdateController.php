<?php

namespace Jobs\Controller;

use Zend\View\Model\ViewModel;
use Core\Controller\CoreController;
use Zend\Session\Container;

class ScoreUpdateController extends CoreController
{

    protected $sideMenus = 'tournament_side_menu';
    protected $upperMenus = 'score_update_upper_menu';

    public function listAction()
    {   
        $this->getSideAndUpperMenus('Score Update', 'Update Score', 'Jobs');
        $maintainProject = $this->user_session->jobSettings->maintainProject;
        $locationID = $this->user_session->userActiveLocation['locationID'];
        $param = 'false';

        if ($this->params()->fromRoute('param1') == 'back') {
            $param = 'true';
        } 

        $jobList = $this->getPaginatedJob($locationID);

        $progressView = new ViewModel(array(
            'maintainProject' => $maintainProject,
            'contractorForJob' => $this->user_session->jobSettings->contractorForJob,
            'paramVal' => $param,
            'paginated' => true,
            'jobs' => $jobList
        ));

        $progressView->setTemplate('jobs/score-update/list');
        $this->getViewHelper('HeadScript')->prependFile('/js/jobs/score-update.js');

        return $progressView;

    }

    public function getPaginatedJob($locationID, $param = null)
    {
        $this->job = $this->CommonTable('Jobs/Model/JobTable')->getJobListForProgressUpdate($locationID, true);
        if (is_null($param)) {
            $this->job->setCurrentPageNumber((int) $this->params()->fromRoute('param3', 1));
        } else {
            $this->job->setCurrentPageNumber((int) $param);
        }
        $this->job->setItemCountPerPage(6);
        return $this->job;
    }

    public function scoreUpdateAction () 
    {
        $jobID = $this->params()->fromRoute('param3');
        $jobData = $this->CommonTable('Jobs/Model/JobTable')->getJobDetails($jobID)->current();
        $this->getSideAndUpperMenus('Score Update', 'Update Score', 'Jobs');
        
        $participantData = $this->CommonTable('Jobs/Model/JobSupervisorTable')->getTournamentOfficialsByJobID($jobID);
        $data = [];

        foreach ($participantData as $key => $value) {
            array_push($data, $value);
        }

        $this->department = $this->CommonTable('Jobs/Model/DepartmentTable')->fetchActiveDepartments();

        $departments = array();
        foreach ($this->department as $dept) {
            $res = [
                'departmentID' => $dept['departmentID'],
                'departmentName' => $dept['departmentName'],
                'departmentCode' => $dept['departmentCode'],
            ];
            array_push($departments, $res);
        }


        $jobRelatedRequestView = new ViewModel(array(
            'jobId' => $jobID,
            'jobData' => $jobData,
            'participantData' => $data,
            'status' => $this->getStatusesList(),
            'departments' => $departments
        ));

        $jobRelatedRequestView->setTemplate('jobs/score-update/event-score-update-view');
        $this->getViewHelper('HeadScript')->prependFile('/js/jobs/score-update.js');

        return $jobRelatedRequestView;
    }

}
