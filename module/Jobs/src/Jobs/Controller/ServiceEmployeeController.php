<?php

namespace Jobs\Controller;

use Zend\View\Model\ViewModel;
use Core\Controller\CoreController;
use Zend\Session\Container;

class ServiceEmployeeController extends CoreController
{

    protected $sideMenus = 'jobs_side_menu_of_service';
    protected $upperMenus = 'jobs_employee_upper_menu_of_service';
    protected $downMenus = 'job_setup_down_menu_of_service';

    public function createAction()
    {   
        $this->getSideAndUpperMenus('Jobs Settings', 'Service Employee', 'Jobs', 'Employee Settings');

        $paginator = $this->getPaginatedEmployees();
        $this->designation = $this->CommonTable('Jobs/Model/DesignationTable')->fetchActiveDesignations();
        $this->department = $this->CommonTable('Jobs/Model/DepartmentTable')->fetchActiveDepartments();

        $designations = array();
        $departments = array();
        foreach ($this->designation as $desig) {
            $res = [
                'designationID' => $desig['designationID'],
                'designationName' => $desig['designationName'],
                'designationCode' => $desig['designationCode'],
            ];
            array_push($designations, $res);
        }

        foreach ($this->department as $dept) {
            $res = [
                'departmentID' => $dept['departmentID'],
                'departmentName' => $dept['departmentName'],
                'departmentCode' => $dept['departmentCode'],
            ];
            array_push($departments, $res);
        }


        $employeeView = new ViewModel(array(
                                'designations' => $designations,
                                'departments' => $departments,
                                'paginated' => true,
                                'employees' => $paginator
                            ));
        $employeeView->setTemplate('jobs/service-employee/create');
        $this->getViewHelper('HeadScript')->prependFile('/js/jobs/serviceEmployee.js');
        return $employeeView;

    }

    public function getPaginatedEmployees($perPage = 6, $param = null)
    {
        $this->employee = $this->CommonTable('Jobs/Model/EmployeeTable')->fetchAll(true);
        if ($param == null) {
            $this->employee->setCurrentPageNumber((int) $this->params()->fromRoute('param1', 1));
        } else {
            $this->employee->setCurrentPageNumber($param);     
        }
        $this->employee->setItemCountPerPage($perPage);
        return $this->employee;
    }

}
