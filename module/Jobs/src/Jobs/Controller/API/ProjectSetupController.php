<?php

namespace Jobs\Controller\API;

use Zend\View\Model\ViewModel;
use Core\Controller\CoreController;
use Zend\Session\Container;
use Jobs\Model\ProjectType;

class ProjectSetupController extends CoreController
{

    //project type save and update function
    public function addAction()
    {
        $request = $this->getRequest();
        if (!$request->isPost()) {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_DATA_SET');
            return $this->JSONRespond();
        }

        $this->beginTransaction();

        $respond = $this->getService('ProjectSetupService')->saveProjectType($request->getPost(), $this->userID);

        if (!$respond['status']) {
            $this->rollback();
            $this->status = false;
            $this->msg = $this->getMessage($respond['msg']);
            return $this->JSONRespond();    
        }

        $this->commit();
        $this->status = true;
        $this->data = $respond['data'];
        $this->msg = $this->getMessage($respond['msg']);
        $this->flashMessenger()->addMessage($this->msg);
        return $this->JSONRespond();
    }

    //get project types by project type search key
    public function getProjectTypesBySearchKeyAction()
    {
        $request = $this->getRequest();
        if (!$request->isPost()) {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_DATA_SET');
            return $this->JSONRespond();
        }

        $respond = $this->getService('ProjectSetupService')->getProjectTypesBySearchKey($request->getPost(), $this->params()->fromRoute('param1', 1));

        $this->status = true;
        $this->html = $respond;
        return $this->JSONRespondHtml();
    }

    /**
     * use to change active state
     */
    public function changeStatusIDAction()
    {
        $request = $this->getRequest();
        if (!$request->isPost()) {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_DATA_SET');
            return $this->JSONRespond();
        }

        $respond = $this->getService('ProjectSetupService')->updateProjectTypeStatus($request->getPost());

        $this->status = $respond['status'];
        $this->msg = $this->getMessage($respond['msg']);
        return $this->JSONRespond();
    }

    //delete project types by job type id
    public function deleteProjectTypeByProjectTypeIDAction()
    {
        $request = $this->getRequest();
        if (!$request->isPost()) {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_DATA_SET');
            return $this->JSONRespond();
        }

        $respond = $this->getService('ProjectSetupService')->deleteProjectTypeByProjectTypeID($request->getPost(), $this->userID);

        if ($respond['status']) {
            $this->msg = $this->getMessage($respond['msg'],$respond['data']);
            $this->flashMessenger()->addMessage($this->msg);
        } else {
            $this->msg = $this->getMessage($respond['msg']);
        }

        $this->status = $respond['status'];
        $this->data = $respond['data'];
        return $this->JSONRespond();
    }

    /**
     *
     * project type list 
     * @return type
     */
    public function searchProjectTypesForDropdownAction()
    {
        $request = $this->getRequest();
        if (!$request->isPost()) {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_DATA_SET');
            return $this->JSONRespond();
        }
        $respond = $this->getService('ProjectSetupService')->searchProjectTypesForDropdown($request->getPost());

        $this->status = true;
        $this->data = $respond;
        return $this->JSONRespond();
    }

}
