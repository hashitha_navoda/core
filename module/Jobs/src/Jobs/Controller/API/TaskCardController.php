<?php

namespace Jobs\Controller\API;

use Core\Controller\CoreController;
use Jobs\Model\TaskCard;

class TaskCardController extends CoreController {

	//task card update function
	public function updateTaskCardAction() {
		$request = $this->getRequest();
		if (!$request->isPost()) {
			$this->status = false;
			$this->msg = $this->getMessage('ERR_DATA_SET');
			return $this->JSONRespond();
		}
		$this->beginTransaction();
		$table = $this->CommonTable('Jobs\Model\TaskCardTable');
		$respond = $this->getService('TaskCardService')->updateTaskCard($request->getPost(), $this->params()->fromRoute('param1', 1), $table);

		if (!$respond['status']) {
			$this->rollback();
			$this->status = false;
			$this->msg = $this->getMessage($respond['msg']);
			return $this->JSONRespond();
		}

		$this->commit();
		$this->status = true;
		$this->data = $respond['data'];
		$this->msg = $this->getMessage($respond['msg']);
		$this->flashMessenger()->addMessage($this->msg);
		return $this->JSONRespond();
	}

	//task card save function
	public function saveTaskCardAction() {
		$request = $this->getRequest();
		if (!$request->isPost()) {
			$this->status = false;
			$this->msg = $this->getMessage('ERR_DATA_SET');
			return $this->JSONRespond();
		}

		$this->beginTransaction();

		$table = $this->CommonTable('Jobs\Model\TaskCardTable');
		$jobCard = new TaskCard();
		$jobCard = $jobCard->getTaskCardCodeValidate($request->getPost()->taskCardCode, $table);

		if (!$jobCard) {
			$this->rollback();
			$this->status = false;
			$this->msg = $this->getMessage('ERR_TSK_CRD_CODE_ALREDY_EXIXT');
			return $this->JSONRespond();
		}

		$respond = $this->getService('TaskCardService')->saveTaskCard($request->getPost());

		if (!$respond['status']) {
			$this->rollback();
			$this->status = false;
			$this->msg = $this->getMessage($respond['msg']);
			return $this->JSONRespond();
		}

		$this->commit();
		$this->status = true;
		$this->data = $respond['data'];
		$this->msg = $this->getMessage($respond['msg']);
		$this->flashMessenger()->addMessage($this->msg);
		return $this->JSONRespond();
	}

	//get task card by task card search key
	public function getTaskCardBySearchKeyAction() {
		$request = $this->getRequest();
		if (!$request->isPost()) {
			$this->status = false;
			$this->msg = $this->getMessage('ERR_DATA_SET');
			return $this->JSONRespond();
		}

		$respond = $this->getService('TaskCardService')->getTaskCardBySearchKey($request->getPost(), $this->params()->fromRoute('param1', 1));

		$this->status = true;
		$this->html = $respond;
		return $this->JSONRespondHtml();
	}

	//delete task card by task card id
	public function deleteTaskCardByTaskCardIDAction() {
		$request = $this->getRequest();
		if (!$request->isPost()) {
			$this->status = false;
			$this->msg = $this->getMessage('ERR_DATA_SET');
			return $this->JSONRespond();
		}

		$respond = $this->getService('TaskCardService')->deleteTaskCardByTaskCardID($request->getPost());

		if ($respond['status']) {
			$this->msg = $this->getMessage($respond['msg'], $respond['data']);
			$this->flashMessenger()->addMessage($this->msg);
		} else {
			$this->msg = $this->getMessage($respond['msg']);
		}

		$this->status = $respond['status'];
		$this->data = $respond['data'];
		return $this->JSONRespond();
	}

	//get task card task product function
	public function getTaskCardTaskProductAction() {
		$request = $this->getRequest();
		if (!$request->isPost()) {
			$this->status = false;
			$this->msg = $this->getMessage('ERR_DATA_SET');
			return $this->JSONRespond();
		}

		$respond = $this->getService('TaskCardService')->getTaskCardTaskProduct($request->getPost());

		$this->status = $respond['status'];
		$this->data = $respond['data'];
		$this->msg = $this->getMessage($respond['msg']);
		return $this->JSONRespond();
	}

	public function getTaskCardDetailsAction() {
		$request = $this->getRequest();
		if (!$request->isPost()) {
			$this->status = false;
			$this->msg = $this->getMessage('ERR_DATA_SET');
			return $this->JSONRespond();
		}

		$respond = $this->getService('TaskCardService')->getTaskCardDetailsByTaskCardIDs($request->getPost());

		$this->status = true;
		$this->data = $respond;
		return $this->JSONRespondHtml();
	}

	public function searchTaskCardsForDropdownAction() {
		$request = $this->getRequest();
		if (!$request->isPost()) {
			$this->status = false;
			$this->msg = $this->getMessage('ERR_DATA_SET');
			return $this->JSONRespond();
		}

		$respond = $this->getService('TaskCardService')->searchTaskCardForDropdown($request->getPost());

		$this->data = array('list' => $respond);
		return $this->JSONRespond();
	}
}
