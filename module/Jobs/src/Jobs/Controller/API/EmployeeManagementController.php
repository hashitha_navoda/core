<?php

namespace Jobs\Controller\API;

use Core\Controller\CoreController;
use Jobs\Model\TaskCard;

class EmployeeManagementController extends CoreController {

	// this function use to get Department related employee performance list
	public function getDepartmentRelatedEmployeeListsDetailsAction() {
		$request = $this->getRequest();
		if (!$request->isPost()) {
			$this->status = false;
			$this->msg = $this->getMessage('ERR_DATA_SET');
			return $this->JSONRespond();
		}

		$this->beginTransaction();
		$respond = $this->getService('EmployeeManagementService')->getDepartmentRelatedEmployeeListsDetails($request->getPost());

		if (!$respond['status']) {
			$this->status = false;
            $this->msg = $this->getMessage($respond['msg']);
            $this->rollback();
            return $this->JSONRespond();
		}

		$this->status = true;
		$this->html = $respond['view'];
		$this->data = $respond['data'];
        return $this->JSONRespondHtml();
	}

	// this function use to get employee related job Task Details
	public function getEmployeeJobTaskListsDetailsAction() {
		$request = $this->getRequest();
		if (!$request->isPost()) {
			$this->status = false;
			$this->msg = $this->getMessage('ERR_DATA_SET');
			return $this->JSONRespond();
		}

		$this->beginTransaction();
		$respond = $this->getService('EmployeeManagementService')->getEmployeeJobTaskListsDetails($request->getPost());

		if (!$respond['status']) {
			$this->status = false;
            $this->msg = $this->getMessage($respond['msg']);
            $this->rollback();
            return $this->JSONRespond();
		}

		$this->status = true;
		$this->html = $respond['view'];
		$this->data = $respond['data'];
        return $this->JSONRespondHtml();
	}

	/**
    * this function use to update incentive value
    * return json respond
    **/
    public function updateIncentiveValueAction() 
    {
        $request = $this->getRequest();
        if (!$request->isPost()) {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_DATA_SET');
            $this->rollback();
            return $this->JSONRespond();
        }
        $this->beginTransaction();
        $respond = $this->getService('EmployeeManagementService')->updateJobTaskEmployeeIncentiveValue($request->getPost());
        if ($respond['status']) {
            $this->commit();
            $this->status = true;
            $this->msg = $this->getMessage($respond['msg']);
        } else {
            $this->status = false;
            $this->msg = $this->getMessage($respond['msg']);
            $this->rollback();
        }
        return $this->JSONRespond();
        
    }
}
