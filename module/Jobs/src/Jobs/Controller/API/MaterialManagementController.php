<?php

namespace Jobs\Controller\API;

use Core\Controller\CoreController;

class MaterialManagementController extends CoreController {

    /**
	 * This function is used to aearch job
	 * @param JSON Request
	 * @return JSON Respond
	 */
	public function getJobServiceBySearchKeyAction() 
	{
		$request = $this->getRequest();
		if (!$request->isPost()) {
			$this->status = false;
			$this->msg = $this->getMessage('ERR_DATA_SET');
			return $this->JSONRespond();
		}

		$respond = $this->getService('ResourceManagementService')->getJobServiceBySearchKey($request->getPost(), $this->params()->fromRoute('param1', 1));

		$this->status = true;
		$this->html = $respond;
		return $this->JSONRespondHtml();
	}

	/**
	 * This function is used to get job service data for update
	 * @param JSON Request
	 * @return JSON Respond
	 */
	public function getJobServiceByJobServiceIdAction() 
	{
		$request = $this->getRequest();
		if (!$request->isPost()) {
			$this->status = false;
			$this->msg = $this->getMessage('ERR_DATA_SET');
			return $this->JSONRespond();
		}

		$respond = $this->getService('ResourceManagementService')->getJobServiceByJobServiceId($request->getPost());

		$this->status = true;
		$this->data = $respond;
		return $this->JSONRespondHtml();
	}

	/**
	 * This function is used to save job service material issue
	 * @param JSON Request
	 * @return JSON Respond
	 */
	public function updateServiceMaterialsAction() 
	{
		$request = $this->getRequest();
		if (!$request->isPost()) {
			$this->status = false;
			$this->msg = $this->getMessage('ERR_DATA_SET');
			return $this->JSONRespond();
		}

		$this->beginTransaction();
		$respond = $this->getService('ResourceManagementService')->updateServiceMaterials($request->getPost());

		if (!$respond['status']) {
			$this->rollback();
			$this->status = false;
			$this->msg = $this->getMessage($respond['msg']);
			return $this->JSONRespond();
		}

		$this->commit();
		$this->status = true;
		$this->data = $respond['data'];
		$this->msg = $this->getMessage($respond['msg'], array($respond['data']));
		return $this->JSONRespond();
	}

	/**
	 * This function is used to get job task employee data
	 * @param JSON Request
	 * @return JSON Respond
	 */
	public function getJobEmployeeByJobTaskIdAction() 
	{
		$request = $this->getRequest();
		if (!$request->isPost()) {
			$this->status = false;
			$this->msg = $this->getMessage('ERR_DATA_SET');
			return $this->JSONRespond();
		}

		$respond = $this->getService('ResourceManagementService')->getJobEmployeeByJobTaskId($request->getPost());

		$this->status = true;
		$this->data = $respond;
		return $this->JSONRespondHtml();
	}
}
