<?php

namespace Jobs\Controller\API;

use Core\Controller\CoreController;

class IncentivesController extends CoreController {

	/**
	 * This function is used to get service list by job card id
	 * @param JSON Request
	 * @return JSON Respond
	 */
	public function getServiceListAction()
	{
		$request = $this->getRequest();
		if (!$request->isPost()) {
			$this->status = false;
			$this->msg = $this->getMessage('ERR_DATA_SET');
			return $this->JSONRespond();
		}

		$respond = $this->getService('IncentivesService')->getServiceForVehicleType($request->getPost());

		$this->status = true;
        $this->data = $respond['data'];
        $this->html = $respond['view'];
        return $this->JSONRespondHtml();	
	}

	/**
	 * This function is used to get job Card List for adding rates
	 * @param JSON Request
	 * @return JSON Respond
	 */
	public function getJobCardListAction()
	{
		$request = $this->getRequest();
		if (!$request->isPost()) {
			$this->status = false;
			$this->msg = $this->getMessage('ERR_DATA_SET');
			return $this->JSONRespond();
		}

		$respond = $this->getService('IncentivesService')->getJobCardForVehicleType($request->getPost());

		$this->status = true;
        $this->data = $respond['data'];
        $this->html = $respond['view'];
        return $this->JSONRespondHtml();	
	}

	/**
	 * This function is used to save Services
	 * @param JSON Request
	 * @return JSON Respond
	 */
	public function saveServiceIncentivesAction()
	{
		$request = $this->getRequest();
		if (!$request->isPost()) {
			$this->status = false;
			$this->msg = $this->getMessage('ERR_DATA_SET');
			return $this->JSONRespond();
		}

		$this->beginTransaction();

		$respond = $this->getService('IncentivesService')->saveServiceIncentives($request->getPost());

		if (!$respond['status']) {
			$this->rollback();
			$this->status = false;
			$this->msg = $this->getMessage($respond['msg']);
			return $this->JSONRespond();
		}

		$this->commit();
		$this->status = true;
		$this->data = $respond['data'];
		$this->msg = $this->getMessage($respond['msg'], array($respond['data']));
		return $this->JSONRespond();
	}

	/**
	 * This function is used to save job card incentives
	 * @param JSON Request
	 * @return JSON Respond
	 */
	public function saveJobCardServiceIncentivesAction()
	{
		$request = $this->getRequest();
		if (!$request->isPost()) {
			$this->status = false;
			$this->msg = $this->getMessage('ERR_DATA_SET');
			return $this->JSONRespond();
		}

		$this->beginTransaction();

		$respond = $this->getService('IncentivesService')->saveJobCardServiceIncentives($request->getPost());

		if (!$respond['status']) {
			$this->rollback();
			$this->status = false;
			$this->msg = $this->getMessage($respond['msg']);
			return $this->JSONRespond();
		}

		$this->commit();
		$this->status = true;
		$this->data = $respond['data'];
		$this->msg = $this->getMessage($respond['msg'], array($respond['data']));
		return $this->JSONRespond();
	}

}
