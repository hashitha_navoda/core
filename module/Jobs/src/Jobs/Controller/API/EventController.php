<?php

namespace Jobs\Controller\API;

use Core\Controller\CoreController;

class EventController extends CoreController {

	/**
	 * This function is used to save job
	 * @param JSON Request
	 * @return JSON Respond
	 */
	public function saveJobAction() 
	{
		$request = $this->getRequest();
		if (!$request->isPost()) {
			$this->status = false;
			$this->msg = $this->getMessage('ERR_DATA_SET');
			return $this->JSONRespond();
		}

		$this->beginTransaction();

		$userLocation = $this->user_session->userActiveLocation['locationID'];
		$respond = $this->getService('EventService')->saveJob($request->getPost(), $userLocation, $this->userID);

		if (!$respond['status']) {
			$this->rollback();
			$this->status = false;
			$this->msg = $this->getMessage($respond['msg']);
			return $this->JSONRespond();
		}

		$this->commit();
		$this->status = true;
		$this->data = $respond['data'];
		$this->msg = $this->getMessage($respond['msg'], array($respond['data']));
		$this->flashMessenger()->addMessage($this->msg);
		return $this->JSONRespond();
	}

	/**
	 * This function is used to search jobs for dropdown
	 */
	public function searchJobsForDropdownAction() {

		$request = $this->getRequest();
		if (!$request->isPost()) {
			$this->status = false;
			$this->msg = $this->getMessage('ERR_DATA_SET');
			return $this->JSONRespond();
		}

		$userLocation = $this->user_session->userActiveLocation['locationID'];
		$respond = $this->getService('EventService')->searchJobsForDropdown($request->getPost(), $userLocation);

		$this->data = array('list' => $respond);
		return $this->JSONRespond();
	}

	/**
	 * This function is used to load jobs depend on project id
	 * @param JSON Request
	 * @return JSON Respond
	 */
	public function loadJobsForDropdownAction() {
        $request = $this->getRequest();
        if (!$request->isPost()) {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_DATA_SET');
            return $this->JSONRespond();
        }
        $respond = $this->getService('EventService')->loadJobsForDropdown($request->getPost());
        $this->status = $respond['status'];
        $this->msg = $this->getMessage($respond['msg']);
        $this->data = $respond['data'];
        return $this->JSONRespond();
    }

    /**
	 * This function is used to aearch job
	 * @param JSON Request
	 * @return JSON Respond
	 */
	public function getJobBySearchKeyAction() 
	{
		$request = $this->getRequest();
		if (!$request->isPost()) {
			$this->status = false;
			$this->msg = $this->getMessage('ERR_DATA_SET');
			return $this->JSONRespond();
		}

		$locationID = $this->user_session->userActiveLocation['locationID'];
		$respond = $this->getService('EventService')->getJobBySearchKey($request->getPost(), $this->params()->fromRoute('param1', 1), $locationID);

		$this->status = true;
		$this->html = $respond;
		return $this->JSONRespondHtml();
	}


	/**
	 * This function is used to search jobs for dropdown
	 */
	public function searchByProjectAndJobAction() {

		$request = $this->getRequest();
		if (!$request->isPost()) {
			$this->status = false;
			$this->msg = $this->getMessage('ERR_DATA_SET');
			return $this->JSONRespond();
		}

		$userLocation = $this->user_session->userActiveLocation['locationID'];
		$respond = $this->getService('EventService')->searchByProjectAndJob($request->getPost(), $userLocation);

		$this->data = array('list' => $respond);

		return $this->JSONRespond();
	}
		
	/**
	 * This function is used to get job supervisor details
	 * @param JSON Request
	 * @return JSON Respond
	 */
	public function getJobSupervisorsByJobIDAction() 
	{
		$request = $this->getRequest();
		if (!$request->isPost()) {
			$this->status = false;
			$this->msg = $this->getMessage('ERR_DATA_SET');
			return $this->JSONRespond();
		}

		$respond = $this->getService('EventService')->getJobSupervisorsByJobID($request->getPost());

		$this->status = true;
		$this->data = $respond;
		return $this->JSONRespondHtml();
	}

	function deleteAction()
    {
        $request = $this->getRequest();
		if (!$request->isPost()) {
			$this->status = false;
			$this->msg = $this->getMessage('ERR_DATA_SET');
			return $this->JSONRespond();
		}

		$respond = $this->getService('EventService')->deleteJob($request->getPost(), $this->userID);

		if (!$respond['status']) {
			$this->rollback();
			$this->status = false;
			$this->msg = $this->getMessage($respond['msg']);
			return $this->JSONRespond();
		}

		$this->commit();
		$this->status = true;
		$this->data = $respond['data'];
		$this->msg = $this->getMessage($respond['msg'], array($respond['data']));
		$this->flashMessenger()->addMessage($this->msg);
		return $this->JSONRespond();
    }


    /**
	 * This function is used to get job data for update
	 * @param JSON Request
	 * @return JSON Respond
	 */
	public function getDataForUpdateAction() 
	{
		$request = $this->getRequest();
		if (!$request->isPost()) {
			$this->status = false;
			$this->msg = $this->getMessage('ERR_DATA_SET');
			return $this->JSONRespond();
		}

		$respond = $this->getService('EventService')->getJobDataForUpdate($request->getPost());

		$this->status = true;
		$this->data = $respond;
		return $this->JSONRespondHtml();
	}

	/**
	 * This function is used to save job
	 * @param JSON Request
	 * @return JSON Respond
	 */
	public function updateJobAction() 
	{
		$request = $this->getRequest();
		if (!$request->isPost()) {
			$this->status = false;
			$this->msg = $this->getMessage('ERR_DATA_SET');
			return $this->JSONRespond();
		}

		$this->beginTransaction();

		$userLocation = $this->user_session->userActiveLocation['locationID'];
		$respond = $this->getService('EventService')->updateJob($request->getPost(), $userLocation, $this->userID);

		if (!$respond['status']) {
			$this->rollback();
			$this->status = false;
			$this->msg = $this->getMessage($respond['msg']);
			return $this->JSONRespond();
		}

		$this->commit();
		$this->status = true;
		$this->data = $respond['data'];
		$this->msg = $this->getMessage($respond['msg'], array($respond['data']));
		$this->flashMessenger()->addMessage($this->msg);

		return $this->JSONRespond();
	}


	/**
	 * This function is used to get job contractor details
	 * @param JSON Request
	 * @return JSON Respond
	 */
	public function getJobContractorsByJobIDAction() 
	{
		$request = $this->getRequest();
		if (!$request->isPost()) {
			$this->status = false;
			$this->msg = $this->getMessage('ERR_DATA_SET');
			return $this->JSONRespond();
		}

		$respond = $this->getService('EventService')->getJobContractorsByJobID($request->getPost());

		$this->status = true;
		$this->data = $respond;
		return $this->JSONRespond();
	}


    /**
    * this function use to get job-project list 
    * @param JSON Request
    * return JSON Respond
    **/
    public function searchJobProjectForDropdownAction()
    {
        $request = $this->getRequest();
        if (!$request->isPost()) {
        	$this->status = false;
        	$this->msg = $this->getMessage('ERR_REQUEST');
        	return $this->JSONRespond();
        }

        $respond = $this->getService('EventService')->searchJobProjectForDropdown($request->getPost());
        
        $this->data = array('list' => $respond);
        return $this->JSONRespond();
    }

    /**
    * this action use to get job task and job task product details
    * @param JSON Request
    * @param JSON Respond
    **/
    public function getJobTaskWithProductDetailsAction()
    {
    	$request = $this->getRequest();
    	if (!$request->isPost()) {
    		$this->status = false;
        	$this->msg = $this->getMessage('ERR_REQUEST');
        	return $this->JSONRespond();	
    	}
        $respond = $this->getService('EventService')->getJobTaskWithProductDetails($request->getPost());

        $this->data = $respond;
        $this->status = true;
        return $this->JSONRespond();
    	
    }

    public function getParticipantsAction()
    {
        $request = $this->getRequest()->getQuery('jobID');

        $respond = $this->getService('EventService')->getParticipants($request);
        $this->status = true;
        $this->html = $respond['view'];
        return $this->JSONRespondHtml();
    }
}
