<?php

namespace Jobs\Controller\API;

use Core\Controller\CoreController;

class ProgressController extends CoreController {

	/**
	 * This function is used to search job by using job Id and project Id
	 * @param JSON Request
	 * @return JSON Respond
	 */
	public function searchByProjectAndJobAction() 
	{
		$request = $this->getRequest();
		if (!$request->isPost()) {
			$this->status = false;
			$this->msg = $this->getMessage('ERR_DATA_SET');
			return $this->JSONRespond();
		}
		$userLocation = $this->user_session->userActiveLocation['locationID'];
		$result = $this->getService('ProgressService')->searchByProjectAndJob($request->getPost(), $userLocation);
		
		$this->status = $result['status'];
		$this->html = $result['view'];
		return $this->JSONRespondHtml();
	}

	/**
	 * This function is used to start the task
	 * @param JSON Request
	 * @return JSON Respond
	 */
	public function startTaskAction () 
    {
        $request = $this->getRequest();
		if (!$request->isPost()) {
			$this->status = false;
			$this->msg = $this->getMessage('ERR_DATA_SET');
			return $this->JSONRespond();
		}

		$this->beginTransaction();
		$result = $this->getService('ProgressService')->startTask($request->getPost());

		if ($result['status']) {
			$this->commit();
		}
		else {
			$this->rollback();
		}

		$this->status = $result['status'];
		$this->msg = $this->getMessage($result['msg']);
		return $this->JSONRespond();
    }

    /**
	 * This function is used to get task deatails
	 * @param JSON Request
	 * @return JSON Respond
	 */
    public function getTaskDetailsByIdAction () 
    {
    	$request = $this->getRequest();
		if (!$request->isPost()) {
			$this->status = false;
			$this->msg = $this->getMessage('ERR_DATA_SET');
			return $this->JSONRespond();
		}

		$result = $this->getService('ProgressService')->getTaskDetailsById($request->getPost());


		$this->status = $result['status'];
		$this->html = $result['view'];
		$this->data = $result['data'];
		return $this->JSONRespondHtml();

    }

    /**
	 * This function is used to update task progress
	 * @param JSON Request
	 * @return JSON Respond
	 */
    public function updateJobTaskProgressAction () 
    {

        $request = $this->getRequest();
		if (!$request->isPost()) {
			$this->status = false;
			$this->msg = $this->getMessage('ERR_DATA_SET');
			return $this->JSONRespond();
		}

		$this->beginTransaction();
		$result = $this->getService('ProgressService')->updatejobTaskProgress($request->getPost());

		if ($result['status']) {
			$this->commit();
		}
		else {
			$this->rollback();
		}

		$this->status = $result['status'];
		$this->data = $result['data'];
		$this->msg = $this->getMessage($result['msg'], (array)$result['data']);
		$this->flashMessenger()->addMessage($this->msg);
		return $this->JSONRespond();
    }

    /**
	 * This function is used to hold the task 
	 * @param JSON Request
	 * @return JSON Respond
	 */
    public function holdTaskAction () 
    {
        $request = $this->getRequest();
		if (!$request->isPost()) {
			$this->status = false;
			$this->msg = $this->getMessage('ERR_DATA_SET');
			return $this->JSONRespond();
		}
		$this->beginTransaction();
		$result = $this->getService('ProgressService')->holdOrEndTask($request->getPost());

		if ($result['status']) {
			$this->commit();
		}
		else {
			$this->rollback();
		}

		$this->status = $result['status'];
		$this->msg = $result['msg'];
		return $this->JSONRespond();
    }

    /**
	 * This function is used to end the task 
	 * @param JSON Request
	 * @return JSON Respond
	 */
    public function endTaskAction () 
    {
        $request = $this->getRequest();
		if (!$request->isPost()) {
			$this->status = false;
			$this->msg = $this->getMessage('ERR_DATA_SET');
			return $this->JSONRespond();
		}
		$this->beginTransaction();
		$result = $this->getService('ProgressService')->holdOrEndTask($request->getPost());

		if ($result['status']) {
			$this->commit();
		}
		else {
			$this->rollback();
		}

		$this->status = $result['status'];
		$this->data = $result['data'];
		$this->msg = $this->getMessage($result['msg'], (array)$result['data']);
		$this->flashMessenger()->addMessage($this->msg);
		return $this->JSONRespond();
    }

    /**
	 * This function is used to get job related products 
	 * @param JSON Request
	 * @return JSON Respond
	 */
    public function getJobRelatedProductsAction()
    {
    	$request = $this->getRequest();
		if (!$request->isPost()) {
			$this->status = false;
			$this->msg = $this->getMessage('ERR_DATA_SET');
			return $this->JSONRespond();
		}
		$this->beginTransaction();
		$result = $this->getService('ProgressService')->getJobRelatedProducts($request->getPost());

		if ($result['status']) {
			$this->commit();
		}
		else {
			$this->rollback();
		}

		$this->status = $result['status'];
		$this->data = $result['data'];
		$this->msg = $result['msg'];
		return $this->JSONRespond();
    }

    /**
	 * This function is used to send material request
	 * @param JSON Request
	 * @return JSON Respond
	 */
    public function sendMaterialRequestAction()
    {
    	$request = $this->getRequest();
		if (!$request->isPost()) {
			$this->status = false;
			$this->msg = $this->getMessage('ERR_DATA_SET');
			return $this->JSONRespond();
		}
		$this->beginTransaction();
		$result = $this->getService('ProgressService')->sendMaterialRequest($request->getPost(), $this->userID);

		if ($result['status']) {
			$this->commit();
		}
		else {
			$this->rollback();
		}

		$this->status = $result['status'];
		$this->data = $result['data'];
		$this->msg = $this->msg = $this->getMessage($result['msg']);
		return $this->JSONRespond();
    }

}