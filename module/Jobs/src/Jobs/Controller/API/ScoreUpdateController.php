<?php

namespace Jobs\Controller\API;

use Core\Controller\CoreController;

class ScoreUpdateController extends CoreController {

	/**
	 * This function is used to search job by using job Id and project Id
	 * @param JSON Request
	 * @return JSON Respond
	 */
	public function searchByProjectAndJobAction() 
	{
		$request = $this->getRequest();
		if (!$request->isPost()) {
			$this->status = false;
			$this->msg = $this->getMessage('ERR_DATA_SET');
			return $this->JSONRespond();
		}
		$userLocation = $this->user_session->userActiveLocation['locationID'];
		$result = $this->getService('ScoreUpdateService')->searchByProjectAndJob($request->getPost(), $userLocation);
		
		$this->status = $result['status'];
		$this->html = $result['view'];
		return $this->JSONRespondHtml();
	}

    public function updateScoreAction()
    {
    	$request = $this->getRequest();
		if (!$request->isPost()) {
			$this->status = false;
			$this->msg = $this->getMessage('ERR_DATA_SET');
			return $this->JSONRespond();
		}

		$this->beginTransaction();
		$respond = $this->getService('ScoreUpdateService')->updateScore($request->getPost());
		if (!$respond['status']) {
			$this->rollback();
			$this->status = false;
			$this->msg = $this->getMessage($respond['msg']);
			return $this->JSONRespond();
		}

		$this->commit();
		$this->status = true;
		$this->data = $respond['data'];
		$this->msg = $this->getMessage($respond['msg'], array($respond['data']));
		$this->flashMessenger()->addMessage($this->msg);
		return $this->JSONRespond();
    }

    public function getScoreAction()
    {
    	$request = $this->getRequest();
		if (!$request->isPost()) {
			$this->status = false;
			$this->msg = $this->getMessage('ERR_DATA_SET');
			return $this->JSONRespond();
		}

		$respond = $this->getService('ScoreUpdateService')->getScore($request->getPost());
		$this->status = true;
		$this->data = $respond;
		return $this->JSONRespond();
    }
}