<?php

namespace Jobs\Controller\API;

use Zend\View\Model\ViewModel;
use Core\Controller\CoreController;
use Zend\Session\Container;
use Jobs\Model\JobType;

class ServiceSetupController extends CoreController
{

    //service save and update function
    public function addAction()
    {
        $request = $this->getRequest();
        if (!$request->isPost()) {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_DATA_SET');
            return $this->JSONRespond();
        }
        
        $this->beginTransaction();

        $type = 'service';

        $respond = $this->getService('TaskSetupService')->saveTask($request->getPost(), $type);

        if (!$respond['status']) {
            $this->rollback();
            $this->status = false;
            $this->msg = $this->getMessage($respond['msg']);
            return $this->JSONRespond();    
        }

        $this->commit();
        $this->status = true;
        $this->data = $respond['data'];
        $this->msg = $this->getMessage($respond['msg']);
        $this->flashMessenger()->addMessage($this->msg);
        return $this->JSONRespond();
    }

    //get service bys search key
    public function getServicesBySearchKeyAction()
    {
       $request = $this->getRequest();
        if (!$request->isPost()) {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_DATA_SET');
            return $this->JSONRespond();
        }

        $type = 'service';
        $respond = $this->getService('TaskSetupService')->getTaskBySearchKey($request->getPost(), $this->params()->fromRoute('param1', 1), $type);

        $this->status = true;
        $this->html = $respond;
        return $this->JSONRespondHtml();
    }

    //delete service by service id
    public function deleteServiceByServiceIDAction()
    {
        $request = $this->getRequest();
        if (!$request->isPost()) {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_DATA_SET');
            return $this->JSONRespond();
        }

        $type = 'service';
        $respond = $this->getService('TaskSetupService')->deleteTaskByTaskID($request->getPost(), $type);

        if ($respond['status']) {
            $this->msg = $this->getMessage($respond['msg'],$respond['data']);
            $this->flashMessenger()->addMessage($this->msg);
        } else {
            $this->msg = $this->getMessage($respond['msg']);
        }

        $this->status = $respond['status'];
        $this->data = $respond['data'];
        return $this->JSONRespond();
    }

    /**
     * This function is used to search tasks for dropdown
     */
    public function searchTasksForDropdownAction()
    {
        $request = $this->getRequest();
        if (!$request->isPost()) {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_DATA_SET');
            return $this->JSONRespond();
        }

        $respond = $this->getService('TaskSetupService')->searchTasksForDropdown($request->getPost());

        $this->data = array('list' => $respond);
        return $this->JSONRespond();
    }


    /**
     * This function is used to get service related sub tasks 
     */
    public function getRelatedSubTasksAction()
    {
        $request = $this->getRequest();
        if (!$request->isPost()) {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_DATA_SET');
            return $this->JSONRespond();
        }
        $this->beginTransaction();
        $respond = $this->getService('TaskSetupService')->getRelatedSubTasks($request->getPost(), "service");

        if ($respond['status']) {
            $this->msg = $this->getMessage($respond['msg']);
            $this->commit();
        } else {
            $this->msg = $this->getMessage($respond['msg']);
            $this->rollback();
        }

        $this->status = $respond['status'];
        $this->data = $respond['data'];
        return $this->JSONRespond();
    }


    /**
     * This function is used to get service related sub tasks details
     */
    public function getSubTaskByTaskIdAction()
    {
        $request = $this->getRequest();
        if (!$request->isPost()) {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_DATA_SET');
            return $this->JSONRespond();
        }
        $this->beginTransaction();
        $respond = $this->getService('TaskSetupService')->getSubTaskByTaskId($request->getPost());

        if ($respond['status']) {
            $this->msg = $this->getMessage($respond['msg']);
            $this->commit();
        } else {
            $this->msg = $this->getMessage($respond['msg']);
            $this->rollback();
        }

        $this->status = $respond['status'];
        $this->data = $respond['data'];
        return $this->JSONRespond();
    }

    /**
    * this function use to change the sub task status.
    * return json respond
    **/
    public function changeSubTaskStatusAction()
    {

        $request = $this->getRequest();
        
        if (!$request->isPost()) {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_DATA_SET');
            $this->rollback();
            return $this->JSONRespond();
        }
        $this->beginTransaction();
        $respond = $this->getService('TaskSetupService')->changeSubTaskStatus($request->getPost());

        if (!$respond['status']) {
            $this->status = false;
            $this->data = $respond['data'];
            $this->msg = $this->getMessage($respond['msg']);
            $this->rollback();
            return $this->JSONRespond();    
        } 
        
        $this->commit();
        $this->status = true;
        $this->msg = $this->getMessage($respond['msg']);
        return $this->JSONRespond();
        
    }


    /**
    * this function use to change the service status.
    * return json respond
    **/
    public function changeStatusIDAction()
    {

        $request = $this->getRequest();
        
        if (!$request->isPost()) {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_DATA_SET');
            $this->rollback();
            return $this->JSONRespond();
        }
        $this->beginTransaction();
        $type = 'service';
        $respond = $this->getService('TaskSetupService')->changeStatusID($request->getPost(), $type);

        if (!$respond['status']) {
            $this->status = false;
            $this->data = $respond['data'];
            $this->msg = $this->getMessage($respond['msg']);
            $this->rollback();
            return $this->JSONRespond();    
        } 
        
        $this->commit();
        $this->status = true;
        $this->msg = $this->getMessage($respond['msg']);
        return $this->JSONRespond();
        
    }

    /**
    * this function use to delete sub task.
    * return json respond
    **/
    public function deleteSubTaskAction()
    {

        $request = $this->getRequest();
        
        if (!$request->isPost()) {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_DATA_SET');
            $this->rollback();
            return $this->JSONRespond();
        }
        $this->beginTransaction();
        $respond = $this->getService('TaskSetupService')->deleteSubTask($request->getPost());

        if (!$respond['status']) {
            $this->status = false;
            $this->data = $respond['data'];
            $this->msg = $this->getMessage($respond['msg']);
            $this->rollback();
            return $this->JSONRespond();    
        } 
        
        $this->commit();
        $this->status = true;
        $this->msg = $this->getMessage($respond['msg']);
        return $this->JSONRespond();
        
    }

     /**
     * This function is used to get service related sub tasks details
     */
    public function getSubTaskAction()
    {
        $request = $this->getRequest();
        if (!$request->isPost()) {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_DATA_SET');
            return $this->JSONRespond();
        }
        $this->beginTransaction();
        $respond = $this->getService('TaskSetupService')->getSubTaskByTaskId($request->getPost());

        if ($respond['status']) {
            $this->msg = $this->getMessage($respond['msg']);
            $this->commit();
        } else {
            $this->msg = $this->getMessage($respond['msg']);
            $this->rollback();
        }

        $this->status = $respond['status'];
        $this->data = $respond['data'];
        return $this->JSONRespond();
    }

}
