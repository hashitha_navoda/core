<?php

namespace Jobs\Controller\API;

use Zend\View\Model\ViewModel;
use Core\Controller\CoreController;
use Zend\Session\Container;
use Zend\View\Model\JsonModel;

class GeneralSetupController extends CoreController
{
    /**
    * this action use to retrive all general setting.
    * return json respond
    **/
    public function getGeneralSettingAction()
    {
        $respond = $this->getService('GeneralSetupService')->getAll();
        $this->status = $respond['status'];
        $this->data = $respond['data'];
        $this->msg = $this->getMessage($respond['msg']);
        return $this->JSONRespond();
    }

    /**
    * this action use to save general setting.
    * return json respond
    **/
    public function saveGeneralSettingAction()
    {
        $request = $this->getRequest();
        if (!$request->isPost()) {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_DATA_SET');
            return $this->JSONRespond();
        }

        $this->beginTransaction();

        $respond = $this->getService('GeneralSetupService')->saveSetting($request->getPost(), $this->userID);

        if (!$respond['status']) {
            $this->rollback();
            $this->status = false;
            $this->msg = $this->getMessage($respond['msg']);
            return $this->JSONRespond();    
        }

        $this->user_session->jobSettings = (object) $this->CommonTable('Jobs\Model\JobGeneralSettingsTable')->fetchAll();
        $this->user_session->jobAdmin = $respond['data']['jobAdminFlag'];
        $this->user_session->jobStoreKeeper = $respond['data']['jobStoreKeeperFlag'];
        
        $this->commit();
        $this->status = true;
        $this->data = $respond['data'];
        $this->msg = $this->getMessage($respond['msg']);
        $this->flashMessenger()->addMessage($this->msg);
        return $this->JSONRespond();
    }

}
