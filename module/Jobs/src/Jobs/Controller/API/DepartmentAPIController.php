<?php

namespace Jobs\Controller\API;

use Core\Controller\CoreController;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;


class DepartmentAPIController extends CoreController
{

    /**
    * this function use to create department.
    * return json respond
    **/
    public function createDepartmentAction()
    {

        $request = $this->getRequest();
        if (!$request->isPost()) {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_DATA_SET');
            $this->rollback();
            return $this->JSONRespond();
        }
        $this->beginTransaction();
        $respond = $this->getService('DepartmentService')->createDepartment($request->getPost(), $this->userID);
        if (!$respond['status']) {
            $this->status = false;
            $this->data = $respond['data'];
            $this->msg = $this->getMessage($respond['msg']);
            $this->rollback();
            return $this->JSONRespond();    
        } 
        
        $this->commit();
        $this->status = true;
        $this->msg = $this->getMessage($respond['msg']);
        return $this->JSONRespond();
    	
    }

    /**
    * this function use to change the department status.
    * return json respond
    **/
    public function changeStatusIDAction() 
    {
        $request = $this->getRequest();
        if (!$request->isPost()) {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_DATA_SET');
            $this->rollback();
            return $this->JSONRespond();
        }
        $this->beginTransaction();
        $updateState = $this->getService('DepartmentService')->updateDepartmentState($request->getPost());
        if ($updateState['status']) {
            $this->commit();
            $this->status = true;
            $this->msg = $this->getMessage($updateState['msg']);
        } else {
            $this->status = false;
            $this->msg = $this->getMessage($updateState['msg']);
            $this->rollback();
        }
        return $this->JSONRespond();
        
    }

    /**
    * this function use to update department.
    * return json respond
    **/
    public function updateAction() 
    {
        $request = $this->getRequest();
        if (!$request->isPost()) {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_DATA_SET');
            return $this->JSONRespond();
        }

        $this->beginTransaction();
        $respond = $this->getService('DepartmentService')->updateDepartment($request->getPost(), $this->userID);

        if (!$respond['status']) {
            $this->status = false;
            $this->msg = $this->getMessage($respond['msg']);
            $this->rollback();     
        } else {
            $this->commit();
            $this->status = true;
            $this->msg = $this->getMessage($respond['msg']);
            $this->flashMessenger()->addMessage($this->msg);
        }

        return $this->JSONRespond();

    } 

    /**
    * this function use to search department list by key.
    * return json respond
    **/
    public function searchAction() 
    {
        $request = $this->getRequest();
        if (!$request->isPost()) {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_DATA_SET');
            return $this->JSONRespond();
        }
        $result = $this->getService('DepartmentService')->departmentSearchByKey($request->getPost('searchKey'), (int) $this->params()->fromRoute('param1', 1));

        $this->status = $result['status'];
        $this->html = $result['view'];
        return $this->JSONRespondHtml();

    }

    /**
     * This function is used to search department for dropdown
     */
    public function searchDepartmentsForDropdownAction()
    {
        $request = $this->getRequest();
        if (!$request->isPost()) {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_DATA_SET');
            return $this->JSONRespond();
        }

        $respond = $this->getService('DepartmentService')->searchDepartmentsForDropdown($request->getPost());

        $this->data = array('list' => $respond);
        return $this->JSONRespond();
    }

    //delete department by department id
    public function deleteDepartmentAction()
    {
        $request = $this->getRequest();
        if (!$request->isPost()) {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_DATA_SET');
            return $this->JSONRespond();
        }
        $this->beginTransaction();
        $respond = $this->getService('DepartmentService')->deleteDepartment($request->getPost());

        if ($respond['status']) {
            $this->msg = $this->getMessage($respond['msg']);
            $this->flashMessenger()->addMessage($this->msg);
            $this->commit();
        } else {
            $this->msg = $this->getMessage($respond['msg']);
            $this->rollback();
        }

        $this->status = $respond['status'];
        $this->data = $respond['data'];
        return $this->JSONRespond();
    }

    //get department related stations
    public function getRelatedDepartmentStationsAction()
    {
        $request = $this->getRequest();
        if (!$request->isPost()) {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_DATA_SET');
            return $this->JSONRespond();
        }
        $this->beginTransaction();
        $respond = $this->getService('DepartmentService')->getRelatedDepartmentStations($request->getPost());

        if ($respond['status']) {
            $this->msg = $this->getMessage($respond['msg']);
            $this->commit();
        } else {
            $this->msg = $this->getMessage($respond['msg']);
            $this->rollback();
        }

        $this->status = $respond['status'];
        $this->data = $respond['data'];
        return $this->JSONRespond();
    }

    //get department related stations by department ID
    public function getStationByDepartmentIdAction()
    {
        $request = $this->getRequest();
        if (!$request->isPost()) {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_DATA_SET');
            return $this->JSONRespond();
        }
        $this->beginTransaction();
        $respond = $this->getService('DepartmentService')->getStationByDepartmentId($request->getPost());

        if ($respond['status']) {
            $this->msg = $this->getMessage($respond['msg']);
            $this->commit();
        } else {
            $this->msg = $this->getMessage($respond['msg']);
            $this->rollback();
        }

        $this->status = $respond['status'];
        $this->data = $respond['data'];
        return $this->JSONRespond();
    }
}