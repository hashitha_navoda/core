<?php

namespace Jobs\Controller\API;

use Core\Controller\CoreController;
use Jobs\Model\TaskCard;

class JobCardSetupController extends CoreController {

	//task card update function
	public function updateJobCardAction() {

		$request = $this->getRequest();
		if (!$request->isPost()) {
			$this->status = false;
			$this->msg = $this->getMessage('ERR_DATA_SET');
			return $this->JSONRespond();
		}
		$this->beginTransaction();
		$jobCardID = $request->getPost()['taskCardID'];

		$table = $this->CommonTable('Jobs\Model\TaskCardTable');
		$respond = $this->getService('TaskCardService')->updateTaskCard($request->getPost(), $jobCardID, $table, $type = 'jobCard');

		$serviceMsg = $respond['msg'];
		$msg = null;

		switch ($serviceMsg) {
		    case "ERR_TSK_CRD_CODE_ALREDY_EXIXT":
		        $msg = "ERR_JOB_CRD_CODE_ALREDY_EXIXT";
		        break;
		    case "SUCC_TASKCARD_UPDATE":
		        $msg = "SUCC_JOBCARD_UPDATE";
		        break;
		    case "ERR_TASKCARD_UPDATE":
		        $msg = "ERR_JOBCARD_UPDATE";
		        break;
		    default:
		    	$msg = $serviceMsg;
		}

		if (!$respond['status']) {
			$this->rollback();
			$this->status = false;
			$this->msg = $this->getMessage($msg);
			return $this->JSONRespond();
		}

		$this->commit();
		$this->status = true;
		$this->data = $respond['data'];
		$this->msg = $this->getMessage($msg);
		$this->flashMessenger()->addMessage($this->msg);
		return $this->JSONRespond();
	}

	//task card save function
	public function saveJobCardAction() {
		$request = $this->getRequest();
		if (!$request->isPost()) {
			$this->status = false;
			$this->msg = $this->getMessage('ERR_DATA_SET');
			return $this->JSONRespond();
		}

		$this->beginTransaction();

		$table = $this->CommonTable('Jobs\Model\TaskCardTable');
		$jobCard = new TaskCard();
		$jobCard = $jobCard->getTaskCardCodeValidate($request->getPost()->taskCardCode, $table);

		if (!$jobCard) {
			$this->rollback();
			$this->status = false;
			$this->msg = $this->getMessage('ERR_JOB_CRD_CODE_ALREDY_EXIXT');
			return $this->JSONRespond();
		}


		$respond = $this->getService('TaskCardService')->saveTaskCard($request->getPost(), $type = 'jobCard');


		$serviceMsg = $respond['msg'];
		$msg = null;

		switch ($serviceMsg) {
		    case "ERR_TSKCRD_TSK_SAVE":
		        $msg = "ERR_JOBCRD_SRVC_SAVE";
		        break;
		    case "SUCC_TSKCRD_SAVE":
		        $msg = "SUCC_JOBCRD_SAVE";
		        break;
		    case "ERR_TSKCRD_SAVE":
		        $msg = "ERR_JOBCRD_SAVE";
		        break;
		    case "ERR_TSKCRD_SAVE":
		        $msg = "ERR_JOBCRD_SAVE";
		        break;
		    default:
		    	$msg = $serviceMsg;
		}

		if (!$respond['status']) {
			$this->rollback();
			$this->status = false;
			$this->msg = $this->getMessage($msg);
			return $this->JSONRespond();
		}

		$this->commit();
		$this->status = true;
		$this->data = $respond['data'];
		$this->msg = $this->getMessage($msg);
		$this->flashMessenger()->addMessage($this->msg);
		return $this->JSONRespond();
	}

	//get task card by task card search key
	public function getJobCardBySearchKeyAction() {
		$request = $this->getRequest();
		if (!$request->isPost()) {
			$this->status = false;
			$this->msg = $this->getMessage('ERR_DATA_SET');
			return $this->JSONRespond();
		}

		$type = 'jobCard';
		$respond = $this->getService('TaskCardService')->getTaskCardBySearchKey($request->getPost(), $this->params()->fromRoute('param1', 1), $type);

		$this->status = true;
		$this->html = $respond;
		return $this->JSONRespondHtml();
	}

	//delete job card by job card id
	public function deleteJobCardByJobCardIDAction() {
		$request = $this->getRequest();
		if (!$request->isPost()) {
			$this->status = false;
			$this->msg = $this->getMessage('ERR_DATA_SET');
			return $this->JSONRespond();
		}

		$type = 'jobCard';
		$respond = $this->getService('TaskCardService')->deleteTaskCardByTaskCardID($request->getPost(), $type);

		$serviceMsg = $respond['msg'];
		$msg = null;

		switch ($serviceMsg) {
		    case "SUCC_TSKCRD_DELETE":
		        $msg = "SUCC_JOBCRD_DELETE";
		        break;
		    case "ERR_TSKCRD_DEL":
		        $msg = "ERR_JOBCRD_DEL";
		        break;
		    case "ERR_TSKCRD_DELETE":
		        $msg = "ERR_JOBCRD_DELETE";
		        break;
		    case "ERR_TSKCRD_DELETE_JOB_RELATE":
		        $msg = "ERR_JOBCRD_DELETE_JOB_RELATE";
		        break;
		    default:
		    	$msg = $serviceMsg;
		}

		if ($respond['status']) {
			$this->msg = $this->getMessage($msg, $respond['data']);
			$this->flashMessenger()->addMessage($this->msg);
		} else {
			$this->msg = $this->getMessage($msg);
		}

		$this->status = $respond['status'];
		$this->data = $respond['data'];
		return $this->JSONRespond();
	}

	
	/**
     * This function is used to get job card related services
     */
    public function getRelatedServicesAction()
    {
    	
        $request = $this->getRequest();
        if (!$request->isPost()) {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_DATA_SET');
            return $this->JSONRespond();
        }
        $this->beginTransaction();
        $respond = $this->getService('TaskCardService')->getRelatedServices($request->getPost());

        if ($respond['status']) {
            $this->msg = $this->getMessage($respond['msg']);
            $this->commit();
        } else {
            $this->msg = $this->getMessage($respond['msg']);
            $this->rollback();
        }

        $this->status = $respond['status'];
        $this->data = $respond['data'];
        return $this->JSONRespond();
    }

    /**
     * This function is used to get vehicle type related job Cards
     */
    public function getRelatedVehicleTypesAction()
    {
    	
        $request = $this->getRequest();
        if (!$request->isPost()) {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_DATA_SET');
            return $this->JSONRespond();
        }
        $this->beginTransaction();
        $respond = $this->getService('TaskCardService')->getRelatedVehicleTypes($request->getPost());

        if ($respond['status']) {
            $this->msg = $this->getMessage($respond['msg']);
            $this->commit();
        } else {
            $this->msg = $this->getMessage($respond['msg']);
            $this->rollback();
        }

        $this->status = $respond['status'];
        $this->data = $respond['data'];
        return $this->JSONRespond();
    }

    /**
     * This function is used to get service related sub tasks details
     */
    public function viewRelatedServicesDetailsAction()
    {
        $request = $this->getRequest();
        if (!$request->isPost()) {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_DATA_SET');
            return $this->JSONRespond();
        }
        $this->beginTransaction();
        $respond = $this->getService('TaskCardService')->viewRelatedServicesDetails($request->getPost());

        if ($respond['status']) {
            $this->msg = $this->getMessage($respond['msg']);
            $this->commit();
        } else {
            $this->msg = $this->getMessage($respond['msg']);
            $this->rollback();
        }

        $this->status = $respond['status'];
        $this->data = $respond['data'];
        return $this->JSONRespond();
    }

    /**
    * this function use to change the sub task status.
    * return json respond
    **/
    public function changeJobCardServiceStatusAction()
    {

        $request = $this->getRequest();
        
        if (!$request->isPost()) {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_DATA_SET');
            $this->rollback();
            return $this->JSONRespond();
        }
        $this->beginTransaction();
        $respond = $this->getService('TaskCardService')->changeJobCardServiceStatus($request->getPost());

        if (!$respond['status']) {
            $this->status = false;
            $this->data = $respond['data'];
            $this->msg = $this->getMessage($respond['msg']);
            $this->rollback();
            return $this->JSONRespond();    
        } 
        
        $this->commit();
        $this->status = true;
        $this->msg = $this->getMessage($respond['msg']);
        return $this->JSONRespond();
        
    }

    /**
    * this function use to change the sub task status.
    * return json respond
    **/
    public function changeStatusIDAction()
    {

        $request = $this->getRequest();
        
        if (!$request->isPost()) {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_DATA_SET');
            $this->rollback();
            return $this->JSONRespond();
        }
        $this->beginTransaction();
        $respond = $this->getService('TaskCardService')->changeStatusID($request->getPost());

        if (!$respond['status']) {
            $this->status = false;
            $this->data = $respond['data'];
            $this->msg = $this->getMessage($respond['msg']);
            $this->rollback();
            return $this->JSONRespond();    
        } 
        
        $this->commit();
        $this->status = true;
        $this->msg = $this->getMessage($respond['msg']);
        return $this->JSONRespond();
        
    }

     //delete service by service id
    public function deleteJobCardServiceAction()
    {
        $request = $this->getRequest();
        if (!$request->isPost()) {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_DATA_SET');
            return $this->JSONRespond();
        }

        $type = 'service';
        $respond = $this->getService('TaskCardService')->deleteJobCardService($request->getPost());

        if ($respond['status']) {
            $this->msg = $this->getMessage($respond['msg']);
        } else {
            $this->msg = $this->getMessage($respond['msg']);
        }

        $this->status = $respond['status'];
        $this->data = $respond['data'];
        return $this->JSONRespond();
    }

    public function searchJobCardsForDropdownAction() {
		$request = $this->getRequest();
		if (!$request->isPost()) {
			$this->status = false;
			$this->msg = $this->getMessage('ERR_DATA_SET');
			return $this->JSONRespond();
		}

		$respond = $this->getService('TaskCardService')->searchJobCardForDropdown($request->getPost());

		$this->data = array('list' => $respond);
		return $this->JSONRespond();
	}

	public function searchJobCardsForDropdownForDashboardAction() {
		$request = $this->getRequest();
		if (!$request->isPost()) {
			$this->status = false;
			$this->msg = $this->getMessage('ERR_DATA_SET');
			return $this->JSONRespond();
		}

		$respond = $this->getService('TaskCardService')->searchJobCardForDropdownForDashboard($request->getPost());

		$this->data = array('list' => $respond);
		return $this->JSONRespond();
	}
}
