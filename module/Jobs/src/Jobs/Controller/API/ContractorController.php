<?php

namespace Jobs\Controller\API;

use Zend\View\Model\ViewModel;
use Core\Controller\CoreController;
use Zend\Session\Container;
use Jobs\Model\ProjectType;

class ContractorController extends CoreController
{
    /**
    * this function use to save contractor.
    **/
    public function createAction()
    {
        $request = $this->getRequest();
        if (!$request->isPost()) {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_DATA_SET');
            return $this->JSONRespond();
        }

        $respond = $this->getService('ContractorService')->saveContractor($request->getPost());
        
        if (!$respond['status']) {
            $this->rollback();
            $this->status = false;
            $this->msg = $this->getMessage($respond['msg']);
            return $this->JSONRespond();    
        }

        $this->commit();
        $this->status = true;
        $this->data = $respond['data'];
        $this->msg = $this->getMessage($respond['msg']);
        $this->flashMessenger()->addMessage($this->msg);
        return $this->JSONRespond();
        
    }

    /**
    * this function use to edit contractor.
    **/
    public function editContractorAction()
    {
        $request = $this->getRequest();
        
        if (!$request->isPost()) {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_DATA_SET');
            return $this->JSONRespond();
        }

        $this->beginTransaction();

        $respond = $this->getService('ContractorService')->editContractor($request->getPost(), $this->params()->fromRoute(), $this->userID);

        if (!$respond['status']) {
            $this->rollback();
            $this->status = false;
            $this->msg = $this->getMessage($respond['msg']);
            return $this->JSONRespond();    
        }

        $this->commit();
        $this->status = true;
        $this->data = $respond['data'];
        $this->msg = $this->getMessage($respond['msg']);
        $this->flashMessenger()->addMessage($this->msg);
        return $this->JSONRespond();
    }

    /**
    * this function use to delete contractors in the list.
    **/
    public function deleteContractorAction()
    {

        $request = $this->getRequest();
        if (!$request->isPost()) {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_DATA_SET');
            return $this->JSONRespond();
        }

        $this->beginTransaction();

        $respond = $this->getService('ContractorService')->deleteContractor($request->getPost(), $this->userID);

        if (!$respond['status']) {
            $this->rollback();
            $this->status = false;
            $this->msg = $this->getMessage($respond['msg']);
            return $this->JSONRespond();    
        }

        $this->commit();
        $this->status = true;
        $this->data = $respond['data'];
        $this->msg = $this->getMessage($respond['msg']);
        return $this->JSONRespond();
    }
    
    /**
    * this function use to search contractor list.
    **/
    public function searchAction() 
    {
        $request = $this->getRequest();
        if (!$request->isPost()) {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_DATA_SET');
            return $this->JSONRespond();
        }
        $result = $this->getService('ContractorService')->contractorSearchByKey($request->getPost('searchKey'), (int) $this->params()->fromRoute('param1', 1));
        $this->status = $result['status'];
        $this->html = $result['view'];
        return $this->JSONRespondHtml();

    }

    /**
    * this function use to change the contractor status.
    **/
    public function changeStatusContractorAction()
    {
        $request = $this->getRequest();
        if (!$request->isPost()) {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_DATA_SET');
            return $this->JSONRespond();
        }

        $this->beginTransaction();

        $respond = $this->getService('ContractorService')->changeStatusContractor($request->getPost(), $this->userID);

        if (!$respond['status']) {
            $this->rollback();
            $this->status = false;
            $this->msg = $this->getMessage($respond['msg']);
            return $this->JSONRespond();    
        }

        $this->commit();
        $this->status = true;
        $this->data = $respond['data'];
        $this->msg = $this->getMessage($respond['msg']);
        return $this->JSONRespond();
    }

}
