<?php

namespace Jobs\Controller\API;

use Core\Controller\CoreController;

class ResourcesController extends CoreController {

	/**
	 * This function is used to save vehicle type
	 * @param JSON Request
	 * @return JSON Respond
	 */
	public function saveVehicleTypeAction()
	{
		$request = $this->getRequest();
		if (!$request->isPost()) {
			$this->status = false;
			$this->msg = $this->getMessage('ERR_DATA_SET');
			return $this->JSONRespond();
		}

		$this->beginTransaction();

		$respond = $this->getService('ResourcesService')->saveVehicleType($request->getPost(), $this->userID);

		if (!$respond['status']) {
			$this->rollback();
			$this->status = false;
			$this->msg = $this->getMessage($respond['msg']);
			return $this->JSONRespond();
		}

		$this->commit();
		$this->status = true;
		$this->data = $respond['data'];
		$this->msg = $this->getMessage($respond['msg'], array($respond['data']));
		$this->flashMessenger()->addMessage($this->msg);
		return $this->JSONRespond();
	}

	/**
	 * This function is used to update vehicle type
	 * @param JSON Request
	 * @return JSON Respond
	 */
	public function updateVehicleTypeAction()
	{
		$request = $this->getRequest();
		if (!$request->isPost()) {
			$this->status = false;
			$this->msg = $this->getMessage('ERR_DATA_SET');
			return $this->JSONRespond();
		}

		$this->beginTransaction();

		$respond = $this->getService('ResourcesService')->updateVehicleType($request->getPost(), $this->userID);

		if (!$respond['status']) {
			$this->rollback();
			$this->status = false;
			$this->msg = $this->getMessage($respond['msg']);
			return $this->JSONRespond();
		}

		$this->commit();
		$this->status = true;
		$this->data = $respond['data'];
		$this->msg = $this->getMessage($respond['msg'], array($respond['data']));
		$this->flashMessenger()->addMessage($this->msg);
		return $this->JSONRespond();
	}

	/**
	 * This function is used to view vehicle type
	 * @param JSON Request
	 * @return JSON Respond
	 */
	public function viewVehicleTypeAction()
	{
		$request = $this->getRequest();
		if (!$request->isPost()) {
			$this->status = false;
			$this->msg = $this->getMessage('ERR_DATA_SET');
			return $this->JSONRespond();
		}

		$this->beginTransaction();

		$respond = $this->getService('ResourcesService')->viewVehicleType($request->getPost());

		if (!$respond['status']) {
			$this->rollback();
			$this->status = false;
			$this->msg = $this->getMessage($respond['msg']);
			return $this->JSONRespond();
		}

		$this->commit();
		$this->status = true;
		$this->data = $respond['data'];
		$this->msg = $this->getMessage($respond['msg'], array($respond['data']));
		return $this->JSONRespond();
	}

	/**
	 * This function is used to change status vehicle type
	 * @param JSON Request
	 * @return JSON Respond
	 */
	public function changeStatusVehicleTypeAction()
	{
		$request = $this->getRequest();
		if (!$request->isPost()) {
			$this->status = false;
			$this->msg = $this->getMessage('ERR_DATA_SET');
			return $this->JSONRespond();
		}

		$this->beginTransaction();

		$respond = $this->getService('ResourcesService')->changeStatusVehicleType($request->getPost());

		if (!$respond['status']) {
			$this->rollback();
			$this->status = false;
			return $this->JSONRespond();
		}

		$this->commit();
		$this->status = true;
		$this->data = $respond['data'];
		$this->msg = $this->getMessage($respond['msg'], array($respond['data']));
		return $this->JSONRespond();
	}

	/**
	 * This function is used to delete vehicle type
	 * @param JSON Request
	 * @return JSON Respond
	 */
	public function deleteVehicleTypeAction()
	{
		$request = $this->getRequest();
		if (!$request->isPost()) {
			$this->status = false;
			$this->msg = $this->getMessage('ERR_DATA_SET');
			return $this->JSONRespond();
		}

		$this->beginTransaction();

		$respond = $this->getService('ResourcesService')->deleteVehicleType($request->getPost(), $this->userID);

		if (!$respond['status']) {
			$this->rollback();
			$this->status = false;
			$this->msg = $this->getMessage($respond['msg']);
			return $this->JSONRespond();
		}

		$this->commit();
		$this->status = true;
		$this->data = $respond['data'];
		$this->msg = $this->getMessage($respond['msg'], array($respond['data']));
		return $this->JSONRespond();
	}

	/**
	 * This function is used to seach vehicle type
	 * @param JSON Request
	 * @return JSON Respond
	 */
	public function searchVehicleTypesAction()
	{
		$request = $this->getRequest();
		if (!$request->isPost()) {
			$this->status = false;
			$this->msg = $this->getMessage('ERR_DATA_SET');
			return $this->JSONRespond();
		}

		$respond = $this->getService('ResourcesService')->searchVehicleTypes($request->getPost(), $this->params()->fromRoute('param1', 1));

		$this->status = true;
		$this->html = $respond;
		return $this->JSONRespondHtml();
	}


	/**
	 * This function is used to seach vehicle type for dropdown
	 * @param JSON Request
	 * @return JSON Respond
	 */
	public function searchVehicleTypesForDropdownAction() {
		$request = $this->getRequest();
		if (!$request->isPost()) {
			$this->status = false;
			$this->msg = $this->getMessage('ERR_DATA_SET');
			return $this->JSONRespond();
		}

		$respond = $this->getService('ResourcesService')->searchVehicleTypeForDropdown($request->getPost());

		$this->data = array('list' => $respond);
		return $this->JSONRespond();
	}

	/**
	 * This function is used to seach vehicle type for dropdown
	 * @param JSON Request
	 * @return JSON Respond
	 */
	public function loadVehicleTypeForDropdownAction() {
		$request = $this->getRequest();
		if (!$request->isPost()) {
			$this->status = false;
			$this->msg = $this->getMessage('ERR_DATA_SET');
			return $this->JSONRespond();
		}

		$respond = $this->getService('ResourcesService')->loadVehicleTypeForDropdown($request->getPost());
		$this->status = $respond['status'];
		$this->data = $respond;
		return $this->JSONRespond();
	}

	/**
	 * This function is used to load vehicle models depend on vehicle type
	 * @param JSON Request
	 * @return JSON Respond
	 */
	public function loadVehicleModelForDropdownAction() {
        $request = $this->getRequest();
        if (!$request->isPost()) {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_DATA_SET');
            return $this->JSONRespond();
        }
        $respond = $this->getService('ResourcesService')->loadVehicleModelForDropdown($request->getPost());
        $this->status = $respond['status'];
        $this->msg = $this->getMessage($respond['msg']);
        $this->data = $respond['data'];
        return $this->JSONRespond();
    }

    /**
	 * This function is used to seach vehicle model for dropdown
	 * @param JSON Request
	 * @return JSON Respond
	 */
    public function searchVehicleModelsForDropdownAction() {
		$request = $this->getRequest();
		if (!$request->isPost()) {
			$this->status = false;
			$this->msg = $this->getMessage('ERR_DATA_SET');
			return $this->JSONRespond();
		}

		$respond = $this->getService('ResourcesService')->searchVehicleModelsForDropdown($request->getPost());

		$this->data = array('list' => $respond);
		return $this->JSONRespond();
	}

	/**
	 * This function is used to get service list by job card id
	 * @param JSON Request
	 * @return JSON Respond
	 */
	public function getServiceListAction()
	{
		$request = $this->getRequest();
		if (!$request->isPost()) {
			$this->status = false;
			$this->msg = $this->getMessage('ERR_DATA_SET');
			return $this->JSONRespond();
		}

		$this->beginTransaction();

		$respond = $this->getService('ResourcesService')->getServiceListByVehicleType($request->getPost());

		if (!$respond['status']) {
			$this->rollback();
			$this->status = false;
			$this->msg = $this->getMessage($respond['msg']);
			return $this->JSONRespond();
		}

		$this->commit();
		$this->status = true;
		$this->data = $respond['data'];
		$this->msg = $this->getMessage($respond['msg'], array($respond['data']));
		return $this->JSONRespond();
	}

	/**
	 * This function is used to get product list by job card service id
	 * @param JSON Request
	 * @return JSON Respond
	 */
	public function getServiceProductListAction()
	{
		$request = $this->getRequest();
		if (!$request->isPost()) {
			$this->status = false;
			$this->msg = $this->getMessage('ERR_DATA_SET');
			return $this->JSONRespond();
		}

		$this->beginTransaction();

		$respond = $this->getService('ResourcesService')->getServiceProductList($request->getPost());

		if (!$respond['status']) {
			$this->rollback();
			$this->status = false;
			$this->msg = $this->getMessage($respond['msg']);
			return $this->JSONRespond();
		}

		$this->commit();
		$this->status = true;
		$this->data = $respond['data'];
		$this->msg = $this->getMessage($respond['msg'], array($respond['data']));
		return $this->JSONRespond();
	}

	/**
	 * This function is used to save Materials
	 * @param JSON Request
	 * @return JSON Respond
	 */
	public function saveMaterialsAction()
	{
		$request = $this->getRequest();
		if (!$request->isPost()) {
			$this->status = false;
			$this->msg = $this->getMessage('ERR_DATA_SET');
			return $this->JSONRespond();
		}

		$this->beginTransaction();

		$respond = $this->getService('ResourcesService')->saveMaterials($request->getPost());

		if (!$respond['status']) {
			$this->rollback();
			$this->status = false;
			$this->msg = $this->getMessage($respond['msg']);
			return $this->JSONRespond();
		}

		$this->commit();
		$this->status = true;
		$this->data = $respond['data'];
		$this->msg = $this->getMessage($respond['msg'], array($respond['data']));
		return $this->JSONRespond();
	}

	/**
	 * This function is used to get product list with product related all uom by job card service id
	 * @param JSON Request
	 * @return JSON Respond
	 */
	public function getServiceProductListWithProductUomAction()
	{
		$request = $this->getRequest();
		if (!$request->isPost()) {
			$this->status = false;
			$this->msg = $this->getMessage('ERR_DATA_SET');
			return $this->JSONRespond();
		}

		$this->beginTransaction();

		$respond = $this->getService('ResourcesService')->getServiceProductListWithProductUom($request->getPost());

		if (!$respond['status']) {
			$this->rollback();
			$this->status = false;
			$this->msg = $this->getMessage($respond['msg']);
			return $this->JSONRespond();
		}

		$this->commit();
		$this->status = true;
		$this->data = $respond['data'];
		$this->msg = $this->getMessage($respond['msg'], array($respond['data']));
		return $this->JSONRespond();
	}

	/**
	 * This function is used to edit Materials
	 * @param JSON Request
	 * @return JSON Respond
	 */
	public function editMaterialsAction()
	{
		$request = $this->getRequest();
		if (!$request->isPost()) {
			$this->status = false;
			$this->msg = $this->getMessage('ERR_DATA_SET');
			return $this->JSONRespond();
		}

		$this->beginTransaction();

		$respond = $this->getService('ResourcesService')->editMaterials($request->getPost());

		if (!$respond['status']) {
			$this->rollback();
			$this->status = false;
			$this->msg = $this->getMessage($respond['msg']);
			return $this->JSONRespond();
		}

		$this->commit();
		$this->status = true;
		$this->data = $respond['data'];
		$this->msg = $this->getMessage($respond['msg'], array($respond['data']));
		return $this->JSONRespond();
	}
}