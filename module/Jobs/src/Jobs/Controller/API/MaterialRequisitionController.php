<?php

namespace Jobs\Controller\API;

use Core\Controller\CoreController;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;

class MaterialRequisitionController extends CoreController
{

    public function updateRequestAction()
    {
        $request = $this->getRequest();
        if (!$request->isPost()) {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_DATA_SET');
            return $this->JSONRespond();
        }

        $postData = $request->getPost();

        $this->beginTransaction();
        $res = $this->getService('MaterialRequisitionService')->approveRequest($postData);

        if (!$res['status']) {
            $this->rollback();
        }
        else {
            $this->commit();
        }

        $this->status = $res['status'];
        $this->msg = $this->getMessage($res['msg']);
        return $this->JSONRespond();
    }

    public function saveAction()
    {
        $request = $this->getRequest();
        if (!$request->isPost()) {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_DATA_SET');
            return $this->JSONRespond();
        }

        $this->beginTransaction();
        
        $res = $this->getService('MaterialRequisitionService')->saveMaterialRequest($request->getPost(), $this->userID);

        if (!$res['status']) {
            $this->rollback();
        } else {
            $this->commit();
        }

        $this->status = $res['status'];
        $this->data = $res['data'];
        $this->msg = $this->getMessage($res['msg']);
        return $this->JSONRespond();
    }

    public function getRequestedProductsAction()
    {
        $request = $this->getRequest();
        if (!$request->isPost()) {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_DATA_SET');
            return $this->JSONRespond();
        }

        $requestId = $request->getPost('requestId');

        $res = $this->getService('MaterialRequisitionService')->getRequestedProductById($requestId);
        $this->status = $res['status'];
        $this->data = $res['data'];
        return $this->JSONRespond();
    }

    public function issueMaterialAction()
    {
        $request = $this->getRequest();
        if (!$request->isPost()) {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_DATA_SET');
            return $this->JSONRespond();
        }

        $materials = $request->getPost('materials');
        $jobId = $request->getPost('jobId');
        $reqId = $request->getPost('reqId');
        
        $this->beginTransaction();
        $res = $this->getService('MaterialRequisitionService')->updateIssueQuantity($materials, $jobId, $reqId);

        if ($res['status']) {
            $this->commit();
        } else {
            $this->rollback();
        }

        $this->status = $res['status'];
        $this->msg = $this->getMessage($res['msg']);
        $this->data = $res['data'];
        return $this->JSONRespond();
    }

    public function sendMaterialRequisitionEmailAction()
    {
        $request = $this->getRequest();
        $documentType = 'Material Requisition Note';

        if ($request->isPost()) {
            $this->mailDocumentAsTemplate($request, $documentType);
            $this->status = TRUE;
            $this->msg = $this->getMessage('SUCC_PAY_EMAIL');
            return $this->JSONRespond();
        }
        $this->status = FALSE;
        $this->msg = $this->getMessage('ERR_GRN_EMAIL', array($documentType));
        return $this->JSONRespond();
    }

}
