<?php

namespace Jobs\Controller\API;

use Core\Controller\CoreController;
use Invoice\Model\Returns;
use Invoice\Model\ReturnsProduct;
use Invoice\Model\ReturnsProductTax;
use Invoice\Model\ReturnsSubProduct;
use Inventory\Model\ItemIn;
use Inventory\Model\ItemOut;
use Inventory\Model\ProductSerial;
use Inventory\Model\ProductBatch;

class ServiceJobsController extends CoreController {

	/**
	 * This function is used to save job
	 * @param JSON Request
	 * @return JSON Respond
	 */
	public function saveJobAction() 
	{

		$request = $this->getRequest();
		if (!$request->isPost()) {
			$this->status = false;
			$this->msg = $this->getMessage('ERR_DATA_SET');
			return $this->JSONRespond();
		}

		$this->beginTransaction();

		$userLocation = $this->user_session->userActiveLocation['locationID'];
		$type = 'service';
		$respond = $this->getService('JobService')->saveJob($request->getPost(), $userLocation, $this->userID, $type);

		if (!$respond['status']) {
			$this->rollback();
			$this->status = false;
			$this->msg = $this->getMessage($respond['msg']);
			return $this->JSONRespond();
		}

		$this->commit();
		$this->status = true;
		$this->data = $respond['data'];
		$this->msg = $this->getMessage($respond['msg'], array($respond['data']));
		$this->flashMessenger()->addMessage($this->msg);
		return $this->JSONRespond();
	}
		
	
   /**
    * this action use to get vehicle details by using vehiclr reg no
    * @param JSON Request
    * @param JSON Respond
    **/
    public function getVehicleDetailsAction()
    {
    	$request = $this->getRequest();
    	if (!$request->isPost()) {
    		$this->status = false;
        	$this->msg = $this->getMessage('ERR_REQUEST');
        	return $this->JSONRespond();	
    	}
        $respond = $this->getService('JobService')->getVehicleDetails($request->getPost());

        $this->data = $respond['data'];
        $this->status = true;
        return $this->JSONRespond();
    	
    }

    /**
	 * This function is used to get edit details
	 * @param JSON Request
	 * @return JSON Respond
	 */
	public function getEditDetailsAction() 
	{

		$request = $this->getRequest();
    	if (!$request->isPost()) {
    		$this->status = false;
        	$this->msg = $this->getMessage('ERR_REQUEST');
        	return $this->JSONRespond();	
    	}

		$locationID = $this->user_session->userActiveLocation['locationID'];
        $this->beginTransaction();
        $respond = $this->getService('JobService')->getServiceJobById($request->getPost(), $locationID);

        
        if (!$respond['status']) {
            $this->status = false;
            $this->data = $respond['data'];
            $this->msg = $this->getMessage($respond['msg']);
            $this->rollback();
            return $this->JSONRespond();    
        } 
        $this->commit();
        $this->status = true;
        $this->data = $respond['data'];
        return $this->JSONRespond();
	}

	/**
	 * This function is used to updates job
	 * @param JSON Request
	 * @return JSON Respond
	 */
	public function updateJobAction() 
	{
		$request = $this->getRequest();
		if (!$request->isPost()) {
			$this->status = false;
			$this->msg = $this->getMessage('ERR_DATA_SET');
			return $this->JSONRespond();
		}

		$this->beginTransaction();

		$userLocation = $this->user_session->userActiveLocation['locationID'];
		$respond = $this->getService('JobService')->updateJob($request->getPost(), $userLocation, $this->userID, $type="service");

		if (!$respond['status']) {
			$this->rollback();
			$this->status = false;
			$this->msg = $this->getMessage($respond['msg']);
			return $this->JSONRespond();
		}

		$this->commit();
		$this->status = true;
		$this->data = $respond['data'];
		$this->msg = $this->getMessage($respond['msg'], array($respond['data']));
		$this->flashMessenger()->addMessage($this->msg);

		return $this->JSONRespond();
	}

	/**
	 * This function is used to search job
	 * @param JSON Request
	 * @return JSON RespondHtml
	 */
	public function getJobBySearchKeyAction() 
	{
		$request = $this->getRequest();
		if (!$request->isPost()) {
			$this->status = false;
			$this->msg = $this->getMessage('ERR_DATA_SET');
			return $this->JSONRespond();
		}

		$respond = $this->getService('JobService')->getServiiceJobBySearchKey($request->getPost(), $this->params()->fromRoute('param1', 1));

		$this->status = true;
		$this->html = $respond;
		return $this->JSONRespondHtml();
	}



	// this function use to get job related services
	public function getJobServicesAction() {
		$request = $this->getRequest();
		if (!$request->isPost()) {
			$this->status = false;
			$this->msg = $this->getMessage('ERR_DATA_SET');
			return $this->JSONRespond();
		}

		$this->beginTransaction();
		$respond = $this->getService('JobService')->getJobRelatedServicesByJobId($request->getPost(), $jobDashboard = false);

		if (!$respond['status']) {
			$this->status = false;
            $this->msg = $this->getMessage($respond['msg']);
            $this->rollback();
            return $this->JSONRespond();
		}

		$this->status = true;
		$this->html = $respond['view'];
		$this->data = $respond['data'];
        return $this->JSONRespondHtml();
	}

	/**
	 * This function is used to delete job
	 * @param JSON Request
	 * @return JSON Respond
	 */
	function deleteAction()
    {
        $request = $this->getRequest();
		if (!$request->isPost()) {
			$this->status = false;
			$this->msg = $this->getMessage('ERR_DATA_SET');
			return $this->JSONRespond();
		}

		$this->beginTransaction();
		$respond = $this->getService('JobService')->deleteJob($request->getPost(), $this->userID);

		if (!$respond['status']) {
			$this->rollback();
			$this->status = false;
			$this->msg = $this->getMessage($respond['msg']);
			return $this->JSONRespond();
		}

		$this->commit();
		$this->status = true;
		$this->data = $respond['data'];
		$this->msg = $this->getMessage($respond['msg'], array($respond['data']));
		$this->flashMessenger()->addMessage($this->msg);
		return $this->JSONRespond();
    }

    /**
	 * This function is used to restart job related services
	 * @param JSON Request
	 * @return JSON Respond
	 */
	function restartJobRelatedServicesAction()
    {
        $request = $this->getRequest();
		if (!$request->isPost()) {
			$this->status = false;
			$this->msg = $this->getMessage('ERR_DATA_SET');
			return $this->JSONRespond();
		}

		$this->beginTransaction();
		$respond = $this->getService('JobDashboardService')->restartJobRelatedServices($request->getPost());

		if (!$respond['status']) {
			$this->rollback();
			$this->status = false;
			$this->msg = $this->getMessage($respond['msg']);
			return $this->JSONRespond();
		}

		$this->commit();
		$this->status = true;
		$this->data = $respond['data'];
		$this->msg = $this->getMessage($respond['msg'], array($respond['data']));
		return $this->JSONRespond();
    }

    public function initiateBusinessAction()
    {
    	$request = $this->getRequest();
        if (!$request->isPost()) {
        	$this->status = false;
            $this->msg = $this->getMessage('ERR_DATA_SET');
            return $this->JSONRespond();
        }
        
        $respond = $this->getService('JobService')->initiateBusinessType($request->getPost());
        if (!$respond) {
        	$this->status = true;
        	$this->msg = $this->getMessage('ERR_SAVE_BUSI_TYPE');
        	return $this->JSONRespond();	
        } 
        
        $this->status = true;
        $this->msg = $this->getMessage('SUCC_SAVE_BUSI_TYPE');
        return $this->JSONRespond();
    }

    public function getAllVehicalAction()
    {
    	$request = $this->getRequest();
        if (!$request->isPost()) {
        	$this->status = false;
        	$this->msg = $this->getMessage('ERR_REQUEST');
        	return $this->JSONRespond();
        }

        $respond = $this->getService('JobService')->getAllServiceVehicals($request->getPost());
        
        $this->data = array('list' => $respond);
        return $this->JSONRespond();	
    }

    public function getJobsByVehicleRegNoAction()
    {
    	$request = $this->getRequest();
        if (!$request->isPost()) {
        	$this->status = false;
        	$this->msg = $this->getMessage('ERR_REQUEST');
        	return $this->JSONRespond();
        }

        $respond = $this->getService('JobService')->getJobsByVehicleRegNo($request->getPost());
        $this->status = true;
        $this->data = $respond['data'];
        $this->html = $respond['view'];
        return $this->JSONRespondHtml();	
    }

    public function getJobSideViewByJobIdAction()
    {
    	$request = $this->getRequest();
        if (!$request->isPost()) {
        	$this->status = false;
        	$this->msg = $this->getMessage('ERR_REQUEST');
        	return $this->JSONRespond();
        }

        $respond = $this->getService('JobService')->getJobSideViewByJobId($request->getPost());
        $this->status = true;
        $this->data = $respond['data'];
        $this->html = $respond['view'];
        return $this->JSONRespondHtml();	
    }

    public function getJobItemChangeSideViewAction()
    {
    	$request = $this->getRequest();
        if (!$request->isPost()) {
        	$this->status = false;
        	$this->msg = $this->getMessage('ERR_REQUEST');
        	return $this->JSONRespond();
        }

        $respond = $this->getService('JobService')->getJobItemChangeSideView($request->getPost());
        $this->status = true;
        $this->data = $respond['data'];
        $this->html = $respond['view'];
        return $this->JSONRespondHtml();	
    }

    public function searchVehiclesForDropdownAction()
    {
    	$request = $this->getRequest();
        if (!$request->isPost()) {
        	$this->status = false;
        	$this->msg = $this->getMessage('ERR_REQUEST');
        	return $this->JSONRespond();
        }

        $respond = $this->getService('JobService')->searchVehiclesForDropdown($request->getPost());
        $this->status = true;
        $this->data = $respond;
        return $this->JSONRespond();
    }

    function cancelJobAction()
    {
        $request = $this->getRequest();
		if (!$request->isPost()) {
			$this->status = false;
			$this->msg = $this->getMessage('ERR_DATA_SET');
			return $this->JSONRespond();
		}

		$this->beginTransaction();
		$respond = $this->getService('JobService')->cancelJob($request->getPost(), $this->userID);

		if (!$respond['status']) {
			$this->rollback();
			$this->status = false;
			$this->msg = $this->getMessage($respond['msg']);
			return $this->JSONRespond();
		} else {
			$this->commit();
			$this->status = true;
			$this->data = $respond['data'];
			$this->msg = $this->getMessage($respond['msg'], array($respond['data']));
			$this->flashMessenger()->addMessage($this->msg);
			return $this->JSONRespond();
		}

    }

    function saveDeliveryNoteReturn($deliData) {

        if ($deliData) {
        	if ($this->user_session->useAccounting == "1") {
        		$this->useAccounting = 1;
        	}

            $multipleDlnCopyFlag = $deliData['multipleDlnCopyFlag'];
            $customerID = $deliData['customerID'];
            $customerName = $deliData['customerName'];
            $dimensionData = $deliData['dimensionData'];
            $customCurrencyId = $deliData['customCurrencyId'];
            $crossLocationFlag = ($deliData['crossLocationFlag'] === 'false') ? false : true;
            $delBatchDetails = $deliData['delBatchDetails'];
            $delSerialDetails = $deliData['delSerialDetails'];
            $ignoreBudgetLimit = $deliData['ignoreBudgetLimit'];
            $salesReturnCustomCurrencyRate = (float) $deliData['salesReturnCustomCurrencyRate'];
            //get customCurrency data by customer CurrencyId
            $customCurrencyData = (object) $this->CommonTable('Core\Model\CurrencyTable')->getCurrency($customCurrencyId);
            $customCurrencySymbol = ($customCurrencyId != "") ? $customCurrencyData->currencySymbol : $this->companyCurrencySymbol;


            $cust = $this->CommonTable('Invoice\Model\CustomerTable')->getCustomerByID($customerID);
            if ($cust->customerName . '-' . $cust->customerCode == $customerName) {
                $products = $deliData['products'];
                $subProducts = $deliData['subProducts'];
                $returnCode = $deliData['returnCode'];
                $locationID = $deliData['locationID'];
                $date = $this->convertDateToStandardFormat($deliData['date']);
                $totalPrice = (float) trim($deliData['returnTotalPrice']) * $salesReturnCustomCurrencyRate;
                $comment = $deliData['returnComment'];


                $result = $this->getReferenceNoForLocation('7', $locationID);
                $locationReferenceID = $result['locRefID'];

                //check return numer already exist in return table if exist give next number for return code
                while ($returnCode) {
                    if ($this->CommonTable('SalesReturnsTable')->checkReturnByCode($returnCode)->current()) {
                        if ($locationReferenceID) {
                            $newReturnCode = $this->getReferenceNumber($locationReferenceID);
                            if ($newReturnCode == $returnCode) {
                                $this->updateReferenceNumber($locationReferenceID);
                                $returnCode = $this->getReferenceNumber($locationReferenceID);
                            } else {
                                $returnCode = $newReturnCode;
                            }
                        } else {

                        	$returnData = [
                        		'status' => false,
                        		'msg' => 'ERR_RETURNAPI_CODE_EXIST',
                        	];
                 
                            return $returnData;
                        }
                    } else {
                        break;
                    }
                }

                $result = $this->CommonTable('Settings\Model\DisplaySetupTable')->fetchAllDetails();
                $displayData = $result->current();
                $FIFOCostingFlag = $displayData['FIFOCostingFlag'];
                $averageCostingFlag = $displayData['averageCostingFlag'];

                if(empty($FIFOCostingFlag) && empty($averageCostingFlag)){

                    $returnData = [
                		'status' => false,
                		'msg' => 'ERR_COSTING_METHOD_NOT_CHOOSE',
                	];
                	return $returnData;
                }
                
                $deliveryNoteID = $deliData['deliveryNoteID'];
                $deliveryNoteOriginalLocationID = $locationID;
                if ($multipleDlnCopyFlag == "false") {
                    $deliveryNoteStatus = $this->CommonTable('Invoice\Model\DeliveryNoteTable')->getDeliveryNoteStatusByDeliveryNoteID($deliveryNoteID)->current();
                    if ($deliveryNoteStatus['deliveryNoteStatus'] == "10") {

                        $returnData = [
	                		'status' => false,
	                		'msg' => 'ERR_INVOICE_DLN',
	                	];
	                	return $returnData;

                    }

                    $deliveryNoteOriginalLocationID = $deliveryNoteStatus['deliveryNoteLocationID'];
                    

                    $checkProduct = $this->_checkDeliveryNoteProductsByDeliveryNoteID($deliveryNoteID);
                    if ($checkProduct == "0") {

                        $returnData = [
	                		'status' => false,
	                		'msg' => 'ERR_RETURNAPI_RETURN_ALL_PRODUCTS',
	                	];
	                	return $returnData;
                    }
                } else {
                    foreach ($deliveryNoteID as $key => $value) {
                        $deliveryNoteStatus = $this->CommonTable('Invoice\Model\DeliveryNoteTable')->getDeliveryNoteStatusByDeliveryNoteID($value)->current();
                        if ($deliveryNoteStatus['deliveryNoteStatus'] == "10") {

                            $returnData = [
		                		'status' => false,
		                		'msg' => 'ERR_INVOICE_DLN',
		                	];
		                	return $returnData;
                        }

                        $checkProduct = $this->_checkDeliveryNoteProductsByDeliveryNoteID($value);
                        if ($checkProduct == "0") {

                            $returnData = [
		                		'status' => false,
		                		'msg' => 'ERR_RETURNAPI_RETURN_ALL_PRODUCTS',
		                	];
		                	return $returnData;
                        }
                    }
                }


                $entityID = $this->createEntity();
                $returnData = array(
                    'salesReturnCode' => $returnCode,
                    'customerID' => $customerID,
                    'locationID' => $locationID,
                    'salesReturnDate' => $date,
                    'salesReturnTotal' => $totalPrice,
                    'salesReturnComment' => $comment,
                    'statusID' => 4,
                    'customCurrencyId' => $customCurrencyId,
                    'salesReturnCustomCurrencyRate' => $salesReturnCustomCurrencyRate,
                    'entityID' => $entityID
                );

                $return = new Returns;
                $return->exchangeArray($returnData);

                $returnID = $this->CommonTable('SalesReturnsTable')->saveSalesReturn($return);

                if ($locationReferenceID) {
                    $this->updateReferenceNumber($locationReferenceID);
                }

                $accountProduct = array();
                foreach ($products as $key => $p) {
                    $batchProducts = $subProducts[$p['productID']];
                    
                    if ($multipleDlnCopyFlag == "false") {
                        $productID = $p['productID'];
                    } else {
                        $productID = $p['productID'];
                        $productID = explode("_", $productID)[1];
                    }

                    $deliveryNoteProductID = $p['deliveryNoteproductID'];
                    $loreturnQty = $returnQty = $p['returnQuantity']['qty'];
                    $productType = $p['productType'];
                    if ($productType == 2) {
                        $loreturnQty = 0;
                    }

                    $pData = $this->CommonTable('Inventory\Model\ProductTable')->getProductByProductID($productID);

                    if($this->useAccounting == 1){
                        if(empty($pData['productInventoryAccountID'])){

                            $returnData = [
		                		'status' => false,
		                		'msg' => 'ERR_GRN_PRODUCT_ACCOUNT'
		                	];
		                	return $returnData;
                        }
                         //check whether Product Cost of Goods Sold Gl account id set or not
                        if(empty($pData['productCOGSAccountID'])){

                            $returnData = [
		                		'status' => false,
		                		'msg' => 'ERR_GRN_PRODUCT_ACCOUNT_COGS'
		                	];
		                	return $returnData;
                        }
                    }
                    // var_dump($productID).die();

                    $locationProduct = $this->CommonTable('Inventory\Model\LocationProductTable')->getLocationProduct($productID, $locationID);
                    $locationProductID = $locationProduct->locationProductID;
                    //get location Product Data for set average costing Price
                    $locationProductAverageCostingPrice = $locationProduct->locationProductAverageCostingPrice;
                    $locationProductQuantity = $locationProduct->locationProductQuantity;
                    $locationProductLastItemInID = $locationProduct->locationProductLastItemInID;

                    $locationProductData = array(
                        'locationProductQuantity' => $locationProduct->locationProductQuantity + $loreturnQty,
                    );
                    $this->CommonTable('Inventory\Model\LocationProductTable')->updateQty($locationProductData, $productID, $locationID);

                    // get original deliveryNote location Product Details
                    $origLocationProduct = $this->CommonTable('Inventory\Model\LocationProductTable')->getLocationProduct($productID, $deliveryNoteOriginalLocationID);
                    $origLocationProductID = $origLocationProduct->locationProductID;

                    //calculate discount value
                    $discountValue = 0;
                    if ($p['productDiscountType'] == "precentage") {
                        $discountValue = ($p['productPrice']  * $p['productDiscount']/ 100) * $salesReturnCustomCurrencyRate;
                    } else if ($p['productDiscountType'] == "value") {
                        $discountValue = $p['productDiscount'] * $salesReturnCustomCurrencyRate;
                        $p['productDiscount'] = $p['productDiscount'] * $salesReturnCustomCurrencyRate;
                    }
                    
                    $returnProductData = array(
                        'salesReturnID' => $returnID,
                        'deliveryNoteProductID' => $deliveryNoteProductID,
                        'productID' => $productID,
                        'salesReturnProductPrice' => $p['productPrice'] * $salesReturnCustomCurrencyRate,
                        'salesReturnProductDiscount' => $p['productDiscount'],
                        'salesReturnProductDiscountType' => $p['productDiscountType'],
                        'salesReturnProductTotal' => $p['productTotal'] * $salesReturnCustomCurrencyRate,
                        'salesReturnProductQuantity' => $returnQty
                    );
                    $returnProdut = new ReturnsProduct;
                    $returnProdut->exchangeArray($returnProductData);
                    $returnProductID = $this->CommonTable('SalesReturnsProductTable')->saveReturnProducts($returnProdut);

                    $deliveryNoteProductQuantity = $this->CommonTable('Invoice\Model\DeliveryNoteProductTable')->getDeliveryNoteProductQuantityByDeliveryNoteProductID($deliveryNoteProductID)->current();

                    $deliveryNoteRemainQty = $deliveryNoteProductQuantity['deliveryNoteProductQuantity'] - $deliveryNoteProductQuantity['deliveryNoteProductCopiedQuantity'];

                    $dNProductCopiedQuantity = $deliveryNoteProductQuantity['deliveryNoteProductCopiedQuantity'] + $returnQty;

                    if ($dNProductCopiedQuantity < $deliveryNoteProductQuantity['deliveryNoteProductQuantity']) {
                        $deliveryNoteProductQuantityUpdateData = array(
                            'deliveryNoteProductCopiedQuantity' => $dNProductCopiedQuantity
                        );
                    } else {
                        $deliveryNoteProductQuantityUpdateData = array(
                            'deliveryNoteProductCopiedQuantity' => $dNProductCopiedQuantity,
                            'copied' => 1
                        );
                    }

                    $this->CommonTable('Invoice\Model\DeliveryNoteProductTable')->updateDeliveryNoteProductReturnQuantity($deliveryNoteProductID, $deliveryNoteProductQuantityUpdateData);

                    if ($p['pTax']) {
                        foreach ($p['pTax']['tL'] as $taxKey => $productTax) {
                            $returnProductTaxData = array(
                                'salesReturnProductID' => $returnProductID,
                                'taxID' => $taxKey,
                                'salesReturnProductTaxPrecentage' => $productTax['tP'],
                                'salesReturnProductTaxAmount' => $productTax['tA'] * $salesReturnCustomCurrencyRate
                            );
                            $returnProductTax = new ReturnsProductTax();
                            $returnProductTax->exchangeArray($returnProductTaxData);
                            $this->CommonTable('SalesReturnsProductTaxTable')->saveReturnsProductTax($returnProductTax);
                        }
                    }

                    if (!count($batchProducts) > 0) {
                        $itemReturnQty = $returnQty;
                        //add item in details for non serial and batch products
                        if ($multipleDlnCopyFlag == "false") {
                            $itemOutDetails = $this->CommonTable('Inventory\Model\ItemOutTable')->getItemOutDetailsForProductByDocument($origLocationProductID, 'Delivery Note', $deliData['deliveryNoteID']);
                        } else {
                            $getDeliveryNoteID = $this->CommonTable('Invoice\Model\DeliveryNoteProductTable')->getDeliveryNoteIDByDeliveryNoteProductID($deliveryNoteProductID)->current();

                            // need to get original locationProductID
                            if ($locationID != $getDeliveryNoteID['deliveryNoteLocationID']) {
                                 // get original deliveryNote location Product Details
                                $origLocationProduct = $this->CommonTable('Inventory\Model\LocationProductTable')->getLocationProduct($productID, $getDeliveryNoteID['deliveryNoteLocationID']);
                                $origLocationProductID = $origLocationProduct->locationProductID;
                            }
                            $itemOutDetails = $this->CommonTable('Inventory\Model\ItemOutTable')->getItemOutDetailsForProductByDocument($origLocationProductID, 'Delivery Note', $getDeliveryNoteID['deliveryNoteID']);
                        }
                        foreach (array_reverse($itemOutDetails) as $itemOutData) {
                            if ($itemReturnQty != 0) {
                                if ($itemOutData->itemOutQty > $itemOutData->itemOutReturnQty) {
                                    $itemInPreDetails = $this->CommonTable('Inventory\Model\ItemInTable')->getItemInDetails($itemOutData->itemOutItemInID);
                                    
                                    $unitPrice = $itemInPreDetails['itemInPrice'];
                                    $unitDiscount = $itemInPreDetails['itemInDiscount'];
                                    if($averageCostingFlag == 1){
                                        $unitPrice = $itemOutData->itemOutAverageCostingPrice;
                                        $unitDiscount = 0;
                                    }
                                    
                                    $leftQty = floatval($itemOutData->itemOutQty) - floatval($itemOutData->itemOutReturnQty);
                                    $itemInInsertQty = 0;
                                    $itemOutUpdateReturnQty = 0;
                                    if ($leftQty >= $itemReturnQty) {
                                        $itemInInsertQty = $itemReturnQty;
                                        $itemOutUpdateReturnQty = floatval($itemOutData->itemOutReturnQty) + floatval($itemReturnQty);
                                        $itemReturnQty = 0;
                                    } else {
                                        $itemInInsertQty = $leftQty;
                                        $itemOutUpdateReturnQty = $itemOutData->itemOutQty;
                                        $itemReturnQty = floatval($itemReturnQty) - floatval($leftQty);                                    
                                    }
                                    //hit data to the item in table with left qty
                                    $newItemInIndex = $this->CommonTable('Inventory\Model\ItemInTable')->getNewItemInIndex($itemInPreDetails['itemInIndexID']);
                                    $itemInData = array(
                                        'itemInIndex' => $newItemInIndex,
                                        'itemInDocumentType' => 'Sales Returns',
                                        'itemInDocumentID' => $returnID,
                                        'itemInLocationProductID' => $locationProductID,
                                        'itemInBatchID' => NULL,
                                        'itemInSerialID' => NULL,
                                        'itemInQty' => $itemInInsertQty,
                                        'itemInPrice' => $unitPrice,
                                        'itemInDiscount' => $unitDiscount,
                                        'itemInDateAndTime' => $this->getGMTDateTime(),
                                    );
                                    $itemInModel = new ItemIn();
                                    $itemInModel->exchangeArray($itemInData);
                                    $this->CommonTable('Inventory\Model\ItemInTable')->saveItemIn($itemInModel);
                                    //update item out return qty
                                    $this->CommonTable('Inventory\Model\ItemOutTable')->updateItemOutReturnQty($itemOutData->itemOutID, $itemOutUpdateReturnQty);
                                
                                    if($this->useAccounting == 1){
                                        if($averageCostingFlag == 1){
                                        //set gl accounts for the journal entry
                                            $productTotal = $itemInInsertQty * $itemOutData->itemOutAverageCostingPrice;
                                            if(isset($accountProduct[$pData['productInventoryAccountID']]['debitTotal'])){
                                                $accountProduct[$pData['productInventoryAccountID']]['debitTotal'] += $productTotal;
                                            }else{
                                                $accountProduct[$pData['productInventoryAccountID']]['debitTotal'] = $productTotal;
                                                $accountProduct[$pData['productInventoryAccountID']]['creditTotal'] = 0.00;
                                                $accountProduct[$pData['productInventoryAccountID']]['accountID'] = $pData['productInventoryAccountID'];
                                            }

                                            if(isset($accountProduct[$pData['productCOGSAccountID']]['creditTotal'])){
                                                $accountProduct[$pData['productCOGSAccountID']]['creditTotal'] += $productTotal;
                                            }else{
                                                $accountProduct[$pData['productCOGSAccountID']]['debitTotal'] = 0.00;
                                                $accountProduct[$pData['productCOGSAccountID']]['creditTotal'] =  $productTotal;
                                                $accountProduct[$pData['productCOGSAccountID']]['accountID'] = $pData['productCOGSAccountID'];
                                            }
                                        }else{
                                            $productTotal = $itemInInsertQty * ($itemInPreDetails['itemInPrice'] - $itemInPreDetails['itemInDiscount']);
                                            if(isset($accountProduct[$pData['productInventoryAccountID']]['debitTotal'])){
                                                $accountProduct[$pData['productInventoryAccountID']]['debitTotal'] += $productTotal;
                                            }else{
                                                $accountProduct[$pData['productInventoryAccountID']]['debitTotal'] = $productTotal;
                                                $accountProduct[$pData['productInventoryAccountID']]['creditTotal'] = 0.00;
                                                $accountProduct[$pData['productInventoryAccountID']]['accountID'] = $pData['productInventoryAccountID'];
                                            }

                                            if(isset($accountProduct[$pData['productCOGSAccountID']]['creditTotal'])){
                                                $accountProduct[$pData['productCOGSAccountID']]['creditTotal'] += $productTotal;
                                            }else{
                                                $accountProduct[$pData['productCOGSAccountID']]['debitTotal'] = 0.00;
                                                $accountProduct[$pData['productCOGSAccountID']]['creditTotal'] =  $productTotal;
                                                $accountProduct[$pData['productCOGSAccountID']]['accountID'] = $pData['productCOGSAccountID'];
                                            }
                                        }
                                    }
                                }
                            } else {
                                break;
                            }
                        }
                        if ($itemReturnQty != 0) {

                            $returnData = [
		                		'status' => false,
		                		'msg' => 'ERR_DLN_NOT_ENOUGH_QTY'
		                	];
		                	return $returnData;
                        }
                    }

                    if (count($batchProducts) > 0) {
                        foreach ($batchProducts as $batchKey => $batchValue) {

                            if ($batchValue['serialID']) {
                                if ((float)$deliveryNoteRemainQty < count($batchProducts)) {

                                    $returnData = [
				                		'status' => false,
				                		'msg' => 'ERR_DLN_NOT_ENOUGH_SERIAL_ITEM_QTY'
				                	];
				                	return $returnData;
                                }
                            }
                            
                            $result = $this->saveReturnSubProductData($batchValue, $returnProductID);
                            if ($batchValue['qtyByBase'] != 0 && $batchValue['batchID']) {
                                if ((float)$deliveryNoteRemainQty < (float)$batchValue['qtyByBase']) {
                                    
                                    $returnData = [
				                		'status' => false,
				                		'msg' => 'ERR_DLN_NOT_ENOUGH_QTY'
				                	];
				                	return $returnData;
                                }

                                if ($crossLocationFlag) {
                                    
                                    // insert new batch record with new locationProduct ID
                                    $bPData = array(
                                        'locationProductID' => $locationProductID,
                                        'productBatchCode' => $delBatchDetails[$batchValue['batchID']]['PBC'],
                                        'productBatchExpiryDate' => ($delBatchDetails[$batchValue['batchID']]['PBExpD'] != '') ? $this->convertDateToStandardFormat($delBatchDetails[$batchValue['batchID']]['PBExpD']) : null,
                                        'productBatchWarrantyPeriod' => $delBatchDetails[$batchValue['batchID']]['PBWoD'],
                                        'productBatchManufactureDate' => ($delBatchDetails[$batchValue['batchID']]['PBManD'] != '') ? $this->convertDateToStandardFormat($delBatchDetails[$batchValue['batchID']]['PBManD']) : null,
                                        'productBatchQuantity' => $batchValue['qtyByBase'],
                                        'productBatchPrice' => $delBatchDetails[$batchValue['batchID']]['BtPrice'],
                                    );
                                    $batchProduct = new ProductBatch();
                                    $batchProduct->exchangeArray($bPData);

                                    $insertedBID = $this->CommonTable('Inventory\Model\ProductBatchTable')->saveBatchProduct($batchProduct);
                                } else {
                          
                                    $batchProduct = $this->CommonTable('Inventory\Model\ProductBatchTable')->getBatchProducts($batchValue['batchID']);
                                    $productBatchQuentity = $batchProduct->productBatchQuantity + $batchValue['qtyByBase'];
                                    $productBatchQty = array(
                                        'productBatchQuantity' => $productBatchQuentity,
                                    );
                                    $this->CommonTable('Inventory\Model\ProductBatchTable')->updateProductBatchQuantity($batchValue['batchID'], $productBatchQty);
                                    
                                }
                                if ($batchValue['serialID']) {
                                    //Add details to item in table for batch and serial products
                                    $itemInDetails = $this->CommonTable('Inventory\Model\ItemInTable')->getBatchSerialProductDetails($origLocationProductID, $batchValue['batchID'], $batchValue['serialID']);

                                    if ($multipleDlnCopyFlag == "false") {
                                        $itemOutDetails = $this->CommonTable('Inventory\Model\ItemOutTable')->getBatchSerialItemOutDetails($batchValue['serialID'],$batchValue['batchID'], 'Delivery Note',$deliData['deliveryNoteID']);
                                    } else {
                                        $getDeliveryNoteID = $this->CommonTable('Invoice\Model\DeliveryNoteProductTable')->getDeliveryNoteIDByDeliveryNoteProductID($deliveryNoteProductID)->current();
                                        $itemOutDetails = $this->CommonTable('Inventory\Model\ItemOutTable')->getBatchSerialItemOutDetails($batchValue['serialID'],$batchValue['batchID'], 'Delivery Note',$getDeliveryNoteID['deliveryNoteID']);
                                    }
                                    
                                    $unitPrice = $itemInDetails['itemInPrice'];
                                    $unitDiscount = $itemInDetails['itemInDiscount'];
                                    if($averageCostingFlag == 1){
                                        $unitPrice = $itemOutDetails->itemOutAverageCostingPrice;
                                        $unitDiscount = 0;
                                    }

                                    $newItemInIndex = $this->CommonTable('Inventory\Model\ItemInTable')->getNewItemInIndex($itemInDetails['itemInID']);
                                    if ($crossLocationFlag) {
                                        $newItemInIndex = null;
                                    }
                                    $itemInData = array(
                                        'itemInIndex' => $newItemInIndex,
                                        'itemInDocumentType' => 'Sales Returns',
                                        'itemInDocumentID' => $returnID,
                                        'itemInLocationProductID' => $locationProductID,
                                        'itemInBatchID' => $batchValue['batchID'],
                                        'itemInSerialID' => $batchValue['serialID'],
                                        'itemInQty' => 1,
                                        'itemInPrice' => $unitPrice,
                                        'itemInDiscount' => $unitDiscount,
                                        'itemInDateAndTime' => $this->getGMTDateTime(),
                                    );
                                    $itemInModel = new ItemIn();
                                    $itemInModel->exchangeArray($itemInData);
                                    $this->CommonTable('Inventory\Model\ItemInTable')->saveItemIn($itemInModel);

                                    if ($crossLocationFlag) { 
                                        $this->CommonTable('Inventory\Model\ItemOutTable')->updateItemOutReturnQty($itemOutDetails->itemOutID,'1');
                                    }
                                    
                                    if($this->useAccounting == 1){
                                        if($averageCostingFlag == 1){
                                        //set gl accounts for the journal entry
                                            $productTotal = 1 * $itemOutDetails->itemOutAverageCostingPrice;
                                            if(isset($accountProduct[$pData['productInventoryAccountID']]['debitTotal'])){
                                                $accountProduct[$pData['productInventoryAccountID']]['debitTotal'] += $productTotal;
                                            }else{
                                                $accountProduct[$pData['productInventoryAccountID']]['debitTotal'] = $productTotal;
                                                $accountProduct[$pData['productInventoryAccountID']]['creditTotal'] = 0.00;
                                                $accountProduct[$pData['productInventoryAccountID']]['accountID'] = $pData['productInventoryAccountID'];
                                            }

                                            if(isset($accountProduct[$pData['productCOGSAccountID']]['creditTotal'])){
                                                $accountProduct[$pData['productCOGSAccountID']]['creditTotal'] += $productTotal;
                                            }else{
                                                $accountProduct[$pData['productCOGSAccountID']]['debitTotal'] = 0.00;
                                                $accountProduct[$pData['productCOGSAccountID']]['creditTotal'] =  $productTotal;
                                                $accountProduct[$pData['productCOGSAccountID']]['accountID'] = $pData['productCOGSAccountID'];
                                            }
                                        }else{
                                            $productTotal = 1 * ($itemInDetails['itemInPrice'] - $itemInDetails['itemInDiscount']);
                                            if(isset($accountProduct[$pData['productInventoryAccountID']]['debitTotal'])){
                                                $accountProduct[$pData['productInventoryAccountID']]['debitTotal'] += $productTotal;
                                            }else{
                                                $accountProduct[$pData['productInventoryAccountID']]['debitTotal'] = $productTotal;
                                                $accountProduct[$pData['productInventoryAccountID']]['creditTotal'] = 0.00;
                                                $accountProduct[$pData['productInventoryAccountID']]['accountID'] = $pData['productInventoryAccountID'];
                                            }

                                            if(isset($accountProduct[$pData['productCOGSAccountID']]['creditTotal'])){
                                                $accountProduct[$pData['productCOGSAccountID']]['creditTotal'] += $productTotal;
                                            }else{
                                                $accountProduct[$pData['productCOGSAccountID']]['debitTotal'] = 0.00;
                                                $accountProduct[$pData['productCOGSAccountID']]['creditTotal'] =  $productTotal;
                                                $accountProduct[$pData['productCOGSAccountID']]['accountID'] = $pData['productCOGSAccountID'];
                                            }
                                        }
                                    }

                                    ///////////////////
                                } else {
                                    //Add details to item in table for batch products
                                    $itemInDetails = $this->CommonTable('Inventory\Model\ItemInTable')->getBatchProductDetails($origLocationProductID, $batchValue['batchID']);
                                    if ($multipleDlnCopyFlag == "false") {
                                        $itemOutDetails = $this->CommonTable('Inventory\Model\ItemOutTable')->getBatchItemOutDetails($batchValue['batchID'], 'Delivery Note',$deliData['deliveryNoteID']);
                                    } else {
                                        $getDeliveryNoteID = $this->CommonTable('Invoice\Model\DeliveryNoteProductTable')->getDeliveryNoteIDByDeliveryNoteProductID($deliveryNoteProductID)->current();
                                        $itemOutDetails = $this->CommonTable('Inventory\Model\ItemOutTable')->getBatchItemOutDetails($batchValue['batchID'], 'Delivery Note',$getDeliveryNoteID['deliveryNoteID']);
                                    }
                                    
                                    $unitPrice = $itemInDetails['itemInPrice'];
                                    $unitDiscount = $itemInDetails['itemInDiscount'];
                                    if($averageCostingFlag == 1){
                                        $unitPrice = $itemOutDetails->itemOutAverageCostingPrice;
                                        $unitDiscount = 0;
                                    }

                                    $newItemInIndex = $this->CommonTable('Inventory\Model\ItemInTable')->getNewItemInIndex($itemInDetails['itemInID']);
                                    if ($crossLocationFlag) {
                                        $newItemInIndex = null;
                                    }
                                    $itemInData = array(
                                        'itemInIndex' => $newItemInIndex,
                                        'itemInDocumentType' => 'Sales Returns',
                                        'itemInDocumentID' => $returnID,
                                        'itemInLocationProductID' => $locationProductID,
                                        'itemInBatchID' => $batchValue['batchID'],
                                        'itemInSerialID' => NULL,
                                        'itemInQty' => $batchValue['qtyByBase'],
                                        'itemInPrice' => $unitPrice,
                                        'itemInDiscount' =>  $unitDiscount,
                                        'itemInDateAndTime' => $this->getGMTDateTime(),
                                    );
                                    $itemInModel = new ItemIn();
                                    $itemInModel->exchangeArray($itemInData);
                                    $this->CommonTable('Inventory\Model\ItemInTable')->saveItemIn($itemInModel);

                                    if ($crossLocationFlag) {
                                        $this->CommonTable('Inventory\Model\ItemOutTable')->updateItemOutReturnQty($itemOutDetails->itemOutID,$batchValue['qtyByBase']);
                                    }

                                    if($this->useAccounting == 1){
                                        if($averageCostingFlag == 1){
                                        //set gl accounts for the journal entry
                                            $productTotal = $batchValue['qtyByBase'] * $itemOutDetails->itemOutAverageCostingPrice;
                                            if(isset($accountProduct[$pData['productInventoryAccountID']]['debitTotal'])){
                                                $accountProduct[$pData['productInventoryAccountID']]['debitTotal'] += $productTotal;
                                            }else{
                                                $accountProduct[$pData['productInventoryAccountID']]['debitTotal'] = $productTotal;
                                                $accountProduct[$pData['productInventoryAccountID']]['creditTotal'] = 0.00;
                                                $accountProduct[$pData['productInventoryAccountID']]['accountID'] = $pData['productInventoryAccountID'];
                                            }

                                            if(isset($accountProduct[$pData['productCOGSAccountID']]['creditTotal'])){
                                                $accountProduct[$pData['productCOGSAccountID']]['creditTotal'] += $productTotal;
                                            }else{
                                                $accountProduct[$pData['productCOGSAccountID']]['debitTotal'] = 0.00;
                                                $accountProduct[$pData['productCOGSAccountID']]['creditTotal'] =  $productTotal;
                                                $accountProduct[$pData['productCOGSAccountID']]['accountID'] = $pData['productCOGSAccountID'];
                                            }
                                        }else{
                                            $productTotal = $batchValue['qtyByBase'] * ($itemInDetails['itemInPrice'] - $itemInDetails['itemInDiscount']);
                                            if(isset($accountProduct[$pData['productInventoryAccountID']]['debitTotal'])){
                                                $accountProduct[$pData['productInventoryAccountID']]['debitTotal'] += $productTotal;
                                            }else{
                                                $accountProduct[$pData['productInventoryAccountID']]['debitTotal'] = $productTotal;
                                                $accountProduct[$pData['productInventoryAccountID']]['creditTotal'] = 0.00;
                                                $accountProduct[$pData['productInventoryAccountID']]['accountID'] = $pData['productInventoryAccountID'];
                                            }

                                            if(isset($accountProduct[$pData['productCOGSAccountID']]['creditTotal'])){
                                                $accountProduct[$pData['productCOGSAccountID']]['creditTotal'] += $productTotal;
                                            }else{
                                                $accountProduct[$pData['productCOGSAccountID']]['debitTotal'] = 0.00;
                                                $accountProduct[$pData['productCOGSAccountID']]['creditTotal'] =  $productTotal;
                                                $accountProduct[$pData['productCOGSAccountID']]['accountID'] = $pData['productCOGSAccountID'];
                                            }
                                        }
                                    }

                                    ///////////////////
                                }
                            } else if ($batchValue['qtyByBase'] != 0 && $batchValue['serialID']) {
                                //Add details to item in table for serial products
                                $itemInDetails = $this->CommonTable('Inventory\Model\ItemInTable')->getSerialProductDetails($origLocationProductID, $batchValue['serialID']);
                                if ($multipleDlnCopyFlag == "false") {
                                    $itemOutDetails = $this->CommonTable('Inventory\Model\ItemOutTable')->getSerialItemOutDetails($batchValue['serialID'], 'Delivery Note',$deliData['deliveryNoteID']);
                                } else {
                                    $getDeliveryNoteID = $this->CommonTable('Invoice\Model\DeliveryNoteProductTable')->getDeliveryNoteIDByDeliveryNoteProductID($deliveryNoteProductID)->current();
                                    $itemOutDetails = $this->CommonTable('Inventory\Model\ItemOutTable')->getSerialItemOutDetails($batchValue['serialID'], 'Delivery Note',$getDeliveryNoteID['deliveryNoteID']);
                                }
                                
                                $unitPrice = $itemInDetails['itemInPrice'];
                                $unitDiscount = $itemInDetails['itemInDiscount'];
                                if($averageCostingFlag == 1){
                                    $unitPrice = $itemOutDetails->itemOutAverageCostingPrice;
                                    $unitDiscount = 0;
                                }

                                $newItemInIndex = $this->CommonTable('Inventory\Model\ItemInTable')->getNewItemInIndex($itemInDetails['itemInID']);
                                if ($crossLocationFlag) {
                                    $newItemInIndex = null;
                                }
                                $itemInData = array(
                                    'itemInIndex' => $newItemInIndex,
                                    'itemInDocumentType' => 'Sales Returns',
                                    'itemInDocumentID' => $returnID,
                                    'itemInLocationProductID' => $locationProductID,
                                    'itemInBatchID' => NULL,
                                    'itemInSerialID' => $batchValue['serialID'],
                                    'itemInQty' => 1,
                                    'itemInPrice' => $unitPrice,
                                    'itemInDiscount' => $unitDiscount,
                                    'itemInDateAndTime' => $this->getGMTDateTime(),
                                );
                                $itemInModel = new ItemIn();
                                $itemInModel->exchangeArray($itemInData);
                                $this->CommonTable('Inventory\Model\ItemInTable')->saveItemIn($itemInModel);
                                if (!$crossLocationFlag) {
                                    $this->CommonTable('Inventory\Model\ItemOutTable')->updateItemOutReturnQty($itemOutDetails->itemOutID,'1');
                                }
                                
                                if($this->useAccounting == 1){
                                    if($averageCostingFlag == 1){
                                        //set gl accounts for the journal entry
                                        $productTotal = 1 * $itemOutDetails->itemOutAverageCostingPrice;
                                        if(isset($accountProduct[$pData['productInventoryAccountID']]['debitTotal'])){
                                            $accountProduct[$pData['productInventoryAccountID']]['debitTotal'] += $productTotal;
                                        }else{
                                            $accountProduct[$pData['productInventoryAccountID']]['debitTotal'] = $productTotal;
                                            $accountProduct[$pData['productInventoryAccountID']]['creditTotal'] = 0.00;
                                            $accountProduct[$pData['productInventoryAccountID']]['accountID'] = $pData['productInventoryAccountID'];
                                        }

                                        if(isset($accountProduct[$pData['productCOGSAccountID']]['creditTotal'])){
                                            $accountProduct[$pData['productCOGSAccountID']]['creditTotal'] += $productTotal;
                                        }else{
                                            $accountProduct[$pData['productCOGSAccountID']]['debitTotal'] = 0.00;
                                            $accountProduct[$pData['productCOGSAccountID']]['creditTotal'] =  $productTotal;
                                            $accountProduct[$pData['productCOGSAccountID']]['accountID'] = $pData['productCOGSAccountID'];
                                        }
                                    }else{
                                        $productTotal = 1* ($itemInDetails['itemInPrice'] - $itemInDetails['itemInDiscount']);
                                        if(isset($accountProduct[$pData['productInventoryAccountID']]['debitTotal'])){
                                            $accountProduct[$pData['productInventoryAccountID']]['debitTotal'] += $productTotal;
                                        }else{
                                            $accountProduct[$pData['productInventoryAccountID']]['debitTotal'] = $productTotal;
                                            $accountProduct[$pData['productInventoryAccountID']]['creditTotal'] = 0.00;
                                            $accountProduct[$pData['productInventoryAccountID']]['accountID'] = $pData['productInventoryAccountID'];
                                        }

                                        if(isset($accountProduct[$pData['productCOGSAccountID']]['creditTotal'])){
                                            $accountProduct[$pData['productCOGSAccountID']]['creditTotal'] += $productTotal;
                                        }else{
                                            $accountProduct[$pData['productCOGSAccountID']]['debitTotal'] = 0.00;
                                            $accountProduct[$pData['productCOGSAccountID']]['creditTotal'] =  $productTotal;
                                            $accountProduct[$pData['productCOGSAccountID']]['accountID'] = $pData['productCOGSAccountID'];
                                        }
                                    }
                                }
                                ///////////////////////////////////
                            }
                            if ($batchValue['qtyByBase'] != 0 && $batchValue['serialID']) {
                                if ($crossLocationFlag) {
                                    // Update product Serial table locationProductID
                                    $serialProductData = array(
                                        'locationProductID' => $locationProductID,
                                        'productSerialSold' => '0',
                                    );

                                } else {
                                    $serialProductData = array(
                                        'productSerialSold' => '0',
                                    );
                                    
                                }
                                $this->CommonTable('Inventory\Model\ProductSerialTable')->updateProductSerialData($serialProductData, $batchValue['serialID']);
                            }
                        }
                    } else {
//                    $this->saveTransferProductData(array('batchID' => 0, 'serialID' => 0, 'qtyByBase' => $p['transferQuantity']['qtyByBase']), $trasferID, $productID);
                    }

                    $itemInData = $this->CommonTable('Inventory\Model\ItemInTable')->getItemInProductDetails($locationProductID);
                    $locationPrTotalQty = 0;
                    $locationPrTotalPrice = 0;
                    $newItemInIds = array();
                    if(count($itemInData) > 0){
                        foreach ($itemInData as $key => $value) {
                            if($value->itemInID > $locationProductLastItemInID){
                                $newItemInIds[] = $value->itemInID; 
                                $remainQty = $value->itemInQty - $value->itemInSoldQty;
                                if($remainQty > 0){
                                    $itemInPrice = $remainQty*$value->itemInPrice;
                                    $itemInDiscount = $remainQty*$value->itemInDiscount;
                                    $locationPrTotalQty += $remainQty;
                                    $locationPrTotalPrice += $itemInPrice - $itemInDiscount; 
                                }
                                $locationProductLastItemInID = $value->itemInID; 
                            }
                        }
                    }
                    $locationPrTotalQty += $locationProductQuantity;
                    $locationPrTotalPrice += $locationProductQuantity*$locationProductAverageCostingPrice;

                    if($locationPrTotalQty > 0){
                        $newAverageCostinPrice = $locationPrTotalPrice/$locationPrTotalQty;
                        $lpdata = array(
                            'locationProductAverageCostingPrice' => $newAverageCostinPrice,
                            'locationProductLastItemInID' => $locationProductLastItemInID,
                        );
                        $this->CommonTable('Inventory\Model\LocationProductTable')->updateLocationProductByArray($lpdata, $locationProductID);
                        
                        foreach ($newItemInIds as $inID) {
                            $intemInData = array(
                                'itemInAverageCostingPrice' =>  $newAverageCostinPrice,
                                );
                            $result = $this->CommonTable('Inventory\Model\ItemInTable')->updateItemInDataByItemInId($intemInData, $inID);
                        }
                    }
                }

                if ($multipleDlnCopyFlag == "false") {
    //                if all products are returned change the deliverynote status
                    if (trim($deliData['deliveryNoteID']) != "") {
                        $this->_updateDeliveryNoteStatusByDeliveryNoteID($deliData['deliveryNoteID']);
                    }
                } else {
                    foreach ($deliData['deliveryNoteID'] as $key => $value) {
                        $this->_updateDeliveryNoteStatusByDeliveryNoteID($value);
                    }
                }
      
                //      call product updated event
                $productIDs = array_map(function($element) {
                    if ($multipleDlnCopyFlag == "false") {
                        return $element['productID'];
                    } else {
                        return explode("_", $element['productID'])[1];
                    }
                }, $products);

                $eventParameter = ['productIDs' => $productIDs, 'locationIDs' => [$locationID]];
                $this->getEventManager()->trigger('productQuantityUpdated', $this, $eventParameter);

                if ($this->useAccounting == 1) {
                    //create data array for the journal entry save.
                    $i=0;
                    $journalEntryAccounts = array();

                    foreach ($accountProduct as $key => $value) {
                        $journalEntryAccounts[$i]['fAccountsIncID'] = $i;
                        $journalEntryAccounts[$i]['financeAccountsID'] = $value['accountID'];
                        $journalEntryAccounts[$i]['financeGroupsID'] = '';
                        $journalEntryAccounts[$i]['journalEntryAccountsDebitAmount'] = $value['debitTotal'];
                        $journalEntryAccounts[$i]['journalEntryAccountsCreditAmount'] = $value['creditTotal'];
                        $journalEntryAccounts[$i]['journalEntryAccountsMemo'] = 'Created By Sales Return '.$returnCode.'.';
                        $i++;
                    }

                    //get journal entry reference number.
                    $jeresult = $this->getReferenceNoForLocation('30', $locationOut);
                    $jelocationReferenceID = $jeresult['locRefID'];
                    $JournalEntryCode = $this->getReferenceNumber($jelocationReferenceID);

                    $journalEntryData = array(
                        'journalEntryAccounts' => $journalEntryAccounts,
                        'journalEntryDate' => $date,
                        'journalEntryCode' => $JournalEntryCode,
                        'journalEntryTypeID' => '',
                        'journalEntryIsReverse' => 0,
                        'journalEntryComment' => 'Journal Entry is posted when create Sales Return '.$returnCode.'.',
                        'documentTypeID' => 5,
                        'journalEntryDocumentID' => $returnID,
                        'ignoreBudgetLimit' => $ignoreBudgetLimit,
                    );

                    $resultData = $this->saveJournalEntry($journalEntryData);

                    $jEDocStatusUpdate = $this->updateDocumentStatusInJournalEntry($returnID,5, 4);
                    if(!$jEDocStatusUpdate['status']){

                        $returnData = [
	                		'status' => false,
	                		'msg' => $jEDocStatusUpdate['msg']
	                	];
	                	return $returnData;
                    }

                    if($resultData['status']){
                        if ($multipleDlnCopyFlag == "false") {
                            $jEDimensionData = $this->CommonTable('Accounting\Model\JournalEntryTable')->getJournalEntryDataWithDimensionsByDocumentTypeIDAndDocumentID(4,$deliveryNoteID);
                            $dimensionData = [];
                            foreach ($jEDimensionData as $value) {
                                if (!is_null($value['journalEntryID'])) {
                                    $temp = [];
                                    $temp['dimensionTypeId'] = $value['dimensionType'];
                                    $temp['dimensionValueId'] = $value['dimensionValueID'];
                                    $dimensionData[$creditNoteCode][] = $temp;
                                }
                            }
                            if (!empty($dimensionData)) {
                                $saveRes = $this->saveDimensionsForJournalEntry($dimensionData[$creditNoteCode], $resultData['data']['journalEntryID'], $ignoreBudgetLimit, $journalEntryData['journalEntryAccounts'], $journalEntryData['journalEntryDate']);
                                if(!$saveRes['status']){

                                    $returnData = [
				                		'status' => false,
				                		'msg' => $saveRes['msg']
				                	];
				                	return $returnData;
                                }   
                            }
                        } else {
                            if (!empty($dimensionData)) {
                                $saveRes = $this->saveDimensionsForJournalEntry($dimensionData[$returnCode], $resultData['data']['journalEntryID'], $ignoreBudgetLimit, $journalEntryData['journalEntryAccounts'], $journalEntryData['journalEntryDate']);
                                if(!$saveRes['status']){

                                    $returnData = [
				                		'status' => false,
				                		'msg' => $saveRes['msg']
				                	];
				                	return $returnData;
                                }   
                            }
                        }
                        
                        $this->setLogMessage($customCurrencySymbol." ". $totalPrice. ' amount return Product details were successfully added - ' . $returnCode);

                        $returnData = [
		            		'status' => true,
		            		'msg' => 'SUCC_RETURNAPI_PRODUCTDETAIL_ADD'
		            	];

		            	return $returnData;
                    }else{

                        $returnData = [
	                		'status' => false,
	                		'msg' => $resultData['msg']
	                	];
	                	return $returnData;
                    }
                }else{
                    
                    $this->setLogMessage($customCurrencySymbol." ". $totalPrice. ' amount return Product details were successfully added - ' . $returnCode);


                    $returnData = [
	            		'status' => true,
	            		'msg' => 'SUCC_RETURNAPI_PRODUCTDETAIL_ADD'
	            	];
                }
                return $returnData;

            } else {

                $returnData = [
            		'status' => false,
            		'msg' => 'ERR_DELINOTEAPI_VALID_CUST'
            	];
            	return $returnData;
            }
        } else {

            $returnData = [
        		'status' => false,
        		'msg' => 'ERR_DELINOTEAPI_VALID_CUST'
        	];
            return $returnData;
        }
    }

    private function _checkDeliveryNoteProductsByDeliveryNoteID($deliveryNoteID){

        $deliveryProductsArray = $this->CommonTable('Invoice\Model\DeliveryNoteProductTable')->getDeliveryNoteProductByDeliveryNoteID($deliveryNoteID, true);

        $dparray;
        $dparrayQty;
        $iparray;
        $iparrayQty;
        $checkProduct = 0;

        foreach ($deliveryProductsArray as $dp) {
            $dp = (object) $dp;
            $dparray[$dp->productID] = $dp->productID;
            $dparrayQty[$dp->productID] = $dp->deliveryNoteProductQuantity;
        }
        $data = $this->CommonTable('SalesReturnsProductTable')->getSalesReturnProductsByDelvieryNoteID($deliveryNoteID);

        foreach ($data as $ip) {
            $ip = (object) $ip;
            $iparray[$ip->productID] = $ip->productID;
            $iparrayQty[$ip->productID]+=$ip->salesReturnProductQuantity;
        }

        foreach ($dparrayQty as $key => $Pdata) {
            if ($iparrayQty[$key] != $Pdata) {
                $checkProduct = 1;
            }
        }

        return $checkProduct;
    }

    private function _updateDeliveryNoteStatusByDeliveryNoteID($deliveryNoteID)
    {
        $checkProduct = $this->_checkDeliveryNoteProductsByDeliveryNoteID($deliveryNoteID);
        if ($checkProduct == "0") {
//            update status
            $closeStatusID = $this->getStatusID('closed');
            $this->CommonTable('Invoice\Model\DeliveryNoteTable')->updateDeliveryNoteStatus(
                    $deliveryNoteID, $closeStatusID);
        }
    }

       
}
