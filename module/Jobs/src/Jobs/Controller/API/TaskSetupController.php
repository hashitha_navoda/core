<?php

namespace Jobs\Controller\API;

use Zend\View\Model\ViewModel;
use Core\Controller\CoreController;
use Zend\Session\Container;
use Jobs\Model\JobType;

class TaskSetupController extends CoreController
{

    //job type save and update function
    public function addAction()
    {
        $request = $this->getRequest();
        if (!$request->isPost()) {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_DATA_SET');
            return $this->JSONRespond();
        }
        
        $this->beginTransaction();

        $respond = $this->getService('TaskSetupService')->saveTask($request->getPost());

        if (!$respond['status']) {
            $this->rollback();
            $this->status = false;
            $this->msg = $this->getMessage($respond['msg']);
            return $this->JSONRespond();    
        }

        $this->commit();
        $this->status = true;
        $this->data = $respond['data'];
        $this->msg = $this->getMessage($respond['msg']);
        $this->flashMessenger()->addMessage($this->msg);
        return $this->JSONRespond();
    }

    //get job types by job type search key
    public function getTaskBySearchKeyAction()
    {
       $request = $this->getRequest();
        if (!$request->isPost()) {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_DATA_SET');
            return $this->JSONRespond();
        }

        $respond = $this->getService('TaskSetupService')->getTaskBySearchKey($request->getPost(), $this->params()->fromRoute('param1', 1));

        $this->status = true;
        $this->html = $respond;
        return $this->JSONRespondHtml();
    }

    //delete job types by job type id
    public function deleteTaskByTaskIDAction()
    {
        $request = $this->getRequest();
        if (!$request->isPost()) {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_DATA_SET');
            return $this->JSONRespond();
        }

        $respond = $this->getService('TaskSetupService')->deleteTaskByTaskID($request->getPost());

        if ($respond['status']) {
            $this->msg = $this->getMessage($respond['msg'],$respond['data']);
            $this->flashMessenger()->addMessage($this->msg);
        } else {
            $this->msg = $this->getMessage($respond['msg']);
        }

        $this->status = $respond['status'];
        $this->data = $respond['data'];
        return $this->JSONRespond();
    }

    /**
     * This function is used to search tasks for dropdown
     */
    public function searchTasksForDropdownAction()
    {
        $request = $this->getRequest();
        if (!$request->isPost()) {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_DATA_SET');
            return $this->JSONRespond();
        }

        $respond = $this->getService('TaskSetupService')->searchTasksForDropdown($request->getPost());

        $this->data = array('list' => $respond);
        return $this->JSONRespond();
    }
}
