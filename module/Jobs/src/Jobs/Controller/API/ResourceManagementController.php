<?php

namespace Jobs\Controller\API;

use Zend\View\Model\ViewModel;
use Core\Controller\CoreController;
use Zend\Session\Container;
use Zend\View\Model\JsonModel;

class ResourceManagementController extends CoreController
{
	/**
	 * This function is used to load jobProducts
	 * @param JSON Request
	 * @return JSON Respond
	 */
	public function loadJobProductsAction() {
        $request = $this->getRequest();
        if (!$request->isPost()) {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_DATA_SET');
            return $this->JSONRespond();
        }
        $respond = $this->getService('ResourceManagementService')->loadJobProductsByJobId($request->getPost());
        $this->status = $respond['status'];
        $this->msg = $this->getMessage($respond['msg']);
        $this->data = $respond['data'];
        return $this->JSONRespond();
    }

    /**
     * This function is used to update jobProducts
     * @param JSON Request
     * @return JSON Respond
     */
    public function updateAction() {
        $request = $this->getRequest();
        if (!$request->isPost()) {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_DATA_SET');
            return $this->JSONRespond();
        }
        
        $this->beginTransaction();
        
        $respond = $this->getService('ResourceManagementService')->update($request->getPost());
        
        if (!$respond['status']) {
            $this->rollback();
            $this->status = false;
            $this->msg = $this->getMessage($respond['msg']);
            return $this->JSONRespond();    
        }

        $this->commit();
        $this->status = true;
        $this->data = $respond['data'];
        $this->msg = $this->getMessage($respond['msg']);
        $this->flashMessenger()->addMessage($this->msg);
        return $this->JSONRespond();
    }
}
