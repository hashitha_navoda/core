<?php

namespace Jobs\Controller\API;

use Core\Controller\CoreController;
use Jobs\Model\TaskCard;

class ServiceJobDashboardController extends CoreController {

	// this function use to get job related services
	public function getJobRelatedServicesAction() {
		$request = $this->getRequest();
		if (!$request->isPost()) {
			$this->status = false;
			$this->msg = $this->getMessage('ERR_DATA_SET');
			return $this->JSONRespond();
		}

		$this->beginTransaction();
		$respond = $this->getService('JobService')->getJobRelatedServicesByJobId($request->getPost());

		if (!$respond['status']) {
			$this->status = false;
            $this->msg = $this->getMessage($respond['msg']);
            $this->rollback();
            return $this->JSONRespond();
		}

		$this->status = true;
		$this->html = $respond['view'];
		$this->data = $respond['data'];
        return $this->JSONRespondHtml();
	}

	public function getJobRelatedMaterialsAction() {
		$request = $this->getRequest();
		if (!$request->isPost()) {
			$this->status = false;
			$this->msg = $this->getMessage('ERR_DATA_SET');
			return $this->JSONRespond();
		}

		$respond = $this->getService('JobService')->getJobRelatedMaterials($request->getPost());

		$this->status = true;
		$this->data = $respond;
        return $this->JSONRespond();
	}

	public function getJobVehicleRelatedStatusAction() {
		$request = $this->getRequest();
		if (!$request->isPost()) {
			$this->status = false;
			$this->msg = $this->getMessage('ERR_DATA_SET');
			return $this->JSONRespond();
		}

		$this->beginTransaction();
		$respond = $this->getService('JobDashboardService')->getJobVehicleRelatedStatus($request->getPost());

		if (!$respond['status']) {
			$this->status = false;
            $this->msg = $this->getMessage($respond['msg']);
            $this->rollback();
            return $this->JSONRespond();
		}

		$this->status = true;
		$this->html = $respond['view'];
		$this->data = $respond['data'];
        return $this->JSONRespondHtml();
	}

	public function getJobVehicleRelatedRemarksAction() {
		$request = $this->getRequest();
		if (!$request->isPost()) {
			$this->status = false;
			$this->msg = $this->getMessage('ERR_DATA_SET');
			return $this->JSONRespond();
		}

		$this->beginTransaction();
		$respond = $this->getService('JobDashboardService')->getJobVehicleRelatedRemarks($request->getPost());

		if (!$respond['status']) {
			$this->status = false;
            $this->msg = $this->getMessage($respond['msg']);
            $this->rollback();
            return $this->JSONRespond();
		}

		$this->status = true;
		$this->html = $respond['view'];
		$this->data = $respond['data'];
        return $this->JSONRespondHtml();
	}

	// this function use to start job service
	public function startJobRelatedServiceAction() {

		$request = $this->getRequest();
		if (!$request->isPost()) {
			$this->status = false;
			$this->msg = $this->getMessage('ERR_DATA_SET');
			return $this->JSONRespond();
		}
		$isFromDashboard = (!isset($request->getPost()['isFromDashboard'])) ? false : true;

		$this->beginTransaction();
		$userLocation = $this->user_session->userActiveLocation['locationID'];
		$respond = $this->getService('JobDashboardService')->startJobRelatedServiceByJobTaskID($request->getPost(), $this->userID, $userLocation);

		if (!$respond['status']) {
			$this->status = false;
            $this->msg = $this->getMessage($respond['msg']);
            $this->rollback();
            return $this->JSONRespond();
		}

		$this->commit();
		if (!$isFromDashboard) {
			$this->status = true;
			$this->data = $respond['data'];
			return $this->JSONRespond();
		} else {

			$this->status = true;
			$this->html = $respond['view'];
			$this->data = $respond['data'];
	        return $this->JSONRespondHtml();
		}

	}

	public function saveVehicleStatusAction() {

		$request = $this->getRequest();
		if (!$request->isPost()) {
			$this->status = false;
			$this->msg = $this->getMessage('ERR_DATA_SET');
			return $this->JSONRespond();
		}

		$this->beginTransaction();
		$respond = $this->getService('JobDashboardService')->saveVehicleStatus($request->getPost(),$this->userID);

		if (!$respond['status']) {
			$this->status = false;
            $this->msg = $this->getMessage($respond['msg']);
            $this->rollback();
            return $this->JSONRespond();
		}

		$this->commit();
		$this->status = true;
		$this->data = $respond['data'];
		$this->msg = $this->getMessage($respond['msg']);
		return $this->JSONRespond();
	}

	public function saveVehicleRemarkAction() {

		$request = $this->getRequest();
		if (!$request->isPost()) {
			$this->status = false;
			$this->msg = $this->getMessage('ERR_DATA_SET');
			return $this->JSONRespond();
		}

		$this->beginTransaction();
		$respond = $this->getService('JobDashboardService')->saveVehicleRemark($request->getPost());

		if (!$respond['status']) {
			$this->status = false;
            $this->msg = $this->getMessage($respond['msg']);
            $this->rollback();
            return $this->JSONRespond();
		}

		$this->commit();
		$this->status = true;
		$this->data = $respond['data'];
		return $this->JSONRespond();
	}

	public function saveTempJobMaterialsAction() {

		$request = $this->getRequest();
		if (!$request->isPost()) {
			$this->status = false;
			$this->msg = $this->getMessage('ERR_DATA_SET');
			return $this->JSONRespond();
		}

		$this->beginTransaction();
		$respond = $this->getService('JobDashboardService')->saveTempJobMaterials($request->getPost());

		if (!$respond['status']) {
			$this->status = false;
            $this->msg = $this->getMessage($respond['msg']);
            $this->rollback();
            return $this->JSONRespond();
		}

		$this->commit();
		$this->status = true;
		$this->data = $respond['data'];
		$this->msg = $this->getMessage($respond['msg']);
		return $this->JSONRespond();
	}

	// this function use to hold job service
	public function holdJobRelatedServiceAction() {

		$request = $this->getRequest();
		if (!$request->isPost()) {
			$this->status = false;
			$this->msg = $this->getMessage('ERR_DATA_SET');
			return $this->JSONRespond();
		}

		$this->beginTransaction();
		$respond = $this->getService('JobDashboardService')->holdJobRelatedServiceByJobTaskID($request->getPost());

		if (!$respond['status']) {
			$this->status = false;
            $this->msg = $this->getMessage($respond['msg']);
            $this->rollback();
            return $this->JSONRespond();
		}

		$this->commit();
		$this->status = true;
		$this->data = $respond['data'];
		return $this->JSONRespond();
	}

	// this function use to stop job service
	public function stopJobRelatedServiceAction() {

		$request = $this->getRequest();
		if (!$request->isPost()) {
			$this->status = false;
			$this->msg = $this->getMessage('ERR_DATA_SET');
			return $this->JSONRespond();
		}

		$isFromDashboard = (!isset($request->getPost()['isFromDashboard'])) ? false : true;

		$this->beginTransaction();
		$userLocation = $this->user_session->userActiveLocation['locationID'];
		$respond = $this->getService('JobDashboardService')->stopJobRelatedServiceByJobTaskID($request->getPost(), $this->userID,$userLocation);

		if (!$respond['status']) {
			$this->status = false;
            $this->msg = $this->getMessage($respond['msg']);
            $this->rollback();
            return $this->JSONRespond();
		}

		$this->commit();
		if (!$isFromDashboard) {
			$this->status = true;
			$this->data = $respond['data'];
			return $this->JSONRespond();
		} else {

			$this->status = true;
			$this->html = $respond['view'];
			$this->data = $respond['data'];
	        return $this->JSONRespondHtml();
		}
	}

	// this function use to start all job service
	public function startAllJobRelatedServiceAction() {

		$request = $this->getRequest();
		if (!$request->isPost()) {
			$this->status = false;
			$this->msg = $this->getMessage('ERR_DATA_SET');
			return $this->JSONRespond();
		}

		$this->beginTransaction();
		$respond = $this->getService('JobDashboardService')->startAllJobRelatedServiceByJobTaskID($request->getPost(), $this->userID);

		if (!$respond['status']) {
			$this->status = false;
            $this->msg = $this->getMessage($respond['msg']);
            $this->rollback();
            return $this->JSONRespond();
		}

		$this->commit();
		$this->status = true;
		return $this->JSONRespond();
	}

	// this function use to end all job service
	public function endAllJobRelatedServiceAction() {

		$request = $this->getRequest();
		if (!$request->isPost()) {
			$this->status = false;
			$this->msg = $this->getMessage('ERR_DATA_SET');
			return $this->JSONRespond();
		}

		$this->beginTransaction();
		$respond = $this->getService('JobDashboardService')->endAllJobRelatedServiceByJobTaskID($request->getPost(), $this->userID);

		if (!$respond['status']) {
			$this->status = false;
            $this->msg = $this->getMessage($respond['msg']);
            $this->rollback();
            return $this->JSONRespond();
		}

		$this->commit();
		$this->status = true;
		return $this->JSONRespond();
	}

	// this function use to get service related employees
	public function getJobTaskRelatedEmployeesAction() {

		$request = $this->getRequest();
		if (!$request->isPost()) {
			$this->status = false;
			$this->msg = $this->getMessage('ERR_DATA_SET');
			return $this->JSONRespond();
		}

		$this->beginTransaction();
		$respond = $this->getService('JobDashboardService')->getJobTaskRelatedEmployees($request->getPost());

		if (!$respond['status']) {
			$this->status = false;
            $this->msg = $this->getMessage($respond['msg']);
            $this->rollback();
            return $this->JSONRespond();
		}

		$this->commit();
		$this->status = true;
		$this->data = $respond['data'];
        return $this->JSONRespond();
	}

	// this function use to restart job service
	public function restartJobRelatedServiceAction() {

		$request = $this->getRequest();
		if (!$request->isPost()) {
			$this->status = false;
			$this->msg = $this->getMessage('ERR_DATA_SET');
			return $this->JSONRespond();
		}
		$isFromDashboard = (!isset($request->getPost()['isFromDashboard'])) ? false : true;

		$this->beginTransaction();
		$userLocation = $this->user_session->userActiveLocation['locationID'];
		$respond = $this->getService('JobDashboardService')->restartJobRelatedServiceByJobTaskID($request->getPost(),$userLocation);

		if (!$respond['status']) {
			$this->status = false;
            $this->msg = $this->getMessage($respond['msg']);
            $this->rollback();
            return $this->JSONRespond();
		}

		$this->commit();

		if (!$isFromDashboard) {
			$this->status = true;
			$this->data = $respond['data'];
			return $this->JSONRespond();
		} else {
			$this->status = true;
			$this->html = $respond['view'];
			$this->data = $respond['data'];
	        return $this->JSONRespondHtml();
		}
	}

	// this function use to get job status
	public function getJobStatusByJobIdAction() {

		$request = $this->getRequest();
		if (!$request->isPost()) {
			$this->status = false;
			$this->msg = $this->getMessage('ERR_DATA_SET');
			return $this->JSONRespond();
		}

		$this->beginTransaction();
		$respond = $this->getService('JobDashboardService')->getJobStatusByJobId($request->getPost());

		if (!$respond['status']) {
			$this->status = false;
            $this->msg = $this->getMessage($respond['msg']);
            $this->rollback();
            return $this->JSONRespond();
		}

		$this->commit();
		$this->status = true;
		$this->data = $respond['data'];
		return $this->JSONRespond();
	}

	public function getJobsForDashboardAction() 
	{
		$request = $this->getRequest();
		if (!$request->isPost()) {
			$this->status = false;
			$this->msg = $this->getMessage('ERR_DATA_SET');
			return $this->JSONRespond();
		}

		$respond = $this->getService('JobDashboardService')->getJobsForDashboard($this->user_session->userActiveLocation['locationID']);
		$this->status = true;
		$this->data = $respond;
		$this->html = $respond['view'];
		return $this->JSONRespondHtml();
	}
	public function getJobEstimatedCostDetailsAction() 
	{
		$request = $this->getRequest();
		if (!$request->isPost()) {
			$this->status = false;
			$this->msg = $this->getMessage('ERR_DATA_SET');
			return $this->JSONRespond();
		}

		$respond = $this->getService('JobDashboardService')->getJobEstimatedCostDetails($this->user_session->userActiveLocation['locationID'], $request->getPost());
		$this->status = true;
		$this->data = $respond;
		$this->html = $respond['view'];
		return $this->JSONRespondHtml();
	}

	public function getServiceSubTasksAction() 
	{
		$request = $this->getRequest();
		if (!$request->isPost()) {
			$this->status = false;
			$this->msg = $this->getMessage('ERR_DATA_SET');
			return $this->JSONRespond();
		}

		$respond = $this->getService('JobDashboardService')->getServiceSubTasks($this->user_session->userActiveLocation['locationID'], $request->getPost());
		$this->status = true;
		$this->data = $respond;
		$this->html = $respond['view'];
		return $this->JSONRespondHtml();
	}

	public function getPendingJobsForDashboardAction() 
	{
		$request = $this->getRequest();
		if (!$request->isPost()) {
			$this->status = false;
			$this->msg = $this->getMessage('ERR_DATA_SET');
			return $this->JSONRespond();
		}

		$respond = $this->getService('JobDashboardService')->getPendingJobsForDashboard($this->user_session->userActiveLocation['locationID'], $request->getPost());
		$this->status = true;
		$this->data = $respond;
		$this->html = $respond['view'];
		return $this->JSONRespondHtml();
	}

	public function getInprogressJobsForDashboardAction() 
	{
		$request = $this->getRequest();
		if (!$request->isPost()) {
			$this->status = false;
			$this->msg = $this->getMessage('ERR_DATA_SET');
			return $this->JSONRespond();
		}

		$respond = $this->getService('JobDashboardService')->getInprogressJobsForDashboard($this->user_session->userActiveLocation['locationID'], $request->getPost());
		$this->status = true;
		$this->data = $respond;
		$this->html = $respond['view'];
		return $this->JSONRespondHtml();
	}


	public function getCompletedJobsForDashboardAction() 
	{
		$request = $this->getRequest();
		if (!$request->isPost()) {
			$this->status = false;
			$this->msg = $this->getMessage('ERR_DATA_SET');
			return $this->JSONRespond();
		}

		$respond = $this->getService('JobDashboardService')->getCompletedJobsForDashboard($this->user_session->userActiveLocation['locationID'], $request->getPost());
		$this->status = true;
		$this->data = $respond;
		$this->html = $respond['view'];
		return $this->JSONRespondHtml();
	}

}
