<?php

namespace Jobs\Controller\API;

use Zend\View\Model\ViewModel;
use Core\Controller\CoreController;
use Jobs\Form\JobTypeForm;
use Zend\Session\Container;
use Jobs\Model\JobType;

class JobSetupController extends CoreController
{

    //job type save and update function
    public function addAction()
    {
        $request = $this->getRequest();
        if (!$request->isPost()) {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_DATA_SET');
            return $this->JSONRespond();
        }
        
        $this->beginTransaction();

        $respond = $this->getService('JobSetupService')->saveJobType($request->getPost(), $this->userID);

        if (!$respond['status']) {
            $this->rollback();
            $this->status = false;
            $this->msg = $this->getMessage($respond['msg']);
            return $this->JSONRespond();    
        }

        $this->commit();
        $this->status = true;
        $this->data = $respond['data'];
        $this->msg = $this->getMessage($respond['msg']);
        $this->flashMessenger()->addMessage($this->msg);
        return $this->JSONRespond();
    }

    //get job types by job type search key
    public function getJobTypesBySearchKeyAction()
    {
       $request = $this->getRequest();
        if (!$request->isPost()) {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_DATA_SET');
            return $this->JSONRespond();
        }

        $respond = $this->getService('JobSetupService')->getJobTypesBySearchKey($request->getPost(), $this->params()->fromRoute('param1', 1));

        $this->status = true;
        $this->html = $respond;
        return $this->JSONRespondHtml();
    }

    //delete job types by job type id
    public function deleteJobTypeByJobTypeIDAction()
    {
        $request = $this->getRequest();
        if (!$request->isPost()) {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_DATA_SET');
            return $this->JSONRespond();
        }

        $respond = $this->getService('JobSetupService')->deleteJobTypeByJobTypeID($request->getPost(), $this->userID);

        if ($respond['status']) {
            $this->msg = $this->getMessage($respond['msg'],$respond['data']);
            $this->flashMessenger()->addMessage($this->msg);
        } else {
            $this->msg = $this->getMessage($respond['msg']);
        }

        $this->status = $respond['status'];
        $this->data = $respond['data'];
        return $this->JSONRespond();
    }

    /**
     * This function is used to change status
     */
    public function changeStatusIDAction()
    {
        $request = $this->getRequest();
        if (!$request->isPost()) {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_DATA_SET');
            return $this->JSONRespond();
        }

        $respond = $this->getService('JobSetupService')->updateJobTypeStatus($request->getPost());

        $this->status = $respond['status'];
        $this->msg = $this->getMessage($respond['msg']);
        return $this->JSONRespond();
    }

}
