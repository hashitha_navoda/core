<?php

namespace Jobs\Controller\API;

use Core\Controller\CoreController;

class VehicleReleaseController extends CoreController {

    /**
	 * This function is used to aearch job
	 * @param JSON Request
	 * @return JSON Respond
	 */
	public function getJobBySearchKeyForVehicleReleaseAction() 
	{
		$request = $this->getRequest();
		if (!$request->isPost()) {
			$this->status = false;
			$this->msg = $this->getMessage('ERR_DATA_SET');
			return $this->JSONRespond();
		}

		$respond = $this->getService('VehicleReleaseService')->getJobBySearchKeyForVehicleRelease($request->getPost(), $this->params()->fromRoute('param1', 1));

		$this->status = true;
		$this->html = $respond;
		return $this->JSONRespondHtml();
	}

	/**
     * this fuction use to out vehical and close job
     * @param JSON request
     * return JSON respond
     **/
    public function vehicleOutAction()
    {
        $request = $this->getRequest();
        if (!$request->isPost()) {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_REQUEST');
            return $this->JSONRespond();
        }

        $this->beginTransaction();
        $respond = $this->getService('VehicleReleaseService')->vehicleOut($request->getPost());

        if (!$respond['status']) {
            $this->rollback();
            $this->status = false;
            $this->msg = $this->getMessage($respond['msg']);
            return $this->JSONRespond();
        }
        
        $this->status = true;
        $this->Commit();
        $this->msg = $this->getMessage($respond['msg']);
        $this->data = $respond['data'];
        return $this->JSONRespond();
           
    }
}