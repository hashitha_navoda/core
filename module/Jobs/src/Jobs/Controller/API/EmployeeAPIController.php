<?php

namespace Jobs\Controller\API;

use Core\Controller\CoreController;

class EmployeeAPIController extends CoreController {

	/**
	 * this function use to create employee.
	 * return json respond
	 **/
	public function createEmployeeAction() {
		$request = $this->getRequest();

		if (!$request->isPost()) {
			$this->status = false;
			$this->msg = $this->getMessage('ERR_DATA_SET');
			$this->rollback();
			return $this->JSONRespond();
		}
		$this->beginTransaction();
		$respond = $this->getService('EmployeeService')->createEmployee($request->getPost(), $this->userID);
		if (!$respond['status']) {
			$this->status = false;
			$this->data = $respond['data'];
			$this->msg = $this->getMessage($respond['msg']);
			$this->rollback();
			return $this->JSONRespond();
		}

		$this->commit();
		$this->status = true;
		$this->msg = $this->getMessage($respond['msg']);
		return $this->JSONRespond();

	}

	/**
	 * this function use to change the employee status.
	 * return json respond
	 **/
	public function changeStatusIDAction() {
		$request = $this->getRequest();
		if (!$request->isPost()) {
			$this->status = false;
			$this->msg = $this->getMessage('ERR_DATA_SET');
			$this->rollback();
			return $this->JSONRespond();
		}
		$this->beginTransaction();
		$updateState = $this->getService('EmployeeService')->updateEmployeeState($request->getPost());
		if ($updateState['status']) {
			$this->commit();
			$this->status = true;
			$this->msg = $this->getMessage($updateState['msg']);
		} else {
			$this->status = false;
			$this->msg = $this->getMessage($updateState['msg']);
			$this->rollback();
		}
		return $this->JSONRespond();

	}

	/**
	 * this function use to update employee.
	 * return json respond
	 **/
	public function updateAction() {
		$request = $this->getRequest();
		if (!$request->isPost()) {
			$this->status = false;
			$this->msg = $this->getMessage('ERR_DATA_SET');
			return $this->JSONRespond();
		}

		$this->beginTransaction();
		$respond = $this->getService('EmployeeService')->updateEmployee($request->getPost(), $this->userID);

		if (!$respond['status']) {

			$this->status = false;
			$this->msg = $this->getMessage($respond['msg']);
			$this->rollback();
		} else {
			$this->commit();
			$this->status = true;
			$this->msg = $this->getMessage($respond['msg']);
			$this->flashMessenger()->addMessage($this->msg);
		}

		return $this->JSONRespond();

	}

	/**
	 * this function use to search employee list by key, Department, Designation.
	 * return json respond
	 **/
	public function searchAction() {
		$request = $this->getRequest();
		if (!$request->isPost()) {
			$this->status = false;
			$this->msg = $this->getMessage('ERR_DATA_SET');
			return $this->JSONRespond();
		}
		$result = $this->getService('EmployeeService')->employeeSearchByKey($request->getPost(), (int) $this->params()->fromRoute('param1', 1));

		$this->status = $result['status'];
		$this->html = $result['view'];
		return $this->JSONRespondHtml();

	}

	/**
	 * this function use to designation details according to the employeeID.
	 * return json respond
	 **/
	public function getEmployeeDesignationAction() {
		$request = $this->getRequest()->getQuery('employeeID');
		$respond = $this->getService('EmployeeService')->getEmployeeDesignations($request);
		$this->status = true;
		$this->data = $respond['data'];
		$this->html = $respond['view'];
		return $this->JSONRespondHtml();
	}

	//delete department by employee id
	public function deleteEmployeeAction() {
		$request = $this->getRequest();
		if (!$request->isPost()) {
			$this->status = false;
			$this->msg = $this->getMessage('ERR_DATA_SET');
			return $this->JSONRespond();
		}

		$this->beginTransaction();
		$respond = $this->getService('EmployeeService')->deleteEmployee($request->getPost(), $this->userID);

		if ($respond['status']) {
			$this->msg = $this->getMessage($respond['msg']);
			$this->flashMessenger()->addMessage($this->msg);
			$this->commit();
		} else {
			$this->msg = $this->getMessage($respond['msg']);
			$this->rollback();
		}

		$this->status = $respond['status'];
		$this->data = $respond['data'];
		return $this->JSONRespond();
	}

	/**
	 * This function is used to search job superviosrs for dropdown
	 */
	public function searchJobSupervisorsForDropdownAction() {
		$request = $this->getRequest();
		if (!$request->isPost()) {
			$this->status = false;
			$this->msg = $this->getMessage('ERR_DATA_SET');
			return $this->JSONRespond();
		}
		$respond = $this->getService('EmployeeService')->searchJobEmployeeForDropdown($request->getPost(), true, false);

		$this->data = array('list' => $respond);
		return $this->JSONRespond();
	}

	/**
	 * This function is used to search job managers for dropdown
	 */
	public function searchJobManagersForDropdownAction() {
		$request = $this->getRequest();
		if (!$request->isPost()) {
			$this->status = false;
			$this->msg = $this->getMessage('ERR_DATA_SET');
			return $this->JSONRespond();
		}
		$respond = $this->getService('EmployeeService')->searchJobEmployeeForDropdown($request->getPost(), false, true);

		$this->data = array('list' => $respond);
		return $this->JSONRespond();
	}

	/**
	 * This function is used to get employee details by employee id
	 */
	public function getEmployeeDetailsAction() {
		$request = $this->getRequest();
		if (!$request->isPost()) {
			$this->status = false;
			$this->msg = $this->getMessage('ERR_DATA_SET');
			return $this->JSONRespond();
		}
		$respond = $this->getService('EmployeeService')->getEmployeeDetailsByEmployeeID($request->getPost());

		$this->status = true;
		$this->data = $respond;
		return $this->JSONRespond();
	}

	/**
	 * This function is used to search job managers for dropdown
	 */
	public function searchDesignationWiseEmployeesForDropdownAction() {
		$request = $this->getRequest();
		if (!$request->isPost()) {
			$this->status = false;
			$this->msg = $this->getMessage('ERR_DATA_SET');
			return $this->JSONRespond();
		}
		$respond = $this->getService('EmployeeService')->searchJobDesignationWiseEmployeeForDropdown($request->getPost(), false, false);

		$this->data = array('list' => $respond);
		return $this->JSONRespond();
	}

	/**
     * @author Ashan <ashan@thinkcube.com>
     * Search employee for dropdown
     * @return type
     */
    public function searchEmployeeForDropdownAction()
    {
        $searchrequest = $this->getRequest();
        if (!$searchrequest->isPost()) {
			$this->status = false;
			$this->msg = $this->getMessage('ERR_DATA_SET');
			return $this->JSONRespond();
		}
		$respond = $this->getService('EmployeeService')->searchEmployeeForDropdown($searchrequest->getPost(), false, false);

		$this->data = array('list' => $respond);
        return $this->JSONRespond();
    }
}