<?php

namespace Jobs\Controller\API;

use Core\Controller\CoreController;

class ServiceCostController extends CoreController {

    /**
	 * This function is used to aearch job
	 * @param JSON Request
	 * @return JSON Respond
	 */
	public function getJobBySearchKeyForServiceCostAction() 
	{
		$request = $this->getRequest();
		if (!$request->isPost()) {
			$this->status = false;
			$this->msg = $this->getMessage('ERR_DATA_SET');
			return $this->JSONRespond();
		}

		$respond = $this->getService('CostService')->getJobBySearchKeyForServiceCost($request->getPost(), $this->params()->fromRoute('param1', 1));

		$this->status = true;
		$this->html = $respond;
		return $this->JSONRespondHtml();
	}

	/**
	 * This function is used to get job service data for update
	 * @param JSON Request
	 * @return JSON Respond
	 */
	public function getJobServiceByJobIdAction() 
	{
		$request = $this->getRequest();
		if (!$request->isPost()) {
			$this->status = false;
			$this->msg = $this->getMessage('ERR_DATA_SET');
			return $this->JSONRespond();
		}

		$respond = $this->getService('CostService')->getJobServiceByJobId($request->getPost());

		$this->status = true;
		$this->data = $respond;
		return $this->JSONRespondHtml();
	}

	/**
	 * This function is used to get job service data for update
	 * @param JSON Request
	 * @return JSON Respond
	 */
	public function getJobDetailsByJobIdAction() 
	{
		$request = $this->getRequest();
		if (!$request->isPost()) {
			$this->status = false;
			$this->msg = $this->getMessage('ERR_DATA_SET');
			return $this->JSONRespond();
		}

		$respond = $this->getService('CostService')->getJobDetailsByJobId($request->getPost());

		$this->status = true;
		$this->data = $respond['data'];
		return $this->JSONRespondHtml();
	}
	
	/**
     * this action use  to save job costing data
     * @param JSON request
     * return JSON respond
     **/
    public function saveAction()
    {
        $request = $this->getRequest();
        if (!$request->isPost()) {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_REQUEST');
            return $this->JSONRespond();
        }

        $this->beginTransaction();
        $respond = $this->getService('CostService')->saveJobCost($request->getPost(), 'service');

        if (!$respond['status']) {
            $this->rollback();
            $this->status = false;
            $this->msg = $this->getMessage($respond['msg']);
            return $this->JSONRespond();
        }
        
        $this->status = true;
        $this->Commit();
        $this->msg = $this->getMessage($respond['msg']);
        $this->data = $respond['data'];
        return $this->JSONRespond();
           
    }

    /**
	 * This function is used to get job employee data
	 * @param JSON Request
	 * @return JSON Respond
	 */
	public function getJobEmployeeByJobIdAction() 
	{
		$request = $this->getRequest();
		if (!$request->isPost()) {
			$this->status = false;
			$this->msg = $this->getMessage('ERR_DATA_SET');
			return $this->JSONRespond();
		}

		$respond = $this->getService('CostService')->getJobEmployeeByJobId($request->getPost());

		$this->status = true;
		$this->data = $respond;
		return $this->JSONRespondHtml();
	}
}
