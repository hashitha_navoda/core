<?php

namespace Jobs\Controller\API;

use Core\Controller\CoreController;

class RateCardController extends CoreController {

    /**
     * this function use to create rate card.
     * return json respond
     **/
    public function createRateCardAction() {
        $request = $this->getRequest();

        if (!$request->isPost()) {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_DATA_SET');
            $this->rollback();
            return $this->JSONRespond();
        }
        $this->beginTransaction();
        $respond = $this->getService('RateCardService')->createRateCard($request->getPost());
        if (!$respond['status']) {
            $this->status = false;
            $this->data = $respond['data'];
            $this->msg = $this->getMessage($respond['msg']);
            $this->rollback();
            return $this->JSONRespond();
        }

        $this->commit();
        $this->status = true;
        $this->msg = $this->getMessage($respond['msg']);
        return $this->JSONRespond();

    }


    //get rate card related service rates
    public function getRelatedServiceRatesAction()
    {
        $request = $this->getRequest();
        if (!$request->isPost()) {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_DATA_SET');
            return $this->JSONRespond();
        }
        $this->beginTransaction();
        $respond = $this->getService('RateCardService')->getRelatedServiceRates($request->getPost());

        if ($respond['status']) {
            $this->msg = $this->getMessage($respond['msg']);
            $this->commit();
        } else {
            $this->msg = $this->getMessage($respond['msg']);
            $this->rollback();
        }

        $this->status = $respond['status'];
        $this->data = $respond['data'];
        return $this->JSONRespond();
    }


    /**
     * this function use to update rate card
     * return json respond
     **/
    public function updateRateCardAction() {
        $request = $this->getRequest();
        if (!$request->isPost()) {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_DATA_SET');
            return $this->JSONRespond();
        }

        $this->beginTransaction();
        $respond = $this->getService('RateCardService')->updateRateCard($request->getPost(), $this->userID);

        if (!$respond['status']) {

            $this->status = false;
            $this->msg = $this->getMessage($respond['msg']);
            $this->rollback();
        } else {
            $this->commit();
            $this->status = true;
            $this->msg = $this->getMessage($respond['msg']);
            $this->flashMessenger()->addMessage($this->msg);
        }

        return $this->JSONRespond();

    }


    /**
     * this function use to change the rate card status.
     * return json respond
     **/
    public function changeStatusIDAction() {
        $request = $this->getRequest();
        if (!$request->isPost()) {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_DATA_SET');
            $this->rollback();
            return $this->JSONRespond();
        }
        $this->beginTransaction();
        $updateState = $this->getService('RateCardService')->changeStatusID($request->getPost());
        if ($updateState['status']) {
            $this->commit();
            $this->status = true;
            $this->msg = $this->getMessage($updateState['msg']);
        } else {
            $this->status = false;
            $this->msg = $this->getMessage($updateState['msg']);
            $this->rollback();
        }
        return $this->JSONRespond();

    }


    //delete rate card by rate card id
    public function deleteRateCardAction() {
        $request = $this->getRequest();
        if (!$request->isPost()) {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_DATA_SET');
            return $this->JSONRespond();
        }

        $this->beginTransaction();
        $respond = $this->getService('RateCardService')->deleteRateCard($request->getPost());

        if ($respond['status']) {
            $this->msg = $this->getMessage($respond['msg']);
            $this->flashMessenger()->addMessage($this->msg);
            $this->commit();
        } else {
            $this->msg = $this->getMessage($respond['msg']);
            $this->rollback();
        }

        $this->status = $respond['status'];
        $this->data = $respond['data'];
        return $this->JSONRespond();
    }


    /**
     * this function use to search rate card list by key.
     * return json respond
     **/
    public function searchAction() {
        $request = $this->getRequest();
        if (!$request->isPost()) {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_DATA_SET');
            return $this->JSONRespond();
        }
        $type = 'serviceEmp';
        $result = $this->getService('RateCardService')->rateCardSearchByKey($request->getPost(), (int) $this->params()->fromRoute('param1', 1));

        $this->status = $result['status'];
        $this->html = $result['view'];
        return $this->JSONRespondHtml();
    }

    /**
     * This function is used to search rate card for dropdown
     */
    public function searchRateCardsForDropdownAction() {
        $request = $this->getRequest();
        if (!$request->isPost()) {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_DATA_SET');
            return $this->JSONRespond();
        }

        $respond = $this->getService('RateCardService')->searchRateCardForDropdown($request->getPost());

        $this->data = array('list' => $respond);
        return $this->JSONRespond();
    }

}