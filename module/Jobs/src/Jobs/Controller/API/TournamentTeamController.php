<?php

namespace Jobs\Controller\API;

use Core\Controller\CoreController;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;


class TournamentTeamController extends CoreController
{

    /**
    * this function use to create team.
    * return json respond
    **/
    public function createTeamAction()
    {
        $request = $this->getRequest();

        if (!$request->isPost()) {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_DATA_SET');
            $this->rollback();
            return $this->JSONRespond();
        }
        $this->beginTransaction();
        $respond = $this->getService('TeamService')->createTeam($request->getPost(), $this->userID, $tournamentFlag = true);
        if (!$respond['status']) {
            $this->status = false;
            $this->data = $respond['data'];
            $this->msg = $this->getMessage($respond['msg']);
            $this->rollback();
            return $this->JSONRespond();    
        } 
        
        $this->commit();
        $this->status = true;
        $this->msg = $this->getMessage($respond['msg']);
        return $this->JSONRespond();
    	
    }

    /**
    * this function use to change the team status.
    * return json respond
    **/
    public function changeStatusIDAction() 
    {
        $request = $this->getRequest();
        if (!$request->isPost()) {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_DATA_SET');
            $this->rollback();
            return $this->JSONRespond();
        }
        $this->beginTransaction();
        $updateState = $this->getService('TeamService')->updateTeamState($request->getPost());
        if ($updateState['status']) {
            $this->commit();
            $this->status = true;
            $this->msg = $this->getMessage($updateState['msg']);
        } else {
            $this->status = false;
            $this->msg = $this->getMessage($updateState['msg']);
            $this->rollback();
        }
        return $this->JSONRespond();
        
    }

    /**
    * this function use to update team.
    * return json respond
    **/
    public function updateAction() 
    {
        $request = $this->getRequest();
        if (!$request->isPost()) {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_DATA_SET');
            return $this->JSONRespond();
        }

        $this->beginTransaction();
        $respond = $this->getService('TeamService')->updateTeam($request->getPost(), $this->userID, $tournamentFlag = true);

        if (!$respond['status']) {

            $this->status = false;
            $this->msg = $this->getMessage($respond['msg']);
            $this->rollback();     
        } else {
            $this->commit();
            $this->status = true;
            $this->msg = $this->getMessage($respond['msg']);
            $this->flashMessenger()->addMessage($this->msg);
        }

        return $this->JSONRespond();

    } 

    /**
    * this function use to search team list by key.
    * return json respond
    **/
    public function searchAction() 
    {
        $request = $this->getRequest();
        if (!$request->isPost()) {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_DATA_SET');
            return $this->JSONRespond();
        }
        $result = $this->getService('TeamService')->teamSearchByKey($request->getPost(), (int) $this->params()->fromRoute('param1', 1));

        $this->status = $result['status'];
        $this->html = $result['view'];
        return $this->JSONRespondHtml();

    }

    public function searchParticipantAction() 
    {
        $request = $this->getRequest();
        if (!$request->isPost()) {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_DATA_SET');
            return $this->JSONRespond();
        }
        $result = $this->getService('TeamService')->teamSearchByKey($request->getPost(), (int) $this->params()->fromRoute('param1', 1),$tournamentFlag = true);

        $this->status = $result['status'];
        $this->html = $result['view'];
        return $this->JSONRespondHtml();

    }

    /**
    * this function use get to team details according to the teamID.
    * return json respond
    **/
    public function getTeamEmployeesAction() 
    {
        $request = $this->getRequest()->getQuery('teamID');
        $respond = $this->getService('TeamService')->getTeamEmployees($request);
        $this->status = true;
        $this->data = $respond['data'];
        $this->html = $respond['view'];
        return $this->JSONRespondHtml();
    }

    public function getTeamPartcipantsAction() 
    {
        $request = $this->getRequest()->getQuery('teamID');
        $respond = $this->getService('TeamService')->getTeamPartcipants($request);
        $this->status = true;
        $this->data = $respond['data'];
        $this->html = $respond['view'];
        return $this->JSONRespondHtml();
    }

    //delete team by team id
    public function deleteTeamByTeamIDAction()
    {
        $request = $this->getRequest();
        if (!$request->isPost()) {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_DATA_SET');
            return $this->JSONRespond();
        }

        $respond = $this->getService('TeamService')->deleteTeamByTeamID($request->getPost());

        if ($respond['status']) {
            $this->msg = $this->getMessage($respond['msg'],$respond['data']);
            $this->flashMessenger()->addMessage($this->msg);
        } else {
            $this->msg = $this->getMessage($respond['msg']);
        }

        $this->status = $respond['status'];
        $this->data = $respond['data'];
        return $this->JSONRespond();
    }

    public function searchTeamsForDropdownAction() {
        $request = $this->getRequest();
        if (!$request->isPost()) {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_DATA_SET');
            return $this->JSONRespond();
        }
        $respond = $this->getService('TeamService')->searchTeamsForDropdown($request->getPost());

        $this->data = array('list' => $respond);
        return $this->JSONRespond();
    }
}