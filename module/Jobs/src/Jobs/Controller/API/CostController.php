<?php

namespace Jobs\Controller\API;

use Core\Controller\CoreController;

class CostController extends CoreController 
{

    /**
    * this action use  to get cost types
    * @param JSON request
    * return JSON respond
    **/
    public function searchCostTypeForDropdownAction()
    {
        $request = $this->getRequest();
        if (!$request->isPost()) {
        	$this->status = false;
        	$this->msg = $this->getMessage('ERR_REQUEST');
        	return $this->JSONRespond();
        }

        $respond = $this->getService('CostTypeService')->searchCostTypeForDropdown($request->getPost());
        
        $this->data = array('list' => $respond['data']['costTypeList']);
        return $this->JSONRespond();
    }

    /**
    * this action use  to save job costing data
    * @param JSON request
    * return JSON respond
    **/
    public function saveAction()
    {
        $request = $this->getRequest();
        if (!$request->isPost()) {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_REQUEST');
            return $this->JSONRespond();
        }

        $this->beginTransaction();
        $respond = $this->getService('CostService')->saveJobCost($request->getPost());

        if (!$respond['status']) {
            $this->rollback();
            $this->status = false;
            $this->msg = $this->getMessage($respond['msg']);
            return $this->JSONRespond();
        }
        
        $this->status = true;
        $this->Commit();
        $this->msg = $this->getMessage($respond['msg']);
        $this->data = $respond['data'];
        return $this->JSONRespond();
           
    }
}