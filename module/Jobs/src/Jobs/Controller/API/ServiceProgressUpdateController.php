<?php

namespace Jobs\Controller\API;

use Core\Controller\CoreController;
use Jobs\Model\TaskCard;

class ServiceProgressUpdateController extends CoreController {

	// this function use to get Department related services for service progress update
	public function getDepartmentRelatedProgressUpdateListsDetailsAction() {
		$request = $this->getRequest();
		if (!$request->isPost()) {
			$this->status = false;
			$this->msg = $this->getMessage('ERR_DATA_SET');
			return $this->JSONRespond();
		}

		$this->beginTransaction();
		$respond = $this->getService('ProgressService')->getDepartmentRelatedLists($request->getPost());

		if (!$respond['status']) {
			$this->status = false;
            $this->msg = $this->getMessage($respond['msg']);
            $this->rollback();
            return $this->JSONRespond();
		}

		$this->status = true;
		$this->html = $respond['view'];
		$this->data = $respond['data'];
        return $this->JSONRespondHtml();
	}

	// this function use to get to resume progress update list details
	public function getToResumeServiceDetailsAction() {

		$request = $this->getRequest();
		if (!$request->isPost()) {
			$this->status = false;
			$this->msg = $this->getMessage('ERR_DATA_SET');
			return $this->JSONRespond();
		}

		$this->beginTransaction();
		$respond = $this->getService('ProgressService')->getToResumeServiceDetails($request->getPost());

		if (!$respond['status']) {
			$this->status = false;
            $this->msg = $this->getMessage($respond['msg']);
            $this->rollback();
            return $this->JSONRespond();
		}

		$this->status = true;
		$this->html = $respond['view'];
		$this->data = $respond['data'];
        return $this->JSONRespondHtml();
	}


	// this function use to start job service
	public function startJobRelatedServiceAction() {

		$request = $this->getRequest();
		if (!$request->isPost()) {
			$this->status = false;
			$this->msg = $this->getMessage('ERR_DATA_SET');
			return $this->JSONRespond();
		}

		$this->beginTransaction();
		$respond = $this->getService('JobDashboardService')->startJobRelatedServiceByJobTaskID($request->getPost(), $this->userID);

		if (!$respond['status']) {
			$this->status = false;
            $this->msg = $this->getMessage($respond['msg']);
            $this->rollback();
            return $this->JSONRespond();
		}

		$this->commit();
		$this->status = true;
		$this->data = $respond['data'];
        $this->msg = $this->getMessage($respond['msg']);
		return $this->JSONRespond();
	}

	// this function use to hold job service
	public function holdJobRelatedServiceAction() {

		$request = $this->getRequest();
		if (!$request->isPost()) {
			$this->status = false;
			$this->msg = $this->getMessage('ERR_DATA_SET');
			return $this->JSONRespond();
		}

		$this->beginTransaction();
		$respond = $this->getService('JobDashboardService')->holdJobRelatedServiceByJobTaskID($request->getPost());

		if (!$respond['status']) {
			$this->status = false;
            $this->msg = $this->getMessage($respond['msg']);
            $this->rollback();
            return $this->JSONRespond();
		}

		$this->commit();
		$this->status = true;
        $this->msg = $this->getMessage($respond['msg']);
		$this->data = $respond['data'];
		return $this->JSONRespond();
	}

	// this function use to stop job service
	public function stopJobRelatedServiceAction() {

		$request = $this->getRequest();
		if (!$request->isPost()) {
			$this->status = false;
			$this->msg = $this->getMessage('ERR_DATA_SET');
			return $this->JSONRespond();
		}

		$this->beginTransaction();
		$respond = $this->getService('JobDashboardService')->stopJobRelatedServiceByJobTaskID($request->getPost(), $this->userID);

		if (!$respond['status']) {
			$this->status = false;
            $this->msg = $this->getMessage($respond['msg']);
            $this->rollback();
            return $this->JSONRespond();
		}

		$this->commit();
		$this->status = true;
        $this->msg = $this->getMessage($respond['msg']);
		$this->data = $respond['data'];
		return $this->JSONRespond();
	}

	// this function use to get job related due services
	public function getJobRelatedDueServicesAction() {

		$request = $this->getRequest();
		if (!$request->isPost()) {
			$this->status = false;
			$this->msg = $this->getMessage('ERR_DATA_SET');
			return $this->JSONRespond();
		}

		$this->beginTransaction();
		$respond = $this->getService('ProgressService')->getJobRelatedDueServices($request->getPost());

		if (!$respond['status']) {
			$this->status = false;
            $this->msg = $this->getMessage($respond['msg']);
            $this->rollback();
            return $this->JSONRespond();
		}

		$this->status = true;
		$this->html = $respond['view'];
		$this->data = $respond['data'];
        return $this->JSONRespondHtml();
	}

	// this function use to get det details for service auto select process
	public function getListsForAutoSelectProcessAction() {
		$request = $this->getRequest();
		if (!$request->isPost()) {
			$this->status = false;
			$this->msg = $this->getMessage('ERR_DATA_SET');
			return $this->JSONRespond();
		}

		$this->beginTransaction();
		$respond = $this->getService('ProgressService')->getListsForAutoSelectProcess($request->getPost());

		if (!$respond['status']) {
			$this->status = false;
            $this->msg = $this->getMessage($respond['msg']);
            $this->rollback();
            return $this->JSONRespond();
		}

		$this->status = true;
		$this->html = $respond['view'];
		$this->data = $respond['data'];
        return $this->JSONRespondHtml();
	}
}
