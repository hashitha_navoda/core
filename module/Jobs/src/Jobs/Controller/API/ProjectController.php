<?php

namespace Jobs\Controller\API;

use Core\Controller\CoreController;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;
use Zend\Session\Container;


class ProjectController extends CoreController
{

    public function initiateBusinessAction()
    {
    	$request = $this->getRequest();
        if (!$request->isPost()) {
        	$this->status = false;
            $this->msg = $this->getMessage('ERR_DATA_SET');
            return $this->JSONRespond();
        }
        
        $respond = $this->getService('ProjectService')->initiateBusinessType($request->getPost());
        if (!$respond) {
        	$this->status = true;
        	$this->msg = $this->getMessage('ERR_SAVE_BUSI_TYPE');
        	return $this->JSONRespond();	
        } 
        
        $this->status = true;
        $this->msg = $this->getMessage('SUCC_SAVE_BUSI_TYPE');
        return $this->JSONRespond();
    }

    /**
    * this function use to create project.
    * return json respond
    **/
    public function createAction() 
    {
        $request = $this->getRequest();

        if (!$request->isPost()) {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_DATA_SET');
            $this->rollback();
            return $this->JSONRespond();
        }
        $this->beginTransaction();
        $locationID = $this->user_session->userActiveLocation['locationID'];
        $respond = $this->getService('ProjectService')->createProject($request->getPost(), $this->userID, $locationID);

        if (!$respond['status']) {
            $this->status = false;
            $this->data = $respond['data'];
            $this->msg = $this->getMessage($respond['msg']);
            $this->rollback();
            return $this->JSONRespond();    
        } 
 
        $this->data = $respond['data'];
        $this->commit();
        $this->status = true;
        $this->msg = $this->getMessage($respond['msg']);
        return $this->JSONRespond();

    }

    /**
    * this function use to get edit details of a project.
    * return json respond
    **/
    public function getEditDetailsAction() 
    {

        $locationID = $this->user_session->userActiveLocation['locationID'];
        $projectID = $this->params()->fromRoute('param1');
        $this->beginTransaction();
        $respond = $this->getService('ProjectService')->getProjectById($projectID, $locationID);
        
        if (!$respond['status']) {
            $this->status = false;
            $this->data = $respond['data'];
            $this->msg = $this->getMessage($respond['msg']);
            $this->rollback();
            return $this->JSONRespond();    
        } 
 
        $this->commit();
        $this->status = true;
        $this->data = $respond['data'];
        $this->msg = $this->getMessage($respond['msg']);
        return $this->JSONRespond();
    }

    /**
    * this function use to get update project.
    * return json respond
    **/
    public function updateAction()
    {
        $request = $this->getRequest();
        if (!$request->isPost()) {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_DATA_SET');
            return $this->JSONRespond();
        }

        $this->beginTransaction();
        $respond = $this->getService('ProjectService')->updateProject($request->getPost(), $this->userID);

        if (!$respond['status']) {

            $this->status = false;
            $this->msg = $this->getMessage($respond['msg']);
            $this->rollback();     
        } else {
            $this->commit();
            $this->status = true;
            $this->msg = $this->getMessage($respond['msg']);
            $this->flashMessenger()->addMessage($this->msg);
        }

        return $this->JSONRespond();

    }

    /**
    * this function use to get project managers details.
    * return json respond
    **/
    public function getProjectManagersAction()
    {
        $request = $this->getRequest()->getQuery('projectId');

        $respond = $this->getService('ProjectService')->getProjectManagers($request);
        $this->status = true;
        $this->html = $respond['view'];
        return $this->JSONRespondHtml();
    }

    /**
    * this function use to search project list by key, projectManager, projectType.
    * return json respond
    **/
    public function searchAction() 
    {
        $request = $this->getRequest();
        if (!$request->isPost()) {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_DATA_SET');
            return $this->JSONRespond();
        }

        $result = $this->getService('ProjectService')->projectSearch($request->getPost(), (int) $this->params()->fromRoute('param1', 1), $this->getStatusesList());
        $this->getViewHelper('HeadScript')->prependFile('/js/jobs/project-list.js');
        $this->status = $result['status'];
        $this->html = $result['view'];
        return $this->JSONRespondHtml();

    }

    /**
    * this function use to delete project.
    * return json respond
    **/
    public function deleteAction()
    {
        $request = $this->getRequest();
        if (!$request->isPost()) {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_DATA_SET');
            return $this->JSONRespond();
        }
        $this->beginTransaction();
        $respond = $this->getService('ProjectService')->deleteProject($request->getPost(), $this->userID);

        if ($respond['status']) {
            $this->msg = $this->getMessage($respond['msg'], $respond['data']);
            $this->flashMessenger()->addMessage($this->msg);
            $this->commit();
        } else {
            $this->msg = $this->getMessage($respond['msg']);
            $this->rollback();
        }

        $this->status = $respond['status'];
        $this->data = $respond['data'];
        return $this->JSONRespond();
    }

	/**
	 * This function is used to search projects for dropdown
	 */
	public function searchProjectsForDropdownAction() {
		$request = $this->getRequest();
		if (!$request->isPost()) {
			$this->status = false;
			$this->msg = $this->getMessage('ERR_DATA_SET');
			return $this->JSONRespond();
		}
		$respond = $this->getService('ProjectService')->searchProjectsForDropdown($request->getPost());

		$this->data = array('list' => $respond);
		return $this->JSONRespond();
	}


    /**
     * This function is used to get project details by project ID
     * @param JSON Request 
     * @return JSON Respond
     */
    public function getProjectDetailsAction() 
    {
        $request = $this->getRequest();
        if (!$request->isPost()) {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_DATA_SET');
            return $this->JSONRespond();
        }
        $respond = $this->getService('ProjectService')->getProjectDetails($request->getPost());

        $this->status = $respond['status']; 
        $this->data = $respond['data'];
        return $this->JSONRespond();
    }
}