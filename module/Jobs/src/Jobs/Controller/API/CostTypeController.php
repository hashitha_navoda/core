<?php

namespace Jobs\Controller\API;

use Zend\View\Model\ViewModel;
use Core\Controller\CoreController;
use Jobs\Form\CostTypeForm;
use Zend\Session\Container;
use Jobs\Model\CostType;

class CostTypeController extends CoreController
{

    /**
     * cost type save and update action
     * @param JSON Request
     * @return JSON Respond
     */
    public function addAction()
    {
        $request = $this->getRequest();
        if (!$request->isPost()) {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_DATA_SET');
            return $this->JSONRespond();
        }

        $this->beginTransaction();

        $respond = $this->getService('CostTypeService')->addCostType($request->getPost());

        if (!$respond['status']) {
            $this->rollback();
            $this->status = $respond['status'];
            $this->msg = $this->getMessage($respond['msg']);
            return $this->JSONRespond();
        }

        $this->commit();
        $this->status = $respond['status'];
        $this->data = $respond['data'];
        $this->msg = $this->getMessage($respond['msg'], array($respond['data']['costTypeName']));
        $this->flashMessenger()->addMessage($this->msg);
        return $this->JSONRespond();
    }

    /**
     * This action is used to search cost type
     * @param JSON Request
     * @return JSON Respond
     */
    public function getCostTypesBySearchKeyAction()
    {
        $request = $this->getRequest();
        if (!$request->isPost()) {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_DATA_SET');
            return $this->JSONRespond();
        }

        $respond = $this->getService('CostTypeService')->getCostTypesBySearchKey($request->getPost(), $this->params()->fromRoute('param1', 1));

        $this->status = true;
        $this->html = $respond;
        return $this->JSONRespondHtml();
    }

    /**
     * This action is used to delete cost type
     * @param JSON Request
     * @return JSON Respond
     */
    public function deleteCostTypeByCostTypeIDAction()
    {
        $request = $this->getRequest();
        if (!$request->isPost()) {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_DATA_SET');
            return $this->JSONRespond();
        }

        $this->beginTransaction();
        $respond = $this->getService('CostTypeService')->deleteCostType($request->getPost(), $this->userID);

        if (!$respond['status']) {
            $this->rollback();
            $this->status = $respond['status'];
            $this->msg = $this->getMessage($respond['msg']);
            return $this->JSONRespond();
        }

        $this->commit();
        $this->status = $respond['status'];
        $this->data = $respond['data'];
        $this->msg = $this->getMessage($respond['msg'], array($respond['data']['costTypeName']));
        $this->flashMessenger()->addMessage($this->msg);
        return $this->JSONRespond();
    }

    /**
     * this action use to change the costtype status.
     * return json respond
     **/
    public function changeStatusIDAction()
    {
        $request = $this->getRequest();
        if (!$request->isPost()) {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_DATA_SET');
            $this->rollback();
            return $this->JSONRespond();
        }
        $this->beginTransaction();
        $respond = $this->getService('CostTypeService')->changeStatusID($request->getPost());
        if ($respond['status']) {
            $this->status = true;
            $this->msg = $this->getMessage($respond['msg']);
            $this->commit();
        } else {
            $this->status = false;
            $this->msg = $this->getMessage($respond['msg']);
            $this->rollback();
        }
        return $this->JSONRespond();
    }

}
