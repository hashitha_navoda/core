<?php

namespace Jobs\Controller;

use Zend\View\Model\ViewModel;
use Core\Controller\CoreController;
use Zend\Session\Container;
use Jobs\Form\TaskSetupForm;

class TaskSetupController extends CoreController
{

    protected $sideMenus = 'jobs_side_menu';
    protected $upperMenus = 'task_settings_upper_menu';
    protected $downMenus = 'job_setup_down_menu';

    public function indexAction()
    {   
        $this->getSideAndUpperMenus('Jobs Settings', 'Create Task', 'Jobs', 'Task Settings');
        $taskList = $this->getPaginatedJobTypes();
        $form = new TaskSetupForm();
        $refCode = $this->getAvailableRefCode();
        $view = new ViewModel(array(
            'form' => $form,
            'taskList' => $taskList,
            'paginated' => true,
            'refCode' => $refCode
        ));
        $this->getViewHelper('HeadScript')->prependFile('/js/jobs/taskSetup.js');
        return $view;
    }

    public function getPaginatedJobTypes($perPage = 6, $param = null)
    {
        $this->paginator = $this->CommonTable('Jobs\Model\TaskTable')->fetchAll(true);
        
        if (is_null($param)) {
	        $this->paginator->setCurrentPageNumber((int) $this->params()->fromRoute('param1', 1));
        } else {
        	$this->paginator->setCurrentPageNumber((int) $param);
        }
        $this->paginator->setItemCountPerPage($perPage);
        $this->setLogMessage('task list accessed');
        return $this->paginator;
    }

    public function getAvailableRefCode()
    {
        $referenceService = $this->getService('ReferenceService');
        $refData = $referenceService->getReferenceNumber(37);
        $refCode = $refData['data']["referenceNo"];
        $locationReferenceID = $refData['data']["locationReferenceID"];
        while(!$this->CommonTable('Jobs\Model\TaskTable')->checkTaskCodeAvailability($refCode)) {
            $referenceService->updateReferenceNumber($locationReferenceID);
            $refData = $referenceService->getReferenceNumber(37);
            $refCode = $refData['data']["referenceNo"];
            $locationReferenceID = $refData['data']["locationReferenceID"];
        }
        return $refCode;
    }
}
