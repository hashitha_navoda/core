<?php

namespace Jobs\Controller;

use Zend\View\Model\ViewModel;
use Core\Controller\CoreController;
use Zend\Session\Container;

class ProjectSetupController extends CoreController
{

    
    protected $sideMenus = 'jobs_side_menu';
    protected $upperMenus = 'category_setting_upper_menu';
	protected $downMenus = 'job_setup_down_menu';

    public function indexAction()
    {   
		$this->getSideAndUpperMenus('Jobs Settings', 'Project Setup', 'Jobs', 'Category Settings');

		$projectTypeList = $this->getPaginatedProjectTypes();
        $view = new ViewModel(array(
            'projectTypeList' => $projectTypeList,
            'paginated' => true
        ));

        $this->getViewHelper('HeadScript')->prependFile('/js/jobs/projectType.js');

        return $view;
    }
    
    public function getPaginatedProjectTypes($perPage = 6, $param = null)
    {
        $this->paginator = $this->CommonTable('Jobs\Model\ProjectTypeTable')->fetchAll(true);
        
        if (is_null($param)) {
	        $this->paginator->setCurrentPageNumber((int) $this->params()->fromRoute('param1', 1));
        } else {
        	$this->paginator->setCurrentPageNumber((int) $param);
        }
        $this->paginator->setItemCountPerPage($perPage);
        $this->setLogMessage('Project Type list accessed');

        return $this->paginator;
    }
}
