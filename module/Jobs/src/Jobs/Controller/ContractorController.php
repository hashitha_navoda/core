<?php

namespace Jobs\Controller;

use Zend\View\Model\ViewModel;
use Core\Controller\CoreController;
use Zend\Session\Container;

class ContractorController extends CoreController
{

    protected $sideMenus = 'jobs_side_menu';
    protected $upperMenus = 'contractor_setting_upper_menu';
    protected $downMenus = 'job_setup_down_menu';

    public function createAction()
    {
        $this->getSideAndUpperMenus('Jobs Settings', 'Contractor Setup', 'Jobs', 'Contractor Settings');
        $contractorList = $this->getPaginatedContractor();
        $view = new ViewModel(array(
            'contractorList' => $contractorList,
            'paginated' => true
        ));
        return $view;
    }

    public function getPaginatedContractor($perPage = 6, $param = null)
    {
        $this->paginator = $this->CommonTable('Jobs\Model\ContractorTable')->fetchAll(true);
        
        if (is_null($param)) {
            $this->paginator->setCurrentPageNumber((int) $this->params()->fromRoute('param1', 1));
        } else {
            $this->paginator->setCurrentPageNumber((int) $param);
        }
        $this->paginator->setItemCountPerPage($perPage);
        $this->setLogMessage('Contractor list accessed');

        return $this->paginator;
    }

}