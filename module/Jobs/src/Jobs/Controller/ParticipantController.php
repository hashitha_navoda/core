<?php

namespace Jobs\Controller;

use Zend\View\Model\ViewModel;
use Core\Controller\CoreController;
use Zend\Session\Container;

class ParticipantController extends CoreController
{

    protected $sideMenus = 'tournament_side_menu';
    protected $upperMenus = 'participants_upper_menu';
    protected $downMenus = 'tournament_setup_down_menu';

    public function createAction()
    {   
        $this->getSideAndUpperMenus('Tournament Settings', 'Participants', 'Jobs', 'Participants');

        $paginator = $this->getPaginatedEmployees();
        $this->designation = $this->CommonTable('Jobs/Model/DesignationTable')->fetchActiveDesignations();
        $this->department = $this->CommonTable('Jobs/Model/DepartmentTable')->fetchActiveDepartments();

        $designations = array();
        $departments = array();
        foreach ($this->designation as $desig) {
            $res = [
                'designationID' => $desig['designationID'],
                'designationName' => $desig['designationName'],
                'designationCode' => $desig['designationCode'],
            ];
            array_push($designations, $res);
        }

        foreach ($this->department as $dept) {
            $res = [
                'departmentID' => $dept['departmentID'],
                'departmentName' => $dept['departmentName'],
                'departmentCode' => $dept['departmentCode'],
            ];
            array_push($departments, $res);
        }


        $userdateFormat = $this->getUserDateFormat();
        $employeeView = new ViewModel(array(
                                'designations' => $designations,
                                'departments' => $departments,
                                'paginated' => true,
                                'employees' => $paginator,
                                'userdateFormat' => $userdateFormat
                            ));
        
        $attachmentsView = new ViewModel();
        $attachmentsView->setTemplate('core/modal/attachmentView');
        $employeeView->addChild($attachmentsView, 'attachmentsView');
        
        $employeeView->setTemplate('jobs/participant/create');
        $this->getViewHelper('HeadScript')->prependFile('/js/jobs/participant.js');
        return $employeeView;

    }

    public function getPaginatedEmployees($perPage = 6, $param = null)
    {
        $this->employee = $this->CommonTable('Jobs/Model/EmployeeTable')->fetchAll(true);
        if ($param == null) {
            $this->employee->setCurrentPageNumber((int) $this->params()->fromRoute('param1', 1));
        } else {
            $this->employee->setCurrentPageNumber($param);     
        }
        $this->employee->setItemCountPerPage($perPage);
        return $this->employee;
    }

}
