<?php

namespace Jobs\Controller;

use Zend\View\Model\ViewModel;
use Core\Controller\CoreController;
use Zend\Session\Container;
use Invoice\Form\AddCustomerForm;
use DateTime;

class ServiceJobDashboardController extends CoreController
{

    protected $sideMenus = 'jobs_side_menu_of_service';
    protected $upperMenus = 'jobs_job_upper_menu';


    public function __construct()
    {
        parent::__construct();
        $this->user_session = new Container('ezBizUser');
        $this->userID = $this->user_session->userID;
        $this->username = $this->user_session->username;
        $this->company = $this->user_session->companyDetails;
        $this->cdnUrl = $this->user_session->cdnUrl;
        $this->useAccounting = $this->user_session->useAccounting;
    }

    
    public function indexAction($projectId = null, $mode = false) {

        $this->getSideAndUpperMenus('Job', null, 'Jobs');

        $pendingJobList = $this->getDashboardJobList(3,0);
        $inProgressJobList = $this->getDashboardJobList(8,0);
        $completedJobList = $this->getDashboardJobList(9,0);
        // $closedJobList = $this->getDashboardJobList(4,0);
        // $HoldJobList = $this->getDashboardJobList(17,0);
        // $allInProgressJobList = array_merge($inProgressJobList, $HoldJobList); 
        $allInProgressJobList = $inProgressJobList; 

        foreach ($completedJobList as $key => $part) {
           $sort[$key] = strtotime($part['completed_at']);
        }
        array_multisort($sort, SORT_DESC, $completedJobList);
        $userLocation = $this->user_session->userActiveLocation['locationID'];
        $pendingJobCount = $this->setListCount($this->CommonTable('Jobs/Model/JobTable')->getJobCounttByStatus(3, $userLocation));
        $inprogressJobCount = $this->setListCount($this->CommonTable('Jobs/Model/JobTable')->getJobCounttByStatus(8, $userLocation));
        $completedJobCount = $this->setListCount($this->CommonTable('Jobs/Model/JobTable')->getJobCounttByStatus(9, $userLocation));
        // $closedJobCount = $this->setListCount($this->CommonTable('Jobs/Model/JobTable')->getJobCounttByStatus(4, $userLocation));

        // $overallJobs = array_merge($pendingJobList, $completedJobList, $allInProgressJobList);

        $view = new ViewModel(array(
            'pendingJobList' => $pendingJobList,
            'inProgressJobList' => $allInProgressJobList,
            'completedJobList' => $completedJobList,
            'pendingJobCount' => $pendingJobCount,
            'inprogressJobCount' => $inprogressJobCount,
            'completedJobCount' => $completedJobCount,
            // 'closedJobCount' => $closedJobCount,
            // 'overallJobs' => $overallJobs,
        ));

        $view->setTemplate('jobs/service-job-dashboard/index');

        $viesServiceSubTaskView = new ViewModel();
        $viesServiceSubTaskView->setTemplate('jobs/service-job-dashboard/view-service-sub-task-modal');

        $view->addChild($viesServiceSubTaskView, 'viewServiceSubTaskModal');


        $this->getViewHelper('HeadScript')->prependFile('/js/jobs/vehicle-dashboard.js');
        $this->getViewHelper('HeadLink')->prependStylesheet('/css/jobs/vehicle-dashboard.css');

        return $view;
    }

    public function getDashboardJobList($status, $offset = null) {

        $userLocation = $this->user_session->userActiveLocation['locationID'];
        $createdTimeStamp = $this->getGMTDateTime('Y-m-d H:i:s');
        $gmtStartDate = split(' ', $createdTimeStamp)[0]; 
        $startDate = $gmtStartDate.' 00:00:00';
        $endDate = date('Y-m-d H:i:s', strtotime("+1 day", strtotime($startDate)));
        $list = $this->CommonTable('Jobs/Model/JobTable')->getJobListByStatus($status, $userLocation, false, $startDate, $endDate, $offset, true);

        $currentDate = date('Y-m-d H:i:s');
        $date = $this->convertDateToStandardFormat($currentDate);

        $jobList = [];
        foreach ($list as $key => $value) {
            $dte = $this->getUserDateTime($value['createdTimeStamp'], 'Y-m-d H:i:s');
            $updtetime = $this->getUserDateTime($value['completed_at'], 'Y-m-d H:i:s');
            $value['createdTimeStamp'] = $dte;
            $value['completed_at'] = $updtetime;
            $jobList[] = $value;
           
        }

        return $jobList;
    }

    public function setListCount($length) {
        if (sizeof($length) == 0) {
            $count = '00';
        } else if ($length < 10) {
            $count = '0'.$length;
        } else {
            $count = $length;
        }

        return $count;
    }

    public function listAction() {

        $this->getSideAndUpperMenus('Job', null, 'Jobs');
        $pendingJobList = $this->getDashboardJobList(3);
        $inProgressJobList = $this->getDashboardJobList(8);
        $completedJobList = $this->getDashboardJobList(9);
        $closedJobList = $this->getDashboardJobList(4);
        $deletedJobList = $this->getDashboardJobList(5);
        $HoldJobList = $this->getDashboardJobList(17);
        $allInProgressJobList = array_merge($inProgressJobList, $HoldJobList); 

        foreach ($completedJobList as $key => $part) {
           $sort[$key] = strtotime($part['completed_at']);
        }
        array_multisort($sort, SORT_DESC, $completedJobList);



        $pendingJobCount = $this->setListCount(sizeof($pendingJobList));
        $inprogressJobCount = $this->setListCount(sizeof($allInProgressJobList));
        $completedJobCount = $this->setListCount(sizeof($completedJobList));
        $closedJobCount = $this->setListCount(sizeof($closedJobList));
        $deletedJobCount = $this->setListCount(sizeof($deletedJobList));

        $AddEmployeeView = new ViewModel();
        $AddEmployeeView->setTemplate('jobs/service-job-dashboard/view-employees-job-task-modal');

        $view = new ViewModel(array(
            'pendingJobList' => $pendingJobList,
            'inProgressJobList' => $allInProgressJobList,
            'completedJobList' => $completedJobList,
            'pendingJobCount' => $pendingJobCount,
            'inprogressJobCount' => $inprogressJobCount,
            'completedJobCount' => $completedJobCount,
            'closedJobCount' => $closedJobCount,
            'deletedJobCount' => $deletedJobCount,
        ));

        $view->setTemplate('jobs/service-job-dashboard/indexx');
        $view->addChild($AddEmployeeView, 'AddEmployeeView');
        $this->getViewHelper('HeadScript')->prependFile('/js/jobs/job-dashboard.js');
        return $view;   
    }
}
