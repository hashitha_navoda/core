<?php

namespace Jobs\Controller;

use Zend\View\Model\ViewModel;
use Core\Controller\CoreController;
use Zend\Session\Container;

class ServiceCostController extends CoreController
{

    
    protected $sideMenus = 'jobs_side_menu_of_service';

    public function indexAction()
    {   
    	$this->getSideAndUpperMenus('Costing', 'Costing', 'Jobs', NULL);

        $userDateFormat = $this->getUserDateFormat();
        $jobList = $this->getPaginatedCompletedJobs();
        $paginatedflag = true;
        $jobCompletedTimeData = $this->getJobCompletedTime();

        $jobView = new ViewModel(array(
            'jobList' => $jobList,
            'jobCompletedTimeData' => $jobCompletedTimeData,
            'paginated' => $paginatedflag,
            'status' => $this->getStatusesList(),
            'userDateFormat' => $userDateFormat,
        ));

        $this->getViewHelper('HeadScript')->prependFile('/js/jobs/service-cost-list.js');

        return $jobView;
    }

    public function getPaginatedCompletedJobs($param = null)
    {
        $this->job = $this->CommonTable('Jobs/Model/JobTable')->getCompletedJobsWithServices(true);
		if (is_null($param)) {
            $this->job->setCurrentPageNumber((int) $this->params()->fromRoute('param1', 1));
        } else {
            $this->job->setCurrentPageNumber((int) $param);
        }
		$this->job->setItemCountPerPage(6);
        return $this->job;
    }

    public function getJobCompletedTime(){
    	$completedTimes = $this->CommonTable('Jobs/Model/JobTable')->getCompletedJobsWithServices();

    	$completedTimeData = [];
    	foreach ($completedTimes as $value) {
            if (!is_null($value['completed_at'])) {
                $completedTimeData[$value['jobId']] = $this->getUserDateTime($value['completed_at']);
            }
        }

    	return $completedTimeData;
    }

    /**
     * This function is for create cost action
     */
    public function createCostAction()
    {
        $this->getSideAndUpperMenus('Costing', 'Costing', 'Jobs', NULL);
        $paramIn = $this->params()->fromRoute('param1');
        if ($paramIn != NULL) {
            $jobServiceData = $this->CommonTable('Jobs\Model\JobTable')->getCompletedJobsWithServices(false,$paramIn);
            
            $jobServiceDetails = [];
            foreach ($jobServiceData as $key => $row) {
                $tempG = array();
                $jobID = $row['jobId'];
                $tempG['jobId'] = $row['jobId'];
                $tempG['jobCode'] = $row['jobReferenceNumber'];
                $tempG['jobCustomer'] = $row['customerName'];
                $tempG['serviceVehicleRegNo'] = $row['serviceVehicleRegNo'];
                $tempG['jobCardName'] = $row['taskCardName'];
                $tempG['vehicalModal'] = $row['vehicleModelName'];
                $tempG['inDate'] = $this->getUserDateTime($row['createdTimeStamp']);
                $tempG['jobSupervisor'] = $row['userFirstName'].' '.$row['userLastName'];
                $tempG['jobStatus'] = $row['jobStatus'];
                $jobServiceDetails[$row['jobId']] = $tempG;
            }

            $jobCostView = new ViewModel(array(
                'jobID' => $jobID,
                'jobData' => $jobServiceDetails[$jobID],
                'companyCurrencySymbol' => $this->companyCurrencySymbol,
            ));

            $jobCostView->setTemplate('jobs/service-cost/service-costing');
            $this->getViewHelper('HeadScript')->prependFile('/js/jobs/service-cost.js');
            $this->setLogMessage("View cost ".$tempG['jobId'].".");
            return $jobCostView;
        } else {
            $this->setLogMessage("Redirect to cost list.");
            $this->redirect()->toRoute('serviceCost', array('action' => 'index'));
        }
    }


    /**
     * This function is for view cost action
     */
    public function viewCostAction()
    {
        $this->getSideAndUpperMenus('Costing', 'Costing', 'Jobs', NULL);
        $paramIn = $this->params()->fromRoute('param1');
        if ($paramIn != NULL) {
            $jobServiceData = $this->CommonTable('Jobs\Model\JobTable')->getCompletedJobsWithServices(false,$paramIn, false, true);
            
            $jobServiceDetails = [];
            $jobCardIDs = [];
            foreach ($jobServiceData as $key => $row) {
                $tempG = array();
                $jobID = $row['jobId'];
                $jobCardData = $this->CommonTable('Jobs\Model\JobTable')->getCompletedJobsWithServices(false,$paramIn)->current();

                $tempG['jobId'] = $row['jobId'];
                $tempG['jobCode'] = $row['jobReferenceNumber'];
                $tempG['jobCustomer'] = $row['customerName'];
                $tempG['jobCardName'] = $jobCardData['taskCardName'];
                $tempG['vehicalModal'] = $row['vehicleModelName'];
                $tempG['vehicleType'] = $row['vehicleType'];
                $tempG['inDate'] = $this->getUserDateTime($row['createdTimeStamp']);
                $tempG['jobSupervisor'] = $row['userFirstName'].' '.$row['userLastName'];
                $tempG['jobStatus'] = $row['jobStatus'];
                
                $jobServices = (isset($jobServiceDetails[$row['jobId']]['jobServices'])) ? $jobServiceDetails[$row['jobId']]['jobServices'] : array();
                $jobTradeMaterials = (isset($jobServiceDetails[$row['jobId']]['jobTradeMaterials'])) ? $jobServiceDetails[$row['jobId']]['jobTradeMaterials'] : array();
                $jobOtherCosts = (isset($jobServiceDetails[$row['jobId']]['jobOtherCosts'])) ? $jobServiceDetails[$row['jobId']]['jobOtherCosts'] : array();

                if ($row['jobProductID'] != null) {
                    $jobTradeMaterials[$row['jobProductID']] = array('jobProductMaterialTypeID' => $row['jobProductMaterialTypeID'],'jobProductID' => $row['jobProductID'],'locationProductID' => $row['locationProductID'], 'jobProductTaskID' => $row['jobProductTaskID'],'jobProductUnitPrice' => $row['productCost'], 'uomID' => $row['uomID'], 'jobProductIssuedQty' => $row['costedProductQty'],'productCode' => $row['productCode'], 'productName' => $row['productName'],'uomAbbr' => $row['uomAbbr'], 'total' => floatval($row['costedProductQty']) * floatval($row['productCost']));
                }

                if ($row['otherJobCostID'] != null) {
                    $jobOtherCosts[$row['otherJobCostID']] = array('otherJobCostUnitCost' => $row['otherJobCostUnitCost'],'otherJobCostNoOfUnits' => $row['otherJobCostNoOfUnits'],'otherCostProductCode' => $row['otherCostProductCode'], 'otherCostProductName' => $row['otherCostProductName'], 'total' => floatval($row['otherJobCostNoOfUnits']) * floatval($row['otherJobCostUnitCost']));
                }

                if (!empty($row['jobTaskTaskCardID'])) {
                    $jobCardIDs[]=$row['jobTaskTaskCardID'];
                }

                if ($row['jobTaskID'] != NULL && empty($row['jobTaskTaskCardID'])) {
                    $serviceKey = $row['jobTaskID'];

                    $serviceProducts = (isset($jobServices[$serviceKey]['serviceProducts'])) ? $jobServices[$serviceKey]['serviceProducts'] : array();
                    $serviceSubTasks = (isset($jobServices[$serviceKey]['serviceSubTasks'])) ? $jobServices[$serviceKey]['serviceSubTasks'] : array();

                    $jobServices[$serviceKey] = array('serviceCode' => $row['taskCode'], 'serviceName' => $row['taskName'],'jobServiceID' => $row['jobTaskID'],'serviceID' => $row['taskID'],'serviceQty' => $row['costedTaskQty'],'serviceRate' => $row['unitRate'], 'jobTaskTaskCardID'=>$row['jobTaskTaskCardID']);
                    
                    if ($row['jobProductID'] != NULL && $row['jobTaskID'] == $row['jobProductTaskID'] && $row['jobProductMaterialTypeID'] == 1) {
                        $serviceProducts[$row['jobProductID']] = array('locationProductID' => $row['locationProductID'], 'jobProductUnitPrice' => $row['jobProductUnitPrice'], 'uomID' => $row['uomID'], 'jobProductIssuedQty' => $row['jobProductIssuedQty'],'jobProductMaterialTypeID' => $row['jobProductMaterialTypeID'],'productCode' => $row['productCode'], 'productName' => $row['productName'],'uomAbbr' => $row['uomAbbr']);
                    }
                    
                    if ($row['jobSubTaskID'] != NULL && $row['jobTaskID'] == $row['jobTaskIdOfJobSubtask']) {
                        $serviceSubTasks[$row['jobSubTaskID']] = array('subTaskCode' => $row['subTaskCode'], 'subTaskName' => $row['subTaskName'],'subTaskID' => $row['subTaskId']);
                    }
                    
                    $jobServices[$serviceKey]['serviceProducts'] = $serviceProducts;
                    $jobServices[$serviceKey]['serviceSubTasks'] = $serviceSubTasks;
                }
                
                $tempG['jobServices'] = $jobServices;
                $tempG['jobTradeMaterials'] = $jobTradeMaterials;
                $tempG['jobOtherCosts'] = $jobOtherCosts;
                $jobServiceDetails[$row['jobId']] = $tempG;
            
            }


            $jobCardID = array_unique($jobCardIDs);
            if (sizeof($jobCardID) > 0 && sizeof($jobCardID) == 1) {
                $jobServiceDetails[$jobID]['jobCardID'] = $jobCardID[0];

                $getJobCostID = $this->CommonTable('Jobs\Model\JobCostTable')->getJobCostIDByJobID($jobID)->current();
                $jobCostID = $getJobCostID['jobCostID'];


                $getJobCardCostData = $this->CommonTable('Jobs\Model\JobCardCostTable')->getJobCardCostData($jobServiceDetails[$jobID]['jobCardID'], $jobCostID);
                $jobServiceDetails[$jobID]['jobCardRateDetails'][] = $getJobCardCostData;
                $jobServiceDetails[$jobID]['useJobCardServices'] = true;
            }

            $jobCostView = new ViewModel(array(
                'jobID' => $jobID,
                'jobData' => $jobServiceDetails[$jobID],
                'companyCurrencySymbol' => $this->companyCurrencySymbol,
            ));

            $jobCostView->setTemplate('jobs/service-cost/view-service-costing');
            $this->getViewHelper('HeadScript')->prependFile('/js/jobs/service-cost-view.js');
            $this->setLogMessage("View cost ".$tempG['jobId'].".");
            return $jobCostView;
        } else {
            $this->setLogMessage("Redirect to cost list.");
            $this->redirect()->toRoute('serviceCost', array('action' => 'index'));
        }
    }
}