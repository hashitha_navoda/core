<?php

namespace Jobs\Controller;

use Zend\View\Model\ViewModel;
use Core\Controller\CoreController;
use Zend\Session\Container;
use Jobs\Form\TaskCardForm;

class JobCardSetupController extends CoreController
{

    protected $sideMenus = 'jobs_side_menu_of_service';
    protected $upperMenus = 'category_setting_upper_menu_of_service';
    protected $downMenus = 'job_setup_down_menu_of_service';

    public function createAction()
    {   
        $this->getSideAndUpperMenus('Jobs Settings', 'Job Card Setup', 'Jobs', 'Category Settings');
        $taskCards = $this->getPaginatedJobCards();

        $AddServiceView = new ViewModel();
        $AddServiceView->setTemplate('jobs/job-card-setup/add-service-modal');
        $vehicleTypeResult = $this->CommonTable('Jobs\Model\VehicleTypeTable')->fetchAll();

        $vehicleTypes = [];
        foreach ($vehicleTypeResult as $value) {
            if ($value['vehicleTypeStatus'] == 1) {
                $temp['vehicleTypeID'] = $value['vehicleTypeID'];
                $temp['vehicleTypeName'] = $value['vehicleTypeName'];

                $vehicleTypes[] = $temp;
            }
        }

        $view = new ViewModel(array(
            'taskCards' => $taskCards,
            'vehicleTypes' => $vehicleTypes,
            'paginated' => true
        ));
        $view->addChild($AddServiceView, 'AddServiceView');

        $this->getViewHelper('HeadScript')->prependFile('/js/jobs/jobCard.js');

        return $view;


    }

    public function getPaginatedJobCards($count = 8, $param = null)
    {
        $this->paginator = $this->CommonTable('Jobs\Model\TaskCardTable')->fetchAll(true);

        if (is_null($param)) {
            $this->paginator->setCurrentPageNumber((int) $this->params()->fromRoute('param1', 1));
        } else {
            $this->paginator->setCurrentPageNumber((int) $param);
        }
        $perPage = $count;
        $this->paginator->setItemCountPerPage($perPage);
        $this->setLogMessage('task list accessed');
        return $this->paginator;
    }
}
