<?php

namespace Jobs\Controller;

use Zend\View\Model\ViewModel;
use Core\Controller\CoreController;
use Zend\Session\Container;

class ParticipantSubTypeController extends CoreController
{

    protected $sideMenus = 'tournament_side_menu';
    protected $upperMenus = 'participants_upper_menu';
    protected $downMenus = 'tournament_setup_down_menu';

    public function createAction()
    {       
        $this->getSideAndUpperMenus('Tournament Settings', 'Participants Sub Type', 'Jobs', 'Participants');

        //get designation list
        $paginator = $this->getPaginatedDesignations();
        $designationView = new ViewModel(array(
                                'designations' => $paginator,
                                'paginated' => true,
                                'isDesignationMaintain' => true
                            ));

        $designationView->setTemplate('jobs/participant-sub-type/create');
        $this->getViewHelper('HeadScript')->prependFile('/js/jobs/participantSubType.js');

        return $designationView;
    }

    public function getPaginatedDesignations($perPage = 6, $param = null)
    {
        $this->designation = $this->CommonTable('Jobs/Model/DesignationTable')->fetchAll(true);
        if ($param == null) {
            $this->designation->setCurrentPageNumber((int) $this->params()->fromRoute('param1', 1));
        } else {
            $this->designation->setCurrentPageNumber($param);     
        }
        $this->designation->setItemCountPerPage($perPage);
        return $this->designation;
    }
}
