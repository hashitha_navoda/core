<?php

namespace Jobs\Controller;

use Zend\View\Model\ViewModel;
use Core\Controller\CoreController;
use Zend\Session\Container;
use Jobs\Form\TaskCardForm;

class TaskCardController extends CoreController
{

    protected $sideMenus = 'jobs_side_menu';
    protected $upperMenus = 'task_settings_upper_menu';
    protected $downMenus = 'job_setup_down_menu';

    public function createAction()
    {   
        $this->getSideAndUpperMenus('Jobs Settings', 'Create Task Card', 'Jobs', 'Task Settings');

        $taskCardForm = new TaskCardForm();

        $uoms = $this->CommonTable('Settings/Model/UomTable')->getUomforSearch();
        $uomList = array();
        foreach ($uoms as $val) {
            $temp['value'] = $val->uomID;
            $temp['text'] = $val->uomName . '(' . $val->uomAbbr.')';
            $uomList[] = $temp;
        }
        $refCode = $this->getAvailableRefCode();

        $this->getViewHelper('HeadScript')->prependFile('/js/jobs/taskCard.js');

        $index = new ViewModel(array(
            'taskCardForm' => $taskCardForm,
            'companyCurrencySymbol' => $this->companyCurrencySymbol,
            'uomList' => $uomList,
            'refCode' => $refCode
        ));
    
        $taskCardProducts = new ViewModel();
        $taskCardProducts->setTemplate('/jobs/task-card/add-task-card-task-products');
        $index->addChild($taskCardProducts, 'addTaskCardTaskProducts');

        $taskCardMultiplePriceView = new ViewModel();
        $taskCardMultiplePriceView->setTemplate('/jobs/task-card/add-multiple-sales-price');
        $index->addChild($taskCardMultiplePriceView, 'taskCardMultiplePriceView');

        return $index;
    }

    public function listAction()
    {
        $this->setLogMessage('task card list accessed');
        $this->getSideAndUpperMenus('Jobs Settings', 'View Task Cards', 'Jobs', 'Task Settings');

        $this->getPaginatedTaskCards(8);
        
        $taskCardView = new ViewModel(array(
            'taskCards' => $this->paginator,
            'paginated' => true,
            ));
        
        return $taskCardView;
    }

    public function getPaginatedTaskCards($count = 8, $param = null)
    {
        $this->paginator = $this->CommonTable('Jobs\Model\TaskCardTable')->fetchAll(true);
        if (is_null($param)) {
            $this->paginator->setCurrentPageNumber((int) $this->params()->fromRoute('param1', 1));
        } else {
            $this->paginator->setCurrentPageNumber((int) $param);
        }

        $this->paginator->setItemCountPerPage($count);

        if (!is_null($param)) {
            return $this->paginator;
        }
    }

    public function viewTaskCardAction()
    {
        $paramIn = $this->params()->fromRoute('param1');
        if ($paramIn != NULL) {
            $this->getSideAndUpperMenus('Jobs Settings', 'View Task Cards', 'Jobs', 'Task Settings');
            $tcData = $this->CommonTable('Jobs\Model\TaskCardTable')->getTaskCardByTaskCardID($paramIn);
            $tcDetails = array();
            $tcID;
            // get task rates
            $taskRates = $this->CommonTable('Jobs\Model\TaskCardTaskUnitPriceTable')->getTaskRatesByTaskCardID($paramIn);
            $taskRate = [];
            foreach ($taskRates as $rateKey => $rateValue) {
                $taskRate[$rateValue['taskID']][] = $rateValue['rates'];
            }
            foreach ($tcData as $row) {
                $tempG = array();
                $tcID = $row['taskCardID'];
                $tempG['tcID'] = $row['taskCardID'];
                $tempG['tcCd'] = $row['taskCardCode'];
                $tempG['tcName'] = $row['taskCardName'];
                $tcTasks = (isset($tcDetails[$row['taskCardID']]['tcTasks'])) ? $tcDetails[$row['taskCardID']]['tcTasks'] : array();
                if ($row['taskID'] != NULL) {
                    $taskKey = $row['taskID'];

                    $taskProducts = (isset($tcTasks[$taskKey]['tcPro'])) ? $tcTasks[$taskKey]['tcPro'] : array();

                    $tcTasks[$taskKey] = array(
                        'tcTaskCode' => $row['taskCode'],
                        'tcTaskName' => $row['taskName'],
                        'defaultRate' => $row['taskCardTaskRate1'],
                        'rate2' => $row['taskCardTaskRate2'],
                        'rate3' => $row['taskCardTaskRate3'],
                        'taskUomName' => $row['taskCardUomName'],
                        'taskUomAbbr' => $row['taskCardUomAbbr']
                    );

                    $productUom = $this->CommonTable('Inventory\Model\ProductUomTable')->getProductUomListByProductID($row['productID']);

                    $thisqty = $this->getProductQuantityViaDisplayUom($row['taskCardTaskProductQty'], $productUom);
                    $thisqty['quantity'] = ($thisqty['quantity'] == 0) ? '-' : $thisqty['quantity'];

                    
                    if ($row['taskCardTaskProductID'] != NULL) {
                        $taskProducts[$row['taskCardTaskProductID']] = array(
                            'tcTaskProductCode' => $row['productCode'],
                            'tcTaskProductName' => $row['productName'],
                            'tcTaskProductQnty' => $thisqty['quantity'],
                            'tcTaskProductUom' => $row['tcProductUomAbbr']
                        );
                    }
                    
                    $tcTasks[$taskKey]['tcPro'] = $taskProducts;
                    $tcTasks[$taskKey]['tcRates'] = $taskRate[$taskKey];
                }
                $tempG['tcTasks'] = $tcTasks;
                $tcDetails[$row['taskCardID']] = $tempG;
            }
                        
            $taskCardForm = new TaskCardForm();
            $taskCardForm->get('taskCardCode')->setValue($tcDetails[$tcID]['tcCd']);
            $taskCardForm->get('taskCardCode')->setAttribute('disabled', TRUE);
            $taskCardForm->get('taskCardName')->setValue($tcDetails[$tcID]['tcName']);
            $taskCardForm->get('taskCardName')->setAttribute('disabled', TRUE);
            
            $tcSingleView = new ViewModel(array(
                    'tcID' => $tcDetails[$tcID]['tcID'],
                    'taskCardData' => $tcDetails[$tcID],
                    'taskCardForm' => $taskCardForm
                ));

            $tcSingleView->setTemplate('jobs/task-card/view-task-card');
            $this->setLogMessage("View task card ".$tempG['tcCd'].".");
            return $tcSingleView;
        } else {
            $this->setLogMessage("Redirect to task card list.");
            $this->redirect()->toRoute('taskCard', array('action' => 'list'));
        }
    }

    public function editAction()
    {
        $this->getSideAndUpperMenus('Jobs Settings', 'View Task Cards', 'Jobs', 'Task Settings');
        $taskCardID = $this->params()->fromRoute('param1');
        $taskCardData = $this->CommonTable('Jobs\Model\TaskCardTable')->getTaskCardByID($taskCardID);
        
        $taskCardForm = new TaskCardForm();

        $uoms = $this->CommonTable('Settings/Model/UomTable')->getUomforSearch();
        $uomList = array();
        foreach ($uoms as $val) {
            $temp['value'] = $val->uomID;
            $temp['text'] = $val->uomName . '(' . $val->uomAbbr.')';
            $uomList[] = $temp;
        }

        $taskCardForm->get('taskCardCode')->setValue($taskCardData->taskCardCode);
        $taskCardForm->get('taskCardName')->setValue($taskCardData->taskCardName);

        $tCard = new ViewModel(array(
            'taskCardForm' => $taskCardForm,
            'editedTaskCardID' => $taskCardID,
            'companyCurrencySymbol' => $this->companyCurrencySymbol,
            'uomList' => $uomList
        ));    
    
        $taskCardProducts = new ViewModel();
        $taskCardProducts->setTemplate('/jobs/task-card/add-task-card-task-products');
        $tCard->addChild($taskCardProducts, 'addTaskCardTaskProducts');

        $taskCardMultiplePriceView = new ViewModel();
        $taskCardMultiplePriceView->setTemplate('/jobs/task-card/add-multiple-sales-price');
        $tCard->addChild($taskCardMultiplePriceView, 'taskCardMultiplePriceView');


        return $tCard;
    }

    public function getAvailableRefCode()
    {
        $referenceService = $this->getService('ReferenceService');
        $refData = $referenceService->getReferenceNumber(38);
        $refCode = $refData['data']["referenceNo"];
        $locationReferenceID = $refData['data']["locationReferenceID"];
        while(!$this->CommonTable('Jobs\Model\TaskCardTable')->checkTaskCardCodeAvailability($refCode)) {
            $referenceService->updateReferenceNumber($locationReferenceID);
            $refData = $referenceService->getReferenceNumber(38);
            $refCode = $refData['data']["referenceNo"];
            $locationReferenceID = $refData['data']["locationReferenceID"];
        }
        return $refCode;
    }
}
