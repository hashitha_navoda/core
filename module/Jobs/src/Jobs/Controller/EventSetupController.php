<?php

namespace Jobs\Controller;

use Core\Controller\CoreController;
use Zend\Session\Container;
use Zend\View\Model\ViewModel;
use Zend\I18n\Translator\Translator;
use Jobs\Form\JobTypeForm;

class EventSetupController extends CoreController
{

	protected $sideMenus = 'tournament_side_menu';
    protected $upperMenus = 'general_setting_upper_menu_of_tournament';
	protected $downMenus = 'tournament_setup_down_menu';

    public function indexAction()
    {
    	$this->getSideAndUpperMenus('Tournament Settings', 'Event Setup', 'Jobs', 'General Settings');

    	$jobTypeList = $this->getPaginatedJobTypes();
        $form = new JobTypeForm();
        $view = new ViewModel(array(
            'form' => $form,
            'jobTypeList' => $jobTypeList,
            'paginated' => true
        ));
        $this->getViewHelper('HeadScript')->prependFile('/js/jobs/eventSetup.js');
        return $view;
    }

    public function getPaginatedJobTypes($perPage = 6, $param = null)
    {
        $this->paginator = $this->CommonTable('Jobs\Model\JobTypeTable')->fetchAll(true);
        
        if (is_null($param)) {
	        $this->paginator->setCurrentPageNumber((int) $this->params()->fromRoute('param1', 1));
        } else {
        	$this->paginator->setCurrentPageNumber((int) $param);
        }
        $this->paginator->setItemCountPerPage($perPage);
        $this->setLogMessage('Event Type list accessed');
        return $this->paginator;
    }
}
