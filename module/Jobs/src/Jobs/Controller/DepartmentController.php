<?php

namespace Jobs\Controller;

use Zend\View\Model\ViewModel;
use Core\Controller\CoreController;
use Zend\Session\Container;

class DepartmentController extends CoreController
{

    protected $sideMenus = 'jobs_side_menu';
    protected $upperMenus = 'jobs_employee_upper_menu';
    protected $downMenus = 'job_setup_down_menu';

    public function createAction()
    {   
        $this->getSideAndUpperMenus('Jobs Settings', 'Department', 'Jobs', 'Employee Settings');

        //get department list
        $paginator = $this->getPaginatedDepartments();
        $departmentView = new ViewModel(array(
                                'departments' => $paginator,
                                'paginated' => true
                            ));

        $departmentView->setTemplate('jobs/department/create');
        $this->getViewHelper('HeadScript')->prependFile('/js/jobs/department.js');

        return $departmentView;

    }

    public function getPaginatedDepartments($perPage = 6, $param = null)
    {
        $this->department = $this->CommonTable('Jobs/Model/DepartmentTable')->fetchAll(true);
        if ($param == null) {
            $this->department->setCurrentPageNumber((int) $this->params()->fromRoute('param1', 1));
        } else {
            $this->department->setCurrentPageNumber($param);     
        }
        $this->department->setItemCountPerPage($perPage);
        return $this->department;
    }
}
