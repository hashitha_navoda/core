<?php

namespace Jobs\Controller;

use Zend\View\Model\ViewModel;
use Core\Controller\CoreController;
use Zend\Session\Container;

class ServiceSetupController extends CoreController
{

    
    protected $sideMenus = 'jobs_side_menu_of_service';
    protected $upperMenus = 'category_setting_upper_menu_of_service';
	protected $downMenus = 'job_setup_down_menu_of_service';

    public function indexAction()
    {   
		$this->getSideAndUpperMenus('Jobs Settings', 'Services Setup', 'Jobs', 'Category Settings');

        $SubTaskAddView = new ViewModel();
        $SubTaskAddView->setTemplate('jobs/service-setup/sub-task-add-modal');

        $vehicleTypeResult = $this->CommonTable('Jobs\Model\VehicleTypeTable')->fetchAll();

        $vehicleTypes = [];
        foreach ($vehicleTypeResult as $value) {
            if ($value['vehicleTypeStatus'] == 1) {
                $temp['vehicleTypeID'] = $value['vehicleTypeID'];
                $temp['vehicleTypeName'] = $value['vehicleTypeName'];

                $vehicleTypes[] = $temp;
            }
        }

		$projectTypeList = $this->getPaginatedProjectTypes();
        $view = new ViewModel(array(
            'ServiceList' => $projectTypeList,
            'vehicleTypes' => $vehicleTypes,
            'paginated' => true
        ));
        $view->addChild($SubTaskAddView, 'SubTaskAddView');


        $this->getViewHelper('HeadScript')->prependFile('/js/jobs/serviceSetup.js');

        return $view;
    }
    
    public function getPaginatedProjectTypes($perPage = 6, $param = null)
    {
        $this->paginator = $this->CommonTable('Jobs\Model\TaskTable')->fetchAll(true);
        
        if (is_null($param)) {
            $this->paginator->setCurrentPageNumber((int) $this->params()->fromRoute('param1', 1));
        } else {
            $this->paginator->setCurrentPageNumber((int) $param);
        }
        $this->paginator->setItemCountPerPage($perPage);
        $this->setLogMessage('task list accessed');
        return $this->paginator;
    }
}
