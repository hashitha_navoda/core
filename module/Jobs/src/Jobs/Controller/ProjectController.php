<?php

namespace Jobs\Controller;

use Zend\View\Model\ViewModel;
use Core\Controller\CoreController;
use Zend\Session\Container;
use Invoice\Form\AddCustomerForm;

class ProjectController extends CoreController
{

    protected $sideMenus = 'jobs_side_menu';
    protected $upperMenus = 'jobs_project_upper_menu';


    public function __construct()
    {
        parent::__construct();
        $this->user_session = new Container('ezBizUser');
        $this->userID = $this->user_session->userID;
        $this->username = $this->user_session->username;
        $this->company = $this->user_session->companyDetails;
        $this->cdnUrl = $this->user_session->cdnUrl;
        $this->useAccounting = $this->user_session->useAccounting;
    }

    public function indexAction()
    {   
        $projectID = $this->params()->fromRoute('param1');
        $mode = $this->params()->fromRoute('param2');

        return ($projectID) ? $this->projectCreate($projectID, $mode): $this->projectCreate(); 
    }
    public function initiateBussiness($jobBusinessType) 
    {
        if (is_null($jobBusinessType)) {
            // get business type details 
            $confDetails = $this->getServiceLocator()->get('config');
            
            $initiateModel = new ViewModel(
                array(
                    'business_type' => $confDetails['business_types'],
                    ));

            $initiateModel->setTemplate('jobs/general-setup/job-wizard');
            $this->getViewHelper('HeadScript')->prependFile('/js/jobs/index.js');
        } else {
            $users = array();
            $userResult = $this->CommonTable('User\Model\UserTable')->getAllUsersWithLicenseData();
            foreach ($userResult as $user) {
                if ($user['userActivated'] == 1 && $user['deleted'] == 0 && $user['licenceName'] != null) {
                    $users[$user['userID']] = $user['userFirstName'] . ' ' . $user['userLastName'];
                }
            }

            $userRoleSetting = false;
            if ($this->user_session->roleID == 1) {
                $userRoleSetting = true;
            }

            $initiateModel = new ViewModel(array(
                    'wizardType' => true,
                    'users' => $users,
                    'userRoleSetting' => $userRoleSetting
            ));
            
            $initiateModel->setTemplate('jobs/project/general-setup');
            $this->getViewHelper('HeadScript')->prependFile('/js/jobs/general-setup.js');
        }


        return $initiateModel;
        
    }

    public function projectCreate($projectId = null, $mode = false) {

        $this->getSideAndUpperMenus('Projects', 'Create Project', 'Jobs');
        if ($projectId) {
            $this->getSideAndUpperMenus('Projects', 'Project List', 'Jobs');
        }
        $dateFormat = $this->getUserDateFormat();

        $currencyResultSet = $this->CommonTable('Core\Model\CurrencyTable')->fetchAll();
        $currencies = array();
        $sup = array();
        $tax = array();
        $ctax = array();
        $productType = array();
        $location = array();
        $currencies['0'] = '';
        foreach ($currencyResultSet as $row) {
            $currencies[$row->currencyID] = $row->currencyName;
        }
        $customerCategories[''] = '';
        $existingCustomerCategories = $this->CommonTable('Invoice\Model\CustomerCategoryTable')->fetchAllForDorpDown();
        $customerCategories = $customerCategories + $existingCustomerCategories;

        //price list
        $customerPriceLists[''] = '';
        $existingCustomerPriceLists = $this->CommonTable('Inventory\Model\PriceListTable')->fetchAllForDorpDown();
        $customerPriceLists = $customerPriceLists + $existingCustomerPriceLists;

        $paymentTermsResultSet = $this->CommonTable('Core\Model\PaymentTermTable')->fetchAll();
        $paymentTerms = array();
        foreach ($paymentTermsResultSet as $row) {
            $paymentTerms[$row->paymentTermID] = $row->paymentTermName;
        }


        // get person Title (Mr, Miss ..) from personTitle.config file
        $config = $this->getServiceLocator()->get('config');
        $customerTitle = $config['personTitle'];
        $countries = ['' => ''] + $config['all_countries'];
        $prorefData = $this->getReferenceNoForLocation(21, $locationID);

        $cusRefData = $this->getReferenceNoForLocation(20, $locationID);
        $cusRid = $refData["refNo"];
        $locationReferenceID = $cusRefData["locRefID"];
        $customerCode = $this->getReferenceNumber($locationReferenceID);

        $currencyValue = $this->getDefaultCurrency();
        $customerForm = new AddCustomerForm(NULL, $paymentTerms, $currencies, $customerTitle, $currencyValue,$customerCategories, "", null,[], $customerPriceLists);
        $customerForm->get('customerFormsubmit')->setValue(_("Add New Customer"));
        $customerForm->get('customerCode')->setAttribute('value', $customerCode);

        $financeAccountsArray = $this->getFinanceAccounts();

        $this->addGLAcccountDetails($customerForm, $financeAccountsArray);

        $projectManagers = $this->CommonTable('Jobs\Model\EmployeeDesignationTable')->fetchEmployeeByDesignationCode('DES001');
        $projectSupervisors = $this->CommonTable('Jobs\Model\EmployeeDesignationTable')->fetchEmployeeByDesignationCode('DES002');


        $customerAddView = new ViewModel(array(
            'customerForm' => $customerForm,
            'countries' => $countries,
            'useAccounting' => $this->useAccounting
        ));

        $projectManagerAddView = new ViewModel(array(
            'projectManagers' => $projectManagers,
        ));

        $projectSupervisorAddView = new ViewModel(array(
            'projectSupervisors' => $projectSupervisors,
        ));

        $projectMaterialAddView = new ViewModel(array(
            'projectSupervisors' => $projectSupervisors,
        ));

        if  ($projectId) {
            $projectRelatedJobs = $this->CommonTable('Jobs\Model\JobTable')->getJobDetailsAccordingToProject($projectId);
        }
        

        $projectTypes = $this->CommonTable('Jobs\Model\ProjectTypeTable')->searchProjectTypesForDropDown();
    
        $projectView = new ViewModel(array(
            'dateFormat' => $dateFormat,
            'projectCode' => ($projectId)? $projectData['projectCode'] : $prorefData["refNo"],
            'currency' => $this->companyCurrencySymbol,
            'mode' => ($projectId) ? $mode: 'create',
            'projectId' => $projectId,
            'projectJobs' => ($projectId) ? $projectRelatedJobs : [],
            'projectTypes' => $projectTypes

        ));

        $customerAddView->setTemplate('invoice/customer/customer-add-modal');
        $projectManagerAddView->setTemplate('jobs/project/project-manager-add-modal');
        $projectSupervisorAddView->setTemplate('jobs/project/project-supervisor-add-modal');
        $projectMaterialAddView->setTemplate('jobs/project/project-materials-add-modal');
        $projectView->setTemplate('jobs/project/create');

        $projectView->addChild($customerAddView, 'customerAddView');
        $projectView->addChild($projectManagerAddView, 'projectManagerAddView');
        $projectView->addChild($projectSupervisorAddView, 'projectSupervisorAddView');
        $projectView->addChild($projectMaterialAddView, 'projectMaterialAddView');

        $this->getViewHelper('HeadScript')->prependFile('/js/invoice/modalAddCustomer.js');
        $this->getViewHelper('HeadScript')->prependFile('/js/jobs/project.js');
        return $projectView;   
    }

    //list action of project
    public function listAction()
    {

        // get display setup details
        $displaySetupDetails = $this->CommonTable('Settings\Model\DisplaySetupTable')->fetchAllDetails()->current();
        
        if ($displaySetupDetails['jobWizardStatus'] == 0) {
            return $this->initiateBussiness($displaySetupDetails['jobBusinessType']); 
            
        } else {        
            $this->getSideAndUpperMenus('Projects', 'Project List', 'Jobs');
            $paginator = $this->getPaginatedProjects();

            $projectManagers = $this->CommonTable('Jobs\Model\EmployeeDesignationTable')->fetchEmployeeByDesignationCode('DES001');
            $projectTypes = $this->CommonTable('Jobs\Model\ProjectTypeTable')->searchProjectTypesForDropDown();

            $projectList = new ViewModel(array(
                'project_list' => $paginator,
                'status' => $this->getStatusesList(),
                'userDateTimeFormat' => $this->convertUserDateFormatToPhpFormat() . ' H:i',
                'paginated' => true
            ));

            $projectList->setTemplate('jobs/project/project-list');
            $projectListView = new ViewModel(['userDateFormat' => $this->getUserDateFormat(), 'projectManagers' => $projectManagers,'projectTypes' => $projectTypes,]);
            $projectListView->addChild($projectList, 'projectList');

            $this->getViewHelper('HeadScript')->prependFile('/js/jobs/project-list.js');
            return $projectListView;

        }

    }

    public function addGLAcccountDetails($customerForm, $financeAccountsArray) 
    {
        $glAccountExist = $this->CommonTable('Accounting\Model\GlAccountSetupTable')->fetchAll();
        if ($this->useAccounting == 1) {
            $glAccountSetupData = $glAccountExist->current();

            if($glAccountSetupData->glAccountSetupSalesAndCustomerReceivableAccountID != ''){
                $customerReceviableAccountID = $glAccountSetupData->glAccountSetupSalesAndCustomerReceivableAccountID;
                $customerReceviableAccountName = $financeAccountsArray[$customerReceviableAccountID]['financeAccountsCode']."_".$financeAccountsArray[$customerReceviableAccountID]['financeAccountsName'];
                $customerForm->get('customerReceviableAccountID')->setAttribute('data-id',$customerReceviableAccountID);
                $customerForm->get('customerReceviableAccountID')->setAttribute('data-value',$customerReceviableAccountName);
            }

            if($glAccountSetupData->glAccountSetupSalesAndCustomerSalesAccountID != ''){
                $customerSalesAccountID = $glAccountSetupData->glAccountSetupSalesAndCustomerSalesAccountID;
                $customerSalesAccountName = $financeAccountsArray[$customerSalesAccountID]['financeAccountsCode']."_".$financeAccountsArray[$customerSalesAccountID]['financeAccountsName'];
                $customerForm->get('customerSalesAccountID')->setAttribute('data-id',$customerSalesAccountID);
                $customerForm->get('customerSalesAccountID')->setAttribute('data-value',$customerSalesAccountName);
            }

            if($glAccountSetupData->glAccountSetupSalesAndCustomerSalesDiscountAccountID != ''){
                $customerSalesDiscountAccountID = $glAccountSetupData->glAccountSetupSalesAndCustomerSalesDiscountAccountID;
                $customerSalesDiscountAccountName = $financeAccountsArray[$customerSalesDiscountAccountID]['financeAccountsCode']."_".$financeAccountsArray[$customerSalesDiscountAccountID]['financeAccountsName'];
                $customerForm->get('customerSalesDiscountAccountID')->setAttribute('data-id',$customerSalesDiscountAccountID);
                $customerForm->get('customerSalesDiscountAccountID')->setAttribute('data-value',$customerSalesDiscountAccountName);
            }

            if($glAccountSetupData->glAccountSetupSalesAndCustomerAdvancePaymentAccountID != ''){
                $customerAdvancePaymentAccountID = $glAccountSetupData->glAccountSetupSalesAndCustomerAdvancePaymentAccountID;
                $customerAdvancePaymentAccountName = $financeAccountsArray[$customerAdvancePaymentAccountID]['financeAccountsCode']."_".$financeAccountsArray[$customerAdvancePaymentAccountID]['financeAccountsName'];
                $customerForm->get('customerAdvancePaymentAccountID')->setAttribute('data-id',$customerAdvancePaymentAccountID);
                $customerForm->get('customerAdvancePaymentAccountID')->setAttribute('data-value',$customerAdvancePaymentAccountName);
            }
        }

    }

    public function getPaginatedProjects($perPage = 6, $param = null)
    {
        $this->employee = $this->CommonTable('Jobs/Model/projectTable')->fetchAll(true);

        if ($param == null) {
            $this->employee->setCurrentPageNumber((int) $this->params()->fromRoute('param1', 1));
        } else {
            $this->employee->setCurrentPageNumber($param);     
        }
        $this->employee->setItemCountPerPage($perPage);
        return $this->employee;
    }
    
}
