<?php

namespace Jobs\Controller;

use Zend\View\Model\ViewModel;
use Core\Controller\CoreController;
use Zend\Session\Container;

class GeneralSetupController extends CoreController
{

    protected $sideMenus = 'jobs_side_menu';
    protected $upperMenus = 'general_setting_upper_menu';
	protected $downMenus = 'job_setup_down_menu';

    public function indexAction()
    {   
		$this->getSideAndUpperMenus('Jobs Settings', 'Basic Setup', 'Jobs', 'General Settings');

    	$users = array();
    	$userResult = $this->CommonTable('User\Model\UserTable')->getAllUsersWithLicenseData();
        foreach ($userResult as $user) {
        	if ($user['userActivated'] == 1 && $user['deleted'] == 0 && $user['licenceName'] != null) {
	            $users[$user['userID']] = $user['userFirstName'] . ' ' . $user['userLastName'];
        	}
        }

        $userRoleSetting = false;
        if ($this->user_session->roleID == 1) {
            $userRoleSetting = true;
        }

        $view = new ViewModel(array(
            'users' => $users,
            'userRoleSetting' => $userRoleSetting
        ));

        $this->getViewHelper('HeadScript')->prependFile('/js/jobs/general-setup.js');

        return $view;
    }

}
