<?php

namespace Jobs\Controller;

use Zend\View\Model\ViewModel;
use Core\Controller\CoreController;
use Zend\Session\Container;

class VehicleController extends CoreController
{

    protected $sideMenus = 'jobs_side_menu';
    protected $upperMenus = 'resource_setting_upper_menu';
	protected $downMenus = 'job_setup_down_menu';

    public function createAction()
    {   
		$this->getSideAndUpperMenus('Jobs Settings', 'Vehicles Setup', 'Jobs', 'Vehicles Settings');
        $this->getPaginatedVehicles();

        $vehiclesList = new ViewModel(array(
            'companyCurrencySymbol' => $this->companyCurrencySymbol,
            'vehiclesList' => $this->paginator,
            'paginated' => true,
        ));
        $vehiclesList->setTemplate('jobs/vehicle/vehicles-list');
        $vehiclesSetupView = new ViewModel(array(
            'companyCurrencySymbol' => $this->companyCurrencySymbol,
        ));
        $vehiclesSetupView->addChild($vehiclesList, 'vehiclesList');
        return $vehiclesSetupView;
    }

    public function getPaginatedVehicles($perPage = 6, $param = null)
    {
        $this->paginator = $this->CommonTable('Jobs\Model\VehicleTable')->fetchAllActiveVehicles(TRUE);
        if ($param == null) {
            $this->paginator->setCurrentPageNumber((int) $this->params()->fromRoute('param1', 1));
        } else {
            $this->paginator->setCurrentPageNumber($param);     
        }
        $this->paginator->setItemCountPerPage($perPage);
        return $this->paginator;
    }

}
