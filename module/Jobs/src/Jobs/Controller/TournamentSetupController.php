<?php

namespace Jobs\Controller;

use Zend\View\Model\ViewModel;
use Core\Controller\CoreController;
use Zend\Session\Container;

class TournamentSetupController extends CoreController
{

    
    protected $sideMenus = 'tournament_side_menu';
    protected $upperMenus = 'general_setting_upper_menu_of_tournament';
	protected $downMenus = 'tournament_setup_down_menu';

    public function indexAction()
    {   
		$this->getSideAndUpperMenus('Tournament Settings', 'Tournament Setup', 'Jobs', 'General Settings');

		$projectTypeList = $this->getPaginatedProjectTypes();
        $view = new ViewModel(array(
            'projectTypeList' => $projectTypeList,
            'paginated' => true
        ));

        $this->getViewHelper('HeadScript')->prependFile('/js/jobs/tournamentType.js');

        return $view;
    }
    
    public function getPaginatedProjectTypes($perPage = 6, $param = null)
    {
        $this->paginator = $this->CommonTable('Jobs\Model\ProjectTypeTable')->fetchAll(true);
        
        if (is_null($param)) {
	        $this->paginator->setCurrentPageNumber((int) $this->params()->fromRoute('param1', 1));
        } else {
        	$this->paginator->setCurrentPageNumber((int) $param);
        }
        $this->paginator->setItemCountPerPage($perPage);
        $this->setLogMessage('Tournament Type list accessed');

        return $this->paginator;
    }
}
