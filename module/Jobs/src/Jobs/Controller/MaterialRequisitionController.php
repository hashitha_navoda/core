<?php

namespace Jobs\Controller;

use Zend\View\Model\ViewModel;
use Core\Controller\CoreController;
use Zend\Session\Container;
use Zend\I18n\Translator\Translator;

class MaterialRequisitionController extends CoreController
{

    protected $sideMenus = 'jobs_side_menu';
    protected $upperMenus = 'jobs_material_requisition_upper_menu';

    public function indexAction()
    {
        if ($this->user_session->jobStoreKeeper) {
            $this->getSideAndUpperMenus('Material Requisition', 'Approve Material', 'Jobs');
        } else {
            $this->getSideAndUpperMenus('Material Requisition', null, 'Jobs');
        }

        $requests = $this->getService('MaterialRequisitionService')->getPaginatedRequests();

        $paginatedData = $requests['data'];
        $paginatedData->setCurrentPageNumber($this->params()->fromRoute('param1', 1));
        $paginatedData->setItemCountPerPage(8);

        $listView = new ViewModel(['requests' => $paginatedData]);
        $listView->setTemplate('jobs/material-requisition/list-admin.phtml');

        $requestListView = new ViewModel();
        $requestListView->setTemplate('jobs/material-requisition/index-admin.phtml');
        $requestListView->addChild($listView, 'requestsList');

        return $requestListView;
    }

    public function materialIssueAction()
    {
        if ($this->user_session->jobAdmin) {
            $this->getSideAndUpperMenus('Material Requisition', 'Issue Material', 'Jobs');
        } else {
            $this->getSideAndUpperMenus('Material Requisition', null, 'Jobs');
        }

        $requests = $this->getService('MaterialRequisitionService')->getPaginatedRequests();

        $paginatedData = $requests['data'];
        $paginatedData->setCurrentPageNumber($this->params()->fromRoute('param1', 1));
        $paginatedData->setItemCountPerPage(8);

        $listView = new ViewModel([
            'requests' => $paginatedData,
            'status' => $this->getStatusesList()
        ]);
        $listView->setTemplate('jobs/material-requisition/list-store.phtml');

        $requestListView = new ViewModel();
        $requestListView->setTemplate('jobs/material-requisition/index-store.phtml');
        $requestListView->addChild($listView, 'requestsList');
        return $requestListView;
    }

    public function searchAction()
    {
        $request = $this->getRequest();
        if (!$request->isPost()) {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_DATA_SET');
            return $this->JSONRespond();
        }

        $key = $request->getPost('searchKey');
        $fromDate = $request->getPost('fromDate');
        $toDate = $request->getPost('toDate');
        $fixDate = $request->getPost('fixDate');

        if ($key == "") {
            $res = $this->getService('MaterialRequisitionService')->getPaginatedRequests();
        } else {
            $service = $this->getService('MaterialRequisitionService');
            $res = $service->findRequests($key, $fromDate, $toDate, $fixDate);
        }


        $paginatedData = $res['data'];
        $paginatedData->setCurrentPageNumber($this->params()->fromRoute('param1', 1));
        $paginatedData->setItemCountPerPage(8);

        $requestListView = new ViewModel([
            'requests' => $paginatedData,
            'status' => $this->getStatusesList()
        ]);

        $admin = false;
        if ($this->user_session->jobAdmin) {
            $requestListView->setTemplate('jobs/material-requisition/list-admin.phtml');
        } else {
            $requestListView->setTemplate('jobs/material-requisition/list-store.phtml');
        }
        $requestListView->setTerminal(true);

        $this->html = $requestListView;
        return $this->JSONRespondHtml();
    }

    public function documentPreviewAction()
    {
        $reuestId = $this->params()->fromRoute('param1');
        $data = $this->getDataForDocument($reuestId);
        

        $path = "/material-requisition/document/"; 
        $translator = new Translator();
        
        $data["email"] = array(
            "to" => '',
            "subject" => "Material Requisition Note from " . $data['companyName'],
            "body" => <<<EMAILBODY


Thank you for your business. <br /><br />

Below Material Requisition note is attached herewith. <br /><br />

<strong>Request Code:</strong> {$data['mRCode']} <br />
<strong>Request Date:</strong> {$data['mRDate']} <br />
<strong>Job Name:</strong> {$data['jobName']} <br /><br />

Best Regards, <br />
{$data['companyName']}

EMAILBODY
        );
        $documentType = 'Material Requisition Note';
              
        $jobView = $this->getCommonPreview($data, $path, null, $documentType, $reuestId, null);
        $jobView->setTerminal(TRUE);

        return $jobView;
    }

    public function getDataForDocument($requestID)
    {
        if (!empty($this->_materialRequisitionViewData)) {
            return $this->_materialRequisitionViewData;
        }
        //get company details
        $data = $this->getDataForDocumentView();

        //get journalEntry Details
        $MaterialRequisitionDetails = $this->CommonTable('Jobs\Model\MaterialRequestTable')->getMaterialRequestByMaterialRequestId($requestID);
        $MaterialRequisitionDeta = [];
        foreach ($MaterialRequisitionDetails as $key => $value) {
            $MaterialRequisitionDeta['mRCode'] = $value['materialRequestCode'];
            $MaterialRequisitionDeta['mRDate'] = $value['materialRequestDate'];
            $MaterialRequisitionDeta['jobName'] = $value['jobName'];
            $MaterialRequisitionDeta['projectName'] = $value['projectName'];
            $MaterialRequisitionDeta['customerName'] = $value['customerTitle'].'.'.$value['customerName'];

            $productSet = array(
                'itemCode' => $value['productCode'],
                'itemName' => $value['productName'],
                'requestedQty' => floatval($value['requestedQuantity']).' '.$value['uomAbbr']
            );
            $MaterialRequisitionDeta['products'][] = $productSet;
            
        }
        
        $completeDataSet = array_merge($data, $MaterialRequisitionDeta);
       
        
        return $this->_materialRequisitionViewData = $completeDataSet;
    }

    public function documentAction()
    {        

        $documentType = 'Material Requisition Note';
        $requestedID = $this->params()->fromRoute('param1');
        $templateID = $this->params()->fromRoute('param2');
        $sm = $this->getServiceLocator();
        $tpl = $sm->get('Template');

        // If template ID is not passed, get default template
        if (!$templateID) {
            $defaultTpl = $tpl->getDefaultTemplate($documentType);
            $templateID = $defaultTpl['templateID'];
        }

        // if saved document file is available, render it
        $filename = $this->getDocumentFileName($documentType, $requestedID, $templateID);

        // Get template details
        $templateDetails = $tpl->getTemplateByID($templateID);

        $data = $this->getDataForDocument($requestedID);

        // Get data table (eg: products list, total, etc.)
        $dataTableView = new ViewModel(array());
        $dataTableView->setTerminal(TRUE);

        switch (strtolower($templateDetails['documentSizeName'])) {
            case 'a4 (portrait)':
            case 'a4 (landscape)':
            case 'a5 (portrait)':
            case 'a5 (landscape)':
                $data['data_table'] = $this->getDocumentDataTable($requestedID, strtolower(trim(preg_replace('/\(.*\)/i', '', $templateDetails['documentSizeName']))));
                break;
        }
        echo $tpl->renderTemplateByID($templateID, $data, $filename);
        exit;
        ///////////////
    }

    public function getDocumentDataTable($requestedID, $documentSize = 'A4')
    {
        
        $data_table_vars = $this->getDataForDocument($requestedID);
        
        $view = new ViewModel($data_table_vars);
        $view->setTemplate('/jobs/material-requisition/templates/' . strtolower($documentSize));
        $view->setTerminal(TRUE);

        return $this->getServiceLocator()
                        ->get('viewrenderer')
                        ->render($view);
    }

}
