<?php

namespace Jobs\Controller;

use Zend\View\Model\ViewModel;
use Core\Controller\CoreController;
use Zend\Session\Container;

class TournamentTeamController extends CoreController
{

    protected $sideMenus = 'tournament_side_menu';
    protected $upperMenus = 'participants_upper_menu';
    protected $downMenus = 'tournament_setup_down_menu';

    public function createAction()
    {   
        $this->getSideAndUpperMenus('Tournament Settings', 'Teams', 'Jobs', 'Participants');


        $paginator = $this->getPaginatedTeams();
        $this->department = $this->CommonTable('Jobs/Model/DepartmentTable')->fetchActiveDepartments();

        $departments = array();
        foreach ($this->department as $dept) {
            $res = [
                'departmentID' => $dept['departmentID'],
                'departmentName' => $dept['departmentName'],
                'departmentCode' => $dept['departmentCode'],
            ];
            array_push($departments, $res);
        }

        $this->designation = $this->CommonTable('Jobs/Model/DesignationTable')->fetchActiveDesignations();
        $designations = array();
        foreach ($this->designation as $desig) {
            $res = [
                'designationID' => $desig['designationID'],
                'designationName' => $desig['designationName'],
                'designationCode' => $desig['designationCode'],
            ];
            array_push($designations, $res);
        }


        $teamView = new ViewModel(array(
                                'paginated' => true,
                                'teams' => $paginator
                            ));
        $teamView->setTemplate('jobs/tournament-team/create');

        $participantAddView = new ViewModel(array(
                'departments' => $departments,             
                'designations' => $designations,             
        ));
        $participantAddView->setTemplate('jobs/tournament-team/tournament-participant-add-modal');
        $teamView->addChild($participantAddView, 'participantAddView');
        
        $this->getViewHelper('HeadScript')->prependFile('/js/jobs/tournamentTeam.js');
        return $teamView;
    }

    public function getPaginatedTeams($perPage = 6, $param = null)
    {
        $this->team = $this->CommonTable('Jobs/Model/TeamTable')->fetchAll(true);
        if ($param == null) {
            $this->team->setCurrentPageNumber((int) $this->params()->fromRoute('param1', 1));
        } else {
            $this->team->setCurrentPageNumber($param);     
        }
        $this->team->setItemCountPerPage($perPage);
        return $this->team;
    }

}
