<?php

namespace Jobs\Controller;

use Zend\View\Model\ViewModel;
use Core\Controller\CoreController;
use Zend\Session\Container;

class VehicleReleaseController extends CoreController
{

    
    protected $sideMenus = 'jobs_side_menu_of_service';

    public function indexAction()
    {   
    	$this->getSideAndUpperMenus('Vehicle Release', 'Vehicle Release', 'Jobs', NULL);

        $userDateFormat = $this->getUserDateFormat();
        $jobList = $this->getPaginatedInvoicedJobs();
        $paginatedflag = true;
        $jobInTimeData = $this->getJobInTime();

        $jobView = new ViewModel(array(
            'jobList' => $jobList,
            'jobInTimeData' => $jobInTimeData,
            'paginated' => $paginatedflag,
            'status' => $this->getStatusesList(),
            'userDateFormat' => $userDateFormat,
        ));

        $this->getViewHelper('HeadScript')->prependFile('/js/jobs/vehicle-release.js');

        return $jobView;
    }

    public function getPaginatedInvoicedJobs($param = null)
    {
        $this->job = $this->CommonTable('Jobs/Model/JobTable')->getCompletedJobsWithServices(true,null,true,false,true);
        if (is_null($param)) {
            $this->job->setCurrentPageNumber((int) $this->params()->fromRoute('param1', 1));
        } else {
            $this->job->setCurrentPageNumber((int) $param);
        }
        $this->job->setItemCountPerPage(6);
        return $this->job;
    }

    public function getJobInTime(){
        $inTimes = $this->CommonTable('Jobs/Model/JobTable')->getCompletedJobsWithServices(false,null,true,false,true);

        $inTimeData = [];
        foreach ($inTimes as $value) {
            if (!is_null($value['createdTimeStamp'])) {
                $inTimeData[$value['jobId']] = $this->getUserDateTime($value['createdTimeStamp']);
            }
        }

        return $inTimeData;
    }
}