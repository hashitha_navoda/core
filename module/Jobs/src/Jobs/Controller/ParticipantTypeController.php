<?php

namespace Jobs\Controller;

use Zend\View\Model\ViewModel;
use Core\Controller\CoreController;
use Zend\Session\Container;

class ParticipantTypeController extends CoreController
{

    protected $sideMenus = 'tournament_side_menu';
    protected $upperMenus = 'participants_upper_menu';
    protected $downMenus = 'tournament_setup_down_menu';

    public function createAction()
    {   
        $this->getSideAndUpperMenus('Tournament Settings', 'Participants Type', 'Jobs', 'Participants');

        //get department list
        $paginator = $this->getPaginatedDepartments();
        $departmentView = new ViewModel(array(
                                'departments' => $paginator,
                                'paginated' => true
                            ));

        $departmentView->setTemplate('jobs/participant-type/create');
        $this->getViewHelper('HeadScript')->prependFile('/js/jobs/participantType.js');

        return $departmentView;

    }

    public function getPaginatedDepartments($perPage = 6, $param = null)
    {
        $this->department = $this->CommonTable('Jobs/Model/DepartmentTable')->fetchAll(true);
        if ($param == null) {
            $this->department->setCurrentPageNumber((int) $this->params()->fromRoute('param1', 1));
        } else {
            $this->department->setCurrentPageNumber($param);     
        }
        $this->department->setItemCountPerPage($perPage);
        return $this->department;
    }
}
