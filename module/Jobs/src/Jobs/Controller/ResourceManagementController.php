<?php

namespace Jobs\Controller;

use Zend\View\Model\ViewModel;
use Core\Controller\CoreController;
use Zend\Session\Container;

class ResourceManagementController extends CoreController
{

    
    protected $sideMenus = 'jobs_side_menu';
    protected $upperMenus = 'resource_management_upper_menu';

    public function indexAction()
    {   
		$this->getSideAndUpperMenus('Resource Management', 'Resource Management', 'Jobs', NULL);

        $maintainProject = $this->user_session->jobSettings->maintainProject;
        $locationID = $this->user_session->userActiveLocation['locationID'];
        $jobList = $this->getPaginatedJob($locationID);

        $resourceManagementView = new ViewModel(array(
            'maintainProject' => $maintainProject,
            'userdateFormat' => $this->getUserDateFormat(),
            'contractorForJob' => $this->user_session->jobSettings->contractorForJob,
            'jobs' => $jobList,
            'paginated' => true
        ));

        $this->getViewHelper('HeadScript')->prependFile('/js/jobs/resource-management.js');

        return $resourceManagementView;
    }

    public function getPaginatedJob($locationID, $param = null)
    {
        $this->job = $this->CommonTable('Jobs/Model/JobTable')->getJobListForRespourceManagement($locationID, true);
        if (is_null($param)) {
            $this->job->setCurrentPageNumber((int) $this->params()->fromRoute('param1', 1));
        } else {
            $this->job->setCurrentPageNumber((int) $param);
        }
        $this->job->setItemCountPerPage(6);
        return $this->job;
    }
    
    public function getPaginatedProjectTypes($perPage = 6, $param = null)
    {
        $this->paginator = $this->CommonTable('Jobs\Model\ProjectTypeTable')->fetchAll(true);
        
        if (is_null($param)) {
	        $this->paginator->setCurrentPageNumber((int) $this->params()->fromRoute('param1', 1));
        } else {
        	$this->paginator->setCurrentPageNumber((int) $param);
        }
        $this->paginator->setItemCountPerPage($perPage);
        $this->setLogMessage('Project Type list accessed');

        return $this->paginator;
    }
}
