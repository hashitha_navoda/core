<?php

namespace Jobs\Controller;

use Zend\View\Model\ViewModel;
use Core\Controller\CoreController;
use Zend\Session\Container;
use Invoice\Form\AddCustomerForm;

class TournamentController extends CoreController
{

    protected $sideMenus = 'tournament_side_menu';
    protected $upperMenus = 'tournament_upper_menu';


    public function __construct()
    {
        parent::__construct();
        $this->user_session = new Container('ezBizUser');
        $this->userID = $this->user_session->userID;
        $this->username = $this->user_session->username;
        $this->company = $this->user_session->companyDetails;
        $this->cdnUrl = $this->user_session->cdnUrl;
        $this->useAccounting = $this->user_session->useAccounting;
    }

    public function indexAction()
    {   
        $projectID = $this->params()->fromRoute('param1');
        $mode = $this->params()->fromRoute('param2');

        return ($projectID) ? $this->projectCreate($projectID, $mode): $this->projectCreate(); 
    }
    
    public function projectCreate($projectId = null, $mode = false) {

        $this->getSideAndUpperMenus('Tournaments', 'Create Tournament', 'Jobs');
        if ($projectId) {
            $this->getSideAndUpperMenus('Tournaments', 'Tournament List', 'Jobs');
        }
        $dateFormat = $this->getUserDateFormat();

        $currencyResultSet = $this->CommonTable('Core\Model\CurrencyTable')->fetchAll();
        $currencies = array();
        $sup = array();
        $tax = array();
        $ctax = array();
        $productType = array();
        $location = array();
        $currencies['0'] = '';
        foreach ($currencyResultSet as $row) {
            $currencies[$row->currencyID] = $row->currencyName;
        }
        $customerCategories[''] = '';
        $existingCustomerCategories = $this->CommonTable('Invoice\Model\CustomerCategoryTable')->fetchAllForDorpDown();
        $customerCategories = $customerCategories + $existingCustomerCategories;

        //price list
        $customerPriceLists[''] = '';
        $existingCustomerPriceLists = $this->CommonTable('Inventory\Model\PriceListTable')->fetchAllForDorpDown();
        $customerPriceLists = $customerPriceLists + $existingCustomerPriceLists;

        $paymentTermsResultSet = $this->CommonTable('Core\Model\PaymentTermTable')->fetchAll();
        $paymentTerms = array();
        foreach ($paymentTermsResultSet as $row) {
            $paymentTerms[$row->paymentTermID] = $row->paymentTermName;
        }


        // get person Title (Mr, Miss ..) from personTitle.config file
        $config = $this->getServiceLocator()->get('config');
        $customerTitle = $config['personTitle'];
        $countries = ['' => ''] + $config['all_countries'];
        $prorefData = $this->getReferenceNoForLocation(40, $locationID);

        $cusRefData = $this->getReferenceNoForLocation(20, $locationID);
        $cusRid = $refData["refNo"];
        $locationReferenceID = $cusRefData["locRefID"];
        $customerCode = $this->getReferenceNumber($locationReferenceID);

        $currencyValue = $this->getDefaultCurrency();
        $customerForm = new AddCustomerForm(NULL, $paymentTerms, $currencies, $customerTitle, $currencyValue,$customerCategories, "", null,[], $customerPriceLists);
        $customerForm->get('customerFormsubmit')->setValue(_("Add New Customer"));
        $customerForm->get('customerCode')->setAttribute('value', $customerCode);

        $financeAccountsArray = $this->getFinanceAccounts();

        $this->addGLAcccountDetails($customerForm, $financeAccountsArray);

        $projectManagers = $this->CommonTable('Jobs\Model\EmployeeDesignationTable')->fetchEmployeeByDesignationCode('DES001');
        $projectSupervisors = $this->CommonTable('Jobs\Model\EmployeeDesignationTable')->fetchEmployeeByDesignationCode('DES002');


        $customerAddView = new ViewModel(array(
            'customerForm' => $customerForm,
            'countries' => $countries,
            'useAccounting' => $this->useAccounting
        ));

        $projectManagerAddView = new ViewModel(array(
            'projectManagers' => $projectManagers,
        ));

        $projectSupervisorAddView = new ViewModel(array(
            'projectSupervisors' => $projectSupervisors,
        ));

        $projectMaterialAddView = new ViewModel(array(
            'projectSupervisors' => $projectSupervisors,
        ));

        if  ($projectId) {
            $projectRelatedJobs = $this->CommonTable('Jobs\Model\JobTable')->getJobDetailsAccordingToProject($projectId);
        }
        

        $projectTypes = $this->CommonTable('Jobs\Model\ProjectTypeTable')->searchProjectTypesForDropDown();
    
        $projectView = new ViewModel(array(
            'dateFormat' => $dateFormat,
            'projectCode' => ($projectId)? $projectData['projectCode'] : $prorefData["refNo"],
            'currency' => $this->companyCurrencySymbol,
            'mode' => ($projectId) ? $mode: 'create',
            'projectId' => $projectId,
            'projectJobs' => ($projectId) ? $projectRelatedJobs : [],
            'projectTypes' => $projectTypes

        ));

        $customerAddView->setTemplate('invoice/customer/customer-add-modal');
        $projectManagerAddView->setTemplate('jobs/project/project-manager-add-modal');
        $projectSupervisorAddView->setTemplate('jobs/project/project-supervisor-add-modal');
        $projectMaterialAddView->setTemplate('jobs/project/project-materials-add-modal');
        $projectView->setTemplate('jobs/tournament/create');

        $projectView->addChild($customerAddView, 'customerAddView');
        $projectView->addChild($projectManagerAddView, 'projectManagerAddView');
        $projectView->addChild($projectSupervisorAddView, 'projectSupervisorAddView');
        $projectView->addChild($projectMaterialAddView, 'projectMaterialAddView');

        $this->department = $this->CommonTable('Jobs/Model/DepartmentTable')->fetchActiveDepartments();

        $departments = array();
        foreach ($this->department as $dept) {
            $res = [
                'departmentID' => $dept['departmentID'],
                'departmentName' => $dept['departmentName'],
                'departmentCode' => $dept['departmentCode'],
            ];
            array_push($departments, $res);
        }

        $this->designation = $this->CommonTable('Jobs/Model/DesignationTable')->fetchActiveDesignations();
        $designations = array();
        foreach ($this->designation as $desig) {
            $res = [
                'designationID' => $desig['designationID'],
                'designationName' => $desig['designationName'],
                'designationCode' => $desig['designationCode'],
            ];
            array_push($designations, $res);
        }
        $participantAddView = new ViewModel(array(
                'departments' => $departments,             
                'designations' => $designations,
                'mode' => ($projectId) ? $mode: 'create',            
        ));
        $participantAddView->setTemplate('jobs/tournament/tournament-participant-add-modal');
        $projectView->addChild($participantAddView, 'participantAddView');

        $this->getViewHelper('HeadScript')->prependFile('/js/invoice/modalAddCustomer.js');
        $this->getViewHelper('HeadScript')->prependFile('/js/jobs/tournament.js');
        return $projectView;   
    }

    //list action of project
    public function listAction()
    {
        $this->getSideAndUpperMenus('Tournaments', 'Tournament List', 'Jobs');
        $paginator = $this->getPaginatedProjects();

        $tournamentOfficials = $this->CommonTable('Jobs\Model\EmployeeTable')->fetchAll();
        $projectTypes = $this->CommonTable('Jobs\Model\ProjectTypeTable')->searchProjectTypesForDropDown();

        $projectList = new ViewModel(array(
            'project_list' => $paginator,
            'status' => $this->getStatusesList(),
            'userDateTimeFormat' => $this->convertUserDateFormatToPhpFormat() . ' H:i',
            'paginated' => true
        ));

        $projectList->setTemplate('jobs/tournament/project-list');
        $projectListView = new ViewModel(['userDateFormat' => $this->getUserDateFormat(), 'tournamentOfficials' => $tournamentOfficials,'projectTypes' => $projectTypes,]);
        $projectListView->addChild($projectList, 'projectList');

        $this->getViewHelper('HeadScript')->prependFile('/js/jobs/tournament-list.js');
        return $projectListView;
    }

    public function addGLAcccountDetails($customerForm, $financeAccountsArray) 
    {
        $glAccountExist = $this->CommonTable('Accounting\Model\GlAccountSetupTable')->fetchAll();
        if ($this->useAccounting == 1) {
            $glAccountSetupData = $glAccountExist->current();

            if($glAccountSetupData->glAccountSetupSalesAndCustomerReceivableAccountID != ''){
                $customerReceviableAccountID = $glAccountSetupData->glAccountSetupSalesAndCustomerReceivableAccountID;
                $customerReceviableAccountName = $financeAccountsArray[$customerReceviableAccountID]['financeAccountsCode']."_".$financeAccountsArray[$customerReceviableAccountID]['financeAccountsName'];
                $customerForm->get('customerReceviableAccountID')->setAttribute('data-id',$customerReceviableAccountID);
                $customerForm->get('customerReceviableAccountID')->setAttribute('data-value',$customerReceviableAccountName);
            }

            if($glAccountSetupData->glAccountSetupSalesAndCustomerSalesAccountID != ''){
                $customerSalesAccountID = $glAccountSetupData->glAccountSetupSalesAndCustomerSalesAccountID;
                $customerSalesAccountName = $financeAccountsArray[$customerSalesAccountID]['financeAccountsCode']."_".$financeAccountsArray[$customerSalesAccountID]['financeAccountsName'];
                $customerForm->get('customerSalesAccountID')->setAttribute('data-id',$customerSalesAccountID);
                $customerForm->get('customerSalesAccountID')->setAttribute('data-value',$customerSalesAccountName);
            }

            if($glAccountSetupData->glAccountSetupSalesAndCustomerSalesDiscountAccountID != ''){
                $customerSalesDiscountAccountID = $glAccountSetupData->glAccountSetupSalesAndCustomerSalesDiscountAccountID;
                $customerSalesDiscountAccountName = $financeAccountsArray[$customerSalesDiscountAccountID]['financeAccountsCode']."_".$financeAccountsArray[$customerSalesDiscountAccountID]['financeAccountsName'];
                $customerForm->get('customerSalesDiscountAccountID')->setAttribute('data-id',$customerSalesDiscountAccountID);
                $customerForm->get('customerSalesDiscountAccountID')->setAttribute('data-value',$customerSalesDiscountAccountName);
            }

            if($glAccountSetupData->glAccountSetupSalesAndCustomerAdvancePaymentAccountID != ''){
                $customerAdvancePaymentAccountID = $glAccountSetupData->glAccountSetupSalesAndCustomerAdvancePaymentAccountID;
                $customerAdvancePaymentAccountName = $financeAccountsArray[$customerAdvancePaymentAccountID]['financeAccountsCode']."_".$financeAccountsArray[$customerAdvancePaymentAccountID]['financeAccountsName'];
                $customerForm->get('customerAdvancePaymentAccountID')->setAttribute('data-id',$customerAdvancePaymentAccountID);
                $customerForm->get('customerAdvancePaymentAccountID')->setAttribute('data-value',$customerAdvancePaymentAccountName);
            }
        }

    }

    public function getPaginatedProjects($perPage = 6, $param = null)
    {
        $this->employee = $this->CommonTable('Jobs/Model/projectTable')->fetchAll(true);

        if ($param == null) {
            $this->employee->setCurrentPageNumber((int) $this->params()->fromRoute('param1', 1));
        } else {
            $this->employee->setCurrentPageNumber($param);     
        }
        $this->employee->setItemCountPerPage($perPage);
        return $this->employee;
    }
    
}
