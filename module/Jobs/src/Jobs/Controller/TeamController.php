<?php

namespace Jobs\Controller;

use Zend\View\Model\ViewModel;
use Core\Controller\CoreController;
use Zend\Session\Container;

class TeamController extends CoreController
{

    protected $sideMenus = 'jobs_side_menu';
    protected $upperMenus = 'jobs_employee_upper_menu';
    protected $downMenus = 'job_setup_down_menu';

    public function createAction()
    {   
        $this->getSideAndUpperMenus('Jobs Settings', 'Teams', 'Jobs', 'Employee Settings');


        $paginator = $this->getPaginatedTeams();
        $this->employees = $this->CommonTable('Jobs/Model/EmployeeTable')->fetchActiveEmployees();
        
        $employees = array();
        foreach ($this->employees as $employee) {
            $res = [
                'employeeID' => $employee['employeeID'],
                'employeeFirstName' => $employee['employeeFirstName'],
                'employeeCode' => $employee['employeeCode'],
            ];
            array_push($employees, $res);
        }

        $teamView = new ViewModel(array(
                                'employees' => $employees,
                                'paginated' => true,
                                'teams' => $paginator
                            ));
        $teamView->setTemplate('jobs/teams/create');
        $this->getViewHelper('HeadScript')->prependFile('/js/jobs/team.js');
        return $teamView;
    }

    public function getPaginatedTeams($perPage = 6, $param = null)
    {
        $this->team = $this->CommonTable('Jobs/Model/TeamTable')->fetchAll(true);
        if ($param == null) {
            $this->team->setCurrentPageNumber((int) $this->params()->fromRoute('param1', 1));
        } else {
            $this->team->setCurrentPageNumber($param);     
        }
        $this->team->setItemCountPerPage($perPage);
        return $this->team;
    }

}
