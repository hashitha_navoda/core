<?php

namespace Jobs\Controller;

use Core\Controller\CoreController;
use Jobs\Form\JobForm;
use Zend\View\Model\ViewModel;

class ResourcesController extends CoreController {

    protected $sideMenus = 'jobs_side_menu_of_service';
    protected $upperMenus = 'resources_settings_upper_menu';
    protected $downMenus = 'job_setup_down_menu_of_service';

	public function indexAction() {
		$this->getSideAndUpperMenus('Jobs Settings', 'Standard Materials', 'Jobs', 'Resources Settings');

        $uoms = $this->CommonTable('Settings/Model/UomTable')->getUomforSearch();
        $uomList = array();
        foreach ($uoms as $val) {
            $temp['value'] = $val->uomID;
            $temp['text'] = $val->uomName . '(' . $val->uomAbbr.')';
            $uomList[] = $temp;
        }

        $view = new ViewModel();

        $serviceList = new ViewModel();
        $serviceList->setTemplate('/jobs/resources/service-list');
        $view->addChild($serviceList, 'serviceList');

        $viewMaterialsModal = new ViewModel();
        $viewMaterialsModal->setTemplate('/jobs/resources/view-materials');
        $view->addChild($viewMaterialsModal, 'viewMaterialsModal');

        $this->getViewHelper('HeadScript')->prependFile('/js/jobs/standard-materials-setup.js');

        $this->getServiceLocator()->get('viewhelpermanager')->get('jsvarsHelper')->setVars([
            'jobSettings'
        ]);

        return $view;
    }

    public function vehiclesAction()
    {   
        $this->getSideAndUpperMenus('Jobs Settings', null, 'Jobs', 'Vehicle Settings');
        $this->getPaginatedVehiclesTypes();

        $view = new ViewModel(array(
            'vehicleTypeList' => $this->paginator,
            'paginated' => true,
        ));

        $addVehicleModels = new ViewModel();
        $addVehicleModels->setTemplate('/jobs/resources/add-vehicle-model');
        $view->addChild($addVehicleModels, 'addVehicleModels');

        $addVehicleSubModels = new ViewModel();
        $addVehicleSubModels->setTemplate('/jobs/resources/add-vehicle-sub-model');
        $view->addChild($addVehicleSubModels, 'addVehicleSubModels');

        $viewVehicleModels = new ViewModel();
        $viewVehicleModels->setTemplate('/jobs/resources/view-vehicle-model');
        $view->addChild($viewVehicleModels, 'viewVehicleModels');

        $viewVehicleSubModels = new ViewModel();
        $viewVehicleSubModels->setTemplate('/jobs/resources/view-vehicle-sub-model');
        $view->addChild($viewVehicleSubModels, 'viewVehicleSubModels');

        $this->getViewHelper('HeadScript')->prependFile('/js/jobs/vehicle-type-setup.js');

        $this->getServiceLocator()->get('viewhelpermanager')->get('jsvarsHelper')->setVars([
            'jobSettings'
        ]);

        return $view;
    }

    public function getPaginatedVehiclesTypes($perPage = 6, $param = null)
    {
        $this->paginator = $this->CommonTable('Jobs\Model\VehicleTypeTable')->fetchAll(TRUE);
        if ($param == null) {
            $this->paginator->setCurrentPageNumber((int) $this->params()->fromRoute('param1', 1));
        } else {
            $this->paginator->setCurrentPageNumber($param);     
        }
        $this->paginator->setItemCountPerPage($perPage);
        return $this->paginator;
    }
}