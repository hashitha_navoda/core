<?php

namespace Jobs\Controller;

use Zend\View\Model\ViewModel;
use Core\Controller\CoreController;
use Zend\Session\Container;

class EmployeeManagementController extends CoreController
{

    protected $sideMenus = 'jobs_side_menu_of_service';
    protected $upperMenus = 'employee_management_upper_menu';
    protected $downMenus = 'job_setup_down_menu_of_service';

    public function indexAction($projectId = null, $mode = false) {

        $this->getSideAndUpperMenus('Employee Management', 'Employee Performance', 'Jobs', null);
        $userDateFormat = $this->getUserDateFormat();

        $departmentList = $this->CommonTable('Jobs/Model/DepartmentTable')->fetchAll(false, true);
        foreach ($completedJobList as $key => $part) {
           $sort[$key] = strtotime($part['updatedTimeStamp']);
        }
        array_multisort($sort, SORT_DESC, $completedJobList);

        $view = new ViewModel(array(
           'departmentList' => $departmentList,
           'userDateFormat' => $userDateFormat,
        ));

        $view->setTemplate('jobs/employee-management/index');
        $this->getViewHelper('HeadScript')->prependFile('/js/jobs/employee-management.js');
        return $view;   
    }
}
