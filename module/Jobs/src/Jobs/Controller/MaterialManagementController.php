<?php

namespace Jobs\Controller;

use Zend\View\Model\ViewModel;
use Core\Controller\CoreController;
use Zend\Session\Container;

class MaterialManagementController extends CoreController
{

    
    protected $sideMenus = 'jobs_side_menu_of_service';

    public function indexAction()
    {   
    	$this->getSideAndUpperMenus('Material Management', 'Material Management', 'Jobs', NULL);

        $userDateFormat = $this->getUserDateFormat();
        $jobList = $this->getPaginatedStartedJobs();
        $paginatedflag = true;
        $serviceStartedTimeData = $this->getTaskStartedTime();

        $jobView = new ViewModel(array(
            'jobList' => $jobList,
            'serviceStartedTimeData' => $serviceStartedTimeData,
            'paginated' => $paginatedflag,
            'status' => $this->getStatusesList(),
            'userDateFormat' => $userDateFormat,
        ));

        $this->getViewHelper('HeadScript')->prependFile('/js/jobs/materil-management-list.js');

        return $jobView;
    }

    public function getPaginatedStartedJobs($param = null)
    {
        $this->job = $this->CommonTable('Jobs/Model/JobTable')->getStartedJobsWithServices(true);
		if (is_null($param)) {
            $this->job->setCurrentPageNumber((int) $this->params()->fromRoute('param1', 1));
        } else {
            $this->job->setCurrentPageNumber((int) $param);
        }
		$this->job->setItemCountPerPage(6);
        return $this->job;
    }

    public function getTaskStartedTime(){
    	$startedTimes = $this->CommonTable('Jobs/Model/JobTable')->getStartedJobsWithServices();

    	$startedTimeData = [];
    	foreach ($startedTimes as $value) {
    		if (!is_null($value['startedAt'])) {
    			$startedTimeData[$value['jobTaskID']] = $this->getUserDateTime($value['startedAt']);
    		}
    	}
    	return $startedTimeData;
    }

    /**
     * This function is for update material action
     */
    public function updateMaterialAction()
    {
        $this->getSideAndUpperMenus('Material Management', 'Material Management', 'Jobs', NULL);
        $paramIn = $this->params()->fromRoute('param1');
        if ($paramIn != NULL) {
            $jobServiceData = $this->CommonTable('Jobs\Model\JobTable')->getJobServiceByJobServiceID($paramIn);
            
            $jobServiceDetails = [];
            foreach ($jobServiceData as $key => $row) {
                $tempG = array();
                $jobID = $row['jobId'];
                $jobTaskID = $row['jobTaskID'];
                $tempG['jobId'] = $row['jobId'];
                $tempG['jobTaskID'] = $row['jobTaskID'];
                $tempG['jobCode'] = $row['jobReferenceNumber'];
                $tempG['jobSupervisorName'] = $row['userFirstName'].' '.$row['userLastName'];
                $tempG['serviceName'] = $row['taskName'];
                $tempG['departmentStationName'] = (is_null($row['departmentStationName'])) ? "Default Station" : $row['departmentStationName'];
                $tempG['startedTime'] = $this->getUserDateTime($row['startedAt']);
                $jobProducts = (isset($jobServiceDetails[$row['jobTaskID']]['jobProducts'])) ? $jobServiceDetails[$row['jobTaskID']]['jobProducts'] : array();

                if (!is_null($row['jobProductID'])) {

                    $productUom = $this->CommonTable('Inventory\Model\ProductUomTable')->getProductUomListByProductID($row['productID']);

                    $thisqty = $this->getProductQuantityViaDisplayUom($row['jobProductAllocatedQty'], $productUom);
                    $thisqty['quantity'] = ($thisqty['quantity'] == 0) ? '-' : $thisqty['quantity'];

                    $jobProducts[$row['jobProductID']] = array('locationProductID' => $row['locationProductID'], 'jobProductUnitPrice' => $row['jobProductUnitPrice'], 'uomID' => $row['uomID'], 'jobProductAllocatedQty' => $thisqty['quantity'], 'jobProductReOrderLevel' => $row['jobProductReOrderLevel'], 'jobProductMaterialTypeID' => $row['jobProductMaterialTypeID'],'productCode' => $row['productCode'], 'productName' => $row['productName'],'uomAbbr' => $row['uomAbbr'], 'productEstimatedCost' => floatval($row['jobProductAllocatedQty']) * floatval($row['jobProductUnitPrice']));
                }

                $tempG['jobProducts'] = $jobProducts;
                $jobServiceDetails[$row['jobTaskID']] = $tempG;
            
            }
            $jobView = new ViewModel(array(
                'jobID' => $jobID,
                'jobData' => $jobServiceDetails[$jobTaskID],
                'companyCurrencySymbol' => $this->companyCurrencySymbol,
            ));

            $jobView->setTemplate('jobs/material-management/material-management');
            $this->getViewHelper('HeadScript')->prependFile('/js/jobs/materil-management.js');
            $this->setLogMessage("View job ".$tempG['jobId'].".");
            return $jobView;
        } else {
            $this->setLogMessage("Redirect to job list.");
            $this->redirect()->toRoute('materialManagement', array('action' => 'index'));
        }
    }


    public function viewAction()
    {
        $this->getSideAndUpperMenus('Material Management', 'Material Management', 'Jobs', NULL);
        $paramIn = $this->params()->fromRoute('param1');
        if ($paramIn != NULL) {
            $jobServiceData = $this->CommonTable('Jobs\Model\JobTable')->getJobServiceByJobServiceID($paramIn);
            
            $jobServiceDetails = [];
            foreach ($jobServiceData as $key => $row) {
                $tempG = array();
                $jobID = $row['jobId'];
                $jobTaskID = $row['jobTaskID'];
                $tempG['jobId'] = $row['jobId'];
                $tempG['jobTaskID'] = $row['jobTaskID'];
                $tempG['jobCode'] = $row['jobReferenceNumber'];
                $tempG['jobSupervisorName'] = $row['userFirstName'].' '.$row['userLastName'];
                $tempG['serviceName'] = $row['taskName'];
                $tempG['departmentStationName'] = (is_null($row['departmentStationName'])) ? "Default Station" : $row['departmentStationName'];
                $tempG['startedTime'] = $this->getUserDateTime($row['startedAt']);
                $jobProducts = (isset($jobServiceDetails[$row['jobTaskID']]['jobProducts'])) ? $jobServiceDetails[$row['jobTaskID']]['jobProducts'] : array();

                if (!is_null($row['jobProductID'])) {

                    $productUom = $this->CommonTable('Inventory\Model\ProductUomTable')->getProductUomListByProductID($row['productID']);

                    $thisqty = $this->getProductQuantityViaDisplayUom($row['jobProductAllocatedQty'], $productUom);
                    $thisqty['quantity'] = ($thisqty['quantity'] == 0) ? '-' : $thisqty['quantity'];

                    $jobProducts[$row['jobProductID']] = array('locationProductID' => $row['locationProductID'],'jobProductID' => $row['jobProductID'],'productID' => $row['productID'], 'jobProductUnitPrice' => $row['jobProductUnitPrice'], 'uomID' => $row['uomID'], 'jobProductAllocatedQty' => $thisqty['quantity'],'jobProductIssuedQty' => $row['jobProductIssuedQty'], 'jobProductReOrderLevel' => $row['jobProductReOrderLevel'], 'jobProductMaterialTypeID' => $row['jobProductMaterialTypeID'],'productCode' => $row['productCode'], 'productName' => $row['productName'],'uomAbbr' => $row['uomAbbr'], 'productEstimatedCost' => floatval($row['jobProductAllocatedQty']) * floatval($row['jobProductUnitPrice']));
                }

                $tempG['jobProducts'] = $jobProducts;
                $jobServiceDetails[$row['jobTaskID']] = $tempG;
            
            }
            
            $jobView = new ViewModel(array(
                'jobID' => $jobID,
                'jobData' => $jobServiceDetails[$jobTaskID],
                'companyCurrencySymbol' => $this->companyCurrencySymbol,
            ));

            $jobView->setTemplate('jobs/material-management/material-management-view');
            $this->getViewHelper('HeadScript')->prependFile('/js/jobs/materil-management.js');
            $this->setLogMessage("View job ".$tempG['jobId'].".");
            return $jobView;
        } else {
            $this->setLogMessage("Redirect to job list.");
            $this->redirect()->toRoute('materialManagement', array('action' => 'index'));
        }
    }
}