<?php

namespace Jobs\Controller;

use Zend\View\Model\ViewModel;
use Core\Controller\CoreController;
use Jobs\Form\CostTypeForm;
use Zend\Session\Container;

class CostTypeController extends CoreController
{

    protected $sideMenus = 'jobs_side_menu';
    protected $upperMenus = 'category_setting_upper_menu';
    protected $downMenus = 'job_setup_down_menu';

    //create action of cost type
    public function indexAction()
    {
        $this->getSideAndUpperMenus('Jobs Settings', 'Cost Type', 'Jobs', 'Category Settings');

        $costTypeList = $this->getPaginatedCostTypes();

        $form = new CostTypeForm();

        $view = new ViewModel(array(
            'form' => $form,
            'costTypeList' => $costTypeList,
            'paginated' => true
        ));

        $this->getViewHelper('HeadScript')->prependFile('/js/jobs/cost-type.js');
        $this->getServiceLocator()->get('viewhelpermanager')->get('jsvarsHelper')->setVars(['getAllActiveNonInventoryItems']);
        return $view;
    }

    public function getPaginatedCostTypes($param = null)
    {
        $this->paginator = $this->CommonTable('Jobs\Model\CostTypeTable')->fetchAll(true);
        if (is_null($param)) {
            $this->paginator->setCurrentPageNumber((int) $this->params()->fromRoute('param1', 1));
        } else {
            $this->paginator->setCurrentPageNumber((int) $param);
        }
        $this->paginator->setItemCountPerPage(6);
        $this->setLogMessage('Cost Type list accessed');
        return $this->paginator;
    }

}
