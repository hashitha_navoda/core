<?php

namespace Jobs\Controller;

use Zend\View\Model\ViewModel;
use Core\Controller\CoreController;
use Zend\Session\Container;

class ProgressController extends CoreController
{

    protected $sideMenus = 'jobs_side_menu';
    protected $upperMenus = 'progress_upper_menu';

    public function listAction()
    {   
        $this->getSideAndUpperMenus('Progress Update', 'Update Progress', 'Jobs');
        $maintainProject = $this->user_session->jobSettings->maintainProject;
        $locationID = $this->user_session->userActiveLocation['locationID'];
        $param = 'false';

        if ($this->params()->fromRoute('param1') == 'back') {
            $param = 'true';
        } 

        $jobList = $this->getPaginatedJob($locationID);

        $progressView = new ViewModel(array(
            'maintainProject' => $maintainProject,
            'contractorForJob' => $this->user_session->jobSettings->contractorForJob,
            'paramVal' => $param,
            'paginated' => true,
            'jobs' => $jobList
        ));

        $progressView->setTemplate('jobs/progress/list');
        $this->getViewHelper('HeadScript')->prependFile('/js/jobs/progress.js');

        return $progressView;

    }

    public function getPaginatedJob($locationID, $param = null)
    {
        $this->job = $this->CommonTable('Jobs/Model/JobTable')->getJobListForProgressUpdate($locationID, true);
        if (is_null($param)) {
            $this->job->setCurrentPageNumber((int) $this->params()->fromRoute('param3', 1));
        } else {
            $this->job->setCurrentPageNumber((int) $param);
        }
        $this->job->setItemCountPerPage(6);
        return $this->job;
    }

    public function viewTaskAction () 
    {
        $this->getSideAndUpperMenus('Progress Update', 'Update Progress', 'Jobs');
        $jobID = $this->params()->fromRoute('param3');
        $jobData = $this->CommonTable('Jobs/Model/JobTable')->getJobDetails($jobID)->current();
        $taskData = $this->CommonTable('Jobs/Model/JobTaskTable')->getJobTaskDetails($jobID);
        $contractorForJob = $this->user_session->jobSettings->contractorForJob;

        $tasks = [];
        foreach ($taskData as $key => $value) {
            $temp = [
                'taskID' => $value['taskID'],
                'jobID' => $value['jobID'],
                'projectID' => $value['projectId'],
                'jobTaskID' => $value['jobTaskID'],
                'taskCode' => $value['taskCode'],
                'taskName' => $value['taskName'],
                'jobTaskEstQty' => floatval($value['jobTaskEstQty']),
                'jobTaskUsedQty' => floatval($value['jobTaskUsedQty']),
                'jobTaskProgress' => floatval($value['jobTaskProgress']),
                'jobTaskStatus'=> $value['jobTaskStatus']
            ];

            $tasks[] = $temp;
        }
 
        $taskProgressEditView = new ViewModel(array(
            'jobId' => $jobID,
            'jobData' => $jobData,
            'tasks' => $tasks,
            'status' => $this->getStatusesList(),
            'contractorForJob' => $contractorForJob
        ));

        $taskProgressEditView->setTemplate('jobs/progress/task-progress-edit');
        $this->getViewHelper('HeadScript')->prependFile('/js/jobs/task-progress-edit.js');


        return $taskProgressEditView;
    }

    public function viewResourcesAction () 
    {
        $this->getSideAndUpperMenus('Progress Update', 'Update Progress', 'Jobs');
        $jobID = $this->params()->fromRoute('param3');
        $jobData = $this->CommonTable('Jobs/Model/JobTable')->getJobDetails($jobID)->current();
        $taskData = $this->CommonTable('Jobs/Model/JobTaskTable')->getJobTaskDetails($jobID);
        $otherProductData = $this->CommonTable('Jobs/Model/JobProductTable')->getJobRelatedOtherMaterials($jobID);
        $contractorForJob = $this->user_session->jobSettings->contractorForJob;

        $otherItems = [];
        foreach ($otherProductData as $key => $item) {
            $tempOtherItem = [
                'jobProductID' => $item['jobProductID'],
                'productCode' => $item['productCode'],
                'productName' => $item['productName'],
                'jobProductAllocatedQty' => floatval($item['jobProductAllocatedQty']),
                'jobProductStatus' => ($item['materialRequestStatus'] == null) ? '3' : $item['materialRequestStatus']
            ];

            $otherItems[] = $tempOtherItem;
            
        }
       
        $tasks = [];
        $status = null;
        foreach ($taskData as $key => $value) {

            $taskProductData = $this->CommonTable('Jobs/Model/JobProductTable')->getTaskProductDatailsbyJobTaskID($value['jobTaskID'], $jobID);
            $taskProducts = [];
            $jobProductStatus = [];
            foreach ($taskProductData as $key => $val) {
                $tempData = [
                    'productName' => $val['productName'],
                    'productCode' => $val['productCode'],
                    'requestedQty' => floatval($val['jobProductAllocatedQty']) 
                ];
                $jobProductStatus[] = $val['materialRequestStatus'];

                $taskProducts[] = $tempData;
            }
            $value['taskRelatedProducts'] = $taskProducts;

            if (count(array_unique($jobProductStatus)) === 1) {
                $status = $jobProductStatus[0];
            }

            $temp = [
                'taskID' => $value['taskID'],
                'jobID' => $value['jobID'],
                'projectID' => $value['projectId'],
                'jobTaskID' => $value['jobTaskID'],
                'taskCode' => $value['taskCode'],
                'taskName' => $value['taskName'],
                'jobTaskEstQty' => floatval($value['jobTaskEstQty']),
                'jobTaskUsedQty' => floatval($value['jobTaskUsedQty']),
                'jobTaskProgress' => floatval($value['jobTaskProgress']),
                'jobTaskStatus'=> $value['jobTaskStatus'],
                'taskRelatedProducts' => $value['taskRelatedProducts'],
                'jobProductStatus' => ($status == null) ? '3': $status
            ];

            if (count($value['taskRelatedProducts']) > 0) {
                $tasks[] = $temp;
            }

        }

        $taskProgressEditView = new ViewModel(array(
            'jobId' => $jobID,
            'jobData' => $jobData,
            'tasks' => $tasks,
            'status' => $this->getStatusesList(),
            'otherItems' => $otherItems,
            'contractorForJob' => $contractorForJob
        ));

        $taskProgressEditView->setTemplate('jobs/progress/progress-update-material-requisition');
        $this->getViewHelper('HeadScript')->prependFile('/js/jobs/progress-update-material-requisition.js');

        return $taskProgressEditView;
    }


    public function getTaskRelatedProducts($taskData) {
        foreach ($taskData as $key => $value) {        
            $taskProductData = $this->CommonTable('Jobs/Model/JobProductTable')->getTaskProductDatailsbyJobTaskID($value['jobTaskID'], $jobID);
            $taskProducts = [];
            foreach ($taskProductData as $key => $val) {
                $temp = [
                    'productName' => $val['productName'],
                    'productCode' => $val['productCode'],
                    'requestedQty' => $val['jobProductAllocatedQty']
                ];

                $taskProducts[] = $temp;
            }
            $value['taskRelatedProducts'] = $taskProducts;

        }

        return $taskData;
    }

    public function viewRequestsAction () 
    {
        $jobID = $this->params()->fromRoute('param3');
        $jobData = $this->CommonTable('Jobs/Model/JobTable')->getJobDetails($jobID)->current();
        $this->getSideAndUpperMenus('Progress Update', 'Update Progress', 'Jobs');
        $requests = $this->CommonTable('Jobs/Model/MaterialRequestTable')->getMaterialRequestByJobId($jobID, true);
        $contractorForJob = $this->user_session->jobSettings->contractorForJob;

        $paginatedData = $requests;
        $paginatedData->setCurrentPageNumber($this->params()->fromRoute('param1', 1));
        $paginatedData->setItemCountPerPage(8);
        

        $jobRelatedRequestView = new ViewModel(array(
            'jobId' => $jobID,
            'requests' => $paginatedData,
            'jobData' => $jobData,
            'status' => $this->getStatusesList(),
            'contractorForJob' => $contractorForJob
        ));

        $jobRelatedRequestView->setTemplate('jobs/progress/job-related-material-request-view');
        $this->getViewHelper('HeadScript')->prependFile('/js/jobs/progress.js');

        return $jobRelatedRequestView;
    }

}
