<?php

namespace Jobs\Controller;

use Zend\View\Model\ViewModel;
use Core\Controller\CoreController;
use Zend\Session\Container;
use Invoice\Form\AddCustomerForm;

class ServiceJobsController extends CoreController
{

    protected $sideMenus = 'jobs_side_menu_of_service';
    protected $upperMenus = 'jobs_job_closed_list_upper_menu_of_service';


    public function __construct()
    {
        parent::__construct();
        $this->user_session = new Container('ezBizUser');
        $this->userID = $this->user_session->userID;
        $this->username = $this->user_session->username;
        $this->company = $this->user_session->companyDetails;
        $this->cdnUrl = $this->user_session->cdnUrl;
        $this->useAccounting = $this->user_session->useAccounting;
    }

    
    public function createAction() {

        $jbID = $this->params()->fromRoute('param1');

        $mode = ($jbID) ? 'edit': 'create';

        $this->getSideAndUpperMenus('Job', null, 'Jobs');
        $dateFormat = $this->getUserDateFormat();

        $sup = array();
        $tax = array();
        $ctax = array();
        $productType = array();
        $location = array();
        $currencies['0'] = '';
        $currencies = array();
        $currencyResultSet = $this->CommonTable('Core\Model\CurrencyTable')->fetchAll();
        foreach ($currencyResultSet as $row) {
            $currencies[$row->currencyID] = $row->currencyName;
        }
        
        $fuelTypes = array();
        $fuelTypeResult = $this->CommonTable('Jobs\Model\FuelTypeTable')->fetchAll();
        foreach ($fuelTypeResult as $row) {
            $fuelTypes[$row['fuelTypeID']] = $row['fuelTypeName'];
        }

        $customerCategories[''] = '';
        $existingCustomerCategories = $this->CommonTable('Invoice\Model\CustomerCategoryTable')->fetchAllForDorpDown();
        $customerCategories = $customerCategories + $existingCustomerCategories;

        //price list
        $customerPriceLists[''] = '';
        $existingCustomerPriceLists = $this->CommonTable('Inventory\Model\PriceListTable')->fetchAllForDorpDown();
        $customerPriceLists = $customerPriceLists + $existingCustomerPriceLists;

        $paymentTermsResultSet = $this->CommonTable('Core\Model\PaymentTermTable')->fetchAll();
        $paymentTerms = array();
        foreach ($paymentTermsResultSet as $row) {
            $paymentTerms[$row->paymentTermID] = $row->paymentTermName;
        }


        // get person Title (Mr, Miss ..) from personTitle.config file
        $config = $this->getServiceLocator()->get('config');
        $customerTitle = $config['personTitle'];
        $countries = ['' => ''] + $config['all_countries'];
        $jobrefData = $this->getReferenceNoForLocation(22, $locationID);

        $cusRefData = $this->getReferenceNoForLocation(20, $locationID);
        $cusRid = $refData["refNo"];
        $locationReferenceID = $cusRefData["locRefID"];
        $customerCode = $this->getReferenceNumber($locationReferenceID);

        $currencyValue = $this->getDefaultCurrency();
        $customerForm = new AddCustomerForm(NULL, $paymentTerms, $currencies, $customerTitle, $currencyValue,$customerCategories, "", null,[], $customerPriceLists);
        $customerForm->get('customerFormsubmit')->setValue(_("Add New Customer"));
        $customerForm->get('customerCode')->setAttribute('value', $customerCode);

        $financeAccountsArray = $this->getFinanceAccounts();

        $this->addGLAcccountDetails($customerForm, $financeAccountsArray);

        $projectManagers = $this->CommonTable('Jobs\Model\EmployeeDesignationTable')->fetchEmployeeByDesignationCode('DES001');
        $projectSupervisors = $this->CommonTable('Jobs\Model\EmployeeDesignationTable')->fetchEmployeeByDesignationCode('DES002');


        $customerAddView = new ViewModel(array(
            'customerForm' => $customerForm,
            'countries' => $countries,
            'useAccounting' => $this->useAccounting
        ));    
        $projectTypes = $this->CommonTable('Jobs\Model\ProjectTypeTable')->searchProjectTypesForDropDown();
    

        $this->emploe = $this->CommonTable('Jobs/Model/EmployeeTable')->fetchAll();

        $employees = array();
        foreach ($this->emploe as $desig) {
            if ($desig['employeeStatus'] == '1') {
                $res = [
                    'employeeID' => $desig['employeeID'],
                    'employeeFirstName' => $desig['employeeFirstName'],
                    'employeeCode' => $desig['employeeCode'],
                ];
                array_push($employees, $res);
            }
        }
        $glAccountExist = $this->CommonTable('Accounting\Model\GlAccountSetupTable')->fetchAll();
        $glAccountSetupData = $glAccountExist->current();
        $accountSetupData = array();
        $customerReceviableAccountID = $glAccountSetupData->glAccountSetupSalesAndCustomerReceivableAccountID;
        $customerSalesAccountID = $glAccountSetupData->glAccountSetupSalesAndCustomerSalesAccountID;
        $customerSalesDiscountAccountID = $glAccountSetupData->glAccountSetupSalesAndCustomerSalesDiscountAccountID;
        $customerAdvancePaymentAccountID = $glAccountSetupData->glAccountSetupSalesAndCustomerAdvancePaymentAccountID;

        $locationID = $this->user_session->userActiveLocation['locationID'];
        $refData = $this->getReferenceNoForLocation(20, $locationID);
        $rid = $refData["refNo"];
        $locationReferenceID = $refData["locRefID"];
        $customerCode = $this->getReferenceNumber($locationReferenceID);

        $accountSetupData = [
            'customerReceviableAccountID' => $customerReceviableAccountID,
            'customerSalesAccountID' => $customerSalesAccountID,
            'customerSalesDiscountAccountID' => $customerSalesDiscountAccountID,
            'customerAdvancePaymentAccountID' => $customerAdvancePaymentAccountID,
            'customerCode' => $customerCode
        ];


        $view = new ViewModel(array(
            'dateFormat' => $dateFormat,
            'jobCode' => ($jbID) ? '' :$jobrefData["refNo"],
            'currency' => $this->companyCurrencySymbol,
            'supervisorName' => ($jbID) ? '' :$this->user_session->fullName,
            'userId' => ($jbID) ? '' :$this->userID,
            'mode' => $mode,
            'jobId' => ($jbID) ? $jbID : null,
            'fuelTypes' => $fuelTypes,
            'employees' => $employees,
            'accountSetupData' => $accountSetupData
        ));

        if (empty($jbID)) { 
            if ($jobrefData["refNo"] == '' || $jobrefData["refNo"] == NULL) {
                if ($lrefID == null) {
                    $title = 'Job Reference Number not set';
                    $msg = $this->getMessage('ERR_DELINOTECON_ADD_REF');
                } else {
                    $title = 'Job Reference Number has reache the maximum limit ';
                    $msg = $this->getMessage('ERR_DELINOTECON_CHANGE_REF');
                }
                $refNotSet = new ViewModel(array(
                    'title' => $title,
                    'msg' => $msg,
                    'controller' => 'company',
                    'action' => 'reference'
                ));
                $refNotSet->setTemplate('/core/modal/entity-missing-pre-requisites-notification');
                $view->addChild($refNotSet, 'refNotSet');
            }
        }

        $customerAddView->setTemplate('invoice/customer/customer-add-modal');
        $view->setTemplate('jobs/service-jobs/create');

        $SubTaskAddView = new ViewModel();
        $SubTaskAddView->setTemplate('jobs/service-jobs/sub-task-add-modal');

        $view->addChild($customerAddView, 'customerAddView');
        $view->addChild($SubTaskAddView, 'SubTaskAddView');
        $this->getViewHelper('HeadLink')->prependStylesheet('/css/select2.min.css');
        $this->getViewHelper('HeadScript')->prependFile('/js/select2.min.js');
        $this->getViewHelper('HeadScript')->prependFile('/js/invoice/modalAddCustomer.js');
        $this->getViewHelper('HeadScript')->prependFile('/js/jobs/service-job.js');
        return $view;   
    }

    public function addGLAcccountDetails($customerForm, $financeAccountsArray) 
    {
        $glAccountExist = $this->CommonTable('Accounting\Model\GlAccountSetupTable')->fetchAll();
        if ($this->useAccounting == 1) {
            $glAccountSetupData = $glAccountExist->current();

            if($glAccountSetupData->glAccountSetupSalesAndCustomerReceivableAccountID != ''){
                $customerReceviableAccountID = $glAccountSetupData->glAccountSetupSalesAndCustomerReceivableAccountID;
                $customerReceviableAccountName = $financeAccountsArray[$customerReceviableAccountID]['financeAccountsCode']."_".$financeAccountsArray[$customerReceviableAccountID]['financeAccountsName'];
                $customerForm->get('customerReceviableAccountID')->setAttribute('data-id',$customerReceviableAccountID);
                $customerForm->get('customerReceviableAccountID')->setAttribute('data-value',$customerReceviableAccountName);
            }

            if($glAccountSetupData->glAccountSetupSalesAndCustomerSalesAccountID != ''){
                $customerSalesAccountID = $glAccountSetupData->glAccountSetupSalesAndCustomerSalesAccountID;
                $customerSalesAccountName = $financeAccountsArray[$customerSalesAccountID]['financeAccountsCode']."_".$financeAccountsArray[$customerSalesAccountID]['financeAccountsName'];
                $customerForm->get('customerSalesAccountID')->setAttribute('data-id',$customerSalesAccountID);
                $customerForm->get('customerSalesAccountID')->setAttribute('data-value',$customerSalesAccountName);
            }

            if($glAccountSetupData->glAccountSetupSalesAndCustomerSalesDiscountAccountID != ''){
                $customerSalesDiscountAccountID = $glAccountSetupData->glAccountSetupSalesAndCustomerSalesDiscountAccountID;
                $customerSalesDiscountAccountName = $financeAccountsArray[$customerSalesDiscountAccountID]['financeAccountsCode']."_".$financeAccountsArray[$customerSalesDiscountAccountID]['financeAccountsName'];
                $customerForm->get('customerSalesDiscountAccountID')->setAttribute('data-id',$customerSalesDiscountAccountID);
                $customerForm->get('customerSalesDiscountAccountID')->setAttribute('data-value',$customerSalesDiscountAccountName);
            }

            if($glAccountSetupData->glAccountSetupSalesAndCustomerAdvancePaymentAccountID != ''){
                $customerAdvancePaymentAccountID = $glAccountSetupData->glAccountSetupSalesAndCustomerAdvancePaymentAccountID;
                $customerAdvancePaymentAccountName = $financeAccountsArray[$customerAdvancePaymentAccountID]['financeAccountsCode']."_".$financeAccountsArray[$customerAdvancePaymentAccountID]['financeAccountsName'];
                $customerForm->get('customerAdvancePaymentAccountID')->setAttribute('data-id',$customerAdvancePaymentAccountID);
                $customerForm->get('customerAdvancePaymentAccountID')->setAttribute('data-value',$customerAdvancePaymentAccountName);
            }
        }

    }

    public function pendingListAction() {
        $this->getSideAndUpperMenus('Job', null, 'Jobs');

        $locationID = $this->user_session->userActiveLocation['locationID'];
        $userDateFormat = $this->getUserDateFormat();
        $jobList = $this->getPaginatedJob($locationID, null, 3);
        $paginatedflag = true;

        $AddEmployeeView = new ViewModel();
        $AddEmployeeView->setTemplate('jobs/service-jobs/view-job-service-employee-modal');

        $actionName = 'pendingList';
        $jobView = new ViewModel(array(
            'jobList' => $jobList,
            'paginated' => $paginatedflag,
            'jbStatus' => 3,
            'status' => $this->getStatusesList(),
            'userDateFormat' => $userDateFormat,
            'actionName' => $actionName,
            'timeZone'=>$this->timeZone
        ));

        $jobView->setTemplate('jobs/service-jobs/list');
        $jobView->addChild($AddEmployeeView, 'AddEmployeeView');

        $this->getViewHelper('HeadScript')->prependFile('/js/jobs/service-job-list.js');
        return $jobView;


    }

    public function inprogressListAction() {
        $this->getSideAndUpperMenus('Job', null, 'Jobs');

        $locationID = $this->user_session->userActiveLocation['locationID'];
        $userDateFormat = $this->getUserDateFormat();
        $jobList = $this->getPaginatedJob($locationID, null, 8);
        $paginatedflag = true;
        $AddEmployeeView = new ViewModel();
        $AddEmployeeView->setTemplate('jobs/service-jobs/view-job-service-employee-modal');

        $actionName = 'inprogressList';
        $jobView = new ViewModel(array(
            'jobList' => $jobList,
            'paginated' => $paginatedflag,
            'jbStatus' => 8,
            'status' => $this->getStatusesList(),
            'userDateFormat' => $userDateFormat,
            'actionName' => $actionName,
            'timeZone'=>$this->timeZone
        ));

        $jobView->setTemplate('jobs/service-jobs/list');
        $jobView->addChild($AddEmployeeView, 'AddEmployeeView');

        $this->getServiceLocator()->get('viewhelpermanager')->get('jsvarsHelper')->setVars([
            'customerListWithoutDefaultCustomer', 'getAllJobTypes', 'getAllProject', 'getEmployees'
        ]);
        $this->getViewHelper('HeadScript')->prependFile('/js/jobs/service-job-list.js');
        return $jobView;

    }

    public function completedListAction() {
        $this->getSideAndUpperMenus('Job', null, 'Jobs');

        $locationID = $this->user_session->userActiveLocation['locationID'];
        $userDateFormat = $this->getUserDateFormat();
        $jobList = $this->getPaginatedJob($locationID, null, 9);
        $paginatedflag = true;
        $AddEmployeeView = new ViewModel();
        $AddEmployeeView->setTemplate('jobs/service-jobs/view-job-service-employee-modal');


        $actionName = 'completedList';
        $jobView = new ViewModel(array(
            'jobList' => $jobList,
            'jbStatus' => 9,
            'paginated' => $paginatedflag,
            'status' => $this->getStatusesList(),
            'userDateFormat' => $userDateFormat,
            'actionName' => $actionName,
            'timeZone'=>$this->timeZone
        ));

        $jobView->setTemplate('jobs/service-jobs/list');
        $jobView->addChild($AddEmployeeView, 'AddEmployeeView');
        $this->getViewHelper('HeadScript')->prependFile('/js/jobs/service-job-list.js');
        return $jobView;
    }

    public function closedListAction() {
        
        $this->getSideAndUpperMenus('Job', 'Closed Jobs', 'Jobs');

        $locationID = $this->user_session->userActiveLocation['locationID'];
        $userDateFormat = $this->getUserDateFormat();
        $jobList = $this->getPaginatedJob($locationID, null, 4);
        $paginatedflag = true;
        $AddEmployeeView = new ViewModel();
        $AddEmployeeView->setTemplate('jobs/service-jobs/view-job-service-employee-modal');

        $actionName = 'closedList';
        $jobView = new ViewModel(array(
            'jobList' => $jobList,
            'jbStatus' => 4,
            'paginated' => $paginatedflag,
            'status' => $this->getStatusesList(),
            'userDateFormat' => $userDateFormat,
            'actionName' => $actionName,
            'timeZone'=>$this->timeZone
        ));

        $jobView->setTemplate('jobs/service-jobs/list');
        $jobView->addChild($AddEmployeeView, 'AddEmployeeView');
        $this->getViewHelper('HeadScript')->prependFile('/js/jobs/service-job-list.js');
        return $jobView;


    }

    public function deletedListAction() {
        
        $this->getSideAndUpperMenus('Job', 'Deleted Jobs', 'Jobs');

        $locationID = $this->user_session->userActiveLocation['locationID'];
        $userDateFormat = $this->getUserDateFormat();
        $jobList = $this->getPaginatedJob($locationID, null, 5);
        $paginatedflag = true;
        $AddEmployeeView = new ViewModel();
        $AddEmployeeView->setTemplate('jobs/service-jobs/view-job-service-employee-modal');

        $actionName = 'deletedList';
        $jobView = new ViewModel(array(
            'jobList' => $jobList,
            'jbStatus' => 5,
            'paginated' => $paginatedflag,
            'status' => $this->getStatusesList(),
            'userDateFormat' => $userDateFormat,
            'actionName' => $actionName,
            'timeZone'=>$this->timeZone
        ));

        $jobView->setTemplate('jobs/service-jobs/list');
        $jobView->addChild($AddEmployeeView, 'AddEmployeeView');
        $this->getViewHelper('HeadScript')->prependFile('/js/jobs/service-job-list.js');
        return $jobView;


    }

    public function getPaginatedJob($locationID, $param = null, $status)
    {
        $this->job = $this->CommonTable('Jobs/Model/JobTable')->getJobListByStatus($status, $locationID, true);

        if (is_null($param)) {
            $this->job->setCurrentPageNumber((int) $this->params()->fromRoute('param1', 1));
        } else {
            $this->job->setCurrentPageNumber((int) $param);
        }
        $this->job->setItemCountPerPage(6);
        return $this->job;
    }    
}
