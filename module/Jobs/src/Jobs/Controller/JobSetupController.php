<?php

namespace Jobs\Controller;

use Core\Controller\CoreController;
use Zend\Session\Container;
use Zend\View\Model\ViewModel;
use Zend\I18n\Translator\Translator;
use Jobs\Form\JobTypeForm;

class JobSetupController extends CoreController
{

	protected $sideMenus = 'jobs_side_menu';
    protected $upperMenus = 'category_setting_upper_menu';
	protected $downMenus = 'job_setup_down_menu';

    public function indexAction()
    {
    	$this->getSideAndUpperMenus('Jobs Settings', 'Job Setup', 'Jobs', 'Category Settings');

    	$jobTypeList = $this->getPaginatedJobTypes();
        $form = new JobTypeForm();
        $view = new ViewModel(array(
            'form' => $form,
            'jobTypeList' => $jobTypeList,
            'paginated' => true
        ));
        $this->getViewHelper('HeadScript')->prependFile('/js/jobs/jobSetup.js');
        return $view;
    }

    public function getPaginatedJobTypes($perPage = 6, $param = null)
    {
        $this->paginator = $this->CommonTable('Jobs\Model\JobTypeTable')->fetchAll(true);
        
        if (is_null($param)) {
	        $this->paginator->setCurrentPageNumber((int) $this->params()->fromRoute('param1', 1));
        } else {
        	$this->paginator->setCurrentPageNumber((int) $param);
        }
        $this->paginator->setItemCountPerPage($perPage);
        $this->setLogMessage('Job Type list accessed');
        return $this->paginator;
    }
}
