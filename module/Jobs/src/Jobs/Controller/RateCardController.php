<?php

namespace Jobs\Controller;

use Zend\View\Model\ViewModel;
use Core\Controller\CoreController;
use Zend\Session\Container;

class RateCardController extends CoreController
{

    protected $sideMenus = 'jobs_side_menu_of_service';
    protected $upperMenus = 'cost_setting_upper_menu';
    protected $downMenus = 'job_setup_down_menu_of_service';

    public function createAction()
    {   
        $this->getSideAndUpperMenus('Jobs Settings', 'Rate Cards', 'Jobs', 'Cost Settings');
        $uoms = $this->CommonTable('Settings/Model/UomTable')->getUomforSearch();
        $uomList = array();
        foreach ($uoms as $val) {
            $temp['value'] = $val->uomID;
            $temp['text'] = $val->uomName . '(' . $val->uomAbbr.')';
            $uomList[] = $temp;
        }

        $serviceRatesAddView = new ViewModel(array(
            'currency' => $this->companyCurrencySymbol,
            'uomList' => $uomList
        ));
        $serviceRatesAddView->setTemplate('jobs/rate-card/service-rate-add-modal');

        //get rate card list
        $paginator = $this->getPaginatedRateCards();
        $rateCardView = new ViewModel(array(
                                'rateCards' => $paginator,
                                'paginated' => true,
                                'currency' => $this->companyCurrencySymbol,
                            ));

        $rateCardView->setTemplate('jobs/rate-card/create');
        $this->getViewHelper('HeadScript')->prependFile('/js/jobs/rate-card.js');
        $rateCardView->addChild($serviceRatesAddView, 'serviceRatesAddView');

        return $rateCardView;

    }

    public function getPaginatedRateCards($perPage = 6, $param = null)
    {

        $this->rateCards = $this->CommonTable('Jobs/Model/RateCardTable')->fetchAll(true);
        if ($param == null) {
            $this->rateCards->setCurrentPageNumber((int) $this->params()->fromRoute('param1', 1));
        } else {
            $this->rateCards->setCurrentPageNumber($param);     
        }
        $this->rateCards->setItemCountPerPage($perPage);
        return $this->rateCards;
    }
}
