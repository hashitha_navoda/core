<?php

namespace Jobs\Controller;

use Core\Controller\CoreController;
use Jobs\Form\EventForm;
use Zend\View\Model\ViewModel;

class EventController extends CoreController {

	protected $sideMenus = 'tournament_side_menu';
	protected $upperMenus = 'events_upper_menu';

	public function createAction() {
		$this->getSideAndUpperMenus('Events', 'Create Event', 'Jobs');

		$jobTypes = array();
		$jBResult = $this->CommonTable('Jobs\Model\JobTypetable')->fetchAll();
		foreach ($jBResult as $jType) {
			$jobTypes[$jType['jobTypeId']] = $jType['jobTypeCode'] . '_' . $jType['jobTypeName'];
		}

        $locationID = $this->user_session->userActiveLocation['locationID'];
        $maintainProject = $this->user_session->jobSettings->maintainProject;
        $contractorForJob = $this->user_session->jobSettings->contractorForJob;
        $contractorForTask = $this->user_session->jobSettings->contractorForTask;
        $rawMaterials = $this->user_session->jobSettings->rawMaterials;
		$fixedAssets = $this->user_session->jobSettings->fixedAssets;

		$reference = $this->getReferenceNoForLocation(41, $locationID);
		$jobReferenceNumber = $reference['refNo'];

        $jobContractors = array();
        $jBCResult = $this->CommonTable('Jobs\Model\ContractorTable')->fetchAll();
        foreach ($jBCResult as $jContractor) {
            $jobContractors[$jContractor['contractorID']] = $jContractor['contractorFirstName'] . ' ' . $jContractor['contractorSecondName'];
        }

        $uoms = array();
        $uomResult = $this->CommonTable('Settings\Model\UomTable')->fetchAll();
        foreach ($uomResult as $uom) {
            $uoms[$uom->uomID] = $uom->uomName . ' (' . $uom->uomAbbr . ')';
        }
          
		$form = new EventForm(array(
			'jobtypes' => $jobTypes,
			'contractors' => $jobContractors,
			'jobCode' => $jobReferenceNumber
        ));

        $userdateFormat = $this->getUserDateFormat();
        $view = new ViewModel(array(
            'jobForm' => $form,
            'userdateFormat' => $userdateFormat,
            'companyCurrencySymbol' => $this->companyCurrencySymbol,
            'maintainProject' => $maintainProject,
            'contractorForJob' => $contractorForJob
		));

		$jobSupervisor = new ViewModel();
		$jobSupervisor->setTemplate('/jobs/job/add-job-supervisors');
		$view->addChild($jobSupervisor, 'addJobSupervisors');

		$jobManager = new ViewModel();
		$jobManager->setTemplate('/jobs/job/add-job-managers');
		$view->addChild($jobManager, 'addJobManagers');

		$tasks = new ViewModel(array(
            'companyCurrencySymbol' => $this->companyCurrencySymbol,
			'uoms' => $uoms,
            'contractorForTask' => $contractorForTask
		));
		$tasks->setTemplate('/jobs/job/add-tasks');
		$view->addChild($tasks, 'addTasks');

		$taskEmployees = new ViewModel();
        $taskEmployees->setTemplate('/jobs/job/add-employee');
        $view->addChild($taskEmployees, 'taskEmployees');

        $taskContractors = new ViewModel(array(
            'contractors' => $jobContractors,
        ));
		$taskContractors->setTemplate('/jobs/job/add-task-contractors');
		$view->addChild($taskContractors, 'taskContractors');

		$taskProducts = new ViewModel(array(
			'companyCurrencySymbol' => $this->companyCurrencySymbol,
            'rawMaterials' => $rawMaterials,
            'fixedAssets' => $fixedAssets
		));
		$taskProducts->setTemplate('/jobs/event/add-task-products');
		$view->addChild($taskProducts, 'addTaskproducts');

        $this->department = $this->CommonTable('Jobs/Model/DepartmentTable')->fetchActiveDepartments();

        $departments = array();
        foreach ($this->department as $dept) {
            $res = [
                'departmentID' => $dept['departmentID'],
                'departmentName' => $dept['departmentName'],
                'departmentCode' => $dept['departmentCode'],
            ];
            array_push($departments, $res);
        }

        $this->designation = $this->CommonTable('Jobs/Model/DesignationTable')->fetchActiveDesignations();
        $designations = array();
        foreach ($this->designation as $desig) {
            $res = [
                'designationID' => $desig['designationID'],
                'designationName' => $desig['designationName'],
                'designationCode' => $desig['designationCode'],
            ];
            array_push($designations, $res);
        }
        $participantAddView = new ViewModel(array(
                'departments' => $departments,             
                'designations' => $designations
        ));
        $participantAddView->setTemplate('jobs/event/event-participant-add-modal');
        $view->addChild($participantAddView, 'participantAddView');

		$this->getViewHelper('HeadScript')->prependFile('/js/jobs/event.js');

        $this->getServiceLocator()->get('viewhelpermanager')->get('jsvarsHelper')->setVars([
            'jobSettings'
        ]);
		return $view;
	}

	public function listAction() 
	{
		$this->getSideAndUpperMenus('Events', 'View Event', 'Jobs');

		$locationID = $this->user_session->userActiveLocation['locationID'];
        $maintainProject = $this->user_session->jobSettings->maintainProject;
        $userDateFormat = $this->getUserDateFormat();
        $jobList = $this->getPaginatedJob($locationID);
        $paginatedflag = true;

        $projectList = $this->CommonTable('Jobs\Model\ProjectTable')->fetchAll();
        $tournamentOfficials = $this->CommonTable('Jobs\Model\EmployeeTable')->fetchAll();

        $jobView = new ViewModel(array(
        	'projectList' => $projectList,
        	'jobSupervisors' => $tournamentOfficials,
            'jobList' => $jobList,
            'paginated' => $paginatedflag,
            'status' => $this->getStatusesList(),
            'userDateFormat' => $userDateFormat,
            'maintainProject' => $maintainProject
        ));

        $this->getServiceLocator()->get('viewhelpermanager')->get('jsvarsHelper')->setVars([
            'customerListWithoutDefaultCustomer', 'getAllJobTypes', 'getAllProject', 'getEmployees'
        ]);
        $this->getViewHelper('HeadScript')->prependFile('/js/jobs/event-list.js');
        return $jobView;
	}

	public function getPaginatedJob($locationID, $param = null)
    {
        $this->job = $this->CommonTable('Jobs/Model/JobTable')->fetchAll($locationID, true);
        if (is_null($param)) {
            $this->job->setCurrentPageNumber((int) $this->params()->fromRoute('param1', 1));
        } else {
            $this->job->setCurrentPageNumber((int) $param);
        }
        $this->job->setItemCountPerPage(6);
        return $this->job;
    }

    public function viewAction()
    {
    	$paramIn = $this->params()->fromRoute('param1');
        if ($paramIn != NULL) {
            $this->getSideAndUpperMenus('Events', 'View Event', 'Jobs');
            $jobData = $this->CommonTable('Jobs\Model\JobTable')->getJobByJobID($paramIn);
            $jobDetails = array();
            $jobID;
            $tradingItemFlag = false;
            $fixedAssetsFlag = false;
            $rawmaterialFlag = false;
            foreach ($jobData as $row) {
                $tempG = array();
                $jobID = $row['jobId'];
                $tempG['jobId'] = $row['jobId'];
                $tempG['jobCode'] = $row['jobReferenceNumber'];
                $tempG['jobName'] = $row['jobName'];
                $tempG['jobCustomer'] = $row['customerCode'].'-'.$row['customerName'];
                $tempG['jobType'] = $row['jobTypeCode'].'-'.$row['jobTypeName'];
                $tempG['jobDueDate'] = $this->convertDateToUserFormat($row['jobStartDate']);
                $tempG['jobEstimatedEndDate'] = $this->convertDateToUserFormat($row['jobEstimatedEndDate']);
                $tempG['jobValue'] = $row['jobWeight'];
                $tempG['jobEstimatedCost'] = $row['jobEstimatedCost'];
                $jobTasks = (isset($jobDetails[$row['jobId']]['jobTasks'])) ? $jobDetails[$row['jobId']]['jobTasks'] : array();
                $jobProducts = (isset($jobDetails[$row['jobId']]['jobProducts'])) ? $jobDetails[$row['jobId']]['jobProducts'] : array();
                $jobSupervisors = (isset($jobDetails[$row['jobId']]['jobSupervisors'])) ? $jobDetails[$row['jobId']]['jobSupervisors'] : array();
                $jobManagers = (isset($jobDetails[$row['jobId']]['jobManagers'])) ? $jobDetails[$row['jobId']]['jobManagers'] : array();

                $jobContractors = (isset($jobDetails[$row['jobId']]['jobContractors'])) ? $jobDetails[$row['jobId']]['jobContractors'] : array();

                if (!is_null($row['jobManagerID'])) {
                    $jobManagers[$row['employeeIdOfJobManager']] = array('jobMangerName' => $row['jobMangerFirstName'].' '.$row['jobMangerSecondName']);
                }

                if (!is_null($row['jobContractorID'])) {
                	$jobContractors[$row['contractorIdOfJob']] = array('contractorName' => $row['jobContractorFirstName'].' '.$row['jobContractorLastName']);
                }

                if (!is_null($row['jobSupervisorTableID'])) {
                	$jobSupervisors[$row['jobSupervisorTableID']] = array('jobSupervisorName' => $row['jobSupervisorFirstName'].' '.$row['jobSupervisorSecondName'],'participantType' => $row['participantType'], 'participantSubType' => $row['participantSubType'], 'teamID' => $row['teamID']);
                }

                if (!is_null($row['jobProductID'])) {

                	$productUom = $this->CommonTable('Inventory\Model\ProductUomTable')->getProductUomListByProductID($row['productID']);

                    $thisqty = $this->getProductQuantityViaDisplayUom($row['jobProductAllocatedQty'], $productUom);
                    $thisqty['quantity'] = ($thisqty['quantity'] == 0) ? '-' : $thisqty['quantity'];

                	$jobProducts[$row['jobProductID']] = array('locationProductID' => $row['locationProductID'], 'jobProductUnitPrice' => $row['jobProductUnitPrice'], 'uomID' => $row['uomID'], 'jobProductAllocatedQty' => $thisqty['quantity'], 'jobProductReOrderLevel' => $row['jobProductReOrderLevel'], 'jobProductMaterialTypeID' => $row['jobProductMaterialTypeID'],'productCode' => $row['productCode'], 'productName' => $row['productName'],'uomAbbr' => $row['uomAbbr'], 'productEstimatedCost' => floatval($row['jobProductAllocatedQty']) * floatval($row['jobProductUnitPrice']));

                    if ($row['jobProductMaterialTypeID'] == "1") {
                        $tradingItemFlag = true;
                    }
                    if ($row['jobProductMaterialTypeID'] == "2") {
                        $fixedAssetsFlag = true;
                    }
                    if ($row['jobProductMaterialTypeID'] == "3") {
                        $rawmaterialFlag = true;
                    }
                }

                if ($row['jobTaskID'] != NULL) {
                    $taskKey = $row['jobTaskID'];

                    $taskEmployees = (isset($jobTasks[$taskKey]['jobEmployees'])) ? $jobTasks[$taskKey]['jobEmployees'] : array();
                    $taskContractors = (isset($jobTasks[$taskKey]['taskContractors'])) ? $jobTasks[$taskKey]['taskContractors'] : array();

                    $jobTasks[$taskKey] = array('taskCode' => $row['taskCode'], 'taskName' => $row['taskName'], 'jobTaskUnitCost' => $row['jobTaskUnitCost'],'jobTaskUnitRate' => $row['jobTaskUnitRate'], 'jobTaskEstQty' => $row['jobTaskEstQty'], 'taskUomID' => $row['taskUomID'], 'taskUomAbbr' => $row['taskUomAbbr'],'taskEstimatedCost' => floatval($row['jobTaskEstQty']) * floatval($row['jobTaskUnitCost']),'jobTaskID' => $row['jobTaskID']);
                    
                    if ($row['jobEmployeeID'] != NULL && $row['jobTaskID'] == $row['employeeJobTaskID']) {
                        $taskEmployees[$row['jobEmployeeID']] = array('employeeName' => $row['jobEmployeeFirstName'].' '.$row['jobEmployeeSecondName'].' ('.$row['designationName'].')', 'employeeJobTaskID' => $row['employeeJobTaskID']);
                    }
                    
                    if ($row['jobTaskContractorID'] != NULL && $row['jobTaskID'] == $row['contactorJobTaskID']) {
                        $taskContractors[$row['jobTaskContractorID']] = array('contractorName' => $row['jobTaskContractorFirstName'].' '.$row['jobTaskContractorLastName'], 'contactorJobTaskID' => $row['contactorJobTaskID']);
                    }
                    
                    $jobTasks[$taskKey]['jobEmployees'] = $taskEmployees;
                    $jobTasks[$taskKey]['taskContractors'] = $taskContractors;
                }
                
                $tempG['jobManagers'] = $jobManagers;
                $tempG['jobContractors'] = $jobContractors;
                $tempG['jobSupervisors'] = $jobSupervisors;
                $tempG['jobProducts'] = $jobProducts;
                $tempG['jobTasks'] = $jobTasks;
                $tempG['jobTaskLength'] = sizeof($jobTasks);
                $tempG['jobProductLength'] = sizeof($jobProducts);
                $tempG['jobSupervisorLength'] = sizeof($jobSupervisors);
                $tempG['jobContractorLength'] = sizeof($jobContractors);
                $tempG['jobManagerLength'] = sizeof($jobManagers);
                $tempG['tradingItemFlag'] = $tradingItemFlag;
                $tempG['fixedAssetsFlag'] = $fixedAssetsFlag;
                $tempG['rawmaterialFlag'] = $rawmaterialFlag;
                $jobDetails[$row['jobId']] = $tempG;
            
            }
            $JobForm = new EventForm();
            $JobForm->get('jobCode')->setValue($jobDetails[$jobID]['jobCode']);
            $JobForm->get('jobCode')->setAttribute('disabled', TRUE);
            $JobForm->get('jobName')->setValue($jobDetails[$jobID]['jobName']);
            $JobForm->get('jobName')->setAttribute('disabled', TRUE);
            $JobForm->get('jobType')->setValue($jobDetails[$jobID]['jobType']);
            $JobForm->get('jobType')->setAttribute('disabled', TRUE);
            $JobForm->get('customer')->setValue($jobDetails[$jobID]['jobCustomer']);
            $JobForm->get('customer')->setAttribute('disabled', TRUE);
            $JobForm->get('dueDate')->setValue($jobDetails[$jobID]['jobDueDate']);
            $JobForm->get('dueDate')->setAttribute('disabled', TRUE);
            $JobForm->get('estimatedEndDate')->setValue($jobDetails[$jobID]['jobEstimatedEndDate']);
            $JobForm->get('estimatedEndDate')->setAttribute('disabled', TRUE);
            $JobForm->get('jobValue')->setValue(number_format($jobDetails[$jobID]['jobValue'],2));
            $JobForm->get('jobValue')->setAttribute('disabled', TRUE);
            $JobForm->get('jobEstCost')->setValue(number_format($jobDetails[$jobID]['jobEstimatedCost'],2));
            $JobForm->get('jobEstCost')->setAttribute('disabled', TRUE);
            
            $jobView = new ViewModel(array(
            		'jobID' => $jobID,
                    'jobForm' => $JobForm,
                    'jobData' => $jobDetails[$jobID],
                    'companyCurrencySymbol' => $this->companyCurrencySymbol,
                ));

            $jobView->setTemplate('jobs/event/view-job');
            $this->setLogMessage("View job ".$tempG['jobId'].".");
            return $jobView;
        } else {
            $this->setLogMessage("Redirect to job list.");
            $this->redirect()->toRoute('jobs', array('action' => 'list'));
        }
    }

    public function editAction()
    {
    	$this->getSideAndUpperMenus('Events', 'Create Event', 'Jobs');

        $jobID = $this->params()->fromRoute('param1');
        $jobData = $this->CommonTable('Jobs\Model\JobTable')->getJobsByJobID($jobID);
        $jobContractorData = $this->CommonTable('Jobs\Model\JobContractorTable')->getJobContractorByJobID($jobID);
        
        $contractorForJob = $this->user_session->jobSettings->contractorForJob;
        $contractorForTask = $this->user_session->jobSettings->contractorForTask;
        $rawMaterials = $this->user_session->jobSettings->rawMaterials;
        $fixedAssets = $this->user_session->jobSettings->fixedAssets;

		$jobTypes = array();
		$jBResult = $this->CommonTable('Jobs\Model\JobTypetable')->fetchAll();
		foreach ($jBResult as $jType) {
			$jobTypes[$jType['jobTypeId']] = $jType['jobTypeCode'] . '_' . $jType['jobTypeName'];
		}


        $jobContractors = array();
        $jBCResult = $this->CommonTable('Jobs\Model\ContractorTable')->fetchAll();
        foreach ($jBCResult as $jContractor) {
            $jobContractors[$jContractor['contractorID']] = $jContractor['contractorFirstName'] . ' ' . $jContractor['contractorSecondName'];
        }
		$form = new EventForm(array(
			'jobtypes' => $jobTypes,
			'contractors' => $jobContractors,
			'jobCode' => $jobReferenceNumber,
		));

        $jContractors = [];
        foreach ($jobContractorData as $key => $value) {
            $jContractors[] = $value['contractorID'];     
        }


        $uoms = array();
        $uomResult = $this->CommonTable('Settings\Model\UomTable')->fetchAll();
        foreach ($uomResult as $uom) {
            $uoms[$uom->uomID] = $uom->uomName . ' (' . $uom->uomAbbr . ')';
        }

        $form->get('jobCode')->setValue($jobData->jobReferenceNumber);
        $form->get('jobEstCost')->setValue(number_format($jobData->jobEstimatedCost,2));
        $form->get('jobName')->setValue($jobData->jobName);
        $form->get('dueDate')->setValue($jobData->jobEstimatedEndDate);
        $form->get('expDate')->setValue($jobData->jobStartDate);
        $form->get('jobTypeIds')->setValue($jobData->jobTypeId);
        $form->get('contactorIds')->setValue($jContractors);

		$userdateFormat = $this->getUserDateFormat();
		$view = new ViewModel(array(
			'jobForm' => $form,
			'jobID' => $jobID,
			'userdateFormat' => $userdateFormat,
			'companyCurrencySymbol' => $this->companyCurrencySymbol,
            'contractorForJob' => $contractorForJob
		));

		$jobSupervisor = new ViewModel();
		$jobSupervisor->setTemplate('/jobs/job/add-job-supervisors');
		$view->addChild($jobSupervisor, 'addJobSupervisors');

		$jobManager = new ViewModel();
		$jobManager->setTemplate('/jobs/job/add-job-managers');
		$view->addChild($jobManager, 'addJobManagers');

		$tasks = new ViewModel(array(
			'companyCurrencySymbol' => $this->companyCurrencySymbol,
            'uoms' => $uoms,
            'contractorForTask' => $contractorForTask
		));
		$tasks->setTemplate('/jobs/job/add-tasks');
		$view->addChild($tasks, 'addTasks');

		$taskEmployees = new ViewModel();
		$taskEmployees->setTemplate('/jobs/job/add-employee');
		$view->addChild($taskEmployees, 'taskEmployees');

        $taskContractors = new ViewModel(array(
            'contractors' => $jobContractors
        ));
        $taskContractors->setTemplate('/jobs/job/add-task-contractors');
        $view->addChild($taskContractors, 'taskContractors');
        
		$taskProducts = new ViewModel(array(
            'companyCurrencySymbol' => $this->companyCurrencySymbol,
            'rawMaterials' => $rawMaterials,
			'fixedAssets' => $fixedAssets
		));
		$taskProducts->setTemplate('/jobs/event/add-task-products');
		$view->addChild($taskProducts, 'addTaskproducts');

        $this->department = $this->CommonTable('Jobs/Model/DepartmentTable')->fetchActiveDepartments();

        $departments = array();
        foreach ($this->department as $dept) {
            $res = [
                'departmentID' => $dept['departmentID'],
                'departmentName' => $dept['departmentName'],
                'departmentCode' => $dept['departmentCode'],
            ];
            array_push($departments, $res);
        }

        $this->designation = $this->CommonTable('Jobs/Model/DesignationTable')->fetchActiveDesignations();
        $designations = array();
        foreach ($this->designation as $desig) {
            $res = [
                'designationID' => $desig['designationID'],
                'designationName' => $desig['designationName'],
                'designationCode' => $desig['designationCode'],
            ];
            array_push($designations, $res);
        }
        $participantAddView = new ViewModel(array(
                'departments' => $departments,             
                'designations' => $designations
        ));
        $participantAddView->setTemplate('jobs/event/event-participant-add-modal');
        $view->addChild($participantAddView, 'participantAddView');


		$this->getViewHelper('HeadScript')->prependFile('/js/jobs/event.js');
		
        $this->getServiceLocator()->get('viewhelpermanager')->get('jsvarsHelper')->setVars([
            'jobSettings'
        ]);
        
		return $view;
    }
}