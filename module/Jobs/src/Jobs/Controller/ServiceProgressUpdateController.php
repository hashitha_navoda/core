<?php

namespace Jobs\Controller;

use Zend\View\Model\ViewModel;
use Core\Controller\CoreController;
use Zend\Session\Container;
use Invoice\Form\AddCustomerForm;

class ServiceProgressUpdateController extends CoreController
{

    protected $sideMenus = 'jobs_side_menu_of_service';
   
    public function __construct()
    {
        parent::__construct();
        $this->user_session = new Container('ezBizUser');
        $this->userID = $this->user_session->userID;
        $this->username = $this->user_session->username;
        $this->company = $this->user_session->companyDetails;
        $this->cdnUrl = $this->user_session->cdnUrl;
        $this->useAccounting = $this->user_session->useAccounting;
    }
 
    public function indexAction($projectId = null, $mode = false) {

        $this->getSideAndUpperMenus('Progress Updates', null, 'Jobs');

        $departmentList = $this->CommonTable('Jobs/Model/DepartmentTable')->fetchAll(false);
        foreach ($completedJobList as $key => $part) {
           $sort[$key] = strtotime($part['updatedTimeStamp']);
        }
        array_multisort($sort, SORT_DESC, $completedJobList);

        $view = new ViewModel(array(
           'departmentList' => $departmentList
        ));

        $view->setTemplate('jobs/service-progress-update/index');
        $this->getViewHelper('HeadScript')->prependFile('/js/jobs/service-progress-update.js');
        return $view;   
    }
}