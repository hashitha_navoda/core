<?php

namespace Jobs\Controller;

use Zend\View\Model\ViewModel;
use Core\Controller\CoreController;
use Zend\Session\Container;

class DepartmentStationController extends CoreController
{

    protected $sideMenus = 'jobs_side_menu_of_service';
    protected $upperMenus = 'department_station_upper_menu';
    protected $downMenus = 'job_setup_down_menu_of_service';

    public function createAction()
    {   
        $this->getSideAndUpperMenus('Jobs Settings', 'Department & Station', 'Jobs', 'Department & Stations');

        $departmentStationAddView = new ViewModel();
        $departmentStationAddView->setTemplate('jobs/departmentStation/department-station-add-modal');

        //get department list
        $paginator = $this->getPaginatedDepartments();
        $departmentView = new ViewModel(array(
                                'departments' => $paginator,
                                'paginated' => true,
                            ));

        $departmentView->setTemplate('jobs/departmentStation/create');
        $this->getViewHelper('HeadScript')->prependFile('/js/jobs/departmentStation.js');
        $departmentView->addChild($departmentStationAddView, 'departmentStationAddView');

        return $departmentView;

    }

    public function getPaginatedDepartments($perPage = 6, $param = null)
    {
        $this->department = $this->CommonTable('Jobs/Model/DepartmentTable')->fetchAll(true, true);
        if ($param == null) {
            $this->department->setCurrentPageNumber((int) $this->params()->fromRoute('param1', 1));
        } else {
            $this->department->setCurrentPageNumber($param);     
        }
        $this->department->setItemCountPerPage($perPage);
        return $this->department;
    }
}
