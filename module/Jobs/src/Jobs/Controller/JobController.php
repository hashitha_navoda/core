<?php

namespace Jobs\Controller;

use Core\Controller\CoreController;
use Jobs\Form\JobForm;
use Zend\View\Model\ViewModel;
use Zend\I18n\Translator\Translator;

class JobController extends CoreController {

	protected $sideMenus = 'jobs_side_menu';
	protected $upperMenus = 'jobs_job_upper_menu';

	public function createAction() {
		$this->getSideAndUpperMenus('Jobs', 'Create Job', 'Jobs', 'Jobs - create');

		$jobTypes = array();
		$jBResult = $this->CommonTable('Jobs\Model\JobTypetable')->fetchAll();
		foreach ($jBResult as $jType) {
			$jobTypes[$jType['jobTypeId']] = $jType['jobTypeCode'] . '_' . $jType['jobTypeName'];
		}

        $locationID = $this->user_session->userActiveLocation['locationID'];
        $maintainProject = $this->user_session->jobSettings->maintainProject;
        $contractorForJob = $this->user_session->jobSettings->contractorForJob;
        $contractorForTask = $this->user_session->jobSettings->contractorForTask;
        $rawMaterials = $this->user_session->jobSettings->rawMaterials;
		$fixedAssets = $this->user_session->jobSettings->fixedAssets;

		$reference = $this->getReferenceNoForLocation(22, $locationID);
		$jobReferenceNumber = $reference['refNo'];

        $jobContractors = array();
        $jBCResult = $this->CommonTable('Jobs\Model\ContractorTable')->fetchAll();
        foreach ($jBCResult as $jContractor) {
            $jobContractors[$jContractor['contractorID']] = $jContractor['contractorFirstName'] . ' ' . $jContractor['contractorSecondName'];
        }

        $uoms = array();
        $uomResult = $this->CommonTable('Settings\Model\UomTable')->fetchAll();
        foreach ($uomResult as $uom) {
            $uoms[$uom->uomID] = $uom->uomName . ' (' . $uom->uomAbbr . ')';
        }
          
		$form = new JobForm(array(
			'jobtypes' => $jobTypes,
			'contractors' => $jobContractors,
			'jobCode' => $jobReferenceNumber
        ));

        $userdateFormat = $this->getUserDateFormat();
        $view = new ViewModel(array(
            'jobForm' => $form,
            'userdateFormat' => $userdateFormat,
            'companyCurrencySymbol' => $this->companyCurrencySymbol,
            'maintainProject' => $maintainProject,
            'contractorForJob' => $contractorForJob
		));

		$jobSupervisor = new ViewModel();
		$jobSupervisor->setTemplate('/jobs/job/add-job-supervisors');
		$view->addChild($jobSupervisor, 'addJobSupervisors');

		$jobManager = new ViewModel();
		$jobManager->setTemplate('/jobs/job/add-job-managers');
		$view->addChild($jobManager, 'addJobManagers');

		$tasks = new ViewModel(array(
            'companyCurrencySymbol' => $this->companyCurrencySymbol,
			'uoms' => $uoms,
            'contractorForTask' => $contractorForTask
		));
		$tasks->setTemplate('/jobs/job/add-tasks');
		$view->addChild($tasks, 'addTasks');

		$taskEmployees = new ViewModel();
        $taskEmployees->setTemplate('/jobs/job/add-employee');
        $view->addChild($taskEmployees, 'taskEmployees');

        $taskContractors = new ViewModel(array(
            'contractors' => $jobContractors,
        ));
		$taskContractors->setTemplate('/jobs/job/add-task-contractors');
		$view->addChild($taskContractors, 'taskContractors');

		$taskProducts = new ViewModel(array(
			'companyCurrencySymbol' => $this->companyCurrencySymbol,
            'rawMaterials' => $rawMaterials,
            'fixedAssets' => $fixedAssets
		));
		$taskProducts->setTemplate('/jobs/job/add-task-products');
		$view->addChild($taskProducts, 'addTaskproducts');

		$this->getViewHelper('HeadScript')->prependFile('/js/jobs/job.js');

        $this->getServiceLocator()->get('viewhelpermanager')->get('jsvarsHelper')->setVars([
            'jobSettings'
        ]);
		return $view;
	}

	public function listAction() 
	{
		$this->getSideAndUpperMenus('Jobs', 'View Job', 'Jobs');

		$locationID = $this->user_session->userActiveLocation['locationID'];
        $maintainProject = $this->user_session->jobSettings->maintainProject;
        $userDateFormat = $this->getUserDateFormat();
        $jobList = $this->getPaginatedJob($locationID);
        $paginatedflag = true;

        $projectList = $this->CommonTable('Jobs\Model\ProjectTable')->fetchAll();
        $jobSupervisors = $this->CommonTable('Jobs\Model\EmployeeDesignationTable')->fetchEmployeeByDesignationCode('DES004');

        $jobView = new ViewModel(array(
        	'projectList' => $projectList,
        	'jobSupervisors' => $jobSupervisors,
            'jobList' => $jobList,
            'paginated' => $paginatedflag,
            'status' => $this->getStatusesList(),
            'userDateFormat' => $userDateFormat,
            'maintainProject' => $maintainProject
        ));

        $this->getServiceLocator()->get('viewhelpermanager')->get('jsvarsHelper')->setVars([
            'customerListWithoutDefaultCustomer', 'getAllJobTypes', 'getAllProject', 'getEmployees'
        ]);
        $this->getViewHelper('HeadScript')->prependFile('/js/jobs/job-list.js');
        return $jobView;
	}

	public function getPaginatedJob($locationID, $param = null)
    {
        $this->job = $this->CommonTable('Jobs/Model/JobTable')->fetchAll($locationID, true);
        if (is_null($param)) {
            $this->job->setCurrentPageNumber((int) $this->params()->fromRoute('param1', 1));
        } else {
            $this->job->setCurrentPageNumber((int) $param);
        }
        $this->job->setItemCountPerPage(6);
        return $this->job;
    }

    public function viewAction()
    {
    	$paramIn = $this->params()->fromRoute('param1');
        if ($paramIn != NULL) {
            $this->getSideAndUpperMenus('Jobs', 'View Job', 'Jobs');
            $jobData = $this->CommonTable('Jobs\Model\JobTable')->getJobByJobID($paramIn);
            $jobDetails = array();
            $jobID;
            $tradingItemFlag = false;
            $fixedAssetsFlag = false;
            $rawmaterialFlag = false;
            foreach ($jobData as $row) {
                $tempG = array();
                $jobID = $row['jobId'];
                $tempG['jobId'] = $row['jobId'];
                $tempG['jobCode'] = $row['jobReferenceNumber'];
                $tempG['jobName'] = $row['jobName'];
                $tempG['jobCustomer'] = $row['customerCode'].'-'.$row['customerName'];
                $tempG['jobType'] = $row['jobTypeCode'].'-'.$row['jobTypeName'];
                $tempG['jobDueDate'] = $this->convertDateToUserFormat($row['jobDueDate']);
                $tempG['jobEstimatedEndDate'] = $this->convertDateToUserFormat($row['jobEstimatedEndDate']);
                $tempG['jobValue'] = $row['jobWeight'];
                $tempG['jobEstimatedCost'] = $row['jobEstimatedCost'];
                $jobTasks = (isset($jobDetails[$row['jobId']]['jobTasks'])) ? $jobDetails[$row['jobId']]['jobTasks'] : array();
                $jobProducts = (isset($jobDetails[$row['jobId']]['jobProducts'])) ? $jobDetails[$row['jobId']]['jobProducts'] : array();
                $jobSupervisors = (isset($jobDetails[$row['jobId']]['jobSupervisors'])) ? $jobDetails[$row['jobId']]['jobSupervisors'] : array();
                $jobManagers = (isset($jobDetails[$row['jobId']]['jobManagers'])) ? $jobDetails[$row['jobId']]['jobManagers'] : array();

                $jobContractors = (isset($jobDetails[$row['jobId']]['jobContractors'])) ? $jobDetails[$row['jobId']]['jobContractors'] : array();

                if (!is_null($row['jobManagerID'])) {
                    $jobManagers[$row['employeeIdOfJobManager']] = array('jobMangerName' => $row['jobMangerFirstName'].' '.$row['jobMangerSecondName']);
                }

                if (!is_null($row['jobContractorID'])) {
                	$jobContractors[$row['contractorIdOfJob']] = array('contractorName' => $row['jobContractorFirstName'].' '.$row['jobContractorLastName']);
                }

                if (!is_null($row['jobSupervisorTableID'])) {
                	$jobSupervisors[$row['jobSupervisorID']] = array('jobSupervisorName' => $row['jobSupervisorFirstName'].' '.$row['jobSupervisorSecondName']);
                }

                if (!is_null($row['jobProductID'])) {

                	$productUom = $this->CommonTable('Inventory\Model\ProductUomTable')->getProductUomListByProductID($row['productID']);

                    $thisqty = $this->getProductQuantityViaDisplayUom($row['jobProductAllocatedQty'], $productUom);
                    $thisqty['quantity'] = ($thisqty['quantity'] == 0) ? '-' : $thisqty['quantity'];

                	$jobProducts[$row['jobProductID']] = array('locationProductID' => $row['locationProductID'], 'jobProductUnitPrice' => $row['jobProductUnitPrice'], 'uomID' => $row['uomID'], 'jobProductAllocatedQty' => $thisqty['quantity'], 'jobProductReOrderLevel' => $row['jobProductReOrderLevel'], 'jobProductMaterialTypeID' => $row['jobProductMaterialTypeID'],'productCode' => $row['productCode'], 'productName' => $row['productName'],'uomAbbr' => $row['uomAbbr'], 'productEstimatedCost' => floatval($row['jobProductAllocatedQty']) * floatval($row['jobProductUnitPrice']));

                    if ($row['jobProductMaterialTypeID'] == "1") {
                        $tradingItemFlag = true;
                    }
                    if ($row['jobProductMaterialTypeID'] == "2") {
                        $fixedAssetsFlag = true;
                    }
                    if ($row['jobProductMaterialTypeID'] == "3") {
                        $rawmaterialFlag = true;
                    }
                }

                if ($row['jobTaskID'] != NULL) {
                    $taskKey = $row['jobTaskID'];

                    $taskEmployees = (isset($jobTasks[$taskKey]['jobEmployees'])) ? $jobTasks[$taskKey]['jobEmployees'] : array();
                    $taskContractors = (isset($jobTasks[$taskKey]['taskContractors'])) ? $jobTasks[$taskKey]['taskContractors'] : array();

                    $jobTasks[$taskKey] = array('taskCode' => $row['taskCode'], 'taskName' => $row['taskName'], 'jobTaskUnitCost' => $row['jobTaskUnitCost'],'jobTaskUnitRate' => $row['jobTaskUnitRate'], 'jobTaskEstQty' => $row['jobTaskEstQty'], 'taskUomID' => $row['taskUomID'], 'taskUomAbbr' => $row['taskUomAbbr'],'taskEstimatedCost' => floatval($row['jobTaskEstQty']) * floatval($row['jobTaskUnitCost']),'jobTaskID' => $row['jobTaskID']);
                    
                    if ($row['jobEmployeeID'] != NULL && $row['jobTaskID'] == $row['employeeJobTaskID']) {
                        $taskEmployees[$row['jobEmployeeID']] = array('employeeName' => $row['jobEmployeeFirstName'].' '.$row['jobEmployeeSecondName'].' ('.$row['designationName'].')', 'employeeJobTaskID' => $row['employeeJobTaskID']);
                    }
                    
                    if ($row['jobTaskContractorID'] != NULL && $row['jobTaskID'] == $row['contactorJobTaskID']) {
                        $taskContractors[$row['jobTaskContractorID']] = array('contractorName' => $row['jobTaskContractorFirstName'].' '.$row['jobTaskContractorLastName'], 'contactorJobTaskID' => $row['contactorJobTaskID']);
                    }
                    
                    $jobTasks[$taskKey]['jobEmployees'] = $taskEmployees;
                    $jobTasks[$taskKey]['taskContractors'] = $taskContractors;
                }
                
                $tempG['jobManagers'] = $jobManagers;
                $tempG['jobContractors'] = $jobContractors;
                $tempG['jobSupervisors'] = $jobSupervisors;
                $tempG['jobProducts'] = $jobProducts;
                $tempG['jobTasks'] = $jobTasks;
                $tempG['jobTaskLength'] = sizeof($jobTasks);
                $tempG['jobProductLength'] = sizeof($jobProducts);
                $tempG['jobSupervisorLength'] = sizeof($jobSupervisors);
                $tempG['jobContractorLength'] = sizeof($jobContractors);
                $tempG['jobManagerLength'] = sizeof($jobManagers);
                $tempG['tradingItemFlag'] = $tradingItemFlag;
                $tempG['fixedAssetsFlag'] = $fixedAssetsFlag;
                $tempG['rawmaterialFlag'] = $rawmaterialFlag;
                $jobDetails[$row['jobId']] = $tempG;
            
            }

            $JobForm = new JobForm();
            $JobForm->get('jobCode')->setValue($jobDetails[$jobID]['jobCode']);
            $JobForm->get('jobCode')->setAttribute('disabled', TRUE);
            $JobForm->get('jobName')->setValue($jobDetails[$jobID]['jobName']);
            $JobForm->get('jobName')->setAttribute('disabled', TRUE);
            $JobForm->get('jobType')->setValue($jobDetails[$jobID]['jobType']);
            $JobForm->get('jobType')->setAttribute('disabled', TRUE);
            $JobForm->get('customer')->setValue($jobDetails[$jobID]['jobCustomer']);
            $JobForm->get('customer')->setAttribute('disabled', TRUE);
            $JobForm->get('dueDate')->setValue($jobDetails[$jobID]['jobDueDate']);
            $JobForm->get('dueDate')->setAttribute('disabled', TRUE);
            $JobForm->get('estimatedEndDate')->setValue($jobDetails[$jobID]['jobEstimatedEndDate']);
            $JobForm->get('estimatedEndDate')->setAttribute('disabled', TRUE);
            $JobForm->get('jobValue')->setValue(number_format($jobDetails[$jobID]['jobValue'],2));
            $JobForm->get('jobValue')->setAttribute('disabled', TRUE);
            $JobForm->get('jobEstCost')->setValue(number_format($jobDetails[$jobID]['jobEstimatedCost'],2));
            $JobForm->get('jobEstCost')->setAttribute('disabled', TRUE);
            

            $jobView = new ViewModel(array(
            		'jobID' => $jobID,
                    'jobForm' => $JobForm,
                    'jobData' => $jobDetails[$jobID],
                    'companyCurrencySymbol' => $this->companyCurrencySymbol,
                ));

            $jobView->setTemplate('jobs/job/view-job');
            $this->setLogMessage("View job ".$tempG['jobId'].".");
            return $jobView;
        } else {
            $this->setLogMessage("Redirect to job list.");
            $this->redirect()->toRoute('jobs', array('action' => 'list'));
        }
    }

    public function editAction()
    {
    	$this->getSideAndUpperMenus('Jobs', 'Create Job', 'Jobs');

        $jobID = $this->params()->fromRoute('param1');
        $jobData = $this->CommonTable('Jobs\Model\JobTable')->getJobsByJobID($jobID);
        $jobContractorData = $this->CommonTable('Jobs\Model\JobContractorTable')->getJobContractorByJobID($jobID);
        
        $contractorForJob = $this->user_session->jobSettings->contractorForJob;
        $contractorForTask = $this->user_session->jobSettings->contractorForTask;
        $rawMaterials = $this->user_session->jobSettings->rawMaterials;
        $fixedAssets = $this->user_session->jobSettings->fixedAssets;

		$jobTypes = array();
		$jBResult = $this->CommonTable('Jobs\Model\JobTypetable')->fetchAll();
		foreach ($jBResult as $jType) {
			$jobTypes[$jType['jobTypeId']] = $jType['jobTypeCode'] . '_' . $jType['jobTypeName'];
		}


        $jobContractors = array();
        $jBCResult = $this->CommonTable('Jobs\Model\ContractorTable')->fetchAll();
        foreach ($jBCResult as $jContractor) {
            $jobContractors[$jContractor['contractorID']] = $jContractor['contractorFirstName'] . ' ' . $jContractor['contractorSecondName'];
        }
		$form = new JobForm(array(
			'jobtypes' => $jobTypes,
			'contractors' => $jobContractors,
			'jobCode' => $jobReferenceNumber,
		));

        $jContractors = [];
        foreach ($jobContractorData as $key => $value) {
            $jContractors[] = $value['contractorID'];     
        }


        $uoms = array();
        $uomResult = $this->CommonTable('Settings\Model\UomTable')->fetchAll();
        foreach ($uomResult as $uom) {
            $uoms[$uom->uomID] = $uom->uomName . ' (' . $uom->uomAbbr . ')';
        }

        $form->get('jobCode')->setValue($jobData->jobReferenceNumber);
        $form->get('jobEstCost')->setValue(number_format($jobData->jobEstimatedCost,2));
        $form->get('jobValue')->setValue(number_format($jobData->jobWeight,2));
        $form->get('jobName')->setValue($jobData->jobName);
        $form->get('dueDate')->setValue($jobData->jobDueDate);
        $form->get('expDate')->setValue($jobData->jobEstimatedEndDate);
        $form->get('jobTypeIds')->setValue($jobData->jobTypeId);
        $form->get('contactorIds')->setValue($jContractors);

		$userdateFormat = $this->getUserDateFormat();
		$view = new ViewModel(array(
			'jobForm' => $form,
			'jobID' => $jobID,
			'userdateFormat' => $userdateFormat,
			'companyCurrencySymbol' => $this->companyCurrencySymbol,
            'contractorForJob' => $contractorForJob
		));

		$jobSupervisor = new ViewModel();
		$jobSupervisor->setTemplate('/jobs/job/add-job-supervisors');
		$view->addChild($jobSupervisor, 'addJobSupervisors');

		$jobManager = new ViewModel();
		$jobManager->setTemplate('/jobs/job/add-job-managers');
		$view->addChild($jobManager, 'addJobManagers');

		$tasks = new ViewModel(array(
			'companyCurrencySymbol' => $this->companyCurrencySymbol,
            'uoms' => $uoms,
            'contractorForTask' => $contractorForTask
		));
		$tasks->setTemplate('/jobs/job/add-tasks');
		$view->addChild($tasks, 'addTasks');

		$taskEmployees = new ViewModel();
		$taskEmployees->setTemplate('/jobs/job/add-employee');
		$view->addChild($taskEmployees, 'taskEmployees');

        $taskContractors = new ViewModel(array(
            'contractors' => $jobContractors
        ));
        $taskContractors->setTemplate('/jobs/job/add-task-contractors');
        $view->addChild($taskContractors, 'taskContractors');
        
		$taskProducts = new ViewModel(array(
            'companyCurrencySymbol' => $this->companyCurrencySymbol,
            'rawMaterials' => $rawMaterials,
			'fixedAssets' => $fixedAssets
		));
		$taskProducts->setTemplate('/jobs/job/add-task-products');
		$view->addChild($taskProducts, 'addTaskproducts');

		$this->getViewHelper('HeadScript')->prependFile('/js/jobs/job.js');
		
        $this->getServiceLocator()->get('viewhelpermanager')->get('jsvarsHelper')->setVars([
            'jobSettings'
        ]);
        
		return $view;
    }

    public function documentPreviewAction()
    {

        $jobID = $this->params()->fromRoute('param1');
        $data = $this->getDataForDocument($jobID);


        if (!$data['customerName']) {
            $customerSalutaion = "";
            $customerNameString = "";
        } else {
            $customerSalutaion = "Dear " . $data['customerName'] . ",";
            $customerNameString = "<strong>Customer Name:</strong>" . $data['customerName'] . " <br />";
        }

        $path = "/jobs/document/"; //.$invoiceID;
        $translator = new Translator();
        // $createNew = $translator->translate('New Job');
        // $createPath = "/job/create";
        $data["email"] = array();
        $documentType = 'Job';
      
        $jobView = $this->getCommonPreview($data, $path, null, $documentType, $jobID, null);
        $jobView->setTerminal(TRUE);

        return $jobView;
    }


    public function getDataForDocument($jobID)
    {

        if (!empty($this->_jobViewData)) {
            return $this->_jobViewData;
        }

        $data = $this->getDataForDocumentView();

        $jobDetails = (array) $this->CommonTable('Jobs\Model\JobTable')->getJobByJobID($jobID)->current();

        $jobTasks = $this->CommonTable('Jobs\Model\JobTaskTable')->getJobTaskDetails($jobID);
        foreach ($jobTasks as $key => $value) {
            $jobDetails['jobTasks'][$value['jobTaskID']] = [
                "taskCode" => $value['taskCode'],
                "taskName" => $value['taskName'],
                "jobTaskUnitRate" => floatval($value['jobTaskUnitRate']),
                "jobTaskEstQty" => floatval($value['jobTaskEstQty']),
                "total" => floatval($value['jobTaskEstQty']) * floatval($value['jobTaskUnitRate']),
            ];
        }


        $jobData = $this->CommonTable('Jobs\Model\JobTable')->getJobByJobID($jobID);
        $jobProductData = [];
        foreach ($jobData as $key => $val) {

            $productUom = $this->CommonTable('Inventory\Model\ProductUomTable')->getProductUomListByProductID($val['productID']);

            $thisqty = $this->getProductQuantityViaDisplayUom($val['jobProductAllocatedQty'], $productUom);
            $availableQty = $this->getProductQuantityViaDisplayUom($val['locationProductQuantity'], $productUom);
            $thisqty['quantity'] = ($thisqty['quantity'] == 0) ? '-' : $thisqty['quantity'];
            $availableQty['quantity'] = ($availableQty['quantity'] == 0) ? '-' : $availableQty['quantity'];


            if ($val['jobProductMaterialTypeID'] == "1") {
                $itemType = "Trading Items";
            }
            // if ($val['jobProductMaterialTypeID'] == "2") {
            //     $itemType = "Fixed Asset Item";
            // }
            if ($val['jobProductMaterialTypeID'] == "3") {
                $itemType = "Raw Material Items";
            }

            $reqQty = ($availableQty['quantity'] < $thisqty['quantity']) ? $thisqty['quantity'] - $availableQty['quantity'] : 0;             

            if ($val['jobProductMaterialTypeID'] != "2" && $val['jobProductMaterialTypeID'] != NULL) {
                $temp = array('locationProductID' => $val['locationProductID'], 'jobProductUnitPrice' => $val['jobProductUnitPrice'], 'locationProductQuantity' => $availableQty['quantity'], 'reuiredQuantity' => $reqQty, 'purchasingUnitPrice' => ($val['productTypeID'] == 2) ? $val['jobProductUnitPrice'] : $val['defaultPurchasePrice'], 'uomID' => $val['uomID'], 'jobProductAllocatedQty' => $thisqty['quantity'], 'jobProductReOrderLevel' => $val['jobProductReOrderLevel'], 'jobProductMaterialTypeID' => $val['jobProductMaterialTypeID'],'productCode' => $val['productCode'], 'productName' => $val['productName'],'uomAbbr' => $val['uomAbbr'], 'productEstimatedCost' => floatval($val['jobProductAllocatedQty']) * floatval($val['jobProductUnitPrice']));
                $jobProductData[$itemType][] = $temp;
            }

        }

        $jobDetails['customerData'] = $jobDetails['customerName'].'-'.$jobDetails['customerCode'];
        $jobDetails['jobProductData'] = $jobProductData;

        $data = array_merge($data, $jobDetails);

        return $this->_jobViewData = $data;
    }

    public function documentAction()
    {
        $sm = $this->getServiceLocator();
        $tpl = $sm->get('Template');
        $documentType = 'Job';
        $jobID = $this->params()->fromRoute('param1');
        $templateID = $this->params()->fromRoute('param2');


        // If template ID is not passed, get default template
        if (!$templateID) {
            $defaultTpl = $tpl->getDefaultTemplate($documentType);
            $templateID = $defaultTpl['templateID'];
        }
        
        // if saved document file is available, render it
        $filename = $this->getDocumentFileName($documentType, $jobID, $templateID);

        // Get template details
        $templateDetails = $tpl->getTemplateByID($templateID);

        $data = $this->getDataForDocument($jobID);

        // Get data table (eg: products list, total, etc.)
        $dataTableView = new ViewModel(array());
        $dataTableView->setTerminal(TRUE);

        switch (strtolower($templateDetails['documentSizeName'])) {
            case 'a4 (portrait)':
            case 'a4 (landscape)':
            case 'a5 (portrait)':
            case 'a5 (landscape)':
                $data['data_table'] = $this->getDocumentDataTable($jobID, strtolower(trim(preg_replace('/\(.*\)/i', '', $templateDetails['documentSizeName']))));
                break;
        }

        echo $tpl->renderTemplateByID($templateID, $data, $filename);
        exit;
    }

    public function getDocumentDataTable($jobID, $documentSize = 'A4')
    {
        
        $data_table_vars = $this->getDataForDocument($jobID);
        $view = new ViewModel($data_table_vars);
        $view->setTemplate('/jobs/job/templates/' . strtolower($documentSize));
        $view->setTerminal(TRUE);

        return $this->getServiceLocator()
                        ->get('viewrenderer')
                        ->render($view);
    }
}