<?php

namespace Jobs\Controller;

use Core\Controller\CoreController;
use Zend\View\Model\ViewModel;

class IncentivesController extends CoreController {

    protected $sideMenus = 'jobs_side_menu_of_service';
    protected $upperMenus = 'cost_setting_upper_menu';
    protected $downMenus = 'job_setup_down_menu_of_service';

    public function indexAction()
    {   
        $this->getSideAndUpperMenus('Jobs Settings', 'Rate Cards and Incentives', 'Jobs', 'Cost Settings');

        $incentiveType = $this->getServiceLocator()->get('config')['incentive_type'];

        $view = new ViewModel();

        $serviceList = new ViewModel(array(
            'incentiveType' => $incentiveType,
        ));
        $serviceList->setTemplate('/jobs/incentives/service-list');
        $view->addChild($serviceList, 'serviceList');

        // $this->getViewHelper('HeadScript')->prependFile('/js/jobs/incentives.js');
        $this->getViewHelper('HeadScript')->prependFile('/js/jobs/incentive_rates.js');
        $this->getServiceLocator()->get('viewhelpermanager')->get('jsvarsHelper')->setVars([
            'jobSettings'
        ]);
        return $view;
    }
}