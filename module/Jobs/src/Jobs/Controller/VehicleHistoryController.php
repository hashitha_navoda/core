<?php

namespace Jobs\Controller;

use Core\Controller\CoreController;
use Jobs\Form\JobForm;
use Zend\View\Model\ViewModel;

class VehicleHistoryController extends CoreController {

	protected $sideMenus = 'jobs_side_menu_of_service';
    protected $upperMenus = 'jobs_job_upper_menu';

	public function indexAction() {

        $this->getSideAndUpperMenus('History', null, 'Jobs');

        $addView = new ViewModel();

        $this->getViewHelper('HeadScript')->prependFile('/js/jobs/vehicle-history.js');
        $this->getViewHelper('HeadLink')->prependStylesheet('/css/jobs/vehicle-history.css');

        return $addView;
    }
}