<?php

namespace Jobs\Service;

use Core\Service\BaseService;
use Zend\I18n\Translator\Translator;
use Zend\View\Model\ViewModel;
use Jobs\Model\MaterialRequest;
use Jobs\Model\MaterialRequestJobProduct;

class MaterialRequisitionService extends BaseService
{

    public function getPaginatedRequests()
    {
        $requests = $this->getModel('MaterialRequestTable')->all(true);
        return $this->returnSuccess($requests);
    }


    public function approveRequest($data)
    {
        $reqId = $data['requestId'];
        $statusId = 16; // approved
        $res = $this->getModel('MaterialRequestTable')->updateStatus($reqId, $statusId);
        if (!$res) {
            return $this->returnError('ERR_APPR_RQST');
        }

        // update job product approved quantity
        // TODO: This part should be move to job product service
        //       Approved quantity need to be validate against jobProductAllocatedQty
        //       Rename jobProductRequestedQty to jobProductAprovedQty
        $reqProducts = $this->getModel('MaterialRequestJobProductTable')->findByRequestId($reqId);
        foreach ($reqProducts as $product) {
            $approvedProducts[] = [
                'jobProductId' => $product['jobProductId'],
                'qty' => $product['requestedQuantity']
            ];
        }
        $jobProduct = $this->getModel('JobProductTable')->updateApprovedQuantity($approvedProducts);
        if (!$jobProduct) {
            return $this->returnError('ERR_APPR_RQST');
        }

        return $this->returnSuccess(null, 'SUCC_APPR_RQST');
    }


    public function findRequests($key, $formDate = null, $toDate = null, $fixDate = null)
    {
        $requests = $this->getModel('MaterialRequestTable')->search($key, $formDate, $toDate, $fixDate);

        return $this->returnSuccess($requests);
    }


    public function saveMaterialRequest($data, $userId)
    {
        $jobId = $data['jobId'];
        $referenceService = $this->getService('ReferenceService');
        $refData = $referenceService->getReferenceNumber(36);
        $materialRequestCode = $refData['data']["referenceNo"];
        $locationReferenceID = $refData['data']["locationReferenceID"];
        $materialRequestDate = $this->convertDateToStandardFormat($data['materialRequestDate']);
        $materialRequestStatus = $this->getStatusIDByStatusName('In Progress');
        $entityId = $this->getService('EntityService')->createEntity($userId)['data']['entityID'];
        $materialRequestData = array(
            'jobId' => $jobId,
            'materialRequestCode' => $materialRequestCode,
            'materialRequestDate' => $materialRequestDate,
            'materialRequestStatus' => $materialRequestStatus,
            'entityId' => $entityId,
        );
        $materialRequest = new MaterialRequest();
        $materialRequest->exchangeArray($materialRequestData);
        $materialRequestId = $this->getModel('MaterialRequestTable')->save($materialRequest);
        if (!$materialRequestId) {
            return $this->returnError('ERR_REQ_MATE_SAVE', null);
        }
        $referenceService->updateReferenceNumber($locationReferenceID);

        $errorFlag = TRUE;
        $updateJobPro = [];
        foreach ($data['reqMateProducts'] as $key => $value) {
            $value['materialRequestId'] = $materialRequestId;
            $materialRequestJobProduct = new MaterialRequestJobProduct();
            $materialRequestJobProduct->exchangeArray($value);
            $mateReqRespond = $this->getModel('MaterialRequestJobProductTable')->save($materialRequestJobProduct);
            $jobProdRespond = $this->getModel('JobProductTable')->updateAllocatedQty($value['jobProductId'], $value['requestedQuantity']);
            if (!$mateReqRespond || !$jobProdRespond) {
                $errorFlag = FALSE;
            }

            $updatedProduct['jobProductId'] = $value['jobProductId'];
            $updatedProduct['jobProductAllocatedQty'] = $jobProdRespond;
            $updatedProducts[$value['jobProductId']] =  $updatedProduct;
        }
        $JobMateReq = $this->getModel('MaterialRequestTable')->getTaskByJobs($jobId);
        $jobMaterialRequest = [];
        foreach ($JobMateReq as $value) {
            $jobMaterialRequest[$value['jobProductId']][$value['materialRequestId']] = array(
                'jobId' => $value['jobId'],
                'jobProductId' => $value['jobProductId'],
                'materialRequestId' => $value['materialRequestId'],
                'materialRequestCode' => $value['materialRequestCode'],
                'materialRequestDate' => $value['materialRequestDate'],
                'materialRequestStatus' => $value['materialRequestStatus'],
                'requestedQuantity' => $value['requestedQuantity'],
            );
        }

        if ($errorFlag) {
            return $this->returnSuccess(array('updatedProducts' => $updatedProducts, 'jobMaterialRequest' => $jobMaterialRequest), 'SUCC_REQ_MATE_SAVE');
        } else {
            return $this->returnError('ERR_REQ_MATE_SAVE', null);
        }
    }


    public function getRequestedProductById($requestId)
    {
        $products = $this->getModel('MaterialRequestJobProductTable')->getProductDetailsByRequestId($requestId);
        $productList = [];

        foreach ($products as $pro) {
            $productList[] = [
                'jobProductId' => $pro['jobProductId'],
                'productCode' => $pro['productCode'],
                'productName' => $pro['productName'],
                'productID' => $pro['productID'],
                'requestedQuantity' => floatval($pro['requestedQuantity']),
                'locationProductQuantity' => floatval($pro['locationProductQuantity']),
                'jobProductIssuedQty' => floatval($pro['issuedQty']),
                'jobProductRequestedQty' => $pro['jobProductRequestedQty'],
                'materialRequestId' => $pro['materialRequestId'],
                'materialRequestJobProductId' => $pro['materialRequestJobProductId'],
                'locationProductDetails' => $this->getLocationProductDetails($this->user_session->userActiveLocation['locationID'], $pro['productID'])
            ];
        }
        return $this->returnSuccess($productList);
    }


    public function updateIssueQuantity($materials, $jobId, $reqID = null)
    {
        $dnProducts = [];
        $productTotal = 0;
        $requestedItemsPastIssueQty = [];

        // get data from material request job product table
        if (!is_null($reqID)) {
            $requestJobProduct = $this->getModel('MaterialRequestJobProductTable')->findByRequestId($reqID);
            foreach ($requestJobProduct as $rJPkey => $rJPvalue) {
                $requestedItemsPastIssueQty[$rJPvalue['materialRequestId']][$rJPvalue['jobProductId']]['qty'] = $rJPvalue['issuedQty']; 
                $requestedItemsPastIssueQty[$rJPvalue['materialRequestId']][$rJPvalue['jobProductId']]['id'] = $rJPvalue['materialRequestJobProductId']; 
                
            }
        }
        foreach ($materials as $pro) {

            if (!is_null($reqID)) {
                // set total issuedQty
                $qtyData = [
                    'issuedQty' => floatval($pro['issueQty']) + floatval($requestedItemsPastIssueQty[$reqID][$pro['jobProductId']]['qty']) 
                ];

                // update materialRequestProduct table
                $updateMaterialReqProTable = $this->getModel('MaterialRequestJobProductTable')->update($qtyData, $requestedItemsPastIssueQty[$reqID][$pro['jobProductId']]['id']);

                if (!$updateMaterialReqProTable) {
                    return $this->returnError('ERR_ITEM_ISSUE_UPDATE');
                }
            }
            
            // get currently issued qty and available qty by jobProductId
            $product = $this->getModel('JobProductTable')->getProductDetailsByJobProductId($pro['jobProductId']);
            if (!$product) {
                return $this->returnError('ERR_RETRIVE_PRO');
            }

            // add issued qty to currently issued qty. and update issued qty.
            $newIssueQty = $product['jobProductIssuedQty'] + $pro['issueQty'];
            $this->getModel('JobProductTable')->updateIssueQuantity($pro['jobProductId'], $newIssueQty);

            if ($product['productHandelingFixedAssets'] == 0) {

                $dnProducts[$product['locationProductID']] = [
                    'productID' => $product['productID'],
                    'productPrice' => $product['jobProductUnitPrice'],
                    'productDiscount' => 0.00,
                    'productDiscountType' => 'percentage',
                    'pTax' => [],
                    // No taxes use for jobs temporally
                    // 'pTax' => [
                    //     'tTa' => '',
                    //     'tL' => [
                    //         '' => [
                    //             'tN' => '',
                    //             'tP' => '',
                    //             'tA' => '',
                    //             'susTax' => '',
                    //         ]
                    //     ],
                    // ],
                    'productType' => $product['productTypeID'],
                    'copied' => '',
                    'deliverQuantity' => [
                        'qty' => $pro['issueQty']
                    ],
                ];
                $productTotal += $pro['issueQty'] * $product['jobProductUnitPrice'];

            }    
        }

        
        if (sizeof($dnProducts) > 0) {
            // generate a delivery note
            $res = $this->generateDeliveryNote($jobId, $dnProducts, $productTotal);    
        } else {
            $res['status'] = true;
        }


        if ($res['status']) {
            return $this->returnSuccess(null, 'SUCC_ITEM_ISSUE_UPDATE');
        } else {
            return $this->returnError('ERR_ITEM_ISSUE_UPDATE', ['error_detail' => $res['msg']]);
        }
    }


    protected function generateDeliveryNote($jobId, $products, $productTotal)
    {
        $locationId = $this->user_session->userActiveLocation['locationID'];
        $job = $this->getModel('JobTable')->getJobsByJobID($jobId);
        if (is_null($job->customerID)) {
            $customerID = 0;
        } else {
            $customerID = $job->customerID;
        }
        $date = $this->getGMTDateTime();

        $referenceService = $this->getService('ReferenceService');
        $refData = $referenceService->getReferenceNumber(6, $locationId)['data'];   // 6 - delivery note document id

        $dnService = $this->getService('DeliveryNoteService');
        $dnCode = $referenceService->checkAndGetNextRef($refData['referenceNo'], $locationId, $refData['locationReferenceID'], $dnService)['data'];

        $referenceService->updateReferenceNumber($refData['locationReferenceID']);

        $deliveryData = array(
            'deliveryNoteCode' => $dnCode['referenceCode'],
            'customerID' => $customerID,
            'deliveryNoteLocationID' => $locationId,
            'deliveryNoteDeliveryDate' => $date['data']['currentTime'],
            'deliveryNoteCharge' => null,
            'deliveryNotePriceTotal' => $productTotal,
            'deliveryNoteAddress' => $job->jobAddress,
            'deliveryNoteComment' => $job->jobReferenceNumber . ' - ' . $job->jobName,
            'salesOrderID' => null,
            'salesPersonID' => null,
            'customCurrencyId' => $this->user_session->companyCurrencyId,
            'deliveryNoteCustomCurrencyRate' => $this->user_session->displaySettings->currencyRate,
            'priceListId' => 0,
            'customerProfileID' => $job->customerProfileID,
        );

        $dnController = $this->getServiceLocator()->get('DeliveryNoteAPI');

        return $dnController->saveDeliverDetails($deliveryData, $products, [], $jobId);
    }
}
