<?php

namespace Jobs\Service;

use Core\Service\BaseService;
use Zend\I18n\Translator\Translator;
use Zend\View\Model\ViewModel;
use Jobs\Model\Department;
use Jobs\Model\DepartmentStation;
use DateTime;

class EmployeeManagementService extends BaseService
{
    /**
     * This function is used to get department related employees performance list
     * @param array $data
     * @return mixed
     */
    public function getDepartmentRelatedEmployeeListsDetails($data) 
    {
        $allEmpDetails = [];
        $departmentID = ($data['departmentID'] == "") ? null : $data['departmentID'];
        $createdTimeStamp = $this->getGMTDateTime('Y-m-d H:i:s')['data']['currentTime'];
        $gmtStartDate = split(' ', $createdTimeStamp)[0]; 
        $startDate = $gmtStartDate.' 00:00';
        $endDate = date('Y-m-d H:i:s', strtotime("+1 day", strtotime($startDate)));

        $toDate = ($data['toDate'] == "") ? null : $data['toDate'];
        $fromDate = ($data['fromDate'] == "") ? null : $data['fromDate'];
        $employeeDetails = [];

        if (!is_null($fromDate)) {
            $fromDate = $fromDate.' 00:00';
            $fromDate = $this->getGMTDateTime('Y-m-d H:i:s', $fromDate)['data']['currentTime'];
        } 
        if (!is_null($toDate)) {
            $toDate = $toDate.' 23:59';
            $toDate = $this->getGMTDateTime('Y-m-d H:i:s', $toDate)['data']['currentTime'];
        } 

        $toDate = (is_null($toDate)) ? $endDate: $toDate;
        $fromDate = (is_null($fromDate)) ? $startDate: $fromDate;

        $departmentEmployees = $this->getModel('EmployeeDepartmentTable')->getEmployeeDepartmentsByDepartmentId($departmentID);

        if (!$departmentEmployees) {
            return $this->returnError('ERR_DEPT_EMP_RETRIVE', 'DEPARTMENT_EMPLOYEE_RETRIVE_FAILD');
        }

        foreach ($departmentEmployees as $key => $value) {
            $employeeWorkTime = $this->calculateEmployeeWorkTime($departmentID, $value['employeeId'], $toDate, $fromDate);

            if (!$employeeWorkTime) {
                return $this->returnError('ERR_DEPT_EMP_WORK_TIME_CAL', 'DEPARTMENT_EMPLOYEE_WORK_TIME_CALCULATE_FAILD');    
            }

            $incentiveTotal = $this->calculateEmployeeIncentive($departmentID, $value['employeeId'], $toDate, $fromDate);

            if (is_null($incentiveTotal)) {
                return $this->returnError('ERR_DEPT_EMP_INCENTIVE_CAL', 'DEPARTMENT_EMPLOYEE_INCENTIVE_CALCULATE_FAILD');
            }

            $jobTasks = $this->getModel('jobTaskEmployeeTable')->getJobTasksByEmployeeID($departmentID, $value['employeeId'], $toDate, $fromDate);

            if (!$jobTasks) {
                return $this->returnError('ERR_DEPT_EMP_COMPLETE_TSK_CAL', 'DEPARTMENT_EMPLOYEE_COMPLETE_TASK_CALCULATE_FAILD');
            }


            $value['totalWorkHours'] =  $employeeWorkTime;           
            $value['completedTasks'] =  $jobTasks->count();   
            $value['totalIncentive'] = number_format($incentiveTotal, 2, '.', '');   

            $employeeDetails[] = $value;
        }

        $defaultEmpRecored = $this->getDefaultEmpPerformanceData($departmentID, $toDate, $fromDate);

        if (!$defaultEmpRecored) {
            return $this->returnError('ERR_DEPT_EMP_RETRIVE', 'DEFAULT_EMPLOYEE_DATA_RETRIVE_FAILD');
        }

        $allEmpDetails [] = $defaultEmpRecored;

        foreach ($employeeDetails as $key => $value) {
            $allEmpDetails [] = $value;
        }

        $status = true;
        $view = new ViewModel(array(
            'employees' => $allEmpDetails,
        ));

        $view->setTerminal(true);
        $view->setTemplate('jobs/employee-management/employee-performance-list');
        
        return [
            'view' => $view,
            'status' => $status
        ]; 
    }

    /**
     * This function is used to calculate employee work time 
     * @param int $departmentID
     * @param int $employeeId
     * @param date $toDate
     * @param date $fromDate
     * @return mixed
     */
    public function calculateEmployeeWorkTime($departmentID, $employeeId, $toDate, $fromDate)
    {

        $workTimeList = $this->getModel('jobTaskEmployeeTable')->getEmployeeWorkTimeListByEmployeeID($departmentID, $employeeId, $toDate, $fromDate);

        if (!$workTimeList) {
            return false;
        }

        $totalWorkingMins = 0;
        foreach ($workTimeList as $key => $value) {

            $startedAt = new DateTime($value['startedAt']);//start time
            $endedAt = new DateTime($value['endedAt']);//end time
            $interval = $startedAt->diff($endedAt);
            $workingHours = $interval->format('%h');
            $workingMinutes = $interval->format('%i');

            $converHoursTomMin = $workingHours * 60;

            $totalMin = $converHoursTomMin + $workingMinutes;

            $totalWorkingMins += $totalMin;
        }

        $totalWorkingHours = floor($totalWorkingMins / 60);
        $remainMins = $totalWorkingMins - ($totalWorkingHours * 60);

        $empTotalWorkinTime = $totalWorkingHours.' hrs  '.$remainMins.' min';

        return $empTotalWorkinTime;

    }

    /**
     * This function is used to calculate employee incentive
     * @param int $departmentID
     * @param int $employeeId
     * @param date $toDate
     * @param date $fromDate
     * @return mixed
     */
    public function calculateEmployeeIncentive($departmentID, $employeeId, $toDate, $fromDate)
    {

        $jobTaskEmployeeIncentives = $this->getModel('jobTaskEmployeeTable')->getEmployeeWorkTimeListByEmployeeID($departmentID, $employeeId, $toDate, $fromDate, $unique= true);

        if (!$jobTaskEmployeeIncentives) {
            return null;
        }

        $totalIncentive = 0;
        foreach ($jobTaskEmployeeIncentives as $key => $value) {

           if (!is_null($value['incentiveValue'])) {
                $totalIncentive += $value['incentiveValue'];
           }
        }
        return $totalIncentive;
    }

    /**
     * This function is used to get employee related services
     * @param array $data
     * @return mixed
     */
    public function getEmployeeJobTaskListsDetails($data)
    {
        $employeeId = ($data['employeeId'] == "") ? null: $data['employeeId'];
        $departmentID = ($data['departmentID'] == "") ? null : $data['departmentID'];

        $createdTimeStamp = $this->getGMTDateTime('Y-m-d H:i:s')['data']['currentTime'];
        $gmtStartDate = split(' ', $createdTimeStamp)[0]; 
        $startDate = $gmtStartDate.' 00:00';
        $endDate = date('Y-m-d H:i:s', strtotime("+1 day", strtotime($startDate)));

        $toDate = ($data['toDate'] == "") ? null : $data['toDate'];
        $fromDate = ($data['fromDate'] == "") ? null : $data['fromDate'];
        $employeeDetails = [];
        $processedEmpJobTaskDetails = [];
        
        if (!is_null($fromDate)) {
            $fromDate = $fromDate.'00:00';
            $fromDate = $this->getGMTDateTime('Y-m-d H:i:s', $fromDate)['data']['currentTime'];
        } 
        if (!is_null($toDate)) {
            $toDate = $toDate.'23:59';
            $toDate = $this->getGMTDateTime('Y-m-d H:i:s', $toDate)['data']['currentTime'];
        } 

        $toDate = (is_null($toDate)) ? $endDate: $toDate;
        $fromDate = (is_null($fromDate)) ? $startDate: $fromDate;
        $employeeJobTasks = $this->getModel('jobTaskEmployeeTable')->getEmployeeWorkTimeListByEmployeeID($departmentID, $employeeId, $toDate, $fromDate, $unique= true);

        if (!$employeeJobTasks) {
            return $this->returnError('ERR_DEPT_EMP_SRVCE_RETRIVE', 'DEFAULT_EMPLOYEE_RELATE_SRVCE_RETRIVE_FAILD');
        }

        foreach ($employeeJobTasks as $key => $value) {

            $startedAt = new DateTime($value['startedAt']);//start time
            $endedAt = new DateTime($value['endedAt']);//end time
            $interval = $startedAt->diff($endedAt);
            $workingHours = $interval->format('%h');
            $workingMinutes = $interval->format('%i');

            $converHoursTomMin = $workingHours * 60;

            $totalMin = $converHoursTomMin + $workingMinutes;

            $totalWorkingHours = floor($totalMin / 60);
            $remainMins = $totalMin - ($totalWorkingHours * 60);
            $durationTime = $totalWorkingHours.' hrs  '.$remainMins.' min';
            $value['incentiveValue'] = number_format($value['incentiveValue'], 2, '.', '');
            $value['duration'] = $durationTime;

            $value['startedAt'] = ($value['startedAt'] != NULL) ? $this->getUserDateTime($value['startedAt']) : NULL;
            $value['endedAt'] = ($value['endedAt'] != NULL) ? $this->getUserDateTime($value['endedAt']) : NULL;

            $employeeDetails [] = $value;
        }

        $result = array();
        foreach ($employeeDetails as $element) {
            $result[$element['jobTaskID']][] = $element;
        }

        foreach ($result as $key => $arrVal) {
            foreach ($arrVal as $i => $subArrVal) {

                if ($i == 0) {
                    $subArrVal['showIncentive'] = true;
                } else {
                    $subArrVal['showIncentive'] = false;
                }
                $processedEmpJobTaskDetails[] = $subArrVal;
            }
        }

        $status = true;
        $view = new ViewModel(array(
            'employeesjobTaskList' => $processedEmpJobTaskDetails,
        ));

        $view->setTerminal(true);
        $view->setTemplate('jobs/employee-management/employee-jobtask-list');
        
        return [
            'view' => $view,
            'status' => $status
        ]; 
    }

    /**
     * This function is used to udate employee incentives
     * @param array data
     * @return mixed
     */
    public function updateJobTaskEmployeeIncentiveValue($data)
    {
        $empId = $data['employeeId'];
        $jobTaskID = $data['jobTaskID'];
        $incentiveVal = $data['incentiveVal'];

        $result = $this->getModel('jobTaskEmployeeTable')->updateJobTaskEmployeeIncentive($empId, $jobTaskID, $incentiveVal);

        if ($result) {
            return $this->returnSuccess(null,'SUCC_EMP_INCENTIVE_UPDATE');
        } else {
            return $this->returnError('ERR_EMP_INCENTIVE_UPDATE',null);
        }

    }

     /**
     * This function is used to get default employee details
     * @param int $departmentId
     * @param date $toDate
     * @param date $fromDate
     * @return mixed
     */
    public function getDefaultEmpPerformanceData($departmentID, $toDate, $fromDate) {
        $employeeWorkTime = $this->calculateEmployeeWorkTime($departmentID, null, $toDate, $fromDate);

        if (!$employeeWorkTime) {
            return false;
        }

        $incentiveTotal = $this->calculateEmployeeIncentive($departmentID, null, $toDate, $fromDate);

        if (is_null($incentiveTotal)) {
            return false;
        }

        $jobTasks = $this->getModel('jobTaskEmployeeTable')->getJobTasksByEmployeeID($departmentID, null, $toDate, $fromDate);

        if (!$jobTasks) {
            return false;
        }

        $defaultEmpArray['employeeId'] = null;
        $defaultEmpArray['employeeCode'] = 'DEF_EMP';
        $defaultEmpArray['employeeFirstName'] = 'Default';
        $defaultEmpArray['employeeSecondName'] = 'Employee';
        $defaultEmpArray['totalWorkHours'] =  $employeeWorkTime;           
        $defaultEmpArray['completedTasks'] =  $jobTasks->count();   
        $defaultEmpArray['totalIncentive'] = number_format($incentiveTotal, 2, '.', '');   

        return $defaultEmpArray;
    }
}