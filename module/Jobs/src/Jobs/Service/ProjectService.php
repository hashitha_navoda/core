<?php

namespace Jobs\Service;

use Core\Service\BaseService;
use Zend\I18n\Translator\Translator;
use Zend\View\Model\ViewModel;
use Jobs\Model\Project;
use Jobs\Model\ProjectManager;
use Jobs\Model\ProjectSupervisor;
use Jobs\Model\ProjectMaterial;


class ProjectService extends BaseService {
	/**
	 * this function use to save business type. this is a one time process.
	 * @param array $data
	 * return array
	 **/
	public function initiateBusinessType($data) {
		// save business type
        if ($data['satge'] == 1) {
    		$displayData = [
    			'jobBusinessType' => $data['id'],
    		];
        } else {
            $displayData = [
                'jobWizardStatus' => 1,
            ];
        }
        $this->user_session->jobBusinessType = $data['id'];
		$saveBusinessType = $this->getModel('DisplaySetupTable')->updateDisplaySetupDetails($displayData, 1);

		return $saveBusinessType;
	}

	/**
	 * This function is used to search projects for dropdown
	 */
	public function searchProjectsForDropdown($data) {
		$searchKey = $data['searchKey'];
		$projects = $this->getModel('ProjectTable')->getProjectListBySearchKey($searchKey);
		$projectList = array();
		foreach ($projects as $pro) {
			$temp['value'] = $pro['projectId'];
			$temp['text'] = $pro['projectCode'] . '-' . $pro['projectName'];
			$projectList[] = $temp;
		}
		return $projectList;
	}


	/**
    * this function use to save project.
    * @param array $data
    * @param int $userId
    * @param int $locationID
    * return array
    **/
    public function createProject($data, $userId, $locationID)
    {
        $projectCode = $data['projectCode'];
        $projectName = $data['projectName'];
        $projectType = $data['projectType'];
        $customerID = $data['customerID'];
        $projectAddress = $data['projectAddress'];
        $NoOfJobs = $data['noOfJobs'];
        $dueDate = $this->convertDateToStandardFormat($data['dueDate']); 
        $projectValue = $data['projectValue'];
        $projectDescription = $data['projectDescription'];
        $projectEstimatedCost = $data['projectEstimatedCost'];
        $projectManagersList = $data['projectManagersList']; 
        $projectSupervisorsList = $data['projectSupervisorsList'];
        $estimatedEndDate = $this->convertDateToStandardFormat($data['estimatedEndDate']);

        $referenceService = $this->getService('ReferenceService');
        $refData = $referenceService->getReferenceNumber(21, $locationID);
        $rid = $refData['data']["referenceNo"];
        $locationReferenceID = $refData['data']["locationReferenceID"];

        while ($projectCode) {
            $result = $this->getModel('ProjectTable')->checkProjectCodeValid($projectCode);
            if ($result > 0) {
                if ($locationReferenceID) {
                    $newProjectCode = $referenceService->getReferencePrefixNumber($locationReferenceID);
                    if ($projectCode == $newProjectCode) {
                        $referenceService->updateReferenceNumber($locationReferenceID);
                        $projectCode = $referenceService->getReferencePrefixNumber($locationReferenceID);
                    } else {
                        $projectCode = $newProjectCode;
                    }
                } else {
                    return $this->returnError('ERR_JOB_CODE_EXIST', null);
                }
            } else {
                break;
            }
        }

        $entityService = $this->getService('EntityService');
        $entityID = $entityService->createEntity($userId)['data']['entityID'];

        $data = array(
            'projectCode' => $projectCode,
	        'projectName' => $projectName,
	        'projectType' => $projectType,
	        'customerID' => $customerID,
	        'projectAddress' => $projectAddress,
	        'noOfJobs' => $NoOfJobs,
	        'dueDate' => $dueDate, 
	        'projectValue' => $projectValue,
	        'projectDescription' => $projectDescription,
	        'projectEstimatedCost' => $projectEstimatedCost,
	        'entityID' => $entityID,
	        'locationId' => $locationID,
	        'estimatedEndDate' => $estimatedEndDate
        );

        $projectData = new Project();
        $projectData->exchangeArray($data);

        $savedResult = $this->getModel('ProjectTable')->saveProject($projectData);

        if (!$savedResult) {
            return $this->returnError('ERR_OCURED_WHILE_ADDED_PROJECT', 'PROJECT_SAVE_FAILD');
        } else {

        	$addRecordToProjectManager = $this->addRecordToProjectManager($projectManagersList, $savedResult);

	        if (!$addRecordToProjectManager) {
                return $this->returnError('ERR_OCURED_WHILE_ADDED_PROJECT', 'PROJECT_SAVE_FAILD');
            }

            $addRecordToProjectSupervisor = $this->addRecordToProjectSupervisor($projectSupervisorsList, $savedResult);

	        if (!$addRecordToProjectSupervisor) {
                return $this->returnError('ERR_OCURED_WHILE_ADDED_PROJECT', 'PROJECT_SAVE_FAILD');
            }
        	
	        if ($data['projectCode'] == $rid) {
	            $referenceService->updateReferenceNumber($locationReferenceID);
	        }
            
            return $this->returnSuccess($savedResult, 'SUCC_ADDED_PROJECT');
        }
    }

    /**
    * this function use to get project by projectId.
    * @param array $projectId
    * @param int $locationID
    * return array
    **/
    public function getProjectById($projectId, $locationId) 
    {
    	$result = $this->getModel('ProjectTable')->getProjectDetailsByProjectID($projectId);
    	if (!$result) {
    		return $this->returnError('ERR_PROJECT_DOSNOT_EXIST', 'PROJECT_RERIVE_FAILD');
    	}
    	else {

    		$projectManagers = $this->getModel('ProjectManagerTable')->getProjectManagersDetailsByProjectID($projectId);

    		$projectSupervisors = $this->getModel('ProjectSupervisorTable')->getProjectSupervisorssDetailsByProjectID($projectId);

    		$projectMaterials = $this->getModel('JobProductTable')->getJobMaterialsDetailsByProjectID($projectId);

    		
    		$projectManagersData=[];
    		$projectSupervisorsData=[];
    		$projectMaterialData=[];

            foreach ($projectManagers as $key => $value) {
                $projectManagersData[$value['employeeID']] = $value['employeeCode'].' - '.$value['employeeFirstName'].' '.$value['employeeSecondName'];
            }

            foreach ($projectSupervisors as $key => $value) {
                $projectSupervisorsData[$value['employeeID']] = $value['employeeCode'].' - '.$value['employeeFirstName'].' '.$value['employeeSecondName'];
            }

            $res = [];
            foreach ($projectMaterials as $key => $value) {

                $res[$value['productID']]['productID'] = $value['productID'];
    			$res[$value['productID']]['productName'] = $value['productName'];
                $res[$value['productID']]['productCode'] = $value['productCode'];
                $res[$value['productID']]['locationProductID'] = $value['locationProductID'];

                if ($value['jobProductAllocatedQty']) {
                    $res[$value['productID']]['jobProductAllocatedQty'] += $value['jobProductAllocatedQty'];
                } else {
                    $res[$value['productID']]['jobProductAllocatedQty'] = $value['jobProductAllocatedQty'];
                }      
    		}

            foreach ($res as $key => $value) {
                $projectMaterialData[] = $value;   
            }
            
    		$data = $result->current();
    		$data['projectManagersList'] = $projectManagersData;
    		$data['projectSupervisorsList'] = $projectSupervisorsData;
    		$data['projectMaterialList'] = $projectMaterialData;
    		
    		return $this->returnSuccess($data);
    	}

    }

    /**
    * this function use to update project.
    * @param array $data
    * @param int $userId
    * return array
    **/
    public function updateProject($data, $userId)
    {
    	$projectManagers = $data['projectManagersList'];
    	$projectSupervisors = $data['projectSupervisorsList'];
        $oldProjectID = $data['projectId'];
    	
        $checkCodeValid = $this->getModel('ProjectTable')->checkProjectCodeValid($data['projectCode'], $data['projectId']);

        $data['dueDate'] = $this->convertDateToStandardFormat($data['dueDate']);
        $data['estimatedEndDate'] = $this->convertDateToStandardFormat($data['estimatedEndDate']);

        if ($checkCodeValid > 0) {
            return $this->returnError('ERR_PROJECT_CODE_EXIST', 'PROJECT_EXIST');
        }

        $entityService = $this->getService('EntityService');
        $entityID = $entityService->updateEntity($data['entityID'], $userId);


        $getInactiveProjectManagers = $this->getModel('ProjectManagerTable')->getInactiveProjectManagers($data['projectId']);
        $getInactiveProjectSupervisors = $this->getModel('ProjectSupervisorTable')->getInactiveProjectSupervisors($data['projectId']);

        $projectData = new Project();
        $projectData->exchangeArray($data);

        $result = $this->getModel('ProjectTable')->replaceProject($projectData);
        $projectData->id = null;
        $projectData->entityID = $entityService->createEntity($userId)['data']['entityID'];
        $savedResult = $this->getModel('ProjectTable')->saveProject($projectData);

        if (!$savedResult) {
            return $this->returnError('ERR_OCURED_WHILE_UPDATE_PROJECT', 'PROJECT_FAILD_UPDATE');
        } else {

        	foreach ($getInactiveProjectManagers as $key => $value) {
	        	array_push($projectManagers, $value['employeeID']);
	        }
	        foreach ($getInactiveProjectSupervisors as $key => $value) {
	        	array_push($projectSupervisors, $value['employeeID']);
	        }

            $updateJobTable = $this->getModel('JobTable')->updateProjectIDInJobTable($oldProjectID, $savedResult);

            if (sizeof($data['deletedJobList']) > 0) {
                foreach ($data['deletedJobList'] as $key => $value) {
                    $tempData = [
                        'jobID' => $value,
                        'projectID' => $savedResult
                    ];

                    $respond = $this->getService('JobService')->deleteJob($tempData, $userID, true);
                    if (!$respond['status']) {
                        return $this->returnError($respond['msg'], $respond['data']);
                    }
                }
            }

	        $addRecordToProjectManager = $this->addRecordToProjectManager($projectManagers, $savedResult);

	        if (!$addRecordToProjectManager) {
                return $this->returnError('ERR_OCURED_WHILE_UPDATE_PROJECT', 'PROJECT_FAILD_UPDATE');
            }

            $addRecordToProjectSupervisor = $this->addRecordToProjectSupervisor($projectSupervisors, $savedResult);

	        if (!$addRecordToProjectSupervisor) {
                return $this->returnError('ERR_OCURED_WHILE_UPDATE_PROJECT', 'PROJECT_FAILD_UPDATE');
            }
		        
            return $this->returnSuccess('PROJECT_SUCESSFULLY_UPDATE', 'SUCC_UPDATE_PROJECT');
        }
    }

    /**
    * this function use to get project mangers that match for the projectID.
    * @param array $projectId
    * return array
    **/
    public function getProjectManagers($projectId) 
    {
    	$projectManagers = $this->getModel('ProjectManagerTable')->getProjectManagersDetailsByProjectID($projectId);
    	$data = [];

    	foreach ($projectManagers as $key => $value) {
            array_push($data, $value);
        }

        $view = new ViewModel(array(
            'projectManagers' => $data,
            'paginated' => false,
        ));

        $view->setTerminal(true);
        $view->setTemplate('jobs/project/project-managers-list.phtml');
        
        return [
            'view' => $view,
            'status' => true,
        ];
    }

    /**
    * this function use to search project by key, projectManager, Customer name and project type.
    * @param array $data
    * @param  int $param
    * @param array $statusList
    * return array
    **/
    public function projectSearch($data, $param, $statusList) 
    {

    	$paginated = false;
        $key = ($data['searchKey'] == '') ? null : $data['searchKey'];
        $projectMgtEmpID = ($data['projectMgtEmpID'] == '') ? null : $data['projectMgtEmpID'];
        $projectTypeID = ($data['projectTypeID'] == '') ? null : $data['projectTypeID'];
        $projectStatusID = ($data['projectStatusID'] == '') ? null : $data['projectStatusID'];

        if (!empty($key) || !is_null($projectMgtEmpID) || !is_null($projectTypeID) || !is_null($projectStatusID)) {
            $projectList = $this->getModel('ProjectTable')->projectSearch($key, $projectMgtEmpID, $projectTypeID, $projectStatusID);

            $status = true;
        } else {
            $status = true;
            $projectController = $this->getServiceLocator()->get("ProjectController");
            $projectList = $projectController->getPaginatedProjects(6, $param);
            $paginated = true;
        }
       
        $view = new ViewModel(array(
            'project_list' => $projectList,
            'paginated' => $paginated,
            'status' => $statusList
        ));

        $view->setTerminal(true);
        $view->setTemplate('jobs/project/project-list.phtml');
        
        return [
            'view' => $view,
            'status' => $status
        ];

    }

    /**
    * this function use to search project by key, projectManager, Customer name and project type.
    * @param array $data
    * @param  int $param
    * @param array $statusList
    * return array
    **/
    public function deleteProject($data, $userId)
    {
        $projectID = $data['projectId'];
        if (!empty($projectID)) {

            $getProject = $this->getModel('ProjectTable')->getProjectDetailsByProjectID($projectID);

           	$getProject = $getProject->current();

            $getRealtedJobs = $this->getModel('JobTable')->getJobByProjectIdForProjectDeletion($projectID);

            if ($getProject['projectStatus'] != 3) {
                return $this->returnError('ERR_CAN_NOT_DELETING_PROJECT',null);
            }
            if (count($getRealtedJobs) > 0) {
                return $this->returnError('ERR_CAN_NOT_DELETING_PROJECT_DUE_TO_JOB',null);
            }

            $entityService = $this->getService('EntityService');
            $updateEntity = $entityService->updateEntity($data['entityID'], $userId);

            $result = $this->getModel('ProjectTable')->deleteProject($projectID);

            if ($result) {
                return $this->returnSuccess(array($getProject['projectCode']),'SUCC_DELETED_PROJECT');
            } else {
                return $this->returnError('ERR_OCURED_WHILE_DELETING_PROJECT',$getProject['projectCode']);
            }

           
        } else {
            return $this->returnError('ERR_OCURED_WHILE_DELETING_PROJECT',$getProject['projectCode']);
        }
    }

    public function addRecordToProjectManager($projectManagers , $saveId) 
    {

    	foreach ($projectManagers as $key => $value) {
            $projectManagerID = null;
            $dataSet = [
                'projectManagerID' => $projectManagerID,
                'projectID' => $saveId,
                'employeeID' => $value
            ];

            $projectManagerData = new ProjectManager();
            $projectManagerData->exchangeArray($dataSet);

            $saveProjectManagers = $this->getModel('ProjectManagerTable')->saveProjectManager($projectManagerData);

            if (!$saveProjectManagers) {
                return false;
            }

        }

        return true;
    }

    public function addRecordToProjectSupervisor($projectSupervisors, $saveId) 
    {
    	foreach ($projectSupervisors as $key => $value) {
            $projectSupervisorID = null;
            $dataSet = [
                'projectSupervisorID' => $projectSupervisorID,
                'projectID' => $saveId,
                'employeeID' => $value
            ];

            $projectSupervisorData = new ProjectSupervisor();
            $projectSupervisorData->exchangeArray($dataSet);

            $saveProjectSupervisors = $this->getModel('ProjectSupervisorTable')->saveProjectSupervisor($projectSupervisorData);

            if (!$saveProjectSupervisors) {
                return false;
            }
        }
        return true;
    }

    public function addRecordToProjectMaterials($projectMaterials, $saveId) 
    {
    	foreach ($projectMaterials as $key => $value) {
            $projectMaterialID = null;
            $dataSet = [
                'projectMaterialID' => $projectMaterialID,
                'projectID' => $saveId,
                'productID' => $key,
                'locationProductID' => $value
            ];

            $projectMaterialData = new ProjectMaterial();
            $projectMaterialData->exchangeArray($dataSet);

            $saveProjectMaterials = $this->getModel('ProjectMaterialTable')->saveProjectMaterial($projectMaterialData);

            if (!$saveProjectMaterials) {
                return false;
            }

        }
        return true;
    }

    /**
     * This function is used to get project details
     */
    public function getProjectDetails($data) {
        $projectID = $data['projectID'];
        $project = $this->getModel('ProjectTable')->getProjectDetailsByProjectID($projectID)->current();

        if ($project) {
            return $this->returnSuccess($project, '');
        } else {
            return $this->returnError('', null);
        }
    }
}