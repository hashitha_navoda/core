<?php

namespace Jobs\Service;

use Core\Service\BaseService;
use Jobs\Model\JobType;
use Zend\View\Model\ViewModel;

class JobSetupService extends BaseService {
	/**
	 * This function is used save and update job type
	 * @param $postData $userID
	 */
	public function saveJobType($data, $userId) {
		$jobTypeCode = $data['jobTypeCode'];
		$jobTypeName = $data['jobTypeName'];
		$editMode = $data['editMode'];
		$tournamentFlag = $data['tournamentFlag'];
		//check wheter editMode is true or false. if it is true then update the job type otherwise save the job type
		if ($editMode == 'true') {
			$jobTypeID = $data['jobTypeID'];
			$oldData = $this->getModel('JobTypeTable')->getJobTypeByJobTypeID($jobTypeID)->current();
			$data = array(
				'jobTypeName' => $jobTypeName,
				'jobTypeID' => $jobTypeID,
			);

			$jobTypeData = new JobType();
			$jobTypeData->exchangeArray($data);
			$result = $this->getModel('JobTypeTable')->updateJobType($jobTypeData);
			if ($tournamentFlag) {
				if ($result) {
					$changeArray = $this->getEventTypeChageDetails($oldData, $data);
                    //set log details
                    $previousData = json_encode($changeArray['previousData']);
                    $newData = json_encode($changeArray['newData']);

                    $this->setLogDetailsArray($previousData,$newData);
                    $this->setLogMessage("Event type ".$oldData['jobTypeCode']." is updated.");
					return $this->returnSuccess(array('jobTypeID' => $jobTypeID), 'SUCC_EVENT_TYPE_UPDATE');
				} else {
					return $this->returnError(null, 'ERR_EVENT_TYPE_UPDATE');
				}
			} else {
				if ($result) {
					return $this->returnSuccess(array('jobTypeID' => $jobTypeID), 'SUCC_JBTYPE_UPDATE');
				} else {
					return $this->returnError(null, 'ERR_JBTYPE_UPDATE');
				}
			}
		} else {
			//check whether job type code alredy in database or not.
			$result = $this->getModel('JobTypeTable')->getJobTypeByJobTypeCode($jobTypeCode);
			if (!($result->count() > 0)) {
				$entityService = $this->getService('EntityService');
				$entityID = $entityService->createEntity($userId)['data']['entityID'];
				$data = array(
					'jobTypeCode' => $jobTypeCode,
					'jobTypeName' => $jobTypeName,
					'entityID' => $entityID,
				);

				$jobTypeData = new JobType();
				$jobTypeData->exchangeArray($data);
				$jobTypeID = $this->getModel('JobTypeTable')->saveJobType($jobTypeData);
				if ($tournamentFlag) {
					if ($jobTypeID) {
						$this->setLogMessage('Event type '.$jobTypeCode.' is created.');
						return $this->returnSuccess(array('jobTypeID' => $jobTypeID), 'SUCC_EVENT_TYPE_SAVE');
					} else {
						return $this->returnError('ERR_EVENT_TYPE_SAVE', null);
					}
				} else {
					if ($jobTypeID) {
						return $this->returnSuccess(array('jobTypeID' => $jobTypeID), 'SUCC_JBTYPE_SAVE');
					} else {
						return $this->returnError('ERR_JBTYPE_SAVE', null);
					}
				}
			} else {
				if ($tournamentFlag) {
					return $this->returnError('ERR_EVENT_TYPE_CODE_ALREDY_EXIXT', 'JTCERR');
				} else {
					return $this->returnError('ERR_JBTYPE_CODE_ALREDY_EXIXT', 'JTCERR');
				}
			}
		}
	}

	public function getEventTypeChageDetails($row, $data)
    {
        $previousData = [];
        $previousData['Event type code'] = $row['jobTypeCode'];
        $previousData['Event type name'] = $row['jobTypeName'];

        $newData = [];
        $newData['Event type code'] = $row['jobTypeCode'];
        $newData['Event type name'] = $data['jobTypeName'];
        return array('previousData' => $previousData, 'newData' => $newData);
    }

	/**
	 * This function is used to get job type by search key
	 */
	public function getJobTypesBySearchKey($data, $param) {
		$paginated = false;
		$jobTypeSearchKey = $data['jobTypeSearchKey'];

		if (!empty($jobTypeSearchKey)) {
			$tbl = $this->getModel('JobTypeTable');
			$jobTypeList = $tbl->getJobTypeforSearch($jobTypeSearchKey, $forSearchFlag = true);
		} else {
			$projectSetup = $this->getServiceLocator()->get("JobSetupController");
			$jobTypeList = $projectSetup->getPaginatedJobTypes(6, $param);
			$paginated = true;
		}

		$view = new ViewModel(array(
			'jobTypeList' => $jobTypeList,
			'paginated' => $paginated,
		));

		$view->setTerminal(true);
		$view->setTemplate('jobs/job-setup/list.phtml');

		return $view;
	}

	/**
	 * This function is used to delete job type
	 **/
	public function deleteJobTypeByJobTypeID($data, $userId) {
		$jobTypeID = $data['jobTypeID'];
		$jobTypeCode = $data['jobTypeCode'];
		$tournamentFlag = $data['tournamentFlag'];
		$jobDetails = $this->getModel('JobTable')->getJobDetailsByJobTypeID($jobTypeID)->current();
		if (!$jobDetails) {
			if (!empty($jobTypeID)) {
				$result = $this->getModel('JobTypeTable')->getJobTypeByJobTypeID($jobTypeID)->current();
				$entityID = $result['entityID'];
				$entityService = $this->getService('EntityService');
				$entityService->updateDeleteInfoEntity($entityID, $userId);

				if ($tournamentFlag) {
					$this->setLogMessage('Event type '.$result['jobTypeCode'].' is deleted.');
					return $this->returnSuccess(array($jobTypeCode), 'SUCC_EVENT_TYPE_DELETE');
				} else {
					return $this->returnSuccess(array($jobTypeCode), 'SUCC_JBTYPE_DELETE');
				}
			} else {
				if ($tournamentFlag) {
					return $this->returnError('ERR_EVENT_TYPE_DELETE', null);
				} else {
					return $this->returnError('ERR_JBTYPE_DELETE', null);
				}
			}
		} else {
			return $this->returnError('', 'deactive');
		}
	}

	/**
	 * This function is used to update job type status
	 **/
	public function updateJobTypeStatus($data) {
		$jobTypeID = $data['jobTypeID'];
		$status = $data['status'];
		$tournamentFlag = $data['tournamentFlag'];
		$oldData = $this->getModel('JobTypeTable')->getJobTypeByJobTypeID($jobTypeID)->current();
		$updateState = $this->getModel('JobTypeTable')->updateJobTypeState($jobTypeID, $status);

		if ($tournamentFlag) {
			if ($updateState) {
				if ($status == 0) {
                    $this->setLogMessage("Status of event type ".$oldData['jobTypeCode']." is deactived");
                } else {
                    $this->setLogMessage("Status of event type ".$oldData['jobTypeCode']." is actived");
                }
				return $this->returnSuccess(null, 'SUCC_EVENT_TYPE_STATUS_UPDATE');
			} else {
				return $this->returnError('ERR_EVENT_TYPE_STATUS_UPDATE', null);
			}
		} else {
			if ($updateState) {
				return $this->returnSuccess(null, 'SUCC_JOBTYPE_STATUS_UPDATE');
			} else {
				return $this->returnError('ERR_JOBTYPE_STATUS_UPDATE', null);
			}
		}
	}

}