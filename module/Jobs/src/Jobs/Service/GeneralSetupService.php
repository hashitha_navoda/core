<?php

namespace Jobs\Service;

use Core\Service\BaseService;
use Zend\I18n\Translator\Translator;
use Zend\View\Model\ViewModel;
use Jobs\Model\JobGeneralSettings;
use Jobs\Model\JobUserRole;

class GeneralSetupService extends BaseService
{
	/**
    * this function use to retrive all general settings
    * return mixed
    **/
    public function getAll()
    {
		$data = $this->getModel('JobGeneralSettingsTable')->fetchAll();
		foreach ($data as $key => $value) {
			$respond[$key] = $value === '1' ? true : false;
		}
		if ($data) {
			$adminData = [];
			$storeKeeperData = [];
			$JobUserRoleData = $this->getModel('JobUserRoleTable')->fetchAll();
			foreach ($JobUserRoleData as $key => $value) {
				if ($value['jobRoleID'] == 1) {
					$adminData[] = $value;
				} else {
					$storeKeeperData[] = $value;
				}
			}

			return $this->returnSuccess(array('jobGeneralSettings' => $respond, 'adminData' => $adminData, 'storeKeeperData' => $storeKeeperData), 'SUCC_JGS_LOAD');
		} else {
            return $this->returnError('ERR_JGS_LOAD', null);
		}
    }

	/**
    * this function use to save vehicle details
    * @param array $data
    * @param int $userId
    * return (int)/false
    **/
	public function saveSetting($data, $userID)
	{
		$gData = array(
			'maintainProject' => ($data['maintain_project'] == "true") ? 1 : 0,
			'rawMaterials' => ($data['raw_materials'] == "true") ? 1 : 0,
			'finishedGoods' => ($data['finished_goods'] == "true") ? 1 : 0,
			'fixedAssets' => ($data['fixed_assets'] == "true") ? 1 : 0,
			'vehicles' => ($data['vehicles'] == "true") ? 1 : 0,
			'contractorForJob' => ($data['assign_for_jobs'] == "true") ? 1 : 0,
			'contractorForTask' => ($data['assign_for_tasks'] == "true") ? 1 : 0,

		);
		$respond = $this->getModel('JobGeneralSettingsTable')->saveSetting($gData);
		if ($respond) {

			$existingJobUserRoles = $this->getModel('JobUserRoleTable')->fetchAll()->current();

			if ($existingJobUserRoles) {
				$result = $this->getModel('JobUserRoleTable')->deleteJobUserRoles();
				if ($result < 1) {
					return $this->returnError('ERR_JGS_ROLE_DELETE',null);					
				}
			}


			if (!is_null($data['jobAdmins'])) {
				foreach ($data['jobAdmins'] as $key => $value) {
					$adminData = array(
						'userID' => $value['adminID'],
						'jobRoleID' => $this->getJobUserRoleID('Admin')
					);		

					$jobAdminData = new JobUserRole();
					$jobAdminData->exchangeArray($adminData);
					$jobAdminID = $this->getModel('JobUserRoleTable')->saveJobUserRole($jobAdminData);

					if(!$jobAdminID) {
						return $this->returnError('ERR_JGS_JOB_ADMIN_SAVE',null);
					}				
				}
			}

			if (!is_null($data['jobStoreKeepers'])) {
				foreach ($data['jobStoreKeepers'] as $key => $value) {
					$storeKeeperData = array(
						'userID' => $value['storeKeeperID'],
						'jobRoleID' => $this->getJobUserRoleID('Store Keeper')
					);		

					$jobStoreKeeperData = new JobUserRole();
					$jobStoreKeeperData->exchangeArray($storeKeeperData);
					$jobStoreKeeperID = $this->getModel('JobUserRoleTable')->saveJobUserRole($jobStoreKeeperData);

					if(!$jobStoreKeeperID) {
						return $this->returnError('ERR_JGS_JOB_STKEEPER_SAVE',null);
					}				
				}
			}

			if ($data['generalSetupWizard'] == "true") {
				$projectService = $this->getService('ProjectService');
				$displaySetup = $projectService->initiateBusinessType(['id' => 1,'stage' => 2]);
			}

			$jobAdminFlag = false;
	        $jobStoreKeeperFlag = false;
	        $getJobUserRole = $this->getModel('JobUserRoleTable')->getJobUserRoleByUserID($userID);
	        foreach ($getJobUserRole as $key => $value) {
	            if ($value['jobRoleID'] == 1) {
	                $jobAdminFlag = true;
	            } 
	            if ($value['jobRoleID'] == 2) {
	                $jobStoreKeeperFlag = true;
	            }
	        }

			return $this->returnSuccess(array('jobAdminFlag' => $jobAdminFlag, 'jobStoreKeeperFlag' => $jobStoreKeeperFlag), 'SUCC_JGS_UPDATE');
		} else {
            return $this->returnError('ERR_JGS_UPDATE', null);
		}
	}
}