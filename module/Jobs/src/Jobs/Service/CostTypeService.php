<?php

namespace Jobs\Service;

use Core\Service\BaseService;
use Jobs\Model\CostType;
use Zend\View\Model\ViewModel;


class CostTypeService extends BaseService 
{
	
	/**
	* this function use to get cost types for drop down 
	* @param array $data
	* return array
	**/
	public function searchCostTypeForDropdown($data) 
	{
		$searchKey = $data['searchKey'];
		$costTypes = $this->getModel('CostTypeTable')->searchCostTypeForDropdown($searchKey);
		if ($costTypes) {
			$costTypeList = [];
			foreach ($costTypes as $costType) {
				$temp['value'] = $costType['costTypeID'];
				$temp['text'] = $costType['costTypeName'];
				$costTypeList[] = $temp;
			}
            return $this->returnSuccess(array('costTypeList' => $costTypeList), 'SUCC_LOAD_COST_TYPE');
		} else {
			return $this->returnError('ERR_LOAD_COST_TYPE', null);
		}
	}

	/**
	 * This service is used to add cost type
	 **/
	public function addCostType($data)
	{
        $costTypeName = $data['costTypeName'];
        $costTypeDescription = $data['costTypeDescription'];
        $productID = $data['productID'];
        $minimumValue = $data['minimumValue'];
        $maximumValue = $data['maximumValue'];
        $editMode = $data['editMode'];

		//check wheter editMode is true or false. if it is true then update the cost type otherwise save the cost type
        if ($editMode == 'true') {
            $costTypeID = $data['costTypeID'];
            $data = array(
                'costTypeDescription' => $costTypeDescription,
                'productID' => $productID,
                'costTypeMinimumValue' => $minimumValue,
                'costTypeMaximumValue' => $maximumValue,
                'costTypeID' => $costTypeID,
            );
            $costTypeData = new CostType();
            $costTypeData->exchangeArray($data);
            $result = $this->getModel('CostTypeTable')->updateCostType($costTypeData);
            if ($result) {
				return $this->returnSuccess(array('costTypeID' => $costTypeID, 'costTypeName' => $costTypeName), 'SUCC_COSTYPE_UPDATE');
            } else {
				return $this->returnError('ERR_COSTYPE_UPDATE', null);
            }
        } else {
			//check whether cost type Name alredy in database or not.
            $result = $this->getModel('CostTypeTable')->getCostTypeByCostTypeName($costTypeName);
            if (!($result->count() > 0)) {
                $entityService = $this->getService('EntityService');
                $entityID = $entityService->createEntity($userId)['data']['entityID'];
                $data = array(
                    'costTypeName' => $costTypeName,
                    'costTypeDescription' => $costTypeDescription,
                    'productID' => $productID,
                    'costTypeMinimumValue' => $minimumValue,
                    'costTypeMaximumValue' => $maximumValue,
                    'entityID' => $entityID
                );
                $costTypeData = new CostType();
                $costTypeData->exchangeArray($data);
                $costTypeID = $this->getModel('CostTypeTable')->saveCostType($costTypeData);
                if ($costTypeID) {
					return $this->returnSuccess(array('costTypeID' => $costTypeID, 'costTypeName' => $costTypeName), 'SUCC_COSTYPE_SAVE');
                } else {
					return $this->returnError('ERR_COSTYPE_SAVE', 'CTCERR');
                }
            } else {
				return $this->returnError('ERR_COSTYPE_NAME_ALREDY_EXIXT', null);
            }
        }
	}

	/**
	 * This service is used to search cost type
	 **/
	public function getCostTypesBySearchKey($data, $param)
	{
		$paginated = false;
        $costTypeSearchKey = $data['costTypeSearchKey'];
        if (!empty($costTypeSearchKey)) {
            $costTypeList = $this->getModel('CostTypeTable')->getCostTypeforSearch($costTypeSearchKey);
            $this->status = true;
        } else {
            $costType = $this->getServiceLocator()->get("CostTypeController");
            $costTypeList = $costType->getPaginatedCostTypes($param);
        }

        $view = new ViewModel(array(
            'costTypeList' => $costTypeList,
            'paginated' => $paginated
        ));

        $view->setTerminal(true);
        $view->setTemplate('jobs/cost-type/list.phtml');

		return $view;
	}

	/**
	 * This service is used to delete cost type
	 **/
	public function deleteCostType($data, $userId)
	{
        $costTypeID = $data['costTypeID'];
        $costTypeName = $data['costTypeName'];
        $activityData = $this->getModel('ActivityCostTypeTable')->getActivityByActivityCostID($costTypeID)->current();
        if (!$activityData) {
            if (!empty($costTypeID)) {
                $result = $this->getModel('CostTypeTable')->getCostTypeByCostTypeID($costTypeID)->current();
                $entityID = $result['entityID'];
				$entityService = $this->getService('EntityService');
				$result = $entityService->updateDeleteInfoEntity($entityID, $userId);
				if ($result) {
				    return $this->returnSuccess(array('costTypeName' => $costTypeName), 'SUCC_COSTYPE_DELETE');
				} else {
				    return $this->returnError('ERR_COSTYPE_DELETE',null);
				}
            } else {
				return $this->returnError('ERR_COSTYPE_DELETE', null);
            }
        } else {
			return $this->returnError(null, 'deactive');
        }
	}

	/**
	 * This service is used to change state of the cost type
	 **/
	public function changeStatusID($data)
	{
	    $costTypeID = $data['costTypeID'];
	    $status = $data['status'];
	    $updateState = $this->getModel('CostTypeTable')->updateCostTypeState($costTypeID, $status);
	    if ($updateState) {
		    return $this->returnSuccess(null, 'SUCC_CST_TYPE_STATUS_UPDATE');
        } else {
			return $this->returnError('ERR_CST_TYPE_STATUS_UPDATE', null);
        }
	}

}
