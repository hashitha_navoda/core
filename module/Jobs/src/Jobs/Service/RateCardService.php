<?php

namespace Jobs\Service;

use Core\Service\BaseService;
use Zend\I18n\Translator\Translator;
use Zend\View\Model\ViewModel;
use Jobs\Model\RateCard;
use Jobs\Model\RateCardTask;
use Jobs\Model\RateCardTaskSubTask;
use Settings\Model\UomTable;

class RateCardService extends BaseService
{
	/**
    * this function use to save rate card..
    * @param array $data
    * return array
    **/
    public function createRateCard($data)
    {

        $rateCardName = $data['rateCardName'];
        $rateCardCode = $data['rateCardCode'];
        $rateCardServiceRates = $data['mainServiceRates'];
        

        $result = $this->getModel('RateCardTable')->getRateCards($rateCardCode);

        if ($result->count() > 0) { 
            return $this->returnError('ERR_RTE_CRD_CODE_ALREDY_EXIST', 'RATE_CRD_EXIST');
        }

        $data = array(
            'rateCardCode' => $rateCardCode,
            'rateCardName' => $rateCardName,
            'rateCardStatus' => 1
        );

        $rateCardData = new RateCard();
        $rateCardData->exchangeArray($data);    
        $savedResult = $this->getModel('RateCardTable')->saveRateCard($rateCardData);

        if (!$savedResult) {
            return $this->returnError('ERR_RTE_CRD_CREATE', 'RATE_CARD_SAVE_FAILD');
        } else {

            foreach ($rateCardServiceRates as $key => $value) {

                $rateCrdTskData = array(
                    'rateCardID' => $savedResult,
                    'taskID' => $key,
                    'rateCardTaskRatePerUnit' => $value['rate'],
                    'uomID' => $value['uom']
                );
                
                $rateCardTaskData = new RateCardTask();
                $rateCardTaskData->exchangeArray($rateCrdTskData);  

                $savedRateCardTask = $this->getModel('RateCardTaskTable')->saveRateCardTask($rateCardTaskData);

                if (!$savedRateCardTask) {
                    return $this->returnError('ERR_RTE_CRD_TSK_CREATE', '');
                }


                if ($savedRateCardTask && $value['subTaskRates']) {

                    foreach ($value['subTaskRates'] as $key => $val) {
                        $subTaskData = array(
                            'rateCardTaskID' => $savedRateCardTask,
                            'subTaskID' => $key,
                            'rateCardTaskSubTaskRate' => $val['rate'],
                            'uomID' => $value['uom']
                        );

                        $rateCardTaskSubTaskData = new RateCardTaskSubTask();
                        $rateCardTaskSubTaskData->exchangeArray($subTaskData);  

                        $savedRateCardTaskSubTask = $this->getModel('RateCardTaskSubTaskTable')->saveRateCardTaskSubTask($rateCardTaskSubTaskData);

                        if (!$savedRateCardTaskSubTask) {
                            return $this->returnError('ERR_RTE_CRD_TSK_SUB_TSK_CREATE', '');
                        }
                    }
                }

            }
            
            return $this->returnSuccess('RATE_CARD_SAVE_SUCESSFULLY', 'SUCC_RTE_CRD_SAVE');
        }
    }

    /**
     * This function is used to get rate card related task(service) rates
     **/
    public function getRelatedServiceRates($data)
    {
        $rateCardID = ($data['rateCardID'] == "") ? null: $data['rateCardID'];
        $result = $this->getModel('RateCardTaskTable')->getRelatedServiceRates($rateCardID);

        $mainServiceRates = [];
        foreach ($result as $key => $value) {
            $subTaskRate = $this->getModel('RateCardTaskSubTaskTable')->getRelatedServiceSubtasksRates($value['rateCardTaskID']);
            $subTaskRates = [];
            foreach ($subTaskRate as $key => $val) {
                $subTaskRates[$val['subTaskId']] = [
                    'name' => $val['subTaskCode'],
                    'rate' => $val['rateCardTaskSubTaskRate']
                ];
            }

            $mainServiceRates[$value['taskID']] = [
                'rate' =>  $value['rateCardTaskRatePerUnit'],
                'uom' =>  $value['rateCardUomId'],
                'uomText' =>  $value['rateCardUomName'] . '(' . $value['rateCardUomAbbr'].')',
                'subTaskRates' =>  $subTaskRates,
                'nameCode' => $value['taskCode'].'-'.$value['taskName']
            ];
        }
            
        if ($result) {
            return $this->returnSuccess($mainServiceRates,null);
        } else {
            return $this->returnError('ERR_RETRIVE_DEP_RELATED_STAT',null);
        }
            
    } 

    /**
     * This function is used update rate card
     * @param $postData
     */
    public function updateRateCard($data) {

        $rateCardID = $data['rateCardID'];

        $deleteFlag = $this->getModel('RateCardTable')->updateDeleteRateCard($rateCardID);

        $getRateCardTasks = $this->getModel('RateCardTaskTable')->getRelatedServiceRates($rateCardID);

        foreach ($getRateCardTasks as $key => $value) {
            $deleteRateCardTaskSubTaskFlag = $this->getModel('RateCardTaskSubTaskTable')->updateDeleteRateCardTaskSubTask($value['rateCardTaskID']);
        }

        $deleteRateCardTaskFlag = $this->getModel('RateCardTaskTable')->updateDeleteRateCardTask($rateCardID);

        if ($deleteFlag && $deleteRateCardTaskFlag) {
            $updatedFlag = $this->createRateCard($data);
            if ($updatedFlag['status']) {
                return $this->returnSuccess($updatedFlag['data'], 'SUCC_RTE_CARD_UPDATE');
            } else {
                return $this->returnError('ERR_RTE_CARD_UPDATE', null);
            }
        } else {
            return $this->returnError('ERR_RTE_CARD_UPDATE', null);
        }
    }

    /**
    * this function use to update rate card state.
    * @param array $data
    * return array
    **/
    public function changeStatusID($data)
    {
        $rateCardID = $data['rateCardID'];
        $status = $data['status'];

        $result = $this->getModel('RateCardTable')->changeStatusID($rateCardID, $status);

        if ($result) {
            return $this->returnSuccess('Rate_CARD_STATE_SUCESSFULLY_UPDATE', 'SUCC_RTE_CRD_STATUS_UPDATE');
        } else {
            return $this->returnError('ERR_RTE_CRD_STATUS_UPDATE', 'RATE_CARD_STATE_FAILD_UPDATE');
        }

    }

    /**
     * This function is used to delete rate card
     **/
    public function deleteRateCard($data, $type = 'taskCard') {
        $rateCardID = $data['rateCardID'];

        if (!empty($rateCardID)) {
            $result = $this->getModel('RateCardTable')->updateDeleteRateCard($rateCardID);
            if ($result) {

                $getRateCardTasks = $this->getModel('RateCardTaskTable')->getRelatedServiceRates($rateCardID);

                foreach ($getRateCardTasks as $key => $value) {
                    $deleteRateCardTaskSubTaskFlag = $this->getModel('RateCardTaskSubTaskTable')->updateDeleteRateCardTaskSubTask($value['rateCardTaskID']);
                }

                $deleteRateCardTaskFlag = $this->getModel('RateCardTaskTable')->updateDeleteRateCardTask($rateCardID);

                return $this->returnSuccess(array($taskCode), 'SUCC_RTE_CRD_DELETE');
            } else {
                return $this->returnError('ERR_RTE_CRD_DELETE', null);
            }
        } else {
            return $this->returnError('ERR_RTE_CRD_DELETE', null); 
        }
    }

    /**
    * this function use to search rate card by key
    * @param array $data
    * @param int $param
    * return array
    **/
public function rateCardSearchByKey($data, $param)
    {
        $paginated = false;
        $key = ($data['searchKey'] == '') ? null : $data['searchKey'];

        if (!empty($key)) {
            $rateCardList = $this->getModel('RateCardTable')->rateCardSearchByKey($key);
            $status = true;
        } else {
            $status = true;
            $rateCardController = $this->getServiceLocator()->get("RateCardController");
            $rateCardList = $rateCardController->getPaginatedRateCards(6, $param);
            $paginated = true;
        }
       
        $view = new ViewModel(array(
            'rateCards' => $rateCardList,
            'paginated' => $paginated,
        ));

        $view->setTerminal(true);
        $view->setTemplate('jobs/rate-card/list.phtml');
        
        return [
            'view' => $view,
            'status' => $status
        ];

    }

    /**
     * This function is used to search rate card for dropdown
     */
    public function searchRateCardForDropdown($data) {
        $searchKey = $data['searchKey'];
        $taskCard = $this->getModel('RateCardTable')->rateCardSearchByKey($searchKey);
        $taskList = array();
        foreach ($taskCard as $dep) {
            $temp['value'] = $dep['rateCardID'];
            $temp['text'] = $dep['rateCardCode'].' - '.$dep['rateCardName'];;
            $taskList[] = $temp;
        }
        return $taskList;
    }
}