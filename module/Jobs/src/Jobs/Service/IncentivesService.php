<?php

namespace Jobs\Service;

use Core\Service\BaseService;
use Zend\View\Model\ViewModel;
use Jobs\Model\TaskCardTask;
use Jobs\Model\ServiceInceniveRate;
use Jobs\Model\RateCardTaskSubTask;
use Jobs\Model\JobCardIncentiveRate;
use Jobs\Model\JobCardServiceIncentiveRate;

class IncentivesService extends BaseService {

	/**
	 * This function is used to get service list by job card id
	 * @param JSON Data
	 */
	public function getServiceList($data)
	{
		$taskCardID = $data['taskCardID'];
		$serviceList = $this->getModel('TaskCardTaskTable')->getTaskList($taskCardID);

		foreach ($serviceList as $key => $value) {
			$respond[] = array(
				'taskCardTaskID' => $value['taskCardTaskID'],
				'taskID' => $value['taskID'],
				'taskText' => $value['taskCode'] . '-' . $value['taskName'],
				'taskCardTaskIncentiveValue' => $value['taskCardTaskIncentiveValue'],
				'taskCardTaskIncentiveType' => $value['taskCardTaskIncentiveType'],
			);
		}

		if($serviceList) {
			return $this->returnSuccess($respond, 'SUCC_RTRV_SRV_LIST');
		} else {
			return $this->returnError('ERR_RTRV_SRV_LIST', null);
		}
	}

	/**
	 * This function is used to get service list by vehicle type
	 * @param JSON Data
	 */
	public function getServiceListByVehicleType($data)
	{
		$vehicleTypeID = $data['vehicleTypeID'];
		$taskCardList = $this->getModel('ServiceVehicleTypeTable')->getTaskCardList($vehicleTypeID);
		$serviceIds = [];

		foreach ($taskCardList as $taskCard) {
			if (!in_array($taskCard['serviceID'], $serviceIds)) {
				array_push($serviceIds, $taskCard['serviceID']);
			}
		}
		if (empty($serviceIds)) {
			return $this->returnError('ERR_RTRV_SRV_LIST', null);
		}

		$serviceList = $this->getModel('TaskCardTaskTable')->getByTaskIds($serviceIds);
		if (!$serviceList) {
			return $this->returnError('ERR_RTRV_SRV_LIST', null);
		}

		foreach ($serviceList as $key => $value) {
				$respond[] = array(
				'jobCardCode' => $value['taskCardCode'],
				'taskCardTaskID' => $value['taskCardTaskID'],
				'taskID' => $value['taskID'],
				'taskName' => $value['taskName'],
				'taskCode' => $value['taskCode'],
				'taskCardTaskIncentiveValue' => $value['taskCardTaskIncentiveValue'],
				'taskCardTaskIncentiveType' => $value['taskCardTaskIncentiveType'],
			);
		}
		return $this->returnSuccess($respond, 'SUCC_RTRV_SRV_LIST');
	}

	/**
	 * This function is used to save Service Incentives
	 * @param JSON Data
	 */
	public function saveServiceIncentives($data)
	{
		$vehicleTypeID = $data['vehicleTypeID'];
		$incentiveData = array_filter($data['incentiveData']);


		if (is_null($vehicleTypeID) || $vehicleTypeID == '0') {
			return $this->returnError('ERR_DATA_SET', null);
		}

		foreach ($incentiveData as $key => $value) {
			$checkExistingRecord = $this->getModel('ServiceInceniveRateTable')->getIncentiveByServiceIdAndVehicleType($key, $vehicleTypeID);

			if ($checkExistingRecord) {
				$res = $this->getModel('ServiceInceniveRateTable')->updateIncentive($checkExistingRecord['serviceInceniveRateId']);
			}

			$incenData = [
				'serviceID' => $key,
				'vehicleTypeID' => $vehicleTypeID,
				'serviceRate' => $value['serviceRate'],
				'incentive' => $value['incentiveRate'],
				'incentiveType' => $value['incentiveFor'],
			];
			$saveData = new ServiceInceniveRate();
			$saveData->exchangeArray($incenData);
			$respond = $this->getModel('ServiceInceniveRateTable')->saveServiceIncentiveRate($saveData);
	
			if(!$respond) {
				return $this->returnError('ERR_SER_INCEN', null);
			}

			if ($value['subTaskRateEnable'] == 'true') {
				if (!is_null($value['subTaskRate'])) {
					$subTaskRates = array_filter($value['subTaskRate']);
					foreach ($subTaskRates as $subTaskID => $val) {
						$subTaskRateData = [
							'serviceInceniveRateId' => $respond,
							'subTaskID' => $subTaskID,
							'uomID' => 1,
							'rateCardTaskSubTaskRate' => $val,
						];
						$saveSubData = new RateCardTaskSubTask();
						$saveSubData->exchangeArray($subTaskRateData);
						$savedRateCardTaskSubTask = $this->getModel('RateCardTaskSubTaskTable')->saveRateCardTaskSubTask($saveSubData);
						if (!$savedRateCardTaskSubTask) {
                            return $this->returnError('ERR_RTE_CRD_TSK_SUB_TSK_CREATE', '');
                        }
					}
				}
			}

		}

		return $this->returnSuccess($vehicleTypeID, 'SUCC_SER_INCEN');
	}


	/**
	 * This function is used to save job Card Service incentives
	 * @param JSON Data
	 */
	public function saveJobCardServiceIncentives($data)
	{
		$vehicleTypeID = $data['vehicleTypeID'];
		$incentiveData = array_filter($data['incentiveData']);


		if (is_null($vehicleTypeID) || $vehicleTypeID == '0') {
			return $this->returnError('ERR_DATA_SET', null);
		}

		foreach ($incentiveData as $key => $value) {
			$checkExistingRecord = $this->getModel('JobCardIncentiveRateTable')->getJobCardIncentiveByJobCardIdAndVehicleType($key, $vehicleTypeID);

			if ($checkExistingRecord) {
				$res = $this->getModel('JobCardIncentiveRateTable')->updateJobCardIncentive($checkExistingRecord['jobCardIncentiveRateID']);
			}

			$incenData = [
				'jobCardID' => $key,
				'vehicleTypeID' => $vehicleTypeID,
				'serviceRate' => $value['serviceRate'],
			];
			$saveData = new JobCardIncentiveRate();
			$saveData->exchangeArray($incenData);
			$respond = $this->getModel('JobCardIncentiveRateTable')->saveJobCardIncentiveRate($saveData);
	
			if(!$respond) {
				return $this->returnError('ERR_SER_INCEN', null);
			}


			if ($checkExistingRecord) {
				$res = $this->getModel('JobCardServiceIncentiveRateTable')->updateJobCardServiceIncentive($checkExistingRecord['jobCardIncentiveRateID']);
			}

			if ($value['serviceIncentiveData']) {
				
				foreach ($value['serviceIncentiveData'] as $serviceID => $val) {
					$serviceIncentiveData = [
						'jobCardIncentiveRateID' => $respond,
						'serviceID' => $serviceID,
						'incentive' => $val,
					];
					$saveSerData = new JobCardServiceIncentiveRate();
					$saveSerData->exchangeArray($serviceIncentiveData);
					$saveJobCardServiceRate = $this->getModel('JobCardServiceIncentiveRateTable')->saveJobCardServiceIncentiveRate($saveSerData);
					if (!$saveJobCardServiceRate) {
                        return $this->returnError('ERR_RTE_CRD_TSK_SUB_TSK_CREATE', '');
                    }
				}
				
			}

		}

		return $this->returnSuccess($vehicleTypeID, 'SUCC_SER_INCEN');
	}

	public function getServiceForVehicleType($data)
	{
		$vehicleTypeID = $data['vehicleTypeID'];
		$serviceList = $this->getModel('ServiceVehicleTypeTable')->getServiceList($vehicleTypeID);
		$serviceIncentiveRateData = [];
		foreach ($serviceList as $value) {
			if ($value['deleteStatus'] != 1) {
				$temp['serviceCode'] = $value['taskCode'];
				$temp['taskName'] = $value['taskName'];
				$temp['serviceRate'] = $value['serviceRate'];
				$temp['incentive'] = $value['incentive'];
				$temp['incentiveType'] = $value['incentiveType'];
				$temp['serviceID'] = $value['serviceID'];
				$temp['vehicleTypeID'] = $value['vehicleTypeID'];
				$temp['subTasks'] = $this->getSubTasks($value['serviceID']);
				$temp['subTaskRates'] = $this->getSubTaskRate($value['serviceInceniveRateId']);
				$serviceIncentiveRateData[]	 = $temp;		
			}
		}
		$view = new ViewModel(array(
            'serviceIncentiveRateData' => $serviceIncentiveRateData,
        ));
        $view->setTerminal(true);
        $view->setTemplate('/jobs/incentives/service-list');

        return ['data' => $serviceIncentiveRateData, 'view' => $view];
	}

	public function getJobCardForVehicleType($data)
	{
		$vehicleTypeID = $data['vehicleTypeID'];
		$jobCardList = $this->getModel('JobCardVehicleTypeTable')->getJobCardForVehicleTypeList($vehicleTypeID);
		$jobCardIncentiveRateData = [];
		foreach ($jobCardList as $value) {
			$jobCardServicesRates = [];
			if ($value['deleteStatus'] != 1) {
				$temp['jobCardCode'] = $value['taskCardCode'];
				$temp['jobCardName'] = $value['taskCardName'];
				$temp['serviceRate'] = $value['serviceRate'];
				$temp['incentive'] = $value['incentive'];
				$temp['incentiveType'] = $value['incentiveType'];
				$temp['jobCardID'] = $value['jobCardID'];
				$temp['vehicleTypeID'] = $value['vehicleTypeID'];
				// $temp['subTasks'] = $this->getSubTasks($value['serviceID']);
				// $temp['subTaskRates'] = $this->getSubTaskRate($value['serviceInceniveRateId']);

				$jobCardServiceList = $this->getModel('taskCardTaskTable')->getTaskCardServiceIncentivesByTaskCardID($value['jobCardID'], $value['jobCardIncentiveRateID']);

				foreach ($jobCardServiceList as $key => $val) {
					if ($val['deleted'] == 0) {
						$tem['serviceCode'] = $val['taskCode'];
						$tem['serviceName'] = $val['taskName'];
						$tem['serviceId'] = $val['taskID'];
						$tem['incentive'] = $val['incentive'];
						$tem['incentiveType'] = $val['incentiveType'];
						// $temp['subTasks'] = $this->getSubTasks($value['serviceID']);
						// $temp['subTaskRates'] = $this->getSubTaskRate($value['serviceInceniveRateId']);
						$jobCardServicesRates[]	 = $tem;
					}
				}

				$temp['services'] = $jobCardServicesRates;
				$jobCardIncentiveRateData[]	 = $temp;		
			}

		}
		$view = new ViewModel(array(
            'jobCardIncentiveRateData' => $jobCardIncentiveRateData,
        ));
        $view->setTerminal(true);
        $view->setTemplate('/jobs/incentives/job-card-list');

        return ['data' => $jobCardIncentiveRateData, 'view' => $view];
	}

	public function getSubTasks($serviceID)
	{
		$tasks = $this->getModel('ServiceSubTaskTable')->getRelatedSubTasks($serviceID);
        
        $subTasks = [];
        foreach ($tasks as $key => $value) {
            $temp['subTaskName'] = $value['subTaskName'];
            $temp['subTaskCode'] = $value['subTaskCode'];
            $temp['subTaskId'] = $value['subTaskId'];

            $subTasks[] = $temp;
        }

        return $subTasks;
	}

	public function getSubTaskRate($serviceInceniveRateId)
	{
		$subTaskRate = $this->getModel('RateCardTaskSubTaskTable')->getRelatedServiceSubtasksRates($serviceInceniveRateId);
        $subTaskRates = [];
        foreach ($subTaskRate as $key => $val) {
            $subTaskRates[$val['subTaskId']] = [
                'name' => $val['subTaskCode'],
                'rate' => $val['rateCardTaskSubTaskRate']
            ];
        }
		
		return $subTaskRates;    
	}
}
