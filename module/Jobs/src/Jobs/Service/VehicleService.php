<?php

namespace Jobs\Service;

use Core\Service\BaseService;
use Zend\I18n\Translator\Translator;
use Zend\View\Model\ViewModel;
use Jobs\Model\Vehicle;

class VehicleService extends BaseService
{
	/**
    * this function use to search vehicles by name, code
    * @param array $data
    * @param int $param
    * @param int $companyCurrencySymbol
    * return array
    **/
    public function searchVehicle($data, $param, $companyCurrencySymbol)
    {
        $paginated = false;
        $key = ($data['searchKey'] == '') ? null : $data['searchKey'];

        if (!empty($key)) {
            $vehiclesList = $this->getModel('VehicleTable')->searchActiveVehicles($key);
            $status = true;
        } else {
            $status = true;
            $vehiclesController = $this->getServiceLocator()->get("VehicleController");
            $vehiclesList = $vehiclesController->getPaginatedVehicles(6, $param);
            $paginated = true;
        }
       
        $view = new ViewModel(array(
            'companyCurrencySymbol' => $companyCurrencySymbol,
            'vehiclesList' => $vehiclesList,
            'paginated' => $paginated,
        ));

        $view->setTerminal(true);
        $view->setTemplate('jobs/vehicle/vehicles-list.phtml');
        
        return [
            'view' => $view,
            'status' => $status
        ];

    }

	/**
    * this function use to save vehicle details
    * @param array $data
    * @param int $userId
    * return (int)/false
    **/
	public function saveVehicle($data, $userId)
	{
		$registerNumberAvailbility = $this->getModel('VehicleTable')->checkRegisterNumber($data->vehicleRegistrationNumber);

		$vehicleCodeAvailbility = $this->getModel('VehicleTable')->checkVehicleCode($data->vehicleCode);

		if (!$registerNumberAvailbility) {
            return $this->returnError('ERR_VEHICLES_UNAVAI_REGNO', null);
		}

		if (!$vehicleCodeAvailbility) {
            return $this->returnError('ERR_VEHICLES_UNAVAI_VEHICLE_CODE', null);
		}

		$entityId = $this->getService('EntityService')->createEntity($userId)['data']['entityID'];
		$vehicleData = array(
		    'vehicleName' => $data->vehicleName,
		    'vehicleCode' => $data->vehicleCode,
		    'vehicleRegistrationNumber' => $data->vehicleRegistrationNumber,
		    'vehicleCost' => $data->vehicleCost,
		    'entityId' => $entityId,
		);
		$vehicle = new Vehicle();
		$vehicle->exchangeArray($vehicleData);
		$savedVehicleData = $this->getModel('VehicleTable')->saveVehicle($vehicle);
		if ($savedVehicleData) {
			return $this->returnSuccess(array('vehicleId' => $savedVehicleData), 'SUCC_VEHICLES_SAVE');
		} else {
            return $this->returnError('ERR_VEHICLES_SAVE', null);
		}
	}

	/**
    * this function use to edit vehicle details
    * @param array $data
    * @param int $userId
    * return (string)/true
    **/
	public function editVehicle($data, $params, $userId)
	{
		$data['vehicleId'] = $params['param1'];
		$data['entityId'] = $params['param2'];

		$registerNumberAvailbility = $this->getModel('VehicleTable')->checkRegisterNumber($data->vehicleRegistrationNumber, $data->vehicleId, true);

		if (!$registerNumberAvailbility) {
            return $this->returnError('ERR_VEHICLES_UNAVAI_REGNO', null);
		}

		$vehicleData = array(
			'vehicleId' => $data->vehicleId,
		    'vehicleName' => $data->vehicleName,
		    'vehicleCode' => $data->vehicleCode,
		    'vehicleRegistrationNumber' => $data->vehicleRegistrationNumber,
		    'vehicleCost' => $data->vehicleCost,
		);
		$savedVehicleData = $this->getModel('VehicleTable')->editVehicle($vehicleData);
		if ($savedVehicleData) {
			return $this->returnSuccess(array('vehicleData' => $vehicleData), 'SUCC_VEHICLES_EDIT');
		} else {
            return $this->returnError('ERR_VEHICLES_EDIT', null);
		}
	}

	/**
    * this function use to delete vehicle details
    * @param array $data
    * @param int $userId
    * return array
    **/
	public function deleteVehicle($data, $userId)
	{
		$vehicleDependability = $this->getModel('VehicleTable')->vehicleDependability($data->vehicleID);
		if ($vehicleDependability) {
			$entityID = $this->getService('EntityService')->updateDeleteInfoEntity($data->entityId, $userId);
			return $this->returnSuccess(array('entityID' => $entityID), 'SUCC_VEHICLES_DELETE');
		} else {
			return $this->returnError('ERR_VEHICLES_DELETE', null);
		}
	}

	/**
    * this function use to change status of vehicle details
    * @param array $data
    * @param int $userId
    * return mixed
    **/
	public function changeStatusVehicle($data, $userId)
	{
		if ($data['vehicleStatus'] == 1) {
		    $updatedVicleStatus = 2;
		} else if ($data['vehicleStatus'] == 2) {
		    $updatedVicleStatus = 1;
		}
		$flagStatusChange = $this->getModel('VehicleTable')->changeVehicleStatus($data['vehicleID'], $updatedVicleStatus);

		if ($flagStatusChange) {
			$flagEntityUpdate = $this->getService('EntityService')->updateEntity($data->entityId, $userId);
			return $this->returnSuccess(array('vehicleStatus' => $updatedVicleStatus), 'SUCC_VEHICLES_STATUS_CHANGE');
		} else {
			return $this->returnError('ERR_VEHICLES_STATUS_CHANGE', null);
		}
	}

}