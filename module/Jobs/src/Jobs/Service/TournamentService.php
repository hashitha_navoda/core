<?php

namespace Jobs\Service;

use Core\Service\BaseService;
use Zend\I18n\Translator\Translator;
use Zend\View\Model\ViewModel;
use Jobs\Model\Project;
use Jobs\Model\ProjectManager;
use Jobs\Model\ProjectSupervisor;
use Jobs\Model\ProjectMaterial;


class TournamentService extends BaseService {
	/**
	 * this function use to save business type. this is a one time process.
	 * @param array $data
	 * return array
	 **/
	public function initiateBusinessType($data) {
		// save business type
        if ($data['satge'] == 1) {
    		$displayData = [
    			'jobBusinessType' => $data['id'],
    		];
        } else {
            $displayData = [
                'jobWizardStatus' => 1,
            ];
        }
        $this->user_session->jobBusinessType = $data['id'];
		$saveBusinessType = $this->getModel('DisplaySetupTable')->updateDisplaySetupDetails($displayData, 1);

		return $saveBusinessType;
	}

	/**
	 * This function is used to search projects for dropdown
	 */
	public function searchProjectsForDropdown($data) {
		$searchKey = $data['searchKey'];
		$projects = $this->getModel('ProjectTable')->getProjectListBySearchKey($searchKey);
		$projectList = array();
		foreach ($projects as $pro) {
			$temp['value'] = $pro['projectId'];
			$temp['text'] = $pro['projectCode'] . '-' . $pro['projectName'];
			$projectList[] = $temp;
		}
		return $projectList;
	}


	/**
    * this function use to save project.
    * @param array $data
    * @param int $userId
    * @param int $locationID
    * return array
    **/
    public function createProject($data, $userId, $locationID)
    {
        $projectCode = $data['projectCode'];
        $projectName = $data['projectName'];
        $projectType = $data['projectType'];
        $projectAddress = $data['projectAddress'];
        $NoOfJobs = $data['noOfJobs'];
        $startDate = $this->convertDateToStandardFormat($data['startDate']); 
        $eventEstimatedCost = $data['eventEstimatedCost'];
        $projectDescription = $data['projectDescription'];
        $tournamentEstimatedCost = $data['tournamentEstimatedCost'];
        $teamID = $data['teamID']; 
        $tournamentOfficials = $data['tournamentOfficials'];
        $endDate = $this->convertDateToStandardFormat($data['endDate']);
        $referenceService = $this->getService('ReferenceService');
        $refData = $referenceService->getReferenceNumber(40, $locationID);
        $rid = $refData['data']["referenceNo"];
        $locationReferenceID = $refData['data']["locationReferenceID"];
        
        $result = $this->getModel('ProjectTable')->checkProjectCodeValid($projectCode);
        if ($result > 0) {
            return $this->returnError('ERR_TOURNAMENT_CODE_DUPLICATE', null);
        } 

        $entityService = $this->getService('EntityService');
        $entityID = $entityService->createEntity($userId)['data']['entityID'];

        $data = array(
            'projectCode' => $projectCode,
	        'projectName' => $projectName,
	        'projectType' => $projectType,
	        'projectAddress' => $projectAddress,
	        'noOfJobs' => $NoOfJobs,
	        'projectStartingAt' => $startDate, 
	        'projectEstimatedCost' => $tournamentEstimatedCost,
	        'eventEstimatedCost' => $eventEstimatedCost,
	        'projectDescription' => $projectDescription,
	        'entityID' => $entityID,
	        'locationId' => $locationID,
	        'estimatedEndDate' => $endDate,
            'teamID' => $teamID
        );

        $projectData = new Project();
        $projectData->exchangeArray($data);
        $savedResult = $this->getModel('ProjectTable')->saveProject($projectData);

        if (!$savedResult) {
            return $this->returnError('ERR_OCURED_WHILE_ADDED_TOURNAMNET', 'TOURNAMNET_SAVE_FAILD');
        } else {

        	$addTournamentOfficials = $this->addTournamentOfficials($tournamentOfficials, $savedResult);

	        if (!$addTournamentOfficials) {
                return $this->returnError('ERR_OCURED_WHILE_ADDED_TOURNAMNET', 'TOURNAMENT_OFFICIAL_SAVE_FAILD');
            }
        	
	        if ($data['projectCode'] == $rid) {
	            $referenceService->updateReferenceNumber($locationReferenceID);
	        }
            $this->setLogMessage('Tournament '.$projectCode.' is created.');
            return $this->returnSuccess($savedResult, 'SUCC_ADDED_TOURNAMENT');
        }
    }

    /**
    * this function use to get project by projectId.
    * @param array $projectId
    * @param int $locationID
    * return array
    **/
    public function getProjectById($projectId, $locationId) 
    {
    	$result = $this->getModel('ProjectTable')->getProjectDetailsByProjectID($projectId);
    	if (!$result) {
    		return $this->returnError('ERR_PROJECT_DOSNOT_EXIST', 'PROJECT_RERIVE_FAILD');
    	}
    	else {

    		$projectManagers = $this->getModel('ProjectManagerTable')->getTournamentOfficialsByProjectID($projectId);

    		$projectSupervisors = $this->getModel('ProjectSupervisorTable')->getProjectSupervisorssDetailsByProjectID($projectId);

    		$projectMaterials = $this->getModel('JobProductTable')->getJobMaterialsDetailsByProjectID($projectId);

    		$projectManagersData=[];
    		$projectSupervisorsData=[];
    		$projectMaterialData=[];

            foreach ($projectManagers as $key => $value) {
                if (is_null($value['teamID'])) {
                    $projectManagersData[$value['participantTypeID'].'-'.$value['participantSubTypeID'].'-'.$value['employeeID']] = ['participantTypeID' => $value['participantTypeID'], 'participantSubTypeID' => $value['participantSubTypeID'], 'participantID' => $value['employeeID']];
                } else {
                    $projectManagersData[$value['participantTypeID'].'-'.$value['participantSubTypeID'].'-'.$value['employeeID'].'-'.$value['teamID']] = ['participantTypeID' => $value['participantTypeID'], 'participantSubTypeID' => $value['participantSubTypeID'], 'participantID' => $value['employeeID'], 'teamID' => $value['teamID']];
                }
            }

            foreach ($projectSupervisors as $key => $value) {
                $projectSupervisorsData[$value['employeeID']] = $value['employeeCode'].' - '.$value['employeeFirstName'].' '.$value['employeeSecondName'];
            }

            $res = [];
            foreach ($projectMaterials as $key => $value) {

                $res[$value['productID']]['productID'] = $value['productID'];
    			$res[$value['productID']]['productName'] = $value['productName'];
                $res[$value['productID']]['productCode'] = $value['productCode'];
                $res[$value['productID']]['locationProductID'] = $value['locationProductID'];

                if ($value['jobProductAllocatedQty']) {
                    $res[$value['productID']]['jobProductAllocatedQty'] += $value['jobProductAllocatedQty'];
                } else {
                    $res[$value['productID']]['jobProductAllocatedQty'] = $value['jobProductAllocatedQty'];
                }      
    		}

            foreach ($res as $key => $value) {
                $projectMaterialData[] = $value;   
            }
            
    		$data = $result->current();
    		$data['projectManagersList'] = $projectManagersData;
    		$data['projectSupervisorsList'] = $projectSupervisorsData;
    		$data['projectMaterialList'] = $projectMaterialData;
    		
    		return $this->returnSuccess($data);
    	}

    }

    /**
    * this function use to update project.
    * @param array $data
    * @param int $userId
    * return array
    **/
    public function updateProject($data, $userId)
    {
    	$tournamentOfficials = $data['tournamentOfficials'];
        $oldProjectID = $data['projectId'];
    	
        $checkCodeValid = $this->getModel('ProjectTable')->checkProjectCodeValid($data['projectCode'], $data['projectId']);

        $data['projectStartingAt'] = $this->convertDateToStandardFormat($data['startDate']);
        $data['estimatedEndDate'] = $this->convertDateToStandardFormat($data['endDate']);
        $data['projectEstimatedCost'] = $data['tournamentEstimatedCost'];
        if ($checkCodeValid > 0) {
            return $this->returnError('ERR_TOURNAMENT_CODE_EXIST', 'PROJECT_EXIST');
        }

        $entityService = $this->getService('EntityService');
        $entityID = $entityService->updateEntity($data['entityID'], $userId);


        $getInactiveProjectManagers = $this->getModel('ProjectManagerTable')->getInactiveProjectManagers($data['projectId']);

        $oldData = $this->getModel('ProjectTable')->getProjectByProjectId($data['projectId']);
        $projectData = new Project();
        $projectData->exchangeArray($data);

        $result = $this->getModel('ProjectTable')->replaceProject($projectData);
        $projectData->id = null;
        $projectData->entityID = $entityService->createEntity($userId)['data']['entityID'];
        $savedResult = $this->getModel('ProjectTable')->saveProject($projectData);

        if (!$savedResult) {
            return $this->returnError('ERR_OCURED_WHILE_UPDATE_TOURNAMENT', 'PROJECT_FAILD_UPDATE');
        } else {


            // $updateJobTable = $this->getModel('JobTable')->updateProjectIDInJobTable($oldProjectID, $savedResult);

            // if (sizeof($data['deletedJobList']) > 0) {
            //     foreach ($data['deletedJobList'] as $key => $value) {
            //         $tempData = [
            //             'jobID' => $value,
            //             'projectID' => $savedResult
            //         ];

            //         $respond = $this->getService('JobService')->deleteJob($tempData, $userID, true);
            //         if (!$respond['status']) {
            //             return $this->returnError($respond['msg'], $respond['data']);
            //         }
            //     }
            // }

	        $addTournamentOfficials = $this->addTournamentOfficials($tournamentOfficials, $savedResult);

	        if (!$addTournamentOfficials) {
                return $this->returnError('ERR_OCURED_WHILE_UPDATE_TOURNAMENT', 'PROJECT_FAILD_UPDATE');
            }
            $changeArray = $this->getTournamentChageDetails($oldData, $data);
            //set log details
            $previousData = json_encode($changeArray['previousData']);
            $newData = json_encode($changeArray['newData']);

            $this->setLogDetailsArray($previousData,$newData);
            $this->setLogMessage("Tournament ".$oldData->projectCode." is updated.");
            return $this->returnSuccess('PROJECT_SUCESSFULLY_UPDATE', 'SUCC_UPDATE_TOURNAMENT');
        }
    }

    public function getTournamentChageDetails($row, $data)
    {
        $previousData = [];
        $previousData['Tournament code'] = $row->projectCode;
        $previousData['Tournament name'] = $row->projectName;
        $previousData['Tournament address'] = $row->projectAddress;
        $previousData['Tournament start date'] = $this->convertDateToStandardFormat($row->projectStartingAt);
        $previousData['Tournament end date'] = $this->convertDateToStandardFormat($row->estimatedEndDate);
        $previousData['Tournament estimated cost'] = $row->projectEstimatedCost;
        $previousData['Tournament description'] = $row->projectDescription;

        $newData = [];
        $newData['Tournament code'] = $data['projectCode'];
        $newData['Tournament name'] = $data['projectName'];
        $newData['Tournament address'] = $data['projectAddress'];
        $newData['Tournament start date'] = $this->convertDateToStandardFormat($data['startDate']);
        $newData['Tournament end date'] = $this->convertDateToStandardFormat($data['endDate']);
        $newData['Tournament estimated cost'] = $data['projectEstimatedCost'];
        $newData['Tournament description'] = $data['projectDescription'];

        return array('previousData' => $previousData, 'newData' => $newData);
    }

    /**
    * this function use to get project mangers that match for the projectID.
    * @param array $projectId
    * return array
    **/
    public function getProjectManagers($projectId) 
    {
    	$projectManagers = $this->getModel('ProjectManagerTable')->getTournamentOfficialsByProjectID($projectId);
    	$data = [];

    	foreach ($projectManagers as $key => $value) {
            array_push($data, $value);
        }


        $view = new ViewModel(array(
            'employees' => $data,
            'paginated' => false,
        ));


        $view->setTerminal(true);
        $view->setTemplate('jobs/tournament/view-officials.phtml');
        
        return [
            'view' => $view,
            'status' => true,
        ];
    }

    /**
    * this function use to search project by key, projectManager, Customer name and project type.
    * @param array $data
    * @param  int $param
    * @param array $statusList
    * return array
    **/
    public function projectSearch($data, $param, $statusList) 
    {

    	$paginated = false;
        $key = ($data['searchKey'] == '') ? null : $data['searchKey'];
        $projectMgtEmpID = ($data['projectMgtEmpID'] == '') ? null : $data['projectMgtEmpID'];
        $projectTypeID = ($data['projectTypeID'] == '') ? null : $data['projectTypeID'];
        $projectStatusID = ($data['projectStatusID'] == '') ? null : $data['projectStatusID'];

        if (!empty($key) || !is_null($projectMgtEmpID) || !is_null($projectTypeID) || !is_null($projectStatusID)) {
            $projectList = $this->getModel('ProjectTable')->projectSearch($key, $projectMgtEmpID, $projectTypeID, $projectStatusID);
            $status = true;
        } else {
            $status = true;
            $projectController = $this->getServiceLocator()->get("ProjectController");
            $projectList = $projectController->getPaginatedProjects(6, $param);
            $paginated = true;
        }
       
        $view = new ViewModel(array(
            'project_list' => $projectList,
            'paginated' => $paginated,
            'status' => $statusList
        ));

        $view->setTerminal(true);
        $view->setTemplate('jobs/tournament/project-list.phtml');
        
        return [
            'view' => $view,
            'status' => $status
        ];

    }

    /**
    * this function use to search project by key, projectManager, Customer name and project type.
    * @param array $data
    * @param  int $param
    * @param array $statusList
    * return array
    **/
    public function deleteProject($data, $userId)
    {
        $projectID = $data['projectId'];
        if (!empty($projectID)) {

            $getProject = $this->getModel('ProjectTable')->getProjectDetailsByProjectID($projectID);

           	$getProject = $getProject->current();

            $getRealtedJobs = $this->getModel('JobTable')->getJobByProjectIdForProjectDeletion($projectID);

            if ($getProject['projectStatus'] != 3) {
                return $this->returnError('ERR_CAN_NOT_DELETING_TOURNAMENT',null);
            }
            if (count($getRealtedJobs) > 0) {
                return $this->returnError('ERR_CAN_NOT_DELETING_TOURNAMENT_DUE_TO_EVENT',null);
            }

            $entityService = $this->getService('EntityService');
            $updateEntity = $entityService->updateEntity($data['entityID'], $userId);

            $result = $this->getModel('ProjectTable')->deleteProject($projectID);

            if ($result) {
                $this->setLogMessage('Tournament '.$getProject['projectCode'].' is deleted.');
                return $this->returnSuccess(array($getProject['projectCode']),'SUCC_DELETED_TOURNAMENT');
            } else {
                return $this->returnError('ERR_OCURED_WHILE_DELETING_TOURNAMENT',$getProject['projectCode']);
            }

           
        } else {
            return $this->returnError('ERR_OCURED_WHILE_DELETING_TOURNAMENT',$getProject['projectCode']);
        }
    }

    public function addTournamentOfficials($tournamentOfficials , $saveId) 
    {

    	foreach ($tournamentOfficials as $key => $value) {
            $projectManagerID = null;
            $dataSet = [
                'projectManagerID' => $projectManagerID,
                'projectID' => $saveId,
                'employeeID' => $value['participantID'],
                'participantTypeID' => $value['participantTypeID'],
                'participantSubTypeID' => $value['participantSubTypeID'],
                'teamID' => ($value['teamID'] == undefind) ? null : $value['teamID']
            ];

            $projectManagerData = new ProjectManager();
            $projectManagerData->exchangeArray($dataSet);

            $saveProjectManagers = $this->getModel('ProjectManagerTable')->saveProjectManager($projectManagerData);

            if (!$saveProjectManagers) {
                return false;
            }

        }

        return true;
    }

    public function addRecordToProjectSupervisor($projectSupervisors, $saveId) 
    {
    	foreach ($projectSupervisors as $key => $value) {
            $projectSupervisorID = null;
            $dataSet = [
                'projectSupervisorID' => $projectSupervisorID,
                'projectID' => $saveId,
                'employeeID' => $value
            ];

            $projectSupervisorData = new ProjectSupervisor();
            $projectSupervisorData->exchangeArray($dataSet);

            $saveProjectSupervisors = $this->getModel('ProjectSupervisorTable')->saveProjectSupervisor($projectSupervisorData);

            if (!$saveProjectSupervisors) {
                return false;
            }
        }
        return true;
    }

    public function addRecordToProjectMaterials($projectMaterials, $saveId) 
    {
    	foreach ($projectMaterials as $key => $value) {
            $projectMaterialID = null;
            $dataSet = [
                'projectMaterialID' => $projectMaterialID,
                'projectID' => $saveId,
                'productID' => $key,
                'locationProductID' => $value
            ];

            $projectMaterialData = new ProjectMaterial();
            $projectMaterialData->exchangeArray($dataSet);

            $saveProjectMaterials = $this->getModel('ProjectMaterialTable')->saveProjectMaterial($projectMaterialData);

            if (!$saveProjectMaterials) {
                return false;
            }

        }
        return true;
    }

    /**
     * This function is used to get project details
     */
    public function getProjectDetails($data) {
        $projectID = $data['projectID'];
        $project = $this->getModel('ProjectTable')->getProjectDetailsByProjectID($projectID)->current();

        if ($project) {
            return $this->returnSuccess($project, '');
        } else {
            return $this->returnError('', null);
        }
    }
}