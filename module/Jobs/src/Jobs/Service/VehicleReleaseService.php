<?php
namespace Jobs\Service;

use Core\Service\BaseService;
use Zend\View\Model\ViewModel;

class VehicleReleaseService extends BaseService 
{

    /**
     * This function is used to get job by search key
     */
    public function getJobBySearchKeyForVehicleRelease($data, $param) {
        $paginated = false;
        $jobSearchKey = ($data['jobSearchKey'] == "") ? null: $data['jobSearchKey'];

        $VehicleReleaseController = $this->getServiceLocator()->get("VehicleReleaseController");
        if ($jobSearchKey != null) {
            $tbl = $this->getModel('JobTable');
            $jobList = $tbl->getJobBySearchKeyForCosting($jobSearchKey,null, null, null, null, true);
        } else {
            $jobList = $VehicleReleaseController->getPaginatedInvoicedJobs($param);
        }

        $jobInTimeData = $VehicleReleaseController->getJobInTime();
        $view = new ViewModel(array(
            'jobList' => $jobList,
            'jobInTimeData' => $jobInTimeData,
            'paginated' => $paginated,
            'status' => $this->getStatusesList(),
        ));

        $view->setTerminal(true);
        $view->setTemplate('jobs/vehicle-release/list-table.phtml');

        return $view;
    }

    /**
     * This function is used to close job
     */
    public function vehicleOut($data) {
        $jobIDs = $data['jobID'];
        $outType = $data['outType'];

        if ($outType == 'singleJob') {
            if(!is_array($jobIDs)){
                $jobIdArray = array($jobIDs);
            } else {
                $jobIdArray = $jobIDs;
            }

            $updateJob = $this->closeJobForVehicleOutByJobIDs($jobIdArray);
            if (!$updateJob) {
                return $this->returnError('ERR_UPDATE_JOB_STATE');
            }
        } else {
            $jobData = $this->getModel('JobTable')->getCompletedJobsWithServices(false,null,true,false,true);

            $jobIdData = [];
            foreach ($jobData as $value) {
                $jobIdData[] = $value['jobId'];
            }

            $jobIds = array_unique($jobIdData);
            $updateJob = $this->closeJobForVehicleOutByJobIDs($jobIds);
            if (!$updateJob) {
                return $this->returnError('ERR_UPDATE_JOB_STATE');
            }
        }
        return $this->returnSuccess(null,'SUCC_VEHICLE_OUT');
    }

    public function closeJobForVehicleOutByJobIDs($jobIds) {
        $gmtDate = $this->getGMTDateTime('Y-m-d H:i:s');
        $currentDateTime = $gmtDate['data']['currentTime'];
        $data = array(
            'jobStatus' => 4,
            'jobClosedDate' => $currentDateTime
        );
        
        $updateJob = $this->getModel('JobTable')->updateJobData($jobIds, $data);

        return $updateJob;
    }

}