<?php

namespace Jobs\Service;

use Core\Service\BaseService;
use Jobs\Model\VehicleModel;
use Jobs\Model\VehicleSubModel;
use Jobs\Model\VehicleType;
use Jobs\Model\TaskCardTaskProduct;
use Jobs\Model\ServiceProduct;
use Zend\View\Model\ViewModel;

class ResourcesService extends BaseService {

	/**
	 * This function is used to save vehicle type
	 * @param JSON Data
	 */
	public function saveVehicleType($data, $userId) 
	{
		if (is_null($data['vehicleModel']) || !isset($data['vehicleTypeName'])) {
			return $this->returnError('ERR_VEH_TYP_SAVE', null);
		}

		$vehicleTypeName = $data['vehicleTypeName'];
		$vehicleTypeStatus = $this->getStatusIDByStatusName('Active');
		$entityService = $this->getService('EntityService');
		$entityId = $entityService->createEntity($userId)['data']['entityID'];
		$vehicleModel = $data['vehicleModel'];

		$data = array(
			'vehicleTypeID' => null,
			'vehicleTypeName' => $vehicleTypeName,
			'vehicleTypeStatus' => $vehicleTypeStatus,
			'entityId' => $entityId,
		);

		$vehicleType = new VehicleType();
		$vehicleType->exchangeArray($data);

		$vehicleTypeID = $this->getModel('VehicleTypeTable')->saveVehicleType($vehicleType);

		if(!$vehicleTypeID || is_null($vehicleTypeID)) {
			return $this->returnError('ERR_VEH_TYP_SAVE', null);
		}

		$vehicleModelExecuteError = FALSE;
		foreach ($vehicleModel as $key => $value) {
			$modelData = array(
				'vehicleModelID' => null,
				'vehicleModelName' => $value['vehicleModelName'],
				'vehicleTypeID' => $vehicleTypeID,
				'vehicleTypeDelete' => '0',
			);
			$vehicleModelData = new VehicleModel();
			$vehicleModelData->exchangeArray($modelData);

			$vehicleModelRespond = $this->getModel('VehicleModelTable')->saveVehicleModel($vehicleModelData);
			if(!$vehicleModelRespond) {
				$vehicleModelExecuteError = TRUE;
			}

			foreach ($value['vehicleSubModelList'] as $val) {
				$subModelData = array(
					'vehicleSubModelID' => null,
					'vehicleSubModelName' => $val['vehicleSubModelName'],
					'vehicleModelID' => $vehicleModelRespond,
				);
				$vehicleSubModelData = new VehicleSubModel();
				$vehicleSubModelData->exchangeArray($subModelData);

				$vehicleSubModelRespond = $this->getModel('VehicleSubModelTable')->saveVehicleSubModel($vehicleSubModelData);
				if(!$vehicleSubModelRespond) {
					return $this->returnError('ERR_VEH_TYP_SAVE', null);
				}
			}
		}

		if($vehicleModelExecuteError) {
			return $this->returnError('ERR_VEH_TYP_SAVE', null);
		}

		return $this->returnSuccess($vehicleTypeID, 'SUCC_VEH_TYP_SAVE');

	}

	/**
	 * This function is used to update vehicle type
	 * @param JSON Data
	 */
	public function updateVehicleType($data, $userId) 
	{
		if (is_null($data['vehicleModel']) || !isset($data['vehicleTypeName']) || !isset($data['vehicleTypeID'])) {
			return $this->returnError('ERR_VEH_TYP_UPDATE', null);
		}

		$vehicleTypeID = $data['vehicleTypeID'];
		$vehicleTypeName = $data['vehicleTypeName'];
		$vehicleModel = $data['vehicleModel'];

		$data = array(
			'vehicleTypeName' => $vehicleTypeName,
		);

		$vehicleTypeUpdateRespond = $this->getModel('VehicleTypeTable')->updateVehicleType($data, $vehicleTypeID);

		if(!$vehicleTypeUpdateRespond) {
			return $this->returnError('ERR_VEH_TYP_UPDATE', null);
		}

		$vehicleModelExecuteError = FALSE;
		$updatedModelIds = [];
		foreach ($vehicleModel as $key => $value) {
			$updatedSubModelIds = [];
			if($value['vehicleModelID'] == 'new'){
				$modelData = array(
					'vehicleModelID' => null,
					'vehicleModelName' => $value['vehicleModelName'],
					'vehicleTypeID' => $vehicleTypeID,
					'vehicleTypeDelete' => '0',
				);
				$vehicleModelData = new VehicleModel();
				$vehicleModelData->exchangeArray($modelData);
				$vehicleModelRespond = $this->getModel('VehicleModelTable')->saveVehicleModel($vehicleModelData);
				if(!$vehicleModelRespond) {
					$vehicleModelExecuteError = TRUE;
				} else {
					$updatedModelIds[] = $vehicleModelRespond;
				}

				foreach ($value['vehicleSubModelList'] as $val) {
					$subModelData = array(
						'vehicleSubModelID' => null,
						'vehicleSubModelName' => $val['vehicleSubModelName'],
						'vehicleModelID' => $vehicleModelRespond,
					);
					$vehicleSubModelData = new VehicleSubModel();
					$vehicleSubModelData->exchangeArray($subModelData);

					$vehicleSubModelRespond = $this->getModel('VehicleSubModelTable')->saveVehicleSubModel($vehicleSubModelData);
					if(!$vehicleSubModelRespond) {
						$vehicleModelExecuteError = TRUE;
					} else {
						$updatedSubModelIds[] = $vehicleSubModelRespond;
					}
				}
			} else {

				foreach ($value['vehicleSubModelList'] as $vl) {
					if ($vl['vehicleSubModelID'] == 'new') {
						$subModelData = array(
							'vehicleSubModelID' => null,
							'vehicleSubModelName' => $vl['vehicleSubModelName'],
							'vehicleModelID' => $value['vehicleModelID']
						);
						$vehicleSubModelData = new VehicleSubModel();
						$vehicleSubModelData->exchangeArray($subModelData);

						$vehicleSubModelRespond = $this->getModel('VehicleSubModelTable')->saveVehicleSubModel($vehicleSubModelData);
						if(!$vehicleSubModelRespond) {
							$vehicleModelExecuteError = TRUE;
						} else {
							$updatedSubModelIds[] = $vehicleSubModelRespond;
						}
					} else {
						$updatedSubModelIds[] = $vl['vehicleSubModelID'];
					}
				}
				$dataSet = $this->getModel('VehicleModelTable')->viewVehicleModel($value['vehicleModelID']);
				foreach ($dataSet as $key => $dat) {
					if (!in_array($dat['vehicleSubModelID'], $updatedSubModelIds)) {
						$vehicleSubModelRespond = $this->getModel('VehicleSubModelTable')->deleteVehicleSubModel($dat['vehicleSubModelID']);
						if(!$vehicleSubModelRespond) {
							$vehicleModelExecuteError = TRUE;
						}
					}
				}
				$updatedModelIds[] = $value['vehicleModelID'];
			}
		}

		$vehicleTypeData = $this->getModel('VehicleTypeTable')->viewVehicleType($vehicleTypeID);
		foreach ($vehicleTypeData as $key => $value) {
			if (!in_array($value['vehicleModelID'], $updatedModelIds)) {
				$vehicleModelRespond = $this->getModel('VehicleModelTable')->deleteVehicleModel($value['vehicleModelID']);
				if(!$vehicleModelRespond) {
					$vehicleModelExecuteError = TRUE;
				}
			}
		}

		if($vehicleModelExecuteError) {
			return $this->returnError('ERR_VEH_TYP_UPDATE', null);
		}
		return $this->returnSuccess($vehicleTypeID, 'SUCC_VEH_TYP_UPDATE');

	}

	/**
	 * This function is used to view vehicle type
	 * @param JSON Data
	 */
	public function viewVehicleType($data) 
	{
		$vehicleTypeID = $data['vehicleTypeID'];
		$vehiclModelID = $data['vehiclModelID'];
		$getVehicleSubModel = $data['getVehicleSubModel'];
		if (!empty($vehicleTypeID)) {
			$vehicleTypeData = $this->getModel('VehicleTypeTable')->viewVehicleType($vehicleTypeID);
			$vehicleModelData = [];
			foreach ($vehicleTypeData as $key => $value) {
				if ($getVehicleSubModel) {

					$dataSet = $this->getModel('VehicleModelTable')->viewVehicleModel($value['vehicleModelID']);
					$vehicleSubModelData = [];
					foreach ($dataSet as $key => $val) {
						$vehicleSubModelData[] = array(
							'vehicleSubModelID' => $val['vehicleSubModelID'],
							'vehicleSubModelName' => $val['vehicleSubModelName']
						);
					}

					$vehicleModelData[] = array(
						'vehicleModelID' => $value['vehicleModelID'],
						'vehicleModelName' => $value['vehicleModelName'],
						'vehicleSubModelList' => $vehicleSubModelData
					);
				} else {
					$vehicleModelData[] = array(
						'vehicleModelID' => $value['vehicleModelID'],
						'vehicleModelName' => $value['vehicleModelName']
					);
				}
				$respond = array(
					'vehicleTypeID' => $value['vehicleTypeID'],
					'vehicleTypeName' => $value['vehicleTypeName'],
					'vehicleModelData' => $vehicleModelData,
				);
			}

			if($vehicleTypeData) {
				return $this->returnSuccess($respond, 'SUCC_VEH_TYP_RTV');
			} else {
				return $this->returnError('ERR_VEH_TYP_RTV', null);
			}
		}

		if (!empty($vehiclModelID)) {
			$vehicleModelData = $this->getModel('VehicleModelTable')->viewVehicleModel($vehiclModelID);
			$vehicleSubModelData = [];
			foreach ($vehicleModelData as $key => $value) {
				$vehicleSubModelData[] = array(
					'vehicleSubModelID' => $value['vehicleSubModelID'],
					'vehicleSubModelName' => $value['vehicleSubModelName']
				);
				$respond = array(
					'vehicleModelID' => $value['vehicleModelID'],
					'vehicleModelName' => $value['vehicleModelName'],
					'vehicleSubModelData' => $vehicleSubModelData,
				);
			}

			if($vehicleModelData) {
				return $this->returnSuccess($respond, 'SUCC_VEH_TYP_RTV');
			} else {
				return $this->returnError('ERR_VEH_TYP_RTV', null);
			}
		}
	}

	/**
	 * This function is used to change status vehicle type
	 * @param JSON Data
	 */
	public function changeStatusVehicleType($data) 
	{
		$vehicleTypeID = $data['vehicleTypeID'];
		$vehicleTypeStatus = $this->getModel('VehicleTypeTable')->changeStatusVehicleType($vehicleTypeID);
		$respond = array('vehicleTypeID' => $vehicleTypeID, 'vehicleTypeStatus' => $vehicleTypeStatus);
		if($vehicleTypeStatus) {
			return $this->returnSuccess($respond, 'SUCC_VEH_TYP_CHG_ST');
		} else {
			return $this->returnError('ERR_VEH_TYP_CHG_ST', null);
		}
	}

	/**
	 * This function is used to delete vehicle type
	 * @param JSON Data
	 */
	public function deleteVehicleType($data, $userId) 
	{
		$vehicleTypeID = $data['vehicleTypeID'];
		$entityId = $this->getModel('VehicleTypeTable')->deleteVehicleType($vehicleTypeID);
		$respond = $this->getService('EntityService')->updateDeleteInfoEntity($entityId, $userId);

		if($respond) {
			return $this->returnSuccess($entityId, 'SUCC_VEH_TYP_DLT');
		} else {
			return $this->returnError('ERR_VEH_TYP_DLT', null);
		}

	}


	public function searchVehicleTypes($data, $param)
	{
		$key = isset($data['searchKey']) ? $data['searchKey'] : null;
		$paginated = false;

		if ($key != null) {
			$list= $this->getModel('VehicleTypeTable')->searchVehicleTypes($key);
		} else {
			$resourcesController = $this->getServiceLocator()->get("ResourcesController");
			$list = $resourcesController->getPaginatedVehiclesTypes(6, $param);
		}

		$view = new ViewModel(array(
			'vehicleTypeList' => $list,
			'paginated' => $paginated,
	        'status' => $this->getStatusesList(),
		));

		$view->setTerminal(true);
		$view->setTemplate('jobs/resources/vehicle-list');

		return $view;
	}

	/**
	 * This function is used to search vehicle type for dropdown
	 */
	public function searchVehicleTypeForDropdown($data) {
		$searchKey = $data['searchKey'];
		$vehicleTypes = $this->getModel('VehicleTypeTable')->getVehicleTypeforDropDown($searchKey);
		$vehicleTypeList = array();
		foreach ($vehicleTypes as $vtype) {
			$temp['value'] = $vtype['vehicleTypeID'];
			$temp['text'] = $vtype['vehicleTypeName'];
			$vehicleTypeList[] = $temp;
		}
		return $vehicleTypeList;
	}



	/**
	 * This function is used to search vehicle model for dropdown
	 */
	public function searchVehicleModelsForDropdown($data) {
		$searchKey = $data['searchKey'];
		$vehicleModels = $this->getModel('VehicleModelTable')->searchVehicleModelsForDropdown($searchKey);
		$vehicleModelsList = array();
		foreach ($vehicleModels as $vModel) {
			$temp['value'] = $vModel['vehicleModelID'];
			$temp['text'] = $vModel['vehicleModelName'];
			$vehicleModelsList[] = $temp;
		}
		return $vehicleModelsList;
	}

	/**
	 * This function is used to load vehicle model depend on vehicle type to dropdown
	 */
	public function loadVehicleModelForDropdown($data) {

		$vehicleTypeID = $data['vehicleTypeID'];
		$vehicleModelID = $data['vehicleModelID'];
		
		if (!empty($vehicleTypeID)) {
			$vehicleModels = $this->getModel('VehicleModelTable')->loadVehicleModelForDropdown($vehicleTypeID);
			if ($vehicleModels) {
				$vehicleModelsList = array();
				foreach ($vehicleModels as $vModel) {
					$temp['value'] = $vModel['vehicleModelID'];
					$temp['text'] = $vModel['vehicleModelName'];
					$vehicleModelsList[] = $temp;
				}
	            return $this->returnSuccess(array('vehicleModelsList' => $vehicleModelsList), 'SUCC_LOAD_JOBS');
			} else {
				return $this->returnError('ERR_LOAD_JOBS', null);
			}
		}

		if (!empty($vehicleModelID)) {
			$vehicleSubModels = $this->getModel('VehicleSubModelTable')->loadVehicleSubModelForDropdown($vehicleModelID);
			if ($vehicleSubModels) {
				$vehicleModelsList = array();
				foreach ($vehicleSubModels as $vModel) {
					$temp['value'] = $vModel['vehicleSubModelID'];
					$temp['text'] = $vModel['vehicleSubModelName'];
					$vehicleSubModelsList[] = $temp;
				}
	            return $this->returnSuccess(array('vehicleSubModelsList' => $vehicleSubModelsList), 'SUCC_LOAD_JOBS');
			} else {
				return $this->returnError('ERR_LOAD_JOBS', null);
			}	
		}
	}

	/**
	 * This function is used to load vehicle model depend on vehicle type to dropdown
	 */
	public function loadVehicleTypeForDropdown($data) {

		
		$vehicleTypes = $this->getModel('VehicleTypeTable')->loadVehicleTypeForDropdown($vehicleTypeID);
		if ($vehicleTypes) {
			$vehicleTypeList = array();
			foreach ($vehicleTypes as $vType) {
				$temp['value'] = $vType['vehicleTypeID'];
				$temp['text'] = $vType['vehicleTypeName'];
				$vehicleTypeList[] = $temp;
			}
            return $this->returnSuccess(array('vehicleTypeList' => $vehicleTypeList), 'SUCC_LOAD_JOBS');
		} else {
			return $this->returnError('ERR_LOAD_JOBS', null);
		}
		
	}

	/**
	 * This function is used to get service list by job card id
	 * @param JSON Data
	 */
	public function getServiceList($data)
	{
		$taskCardID = $data['taskCardID'];
		$serviceList = $this->getModel('TaskCardTaskTable')->getTaskList($taskCardID);

		foreach ($serviceList as $key => $value) {
			$respond[$value['taskCardTaskID']] = array(
				'taskCardTaskID' => $value['taskCardTaskID'],
				'taskID' => $value['taskID'],
				'taskCode' => $value['taskCode'],
				'taskName' => $value['taskName'],
			);
		}

		if($serviceList) {
			return $this->returnSuccess($respond, 'SUCC_RTRV_SRV_LIST');
		} else {
			return $this->returnError('ERR_RTRV_SRV_LIST', null);
		}
	}

	/**
	 * This function is used to get service list by vehicle type
	 * @param JSON Data
	 */
	public function getServiceListByVehicleType($data)
	{
		$vehicleTypeID = $data['vehicleTypeID'];
		$serviceList = $this->getModel('ServiceVehicleTypeTable')->getTaskCardList($vehicleTypeID);
		$serviceIncentiveRateData = [];
		foreach ($serviceList as $value) {
				$temp['taskCode'] = $value['taskCode'];
				$temp['taskName'] = $value['taskName'];
				$temp['serviceID'] = $value['serviceID'];
				$temp['vehicleTypeID'] = $value['vehicleTypeID'];
				$serviceIncentiveRateData[]	 = $temp;		
		}
		return $this->returnSuccess($serviceIncentiveRateData, 'SUCC_RTRV_SRV_LIST');
	}

	/**
	 * This function is used to get product list by job card service id
	 * @param JSON Data
	 */
	public function getServiceProductList($data)
	{
		$taskCardTaskID = $data['taskCardTaskID'];
		$productList = $this->getModel('TaskCardTaskProductTable')->getProductList($taskCardTaskID);

		foreach ($productList as $key => $value) {
			$product = array(
				'taskCardTaskProductID' => $value['taskCardTaskProductID'],
				'locationProductID' => $value['locationProductID'],
				'taskCardTaskProductName' => $value['taskCardTaskProductName'],
				'taskCardTaskProductType' => $value['taskCardTaskProductType'],
				'taskCardTaskProductQty' => $value['taskCardTaskProductQty'],
				'taskCardTaskProductUomID' => $value['taskCardTaskProductUomID'],
				'taskCardTaskProductUomText' => $value['uomName'] . "(" . $value['uomAbbr'] . ")",
			);
			if ($value['taskCardTaskProductType'] == '1') {
				$serviceMaterialsList[] = $product;
			} else if ($value['taskCardTaskProductType'] == '2') {
				$tradeMaterialsList[] = $product;
			} else {
				$others[] = $product;
			}
		}

		$respond = array(
			'serviceMaterialsList' => $serviceMaterialsList,
			'tradeMaterialsList' => $tradeMaterialsList,
			'others' => $others,
		);

		if($productList) {
			return $this->returnSuccess($respond, 'SUCC_RTRV_SRV_PRO_LIST');
		} else {
			return $this->returnError('ERR_RTRV_SRV_PRO_LIST', null);
		}
	}

	/**
	 * This function is used to get product list with all product related uom by job card service id
	 * @param JSON Data
	 */
	public function getServiceProductListWithProductUom($data)
	{
		$serviceID = $data['serviceID'];
		$vehicleTypeID = $data['vehicleTypeID'];
		$productList = $this->getModel('ServiceProductTable')->getProductList($serviceID, $vehicleTypeID);

		foreach ($productList as $key => $value) {
			$locationProductID = $value['locationProductID'];
			$uomList = $this->getModel('Inventory\Model\ProductUomTable')->getAllRelatedUomByLocationProductId($locationProductID);
			$productUomList = [];
			foreach ($uomList as $uom) {
			    $productUomList[] = array(
			        'uomID' => $uom['uomID'],
			        'uomText' => $uom['uomName'] . "(" . $uom['uomAbbr'] . ")",
			    );
			}
			$product = array(
				'taskCardTaskProductID' => $value['serviceProductID'],
				'locationProductID' => $locationProductID,
				'taskCardTaskProductName' => $value['productName'],
				'taskCardTaskProductType' => $value['serviceProductType'],
				'taskCardTaskProductQty' => $value['serviceProductQty'],
				'taskCardTaskProductUomID' => $value['serviceProductUomID'],
				'taskCardTaskProductAllUom' => $productUomList,
			);
			if ($value['serviceProductType'] == '1') {
				$serviceMaterialsList[] = $product;
			} else if ($value['serviceProductType'] == '2') {
				$tradeMaterialsList[] = $product;
			} else {
				$others[] = $product;
			}
		}

		$respond = array(
			'serviceMaterialsList' => $serviceMaterialsList,
			'tradeMaterialsList' => $tradeMaterialsList,
			'others' => $others,
		);

		if($productList) {
			return $this->returnSuccess($respond, 'SUCC_RTRV_SRV_PRO_LIST');
		} else {
			return $this->returnError('ERR_RTRV_SRV_PRO_LIST', null);
		}
	}

	/**
	 * This function is used to edit materials
	 * @param JSON Data
	 */
	public function editMaterials($data)
	{
		if (is_null($data['jobCardServiceId'])) {
			return $this->returnError('ERR_DATA_SET', null);
		}

		$jobCardServiceId = $data['jobCardServiceId'];
		$vehicleTypeID = $data['vehicleTypeID'];
		$materialsList = $data['materialsList'];
		$executeError = FALSE;
		$updatedMaterialIds = [];
		foreach ($materialsList as $key => $value) {
			if($value['taskCardTaskProductID'] == 'new'){
				$data = array(
					'serviceProductID' => null,
					'serviceID' => $jobCardServiceId,
					'vehicleTypeID' => $vehicleTypeID,
					'locationProductID' => $value['locationProductID'],
					'serviceProductType' => $value['taskCardTaskProductType'],
					'serviceProductQty' => $value['taskCardTaskProductQty'],
					'serviceProductUomID' => $value['taskCardTaskProductUomID'],
				);
				$taskCardTaskProduct = new ServiceProduct();
				$taskCardTaskProduct->exchangeArray($data);
				$respond = $this->getModel('ServiceProductTable')->saveTaskCardTaskProducts($taskCardTaskProduct);
				if(!$respond) {
					$executeError = TRUE;
				} else {
					$updatedMaterialIds[] = $respond;
				}
			} else {
				$id = $value['taskCardTaskProductID'];
				$data = array(
					'serviceProductQty' => $value['taskCardTaskProductQty'],
					'serviceProductUomID' => $value['taskCardTaskProductUomID'],
				);
				$respond = $this->getModel('ServiceProductTable')->updateTaskCardTaskProducts($data, $id);
				if(!$respond) {
					$executeError = TRUE;
				} else {
					$updatedMaterialIds[] = $value['taskCardTaskProductID'];
				}
			}
		}

		$productList = $this->getModel('ServiceProductTable')->getProductList($jobCardServiceId, $vehicleTypeID);
		foreach ($productList as $key => $value) {
			if (!in_array($value['serviceProductID'], $updatedMaterialIds)) {
				$respond = $this->getModel('ServiceProductTable')->deleteProduct($value['serviceProductID']);
				if(!$respond) {
					$executeError = TRUE;
				}
			}
		}

		if($executeError) {
			return $this->returnError('ERR_SVD_MTR', null);
		}
		return $this->returnSuccess($vehicleTypeID, 'SUCC_SVD_MTR');
	}
}