<?php

namespace Jobs\Service;

use Core\Service\BaseService;
use Jobs\Model\JobTaskCost;
use Jobs\Model\JobProductCost;
use Jobs\Model\JobCost;
use Jobs\Model\OtherJobCost;
use Jobs\Model\JobProduct;
use Jobs\Model\JobCardCost;
use Invoice\Model\InvoiceProductDeliveryNoteProduct;
use Zend\View\Model\ViewModel;

class CostService extends BaseService 
{
	
	/**
	* this function use to save job cost and update related tables
	* @param array $data
	* return array
	**/
	public function saveJobCost($data, $type = "construction") 
	{
		// save record to the job Cost table
		$saveData = [
			'jobID' => $data['jobID']
		];

		$jobCost = new JobCost();
		$jobCost->exchangeArray($saveData);

		// save Job Cost
		$jobCostSave = $this->getModel('JobCostTable')->save($jobCost);

		if($jobCostSave == null) {
			return $this->returnError('ERR_SAVE_JOB_COST');
		}

		// add data to the job task cost table
		foreach (array_filter($data['jobTask']) as $key => $jobTask) {

			$saveJobTaskData = [
				'jobTaskID' => ($jobTask['jobTaskID'] == 'default') ? 0 : $jobTask['jobTaskID'],
				'unitCost' => floatval($jobTask['jobTaskUnitCost']),
				'unitRate' => floatval($jobTask['jobTaskUnitRate']),
				'Qty' => floatval($jobTask['jobTaskQty']),
				'jobCostID' => $jobCostSave
			];
			$jobTaskCost = new JobTaskCost();
			$jobTaskCost->exchangeArray($saveJobTaskData);
			
			// save job task cost details
			$saveJobTaskCost = $this->getModel('JobTaskCostTable')->save($jobTaskCost);

			if (!$saveJobTaskCost) {
				return $this->returnError('ERR_SAVE_JOB_TASK_COST');
			}

			// update job Task table 
			$updatedDataSet = [
				'jobTaskInvoicedQty' => floatval($jobTask['jobTaskQty'])
			];

			// update
			if ($jobTask['jobTaskID'] != 'default') {
				$updateJobTask = $this->getModel('JobTaskTable')->updateJobTask($updatedDataSet, $jobTask['jobTaskID']);

				if (!$updateJobTask) {
					return $this->returnError('ERR_UPDATE_JOB_TASK');
				}
			}

			if ($type == 'service' && $jobTask['jobTaskID'] != 'default') {
				$getRelatedEmp = $this->getModel('jobTaskEmployeeTable')->getJobTaskRelatedEmployees($jobTask['jobTaskID']);
				foreach ($getRelatedEmp as $key => $value) {
					$allocateIncentiveVal = 0;

					if (floatval($jobTask['jobTaskQty']) > 1) {
						$allocateIncentiveVal = floatval($value['incentiveValue']) * floatval($jobTask['jobTaskQty']); 
						$updateJobTaskEmployeeIncentiveVal = $this->getModel('jobTaskEmployeeTable')->updateJobTaskEmployeeIncentiveVal($value['jobTaskEmployeeID'], $allocateIncentiveVal);
					}	
				}
			}


		}

		if ($type == 'service') {

			// add data to the job task cost table
			foreach (array_filter($data['jobCard']) as $key => $jobCard) {

				$saveJobCardData = [
					'jobCardID' => $jobCard['jobCardID'],
					'unitCost' => $jobCard['jobCardUnitCost'],
					'unitRate' => $jobCard['jobCardUnitRate'],
					'Qty' => $jobCard['jobCardQty'],
					'jobCostID' => $jobCostSave
				];
				$jobCardCost = new JobCardCost();
				$jobCardCost->exchangeArray($saveJobCardData);

				// save job card cost details
				$saveJobCardCost = $this->getModel('JobCardCostTable')->save($jobCardCost);

				
				$relateJobTask = $this->getModel('JobTaskTable')->getServiceDetailsByJobCardIdAndJobId($jobCard['jobCardID'], $data['jobID']);
				//save taskCard task in JobTaskCostTable
				foreach ($relateJobTask as $key => $task) {
					$saveJobTaskData = [
						'jobTaskID' => ($task['jobTaskID'] == 'default') ? 0 : $task['jobTaskID'],
						'unitCost' => 0,
						'unitRate' => 0,
						'Qty' => 0,
						'jobCostID' => $jobCostSave,
						'isJobCardTask' => 1
					];
					$jobTaskCost = new JobTaskCost();
					$jobTaskCost->exchangeArray($saveJobTaskData);
					
					// save job task cost details
					$saveJobTaskCost = $this->getModel('JobTaskCostTable')->save($jobTaskCost);

					if (!$saveJobTaskCost) {
						return $this->returnError('ERR_SAVE_JOB_TASK_COST');
					}

					// update job Task table 
					$updatedDataSet = [
						'jobTaskInvoicedQty' => floatval($task['jobTaskQty'])
					];

					// update
					if ($task['jobTaskID'] != 'default') {
						$updateJobTask = $this->getModel('JobTaskTable')->updateJobTask($updatedDataSet, $task['jobTaskID']);

						if (!$updateJobTask) {
							return $this->returnError('ERR_UPDATE_JOB_TASK');
						}
					}
				}
			}

		}

		// get job related Details
		$jobDetails = $this->getModel('JobTable')->getJobAndCustomerDetailsByJobID($data['jobID'])->current();
		
		// add data to the job product cost table
		foreach (array_filter($data['jobProduct']) as $key => $jobProduct) {
			
			$saveJobProduct = [
				'jobProductID' => $jobProduct['jobProductID'], 
				'productCost' => $jobProduct['jobProductUnitPrice'],
				'Qty' => $jobProduct['jobProductQty'],
				'jobCostID' => $jobCostSave
			];
			$jobProductCost = new JobProductCost();
			$jobProductCost->exchangeArray($saveJobProduct);

			// save job product cost details
			$saveJobProduct = $this->getModel('JobProductCostTable')->save($jobProductCost);

			if (!$saveJobProduct) {
				return $this->returnError('ERR_SAVE_JOB_PRODUCT');
			}

			// get jobProduct details
			$jobyProduct = $this->getModel('JobProductTable')->getJobProductByJobProductID($jobProduct['jobProductID']);
			
			// update job product table
			$updateJobPro = [
				'jobProductInvoicedQty' => floatval($jobProduct['jobProductQty']) + floatval($jobyProduct->jobProductInvoicedQty)
			];

			$updateJobProduct = $this->getModel('JobProductTable')->update($updateJobPro, $jobProduct['jobProductID']);
			
			if (!$updateJobProduct) {
				return $this->returnError('ERR_UPDATE_JOB_PRODUCT');
			}
		}


		$productData = [];
		$newProductData = [];
		if ($data['otherMaterials'] != null) {
			foreach ($data['otherMaterials'] as $value) {
				$pData = array(
                    'jobID' => $data['jobID'],
                    'locationProductID' => $value['jobMaterialTaskLocationProductID'],
                    'jobProductUnitPrice' => $value['unitPrice'],
                    'uomID' => $value['jobMaterialSelectedUomID'],
                    'jobProductAllocatedQty' => $value['issueQty'],
                    'jobProductReOrderLevel' => 0,
                    'jobProductMaterialTypeID' => 2,
                    'jobProductTaskCardID' => null,
                    'jobTaskID' => null,
                    'jobProductDescription' => $value['itemDes'],
                );

                $jobProductData = new JobProduct();
                $jobProductData->exchangeArray($pData);
                $jobProductID = $this->getModel('JobProductTable')->saveJobProduct($jobProductData);	

                $temp['jobProductId'] = $jobProductID;
                $temp['issueQty'] = $value['issueQty'];	

                $productData[] = $temp;
                $newProductData[$value['jobMaterialProID']] = [
                	'jobProductID' => $jobProductID,
                	'jobProductQty' => $value['issueQty'],
                	'jobProductUnitPrice' => $value['unitPrice'],
                	'locationProductID' => $value['jobMaterialTaskLocationProductID'],
                ];		
			}

			if (sizeof($productData) > 0) {
	            $materialRequisitionService = $this->getService('MaterialRequisitionService');
	            $update = $materialRequisitionService->updateIssueQuantity($productData, $data['jobID']);
	            if (!$update['status']) {
	                return $this->returnError($update['msg'], null);
	            }
	        }
		}


		foreach (array_filter($newProductData) as $key => $jobProduct) {
			
			$saveJobProduct = [
				'jobProductID' => $jobProduct['jobProductID'], 
				'productCost' => $jobProduct['jobProductUnitPrice'],
				'Qty' => $jobProduct['jobProductQty'],
				'jobCostID' => $jobCostSave
			];
			$jobProductCost = new JobProductCost();
			$jobProductCost->exchangeArray($saveJobProduct);

			// save job product cost details
			$saveJobProduct = $this->getModel('JobProductCostTable')->save($jobProductCost);

			if (!$saveJobProduct) {
				return $this->returnError('ERR_SAVE_JOB_PRODUCT');
			}

			// get jobProduct details
			$jobyProduct = $this->getModel('JobProductTable')->getJobProductByJobProductID($jobProduct['jobProductID']);
			
			// update job product table
			$updateJobPro = [
				'jobProductInvoicedQty' => floatval($jobProduct['jobProductQty']) + floatval($jobyProduct->jobProductInvoicedQty)
			];

			$updateJobProduct = $this->getModel('JobProductTable')->update($updateJobPro, $jobProduct['jobProductID']);
			
			if (!$updateJobProduct) {
				return $this->returnError('ERR_UPDATE_JOB_PRODUCT');
			}
		}

		// add data to the other job cost table
		foreach (array_filter($data['jobCost']) as $key => $jobOtherCost) {
		 	
		 	$saveJobOtherCost = [
		 		'jobID' => $jobOtherCost['jobID'],
		 		'costType' => $jobOtherCost['costID'],
		 		'otherJobCostUnitCost' => $jobOtherCost['unitCost'],
		 		'otherJobCostNoOfUnits' => $jobOtherCost['units'],
		 		'jobCostID' => $jobCostSave
		 	];

		 	$jobOtherCosts = new OtherJobCost();
		 	$jobOtherCosts->exchangeArray($saveJobOtherCost);

		 	$saveOtherCost = $this->getModel('OtherJobCostTable')->save($jobOtherCosts);

		 	if (!$saveOtherCost) {
		 		return $this->returnError('ERR_SAVE_OTHER_COST');
		 	}
		 } 

		  // get user active location
        $locationID = $this->user_session->userActiveLocation['locationID'];

		$dataForInvoice = [
		 	'jobCostID' => $jobCostSave,
		 	'locationID' => $locationID,
		 	'jobID' => $data['jobID'],
		 	'invoiceComment' => $data['invoiceComment'],
		 	'projectID' => $jobDetails['projectId'],
		 	'jobDetails' => $jobDetails
		];



		// save invoice
		$returnDetails = $this->getDataForInvoice($dataForInvoice, $type);
		if (!$returnDetails['status']) {
			return $this->returnError($returnDetails['msg']);
		}
		
		$jobCostInvoiceData = array(
			'salesInvocieID' => $returnDetails['data']['salesInvoiceID']
		);

		$updateJobCost = $this->getModel('JobCostTable')->updateJobCost($jobCostInvoiceData, $jobCostSave);

		if (!$updateJobCost) {
			return $this->returnError('ERR_UPDATE_JOB_COST');
		}

		// update deliveryNoteProduct
		$updateDeliveryNote = $this->updateSalesInvoiceAndDeliveryNoteDetails($returnDetails['data']['salesInvoiceID'],$dataForInvoice);
		if (!$updateDeliveryNote['status']) {
			return $updateDeliveryNote;
		}
		// update job status
		$jobUpdateStatus = $this->jobStatusUpdate($jobDetails, $type);
		if (!$jobUpdateStatus['status']) {
			return $this->returnError($jobUpdateStatus['msg']);
		}

		if (!is_null($jobDetails['projectId'])) {
			//update project status
			$getNotClosedJobs =  $this->getModel('JobTable')->getJobDetailsByProjectID($jobDetails['projectId'], true)->current();
			if (!$getNotClosedJobs) {
				$status = 4; // 4 mean closed
				$updateProject = $this->getModel('ProjectTable')->updateProjectStatus($jobDetails['projectId'], $status);
				if (!$updateProject) {
					return $this->returnError('ERR_UPDATE_PROJECT_STATE');
				}
			}	
		}

		return $this->returnSuccess($returnDetails['data'], $returnDetails['msg']);
	}

	/**
	* this function use to update delivery note copied qty
	* @param int $invoiceID
	* @param array $commonDetails
	* return array
	**/
	public function updateSalesInvoiceAndDeliveryNoteDetails($invoiceID, $commonDetails)
	{
		// get job product details by given jobCost id
		$jobProductDetails = $this->getModel('JobProductCostTable')->getJobProductByJobCostID($commonDetails['jobCostID']);
		// get all deliveryNote Product details that related to the given jobID
		$deliveryNoteDetails = $this->getModel('deliveryNoteTable')->getDeliveryNoteProductsByJobID($commonDetails['jobID']);
		$deliveryNoteProductSet = [];
		foreach ($deliveryNoteDetails as $key => $deliveryProValue) {
			$deliveryNoteProductSet[$deliveryProValue['deliveryNoteLocationID']][$deliveryProValue['productID']][] = [
				'proQty' => $deliveryProValue['deliveryNoteProductQuantity'],
				'cpyQty' => $deliveryProValue['deliveryNoteProductCopiedQuantity'],
				'delProID' => $deliveryProValue['deliveryNoteProductID'],
				'proID' => $deliveryProValue['productID'],
			];
		}
		$invoiceProductArray = [];
		// get all invoice product detials that related to the given invoiceID
		$invoiceProductDetails = $this->getModel('invoiceProductTable')->getProductsByInvoiceID($invoiceID);
		foreach ($invoiceProductDetails as $key => $invProdctDetails) {
			$invoiceProductArray[$invProdctDetails['productID']] = $invProdctDetails['salesInvoiceProductID']; 
		}

		
		// get job Task details for given location and cost ID
		$jobTaskDetals = $this->getModel('jobTaskCostTable')->getJobTaskByJobCostIDAndLocationID($commonDetails['jobCostID'], $commonDetails['locationID']);
		$jobTaskProductSet = [];
		foreach ($jobTaskDetals as $key => $jobTaskValue) {

			if (!$jobTaskValue['isJobCardTask']) {
				$jobTaskProductSet[$jobTaskValue['jobTaskID']] = $jobTaskValue['productID'];
			} else {
				$getJobCardProductId = $this->getModel('TaskCardTable')->getTaskCardByID($jobTaskValue['jobTaskTaskCardID']);
				$jobTaskProductSet[$jobTaskValue['jobTaskID']] = $getJobCardProductId->productID;
			}
		}


		// check about default tasks
		$defaultJobTask = $this->getModel('JobTaskCostTable')->getDefaultJobTaskDetails($commonDetails['jobCostID'])->current();
		if ($defaultJobTask) {
			$productCode = 'NONINVENT_DEF_JOB';
        	$productDetails = $this->getModel('LocationProductTable')->getLocationProductAndUomByProductCodeAndLocationId($productCode, $commonDetails['locationID']);
			$jobTaskProductSet[0] = $productDetails['productID'];
		}

		$jobCostDeliveryNoteSet = [];
		foreach ($jobProductDetails as $key => $jobProductValue) {
			$currentJobTaskID = ($jobProductValue['jobTaskID'] == null) ? 0 : $jobProductValue['jobTaskID'];
			$jobCostProductQty = $jobProductValue['Qty'];
			$coun = 0;
	
			while ($jobCostProductQty != 0) {
				if (isset($deliveryNoteProductSet[$commonDetails['locationID']][$jobProductValue['productID']][$coun]['delProID']) && $deliveryNoteProductSet[$commonDetails['locationID']][$jobProductValue['productID']][$coun]['delProID'] > 0) {
					$checkAvailablityOfDeliveryNoteProduct = $this->getModel('DeliveryNoteProductTable')->getDeliveryNoteIDByDeliveryNoteProductID($deliveryNoteProductSet[$commonDetails['locationID']][$jobProductValue['productID']][$coun]['delProID'])->current();
					$remaingQty = ($checkAvailablityOfDeliveryNoteProduct) ? ($checkAvailablityOfDeliveryNoteProduct['deliveryNoteProductQuantity'] - $checkAvailablityOfDeliveryNoteProduct['deliveryNoteProductCopiedQuantity']) : 0;
					$jobProductCostQty = 0;
					
					if ($remaingQty != 0) {
						if($deliveryNoteProductSet[$commonDetails['locationID']][$jobProductValue['productID']][$coun]['proQty'] 
							- $deliveryNoteProductSet[$commonDetails['locationID']][$jobProductValue['productID']][$coun]['cpyQty'] > $jobCostProductQty) {
							$updatedeliveryNoteDataSet = [
								'deliveryNoteProductCopiedQuantity' => floatval($deliveryNoteProductSet[$commonDetails['locationID']][$jobProductValue['productID']][$coun]['cpyQty']) + floatval($jobCostProductQty)
							];
							$jobProductCostQty = $jobCostProductQty;
							$jobCostProductQty = 0;

						} else {
							$updatedeliveryNoteDataSet = [
								'deliveryNoteProductCopiedQuantity' => floatval($deliveryNoteProductSet[$commonDetails['locationID']][$jobProductValue['productID']][$coun]['proQty']),
								'copied' => 1
							];
						
							$jobProductCostQty =  floatval($deliveryNoteProductSet[$commonDetails['locationID']][$jobProductValue['productID']][$coun]['proQty']);
							$jobCostProductQty = floatval($jobCostProductQty) - (floatval($deliveryNoteProductSet[$commonDetails['locationID']][$jobProductValue['productID']][$coun]['proQty'] - $deliveryNoteProductSet[$commonDetails['locationID']][$jobProductValue['productID']][$coun]['cpyQty']));
						
						}

						$jobCostDeliveryNoteSet[] = [
							'jobID' => $jobProductValue['jobID'],
							'jobCostID' => $jobProductValue['jobCostID'],
							'jobProductCostQty' => $jobProductCostQty,
							'deliveryNoteProductID' => $deliveryNoteProductSet[$commonDetails['locationID']][$jobProductValue['productID']][$coun]['delProID'],
							'invoiceProductID' => ($invoiceProductArray[$jobTaskProductSet[$currentJobTaskID]] == null) ? $invoiceProductArray[$deliveryNoteProductSet[$commonDetails['locationID']][$jobProductValue['productID']][$coun]['proID']]: $invoiceProductArray[$jobTaskProductSet[$currentJobTaskID]]
						];

						// update deliveryNote ProductTable
						$updateDeliveryNoteProdct = $this->getModel('DeliveryNoteProductTable')->updateDeliveryNoteProduct($deliveryNoteProductSet[$commonDetails['locationID']][$jobProductValue['productID']][$coun]['delProID'], $updatedeliveryNoteDataSet);
						$coun++;
						if (!$updateDeliveryNoteProdct) {
							return $this->returnError('ERR_UPDATE_DLN');
						}
					} else {
						$coun++;
					}
				} else {
					$jobCostProductQty = 0;
					$coun++;
				}
			}
		}

		// update delivery note status
		$updateDeliveryNote = $this->updateDeliveryNote($commonDetails['jobID']);

		if (!$updateDeliveryNote['status']) {
			return $updateDeliveryNote;
		}

		// insert data to the invoiceProductDeliveryNoteProduct table
		$respond = $this->updateInvoiceProductCreditNoteProductCost($jobCostDeliveryNoteSet);
		return $respond;
	}

	/**
	* this function use to update delivery note status when all delivery note products are copied.
	* @param int $jobID
	* return array
	**/
	public function updateDeliveryNote($jobID)
	{
		// get deliveryNote with deliveryNote Product by given job ID
		$deliveryNoteDetails = $this->getModel('DeliveryNoteTable')->getDeliveryNoteProductsByJobID($jobID, true);
		$deliveryNoteStatus = [];
		foreach ($deliveryNoteDetails as $key => $value) {
			if (!$deliveryNoteStatus[$value['deliveryNoteID']]) {
				$deliveryNoteStatus[$value['deliveryNoteID']] = true;
			}
			
			if($value['copied'] == '0') {
				$deliveryNoteStatus[$value['deliveryNoteID']] = false;
			}

		} 
		foreach ($deliveryNoteStatus as $key => $value) {
			if ($value) {
				// update deliveryNote status
				$data = [
					'deliveryNoteStatus' => '4'
				];
				$status = $this->getModel('DeliveryNoteTable')->updateDeliveryNote($data, $key);
				if (!$status) {
					return $this->returnError('ERR_UPDATE_DLN_STATUS');
				}
				
			}
		}
		return $this->returnSuccess();
	}
	
	/**
	* this function use to insert data to the invoiceproductdeliverynoteproduct table
	* @param array $data
	* return array
	**/
	public function updateInvoiceProductCreditNoteProductCost($data)
	{
		$invoiceDeliveryNote = new InvoiceProductDeliveryNoteProduct();
		foreach ($data as $key => $saveData) {
			$invoiceDeliveryNote->exchangeArray($saveData);

			// save data to the invoiceProduct CreditNoteProduct table
			$save = $this->getModel('InvoiceProductDeliveryNoteProductTable')->save($invoiceDeliveryNote);
			if (!$save) {
				return $this->returnError('ERR_INV_DEL_ITEM_SAVE');
			}
		}
		return $this->returnSuccess();
	}

	/**
	* this function use to get job related items for given job Cost ID
	* @param array $data
	* return array
	**/
	public function getDataForInvoice($data, $type)
	{
		$counter = 1;
		$productSet = [];
		$totalAmount = 0;
		$customerID = $data['jobDetails']['customerID'];

		// set basic data for invoice
		$invDetails = $this->setBasicDataForInvoice($data['jobDetails'], $data['locationID'], $data['invoiceComment']);

		if ($type == "service") {
			// get job product details by given jobCost id
			$jobProductDetails = $this->getModel('JobProductCostTable')->getJobProductByJobCostID($data['jobCostID']);
			
			foreach ($jobProductDetails as $key => $jobProductValue) {
				if ($jobProductValue['jobProductMaterialTypeID'] == 2 && $jobProductValue['Qty'] > 0) {
					$proIndex = $jobProductValue['productID'].'_'.$counter;

					// get item related tax details
					$taxDetails = $this->getTaxDetails($jobProductValue['productID'], $jobProductValue['productCost'], $jobProductValue['Qty']);
					
					// set data to create invoice
					$productSet = $this->resetInvoicedItems($proIndex, $jobProductValue, $productSet, $data['locationID'], $data['jobID'], $jobProductValue['productCost'], $jobProductValue['Qty'], $jobProductValue['locationProductQuantity'], $jobProductValue['uomID'], $taxDetails);
					$totalAmount += floatval($productSet[$proIndex]['productTotal']);
					
					$counter++;
				}
			}
		}

		// get job task related details
		$jobTaskDetails = $this->getModel('JobTaskCostTable')->getJobTaskByJobCostIDAndLocationIDForInvoice($data['jobCostID'], $data['locationID']);
		
		foreach ($jobTaskDetails as $key => $jobTaskValue) {
			
			$proIndex = $jobTaskValue['productID'].'_'.$counter;

			// get item related tax details
			$taxDetails = $this->getTaxDetails($jobTaskValue['productID'], $jobTaskValue['unitRate'], $jobTaskValue['Qty']);
			
			// set data to create invoice
			$productSet = $this->resetInvoicedItems($proIndex, $jobTaskValue, $productSet, $data['locationID'], $data['jobID'], $jobTaskValue['unitRate'], $jobTaskValue['Qty'], $jobTaskValue['locationProductQuantity'], $jobTaskValue['uomID'], $taxDetails);
			$totalAmount += floatval($productSet[$proIndex]['productTotal']);
			$counter++;
		}

		if ($type == 'service') {
			// get job task related details
			$jobCardDetails = $this->getModel('JobCardCostTable')->getJobCardByJobCostIDAndLocationID($data['jobCostID'], $data['locationID']);


			foreach ($jobCardDetails as $key => $jobCardValue) {
				$proIndex = $jobCardValue['productID'].'_'.$counter;

				// get item related tax details
				$taxDetails = $this->getTaxDetails($jobCardValue['productID'], $jobCardValue['unitRate'], $jobCardValue['Qty']);
				
				// set data to create invoice
				$productSet = $this->resetInvoicedItems($proIndex, $jobCardValue, $productSet, $data['locationID'], $data['jobID'], $jobCardValue['unitRate'], $jobCardValue['Qty'], $jobCardValue['locationProductQuantity'], $jobCardValue['uomID'], $taxDetails);
				$totalAmount += floatval($productSet[$proIndex]['productTotal']);
				$counter++;
			}
		}

		// get default jobTask details
		$defaultJobTask = $this->getModel('JobTaskCostTable')->getDefaultJobTaskDetails($data['jobCostID'])->current();
		if ($defaultJobTask) {
			// get and set default item details
			$defaultItemDetails = $this->getOrCreateNonInventoryItem($data['locationID']);
			if (!$defaultItemDetails['status']) {
				return $defaultItemDetails;
			}
			$proDetails = $defaultItemDetails['data'];
			
			$proIndex = $proDetails['productID'].'_'.$counter;
			
			// get item related tax details
			$taxDetails = $this->getTaxDetails($proDetails['productID'], $defaultJobTask['unitRate'], $defaultJobTask['Qty']);
			
			// set data to create invoice
			$productSet = $this->resetInvoicedItems($proIndex, $proDetails, $productSet, $data['locationID'], $data['jobID'], $defaultJobTask['unitRate'], $defaultJobTask['Qty'], $proDetails['locationProductQuantity'], $proDetails['uomID'], $taxDetails);
			$totalAmount += floatval($productSet[$proIndex]['productTotal']);
			$counter++;	
		}

		if ($type == "service") {
			// get job other cost related details
			$jobOtherCostDetails = $this->getModel('OtherJobCostTable')->getOtherJobCostDetailsByJobCostIDAndLocationID($data['jobCostID'], $data['locationID']);
			foreach ($jobOtherCostDetails as $key => $jobOtherCostValue) {
				$proIndex = $jobProductValue['productID'].'_'.$counter;
				
				// get item related tax details
				$taxDetails = $this->getTaxDetails($jobOtherCostValue['productID'], $jobOtherCostValue['otherJobCostUnitCost'], $jobOtherCostValue['otherJobCostNoOfUnits']);
				
				// set data to create invoice
				$productSet = $this->resetInvoicedItems($proIndex, $jobOtherCostValue, $productSet, $data['locationID'], $data['jobID'], $jobOtherCostValue['otherJobCostUnitCost'], $jobOtherCostValue['otherJobCostNoOfUnits'], $jobOtherCostValue['locationProductQuantity'], $jobOtherCostValue['uomID'], $taxDetails);
				$totalAmount += floatval($productSet[$proIndex]['productTotal']);
				$counter++;	
			}
		}
		$invDetails['products'] = $productSet;
		$invDetails['invoiceTotalPrice'] = $totalAmount;
		$invDetails['jobID'] = $data['jobID'];
		$invDetails['ignoreBudgetLimit'] = true;

		$tempDimDta = [];
		$tempDimDta['dimensionTypeId'] = 'job';
		$tempDimDta['dimensionValueId'] = $data['jobID'];

		$invDetails['dimensionData'][$invDetails['invoiceCode']][] = $tempDimDta;

		if (!is_null($data['projectID'])) {
			$tempDimDta2['dimensionTypeId'] = 'project';
			$tempDimDta2['dimensionValueId'] = 5;
			$invDetails['dimensionData'][$invDetails['invoiceCode']][] = $tempDimDta2;
		}

		$customerID = (is_null($customerID)) ? 0 : $customerID;
		$saveInvoice = $this->getServiceLocator()->get("InvoiceController");
        $savedInvoice = $saveInvoice->storeInvoice($invDetails, $customerID, false, false);
	

        if ($savedInvoice['status']) {
        	// update customer outstanding
        	$updateCustomer = $this->updateCustomerOutstanding($savedInvoice['totalPrice'],$customerID);
        	if (!$updateCustomer) {
        		return $this->returnError('ERR_UPDATE_CUSTOMER_OUTSTNDING');
        	}
        }
        
		return $savedInvoice;
	}

	public function getOrCreateNonInventoryItem($locationID)
	{
		// check product code availability
		$proCode = 'DEF_JOB';
		$proName = 'Default materials for job';
        $productCode = 'NONINVENT_'.$proCode;
        $productDetails = $this->getModel('LocationProductTable')->getLocationProductAndUomByProductCodeAndLocationId($productCode, $locationID);
        if ($productDetails != null) {
        		return $this->returnSuccess($productDetails);
        } else {
	        // save product details
	        $productSaveDataSet = [
	        	'code' => $proCode,
	        	'name' => $proName,
	        ];
	    
	        $pI = $this->getServiceLocator()->get("PurchaseInvoiceController");
	        $productStatus =  $pI->insertCompundCostItems($productSaveDataSet, $locationID);
	        if ($productStatus == null) {
	        	return $this->returnError('ERR_PRODUCT_SAVE');
	        	
	        }
	        return $this->returnSuccess($productStatus);
	        

        }
        
	}
	
	/**
	* this function use to update customer current balance
	* @param float $invoiceTotal
	* @param int $customerID
	* return boolean
	**/
	public function updateCustomerOutstanding($invoiceTotal, $customerID)
	{
        $cust = $this->getModel('CustomerTable')->getCustomerByID($customerID);
		
        $custNewCurrentBalance = floatval($cust->customerCurrentBalance) + floatval($invoiceTotal);
        $data = [
            'customerCurrentBalance' => $custNewCurrentBalance,
        ];
        $updtCus = $this->getModel('CustomerTable')->updateCustomerByCustomerID($data, $customerID);
        return $updtCus;	
	}

	/**
	* this function use to rearrage job related items to create invoice
	* @param string $index
	* @param array $dataSet
	* @param array $dataArray
	* @param int $locationID
	* @param int $jobID
	* @param float $unitPrice
	* @param float $quantity
	* @param float $availableQuantity
	* @param int $uomID
	* @param array $tax
	* return array
	**/
	public function resetInvoicedItems($index, $dataSet, $dataArray, $locationID, $jobID, $unitPrice, $quantity, $availableQuantity, $uomID, $tax)
	{

		// calculate total value
		$totalValue = (floatval($unitPrice) * floatval($quantity)) + floatval($tax['tTA']);
		$avbleQty = [
			'qty' => $availableQuantity
		];

		$usdQty = [
			'qty' => $quantity
		];

		$dataArray[$index] = [
			'productID' => $dataSet['productID'],
			'documentTypeID' => 19, // job type is 19
			'documentID' => $jobID,
			'locationID' => $locationID,
			'locationProductID' => $dataSet['locationProductID'],
			'productIncID' => $index,
			'productCode' => $dataSet['productCode'],
			'productName' => $dataSet['productName'],
			'productPrice' => floatval($unitPrice),
			'inclusiveTaxProductPrice' => null,
			'productDiscount' => null,
			'productDiscountType' => null,
			'productTotal' => $totalValue,
			'pTax' => $tax,
			'productType' => $dataSet['productTypeID'],
			'productDescription' => $dataSet['jobProductDescription'],
			'stockUpdate' =>  false,
			'availableQuantity' => $avbleQty,
			'deliverQuantity' => $usdQty,
			'selectedUomId' => $uomID

		];

		return $dataArray;
	}

	/**
	* this function use to calculate product wise tax details
	* @param int $productID
	* @param float $unitPrice
	* @param float $qty
	* return array
	**/
	public function getTaxDetails($productID, $unitPrice, $qty)
	{
		// get product related tax details
		$taxDetails = $this->getModel('ProductTaxTable')->getProTaxDetailsByProductID($productID, true);

		// get tax details for this item
		$taxForThis = $this->calculateItemCustomTax($unitPrice, $productID, $qty);
		return $taxForThis;		
	}

	/**
	* this function use to set data for create invoice
	* @param array $jobDetails
	* @param int $locationID
	* return array
	**/
	public function setBasicDataForInvoice($jobDetails, $locationID, $invoiceComment)
	{
		$complateDataSet = [];
		
		// get invoice reference number
		$reference = $this->getService('ReferenceService')->getReferenceNumber(3, $locationID);

		// get currency details
		$currency = $this->getModel('DisplaySetupTable')->fetchAllDetails()->current();

		$complateDataSet = [
			'invoiceCode' => $reference['data']['referenceNo'],
			'locationOutID' => $locationID,
			'invoiceDate' => date('Y-m-d'),
			'dueDate' => date('Y-m-d'),
			'customer' => (is_null($jobDetails['customerID'])) ? 0 : $jobDetails['customerID'],
			'customerProfID' => $jobDetails['customerProfileID'],
			'customerName' => $jobDetails['customerName'],
			'deliveryCharge' => null,
			'invoiceComment' => "This invoice for job ". $jobDetails['jobReferenceNumber'].". \n".$invoiceComment,
			'paymentTermID' => 1,
			'invoiceDiscountType' => 'presentage',
			'invoiceDiscountRate' => null,
			'susTaxAmount' => 0,
			'showTax' => 1,
			'finalTotalWithInclusiveTax' => null,
			'deliveryNoteID' => null,
			'salesOrderID' => null,
			'quotationID' => null,
			'activityID' => null,
			'jobID'=> null,
			'projectID' => null,
			'salesPersonID' => null,
			'suspendedTax' => 0,
			'deliveryAddress' => null, 
			'promotionID' => null,
			'promotionDiscountValue' => 0,
			'invoiceWisePromotionDiscountType' => null,
			'customCurrencyId' => $currency['currencyID'],
			'customCurrencyRate' =>  $currency['currencyRate'],
			'priceListId' => null,
			'salesInvoiceDeliveryChargeEnable' => 0,
			'inclusiveTax' => false

		];

		return $complateDataSet;
		
	}

	/**
	* this function use to update job status as close when all job task products and all job products are invoiced.
	* @param array $jobDetails
	* return array
	**/
	public function jobStatusUpdate($jobDetails, $type)
	{
		$canCloseJob = true;
		
		// get all job product details that related to the given job ID
		$jobProductDetails = $this->getModel('JobProductTable')->getOpenJobProductByJobID($jobDetails['jobId'])->current();
		if ($jobProductDetails != '') {
			$canCloseJob = false;
		}

		// get all open state job products by job ID
		$jobOpenProductDetails = $this->getModel('JobProductTable')->getNotUsedJobProductByJobID($jobDetails['jobId'])->current();
		if ($jobOpenProductDetails != '') {
			$canCloseJob = false;
		}		

		// get all job task details that related to the given job ID
		$jobTaskDetails = $this->getModel('JobTaskTable')->getOpenJobTaskByJobID($jobDetails['jobId'])->current();
		if ($jobTaskDetails != '') {
			$canCloseJob = false;
		}

		// get open state job task for given jobID
		$jobTaskOpenDetails = $this->getModel('JobTaskTable')->getOpenStateJobTasjByJobID($jobDetails['jobId'])->current();
		if ($jobTaskOpenDetails != '') {
			$canCloseJob = false;
		}		

		if ($canCloseJob) {
			// update job status as closed
			$status = 4; // 4 mean closed
			$updateJob = $this->getModel('JobTable')->updateJobstatusID($jobDetails['jobId'], $status);
			if (!$updateJob) {
				return $this->returnError('ERR_UPDATE_JOB_STATE');
			}
		} else {
			$status = 8; // 8 mean in progress
			$updateJob = $this->getModel('JobTable')->updateJobstatusID($jobDetails['jobId'], $status);
			if (!$updateJob) {
				return $this->returnError('ERR_UPDATE_JOB_STATE');
			}
		}

		if ($type == "service") {
			$status = 20; // 20 mean invoiced
			$updateJob = $this->getModel('JobTable')->updateJobstatusID($jobDetails['jobId'], $status);
			if (!$updateJob) {
				return $this->returnError('ERR_UPDATE_JOB_STATE');
			}
		}

		return $this->returnSuccess(null,'SUCC_UPDATE_JOB_STATE');

	} 


	/**
     * This function is used to get job by search key
     */
    public function getJobBySearchKeyForServiceCost($data, $param, $locationID) {
        $paginated = false;
        $jobSearchKey = ($data['jobSearchKey'] == "") ? null: $data['jobSearchKey'];
        $jobCard = ($data['jobCard'] == "") ? null : $data['jobCard'];
        $serviceType = ($data['serviceType'] == "") ? null : $data['serviceType'];
        $serviceEndingDate = ($data['serviceEndingDate'] == "") ? null : $data['serviceEndingDate'];
        $serviceStartingDate = ($data['serviceStartingDate'] == "") ? null : $data['serviceStartingDate'];

        if (!is_null($serviceEndingDate)) {
            $serviceStartingDate = $this->getGMTDateTime('Y-m-d H:i:s', $serviceStartingDate)['data']['currentTime'];
            $serviceEndingDate = $this->getGMTDateTime('Y-m-d H:i:s', $serviceEndingDate)['data']['currentTime'];
        }

        $ServiceCostControllerController = $this->getServiceLocator()->get("ServiceCostController");
        if ($jobSearchKey != null || $jobCard != null || $serviceType != null || $serviceEndingDate != null) {
            $tbl = $this->getModel('JobTable');
            $jobList = $tbl->getJobBySearchKeyForCosting($jobSearchKey,$jobCard, $serviceType, $serviceStartingDate, $serviceEndingDate);
        } else {
            $jobList = $ServiceCostControllerController->getPaginatedCompletedJobs($param);
        }

        $jobCompletedTimeData = $ServiceCostControllerController->getJobCompletedTime();
        $view = new ViewModel(array(
            'jobList' => $jobList,
            'jobCompletedTimeData' => $jobCompletedTimeData,
            'paginated' => $paginated,
            'status' => $this->getStatusesList(),
        ));

        $view->setTerminal(true);
        $view->setTemplate('jobs/service-cost/list-table.phtml');

        return $view;
    }

    public function getJobServiceByJobId($data) {
		$jobServices = $this->getModel('JobTaskTable')->getJobTaskDetails($data['jobID']);
		$jobSupervisorData = [];
    	foreach ($jobServices as $value) {
    		$jobSupervisorData[$value['jobTaskID']]['jobTaskID'] = $value['jobTaskID'];
    		$jobSupervisorData[$value['jobTaskID']]['taskCode'] = $value['taskCode'];
    		$jobSupervisorData[$value['jobTaskID']]['taskName'] = $value['taskName'];
    		$jobSupervisorData[$value['jobTaskID']]['startedTime'] = $this->getUserDateTime($value['startedAt']);
    		$jobSupervisorData[$value['jobTaskID']]['endedTime'] = $this->getUserDateTime($value['endedAt']);
    	}

		return $jobSupervisorData;
	}

	public function getJobDetailsByJobId($data) {
        $jobServiceData = $this->getModel('JobTable')->getCompletedJobsWithServices(false,$data['jobID'], false);
            
        $jobServiceDetails = [];
        $jobCardIDs = [];
        foreach ($jobServiceData as $key => $row) {
            $tempG = array();
            $jobID = $row['jobId'];
            $tempG['jobId'] = $row['jobId'];
            $tempG['jobCode'] = $row['jobReferenceNumber'];
            $tempG['jobCustomer'] = $row['customerName'];
            $tempG['jobCardName'] = $row['taskCardName'];
            $tempG['vehicalModal'] = $row['vehicleModelName'];
            $tempG['vehicleType'] = $row['vehicleType'];
            $tempG['inDate'] = $this->getUserDateTime($row['createdTimeStamp']);
            $tempG['jobSupervisor'] = $row['userFirstName'].' '.$row['userLastName'];
            $tempG['jobStatus'] = $row['jobStatus'];
            $productUom = $this->getModel('ProductUomTable')->getProductUomListByProductID($row['productID']);

            $thisqty = $this->getProductQuantityViaDisplayUom($row['jobProductIssuedQty'], $productUom);
            $thisqty['quantity'] = ($thisqty['quantity'] == 0) ? '-' : $thisqty['quantity'];
            
            $jobServices = (isset($jobServiceDetails[$row['jobId']]['jobServices'])) ? $jobServiceDetails[$row['jobId']]['jobServices'] : array();
            $jobTradeMaterials = (isset($jobServiceDetails[$row['jobId']]['jobTradeMaterials'])) ? $jobServiceDetails[$row['jobId']]['jobTradeMaterials'] : array();

            if ($row['jobProductID'] != null) {
                $jobTradeMaterials[$row['jobProductID']] = array('jobProductMaterialTypeID' => $row['jobProductMaterialTypeID'],'jobProductID' => $row['jobProductID'],'locationProductID' => $row['locationProductID'], 'jobProductTaskID' => $row['jobProductTaskID'],'jobProductUnitPrice' => $row['jobProductUnitPrice'], 'uomID' => $row['uomID'], 'jobProductIssuedQty' => $thisqty['quantity'],'productCode' => $row['productCode'], 'productName' => $row['productName'],'uomAbbr' => $row['uomAbbr'], 'total' => floatval($row['jobProductIssuedQty']) * floatval($row['jobProductUnitPrice']));
            }

            if ($row['jobTaskID'] != NULL) {
                $serviceKey = $row['jobTaskID'];

                $serviceProducts = (isset($jobServices[$serviceKey]['serviceProducts'])) ? $jobServices[$serviceKey]['serviceProducts'] : array();
                $serviceSubTasks = (isset($jobServices[$serviceKey]['serviceSubTasks'])) ? $jobServices[$serviceKey]['serviceSubTasks'] : array();


                $jobServices[$serviceKey] = array('serviceCode' => $row['taskCode'], 'serviceName' => $row['taskName'],'jobServiceID' => $row['jobTaskID'],'serviceID' => $row['taskID'], 'serviceRate' => $serviceRate, 'jobTaskTaskCardID' => $row['jobTaskTaskCardID']);



                if (!empty($jobServices[$serviceKey]['jobTaskTaskCardID'])) {
                	$jobCardIDs[]=$jobServices[$serviceKey]['jobTaskTaskCardID'];
                }
              	
                if ($row['jobProductID'] != NULL && $row['jobTaskID'] == $row['jobProductTaskID'] && $row['jobProductMaterialTypeID'] == 1) {
                    $serviceProducts[$row['jobProductID']] = array('locationProductID' => $row['locationProductID'], 'jobProductUnitPrice' => $row['jobProductUnitPrice'], 'uomID' => $row['uomID'], 'jobProductIssuedQty' => $thisqty['quantity'],'jobProductMaterialTypeID' => $row['jobProductMaterialTypeID'],'productCode' => $row['productCode'], 'productName' => $row['productName'],'uomAbbr' => $row['uomAbbr']);
                }
                
                if ($row['jobSubTaskID'] != NULL && $row['jobTaskID'] == $row['jobTaskIdOfJobSubtask']) {
                    $serviceSubTasks[$row['jobSubTaskID']] = array('subTaskCode' => $row['subTaskCode'], 'subTaskName' => $row['subTaskName'],'subTaskID' => $row['subTaskId']);
                }
                
                $jobServices[$serviceKey]['serviceProducts'] = $serviceProducts;
                $jobServices[$serviceKey]['serviceSubTasks'] = $serviceSubTasks;
            }
            
            $tempG['jobServices'] = $jobServices;
            $tempG['jobTradeMaterials'] = $jobTradeMaterials;
            $jobServiceDetails[$row['jobId']] = $tempG;
        
        }


        $jobCardID = array_unique($jobCardIDs);
        if (sizeof($jobCardID) > 0 && sizeof($jobCardID) == 1) {
        	$jobServiceDetails[$data['jobID']]['jobCardID'] = $jobCardID[0];
      		$getJobCardServiceRate = $this->getModel('JobCardIncentiveRateTable')->getJobCardIncentiveByJobCardIdAndVehicleType($jobServiceDetails[$data['jobID']]['jobCardID'], $jobServiceDetails[$data['jobID']]['vehicleType']);
        	$jobServiceDetails[$data['jobID']]['jobCardRateDetails'] = $getJobCardServiceRate;
        	$jobServiceDetails[$data['jobID']]['useJobCardServices'] = true;
        }

        $materialData = $this->getModel('JobTempMaterialTable')->getJobRelatedMaterials($data['jobID']);

    	$tempMaterials = [];
    	foreach ($materialData as $value) {
    		$tempMaterials[$value['jobTempMaterialID']] = $value;
    	}

        $jobServiceDetails[$data['jobID']]['tempMaterials'] = $tempMaterials;

        $jobServiceDataFinal = $this->getServiceRateByServiceIdAndVehicleTypeId($jobServiceDetails[$jobID]);
        return $this->returnSuccess($jobServiceDataFinal,'success');
    }

    public function getServiceRateByServiceIdAndVehicleTypeId($serviceData)
    {	
    	$jobData = $serviceData;
    	$vehicleTypeID = $jobData['vehicleType'];
    	$jobServices = [];
    	foreach ($serviceData['jobServices'] as $key => $value) {
    		$jobServices[$key] = $value;
			$res = $this->getModel('ServiceInceniveRateTable')->getIncentiveByServiceIdAndVehicleType($value['serviceID'], $vehicleTypeID);
    		if (!empty($value['serviceSubTasks'])) {
    			if ($res) {
    				$subTaskRate = $this->getModel('RateCardTaskSubTaskTable')->getRelatedServiceSubtasksRates($res['serviceInceniveRateId']);
					if (count($subTaskRate) > 0) {
						$serviceRateBySubTask = 0;
						foreach ($subTaskRate as $sub) {
							foreach ($value['serviceSubTasks'] as $val) {
								if ($val['subTaskID'] == $sub['subTaskId']) {
									$serviceRateBySubTask += floatval($sub['rateCardTaskSubTaskRate']);
								}
								
							}
						}
						
						$jobServices[$key]['serviceRate'] = $serviceRateBySubTask;
					} else {
	    				$jobServices[$key]['serviceRate'] = (is_null($res['serviceRate']) ? 0 : floatval($res['serviceRate'])); 
					}
    			} else {
    				$jobServices[$key]['serviceRate'] = 0; 
    			}
    		} else {
    			if ($res) {
    				$jobServices[$key]['serviceRate'] = (is_null($res['serviceRate']) ? 0 : floatval($res['serviceRate'])); 
    			}
    		}
    	}
    	$jobData['jobServices'] = $jobServices;
    	
    	return $jobData;
    }

    public function getJobEmployeeByJobId($data) {
        $jobServices = $this->getModel('JobTable')->getJobEmployeesByJobId($data['jobID']);
        $jobEmployeeData = [];
        foreach ($jobServices as $value) {
            $jobEmployeeData[$value['jobId']]['jobId'] = $value['jobId'];
            $jobEmployeeData[$value['jobId']]['employeeCode'] = $value['employeeCode'];
            $jobEmployeeData[$value['jobId']]['employeeName'] = $value['employeeFirstName'].' '.$value['employeeSecondName'];
        }

        return $jobEmployeeData;
    }
}
