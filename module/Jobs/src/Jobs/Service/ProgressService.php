<?php

namespace Jobs\Service;

use Core\Service\BaseService;
use Zend\I18n\Translator\Translator;
use Zend\View\Model\ViewModel;
use Jobs\Model\Department;
use Jobs\Model\MaterialRequest;
use Jobs\Model\MaterialRequestJobProduct;


class ProgressService extends BaseService
{
	
    /**
     * This function is used to search by project and job id
     */
    public function searchByProjectAndJob($data, $userLocation) {
        $searchKey = null;
        $searchProject = ($data['projectID']) ? $data['projectID'] : null;

        $searchJob = ($data['jobID']) ? $data['jobID'] : null;

        $jobs = $this->getModel('JobTable')->jobSearchByKey($searchKey, $userLocation, $searchProject, null, null, $searchJob);
        
        $view = new ViewModel(array(
            'jobs' => $jobs,
            'contractorForJob' => $this->user_session->jobSettings->contractorForJob
        ));

        $view->setTerminal(true);
        $view->setTemplate('jobs/progress/progress-job-list.phtml');
        
        return [
            'view' => $view,
            'status' => true,
            'data' => $data
        ];

    }

    /**
     * This function is used start jobTask
     */
    public function startTask($data) {

        $jobTaskID = $data['jobTaskID'];
        $status = $data['status'];
        $jobID = $data['jobID'];
        $projectID = $data['projectID'];
        $action = $data['action'];

        $result = true;
        if ($action == 'start') {
            $result = $this->checkRelatedTaskProductIsApproved($jobTaskID, $jobID); 
        }
        if (!$result) {
            return $this->returnError('ERR_JOB_TSK_PRODUCT_REQUEST', 'JOB_TASK_PRODUCT_REQUEST_NOT_APPROVED');
        }

        $updateJobTaskState = $this->getModel('JobTaskTable')->updateJobTaskStatusByJobTaskID($jobTaskID, $status);
       
        if ($updateJobTaskState) {   
            $updateJobState = $this->getModel('JobTable')->updateJobStatus($jobID, $status);
            if (!$updateJobState) {
                return $this->returnError('ERR_JOB_TSK_STATUS_UPDATE', 'JOB_TASK_STATE_FAILD_UPDATE');
            }

            $updateProjectState = $this->getModel('ProjectTable')->updateProjectStatus($projectID, $status);
            if (!$updateProjectState) {
                return $this->returnError('ERR_JOB_TSK_STATUS_UPDATE', 'JOB_TASK_STATE_FAILD_UPDATE');
            }
            return $this->returnSuccess('ERR_JOB_TSK_STATUS_UPDATE', 'JOB_TASK_STATE_FAILD_UPDATE');
        } else {
            return $this->returnError('ERR_JOB_TSK_STATUS_UPDATE', 'JOB_TASK_STATE_FAILD_UPDATE');
        }

    }

    /**
     * This function is used get task details
     */
    public function getTaskDetailsById ($data)
    {
        $jobTaskID = $data['jobTaskID'];
        
        $getJobTaskDetails = $this->getModel('JobTaskTable')->getTaskDetailsByJobTaskId($jobTaskID);

        if (!$getJobTaskDetails) {
            return $this->returnError('ERR_GET_TASK_DETAILS', 'TASK_DETAILS_FAILD_TO_RETRIVE');
        }

        if (is_null($getJobTaskDetails['jobTaskProgress'])) {
            $getJobTaskDetails['jobTaskProgress'] = 0;
            
        }

        if (is_null($getJobTaskDetails['jobTaskUsedQty'])) {
            $getJobTaskDetails['jobTaskUsedQty'] = 0;
            
        }

        $getJobTaskDetails['jobTaskProgress'] = floatval($getJobTaskDetails['jobTaskProgress'] );
        $getJobTaskDetails['jobTaskUsedQty'] = floatval($getJobTaskDetails['jobTaskUsedQty'] );
        $getJobTaskDetails['jobTaskEstQty'] = floatval($getJobTaskDetails['jobTaskEstQty'] );

        $view = new ViewModel(array(
            'task' => $getJobTaskDetails,
        ));

        $view->setTerminal(true);
        $view->setTemplate('jobs/progress/task-progress-update-modal.phtml');
        
        return [
            'view' => $view,
            'status' => true,
            'data' => $getJobTaskDetails,
        ];
    }

    /**
     * This function is used update task progress
     */
    public function updatejobTaskProgress($data) {

        $jobTaskID = $data['jobTaskID'];
        $progress =  floatval($data['jobTaskProgress']);
        $jobID = $data['jobID'];
        $projectID = $data['projectID'];
        $usedQuantity = $data['usedQuantity'];

        $updateJobTaskProgress = $this->getModel('JobTaskTable')->updateJobTaskProgress($jobTaskID, $progress, $usedQuantity);

        if ($updateJobTaskProgress) {

            if ($progress == 100) {
                $status = $this->getStatusIDByStatusName('completed');
                $updateJobTaskState = $this->getModel('JobTaskTable')->updateJobTaskStatusByJobTaskID($jobTaskID, $status);

                //this funtion return if estimation information required to display
                if($this->checkProductEstimation($jobTaskID)) {
                    return $this->checkProductEstimation($jobTaskID, 'complete');
                }

            } else {
                $status = $this->getStatusIDByStatusName('in progress');
                $updateJobTaskState = $this->getModel('JobTaskTable')->updateJobTaskStatusByJobTaskID($jobTaskID, $status);
                $updateJobState = $this->getModel('JobTable')->updateJobStatus($jobID, $status);
                if (!$updateJobState) {
                    return $this->returnError('ERR_JOB_TASK_PROGRESS_UPDATE', 'JOB_TASK_STATE_FAILD_UPDATE');
                }

                $updateProjectState = $this->getModel('ProjectTable')->updateProjectStatus($projectID, $status);
                if (!$updateProjectState) {
                    return $this->returnError('ERR_JOB_TASK_PROGRESS_UPDATE', 'JOB_TASK_STATE_FAILD_UPDATE');
                }
            }

            return $this->returnSuccess('', 'SUCC_JOB_TASK_PROGRESS_UPDATE');
        } else {
            return $this->returnError('ERR_JOB_TASK_PROGRESS_UPDATE', 'JOB_TASK_STATE_FAILD_UPDATE');
        }

    }

    /**
     * This function is used update jobTaskStatus
     */
    public function holdOrEndTask($data) {

        $jobTaskID = $data['jobTaskID'];
        $status = $data['status'];
        $jobID = $data['jobID'];
        $projectID = $data['projectID'];

        $updateJobTaskState = $this->getModel('JobTaskTable')->updateJobTaskStatusByJobTaskID($jobTaskID, $status);
       
        if ($updateJobTaskState) {

            if ($status == $this->getStatusIDByStatusName('hold')) {
                $checkJobStatusCanChange = $this->checkJobCanUpdateTheState($jobID, $status);
                if ($checkJobStatusCanChange) {
                    $updateJobState = $this->getModel('JobTable')->updateJobStatus($jobID, $status);
                    if (!$updateJobState) {
                        return $this->returnError('ERR_JOB_TSK_STATUS_UPDATE', 'JOB_TASK_STATE_FAILD_UPDATE');
                    }
                }

                $checkProjectStatusCanChange = $this->checkProjectCanUpdateTheState($projectID, $status);
                if ($checkProjectStatusCanChange) {
                    $updateProjectState = $this->getModel('ProjectTable')->updateProjectStatus($projectID, $status);
                    if (!$updateProjectState) {
                        return $this->returnError('ERR_JOB_TSK_STATUS_UPDATE', 'JOB_TASK_STATE_FAILD_UPDATE');
                    }
                }
            }

            //this funtion return if estimation information required to display
            if($this->checkProductEstimation($jobTaskID)) {
                return $this->checkProductEstimation($jobTaskID, 'end');
            }

            return $this->returnSuccess('', 'SUCC_JOB_TASK_PROGRESS_UPDATE');
        } else {
            return $this->returnError('ERR_JOB_TSK_STATUS_UPDATE', 'JOB_TASK_STATE_FAILD_UPDATE');
        }

    }

    /**
     * This function is used to check whether job can change the status
     */
    public function checkJobCanUpdateTheState ($jobID, $status) {
        $taskList = $this->getModel('JobTaskTable')->getJobTaskDetails($jobID);
        $requestStatusTaskCount = 0;
        $taskCount = $taskList->count();

        foreach ($taskList as $key => $value) {
            if ($value['jobTaskStatus'] == $status) {
                $requestStatusTaskCount += 1;
            }
        }

        if ($taskCount == $requestStatusTaskCount) {
            return true;
        }
        return false;
    }

    /**
     * This function is used to check whether project can change the status
     */
    public function checkProjectCanUpdateTheState ($projectID, $status) {
        $jobList = $this->getModel('JobTable')->getJobDetailsAccordingToProject($projectID);
        $requestStatusJobCount = 0;
        $jobCount = $jobList->count();

        foreach ($jobList as $key => $value) {
            if ($value['jobStatus'] == $status) {
                $requestStatusJobCount += 1;
            }
        }

        if ($jobCount == $requestStatusJobCount) {
            return true;
        }
        return false;
    }

    /**
     * This function is used to job related products
     */
    public function getJobRelatedProducts($data) {
        $jobID = $data['jobID'];
        $productList = $this->getModel('JobProductTable')->getJobRelatedMaterials($jobID);

        if (!$productList) {
            return $this->returnError('ERR_JOB_TSK_STATUS_UPDATE', 'JOB_TASK_STATE_FAILD_UPDATE');
        }

        $products = [];
        foreach ($productList as $key => $value) {
            $temp = [

                'jobTaskID' => $value['jobTaskID'],
                'jobProductAllocatedQty' => floatval($value['jobProductAllocatedQty']),
                'jobProductID' => $value['jobProductID'],
                'jobProductStatus' => ($value['materialRequestStatus'] == null) ? '3': $value['materialRequestStatus']

            ];

            $products[] = $temp;
        }
        
        return $this->returnSuccess($products, 'ERR_JOB_TSK_STATUS_UPDATE');
    }

    /**
     * This function is used to send material request
     */
    public function sendMaterialRequest($data, $userId) {

        $jobID = $data['jobID'];
        $items = $data['itemArray'];
        $currentDate = date('Y-m-d');

        $referenceService = $this->getService('ReferenceService');
        $refData = $referenceService->getReferenceNumber(36);
        $rid = $refData['data']["referenceNo"];
        $locationReferenceID = $refData['data']["locationReferenceID"];
        $entityId = $this->getService('EntityService')->createEntity($userId)['data']['entityID'];
        $materialRequestTableData = [
            'jobId' => $jobID,
            'materialRequestCode' => $rid,
            'materialRequestDate' => $this->convertDateToStandardFormat($currentDate),
            'materialRequestStatus' => $this->getStatusIDByStatusName('in progress'),
            'entityId' => $entityId
        ];

        $materialRequestData = new MaterialRequest();
        $materialRequestData->exchangeArray($materialRequestTableData);

        $materialRequestId = $this->getModel('MaterialRequestTable')->save($materialRequestData);

        if (!$materialRequestId) {
            return $this->returnError('ERR_REQ_MATE_SAVE', null);
        }
        $referenceService->updateReferenceNumber($locationReferenceID);

        foreach ($items as $key => $value) {
            $materialData = [
                'materialRequestId' => $materialRequestId,
                'jobProductId' => $value['jobProductID'],
                'requestedQuantity' => $value['jobProductAllocatedQty'],
            ];
            
            $materialRequestJobProductData = new MaterialRequestJobProduct();
            $materialRequestJobProductData->exchangeArray($materialData);

            $materialRequestJobProductFlag = $this->getModel('MaterialRequestJobProductTable')->save($materialRequestJobProductData);
            if (!$materialRequestJobProductFlag) {
                return $this->returnError('ERR_REQ_MATE_SAVE', null);
            }

            $updatejobProductStatus = $this->getModel('JobProductTable')->updateJobProductStatus($value['jobProductID'], $this->getStatusIDByStatusName('in progress'));
            if (!$updatejobProductStatus) {
                return $this->returnError('ERR_REQ_MATE_SAVE', null);
            }

        }
       
        return $this->returnSuccess(null, 'SUCC_REQ_MATE_SAVE');        
    }

    /**
     * This function is used to check whether task related 
     */
    public function checkRelatedTaskProductIsApproved($jobTaskID, $jobID) {

        $taskRelatedProducts = $this->getModel('JobProductTable')->getTaskProductDatailsbyJobTaskID($jobTaskID, $jobID);

        $productCount = count($taskRelatedProducts);
        $tempCount = 0;

        if ($productCount == 0) {
            return true;
        }

        foreach ($taskRelatedProducts as $key => $value) {
            $res = $this->getModel('MaterialRequestJobProductTable')->checkProductRelatedRequestsApproved($value['jobProductID'])->current();
            if ($res['materialRequestStatus'] == '16') {
                $tempCount += 1;
            }
        }
        
        if ($tempCount == $productCount) {
            return true;
        }
        return false;
    }

    public function checkProductEstimation($jobTaskID, $action = null) {
        $products = $this->getModel('JobProductTable')->getJobProductByjobTaskID($jobTaskID);
        $exactlyEstimation = [];
        $overEstimation = [];
        $underEstimation = [];
        foreach ($products as $key => $value) {
            if ($value['jobProductEstimatedQty'] > $value['jobProductUsedQty']) {
                $value['estimationDifferent'] = $value['jobProductEstimatedQty'] - $value['jobProductUsedQty'];
                $overEstimation[] = $value;
            } else if ($value['jobProductEstimatedQty'] < $value['jobProductUsedQty']) {
                $value['estimationDifferent'] = $value['jobProductUsedQty'] - $value['jobProductEstimatedQty'];
                $underEstimation[] = $value;
            } else {
                $value['estimationDifferent'] = 0;
                $exactlyEstimation[] = $value;
            }
        }

        $overEstimationStr = '';
        foreach ($overEstimation as $key => $value) {
            if ($overEstimationStr === '') {
                $overEstimationStr = $value['productName'] . "(" . $value['estimationDifferent'] . ")";
            } else {
                $overEstimationStr .= ', ' . $value['productName'] . "(" . $value['estimationDifferent'] . ")";
            }
        }

        $underEstimationStr = '';
        foreach ($underEstimation as $key => $value) {
            if ($underEstimationStr === '') {
                $underEstimationStr = $value['productName'] . "(" . $value['estimationDifferent'] . ")";
            } else {
                $underEstimationStr .= ', ' . $value['productName'] . "(" . $value['estimationDifferent'] . ")";
            }
        }

        $data = array($underEstimationStr, $overEstimationStr);

        if (count($overEstimation) > 0 && count($underEstimation) > 0 && $action === 'end') {
            return $this->returnSuccess($data, 'INFO_TSK_END_OVR_UNDR_ESTI');
        } else if (count($overEstimation) > 0 && $action === 'end') {
            return $this->returnSuccess($data, 'INFO_TSK_END_OVR_ESTI');
        } else if (count($underEstimation) > 0 && $action === 'end') {
            return $this->returnSuccess($data, 'INFO_TSK_END_UNDR_ESTI');
        } else if (count($overEstimation) > 0 && count($underEstimation) > 0 && $action === 'complete') {
            return $this->returnSuccess($data, 'INFO_TSK_COM_OVR_UNDR_ESTI');
        } else if (count($overEstimation) > 0 && $action === 'complete') {
            return $this->returnSuccess($data, 'INFO_TSK_COM_OVR_ESTI');
        } else if (count($underEstimation) > 0 && $action === 'complete') {
            return $this->returnSuccess($data, 'INFO_TSK_COM_UNDR_ESTI');
        } else if (count($overEstimation) > 0 && count($underEstimation) > 0) {
            return $this->returnSuccess($data, 'INFO_TSK_OVR_UNDR_ESTI');
        } else if (count($overEstimation) > 0 ) {
            return $this->returnSuccess($data, 'INFO_TSK_OVR_ESTI');
        } else if (count($underEstimation) > 0) {
            return $this->returnSuccess($data, 'INFO_TSK_UNDR_ESTI');
        } else {
            return false;
        }
    }

    /**
    * this function use to details for resume task process vie in progress update
    * @param array $data
    * return mix
    **/
    public function getToResumeServiceDetails($data)
    {
        $jobTaskID = $data['jobTaskId'];
        $departmentID = $data['departmentID'];
        $pendingServices = [];
        $progressServices = [];

        $services = $this->getModel('JobTaskTable')->getServicesByJobTaskId($jobTaskID)->current();

        if (!$services) {
            return $this->returnError('ERR_DEPT_RELATE_SERVICE_RETRIVE', null);
        }

        $pendingServices[] =  $services;

        $departmentStationList = $this->getDepartmentStationList($departmentID);
        $departmentEmployeeList = $this->getDepartmentEmployeeList($departmentID);

        $progressServices = $this->getProgerssServiceList($departmentID, $status = 'progress');
        $endedServices = $this->getEndedServiceList($departmentID, $status = 'ended');

        $status = true;

        $view = new ViewModel(array(
            'pendingServices' => $pendingServices,
            'departmentStations' => $departmentStationList,
            'departmentEmployees' => $departmentEmployeeList,
            'progressServices' => $progressServices,
            'endedServices' => $endedServices,
            'timeZone' => $this->timeZone
        ));

        $view->setTerminal(true);
        $view->setTemplate('jobs/service-progress-update/progress-update-list-views.phtml');
        
        return [
            'view' => $view,
            'status' => $status
        ];

    }

    /**
    * this function use to get due services of the job
    * @param array $data
    * return mix
    **/
    public function getJobRelatedDueServices($data)
    {
        $jobID = $data['jobId'];
       

        $services = $this->getModel('JobTaskTable')->getJobRelatedDueServices($jobID);

        if($services) {
            $status = true;
        } else {
            $status = false;
        }

        $view = new ViewModel(array(
            'dueServices' => $services,
        ));

        $view->setTerminal(true);
        $view->setTemplate('jobs/service-progress-update/view-due-service-list.phtml');        
        return [
            'view' => $view,
            'status' => $status,
            'data' => $services->count()
        ];

    }

     /**
    * this function use to get department related list for progress update
    * @param array $data
    * return mix
    **/
    public function getDepartmentRelatedLists($data)
    {

        $departmentID = $data['departmentID'];
        $pendingServices = [];
        $otherServices = [];

        $services = $this->getModel('JobTaskTable')->getServicesByDepartmentId($departmentID, $status = 'pending');

        if (!$services) {
            return $this->returnError('ERR_DEPT_RELATE_SERVICE_RETRIVE', null);
        }

        foreach ($services as $key => $value) {
            if ($value['restarted']) {
                $pendingServices [] = $value;
            } else {
                $otherServices [] = $value;
            }
        }

        if (sizeof($otherServices) > 0) {
            foreach ($otherServices as $key => $part) {
               $sort[$key] = strtotime($part['createdTimeStamp']);
            }
            array_multisort($sort, SORT_ASC, $otherServices);
        }

        foreach ($otherServices as $key => $ser) {
            array_push($pendingServices, $ser);
        }

        $departmentStationList = $this->getDepartmentStationList($departmentID);
        $departmentEmployeeList = $this->getDepartmentEmployeeList($departmentID);

        $progressServices = $this->getProgerssServiceList($departmentID, $status = 'progress');
        $endedServices = $this->getEndedServiceList($departmentID, $status = 'ended');

        $status = true;
        $view = new ViewModel(array(
            'pendingServices' => $pendingServices,
            'departmentStations' => $departmentStationList,
            'departmentEmployees' => $departmentEmployeeList,
            'progressServices' => $progressServices,
            'endedServices' => $endedServices,
            'timeZone' => $this->timeZone
        ));

        $view->setTerminal(true);
        $view->setTemplate('jobs/service-progress-update/progress-update-list-views.phtml');
        
        return [
            'view' => $view,
            'status' => $status
        ];

    }

    /**
    * this function use to get department related list for progress update auto selecting process
    * @param array $data
    * return mix
    **/
    public function getListsForAutoSelectProcess($data)
    {

        $departmentID = $data['departmentID'];
        $jobTaskID = $data['jobTaskId'];
        $pendingServices = [];
        $otherServices = [];

        $services = $this->getModel('JobTaskTable')->getServicesByDepartmentId($departmentID, $status = 'pending');

        if (!$services) {
            return $this->returnError('ERR_DEPT_RELATE_SERVICE_RETRIVE', null);
        }

        foreach ($services as $key => $value) {
            if ($jobTaskID == $value['jobTaskID']) {
                $pendingServices [] = $value;
            } else {
                $otherServices [] = $value;
            }
        }

        if (sizeof($otherServices) > 0) {
            foreach ($otherServices as $key => $part) {
               $sort[$key] = strtotime($part['createdTimeStamp']);
            }
            array_multisort($sort, SORT_ASC, $otherServices);
        }

        foreach ($otherServices as $key => $ser) {
            array_push($pendingServices, $ser);
        }

        $departmentStationList = $this->getDepartmentStationList($departmentID);
        $departmentEmployeeList = $this->getDepartmentEmployeeList($departmentID);
        $progressServices = $this->getProgerssServiceList($departmentID, $status = 'progress');
        $endedServices = $this->getEndedServiceList($departmentID, $status = 'ended');

        $status = true;
        $view = new ViewModel(array(
            'pendingServices' => $pendingServices,
            'departmentStations' => $departmentStationList,
            'departmentEmployees' => $departmentEmployeeList,
            'progressServices' => $progressServices,
            'endedServices' => $endedServices,
            'timeZone' => $this->timeZone
        ));

        $view->setTerminal(true);
        $view->setTemplate('jobs/service-progress-update/progress-update-list-views.phtml');
        
        return [
            'view' => $view,
            'status' => $status
        ];

    }

    /**
    * this function use to get department station list by department id
    * @param int $departmentID
    * return array
    **/
    public function getDepartmentStationList($departmentID) {
        $departmentStationList = [];

        $departmentStations = $this->getModel('DepartmentStationTable')->getDepartmentRelatedStations($departmentID);

        if (!$departmentStations) {
            return $this->returnError('ERR_RETRIVE_DEP_RELATED_STAT', null);
        }

        foreach ($departmentStations as $key => $value) {
            $result = $this->getModel('jobTaskTable')->getInprogressJobTaskByStation($value['departmentStationId']);

            if (sizeof($result) > 0) {
                $value['engaged'] = true;

            } else {
                $value['engaged'] = false;
            }
            $departmentStationList[] = $value;
            
        }

        return $departmentStationList;
    }

    /**
    * this function use to get department employee list by department id
    * @param int $departmentID
    * return array
    **/
    public function getDepartmentEmployeeList($departmentID) {
        $departmentEmployeeList = [];
        $departmentEmployees = $this->getModel('EmployeeDepartmentTable')->getEmployeeDepartmentsByDepartmentId($departmentID);

        if (!$departmentEmployees) {
            return $this->returnError('ERR_RETRIVE_DEP_RELATED_EMP', null);
        }

        foreach ($departmentEmployees as $key => $value) {
            $result = $this->getModel('jobTaskTable')->getInprogressJobTaskByEmployeeId($value['employeeId']);

            if (sizeof($result) > 0) {
                $value['engaged'] = true;

            } else {
                $value['engaged'] = false;
            }
            $departmentEmployeeList[] = $value;
            
        }

        return $departmentEmployeeList;
    }

    /**
    * this function use to get progress update inprogress service list by department id
    * @param int $departmentID
    * @param string $status
    * return array
    **/
    public function getProgerssServiceList($departmentID, $status) {
        $progressServices = [];
        $progressList = $this->getModel('JobTaskTable')->getServicesByDepartmentId($departmentID, $status);

        if (!$progressList) {
            return $this->returnError('ERR_DEPT_RELATE_SERVICE_RETRIVE', null);
        }
        foreach ($progressList as $key => $val) {
            $progressServices [] = $val;
        }
        foreach ($progressServices as $key => $value) {
           $serviceKeys[$key] = strtotime($value['startedAt']);
        }
        array_multisort($serviceKeys, SORT_ASC, $progressServices);

        return $progressServices;
    }

     /**
    * this function use to get progress update end service list by department id
    * @param int $departmentID
    * @param string $status
    * return array
    **/
    public function getEndedServiceList($departmentID, $status) {
        $endedServices = [];
        $endedList = $this->getModel('JobTaskTable')->getServicesByDepartmentId($departmentID, $status);

        if (!$endedList) {
            return $this->returnError('ERR_DEPT_RELATE_SERVICE_RETRIVE', null);
        }
        foreach ($endedList as $key => $val) {
            $endedServices [] = $val;
        }
        foreach ($endedServices as $key => $end) {
           $endServiceKeys[$key] = strtotime($end['endedAt']);
        }
        array_multisort($endServiceKeys, SORT_DESC, $endedServices);

        return $endedServices;
    }
}
