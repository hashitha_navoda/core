<?php

namespace Jobs\Service;

use Core\Service\BaseService;
use Zend\I18n\Translator\Translator;
use Zend\View\Model\ViewModel;
use Jobs\Model\Department;
use Jobs\Model\DepartmentStation;

class DepartmentService extends BaseService
{
	/**
    * this function use to save department.
    * @param array $data
    * return array
    **/
    public function createDepartment($data, $userId)
    {
        $departmentStations = $data['departmentStations'];
        $departmentStationsData = [];
        $departmentName = $data['departmentName'];
        $departmentCode = $data['departmentCode'];
        $tournamentFlag = $data['tournamentFlag'];
        $departmentID = null;

        $result = $this->getModel('DepartmentTable')->getDepartments($departmentCode);

        if ($result->count() > 0) { 
            if ($tournamentFlag) {
                return $this->returnError('ERR_PART_TYPE_CODE_ALREDY_EXIST', 'DEPARTMENT_EXIST');
            } else {
                return $this->returnError('ERR_JOB_DEPT_CODE_ALREDY_EXIST', 'DEPARTMENT_EXIST');
            }
        }

        $data = array(
            'departmentCode' => $departmentCode,
            'departmentName' => $departmentName,
            'departmentID' => $departmentID,
        );

        $departmentData = new Department();
        $departmentData->exchangeArray($data);
        
        $savedResult = $this->getModel('DepartmentTable')->saveDepartments($departmentData);

        if (!$savedResult) {
            if ($tournamentFlag) {
                return $this->returnError('ERR_PART_TYPE_SAVE', 'DEPARTMENT_SAVE_FAILD');
            } else {
                return $this->returnError('ERR_JOB_DEPT_SAVE', 'DEPARTMENT_SAVE_FAILD');
            }
        } else {
            if (sizeof($departmentStations) > 0) {
                foreach ($departmentStations as $key => $value) {
                    $departmentStationsData[] = ['name' => $value, 'code' => $key];
                }


                $result = $this->insertDepartmentStation($departmentStationsData, $savedResult);

                if(!$result) {
                    return $this->returnError('ERR_JOB_DEPT_STAT_SAVE', 'DEPARTMENT_STATION_SAVE_FAILD');
                }
            }

            if ($tournamentFlag) {
                $this->setLogMessage('Participant type '.$departmentCode.' is created.');
                return $this->returnSuccess('PARTICIPANT_TYPE_SAVE_SUCESSFULLY', 'SUCC_PART_TYPE_SAVE');
            } else {
                return $this->returnSuccess('DEPARTMENT_SAVE_SUCESSFULLY', 'SUCC_JOB_DEPT_SAVE');
            }
        }
    }

    /**
    * this function use to update department.
    * @param array $data
    * @param int $userId
    * return array
    **/
    public function updateDepartment($data, $userId, $type = 'construction')
    {
        $departmentStations = $data['departmentStations'];
        $tournamentFlag = $data['tournamentFlag'];
        $departmentStationsData = [];
        $checkCodeValid = $this->getModel('DepartmentTable')->checkDepartmentCodeValid($data['departmentID'], $data['departmentCode']);

        if ($checkCodeValid > 0) {
            if ($tournamentFlag) {
                return $this->returnError('ERR_PART_TYPE_CODE_ALREDY_EXIST', 'DEPARTMENT_EXIST');
            } else {
                return $this->returnError('ERR_JOB_DEPT_CODE_ALREDY_EXIST', 'DEPARTMENT_EXIST');
            }
        }
        $oldDepartmentID = $data['departmentID'];
        $oldData = $this->getModel('DepartmentTable')->getDepartmentByDepartmentID($oldDepartmentID)->current();
        $departmentData = new Department();
        $departmentData->exchangeArray($data);
        $result = $this->getModel('DepartmentTable')->updateDepartmentDelete($data['departmentID']);


        if ($result) {
            $savedResult = $this->getModel('DepartmentTable')->saveDepartments($departmentData);
            if ($savedResult) {
                $updateEmployeeTable = $this->getModel('EmployeeTable')->updateDeparmentIDInEmployeeTable($oldDepartmentID, $savedResult);

                if ($type == 'service') {

                    $updateOldStations = $this->getModel('DepartmentStationTable')->updateOldDeparmentIdRelateStation($oldDepartmentID);

                    if (sizeof($departmentStations) > 0) {
                        foreach ($departmentStations as $key => $value) {
                            $departmentStationsData[] = ['name' => $value, 'code' => $key];
                        }

                        $result = $this->insertDepartmentStation($departmentStationsData, $savedResult);

                        if(!$result) {
                            return $this->returnError('ERR_JOB_DEPT_STAT_SAVE', 'DEPARTMENT_STATION_SAVE_FAILD');
                        }
                    }

                    $updateEmployeeDepartment = $this->getModel('EmployeeDepartmentTable')->updateEmployeeDepartmentWithNewDepartmentId($oldDepartmentID, $savedResult);

                    if(!$updateEmployeeDepartment) {
                        return $this->returnError('ERR_EMP_DEPT_UPDATE', null);
                    }

                }

                $updateTaskDepartment = $this->getModel('TaskTable')->updateTaskDepartmentWithNewDepartmentId($oldDepartmentID, $savedResult);

            } else {
                if ($tournamentFlag) {
                    return $this->returnError('ERR_PART_TYPE_UPDATE', 'DEPARTMENT_FAILD_UPDATE');
                } else {
                    return $this->returnError('ERR_JOB_DEPT_UPDATE', 'DEPARTMENT_FAILD_UPDATE');
                }
            }
            if ($tournamentFlag) {
                $changeArray = $this->getParticipantTypeChageDetails($oldData, $data);
                //set log details
                $previousData = json_encode($changeArray['previousData']);
                $newData = json_encode($changeArray['newData']);

                $this->setLogDetailsArray($previousData,$newData);
                $this->setLogMessage("Participant type ".$oldData['departmentCode']." is updated.");
                return $this->returnSuccess(array('jobTypeID' => $jobTypeID), 'SUCC_EVENT_TYPE_UPDATE');
                return $this->returnSuccess('PARTICIPANT_TYPE_SUCESSFULLY_UPDATE', 'SUCC_PART_TYPE_UPDATE');
            } else {
                return $this->returnSuccess('DEPARTMENT_SUCESSFULLY_UPDATE', 'SUCC_JOB_DEPT_UPDATE');
            }
        } else {
            if ($tournamentFlag) {
                return $this->returnError('ERR_PART_TYPE_UPDATE', 'PARTICIPANT_TYPE_FAILD_UPDATE');
            } else {
                return $this->returnError('ERR_JOB_DEPT_UPDATE', 'DEPARTMENT_FAILD_UPDATE');
            }
        }
    }

    public function getParticipantTypeChageDetails($row, $data)
    {
        $previousData = [];
        $previousData['Participant type code'] = $row['departmentCode'];
        $previousData['Participant type name'] = $row['departmentName'];

        $newData = [];
        $newData['Participant type code'] = $data['departmentCode'];
        $newData['Participant type name'] = $data['departmentName'];
        return array('previousData' => $previousData, 'newData' => $newData);
    }

    /**
    * this function use to search department by key.
    * @param string $departmentSearchKey
    * @param int $param
    * return array
    **/
    public function departmentSearchByKey($departmentSearchKey, $param, $type = 'construction')
    {
        $paginated = false;
        if (!empty($departmentSearchKey)) {
            $DepartmentList = $this->getModel('DepartmentTable')->departmentSearchByKey($departmentSearchKey);
            $status = true;
        } else {
            $status = true;
            $departmentController = $this->getServiceLocator()->get("DepartmentController");
            $DepartmentList = $departmentController->getPaginatedDepartments(6, $param);
            $paginated = true;
        }
       
        $view = new ViewModel(array(
            'departments' => $DepartmentList,
            'paginated' => $paginated,
        ));

        $view->setTerminal(true);

        if ($type == 'construction') {  
            $view->setTemplate('jobs/department/list.phtml');
        } else {
            $view->setTemplate('jobs/departmentStation/list.phtml');
        }

        return [
            'view' => $view,
            'status' => $status
        ];

    }


    public function searchDepartmentsForDropdown($data)
    {
        $searchKey = $data['searchKey'];
        $departments = $this->getModel('DepartmentTable')->getDepartmentforSearch($searchKey);
        $departmentList = array();
        foreach ($departments as $dep) {
            $temp['value'] = $dep['departmentID'];
            $temp['text'] = $dep['departmentCode'] . '-' . $dep['departmentName'];
            $departmentList[] = $temp;
        }
        return $departmentList;
    }

    /**
     * This function is used to delete department
     **/
    public function deleteDepartment($data, $type = 'construction')
    {
        $departmentID = $data['departmentID'];
        $tournamentFlag = $data['tournamentFlag'];
        if (!empty($departmentID)) {

            $checkDepartmentUsed = $this->getModel('EmployeeTable')->checkDepartmentUsed($departmentID);
            $oldData = $this->getModel('DepartmentTable')->getDepartmentByDepartmentID($departmentID)->current();
            if ($type == 'service') {
                $checkDepartmentUsedInService = $this->getModel('TaskTable')->checkDepartmentUsedInService($departmentID);
                if (!$checkDepartmentUsedInService) {
                    return $this->returnError('ERR_DEPT_ALREADY_USED_SRVC',null);
                }
            }

            if ($checkDepartmentUsed) {
                $result = $this->getModel('DepartmentTable')->updateDepartmentDelete($departmentID);

                if ($result) {
                    $deleteStations = $this->getModel('DepartmentStationTable')->updateOldDeparmentIdRelateStation($departmentID);
                    if ($tournamentFlag) {
                        $this->setLogMessage('Participant type '.$oldData['departmentCode'].' is deleted.');
                        return $this->returnSuccess('PARTICIPANT_TYPE_DELETED_SUCESSFULLY','SUCC_PART_TYPE_DELETEs');
                    } else {
                        return $this->returnSuccess('DEPARTMENT_DELETED_SUCESSFULLY','SUCC_DEPT_DELETE');
                    }
                } else {
                    if ($tournamentFlag) {
                        return $this->returnError('ERR_PART_TYPE_DELETE',null);
                    } else {
                        return $this->returnError('ERR_DEPT_DELETE',null);
                    }
                }


            } else {
                if ($tournamentFlag) {
                    return $this->returnError('ERR_PART_TYPE_ALREADY_USED',null);
                } else {
                    return $this->returnError('ERR_DEPT_ALREADY_USED',null);
                }
            }
           
        } else {
            if ($tournamentFlag) {
                return $this->returnError('ERR_PART_TYPE_DELETE',null);
            } else {
                return $this->returnError('ERR_DEPT_DELETE',null);
            }
        }
    }

    /**
     * This function is used to get department related stations
     **/
    public function getRelatedDepartmentStations($data)
    {
        $departmentID = ($data['departmentID'] == "") ? null: $data['departmentID'];
        $result = $this->getModel('DepartmentStationTable')->getDepartmentRelatedStations($departmentID);

        $departmentStations = [];
        foreach ($result as $key => $value) {
            $departmentStations[$value['departmentStationCode']] = $value['departmentStationName'];
        }
        
        if ($result) {
            return $this->returnSuccess($departmentStations,null);
        } else {
            return $this->returnError('ERR_RETRIVE_DEP_RELATED_STAT',null);
        }
            
    } 

    /**
     * This function is used to get department related stations by departmentId
     **/
    public function getStationByDepartmentId($data)
    {
        $departmentID = ($data['departmentID'] == "") ? null: $data['departmentID'];
        $result = $this->getModel('DepartmentStationTable')->getDepartmentRelatedStations($departmentID);

        $departmentStations = [];
        foreach ($result as $key => $value) {
            $departmentStations[] = ['name' => $value['departmentStationName'], 'code' => $value['departmentStationCode'], 'status' => $value['departmentStationStatus']];
        }
        
        if ($result) {
            return $this->returnSuccess($departmentStations,null);
        } else {
            return $this->returnError('ERR_RETRIVE_DEP_RELATED_STAT',null);
        }
            
    }

    /**
     * This function is used to change the department station status
     **/
    public function changeStationStatus($data)
    {
        $departmentID = ($data['departmentId']) ? $data['departmentId']: null;
        $stationCode = ($data['stationCode']) ? $data['stationCode']: null;
        $status = ($data['stationStatus']) ? $data['stationStatus']: null;

        $result = $this->getModel('DepartmentStationTable')->changeStationStatus($departmentID, $stationCode, $status);

        if ($result) {
            return $this->returnSuccess(null,'SUCC_STATION_STATUS_CHANGE');
        } else {
            return $this->returnError('ERR_STATION_STATUS_CHANGE',null);
        }
            
    }

    /**
     * This function is used to delete department related stations
     **/
    public function deleteStation($data)
    {
        $departmentID = ($data['departmentId']) ? $data['departmentId']: null;
        $stationCode = ($data['stationCode']) ? $data['stationCode']: null;

        $result = $this->getModel('DepartmentStationTable')->deleteStationByStationCode($departmentID, $stationCode);

        if ($result) {
            return $this->returnSuccess(null,'SUCC_STATION_DELETE');
        } else {
            return $this->returnError('ERR_STATION_DELETE',null);
        }
            
    }

    /**
     * This function is used to save department stations
     **/
    public function insertDepartmentStation($departmentStationsData, $savedResult)
    {
        foreach ($departmentStationsData as $key => $val) {
            $stationData = array(
                'departmentStationCode' => $val['code'],
                'departmentStationName' => $val['name'],
                'departmentId' => $savedResult,
            );

            $departmentStatData = new DepartmentStation();
            $departmentStatData->exchangeArray($stationData);

            $savedStationResult = $this->getModel('DepartmentStationTable')->saveDepartmentStation($departmentStatData);
            if (!$savedStationResult) {
                return false; 
                return $this->returnError('ERR_JOB_DEPT_STAT_SAVE', 'DEPARTMENT_STATION_SAVE_FAILD');
            }
        }
        return true;
            
    }


    /**
     * This function is used to update department State
     **/
    public function updateDepartmentState($data)
    {
        $departmentID = $data['departmentID'];
        $status = $data['status'];
        $tournamentFlag = $data['tournamentFlag'];
        $result = $this->getModel('DepartmentTable')->updateDepartmentState($departmentID, $status);

        if ($tournamentFlag) {
            if ($result) {
                return $this->returnSuccess('PARTICIPANT_TYPE_STATE_SUCESSFULLY_UPDATE', 'SUCC_PART_TYPE_STATUS_UPDATE');
            } else {
                return $this->returnError('ERR_PART_TYPE_STATE_UPDATE', 'PARTICIPANT_TYPE_STATE_FAILD_UPDATE');
            }
        } else {
            if ($result) {
                return $this->returnSuccess('DEPARTMENT_STATE_SUCESSFULLY_UPDATE', 'SUCC_DEPT_STATUS_UPDATE');
            } else {
                return $this->returnError('ERR_DEPT_STATE_UPDATE', 'DEPARTMENT_STATE_FAILD_UPDATE');
            }
        }
    }
}