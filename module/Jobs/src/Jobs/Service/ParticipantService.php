<?php

namespace Jobs\Service;

use Core\Service\BaseService;
use Jobs\Model\Employee;
use Jobs\Model\EmployeeDesignation;
use Jobs\Model\EmployeeDepartment;
use Zend\View\Model\ViewModel;

class ParticipantService extends BaseService {
	
	/**
    * this function use to save employee.
    * @param array $data
    * @param int $userId
    * return array
    **/
    public function createParticipant($data, $userId)
    {
        
        $employeeCode = $data['employeeCode'];
        $employeeFirstName = $data['employeeFirstName'];
        $employeeSecondName = $data['employeeSecondName'];
        $employeeAddress = $data['employeeAddress'];
        $employeeTP = $data['employeeTP'];
        $employeeEmail = $data['employeeEmail'];
        $employeeIDNo = $data['employeeIDNo'];
        $employeeGender = ($data['employeeGender'] == "male") ? 1 : 0;
        $employeeDOB = $this->convertDateToStandardFormat($data['employeeDOB']);
        $employeeParticipantSubType = $data['employeeParticipantSubType'];
        $employeeParticipantType = $data['employeeParticipantType'];
        $employeeID = null;

        $result = $this->getModel('EmployeeTable')->getEmployeeByCode($employeeCode);
        $employeeIDNoDataset = $this->getModel('EmployeeTable')->checkEmployeeIDNovalid($employeeIDNo);

        if ($result->count() > 0) { 
            return $this->returnError('ERR_PART_CODE_ALREDY_EXIST', 'EMPLOYEE_EXIST');
        }
        if ($employeeIDNoDataset->count() > 0) { 
            return $this->returnError('ERR_PART_ID_NO_ALREDY_EXIST', 'EMPLOYEE_ID_NO_EXIST');
        }

        $entityService = $this->getService('EntityService');
        $entityID = $entityService->createEntity($userId)['data']['entityID'];

        $data = array(
            'employeeID' => $employeeID,
            'employeeCode' => $employeeCode,
            'employeeFirstName' => $employeeFirstName,
            'employeeSecondName' => $employeeSecondName,
            'employeeAddress' => $employeeAddress,
            'employeeTP' => $employeeTP,
            'employeeEmail' => $employeeEmail,
            'employeeIDNo' => $employeeIDNo,
            'employeeDOB' => $employeeDOB,
            'employeeGender' => $employeeGender,
            'entityID' => $entityID
        );


        $employeeData = new Employee();
        $employeeData->exchangeArray($data);
        
        $savedResult = $this->getModel('EmployeeTable')->saveEmployees($employeeData);

        if (!$savedResult) {
            return $this->returnError('ERR_PART_SAVE', 'EMPLOYEE_SAVE_FAILD');
        } else {

            foreach ($employeeParticipantSubType as $key => $value) {
                $employeeDesignationID = null;
                $data = [
                    'employeeDesignationID' => $employeeDesignationID,
                    'employeeID' => $savedResult,
                    'designationID' => $value
                ];

                $employeeDesignationData = new EmployeeDesignation();
                $employeeDesignationData->exchangeArray($data);

                $saveEmployeeDesignations = $this->getModel('EmployeeDesignationTable')->saveEmployeeDesignation($employeeDesignationData);
            
                if (!$saveEmployeeDesignations) {
                    return $this->returnError('ERR_PART_SAVE', 'EMPLOYEE_SAVE_FAILD');
                }
            }

            foreach ($employeeParticipantType as $key => $value) {
                $employeeDepartmentID = null;
                $empDeptdata = [
                    'employeeDepartmentID' => $employeeDepartmentID,
                    'employeeId' => $savedResult,
                    'departmentId' => $value
                ];
                $employeeDepartmentData = new EmployeeDepartment();
                $employeeDepartmentData->exchangeArray($empDeptdata);


                $saveEmployeeDepartments = $this->getModel('EmployeeDepartmentTable')->saveEmployeeDepartment($employeeDepartmentData);
                
                if (!$saveEmployeeDepartments) {
                    return $this->returnError('ERR_PART_SAVE', 'EMPLOYEE_SAVE_FAILD');
                }
            }

            $this->setLogMessage('Participant '.$employeeCode.' is created.');
            return $this->returnSuccess($savedResult, 'SUCC_PART_SAVE');
        }
    }

    /**
    * this function use to update employee.
    * @param array $data
    * @param int $userId
    * return array
    **/
    public function updateEmployee($data, $userId)
    {
        $checkCodeValid = $this->getModel('EmployeeTable')->checkEmployeeCodeValid($data['employeeID'], $data['employeeCode']);
        $employeeDesignations = $data['employeeParticipantSubType'];
        $employeeDepartments = $data['employeeParticipantType'];
        $employeeIDNo = $data['employeeIDNo'];
        $oldData = $this->getModel('EmployeeTable')->getEmployeeByID($data['employeeID']);
        $employeeIDNoDataset = $this->getModel('EmployeeTable')->checkEmployeeIDNovalid($employeeIDNo, $data['employeeID'], true);
        
        if ($checkCodeValid > 0) {
            return $this->returnError('ERR_PART_CODE_ALREDY_EXIST', 'EMPLOYEE_EXIST');
        }

        if ($employeeIDNoDataset->count() > 0) { 
            return $this->returnError('ERR_PART_ID_NO_ALREDY_EXIST', 'EMPLOYEE_ID_NO_EXIST');
        }

        $getInactiveEmployeeDesignations = $this->getModel('EmployeeDesignationTable')->getInactiveEmployeeDesignations($data['employeeID']);

        $getInactiveEmployeeDepartments = $this->getModel('EmployeeDepartmentTable')->getInactiveEmployeeDepartments($data['employeeID']);

        $oldEmpId = $data['employeeID'];
        $entityService = $this->getService('EntityService');
        $deleteRecord = $entityService->updateDeleteInfoEntity($data['entityID'], $userId);
        $entityID = $entityService->createEntity($userId)['data']['entityID'];

        $data['entityID'] = $entityID;
        $data['employeeDOB'] = $this->convertDateToStandardFormat($data['employeeDOB']);
        $empNewData = $data;
        $employeeData = new Employee();
        $employeeData->exchangeArray($data);

        $savedResult = $this->getModel('EmployeeTable')->saveEmployees($employeeData);
        if (!$savedResult) {
            return $this->returnError('ERR_PART_UPDATE', 'EMPLOYEEE_FAILD_UPDATE');
        } else {

            // $updateProjectManagersTable = $this->getModel('ProjectManagerTable')->updateProjectManagersTableWithNewEmpId($data['employeeID'], $savedResult);
            // $updateProjectSupervisorTable = $this->getModel('ProjectSupervisorTable')->updateProjectSupervisorsTableWithNewEmpId($data['employeeID'], $savedResult);

            foreach ($getInactiveEmployeeDesignations as $key => $value) {
                array_push($employeeDesignations, $value['designationID']);
            }
    
            foreach ($employeeDesignations as $key => $value) {
                $employeeDesignationID = null;
                $data = [
                    'employeeDesignationID' => $employeeDesignationID,
                    'employeeID' => $savedResult,
                    'designationID' => $value
                ];

                $employeeDesignationData = new EmployeeDesignation();
                $employeeDesignationData->exchangeArray($data);

                $saveEmployeeDesignations = $this->getModel('EmployeeDesignationTable')->saveEmployeeDesignation($employeeDesignationData);
            
                if (!$saveEmployeeDesignations) {
                    return $this->returnError('ERR_PART_UPDATE', 'EMPLOYEE_SAVE_FAILD');
                }
            }

            foreach ($getInactiveEmployeeDepartments as $key => $value) {
                array_push($employeeDepartments, $value['departmentID']);
            }
            
            foreach ($employeeDepartments as $key => $value) { 
                $countOfEmpDeptByIds = $this->getModel('EmployeeDepartmentTable')->getEmpDeptByIds($value, $oldEmpId);

                if (count($countOfEmpDeptByIds) > 0) {
                    $updateEmpDeptRecord = $this->getModel('EmployeeDepartmentTable')->updateEmpRecordWithNewEmpID($value, $oldEmpId, $savedResult);

                    if (!$updateEmpDeptRecord) {
                        return $this->returnError('ERR_PART_UPDATE', 'EMPLOYEE_SAVE_FAILD');
                    }
                } else {
                    $employeeDepartmentID = null;
                    $empDeptdata = [
                        'employeeDepartmentID' => $employeeDepartmentID,
                        'employeeId' => $savedResult,
                        'departmentId' => $value
                    ];
                    $employeeDepartmentData = new EmployeeDepartment();
                    $employeeDepartmentData->exchangeArray($empDeptdata);


                    $saveEmployeeDepartments = $this->getModel('EmployeeDepartmentTable')->saveEmployeeDepartment($employeeDepartmentData);
                    
                    if (!$saveEmployeeDepartments) {
                        return $this->returnError('ERR_PART_UPDATE', 'EMPLOYEE_SAVE_FAILD');
                    }
                }
            }
            $changeArray = $this->getParticipantChageDetails($oldData, $empNewData);
            //set log details
            $previousData = json_encode($changeArray['previousData']);
            $newData = json_encode($changeArray['newData']);

            $this->setLogDetailsArray($previousData,$newData);
            $this->setLogMessage("Participant ".$oldData->employeeCode." is updated.");
            return $this->returnSuccess($savedResult, 'SUCC_PART_UPDATE');
        }
    }

    public function getParticipantChageDetails($row, $data)
    {
        $previousData = [];
        $previousData['Participant code'] = $row->employeeCode;
        $previousData['Participant first name'] = $row->employeeFirstName;
        $previousData['Participant second name'] = $row->employeeSecondName;
        $previousData['Participant address'] = $row->employeeAddress;
        $previousData['Participant email'] = $row->employeeEmail;
        $previousData['Participant telephone'] = $row->employeeTP;
        $previousData['Participant ID no'] = $row->employeeIDNo;
        $previousData['Participant DOB'] = $row->employeeDOB;

        $newData = [];
        $newData['Participant code'] = $data['employeeCode'];
        $newData['Participant first name'] = $data['employeeFirstName'];
        $newData['Participant second name'] = $data['employeeSecondName'];
        $newData['Participant address'] = $data['employeeAddress'];
        $newData['Participant email'] = $data['employeeEmail'];
        $newData['Participant telephone'] = $data['employeeTP'];
        $newData['Participant ID no'] = $data['employeeIDNo'];
        $newData['Participant DOB'] = $data['employeeDOB'];
        return array('previousData' => $previousData, 'newData' => $newData);
    }

    /**
    * this function use to update employee state.
    * @param array $data
    * return array
    **/
    public function updateEmployeeState($data)
    {
        $employeeID = $data['employeeID'];
        $status = $data['status'];
        $oldData = $this->getModel('EmployeeTable')->getEmployeeByID($data['employeeID']);
        $result = $this->getModel('EmployeeTable')->updateEmployeeState($employeeID, $status);

        if ($result) {
            if ($status == 0) {
                $this->setLogMessage("Status of participant ".$oldData->employeeCode." is deactived");
            } else {
                $this->setLogMessage("Status of participant ".$oldData->employeeCode." is actived");
            }
            return $this->returnSuccess('EMPLOYEE_STATE_SUCESSFULLY_UPDATE', 'SUCC_PART_STATUS_UPDATE');
        } else {
            return $this->returnError('ERR_PART_STATE_UPDATE', 'EMPLOYEE_STATE_FAILD_UPDATE');
        }

    }

    /**
    * this function use to search employee by key, Department, Designation
    * @param array $data
    * @param int $param
    * return array
    **/
    public function employeeSearchByKey($data, $param)
    {
        $paginated = false;
        $key = ($data['searchKey'] == '') ? null : $data['searchKey'];
        $designationID = ($data['searchDesignationID'] == '') ? null : $data['searchDesignationID'];
        $departmentID = ($data['searchDepartmentID'] == '') ? null : $data['searchDepartmentID'];

        if (!empty($key) || !empty($designationID) || !empty($departmentID)) {
            $EmployeeList = $this->getModel('EmployeeTable')->employeeSearchByKey($key, $designationID, $departmentID, false, false, "serviceEmp");
            $status = true;
        } else {
            $status = true;
            $employeeController = $this->getServiceLocator()->get("EmployeeController");
            $EmployeeList = $employeeController->getPaginatedEmployees(6, $param);
            $paginated = true;
        }
       
        $view = new ViewModel(array(
            'employees' => $EmployeeList,
            'paginated' => $paginated,
        ));

        $view->setTerminal(true);
        $view->setTemplate('jobs/participant/list.phtml');
        
        return [
            'view' => $view,
            'status' => $status
        ];

    }

    /**
    * this function use to get designation details according to the employeeID
    * @param int $id
    * return array
    **/
    public function getEmployeeDesignations ($id) {
        $employeeDesignations = $this->getModel('EmployeeDesignationTable')->getEmployeeDesignations($id);
        $data = [];

        foreach ($employeeDesignations as $key => $value) {
            array_push($data, $value);
        }

        $view = new ViewModel(array(
            'designations' => $data,
            'paginated' => false,
        ));

        $view->setTerminal(true);
        $view->setTemplate('jobs/participant/participant-sub-type-view.phtml');
        
        return [
            'view' => $view,
            'status' => true,
            'data' => $data
        ];
    }

    /**
     * This function is used to delete employee
     **/
    public function deleteEmployee($data, $userId)
    {
        $employeeID = $data['employeeID'];
        if (!empty($employeeID)) {

            $checkEmployeeUsedInTeam = $this->getModel('EmployeeTeamTable')->checkEmployeeUsed($employeeID);
            $oldData = $this->getModel('EmployeeTable')->getEmployeeByID($employeeID);
            if ($checkEmployeeUsedInTeam) {
                $entityService = $this->getService('EntityService');
                $result = $entityService->updateDeleteInfoEntity($data['entityID'], $userId);
                if ($result) {
                    $this->setLogMessage('Participant '.$oldData->employeeCode.' is deleted.');
                    return $this->returnSuccess(array($taskCode),'SUCC_PART_DELETE');
                } else {
                    return $this->returnError('ERR_PART_DELETE',null);
                }
            } else {
                return $this->returnError('ERR_PART_ALREADY_USED',null);
            }
           
        } else {
            return $this->returnError('ERR_PART_DELETE',null);
        }
    }

    /**
	 * This function is used to search designation wise for dropdown
	 */
	public function searchJobDesignationWiseEmployeeForDropdown($data) {
		$searchKey = $data['searchKey'];
		$employees = $this->getModel('EmployeeDesignationTable')->getDesignationWiseEmployeeBySearchKey($searchKey);
		$employeeList = array();
		foreach ($employees as $emp) {
			$temp['value'] = $emp['employeeDesignationID'];
			$temp['text'] = $emp['employeeFirstName'] . ' ' . $emp['employeeSecondName'] . ' (' . $emp['designationName'] . ')';
			$employeeList[] = $temp;
		}
		return $employeeList;
	}

	public function getDepartmentWiseEmployeesForDropdown($data) {
        $participantTypeID = $data['participantTypeID'];
        $participantSubTypeID = $data['participantSubTypeID'];

        $employees = $this->getModel('EmployeeTable')->getEmployeeByDepartmentIDAndDesignationID($participantTypeID, $participantSubTypeID);

        $employeeList = array();
        foreach ($employees as $emp) {
            $employeeList[$emp['employeeID']] = $emp['employeeCode'].' - '.$emp['employeeFirstName'];
        }

        return $employeeList;
    }

    public function getParticipants()
    {
        $employeesData = $this->getModel('EmployeeTable')->fetchActiveEmployees();
        $employees = array();
        foreach ($employeesData as $employee) {
            $employees[$employee['employeeID']] = $employee['employeeCode'].' - '.$employee['employeeFirstName'];
        }

        $departmentData = $this->getModel('DepartmentTable')->fetchActiveDepartments();
        $departments = array();
        foreach ($departmentData as $dept) {
            $departments[$dept['departmentID']] = $dept['departmentCode'].' - '.$dept['departmentName'];
        }

        $designationData = $this->getModel('DesignationTable')->fetchActiveDesignations();
        $designations = array();
        foreach ($designationData as $desig) {
            $designations[$desig['designationID']] = $desig['designationCode'].' - '.$desig['designationName'];
        }

        return ['participantData' => $employees, 'participantTypeData' => $departments, 'participantSubTypeData' => $designations];
    }

    public function getEmployeeDetailsByEmployeeID($data) {
		$superviosrDetails = $this->getModel('EmployeeTable')->getEmployeeByID($data['employeeID']);

		return $superviosrDetails;
	}

    /**
     * This function is used to search job supervisors for dropdown
     */
    public function searchJobEmployeeForDropdown($data, $superviosrFlag, $managerFlag) {
        $searchKey = $data['searchKey'];
        $superviosrs = $this->getModel('EmployeeTable')->employeeSearchByKey($searchKey, null, null, $superviosrFlag, $managerFlag);
        $supervisorList = array();
        foreach ($superviosrs as $sup) {
            $temp['value'] = $sup['employeeID'];
            $temp['text'] = $sup['employeeFirstName'] . ' ' . $sup['employeeSecondName'];
            $supervisorList[] = $temp;
        }
        return $supervisorList;
    }

    /**
    * this function use to get department details according to the employeeID
    * @param int $id
    * return array
    **/
    public function getPartcipantTypes($id) {
        $employeeDepartments = $this->getModel('EmployeeDepartmentTable')->getEmployeeDepartments($id);
        $data = [];

        foreach ($employeeDepartments as $key => $value) {
            array_push($data, $value);
        }

        $view = new ViewModel(array(
            'departments' => $data,
            'paginated' => false,
        ));

        $view->setTerminal(true);
        $view->setTemplate('jobs/participant/participant-type-view.phtml');
        
        return [
            'view' => $view,
            'status' => true,
            'data' => $data
        ];
    }

    public function getUploadedAttachemnts($id) {
        $uploadedAttachments = [];
        $attachmentData = $this->getModel('DocumentAttachementMappingTable')->getDocumentRelatedAttachmentsByDocumentIDAndDocumentTypeID($id, 35);
        $companyName = $this->getSubdomain();
        $path = '/userfiles/' . md5($companyName) . "/attachments";

        foreach ($attachmentData as $value) {
            $temp = [];
            $temp['documentAttachemntMapID'] = $value['documentAttachemntMapID'];
            $temp['documentRealName'] = $value['documentRealName'];
            $temp['docLink'] = $path . "/" .$value['documentName'];
            $uploadedAttachments[$value['documentAttachemntMapID']] = $temp;
        }

        
        return [
            'status' => true,
            'data' => $uploadedAttachments
        ];
    }

    /**
     * This function is used to Employee related department
     **/
    public function changeStatusOfEmployeeeDepartment($data)
    {
        $employeeDeaprtmentID = ($data['employeeDeaprtmentID']) ? $data['employeeDeaprtmentID']: null;
        $status = $data['status'];
        $result = $this->getModel('EmployeeDepartmentTable')->changeStatusOfEmployeeeDepartment($employeeDeaprtmentID, $status);

        if ($result) {
            return ($type == 'service') ? $this->returnSuccess(null,'SUCC_SER_STATUS_CHANGE') : $this->returnSuccess(null,'SUCC_EMP_DEP_STATUS_CHANGE');
        } else {
            return ($type == 'service') ? $this->returnError('ERR_SER_STATUS_CHANGE',null) : $this->returnError('ERR_EMP_DEP_STATUS_CHANGE',null);
        }
            
    }

}