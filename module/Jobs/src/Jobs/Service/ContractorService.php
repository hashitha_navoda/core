<?php

namespace Jobs\Service;

use Core\Service\BaseService;
use Zend\I18n\Translator\Translator;
use Zend\View\Model\ViewModel;
use Jobs\Model\Contractor;

class ContractorService extends BaseService
{
	public function saveContractor($data)
	{
		$entityId = $this->getService('EntityService')->createEntity($this->userID)['data']['entityID'];
		
		$ContractorData = array(
			'contractorFirstName' => $data['contractorFirstName'],
			'contractorSecondName' => $data['contractorSecondName'],
			'contractorTP' => $data['contractorTP'],
			'entityID' => $entityId,
			'IsPermanentFlag' => $data['IsPermanentFlag'] == "true" ? 1 : 0
		);
			

		$contractorRespond = $this->getModel('ContractorTable')->saveContractor($ContractorData);
		if ($contractorRespond) {
			return $this->returnSuccess(array('contractorID' => $contractorRespond), 'SUCC_CONTRACTOR_SAVE');
		} else {
            return $this->returnError('ERR_CONTRACTOR_SAVE', null);
		}

	}

	public function editContractor($data, $params, $userId)
	{
		$contractorID = $params['param1'];
		$entityID = $params['param2'];
		$contractorData = array(
			'contractorFirstName' => $data['contractorFirstName'],
			'contractorSecondName' => $data['contractorSecondName'],
			'contractorTP' => $data['contractorTP'],
			'IsPermanentFlag' => $data['IsPermanentFlag'] == "true" ? 1 : 0
		);

		$contractorRespond = $this->getModel('ContractorTable')->editContractor($contractorData, $contractorID);
		if ($contractorRespond) {
			return $this->returnSuccess(array('ContractorData' => $ContractorData), 'SUCC_CONTRACTOR_EDIT');
		} else {
            return $this->returnError('ERR_CONTRACTOR_EDIT', null);
		}
	}

	public function deleteContractor($data, $userId)
	{
		$contractorDependability = $this->getModel('ContractorTable')->contractorDependability($data->contractorID)->current();
		if ($contractorDependability) {
			$entityID = $this->getService('EntityService')->updateDeleteInfoEntity($contractorDependability['entityID'], $userId);
			return $this->returnSuccess(array('entityID' => $entityID), 'SUCC_CONTRACTOR_DELETE');
		} else {
			return $this->returnError('ERR_CONTRACTOR_DELETE', null);
		}
	}

	public function contractorSearchByKey($contractorSearchKey, $param)
    {
        $paginated = false;
        if (!empty($contractorSearchKey)) {
            $ContractorList = $this->getModel('ContractorTable')->contractorSearchByKey($contractorSearchKey);
            $status = true;
        } else {
            $status = true;
            $contractorController = $this->getServiceLocator()->get("ContractorController");
            $ContractorList = $contractorController->getPaginatedContractor(6, $param);
            $paginated = true;
        }
       
        $view = new ViewModel(array(
            'contractorList' => $ContractorList,
            'paginated' => $paginated,
        ));

        $view->setTerminal(true);
        $view->setTemplate('jobs/contractor/list.phtml');
        
        return [
            'view' => $view,
            'status' => $status
        ];

    }

    /**
    * this function use to change status of contractor details
    **/
	public function changeStatusContractor($data, $userId)
	{
		if ($data['contractorStatus'] == 1) {
		    $updatedContracterStatus = 2;
		} else if ($data['contractorStatus'] == 2) {
		    $updatedContracterStatus = 1;
		}
		$flagStatusChange = $this->getModel('ContractorTable')->changeContracterStatus($data['contractorID'], $updatedContracterStatus);

		if ($flagStatusChange) {
			$flagEntityUpdate = $this->getService('EntityService')->updateEntity($data->entityId, $userId);
			return $this->returnSuccess(array('contractorStatus' => $updatedContracterStatus), 'SUCC_CONTRACTOR_STATUS_CHANGE');
		} else {
			return $this->returnError('ERR_CONTRACTOR_STATUS_CHANGE', null);
		}
	}
}