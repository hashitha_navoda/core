<?php

namespace Jobs\Service;

use Core\Service\BaseService;
use Zend\I18n\Translator\Translator;
use Zend\View\Model\ViewModel;
use Jobs\Model\JobProduct;

class ResourceManagementService extends BaseService {

	/**
	 * This function is used to load jobproducts by jobId
	 **/
	public function loadJobProductsByJobId($data)
	{
        $JobProducts = $this->getModel('JobProductTable')->jobProductsByJobId($data['jobId']);
        $JobMateReq = $this->getModel('MaterialRequestTable')->getTaskByJobs($data['jobId']);
        $availableJobProductList = [];
        $unavailableJobProductList = [];
        $jobMaterialRequest = [];
        
        if ($JobProducts) {
        	foreach ($JobProducts as $value) {
                if (floatval($value['jobProductIssuedQty']) > 0) {
                    $availableJobProductList[$value['jobProductMaterialTypeID']][$value['taskID']][$value['jobProductID']] = $value;
                    $availableJobProductList[$value['jobProductMaterialTypeID']][$value['taskID']][$value['jobProductID']] = array(
                        'jobID' => $value['jobID'],
                        'jobProductID' => $value['jobProductID'],
                        'locationProductID' => $value['locationProductID'],
                        'productID' => $value['productID'],
                        'productCode' => $value['productCode'],
                        'productName' => $value['productName'],
                        'uomAbbr' => $value['uomAbbr'],
                        'uomName' => $value['uomName'],
                        'taskCode' => $value['taskCode'],
                        'taskName' => $value['taskName'],
                        'jobProductAllocatedQty' => floatval($value['jobProductAllocatedQty']),
                        'jobProductIssuedQty' => floatval($value['jobProductIssuedQty']),
                        'jobProductReOrderLevel' => floatval($value['jobProductReOrderLevel']),
                        'jobProductRequestedQty' => floatval($value['jobProductRequestedQty']),
                        'jobProductUnitPrice' => floatval($value['jobProductUnitPrice']),
                        'jobProductUsedQty' => floatval($value['jobProductUsedQty']),
                    );
                } else {
                    $unavailableJobProductList[$value['jobProductMaterialTypeID']][$value['taskID']][$value['jobProductID']] = $value;
                    $unavailableJobProductList[$value['jobProductMaterialTypeID']][$value['taskID']][$value['jobProductID']] = array(
                        'jobID' => $value['jobID'],
                        'jobProductID' => $value['jobProductID'],
                        'locationProductID' => $value['locationProductID'],
                        'productID' => $value['productID'],
                        'productCode' => $value['productCode'],
                        'productName' => $value['productName'],
                        'uomAbbr' => $value['uomAbbr'],
                        'uomName' => $value['uomName'],
                        'taskCode' => $value['taskCode'],
                        'taskName' => $value['taskName'],
                        'jobProductAllocatedQty' => floatval($value['jobProductAllocatedQty']),
                        'jobProductIssuedQty' => floatval($value['jobProductIssuedQty']),
                        'jobProductReOrderLevel' => floatval($value['jobProductReOrderLevel']),
                        'jobProductRequestedQty' => floatval($value['jobProductRequestedQty']),
                        'jobProductUnitPrice' => floatval($value['jobProductUnitPrice']),
                        'jobProductUsedQty' => floatval($value['jobProductUsedQty']),
                    );
                }
        		
        	}
            foreach ($JobMateReq as $value) {
                $jobMaterialRequest[$value['jobProductId']][$value['materialRequestId']] = array(
                    'jobId' => $value['jobId'],
                    'jobProductId' => $value['jobProductId'],
                    'materialRequestId' => $value['materialRequestId'],
                    'materialRequestCode' => $value['materialRequestCode'],
                    'materialRequestDate' => $value['materialRequestDate'],
                    'materialRequestStatus' => $value['materialRequestStatus'],
                    'requestedQuantity' => $value['requestedQuantity'],
                );
            }
        	return $this->returnSuccess(array(
                'availableJobProductList' => $availableJobProductList, 
                'unavailableJobProductList' => $unavailableJobProductList, 
                'jobMaterialRequest' => $jobMaterialRequest
            ), 'SUCC_LOAD_RM_PRODUCT');
        } else {
        	return $this->returnError('ERR_LOAD_RM_PRODUCT', null);
        }
	}

	/**
	 * This function is used to update jobProducts
	 **/
	public function update($data)
	{
		$errorFlag = true;
        foreach ($data['updateData'] as $value) {
        	$updateData = array(
                'jobProductUsedQty' => $value['jobProductUsedQty'],
            );
	        $JobProducts = $this->getModel('JobProductTable')->update($updateData, $value['jobProductID']);
	        if (!$JobProducts) {
	        	$errorFlag = false;
	        }
        }
        if ($errorFlag) {
        	return $this->returnSuccess(null, 'SUCC_UPDATE_RM_PRODUCT');
        } else {
        	return $this->returnError('ERR_UPDATE_RM_PRODUCT', null);
        }
	}

    /**
     * This function is used to get job by search key
     */
    public function getJobServiceBySearchKey($data, $param, $locationID) {
        $paginated = false;
        $jobSearchKey = ($data['jobSearchKey'] == "") ? null: $data['jobSearchKey'];
        $jobCard = ($data['jobCard'] == "") ? null : $data['jobCard'];
        $serviceType = ($data['serviceType'] == "") ? null : $data['serviceType'];
        $serviceEndingDate = ($data['serviceEndingDate'] == "") ? null : $data['serviceEndingDate'];
        $serviceStartingDate = ($data['serviceStartingDate'] == "") ? null : $data['serviceStartingDate'];

        if (!is_null($serviceEndingDate)) {
            $serviceStartingDate = $this->getGMTDateTime('Y-m-d H:i:s', $serviceStartingDate)['data']['currentTime'];
            $serviceEndingDate = $this->getGMTDateTime('Y-m-d H:i:s', $serviceEndingDate)['data']['currentTime'];
        }

        $MaterialManagementController = $this->getServiceLocator()->get("MaterialManagementController");
        if ($jobSearchKey != null || $jobCard != null || $serviceType != null || $serviceEndingDate != null) {
            $tbl = $this->getModel('JobTable');
            $jobList = $tbl->getJobServiceBySearchKey($jobSearchKey,$jobCard, $serviceType, $serviceStartingDate, $serviceEndingDate);
        } else {
            $jobList = $MaterialManagementController->getPaginatedStartedJobs($param);
        }

        $serviceStartedTimeData = $MaterialManagementController->getTaskStartedTime();
        $view = new ViewModel(array(
            'jobList' => $jobList,
            'serviceStartedTimeData' => $serviceStartedTimeData,
            'paginated' => $paginated,
            'status' => $this->getStatusesList(),
        ));

        $view->setTerminal(true);
        $view->setTemplate('jobs/material-management/list-table.phtml');

        return $view;
    }


    public function getJobServiceByJobServiceId($data) {
        $jobServiceData = $this->getModel('JobTable')->getJobServiceByJobServiceID($data['jobServiceID']);
            
        $jobServiceDetails = [];
        foreach ($jobServiceData as $key => $row) {
            $tempG = array();
            $jobID = $row['jobId'];
            $jobTaskID = $row['jobTaskID'];
            $tempG['jobId'] = $row['jobId'];
            $tempG['jobId'] = $row['jobTaskID'];
            $tempG['jobCode'] = $row['jobReferenceNumber'];
            $tempG['jobSupervisorName'] = $row['userFirstName'].' '.$row['userLastName'];
            $tempG['serviceName'] = $row['taskName'];
            $tempG['startedTime'] = $this->getUserDateTime($row['startedAt']);
            $jobProducts = (isset($jobServiceDetails[$row['jobTaskID']]['jobProducts'])) ? $jobServiceDetails[$row['jobTaskID']]['jobProducts'] : array();

            if (!is_null($row['jobProductID'])) {

                $productUom = $this->getModel('ProductUomTable')->getProductUomListByProductID($row['productID']);

                $thisqty = $this->getProductQuantityViaDisplayUom($row['jobProductAllocatedQty'], $productUom);
                $thisqty['quantity'] = ($thisqty['quantity'] == 0) ? '-' : $thisqty['quantity'];

                $jobProducts[$row['jobProductID']] = array('locationProductID' => $row['locationProductID'],'jobProductID' => $row['jobProductID'],'productID' => $row['productID'], 'jobProductUnitPrice' => $row['jobProductUnitPrice'], 'uomID' => $row['uomID'], 'jobProductAllocatedQty' => $thisqty['quantity'],'jobProductIssuedQty' => $row['jobProductIssuedQty'], 'jobProductReOrderLevel' => $row['jobProductReOrderLevel'], 'jobProductMaterialTypeID' => $row['jobProductMaterialTypeID'],'productCode' => $row['productCode'], 'productName' => $row['productName'],'uomAbbr' => $row['uomAbbr'], 'productEstimatedCost' => floatval($row['jobProductAllocatedQty']) * floatval($row['jobProductUnitPrice']));
            }

            $tempG['jobProducts'] = $jobProducts;
            $jobServiceDetails[$row['jobTaskID']] = $tempG;
        
        }

        return $this->returnSuccess($jobServiceDetails[$jobTaskID],'success');
    }


    public function updateServiceMaterials($data) {
        $serviceMaterials = $data['serviceMaterials'];
        $jobID = $data['jobId'];
        $jobServiceID = $data['jobServiceID'];
        $saveMaterialType = $data['saveMaterialType'];

        $productData = [];

        if ($data['saveMaterialType'] == 'both') {
            $saveMaterialType = "trade";
        }

        if ($saveMaterialType == "trade") {
            foreach ($serviceMaterials as $value) {
                $temp = [];
                if ($value['issueQty'] > 0) {
                    if ($value['jobProductMaterialTypeID'] == 2) {
                        if ($value['jobProductID'] == "") {
                            $pData = array(
                                'jobID' => $jobID,
                                'locationProductID' => $value['jobMaterialTaskLocationProductID'],
                                'jobProductUnitPrice' => $value['unitPrice'],
                                'uomID' => $value['jobMaterialSelectedUomID'],
                                'jobProductAllocatedQty' => $value['issueQty'],
                                'jobProductReOrderLevel' => 0,
                                'jobProductMaterialTypeID' => 2,
                                'jobProductTaskCardID' => null,
                                'jobTaskID' => $jobServiceID,
                            );

                            $jobProductData = new JobProduct();
                            $jobProductData->exchangeArray($pData);
                            $jobProductID = $this->getModel('JobProductTable')->saveJobProduct($jobProductData);

                            $temp['jobProductId'] = $jobProductID;
                            $temp['issueQty'] = $value['issueQty'];
                            
                        } else {
                            $temp['jobProductId'] = $value['jobProductID']; 
                            $temp['issueQty'] = $value['issueQty']; 
                        }
                        $productData[] = $temp;          
                    }
                }
            }
        }
        
        if ($data['saveMaterialType'] == 'both') {
            $saveMaterialType = "service";
        }


        if ($saveMaterialType == "service") {
            foreach ($serviceMaterials as $value) {

                if ($value['jobProductID'] == "new") {
                    $pData = array(
                        'jobID' => $jobID,
                        'locationProductID' => $value['jobMaterialTaskLocationProductID'],
                        'jobProductUnitPrice' => $value['unitPrice'],
                        'uomID' => $value['jobMaterialSelectedUomID'],
                        'jobProductAllocatedQty' => $value['jobProductAllocatedQty'],
                        'jobProductEstimatedQty' => $value['jobProductAllocatedQty'],
                        'jobProductReOrderLevel' => $value['jobProductAllocatedQty'],
                        'jobProductMaterialTypeID' => 1,
                        'jobProductTaskCardID' => null,
                        'jobTaskID' => $jobServiceID,
                    );

                    $jobProductData = new JobProduct();
                    $jobProductData->exchangeArray($pData);
                    $jobProductID = $this->getModel('JobProductTable')->saveJobProduct($jobProductData);
                } else {
                    $jobProductID = $value['jobProductID'];
                }


                $temp = [];
                if ($value['jobProductMaterialTypeID'] == 1 && $value['issueQty'] > 0) {
                    $temp['jobProductId'] = $jobProductID; 
                    $temp['issueQty'] = $value['issueQty']; 
                    $productData[] = $temp;          
                }
            }
        }

        if (sizeof($productData) > 0) {
            $materialRequisitionService = $this->getService('MaterialRequisitionService');
            $update = $materialRequisitionService->updateIssueQuantity($productData, $jobID);
            if ($update['status']) {

                $data = [
                    "issueMaterialFlag" => 1
                ];
                //update 
                $updateJobService = $this->getModel('JobTaskTable')->updateJobTask($data, $jobServiceID);

                return $this->returnSuccess(null, $update['msg']);
            } else {
                return $this->returnError($update['msg'], null);
            }
        } else {
            return $this->returnError('ERR_ISSUE_QTY_MATRL_MNGMNT', null);
        }
    }

    public function getJobEmployeeByJobTaskId($data) {
        $jobServices = $this->getModel('JobTaskTable')->getTaskDetailsByJobTaskId($data['jobTaskID'], true);
        $jobEmployeeData = [];
        foreach ($jobServices as $value) {
            $jobEmployeeData[$value['jobTaskID']]['jobTaskID'] = $value['jobTaskID'];
            $jobEmployeeData[$value['jobTaskID']]['employeeCode'] = $value['employeeCode'];
            $jobEmployeeData[$value['jobTaskID']]['employeeName'] = $value['employeeFirstName'].' '.$value['employeeSecondName'];
        }

        return $jobEmployeeData;
    }
}
